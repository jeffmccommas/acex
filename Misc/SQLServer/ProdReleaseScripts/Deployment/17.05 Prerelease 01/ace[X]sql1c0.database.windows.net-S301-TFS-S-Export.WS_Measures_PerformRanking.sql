﻿


/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/



/****** Object:  StoredProcedure [Export].[WS_Measures_PerformRanking]    Script Date: 5/3/2017 9:11:13 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_PerformRanking]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [Export].[WS_Measures_PerformRanking]
GO

/****** Object:  StoredProcedure [Export].[WS_Measures_PerformRanking]    Script Date: 5/3/2017 9:11:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_PerformRanking]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WS_Measures_PerformRanking] AS'
END
GO


-------------------------------------------------------------------------------
--  Name:               [Export].[WS_Measures_PerformRanking]
--  Author:             Philip Victor
--  Description :     Rank the measures for the measure# / measure profile
-----------------------------------------------------------------------------
ALTER PROCEDURE [Export].[WS_Measures_PerformRanking]
        @reportProfileName [VARCHAR](50) ,
        @measureProfileName [VARCHAR](50) ,
        @measureNumber [INT] ,
        @runId [UNIQUEIDENTIFIER],
        @isTrialRun [BIT] = 0
        WITH RECOMPILE          -- we need to recompile the query plans due to the usage of temp tables with dynamic content within our static queries
AS
BEGIN

        SET NOCOUNT ON;

        DECLARE @client_id [INT] ,
                @fuel_type [VARCHAR](100) ,
                @default_actions [VARCHAR](2000) ,
                @actions [VARCHAR](2000) ,
                @is_trial_run BIT ,
                @enduse_list [VARCHAR](1024) ,
                @ordered_enduse_list [VARCHAR](1024) ,
                @criteria_id1 [VARCHAR](50) ,
                @criteria_detail1 [VARCHAR](255) ,
                @criteria_sort1 [VARCHAR](255) ,
                @criteria_id2 [VARCHAR](50) ,
                @criteria_detail2 [VARCHAR](255) ,
                @criteria_sort2 [VARCHAR](255) ,
                @criteria_id3 [VARCHAR](50) ,
                @criteria_detail3 [VARCHAR](255) ,
                @criteria_sort3 [VARCHAR](255) ,
                @criteria_id4 [VARCHAR](50) ,
                @criteria_detail4 [VARCHAR](255) ,
                @criteria_sort4 [VARCHAR](255) ,
                @criteria_id5 [VARCHAR](50) ,
                @criteria_detail5 [VARCHAR](255) ,
                @criteria_sort5 [VARCHAR](255) ,
                @criteria_id6 [VARCHAR](50) ,
                @criteria_detail6 [VARCHAR](255) ,
                @criteria_sort6 [VARCHAR](255) ,
                @tab_char [CHAR](1) = CHAR(9) ,     -- tab character
                @progress_msg [VARCHAR](5000) ,
                @component [VARCHAR](400) = '[Export].[WS_Measures_PerformRanking]' ,
                @count [INT];

        SELECT
            @client_id = [ClientId], @fuel_type = [FuelType]
                FROM [Export].[WSReportProfile]
                WHERE [ProfileName] = @reportProfileName
                        AND [FuelType] IN ('gas', 'electric', 'all');


    -- report settings
        DECLARE @rpt_setting TABLE (
                                    [profile_name] [VARCHAR](100) ,
                                    [default_actions] [VARCHAR](2000)
        );

    -- measure settings
        DECLARE @measure_setting TABLE (
                                       [profile_name] [VARCHAR](100) ,
                                        [actions] [VARCHAR](2000)
        );

    --  grab the report settings we are interesting in
        INSERT @rpt_setting ([profile_name], [default_actions])
                        EXEC [Export].[WS_Measures_PivotReportSetting] @clientId = @client_id, @reportProfileName = @reportProfileName, @settingList = 'measure_default_actions';


        SELECT
        @default_actions = [default_actions]
                FROM @rpt_setting;


    --  grab the measure settings we are interesting in
        INSERT @measure_setting ([profile_name], [actions])
                        EXEC [Export].[WS_Measures_PivotMeasureSetting] @clientId = @client_id, @measureProfileName = @measureProfileName, @settingList = 'actions';

    -- setup the hard modelled measure settings
        SELECT
        @enduse_List = [EnduseList], @ordered_enduse_List = [OrderedEnduseList], @criteria_id1 = [CriteriaId1], @criteria_detail1 = [CriteriaDetail1],
                        @criteria_sort1 = [CriteriaSort1], @criteria_id2 = [CriteriaId2], @criteria_detail2 = [CriteriaDetail2], @criteria_sort2 = [CriteriaSort2],
                        @criteria_id3 = [CriteriaId3], @criteria_detail3 = [CriteriaDetail3], @criteria_sort3 = [CriteriaSort3], @criteria_id4 = [CriteriaId4],
                        @criteria_detail4 = [CriteriaDetail4], @criteria_sort4 = [CriteriaSort4], @criteria_id5 = [CriteriaId5], @criteria_detail5 = [CriteriaDetail5],
                        @criteria_sort5 = [CriteriaSort5], @criteria_id6 = [CriteriaId6], @criteria_detail6 = [CriteriaDetail6], @criteria_sort6 = [CriteriaSort6]
                FROM [Export].[WSMeasureProfile]
                WHERE [ProfileName] = @measureProfileName;

    -- setup the softmodelled measure settings
        SELECT @actions = [actions]
                FROM @measure_setting;

        SET @progress_msg = '@isTrialRun:' + CAST(@isTrialRun AS VARCHAR);
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        SET @progress_msg = 'Perform Measure Ranking start     ...   ' + '@reportProfileName:''' + ISNULL(@reportProfileName, '') + '''; ' + '@measureNumber:'''
                + ISNULL(CAST(@measureNumber AS [VARCHAR]), '') + '''; ' + '@measureProfileName:''' + ISNULL(@measureProfileName, '') + '''; '
                + '@ordered_enduse_list:''' + ISNULL(@ordered_enduse_list, '') + '''; ' + '@isTrialRun:''' + ISNULL(CAST(@isTrialRun AS [VARCHAR]), '') + '''; '
                + '@default_actions:''' + ISNULL(@default_actions, '') + '''; ' + '@actions:''' + ISNULL(@actions, '') + '''; ';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

    -- set the action filter
        SET @actions = REPLACE(@actions, @tab_char, '');

        IF ISNULL(LTRIM(RTRIM(@actions)), '') = ''
        BEGIN
                SET @actions = REPLACE(@default_actions, @tab_char, '');
        END;

        SELECT @progress_msg = 'Action Filter: @actions:''' + @actions + ''';';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        DECLARE @action_filter TABLE (
                        [ActionKey] [VARCHAR](100) PRIMARY KEY   NOT NULL
        );

        INSERT @action_filter ([ActionKey])
                SELECT DISTINCT [param]
                        FROM [dbo].[ufn_split_values](@actions, ',');

    --  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    -- declare the temp tables we will be using
    --  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    -- this table contains the ordered set of enduse(s) that are used to sort and filter the actions
    --
    -- Assumption:  The order the rows are inserted into #odered_enduse_list are the same as the left-to-right ordering of the original comma-separated list
        CREATE TABLE [#WS_Measures_OrderedEnduseList] (
                                            [ordered_enduse] VARCHAR(50) ,
                                            [enduse_order] [INT] IDENTITY(1, 1)
        );

        INSERT [#WS_Measures_OrderedEnduseList] ([ordered_enduse])
                SELECT LTRIM(RTRIM(CAST ([param] AS VARCHAR(50))))
                        FROM [dbo].[ufn_split_values](@ordered_enduse_list, ',');


    --  this table contains the action items we are interested in
        CREATE TABLE [#WS_Measures_ActionItem] (
                                    [ActionItemKey] [INT] NULL ,
                                    [ActionItemDesc] [VARCHAR](100) NULL ,
                                    [MeasureId] [VARCHAR](256) NULL ,  --   this is the [ClientAction].[ActionKey] ... aka ActionKey
                                    [EnergyObjectKey] [VARCHAR](256) NULL ,
                                    [PremiseKey] [INT] NOT NULL ,
                                    [RefCost] [DECIMAL](18, 4) NULL ,
                                    [RefSavings] [DECIMAL](18, 4) NULL
        );
        CREATE INDEX [WS_Measures_ActionItem_idx0] ON [#WS_Measures_ActionItem]([PremiseKey]);

    -- this table contains the actions and measure values for the accounts we are currently processing
        CREATE TABLE [#WS_Measures_FactActionItem] (
                                        [ActionItemKey] [INT] NOT NULL ,   --   this is the [ClientAction].[ClientActionId]
                                        [PremiseKey] [INT] NOT NULL ,
                                        [EnergyObjectKey] [VARCHAR](256) NOT NULL ,
                                        [Payback] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [CO2Savings] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [ROI] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [AnnualUsageElectric] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [AnnualUsageGas] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [AnnualUsageWater] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [AnnualUsagePropane] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [AnnualUsageOil] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [AnnualUsageWood] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [UsageSavingsElectric] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [UsageSavingsGas] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [UsageSavingsWater] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [UsageSavingsPropane] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [UsageSavingsOil] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [UsageSavingsWood] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [DollarSavings] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [Cost] [DECIMAL](18, 4) NOT NULL DEFAULT 0 ,
                                        [Winter] [BIT] NULL DEFAULT 1 ,
                                        [Spring] [BIT] NULL DEFAULT 1 ,
                                        [Summer] [BIT] NULL DEFAULT 1 ,
                                        [Fall] [BIT] NULL DEFAULT 1
        );
        CREATE INDEX [WS_Measures_FactActionItem_idx0] ON [#WS_Measures_FactActionItem]([PremiseKey], [ActionItemKey]);


    -- populate #fact_action_item with accounts that have been mapped to the measure we are processing
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = 'Populating [#WS_Measures_FactActionItem]';

        INSERT INTO [#WS_Measures_FactActionItem] ([ActionItemKey], [PremiseKey], [EnergyObjectKey], [DollarSavings], [Cost])
                SELECT DISTINCT [ca].[ClientActionID] AS [ActionItemKey], [account_list].[Premisekey] AS [PremiseKey],
                                REPLACE([ce].[EnduseKey], 'enduse.', '') AS [EnergyObjectKey], [measure].[AnnualSavingsEstimate] AS [DollarSavings],     -- Done this way to include 'all' fuel type.
                                [measure].[UpfrontCost] AS [Cost]
                        FROM [dbo].[FactMeasure] AS [measure] -- we will only look at actions for premises that are mapped to the measure column we are currently processing
                        INNER JOIN [#WS_Measures_AccountMeasureProfileMap] AS [account_list] ON [account_list].[PremiseId] = [measure].[PremiseId]
                                                                                     AND [account_list].[AccountId] = [measure].[AccountId]
                                                                                     AND [account_list].[MeasureNumber] = @measureNumber
                                                                                     AND [account_list].[MeasureProfileName] = @measureProfileName
                -- note: Export.WS_ClientAction will contain data for a single client
                        INNER JOIN [Export].[WS_ClientAction] AS [ca] ON [ca].[ActionKey] = [measure].[actionkey]
                        INNER JOIN [Export].[WS_ClientActionAppliance] AS [ap] ON [ap].[ClientActionID] = [ca].[ClientActionID]
                        INNER JOIN [Export].[WS_ClientEnduseAppliance] AS [cea] ON [cea].[ApplianceKey] = [ap].[ApplianceKey]
                        INNER JOIN [Export].[WS_ClientEnduse] AS [ce] ON [ce].[ClientEnduseID] = [cea].[ClientEnduseID]
                        INNER JOIN @action_filter AS [action_filter] ON [action_filter].[ActionKey] = [measure].[ActionKey]
                        LEFT OUTER JOIN (SELECT [PremiseID] ,
                                                        [AccountID] ,
                                                        [ActionKey] ,
                                                        [StatusKey] ,
                                                        ROW_NUMBER() OVER (PARTITION BY [PremiseID], [AccountID], [ActionKey] ORDER BY [CreateDate] DESC) AS [rank] -- most recent
                                                FROM [dbo].[FactAction]) AS [action] ON [action].[PremiseId] = [measure].[PremiseId]
                                                                                        AND [action].[AccountId] = [measure].[AccountId]
                                                                                        AND [action].[ActionKey] = [measure].[ActionKey]
                                                                                        AND [action].[rank] = 1
                                                                                        AND         -- most recent
                                                                                        [action].[StatusKey] = 2        -- completed action
                        WHERE [measure].[CommodityKey] IN (@fuel_type, 'all')
                                AND [measure].[ClientId] = @client_id
                                AND [action].[PremiseId] IS NULL;   -- this will ignore completed actions


        SELECT @count = COUNT(*)
                FROM [#WS_Measures_FactActionItem];

        SET @progress_msg = '[#WS_Measures_FactActionItem] contains ' + CAST(@count AS VARCHAR) + ' rows';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

    -- remove action_items that are not in our enduse list
    -- NOTE: if we use this list in the previous query it really bogs down due to the calculation required to genereate the enduse category...therefore I will simply remove the entries now
        SELECT @count = COUNT(*)
                FROM [#WS_Measures_OrderedEnduseList];

        IF @count != 0
        BEGIN
                DELETE [fact_action_item]
                        FROM [#WS_Measures_FactActionItem] AS [fact_action_item]
                        LEFT OUTER JOIN [#WS_Measures_OrderedEnduseList] AS [ordered_enduse_list] ON [fact_action_item].[EnergyObjectKey] = [ordered_enduse_list].[ordered_enduse]
                        WHERE [ordered_enduse_list].[ordered_enduse] IS NULL;  -- no match found

                SELECT @count = @@ROWCOUNT;
                SET @progress_msg = 'Removed ' + CAST(@count AS VARCHAR) + ' entries from [#WS_Measures_FactActionItem] due to ordered_enduse_list filtering';
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

                SELECT @count = COUNT(*)
                        FROM [#WS_Measures_FactActionItem];
                SET @progress_msg = '[#WS_Measures_FactActionItem] contains ' + CAST(@count AS VARCHAR) + ' rows';
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        END;

    -- populate the [#WS_Measures_ActionItem] table with our action items.
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = 'Populating [#WS_Measures_ActionItem]';

        INSERT INTO [#WS_Measures_ActionItem] ([ActionItemKey], [ActionItemDesc], [MeasureId], [EnergyObjectKey], [PremiseKey])
                SELECT
            DISTINCT [fact].[ActionItemKey], [action].[ActionKey], [action].[ActionKey], [fact].[EnergyObjectKey], [fact].[PremiseKey]
                        FROM [Export].[WS_ClientAction] AS [action]
                        INNER JOIN [#WS_Measures_FactActionItem] AS [fact] ON [fact].[ActionItemKey] = [action].[ClientActionID];

        SELECT @count = COUNT(*)
                FROM [#WS_Measures_ActionItem];
        SET @progress_msg = '[#WS_Measures_ActionItem] contains ' + CAST(@count AS VARCHAR) + ' rows';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

    --
    --  process the criteria
    --
        DECLARE @sqlcmd [VARCHAR](8000) ,
                @new_valid_column_name [VARCHAR](255) ,
                @orderby_clause [VARCHAR](2000) = '' ,
                @valid_clause [VARCHAR](8000) = '' ,
                @qualifier_tag [VARCHAR](50) = '%QUALIFIER%' ,
                @column_list [VARCHAR](8000) ,
                @column_list_qualified [VARCHAR](8000) ,
                @continue_validating [BIT] = 1 ,
                @validation_count [INT] = 1;

    -- set the initial column lists
        SET @column_list = @qualifier_tag + '[ActionItemKey],' + @qualifier_tag + '[ActionItemDesc], ' + @qualifier_tag + '[MeasureId], ' + @qualifier_tag
                + '[EnergyObjectKey], ' + @qualifier_tag + '[PremiseKey], ' + @qualifier_tag + '[RefCost], ' + @qualifier_tag + '[RefSavings]';

        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = 'Start adding Action Item Ranking columns:';

    --  Criteria 1
        IF @criteria_id1 <> 'NOVALUE'
        BEGIN
                EXEC [Export].[WS_Measures_AddColsToActionItemRanking] @reportProfileName = @reportProfileName, @criteriaId = @criteria_id1,
                        @criteriaDetail = @criteria_detail1, @criteriaSort = @criteria_sort1, @qualifierTag = @qualifier_tag, @runId = @runId,
                        @orderbyClause = @orderby_clause OUTPUT, @validClause = @valid_clause OUTPUT, @columnList = @column_list OUTPUT;
        END;

    --  Criteria 2
        IF @criteria_id2 <> 'NOVALUE'
        BEGIN
                EXEC [Export].[WS_Measures_AddColsToActionItemRanking] @reportProfileName = @reportProfileName, @criteriaId = @criteria_id2,
                        @criteriaDetail = @criteria_detail2, @criteriaSort = @criteria_sort2, @qualifierTag = @qualifier_tag, @runId = @runId,
                        @orderbyClause = @orderby_clause OUTPUT, @validClause = @valid_clause OUTPUT, @columnList = @column_list OUTPUT;
        END;

    --  Criteria 3
        IF @criteria_id3 <> 'NOVALUE'
        BEGIN
                EXEC [Export].[WS_Measures_AddColsToActionItemRanking] @reportProfileName = @reportProfileName, @criteriaId = @criteria_id3,
                        @criteriaDetail = @criteria_detail3, @criteriaSort = @criteria_sort3, @qualifierTag = @qualifier_tag, @runId = @runId,
                        @orderbyClause = @orderby_clause OUTPUT, @validClause = @valid_clause OUTPUT, @columnList = @column_list OUTPUT;
        END;

    --  Criteria 4
        IF @criteria_id4 <> 'NOVALUE'
        BEGIN
                EXEC [Export].[WS_Measures_AddColsToActionItemRanking] @reportProfileName = @reportProfileName, @criteriaId = @criteria_id4,
                        @criteriaDetail = @criteria_detail4, @criteriaSort = @criteria_sort4, @qualifierTag = @qualifier_tag, @runId = @runId,
                        @orderbyClause = @orderby_clause OUTPUT, @validClause = @valid_clause OUTPUT, @columnList = @column_list OUTPUT;
        END;

    --  Criteria 5
        IF @criteria_id5 <> 'NOVALUE'
        BEGIN
                EXEC [Export].[WS_Measures_AddColsToActionItemRanking] @reportProfileName = @reportProfileName, @criteriaId = @criteria_id5,
                        @criteriaDetail = @criteria_detail5, @criteriaSort = @criteria_sort5, @qualifierTag = @qualifier_tag, @runId = @runId,
                        @orderbyClause = @orderby_clause OUTPUT, @validClause = @valid_clause OUTPUT, @columnList = @column_list OUTPUT;
        END;

    --  Criteria 6
        IF @criteria_id6 <> 'NOVALUE'
        BEGIN
                EXEC [Export].[WS_Measures_AddColsToActionItemRanking] @reportProfileName = @reportProfileName, @criteriaId = @criteria_id6,
                        @criteriaDetail = @criteria_detail6, @criteriaSort = @criteria_sort6, @qualifierTag = @qualifier_tag, @runId = @runId,
                        @orderbyClause = @orderby_clause OUTPUT, @validClause = @valid_clause OUTPUT, @columnList = @column_list OUTPUT;
        END;

        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = 'Success adding Action Item Ranking:';

    -- setup the qualifier for the column lists
        SET @column_list_qualified = REPLACE(@column_list, '%QUALIFIER%', '[rpt_action_item].');
        SET @column_list = REPLACE(@column_list, '%QUALIFIER%', '');

        SET @progress_msg = '@column_list:''' + ISNULL(@column_list, '') + '''; ' + '@column_list_qualified:''' + ISNULL(@column_list_qualified, '') + '''; ';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

    -- okay...we have our tables populated...lets start ranking the values

    -- create a definition of our temporary work table...we will populate later
        SELECT *
                INTO [#WS_Measures_ActionItemWork]
                FROM [#WS_Measures_ActionItem]
                WHERE 1 = 2;

        CREATE INDEX [#WS_Measures_ActionItemWork_idx0] ON [#WS_Measures_ActionItemWork]([PremiseKey]);

        SET @continue_validating = 1;
        SET @validation_count = 1;
        DECLARE @position_of_last_and INT;

    --
    --  iterate over the validation clause...each iteration will drop a criterion off the end of validation clause (least significant).
    --  NOTE: the validation criteria is used to rank the measure, it will not filter out measures.  if the measure satisfies all the criteria it will
    --        have a level 1 ranking...as we drop criteria we increment the level #...therefore the lower the level the more criteria that were satisfied
    --

        WHILE @continue_validating = 1
        BEGIN

                SELECT @progress_msg = 'Validation   @validation_count:' + CAST(@validation_count AS VARCHAR) + '; @valid_clause:''' + @valid_clause + ''''
                                + '; @orderby_clause:''' + @orderby_clause + '''';
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        --
        -- add the valid_levelN columns
        --
                SET @new_valid_column_name = 'valid_level' + CONVERT (VARCHAR, @validation_count);

                SELECT @progress_msg = 'Adding the valid_levelN column : ' + @new_valid_column_name
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

                SELECT  @sqlcmd = 'alter table [#WS_Measures_ActionItem] add [' + @new_valid_column_name + '] [int] NULL';
                EXEC (@sqlcmd);

                SELECT  @sqlcmd = 'alter table [#WS_Measures_ActionItemRankingWork] add [' + @new_valid_column_name + '] [int] NULL';
                EXEC (@sqlcmd);

                SELECT  @sqlcmd = 'alter table [#WS_Measures_ActionItemWork] add [' + @new_valid_column_name + '] [int] NULL';
                EXEC (@sqlcmd);

        -- update the valid_levelN column
                SELECT  @sqlcmd = '
                    update  rpt_action_item
                    set ' + @new_valid_column_name + ' = ' + @valid_clause + ' then 1 else 0 end
                    from [#WS_Measures_ActionItem] as [rpt_action_item];';

                SELECT @progress_msg = 'Update the valid_levelN column :   @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

                EXEC (@sqlcmd);

                SELECT @progress_msg = 'Success updating the column: @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        -- we have now altered the tables and updated the value of valid_levelN

        --
        -- the following will exclude actions that were recommended the last time the report was run
        --
                SELECT  @sqlcmd = '
                            Insert [#WS_Measures_ActionItemWork] (
                                ' + @column_list + ', [' + @new_valid_column_name + '])
                                select  *
                                from
                                (
                                    select ' + @column_list_qualified + ', [' + @new_valid_column_name + ']
                                    from [#WS_Measures_ActionItem] as [rpt_action_item]
                                    where not exists
                                    (
                                        select
                                            [previous_report].[profile_name], [previous_report].[PremiseID], [previous_report].[history_date],
                                            [previous_report].[MeasureID], [previous_report].[report_year], [previous_report].[report_month]
                                        from
                                            (
                                                select  [profile_name],  [PremiseID],   [history_date],
                                                        [measureId],    [report_year],    [report_month],
                                                        RANK() over (partition by [PremiseID] order by [report_year] desc, [report_month] desc) as [row_id]
                                                from    [Export].[WS_Measures_History]
                                            ) as [previous_report]
                                            inner join [dbo].[DimPremise] as [premise] on
                                                [premise].[PremiseID] = [previous_report].[PremiseID]
                                        where
                                            [previous_report].[row_id] = 1 and
                                            [rpt_action_item].[MeasureID] = [previous_report].[MeasureID] and
                                            [rpt_action_item].[PremiseKey] = [premise].[PremiseKey]
                                    )
                                ) as [data] ';

                SELECT @progress_msg = 'Exclude actions that were recommended the last time the report was run - First Run:   @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

                EXEC (@sqlcmd);

                SELECT @progress_msg = 'Success excluding actions - First Run: @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        -----------------------------------------------------------------------------------------------
        --  this is removing entries without a valid measure
        -----------------------------------------------------------------------------------------------
                SELECT  @sqlcmd = '
                        delete [action_item_work]
                            from
                                 [#WS_Measures_ActionItemWork]  as [action_item_work]
                                inner join
                                    (
                                        select [PremiseKey], sum(isnull([' + @new_valid_column_name + '], 0)) as [ValidActionCount]
                                        from  [#WS_Measures_ActionItem]
                                        group by [PremiseKey]
                                    )as [valid_actions] on
                                        [valid_actions].[PremiseKey] = [action_item_work].[PremiseKey] and
                                        [valid_actions].[ValidActionCount] < 1;';

                SELECT @progress_msg = 'Removing entries without a valid measure - First Run:   @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

                EXEC (@sqlcmd);

                SELECT @progress_msg = 'Success Removing entries - First Run:  @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;


        --  rank the measures (this will include invalid and valid...however valid will be ranked higher)
        --
                SELECT  @sqlcmd = '
                        insert [#WS_Measures_ActionItemRankingWork]
                        (' + @column_list + ', [RankId], [LevelNum], [' + @new_valid_column_name + '])
                        select ' + @column_list + ', [rank_id], ' + CONVERT (VARCHAR, @validation_count) + ', [' + @new_valid_column_name + ']
                        from
                        (
                            select  ' + @column_list + ', [' + @new_valid_column_name + ']
                            , row_number () over (partition by [action_item_work].[PremiseKey] order by [action_item_work].[' + @new_valid_column_name + '] DESC, '
                                            + @orderby_clause + ', [oel].[enduse_order] ASC, [action_item_work].[MeasureId] ASC) as rank_id
                            from
                                 [#WS_Measures_ActionItemWork]  as [action_item_work]
                                inner join [#WS_Measures_OrderedEnduseList] oel  on
                                    oel.[ordered_enduse] = [action_item_work].[EnergyObjectKey]
                        ) as [action_item];';

                SELECT @progress_msg = 'Rank the measures (this will include invalid and valid...however valid will be ranked higher) - First Run:  @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

                EXEC (@sqlcmd);

                SELECT @progress_msg = 'Success ranking the measures - First Run:  @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        --
        -- remove premises from the main table because we have already included their actions in the ranked table
        --
                SELECT @progress_msg = 'Remove premises from the [#WS_Measures_ActionItem] because we have already included their actions in the ranked table. - First Run';
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

                DELETE [action_item]
                        FROM [#WS_Measures_ActionItem] AS [action_item]
                        INNER JOIN [#WS_Measures_ActionItemWork] AS [action_item_work] ON [action_item].[PremiseKey] = [action_item_work].[PremiseKey];

                SELECT @progress_msg = 'Success remove premises from  [#WS_Measures_ActionItem] - removed ' + CAST(@@ROWCOUNT AS VARCHAR) + ' rows';
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;


                IF CHARINDEX('AND', @valid_clause) > 1
                BEGIN

                        -- drop the least significant criterion
                        -- we are searching for "dna" because it is "and" reversed.
                        SET @position_of_last_and = LEN(@valid_clause) - CHARINDEX('DNA', REVERSE(@valid_clause)) - 1;
                        SET @valid_clause = SUBSTRING(@valid_clause, 0, @position_of_last_and);

                        SELECT  @validation_count += 1;

                END;
                ELSE
                    BEGIN
                            SELECT  @continue_validating = 0;
                    END;

                TRUNCATE TABLE [#WS_Measures_ActionItemWork] ;
    --bottom of loop
        END;

    --
    --  Outside of the numeric column checking loop
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = 'Numeric Column Checking Loop   COMPLETE';

        SELECT  @validation_count += 1;

    -- *** the following logic is based on the original SoCal gas report...however due to bugs the #HE_Report_actionitems2 was always empty at this point
    -- If I have enough time I will fix this logic to make it more streamline / maintainable...if not <sigh> glambert

    --
    -- add the final valid_levelN column
    --
        SET @new_valid_column_name = 'valid_level' + CONVERT (VARCHAR, @validation_count);

        SELECT @progress_msg = 'Adding the final valid_levelN column: ' + @new_valid_column_name ;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        SELECT  @sqlcmd = 'alter table  [#WS_Measures_ActionItem] add [' + @new_valid_column_name + '] [int] NULL';
        EXEC (@sqlcmd);

        SELECT  @sqlcmd = 'alter table [#WS_Measures_ActionItemRankingWork] add [' + @new_valid_column_name + '] [int] NULL';
        EXEC (@sqlcmd);

        SELECT  @sqlcmd = 'alter table [#WS_Measures_ActionItemWork] add [' + @new_valid_column_name + '] [int] NULL';
        EXEC (@sqlcmd);


    -- we cannot update the new valid_levelN column because we have no validation clause...therefore they are all invalid at this level
           SELECT @progress_msg = 'Success updating the column: @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

    -- log some metrics
        SELECT @progress_msg = '[#WS_Measures_ActionItem] count:' + CAST(ISNULL(COUNT(*), 0) AS VARCHAR)
                FROM [#WS_Measures_ActionItem];
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        SELECT @progress_msg = '#WS_Measures_OrderedEnduseList count:' + CAST(ISNULL(COUNT(*), 0) AS VARCHAR)
                FROM [#WS_Measures_OrderedEnduseList];
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

    --
    -- at this point all that is remaining is premises that do not have valid actions
    -- we will exclude actions that were recommended the last time the report was run
    -- NOTE: there are no valid actions at this point because
    --      1) there truly are no valid actions based on the criteria
    --                      OR
    --      2) the valid action was on the previous report
    --  since we are excluding actions on previous report this will be purely invalid actions
    --
        SELECT  @sqlcmd = '
                Insert [#WS_Measures_ActionItemWork] (
                    ' + @column_list + ', [' + @new_valid_column_name + '])
                    select  *
                    from
                    (
                        select ' + @column_list_qualified + ', [' + @new_valid_column_name + ']
                        from [#WS_Measures_ActionItem] as [rpt_action_item]
                        where not exists
                        (
                            select
                                [previous_report].[profile_name], [previous_report].[PremiseID],    [previous_report].[history_date],
                                [previous_report].[MeasureID], [previous_report].[report_year], [previous_report].[report_month]
                            from
                                (
                                    select  [profile_name],     [PremiseID],    [history_date],
                                            [measureId],        [report_year],    [report_month],
                                            RANK() over (partition by [PremiseID] order by [report_year] desc, [report_month] desc) as [row_id]
                                    from    [Export].[WS_Measures_History]
                                ) as [previous_report]
                                inner join [dbo].[DimPremise] as [premise] on
                                    [premise].[PremiseID] = [previous_report].[PremiseID]
                            where
                                [previous_report].[row_id] = 1 and
                                [rpt_action_item].[MeasureID] = [previous_report].[MeasureID] and
                                [rpt_action_item].[PremiseKey] = [premise].[PremiseKey]
                        )
                    ) as [data] ';

        SELECT @progress_msg = 'Exclude actions that were recommended the last time the report was run -  Second Run  : @sqlcmd:' + @sqlcmd;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        EXEC (@sqlcmd);

        SELECT @progress_msg = 'Success excluding actions -  Second Run : @sqlcmd:' + @sqlcmd;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

    --
    --  rank the measures
    --
        SELECT  @sqlcmd = '
                insert [#WS_Measures_ActionItemRankingWork]
                (' + @column_list + ', [RankId], [LevelNum], [' + @new_valid_column_name + '])
                select ' + @column_list + ', [rank_id], ' + CONVERT (VARCHAR, @validation_count) + ', [' + @new_valid_column_name + ']
                from
                (
                    select  ' + @column_list + ', [' + @new_valid_column_name + ']
                    , row_number () over (partition by [action_item_work].[PremiseKey] order by [action_item_work].[' + @new_valid_column_name + '] DESC, '
                                + @orderby_clause + ', [oel].[enduse_order] ASC, [action_item_work].[MeasureId] ASC) as rank_id
                    from
                        [#WS_Measures_ActionItemWork]  as [action_item_work]
                        inner join #WS_Measures_OrderedEnduseList oel  on
                            oel.[ordered_enduse] = [action_item_work].[EnergyObjectKey]
                ) as [action_item];';

        SELECT @progress_msg = 'Rank the measures (this will include invalid and valid...however valid will be ranked higher) - Second Run:  @sqlcmd:' + @sqlcmd;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        EXEC (@sqlcmd);

        SELECT @progress_msg = 'Success ranking the measures - Second Run:  @sqlcmd:' + @sqlcmd;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

    --
    -- remove premises from the main table because we have already included their actions in the ranked table
    --
        SELECT @progress_msg = 'Remove premises from the [#WS_Measures_ActionItem] because we have already included their actions in the ranked table. - First Run';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        DELETE [action_item]
                FROM [#WS_Measures_ActionItem]  AS [action_item]
                INNER JOIN [#WS_Measures_ActionItemWork] AS [action_item_work] ON [action_item].[PremiseKey] = [action_item_work].[PremiseKey];

        SELECT @progress_msg = 'Success remove premises from  [#WS_Measures_ActionItem] - removed ' + CAST(@@ROWCOUNT AS VARCHAR) + ' rows';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

    -- log some metrics
        SELECT @progress_msg = '[#WS_Measures_ActionItem] final count:' + CAST(ISNULL(COUNT(*), 0) AS VARCHAR)
                FROM [#WS_Measures_ActionItem];
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;


    -- at this point [#WS_Measures_ActionItemRankingWork] should have our rankings for the measure/profile we are processing
    -- the caller of this proc can use the rankings as it sees fit

        SELECT @count = COUNT(*)
                FROM [#WS_Measures_ActionItemRankingWork] ;
        SELECT @progress_msg = '[#WS_Measures_ActionItemRankingWork]  contains ' + CAST(@count AS VARCHAR) + ' rows';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        SET @progress_msg = 'Finished performing Measure Ranking ...' + '@reportProfileName:''' + ISNULL(@reportProfileName, '') + '''; ' + '@measureNumber:'''
                + ISNULL(CAST(@measureNumber AS [VARCHAR]), '') + '''; ' + '@measureProfileName:''' + ISNULL(@measureProfileName, '') + '''; ';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;


END;


GO
