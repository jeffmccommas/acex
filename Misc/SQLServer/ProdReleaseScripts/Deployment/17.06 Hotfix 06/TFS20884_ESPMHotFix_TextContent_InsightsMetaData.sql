/*
Run this script on:

acePsql1c0.database.windows.net.InsightsMetaData    -  This database will be modified

to synchronize it with:

aceusql1c0.database.windows.net.InsightsMetaData

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.1.0.3760 from Red Gate Software Ltd at 8/2/2017 3:13:18 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

PRINT(N'Drop constraints from [cm].[ClientTextContentLocaleContent]')
ALTER TABLE [cm].[ClientTextContentLocaleContent] NOCHECK CONSTRAINT [FK_ClientTextContentLocaleContent_ClientTextContent]
ALTER TABLE [cm].[ClientTextContentLocaleContent] NOCHECK CONSTRAINT [FK_ClientTextContentLocaleContent_TypeLocale]

PRINT(N'Drop constraints from [cm].[ClientTextContent]')
ALTER TABLE [cm].[ClientTextContent] NOCHECK CONSTRAINT [FK_ClientTextContent_Client]
ALTER TABLE [cm].[ClientTextContent] NOCHECK CONSTRAINT [FK_ClientTextContent_TypeCategory]
ALTER TABLE [cm].[ClientTextContent] NOCHECK CONSTRAINT [FK_ClientTextContent_TypeTextContent]


PRINT(N'Add row to [cm].[ClientTextContent]')
SET IDENTITY_INSERT [cm].[ClientTextContent] ON
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey], [DateUpdated]) VALUES (8062, 224, 'tab.espmmeters.meters.noaccesstoaccount', 'espm', '2017-08-01 18:36:04.207')
SET IDENTITY_INSERT [cm].[ClientTextContent] OFF

PRINT(N'Add rows to [cm].[ClientTextContentLocaleContent]')
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (8062, 'en-US', N'You do not have access to the account. Please accept the account to view the details.', N'You do not have access to the account. Please accept the account to view the details.', N'You do not have access to the account. Please accept the account to view the details.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (8062, 'es-ES', N'Usted no tiene acceso a la cuenta. Por favor acepte la cuenta para ver los detalles.', N'Usted no tiene acceso a la cuenta. Por favor acepte la cuenta para ver los detalles.', N'Usted no tiene acceso a la cuenta. Por favor acepte la cuenta para ver los detalles.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (8062, 'ru-RU', N'Вы не имеют доступа к учетной записи. Примите, пожалуйста, учетную запись для просмотра сведений.', N'Вы не имеют доступа к учетной записи. Примите, пожалуйста, учетную запись для просмотра сведений.', N'Вы не имеют доступа к учетной записи. Примите, пожалуйста, учетную запись для просмотра сведений.', 0)
PRINT(N'Operation applied to 3 rows out of 3')

PRINT(N'Add constraints to [cm].[ClientTextContentLocaleContent]')
ALTER TABLE [cm].[ClientTextContentLocaleContent] WITH CHECK CHECK CONSTRAINT [FK_ClientTextContentLocaleContent_ClientTextContent]
ALTER TABLE [cm].[ClientTextContentLocaleContent] WITH CHECK CHECK CONSTRAINT [FK_ClientTextContentLocaleContent_TypeLocale]

PRINT(N'Add constraints to [cm].[ClientTextContent]')
ALTER TABLE [cm].[ClientTextContent] WITH CHECK CHECK CONSTRAINT [FK_ClientTextContent_Client]
ALTER TABLE [cm].[ClientTextContent] CHECK CONSTRAINT [FK_ClientTextContent_TypeCategory]
ALTER TABLE [cm].[ClientTextContent] CHECK CONSTRAINT [FK_ClientTextContent_TypeTextContent]
COMMIT TRANSACTION
GO
