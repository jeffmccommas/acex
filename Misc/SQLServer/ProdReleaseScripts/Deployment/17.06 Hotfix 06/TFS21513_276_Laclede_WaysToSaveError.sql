/*
Run this script on:
acePsql1c0.database.windows.net.InsightsMetaData    -  This database will be modified
to synchronize it with:
aceusql1c0.database.windows.net.InsightsMetaData
You are recommended to back up your database before running this script
Script created by SQL Data Compare version 12.1.0.3760 from Red Gate Software Ltd at 8/2/2017 12:52:25 PM
*/	
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)
PRINT(N'Drop constraints from [cm].[ClientActionAppliance]')
ALTER TABLE [cm].[ClientActionAppliance] NOCHECK CONSTRAINT [FK_ClientActionAppliance_ClientAction]
ALTER TABLE [cm].[ClientActionAppliance] NOCHECK CONSTRAINT [FK_ClientActionAppliance_TypeAppliance]

PRINT(N'Add row to [cm].[ClientActionAppliance]')
INSERT INTO [cm].[ClientActionAppliance] ([ClientActionID], [ApplianceKey], [DisplayOrder]) VALUES (1425, 'tv', 1)

PRINT(N'Add constraints to [cm].[ClientActionAppliance]')
ALTER TABLE [cm].[ClientActionAppliance] CHECK CONSTRAINT [FK_ClientActionAppliance_ClientAction]
ALTER TABLE [cm].[ClientActionAppliance] CHECK CONSTRAINT [FK_ClientActionAppliance_TypeAppliance]
COMMIT 
