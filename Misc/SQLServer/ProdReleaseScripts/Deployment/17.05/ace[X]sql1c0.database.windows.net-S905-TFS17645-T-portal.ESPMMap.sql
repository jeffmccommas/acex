/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_224 DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


-- =============================================
-- Author:   Muazzam Ali / Jayaraman, Vishwanath
-- Create date: 4/14/2017
-- Udated Date: 5/16/2017 - New column 'MeterId' added
-- Description: Creating Portal Schema for DW Database and ESPM Map Table 
-- Database Name: Insight DW database
-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
SELECT  schema_name
FROM    information_schema.schemata
WHERE   schema_name = 'portal' ) 

BEGIN
EXEC sp_executesql N'CREATE SCHEMA portal'
END
GO

/****** Object:  Table [portal].[ESPMMap]    Script Date: 4/14/2017 11:02:49 AM ******/
DROP TABLE IF EXISTS [portal].[ESPMMap]
GO
/****** Object:  Table [portal].[ESPMMap]    Script Date: 4/14/2017 11:02:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[ESPMMap]') AND type in (N'U'))
BEGIN
CREATE TABLE [portal].[ESPMMap](
	[MapId] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [nvarchar](50) NOT NULL,
	[PremiseId] [nvarchar](50) NULL,
	[UpdatedBy] [int] NOT NULL,
	[UpdatedDate] [date] NOT NULL,
	[MeterId] [VARCHAR](200) NOT NULL,
 CONSTRAINT [PK_ESPMMap] PRIMARY KEY CLUSTERED 
(
	[MapId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO

