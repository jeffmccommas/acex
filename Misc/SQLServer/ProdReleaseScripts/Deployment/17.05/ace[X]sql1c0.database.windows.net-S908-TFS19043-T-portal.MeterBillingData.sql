/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_224 DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


-- =============================================
-- Author:   Muazzam Ali / Jayaraman, Vishwanath
-- Create date: 5/17/2017
-- Description: Creating new portal.MeterBillingData Table to
-- store billing data for ESPM transfer process
-- Database Name: Insight DW database
-- =============================================


/****** Object:  Table [portal].[MeterBillingData]    Script Date: 5/17/2017 4:41:05 PM ******/
DROP TABLE IF EXISTS [portal].[MeterBillingData]
GO

/****** Object:  Table [portal].[MeterBillingData]    Script Date: 5/17/2017 4:41:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[MeterBillingData]') AND type in (N'U'))
BEGIN
CREATE TABLE [portal].[MeterBillingData](
	[UsageId] [bigint] NOT NULL,
	[MeterId] [varchar](50) NOT NULL,
	[Usage] [varchar](50) NULL,
	[Cost] [varchar](50) NULL,
	[Startdate] [date] NULL,
	[Enddate] [date] NULL,
	[UpdatedDate] [date] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
 CONSTRAINT [PK_portal.MeterConsumptionData] PRIMARY KEY CLUSTERED 
(
	[UsageId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO
