/********************************************************/
/***** RUN ONLY ON THE UAT INSIGHTS DATABASE          ***/
/********************************************************/

-- =============================================
-- Author:   Muazzam Ali / Jayaraman, Vishwanath
-- Create date: 4/14/2017
-- Description: New portal schema and all table to support Portal project (Energy Star and more)
-- Database Name: [UAT Only] Insights database
-- =============================================


IF NOT EXISTS (
SELECT  schema_name
FROM    information_schema.schemata
WHERE   schema_name = 'portal' ) 

BEGIN
EXEC sp_executesql N'CREATE SCHEMA portal'
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[PortalLogin]') AND type in (N'U'))
ALTER TABLE [portal].[PortalLogin] DROP CONSTRAINT IF EXISTS [FK_PortalLogin_EnvID]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[PortalLogin]') AND type in (N'U'))
ALTER TABLE [portal].[PortalLogin] DROP CONSTRAINT IF EXISTS [FK_PortalLogin_ClientID]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[PortalConfig]') AND type in (N'U'))
ALTER TABLE [portal].[PortalConfig] DROP CONSTRAINT IF EXISTS [FK_PortalConfig_PortalConfigType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[ClientPortalConfig]') AND type in (N'U'))
ALTER TABLE [portal].[ClientPortalConfig] DROP CONSTRAINT IF EXISTS [FK_ClientPortalConfigId_PortalConfig]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[ClientPortalConfig]') AND type in (N'U'))
ALTER TABLE [portal].[ClientPortalConfig] DROP CONSTRAINT IF EXISTS [FK_ClientPortalConfigId_Client]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[PortalLogin]') AND type in (N'U'))
ALTER TABLE [portal].[PortalLogin] DROP CONSTRAINT IF EXISTS [DF__PortalLog__Faile__3D9E16F4]
GO
/****** Object:  Index [UQ_PortalLogin_Username_ClientID_EnvID]    Script Date: 4/14/2017 9:56:41 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[PortalLogin]') AND type in (N'U'))
ALTER TABLE [portal].[PortalLogin] DROP CONSTRAINT IF EXISTS [UQ_PortalLogin_Username_ClientID_EnvID]
GO
/****** Object:  Table [portal].[PortalLogin]    Script Date: 4/14/2017 9:56:41 AM ******/
DROP TABLE IF EXISTS [portal].[PortalLogin]
GO
/****** Object:  Table [portal].[PortalConfigType]    Script Date: 4/14/2017 9:56:41 AM ******/
DROP TABLE IF EXISTS [portal].[PortalConfigType]
GO
/****** Object:  Table [portal].[PortalConfig]    Script Date: 4/14/2017 9:56:41 AM ******/
DROP TABLE IF EXISTS [portal].[PortalConfig]
GO
/****** Object:  Table [portal].[ClientPortalConfig]    Script Date: 4/14/2017 9:56:41 AM ******/
DROP TABLE IF EXISTS [portal].[ClientPortalConfig]
GO
/****** Object:  Table [portal].[ClientPortalConfig]    Script Date: 4/14/2017 9:56:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[ClientPortalConfig]') AND type in (N'U'))
BEGIN
CREATE TABLE [portal].[ClientPortalConfig](
	[ClientPortalConfigId] [int] IDENTITY(1,1) NOT NULL,
	[ClientID] [int] NOT NULL,
	[PortalConfigId] [int] NOT NULL,
	[PortalConfigValue] [varchar](100) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_ClientPortalConfig] PRIMARY KEY CLUSTERED 
(
	[ClientPortalConfigId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO
/****** Object:  Table [portal].[PortalConfig]    Script Date: 4/14/2017 9:56:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[PortalConfig]') AND type in (N'U'))
BEGIN
CREATE TABLE [portal].[PortalConfig](
	[PortalConfigId] [int] IDENTITY(1,1) NOT NULL,
	[PortalConfigName] [varchar](100) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[PortalConfigTypeId] [int] NOT NULL,
 CONSTRAINT [PK_PortalConfig] PRIMARY KEY CLUSTERED 
(
	[PortalConfigId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO
/****** Object:  Table [portal].[PortalConfigType]    Script Date: 4/14/2017 9:56:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[PortalConfigType]') AND type in (N'U'))
BEGIN
CREATE TABLE [portal].[PortalConfigType](
	[PortalConfigTypeId] [int] IDENTITY(1,1) NOT NULL,
	[PortalConfigTypeName] [varchar](100) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_PortalConfigType] PRIMARY KEY CLUSTERED 
(
	[PortalConfigTypeId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO
/****** Object:  Table [portal].[PortalLogin]    Script Date: 4/14/2017 9:56:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[PortalLogin]') AND type in (N'U'))
BEGIN
CREATE TABLE [portal].[PortalLogin](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](128) NOT NULL,
	[ClientID] [int] NOT NULL,
	[FirstName] [varchar](32) NULL,
	[LastName] [varchar](32) NULL,
	[PasswordHash] [varchar](128) NULL,
	[PasswordSalt] [varchar](64) NULL,
	[UpdateDate] [datetime] NULL,
	[EnvID] [tinyint] NOT NULL,
	[UserType] [varchar](5) NULL,
	[Enabled] [bit] NULL,
	[FailedLoginCount] [int] NULL,
 CONSTRAINT [PK_PortalLogin_UserID] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO
SET IDENTITY_INSERT [portal].[ClientPortalConfig] ON 

GO
INSERT [portal].[ClientPortalConfig] ([ClientPortalConfigId], [ClientID], [PortalConfigId], [PortalConfigValue], [IsEnabled]) VALUES (1, 224, 1, N'QWNsYXJhVGVzdDpBRVNQTTMzMCE=', 1)
GO
INSERT [portal].[ClientPortalConfig] ([ClientPortalConfigId], [ClientID], [PortalConfigId], [PortalConfigValue], [IsEnabled]) VALUES (2, 224, 2, N'3', 1)
GO
SET IDENTITY_INSERT [portal].[ClientPortalConfig] OFF
GO
SET IDENTITY_INSERT [portal].[PortalConfig] ON 

GO
INSERT [portal].[PortalConfig] ([PortalConfigId], [PortalConfigName], [IsEnabled], [PortalConfigTypeId]) VALUES (1, N'ESPMSECRETKEY', 1, 1)
GO
INSERT [portal].[PortalConfig] ([PortalConfigId], [PortalConfigName], [IsEnabled], [PortalConfigTypeId]) VALUES (2, N'INCORRECTLOGINS', 1, 1)
GO
SET IDENTITY_INSERT [portal].[PortalConfig] OFF
GO
SET IDENTITY_INSERT [portal].[PortalConfigType] ON 

GO
INSERT [portal].[PortalConfigType] ([PortalConfigTypeId], [PortalConfigTypeName], [IsEnabled]) VALUES (1, N'APPCONFIG', 1)
GO
SET IDENTITY_INSERT [portal].[PortalConfigType] OFF
GO
SET IDENTITY_INSERT [portal].[PortalLogin] ON 

GO
INSERT [portal].[PortalLogin] ([UserID], [Username], [ClientID], [FirstName], [LastName], [PasswordHash], [PasswordSalt], [UpdateDate], [EnvID], [UserType], [Enabled], [FailedLoginCount]) VALUES (17, N'vjayaraman', 224, N'Vishwanath', N'Jayaraman', N'JqnBUysxCwQWh3lnKIYztYVhm3lDgRuF8Zy00jF+1Wk=', N'b8411507-a630-4da4-b78c-6aa6d0e535a3', CAST(N'2017-04-06T18:47:52.097' AS DateTime), 3, N'admin', 1, 0)
GO
INSERT [portal].[PortalLogin] ([UserID], [Username], [ClientID], [FirstName], [LastName], [PasswordHash], [PasswordSalt], [UpdateDate], [EnvID], [UserType], [Enabled], [FailedLoginCount]) VALUES (18, N'ccarney', 224, N'Catherine', N'Carney', N'gtB8Wwr/qjREhZpJpTawJ2yXJqMKxu1oIAAwAcs7xXQ=', N'CC1F5582EE11B1EFD454613C1C400DCC', CAST(N'2017-04-11T20:17:41.707' AS DateTime), 3, N'admin', 1, 0)
GO
INSERT [portal].[PortalLogin] ([UserID], [Username], [ClientID], [FirstName], [LastName], [PasswordHash], [PasswordSalt], [UpdateDate], [EnvID], [UserType], [Enabled], [FailedLoginCount]) VALUES (19, N'mali', 224, N'Muazzam', N'Ali', N'gtB8Wwr/qjREhZpJpTawJ2yXJqMKxu1oIAAwAcs7xXQ=', N'CC1F5582EE11B1EFD454613C1C400DCC', CAST(N'2016-12-07T17:24:31.220' AS DateTime), 3, N'admin', 1, 0)
GO
INSERT [portal].[PortalLogin] ([UserID], [Username], [ClientID], [FirstName], [LastName], [PasswordHash], [PasswordSalt], [UpdateDate], [EnvID], [UserType], [Enabled], [FailedLoginCount]) VALUES (20, N'Irina', 224, N'Irina', N'Faynstein', N'7rJ3L51X5RuDqs5iX1AbMpMlcXfqaOkH+bO32++A7V4=', N'03464ee5-5c90-44ab-a9f6-b91522fd3553', CAST(N'2017-04-04T17:14:50.313' AS DateTime), 3, N'admin', 1, 0)
GO
INSERT [portal].[PortalLogin] ([UserID], [Username], [ClientID], [FirstName], [LastName], [PasswordHash], [PasswordSalt], [UpdateDate], [EnvID], [UserType], [Enabled], [FailedLoginCount]) VALUES (21, N'nsindhu', 224, N'Nimrita', N'Sindhu', N'/Fnz3N0xjsVlUgQlpcATNdxNLOYXeGzK8pFsIrPpMFY=', N'153c6c8c-34bc-4d27-9b3a-33fc340f488e', CAST(N'2017-04-06T18:32:56.470' AS DateTime), 3, N'std', 1, 0)
GO
INSERT [portal].[PortalLogin] ([UserID], [Username], [ClientID], [FirstName], [LastName], [PasswordHash], [PasswordSalt], [UpdateDate], [EnvID], [UserType], [Enabled], [FailedLoginCount]) VALUES (22, N'glawes', 224, N'Graham', N'Lawes', N'WqySAN23Y/WC46wsFg5W4JQKNiUUTP0BlO7M+BPW348=', N'6c1a00ca-dfa8-42b1-b734-73c5c6ad55ff', CAST(N'2017-04-06T18:35:11.800' AS DateTime), 3, N'std', 1, 0)
GO
INSERT [portal].[PortalLogin] ([UserID], [Username], [ClientID], [FirstName], [LastName], [PasswordHash], [PasswordSalt], [UpdateDate], [EnvID], [UserType], [Enabled], [FailedLoginCount]) VALUES (23, N'duser', 224, N'Demo', N'User', N'Zt53Ic0+zVBbPUgHuUEs4OhEH8FA3t18qiVYkm+iAAk=', N'3632e6e2-36b7-4565-b039-ab8fe81b4b0f', CAST(N'2017-04-07T15:18:38.863' AS DateTime), 3, N'std', 1, 0)
GO
INSERT [portal].[PortalLogin] ([UserID], [Username], [ClientID], [FirstName], [LastName], [PasswordHash], [PasswordSalt], [UpdateDate], [EnvID], [UserType], [Enabled], [FailedLoginCount]) VALUES (24, N'AclaraDev', 224, N'AclaraDev', N'Development', N'j1ZtpgChBCY5hlwaGJOlvayvYqL5XvxwzdXwbtgdQPo=', N'ad35f657-892c-43ac-9e8a-e0ca333162ed', CAST(N'2017-04-07T18:42:26.183' AS DateTime), 3, N'admin', 1, 0)
GO
INSERT [portal].[PortalLogin] ([UserID], [Username], [ClientID], [FirstName], [LastName], [PasswordHash], [PasswordSalt], [UpdateDate], [EnvID], [UserType], [Enabled], [FailedLoginCount]) VALUES (25, N'AclaraProd', 224, N'AclaraProd', N'Development', N'j1ZtpgChBCY5hlwaGJOlvayvYqL5XvxwzdXwbtgdQPo=', N'ad35f657-892c-43ac-9e8a-e0ca333162ed', CAST(N'2017-04-07T18:42:26.183' AS DateTime), 0, N'admin', 1, 0)
GO
INSERT [portal].[PortalLogin] ([UserID], [Username], [ClientID], [FirstName], [LastName], [PasswordHash], [PasswordSalt], [UpdateDate], [EnvID], [UserType], [Enabled], [FailedLoginCount]) VALUES (26, N'AclaraUat', 224, N'AclaraProd', N'Development', N'j1ZtpgChBCY5hlwaGJOlvayvYqL5XvxwzdXwbtgdQPo=', N'ad35f657-892c-43ac-9e8a-e0ca333162ed', CAST(N'2017-04-07T18:42:26.183' AS DateTime), 1, N'admin', 1, 0)
GO
INSERT [portal].[PortalLogin] ([UserID], [Username], [ClientID], [FirstName], [LastName], [PasswordHash], [PasswordSalt], [UpdateDate], [EnvID], [UserType], [Enabled], [FailedLoginCount]) VALUES (27, N'AclaraQa', 224, N'AclaraQa', N'Development', N'j1ZtpgChBCY5hlwaGJOlvayvYqL5XvxwzdXwbtgdQPo=', N'ad35f657-892c-43ac-9e8a-e0ca333162ed', CAST(N'2017-04-07T18:42:26.183' AS DateTime), 2, N'admin', 1, 0)

GO
SET IDENTITY_INSERT [portal].[PortalLogin] OFF
GO
/****** Object:  Index [UQ_PortalLogin_Username_ClientID_EnvID]    Script Date: 4/14/2017 9:56:41 AM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[portal].[PortalLogin]') AND name = N'UQ_PortalLogin_Username_ClientID_EnvID')
ALTER TABLE [portal].[PortalLogin] ADD  CONSTRAINT [UQ_PortalLogin_Username_ClientID_EnvID] UNIQUE NONCLUSTERED 
(
	[UserID] ASC,
	[ClientID] ASC,
	[EnvID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[DF__PortalLog__Faile__3D9E16F4]') AND type = 'D')
BEGIN
ALTER TABLE [portal].[PortalLogin] ADD  DEFAULT ((0)) FOR [FailedLoginCount]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[portal].[FK_ClientPortalConfigId_Client]') AND parent_object_id = OBJECT_ID(N'[portal].[ClientPortalConfig]'))
ALTER TABLE [portal].[ClientPortalConfig]  WITH CHECK ADD  CONSTRAINT [FK_ClientPortalConfigId_Client] FOREIGN KEY([ClientID])
REFERENCES [dbo].[Client] ([ClientID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[portal].[FK_ClientPortalConfigId_Client]') AND parent_object_id = OBJECT_ID(N'[portal].[ClientPortalConfig]'))
ALTER TABLE [portal].[ClientPortalConfig] CHECK CONSTRAINT [FK_ClientPortalConfigId_Client]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[portal].[FK_ClientPortalConfigId_PortalConfig]') AND parent_object_id = OBJECT_ID(N'[portal].[ClientPortalConfig]'))
ALTER TABLE [portal].[ClientPortalConfig]  WITH CHECK ADD  CONSTRAINT [FK_ClientPortalConfigId_PortalConfig] FOREIGN KEY([PortalConfigId])
REFERENCES [portal].[PortalConfig] ([PortalConfigId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[portal].[FK_ClientPortalConfigId_PortalConfig]') AND parent_object_id = OBJECT_ID(N'[portal].[ClientPortalConfig]'))
ALTER TABLE [portal].[ClientPortalConfig] CHECK CONSTRAINT [FK_ClientPortalConfigId_PortalConfig]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[portal].[FK_PortalConfig_PortalConfigType]') AND parent_object_id = OBJECT_ID(N'[portal].[PortalConfig]'))
ALTER TABLE [portal].[PortalConfig]  WITH CHECK ADD  CONSTRAINT [FK_PortalConfig_PortalConfigType] FOREIGN KEY([PortalConfigTypeId])
REFERENCES [portal].[PortalConfigType] ([PortalConfigTypeId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[portal].[FK_PortalConfig_PortalConfigType]') AND parent_object_id = OBJECT_ID(N'[portal].[PortalConfig]'))
ALTER TABLE [portal].[PortalConfig] CHECK CONSTRAINT [FK_PortalConfig_PortalConfigType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[portal].[FK_PortalLogin_ClientID]') AND parent_object_id = OBJECT_ID(N'[portal].[PortalLogin]'))
ALTER TABLE [portal].[PortalLogin]  WITH CHECK ADD  CONSTRAINT [FK_PortalLogin_ClientID] FOREIGN KEY([ClientID])
REFERENCES [dbo].[Client] ([ClientID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[portal].[FK_PortalLogin_ClientID]') AND parent_object_id = OBJECT_ID(N'[portal].[PortalLogin]'))
ALTER TABLE [portal].[PortalLogin] CHECK CONSTRAINT [FK_PortalLogin_ClientID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[portal].[FK_PortalLogin_EnvID]') AND parent_object_id = OBJECT_ID(N'[portal].[PortalLogin]'))
ALTER TABLE [portal].[PortalLogin]  WITH CHECK ADD  CONSTRAINT [FK_PortalLogin_EnvID] FOREIGN KEY([EnvID])
REFERENCES [dbo].[Environment] ([EnvID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[portal].[FK_PortalLogin_EnvID]') AND parent_object_id = OBJECT_ID(N'[portal].[PortalLogin]'))
ALTER TABLE [portal].[PortalLogin] CHECK CONSTRAINT [FK_PortalLogin_EnvID]
GO
