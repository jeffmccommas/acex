USE [Insights]
GO
/****** Object:  StoredProcedure [dbo].[EndpointTrackingInsert]    Script Date: 3/28/2017 1:53:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[EndpointTrackingInsert]
    @ClientID int,
    @UserID int,
	@Verb varchar(8),
    @ResourceName varchar(500),
    @EndpointID int,
    @UserAgent varchar(128),
    @SourceIPAddress varchar(32),
	@XCEMessageID varchar(64),
    @XCEChannel varchar(64),
    @XCELocale varchar(64),
    @XCEMeta varchar(64),
    @Query varchar(256),
	@MachineName VARCHAR(50),
	@IncludeInd bit,
	@LogDate DATETIME,
	@ThirdPartyClientId VARCHAR(64) = NULL
  
AS
BEGIN
    SET NOCOUNT ON;
    declare @currentDate Datetime
    declare @partitionKey tinyint
     
    set     @currentDate = getdate()
    set     @partitionKey = DATEPART(weekday, @currentDate)
 
    INSERT INTO [dbo].[EndpointTracking]
           ([ClientID]
		   ,[UserID]
		   ,[Verb]
           ,[ResourceName]
           ,[EndpointID]
           ,[UserAgent]
           ,[SourceIPAddress]
           ,[XCEMessageId]
           ,[XCEChannel]
           ,[XCELocale]
		   ,[XCEMeta]
		   ,[Query]
		   ,[MachineName]
		   ,[IncludeInd]
		   ,[LogDate]
		   ,[PartitionKey]
		   ,[ThirdPartyClientId])
     VALUES
           (@ClientID
		   ,@UserID
           ,@Verb
           ,@ResourceName
           ,@EndpointID
           ,@UserAgent
           ,@SourceIPAddress
           ,@XCEMessageID
           ,@XCEChannel
           ,@XCELocale
		   ,@XCEMeta
		   ,@Query
		   ,@MachineName
		   ,@IncludeInd
		   ,@LogDate
		   ,@partitionKey
		   ,@ThirdPartyClientId);
END


