﻿
/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  Table [Holding].[DisAggCustomersFromAPI]    Script Date: 4/26/2017 1:20:02 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Holding].[DisAggCustomersFromAPI]') AND type in (N'U'))
    DROP TABLE [Holding].[DisAggCustomersFromAPI]
GO

/****** Object:  Table [Holding].[DisAggCustomersFromAPI]    Script Date: 4/26/2017 1:20:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Holding].[DisAggCustomersFromAPI]') AND type in (N'U'))
BEGIN
CREATE TABLE [Holding].[DisAggCustomersFromAPI](
    [ClientId] [int] NOT NULL,
    [CustomerId] [varchar](50) NOT NULL,
    [AccountId] [varchar](50) NOT NULL,
)
END
GO