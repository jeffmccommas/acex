﻿


/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[S_ReadDisAggQueueForAPI]    Script Date: 4/26/2017 2:40:26 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_ReadDisAggQueueForAPI]') AND type in (N'P', N'PC'))
DROP PROCEDURE [ETL].[S_ReadDisAggQueueForAPI]
GO

/****** Object:  StoredProcedure [ETL].[S_ReadDisAggQueueForAPI]    Script Date: 4/26/2017 2:40:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_ReadDisAggQueueForAPI]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[S_ReadDisAggQueueForAPI] AS'
END
GO

-- =============================================
-- Author:  Philip Victor
-- Create date: 03/20/2017
-- Description:
-- =============================================
ALTER PROCEDURE [ETL].[S_ReadDisAggQueueForAPI]
    @ClientID INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

            -- Setup Temp Tables.
            DECLARE @HoldingData AS TABLE(
                ClientId INT NOT NULL,
                PremiseId VARCHAR(50) NOT NULL,
                AccountId VARCHAR(50) NOT NULL,
                CustomerId VARCHAR(50) NOT NULL,
                SourceId INT NULL,
                TrackingId VARCHAR(50) NULL,
                TrackingDate DATETIME NULL,
                INDEX IX3 NONCLUSTERED(ClientId,CustomerId,AccountId,PremiseId));

            DECLARE @ResultsTable AS TABLE(
                    ClientId INT NOT NULL,
                    CustomerId VARCHAR(50) NOT NULL,
                    AccountId VARCHAR(50) NOT NULL,
                    PremiseId VARCHAR(50) NOT NULL,
                    StartDate DATETIME NOT NULL,
                    EndDate DATETIME NOT NULL,
                    TotalUnits DECIMAL(18,2) NOT NULL ,
                    TotalCost DECIMAL(18,2) NOT NULL,
                    CommodityKey VARCHAR(20) NULL,
                    UomKey VARCHAR(50) NOT NULL,
                    BillDays INT NOT NULL,
                    INDEX IX3 NONCLUSTERED([ClientId],[CustomerId],[AccountId],[PremiseId],[StartDate],[EndDate],[CommodityKey]));

            -- Get data from Holding Table.
            INSERT INTO @HoldingData
                    ( [ClientId] ,
                      [PremiseId] ,
                      [AccountId] ,
                      [CustomerId] ,
                      [SourceId] ,
                      [TrackingId] ,
                      [TrackingDate]
                    )
            SELECT a.ClientId ,
                        a.PremiseId ,
                        a.AccountId ,
                        a.CustomerId ,
                        a.SourceId ,
                        a.TrackingId ,
                        a.TrackingDate
            FROM (SELECT daq.*,
                            ROW_NUMBER() OVER ( PARTITION BY daq.PremiseId,
                            daq.AccountId, daq.CustomerId ORDER BY daq.TrackingDate DESC ) AS Latest
                         FROM [Holding].[DisAggReCalcQueue] daq
                         INNER JOIN [Holding].[DisAggCustomersFromAPI] dcapi ON [daq].[ClientId] = dcapi.ClientId
                                    AND [daq].[CustomerId] = dcapi.[CustomerId]
                                    AND [daq].[AccountId] =  dcapi.[AccountId]
                        WHERE daq.[ClientId]=@ClientID) AS a
            WHERE a.Latest  = 1

            DECLARE @CustomerId VARCHAR(50),
                            @AccountId VARCHAR(50),
                            @PremiseId VARCHAR(50)

            -- Setup a cursor.
            DECLARE theCursor cursor local static forward_only for
                SELECT [CustomerId], [AccountId], [PremiseId] FROM @HoldingData
                WHERE [ClientId] = @ClientID

            OPEN theCursor;
            FETCH NEXT FROM theCursor into @CustomerId, @AccountId, @PremiseId;

            -- Use the same stored procedure as the API
            WHILE @@fetch_status = 0 BEGIN

                    DECLARE @Temp AS TABLE(
                            PremiseKey INT NOT NULL,
                            PremiseId VARCHAR(50) NOT NULL,
                            StartDate DATETIME NOT NULL,
                            EndDate DATETIME NOT NULL,
                            TotalUnits DECIMAL(18,2) NOT NULL ,
                            TotalCost DECIMAL(18,2) NOT NULL,
                            CommodityKey VARCHAR(20) NULL,
                            UomKey VARCHAR(50) NOT NULL,
                            BillDays INT NOT NULL,
                            INDEX IX3 NONCLUSTERED ([PremiseId],[StartDate],[EndDate],[TotalUnits],[TotalCost],[CommodityKey],[UomKey]))

                    INSERT INTO @Temp
                            ( [PremiseKey],
                              [PremiseId] ,
                              [StartDate] ,
                              [EndDate] ,
                              [TotalUnits] ,
                              [TotalCost] ,
                              [CommodityKey] ,
                              [UomKey] ,
                              [BillDays]
                            )
                        EXEC [dbo].[uspBDGetBills] @ClientID, @CustomerId, @AccountId, @PremiseId

                    -- When dealing with Premise Attributes as well as Bills there may be repeats.
                      MERGE @ResultsTable AS rt
                      USING
                              (  SELECT @ClientID AS ClientId,
                              @CustomerId AS CustomerId,
                              @AccountId AS AccountId,
                              [PremiseId],
                              [StartDate],
                              [EndDate],
                              [TotalUnits],
                              [TotalCost],
                              [CommodityKey],
                              [UomKey],
                              [BillDays]
                      FROM @Temp) AS source
                    ON
                        rt.[ClientId] = source.[ClientId] AND
                        rt.[CustomerId] = source.[CustomerId] AND
                        rt.[AccountId] = source.[AccountId] AND
                        rt.[PremiseId] = source.[PremiseId] AND
                        rt.[StartDate] = source.[StartDate] AND
                        rt.[EndDate] = source.[EndDate] AND
                        rt.[CommodityKey] = source.[CommodityKey]
                    WHEN MATCHED THEN
                        UPDATE SET
                                [TotalUnits] = source.[TotalUnits] ,
                                [TotalCost] = source.[TotalCost],
                                [UomKey] = source.[UomKey],
                                [BillDays] = source.[BillDays],
                                [CommodityKey] = source.[CommodityKey]
                    WHEN NOT MATCHED THEN
                        INSERT ( [ClientId]
                          ,[CustomerId]
                          ,[AccountId]
                          ,[PremiseId]
                          ,[StartDate]
                          ,[EndDate]
                          ,[CommodityKey]
                          ,[TotalUnits]
                          ,[TotalCost]
                          ,[UomKey]
                          ,[BillDays] )
                        VALUES (  source.[ClientId]
                          ,source.[CustomerId]
                          ,source.[AccountId]
                          ,source.[PremiseId]
                          ,source.[StartDate]
                          ,source.[EndDate]
                          ,source.[CommodityKey]
                          ,source.[TotalUnits]
                          ,source.[TotalCost]
                          ,source.[UomKey]
                          ,source.[BillDays]);


                    FETCH NEXT FROM theCursor into @CustomerId, @AccountId, @PremiseId;
            END

            -- Send back the contents of the Results Table
            SELECT * FROM @ResultsTable


END





GO
