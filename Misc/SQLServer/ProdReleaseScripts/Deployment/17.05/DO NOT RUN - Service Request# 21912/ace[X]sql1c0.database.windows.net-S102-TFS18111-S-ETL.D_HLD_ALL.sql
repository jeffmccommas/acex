﻿
/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[D_HLD_All]    Script Date: 4/26/2017 2:35:59 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[D_HLD_All]') AND type in (N'P', N'PC'))
DROP PROCEDURE [ETL].[D_HLD_All]
GO

/****** Object:  StoredProcedure [ETL].[D_HLD_All]    Script Date: 4/26/2017 2:35:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[D_HLD_All]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[D_HLD_All] AS'
END
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 8/8/2014
-- Description:
--
-- Phil Victor - 01/12/2017 - Added CostDetails
-- =============================================
ALTER PROCEDURE [ETL].[D_HLD_All]
                 @ClientID INT
AS

BEGIN

    SET NOCOUNT ON;

    TRUNCATE TABLE [Holding].[Billing]
    TRUNCATE TABLE [Holding].[Billing_BillCostDetails]
    TRUNCATE TABLE [Holding].[Billing_ServiceCostDetails]

    TRUNCATE TABLE Holding.Customer
    TRUNCATE TABLE Holding.[Events]
    TRUNCATE TABLE Holding.Premise
    TRUNCATE TABLE Holding.PremiseAttributes
    TRUNCATE TABLE Holding.ActionItem

    TRUNCATE TABLE [Holding].[DisAggReCalcQueue]
    TRUNCATE TABLE [Holding].[DisAggCustomersFromAPI]

END


GO
