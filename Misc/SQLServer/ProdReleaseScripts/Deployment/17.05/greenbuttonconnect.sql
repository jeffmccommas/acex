USE [Insights]
GO
/****** Object:  Index [IX_MeterId]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[greenbutton].[usage_points]') AND name = N'IX_MeterId')
DROP INDEX [IX_MeterId] ON [greenbutton].[usage_points]
GO
/****** Object:  Index [IX_retail_customers_key]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[greenbutton].[retail_customers]') AND name = N'IX_retail_customers_key')
DROP INDEX [IX_retail_customers_key] ON [greenbutton].[retail_customers]
GO
/****** Object:  Index [IX_MeterId]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[greenbutton].[reading_types]') AND name = N'IX_MeterId')
DROP INDEX [IX_MeterId] ON [greenbutton].[reading_types]
GO
/****** Object:  Index [IX_refresh_token_grant_type]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[greenbutton].[authorizations]') AND name = N'IX_refresh_token_grant_type')
DROP INDEX [IX_refresh_token_grant_type] ON [greenbutton].[authorizations]
GO
/****** Object:  Index [IX_code_grant_type]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[greenbutton].[authorizations]') AND name = N'IX_code_grant_type')
DROP INDEX [IX_code_grant_type] ON [greenbutton].[authorizations]
GO
/****** Object:  Index [IX_authorizations]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[greenbutton].[authorizations]') AND name = N'IX_authorizations')
DROP INDEX [IX_authorizations] ON [greenbutton].[authorizations]
GO
/****** Object:  Index [IX_access_token_grant_type]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[greenbutton].[authorizations]') AND name = N'IX_access_token_grant_type')
DROP INDEX [IX_access_token_grant_type] ON [greenbutton].[authorizations]
GO
/****** Object:  Table [greenbutton].[usage_points]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[usage_points]') AND type in (N'U'))
DROP TABLE [greenbutton].[usage_points]
GO
/****** Object:  Table [greenbutton].[usage_point_related_links]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[usage_point_related_links]') AND type in (N'U'))
DROP TABLE [greenbutton].[usage_point_related_links]
GO
/****** Object:  Table [greenbutton].[time_configurations]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[time_configurations]') AND type in (N'U'))
DROP TABLE [greenbutton].[time_configurations]
GO
/****** Object:  Table [greenbutton].[subscription_usage_points]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[subscription_usage_points]') AND type in (N'U'))
DROP TABLE [greenbutton].[subscription_usage_points]
GO
/****** Object:  Table [greenbutton].[subscription]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[subscription]') AND type in (N'U'))
DROP TABLE [greenbutton].[subscription]
GO
/****** Object:  Table [greenbutton].[retail_customers]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[retail_customers]') AND type in (N'U'))
DROP TABLE [greenbutton].[retail_customers]
GO
/****** Object:  Table [greenbutton].[reading_types]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[reading_types]') AND type in (N'U'))
DROP TABLE [greenbutton].[reading_types]
GO
/****** Object:  Table [greenbutton].[authorizations]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[authorizations]') AND type in (N'U'))
DROP TABLE [greenbutton].[authorizations]
GO
/****** Object:  Table [greenbutton].[application_information_scopes]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[application_information_scopes]') AND type in (N'U'))
DROP TABLE [greenbutton].[application_information_scopes]
GO
/****** Object:  Table [greenbutton].[application_information_grant_types]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[application_information_grant_types]') AND type in (N'U'))
DROP TABLE [greenbutton].[application_information_grant_types]
GO
/****** Object:  Table [greenbutton].[application_information]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[application_information]') AND type in (N'U'))
DROP TABLE [greenbutton].[application_information]
GO
/****** Object:  Schema [greenbutton]    Script Date: 4/24/2017 12:12:18 PM ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'greenbutton')
DROP SCHEMA [greenbutton]
GO
ALTER ROLE [db_owner] ADD MEMBER [Aclweb]
GO
/****** Object:  Schema [greenbutton]    Script Date: 4/24/2017 12:12:18 PM ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'greenbutton')
EXEC sys.sp_executesql N'CREATE SCHEMA [greenbutton]'

GO
/****** Object:  Table [greenbutton].[application_information]    Script Date: 4/24/2017 12:12:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[application_information]') AND type in (N'U'))
BEGIN
CREATE TABLE [greenbutton].[application_information](
	[application_information_id] [bigint] IDENTITY(1,1) NOT NULL,
	[uuid] [varchar](255) NOT NULL,
	[self_link_href] [varchar](2083) NULL,
	[self_link_rel] [varchar](2083) NULL,
	[up_link_href] [varchar](2083) NULL,
	[up_link_rel] [varchar](2083) NULL,
	[dataCustodianId] [varchar](64) NOT NULL,
	[dataCustodianApplicationStatus] [tinyint] NOT NULL,
	[dataCustodianScopeSelectionScreenURI] [varchar](2083) NOT NULL,
	[dataCustodianResourceEndpoint] [varchar](2083) NOT NULL,
	[dataCustodianBulkRequestURI] [varchar](2083) NOT NULL,
	[thirdPartyApplicationDescription] [varchar](2000) NULL,
	[thirdPartyApplicationStatus] [tinyint] NULL,
	[thirdPartyUserPortalScreenURI] [varchar](2083) NULL,
	[thirdPartyApplicationType] [tinyint] NULL,
	[thirdPartyApplicationUse] [tinyint] NULL,
	[thirdPartyPhone] [varchar](32) NULL,
	[thirdPartyNotifyUri] [varchar](2083) NOT NULL,
	[thirdPartyScopeSelectionScreenURI] [varchar](2083) NOT NULL,
	[authorizationServerUri] [varchar](2083) NULL,
	[authorizationServerAuthorizationEndpoint] [varchar](2083) NOT NULL,
	[authorizationServerRegistrationEndpoint] [varchar](2083) NULL,
	[authorizationServerTokenEndpoint] [varchar](2083) NOT NULL,
	[client_secret] [nvarchar](512) NOT NULL,
	[logo_uri] [varchar](2083) NULL,
	[client_name] [varchar](256) NOT NULL,
	[client_uri] [varchar](2083) NULL,
	[redirect_uri] [varchar](2083) NOT NULL,
	[client_id] [varchar](64) NOT NULL,
	[tos_uri] [varchar](2083) NULL,
	[policy_uri] [varchar](2083) NULL,
	[software_id] [varchar](256) NOT NULL,
	[software_version] [varchar](32) NOT NULL,
	[client_id_issued_at] [bigint] NOT NULL,
	[client_secret_expires_at] [bigint] NOT NULL,
	[contacts] [varchar](256) NULL,
	[token_endpoint_auth_method] [varchar](64) NOT NULL,
	[registration_client_uri] [varchar](2083) NOT NULL,
	[registration_access_token] [varchar](512) NOT NULL,
	[enabled] [bit] NULL,
	[environment_type] [varchar](10) NULL,
	[published] [datetime] NULL,
	[updated] [datetime] NULL,
 CONSTRAINT [PK_application_information] PRIMARY KEY CLUSTERED 
(
	[application_information_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [greenbutton].[application_information_grant_types]    Script Date: 4/24/2017 12:12:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[application_information_grant_types]') AND type in (N'U'))
BEGIN
CREATE TABLE [greenbutton].[application_information_grant_types](
	[application_information_id] [bigint] NOT NULL,
	[grant_type] [varchar](12) NOT NULL,
 CONSTRAINT [PK_application_information_grant_types] PRIMARY KEY CLUSTERED 
(
	[application_information_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [greenbutton].[application_information_scopes]    Script Date: 4/24/2017 12:12:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[application_information_scopes]') AND type in (N'U'))
BEGIN
CREATE TABLE [greenbutton].[application_information_scopes](
	[application_information_id] [bigint] NOT NULL,
	[scope] [varchar](2000) NOT NULL,
 CONSTRAINT [PK_application_information_scopes] PRIMARY KEY CLUSTERED 
(
	[application_information_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [greenbutton].[authorizations]    Script Date: 4/24/2017 12:12:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[authorizations]') AND type in (N'U'))
BEGIN
CREATE TABLE [greenbutton].[authorizations](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[description] [varchar](250) NULL,
	[published] [datetime] NOT NULL,
	[self_link_href] [varchar](2083) NULL,
	[self_link_rel] [varchar](2083) NULL,
	[up_link_href] [varchar](2083) NULL,
	[up_link_rel] [varchar](2083) NULL,
	[updated] [datetime] NOT NULL,
	[uuid] [varchar](255) NOT NULL,
	[access_token] [varchar](512) NULL,
	[authorization_uri] [varchar](2083) NULL,
	[ap_duration] [bigint] NULL,
	[ap_start] [bigint] NULL,
	[code] [varchar](255) NULL,
	[error] [varchar](25) NULL,
	[error_description] [varchar](256) NULL,
	[error_uri] [varchar](2083) NULL,
	[expiresin] [bigint] NULL,
	[grant_type] [varchar](50) NULL,
	[pp_duration] [bigint] NULL,
	[pp_start] [bigint] NULL,
	[refresh_token] [varchar](512) NULL,
	[resourceURI] [varchar](2083) NULL,
	[responseType] [varchar](5) NULL,
	[scope] [varchar](256) NULL,
	[state] [varchar](256) NULL,
	[status] [tinyint] NULL,
	[third_party] [varchar](50) NOT NULL,
	[tokenType] [tinyint] NULL,
	[application_information_id] [bigint] NULL,
	[retail_customer_id] [bigint] NULL,
	[subscription_id] [bigint] NULL,
	[authCodeExpiresIn] [bigint] NULL,
	[refreshTokenExpiresIn] [bigint] NULL,
	[end_date] [datetime] NULL,
 CONSTRAINT [PK_authorizations] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [greenbutton].[reading_types]    Script Date: 4/24/2017 12:12:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[reading_types]') AND type in (N'U'))
BEGIN
CREATE TABLE [greenbutton].[reading_types](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[uuid] [varchar](255) NOT NULL,
	[published] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[self_link_href] [varchar](2083) NOT NULL,
	[self_link_rel] [varchar](2083) NOT NULL,
	[up_link_href] [varchar](2083) NOT NULL,
	[up_link_ref] [varchar](2083) NOT NULL,
	[accumulationBehaviour] [varchar](255) NULL,
	[aggregate] [varchar](255) NULL,
	[rational_denomiator] [decimal](19, 2) NULL,
	[rational_numerator] [decimal](19, 2) NULL,
	[commodity] [varchar](255) NOT NULL,
	[consumptionsTier] [varchar](255) NULL,
	[cpp] [varchar](255) NULL,
	[currency] [varchar](255) NULL,
	[dateQualifier] [varchar](255) NOT NULL,
	[flowDirection] [varchar](255) NOT NULL,
	[interharmonic_denominator] [decimal](19, 2) NULL,
	[interharmonic_numerator] [decimal](19, 2) NULL,
	[intervalLength] [bigint] NOT NULL,
	[kind] [varchar](255) NOT NULL,
	[measuringPeriod] [varchar](255) NULL,
	[phase] [varchar](255) NOT NULL,
	[powerOfTenMultiplier] [varchar](255) NOT NULL,
	[timeAttribute] [varchar](255) NOT NULL,
	[tou] [varchar](255) NULL,
	[uom] [varchar](255) NOT NULL,
	[meterid] [varchar](50) NULL,
 CONSTRAINT [PK_reading_types] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [greenbutton].[retail_customers]    Script Date: 4/24/2017 12:12:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[retail_customers]') AND type in (N'U'))
BEGIN
CREATE TABLE [greenbutton].[retail_customers](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[data_custodian_client_id] [int] NOT NULL,
	[data_custodian_customer_id] [varchar](50) NOT NULL,
	[data_custodian_account_id] [varchar](50) NOT NULL,
	[data_custodian_premise_id] [varchar](50) NOT NULL,
	[first_name] [varchar](30) NOT NULL,
	[last_name] [varchar](30) NOT NULL,
	[enabled] [bit] NOT NULL,
 CONSTRAINT [PK_retail_customers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [greenbutton].[subscription]    Script Date: 4/24/2017 12:12:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[subscription]') AND type in (N'U'))
BEGIN
CREATE TABLE [greenbutton].[subscription](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[uuid] [varchar](255) NULL,
	[self_link_href] [varchar](255) NULL,
	[self_link_rel] [varchar](255) NULL,
	[up_link_href] [varchar](255) NULL,
	[up_link_rel] [varchar](255) NULL,
	[application_information_id] [bigint] NULL,
	[authorization_id] [bigint] NULL,
	[retail_customer_id] [bigint] NULL,
	[published] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
 CONSTRAINT [PK_subscription] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [greenbutton].[subscription_usage_points]    Script Date: 4/24/2017 12:12:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[subscription_usage_points]') AND type in (N'U'))
BEGIN
CREATE TABLE [greenbutton].[subscription_usage_points](
	[subscription_id] [bigint] NOT NULL,
	[usage_point_id] [bigint] NOT NULL,
 CONSTRAINT [PK_subscription_usage_points] PRIMARY KEY CLUSTERED 
(
	[subscription_id] ASC,
	[usage_point_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
/****** Object:  Table [greenbutton].[time_configurations]    Script Date: 4/24/2017 12:12:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[time_configurations]') AND type in (N'U'))
BEGIN
CREATE TABLE [greenbutton].[time_configurations](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[uuid] [varchar](255) NOT NULL,
	[published] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
	[self_link_href] [varchar](2083) NOT NULL,
	[self_link_rel] [varchar](2083) NOT NULL,
	[up_link_href] [varchar](2083) NOT NULL,
	[up_link_rel] [varchar](2083) NOT NULL,
	[dstEndRule] [varchar](8) NULL,
	[dstStartRule] [varchar](8) NULL,
	[dstOffset] [bigint] NOT NULL,
	[tzOffset] [bigint] NOT NULL,
 CONSTRAINT [PK_time_configurations] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [greenbutton].[usage_point_related_links]    Script Date: 4/24/2017 12:12:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[usage_point_related_links]') AND type in (N'U'))
BEGIN
CREATE TABLE [greenbutton].[usage_point_related_links](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[usage_point_id] [bigint] NOT NULL,
	[href] [varchar](255) NOT NULL,
	[rel] [varchar](255) NOT NULL,
 CONSTRAINT [PK_usage_point_related_links] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [greenbutton].[usage_points]    Script Date: 4/24/2017 12:12:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[greenbutton].[usage_points]') AND type in (N'U'))
BEGIN
CREATE TABLE [greenbutton].[usage_points](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[uuid] [varchar](255) NOT NULL,
	[self_link_href] [varchar](2083) NULL,
	[self_link_rel] [varchar](2083) NULL,
	[up_link_href] [varchar](2083) NULL,
	[up_link_rel] [varchar](2083) NULL,
	[meterid] [varchar](50) NULL,
	[local_time_parameters_id] [bigint] NULL,
	[published] [datetime] NOT NULL,
	[updated] [datetime] NOT NULL,
 CONSTRAINT [PK_subscription_usage_point] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_access_token_grant_type]    Script Date: 4/24/2017 12:12:18 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[greenbutton].[authorizations]') AND name = N'IX_access_token_grant_type')
CREATE NONCLUSTERED INDEX [IX_access_token_grant_type] ON [greenbutton].[authorizations]
(
	[access_token] ASC,
	[grant_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_authorizations]    Script Date: 4/24/2017 12:12:18 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[greenbutton].[authorizations]') AND name = N'IX_authorizations')
CREATE NONCLUSTERED INDEX [IX_authorizations] ON [greenbutton].[authorizations]
(
	[application_information_id] ASC,
	[code] ASC,
	[third_party] ASC,
	[status] ASC,
	[refresh_token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_code_grant_type]    Script Date: 4/24/2017 12:12:18 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[greenbutton].[authorizations]') AND name = N'IX_code_grant_type')
CREATE NONCLUSTERED INDEX [IX_code_grant_type] ON [greenbutton].[authorizations]
(
	[code] ASC,
	[grant_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_refresh_token_grant_type]    Script Date: 4/24/2017 12:12:18 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[greenbutton].[authorizations]') AND name = N'IX_refresh_token_grant_type')
CREATE NONCLUSTERED INDEX [IX_refresh_token_grant_type] ON [greenbutton].[authorizations]
(
	[refresh_token] ASC,
	[grant_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_MeterId]    Script Date: 4/24/2017 12:12:18 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[greenbutton].[reading_types]') AND name = N'IX_MeterId')
CREATE NONCLUSTERED INDEX [IX_MeterId] ON [greenbutton].[reading_types]
(
	[meterid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_retail_customers_key]    Script Date: 4/24/2017 12:12:18 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[greenbutton].[retail_customers]') AND name = N'IX_retail_customers_key')
CREATE NONCLUSTERED INDEX [IX_retail_customers_key] ON [greenbutton].[retail_customers]
(
	[data_custodian_client_id] ASC,
	[data_custodian_customer_id] ASC,
	[data_custodian_account_id] ASC,
	[data_custodian_premise_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_MeterId]    Script Date: 4/24/2017 12:12:18 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[greenbutton].[usage_points]') AND name = N'IX_MeterId')
CREATE NONCLUSTERED INDEX [IX_MeterId] ON [greenbutton].[usage_points]
(
	[meterid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
ALTER AUTHORIZATION ON SCHEMA::greenbutton TO AclWeb  
GO   

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [greenbutton].[time_configurations] ON 

GO
INSERT [greenbutton].[time_configurations] ([id], [uuid], [published], [updated], [self_link_href], [self_link_rel], [up_link_href], [up_link_rel], [dstEndRule], [dstStartRule], [dstOffset], [tzOffset]) VALUES (1, N'31F9E775-3A5F-4770-8B4C-656E549FB134', CAST(N'2017-02-28 16:42:29.130' AS DateTime), CAST(N'2017-02-28 16:42:29.130' AS DateTime), N'/Datacustodian/espi/1_1/resource/LocalTimeParameters/1', N'self', N'/Datacustodian/espi/1_1/resource/LocalTimeParameters', N'up', N'B40E2000', N'360E2000', 3600, -18000)
GO
INSERT [greenbutton].[time_configurations] ([id], [uuid], [published], [updated], [self_link_href], [self_link_rel], [up_link_href], [up_link_rel], [dstEndRule], [dstStartRule], [dstOffset], [tzOffset]) VALUES (2, N'77CE5940-013B-48CE-A3AB-2398A87180A1', CAST(N'2017-02-28 16:47:02.607' AS DateTime), CAST(N'2017-02-28 16:47:02.607' AS DateTime), N'/Datacustodian/espi/1_1/resource/LocalTimeParameters/2', N'self', N'/Datacustodian/espi/1_1/resource/LocalTimeParameters', N'up', N'B40E2000', N'360E2000', 3600, -21600)
GO
INSERT [greenbutton].[time_configurations] ([id], [uuid], [published], [updated], [self_link_href], [self_link_rel], [up_link_href], [up_link_rel], [dstEndRule], [dstStartRule], [dstOffset], [tzOffset]) VALUES (3, N'C30B8614-B117-43A4-AD2E-E20B7A2DE645', CAST(N'2017-02-28 16:47:02.607' AS DateTime), CAST(N'2017-02-28 16:47:02.607' AS DateTime), N'/Datacustodian/espi/1_1/resource/LocalTimeParameters/3', N'self', N'/Datacustodian/espi/1_1/resource/LocalTimeParameters', N'up', N'B40E2000', N'360E2000', 3600, -25200)
GO
INSERT [greenbutton].[time_configurations] ([id], [uuid], [published], [updated], [self_link_href], [self_link_rel], [up_link_href], [up_link_rel], [dstEndRule], [dstStartRule], [dstOffset], [tzOffset]) VALUES (4, N'F2B4020F-9A0A-464A-806A-0B8302F51A7B', CAST(N'2017-02-28 16:47:02.607' AS DateTime), CAST(N'2017-02-28 16:47:02.607' AS DateTime), N'/Datacustodian/espi/1_1/resource/LocalTimeParameters/4', N'self', N'/Datacustodian/espi/1_1/resource/LocalTimeParameters', N'up', N'B40E2000', N'360E2000', 3600, -28800)
GO
INSERT [greenbutton].[time_configurations] ([id], [uuid], [published], [updated], [self_link_href], [self_link_rel], [up_link_href], [up_link_rel], [dstEndRule], [dstStartRule], [dstOffset], [tzOffset]) VALUES (5, N'ED3EDE0C-6548-45DF-9F24-261C38DCD680', CAST(N'2017-02-28 16:47:02.620' AS DateTime), CAST(N'2017-02-28 16:47:02.620' AS DateTime), N'/Datacustodian/espi/1_1/resource/LocalTimeParameters/5', N'self', N'/Datacustodian/espi/1_1/resource/LocalTimeParameters', N'up', N'B40E2000', N'360E2000', 3600, -32400)
GO
INSERT [greenbutton].[time_configurations] ([id], [uuid], [published], [updated], [self_link_href], [self_link_rel], [up_link_href], [up_link_rel], [dstEndRule], [dstStartRule], [dstOffset], [tzOffset]) VALUES (6, N'B4FEA3D8-196B-464E-B68B-2AECC9C3E0BC', CAST(N'2017-02-28 16:47:02.620' AS DateTime), CAST(N'2017-02-28 16:47:02.620' AS DateTime), N'/Datacustodian/espi/1_1/resource/LocalTimeParameters/6', N'self', N'/Datacustodian/espi/1_1/resource/LocalTimeParameters', N'up', N'B40E2000', N'360E2000', 3600, -36000)
GO
SET IDENTITY_INSERT [greenbutton].[time_configurations] OFF
GO