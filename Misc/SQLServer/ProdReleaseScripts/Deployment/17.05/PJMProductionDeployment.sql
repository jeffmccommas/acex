  using InsightsMetaData
  
  update cm.clientconfiguration set value = 'https://sso.pjm.com/access/authenticate/' where environmentkey = 'prod' and clientid = 233 and configurationkey = 'webserviceclient.drhub.authurl'
  
  update cm.clientconfiguration set value = 'pjmauth' where environmentkey = 'prod' and clientid = 233 and configurationkey = 'webserviceclient.drhub.cookiename'

  update cm.clientconfiguration set value = 'https://drhub.pjm.com/drhub' where environmentkey = 'prod' and clientid = 233 and configurationkey = 'webserviceclient.drhub.serviceurl'

  update cm.clientconfiguration set value = 'Ac!ara201704' where environmentkey = 'prod' and clientid = 233 and configurationkey = 'webserviceclient.drhub.password'

  update admin.ETLBulkConfiguration set XMLConfiguration = '<NotificationXmlConfiguration>
<TeamNameMapping>
<MapInfo key="1621515">2017-AK Steel</MapInfo>
<MapInfo key="1621523">2017-American Sand and Gravel_Knapp Foundry_Ohio Star Forge</MapInfo>
<MapInfo key="1621512">2017-Ashta Chemicals_Charter Steel</MapInfo>
<MapInfo key="1621518">2017-Bunting Bearings_Simcote</MapInfo>
<MapInfo key="1621517">2017-Concast_US Steel Tubular Products</MapInfo>
<MapInfo key="1621521">2017-Dietrich Industries_Quaker City Castings_Vallourec Star</MapInfo>
<MapInfo key="1621520">2017-Falcon Foundry</MapInfo>
<MapInfo key="1621510">2017-Linde_Material Science Corporation Walbridge</MapInfo>
<MapInfo key="1621509">2017-North Star Bluescope Steel</MapInfo>
<MapInfo key="1621522">2017-Nucor Steel Marion</MapInfo>
<MapInfo key="1621514">2017-Praxair</MapInfo>
<MapInfo key="1621519">2017-Reserve Alloys</MapInfo>
<MapInfo key="1621516">2017-US Gypsum Co</MapInfo>
<MapInfo key="1621525">2017-Viking Forge Corp</MapInfo>
<MapInfo key="1621511">2017-Worthington Industries</MapInfo>
<MapInfo key="1621513">2017-NASA Lewis Research Center</MapInfo>
<MapInfo key="1621524">2017-Milliron Industries</MapInfo>
</TeamNameMapping>
</NotificationXmlConfiguration>' where clientID = 233 and Environment = 'prod'