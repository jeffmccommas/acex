/********************************************************/
/***** RUN ONLY ON THE INSIGHTS DATABASE              ***/
/********************************************************/

-- =============================================
-- Author:   Muazzam Ali
-- Create date: 5/02/2017
-- Description: Creating ESPM Eventlog tables and insert 
-- stored procedures. Used by NLOG
-- Database Name: Insights database
-- =============================================


/****** Object:  StoredProcedure [portal].[LogInsert]    Script Date: 5/2/2017 12:42:59 PM ******/
DROP PROCEDURE IF EXISTS [portal].[LogInsert]
GO
/****** Object:  StoredProcedure [portal].[EndpointTrackingInsert]    Script Date: 5/2/2017 12:42:59 PM ******/
DROP PROCEDURE IF EXISTS [portal].[EndpointTrackingInsert]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[EventLog]') AND type in (N'U'))
ALTER TABLE [portal].[EventLog] DROP CONSTRAINT IF EXISTS [DF_EventLog_NewDate]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[EndpointTracking]') AND type in (N'U'))
ALTER TABLE [portal].[EndpointTracking] DROP CONSTRAINT IF EXISTS [DF_EndpointTracking_NewDate]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[EndpointTracking]') AND type in (N'U'))
ALTER TABLE [portal].[EndpointTracking] DROP CONSTRAINT IF EXISTS [DF_EndpointTracking_ClientID]
GO
/****** Object:  Table [portal].[EventLog]    Script Date: 5/2/2017 12:42:59 PM ******/
DROP TABLE IF EXISTS [portal].[EventLog]
GO
/****** Object:  Table [portal].[EndpointTracking]    Script Date: 5/2/2017 12:42:59 PM ******/
DROP TABLE IF EXISTS [portal].[EndpointTracking]
GO
/****** Object:  Table [portal].[EndpointTracking]    Script Date: 5/2/2017 12:42:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[EndpointTracking]') AND type in (N'U'))
BEGIN
CREATE TABLE [portal].[EndpointTracking](
	[EndpointTrackingID] [int] IDENTITY(1,1) NOT NULL,
	[ClientID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[Verb] [varchar](8) NULL,
	[ResourceName] [varchar](500) NULL,
	[EndpointID] [int] NULL,
	[UserAgent] [varchar](128) NULL,
	[SourceIPAddress] [varchar](32) NULL,
	[XCEMessageId] [varchar](64) NULL,
	[XCEChannel] [varchar](64) NULL,
	[XCELocale] [varchar](64) NULL,
	[XCEMeta] [varchar](64) NULL,
	[Query] [varchar](256) NULL,
	[MachineName] [varchar](50) NULL,
	[IncludeInd] [bit] NULL,
	[LogDate] [datetime] NULL,
	[NewDate] [datetime] NOT NULL,
	[PartitionKey] [tinyint] NOT NULL,
	[ThirdPartyClientId] [varchar](64) NULL,
 CONSTRAINT [PK_EndpointTracking] PRIMARY KEY CLUSTERED 
(
	[EndpointTrackingID] ASC,
	[PartitionKey] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO
/****** Object:  Table [portal].[EventLog]    Script Date: 5/2/2017 12:42:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[EventLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [portal].[EventLog](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[EventLevel] [varchar](50) NULL,
	[LoggerName] [varchar](500) NULL,
	[MachineName] [varchar](100) NULL,
	[LogMessage] [varchar](max) NULL,
	[PartitionKey] [tinyint] NOT NULL,
	[Exception] [varchar](max) NULL,
	[LogDate] [datetime] NULL,
	[NewDate] [datetime] NOT NULL,
 CONSTRAINT [PK_EventLog] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC,
	[PartitionKey] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[DF_EndpointTracking_ClientID]') AND type = 'D')
BEGIN
ALTER TABLE [portal].[EndpointTracking] ADD  CONSTRAINT [DF_EndpointTracking_ClientID]  DEFAULT ((0)) FOR [ClientID]
END

GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[DF_EndpointTracking_NewDate]') AND type = 'D')
BEGIN
ALTER TABLE [portal].[EndpointTracking] ADD  CONSTRAINT [DF_EndpointTracking_NewDate]  DEFAULT (getutcdate()) FOR [NewDate]
END

GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[DF_EventLog_NewDate]') AND type = 'D')
BEGIN
ALTER TABLE [portal].[EventLog] ADD  CONSTRAINT [DF_EventLog_NewDate]  DEFAULT (getutcdate()) FOR [NewDate]
END

GO
/****** Object:  StoredProcedure [portal].[EndpointTrackingInsert]    Script Date: 5/2/2017 12:42:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[EndpointTrackingInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [portal].[EndpointTrackingInsert] AS' 
END
GO



ALTER PROCEDURE [portal].[EndpointTrackingInsert]
    @ClientID INT,
    @UserID INT,
	@Verb VARCHAR(8),
    @ResourceName VARCHAR(500),
    @EndpointID INT,
    @UserAgent VARCHAR(128),
    @SourceIPAddress VARCHAR(32),
	@XCEMessageID VARCHAR(64),
    @XCEChannel VARCHAR(64),
    @XCELocale VARCHAR(64),
    @XCEMeta VARCHAR(64),
    @Query VARCHAR(256),
	@MachineName VARCHAR(50),
	@IncludeInd BIT,
	@LogDate DATETIME,
	@ThirdPartyClientId VARCHAR(64) = NULL
  
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @currentDate DATETIME
    DECLARE @partitionKey TINYINT
     
    SET     @currentDate = GETDATE()
    SET     @partitionKey = DATEPART(WEEKDAY, @currentDate)
 
    INSERT INTO [portal].[EndpointTracking]
           ([ClientID]
		   ,[UserID]
		   ,[Verb]
           ,[ResourceName]
           ,[EndpointID]
           ,[UserAgent]
           ,[SourceIPAddress]
           ,[XCEMessageId]
           ,[XCEChannel]
           ,[XCELocale]
		   ,[XCEMeta]
		   ,[Query]
		   ,[MachineName]
		   ,[IncludeInd]
		   ,[LogDate]
		   ,[PartitionKey]
		   ,[ThirdPartyClientId])
     VALUES
           (@ClientID
		   ,@UserID
           ,@Verb
           ,@ResourceName
           ,@EndpointID
           ,@UserAgent
           ,@SourceIPAddress
           ,@XCEMessageID
           ,@XCEChannel
           ,@XCELocale
		   ,@XCEMeta
		   ,@Query
		   ,@MachineName
		   ,@IncludeInd
		   ,@LogDate
		   ,@partitionKey
		   ,@ThirdPartyClientId);
END





GO
/****** Object:  StoredProcedure [portal].[LogInsert]    Script Date: 5/2/2017 12:42:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[LogInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [portal].[LogInsert] AS' 
END
GO


ALTER PROCEDURE [portal].[LogInsert]
    @UserID INT,
    @Level VARCHAR(50),
    @Logger VARCHAR(500),
    @MachineName VARCHAR(50),
    @Message VARCHAR(MAX),
	@Exception VARCHAR(MAX),
	@LogDate DATETIME
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @currentDate DATETIME
    DECLARE @partitionKey TINYINT
     
    SET     @currentDate = GETDATE()
    SET     @partitionKey = DATEPART(WEEKDAY, @currentDate)
 
    INSERT INTO [portal].[EventLog]
           ([UserID]
           ,[EventLevel]
           ,[LoggerName]
           ,[MachineName]
           ,[LogMessage]
		   ,[Exception]
		   ,[LogDate]
		   ,[PartitionKey])
     VALUES
           (@UserID
           ,@Level
           ,@Logger
           ,@MachineName
           ,@Message
		   ,@Exception
           ,@LogDate
		   ,@partitionKey);
END




GO
