/********************************************************/
/***** RUN ON INSIGHTSMETADATA ***/
/***** UPDATE TABlES DATA ************************/
/********************************************************/
SELECT BuildingCategoryId, BuildingSizeId, EUICooking, ClientId, ClimateId
INTO #temp				  
FROM [cm].[BEMClientBuildingCategorySizeEUI] WHERE clientId = 0 AND CommodityId = 1


UPDATE
    Table_A
SET
    Table_A.EUICooking = Table_B.EUICooking
FROM
    [cm].[BEMClientBuildingCategorySizeEUI] AS Table_A
    Inner JOIN #temp AS Table_B
        ON Table_A.BuildingCategoryId = Table_B.BuildingCategoryId
		AND Table_A.ClientId = Table_B.ClientId
		AND Table_B.BuildingSizeId = Table_A.BuildingSizeId
		AND Table_B.ClimateId = Table_A.ClimateId
WHERE
    Table_A.ClientId = 0
    AND Table_A.CommodityId = 2

DROP TABLE #temp