/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_224 DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


-- =============================================
-- Author:   Muazzam Ali / Jayaraman, Vishwanath
-- Create date: 6/08/2017
-- Description: Creating new portal.ESPMMetersWhiteList Table to
-- Store white list to safely test in production
-- Database Name: Insight DW database
-- =============================================


/****** Object:  Table [portal].[ESPMMetersWhiteList]    Script Date: 6/08/2017 12:41:05 PM ******/
DROP TABLE IF EXISTS [portal].[ESPMMetersWhiteList]
GO

/****** Object:  Table [portal].[ESPMMetersWhiteList]    Script Date: 6/8/2017 12:48:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[ESPMMetersWhiteList]') AND type in (N'U'))
BEGIN
CREATE TABLE [portal].[ESPMMetersWhiteList](
	[ListId] [int] IDENTITY(1,1) NOT NULL,
	[MeterId] [varchar](200) NOT NULL,
	[MeterType] [varchar](20) NULL,
	[Reason] [varchar](255) NULL,
	[UpdatedBy] [int] NOT NULL,
	[UpdatedDate] [date] NOT NULL,
 CONSTRAINT [PK_ESPMMetersWhiteList] PRIMARY KEY CLUSTERED 
(
	[ListId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO
