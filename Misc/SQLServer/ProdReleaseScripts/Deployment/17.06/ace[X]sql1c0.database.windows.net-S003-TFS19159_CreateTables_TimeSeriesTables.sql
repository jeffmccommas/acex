/********************************************************/
/***** RUN ON SeriesMgt1 ***/
/***** CREATE NEW TABLES ************************/
/********************************************************/


--USE [SeriesMgt1]
--GO
--if not exists (select * from master.dbo.syslogins where loginname = N'aclseriesmgt1sqllogin')
--BEGIN
--	declare @logindb nvarchar(132), @loginlang nvarchar(132)
--    select @logindb = N'master', @loginlang = N'us_english'
--	if @logindb is null or not exists (select * from master.dbo.sysdatabases where name = @logindb)
--		select @logindb = N'master'
--	if @loginlang is null or (not exists (select * from master.dbo.syslanguages where name = @loginlang) and @loginlang <> N'us_english')
--		select @loginlang = @@language
--	exec sp_addlogin N'aclseriesmgt1sqllogin', 'aclseriesmgt1#1sqllogin', @logindb, @loginlang
--END
--GO

--alter login aclseriesmgt1sqllogin with default_database = SeriesMgt1;
--GO
--create user aclseriesmgt1user from login aclseriesmgt1sqllogin;
--GO
--exec sp_addrolemember 'db_owner', aclseriesmgt1user
--GO


/****** Object:  Table [dbo].[SeriesCollectionInfo]    Script Date: 2/24/2017 2:56:58 PM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
CREATE TABLE [SeriesMgt1].[dbo].SeriesCollectionInfo
    (
      [id] [BIGINT] IDENTITY(1, 1)
                    NOT NULL ,
      [Specs] [NVARCHAR](512) NULL ,
      [TimeSeries] [NVARCHAR](512) NULL ,
      [CollectionId] [NVARCHAR](32) NOT NULL ,
      [AltCollectionId] [NVARCHAR](32) NULL ,
      [TClid] [NVARCHAR](32) NULL ,
      [TCustid] [NVARCHAR](32) NULL ,
      [TAcctid] [NVARCHAR](32) NULL ,
      [State] [NVARCHAR](32) NULL ,
      [Archive] [NVARCHAR](512) NULL
    )
ON  [PRIMARY]; 
GO

/**
This is enough information to manage meter time series and not to manage meters.
It has information to map back to the existing meter repo and forward to the maintained time series.
id is the unique auto generated key for the meter spec.
'Temporary' client, customer, and account ID are ther -- temporary because we should be able to normalize to the point we dont need them.
Specs is some info which means something to the one who created this meter spec
TimeSeries is a simple solution for a forward map to SeriesInfo.  It is a csv list of SeriesInfio.id. 
Expect at most 10's of these and will go relational if it becomes more numerous.
CollectionId is the "foreign key" -- the unique text id that is used in the current system such as meter-id, servicpointid, 
etc. It is up to the main system to keep this unique and to provide mapping info (e.g., when series data shows up what
is the field that maps the AMI 'meterid').  This is not necessarially the same as the 'physical meter id'.  It is sized
to support a GUID but doesn't have to be one.
State records the active/inactive/whatever for accumulationg on any owned series
Archive records historical data the 'owner' of this meter and series definitions when ownership changes.
State and archive retain data and tracability while a MeterInfo is 'active' after a meter is 'moved' to a new customer.
If a MeterId is 'moved' to another customer a new Meterinfo is created and new series are generated.
Old data is retained and can be recalled however the "current" data will be active for the latest owner.

Initial integration is for automated initialization from billing data (no UL management) so will copy things like service point 
into 'alt' collection id (this should be an option for the series collection based on UL wishes.)
Similiarly keep other id's such as clientID, AccountID, customerID since may want to query/aggregate on these prior to 
full UL integration.
Note: look into graph database for this.

**/


INSERT  INTO [SeriesMgt1].[dbo].SeriesCollectionInfo
        ( [Specs] ,
          [TimeSeries] ,
          [CollectionId] ,
          State
        )
VALUES  ( 'collectionStarterSpecs' ,
          '10000000' ,
          'collectionStarterId' ,
          'active'
        );
GO

--GRANT SELECT, INSERT, UPDATE, DELETE ON SeriesCollectionInfo to aclseriesmgt1sqllogin
--GO

/****** Object:  Table [dbo].[SeriesInfo]    Script Date: 2/24/2017 2:56:58 PM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
CREATE TABLE [SeriesMgt1].[dbo].[SeriesInfo]
    (
      [id] [BIGINT] IDENTITY(10000000, 1)
                    NOT NULL ,
      [Specs] [NVARCHAR](512) NULL ,
      [CollectionKey] [BIGINT] NULL ,
      [DidSpec] [NVARCHAR](64) NOT NULL ,
      [TSSpec] [NVARCHAR](64) NOT NULL
    )
ON  [PRIMARY]; 
GO
INSERT  INTO [SeriesMgt1].[dbo].[SeriesInfo]
        ( [Specs] ,
          [CollectionKey] ,
          [DidSpec] ,
          [TSSpec]
        )
VALUES  ( 'seriesStarterSpecs' ,
          0 ,
          'yyyyMM' ,
          'yyyy-MM-dd HH:mm:ssZ'
        );
GO
--GRANT SELECT, INSERT, UPDATE, DELETE ON SeriesInfo to aclseriesmgt1sqllogin
--GO



/**
This maintains information about a cassandra time series being maintained.
id is the unique auto generated key for the series spec.
Specs is some info wich means something to the one who created this series spec
[CollectionKey] is the "foreign key" to the SeriesCollectionInfo table 'id'.
id and DidSPec and TSSpec are used to create the partition key/clustering index for the 
cassandra time series as shown below. id is used as is.
DidSPec is used via format to select a part of the timestamp to bucketize the cassandra data
Together these create a hash friendly and efficient partition key
TSSpec is used via format to prepare the time stamp for the cassandra data
**/






/**
Cassandra data schema (as of this writing)

CREATE TABLE aclara_dev_01.value_series_meta (
    id bigint,
	did bigint,
    ts timestamp,
    val double,
	meta map<text,text>,
    PRIMARY KEY ((id, did), ts)
) WITH CLUSTERING ORDER BY (ts DESC)

Most columns should be self explainatory.
meta is intended to be name/value pairs for additional datasuch as 'estimate' and might be surfaced in additional columns
Expectation is that cassandra rowsets will be retreived and additional relational processing performed on returned rowsets.

**/
GO


