/********************************************************/
/***** RUN ON MASTER ***/
/***** CREATE A NEW DATABASE ************************/
/********************************************************/
IF EXISTS ( SELECT  name
            FROM    master.dbo.sysdatabases
            WHERE   name = N'SeriesMgt1' )
    DROP DATABASE [SeriesMgt1];
GO

CREATE DATABASE [SeriesMgt1];  
GO