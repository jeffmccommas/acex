

/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[S_PremiseAttributes]    Script Date: 5/24/2017 2:08:33 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_PremiseAttributes]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [ETL].[S_PremiseAttributes]
GO

/****** Object:  StoredProcedure [ETL].[S_PremiseAttributes]    Script Date: 5/24/2017 2:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_PremiseAttributes]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[S_PremiseAttributes] AS'
END
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 7/21/2014
-- Description:
-- =============================================
ALTER PROCEDURE [ETL].[S_PremiseAttributes]
                @ClientID INT
AS

BEGIN

        SET NOCOUNT ON;

        SELECT ClientID, CustomerID, AccountID, PremiseID, AttributeId, AttributeKey, OptionId, OptionValue, SourceId, TrackingId, TrackingDate
        FROM
        (SELECT ClientID, AccountID, CustomerID, PremiseID, AttributeId, AttributeKey, OptionId, OptionValue, SourceId, TrackingId, TrackingDate
                ,ROW_NUMBER() OVER ( PARTITION BY
                ClientId, PremiseId, AccountId, AttributeKey
                ORDER BY TrackingDate DESC ) AS SortId
        FROM Holding.PremiseAttributes WITH(NOLOCK)
        WHERE ClientId = @ClientID) b
        WHERE SortId = 1
        ORDER BY ClientId, CustomerID, AccountId, PremiseId, AttributeKey

END

GO

/****** Object:  View [Holding].[v_PremiseAttributes]    Script Date: 5/24/2017 2:12:20 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Holding].[v_PremiseAttributes]'))
    DROP VIEW [Holding].[v_PremiseAttributes]
GO

/****** Object:  View [Holding].[v_PremiseAttributes]    Script Date: 5/24/2017 2:12:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Holding].[v_PremiseAttributes]'))
        EXEC dbo.sp_executesql @statement = N'
        -- =============================================
        -- Author:      Jason Khourie
        -- Create date: 6/24/2014
        -- =============================================

        CREATE VIEW [Holding].[v_PremiseAttributes]
        AS
            SELECT  theSelected.ClientID ,
                    theSelected.AccountID ,
                    theSelected.CustomerID ,
                    theSelected.PremiseID ,
                    theSelected.AttributeId ,
                    theSelected.AttributeKey ,
                    theSelected.OptionId ,
                    theSelected.OptionValue ,
                    theSelected.SourceId ,
                    theSelected.TrackingID ,
                    theSelected.TrackingDate
            FROM    ( SELECT    pa.ClientID ,
                                pa.AccountID ,
                                pa.CustomerID ,
                                pa.PremiseID ,
                                pa.AttributeId ,
                                pa.AttributeKey ,
                                pa.OptionId ,
                                pa.OptionValue ,
                                pa.SourceId ,
                                pa.TrackingID ,
                                pa.TrackingDate ,
                                ROW_NUMBER() OVER ( PARTITION BY pa.ClientID,
                                                    pa.AccountID, pa.CustomerID,
                                                    pa.PremiseID, pa.AttributeId, pa.AttributeKey ORDER BY pa.TrackingDate DESC ) AS theRank
                      FROM      Holding.PremiseAttributes pa WITH ( NOLOCK )
                                INNER JOIN Holding.Client cl ON cl.ClientId = pa.ClientID
                    ) theSelected
            WHERE   theSelected.theRank = 1;'
GO
