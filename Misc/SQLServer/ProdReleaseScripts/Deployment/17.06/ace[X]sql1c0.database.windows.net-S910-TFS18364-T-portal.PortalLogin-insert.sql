/********************************************************/
/***** RUN ONLY ON THE INSIGHTS DATABASE ***/
/********************************************************/


-- =============================================
-- Author:   Muazzam Ali / Jayaraman, Vishwanath
-- Create date: 6/19/2017
-- Description: Insert User info for web job
-- Database Name: Insights database
-- =============================================


BEGIN
   IF NOT EXISTS (SELECT * FROM [portal].[PortalLogin] WHERE Username = 'UILWEBJOBUSER')
                   
   BEGIN
       INSERT INTO [portal].[PortalLogin] (Username,ClientID,FirstName,LastName,PasswordHash,PasswordSalt,UpdateDate,EnvID,UserType,Enabled,FailedLoginCount)
       VALUES ('UILWEBJOBUSER',224,'WebJob','WebJob','JqnBUysxCwQWh3lnKIYztYVhm3lDgRuF8Zy00jF+1Wk=','b8411507-a630-4da4-b78c-6aa6d0e535a3',getdate(),2,  'Web',1,0)
   END
END
