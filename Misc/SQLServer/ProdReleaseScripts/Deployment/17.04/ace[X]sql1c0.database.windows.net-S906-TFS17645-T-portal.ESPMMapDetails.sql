/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_224 DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

-- =============================================
-- Author:   Muazzam Ali / Jayaraman, Vishwanath
-- Create date: 4/14/2017
-- Description: Creating  ESPM Map Details Table 
-- Database Name: Insight DW database
-- =============================================


/****** Object:  Table [portal].[EspmMapDetails]    Script Date: 4/14/2017 11:02:49 AM ******/
DROP TABLE IF EXISTS [portal].[EspmMapDetails]
GO

/****** Object:  Table [portal].[EspmMapDetails]    Script Date: 4/14/2017 11:02:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[portal].[EspmMapDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [portal].[EspmMapDetails](
	[MapDetailId] [int] IDENTITY(1,1) NOT NULL,
	[MapId] [int] NOT NULL,
	[MapAccountId] [nvarchar](50) NOT NULL,
	[MapPremiseId] [nvarchar](50) NOT NULL,
	[Enabled] [bit] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_EspmMapDetails] PRIMARY KEY CLUSTERED 
(
	[MapDetailId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO
