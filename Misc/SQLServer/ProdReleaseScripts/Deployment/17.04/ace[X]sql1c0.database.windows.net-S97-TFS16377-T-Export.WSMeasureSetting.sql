﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  Table [Export].[WSMeasureSetting]    Script Date: 4/5/2017 2:39:08 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSMeasureSetting]') AND type in (N'U'))
    DROP TABLE [Export].[WSMeasureSetting]
GO

/****** Object:  Table [Export].[WSMeasureSetting]    Script Date: 4/5/2017 2:39:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSMeasureSetting]') AND type in (N'U'))
BEGIN
    CREATE TABLE [Export].[WSMeasureSetting](
        [ProfileName] [varchar](50) NOT NULL,
        [ClientId] [int] NOT NULL,
        [Name] [varchar](100) NOT NULL,
        [Value] [varchar](4000) NOT NULL,
        [Description] [varchar](4000) NULL,
    PRIMARY KEY CLUSTERED
        (
            [ProfileName] ASC,
            [ClientId] ASC,
            [Name] ASC
        )WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
    )
END
GO

INSERT [Export].[WSMeasureSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.201705.1.M1', 224, N'account_mapper_expression', N'(1 = 1) /* everyone */', N'acct mapper expression')
GO
INSERT [Export].[WSMeasureSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.201705.1.M1', 224, N'actions', N'acvacation,avoidheatunoccupied,keeptempssimilar,precoolhomeoutdoor,raisetempac,raisetemproomac,settempwanted,shiftcacregular,shiftroomacregular,timerwindowac', N'action list')
GO
INSERT [Export].[WSMeasureSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.201705.1.M1', 224, N'fixed_savings', N'0', N'fixed savings value')
GO
INSERT [Export].[WSMeasureSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.201705.1.M1', 224, N'track_history', N'1', N'track measures history')
GO

INSERT [Export].[WSMeasureSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.201705.1.M2', 224, N'account_mapper_expression', N'(1 = 1) /* everyone */', N'acct mapper expression')
GO
INSERT [Export].[WSMeasureSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.201705.1.M2', 224, N'actions', N'airdrylaundry,avoidoverdryingclothes,closecurtains,cookoutside,install_ledgeneral,runwholehousefan,skipdrydishwasher,useceilingfans,useventfans,usewindowfan,washclothesincold', N'action list')
GO
INSERT [Export].[WSMeasureSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.201705.1.M2', 224, N'fixed_savings', N'0', N'fixed savings value')
GO
INSERT [Export].[WSMeasureSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.201705.1.M2', 224, N'track_history', N'1', N'track measures history')
GO

INSERT [Export].[WSMeasureSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'TBD', 224, N'account_mapper_expression', N'([staging].[HESParticipant] = ''1'')', N'acct mapper expression')
GO
INSERT [Export].[WSMeasureSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'TBD', 224, N'actions', N'TBD', N'action list')
GO
INSERT [Export].[WSMeasureSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'TBD', 224, N'fixed_savings', N'0', N'fixed savings value')
GO
INSERT [Export].[WSMeasureSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'UIHES', 224, N'account_mapper_expression', N'([staging].[HESParticipant] = ''0'')', N'acct mapper expression')
GO
INSERT [Export].[WSMeasureSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'UIHES', 224, N'actions', N'caulkandweatherize, improveinstallroomac, insulateattic, insulateceiling, insulateductwork, sealairducts', N'action list')
GO
INSERT [Export].[WSMeasureSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'UIHES', 224, N'fixed_savings', N'0', N'fixed savings value')
GO
