﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  StoredProcedure [Export].[WS_Measures_ProcessPromo]    Script Date: 4/3/2017 3:29:19 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_ProcessPromo]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [Export].[WS_Measures_ProcessPromo]
GO

/****** Object:  StoredProcedure [Export].[WS_Measures_ProcessPromo]    Script Date: 4/3/2017 3:29:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_ProcessPromo]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WS_Measures_ProcessPromo] AS'
END
GO


-------------------------------------------------------------------------------
--  Name:               [Export].[WS_Measures_ProcessPromo]
--  Version:            1
--  Author:             Philip Victor
--  Description :     Process a measure profile that is configured as a promotion
----------------------------------------------------------------------------
ALTER PROCEDURE [Export].[WS_Measures_ProcessPromo]
        @reportProfileName [VARCHAR](50) ,
        @measureProfileName [VARCHAR](50) ,
        @measureNumber [INT] ,
        @runId [UNIQUEIDENTIFIER]  ,
        @isTrialRun [BIT] = 0
        WITH RECOMPILE          -- force generation of query plan due to usage of temptable in static query.
AS
BEGIN

        SET NOCOUNT ON;

        DECLARE @client_id [INT] ,
                @actions [VARCHAR](2000) ,
                @fixed_savings DECIMAL ,
                @is_trial_run BIT ,
                @tab_char [CHAR](1) = CHAR(9) ,     -- tab character
                @progress_msg [VARCHAR](5000) ,
                @component [VARCHAR](400) = '[Export].[WS_Measures_ProcessPromo]' ,
                @count [INT];

        SELECT
        @client_id = [ClientId]
                FROM [Export].[WSReportProfile]
                WHERE [ProfileName] = @reportProfileName;


    -- measure settings
        DECLARE @measure_setting TABLE (
                                        [profile_name] [VARCHAR](100) ,
                                        [actions] [VARCHAR](2000) ,
                                        [fixed_savings] [VARCHAR](100)
        );


    --  grab the measure settings we are interesting in
        INSERT @measure_setting ([profile_name], [actions], [fixed_savings])
                        EXEC [Export].[WS_Measures_PivotMeasureSetting] @clientId = @client_id, @measureProfileName = @measureProfileName, @settingList = 'actions, fixed_savings';


    -- setup the softmodelled measure settings
        SELECT
        @actions = [actions], @fixed_savings = CASE WHEN ISNUMERIC([fixed_savings]) = 1 THEN CAST([fixed_savings] AS DECIMAL)
                                                    ELSE CAST(NULL AS DECIMAL)
                                               END
                FROM @measure_setting;

        SET @progress_msg = '@isTrialRun:' + CAST(@isTrialRun AS VARCHAR);
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        SET @progress_msg = 'Processing promos - Start     ...   ' + '@reportProfileName:''' + ISNULL(@reportProfileName, '') + '''; ' + '@measureNumber:'''
                + ISNULL(CAST(@measureNumber AS [VARCHAR]), '') + '''; ' + '@measureProfileName:''' + ISNULL(@measureProfileName, '') + '''; '
                + '@fixed_savings:''' + ISNULL(CAST(@fixed_savings AS [VARCHAR]), '') + '''; ' + '@actions:''' + ISNULL(@actions, '') + '''; ';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

    -- for promotion we will simple use the promotion definition to populate the ranking table.
    -- populate [#WS_Measures_ActionItemRankingWork] with accounts that have been mapped to the measure we are processing
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = 'Populating [#WS_Measures_ActionItemRankingWork]';

        INSERT INTO [#WS_Measures_ActionItemRankingWork] ([MeasureId], [ActionItemDesc], [PremiseKey], [RefSavings], [RankId], [LevelNum])
                SELECT DISTINCT @measureProfileName,        -- this will be used as the MeasureId...it will eventually make its way onto the report (aka promo code)
                                @measureProfileName,        -- this will be used as the ActionItemDesc...it will eventually make its way onto the report (aka promo code) as MTitleN, where N is the measure#
                                [PremiseKey], @fixed_savings, 1, 1
                        FROM [#WS_Measures_AccountMeasureProfileMap] AS [account_list]
                        WHERE [account_list].[MeasureNumber] = @measureNumber
                                AND [account_list].[MeasureProfileName] = @measureProfileName;


        SELECT @count = COUNT(*)
                FROM  [#WS_Measures_ActionItemRankingWork];
        SELECT @progress_msg = '[#WS_Measures_ActionItemRankingWork] contains ' + CAST(@count AS VARCHAR) + ' rows';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        SET @progress_msg = 'Processing promos - Success   ...' + '@reportProfileName:''' + ISNULL(@reportProfileName, '') + '''; ' + '@measureNumber:'''
                + ISNULL(CAST(@measureNumber AS [VARCHAR]), '') + '''; ' + '@measureProfileName:''' + ISNULL(@measureProfileName, '') + '''; ';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;


END;

GO
