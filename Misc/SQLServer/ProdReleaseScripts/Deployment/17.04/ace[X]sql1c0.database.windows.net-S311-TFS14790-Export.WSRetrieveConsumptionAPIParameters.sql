﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  StoredProcedure [Export].[WSRetrieveConsumptionAPIParameters]    Script Date: 2/28/2017 1:17:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSRetrieveConsumptionAPIParameters]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [Export].[WSRetrieveConsumptionAPIParameters]
GO

/****** Object:  StoredProcedure [Export].[WSRetrieveConsumptionAPIParameters]    Script Date: 2/28/2017 1:17:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSRetrieveConsumptionAPIParameters]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WSRetrieveConsumptionAPIParameters] AS'
END
GO

-- =============================================
-- Author:      Philip Victor
-- Create date: 2/13/2017
-- Description: Query to retrieve the parameters needed to get Meter Usage via the Consumption API.
-- =============================================
ALTER PROCEDURE [Export].[WSRetrieveConsumptionAPIParameters]
    -- Add the parameters for the stored procedure here
    @ClientId INT ,
    @GroupId INT ,
    @Commodity VARCHAR(MAX)
AS
    BEGIN

    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements
        SET NOCOUNT ON;

        DECLARE @CommodityId AS INT;
        DECLARE @CommodityKey AS INT;

        SELECT  @CommodityId = [CommodityId] ,
                @CommodityKey = [CommodityKey]
        FROM    [dbo].[DimCommodity]
        WHERE   [CommodityDesc] = @Commodity;

        SELECT  [subQ].[CustomerId] ,
                [subQ].[AccountId] ,
                [subQ].[PremiseId] ,
                [subQ].[ServiceContractKey] ,
                CONVERT(VARCHAR(MAX), ISNULL(STUFF(( SELECT DISTINCT (', ' + [dm].[MeterId] )
                                                     FROM   [dbo].[DimMeter] dm INNER JOIN
                                                     [dbo].[FactServicePoint] fsp ON [fsp].[ServicePointKey] = [dm].[ServicePointKey] AND [fsp].[ClientId] = [dm].[ClientId]
                                                     WHERE  [dm].[ClientId] = @ClientId
                                                            AND [fsp].[ServiceContractKey] = [subQ].[ServiceContractKey]
                                                            AND [dm].[CommodityKey] = @CommodityKey
                                                   FOR
                                                     XML PATH('')
                                                   ), 1, 1, ''), '')) AS MeterIds
        FROM    ( SELECT    [dc].[CustomerId] ,
                            [fspb].[AccountId] ,
                            [fspb].[PremiseId] ,
                            [fsp].[ServiceContractKey]
                  FROM      [Export].[WSBenchmarkGroups] wsbmk
                            INNER  JOIN [dbo].[DimServicePoint] dsp ON [dsp].[PremiseKey] = [wsbmk].[PremiseKey]
                                                              AND [dsp].[ClientId] = [wsbmk].[ClientID]
                            INNER JOIN [dbo].[FactServicePoint] fsp ON [fsp].[ServicePointKey] = [dsp].[ServicePointKey]
                                                              AND [fsp].[ClientId] = [dsp].[ClientId]
                            INNER JOIN [dbo].[FactServicePointBilling] [fspb] ON [fspb].[ServiceContractKey] = [fsp].[ServiceContractKey]
                                                              AND [fspb].[ClientId] = [fsp].[ClientId]
                                                              AND fspb.[CommodityId] = @CommodityId
                            INNER  JOIN [dbo].[FactCustomerPremise] fcp ON [fcp].[PremiseKey] = [wsbmk].[PremiseKey]
                                                              AND [fcp].[ClientId] = [fspb].[ClientId]
                            INNER JOIN [dbo].[DimCustomer] dc ON [dc].[CustomerKey] = [fcp].[CustomerKey]
                                                              AND [dc].[ClientId] = [fcp].[ClientId]
                  WHERE     [wsbmk].[ClientID] = @ClientId
                            AND [wsbmk].[GroupID] = @GroupId
                  GROUP BY  [wsbmk].[GroupID] ,
                            [dc].[CustomerId] ,
                            [fspb].[AccountId] ,
                            [fspb].[PremiseId] ,
                            [fsp].[ServiceContractKey]
                ) subQ;

    END;


GO

