﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  StoredProcedure [Export].[WS_CreateSnapshot]    Script Date: 4/9/2017 4:50:15 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_CreateSnapshot]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [Export].[WS_Measures_CreateSnapshot]
GO

/****** Object:  StoredProcedure [Export].[WS_Measures_CreateSnapshot]    Script Date: 4/9/2017 4:50:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_CreateSnapshot]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WS_Measures_CreateSnapshot] AS'
END
GO

    -------------------------------------------------------------------------------
    --  Name:               [Export].[WS_Measures_CreateSnapshot]
    --  Author:             Philip Victor
    --  Description :    This will create a snapshot of the current report
    -----------------------------------------------------------------------------
    ALTER PROCEDURE [Export].[WS_Measures_CreateSnapshot] @reportAlias [VARCHAR](100) ,
        @profileName [VARCHAR](100) ,
        @reportYear [VARCHAR](4) ,
        @reportMonth [VARCHAR](2) ,
        @reportNumber [INT] ,
        @runId [UNIQUEIDENTIFIER],
        @isTrialRun [BIT]
    AS
    BEGIN


        DECLARE @tablename [VARCHAR](256) = '[Export].[WS_Measures_Daily_Snapshot_For_' + @profileName + ']' ,
                @sql_cmd [VARCHAR](8000) ,
                @progress_msg VARCHAR(5000) ,
                @component VARCHAR(400) = '[Export].[WS_CreateSnapshot]' ,
                @count INT;

        SET @progress_msg = 'Executing [Export].[WS_Measures_Daily_Snapshot_For_] ' + '@reportAlias:''' + @reportAlias + '''; ' + '@profileName:''' + @profileName
                + '''; ' + '@reportYear :''' + @reportYear + '''; ' + '@reportMonth :''' + @reportMonth + '''; ' + '@reportNumber :''' + CAST(@reportNumber AS VARCHAR)
                + '''; ' + '@isTrialRun :''' + CAST(@isTrialRun AS VARCHAR) + '''; ';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;


        SELECT  @sql_cmd = 'If object_id (''' + @tablename + ''') is not null Drop Table ' + @tablename + ';';
        BEGIN

                SET @progress_msg = 'Dropping previous copy of ' + @tablename;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

                EXEC (@sql_cmd);

                SET @progress_msg = 'Previous copy of ' + @tablename + ' dropped';
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg
        END;


        SET @progress_msg = 'Duplicating  [Export].[WS_Measures_StagingAllColumns] to  ' + @tablename;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        SELECT @sql_cmd = 'Select * Into ' + @tablename + ' from [Export].[WS_Measures_StagingAllColumns];';
        EXEC (@sql_cmd);

        SELECT @progress_msg = 'Success duplicating: @sql_cmd:''' + @sql_cmd + ''';';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

    -- generate the release candidate
        IF @isTrialRun != 1
        BEGIN
                SET @tablename = '[Export].[WS_Measures_Release_Candidate_For_' + @profileName + ']';

                SELECT  @sql_cmd = 'If object_id (''' + @tablename + ''') is not null Drop Table ' + @tablename + ';';
                BEGIN
                        SET @progress_msg = 'Dropping previous copy of ' + @tablename;
                        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

                        EXEC (@sql_cmd);

                        SET @progress_msg = 'Previous copy of ' + @tablename + ' dropped';
                        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg
                END;

                SET @progress_msg = 'Duplicating  [Export].[WS_Measures_StagingAllColumns] to  ' + @tablename;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

                SELECT @sql_cmd = 'Select * Into ' + @tablename + ' from [Export].[WS_Measures_StagingAllColumns];';
                EXEC (@sql_cmd);

                SELECT @progress_msg = 'Success duplicating: @sql_cmd:''' + @sql_cmd + ''';';
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;
        END;

    END;

GO
