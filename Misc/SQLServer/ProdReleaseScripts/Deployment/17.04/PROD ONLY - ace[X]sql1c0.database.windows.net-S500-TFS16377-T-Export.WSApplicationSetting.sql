﻿


/***************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_224 SHARD      ***/
/***************************************************/



/****** Object:  Table [Export].[WSApplicationSetting]    Script Date: 4/5/2017 2:48:55 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSApplicationSetting]') AND type in (N'U'))
    DROP TABLE [Export].[WSApplicationSetting]
GO

/****** Object:  Table [Export].[WSApplicationSetting]    Script Date: 4/5/2017 2:48:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSApplicationSetting]') AND type in (N'U'))
BEGIN
    CREATE TABLE [Export].[WSApplicationSetting](
        [AppName] [varchar](50) NOT NULL,
        [ClientId] [int] NOT NULL,
        [Name] [varchar](100) NOT NULL,
        [Value] [varchar](4000) NOT NULL,
        [Description] [varchar](4000) NULL,
    PRIMARY KEY CLUSTERED
        (
            [AppName] ASC,
            [ClientId] ASC,
            [Name] ASC
        )WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
    )
END
GO

INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'extreme_cold_day_temp_threshold', N'15', N'Cold day threshold if threshold not found in wsreportsettings table')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'extreme_hot_day_temp_threshold', N'85', N'Hot day threshold if threshold not found in wsreportsettings table')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'forecast_email_distribution_list', N'mali@aclara.com,CEInsightsWeather@aclara.com,ifaynstein@aclara.com', N'comma separated list of weather email recipients ')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'forecast_email_from', N'ACE_Prod@aclarax.com', N'SMTP SSL boolean')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'forecast_email_from_name', N'AclaraProd', NULL)
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'forecast_email_smtp_auth', N'Basic', N'SMTP Authentication type')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'forecast_email_smtp_password', N'Xaclaraprod2311X', N'SMTP Password')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'forecast_email_smtp_port', N'587', N'SMTP port')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'forecast_email_smtp_server', N'smtp.sendgrid.net', N'SMTP server for email')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'forecast_email_smtp_ssl', N'true', N'SMTP SSL boolean')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'forecast_email_smtp_user', N'aclaraprod', N'SMTP UserId')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'forecast_email_subject', N'Weather Sensitivity Program - Daily Forecast', N'email subject')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'forecast_end_period', N'5', N'today plus 5 days to include in the forecast check')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'forecast_service_key', N'f9fecd4b70087335bbe7be273e29ca5a', N'Key for forecast service')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'forecast_service_url', N'https://api.darksky.net/forecast/', N'Url for forecast service')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'forecast_start_period', N'2', N'today plus value to include in forecast check')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'forecast_total_days', N'7', N'Total days to include in forecast')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'participant_zip_codes', N'06231,06232', N'List of zip codes to run report for')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'report_users', N'mali,phil,lydia,irina,ccarney,ifarias,cholloway,ssteele', N'cold or hot')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'StorageAccountKey', N'OsqLkkDiRo6n44dT2X/opDg59OnesE2U2j9OtidHTYE6qx49yYgD2aA/yiXzWRRa0vRYv6NiV7X7D4vrE+QK3A==', N'Azure Storage Account key to store report')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'StorageAccountName', N'aclaceweathersenstgprod', N'Azure Storage Account Name to store report')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'StorageContainerName', N'weathersensitivityreports', N'Azure Storage Account container to store report')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'temperature_type', N'hot', N'cold or hot')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'web_job_account_id', N'$AclAceWeatherSensitivityWebJobsWaProd', N'account id')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'web_job_base_url', N'aclaceweathersensitivitywebjobswaprod.scm.azurewebsites.net', N'base url for webjob api')
GO
INSERT [Export].[WSApplicationSetting] ([AppName], [ClientId], [Name], [Value], [Description]) VALUES (N'WeatherSensitivityReports', 224, N'web_job_key', N'4od0LrXmMTZErH8pJGFM9lewDof2c3oM1l7YRtmfe59oQh96Q9C2qRkQRdWt', N'web job key')
GO
