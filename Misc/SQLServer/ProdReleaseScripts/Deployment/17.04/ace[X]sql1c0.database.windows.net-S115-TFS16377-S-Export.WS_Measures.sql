﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [Export].[WS_Measures]    Script Date: 3/29/2017 2:33:38 PM ******/
IF EXISTS ( SELECT * FROM [sys].[objects] WHERE [object_id] = OBJECT_ID(N'[Export].[WS_Measures]') AND [type] IN (N'P', N'PC') )
        DROP PROCEDURE [Export].[WS_Measures];
GO

/****** Object:  StoredProcedure [Export].[WS_Measures]    Script Date: 3/29/2017 2:33:38 PM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF NOT EXISTS ( SELECT * FROM [sys].[objects] WHERE [object_id] = OBJECT_ID(N'[Export].[WS_Measures]') AND [type] IN (N'P', N'PC') )
BEGIN
        EXEC [dbo].[sp_executesql] @statement = N'CREATE PROCEDURE [Export].[WS_Measures] AS';
END;
GO

/****** Object:  StoredProcedure [Export].[WS_Measures]    Script Date: 4/10/2017 1:55:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-------------------------------------------------------------------------------
--  Name:               [Export].[WS_Measures]
--  Author:             Phil Victor
--  Description :     Determine the measures that will be included on the report
--  Notes:
-----------------------------------------------------------------------------
ALTER PROCEDURE [Export].[WS_Measures] @report_profile_name VARCHAR(50) ,
        @report_year VARCHAR(4) ,
        @report_month VARCHAR(2) ,
        @run_id UNIQUEIDENTIFIER = NULL ,
        @is_trial_run BIT = 0
        WITH RECOMPILE  -- force regenerate of query plan
AS
BEGIN

        SET NOCOUNT ON;

        IF @run_id IS NULL
            SET @run_id = NEWID();

        DECLARE @client_id [INT] ,
                @actions [VARCHAR](2000) ,
                @measure_count [INT] ,
                @track_history [BIT] ,
                @ordered_measure_list [VARCHAR](200) ,
                @measure_profile_name [VARCHAR](100) ,
                @fuel_type [VARCHAR](100) ,
                @energyobjectcategory_List [VARCHAR](1024) ,
                @period [VARCHAR](10) ,
                @season_List [VARCHAR](100) ,
                @tab_char [CHAR](1) = CHAR(9) ,     -- tab character
                @progress_msg [VARCHAR](5000) ,
                @component [VARCHAR](400) = '[Export].[WS_Measures]' ,
                @count [INT] ,
                @measure_number_literal [VARCHAR](2) ,
                @measure_number [INT] ,
                @current_measure_number [INT] ,
                @measure_profile_type [VARCHAR](50) ,
                @sqlcmd [NVARCHAR](MAX);

    --\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    --
    -- fetch all the configuration settings and ancillary data we will need

        SET @progress_msg = '*** IN [Export].[WS_Measures] : @report_profile_name = ' + @report_profile_name + ', @report_year = ' + @report_year +
                                '@report_month = ' + @report_month + ', @is_trial = ' + CAST(@is_trial_run AS VARCHAR(1))
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

        SELECT
          @client_id = [ClientId], @fuel_type = [FuelType], @energyobjectcategory_List = [EnergyObjectCategoryList], @period = [period],
                        @season_List = [SeasonList]
                FROM [Export].[WSReportProfile]
                WHERE [ProfileName] = @report_profile_name;


    -- report settings
        DECLARE @WS_Measures_ReportSettings TABLE (
                                                [profile_name] [VARCHAR](100) ,
                                                [measure_count] [VARCHAR](20) ,
                                                [ordered_measure_list] [VARCHAR](1000) ,
                                                [default_actions] [VARCHAR](2000)
        );

    -- mapping of the report profile to measure profile
        DECLARE @WS_Measure_ReportMap TABLE (
                                                [sort_idx] [INT] ,
                                                [rpt_profile_name] [VARCHAR](100) ,
                                                [measure_number] [INT] ,
                                                [measure_profile_name] [VARCHAR](100)
        );

    -- this is the global ranking table
        CREATE TABLE [#WS_Measures_ActionItemRankingGlobal] (
                                                    [MeasureNumber] [INT] NOT NULL ,
                                                    [MeasureProfileName] [VARCHAR](50) NOT NULL ,
                                                    [StagingMeasureNumber] [INT] NOT NULL ,         --  indicates the staging table measure number column that was populated by the MeasureId...0 indicates a measure that has not been mapped
                                                    [ActionItemKey] [INT] NULL ,
                                                    [ActionItemDesc] [VARCHAR](100) NULL ,
                                                    [MeasureId] [VARCHAR](256) NULL ,   --  this is the [ClientAction].[ActionKey] ... aka ActionKey
                                                    [EnergyObjectKey] [VARCHAR](256) NULL ,
                                                    [PremiseKey] [INT] NOT NULL ,
                                                    [RefCost] [DECIMAL](18, 4) NULL ,
                                                    [RefSavings] [DECIMAL](18, 4) NULL ,
                                                    [RankId] [INT] NOT NULL ,
                                                    [LevelNum] [INT] NOT NULL
        );
        CREATE INDEX [WS_Measures_ActionItemRankingGlobal_idx0] ON [#WS_Measures_ActionItemRankingGlobal]([PremiseKey], [MeasureId], [MeasureNumber], [RankId]) INCLUDE ([MeasureProfileName]);

    -- this contains the account to measure profile mapping for a particular measure number
        CREATE TABLE [#WS_Measures_AccountMeasureProfileMap] (
                                                            [PremiseId] [VARCHAR](50) NOT NULL ,
                                                            [AccountId] [VARCHAR](50) NOT NULL ,
                                                            [PremiseKey] [INT] NOT NULL ,
                                                            [MeasureNumber] [INT] NOT NULL ,
                                                            [MeasureProfileName] [VARCHAR](50) NOT NULL
        );
        CREATE UNIQUE INDEX [#WS_Measures_AccountMeasureProfileMap_idx0] ON [#WS_Measures_AccountMeasureProfileMap]([PremiseId], [AccountId], [MeasureNumber]);
        CREATE UNIQUE INDEX [#WS_Measures_AccountMeasureProfileMap_idx1] ON [#WS_Measures_AccountMeasureProfileMap]([PremiseKey], [MeasureNumber]);

    --  ordered set of measure that is used to populate the staging table
        DECLARE @ordered_measure TABLE (
                                        [sort_idx] [INT] IDENTITY(1, 1) ,
                                        [measure_number] [int]
        );

    --  grab the report settings we are interesting in
        INSERT @WS_Measures_ReportSettings ([profile_name], [measure_count], [ordered_measure_list])
                        EXEC [Export].[WS_Measures_PivotReportSetting] @clientId = @client_id, @reportProfileName = @report_profile_name,
                                @settingList = 'measure_count, measure_order_list';

        SELECT
         @measure_count = CAST([measure_count] AS [INT]), @ordered_measure_list = [ordered_measure_list]
                FROM @WS_Measures_ReportSettings;

    -- get the measure profiles for the report
        INSERT @WS_Measure_ReportMap ([sort_idx], [rpt_profile_name], [measure_number], [measure_profile_name])
                        EXEC [Export].[WS_Measures_FetchMeasureProfiles] @clientId = @client_id, @reportProfileName = @report_profile_name;

        SELECT @count = COUNT(*)  FROM @WS_Measure_ReportMap
        SET @progress_msg = '@WS_Measure_ReportMap contains ' + CAST(@count AS VARCHAR) + ' rows.' ;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

    -- setup the ordered measure list
        IF ISNULL(LTRIM(RTRIM(@ordered_measure_list)), '') != ''
        BEGIN
        -- the split function puts 1 entry into the table if the list is empty...we don't want this to happen
                INSERT @ordered_measure ([measure_number])
                        SELECT CAST ([param] AS [INT])
                                FROM [dbo].[ufn_split_values](@ordered_measure_list, ',');
        END;

    --
    --\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    --
        SET @progress_msg = '@is_trial_run:' + CAST(@is_trial_run AS VARCHAR);
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

        SET @progress_msg = 'Fetched Report / Measure Settings...' + '@measure_count:''' + ISNULL(CAST(@measure_count AS [VARCHAR]), '') + '''; '
                + '@ordered_measure_list:''' + ISNULL(@ordered_measure_list, '') + '''; ';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;
    --
    --\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    --
    --  delete history for this report profile

        IF @is_trial_run = 0
        BEGIN
        -- this is NOT a trial run
        --
        -- delete the history for this run
        --
        -- NOTE:    For UIL we have setup a unique profile for each report instance (once a profile is released it will not be re-released).
        --          Therefore, when we remove history we will remove all history for the profile regardless of year/month.
        --
                DELETE FROM [Export].[WS_Measures_History]
                        WHERE [profile_name] = @report_profile_name;

                SET @count = @@ROWCOUNT;
                SELECT @progress_msg = 'Removed ' + CAST(@count AS VARCHAR) + ' Rows from [Export].[WS_Measures_History] where ' + '@profile_name = '''
                                + @report_profile_name + '''';
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;
        END;
    --
    --\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    --
    --  map measure profiles to accounts
        EXEC [Export].[WS_Measures_MapMeasureProfileToAccount] @reportProfileName = @report_profile_name, @runId = @run_id;

        SELECT @count = COUNT(*)
                FROM [#WS_Measures_AccountMeasureProfileMap];
        SET @progress_msg = '[#WS_Measures_AccountMeasureProfileMap] contains ' + CAST(@count AS VARCHAR) + ' rows';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;
    --
    --\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    --
    -- iterate over the measures and process one at a time
        DECLARE [measure_profile_cursor] CURSOR LOCAL STATIC FORWARD_ONLY
        FOR
        SELECT [mm].[measure_number], [measure_profile_name]
                FROM @WS_Measure_ReportMap [mm]
                JOIN    @ordered_measure [om] ON [mm].[measure_number] = [om].[measure_number]
                ORDER BY [om].[sort_idx];

        OPEN [measure_profile_cursor];
        FETCH NEXT FROM [measure_profile_cursor] INTO @measure_number, @measure_profile_name;
        WHILE @@FETCH_STATUS = 0
        BEGIN

                SET @progress_msg = 'Processing Measure Profile ...' + '@measure_number:''' + ISNULL(CAST(@measure_number AS [VARCHAR]), '') + '''; '
                        + '@measure_profile_name:''' + ISNULL(@measure_profile_name, '') + '''; ';
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

        -- this table contains the rankings accumulated by the call to usp_HE_Report_Get_Measures_ProcessMeasure
        -- we must create this from scratch for each iteration because the sproc will dynamically add columns to the table
                CREATE TABLE [#WS_Measures_ActionItemRankingWork] ([ActionItemKey] [INT] NULL ,
                                                                   [ActionItemDesc] [VARCHAR](100) NULL ,
                                                                   [MeasureId] [VARCHAR](256) NULL ,   --this is the [cm].[ClientAction].actionkey i think
                                                                   [EnergyObjectKey] [VARCHAR](256) NULL ,
                                                                   [PremiseKey] [INT] NOT NULL ,
                                                                   [RefCost] [DECIMAL](18, 4) NULL ,
                                                                   [RefSavings] [DECIMAL](18, 4) NULL ,
                                                                   [RankId] [INT] NULL ,
                                                                   [LevelNum] [INT] NULL);

        -- retrieve the hard modelled measure settings
                SELECT   @measure_profile_type = [Type]
                        FROM [Export].[WSMeasureProfile]
                        WHERE [ProfileName] = @measure_profile_name;

        --  exec the appropiate sproc based on the type of profile
                IF @measure_profile_type = 'rank'
                    BEGIN
                            EXEC [Export].[WS_Measures_PerformRanking] @reportProfileName = @report_profile_name, @measureProfileName = @measure_profile_name,
                                    @measureNumber = @measure_number, @runId = @run_id, @isTrialRun = @is_trial_run;

                    END;
                ELSE
                        IF @measure_profile_type = 'promo'
                            BEGIN
                                    EXEC [Export].[WS_Measures_ProcessPromo] @reportProfileName = @report_profile_name,
                                            @measureProfileName = @measure_profile_name, @measureNumber = @measure_number, @runId = @run_id,
                                            @isTrialRun = @is_trial_run;


                            END;
                        ELSE
                            BEGIN
                -- make sure that the measure_type is supported
                                    RAISERROR(N'MeasureProfile Type must be ''calc'' or ''promo''' , -1 , 1);
                            END;

                SELECT @count = COUNT(*)  FROM [#WS_Measures_ActionItemRankingWork]
                SET @progress_msg = '[#WS_Measures_ActionItemRankingWork] contains ' + CAST(@count AS VARCHAR) + ' rows FOR  ...' + '@measure_number:''' + ISNULL(CAST(@measure_number AS [VARCHAR]), '') + '''; '
                                                                    + '@measure_profile_name:''' + ISNULL(@measure_profile_name, '') + '''; ';
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

        --  update the global ranking table
                INSERT INTO [#WS_Measures_ActionItemRankingGlobal] ([MeasureNumber], [MeasureProfileName], [StagingMeasureNumber], [ActionItemKey],
                                                                    [ActionItemDesc], [MeasureId], [EnergyObjectKey], [PremiseKey], [RefCost], [RefSavings],
                                                                    [RankId], [LevelNum])
                        SELECT @measure_number, @measure_profile_name, 0, [ActionItemKey], [ActionItemDesc], [MeasureId], [EnergyObjectKey], [PremiseKey],
                                        [RefCost], [RefSavings], [RankId], [LevelNum]
                                FROM [#WS_Measures_ActionItemRankingWork];

        -- drop the work table
                DROP TABLE [#WS_Measures_ActionItemRankingWork];

                FETCH NEXT FROM [measure_profile_cursor] INTO @measure_number, @measure_profile_name;
        END;

        CLOSE [measure_profile_cursor];
        DEALLOCATE [measure_profile_cursor];
    -- ok...at this point our global ranking table should contain rankings for all measure columns
    --
    --\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    --

        SELECT @count = COUNT(*)  FROM [#WS_Measures_ActionItemRankingGlobal]
        SET @progress_msg = '[#WS_Measures_ActionItemRankingGlobal] contains  ' + CAST(@count AS VARCHAR) + ' rows.';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

    -- update the staging table based on the rankings
        DECLARE @measure_fields_valid_expression [VARCHAR](4000);

    -- we will use the @ordered_measure table to determine the primary order in which we evaluate each measure field on the staging table
        DECLARE [ordered_measure_cursor] CURSOR LOCAL STATIC FORWARD_ONLY
        FOR
        SELECT [rpt_measure_map].[measure_number], [rpt_measure_map].[measure_profile_name]
                FROM @WS_Measure_ReportMap AS [rpt_measure_map]
                LEFT OUTER JOIN @ordered_measure AS [ordered_measure] ON [rpt_measure_map].[measure_number] = [ordered_measure].[measure_number]
                ORDER BY ISNULL([ordered_measure].[sort_idx], 9999) ASC, -- if measure is not in the ordered list it will be processed at the end
                        [rpt_measure_map].[sort_idx];

        OPEN [ordered_measure_cursor];
        FETCH NEXT FROM [ordered_measure_cursor] INTO @measure_number, @measure_profile_name;

        SET @current_measure_number = -1;
        WHILE @@FETCH_STATUS = 0
        BEGIN
                IF @measure_number != @current_measure_number
                BEGIN
            -- we are processing the measures in @measure_number order (ie all measure# will be contiguous because they are the high order portion of the sort key)

                        SET @measure_number_literal = CAST(@measure_number AS VARCHAR);

                         -- clear the columns
                        SELECT @sqlcmd = '
                            UPDATE  [Export].[WS_Measures_StagingAllColumns]
                            SET
                                [MID' + @measure_number_literal + '] = '''',
                                [MTitle' + @measure_number_literal + '] = '''',
                                [MText' + @measure_number_literal + '] = '''',
                                [MCost' + @measure_number_literal + '] = '''',
                                [MSavings' + @measure_number_literal + '] = ''''
                        ;';

                        SET @progress_msg = 'Execute: @sqlcmd:' + @sqlcmd;
                        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

                        EXEC (@sqlcmd);

                        SET @progress_msg = 'Success running: @sqlcmd:' + @sqlcmd;
                        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

                        SET @measure_fields_valid_expression = ISNULL(@measure_fields_valid_expression + ' and ', '') + '[MID' + @measure_number_literal
                                + '] != ''''';

                        SET @current_measure_number = @measure_number;
                END;

                --  update the staging table with values for the measure savings.
                EXEC [Export].[WS_Measures_UpdateStaging]
                        @reportProfileName      = @report_profile_name,
                        @measureProfileName     = @measure_profile_name,
                        @measureNumber          = @measure_number,
                        @runId                  = @run_id,
                        @isTrialRun             = @is_trial_run;

                FETCH NEXT FROM [ordered_measure_cursor] INTO @measure_number, @measure_profile_name;
        END;
        CLOSE [ordered_measure_cursor];

    --  add a column to the staging table to indicate whether or not all the measure columns have a value
        IF NOT EXISTS ( SELECT *
                                FROM [sys].[columns]
                                WHERE [name] = 'MeasureFieldsValid'
                                        AND [object_id] = OBJECT_ID('[Export].[WS_Measures_StagingAllColumns]') )
        BEGIN
                ALTER TABLE [Export].[WS_Measures_StagingAllColumns] ADD [MeasureFieldsValid] [BIT] NOT NULL DEFAULT 0;
        END;
        ELSE
        BEGIN
        -- column already exists...lets make sure the bit is reset
                SET @sqlcmd = 'update [Export].[WS_Measures_StagingAllColumns] set [MeasureFieldsValid] = 0;';
                EXEC(@sqlcmd);
        END;

        IF ISNULL(@measure_fields_valid_expression, '') != ''
        BEGIN

                SET @sqlcmd = 'update [Export].[WS_Measures_StagingAllColumns] set [MeasureFieldsValid] = 1 where ' + @measure_fields_valid_expression + ';';

                SET @progress_msg = 'Execute   @sqlcmd:''' + @sqlcmd + '''';
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

                EXEC (@sqlcmd);
                SELECT @count = @@ROWCOUNT;
                SET @progress_msg = 'Success running @sqlcmd:''' + @sqlcmd + '''; Updated ' + CAST(@count AS [VARCHAR]) + ' rows;';
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;
        END;

    -- save our rankings for debugging purposes
        DECLARE @tablename [VARCHAR](256) = '[Export].[WS_Measures_ReportRanking_' + @report_profile_name + ']';

        IF OBJECT_ID ('' + @tablename + '') IS NOT NULL
        BEGIN
                        SET @sqlcmd = ' Drop Table ' + @tablename + ';';

                        SET @progress_msg = 'Execute   @sqlcmd:' + @sqlcmd;
                        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

                        EXEC (@sqlcmd);

                        SET @progress_msg = 'Success running @sqlcmd:' + @sqlcmd;
                        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;
        END

         SET @sqlcmd = 'Select * into ' + @tablename + 'from [#WS_Measures_ActionItemRankingGlobal];';

        SET @progress_msg = 'Execute   @sqlcmd:' + @sqlcmd;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

        EXEC (@sqlcmd);

        SET @progress_msg = 'Success running @sqlcmd:' + @sqlcmd;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

    --
    --\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    --
    --  update history

        IF @is_trial_run = 0
        BEGIN
        -- this is not a trial run...save history so that the measures do not get picked up again when we run the next report

        SELECT  @is_trial_run AS IsTrialRun
              --   SET @progress_msg = 'Start to save measure history';
        --        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

        ---- NOTE: at the start of this sproc we deleted the entries for this run from the history table...therefore we now need to simply add the measures that will
        ----       be on the export for this run
                DECLARE @history_date [DATETIME] = GETDATE();

                OPEN [ordered_measure_cursor];      -- reopen our cursor
                FETCH NEXT FROM [ordered_measure_cursor] INTO @measure_number, @measure_profile_name;

                SET @current_measure_number = -1;

                WHILE @@FETCH_STATUS = 0
                BEGIN
                        IF @measure_number != @current_measure_number
                        BEGIN

        --        -- we are processing the measures in @measure_number order (ie all measure# will be contiguous because they are the high order portion of the sort key)

                                SET @measure_number_literal = CAST(@measure_number AS VARCHAR);

                                SELECT
                                    @track_history = CAST(ISNULL([Value], '0') AS [BIT])
                                        FROM [Export].[WSMeasureSetting]
                                        WHERE [ClientId] = @client_id
                                                AND [ProfileName] = @measure_profile_name
                                                AND [Name] = 'track_history';

                                 SET @progress_msg = 'Process measure history for @measure_profile_name = ' + @measure_profile_name + ' , @track_history = ' + CAST(@track_history AS VARCHAR);
                                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

                                IF @track_history = 1
                                BEGIN
                                        SET @sqlcmd = '
                                                    insert [Export].[WS_Measures_History]
                                                        (   [profile_name],
                                                            [PremiseID],
                                                            [MeasureID],
                                                            [history_date],
                                                            [report_year],
                                                            [report_month],
                                                            [ClientId],
                                                            [MeasureNumber]
                                                        )
                                                        select
                                                            ''' + @report_profile_name + ''',
                                                            premiseID,
                                                            MID' + @measure_number_literal + ',
                                                            @history_date,
                                                            ''' + @report_year + ''',
                                                            ''' + @report_Month + ''',
                                                            ' + CAST(@client_id AS [VARCHAR]) + ',
                                                            ' + @measure_number_literal + '
                                                            from
                                                                [Export].[WS_Measures_StagingAllColumns]
                                                            where MID' + @measure_number_literal + ' != '''' and [MeasureFieldsValid] = 1;';

                                        SET @progress_msg = 'Execute   @sqlcmd:' + @sqlcmd;
                                        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

                                        EXEC [sp_executesql] @sqlcmd, N'@history_date [datetime]', @history_date = @history_date;
                                        SET @count = @@ROWCOUNT;

                                        SET @progress_msg = 'Success running @sqlcmd:' + @sqlcmd;
                                        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

                                        SET @progress_msg = 'Success performing insert into history count:' + CAST(@count AS VARCHAR) + '; history_date:  '''
                                                        + CAST(@history_date AS VARCHAR) + ''';';
                                        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;
                                END;

                                SET @current_measure_number = @measure_number;
                        END;

                        FETCH NEXT FROM [ordered_measure_cursor] INTO @measure_number, @measure_profile_name;
                END;
                CLOSE [ordered_measure_cursor];
                DEALLOCATE [ordered_measure_cursor];

        END;
    --
    --\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    --

        SET @progress_msg = '*** COMPLETED  [Export].[WS_Measures] : @report_profile_name = ' + @report_profile_name + ', @report_year = ' + @report_year +
                                '@report_month = ' + @report_month + ', @is_trial = ' + CAST(@is_trial_run AS VARCHAR)
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;
END;





