﻿
/***************************************************/
/***** RUN ONLY ON THE INSIGHTS SHARD      ***/
/***************************************************/


/****** Object:  StoredProcedure [ETL].[S_API_ProfileCustomerAccountsForBillDisaggs]    Script Date: 4/21/2017 8:24:27 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_API_ProfileCustomerAccountsForBillDisaggs]') AND type in (N'P', N'PC'))
        DROP PROCEDURE [ETL].[S_API_ProfileCustomerAccountsForBillDisaggs]
GO

/****** Object:  StoredProcedure [ETL].[S_API_ProfileCustomerAccountsForBillDisaggs]    Script Date: 4/21/2017 8:24:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_API_ProfileCustomerAccountsForBillDisaggs]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[S_API_ProfileCustomerAccountsForBillDisaggs] AS'
END
GO

-- =============================================
-- Author:      Philip Victor
-- Create date: 04/21/2017
-- Description: Retrieve customers for BillDisaggs sync in ETL
-- =============================================
ALTER PROCEDURE [ETL].[S_API_ProfileCustomerAccountsForBillDisaggs]
                        @ClientID INT
        AS

        BEGIN

                SET NOCOUNT ON;

                SELECT  DISTINCT
                        [ClientID],
                        [CustomerID],
                        [AccountID]
                FROM [wh].[ProfileCustomerAccount]
                WHERE   [ClientID] = @ClientID
                ORDER BY [CustomerID]

        END
GO