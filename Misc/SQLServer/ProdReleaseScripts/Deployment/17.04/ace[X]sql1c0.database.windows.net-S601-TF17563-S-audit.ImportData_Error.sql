﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [audit].[InsertProfileImportErrors]    Script Date: 4/19/2017 10:50:23 AM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[audit].[InsertProfileImportErrors]')
                    AND type IN ( N'P', N'PC' ) )
    DROP PROCEDURE [audit].[InsertProfileImportErrors];
GO
/****** Object:  StoredProcedure [audit].[InsertCustomerImportErrors]    Script Date: 4/19/2017 10:50:23 AM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[audit].[InsertCustomerImportErrors]')
                    AND type IN ( N'P', N'PC' ) )
    DROP PROCEDURE [audit].[InsertCustomerImportErrors];
GO
/****** Object:  StoredProcedure [audit].[InsertBillImportErrors]    Script Date: 4/19/2017 10:50:23 AM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[audit].[InsertBillImportErrors]')
                    AND type IN ( N'P', N'PC' ) )
    DROP PROCEDURE [audit].[InsertBillImportErrors];
GO
/****** Object:  StoredProcedure [audit].[InsertActionItemImportErrors]    Script Date: 4/19/2017 10:50:23 AM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[audit].[InsertActionItemImportErrors]')
                    AND type IN ( N'P', N'PC' ) )
    DROP PROCEDURE [audit].[InsertActionItemImportErrors];
GO

/****** Object:  StoredProcedure [audit].[InsertActionItemImportErrors]    Script Date: 4/19/2017 10:50:23 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[audit].[ImportData_InsertActionItemErrors]')
                        AND type IN ( N'P', N'PC' ) )
    BEGIN
        EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [audit].[ImportData_InsertActionItemErrors] AS';
    END;
GO



ALTER PROCEDURE [audit].[ImportData_InsertActionItemErrors]
    (
      @TrackingId AS VARCHAR(50) ,
      @ClientId AS INT ,
      @ErrorDateTime AS DATETIME ,
      @FileName AS VARCHAR(MAX) ,
      @CustomerId AS VARCHAR(50) ,
      @AccountId AS VARCHAR(50) ,
      @PremiseId AS VARCHAR(50) ,
      @ActionKey AS VARCHAR(256) ,
      @MessageText AS VARCHAR(MAX)
    )
AS
    BEGIN
        INSERT  INTO [audit].[ImportData_Errors_ActionItem]
                ( [TrackingId] ,
                  [ErrorDateTime] ,
                  [FileName] ,
                  [ClientId] ,
                  [CustomerId] ,
                  [AccountId] ,
                  [PremiseId] ,
                  [ActionKey] ,
                  [MessageText]
                )
        VALUES  ( @TrackingId ,
                  @ErrorDateTime ,
                  @FileName ,
                  @ClientId ,
                  @CustomerId ,
                  @AccountId ,
                  @PremiseId ,
                  @ActionKey ,
                  @MessageText
                );
    END;




GO
/****** Object:  StoredProcedure [audit].[ImportData_InsertBillErrors]    Script Date: 4/19/2017 10:50:23 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[audit].[ImportData_InsertBillErrors]')
                        AND type IN ( N'P', N'PC' ) )
    BEGIN
        EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [audit].[ImportData_InsertBillErrors] AS';
    END;
GO




ALTER PROCEDURE [audit].[ImportData_InsertBillErrors]
    (
      @TrackingId AS VARCHAR(50) ,
      @ClientId AS INT ,
      @ErrorDateTime AS DATETIME ,
      @FileName AS VARCHAR(MAX) ,
      @AccountId AS VARCHAR(50) ,
      @PremiseId AS VARCHAR(50) ,
      @ServicePointId AS VARCHAR(64) ,
      @StartDate AS VARCHAR(20) ,
      @EndDate AS VARCHAR(20) ,
      @MessageText AS VARCHAR(MAX)
    )
AS
    BEGIN
        INSERT  [audit].[ImportData_Errors_Bill]
                ( [TrackingId] ,
                  [FileName] ,
                  [ErrorDateTime] ,
                  [ClientId] ,
                  [AccountId] ,
                  [PremiseId] ,
                  [ServicePointId] ,
                  [StartDate] ,
                  [EndDate] ,
                  [MessageText]
                )
        VALUES  ( @TrackingId ,
                  @FileName ,
                  @ErrorDateTime ,
                  @ClientId ,
                  @AccountId ,
                  @PremiseId ,
                  @ServicePointId ,
                  @StartDate ,
                  @EndDate ,
                  @MessageText
                );
    END;





GO
/****** Object:  StoredProcedure [audit].[ImportData_InsertCustomerErrors]    Script Date: 4/19/2017 10:50:23 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[audit].[ImportData_InsertCustomerErrors]')
                        AND type IN ( N'P', N'PC' ) )
    BEGIN
        EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [audit].[ImportData_InsertCustomerErrors] AS';
    END;
GO


ALTER PROCEDURE [audit].[ImportData_InsertCustomerErrors]
    (
      @TrackingId AS VARCHAR(50) ,
      @FileName AS VARCHAR(MAX) ,
      @ClientId AS INT ,
      @ErrorDateTime AS DATETIME ,
      @CustomerId AS VARCHAR(50) ,
      @AccountId AS VARCHAR(50) ,
      @PremiseId AS VARCHAR(50) ,
      @MessageText AS VARCHAR(MAX)
    )
AS
    BEGIN
        INSERT  INTO [audit].[ImportData_Errors_Customer]
                ( [TrackingId] ,
                  [FileName] ,
                  [ErrorDateTime] ,
                  [ClientId] ,
                  [CustomerId] ,
                  [AccountId] ,
                  [PremiseId] ,
                  [MessageText]
                )
        VALUES  ( @TrackingId ,
                  @FileName ,
                  @ErrorDateTime ,
                  @ClientId ,
                  @CustomerId ,
                  @AccountId ,
                  @PremiseId ,
                  @MessageText
                );
    END;



GO
/****** Object:  StoredProcedure [audit].[ImportData_InsertProfileErrors]    Script Date: 4/19/2017 10:50:23 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[audit].[ImportData_InsertProfileErrors]')
                        AND type IN ( N'P', N'PC' ) )
    BEGIN
        EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [audit].[ImportData_InsertProfileErrors] AS';
    END;
GO


ALTER PROCEDURE [audit].[ImportData_InsertProfileErrors]
    (
      @TrackingId AS VARCHAR(50) ,
      @FileName AS VARCHAR(MAX) ,
      @ClientId AS INT ,
      @ErrorDateTime AS DATETIME ,
      @AccountId AS VARCHAR(50) ,
      @PremiseId AS VARCHAR(50) ,
      @AttributeKey AS VARCHAR(256) ,
      @AttributeValue AS VARCHAR(256) ,
      @MessageText AS VARCHAR(MAX)
    )
AS
    BEGIN
        INSERT  [audit].[ImportData_Errors_Profile]
                ( [TrackingId] ,
                  [FileName] ,
                  [ErrorDateTime] ,
                  [ClientId] ,
                  [AccountId] ,
                  [PremiseId] ,
                  [AttributeKey] ,
                  [AttributeValue] ,
                  [MessageText]
                )
        VALUES  ( @TrackingId ,
                  @FileName ,
                  @ErrorDateTime ,
                  @ClientId ,
                  @AccountId ,
                  @PremiseId ,
                  @AttributeKey ,
                  @AttributeValue ,
                  @MessageText
                );
    END;



GO
