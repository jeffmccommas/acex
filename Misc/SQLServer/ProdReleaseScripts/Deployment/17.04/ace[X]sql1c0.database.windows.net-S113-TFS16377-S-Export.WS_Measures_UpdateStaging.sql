﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  StoredProcedure [Export].[WS_Measures_UpdateStaging]    Script Date: 4/4/2017 12:35:32 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_UpdateStaging]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [Export].[WS_Measures_UpdateStaging]
GO

/****** Object:  StoredProcedure [Export].[WS_Measures_UpdateStaging]    Script Date: 4/4/2017 12:35:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_UpdateStaging]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WS_Measures_UpdateStaging] AS'
END
GO

-------------------------------------------------------------------------------
--  Name:               [Export].[WS_Measures_UpdateStaging]
--  Author:             Philip Victor
--  Description :     Rank the measures for the measure# / measure profile
-----------------------------------------------------------------------------
ALTER PROCEDURE [Export].[WS_Measures_UpdateStaging] @reportProfileName [VARCHAR](50) ,
        @measureProfileName [VARCHAR](50) ,
        @measureNumber [INT] ,
        @runId [UNIQUEIDENTIFIER] ,
        @isTrialRun [BIT] = 0
        WITH RECOMPILE          -- force generation of query plan
AS
BEGIN

        SET NOCOUNT ON;

        DECLARE @client_id [INT] ,
                @measure_number_literal [VARCHAR](2) ,
                @measure_profile_type [VARCHAR](50) ,
                @sqlcmd [VARCHAR](8000) ,
                @default_actions [VARCHAR](2000) ,
                @actions [VARCHAR](2000) ,
                @is_trial_run BIT ,
                @tab_char [CHAR](1) = CHAR(9) ,     -- tab character
                @progress_msg [VARCHAR](5000) ,
                @component [VARCHAR](400) = '[Export].[WS_Measures_UpdateStaging]' ,
                @count [INT];

        SELECT
        @client_id = [ClientId]
                FROM [Export].[WSReportProfile]
                WHERE [ProfileName] = @reportProfileName;

    -- report settings
        DECLARE @WS_Measure_Rpt_Setting TABLE (
                                                [profile_name] [VARCHAR](100) ,
                                                [default_actions] [VARCHAR](2000)
        );

    -- measure settings
        DECLARE @WS_Measure_Setting TABLE (
                                            [profile_name] [VARCHAR](100) ,
                                            [actions] [VARCHAR](2000)
        );

    --  grab the report settings we are interesting in
        INSERT @WS_Measure_Rpt_Setting ([profile_name], [default_actions])
                        EXEC [Export].[WS_Measures_PivotReportSetting] @clientId = @client_id, @reportProfileName = @reportProfileName,
                                @settingList = 'measure_default_actions';

        SELECT
        @default_actions = [default_actions]
                FROM @WS_Measure_Rpt_Setting;

    --  grab the soft modelled measure settings we are interesting in
        INSERT @WS_Measure_Setting ([profile_name], [actions])
                        EXEC [Export].[WS_Measures_PivotMeasureSetting] @clientId = @client_id, @measureProfileName = @measureProfileName, @settingList = 'actions';

    -- setup the softmodelled measure settings
        SELECT @actions = [actions]
                FROM @WS_Measure_Setting;

    -- retrieve the hard modelled measure settings
        SELECT
        @measure_profile_type = [Type]
                FROM [Export].[WSMeasureProfile]
                WHERE [ProfileName] = @measureProfileName;

        SET @progress_msg = '@isTrialRun:' + CAST(@isTrialRun AS VARCHAR);
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        SET @progress_msg = 'Update All Staging Cols:  Start     ...   ' + '@reportProfileName:''' + ISNULL(@reportProfileName, '') + '''; '
                + '@measureNumber:''' + ISNULL(CAST(@measureNumber AS [VARCHAR]), '') + '''; ' + '@measureProfileName:''' + ISNULL(@measureProfileName, '')
                + '''; ' + '@measure_profile_type:''' + ISNULL(@measure_profile_type, '') + '''; ' + '@isTrialRun:''' + ISNULL(CAST(@isTrialRun AS [VARCHAR]),
                                                                                                                               '') + '''; '
                + '@default_actions:''' + ISNULL(@default_actions, '') + '''; ' + '@actions:''' + ISNULL(@actions, '') + '''; ';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

    -- Set the action filter
        SET @actions = REPLACE(@actions, @tab_char, '');

        IF ISNULL(LTRIM(RTRIM(@actions)), '') = ''
        BEGIN
                SET @actions = REPLACE(@default_actions, @tab_char, '');
        END;

        SET @progress_msg = 'The Action Filter: @actions:''' + @actions + ''';';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        DECLARE @action_filter TABLE (
                    [ActionKey] [VARCHAR](100) PRIMARY KEY   NOT NULL
        );

        INSERT @action_filter ([ActionKey])
                SELECT DISTINCT [param]
                        FROM [dbo].[ufn_split_values](@actions, ',');

    --  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    -- declare the temp tables we will be using
    --  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    -- this table contains the selected measures that will be used to update the staging table.
        CREATE TABLE [#WS_Measure_Selected_Measures] (
                                                      [PremiseKey] [INT] NOT NULL ,
                                                      [PremiseId] [VARCHAR](50) NOT NULL ,
                                                      [AccountId] [VARCHAR](50) NOT NULL ,
                                                      [RankId] [INT] NOT NULL ,
                                                      [MeasureId] [VARCHAR](50) NOT NULL ,        --  this is the [ClientAction].[ActionKey] ... aka ActionKey
                                                      [Title] [VARCHAR](50) NOT NULL ,
                                                      [Text] [VARCHAR](50) NOT NULL ,
                                                      [Cost] [VARCHAR](50) NOT NULL ,
                                                      [Savings] [VARCHAR](50) NOT NULL
        );

    -- update the staging table
        SET @measure_number_literal = CAST(@measureNumber AS VARCHAR);

        SELECT @sqlcmd = '
            insert into [#WS_Measure_Selected_Measures]
                (
                    [PremiseKey],
                    [PremiseId],
                    [AccountId],
                    [RankId],
                    [MeasureId],
                    [Title],
                    [Text],
                    [Cost],
                    [Savings]
                )
                select
                    [premise].[PremiseKey], [premise].[PremiseId], [premise].[AccountId],   [ranking].[RankId],
                    coalesce ([ranking].[MeasureId], '''') as [MeasureId],
                    coalesce (left ([ranking].[ActionItemDesc], 50), '''') as [Title],
                    '''' as [Text],
                    coalesce( cast( cast( [ranking].[RefCost]    as [numeric] ) as [varchar]), '''') as [Cost],
                    coalesce( cast( cast( [ranking].[RefSavings] as [numeric] ) as [varchar]), '''') as [Savings]
                from
                    [dbo].[DimPremise] as [premise]
                    inner join
                        (
                            select [PremiseKey], min([RankId]) as [RankId]
                                from [#WS_Measures_ActionItemRankingGlobal]
                                where [MeasureNumber] = ' + @measure_number_literal + ' and [MeasureProfileName] = ''' + @measureProfileName
                        + ''' and [StagingMeasureNumber] = 0
                                group by [PremiseKey]
                        ) as [best_fit] on [best_fit].[PremiseKey] = [premise].[PremiseKey]
                    inner join [#WS_Measures_ActionItemRankingGlobal] as [ranking] on
                        [ranking].[PremiseKey] = [best_fit].[PremiseKey] and [ranking].[RankId] = [best_fit].[RankId] and
                        [ranking].[MeasureNumber] = ' + @measure_number_literal + ' and [ranking].[MeasureProfileName] = ''' + @measureProfileName + ''';
        ';

        SET @progress_msg = 'Determine selected measures: @sqlcmd: ' + @sqlcmd;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        EXEC (@sqlcmd);

        SELECT @count = COUNT(*)
                FROM [#WS_Measure_Selected_Measures];

        SET @progress_msg = 'Success running @sqlcmd: ' + @sqlcmd +  '   Selected ' + CAST(@count AS [VARCHAR]) + ' measures';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        SELECT @sqlcmd = '
        update  [staging]
            set
                [staging].[MID' + @measure_number_literal + '] = [selected_measure].[MeasureId],
                [staging].[MTitle' + @measure_number_literal + '] = [selected_measure].[Title],
                [staging].[MText' + @measure_number_literal + '] = [selected_measure].[Text],
                [staging].[MCost' + @measure_number_literal + '] = [selected_measure].[Cost],
                [staging].[MSavings' + @measure_number_literal + '] = [selected_measure].[Savings]
            from
                [Export].[WS_Measures_StagingAllColumns] as [staging]
                inner join  [#WS_Measure_Selected_Measures] as [selected_measure] on
                    [selected_measure].[PremiseID] = [staging].[PremiseID] and
                    [selected_measure].[AccountId] = [staging].[Account_Number];';


        SET @progress_msg = 'Update StagingAllCols with selected measures:   @sqlcmd:' + @sqlcmd;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        EXEC (@sqlcmd);
        SET @count = @@ROWCOUNT;

        SET @progress_msg = 'Success running @sqlcmd:' + @sqlcmd;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        SET @progress_msg = 'Updated ' + CAST(@count AS [VARCHAR]) + ' staging rows';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;


    -- okay...lets flag the ranking as unavailable if we have included it on the report
        IF @measure_profile_type = 'rank'
        BEGIN
        --  for all accounts that have the ranking we will set the IStagingMeasureNumber to the measure # we are processing for the measure that was selected
                UPDATE [#WS_Measures_ActionItemRankingGlobal]
                        SET     [StagingMeasureNumber] = @measureNumber
                        FROM [#WS_Measures_ActionItemRankingGlobal] AS [ranking_global]
                        INNER JOIN [#WS_Measure_Selected_Measures] AS [selected_measure] ON [ranking_global].[PremiseKey] = [selected_measure].[PremiseKey]
                                                                                            AND [ranking_global].[MeasureId] = [selected_measure].[MeasureId];

                SET @count = @@ROWCOUNT;
                SELECT @progress_msg = 'Flagged for rank ' + CAST(@count AS [VARCHAR]) + ' global ranking entries as unvailable';
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;
        END;
        ELSE
                IF @measure_profile_type = 'promo'
                BEGIN
        -- the @action_list for a promo defines a set of actions that are used exclusively by the promo
        -- for all accounts that have been assigned the promo, we will set the StagingMeasureNumber to the measure # we are processing for all measure# for all actionkeys associated with the selected measure
                        UPDATE [#WS_Measures_ActionItemRankingGlobal]
                                SET     [StagingMeasureNumber] = @measureNumber
                                FROM [#WS_Measures_ActionItemRankingGlobal] AS [ranking_global]
                                INNER JOIN [#WS_Measure_Selected_Measures] AS [selected_measure] ON [ranking_global].[PremiseKey] = [selected_measure].[PremiseKey]
                                INNER JOIN @action_filter AS [promo_action] ON [ranking_global].[MeasureId] = [promo_action].[ActionKey];

                        SET @count = @@ROWCOUNT;
                        SELECT @progress_msg = 'Flagged for promo ' + CAST(@count AS [VARCHAR]) + ' global ranking entries as unvailable';
                        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

                END;
                ELSE
                BEGIN
                        SELECT @progress_msg = 'WARNING...flagging global ranking table not implemented.  @measure_profile_type:''' + @measure_profile_type + ''';';
                        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;
                END;

        SET @progress_msg = 'Update of StagingAllCols a Success ...' + '@reportProfileName:''' + ISNULL(@reportProfileName, '') + '''; ' + '@measureNumber:'''
                + ISNULL(CAST(@measureNumber AS [VARCHAR]), '') + '''; ' + '@measureProfileName:''' + ISNULL(@measureProfileName, '') + '''; ';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;


END;




GO
