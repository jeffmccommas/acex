﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [Export].[WS_Measures_AddColsToActionItemRanking]    Script Date: 3/30/2017 12:47:37 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_AddColsToActionItemRanking]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [Export].[WS_Measures_AddColsToActionItemRanking]
GO

/****** Object:  StoredProcedure [Export].[WS_Measures_AddColsToActionItemRanking]    Script Date: 3/30/2017 12:47:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_AddColsToActionItemRanking]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WS_Measures_AddColsToActionItemRanking] AS'
END
GO

-------------------------------------------------------------------------------
--  Name:               [Export].[WS_Measures_AddColsToActionItemRanking]
--  Author:             Philip Victor
--  Description :     Add columns and compute based on criteria selected for recommending Measures.
-----------------------------------------------------------------------------

ALTER PROCEDURE [Export].[WS_Measures_AddColsToActionItemRanking]
        @reportProfileName VARCHAR(50) ,
        @criteriaId VARCHAR(20) ,
        @criteriaDetail VARCHAR(20) ,
        @criteriaSort VARCHAR(20) ,
        @qualifierTag [VARCHAR](50) ,
        @runId UNIQUEIDENTIFIER ,
        @orderbyClause VARCHAR(2000) OUTPUT ,
        @validClause VARCHAR(8000) OUTPUT ,
        @columnList VARCHAR(8000) OUTPUT
AS
BEGIN

        SET NOCOUNT ON;

        DECLARE @sqlcmd VARCHAR(8000);

        DECLARE @enduse VARCHAR(50);

        DECLARE @clientID INT ,
                @fuel_type VARCHAR(100) ,
                @period VARCHAR(10) ,
                @season_List VARCHAR(100) ,
                @component VARCHAR(400) = '[Export].[WS_Measures_AddColsToActionItemRanking]' ,
                @progress_msg VARCHAR(5000);

        SELECT @progress_msg = 'Start [Export].[WS_Measures_AddColsToActionItemRanking]   @criteriaId:' + @criteriaId;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        SELECT
          @clientID = [ClientId], @fuel_type = [FuelType], @period = [period], @season_List = [SeasonList]
                FROM [Export].[WSReportProfile]
                WHERE [ProfileName] = @reportProfileName;

    --
    -- add the column definitions, sort criteria, etc
    --
        DECLARE @order_by_column VARCHAR(1000) = NULL ,
                @validate_column VARCHAR(1000) = NULL;

        IF @criteriaId = 'SAVINGS'
        BEGIN
                SELECT @sqlcmd = 'Alter table [#WS_Measures_ActionItem] add DollarSavings [decimal](18, 4) NULL, DollarSavings_flag int NULL';
                EXEC (@sqlcmd);

                -- [#WS_Measures_ActionItemRankingWork] is declared in [Export].[WS_Measures]
                SELECT @sqlcmd = 'Alter table [#WS_Measures_ActionItemRankingWork] add DollarSavings  [decimal](18, 4) NULL, DollarSavings_flag int NULL';
                EXEC (@sqlcmd);

                SELECT @columnList = @columnList + ', ' + @qualifierTag + 'DollarSavings, ' + @qualifierTag + 'DollarSavings_flag';

                SET @order_by_column = 'DollarSavings';
                SET @validate_column = 'DollarSavings_flag = 1';

        END;
        ELSE
                IF @criteriaId = 'INVESTMENT'
                BEGIN
                        SELECT @sqlcmd = 'Alter table [#WS_Measures_ActionItem] add Cost [decimal](18, 4) NULL, Cost_flag int NULL';
                        EXEC (@sqlcmd);

                        -- [#WS_Measures_ActionItemRankingWork] is declared in [Export].[WS_Measures]
                        SELECT @sqlcmd = 'Alter table [#WS_Measures_ActionItemRankingWork]  add Cost [decimal](18, 4) NULL, Cost_flag int NULL';
                        EXEC (@sqlcmd);

                        SELECT @columnList = @columnList + ', ' + @qualifierTag + 'Cost, ' + @qualifierTag + 'Cost_flag';

                        SET @order_by_column = 'Cost';
                        SET @validate_column = 'Cost_flag = 1';

                END;
                ELSE
                        IF @criteriaId = 'ROI'
                        BEGIN

                                SELECT @sqlcmd = 'Alter table [#WS_Measures_ActionItem]  add ROI [decimal](18, 4) NULL, ROI_flag int NULL';
                                EXEC (@sqlcmd);

                                SELECT @sqlcmd = 'Alter table [#WS_Measures_ActionItemRankingWork] add ROI [decimal](18, 4) NULL, ROI_flag int NULL';
                                EXEC (@sqlcmd);

                                SELECT @columnList = @columnList + ', ' + @qualifierTag + 'ROI, ' + @qualifierTag + 'ROI_flag';

                                SET @order_by_column = 'ROI';
                                SET @validate_column = 'ROI_flag = 1';

                        END;
                        ELSE
                                IF @criteriaId = 'CO2Savings'
                                BEGIN
                                        SELECT @sqlcmd = 'Alter table [#WS_Measures_ActionItem]  add CO2Savings [decimal](18, 4) NULL, CO2Savings_flag int NULL';
                                        EXEC (@sqlcmd);

                                        SELECT @sqlcmd = 'Alter table [#WS_Measures_ActionItemRankingWork] add CO2Savings [decimal](18, 4) NULL, CO2Savings_flag int NULL';
                                        EXEC (@sqlcmd);

                                        SELECT @columnList = @columnList + ', ' + @qualifierTag + 'CO2Savings, ' + @qualifierTag + 'CO2Savings_flag';

                                        SET @order_by_column = 'CO2Savings';
                                        SET @validate_column = 'CO2Savings_flag = 1';
                                END;

    -- setup the orderbyClause and validClause
        IF @order_by_column IS NOT NULL
        BEGIN
                IF LEN(@orderbyClause) > 0
                BEGIN
                        SET @orderbyClause = @orderbyClause + ', ';
                END;
                SET @orderByClause = @orderByClause + ' ' + @order_by_column + ' ' + @criteriaSort + ' ';
        END;

        IF @validate_column IS NOT NULL
        BEGIN
                IF LEN(@validClause) = 0
                BEGIN
                        SELECT  @validClause = 'case when ';
                END;
                ELSE
                BEGIN
                        SELECT  @validClause = @validClause + ' AND ';
                END;
                SET @validClause = @validClause + @validate_column + ' ';
        END;

    --
    -- update the new columns
    -- NOTE: for validation we are converting to numeric to force rounding to the nearest dollar (this is how it is eventually converted when placed into the report table)
    --
    --  NOTE: Each time this sproc is called we will zero out these columns...for PERFORMANCE reasons we should only do this once.
        SET @sqlcmd = '
        update  [rpt_action_item]
                set [RefSavings] = coalesce([fact_action_item].[DollarSavings], 0),
                        [RefCost] = coalesce([fact_action_item].[Cost], 0)
            from    [#WS_Measures_ActionItem] as [rpt_action_item]
                left outer join [#WS_Measures_FactActionItem] as [fact_action_item] on
                    [rpt_action_item].[PremiseKey] = [fact_action_item].[PremiseKey] and
                    [rpt_action_item].[ActionItemKey] = [fact_action_item].[ActionItemKey]
            where
                    ([rpt_action_item].[RefSavings] is null or [rpt_action_item].[RefCost] is null);';  -- only update once...we go through this proc multiple times so i am trying to get some performance

        SELECT @progress_msg = 'Executing:   @sqlcmd:' + @sqlcmd;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        EXEC (@sqlcmd);

        SELECT @progress_msg = 'Success running: @sqlcmd:' + @sqlcmd;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;


    -- build our sql to update the new column
        DECLARE @set_clause VARCHAR(1000) = NULL;

        IF @criteriaId = 'SAVINGS'
        BEGIN
                SET @set_clause = 'Set [DollarSavings] = coalesce ([fact_action_item].[DollarSavings], 0),
                            [DollarSavings_flag] = case when convert(numeric, coalesce([fact_action_item].[DollarSavings], 0)) ' + @criteriaDetail
                            + ' then 1 else 0 end ';

        END;
        ELSE
                IF @criteriaId = 'INVESTMENT'
                BEGIN
                        SET @set_clause = 'Set [Cost]' + ' = coalesce([fact_action_item].[Cost], 0),
                                    [Cost_flag] = case when convert(numeric, coalesce([fact_action_item].[Cost], 0)) ' + @criteriaDetail + ' then 1 else 0 end ';

                END;
                ELSE
                        IF @criteriaId = 'ROI'
                        BEGIN
                                SET @set_clause = 'Set [ROI]' + ' = coalesce ([fact_action_item].ROI], 0),
                                            [ROI_flag] = case when convert(numeric, coalesce([fact_action_item].ROI], 0)) ' + @criteriaDetail + ' then 1 else 0 end ';

                        END;
                        ELSE
                                IF @criteriaId = 'CO2Savings'
                                BEGIN
                                        SET @set_clause = 'Set [CO2Savings]' + ' = coalesce (f[act_action_item].[CO2Savings], 0),
                                                [CO2Savings_flag] = case when convet(numeric, coalesce([fact_action_item].[CO2Savings], 0)) ' + @criteriaDetail + ' then 1 else 0 end ';

                                END;

    -- build and execute the command using the @set_clause
        IF @set_clause IS NOT NULL
        BEGIN
                SET @sqlcmd = 'update   [rpt_action_item]
                                                ' + @set_clause + '
                                                        from
                                                            [#WS_Measures_ActionItem] as [rpt_action_item]
                                                            left outer join [#WS_Measures_FactActionItem] as [fact_action_item] on
                                                                [rpt_action_item].[PremiseKey] = [fact_action_item].[PremiseKey] and
                                                                [rpt_action_item].[ActionItemKey] = [fact_action_item].[ActionItemKey];';

                SET @progress_msg = 'Executing:   @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

                EXEC (@sqlcmd);

                SET @progress_msg = 'Success running: @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;
        END;

        SET @progress_msg = 'Success [Export].[WS_Measures_AddColsToActionItemRanking]  @criteriaId:' + @criteriaId;
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        SET NOCOUNT OFF;
END;

GO
