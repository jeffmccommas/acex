﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  StoredProcedure [Export].[WS_Measures_MapMeasureProfileToAccount]    Script Date: 3/30/2017 10:04:44 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_MapMeasureProfileToAccount]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [Export].[WS_Measures_MapMeasureProfileToAccount]
GO

/****** Object:  StoredProcedure [Export].[WS_Measures_MapMeasureProfileToAccount]    Script Date: 3/30/2017 10:04:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_MapMeasureProfileToAccount]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WS_Measures_MapMeasureProfileToAccount] AS'
END
GO

-------------------------------------------------------------------------------
--  Name:               WS_Measures_MapMeasureProfileToAccount
--  Author:             Philip Victor
--  Description :     This stored procedure is used to map a measure profile to an account
--  Notes:
--      This sproc does not perform the actual work.  It acts as a dispatcher to call a configurable worker stored procedure
--      that will perform the actual work.  The worker  stored proc is called with the measure profile and measure numberand
--      it is the responsibility for the implementation of the worker proc to do the mapping.
-----------------------------------------------------------------------------

ALTER PROCEDURE [Export].[WS_Measures_MapMeasureProfileToAccount] @reportProfileName [VARCHAR](50) ,
        @runId [UNIQUEIDENTIFIER]
AS
BEGIN
        SET NOCOUNT ON;

        DECLARE @client_id [INT] ,
                @i [INT] ,
                @progress_msg [VARCHAR](5000) ,
                @measure_number [INT] ,
                @measure_count [INT] ,
                @measure_profile_name [VARCHAR](100) ,
                @component [VARCHAR](400) = '[Export].[WS_Measures_MapMeasureProfileToAccount]' ,
                @account_mapping_proc [VARCHAR](255) ,
            -- the following is the stored procedure that will be used to perform the mapping if the promotion does not have a mapping proc defined
                @default_account_mapping_proc [VARCHAR](255) = '[Export].[WS_Measures_DefaultMeasureProfileToAccountMapper]' ,
                @sql_cmd [VARCHAR](4000);


    -- this table contains report settings
        DECLARE @rpt_setting TABLE (
                                    [profile_name] [VARCHAR](100) ,
                                    [measure_count] [VARCHAR](20)
        );


    -- this table contains measure settings
        DECLARE @measure_setting TABLE (
                                    [account_mapping_proc] [VARCHAR](100)
        );

    -- mapping of the report profile to measure profile
        DECLARE @rpt_measure_map TABLE (
                                        [sort_idx] [INT] ,
                                        [rpt_profile_name] [VARCHAR](100) ,
                                        [measure_number] [INT] ,
                                        [measure_profile_name] [VARCHAR](100)
        );

        SELECT
        @client_id = [clientId]
                FROM [export].[WSReportProfile]
                WHERE [ProfileName] = @reportProfileName;

    --  grab the report settings we are interesting in
        INSERT @rpt_setting ([profile_name], [measure_count])
                        EXEC [export].[WS_Measures_PivotReportSetting] @clientId = @client_id, @reportProfileName = @reportProfileName, @settingList = 'measure_count';

        SELECT
        @measure_count = ISNULL(CAST([measure_count] AS [INT]), 0)
                FROM @rpt_setting;

        SET @progress_msg = 'Mapping Measure Profiles to Accounts start  ' + '@client_id:''' + CAST(@client_id AS VARCHAR) + '''; ' + '@reportProfileName:'''
                + @reportProfileName + '''; ' + '@measure_count:' + CAST(@measure_count AS VARCHAR) + '; ';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

    --
    -- get the measures configured for the report
    --
        INSERT @rpt_measure_map ([sort_idx], [rpt_profile_name], [measure_number], [measure_profile_name])
                        EXEC [Export].[WS_Measures_FetchMeasureProfiles] @clientId = @client_id, @reportProfileName = @reportProfileName;

    -- iterate over the measure profiles and call the mapper
        DECLARE [measure_profile_cursor] CURSOR LOCAL STATIC FORWARD_ONLY
        FOR
        SELECT [measure_number], [measure_profile_name]
                FROM @rpt_measure_map
                ORDER BY [sort_idx];

        OPEN [measure_profile_cursor];
        FETCH NEXT FROM [measure_profile_cursor] INTO @measure_number, @measure_profile_name;

        WHILE @@FETCH_STATUS = 0
        BEGIN

                SET @progress_msg = 'Mapping @measure_profile_name:''' + @measure_profile_name + '''; measure:' + CAST(@measure_number AS VARCHAR) + '';
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        -- get our mapping proc
                SET @account_mapping_proc = NULL;   -- set to null in case following query does not update the variable due to no rows being found
                SELECT @account_mapping_proc = [Value]
                        FROM [Export].[WSMeasureSetting]
                        WHERE [ClientId] = @client_id
                                AND [ProfileName] = @measure_profile_name
                                AND [Name] = 'account_mapping_proc';

                IF ISNULL(LTRIM(RTRIM(@account_mapping_proc)), '') = ''
                    BEGIN
                            SET @account_mapping_proc = @default_account_mapping_proc;
                            SET @progress_msg = 'Measure Profile does not have a account_mapping_proc setting...defaulting to ''' + @default_account_mapping_proc
                                    + '''';
                            EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;
                    END
                ELSE
                    BEGIN
                            SET @progress_msg = 'Using ''' + @account_mapping_proc + ''' to perform the mapping';
                            EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;
                    END;

        -- call the mapper
                SET @sql_cmd = 'exec ' + @account_mapping_proc + ' ' + '@measureProfileName = ''' + @measure_profile_name + ''', ' + '@measureNumber = '
                        + CAST(@measure_number AS VARCHAR) + ', ' + '@runId = ''' + CAST(@runId AS VARCHAR(36)) + ''';';

                SELECT @progress_msg = 'start   @sql_cmd:' + @sql_cmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

                EXEC(@sql_cmd);

                SELECT @progress_msg = 'success @sql_cmd:' + @sql_cmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;


                FETCH NEXT FROM [measure_profile_cursor] INTO @measure_number, @measure_profile_name;
        END;
        CLOSE [measure_profile_cursor];
        DEALLOCATE [measure_profile_cursor];

END;

GO
