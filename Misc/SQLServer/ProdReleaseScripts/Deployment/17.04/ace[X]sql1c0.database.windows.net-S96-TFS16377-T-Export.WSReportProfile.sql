﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  Table [Export].[WSReportProfile]    Script Date: 4/5/2017 2:27:59 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSReportProfile]') AND type in (N'U'))
    DROP TABLE [Export].[WSReportProfile]
GO

/****** Object:  Table [Export].[WSReportProfile]    Script Date: 4/5/2017 2:27:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSReportProfile]') AND type in (N'U'))
BEGIN
    CREATE TABLE [Export].[WSReportProfile](
        [ProfileName] [varchar](50) NOT NULL,
        [ClientId] [int] NULL,
        [FuelType] [varchar](100) NULL,
        [Period] [varchar](10) NULL,
        [EnergyObjectCategoryList] [varchar](1000) NULL,
        [SeasonList] [varchar](100) NULL,
     CONSTRAINT [PK_WSReportProfile] PRIMARY KEY CLUSTERED
        (
            [ProfileName] ASC
        )WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
    )
END
GO

INSERT [Export].[WSReportProfile] ([ProfileName], [ClientId], [FuelType], [Period], [EnergyObjectCategoryList], [SeasonList]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'electric', N'monthly', N'cooling, hotwater', N'Winter, Spring, Summer, Fall')
GO
