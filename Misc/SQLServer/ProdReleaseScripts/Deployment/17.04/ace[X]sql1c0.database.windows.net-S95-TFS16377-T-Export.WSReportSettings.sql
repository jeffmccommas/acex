﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  Table [Export].[WSReportSetting]    Script Date: 4/5/2017 2:19:47 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSReportSetting]') AND type in (N'U'))
    DROP TABLE [Export].[WSReportSetting]
GO
/****** Object:  Table [Export].[WSReportSetting]    Script Date: 4/5/2017 2:19:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSReportSetting]') AND type in (N'U'))
BEGIN
CREATE TABLE [Export].[WSReportSetting](
    [ProfileName] [varchar](50) NOT NULL,
    [ClientId] [int] NOT NULL,
    [Name] [varchar](100) NOT NULL,
    [Value] [varchar](4000) NOT NULL,
    [Description] [varchar](4000) NULL,
PRIMARY KEY CLUSTERED
(
    [ProfileName] ASC,
    [ClientId] ASC,
    [Name] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END


INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'ami_data_timezone', N'Eastern Standard Time', N'The timezone of the AMI data (i.e. Pacific Standard Time, Eastern Standard Time, Central Standard Time, US Mountain Standard Time. ')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'ami_end_date', N'07/31/2016', N'end date for AMI data')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'ami_min_data_points', N'20', N'The number of days worth of AMI data needed.')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'ami_min_extreme_data_points', N'3', N'The minimum number of exterme day AMI data needed')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'ami_start_date', N'07/01/2016', N'start date for AMI data')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'commodity', N'electric', N'the commodity')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'effective_end', N'05/31/2017', N'End date when report is valid')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'effective_start', N'05/01/2017', N'Start date when report is valid')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'extreme_efficent_home_use_percentile', N'15', N'The usage at this percentile is the “Efficient Home Use')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'extreme_max_average_percentile', N'55', N'Usage that is in between this and the Min Average Percentile is averaged to calculate the “Average Home Use" on extreme days')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'extreme_min_average_percentile', N'15', N'Usage that is in between this and the Max Average Percentile is averaged to calculate the “Average Home Use" on extreme days')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'extreme_number_of_days', N'5', N'The number of extreme (hot or cold) days to average for the Peer comparison')
/*GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'is_trial_run', N'1', N'indicates whether or not we are running a trial report')*/
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'measure_count', N'3', N'the number of measure used by the report')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'measure_default_actions', N'TBD', N'default actions for the measures')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'measure_order_list', N'3,2,1', N'ordered list that determines the order we calculate measures')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'measure_profile_list_1', N'PRINT.WSELEC.201705.1.M1', N'list of measure 1 profiles for the report')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'measure_profile_list_2', N'PRINT.WSELEC.201705.1.M2', N'list of measure 2 profiles for the report')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'measure_profile_list_3', N'UIHES, TBD', N'list of measure 3 (promos) profiles for the report')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'measure_programchannel', N'ws2017elec.channel', N'The Program channel')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'measure_programenrollmentstatus', N'ws2017elec.treatmentgroup.enrollmentstatus', N'Program Enrollment Status content attribute')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'measure_programenrollmentstatusenrolled', N'ws2017elec.treatmentgroup.enrollmentstatus.enrolled', N'Program Enrollment Status Enrolled content attribute')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'measure_programgroupnumber', N'ws2017elec.treatmentgroup.groupnumber', N'Program Treatment Group Number content attribute')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'measure_reportchannel', N'ws2017elec.channel.printandemail', N'The Report channel')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'measure_segments', N'T-3-1, T-3-2, T-3-3, T-3-4, T-3-5', N'segment filter')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'report_alias', N'printwse', N'profile alias that is used to map the profile to the alias used by the ADF activity')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'report_month', N'05', N'The Month of the report')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'report_number', N'1', N'Program report number')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'report_year', N'2017', N'The Year of the report')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'self_max_average_percentile', N'55', N'Usage that is in between this and the Min Average Percentile is averaged to calculate the “Average Home Use')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'self_min_average_percentile', N'15', N'Usage that is in between this and the Max Average Percentile is averaged to calculate the “Average Home Use')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'self_number_of_extreme_days', N'5', N'The number of extreme (hot or cold) days to average for the Self comparison')
GO
INSERT [Export].[WSReportSetting] ([ProfileName], [ClientId], [Name], [Value], [Description]) VALUES (N'PRINT.WSELEC.20175.1', 224, N'temperature_type', N'hot', N'cold or hot')
GO
