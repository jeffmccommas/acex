﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/



/****** Object:  StoredProcedure [Export].[WS_Measures_RecordProgress]    Script Date: 3/29/2017 9:58:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_RecordProgress]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WS_Measures_RecordProgress] AS'
END
GO

-- =============================================
-- Author:   Philip Victor
-- Create date: 3/29/2017
-- Description: Stored procedure for recording measures progress for Weather Sensitivity report.
--
-- =============================================
ALTER PROCEDURE [Export].[WS_Measures_RecordProgress] @runId VARCHAR(1024) ,
        @component VARCHAR(400) = NULL ,
        @text VARCHAR(MAX)
AS
BEGIN
        SET NOCOUNT ON;

        INSERT INTO [audit].[WSLog] (
        [Date],
        [Thread],
        [Level],
        [Logger],
        [Message],
        [Exception])
                VALUES (
                GETUTCDATE(),
                @runId,
                'INFO',
                @component,
                ISNULL(REPLACE(REPLACE(REPLACE(@text, ' ', '<>'), '><', ''), '<>', ' '), 'ISNULL')
                , '');
END;