﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [Export].[WS_Measures_DefaultMeasureProfileToAccountMapper]    Script Date: 3/30/2017 10:30:05 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_DefaultMeasureProfileToAccountMapper]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [Export].[WS_Measures_DefaultMeasureProfileToAccountMapper]
GO

/****** Object:  StoredProcedure [Export].[WS_Measures_DefaultMeasureProfileToAccountMapper]    Script Date: 3/30/2017 10:30:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_DefaultMeasureProfileToAccountMapper]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WS_Measures_DefaultMeasureProfileToAccountMapper] AS'
END
GO


-------------------------------------------------------------------------------
--  Name:               [Export].[WS_Measures_DefaultMeasureProfileToAccountMapper]
--  Author:             Philip Victor
--  Description :       This will map a measure profile to an account based on the data in the
--                                  [Export].[WS_Measures_StagingAllColumns] table.
-----------------------------------------------------------------------------

ALTER PROCEDURE [Export].[WS_Measures_DefaultMeasureProfileToAccountMapper]
        @measureProfileName [VARCHAR](50) ,
        @measureNumber [INT] ,
        @runId [UNIQUEIDENTIFIER] = NULL
AS
BEGIN
        SET NOCOUNT ON;

        DECLARE @client_id [INT] ,
                @progress_msg [VARCHAR](5000) ,
                @component [VARCHAR](400) = '[export].[WS_Measures_DefaultMeasureProfileToAccountMapper]' ,
                @filter_expression [VARCHAR](1000) ,
            -- the following is the filter expression that will be used if the promotion does not have a filter setting
                @default_filter_expression [VARCHAR](100) = '(1 = 1) /* everyone */ ' ,
                @sql_cmd [VARCHAR](8000) ,
                @update_count [INT] ,
                @account_total [INT] ,
                @setting_list [VARCHAR](2000);

        SELECT
        @client_id = [clientId]
                FROM [Export].[WSMeasureProfile]
                WHERE [ProfileName] = @measureProfileName;

        SET @progress_msg = 'Mapping start - @client_id: ' + CAST(@client_id AS VARCHAR) + '; @measureProfileName: ''' + @measureProfileName + '''';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;


    --  get the expression that is used to perform the mapping
        SELECT @filter_expression = [Value]
                FROM [Export].[WSMeasureSetting]
                WHERE [ClientId] = @client_id
                        AND [ProfileName] = @measureProfileName
                        AND [Name] = 'account_mapper_expression';

        IF ISNULL(LTRIM(RTRIM(@filter_expression)), '') = ''
            BEGIN
            -- there was no expression setup for the profile...use the default
                    SET @filter_expression = @default_filter_expression;
                    SET @progress_msg = 'Profile does not have a account_mapper_expression setting...defaulting to ''' + @default_filter_expression + '''';
                    EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;
            END
        ELSE
            BEGIN
                SET @progress_msg = 'Using ''' + @filter_expression + ''' as filter expression';
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;
            END

    --  map the account to a measure profile.
    -- this will only map accounts that have not already been mapped to the measure profile for a particular measure #
    -- the #account_measure_profile_map contains the mapping of the account to a measure profile.  it is assumed to have
    -- been created by the caller.
        SET @sql_cmd = 'merge into [#WS_Measures_AccountMeasureProfileMap] as [current]
            using
                (
                    select  distinct
                        [staging].[PremiseId], [staging].[Account_Number] as [AccountId], [premise].[PremiseKey], ''' + @measureProfileName
                + ''' as [profile_name], ' + CAST(@measureNumber AS VARCHAR) + ' as [measure_number] '
                + '
                        from
                            [Export].[WS_Measures_StagingAllColumns] as [staging]
                            inner join [dbo].[DimPremise] as [premise] on [premise].[PremiseId] = [staging].[PremiseId] and [premise].[AccountId] = [staging].[Account_Number]

                        where ' + @filter_expression + '
                ) as [new]
            on
                [current].[PremiseId] = [new].[PremiseId] and
                [current].[AccountId] = [new].[AccountId] and
                [current].[MeasureNumber] = [new].[measure_number]
            when not matched by target then
                insert
                    (
                        [PremiseId],        [AccountId],        [PremiseKey],       [MeasureNumber],        [MeasureProfileName]
                    )
                    values
                    (
                        [new].[PremiseId],  [new].[AccountId],  [new].[PremiseKey], [new].[measure_number], [new].[profile_name]
                    );';

        SET @progress_msg = 'start   @sql_cmd:''' + @sql_cmd + '''';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        EXEC(@sql_cmd);

        SET @update_count = @@ROWCOUNT;
        SET @progress_msg = 'Success @sql_cmd:''' + @sql_cmd + '''';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;

        SELECT @account_total = COUNT(*)
                FROM [Export].[WS_Measures_StagingAllColumns];
        SET @progress_msg = 'Mapped ' + CAST(@update_count AS VARCHAR) + ' Accounts out of a possible ' + CAST(@account_total AS VARCHAR)
                        + ' to measureProfile:''' + @measureProfileName + '''; measureNumber:''' + CAST(@measureNumber AS VARCHAR) + '''';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @runId, @component = @component, @text = @progress_msg;
END;

GO
