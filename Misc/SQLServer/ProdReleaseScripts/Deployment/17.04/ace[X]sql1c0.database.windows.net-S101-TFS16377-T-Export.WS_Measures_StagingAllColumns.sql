﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[DF__WS_Measur__Measu__3F076E43]') AND type = 'D')
BEGIN
    ALTER TABLE [Export].[WS_Measures_StagingAllColumns] DROP CONSTRAINT [DF__WS_Measur__Measu__3F076E43]
END

GO
/****** Object:  Index [IDX_WS_Measures_StagingAllColumns_PremiseId]    Script Date: 4/3/2017 11:30:20 AM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_StagingAllColumns]') AND name = N'IDX_WS_Measures_StagingAllColumns_PremiseId')
    DROP INDEX [IDX_WS_Measures_StagingAllColumns_PremiseId] ON [Export].[WS_Measures_StagingAllColumns]
GO
/****** Object:  Table [Export].[WS_Measures_StagingAllColumns]    Script Date: 4/3/2017 11:30:20 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_StagingAllColumns]') AND type in (N'U'))
    DROP TABLE [Export].[WS_Measures_StagingAllColumns]
GO
/****** Object:  Table [Export].[WS_Measures_StagingAllColumns]    Script Date: 4/3/2017 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_StagingAllColumns]') AND type in (N'U'))
BEGIN
    CREATE TABLE [Export].[WS_Measures_StagingAllColumns](
        [Account_Number] [varchar](50) NULL,
        [CustomerID] [varchar](50) NULL,
        [Email] [varchar](50) NULL,
        [GroupID] [varchar](50) NULL,
        [Mailing_Address] [varchar](50) NULL,
        [Mailing_Address 2] [varchar](50) NULL,
        [Mailing_City] [varchar](50) NULL,
        [Mailing_Name] [varchar](50) NULL,
        [Mailing_State] [varchar](50) NULL,
        [Mailing_Zip] [varchar](50) NULL,
        [MaskedAccountNumber] [varchar](50) NULL,
        [MCost1] [varchar](50) NULL,
        [MCost2] [varchar](50) NULL,
        [MCost3] [varchar](50) NULL,
        [MID1] [varchar](50) NULL,
        [MID2] [varchar](50) NULL,
        [MID3] [varchar](50) NULL,
        [MSavings1] [varchar](50) NULL,
        [MSavings2] [varchar](50) NULL,
        [MSavings3] [varchar](50) NULL,
        [MText1] [varchar](50) NULL,
        [MText2] [varchar](50) NULL,
        [MText3] [varchar](50) NULL,
        [MTitle1] [varchar](50) NULL,
        [MTitle2] [varchar](50) NULL,
        [MTitle3] [varchar](50) NULL,
        [Premise_Address] [varchar](50) NULL,
        [Premise_Address 2] [varchar](50) NULL,
        [Premise_City] [varchar](50) NULL,
        [Premise_Name] [varchar](50) NULL,
        [Premise_State] [varchar](50) NULL,
        [Premise_Zip] [varchar](50) NULL,
        [PremiseID] [varchar](50) NULL,
        [ProgramReportNumber] [varchar](50) NULL,
        [ReferrerID] [varchar](50) NULL,
        [Subscriber_Key] [varchar](50) NULL,
        [HESParticipant] [varchar](50) NULL,
        [MeasureFieldsValid] [bit] NOT NULL,
        [HasElectric] [varchar](50) NULL,
        [HasGas] [varchar](50) NULL,
        [HasWater] [varchar](50) NULL
    )
END
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_WS_Measures_StagingAllColumns_PremiseId]    Script Date: 4/3/2017 11:30:21 AM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_StagingAllColumns]') AND name = N'IDX_WS_Measures_StagingAllColumns_PremiseId')
    CREATE NONCLUSTERED INDEX [IDX_WS_Measures_StagingAllColumns_PremiseId] ON [Export].[WS_Measures_StagingAllColumns]
    (
        [PremiseID] ASC
    )WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[DF__WS_Measur__Measu__3F076E43]') AND type = 'D')
BEGIN
    ALTER TABLE [Export].[WS_Measures_StagingAllColumns] ADD  DEFAULT ((0)) FOR [MeasureFieldsValid]
END

GO
