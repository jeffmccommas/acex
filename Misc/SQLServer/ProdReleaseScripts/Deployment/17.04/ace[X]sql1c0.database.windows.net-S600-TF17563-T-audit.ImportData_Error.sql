﻿


/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  Table [audit].[ProfileImportErrors]    Script Date: 4/19/2017 9:47:25 AM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[audit].[ProfileImportErrors]')
                    AND type IN ( N'U' ) )
    DROP TABLE [audit].[ProfileImportErrors];
GO

/****** Object:  Table [audit].[CustomerImportErrors]    Script Date: 4/19/2017 9:47:25 AM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[audit].[CustomerImportErrors]')
                    AND type IN ( N'U' ) )
    DROP TABLE [audit].[CustomerImportErrors];
GO

/****** Object:  Table [audit].[BillImportErrors]    Script Date: 4/19/2017 9:47:25 AM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[audit].[BillImportErrors]')
                    AND type IN ( N'U' ) )
    DROP TABLE [audit].[BillImportErrors];
GO

/****** Object:  Table [audit].[ActionItemImportErrors]    Script Date: 4/19/2017 9:47:25 AM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[audit].[ActionItemImportErrors]')
                    AND type IN ( N'U' ) )
    DROP TABLE [audit].[ActionItemImportErrors];
GO

/****** Object:  Table [audit].[ImportData_Errors_ActonItem]    Script Date: 4/19/2017 9:47:25 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[audit].[ImportData_Errors_ActonItem]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [audit].[ImportData_Errors_ActonItem]
            (
              [ActionItemErrorId] [INT] IDENTITY(1, 1) NOT NULL ,
              [TrackingId] [VARCHAR](50) NOT NULL ,
              [FileName] [VARCHAR](MAX) NULL ,
              [ErrorDateTime] [DATETIME] NOT NULL ,
              [ClientId] [INT] NOT NULL ,
              [CustomerId] [VARCHAR](50) NULL ,
              [AccountId] [VARCHAR](50) NULL ,
              [PremiseId] [VARCHAR](50) NULL ,
              [ActionKey] [VARCHAR](256) NULL ,
              [MessageText] [VARCHAR](MAX) NULL ,
              CONSTRAINT [PK_ImportData_Errors_ActonItem] PRIMARY KEY CLUSTERED
                ( [ActionItemErrorId] ASC )
                WITH ( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF )
            );
    END;
GO
/****** Object:  Table [audit].[ImportData_Errors_Bill]    Script Date: 4/19/2017 9:47:25 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[audit].[ImportData_Errors_Bill]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [audit].[ImportData_Errors_Bill]
            (
              [BillImportErrorId] [INT] IDENTITY(1, 1) NOT NULL ,
              [TrackingId] [VARCHAR](50) NOT NULL ,
              [FileName] [VARCHAR](MAX) NULL ,
              [ErrorDateTime] [DATETIME] NOT NULL ,
              [ClientId] [INT] NOT NULL ,
              [AccountId] [VARCHAR](50) NULL ,
              [PremiseId] [VARCHAR](50) NULL ,
              [ServicePointId] [VARCHAR](64) NULL ,
              [StartDate] [VARCHAR](20) NULL ,
              [EndDate] [VARCHAR](20) NULL ,
              [MessageText] [VARCHAR](MAX) NULL ,
              CONSTRAINT [PK_ImportData_Errors_Bill] PRIMARY KEY CLUSTERED
                ( [BillImportErrorId] ASC )
                WITH ( STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF )
            );
    END;
GO
/****** Object:  Table [audit].[ImportData_Errors_Customer]    Script Date: 4/19/2017 9:47:25 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[audit].[ImportData_Errors_Customer]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [audit].[ImportData_Errors_Customer]
            (
              [CustomerErrorId] [INT] IDENTITY(1, 1) NOT NULL ,
              [TrackingId] [VARCHAR](50) NOT NULL ,
              [FileName] [VARCHAR](MAX) NULL ,
              [ErrorDateTime] [DATETIME] NOT NULL ,
              [ClientId] [INT] NOT NULL ,
              [CustomerId] [VARCHAR](50) NULL ,
              [AccountId] [VARCHAR](50) NULL ,
              [PremiseId] [VARCHAR](50) NULL ,
              [MessageText] [VARCHAR](MAX) NULL ,
              CONSTRAINT [PK_ImportData_Errors_Customer] PRIMARY KEY CLUSTERED
                ( [CustomerErrorId] ASC )
                WITH ( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF )
            );
    END;
GO
/****** Object:  Table [audit].[ImportData_Errors_Profile]    Script Date: 4/19/2017 9:47:25 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[audit].[ImportData_Errors_Profile]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [audit].[ImportData_Errors_Profile]
            (
              [ProfileErrorId] [INT] IDENTITY(1, 1) NOT NULL ,
              [TrackingId] [VARCHAR](50) NOT NULL ,
              [FileName] [VARCHAR](MAX) NULL ,
              [ErrorDateTime] [DATETIME] NOT NULL ,
              [ClientId] [INT] NOT NULL ,
              [AccountId] [VARCHAR](50) NULL ,
              [PremiseId] [VARCHAR](50) NULL ,
              [AttributeKey] [VARCHAR](256) NULL ,
              [AttributeValue] [VARCHAR](256) NULL ,
              [MessageText] [TEXT] NULL ,
              CONSTRAINT [PK_ImportData_Errors_Profile] PRIMARY KEY CLUSTERED
                ( [ProfileErrorId] ASC )
                WITH ( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF )
            );
    END;
GO
