﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  StoredProcedure [Export].[WS_Measures_PivotSetting]    Script Date: 3/29/2017 2:57:00 PM ******/
IF EXISTS ( SELECT * FROM [sys].[objects] WHERE [object_id] = OBJECT_ID(N'[Export].[WS_Measures_PivotSetting]') AND [type] IN (N'P', N'PC') )
        DROP PROCEDURE [Export].[WS_Measures_PivotSetting];
GO

/****** Object:  StoredProcedure [Export].[WS_Measures_PivotReportSetting]    Script Date: 3/29/2017 2:57:00 PM ******/
IF EXISTS ( SELECT * FROM [sys].[objects] WHERE [object_id] = OBJECT_ID(N'[Export].[WS_Measures_PivotReportSetting]') AND [type] IN (N'P', N'PC') )
        DROP PROCEDURE [Export].[WS_Measures_PivotReportSetting];
GO

/****** Object:  StoredProcedure [Export].[WS_Measures_PivotMeasureSetting]    Script Date: 3/29/2017 2:57:00 PM ******/
IF EXISTS ( SELECT * FROM [sys].[objects] WHERE [object_id] = OBJECT_ID(N'[Export].[WS_Measures_PivotMeasureSetting]') AND [type] IN (N'P', N'PC') )
        DROP PROCEDURE [Export].[WS_Measures_PivotMeasureSetting];
GO

/****** Object:  StoredProcedure [Export].[WS_Measures_PivotSetting]    Script Date: 3/29/2017 2:57:00 PM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF NOT EXISTS ( SELECT * FROM [sys].[objects] WHERE [object_id] = OBJECT_ID(N'[Export].[WS_Measures_PivotSetting]') AND [type] IN (N'P', N'PC') )
BEGIN
        EXEC [dbo].[sp_executesql] @statement = N'CREATE PROCEDURE [Export].[WS_Measures_PivotSetting] AS';
END;
GO

ALTER PROCEDURE [Export].[WS_Measures_PivotSetting] (@clientId [INT] ,
                                         @tableName [VARCHAR](255) ,
                                         @keyName [VARCHAR](100) ,
                                         @keyValue [VARCHAR](100) ,
                                         @settingList [VARCHAR](4000))
AS
BEGIN
-------------------------------------------------------------------------------
--  Name:               WS_Measures_PivotSetting
--  Author:             Phil Victor
--  Description :     Pivot the name/value pair settings entries for so they appear as a table.
--                              Each setting in the @settingList will become a column in the pivot table.  In addition to this
--                              the first column returned will have the column name set to @keyName and the value set to @keyValue.
-----------------------------------------------------------------------------
        DECLARE @pivot_cmd [VARCHAR](8000) ,
                @column_names [VARCHAR](4000) = NULL ,
                @result_set_columns [VARCHAR](4000) = NULL ,
                @column_literals [VARCHAR](4000) = NULL ,
                @column_defn [VARCHAR](4000) = NULL ,
                @start_idx [INT] ,
                @stop_idx [INT] ,
                @setting_name [VARCHAR](100) ,
                @setting_list [VARCHAR](4000) = @settingList + ','; -- add the delimiter to the end to make it easire to parse

    -- build our column lists based on the supplied settings

        SET @start_idx = 1;
        SET @stop_idx = CHARINDEX(',', @setting_list);
        WHILE (@stop_idx > 0)
        BEGIN

                SELECT @setting_name = SUBSTRING(@setting_list, @start_idx, @stop_idx - @start_idx);
                SET @setting_name = LTRIM(RTRIM(@setting_name));    -- remove leading and trailing spaces

                SET @column_defn = ISNULL(@column_defn + ', ', '') + '[' + @setting_name + '] [varchar](1000)';
                SET @column_names = ISNULL(@column_names + ', ', '') + '[' + @setting_name + ']';
                SET @result_set_columns = ISNULL(@result_set_columns + ', ', '') + '[pvt].[' + @setting_name + '] as ' + '[' + @setting_name + ']';
                SET @column_literals = ISNULL(@column_literals + ', ', '') + '''' + @setting_name + '''';

                SET @start_idx = @stop_idx + 1;
                SET @stop_idx = CHARINDEX(',', @setting_list, @start_idx);

        END;

        SET @pivot_cmd = '
            select ' + '''' + @keyValue + ''' as [' + @keyName + '], ' + @result_set_columns + '
                from
                    (
                        select [Name],[Value] from ' + @tableName + '
                            where ' + '[ClientId] = ' + CAST(@clientId AS VARCHAR) + ' and ' + @keyName + '= ''' + @keyValue + ''' and ' + '[Name] in ('
                + @column_literals + ')' + ') as src
                pivot
                    (
                        min([Value])
                        for [Name] in (' + @column_names + ')
                    ) as [pvt];
        ';

        EXEC(@pivot_cmd);

        RETURN;
END;

GO

/****** Object:  StoredProcedure [Export].[WS_Measures_PivotMeasureSetting]    Script Date: 3/29/2017 2:57:00 PM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF NOT EXISTS ( SELECT * FROM [sys].[objects] WHERE [object_id] = OBJECT_ID(N'[Export].[WS_Measures_PivotMeasureSetting]')  AND [type] IN (N'P', N'PC') )
BEGIN
        EXEC [dbo].[sp_executesql] @statement = N'CREATE PROCEDURE [Export].[WS_Measures_PivotMeasureSetting] AS';
END;
GO

-------------------------------------------------------------------------------
--  Name:               WS_Measures_PivotMeasureSetting
--  Author:             Phil Victor
--  Description :       Pivot the [Export].[WSMeasureSetting] entries for a profile so they appear as a table.
-----------------------------------------------------------------------------
ALTER PROCEDURE [Export].[WS_Measures_PivotMeasureSetting] (@clientId [INT] ,
                                                @measureProfileName [VARCHAR](100) ,
                                                @settingList [VARCHAR](4000))
AS
BEGIN

        EXEC [Export].[WS_Measures_PivotSetting] @clientId = @clientId, @tableName = '[export].[WSMeasureSetting]', @keyName = 'ProfileName',
                @keyValue = @measureProfileName, @settingList = @settingList;
        RETURN;
END;

GO


/****** Object:  StoredProcedure [Export].[WS_Measures_PivotReportSetting]    Script Date: 3/29/2017 2:57:00 PM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF NOT EXISTS ( SELECT * FROM [sys].[objects] WHERE [object_id] = OBJECT_ID(N'[Export].[WS_Measures_PivotReportSetting]') AND [type] IN (N'P', N'PC') )
BEGIN
        EXEC [dbo].[sp_executesql] @statement = N'CREATE PROCEDURE [Export].[WS_Measures_PivotReportSetting] AS';
END;
GO

-------------------------------------------------------------------------------
--  Name:               WS_Measures_PivotReportSetting
--  Version:            1
--  Author:             Phil Victor
--  Description :       Pivot the [Export].[WSReportSetting] entries for a profile so they appear as a table.
-----------------------------------------------------------------------------
ALTER PROCEDURE [Export].[WS_Measures_PivotReportSetting] (@clientId [INT] ,
                                               @reportProfileName [VARCHAR](100) ,
                                               @settingList [VARCHAR](4000))
AS
BEGIN

        EXEC [Export].[WS_Measures_PivotSetting] @clientId = @clientId, @tableName = '[Export].[WSReportSetting]', @keyName = 'ProfileName',
                @keyValue = @reportProfileName, @settingList = @settingList;
        RETURN;
END;

GO


