﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [Export].[WS_Measures_FetchMeasureProfiles]    Script Date: 3/30/2017 9:49:36 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_FetchMeasureProfiles]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [Export].[WS_Measures_FetchMeasureProfiles]
GO

/****** Object:  StoredProcedure [Export].[WS_Measures_FetchMeasureProfiles]    Script Date: 3/30/2017 9:49:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_FetchMeasureProfiles]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WS_Measures_FetchMeasureProfiles] AS'
END
GO


-------------------------------------------------------------------------------
--  Name:              WSFetchMeasureProfilesForReport
--  Author:             Philip Victor
--  Description :    Fetches the measure profiles associated with a weather sensitivity report
-----------------------------------------------------------------------------

ALTER PROCEDURE [Export].[WS_Measures_FetchMeasureProfiles]
    @reportProfileName [VARCHAR](100) = NULL ,
    @clientId [INT] = 224
AS
    BEGIN
    -- the set of report profiles we are fetching
        DECLARE @rpt_profile TABLE
            (
              [profile_name] [VARCHAR](100)
            );

    -- the set of measure profiles
        DECLARE @measure_profile TABLE
            (
              [profile_name] [VARCHAR](100)
            );

    -- mapping of the report profile to measure profile
        DECLARE @rpt_measure_map TABLE
            (
              [sort_idx] [INT] IDENTITY(1, 1) ,
              [rpt_profile_name] [VARCHAR](100) ,
              [measure_number] [INT] ,
              [measure_profile_name] [VARCHAR](100)
            );

        DECLARE @rpt_profile_name [VARCHAR](100) ,
            @measure_number [INT] ,
            @measure_count [INT] ,
            @setting_name [VARCHAR](100) ,
            @measure_profile_list [VARCHAR](4000) ,
            @measure_profile_name [VARCHAR](100);

        SET NOCOUNT ON;

        INSERT  INTO @rpt_profile
                ( [profile_name]
                )
                SELECT  [ProfileName]
                FROM    [Export].[WSReportProfile]
                WHERE   ( ISNULL(LTRIM(RTRIM(@reportProfileName)), '') = ''
                          OR [ProfileName] = @reportProfileName
                        )
                        AND [ClientId] = @clientId
                ORDER BY [ProfileName];

        IF @@rowcount = 0
            BEGIN
                PRINT 'Weather Sensitivity Report Profile '''
                    + @reportProfileName + ''' NOT found.';
            END;
        ELSE
            BEGIN

                DECLARE rpt_profile_cursor CURSOR LOCAL STATIC FORWARD_ONLY
                FOR
                    SELECT  [profile_name]
                    FROM    @rpt_profile;

                OPEN rpt_profile_cursor;

                FETCH NEXT FROM rpt_profile_cursor INTO @rpt_profile_name;

                WHILE @@fetch_status = 0
                    BEGIN

                        SET @measure_count = NULL;
                        SELECT  @measure_count = CAST([Value] AS [INT])
                        FROM    [Export].[WSReportSetting]
                        WHERE   [clientId] = @clientId
                                AND [ProfileName] = @rpt_profile_name
                                AND [name] = 'measure_count';

            -- iterate over the measure associated with the report
                        SET @measure_number = 1;
                        WHILE @measure_number <= @measure_count
                            BEGIN

                                SET @setting_name = 'measure_profile_list_'
                                    + CAST(@measure_number AS VARCHAR);

                                SET @measure_profile_list = NULL;

                                SELECT  @measure_profile_list = [Value]
                                FROM    [Export].[WSReportSetting]
                                WHERE   [clientId] = @clientId
                                        AND [ProfileName] = @rpt_profile_name
                                        AND [Name] = @setting_name;

                                IF ISNULL(LTRIM(RTRIM(@measure_profile_list)),
                                          '') != ''
                                    BEGIN
                    -- the split function puts 1 entry into the table if the list is empty...we don't want this to happen

                                        DELETE  FROM @measure_profile;

                                        INSERT  @measure_profile
                                                ( [profile_name]
                                                )
                                                SELECT  CAST (param AS VARCHAR(100))
                                                FROM    [dbo].[ufn_split_values](@measure_profile_list, ',');

                    -- add to our map table
                                        INSERT  INTO @rpt_measure_map
                                                ( [rpt_profile_name] ,
                                                  [measure_number] ,
                                                  [measure_profile_name]
                                                )
                                                SELECT  @rpt_profile_name ,
                                                        @measure_number ,
                                                        [profile_name]
                                                FROM    @measure_profile;

                                    END;

                                SET @measure_number += 1;
                            END;

                        FETCH NEXT FROM rpt_profile_cursor INTO @rpt_profile_name;
                    END;

                CLOSE rpt_profile_cursor;
                DEALLOCATE rpt_profile_cursor;

        -- dump our table (the order will be the same order the rows were inserted...this maintains the order of the measures within the measure_profile_list setting)
        -- the consumer of this result can use [sort_idx] to ensure correct ordering
                SELECT  [sort_idx] ,
                        [rpt_profile_name] ,
                        [measure_number] ,
                        [measure_profile_name]
                FROM    @rpt_measure_map
                ORDER BY [sort_idx];

            END;
    END;

GO
