﻿


/***************************************************/
/***** RUN ONLY ON THE INSIGHTSMETADATA SHARD      ***/
/***************************************************/

IF NOT EXISTS ( SELECT  1
                FROM    [admin].[ETLBulkConfiguration]
                WHERE   [ClientComponent] = 'InsightsDW.ImportData.Notification'
                        AND [ClientID] = 0
                        AND [Environment] = 'production' )
    BEGIN
        INSERT  INTO [admin].[ETLBulkConfiguration]
                ( [ClientID] ,
                  [Environment] ,
                  [ClientComponent] ,
                  [XMLConfiguration]
                )
        VALUES  ( 0 ,
                  'production' ,
                  'InsightsDW.ImportData.Notification' ,
                  '<InsightsDW.ImportData.Notification><EmailEnabled>true</EmailEnabled><EmailDistributionList>CEBIEngineersProd@aclara.com,ifarias@aclara.com,cholloway@aclara.com,ssteele@aclara.com</EmailDistributionList><EmailFrom>ACE_Prod@aclara.com</EmailFrom><EmailFromName>AclaraProd</EmailFromName><EmailSmtpAuth>Basic</EmailSmtpAuth><EmailSmtpPassword>Xaclaradev2311X</EmailSmtpPassword><EmailSmtpPort>587</EmailSmtpPort><EmailSmtpServer>smtp.sendgrid.net</EmailSmtpServer><EmailSmtpSsl>true</EmailSmtpSsl><EmailSmtpUser>aclaraprod</EmailSmtpUser><EmailSubject>Error Message from Import Data on PROD</EmailSubject></InsightsDW.ImportData.Notification>'
                );
    END;
GO

IF NOT EXISTS ( SELECT  1
                FROM    [admin].[ETLBulkConfiguration]
                WHERE   [ClientComponent] = 'InsightsDW.ImportData.Notification'
                        AND [ClientID] = 0
                        AND [Environment] = 'uat' )
    BEGIN
        INSERT  INTO [admin].[ETLBulkConfiguration]
                ( [ClientID] ,
                  [Environment] ,
                  [ClientComponent] ,
                  [XMLConfiguration]
                )
        VALUES  ( 0 ,
                  'uat' ,
                  'InsightsDW.ImportData.Notification' ,
                  '<InsightsDW.ImportData.Notification><EmailEnabled>true</EmailEnabled><EmailDistributionList>CEBIEngineersProd@aclara.com,cholloway@aclara.com,ifaynstein@aclara.com,ssteele@aclara.com</EmailDistributionList><EmailFrom>ACE_UAT@aclara.com</EmailFrom><EmailFromName>Aclara UAT</EmailFromName><EmailSmtpAuth>Basic</EmailSmtpAuth><EmailSmtpPassword>Xaclaradev2311X</EmailSmtpPassword><EmailSmtpPort>587</EmailSmtpPort><EmailSmtpServer>smtp.sendgrid.net</EmailSmtpServer><EmailSmtpSsl>true</EmailSmtpSsl><EmailSmtpUser>aclaradev</EmailSmtpUser><EmailSubject>Error Message from Import Data on UAT</EmailSubject></InsightsDW.ImportData.Notification>'
                );
    END;
GO

IF NOT EXISTS ( SELECT  1
                FROM    [admin].[ETLBulkConfiguration]
                WHERE   [ClientComponent] = 'InsightsDW.ImportData.Notification'
                        AND [ClientID] = 0
                        AND [Environment] = 'qa' )
    BEGIN
        INSERT  INTO [admin].[ETLBulkConfiguration]
                ( [ClientID] ,
                  [Environment] ,
                  [ClientComponent] ,
                  [XMLConfiguration]
                )
        VALUES  ( 0 ,
                  'qa' ,
                  'InsightsDW.ImportData.Notification' ,
                  '<InsightsDW.ImportData.Notification><EmailEnabled>true</EmailEnabled><EmailDistributionList>CEBIEngineersDev@aclara.com,ifarias@aclara.com,cholloway@aclara.com,pvictor@aclara.com,ssteele@aclara.com</EmailDistributionList><EmailFrom>ACE_QA@aclara.com</EmailFrom><EmailFromName>Aclara QA</EmailFromName><EmailSmtpAuth>Basic</EmailSmtpAuth><EmailSmtpPassword>Xaclaradev2311X</EmailSmtpPassword><EmailSmtpPort>587</EmailSmtpPort><EmailSmtpServer>smtp.sendgrid.net</EmailSmtpServer><EmailSmtpSsl>true</EmailSmtpSsl><EmailSmtpUser>aclaradev</EmailSmtpUser><EmailSubject>Error Message from Import Data on QA</EmailSubject></InsightsDW.ImportData.Notification>'
                );
    END;
GO

IF NOT EXISTS ( SELECT  1
                FROM    [admin].[ETLBulkConfiguration]
                WHERE   [ClientComponent] = 'InsightsDW.ImportData.Notification'
                        AND [ClientID] = 0
                        AND [Environment] = 'development' )
    BEGIN
        INSERT  INTO [admin].[ETLBulkConfiguration]
                ( [ClientID] ,
                  [Environment] ,
                  [ClientComponent] ,
                  [XMLConfiguration]
                )
        VALUES  ( 0 ,
                  'development' ,
                  'InsightsDW.ImportData.Notification' ,
                  '<InsightsDW.ImportData.Notification><EmailEnabled>true</EmailEnabled><EmailDistributionList>CEBIEngineersDev@aclara.com,ifarias@aclara.com,cholloway@aclara.com,pvictor@aclara.com,ssteele@aclara.com</EmailDistributionList><EmailFrom>ACE_Dev@aclara.com</EmailFrom><EmailFromName>Aclara Dev</EmailFromName><EmailSmtpAuth>Basic</EmailSmtpAuth><EmailSmtpPassword>Xaclaradev2311X</EmailSmtpPassword><EmailSmtpPort>587</EmailSmtpPort><EmailSmtpServer>smtp.sendgrid.net</EmailSmtpServer><EmailSmtpSsl>true</EmailSmtpSsl><EmailSmtpUser>aclaradev</EmailSmtpUser><EmailSubject>Error Message from Import Data on DEV</EmailSubject></InsightsDW.ImportData.Notification>'
                );
    END;
GO


