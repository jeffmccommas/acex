﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  Table [Export].[WS_Measures_History]    Script Date: 3/29/2017 3:18:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_History]') AND type in (N'U'))
    DROP TABLE [Export].[WS_Measures_History]
GO

/****** Object:  Table [Export].[WS_Measures_History]    Script Date: 3/29/2017 3:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_History]') AND type in (N'U'))
BEGIN
CREATE TABLE [Export].[WS_Measures_History](
    [Profile_Name] [varchar](50) NOT NULL,
    [PremiseID] [varchar](50) NOT NULL,
    [MeasureID] [varchar](50) NOT NULL,
    [History_Date] [datetime] NOT NULL,
    [Report_Year] [int] NULL,
    [Report_Month] [int] NULL,
    [ClientId] [int] NULL,
    [MeasureNumber] [int] NOT NULL DEFAULT ((1))
)
END
GO
