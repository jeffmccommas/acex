﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  StoredProcedure [Export].[WS_Measures_CreateStagingAllcols]    Script Date: 3/29/2017 9:54:41 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_CreateStagingAllCols]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [Export].[WS_Measures_CreateStagingAllCols]
GO
/****** Object:  StoredProcedure [Export].[WS_Measures_CreateStagingAllCols]    Script Date: 3/29/2017 9:54:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WS_Measures_CreateStagingAllCols]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WS_Measures_CreateStagingAllCols] AS'
END
GO

-------------------------------------------------------------------------------
--  Name:               [Export].[WS_Measures_CreateStagingAllCols]
--  Author:             Philip Victor
--  Description :
--  Notes:
-------------------------------------------------------------------------------
ALTER PROCEDURE [Export].[WS_Measures_CreateStagingAllCols] @profile_name VARCHAR(50) ,
        @report_year VARCHAR(4) ,
        @report_month VARCHAR(2) ,
        @report_number VARCHAR(2) ,
        @treatment_groups VARCHAR(50) ,
        @programgroupnumber VARCHAR(1024) ,
        @programenrollmentstatus VARCHAR(1024) ,
        @programchannel VARCHAR(1024) ,
        @programenrollmentstatusenrolled VARCHAR(1024) ,
        @report_channel VARCHAR(128) ,
        @run_id VARCHAR(1024)
        WITH RECOMPILE        -- force regeneration of query plan
AS
BEGIN
        SET NOCOUNT ON;

    --  Declare additional needed to populate Profile Table values
        DECLARE @client_id INT ,
                @fuel_type VARCHAR(100) ,
                @energyobjectcategory_List VARCHAR(1024) ,
                @period VARCHAR(10) ,
                @season_List VARCHAR(100) ,
                @progress_msg VARCHAR(MAX) ,
                @component VARCHAR(400) = '[Export].[WS_Measures_CreateStagingAllCols]' ,
                @count INT ,
                @new_line CHAR(2) = CHAR(13) + CHAR(10);

    --  Populate parameters
        SELECT
                        @client_id = [ClientId], @fuel_type = [FuelType], @energyobjectcategory_List = [EnergyObjectCategoryList], @period = [Period],
                        @season_List = [SeasonList]
                FROM [Export].[WSReportProfile]
                WHERE [ProfileName] = @profile_name;

        IF @@ROWCOUNT = 0
                RAISERROR(N'Invalid Profile Name' , -9 , 1);


    -- get the commodity id
        DECLARE @commodity_id INT;
        SELECT
                        @commodity_id = [CommodityID]
                FROM [cm].[TypeCommodity]
                WHERE [CommodityKey] = @fuel_type;

        SET @progress_msg = 'Create Staging   ' + '@client_id:''' + CAST(@client_id AS VARCHAR) + '''; ' + '@fuel_type:''' + @fuel_type + '''; '
                + '@commodity_id:''' + CAST(@commodity_id AS VARCHAR) + '''; ' + '@profile_name:''' + @profile_name + '''; ' + '@report_year:''' + @report_year
                + '''; ' + '@report_month:''' + @report_month + '''; ' + '@treatment_groups:''' + @treatment_groups + '''; ';

        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

    --  Declare additional needed parameters
        DECLARE @sqlcmd VARCHAR(MAX) ,
                @sqlcmd_cursor VARCHAR(8000) ,
                @column_name VARCHAR(128) ,
                @column_count INT   = 1;

        DECLARE @num_energyObjectCategory INT ,
                @count_cols INT;

    -- create enrollment_filter table
        IF OBJECT_ID('tempdb..#WS_Measures_Enrollment_Filter') IS NOT NULL
                DROP TABLE [#WS_Measures_Enrollment_Filter];
        CREATE TABLE [#WS_Measures_Enrollment_Filter] ([PremiseKey] INT NOT NULL ,
                                                     [GroupId] VARCHAR(255));
        CREATE UNIQUE NONCLUSTERED INDEX [IDX_WS_Measures_Enrollment_Filter_PremiseKey] ON [#WS_Measures_Enrollment_Filter]([PremiseKey]);

    -- create benchmark_filter table
        IF OBJECT_ID('tempdb..#WS_Measures_Benchmark_Filter') IS NOT NULL
                DROP TABLE [#WS_Measures_Benchmark_Filter];
        CREATE TABLE [#WS_Measures_Benchmark_Filter] ([PremiseKey] INT NOT NULL ,
                                                    [GroupId] VARCHAR(255));
        CREATE UNIQUE NONCLUSTERED INDEX [IDX_WS_Measures_Benchmark_Filter_PremiseKey] ON [#WS_Measures_Benchmark_Filter]([PremiseKey]);

    -- create treatment group filter
        IF OBJECT_ID('tempdb..#WS_Measures_Treatment_Group_Filter') IS NOT NULL
                DROP TABLE [#WS_Measures_Treatment_Group_Filter];

        CREATE TABLE [#WS_Measures_Treatment_Group_Filter] ([GroupId] VARCHAR(20));
        INSERT [#WS_Measures_Treatment_Group_Filter] ([GroupId])
                SELECT CAST ([param] AS VARCHAR(20))
                        FROM [dbo].[ufn_split_values](@treatment_groups, ',');

    -- Truncate the WSStaging_All_Columns table if it exists
        IF OBJECT_ID('[Export].[WS_Measures_StagingAllColumns]') IS NOT NULL
                TRUNCATE TABLE [Export].[WS_Measures_StagingAllColumns];

    -- table has been created
        SET @progress_msg = 'WS_Measures_StagingAllColumns table has been truncated';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

    --
    --  populate the enrollment_filter table based on treatment groups...this will contain the valid premises based on the group enrollment
    --
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = 'Populating the Enrollment filter';

        INSERT INTO [#WS_Measures_Enrollment_Filter] ([PremiseKey], [GroupId])
                SELECT [group].[PremiseKey], [group].[GroupId]
                        FROM (SELECT [PremiseKey] AS [PremiseKey] ,
                                        [Value] AS [GroupId] ,
                                        ROW_NUMBER() OVER (PARTITION BY [PremiseKey] ORDER BY [EffectiveDate] DESC) AS [rank]
                                FROM [dbo].[FactPremiseAttribute]
                                WHERE [ContentAttributeKey] = @programgroupnumber) AS [group]
                        INNER JOIN [#WS_Measures_Treatment_Group_Filter] AS [group_filter] ON [group_filter].[GroupId] = [group].[GroupId]
                        INNER JOIN (SELECT [PremiseKey] AS [PremiseKey] ,
                                                [Value] AS [Status] ,
                                                ROW_NUMBER() OVER (PARTITION BY [PremiseKey] ORDER BY [EffectiveDate] DESC) AS [rank]
                                        FROM [dbo].[FactPremiseAttribute]
                                        WHERE [ContentAttributeKey] = @programenrollmentstatus) AS [enrollment_status] ON [enrollment_status].[PremiseKey] = [group].[PremiseKey]
                        INNER JOIN (SELECT [PremiseKey] AS [PremiseKey] ,
                                                [Value] AS [Channel] ,
                                                ROW_NUMBER() OVER (PARTITION BY [PremiseKey] ORDER BY [EffectiveDate] DESC) AS [rank]
                                        FROM [dbo].[FactPremiseAttribute]
                                        WHERE [ContentAttributeKey] = @programchannel) AS [channel] ON [channel].[PremiseKey] = [group].[PremiseKey]
                        WHERE [group].[rank] = 1 AND                 -- most recent
                                [enrollment_status].[rank] = 1 AND     -- most recent
                                [channel].[rank] = 1 AND    -- most recent
                                [enrollment_status].[Status] = @programenrollmentstatusenrolled AND -- we only want if still enrolled
                                [channel].[Channel] = @report_channel;  -- ensure the channel matches out request (IE make sure customer has not opted out)


        SELECT @count = COUNT(*) FROM [#WS_Measures_Enrollment_Filter];
        SET @progress_msg = 'Success in populating the Enrollment Filter - count: ' + CAST(@count AS VARCHAR);
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

    --
    --  populate the benchmark_filter table based on billing data...this will contain the accounts that are in the treatment group and have valid billing data
    --
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = 'Populating the Benchmark filter';

    -- To make sure a customer is still active look at their bills
    -- month1 ... month4 represent the first 4 months on the report (most recent month is month1)
    -- for example; if we are running the report for Oct them month1=Oct; month2=Sep; month3=Aug; month4=Jul
    --
        DECLARE @month1_start_date DATE = CAST(@report_year + '-' + @report_month + '-01' AS DATE);     -- first day of the month
        DECLARE @month1_end_date DATE = DATEADD(DAY, -1, DATEADD(MONTH, 1, @month1_start_date));    -- last day of the month

        DECLARE @month2_start_date DATE = DATEADD(MONTH, -1, @month1_start_date);                   -- start of month 2
        DECLARE @month2_end_date DATE = DATEADD(DAY, -1, DATEADD(MONTH, 1, @month2_start_date));    -- last day of the month

        DECLARE @report_end_date DATE = @month1_end_date;                                           -- report end date
        DECLARE @report_start_date DATE = DATEADD(MONTH, -24, @report_end_date);                    -- report start date

        SET @progress_msg = 'Month Filters   ' + '@month1:{ start:''' + CAST(@month1_start_date AS VARCHAR) + ''', end:''' + CAST(@month1_end_date AS VARCHAR)
                + '}; ' + '@month2:{ start:''' + CAST(@month2_start_date AS VARCHAR) + ''', end:''' + CAST(@month2_end_date AS VARCHAR) + '}; '
                + '@report:{ start:''' + CAST(@report_start_date AS VARCHAR) + ''', end:''' + CAST(@report_end_date AS VARCHAR) + '}; ';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

        SELECT @progress_msg = 'Filtering accounts with at least 1 bill between' + CAST(@month2_start_date AS VARCHAR) + ' and '
                        + CAST(@month1_end_date AS VARCHAR);
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

        INSERT INTO [#WS_Measures_Benchmark_Filter] ([PremiseKey], [GroupId])
                SELECT
            DISTINCT [enrollment_filter].[PremiseKey], [enrollment_filter].[GroupId]
                        FROM [#WS_Measures_Enrollment_Filter] AS [enrollment_filter]
                        INNER JOIN (
                        -- this is used to ensure the premise has quality data
                        -- NOTE: we are joining against the enrollment_filter within this block for performance reasons
                                    SELECT DISTINCT [bm_group].[PremiseKey]
                                        FROM [Export].[WSBenchmarkGroups] AS [bm_group]
                                        INNER JOIN [#WS_Measures_Enrollment_Filter] AS [enrollment_filter] ON [enrollment_filter].[PremiseKey] = [bm_group].[PremiseKey]
                                        INNER JOIN [Export].[WSBenchmarkResults] AS [benchmark_results] ON [benchmark_results].[GroupId] = [bm_group].[GroupID]
                                        WHERE ISNULL([benchmark_results].[ExtremeDay_AverageHomeUse], 0) > 0
                                                AND         -- positive usage
                                                ISNULL([benchmark_results].[ExtremeDay_EfficientHomeUse], 0) > 0
                                                AND         -- positive usage
                                                ISNULL([benchmark_results].[ExtremeDay_MyUsage], 0) > 0
                                                AND         -- positive usage
                                                [benchmark_results].[CommodityDesc] = @fuel_type
                                                AND [benchmark_results].[IsActive] = 1) AS [quality_filter] ON [quality_filter].[PremiseKey] = [enrollment_filter].[PremiseKey]
                        INNER JOIN (
                        -- this is used to ensure our billing filter requirements are met
                        -- NOTE: we are joining against the enrollment_filter within this block for performance reasons
                                    SELECT [MonthlyBillingDates].[PremiseKey]
                                        FROM (SELECT DISTINCT [enrollment_filter].[PremiseKey],
                                                        DATEADD(DAY, -(DATEPART(DAY, [fspb].[EndDate]) - 1), [fspb].[EndDate]) AS [BillMonth] -- convert all bills to first of the month
                                                FROM [dbo].[DimServiceContract] AS [sc]
                                                INNER JOIN [#WS_Measures_Enrollment_Filter] AS [enrollment_filter] ON [enrollment_filter].[PremiseKey] = [sc].[PremiseKey]
                                                INNER JOIN [dbo].[FactServicePointBilling] AS [fspb] ON [sc].[ServiceContractKey] = [fspb].[ServiceContractKey]
                                                WHERE [fspb].[EndDate] BETWEEN @month2_start_date AND @month1_end_date
                                              ) AS [MonthlyBillingDates]
                                        GROUP BY [MonthlyBillingDates].[PremiseKey]) AS [monthly_bm_filter] ON [monthly_bm_filter].[PremiseKey] = [enrollment_filter].[PremiseKey];
                                        --HAVING -- last bill must be in the first or second month of the report
                                --MAX([MonthlyBillingDates].[BillMonth]) BETWEEN @month2_start_date AND @month1_end_date) AS [monthly_bm_filter] ON [monthly_bm_filter].[PremiseKey] = [enrollment_filter].[PremiseKey];


        SELECT @count = COUNT(*) FROM [#WS_Measures_Benchmark_Filter];
        SET @progress_msg = 'Success in populating the Benchmark Filter - count: ' + CAST(@count AS VARCHAR);
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;


    --  Insert the rows into [InsightsDW].[Export].[WS_Measures_StagingAllColumns]
    --  this statement determines the data that will end up in the final export...after this point only updates are done to the [Export].[WSStaging_All_Columns] table

    --  --
    --  --note: the Dimcustomer  should get the most recent customer below
    --  --that wht the sort on the ROW_NUMBER() is desc
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = 'Populating [Export].[WS_Measures_StagingAllColumns]';

        INSERT INTO [Export].[WS_Measures_StagingAllColumns] ([PremiseID], [Premise_Address], [Premise_Address 2], [Premise_City], [Premise_State],
                                                              [Premise_Zip], [HasElectric], [HasGas], [HasWater], [GroupId], [ReferrerID], [Mailing_Name],
                                                              [Premise_Name], [Mailing_Address], [Mailing_Address 2], [Mailing_City], [Mailing_State],
                                                              [Mailing_Zip], [CustomerID], [Email], [Account_Number], [ProgramReportNumber], [HESParticipant],
                                                              [MaskedAccountNumber], [Subscriber_Key])
                SELECT DISTINCT [p].[PremiseId] AS [PremiseID], COALESCE(REPLACE([p].[Street1], ',', ' '), '') AS [Premise_Address],
                                COALESCE(REPLACE([p].[Street2], ',', ' '), '') AS [Premise_Address 2], COALESCE([p].[City], '') AS [Premise_City],
                                COALESCE([p].[StateProvince], '') AS [Premise_State], COALESCE([p].[PostalCode], '') AS [Premise_Zip],
                                [p].[ElectricService] AS [HasElectric], [p].[GasService] AS [HasGas], [p].[WaterService] AS [HasWater],
                                [benchmark_filter].[GroupId] AS [GroupId], CAST(@client_id AS VARCHAR) AS [ReferrerID],
                                LEFT((REPLACE([c].[FirstName] + ' ' + [c].[LastName], ',', ' ')), 50) AS [Mailing_Name],
                                LEFT((REPLACE([c].[FirstName] + ' ' + [c].[LastName], ',', ' ')), 50) AS [Premise_Name],
                                COALESCE(REPLACE([c].[Street1], ',', ' '), '') AS [Mailing_Address],
                                COALESCE(REPLACE([c].[Street2], ',', ' '), '') AS [Mailing_Address 2], COALESCE([c].[City], '') AS [Mailing_City],
                                COALESCE([c].[StateProvince], '') AS [Mailing_State], COALESCE([c].[PostalCode], '') AS [Mailing_Zip], [c].[CustomerId] AS [CustomerID],
                                REPLACE(SUBSTRING([c].[EmailAddress], 1, 50), ',', ' ') AS [Email], [p].[AccountId] AS [Account_Number],
                                @report_number AS [ProgramReportNumber], '0' AS [HESParticipant],     -- we will update this later

            -- Only display the last 4 digits of the account number
                                SUBSTRING('********************', 1, LEN([p].[AccountId]) - 4) + SUBSTRING([p].[AccountId], LEN([p].[AccountId]) - 4 + 1,
                                                                                                         LEN([p].[AccountId]) - 4) AS [MaskedAcccountNumber],

            -- only email will have Subscriber_Key
                                COALESCE(CAST([p].[AccountId] AS VARCHAR), '') AS [Subscriber_Key]
                        FROM [dbo].[DimPremise] [p]
                        INNER JOIN (SELECT [PremiseKey] ,
                                                [CustomerKey] ,
                                                ROW_NUMBER() OVER (PARTITION BY [PremiseKey] ORDER BY [DateCreated] DESC) AS [rank]
                                        FROM [dbo].[FactCustomerPremise]) AS [fcp] ON [p].[PremiseKey] = [fcp].[PremiseKey]
                                                                                  AND [fcp].[rank] = 1  -- most recent
                        INNER JOIN [dbo].[DimCustomer] [c] ON [c].[CustomerKey] = [fcp].[CustomerKey]
                                                            AND [fcp].[rank] = 1 -- most recent
                        INNER JOIN [#WS_Measures_Benchmark_Filter] AS [benchmark_filter] ON [benchmark_filter].[PremiseKey] = [p].[PremiseKey];

        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                @text = 'Success populating [Export].[WS_Measures_StagingAllColumns]';

        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = 'Updating HESParticipant value';

    -- UPDATE the home energy audit
        UPDATE [staging]
                SET     [HESParticipant] = '1'
                FROM [Export].[WS_Measures_StagingAllColumns] AS [staging]
                INNER JOIN (
                -- completed home audit query
                            SELECT DISTINCT [PremiseID], [AccountID], [StatusKey],
                                        ROW_NUMBER() OVER (PARTITION BY [PremiseID], [AccountID] ORDER BY [CreateDate] DESC) AS [rank]  -- most recent
                                FROM [dbo].[factAction]
                                WHERE [ActionKey] = 'audithome') AS [home_energy_audit] ON [staging].[PremiseID] = [home_energy_audit].[PremiseID]
                                                                                       AND [staging].[Account_Number] = [home_energy_audit].[AccountID]
                                                                                       AND [home_energy_audit].[StatusKey] = 2
                                                                                       AND     -- completed
                                                                                       [home_energy_audit].[rank] = 1 -- most recent
                WHERE [staging].[HESParticipant] != '1';


        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = 'Success updating HESParticipant value';

        SET NOCOUNT OFF;
END;

GO
