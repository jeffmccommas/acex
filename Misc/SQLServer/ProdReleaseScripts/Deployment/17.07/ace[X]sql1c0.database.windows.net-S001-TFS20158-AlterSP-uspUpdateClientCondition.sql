USE [InsightsMetaData]
/********************************************************/
/***** RUN ON INSIGHTSMETADATA ***/
/***** UPDATE STORED PROCEDURE ************************/
/********************************************************/
GO
/****** Object:  StoredProcedure [cm].[uspUpdateClientCondition]    Script Date: 6/28/2017 9:36:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Srini Ambati
-- Create date: 01/01/2015
-- Description:	This table merges the data from contentful into SQL tables for use by BI
-- Usage Example:
/*
BEGIN TRANSACTION

	declare @p1 cm.ClientConditionTable
	

	exec [cm].[uspUpdateClientCondition] @clientConditionTable=@p1

	SELECT * FROM [cm].[TypeCondition]
	SELECT * FROM [cm].[ClientCondition]

ROLLBACK TRANSACTION
*/
-- =============================================
ALTER PROCEDURE [cm].[uspUpdateClientCondition]
    (
      @clientConditionTable [cm].[ClientConditionTable] READONLY 
    )
AS
    BEGIN
	SELECT * FROM cm.TypeCondition
	-- Step 1: Declara local variables and assign values
        DECLARE @maxConditionID INT;
        DECLARE @currentDateTime DATETIME;

		BEGIN TRANSACTION

		BEGIN TRY

			SELECT  @maxConditionID = ISNULL(MAX(ConditionID), 0) + 1
			FROM    [cm].[TypeCondition] WITH ( NOLOCK );
		
			SET @currentDateTime = GETDATE();

			-- Step 2: Insert new Condition keys 
			;WITH    NewCondition ( ConditionKey )
					  AS ( SELECT DISTINCT
									ConditionKey
						   FROM     @clientConditionTable
						 )
				INSERT  INTO [cm].[TypeCondition]
						( ConditionKey ,
						  ConditionID ,
						  DateUpdated
						)
						SELECT  na.ConditionKey ,
								ROW_NUMBER() OVER ( ORDER BY na.ConditionKey )
								+ @maxConditionID ,
								@currentDateTime
						FROM    NewCondition AS na WITH ( NOLOCK )
								LEFT OUTER JOIN [cm].[TypeCondition] AS ta WITH ( NOLOCK ) ON na.ConditionKey = ta.ConditionKey
						WHERE   ta.ConditionKey IS NULL

		-- Step 3: Merge Client Conditions
			MERGE ClientCondition AS TARGET
			USING @clientConditionTable AS SOURCE
			ON ( TARGET.ClientID = SOURCE.ClientID
				 AND TARGET.ConditionKey = SOURCE.ConditionKey
			   )
			WHEN MATCHED THEN
				UPDATE SET
						TARGET.[ConditionKey] = SOURCE.[ConditionKey],
						TARGET.[CategoryKey] = SOURCE.[CategoryKey] ,
						TARGET.[Description] = SOURCE.[Description] ,
						TARGET.[ProfileAttributeKey] = SOURCE.[ProfileAttributeKey] ,
						TARGET.[Operator] = SOURCE.[Operator] ,
						TARGET.[Value] = SOURCE.[Value] ,
						TARGET.[ProfileOptionKey] = SOURCE.[ProfileOptionKey] ,
						TARGET.[DateUpdated] = @currentDateTime
			WHEN NOT MATCHED BY TARGET THEN
				INSERT ( [ClientID] ,
						 [ConditionKey] ,
						 [CategoryKey] ,
						 [Description] ,
						 [ProfileAttributeKey] ,
						 [Operator] ,
						 [Value] ,
						 [ProfileOptionKey] ,
						 [DateUpdated]
					   )
				VALUES ( SOURCE.[ClientID] ,
						 SOURCE.[ConditionKey] ,
						 SOURCE.[CategoryKey] ,
						 SOURCE.[Description] ,
						 SOURCE.[ProfileAttributeKey] ,
						 SOURCE.[Operator] ,
						 SOURCE.[Value] ,
						 SOURCE.[ProfileOptionKey] ,
						 @currentDateTime
					   );
			--WHEN NOT MATCHED BY SOURCE THEN
			--    DELETE;

		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			THROW;

		END CATCH;

		IF @@TRANCOUNT > 0
			COMMIT TRANSACTION;
    END
