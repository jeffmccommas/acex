
/********************************************************/
/***** RUN ONLY ON THE InsightsDW_210 DATABASE ***/
/********************************************************/


USE [InsightsDW_210];
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_210]    Script Date: 7/17/2017 11:06:26 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[ConvertBillDataToXML_210];
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_101]    Script Date: 7/17/2017 11:06:26 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[ConvertBillDataToXML_101];
GO

/****** Object:  Table [dbo].[RawBillTemp_87]    Script Date: 7/17/2017 11:06:26 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_87];
GO

/****** Object:  Table [dbo].[RawBillTemp_210]    Script Date: 7/17/2017 11:06:26 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_210];
GO

/****** Object:  Table [dbo].[RawBillTemp_101]    Script Date: 7/17/2017 11:06:26 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_101];
GO

/****** Object:  Table [dbo].[RawBillTemp_210]    Script Date: 7/17/2017 11:06:26 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS (
                  SELECT *
                  FROM   [sys].[objects]
                  WHERE  [object_id] = OBJECT_ID(N'[dbo].[RawBillTemp_210]')
                         AND [type] IN ( N'U' )
              )
BEGIN
    CREATE TABLE [dbo].[RawBillTemp_210]
        (
            [TrackingId] [VARCHAR](255) NULL
          , [account_id] [VARCHAR](MAX) NULL
          , [premise_id] [VARCHAR](MAX) NULL
          , [mail_address_line_1] [VARCHAR](MAX) NULL
          , [mail_address_line_2] [VARCHAR](MAX) NULL
          , [mail_address_line_3] [VARCHAR](MAX) NULL
          , [mail_city] [VARCHAR](MAX) NULL
          , [mail_state] [VARCHAR](MAX) NULL
          , [mail_zip_code] [VARCHAR](MAX) NULL
          , [first_name] [VARCHAR](MAX) NULL
          , [last_name] [VARCHAR](MAX) NULL
          , [phone_1] [VARCHAR](MAX) NULL
          , [phone_2] [VARCHAR](MAX) NULL
          , [email] [VARCHAR](MAX) NULL
          , [customer_type] [VARCHAR](MAX) NULL
          , [customer_id] [VARCHAR](MAX) NULL
          , [active_date] [VARCHAR](MAX) NULL
          , [inactive_date] [VARCHAR](MAX) NULL
          , [read_cycle] [VARCHAR](MAX) NULL
          , [rate_code] [VARCHAR](MAX) NULL
          , [service_point_id] [VARCHAR](MAX) NULL
          , [service_house_number] [VARCHAR](MAX) NULL
          , [service_street_name] [VARCHAR](MAX) NULL
          , [service_unit] [VARCHAR](MAX) NULL
          , [service_city] [VARCHAR](MAX) NULL
          , [service_state] [VARCHAR](MAX) NULL
          , [service_zip_code] [VARCHAR](MAX) NULL
          , [meter_type] [VARCHAR](MAX) NULL
          , [meter_units] [VARCHAR](MAX) NULL
          , [usage_value] [VARCHAR](MAX) NULL
          , [bill_enddate] [VARCHAR](MAX) NULL
          , [bill_days] [VARCHAR](MAX) NULL
          , [is_estimate] [VARCHAR](MAX) NULL
          , [usage_charge] [VARCHAR](MAX) NULL
          , [clientId] [VARCHAR](3) NOT NULL
          , [programs] [VARCHAR](MAX) NULL
          , [service_commodity] [VARCHAR](MAX) NULL
          , [account_structure_type] [VARCHAR](MAX) NULL
          , [meter_id] [VARCHAR](MAX) NULL
          , [meter_replaces_meterid] [VARCHAR](MAX) NULL
          , [service_read_date] [VARCHAR](MAX) NULL
          , [basic_charge] [VARCHAR](MAX) NULL
          , [bill_taxes] [VARCHAR](MAX) NULL
          , [demand_unit] [VARCHAR](MAX) NULL
          , [demand_quantity] [VARCHAR](MAX) NULL
          , [demand_charge] [VARCHAR](MAX) NULL
          , [service_agreement_id] [VARCHAR](MAX) NULL
          , [BSEG_START_DATE] [VARCHAR](MAX) NULL
          , [BSEG_END_DATE] [VARCHAR](MAX) NULL
          , [BSEG_ID] [VARCHAR](MAX) NULL
          , [CXX_BSEG_ID] [VARCHAR](MAX) NULL
          , [CSS_ORG_BILL_ENDDATE] [VARCHAR](MAX) NULL
        );
END;
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_210]    Script Date: 7/17/2017 11:06:26 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS (
                  SELECT *
                  FROM   [sys].[objects]
                  WHERE  [object_id] = OBJECT_ID(N'[dbo].[ConvertBillDataToXML_210]')
                         AND [type] IN ( N'P', N'PC' )
              )
BEGIN
    EXEC [sys].[sp_executesql] @statement = N'CREATE PROCEDURE [dbo].[ConvertBillDataToXML_210] AS';
END;
GO
-- =============================================
-- Author:      Ubaid
-- Create date: 06/04/2015
-- Description: This will create customer, billing and profile xml
--              for input  into bulk import
----------------------------------------------------------
--- Philip Victor :- Modified for new Raw table schema - 12/7/2016
--  Susan Steele:  - Modified - removed program data and profile xml.  client is not passing profile or program info - 1-4-17
--  Susan Steele:  Added profile xml back in.  It is required in order to record premisetype=res or bus in the factpremiseattribute table.
--  Philip Victor :- Added NexusTypes
--  Susan Steele:  added Servicepointid and ServiceContractID 4/6/17
--  Susan Steele:  added parameters for rebill - bill record, cancelled bill record, and original bill date. - 05-16-2017
-- =============================================
ALTER PROCEDURE [dbo].[ConvertBillDataToXML_210]
    @ClientId AS INT
  , @TrackingId AS VARCHAR(255)
AS
BEGIN
    SET NOCOUNT ON;

    -- Check for missing required data
    IF EXISTS (
                  SELECT 1
                  FROM   [dbo].[RawBillTemp_210]
                  WHERE  (
                             [BSEG_ID] IS NULL
                             OR LEN([BSEG_ID]) = 0
                         )
                         AND (
                                 [CXX_BSEG_ID] IS NULL
                                 OR LEN([CXX_BSEG_ID]) = 0
                             )
              )
    BEGIN
        THROW 2147483647, 'Missing BSEG_ID or CXX_BSEG_ID', 16;
    END;

    --Set up for potential programs.  Copied from the default convertbilldatatoxml stored proc

    DECLARE @ProgramMapping AS TABLE
        (
            [ProgramName] VARCHAR(255)
          , [PremiseAttributeKey] VARCHAR(255)
        );

    INSERT INTO @ProgramMapping
    VALUES ('EpaymentSubscriber', 'paperlessbilling.enrollmentstatus');
    INSERT INTO @ProgramMapping
    VALUES ('GreenpowerPurchaser', 'greenprogram.enrollmentstatus');
    INSERT INTO @ProgramMapping
    VALUES ('BudgetBilling', 'budgetbilling.enrollmentstatus');
    INSERT INTO @ProgramMapping
    VALUES ('LowIncome', 'lowincomeprogram.enrollmentstatus');
    INSERT INTO @ProgramMapping
    VALUES ('MyAccount', 'utilityportal.enrollmentstatus');
    INSERT INTO @ProgramMapping
    VALUES ('Photovoltaic', 'solarprogram.enrollmentstatus');

    BEGIN TRY
        WITH XMLNAMESPACES (
                               DEFAULT 'Aclara:Insights'
                           )
        --*******************************************
        --This section creates the Customer.xml file
        --*******************************************
        SELECT (
                   SELECT
                       /** Customer Element **/
                         [customer_id] AS '@CustomerId'
                       , LTRIM(RTRIM([first_name])) AS '@FirstName'
                       , LTRIM(RTRIM([last_name])) AS '@LastName'
                       , LTRIM(RTRIM([mail_address_line_1])) AS '@Street1'
                       , LTRIM(RTRIM([mail_address_line_2])) AS '@Street2'
                       , LTRIM(RTRIM([mail_city])) AS '@City'
                       , LTRIM(RTRIM([mail_state])) AS '@State'
                       , CASE WHEN ISNUMERIC([mail_zip_code]) <> 1 THEN 'CA'
                              ELSE 'US'
                         END AS '@Country'
                       , CASE WHEN LEN(LTRIM(RTRIM([mail_zip_code]))) < 5 --any postal code less than 5 characters is invalid
                       THEN       [mail_zip_code] --use the bad zip code in order to notify the client
                              WHEN ISNUMERIC(REPLACE(REPLACE([mail_zip_code], ' ', ''), '-', '')) <> 1 --Canadian postal codes are non-numerid.  Canada's postal codes are alphanumeric. They are in the format A1A 1A1, where A is a letter and 1 is a digit, with a space separating the third and fourth characters.
                       THEN       CASE WHEN LEN(LTRIM(RTRIM([mail_zip_code]))) < 6 -- Canadian postal codes must be 6 characters
                       THEN                [mail_zip_code]                                      --use the bad zip code in order to notify the client
                                       ELSE REPLACE(REPLACE([mail_zip_code], ' ', ''), '-', '') --in our dimpostalcode table, we store postal codes without spaces or dashes
                                  END
                              WHEN LEN(LTRIM(RTRIM([mail_zip_code]))) > 4 --for US postal codes, we are only using the first 5 digits
                       THEN       CAST(SUBSTRING(LTRIM(RTRIM([mail_zip_code])), 1, 5) AS VARCHAR(5))
                         END AS '@PostalCode'
                       , NULLIF(REPLACE(
                                           REPLACE(
                                                      REPLACE(REPLACE(REPLACE([phone_1], '(', ''), ')', ''), '-', '')
                                                    , '-'
                                                    , ''
                                                  )
                                         , ' '
                                         , ''
                                       ) ,'') AS '@PhoneNumber'
                       , NULLIF(REPLACE(
                                           REPLACE(
                                                      REPLACE(REPLACE(REPLACE([phone_1], '(', ''), ')', ''), '-', '')
                                                    , '-'
                                                    , ''
                                                  )
                                         , ' '
                                         , ''
                                       ) ,'') AS '@MobilePhoneNumber'
                       , CASE WHEN LEN([email]) < 6 THEN NULL
                              ELSE COALESCE(NULLIF(LTRIM(RTRIM([email])), ''), [email])
                         END AS '@EmailAddress'
                       , NULL AS '@AlternateEmailAddress'
                       , 'utility' AS '@Source'
                       , CASE WHEN LTRIM(RTRIM([customer_type])) = 'COM' THEN 'true'
                              ELSE 'false'
                         END AS '@IsBusiness'
                       , 'true' AS '@IsAuthenticated'
                       /** Account Element **/
                       , [account_id] AS 'Account/@AccountId'
                       /** Premise Element **/
                       , [premise_id] AS 'Account/Premise/@PremiseId'
                       , LTRIM(RTRIM([service_house_number])) + RTRIM(ISNULL(' ' + [service_street_name], ''))
                         + RTRIM(ISNULL(' ' + [service_unit], '')) AS 'Account/Premise/@PremiseStreet1'
                       , [service_city] AS 'Account/Premise/@PremiseCity'
                       , CASE WHEN LEN(LTRIM(RTRIM([service_zip_code]))) < 5 THEN NULL
                              WHEN LEN(LTRIM(RTRIM([service_zip_code]))) = 5 THEN
                                  CAST(SUBSTRING([service_zip_code], 1, 5) AS VARCHAR(5))
                              WHEN LEN(LTRIM(RTRIM([service_zip_code]))) = 10 THEN
                                  CAST((SUBSTRING([service_zip_code], 1, 5) + SUBSTRING([service_zip_code], 7, 4)) AS VARCHAR(9))
                              ELSE NULL
                         END AS 'Account/Premise/@PremisePostalCode'
                       , CAST(LTRIM(RTRIM([service_state])) AS VARCHAR(2)) AS 'Account/Premise/@PremiseState'
                       , 'US' AS 'Account/Premise/@PremiseCountry'
                       , 'true' AS 'Account/Premise/@HasElectricService'
                       , 'true' AS 'Account/Premise/@HasGasService'
                       , 'true' AS 'Account/Premise/@HasWaterService'
                   FROM  [dbo].[RawBillTemp_210]
                   WHERE [clientId] = @ClientId
                         AND [TrackingId] = @TrackingId
                   FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
               ) AS [XmlData]
             , 'Customer' AS [XmlType]
        UNION
        --*******************************************
        --This section creates the Bill.xml file
        --*******************************************
        SELECT (
                   SELECT
                       /** Customer Element **/
                         [customer_id] AS '@CustomerId'
                       , 'utility' AS '@Source'
                       /** Account Element **/
                       , [account_id] AS 'Account/@AccountId'
                       /** Bill Element **/
                       , [read_cycle] AS 'Account/Bill/@BillCycleScheduleId'
                       /** bill end date**/
                       /**If the original bill end date exists, this is a re-bill.  Use the original bill end date and bill days*/
                       /**If the original bill date is not present, this is the original bill.  Use bill_end date and bill days**/
                       , CASE WHEN LEN(RTRIM(LTRIM([CSS_ORG_BILL_ENDDATE]))) = 0 THEN
                                  CONVERT(VARCHAR, CAST([bill_enddate] AS DATETIME), 126)
                              ELSE CONVERT(VARCHAR, CAST([CSS_ORG_BILL_ENDDATE] AS DATETIME), 126)
                         END AS 'Account/Bill/@EndDate'
                       /** bill start date**/
                       /**Calculate the bill start date from bill end date - number of bill days*/
                       , CASE WHEN LEN(RTRIM(LTRIM([CSS_ORG_BILL_ENDDATE]))) = 0 THEN
                                  CONVERT(
                                             VARCHAR
                                           , DATEADD(d, (CAST([bill_days] AS INT) * -1), CAST([bill_enddate] AS DATETIME))
                                           , 126
                                         )
                              ELSE
                                  CONVERT(
                                             VARCHAR
                                           , DATEADD(
                                                        d
                                                      , (CAST([bill_days] AS INT) * -1)
                                                      , CAST([CSS_ORG_BILL_ENDDATE] AS DATETIME)
                                                    )
                                           , 126
                                         )
                         END AS 'Account/Bill/@StartDate'
                       , 1 AS 'Account/Bill/@BillPeriodType'
                       /**Bill segment and cancelled bill segment**/
                       , CASE WHEN LEN(RTRIM(LTRIM([BSEG_ID]))) > 0 THEN RTRIM(LTRIM([BSEG_ID]))
                         END AS 'Account/Bill/@UtilityBillRecordId'
                       , CASE WHEN LEN(RTRIM(LTRIM([CXX_BSEG_ID]))) > 0 THEN RTRIM(LTRIM([CXX_BSEG_ID]))
                         END AS 'Account/Bill/@CanceledUtilityBillRecordId'

                       /** Bill CostDetails Element **/
                       , CASE WHEN ISNUMERIC([basic_charge]) = 1 THEN CAST([basic_charge] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@STD_CUST'
                       , CASE WHEN ISNUMERIC([bill_taxes]) = 1 THEN CAST([bill_taxes] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@GOVT_TAX_MUNI'
                       , CASE WHEN LEN(LTRIM(RTRIM([demand_unit]))) > 0 THEN
                                  CAST(LTRIM(RTRIM([demand_unit])) AS VARCHAR(50))
                         END AS 'Account/Bill/BillCostDetails/@MAX_DMND_UOM'
                       , CASE WHEN ISNUMERIC([demand_quantity]) = 1 THEN CAST([demand_quantity] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@MAX_DMND'
                       , CASE WHEN ISNUMERIC([demand_charge]) = 1 THEN CAST([demand_charge] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@STD_MAX_DMND'
                       /** Premise Element*/
                       , [premise_id] AS 'Account/Bill/Premise/@PremiseId'
                       /** Service Element **/
                       , CASE WHEN ISNUMERIC([usage_value]) = 1 THEN CAST([usage_value] AS DECIMAL(18, 2))
                              ELSE 0
                         END AS 'Account/Bill/Premise/Service/@TotalUsage'
                       , CASE WHEN ISNUMERIC([usage_charge]) = 1 THEN CAST([usage_charge] AS DECIMAL(18, 2))
                              ELSE 0
                         END AS 'Account/Bill/Premise/Service/@TotalCost'
                       , [service_commodity] AS 'Account/Bill/Premise/Service/@Commodity'
                       , [service_agreement_id] AS 'Account/Bill/Premise/Service/@ServiceContractId'
                       , [service_point_id] AS 'Account/Bill/Premise/Service/@ServicePointId'
                       , CASE WHEN [meter_units] = '1' THEN '3'
                              WHEN [meter_units] = '2' THEN '2'
                              WHEN [meter_units] = '3' THEN '1'
                              WHEN [meter_units] = '6' THEN '4'
                              WHEN [meter_units] = '4' THEN '5'
                              WHEN [meter_units] = '5' THEN '6'
                              WHEN [meter_units] = '7' THEN '7'
                              WHEN [meter_units] = '8' THEN '8'
                              WHEN [meter_units] = '9' THEN '9'
                              WHEN [meter_units] = '10' THEN '10'
                              WHEN [meter_units] = '11' THEN '11'
                              WHEN [meter_units] = '12' THEN '12'
                              WHEN [meter_units] = '13' THEN '13'
                              WHEN [meter_units] = '99' THEN '99'
                              ELSE '-1'
                         END AS 'Account/Bill/Premise/Service/@UOM'
                       , IIF(LEN([active_date]) = 0, NULL, CONVERT(VARCHAR, CAST([active_date] AS DATETIME), 126)) AS 'Account/Bill/Premise/Service/@AMIStartDate'
                       , IIF(LEN([inactive_date]) = 0, NULL, CONVERT(VARCHAR, CAST([inactive_date] AS DATETIME), 126)) AS 'Account/Bill/Premise/Service/@AMIEndDate'
                       --IIF(LEN(service_read_date)=0,NULL,CONVERT(VARCHAR, CAST(service_read_date AS DATETIME), 126)) AS 'Account/Bill/Premise/Service/@ReadDate' , (leave out per task 17398)
                       , [rate_code] AS 'Account/Bill/Premise/Service/@RateClass'
                       , [meter_type] AS 'Account/Bill/Premise/Service/@MeterType'
                       , [meter_id] AS 'Account/Bill/Premise/Service/@MeterId'
                       , [meter_replaces_meterid] AS 'Account/Bill/Premise/Service/@ReplacedMeterId'
                       , CASE WHEN [is_estimate] = '0' THEN 'Actual'
                              WHEN [is_estimate] = '1' THEN 'Estimated'
                         END AS 'Account/Bill/Premise/Service/@ReadQuality'
                       /** Service CostDetails Element **/
                       , CASE WHEN ISNUMERIC([basic_charge]) = 1 THEN CAST([basic_charge] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@STD_CUST'
                       , CASE WHEN ISNUMERIC([bill_taxes]) = 1 THEN CAST([bill_taxes] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@GOVT_TAX_MUNI'
                       , CASE WHEN LEN(LTRIM(RTRIM([demand_unit]))) > 0 THEN
                                  CAST(LTRIM(RTRIM([demand_unit])) AS VARCHAR(50))
                         END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@MAX_DMND_UOM'
                       , CASE WHEN ISNUMERIC([demand_quantity]) = 1 THEN CAST([demand_quantity] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@MAX_DMND'
                       , CASE WHEN ISNUMERIC([demand_charge]) = 1 THEN CAST([demand_charge] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@STD_MAX_DMND'
                   FROM  [dbo].[RawBillTemp_210]
                   WHERE [clientId] = @ClientId
                         AND [TrackingId] = @TrackingId
                   FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
               ) AS [XmlData]
             , 'Bill' AS [XmlType]
        UNION
        SELECT
            --*******************************************
            --This section creates the Profile.xml file
            --*******************************************
            (
                SELECT
                    /** Customer Element **/
                     [customer_id] AS '@CustomerId'
                   /** Account Element **/
                   , [account_id] AS 'Account/@AccountId'
                   , [premise_id] AS 'Account/Premise/@PremiseId'
                   /** ProfileItem Element **/
                   , (
                         SELECT 'premisetype' AS 'ProfileItem/@AttributeKey'
                              , [PremiseType] AS 'ProfileItem/@AttributeValue'
                              , 'utility' AS 'ProfileItem/@Source'
                              , GETDATE() AS 'ProfileItem/@ModifiedDate'
                         FOR XML PATH(''), TYPE, ELEMENTS
                     ) AS 'Account/Premise'
                   , IIF([Attribute1] IS NULL
                       , NULL
                       , (
                             SELECT [Attribute1] AS 'ProfileItem/@AttributeKey'
                                  , [Attribute1] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                  , 'utility' AS 'ProfileItem/@Source'
                                  , GETDATE() AS 'ProfileItem/@ModifiedDate'
                             FOR XML PATH(''), TYPE, ELEMENTS
                         )) AS 'Account/Premise'
                   , IIF([Attribute2] IS NULL
                       , NULL
                       , (
                             SELECT [Attribute2] AS 'ProfileItem/@AttributeKey'
                                  , [Attribute2] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                  , 'utility' AS 'ProfileItem/@Source'
                                  , GETDATE() AS 'ProfileItem/@ModifiedDate'
                             FOR XML PATH(''), TYPE, ELEMENTS
                         )) AS 'Account/Premise'
                   , IIF([Attribute3] IS NULL
                       , NULL
                       , (
                             SELECT [Attribute3] AS 'ProfileItem/@AttributeKey'
                                  , [Attribute3] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                  , 'utility' AS 'ProfileItem/@Source'
                                  , GETDATE() AS 'ProfileItem/@ModifiedDate'
                             FOR XML PATH(''), TYPE, ELEMENTS
                         )) AS 'Account/Premise'
                   , IIF([Attribute4] IS NULL
                       , NULL
                       , (
                             SELECT [Attribute4] AS 'ProfileItem/@AttributeKey'
                                  , [Attribute4] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                  , 'utility' AS 'ProfileItem/@Source'
                                  , GETDATE() AS 'ProfileItem/@ModifiedDate'
                             FOR XML PATH(''), TYPE, ELEMENTS
                         )) AS 'Account/Premise'
                   , IIF([Attribute5] IS NULL
                       , NULL
                       , (
                             SELECT [Attribute5] AS 'ProfileItem/@AttributeKey'
                                  , [Attribute5] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                  , 'utility' AS 'ProfileItem/@Source'
                                  , GETDATE() AS 'ProfileItem/@ModifiedDate'
                             FOR XML PATH(''), TYPE, ELEMENTS
                         )) AS 'Account/Premise'
                   , IIF([Attribute6] IS NULL
                       , NULL
                       , (
                             SELECT [Attribute6] AS 'ProfileItem/@AttributeKey'
                                  , [Attribute6] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                  , 'utility' AS 'ProfileItem/@Source'
                                  , GETDATE() AS 'ProfileItem/@ModifiedDate'
                             FOR XML PATH(''), TYPE, ELEMENTS
                         )) AS 'Account/Premise'
                FROM (
                         SELECT [customer_id]
                              , [account_id]
                              , [premise_id]
                              , CASE WHEN LTRIM(RTRIM([customer_type])) = 'COM' THEN 'premisetype.business'
                                     ELSE 'premisetype.residential'
                                END AS [PremiseType]
                              , [pm1].[PremiseAttributeKey] AS [Attribute1]
                              , [pm2].[PremiseAttributeKey] AS [Attribute2]
                              , [pm3].[PremiseAttributeKey] AS [Attribute3]
                              , [pm4].[PremiseAttributeKey] AS [Attribute4]
                              , [pm5].[PremiseAttributeKey] AS [Attribute5]
                              , [pm6].[PremiseAttributeKey] AS [Attribute6]
                         FROM   [dbo].[RawBillTemp_210]
                                CROSS APPLY (SELECT [str] = [Programs] + ',,,,,,') [f1]
                                CROSS APPLY (SELECT [p1] = CHARINDEX(',', [str])) [ap1]
                                CROSS APPLY (SELECT [p2] = CHARINDEX(',', [str], [p1] + 1)) [ap2]
                                CROSS APPLY (SELECT [p3] = CHARINDEX(',', [str], [p2] + 1)) [ap3]
                                CROSS APPLY (SELECT [p4] = CHARINDEX(',', [str], [p3] + 1)) [ap4]
                                CROSS APPLY (SELECT [p5] = CHARINDEX(',', [str], [p4] + 1)) [ap5]
                                CROSS APPLY (SELECT [p6] = CHARINDEX(',', [str], [p5] + 1)) [ap6]
                                CROSS APPLY (
                                                SELECT [Program1] = SUBSTRING([str], 1, [p1] - 1)
                                                     , [Program2] = SUBSTRING([str], [p1] + 1, [p2] - [p1] - 1)
                                                     , [Program3] = SUBSTRING([str], [p2] + 1, [p3] - [p2] - 1)
                                                     , [Program4] = SUBSTRING([str], [p3] + 1, [p4] - [p3] - 1)
                                                     , [Program5] = SUBSTRING([str], [p4] + 1, [p5] - [p4] - 1)
                                                     , [Program6] = SUBSTRING([str], [p5] + 1, [p6] - [p5] - 1)
                                            ) [Programs]
                                LEFT JOIN @ProgramMapping [pm1] ON [pm1].[ProgramName] = [Program1]
                                LEFT JOIN @ProgramMapping [pm2] ON [pm2].[ProgramName] = [Program2]
                                LEFT JOIN @ProgramMapping [pm3] ON [pm3].[ProgramName] = [Program3]
                                LEFT JOIN @ProgramMapping [pm4] ON [pm4].[ProgramName] = [Program4]
                                LEFT JOIN @ProgramMapping [pm5] ON [pm5].[ProgramName] = [Program5]
                                LEFT JOIN @ProgramMapping [pm6] ON [pm6].[ProgramName] = [Program6]
                         WHERE  [clientId] = @ClientId
                                AND [TrackingId] = @TrackingId
                     ) [s]
                FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
            ) AS [XmlData]
          , 'Profile' AS [XmlType];
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;

GO
