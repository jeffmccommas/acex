USE [InsightsMetaData]
GO
/****** Object:  StoredProcedure [cm].[uspSelectContentClientCondition]    Script Date: 6/30/2017 10:20:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Srini Ambati
-- Create date: 07/09/2014
-- Description:	This proc returns Action Content specific to client
-- Usage Example:
/*
BEGIN TRANSACTION

EXEC [cm].[uspSelectContentClientCondition] 256

ROLLBACK TRANSACTION
*/
-- =============================================

ALTER PROCEDURE [cm].[uspSelectContentClientCondition] ( @clientID INT )
AS
    BEGIN
		
        SELECT  cc.[ConditionKey] AS [Key] ,
                [CategoryKey] AS [Category] ,
                [Description] ,
                [ProfileAttributeKey] ,
                [Operator] ,
                [Value] ,
                [ProfileOptionKey]
        FROM    [cm].[ClientCondition] AS cc WITH ( NOLOCK )
				INNER JOIN [cm].TypeCondition AS tc WITH ( NOLOCK ) ON tc.ConditionKey = cc.ConditionKey
                INNER JOIN ( SELECT MAX(clientid) AS clientid ,
                                    ConditionKey
                             FROM   cm.ClientCondition WITH ( NOLOCK )
                             WHERE  clientid IN ( 0, @clientID )
                             GROUP BY ConditionKey
                           ) AS ba ON ba.clientid = cc.ClientID
                                      AND ba.ConditionKey = cc.ConditionKey              
		WHERE [cc].[Disable] = 0
		ORDER BY cc.[ConditionKey]
    END
