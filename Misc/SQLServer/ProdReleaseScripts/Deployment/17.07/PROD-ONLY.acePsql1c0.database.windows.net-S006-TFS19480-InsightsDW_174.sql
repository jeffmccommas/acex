

/********************************************************/
/***** RUN ONLY ON THE InsightsDW_174 DATABASE ***/
/********************************************************/

USE [InsightsDW_174];
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_210]    Script Date: 7/17/2017 10:58:50 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[ConvertBillDataToXML_210];
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_174]    Script Date: 7/17/2017 10:58:50 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[ConvertBillDataToXML_174];
GO

/****** Object:  Table [dbo].[RawBillTemp_87]    Script Date: 7/17/2017 10:58:50 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_87];
GO

/****** Object:  Table [dbo].[RawBillTemp_210]    Script Date: 7/17/2017 10:58:50 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_210];
GO

/****** Object:  Table [dbo].[RawBillTemp_174]    Script Date: 7/17/2017 10:58:50 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_174];
GO

/****** Object:  Table [dbo].[RawBillTemp_174]    Script Date: 7/17/2017 10:58:50 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF NOT EXISTS (
                  SELECT *
                  FROM   [sys].[objects]
                  WHERE  [object_id] = OBJECT_ID(N'[dbo].[RawBillTemp_174]')
                         AND [type] IN ( N'U' )
              )
BEGIN
    CREATE TABLE [dbo].[RawBillTemp_174]
        (
            [TrackingId] [VARCHAR](255) NULL
          , [customer_id] [VARCHAR](MAX) NULL
          , [premise_id] [VARCHAR](MAX) NULL
          , [mail_address_line_1] [VARCHAR](MAX) NULL
          , [mail_address_line_2] [VARCHAR](MAX) NULL
          , [mail_address_line_3] [VARCHAR](MAX) NULL
          , [mail_city] [VARCHAR](MAX) NULL
          , [mail_state] [VARCHAR](MAX) NULL
          , [mail_zip_code] [VARCHAR](MAX) NULL
          , [first_name] [VARCHAR](MAX) NULL
          , [last_name] [VARCHAR](MAX) NULL
          , [phone_1] [VARCHAR](MAX) NULL
          , [phone_2] [VARCHAR](MAX) NULL
          , [email] [VARCHAR](MAX) NULL
          , [customer_type] [VARCHAR](MAX) NULL
          , [account_id] [VARCHAR](MAX) NULL
          , [active_date] [VARCHAR](MAX) NULL
          , [inactive_date] [VARCHAR](MAX) NULL
          , [read_cycle] [VARCHAR](MAX) NULL
          , [rate_code] [VARCHAR](MAX) NULL
          , [service_point_id] [VARCHAR](MAX) NULL
          , [service_house_number] [VARCHAR](MAX) NULL
          , [service_street_name] [VARCHAR](MAX) NULL
          , [service_unit] [VARCHAR](MAX) NULL
          , [service_city] [VARCHAR](MAX) NULL
          , [service_state] [VARCHAR](MAX) NULL
          , [service_zip_code] [VARCHAR](MAX) NULL
          , [meter_type] [VARCHAR](MAX) NULL
          , [meter_units] [VARCHAR](MAX) NULL
          , [bldg_sq_foot] [VARCHAR](MAX) NULL
          , [year_built] [VARCHAR](MAX) NULL
          , [bedrooms] [VARCHAR](MAX) NULL
          , [assess_value] [VARCHAR](MAX) NULL
          , [usage_value] [VARCHAR](MAX) NULL
          , [bill_enddate] [VARCHAR](MAX) NULL
          , [bill_days] [VARCHAR](MAX) NULL
          , [is_estimate] [VARCHAR](MAX) NULL
          , [usage_charge] [VARCHAR](MAX) NULL
          , [clientId] [VARCHAR](3) NOT NULL
          , [programs] [VARCHAR](MAX) NULL
          , [service_commodity] [VARCHAR](MAX) NULL
          , [account_structure_type] [VARCHAR](MAX) NULL
          , [meter_id] [VARCHAR](MAX) NULL
          , [billperiod_type] [VARCHAR](MAX) NULL
          , [meter_replaces_meterid] [VARCHAR](MAX) NULL
          , [service_read_date] [VARCHAR](MAX) NULL
          , [BudgetBillingStatus] [VARCHAR](MAX) NULL
          , [LastPaymentAmount] [VARCHAR](MAX) NULL
          , [LastPaymentDate] [VARCHAR](MAX) NULL
          , [TotalAmount] [VARCHAR](MAX) NULL
          , [PastDue] [VARCHAR](MAX) NULL
          , [PaymentType] [VARCHAR](MAX) NULL
          , [DueDate] [VARCHAR](MAX) NULL
          , [BillMonth] [VARCHAR](MAX) NULL
          , [BillDate] [VARCHAR](MAX) NULL
          , [BillCycle] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR2] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR2_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR3] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR3_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR4] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR4_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR5] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR5_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR6] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR6_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR7] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR7_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR8] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR8_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR9] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR9_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR10] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR10_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR11] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR11_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR12] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR12_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR13] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR13_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR14] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR14_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR15] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR15_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR16] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR16_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR17] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR17_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR18] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR18_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR19] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR19_DESC] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR20] [VARCHAR](MAX) NULL
          , [Bill_OTHR_OTHR20_DESC] [VARCHAR](MAX) NULL
          , [PremiseDescription] [VARCHAR](MAX) NULL
          , [ReadCycle] [VARCHAR](MAX) NULL
          , [RetailSupply] [VARCHAR](MAX) NULL
          , [NextReaddate] [VARCHAR](MAX) NULL
          , [MeterDescription] [VARCHAR](MAX) NULL
          , [MeterLocation] [VARCHAR](MAX) NULL
          , [MeterMultiplier] [VARCHAR](MAX) NULL
          , [MeterType] [VARCHAR](MAX) NULL
          , [MeterTwoWay] [VARCHAR](MAX) NULL
          , [STD_ENGY] [VARCHAR](MAX) NULL
          , [DIST_ENGY] [VARCHAR](MAX) NULL
          , [SUPP_ENGY] [VARCHAR](MAX) NULL
          , [TOTAL] [VARCHAR](MAX) NULL
          , [TOTAL_UOM] [VARCHAR](MAX) NULL
          , [TOTAL_START_READ] [VARCHAR](MAX) NULL
          , [TOTAL_END_READ] [VARCHAR](MAX) NULL
          , [TOTAL_READ_TYPE] [VARCHAR](MAX) NULL
          , [GOVT_OTHR] [VARCHAR](MAX) NULL
          , [GOVT_OTHER_DESC] [VARCHAR](MAX) NULL
          , [STD_CUST] [VARCHAR](MAX) NULL
          , [STD_CUST_DESC] [VARCHAR](MAX) NULL
          , [Service_OTHR_OTHR] [VARCHAR](MAX) NULL
          , [Service_OTHR_OTHR_DESC] [VARCHAR](MAX) NULL
          , [PTR_REBATE] [VARCHAR](MAX) NULL
          , [TOTAL_GENERATED] [VARCHAR](MAX) NULL
        );
END;
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_174]    Script Date: 7/17/2017 10:58:50 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS (
                  SELECT *
                  FROM   [sys].[objects]
                  WHERE  [object_id] = OBJECT_ID(N'[dbo].[ConvertBillDataToXML_174]')
                         AND [type] IN ( N'P', N'PC' )
              )
BEGIN
    EXEC [sys].[sp_executesql] @statement = N'CREATE PROCEDURE [dbo].[ConvertBillDataToXML_174] AS';
END;
GO

-- =============================================
-- Author:      Ubaid
-- Create date: 06/04/2015
-- Description: This will create customer, billing and profile xml
--              for input  into bulk import
----------------------------------------------------------
--- SS - Copied from 210 and made modifications to correspond to Ameren's (174) RawBillTemp table
--  SS - Added Bill Details and Service Details
-- =============================================
ALTER PROCEDURE [dbo].[ConvertBillDataToXML_174]
    @ClientId AS INT
  , @TrackingId AS VARCHAR(255)
AS
BEGIN
    SET NOCOUNT ON;

    --Set up for potential programs.  Copied from the default convertbilldatatoxml stored proc
    DECLARE @ProgramMapping AS TABLE
        (
            [ProgramName] VARCHAR(255)
          , [PremiseAttributeKey] VARCHAR(255)
        );

    INSERT INTO @ProgramMapping
    VALUES ('EpaymentSubscriber', 'paperlessbilling.enrollmentstatus');
    INSERT INTO @ProgramMapping
    VALUES ('GreenpowerPurchaser', 'greenprogram.enrollmentstatus');
    INSERT INTO @ProgramMapping
    VALUES ('BudgetBilling', 'budgetbilling.enrollmentstatus');
    INSERT INTO @ProgramMapping
    VALUES ('LowIncome', 'lowincomeprogram.enrollmentstatus');
    INSERT INTO @ProgramMapping
    VALUES ('MyAccount', 'utilityportal.enrollmentstatus');
    INSERT INTO @ProgramMapping
    VALUES ('Photovoltaic', 'solarprogram.enrollmentstatus');

    BEGIN TRY
        WITH XMLNAMESPACES (
                               DEFAULT 'Aclara:Insights'
                           )
        SELECT (
                   SELECT
                       /** Customer Element **/
                         [customer_id] AS '@CustomerId'
                       , LTRIM(RTRIM([first_name])) AS '@FirstName'
                       , LTRIM(RTRIM([last_name])) AS '@LastName'
                       , LTRIM(RTRIM([mail_address_line_1])) AS '@Street1'
                       , LTRIM(RTRIM([mail_address_line_2])) AS '@Street2'
                       , LTRIM(RTRIM([mail_city])) AS '@City'
                       , LTRIM(RTRIM([mail_state])) AS '@State'
                       , 'US' AS '@Country'
                       , CASE WHEN LEN(LTRIM(RTRIM([mail_zip_code]))) = 5 THEN
                                  CAST(SUBSTRING([mail_zip_code], 1, 5) AS VARCHAR(5))
                              ELSE
                                  CAST((SUBSTRING([mail_zip_code], 1, 5) + SUBSTRING([mail_zip_code], 7, 4)) AS VARCHAR(9))
                         END AS '@PostalCode'
                       , NULLIF(REPLACE(
                                           REPLACE(
                                                      REPLACE(REPLACE(REPLACE([phone_1], '(', ''), ')', ''), '-', '')
                                                    , '-'
                                                    , ''
                                                  )
                                         , ' '
                                         , ''
                                       ) ,'') AS '@PhoneNumber'
                       , NULLIF(REPLACE(
                                           REPLACE(
                                                      REPLACE(REPLACE(REPLACE([phone_2], '(', ''), ')', ''), '-', '')
                                                    , '-'
                                                    , ''
                                                  )
                                         , ' '
                                         , ''
                                       ) ,'') AS '@MobilePhoneNumber'
                       , CASE WHEN LEN([email]) < 6 THEN NULL
                              ELSE COALESCE(NULLIF(LTRIM(RTRIM([email])), ''), [email])
                         END AS '@EmailAddress'
                       , NULL AS '@AlternateEmailAddress'
                       , 'utility' AS '@Source'
                       , CASE WHEN LTRIM(RTRIM([customer_type])) = 'RESIDENTIAL' THEN 'false'
                              ELSE 'true'
                         END AS '@IsBusiness'
                       , 'true' AS '@IsAuthenticated'
                       /** Account Element **/
                       , [account_id] AS 'Account/@AccountId'
                       /** Premise Element **/
                       , [premise_id] AS 'Account/Premise/@PremiseId'
                       , LTRIM(RTRIM([service_house_number])) + RTRIM(ISNULL(' ' + [service_street_name], ''))
                         + RTRIM(ISNULL(' ' + [service_unit], '')) AS 'Account/Premise/@PremiseStreet1'
                       , [service_city] AS 'Account/Premise/@PremiseCity'
                       , CASE WHEN LEN(LTRIM(RTRIM([service_zip_code]))) < 5 THEN NULL
                              WHEN LEN(LTRIM(RTRIM([service_zip_code]))) = 5 THEN
                                  CAST(SUBSTRING([service_zip_code], 1, 5) AS VARCHAR(5))
                              WHEN LEN(LTRIM(RTRIM([service_zip_code]))) = 10 THEN
                                  CAST((SUBSTRING([service_zip_code], 1, 5) + SUBSTRING([service_zip_code], 7, 4)) AS VARCHAR(9))
                              ELSE NULL
                         END AS 'Account/Premise/@PremisePostalCode'
                       , CAST(LTRIM(RTRIM([service_state])) AS VARCHAR(2)) AS 'Account/Premise/@PremiseState'
                       , 'US' AS 'Account/Premise/@PremiseCountry'
                       , 'true' AS 'Account/Premise/@HasElectricService'
                       , 'true' AS 'Account/Premise/@HasGasService'
                       , 'true' AS 'Account/Premise/@HasWaterService'
                   FROM  [dbo].[RawBillTemp_174]
                   WHERE [clientId] = @ClientId
                         AND [TrackingId] = @TrackingId
                   FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
               ) AS [XmlData]
             , 'Customer' AS [XmlType]
        UNION
        SELECT (
                   SELECT
                       /** Customer Element **/
                         [customer_id] AS '@CustomerId'
                       , 'customer' AS '@Source'
                       /** Account Element **/
                       , [account_id] AS 'Account/@AccountId'
                       /** Bill Element **/
                       , [ReadCycle] AS 'Account/Bill/@BillCycleScheduleId'
                       /** bill end date**/
                       , CONVERT(VARCHAR, CAST([bill_enddate] AS DATETIME), 126) AS 'Account/Bill/@EndDate'
                       , 1 AS 'Account/Bill/@BillPeriodType'
                       /**bill start date using end date and bill days**/
                       , CONVERT(
                                    VARCHAR
                                  , DATEADD(d, (CAST([bill_days] AS INT) * -1), CAST([bill_enddate] AS DATETIME))
                                  , 126
                                ) AS 'Account/Bill/@StartDate'
                       /** Bill CostDetails Element **/
                       , CASE WHEN RTRIM(LTRIM([BudgetBillingStatus])) = 'Y' THEN 'Y'
                              ELSE 'N'
                         END AS 'Account/Bill/BillCostDetails/@BudgetBillingStatus'
                       , CASE WHEN ISNUMERIC([LastPaymentAmount]) = 1 THEN CAST([LastPaymentAmount] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@LastPaymentAmount'
                       , CASE WHEN LEN(RTRIM(LTRIM([LastPaymentDate]))) > 0 THEN
                                  CASE WHEN ISDATE([LastPaymentDate]) = 1 THEN
                                           CONVERT(VARCHAR, CAST([LastPaymentDate] AS DATETIME), 126)
                                  END
                         END AS 'Account/Bill/BillCostDetails/@LastPaymentDate'
                       , CASE WHEN ISNUMERIC([TotalAmount]) = 1 THEN CAST([TotalAmount] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@TotalAmount'
                       , CASE WHEN ISNUMERIC([PastDue]) = 1 THEN CAST([PastDue] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@PastDue'
                       , CASE WHEN LEN(RTRIM(LTRIM([PaymentType]))) > 0 THEN
                                  CAST(LTRIM(RTRIM([PaymentType])) AS VARCHAR(50))
                         END AS 'Account/Bill/BillCostDetails/@Paymenttype'
                       , CASE WHEN LEN(RTRIM(LTRIM([DueDate]))) > 0 THEN
                                  CASE WHEN ISDATE([DueDate]) = 1 THEN CONVERT(VARCHAR, CAST([DueDate] AS DATETIME), 126)
                                  END
                         END AS 'Account/Bill/BillCostDetails/@DueDate'
                       , CASE WHEN LEN(RTRIM(LTRIM([BillMonth]))) > 0 THEN CAST([BillMonth] AS INT)
                         END AS 'Account/Bill/BillCostDetails/@BillMonth'
                       , CASE WHEN LEN(RTRIM(LTRIM([BillDate]))) > 0 THEN
                                  CASE WHEN ISDATE([BillDate]) = 1 THEN
                                           CONVERT(VARCHAR, CAST([BillDate] AS DATETIME), 126)
                                  END
                         END AS 'Account/Bill/BillCostDetails/@BillDate'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR]) = 1 THEN CAST([Bill_OTHR_OTHR] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR_DESC]))) > 0 THEN [Bill_OTHR_OTHR_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR2]) = 1 THEN CAST([Bill_OTHR_OTHR2] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR2'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR2_DESC]))) > 0 THEN [Bill_OTHR_OTHR2_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR2_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR3]) = 1 THEN CAST([Bill_OTHR_OTHR3] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR3'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR3_DESC]))) > 0 THEN [Bill_OTHR_OTHR3_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR3_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR4]) = 1 THEN CAST([Bill_OTHR_OTHR4] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR4'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR4_DESC]))) > 0 THEN [Bill_OTHR_OTHR4_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR4_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR5]) = 1 THEN CAST([Bill_OTHR_OTHR5] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR5'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR5_DESC]))) > 0 THEN [Bill_OTHR_OTHR5_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR5_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR6]) = 1 THEN CAST([Bill_OTHR_OTHR6] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR6'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR6_DESC]))) > 0 THEN [Bill_OTHR_OTHR6_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR6_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR7]) = 1 THEN CAST([Bill_OTHR_OTHR7] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR7'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR7_DESC]))) > 0 THEN [Bill_OTHR_OTHR7_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR7_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR8]) = 1 THEN CAST([Bill_OTHR_OTHR8] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR8'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR8_DESC]))) > 0 THEN [Bill_OTHR_OTHR8_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR8_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR9]) = 1 THEN CAST([Bill_OTHR_OTHR9] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR9'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR9_DESC]))) > 0 THEN [Bill_OTHR_OTHR9_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR9_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR10]) = 1 THEN CAST([Bill_OTHR_OTHR10] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR10'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR10_DESC]))) > 0 THEN [Bill_OTHR_OTHR10_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR10_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR11]) = 1 THEN CAST([Bill_OTHR_OTHR11] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR11'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR11_DESC]))) > 0 THEN [Bill_OTHR_OTHR11_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR11_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR12]) = 1 THEN CAST([Bill_OTHR_OTHR12] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR12'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR12_DESC]))) > 0 THEN [Bill_OTHR_OTHR12_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR12_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR13]) = 1 THEN CAST([Bill_OTHR_OTHR13] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR13'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR13_DESC]))) > 0 THEN [Bill_OTHR_OTHR13_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR13_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR14]) = 1 THEN CAST([Bill_OTHR_OTHR14] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR14'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR14_DESC]))) > 0 THEN [Bill_OTHR_OTHR14_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR14_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR15]) = 1 THEN CAST([Bill_OTHR_OTHR15] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR15'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR15_DESC]))) > 0 THEN [Bill_OTHR_OTHR15_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR15_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR16]) = 1 THEN CAST([Bill_OTHR_OTHR16] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR16'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR16_DESC]))) > 0 THEN [Bill_OTHR_OTHR16_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR16_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR17]) = 1 THEN CAST([Bill_OTHR_OTHR17] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR17'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR17_DESC]))) > 0 THEN [Bill_OTHR_OTHR17_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR17_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR18]) = 1 THEN CAST([Bill_OTHR_OTHR18] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR18'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR18_DESC]))) > 0 THEN [Bill_OTHR_OTHR18_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR18_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR19]) = 1 THEN CAST([Bill_OTHR_OTHR19] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR19'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR19_DESC]))) > 0 THEN [Bill_OTHR_OTHR19_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR19_DESC'
                       , CASE WHEN ISNUMERIC([Bill_OTHR_OTHR20]) = 1 THEN CAST([Bill_OTHR_OTHR20] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR20'
                       , CASE WHEN LEN(RTRIM(LTRIM([Bill_OTHR_OTHR20_DESC]))) > 0 THEN [Bill_OTHR_OTHR20_DESC]
                         END AS 'Account/Bill/BillCostDetails/@Bill_OTHR_OTHR20_DESC'
                       /** Premise Element*/
                       , [premise_id] AS 'Account/Bill/Premise/@PremiseId'
                       /** Service Element **/
                       , [service_point_id] AS 'Account/Bill/Premise/Service/@ServicePointId'
                       , CASE WHEN ISNUMERIC([usage_value]) = 1 THEN CAST([usage_value] AS DECIMAL(18, 2))
                              ELSE 0
                         END AS 'Account/Bill/Premise/Service/@TotalUsage'
                       , CASE WHEN ISNUMERIC([usage_charge]) = 1 THEN CAST([usage_charge] AS DECIMAL(18, 2))
                              ELSE 0
                         END AS 'Account/Bill/Premise/Service/@TotalCost'
                       , CASE WHEN RTRIM(LTRIM([MeterDescription])) = 'Electric' THEN '1'
                              WHEN RTRIM(LTRIM([MeterDescription])) = 'Gas' THEN '2'
                              ELSE '-1'
                         END AS 'Account/Bill/Premise/Service/@Commodity'
                       , [meter_units] AS 'Account/Bill/Premise/Service/@UOM'
                       , IIF(LEN([active_date]) = 0, NULL, CONVERT(VARCHAR, CAST([active_date] AS DATETIME), 126)) AS 'Account/Bill/Premise/Service/@AMIStartDate'
                       , IIF(LEN([inactive_date]) = 0, NULL, CONVERT(VARCHAR, CAST([inactive_date] AS DATETIME), 126)) AS 'Account/Bill/Premise/Service/@AMIEndDate'
                       , IIF(LEN([service_read_date]) = 0
                           , NULL
                           , CONVERT(VARCHAR, CAST([service_read_date] AS DATETIME), 126)) AS 'Account/Bill/Premise/Service/@ReadDate'
                       , [rate_code] AS 'Account/Bill/Premise/Service/@RateClass'
                       , [meter_type] AS 'Account/Bill/Premise/Service/@MeterType'
                       , [meter_id] AS 'Account/Bill/Premise/Service/@MeterId'
                       , [meter_replaces_meterid] AS 'Account/Bill/Premise/Service/@ReplacedMeterId'
                       , CASE WHEN [is_estimate] = '0' THEN 'Actual'
                              WHEN [is_estimate] = '1' THEN 'Estimated'
                         END AS 'Account/Bill/Premise/Service/@ReadQuality'
                       /** Service CostDetails Element **/
                       , CASE WHEN LEN(LTRIM(RTRIM([PremiseDescription]))) > 0 THEN
                                  CAST(LTRIM(RTRIM([PremiseDescription])) AS VARCHAR(100))
                         END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@PremiseDescription'
                       , CASE WHEN LEN(LTRIM(RTRIM([RetailSupply]))) > 0 THEN
                                  CAST(LTRIM(RTRIM([RetailSupply])) AS VARCHAR(100))
                         END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@RetailSupply'
                       , CASE WHEN LEN(RTRIM(LTRIM([NextReaddate]))) > 0 THEN
                                  CASE WHEN ISDATE([NextReaddate]) = 1 THEN
                                           CONVERT(VARCHAR, CAST([NextReaddate] AS DATETIME), 126)
                                  END
                         END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@NextReaddate'
                       , CASE WHEN LEN(LTRIM(RTRIM([MeterLocation]))) > 0 THEN
                                  CAST(LTRIM(RTRIM([MeterLocation])) AS VARCHAR(100))
                         END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@MeterLocation'
                       , CASE WHEN LEN(RTRIM(LTRIM([MeterMultiplier]))) > 0 THEN
                                  CAST([MeterMultiplier] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@MeterMultiplier'
                       , CASE WHEN LEN(RTRIM(LTRIM([MeterTwoWay]))) > 0 THEN CAST([MeterTwoWay] AS INT)
                         END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@MeterTwoWay'
                       , CASE WHEN ISNUMERIC([STD_ENGY]) = 1 THEN CAST([STD_ENGY] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@STD_ENGY'
                       , CASE WHEN ISNUMERIC([DIST_ENGY]) = 1 THEN CAST([DIST_ENGY] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@DIST_ENGY'
                       , CASE WHEN LEN(LTRIM(RTRIM([SUPP_ENGY]))) > 0 THEN CAST(LTRIM(RTRIM([SUPP_ENGY])) AS VARCHAR(50))
                         END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@SUPP_ENGY'
                       , CASE WHEN ISNUMERIC([TOTAL]) = 1 THEN CAST([TOTAL] AS DECIMAL(18, 2))
                         END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@TOTAL'
                   FROM  [dbo].[RawBillTemp_174]
                   WHERE [clientId] = @ClientId
                         AND [TrackingId] = @TrackingId
                   FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
               ) AS [XmlData]
             , 'Bill' AS [XmlType]
        UNION
        SELECT (
                   SELECT
                       /** Customer Element **/
                        [customer_id] AS '@CustomerId'
                      /** Account Element **/
                      , [account_id] AS 'Account/@AccountId'
                      , [premise_id] AS 'Account/Premise/@PremiseId'
                      /** ProfileItem Element **/
                      , (
                            SELECT 'premisetype' AS 'ProfileItem/@AttributeKey'
                                 , [PremiseType] AS 'ProfileItem/@AttributeValue'
                                 , 'utility' AS 'ProfileItem/@Source'
                                 , GETDATE() AS 'ProfileItem/@ModifiedDate'
                            FOR XML PATH(''), TYPE, ELEMENTS
                        ) AS 'Account/Premise'
                      , IIF([Attribute1] IS NULL
                          , NULL
                          , (
                                SELECT [Attribute1] AS 'ProfileItem/@AttributeKey'
                                     , [Attribute1] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                     , 'utility' AS 'ProfileItem/@Source'
                                     , GETDATE() AS 'ProfileItem/@ModifiedDate'
                                FOR XML PATH(''), TYPE, ELEMENTS
                            )) AS 'Account/Premise'
                      , IIF([Attribute2] IS NULL
                          , NULL
                          , (
                                SELECT [Attribute2] AS 'ProfileItem/@AttributeKey'
                                     , [Attribute2] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                     , 'utility' AS 'ProfileItem/@Source'
                                     , GETDATE() AS 'ProfileItem/@ModifiedDate'
                                FOR XML PATH(''), TYPE, ELEMENTS
                            )) AS 'Account/Premise'
                      , IIF([Attribute3] IS NULL
                          , NULL
                          , (
                                SELECT [Attribute3] AS 'ProfileItem/@AttributeKey'
                                     , [Attribute3] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                     , 'utility' AS 'ProfileItem/@Source'
                                     , GETDATE() AS 'ProfileItem/@ModifiedDate'
                                FOR XML PATH(''), TYPE, ELEMENTS
                            )) AS 'Account/Premise'
                      , IIF([Attribute4] IS NULL
                          , NULL
                          , (
                                SELECT [Attribute4] AS 'ProfileItem/@AttributeKey'
                                     , [Attribute4] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                     , 'utility' AS 'ProfileItem/@Source'
                                     , GETDATE() AS 'ProfileItem/@ModifiedDate'
                                FOR XML PATH(''), TYPE, ELEMENTS
                            )) AS 'Account/Premise'
                      , IIF([Attribute5] IS NULL
                          , NULL
                          , (
                                SELECT [Attribute5] AS 'ProfileItem/@AttributeKey'
                                     , [Attribute5] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                     , 'utility' AS 'ProfileItem/@Source'
                                     , GETDATE() AS 'ProfileItem/@ModifiedDate'
                                FOR XML PATH(''), TYPE, ELEMENTS
                            )) AS 'Account/Premise'
                      , IIF([Attribute6] IS NULL
                          , NULL
                          , (
                                SELECT [Attribute6] AS 'ProfileItem/@AttributeKey'
                                     , [Attribute6] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                     , 'utility' AS 'ProfileItem/@Source'
                                     , GETDATE() AS 'ProfileItem/@ModifiedDate'
                                FOR XML PATH(''), TYPE, ELEMENTS
                            )) AS 'Account/Premise'
                   FROM (
                            SELECT [customer_id]
                                 , [account_id]
                                 , [premise_id]
                                 , CASE WHEN LTRIM(RTRIM([customer_type])) = 'RESIDENTIAL' THEN
                                            'premisetype.residential'
                                        ELSE 'premisetype.business'
                                   END AS [PremiseType]
                                 , [pm1].[PremiseAttributeKey] AS [Attribute1]
                                 , [pm2].[PremiseAttributeKey] AS [Attribute2]
                                 , [pm3].[PremiseAttributeKey] AS [Attribute3]
                                 , [pm4].[PremiseAttributeKey] AS [Attribute4]
                                 , [pm5].[PremiseAttributeKey] AS [Attribute5]
                                 , [pm6].[PremiseAttributeKey] AS [Attribute6]
                            FROM   [dbo].[RawBillTemp_174]
                                   CROSS APPLY (SELECT [str] = [Programs] + ',,,,,,') [f1]
                                   CROSS APPLY (SELECT [p1] = CHARINDEX(',', [str])) [ap1]
                                   CROSS APPLY (SELECT [p2] = CHARINDEX(',', [str], [p1] + 1)) [ap2]
                                   CROSS APPLY (SELECT [p3] = CHARINDEX(',', [str], [p2] + 1)) [ap3]
                                   CROSS APPLY (SELECT [p4] = CHARINDEX(',', [str], [p3] + 1)) [ap4]
                                   CROSS APPLY (SELECT [p5] = CHARINDEX(',', [str], [p4] + 1)) [ap5]
                                   CROSS APPLY (SELECT [p6] = CHARINDEX(',', [str], [p5] + 1)) [ap6]
                                   CROSS APPLY (
                                                   SELECT [Program1] = SUBSTRING([str], 1, [p1] - 1)
                                                        , [Program2] = SUBSTRING([str], [p1] + 1, [p2] - [p1] - 1)
                                                        , [Program3] = SUBSTRING([str], [p2] + 1, [p3] - [p2] - 1)
                                                        , [Program4] = SUBSTRING([str], [p3] + 1, [p4] - [p3] - 1)
                                                        , [Program5] = SUBSTRING([str], [p4] + 1, [p5] - [p4] - 1)
                                                        , [Program6] = SUBSTRING([str], [p5] + 1, [p6] - [p5] - 1)
                                               ) [Programs]
                                   LEFT JOIN @ProgramMapping [pm1] ON [pm1].[ProgramName] = [Program1]
                                   LEFT JOIN @ProgramMapping [pm2] ON [pm2].[ProgramName] = [Program2]
                                   LEFT JOIN @ProgramMapping [pm3] ON [pm3].[ProgramName] = [Program3]
                                   LEFT JOIN @ProgramMapping [pm4] ON [pm4].[ProgramName] = [Program4]
                                   LEFT JOIN @ProgramMapping [pm5] ON [pm5].[ProgramName] = [Program5]
                                   LEFT JOIN @ProgramMapping [pm6] ON [pm6].[ProgramName] = [Program6]
                            WHERE  [clientId] = @ClientId
                                   AND [TrackingId] = @TrackingId
                        ) [s]
                   FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
               ) AS [XmlData]
             , 'Profile' AS [XmlType];

    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;

END;

GO