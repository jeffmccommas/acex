USE [InsightsMetaData]
GO

/****** Object:  View [cm].[vConditions]    Script Date: 6/30/2017 12:44:17 PM ******/
DROP VIEW [cm].[vConditions]
GO

/****** Object:  View [cm].[vConditions]    Script Date: 6/30/2017 12:44:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [cm].[vConditions]
WITH SCHEMABINDING
AS
    SELECT  [ClientID] ,
            cc.[ConditionKey] ,
            [CategoryKey] ,
            [Description] ,
            [ProfileAttributeKey] ,
            [Operator] ,
            [Value] ,
            [ProfileOptionKey]
    FROM    [cm].[ClientCondition] AS cc WITH ( NOLOCK )
	RIGHT JOIN [cm].[TypeCondition] AS tc ON tc.ConditionKey = cc.ConditionKey



GO


