/********************************************************/
/***** RUN ONLY ON THE InsightsDW_101 DATABASE ***/
/********************************************************/

USE [InsightsDW_101];
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_101]    Script Date: 7/17/2017 10:45:41 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[ConvertBillDataToXML_101];
GO

/****** Object:  Table [dbo].[RawBillTemp_87]    Script Date: 7/17/2017 10:45:41 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_87];
GO

/****** Object:  Table [dbo].[RawBillTemp_276]    Script Date: 7/17/2017 10:45:41 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_276];
GO

/****** Object:  Table [dbo].[RawBillTemp_256]    Script Date: 7/17/2017 10:45:41 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_256];
GO

/****** Object:  Table [dbo].[RawBillTemp_224]    Script Date: 7/17/2017 10:45:41 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_224];
GO

/****** Object:  Table [dbo].[RawBillTemp_101]    Script Date: 7/17/2017 10:45:41 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_101];
GO

/****** Object:  Table [dbo].[RawBillTemp_101]    Script Date: 7/17/2017 10:45:41 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF NOT EXISTS (
                  SELECT *
                  FROM   [sys].[objects]
                  WHERE  [object_id] = OBJECT_ID(N'[dbo].[RawBillTemp_101]')
                         AND [type] IN ( N'U' )
              )
BEGIN
    CREATE TABLE [dbo].[RawBillTemp_101]
        (
            [TrackingId] [VARCHAR](255) NULL
          , [CUSTOMER_ID] [VARCHAR](255) NULL
          , [PREMISE_ID] [VARCHAR](255) NULL
          , [SECONDARY_ID] [VARCHAR](255) NULL
          , [MAIL_ADDRESS_LINE_1] [VARCHAR](255) NULL
          , [MAIL_ADDRESS_LINE_2] [VARCHAR](255) NULL
          , [MAIL_ADDRESS_LINE_3] [VARCHAR](255) NULL
          , [MAIL_CITY] [VARCHAR](255) NULL
          , [MAIL_STATE] [VARCHAR](255) NULL
          , [MAIL_ZIP_CODE] [VARCHAR](255) NULL
          , [FIRST_NAME] [VARCHAR](255) NULL
          , [LAST_NAME] [VARCHAR](255) NULL
          , [PHONE_1] [VARCHAR](255) NULL
          , [PHONE_2] [VARCHAR](255) NULL
          , [EMAIL] [VARCHAR](255) NULL
          , [OWNER] [VARCHAR](255) NULL
          , [CUSTOMER_TYPE] [VARCHAR](255) NULL
          , [LANGUAGE_PREFERENCE] [VARCHAR](255) NULL
          , [BUSINESS_NAME] [VARCHAR](255) NULL
          , [OTHER_BUSINESS_SEGMENT] [VARCHAR](255) NULL
          , [ACCOUNT_ID] [VARCHAR](255) NULL
          , [ACTIVE_DATE] [VARCHAR](255) NULL
          , [INACTIVE_DATE] [VARCHAR](255) NULL
          , [IS_ACCOUNT_DELETION] [VARCHAR](255) NULL
          , [READ_CYCLE] [VARCHAR](255) NULL
          , [RATE_CODE] [VARCHAR](255) NULL
          , [SERVICE_POINT_ID] [VARCHAR](255) NULL
          , [SERVICE_HOUSE_NUMBER] [VARCHAR](255) NULL
          , [SERVICE_STREET_NAME] [VARCHAR](255) NULL
          , [SERVICE_UNIT] [VARCHAR](255) NULL
          , [SERVICE_CITY] [VARCHAR](255) NULL
          , [SERVICE_STATE] [VARCHAR](255) NULL
          , [SERVICE_ZIP_CODE] [VARCHAR](255) NULL
          , [METER_TYPE] [VARCHAR](255) NULL
          , [METER_UNITS] [VARCHAR](255) NULL
          , [VOLTAGE_CLASS] [VARCHAR](255) NULL
          , [LATITUDE] [VARCHAR](255) NULL
          , [LONGITUDE] [VARCHAR](255) NULL
          , [DWELLING_TYPE] [VARCHAR](255) NULL
          , [BLDG_SQ_FOOT] [VARCHAR](255) NULL
          , [PARCEL_SQ_FOOT] [VARCHAR](255) NULL
          , [YEAR_BUILT] [VARCHAR](255) NULL
          , [TOTAL_ROOMS] [VARCHAR](255) NULL
          , [ASSESS_VALUE] [VARCHAR](255) NULL
          , [HEAT_TYPE] [VARCHAR](255) NULL
          , [PHOTOVOLTAIC] [VARCHAR](255) NULL
          , [POOL] [VARCHAR](255) NULL
          , [BEDROOMS] [VARCHAR](255) NULL
          , [REGISTER_ID] [VARCHAR](255) NULL
          , [USAGE_VALUE] [VARCHAR](255) NULL
          , [DATE_TO] [VARCHAR](255) NULL
          , [DURATION] [VARCHAR](255) NULL
          , [IS_ESTIMATE] [VARCHAR](255) NULL
          , [USAGE_CHARGE] [VARCHAR](255) NULL
          , [DEMAND_PEAK] [VARCHAR](255) NULL
        );
END;
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_101]    Script Date: 7/17/2017 10:45:41 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS (
                  SELECT *
                  FROM   [sys].[objects]
                  WHERE  [object_id] = OBJECT_ID(N'[dbo].[ConvertBillDataToXML_101]')
                         AND [type] IN ( N'P', N'PC' )
              )
BEGIN
    EXEC [sys].[sp_executesql] @statement = N'CREATE PROCEDURE [dbo].[ConvertBillDataToXML_101] AS';
END;
GO

-- =============================================
-- Author:		Ubaid
-- Create date: 06/04/2015
-- Description:	This will create customer, billing and profile xml
--				for input  into bulk import
-- Updates:  Susan
-- Trimmed FirstName, LastName, others.  Removing a - from phone number
-- =============================================
ALTER PROCEDURE [dbo].[ConvertBillDataToXML_101]
    @ClientId AS INT
  , @TrackingId AS VARCHAR(255)
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        WITH XMLNAMESPACES (
                               DEFAULT 'Aclara:Insights'
                           )
        SELECT (
                   SELECT [CUSTOMER_ID] AS '@CustomerId'                                                                                        -- Customer attributes
                        , LTRIM(RTRIM([FIRST_NAME])) AS '@FirstName'                                                                            -- Customer attributes
                        , LTRIM(RTRIM([LAST_NAME])) AS '@LastName'                                                                              -- Customer attributes
                        , LTRIM(RTRIM([MAIL_ADDRESS_LINE_1])) AS '@Street1'                                                                     -- Customer attributes
                        , LTRIM(RTRIM([MAIL_ADDRESS_LINE_2] + ' ' + [MAIL_ADDRESS_LINE_3])) AS '@Street2'
                        , [MAIL_CITY] AS '@City'                                                                                                -- Customer attributes
                        , [MAIL_STATE] AS '@State'                                                                                              -- Customer attributes
                        , 'US' AS '@Country'                                                                                                    -- Customer attributes
                        , CASE WHEN LEN(LTRIM(RTRIM([MAIL_ZIP_CODE]))) = 5 THEN
                                   CAST(SUBSTRING([MAIL_ZIP_CODE], 1, 5) AS VARCHAR(5))
                               ELSE
                                   CAST((SUBSTRING([MAIL_ZIP_CODE], 1, 5) + SUBSTRING([MAIL_ZIP_CODE], 7, 4)) AS VARCHAR(9))
                          END AS '@PostalCode'                                                                                                  -- Customer attributes
                        , NULLIF(REPLACE(REPLACE(REPLACE(REPLACE([PHONE_1], '(', ''), ')', ''), ' ', ''), '-', ''), '') AS '@PhoneNumber'       -- Customer attributes,
                        , NULLIF(REPLACE(REPLACE(REPLACE(REPLACE([PHONE_2], '(', ''), ')', ''), ' ', ''), '-', ''), '') AS '@MobilePhoneNumber' -- Customer attributes
                        , CASE WHEN LEN([EMAIL]) < 6 THEN NULL
                               ELSE COALESCE(NULLIF(LTRIM(RTRIM([EMAIL])), ''), [EMAIL])
                          END AS '@EmailAddress'                                                                                                -- Customer attributes
                        , NULL AS '@AlternateEmailAddress'                                                                                      -- Customer attributes
                        , 'utility' AS '@Source'                                                                                                -- Customer attributes
                        , 'false' AS '@IsBusiness'                                                                                              -- Customer attributes
                        , 'true' AS '@IsAuthenticated'                                                                                          -- Customer attributes
                        , [ACCOUNT_ID] AS 'Account/@AccountId'                                                                                  -- Account attributes	
                        , [PREMISE_ID] AS 'Account/Premise/@PremiseId'                                                                          -- Premise attributes
                        , LTRIM(RTRIM([SERVICE_HOUSE_NUMBER])) + RTRIM(ISNULL(' ' + [SERVICE_STREET_NAME], ''))
                          + RTRIM(ISNULL(' ' + [SERVICE_UNIT], '')) AS 'Account/Premise/@PremiseStreet1'                                        -- Premise attributes
                        , LTRIM(RTRIM([SERVICE_CITY])) AS 'Account/Premise/@PremiseCity'                                                        -- Premise attributes
                        , CASE WHEN LEN(LTRIM(RTRIM([SERVICE_ZIP_CODE]))) < 5 THEN NULL
                               WHEN LEN(LTRIM(RTRIM([SERVICE_ZIP_CODE]))) = 5 THEN
                                   CAST(SUBSTRING([SERVICE_ZIP_CODE], 1, 5) AS VARCHAR(5))
                               WHEN LEN(LTRIM(RTRIM([SERVICE_ZIP_CODE]))) = 10 THEN
                                   CAST((SUBSTRING([SERVICE_ZIP_CODE], 1, 5) + SUBSTRING([SERVICE_ZIP_CODE], 7, 4)) AS VARCHAR(9))
                               ELSE NULL
                          END AS 'Account/Premise/@PremisePostalCode'                                                                           -- Premise attributes
                        , CAST(LTRIM(RTRIM([SERVICE_STATE])) AS VARCHAR(2)) AS 'Account/Premise/@PremiseState'                                  -- Premise attributes
                        , 'US' AS 'Account/Premise/@PremiseCountry'                                                                             -- Premise attributes
                        , 'false' AS 'Account/Premise/@HasElectricService'                                                                      -- Premise attributes
                        , 'true' AS 'Account/Premise/@HasGasService'                                                                            -- Premise attributes
                        , 'false' AS 'Account/Premise/@HasWaterService'                                                                         -- Premise attributes
                   FROM   [dbo].[RawBillTemp_101]
                   WHERE  [TrackingId] = @TrackingId
                   FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
               ) AS [XmlData]
             , 'Customer' AS [XmlType]
        UNION
        SELECT (
                   SELECT [CUSTOMER_ID] AS '@CustomerId'                                                                                                                 -- Customer attributes
                        , 'customer' AS '@Source'                                                                                                                        -- Customer attributes
                        , [ACCOUNT_ID] AS 'Account/@AccountId'                                                                                                           -- Account attributes
                        , 1 AS 'Account/Bill/@BillPeriodType'                                                                                                            -- Bill attributes
                        , CONVERT(VARCHAR, DATEADD(d, (CAST([DURATION] AS INT) * -1), CAST([DATE_TO] AS DATETIME)), 126) AS 'Account/Bill/@StartDate'                    -- Bill attributes
                        , CONVERT(VARCHAR, CAST([DATE_TO] AS DATETIME), 126) AS 'Account/Bill/@EndDate'                                                                  -- Bill attributes
                        , [PREMISE_ID] AS 'Account/Bill/Premise/@PremiseId'                                                                                              -- Premise attributes
                        , CASE WHEN ISNUMERIC([USAGE_VALUE]) = 1 THEN CAST([USAGE_VALUE] AS DECIMAL(18, 2))
                               ELSE 0
                          END AS 'Account/Bill/Premise/Service/@TotalUsage'                                                                                              -- Service attributes
                        , CASE WHEN ISNUMERIC([USAGE_CHARGE]) = 1 THEN CAST([USAGE_CHARGE] AS DECIMAL(18, 2))
                               ELSE 0
                          END AS 'Account/Bill/Premise/Service/@TotalCost'                                                                                               -- Service attributes
                        , 2 AS 'Account/Bill/Premise/Service/@Commodity'                                                                                                 -- Service attributes
                        , CASE WHEN 2 = 1 THEN '2'
                               WHEN 2 = 2 THEN '6'
                          END AS 'Account/Bill/Premise/Service/@UOM'                                                                                                     -- Service attributes
                        , [SERVICE_POINT_ID] AS 'Account/Bill/Premise/Service/@ServicePointId'                                                                           -- Service attributes
                        , CONVERT(VARCHAR, DATEADD(d, (CAST([DURATION] AS INT) * -1), CAST([DATE_TO] AS DATETIME)), 126) AS 'Account/Bill/Premise/Service/@AMIStartDate' -- Service attributes
                        , CONVERT(VARCHAR, CAST([DATE_TO] AS DATETIME), 126) AS 'Account/Bill/Premise/Service/@AMIEndDate'                                               -- Service attributes
                        , [RATE_CODE] AS 'Account/Bill/Premise/Service/@RateClass'                                                                                       -- Service attributes										 	 
                   FROM   [dbo].[RawBillTemp_101]
                   WHERE  [TrackingId] = @TrackingId
                   FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
               ) AS [XmlData]
             , 'Bill' AS [XmlType]
        UNION
        SELECT (
                   SELECT [CUSTOMER_ID] AS '@CustomerId'               -- Customer attributes
                        , [ACCOUNT_ID] AS 'Account/@AccountId'         -- Account attributes
                        , [PREMISE_ID] AS 'Account/Premise/@PremiseId' -- Premise attributes
                                                                       -- yearbuilt	
                        , CASE WHEN [YEAR_BUILT] IS NOT NULL
                                    AND [YEAR_BUILT] != ''
                                    AND LEN(LTRIM(RTRIM([YEAR_BUILT]))) = 4
                                    AND ISNUMERIC(LTRIM(RTRIM([YEAR_BUILT]))) = 1
                                    AND CAST(LTRIM(RTRIM([YEAR_BUILT])) AS INT) > 1800 THEN
                                   (
                                       SELECT
                                           -- ProfileItem attributes
                                           'house.yearbuilt' AS 'ProfileItem/@AttributeKey'
                                         , [YEAR_BUILT] AS 'ProfileItem/@AttributeValue'
                                         , GETDATE() AS 'ProfileItem/@CreateDate'
                                         , GETDATE() AS 'ProfileItem/@ModifiedDate'
                                         , 'utility' AS 'ProfileItem/@Source'
                                       FOR XML PATH(''), TYPE, ELEMENTS
                                   )
                               ELSE NULL
                          END AS 'Account/Premise'                     --   bldg_sq_foot  == house.totalarea
                        , CASE WHEN [BLDG_SQ_FOOT] IS NOT NULL
                                    AND [BLDG_SQ_FOOT] != ''
                                    AND ISNUMERIC([BLDG_SQ_FOOT]) = 1
                                    AND CAST(LTRIM(RTRIM([BLDG_SQ_FOOT])) AS INT) > 0 THEN
                                   (
                                       SELECT
                                           -- ProfileItem attributes
                                           'house.totalarea' AS 'ProfileItem/@AttributeKey'
                                         , [BLDG_SQ_FOOT] AS 'ProfileItem/@AttributeValue'
                                         , GETDATE() AS 'ProfileItem/@CreateDate'
                                         , GETDATE() AS 'ProfileItem/@ModifiedDate'
                                         , 'utility' AS 'ProfileItem/@Source'
                                       FOR XML PATH(''), TYPE, ELEMENTS
                                   )
                               ELSE NULL
                          END AS 'Account/Premise'
                   FROM   [dbo].[RawBillTemp_101]
                   WHERE  (
                              (
                                  [BLDG_SQ_FOOT] IS NOT NULL
                                  AND [BLDG_SQ_FOOT] != ''
                                  AND ISNUMERIC([BLDG_SQ_FOOT]) = 1
                                  AND CAST(LTRIM(RTRIM([BLDG_SQ_FOOT])) AS INT) > 0
                              )
                              OR (
                                     [YEAR_BUILT] IS NOT NULL
                                     AND [YEAR_BUILT] != ''
                                     AND LEN(LTRIM(RTRIM([YEAR_BUILT]))) = 4
                                     AND ISNUMERIC(LTRIM(RTRIM([YEAR_BUILT]))) = 1
                                     AND CAST(LTRIM(RTRIM([YEAR_BUILT])) AS INT) > 1800
                                 )
                          )
                          AND [TrackingId] = @TrackingId
                   FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
               ) AS [XmlData]
             , 'Profile' AS [XmlType];

    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;

END;







GO
