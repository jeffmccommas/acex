

/********************************************************/
/***** RUN ONLY ON THE InsightsDW_224 DATABASE ***/
/********************************************************/



USE [InsightsDW_224];
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_224]    Script Date: 7/17/2017 11:11:47 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[ConvertBillDataToXML_224];
GO

/****** Object:  Table [dbo].[RawBillTemp_224]    Script Date: 7/17/2017 11:11:47 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_224];
GO

/****** Object:  Table [dbo].[RawBillTemp_224]    Script Date: 7/17/2017 11:11:47 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF NOT EXISTS (
                  SELECT *
                  FROM   [sys].[objects]
                  WHERE  [object_id] = OBJECT_ID(N'[dbo].[RawBillTemp_224]')
                         AND [type] IN ( N'U' )
              )
BEGIN
    CREATE TABLE [dbo].[RawBillTemp_224]
        (
            [TrackingId] [VARCHAR](255) NULL
          , [customer_id] [VARCHAR](255) NULL
          , [premise_id] [VARCHAR](255) NULL
          , [mail_address_line_1] [VARCHAR](255) NULL
          , [mail_address_line_2] [VARCHAR](255) NULL
          , [mail_address_line_3] [VARCHAR](255) NULL
          , [mail_city] [VARCHAR](255) NULL
          , [mail_state] [VARCHAR](255) NULL
          , [mail_zip_code] [VARCHAR](255) NULL
          , [first_name] [VARCHAR](255) NULL
          , [last_name] [VARCHAR](255) NULL
          , [phone_1] [VARCHAR](255) NULL
          , [phone_2] [VARCHAR](255) NULL
          , [email] [VARCHAR](255) NULL
          , [customer_type] [VARCHAR](255) NULL
          , [account_id] [VARCHAR](255) NULL
          , [active_date] [VARCHAR](255) NULL
          , [inactive_date] [VARCHAR](255) NULL
          , [read_cycle] [VARCHAR](255) NULL
          , [rate_code] [VARCHAR](255) NULL
          , [service_house_number] [VARCHAR](255) NULL
          , [service_street_name] [VARCHAR](255) NULL
          , [service_unit] [VARCHAR](255) NULL
          , [service_city] [VARCHAR](255) NULL
          , [service_state] [VARCHAR](255) NULL
          , [service_zip_code] [VARCHAR](255) NULL
          , [meter_type] [VARCHAR](255) NULL
          , [meter_units] [VARCHAR](255) NULL
          , [bldg_sq_foot] [VARCHAR](255) NULL
          , [year_built] [VARCHAR](255) NULL
          , [bedrooms] [VARCHAR](255) NULL
          , [assess_value] [VARCHAR](255) NULL
          , [usage_value] [VARCHAR](255) NULL
          , [bill_enddate] [VARCHAR](255) NULL
          , [bill_days] [VARCHAR](255) NULL
          , [is_estimate] [VARCHAR](255) NULL
          , [usage_charge] [VARCHAR](255) NULL
          , [ClientId] [INT] NOT NULL
          , [Programs] [VARCHAR](MAX) NULL
          , [service_commodity] [VARCHAR](255) NULL
          , [account_structure_type] [VARCHAR](255) NULL
          , [meter_id] [VARCHAR](255) NULL
          , [billperiod_type] [VARCHAR](255) NULL
          , [meter_replaces_meterid] [VARCHAR](255) NULL
          , [service_read_date] [VARCHAR](255) NULL
          , [service_read_startdate] [VARCHAR](255) NULL
          , [kwh_onpeak] [VARCHAR](255) NULL
          , [kwh_offpeak] [VARCHAR](255) NULL
          , [kwh_shoulder] [VARCHAR](255) NULL
          , [kw_onpeak] [VARCHAR](255) NULL
          , [kw_offpeak] [VARCHAR](255) NULL
          , [kw_shoulder] [VARCHAR](255) NULL
          , [child_first_name] [VARCHAR](255) NULL
          , [child_last_name] [VARCHAR](255) NULL
        );
END;
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_224]    Script Date: 7/17/2017 11:11:47 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS (
                  SELECT *
                  FROM   [sys].[objects]
                  WHERE  [object_id] = OBJECT_ID(N'[dbo].[ConvertBillDataToXML_224]')
                         AND [type] IN ( N'P', N'PC' )
              )
BEGIN
    EXEC [sys].[sp_executesql] @statement = N'CREATE PROCEDURE [dbo].[ConvertBillDataToXML_224] AS';
END;
GO

-- =============================================
-- Author:      Ubaid
-- Create date: 06/04/2015
-- Description: This will create customer, billing and profile xml
--              for input  into bulk import
-- Updated:		07/11/2017 -- Changes for Energy Star Portfolio Manager (ESPM)
-- Updated:		07/13/2017 -- Change to ZIP code pre-processing to prevent 9-digit ZIP codes
-- =============================================
ALTER PROCEDURE [dbo].[ConvertBillDataToXML_224]
    @ClientId AS INT
  , @TrackingId AS VARCHAR(255)
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @ProgramMapping AS TABLE
        (
            [ProgramName] VARCHAR(255)
          , [PremiseAttributeKey] VARCHAR(255)
        );

    INSERT INTO @ProgramMapping
    VALUES ('EpaymentSubscriber', 'paperlessbilling.enrollmentstatus');
    INSERT INTO @ProgramMapping
    VALUES ('GreenpowerPurchaser', 'greenprogram.enrollmentstatus');
    INSERT INTO @ProgramMapping
    VALUES ('BudgetBilling', 'budgetbilling.enrollmentstatus');
    INSERT INTO @ProgramMapping
    VALUES ('LowIncome', 'lowincomeprogram.enrollmentstatus');
    INSERT INTO @ProgramMapping
    VALUES ('MyAccount', 'utilityportal.enrollmentstatus');
    INSERT INTO @ProgramMapping
    VALUES ('Photovoltaic', 'solarprogram.enrollmentstatus');

    BEGIN TRY
        WITH XMLNAMESPACES (
                               DEFAULT 'Aclara:Insights'
                           )
        SELECT (
                   SELECT [customer_id] AS '@CustomerId'
                        , CASE WHEN LEN(LTRIM(RTRIM([last_name]))) = 0 THEN ''
                               ELSE [first_name]
                          END AS '@FirstName'
                        , CASE WHEN LEN(LTRIM(RTRIM([last_name]))) = 0 THEN [first_name]
                               ELSE [last_name]
                          END AS '@LastName'
                        , [mail_address_line_1] AS '@Street1'
                        , [mail_address_line_2] AS '@Street2'
                        , [mail_city] AS '@City'
                        , [mail_state] AS '@State'
                        , 'US' AS '@Country'
                        , CASE WHEN LEN(LTRIM(RTRIM([mail_zip_code]))) > 4 THEN
                                   CAST(SUBSTRING([mail_zip_code], 1, 5) AS VARCHAR(5))
                               ELSE [mail_zip_code]
                          END AS '@PostalCode'
                        , NULLIF(REPLACE(REPLACE(REPLACE(REPLACE([phone_1], '(', ''), ')', ''), ' ', ''), '-', ''), '') AS '@PhoneNumber'
                        , NULLIF(REPLACE(REPLACE(REPLACE(REPLACE([phone_2], '(', ''), ')', ''), ' ', ''), '-', ''), '') AS '@MobilePhoneNumber'
                        , CASE WHEN LEN([email]) < 6 THEN NULL
                               ELSE COALESCE(NULLIF(LTRIM(RTRIM([email])), ''), [email])
                          END AS '@EmailAddress'
                        , NULL AS '@AlternateEmailAddress'
                        , 'utility' AS '@Source'
                        , CASE WHEN [customer_type] = 'Commercial' THEN 'true'
                               ELSE 'false'
                          END AS '@IsBusiness'
                        , 'true' AS '@IsAuthenticated'
                        , [account_id] AS 'Account/@AccountId'
                        , [premise_id] AS 'Account/Premise/@PremiseId'
                        , LTRIM(RTRIM([service_house_number])) + RTRIM(ISNULL(' ' + [service_street_name], ''))
                          + RTRIM(ISNULL(' ' + [service_unit], '')) AS 'Account/Premise/@PremiseStreet1'
                        , [service_city] AS 'Account/Premise/@PremiseCity'
                        , CASE WHEN LEN(LTRIM(RTRIM([service_zip_code]))) < 5 THEN NULL
                               ELSE CAST(SUBSTRING([service_zip_code], 1, 5) AS VARCHAR(5))
                          END AS 'Account/Premise/@PremisePostalCode'
                        , CAST(LTRIM(RTRIM([service_state])) AS VARCHAR(2)) AS 'Account/Premise/@PremiseState'
                        , 'US' AS 'Account/Premise/@PremiseCountry'
                        , 'true' AS 'Account/Premise/@HasElectricService'
                        , 'true' AS 'Account/Premise/@HasGasService'
                        , 'true' AS 'Account/Premise/@HasWaterService'
                   FROM   [dbo].[RawBillTemp_224]
                   WHERE  [ClientId] = @ClientId
                          AND [TrackingId] = @TrackingId
                   FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
               ) AS [XmlData]
             , 'Customer' AS [XmlType]
        UNION
        SELECT (
                   SELECT [customer_id] AS '@CustomerId'
                        , 'customer' AS '@Source'
                        , [account_id] AS 'Account/@AccountId'
                        , [billperiod_type] AS 'Account/Bill/@BillPeriodType'
                        , [read_cycle] AS 'Account/Bill/@BillCycleScheduleId'
                        , CONVERT(VARCHAR, CAST([service_read_startdate] AS DATETIME), 126) AS 'Account/Bill/@StartDate'
                        , CONVERT(VARCHAR, CAST([service_read_date] AS DATETIME), 126) AS 'Account/Bill/@EndDate'

                        /** Bill CostDetails Element **/
                        /** child_first_name|child_last_name (for ESPM)  **/
                        , CASE WHEN LEN(LTRIM(RTRIM([child_first_name]))) > 0 THEN
                                   CAST(LTRIM(RTRIM([child_first_name])) AS VARCHAR(255))
                          END AS 'Account/Bill/BillCostDetails/@ChildFirstName'
                        , CASE WHEN LEN(LTRIM(RTRIM([child_last_name]))) > 0 THEN
                                   CAST(LTRIM(RTRIM([child_last_name])) AS VARCHAR(255))
                          END AS 'Account/Bill/BillCostDetails/@ChildLastName'
                        , [premise_id] AS 'Account/Bill/Premise/@PremiseId'
                        , CASE WHEN ISNUMERIC([usage_value]) = 1 THEN CAST([usage_value] AS DECIMAL(18, 2))
                               ELSE 0
                          END AS 'Account/Bill/Premise/Service/@TotalUsage'
                        , CASE WHEN ISNUMERIC(REPLACE([usage_charge], ',', '')) = 1 THEN
                                   CAST(REPLACE([usage_charge], ',', '') AS DECIMAL(18, 2))
                               ELSE 0
                          END AS 'Account/Bill/Premise/Service/@TotalCost'
                        , [service_commodity] AS 'Account/Bill/Premise/Service/@Commodity'
                        , CASE WHEN [meter_units] = '1' THEN '3'
                               WHEN [meter_units] = '2' THEN '2'
                               WHEN [meter_units] = '3' THEN '1'
                               WHEN [meter_units] = '6' THEN '4'
                               WHEN [meter_units] = '4' THEN '5'
                               WHEN [meter_units] = '5' THEN '6'
                               WHEN [meter_units] = '7' THEN '7'
                               WHEN [meter_units] = '8' THEN '8'
                               WHEN [meter_units] = '9' THEN '9'
                               WHEN [meter_units] = '10' THEN '10'
                               WHEN [meter_units] = '11' THEN '11'
                               WHEN [meter_units] = '12' THEN '12'
                               WHEN [meter_units] = '13' THEN '13'
                               WHEN [meter_units] = '99' THEN '99'
                               ELSE '-1'
                          END AS 'Account/Bill/Premise/Service/@UOM'
                        , IIF(LEN([active_date]) = 0, NULL, CONVERT(VARCHAR, CAST([active_date] AS DATETIME), 126)) AS 'Account/Bill/Premise/Service/@AMIStartDate'
                        , IIF(LEN([inactive_date]) = 0, NULL, CONVERT(VARCHAR, CAST([inactive_date] AS DATETIME), 126)) AS 'Account/Bill/Premise/Service/@AMIEndDate'
                        , IIF(LEN([service_read_date]) = 0
                            , NULL
                            , CONVERT(VARCHAR, CAST([service_read_date] AS DATETIME), 126)) AS 'Account/Bill/Premise/Service/@ReadDate'
                        , [rate_code] AS 'Account/Bill/Premise/Service/@RateClass'
                        , [meter_type] AS 'Account/Bill/Premise/Service/@MeterType'
                        , [meter_id] AS 'Account/Bill/Premise/Service/@MeterId'
                        , [meter_replaces_meterid] AS 'Account/Bill/Premise/Service/@ReplacedMeterId'
                        , CASE WHEN [is_estimate] = '0' THEN 'Actual'
                               WHEN [is_estimate] = '1' THEN 'Estimated'
                          END AS 'Account/Bill/Premise/Service/@ReadQuality'
                        /** Service CostDetails Element **/
                        /** |kwh_onpeak|kwh_offpeak|kwh_shoulder|kw_onpeak|kw_offpeak|kw_shoulder|  (for ESPM)  **/
                        , CASE WHEN ISNUMERIC([kwh_onpeak]) = 1 THEN CAST([kwh_onpeak] AS DECIMAL(18, 2))
                          END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@KwhOnpeak'
                        , CASE WHEN ISNUMERIC([kwh_offpeak]) = 1 THEN CAST([kwh_offpeak] AS DECIMAL(18, 2))
                          END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@KwhOffpeak'
                        , CASE WHEN ISNUMERIC([kwh_shoulder]) = 1 THEN CAST([kwh_shoulder] AS DECIMAL(18, 2))
                          END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@KwhShoulder'
                        , CASE WHEN ISNUMERIC([kw_onpeak]) = 1 THEN CAST([kw_onpeak] AS DECIMAL(18, 2))
                          END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@KwOnpeak'
                        , CASE WHEN ISNUMERIC([kw_offpeak]) = 1 THEN CAST([kw_offpeak] AS DECIMAL(18, 2))
                          END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@KwOffpeak'
                        , CASE WHEN ISNUMERIC([kw_shoulder]) = 1 THEN CAST([kw_shoulder] AS DECIMAL(18, 2))
                          END AS 'Account/Bill/Premise/Service/ServiceCostDetails/@KwShoulder'
                   FROM   [dbo].[RawBillTemp_224]
                   WHERE  [ClientId] = @ClientId
                          AND [TrackingId] = @TrackingId
                   FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
               ) AS [XmlData]
             , 'Bill' AS [XmlType]
        UNION
        SELECT (
                   SELECT [customer_id] AS '@CustomerId'
                        , [customer_id] AS 'Account/@AccountId'
                        , [premise_id] AS 'Account/Premise/@PremiseId'
                        , (
                              SELECT 'premisetype' AS 'ProfileItem/@AttributeKey'
                                   , [PremiseType] AS 'ProfileItem/@AttributeValue'
                                   , 'utility' AS 'ProfileItem/@Source'
                                   , GETDATE() AS 'ProfileItem/@ModifiedDate'
                              FOR XML PATH(''), TYPE, ELEMENTS
                          ) AS 'Account/Premise'
                        , IIF([Attribute1] IS NULL
                            , NULL
                            , (
                                  SELECT [Attribute1] AS 'ProfileItem/@AttributeKey'
                                       , [Attribute1] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                       , 'utility' AS 'ProfileItem/@Source'
                                       , GETDATE() AS 'ProfileItem/@ModifiedDate'
                                  FOR XML PATH(''), TYPE, ELEMENTS
                              )) AS 'Account/Premise'
                        , IIF([Attribute2] IS NULL
                            , NULL
                            , (
                                  SELECT [Attribute2] AS 'ProfileItem/@AttributeKey'
                                       , [Attribute2] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                       , 'utility' AS 'ProfileItem/@Source'
                                       , GETDATE() AS 'ProfileItem/@ModifiedDate'
                                  FOR XML PATH(''), TYPE, ELEMENTS
                              )) AS 'Account/Premise'
                        , IIF([Attribute3] IS NULL
                            , NULL
                            , (
                                  SELECT [Attribute3] AS 'ProfileItem/@AttributeKey'
                                       , [Attribute3] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                       , 'utility' AS 'ProfileItem/@Source'
                                       , GETDATE() AS 'ProfileItem/@ModifiedDate'
                                  FOR XML PATH(''), TYPE, ELEMENTS
                              )) AS 'Account/Premise'
                        , IIF([Attribute4] IS NULL
                            , NULL
                            , (
                                  SELECT [Attribute4] AS 'ProfileItem/@AttributeKey'
                                       , [Attribute4] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                       , 'utility' AS 'ProfileItem/@Source'
                                       , GETDATE() AS 'ProfileItem/@ModifiedDate'
                                  FOR XML PATH(''), TYPE, ELEMENTS
                              )) AS 'Account/Premise'
                        , IIF([Attribute5] IS NULL
                            , NULL
                            , (
                                  SELECT [Attribute5] AS 'ProfileItem/@AttributeKey'
                                       , [Attribute5] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                       , 'utility' AS 'ProfileItem/@Source'
                                       , GETDATE() AS 'ProfileItem/@ModifiedDate'
                                  FOR XML PATH(''), TYPE, ELEMENTS
                              )) AS 'Account/Premise'
                        , IIF([Attribute6] IS NULL
                            , NULL
                            , (
                                  SELECT [Attribute6] AS 'ProfileItem/@AttributeKey'
                                       , [Attribute6] + '.enrolled' AS 'ProfileItem/@AttributeValue'
                                       , 'utility' AS 'ProfileItem/@Source'
                                       , GETDATE() AS 'ProfileItem/@ModifiedDate'
                                  FOR XML PATH(''), TYPE, ELEMENTS
                              )) AS 'Account/Premise'
                   FROM   (
                              SELECT [customer_id]
                                   , [premise_id]
                                   , CASE WHEN [customer_type] = 'Commercial' THEN 'premisetype.business'
                                          ELSE 'premisetype.residential'
                                     END AS [PremiseType]
                                   , [pm1].[PremiseAttributeKey] AS [Attribute1]
                                   , [pm2].[PremiseAttributeKey] AS [Attribute2]
                                   , [pm3].[PremiseAttributeKey] AS [Attribute3]
                                   , [pm4].[PremiseAttributeKey] AS [Attribute4]
                                   , [pm5].[PremiseAttributeKey] AS [Attribute5]
                                   , [pm6].[PremiseAttributeKey] AS [Attribute6]
                              FROM   [dbo].[RawBillTemp_224]
                                     CROSS APPLY (SELECT [str] = [Programs] + ',,,,,,') [f1]
                                     CROSS APPLY (SELECT [p1] = CHARINDEX(',', [str])) [ap1]
                                     CROSS APPLY (SELECT [p2] = CHARINDEX(',', [str], [p1] + 1)) [ap2]
                                     CROSS APPLY (SELECT [p3] = CHARINDEX(',', [str], [p2] + 1)) [ap3]
                                     CROSS APPLY (SELECT [p4] = CHARINDEX(',', [str], [p3] + 1)) [ap4]
                                     CROSS APPLY (SELECT [p5] = CHARINDEX(',', [str], [p4] + 1)) [ap5]
                                     CROSS APPLY (SELECT [p6] = CHARINDEX(',', [str], [p5] + 1)) [ap6]
                                     CROSS APPLY (
                                                     SELECT [Program1] = SUBSTRING([str], 1, [p1] - 1)
                                                          , [Program2] = SUBSTRING([str], [p1] + 1, [p2] - [p1] - 1)
                                                          , [Program3] = SUBSTRING([str], [p2] + 1, [p3] - [p2] - 1)
                                                          , [Program4] = SUBSTRING([str], [p3] + 1, [p4] - [p3] - 1)
                                                          , [Program5] = SUBSTRING([str], [p4] + 1, [p5] - [p4] - 1)
                                                          , [Program6] = SUBSTRING([str], [p5] + 1, [p6] - [p5] - 1)
                                                 ) [Programs]
                                     LEFT JOIN @ProgramMapping [pm1] ON [pm1].[ProgramName] = [Program1]
                                     LEFT JOIN @ProgramMapping [pm2] ON [pm2].[ProgramName] = [Program2]
                                     LEFT JOIN @ProgramMapping [pm3] ON [pm3].[ProgramName] = [Program3]
                                     LEFT JOIN @ProgramMapping [pm4] ON [pm4].[ProgramName] = [Program4]
                                     LEFT JOIN @ProgramMapping [pm5] ON [pm5].[ProgramName] = [Program5]
                                     LEFT JOIN @ProgramMapping [pm6] ON [pm6].[ProgramName] = [Program6]
                              WHERE  [ClientId] = @ClientId
                                     AND [TrackingId] = @TrackingId
                          ) [s]
                   FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
               ) AS [XmlData]
             , 'Profile' AS [XmlType];

    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;

END;
GO
