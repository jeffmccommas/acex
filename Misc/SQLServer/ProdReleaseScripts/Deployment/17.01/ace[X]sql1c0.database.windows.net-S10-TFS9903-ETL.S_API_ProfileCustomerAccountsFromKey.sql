
/*********************************************/
/***** RUN ONLY ON THE INSIGHTS DATABASE ****/
/*********************************************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_API_ProfileCustomerAccountsFromKey]'))
BEGIN

        EXEC(
                '-- =============================================
                -- Author:      Philip Victor
                -- Create date: 12/05/2016
                -- Description: Retrieve the customers associated with a bill update for the API
                -- =============================================

                CREATE PROCEDURE [ETL].[S_API_ProfileCustomerAccountsFromKey]
                                        @ClientID INT
                        AS

                        BEGIN

                                SET NOCOUNT ON;

                                SELECT  DISTINCT
                                        pca.[ClientID],
                                        pca.[CustomerID],
                                        pca.[AccountID],
                                        pca.[EmailAddress],
                                        pca.[PhoneNumber],
                                        pca.[UpdateFromUIDate],
                                        pca.[UpdateFromETLDate]
                                FROM [wh].[ProfileCustomerAccount] pca WITH (NOLOCK)
                                    INNER JOIN [ETL].[KEY_CustomerProfileAccounts_API] tky WITH (NOLOCK)
                                    ON pca.[ClientID] = tky.[ClientID]
                                    AND pca.[CustomerID] = tky.[CustomerID]
                                    AND  pca.[AccountID] = tky.[AccountID]
                                WHERE   pca.[ClientID] = @ClientID
                                ORDER BY [CustomerID]

                        END;'
   )

END
GO