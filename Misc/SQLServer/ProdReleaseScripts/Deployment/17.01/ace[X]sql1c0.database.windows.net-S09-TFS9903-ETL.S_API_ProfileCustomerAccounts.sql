
/***************************************************/
/********* RUN ONLY ON THE INSIGHTS DATABASE ******/
/***************************************************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects  WHERE object_id = OBJECT_ID(N'[ETL].[S_API_ProfileCustomerAccounts]'))
BEGIN

        EXEC(
                '-- =============================================
                -- Author: Phil Victor
                -- Create date: 11/28/2016
                -- Description: Script to select distinct customers for ETL sync
                -- Based off the Update dates we can determine which customers to choose.
                -- =============================================
                CREATE PROCEDURE [ETL].[S_API_ProfileCustomerAccounts]
                                        @ClientID INT
                        AS

                        BEGIN

                                SET NOCOUNT ON;

                                SELECT  DISTINCT
                                        [ClientID],
                                        [CustomerID],
                                        [AccountID],
                                        [EmailAddress],
                                        [PhoneNumber]
                                FROM [wh].[ProfileCustomerAccount] WITH (NOLOCK)
                                WHERE [ClientID] = @ClientID AND
                                [UpdateFromUIDate] >= [UpdateFromETLDate] AND
                                ([EmailAddress] IS NOT NULL OR [PhoneNumber] IS NOT NULL)
                                ORDER BY [CustomerID]

                        END;'

   )

END
GO