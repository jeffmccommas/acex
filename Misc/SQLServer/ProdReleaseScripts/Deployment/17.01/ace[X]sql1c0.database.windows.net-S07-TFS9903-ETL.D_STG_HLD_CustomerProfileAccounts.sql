﻿
/*********************************************/
/***** RUN ONLY ON THE INSIGHTS DATABASE ****/
/*********************************************/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects  WHERE object_id = OBJECT_ID(N'[ETL].[D_STG_HLD_CustomerProfileAccounts]'))
BEGIN

        EXEC(
                '-- =============================================
                -- Author: Philip Victor
                -- Create date: 12/7/2016
                -- Description: Empty the CustomerProfileAccount update table.
                -- =============================================
                CREATE PROCEDURE [ETL].[D_STG_HLD_CustomerProfileAccounts]
                AS

                BEGIN

                    SET NOCOUNT ON;

                    TRUNCATE TABLE [ETL].[U_ProfileCustomerAccounts_API]

                END;'

   )

END
GO