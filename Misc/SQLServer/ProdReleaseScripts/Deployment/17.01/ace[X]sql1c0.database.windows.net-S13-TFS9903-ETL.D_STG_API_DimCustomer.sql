﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[D_STG_API_DimCustomer]'))
BEGIN

        EXEC(
                '-- =============================================
                    -- Author: Philip Victor
                    -- Create date: 12/12/2016
                    -- Description:
                    -- =============================================
                    CREATE PROCEDURE [ETL].[D_STG_API_DimCustomer]
                    AS

                    BEGIN

                        SET NOCOUNT ON;

                        TRUNCATE TABLE [ETL].[T1U_API_DimCustomer]

                END;'
   )

END
GO


