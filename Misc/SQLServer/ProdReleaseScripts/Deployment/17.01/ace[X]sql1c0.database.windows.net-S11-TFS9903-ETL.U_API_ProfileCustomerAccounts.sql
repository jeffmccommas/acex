
/*********************************************/
/***** RUN ONLY ON THE INSIGHTS DATABASE ****/
/*********************************************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF NOT EXISTS (SELECT  *  FROM    sys.objects  WHERE   object_id = OBJECT_ID(N'[ETL].[U_API_ProfileCustomerAccounts]'))
BEGIN

        EXEC(
                                '-- =============================================
                                --- Author: Philip Victor
                                --- Create date: 12/9/2016
                                --- Description:    Update the ProfileCustomerAccounts with new data from the ETL.
                                -- =============================================
                                CREATE PROCEDURE [ETL].[U_API_ProfileCustomerAccounts]
                                    @ClientID INT
                                AS
                                    BEGIN

                                        SET NOCOUNT ON;

                                        UPDATE [wh].[ProfileCustomerAccount]
                                        SET  [PhoneNumber]= upca.[PhoneNumber] ,
                                                 [EmailAddress] = upca.[EmailAddress],
                                                 [UpdateFromETLDate] = GETUTCDATE()
                                        FROM [wh].[ProfileCustomerAccount] pca WITH (NOLOCK)
                                                INNER JOIN [ETL].[U_ProfileCustomerAccounts_API] upca  WITH (NOLOCK)
                                                ON upca.[ClientID] = pca.[ClientID]
                                                AND upca.[CustomerId] = pca.[CustomerID]
                                                AND upca.[AccountID] = pca.[AccountID]
                                        WHERE   pca.[ClientID] = @ClientID


                                    END;'
            )

END
GO
