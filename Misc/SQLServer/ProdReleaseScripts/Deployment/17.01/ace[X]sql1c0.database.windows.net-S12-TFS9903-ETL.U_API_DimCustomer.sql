﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[U_API_DimCustomer]'))
BEGIN

        EXEC(
                '-- =============================================
                -- Author: Philip Victor
                -- Create date: 12/7/2016
                -- Description: Update the DimCustomer table with data from the API
                -- =============================================
                CREATE PROCEDURE [ETL].[U_API_DimCustomer]
                                @ClientID INT
                AS

                BEGIN

                    SET NOCOUNT ON;


                    UPDATE [dbo].[DimCustomer]
                    SET PhoneNumber = c.[PhoneNumber],
                        EmailAddress = c.[EmailAddress],
                        UpdateDate = GETUTCDATE()
                    FROM [dbo].[DimCustomer] cd WITH (NOLOCK)
                        INNER JOIN [ETL].[T1U_API_DimCustomer] c WITH (NOLOCK)
                        ON cd.[ClientId] = c.[ClientId]
                        AND cd.[CustomerId] = c.[CustomerId]
                    WHERE cd.[ClientID] = @ClientID

                END;'
   )

END
GO


