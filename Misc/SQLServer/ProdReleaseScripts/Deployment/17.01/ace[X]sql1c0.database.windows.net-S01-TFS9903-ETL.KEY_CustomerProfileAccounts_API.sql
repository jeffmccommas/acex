﻿

/*******************************************/
/***** RUN ONLY ON THE INSIGHTS DATABASE ***/
/*******************************************/

SET ANSI_NULLS ON
GO

SET ANSI_PADDING ON
GO

-- =============================================
-- Author: Phil Victor
-- Create date: 12/06/2016
-- =============================================

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
           WHERE TABLE_NAME = N'KEY_CustomerProfileAccounts_API')
BEGIN
        CREATE TABLE [ETL].[KEY_CustomerProfileAccounts_API](
            [ClientID] [INT] NOT NULL,
            [CustomerId] [NVARCHAR](50) NOT NULL,
            [AccountID] [NVARCHAR](50) NOT NULL,
            [EmailAddress] [NVARCHAR](255) NOT NULL,
            [PhoneNumber] [NVARCHAR](15) NOT NULL,
            [CreateDate] [DATETIME] NULL,
            [UpdateDate] [DATETIME] NULL
        )

END

GO