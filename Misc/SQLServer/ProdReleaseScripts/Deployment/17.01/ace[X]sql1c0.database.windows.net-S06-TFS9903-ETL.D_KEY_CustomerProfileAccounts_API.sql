
/*********************************************/
/***** RUN ONLY ON THE INSIGHTS DATABASE ****/
/*********************************************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[D_KEY_CustomerProfileAccounts_API]'))
BEGIN

        EXEC(
            '-- =============================================
            -- Author:  Philip Victor
            -- Create date: 12/7/2016
            -- Description: Empty the CustomerProfileAccount Key table
            -- =============================================
            CREATE PROCEDURE [ETL].[D_KEY_CustomerProfileAccounts_API]
            AS

            BEGIN

                SET NOCOUNT ON;

                TRUNCATE TABLE [ETL].[KEY_CustomerProfileAccounts_API]

            END;'

   )

END
GO

