
/*******************************************/
/***** RUN ONLY ON THE INSIGHTS DATABASE ***/
/*******************************************/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
           WHERE TABLE_NAME = N'U_ProfileCustomerAccounts_API')
BEGIN
        CREATE TABLE [ETL].[U_ProfileCustomerAccounts_API](
            [ClientID] [INT] NOT NULL,
            [CustomerId] [NVARCHAR](50) NOT NULL,
            [AccountID] [NVARCHAR](50) NOT NULL,
            [EmailAddress] [NVARCHAR](255) NOT NULL,
            [PhoneNumber] [NVARCHAR](15) NOT   NULL
        )
END

GO

SET ANSI_PADDING OFF
GO

