﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_210 DATABASE SHARD ***/
/********************************************************/

SET ANSI_NULLS ON
GO

SET ANSI_PADDING ON
GO

-- =============================================
-- Author: Phil Victor
-- Create date: 12/22/2016
-- =============================================

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
           WHERE TABLE_NAME = N'DimActionKeyType')
BEGIN
    CREATE TABLE [dbo].[DimActionKeyType](
        [ActionKeyTypeKey] [varchar](50) NOT NULL,
        [ActionKeyTypeDesc] [varchar](50) NOT NULL,
        [ActionKeyTypeId] [int] NOT NULL,
     CONSTRAINT [PK_DimActionKeyType] PRIMARY KEY CLUSTERED
    (
        [ActionKeyTypeKey] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
    )

    INSERT [dbo].[DimActionKeyType] ([ActionKeyTypeKey], [ActionKeyTypeDesc], [ActionKeyTypeId]) VALUES (N'programname', N'programname', 1)

    INSERT [dbo].[DimActionKeyType] ([ActionKeyTypeKey], [ActionKeyTypeDesc], [ActionKeyTypeId]) VALUES (N'measurename', N'measurename', 2)

    INSERT [dbo].[DimActionKeyType] ([ActionKeyTypeKey], [ActionKeyTypeDesc], [ActionKeyTypeId]) VALUES (N'incentivetype', N'incentivetype', 3)

    INSERT [dbo].[DimActionKeyType] ([ActionKeyTypeKey], [ActionKeyTypeDesc], [ActionKeyTypeId]) VALUES (N'incentiveamount', N'incentiveamount', 4)

    INSERT [dbo].[DimActionKeyType] ([ActionKeyTypeKey], [ActionKeyTypeDesc], [ActionKeyTypeId]) VALUES (N'upfrontcost', N'upfrontcost', 5)

END

SET ANSI_PADDING OFF
GO