
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_API_BillingCustomers]'))
BEGIN

        EXEC(
                '-- =============================================
                -- Author:      Philip Victor
                -- Create date: 12/05/2016
                -- Description: Retrieve the customers associated with a bill update for the API
                -- =============================================
                CREATE PROCEDURE [ETL].[S_API_BillingCustomers]
                                 @ClientID INT
                AS
                BEGIN

                    SELECT  kfb.[ClientId],
                                    kfb.[CustomerId],
                                    kfb.[AccountId],
                                    dc.[PhoneNumber],
                                    dc.[EmailAddress],
                                    dc.[CreateDate],
                                    dc.[UpdateDate]
                    FROM [ETL].[KEY_FactBilling] kfb WITH (NOLOCK)
                    INNER JOIN [dbo].[DimClient] dcl WITH (NOLOCK) ON dcl.[ClientId] = kfb.[ClientId]
                    INNER JOIN [dbo].[DimSource] ds WITH (NOLOCK) ON ds.[SourceId] = kfb.[SourceId]
                    LEFT JOIN [dbo].[DimCustomer] dc WITH (NOLOCK) ON dc.[CustomerId] = kfb.[CustomerId]
                                                    AND dc.[ClientId] = kfb.[ClientId]
                    WHERE kfb.[ClientId] = @ClientID
                    AND dc.[CustomerId] IS NOT NULL
                    AND (dc.[PhoneNumber] IS NOT NULL OR dc.[EmailAddress] IS NOT NULL)
                END;'
   )

END
GO
