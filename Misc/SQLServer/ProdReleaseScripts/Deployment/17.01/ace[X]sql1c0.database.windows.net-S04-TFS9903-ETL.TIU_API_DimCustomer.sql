﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
           WHERE TABLE_NAME = N'T1U_API_DimCustomer')
BEGIN

        CREATE TABLE [ETL].[T1U_API_DimCustomer](
            [ClientId] [INT] NOT NULL,
            [CustomerId] [VARCHAR](50) NOT NULL,
            [PhoneNumber] [VARCHAR](15) NULL,
            [EmailAddress] [VARCHAR](255) NULL
         CONSTRAINT [PK_ETL_T1U_API_DimCustomer] PRIMARY KEY CLUSTERED
        (
            [ClientId] ASC,
            [CustomerId] ASC
        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
        )

END

GO

SET ANSI_PADDING OFF
GO


