﻿USE [msdb]
GO

/****** Object:  Job [PaaS_ETL_210]    Script Date: 7/10/2017 1:59:45 PM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'4e5fe555-53c8-4543-a98b-63988a21202e', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ETL_210]    Script Date: 7/10/2017 1:59:45 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [CE_UserApp]    Script Date: 7/10/2017 1:59:45 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'CE_UserApp' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'CE_UserApp'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'PaaS_ETL_210',
        @enabled=1,
        @notify_level_eventlog=0,
        @notify_level_email=2,
        @notify_level_netsend=0,
        @notify_level_page=0,
        @delete_level=0,
        @description=N'No description available.',
        @category_name=N'CE_UserApp',
        @owner_login_name=N'sa',
        @notify_email_operator_name=N'ACE BI UAT', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [ExecutePackage]    Script Date: 7/10/2017 1:59:45 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'ExecutePackage',
        @step_id=1,
        @cmdexec_success_code=0,
        @on_success_action=1,
        @on_success_step_id=0,
        @on_fail_action=2,
        @on_fail_step_id=0,
        @retry_attempts=0,
        @retry_interval=0,
        @os_run_priority=0, @subsystem=N'SSIS',
        @command=N'/ISSERVER "\"\SSISDB\BI\InsightsDW\Load_Warehouse.dtsx\"" /SERVER "\"azuUsql001\bi1\"" /Par "\"$Project::ClientID(Int16)\"";210 /Par "\"$Project::DatabaseServerNameBI\"";"\"aceUsql1c0.database.windows.Net\"" /Par "\"$Project::CM.DisAggDW.ConnectionString\"";"\"Data Source=aceUsql1c0.database.windows.Net;Initial Catalog=insightsDW_210;Integrated Security=False;User ID=AclaraCEAdmin@aceUsql1c0;Password=Acl@r@282;\"" /Par "\"$Project::CM.DisAggDW.InitialCatalog\"";"\"insightsDW_210\"" /Par "\"$Project::CM.DisAggDW.Password\"";"\"Acl@r@282\"" /Par "\"$Project::CM.DisAggDW.UserName\"";"\"AclaraCEAdmin@aceUsql1c0\"" /Par "\"$Project::CM.InsightsDW.ConnectionString\"";"\"Data Source=aceUsql1c0.database.windows.Net;Initial Catalog=InsightsDW_210;Integrated Security=False;\"" /Par "\"$Project::CM.InsightsDW.InitialCatalog\"";"\"InsightsDW_210\"" /Par "\"$Project::CM.InsightsDW.Password\"";"\"Acl@r@282\"" /Par "\"$Project::CM.Metadata.ConnectionString\"";"\"Data Source=aceUsql1c0.database.windows.Net;Initial Catalog=InsightsMetaData;User ID=AclaraCEAdmin@aceUsql1c0;Password=Acl@r@282;Encrypt=true;Trusted_Connection=false;Provider=SQLOLEDB.1;\"" /Par "\"$Project::CM.Metadata.Password\"";"\"Acl@r@282\"" /Par "\"$Project::CM.Metadata.UserName\"";"\"AclaraCEAdmin@aceUsql1c0\"" /Par "\"$Project::CM.SourceData.ConnectionString\"";"\"Data Source=aceUsql1c0.database.windows.Net;Initial Catalog=InsightsDW_210;User ID=AclaraCEAdmin@aceUsql1c0;Password=Acl@r@282;Persist Security Info=True;Provider=SQLOLEDB.1;\"" /Par "\"$Project::CM.SourceData.InitialCatalog\"";"\"InsightsDW_210\"" /Par "\"$Project::CM.SourceData.Password\"";"\"Acl@r@282\"" /Par "\"$Project::CM.SourceData.UserName\"";"\"AclaraCEAdmin@aceUsql1c0\"" /Par "\"$Project::CM.Warehouse.ConnectionString\"";"\"Data Source=aceUsql1c0.database.windows.Net;Initial Catalog=InsightsDW_210;User ID=AclaraCEAdmin@aceUsql1c0;Password=Acl@r@282;Encrypt=true;Trusted_Connection=false;Provider=SQLOLEDB.1;\"" /Par "\"$Project::CM.Warehouse.InitialCatalog\"";"\"insightsDW_210\"" /Par "\"$Project::CM.Warehouse.Password\"";"\"Acl@r@282\"" /Par "\"$Project::CM.Warehouse.UserName\"";"\"AclaraCEAdmin@aceUsql1c0\"" /Par "\"$ServerOption::LOGGING_LEVEL(Int16)\"";2 /Par "\"$ServerOption::SYNCHRONIZED(Boolean)\"";True /CALLERINFO SQLAGENT /REPORTING E',
        @database_name=N'master',
        @flags=0,
        @proxy_name=N'SSISProxy'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily 9 pm',
        @enabled=1,
        @freq_type=4,
        @freq_interval=1,
        @freq_subday_type=1,
        @freq_subday_interval=0,
        @freq_relative_interval=0,
        @freq_recurrence_factor=0,
        @active_start_date=20141107,
        @active_end_date=99991231,
        @active_start_time=224000,
        @active_end_time=235959,
        @schedule_uid=N'2a6abff1-29fd-4bd1-8600-4cd1cf242568'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


