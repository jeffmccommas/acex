/*
Run this script on:

aceUsql1c0.database.windows.net.Insights    -  This database will be modified

to synchronize it with:

aceQsql1c0.database.windows.net.Insights

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 11.1.3 from Red Gate Software Ltd at 11/19/2015 10:59:30 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

PRINT(N'Drop constraints from [dbo].[UserRole]')
ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK_UserRole_Role]
ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK_UserRole_User]

PRINT(N'Drop constraints from [dbo].[UserEndpoint]')
ALTER TABLE [dbo].[UserEndpoint] DROP CONSTRAINT [FK_UserEndpoint_Endpoint]
ALTER TABLE [dbo].[UserEndpoint] DROP CONSTRAINT [FK_UserEndpoint_User]

PRINT(N'Drop constraints from [dbo].[User]')
ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_User_Client]

PRINT(N'Drop constraint FK_UserEnvironment_User from [dbo].[UserEnvironment]')
ALTER TABLE [dbo].[UserEnvironment] DROP CONSTRAINT [FK_UserEnvironment_User]

PRINT(N'Drop constraints from [cp].[ControlPanelLoginClient]')
ALTER TABLE [cp].[ControlPanelLoginClient] DROP CONSTRAINT [FK_ControlPanelLoginClient_ControlPanelLogin]

PRINT(N'Drop constraint FK_ClientProperty_Client from [dbo].[ClientProperty]')
ALTER TABLE [dbo].[ClientProperty] DROP CONSTRAINT [FK_ClientProperty_Client]

PRINT(N'Add row to [dbo].[Client]')
INSERT INTO [dbo].[Client] ([ClientID], [Name], [Description], [RateCompanyID], [ReferrerID], [NewDate], [UpdDate], [AuthType], [EnableInd]) VALUES (61, 'LADWP', 'LADWP', 377, NULL, '2015-11-16 19:05:55.497', '2015-11-16 19:05:55.497', 1, 1)

PRINT(N'Add row to [cp].[ControlPanelLoginClient]')
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (19, 61)

PRINT(N'Add row to [dbo].[User]')
SET IDENTITY_INSERT [dbo].[User] ON
INSERT INTO [dbo].[User] ([UserID], [ClientID], [ActorName], [CEAccessKeyID], [NewDate], [UpdDate], [EnableInd]) VALUES (30, 61, 'LADWP', 'c7dad048cbb35e4de6d971d6f2e1961af3cd', '2015-11-18 14:21:04.383', '2015-11-18 14:21:04.383', 1)
SET IDENTITY_INSERT [dbo].[User] OFF

PRINT(N'Add rows to [dbo].[UserEndpoint]')
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (30, 0, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (30, 1, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (30, 2, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (30, 3, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (30, 4, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (30, 5, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (30, 6, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (30, 7, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (30, 8, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (30, 9, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (30, 10, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (30, 11, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (30, 12, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (30, 13, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (30, 14, 1)
PRINT(N'Operation applied to 15 rows out of 15')

PRINT(N'Add row to [dbo].[UserRole]')
INSERT INTO [dbo].[UserRole] ([UserID], [RoleID], [EnableInd]) VALUES (30, 2, 1)

PRINT(N'Add constraints to [dbo].[UserRole]')
ALTER TABLE [dbo].[UserRole] ADD CONSTRAINT [FK_UserRole_Role] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role] ([RoleID])
ALTER TABLE [dbo].[UserRole] WITH NOCHECK ADD CONSTRAINT [FK_UserRole_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])

PRINT(N'Add constraints to [dbo].[UserEndpoint]')
ALTER TABLE [dbo].[UserEndpoint] ADD CONSTRAINT [FK_UserEndpoint_Endpoint] FOREIGN KEY ([EndpointID]) REFERENCES [dbo].[Endpoint] ([EndpointID])
ALTER TABLE [dbo].[UserEndpoint] ADD CONSTRAINT [FK_UserEndpoint_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])

PRINT(N'Add constraints to [dbo].[User]')
ALTER TABLE [dbo].[User] ADD CONSTRAINT [FK_User_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])

PRINT(N'Add constraint FK_UserEnvironment_User to [dbo].[UserEnvironment]')
ALTER TABLE [dbo].[UserEnvironment] WITH NOCHECK ADD CONSTRAINT [FK_UserEnvironment_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])

PRINT(N'Add constraints to [cp].[ControlPanelLoginClient]')
ALTER TABLE [cp].[ControlPanelLoginClient] ADD CONSTRAINT [FK_ControlPanelLoginClient_ControlPanelLogin] FOREIGN KEY ([CpID]) REFERENCES [cp].[ControlPanelLogin] ([CpID])

PRINT(N'Add constraint FK_ClientProperty_Client to [dbo].[ClientProperty]')
ALTER TABLE [dbo].[ClientProperty] WITH NOCHECK ADD CONSTRAINT [FK_ClientProperty_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
COMMIT TRANSACTION
GO
