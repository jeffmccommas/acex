USE [Insights]
GO

/****** Object:  StoredProcedure [wh].[uspMEGetMeasures]    Script Date: 11/2/2015 11:52:01 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- GetMeasures
ALTER PROCEDURE [wh].[uspMEGetMeasures] ( 
	@ClientId INT, 
	@CustomerId VARCHAR(50), 
	@AccountId VARCHAR(50) = NULL, 
	@PremiseId VARCHAR(50) = NULL
	) AS
BEGIN		
	SET NOCOUNT ON;
			
	SELECT [ClientId]
      ,[CustomerId]
      ,[AccountId]
      ,[PremiseId]
      ,[ActionKey]
      ,[ActionTypeKey]
      ,[AnnualCost]
      ,[AnnualCostVariancePercent]
      ,[AnnualSavingsEstimate]
      ,[AnnualSavingsEstimateCurrencyKey]
      ,[Roi]
      ,[Payback]
      ,[UpfrontCost]
      ,[CommodityKey]
      ,[ElecSavEst]
      ,[ElecSavEstCurrencyKey]
      ,[ElecUsgSavEst]
      ,[ElecUsgSavEstUomKey]
      ,[GasSavEst]
      ,[GasSavEstCurrencyKey]
      ,[GasUsgSavEst]
      ,[GasUsgSavEstUomKey]
      ,[WaterSavEst]
      ,[WaterSavEstCurrencyKey]
      ,[WaterUsgSavEst]
      ,[WaterUsgSavEstUomKey]
      ,[Priority]
      ,[NewDate]
	  , ISNULL([RebateAmount], 0) AS RebateAmount
  FROM [wh].[Measure]
  WHERE ClientId = @ClientID  
		  AND CustomerId = @CustomerID 
		  AND (AccountId = @AccountID OR @AccountID IS NULL) 
  		  AND (PremiseId = @PremiseID OR @PremiseID IS NULL)         
	ORDER BY CustomerId, AccountId, PremiseId, ActionKey, CommodityKey

END




GO


