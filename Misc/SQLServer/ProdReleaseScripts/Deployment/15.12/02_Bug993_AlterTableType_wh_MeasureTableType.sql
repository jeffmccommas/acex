USE [Insights]
GO


/****** Object:  StoredProcedure [wh].[uspMESaveMeasures]    Script Date: 11/2/2015 11:41:10 AM ******/
DROP PROCEDURE [wh].[uspMESaveMeasures]
GO

/****** Object:  UserDefinedTableType [wh].[MeasureTableType]    Script Date: 11/2/2015 11:38:58 AM ******/
DROP TYPE [wh].[MeasureTableType]
GO

/****** Object:  UserDefinedTableType [wh].[MeasureTableType]    Script Date: 11/2/2015 11:38:58 AM ******/
CREATE TYPE [wh].[MeasureTableType] AS TABLE(
	[ClientId] [INT] NOT NULL,
	[CustomerId] [VARCHAR](50) NOT NULL,
	[AccountId] [VARCHAR](50) NOT NULL,
	[PremiseId] [VARCHAR](50) NOT NULL,
	[ActionKey] [VARCHAR](50) NOT NULL,
	[ActionTypeKey] [VARCHAR](50) NOT NULL,
	[AnnualCost] [DECIMAL](18, 6) NOT NULL,
	[AnnualCostVariancePercent] [DECIMAL](18, 6) NOT NULL,
	[AnnualSavingsEstimate] [DECIMAL](18, 6) NOT NULL,
	[AnnualSavingsEstimateCurrencyKey] [VARCHAR](16) NOT NULL,
	[Roi] [DECIMAL](18, 6) NULL,
	[Payback] [DECIMAL](18, 6) NULL,
	[UpfrontCost] [DECIMAL](18, 6) NULL,
	[CommodityKey] [VARCHAR](50) NOT NULL,
	[ElecSavEst] [DECIMAL](18, 6) NULL,
	[ElecSavEstCurrencyKey] [VARCHAR](16) NULL,
	[ElecUsgSavEst] [DECIMAL](18, 6) NULL,
	[ElecUsgSavEstUomKey] [VARCHAR](16) NULL,
	[GasSavEst] [DECIMAL](18, 6) NULL,
	[GasSavEstCurrencyKey] [VARCHAR](16) NULL,
	[GasUsgSavEst] [DECIMAL](18, 6) NULL,
	[GasUsgSavEstUomKey] [VARCHAR](16) NULL,
	[WaterSavEst] [DECIMAL](18, 6) NULL,
	[WaterSavEstCurrencyKey] [VARCHAR](16) NULL,
	[WaterUsgSavEst] [DECIMAL](18, 6) NULL,
	[WaterUsgSavEstUomKey] [VARCHAR](16) NULL,
	[Priority] [INT] NOT NULL,
	[NewDate] [DATETIME] NOT NULL,
	[RebateAmount] [DECIMAL](18, 6) NULL
)
GO


