USE [Insights]
GO

/****** Object:  StoredProcedure [wh].[uspMESaveMeasures]    Script Date: 11/2/2015 11:41:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =================================================================================
-- Author:		MGanley
-- Create date: 7/6/2015
-- Description:	Save Measures
-- =================================================================================
CREATE PROCEDURE  [wh].[uspMESaveMeasures]
				@ClientID INT,
				@CustomerID VARCHAR(50),
				@AccountID VARCHAR(50),
				@PremiseID VARCHAR(50),
				@MeasureTable MeasureTableType READONLY
AS
BEGIN

	-- within a transaction
	BEGIN TRANSACTION;

	BEGIN TRY

		 --delete all measures (aka. actions) from the Measure table for this customer premise
		 DELETE FROM wh.Measure
			WHERE ClientId = @ClientID AND CustomerId = @CustomerID AND AccountId = @AccountID AND PremiseId = @PremiseID;

		 --insert all measures for this customer premise from the provided table value parameter
		 INSERT INTO [wh].[Measure]
           ([ClientId]
           ,[CustomerId]
           ,[AccountId]
           ,[PremiseId]
           ,[ActionKey]
           ,[ActionTypeKey]
           ,[AnnualCost]
           ,[AnnualCostVariancePercent]
           ,[AnnualSavingsEstimate]
           ,[AnnualSavingsEstimateCurrencyKey]
           ,[Roi]
           ,[Payback]
           ,[UpfrontCost]
           ,[CommodityKey]
           ,[ElecSavEst]
           ,[ElecSavEstCurrencyKey]
           ,[ElecUsgSavEst]
           ,[ElecUsgSavEstUomKey]
           ,[GasSavEst]
           ,[GasSavEstCurrencyKey]
           ,[GasUsgSavEst]
           ,[GasUsgSavEstUomKey]
           ,[WaterSavEst]
           ,[WaterSavEstCurrencyKey]
           ,[WaterUsgSavEst]
           ,[WaterUsgSavEstUomKey]
           ,[Priority]
           ,[NewDate]
		   ,[RebateAmount])
			SELECT [ClientId]
				  ,[CustomerId]
				  ,[AccountId]
				  ,[PremiseId]
				  ,[ActionKey]
				  ,[ActionTypeKey]
				  ,[AnnualCost]
				  ,[AnnualCostVariancePercent]
				  ,[AnnualSavingsEstimate]
				  ,[AnnualSavingsEstimateCurrencyKey]
				  ,[Roi]
				  ,[Payback]
				  ,[UpfrontCost]
				  ,[CommodityKey]
				  ,[ElecSavEst]
				  ,[ElecSavEstCurrencyKey]
				  ,[ElecUsgSavEst]
				  ,[ElecUsgSavEstUomKey]
				  ,[GasSavEst]
				  ,[GasSavEstCurrencyKey]
				  ,[GasUsgSavEst]
				  ,[GasUsgSavEstUomKey]
				  ,[WaterSavEst]
				  ,[WaterSavEstCurrencyKey]
				  ,[WaterUsgSavEst]
				  ,[WaterUsgSavEstUomKey]
				  ,[Priority]
				  ,[NewDate]
				  ,[RebateAmount]
			  FROM @MeasureTable

	END TRY

	BEGIN CATCH
		SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState, ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage;
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;
	END CATCH;

	IF @@TRANCOUNT > 0
		COMMIT TRANSACTION;


END






GO


