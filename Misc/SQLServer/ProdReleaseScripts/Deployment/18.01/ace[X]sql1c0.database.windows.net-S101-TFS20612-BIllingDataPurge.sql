-- ************************
-- Run on All Insights Shards including Insights_test
-- ************************
--Create stored procedures to insert data to be purged into the temp holding table dbo.archiveBillingData 
if exists (select * from sysobjects where type = 'p' and name = 'archiveBillingData')
drop procedure dbo.archiveBillingData
go

create procedure dbo.archiveBillingData @clientID int, @environmentkey varchar(20)
as

set nocount on
declare @daysToKeep int
declare @mindate date
declare @mindatekey int

	select @daysToKeep = (
		select TRY_CAST(VALUE AS INT) from cm.clientconfiguration 
		where configurationkey = 'billing.retentionperiod'
		and environmentkey = @environmentkey
		and clientid = @clientID
		)

	select @mindate = (select dateadd(dd,-1 * @daysToKeep,getdate()))
	select @mindatekey = (select datekey from dbo.dimDate where fullDateAlternateKey = @mindate)

	-- get main billing data with bill period start date <  purge policy date
	insert into dbo.archivePurgeFactServicePointBilling
	select top 10000 t1.* from dbo.FactServicePointBilling t1 join dbo.dimServiceContract t2
		on t1.ServiceContractKey = t2.ServiceContractKey
	where billperiodEndDateKey < @minDateKey 
	and not exists (select premisekey 
		from dbo.ClientDataPurgeAccountsToKeep t3
		where t2.premisekey = t3.premisekey)

	-- get bill cost details for those bills
	insert into dbo.archivePurgeFactServicePointBilling_BillCostDetails
	select t1.* from dbo.FactServicePointBilling_BillCostDetails t1 join dbo.archivePurgeFactServicePointBilling t2
		on t1.BillingCostDetailsKey = t2.BillingCostDetailsKey

	-- get the service cost details for those bills
	insert into dbo.archivePurgeFactServicePointBilling_ServiceCostDetails
	select t1.* from dbo.FactServicePointBilling_ServiceCostDetails t1 join dbo.archivePurgeFactServicePointBilling t2
		on t1.ServiceCostDetailsKey = t2.ServiceCostDetailsKey

set nocount off
go

--Create stored procedures to purge data from table dbo.purgeBillingData
if exists (select * from sysobjects where type = 'p' and name = 'purgeBillingData')
drop procedure dbo.purgeBillingData
go
create procedure dbo.purgeBillingData
as

set nocount on

	-- purge data that was just saved
	-- main billing data  ** may need to do this delete last because of FK�s on billcostdetails and servicecostdetails
	delete dbo.FactServicePointBilling
	from dbo.FactServicePointBilling t1 join dbo.archivePurgeFactServicePointBilling t2
		on t1.ServiceContractKey = t2.ServiceContractKey
		and t1.BillPeriodStartDateKey = t2.BillPeriodStartDateKey
		and t1.BillPeriodEndDateKey = t2.BillPeriodEndDateKey
		and t1.commodityid = t2.commodityid
		and t1.uomid = t2.uomid

	-- bill cost details for those bills
	delete dbo.FactServicePointBilling_BillCostDetails
	from dbo.FactServicePointBilling_BillCostDetails t1 join dbo.archivePurgeFactServicePointBilling_BillCostDetails t2
		on t1.BillingCostDetailsKey = t2.BillingCostDetailsKey
		and t1.BillingCostDetailName = t2.BillingCostDetailName
		and t1.BillingCostDetailValue = t2.BillingCostDetailValue

	-- service cost details for those bills
	delete dbo.FactServicePointBilling_ServiceCostDetails
	from dbo.FactServicePointBilling_ServiceCostDetails t1 join dbo.archivePurgeFactServicePointBilling_ServiceCostDetails t2
		on t1.ServiceCostDetailsKey = t2.ServiceCostDetailsKey
		and t1.ServiceCostDetailName = t2.ServiceCostDetailName
		and t1.ServiceCostDetailValue = t2.ServiceCostDetailValue

set nocount off
go

--Create stored procedures to cleanup holding tables when process has finished dbo.CleanupArchiveBillingDataHoldingTables
if exists (select * from sysobjects where type = 'p' and name = 'CleanupArchiveBillingDataHoldingTables')
drop procedure dbo.CleanupArchiveBillingDataHoldingTables
go
create procedure dbo.CleanupArchiveBillingDataHoldingTables
as

set nocount on
	truncate table dbo.archivePurgeFactServicePointBilling_ServiceCostDetails
	truncate table dbo.archivePurgeFactServicePointBilling_BillCostDetails
	truncate table dbo.archivePurgeFactServicePointBilling
set nocount off
go
