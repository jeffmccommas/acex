-- ************************************************************
-- ************************************************************
-- run on all InsightsDW_xxx shards, including InsightsDW_Test
-- ************************************************************
-- ************************************************************

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[getAnonymousUser]') and type in (N'P'))
drop procedure dbo.getAnonymousUser
go

create procedure dbo.getAnonymousUser  @clientID int, @EmailAddress varchar(255), @PostalCode varchar(15)
as

set nocount on
BEGIN TRY

-- 01/11/2018 add for bug 25301 - strip any hyphens or spaces from zipcode before matching to cm.emzipcode
select @PostalCode = replace(@PostalCode,'-','')
select @PostalCode = replace(@PostalCode,' ','')

-- first validate that if zipcode is passed in that it is a valid zipcode for the client
    IF (select count(*) from cm.emzipcode where zipcode = isnull(@postalcode,'') and clientid = @clientID) = 0
        BEGIN
            RAISERROR('unauthenticatedlogin.invalidpostalcode', 11, 1)
        END
	-- if it passes that validation, then check if it the email already exists or not
	ELSE
		BEGIN
			declare @EmailExists int
			select @EmailExists = (select count(*) from dbo.DimAnonymousUser where EmailAddress = @emailaddress)

			-- if the email exists and the postalcode is null this is a return user - return matched results from dimAnonymousUser
			if @EmailExists = 1 and @PostalCode is null
				select EmailAddress,PostalCode,CustomerID from dbo.DimAnonymousUser where EmailAddress = @emailaddress

			-- if the email exists and the postalcode is not null, check that the postal code passed in is the same as existing one on record
			if @EmailExists = 1 and @PostalCode is not null and (select count(*) from dbo.DimAnonymousUser where EmailAddress = @emailaddress and PostalCode = @PostalCode) = 0
				BEGIN
					RAISERROR('unauthenticatedlogin.emailexistsdup', 11, 2)
				END
			if @EmailExists = 1 and @PostalCode is not null and (select count(*) from dbo.DimAnonymousUser where EmailAddress = @emailaddress and PostalCode = @PostalCode) = 1
				BEGIN
					RAISERROR('unauthenticatedlogin.emailexistsdup', 11, 3)
					--select EmailAddress,PostalCode,CustomerID from dbo.DimAnonymousUser where EmailAddress = @emailaddress
				END
		
			-- if the email does not exist and does not have postalcode 
			if @EmailExists = 0 and @PostalCode is null
				BEGIN
					--RAISERROR('unauthenticatedlogin.invalidpostalcode', 11, 4)
					RAISERROR('unauthenticatedlogin.emailnotfound', 11, 4)
				END

			-- if the email does not exists and there is a postalcode, insert new row to table
			if @EmailExists = 0 and @PostalCode is not null
			BEGIN
				BEGIN TRY 
					-- get a new anonymous customerid = X_ + guid
					declare @newCustomerID varchar(50)
					select @newCustomerID = 'X_' + replace(convert(varchar(50),NEWID()),'-','')
	
					-- insert the user to dimAnonymousUser using @newCustomerID for customerid, accountid and premiseid
					insert into dbo.DimAnonymousUser (EmailAddress,PostalCode,CustomerID)
					select @emailaddress, @postalcode, @newCustomerID

					-- return the data back from sp
					-- select @emailaddress, @postalcode, @newCustomerID
					select @emailaddress as EmailAddress, @postalcode as PostalCode, @newCustomerID as CustomerId
				END TRY
				BEGIN CATCH
					SELECT ERROR_NUMBER() AS ErrorNumber 
				END CATCH
			END
		END
END TRY
BEGIN CATCH
   THROW; --http://msdn.microsoft.com/en-us/library/ee677615.aspx
END CATCH

set nocount off
go

