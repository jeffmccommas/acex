-- *************************************************************
-- *************************************************************
-- Run on all InsightsDW_xxx shards, including InsightsDW_Test
-- *************************************************************
-- *************************************************************

-- **********
-- new index
-- **********
if exists (select * from sysindexes where name = 'idx_dimAnonUserZipCust')
drop index idx_dimAnonUserZipCust on dbo.dimAnonymousUser 

create index idx_dimAnonUserZipCust on dbo.dimAnonymousUser (PostalCode, CustomerID)
go

-- **********
-- update sp
-- **********
If exists (select * from sysobjects where type = 'p' and name = 'GetCustomerBasic')
drop procedure dbo.GetCustomerBasic

/****** Object:  StoredProcedure [dbo].[GetCustomerBasic]    Script Date: 1/8/2018 1:26:09 PM ******/
-- 01/10/2018 - update for returning specific zipcode for anonymous users - tfs 24034
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetCustomerBasic]
    @ClientID AS INT,
    @CustomerID AS VARCHAR(50),
    @AccountID AS VARCHAR(50),
    @PremiseID AS VARCHAR(50)  
AS 
SET NOCOUNT ON

-- check to see if this is a NEW anonymous user that has not been loaded to DW yet
IF exists (select * from dbo.dimAnonymousUser where CustomerID = @customerID) 
	and not exists (select * from dimcustomer where CustomerID = @customerID)
	-- if so, get data from dimanonymoususer and cm.emzipcode
	BEGIN
		SELECT top 1 @ClientID, @CustomerID as CustomerId, @AccountID as AccountId, @PremiseID as PremiseId,
		t1.PostalCode AS CustomerPostalCode, t1.PostalCode AS PremisePostalCode, convert(date,getdate()) as DateCreated, 
		t2.City as CustomerCity, t2.State As CustomerState, 
		t2.City AS PremiseCity, t2.State As PremiseState
		FROM dbo.dimAnonymousUser t1 join cm.emZipCode t2
			on t1.postalcode = t2.zipcode
		WHERE t1.customerid = @customerID
	END
ELSE 
	-- run the original query from this stored procedure
	BEGIN
		SELECT dbo.DimCustomer.ClientId, dbo.DimCustomer.CustomerId, dbo.DimPremise.AccountId, dbo.DimPremise.PremiseId, 
			   dbo.DimCustomer.PostalCode AS CustomerPostalCode, dbo.DimPremise.PostalCode AS PremisePostalCode, dbo.FactCustomerPremise.DateCreated, 
			   dbo.DimCustomer.City As CustomerCity, dbo.DimCustomer.StateProvince As CustomerState, 
			   dbo.DimPremise.City AS PremiseCity, dbo.DimPremise.StateProvince As PremiseState
		FROM dbo.FactCustomerPremise INNER JOIN
			dbo.DimCustomer ON dbo.FactCustomerPremise.CustomerKey = dbo.DimCustomer.CustomerKey INNER JOIN
			dbo.DimPremise ON dbo.FactCustomerPremise.PremiseKey = dbo.DimPremise.PremiseKey
		WHERE (dbo.DimCustomer.ClientId = @ClientID) 
			  AND (dbo.DimCustomer.CustomerId = @CustomerID) 
			  AND (dbo.DimPremise.PremiseId = @PremiseID) 
			  AND (dbo.DimPremise.AccountId = @AccountID) 		
		ORDER BY dbo.FactCustomerPremise.DateCreated DESC
	END

SET NOCOUNT OFF

GO


