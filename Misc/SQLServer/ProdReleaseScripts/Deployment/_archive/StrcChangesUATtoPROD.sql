/*
Run this script on:

        acePsql1c0.database.windows.net.InsightsMetadata    -  This database will be modified

to synchronize it with:

        aceUsql1c0.database.windows.net.InsightsMetadata

You are recommended to back up your database before running this script

Script created by SQL Compare version 11.1.3 from Red Gate Software Ltd at 10/28/2015 8:39:24 AM

*/
Use InsightsMetaData

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Removing schema binding from [cm].[vTextContent]'
GO



ALTER VIEW [cm].[vTextContent]
AS
    SELECT  ctc.[ClientID] ,
            ttc.[TextContentKey] ,
            ctc.[CategoryKey] ,
            ctclc.[ShortText] ,
            ctclc.[MediumText] ,
            ctclc.[LongText] ,
			ctclc.[RequireVariableSubstitution]
    FROM    [cm].[ClientTextContent] AS ctc WITH ( NOLOCK )
			RIGHT JOIN [cm].TypeTextContent AS ttc WITH ( NOLOCK ) ON ttc.TextContentKey = ctc.TextContentKey
            LEFT JOIN [cm].ClientTextContentLocaleContent AS ctclc WITH ( NOLOCK ) ON ctclc.ClientTextContentID = ctc.ClientTextContentID
    WHERE   ctclc.LocaleKey = 'en-US'







GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Removing schema binding from [cm].[vExpressions]'
GO

ALTER  VIEW [cm].[vExpressions]
AS

	SELECT [ClientID]
		  ,te.[ExpressionKey]
		  ,[Name]
		  ,[Description]
		  ,[Formula]
		  ,[Comments]
	  FROM [cm].[ClientExpression] AS ce WITH (NOLOCK)
	  RIGHT JOIN [cm].[TypeExpression] AS te WITH (NOLOCK) ON te.ExpressionKey = ce.ExpressionKey

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Removing schema binding from [cm].[vProfileDefaults]'
GO


ALTER  VIEW [cm].[vProfileDefaults]
AS
    SELECT  cpdc.[ClientID] ,
			tfd.[ProfileDefaultKey] ,
            cfd.[ProfileDefaultCollectionKey] ,
            cfd.[ProfileAttributeKey] ,
            cfd.[DefaultKey] ,
            ISNULL(cfd.[DefaultValue], '') AS DefaultValue
    FROM    [cm].[ClientProfileDefault] AS cfd WITH ( NOLOCK )
            RIGHT JOIN [cm].[TypeProfileDefault] AS tfd WITH ( NOLOCK ) ON tfd.ProfileDefaultKey = cfd.ProfileDefaultKey
			INNER JOIN [cm].[ClientProfileDefaultCollection] AS cpdc WITH (NOLOCK) ON cpdc.ProfileDefaultCollectionKey = cfd.ProfileDefaultCollectionKey



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Removing schema binding from [cm].[vAsset]'
GO


ALTER VIEW [cm].[vAsset]
AS
    SELECT  ca.[ClientID],
			ca.AssetKey AS 'Title',
            calc.[Description] ,
            calc.[FileContentType] , 
	        calc.[FileName] ,
	        calc.[FileUrl]
    FROM    [cm].ClientAsset AS ca 
            INNER JOIN [cm].ClientAssetLocaleContent AS calc WITH ( NOLOCK ) ON ca.ClientAssetID = calc.ClientAssetID
    WHERE   calc.LocaleKey = 'en-US'


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Removing schema binding from [cm].[vFileContent]'
GO



ALTER VIEW [cm].[vFileContent]
AS
    SELECT  cfc.[ClientID] ,
            cfc.[FileContentKey] ,
			cfc.[Type] ,
			cfc.[CategoryKey] ,
			cfc.[SmallFile] ,
			cfc.[MediumFile] ,
			cfc.[LargeFile]
			 --,
    --        cflc.[Title] ,
    --        cflc.[Description] ,
    --        cflc.[FileName] ,
    --        cflc.[FileContentType] ,
    --        cflc.[FileUrl] ,
    --        cflc.[FileSize]
    FROM    [cm].[ClientFileContent] AS cfc WITH ( NOLOCK )
			INNER JOIN [cm].ClientAsset AS ca WITH ( NOLOCK ) ON (cfc.SmallFile = ca.AssetKey OR cfc.MediumFile = ca.AssetKey OR cfc.LargeFile = ca.AssetKey)
            INNER JOIN [cm].ClientAssetLocaleContent AS cflc WITH ( NOLOCK ) ON ca.ClientAssetID = cflc.ClientAssetID
    WHERE   cflc.LocaleKey = 'en-US'



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Removing schema binding from [cm].[vProfileOptions]'
GO

ALTER VIEW [cm].[vProfileOptions]
AS
    SELECT  cpo.[ClientID] ,
            tpo.[ProfileOptionKey] ,
			cpo.[ProfileOptionValue] ,
            cpo.[NameKey] ,
            cpo.[Description]
    FROM    [cm].[ClientProfileOption] AS cpo WITH ( NOLOCK )
	RIGHT JOIN [cm].[TypeProfileOption] AS tpo WITH ( NOLOCK ) ON tpo.ProfileOptionKey = cpo.ProfileOptionKey






GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientActionSavings]'
GO
ALTER TABLE [cm].[ClientActionSavings] DROP CONSTRAINT [FK_ClientActionSavings_TypeAction]
ALTER TABLE [cm].[ClientActionSavings] DROP CONSTRAINT [FK_ClientActionSavings_Client]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientActionSavingsCondition]'
GO
ALTER TABLE [cm].[ClientActionSavingsCondition] DROP CONSTRAINT [FK_ClientActionSavingsCondition_ClientActionSavings]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientAction]'
GO
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeAction]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeActionType]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeUom]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeUom1]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_Client]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeCommodity]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeExpression]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeDifficulty]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeHabitInterval]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_ClientFileContent1]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_ClientFileContent2]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeTextContent]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeExpression1]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_ClientFileContent]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_ClientFileContent3]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientActionAppliance]'
GO
ALTER TABLE [cm].[ClientActionAppliance] DROP CONSTRAINT [FK_ClientActionAppliance_ClientAction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientActionCondition]'
GO
ALTER TABLE [cm].[ClientActionCondition] DROP CONSTRAINT [FK_ClientActionCondition_ClientAction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientActionSeason]'
GO
ALTER TABLE [cm].[ClientActionSeason] DROP CONSTRAINT [FK_ClientActionSeasonality_ClientAction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientActionWhatIfData]'
GO
ALTER TABLE [cm].[ClientActionWhatIfData] DROP CONSTRAINT [FK_ClientActionWhatIfData_ClientAction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientApplianceExpression]'
GO
ALTER TABLE [cm].[ClientApplianceExpression] DROP CONSTRAINT [FK_ClientApplianceExpression_TypeExpression]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientApplianceProfileAttribute]'
GO
ALTER TABLE [cm].[ClientApplianceProfileAttribute] DROP CONSTRAINT [FK_ClientApplianceProfileAttribute_TypeProfileAttribute]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientAsset]'
GO
ALTER TABLE [cm].[ClientAsset] DROP CONSTRAINT [FK_ClientAsset_TypeAsset]
ALTER TABLE [cm].[ClientAsset] DROP CONSTRAINT [FK_ClientAsset_Client]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientAssetLocaleContent]'
GO
ALTER TABLE [cm].[ClientAssetLocaleContent] DROP CONSTRAINT [FK_ClientAssetLocaleContent_ClientAsset]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientCondition]'
GO
ALTER TABLE [cm].[ClientCondition] DROP CONSTRAINT [FK_ClientCondition_TypeCategory]
ALTER TABLE [cm].[ClientCondition] DROP CONSTRAINT [FK_ClientCondition_TypeProfileAttribute]
ALTER TABLE [cm].[ClientCondition] DROP CONSTRAINT [FK_ClientCondition_TypeProfileOption]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientConfigurationBulk]'
GO
ALTER TABLE [cm].[ClientConfigurationBulk] DROP CONSTRAINT [FK_ClientConfigurationBulk_TypeCategory]
ALTER TABLE [cm].[ClientConfigurationBulk] DROP CONSTRAINT [FK_ClientConfigurationBulk_TypeConfigurationBulk]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientConfiguration]'
GO
ALTER TABLE [cm].[ClientConfiguration] DROP CONSTRAINT [FK_ClientConfiguration_TypeCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientEnumerationEnumerationItem]'
GO
ALTER TABLE [cm].[ClientEnumerationEnumerationItem] DROP CONSTRAINT [FK_ClientEnumerationEnumerationItem_ClientEnumeration]
ALTER TABLE [cm].[ClientEnumerationEnumerationItem] DROP CONSTRAINT [FK_ClientEnumerationEnumerationItem_TypeEnumerationItem]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientEnumerationItem]'
GO
ALTER TABLE [cm].[ClientEnumerationItem] DROP CONSTRAINT [FK_ClientEnumerationItem_Client]
ALTER TABLE [cm].[ClientEnumerationItem] DROP CONSTRAINT [FK_ClientEnumerationItem_TypeEnumerationItem]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientExpression]'
GO
ALTER TABLE [cm].[ClientExpression] DROP CONSTRAINT [FK_ClientExpression_Client]
ALTER TABLE [cm].[ClientExpression] DROP CONSTRAINT [FK_ClientExpression_TypeExpression]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientFileContent]'
GO
ALTER TABLE [cm].[ClientFileContent] DROP CONSTRAINT [FK_ClientFileContent_TypeCategory]
ALTER TABLE [cm].[ClientFileContent] DROP CONSTRAINT [FK_ClientFileContentLargeFile_TypeAsset]
ALTER TABLE [cm].[ClientFileContent] DROP CONSTRAINT [FK_ClientFileContentMediumFile_TypeAsset]
ALTER TABLE [cm].[ClientFileContent] DROP CONSTRAINT [FK_ClientFileContentSmallFile_TypeAsset]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientProfileAttribute]'
GO
ALTER TABLE [cm].[ClientProfileAttribute] DROP CONSTRAINT [FK_ClientProfileAttribute_Client]
ALTER TABLE [cm].[ClientProfileAttribute] DROP CONSTRAINT [FK_ClientProfileAttribute_TypeProfileAttribute]
ALTER TABLE [cm].[ClientProfileAttribute] DROP CONSTRAINT [FK_ClientProfileAttribute_TypeTextContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientProfileAttributeCondition]'
GO
ALTER TABLE [cm].[ClientProfileAttributeCondition] DROP CONSTRAINT [FK_ClientProfileAttributeCondition_ClientProfileAttribute]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientProfileAttributeProfileOption]'
GO
ALTER TABLE [cm].[ClientProfileAttributeProfileOption] DROP CONSTRAINT [FK_ClientProfileAttributeProfileOption_ClientProfileAttribute]
ALTER TABLE [cm].[ClientProfileAttributeProfileOption] DROP CONSTRAINT [FK_ClientProfileAttributeProfileOption_TypeProfileOption]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientProfileDefault]'
GO
ALTER TABLE [cm].[ClientProfileDefault] DROP CONSTRAINT [FK_ClientProfileDefault_TypeProfileOption]
ALTER TABLE [cm].[ClientProfileDefault] DROP CONSTRAINT [FK_ClientProfileDefault_TypeProfileAttribute]
ALTER TABLE [cm].[ClientProfileDefault] DROP CONSTRAINT [FK_ClientProfileDefault_TypeProfileDefaultCollection]
ALTER TABLE [cm].[ClientProfileDefault] DROP CONSTRAINT [FK_ClientProfileDefault_TypeProfileDefault]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientProfileOption]'
GO
ALTER TABLE [cm].[ClientProfileOption] DROP CONSTRAINT [FK_ClientProfileOption_TypeTextContent]
ALTER TABLE [cm].[ClientProfileOption] DROP CONSTRAINT [FK_ClientProfileOption_TypeProfileOption]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientProfileSection]'
GO
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent]
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent3]
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent2]
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientSeason]'
GO
ALTER TABLE [cm].[ClientSeason] DROP CONSTRAINT [FK_ClientSeason_TypeTextContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientTabAction]'
GO
ALTER TABLE [cm].[ClientTabAction] DROP CONSTRAINT [FK_ClientTabAction_TypeAction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientTabCondition]'
GO
ALTER TABLE [cm].[ClientTabCondition] DROP CONSTRAINT [FK_ClientTabConditionality_ClientTab]
ALTER TABLE [cm].[ClientTabCondition] DROP CONSTRAINT [FK_ClientTabConditionality_TypeConditionality]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientTabConfiguration]'
GO
ALTER TABLE [cm].[ClientTabConfiguration] DROP CONSTRAINT [FK_ClientTabConfigurationality_ClientTab]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientTabTextContent]'
GO
ALTER TABLE [cm].[ClientTabTextContent] DROP CONSTRAINT [FK_ClientTabTextContentality_ClientTab]
ALTER TABLE [cm].[ClientTabTextContent] DROP CONSTRAINT [FK_ClientTabTextContentality_TypeTextContentality]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientTab]'
GO
ALTER TABLE [cm].[ClientTab] DROP CONSTRAINT [FK_ClientTab_TypeTextContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientTextContent]'
GO
ALTER TABLE [cm].[ClientTextContent] DROP CONSTRAINT [FK_ClientTextContent_TypeCategory]
ALTER TABLE [cm].[ClientTextContent] DROP CONSTRAINT [FK_ClientTextContent_Client]
ALTER TABLE [cm].[ClientTextContent] DROP CONSTRAINT [FK_ClientTextContent_TypeTextContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientTextContentLocaleContent]'
GO
ALTER TABLE [cm].[ClientTextContentLocaleContent] DROP CONSTRAINT [FK_ClientTextContentLocaleContent_ClientTextContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientWidgetTextContent]'
GO
ALTER TABLE [cm].[ClientWidgetTextContent] DROP CONSTRAINT [FK_ClientWidgetTextContentality_TypeTextContentality]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientWidget]'
GO
ALTER TABLE [cm].[ClientWidget] DROP CONSTRAINT [FK_ClientWidget_TypeTextContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[TypeEnumeration]'
GO
ALTER TABLE [cm].[TypeEnumeration] DROP CONSTRAINT [FK_TypeEnumeration_TypeCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[TypeProfileAttribute]'
GO
ALTER TABLE [cm].[TypeProfileAttribute] DROP CONSTRAINT [FK_TypeProfileAttribute_TypeEntityLevel]
ALTER TABLE [cm].[TypeProfileAttribute] DROP CONSTRAINT [FK_TypeProfileAttribute_TypeProfileAttributeType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientProfileSectionAttribute]'
GO
ALTER TABLE [cm].[ClientProfileSectionAttribute] DROP CONSTRAINT [FK_ClientProfileSectionAttribute_TypeProfileAttribute]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientWhatIfData]'
GO
ALTER TABLE [cm].[ClientWhatIfData] DROP CONSTRAINT [FK_ClientWhatIfData_TypeProfileAttribute]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientAppliance]'
GO
ALTER TABLE [cm].[ClientAppliance] DROP CONSTRAINT [FK_ClientAppliance_TypeTextContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientCommodity]'
GO
ALTER TABLE [cm].[ClientCommodity] DROP CONSTRAINT [FK_ClientCommodity_TypeTextContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientCurrency]'
GO
ALTER TABLE [cm].[ClientCurrency] DROP CONSTRAINT [FK_ClientCurrency_TypeTextContent]
ALTER TABLE [cm].[ClientCurrency] DROP CONSTRAINT [FK_ClientCurrency_TypeTextContent1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientEnduse]'
GO
ALTER TABLE [cm].[ClientEnduse] DROP CONSTRAINT [FK_ClientEnduse_TypeTextContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientMeasurement]'
GO
ALTER TABLE [cm].[ClientMeasurement] DROP CONSTRAINT [FK_ClientMeasurement_TypeTextContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [cm].[ClientUOM]'
GO
ALTER TABLE [cm].[ClientUOM] DROP CONSTRAINT [FK_ClientUOM_ClientUOM1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[ClientActionSavings]'
GO
ALTER TABLE [cm].[ClientActionSavings] DROP CONSTRAINT [DF_ClientActionSavings_DateUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[ClientAction]'
GO
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [DF_ClientAction_DateUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[ClientAction]'
GO
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [DF_ClientAction_SavingsCalcMethod]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[ClientAsset]'
GO
ALTER TABLE [cm].[ClientAsset] DROP CONSTRAINT [DF_ClientAsset_DateUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[ClientEnumerationItem]'
GO
ALTER TABLE [cm].[ClientEnumerationItem] DROP CONSTRAINT [DF_ClientEnumerationItem_DateUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[ClientExpression]'
GO
ALTER TABLE [cm].[ClientExpression] DROP CONSTRAINT [DF_ClientExpression_DateUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[ClientProfileAttribute]'
GO
ALTER TABLE [cm].[ClientProfileAttribute] DROP CONSTRAINT [DF_ClientProfileAttribute_DateUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[ClientProfileAttribute]'
GO
ALTER TABLE [cm].[ClientProfileAttribute] DROP CONSTRAINT [DF_ClientProfileAttribute_QuestionTextKey]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[ClientProfileDefault]'
GO
ALTER TABLE [cm].[ClientProfileDefault] DROP CONSTRAINT [DF_ClientProfileDefault_DateUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[ClientProfileDefault]'
GO
ALTER TABLE [cm].[ClientProfileDefault] DROP CONSTRAINT [DF_ClientProfileDefault_ProfileDefaultKey]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[ClientProfileDefault]'
GO
ALTER TABLE [cm].[ClientProfileDefault] DROP CONSTRAINT [DF_ClientID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[ClientTextContent]'
GO
ALTER TABLE [cm].[ClientTextContent] DROP CONSTRAINT [DF_ClientTextContent_DateUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[TypeAsset]'
GO
ALTER TABLE [cm].[TypeAsset] DROP CONSTRAINT [DF_TypeAsset_DateUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[TypeConfigurationBulk]'
GO
ALTER TABLE [cm].[TypeConfigurationBulk] DROP CONSTRAINT [DF_TypeConfigurationBulk_DateUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[TypeProfileAttribute]'
GO
ALTER TABLE [cm].[TypeProfileAttribute] DROP CONSTRAINT [DF_TypeProfileAttribute_DateUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[TypeProfileOption]'
GO
ALTER TABLE [cm].[TypeProfileOption] DROP CONSTRAINT [DF_TypeProfileOption_DateUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [cm].[TypeTextContent]'
GO
ALTER TABLE [cm].[TypeTextContent] DROP CONSTRAINT [DF_TypeTextContent_DateUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_INV_FactPremiseAttribute_3] from [ETL].[INV_FactPremiseAttribute]'
GO
DROP INDEX [IX_INV_FactPremiseAttribute_3] ON [ETL].[INV_FactPremiseAttribute]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [cm].[uspUpdateClientAction]'
GO
DROP PROCEDURE [cm].[uspUpdateClientAction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [cm].[uspDeleteContentOrphan]'
GO
DROP PROCEDURE [cm].[uspDeleteContentOrphan]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping types'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TYPE [cm].[ClientActionTable]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating types'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE TYPE [cm].[ClientActionTable] AS TABLE
(
[ClientID] [int] NOT NULL,
[ActionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionTypeKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Hide] [bit] NOT NULL,
[Disable] [bit] NOT NULL,
[CommodityKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DifficultyKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HabitIntervalKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NameKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DescriptionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AnnualCost] [decimal] (18, 2) NOT NULL,
[UpfrontCost] [decimal] (18, 2) NOT NULL,
[AnnualSavingsEstimate] [decimal] (18, 2) NOT NULL,
[AnnualUsageSavingsEstimate] [decimal] (18, 2) NULL,
[AnnualUsageSavingsUomKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CostVariancePercent] [decimal] (18, 2) NOT NULL,
[PaybackTime] [int] NOT NULL,
[ROI] [decimal] (18, 2) NOT NULL,
[RebateAvailable] [bit] NOT NULL,
[RebateAmount] [decimal] (18, 2) NULL,
[RebateUrl] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RebateImageKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IconClass] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImageKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VideoKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConditionKeys] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApplianceKeys] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SeasonKeys] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionPriority] [int] NULL,
[SavingsCalcMethod] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SavingsAmount] [decimal] (18, 2) NULL,
[WhatIfDataKeys] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CostExpression] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RebateExpression] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NextStepLink] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NextStepLinkText] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NextStepLinkType] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tags] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
PRIMARY KEY CLUSTERED  ([ClientID], [ActionKey])
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [cm].[TypeTextContent]'
GO
CREATE TABLE [cm].[RG_Recovery_1_TypeTextContent]
(
[TextContentKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TextContentID] [int] NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_TypeTextContent_DateUpdated] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_6772_PK_TextContent] on [cm].[RG_Recovery_1_TypeTextContent]'
GO
ALTER TABLE [cm].[RG_Recovery_1_TypeTextContent] ADD CONSTRAINT [RG_Recovery_6772_PK_TextContent] PRIMARY KEY CLUSTERED  ([TextContentKey]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [cm].[RG_Recovery_1_TypeTextContent]([TextContentKey], [TextContentID], [DateUpdated]) SELECT [TextContentKey], [TextContentID], [DateUpdated] FROM [cm].[TypeTextContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [cm].[TypeTextContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[cm].[RG_Recovery_1_TypeTextContent]', N'TypeTextContent', N'OBJECT'
EXEC sp_rename N'[cm].[TypeTextContent].[RG_Recovery_6772_PK_TextContent]', N'PK_TextContent', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TypeTextContent] on [cm].[TypeTextContent]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TypeTextContent] ON [cm].[TypeTextContent] ([TextContentID]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [cm].[ClientTextContent]'
GO
CREATE TABLE [cm].[RG_Recovery_2_ClientTextContent]
(
[ClientTextContentID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[TextContentKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CategoryKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_ClientTextContent_DateUpdated] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_600f_PK_ClientTextContent] on [cm].[RG_Recovery_2_ClientTextContent]'
GO
ALTER TABLE [cm].[RG_Recovery_2_ClientTextContent] ADD CONSTRAINT [RG_Recovery_600f_PK_ClientTextContent] PRIMARY KEY CLUSTERED  ([ClientTextContentID]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_2_ClientTextContent] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [cm].[RG_Recovery_2_ClientTextContent]([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey], [DateUpdated]) SELECT [ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey], [DateUpdated] FROM [cm].[ClientTextContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_2_ClientTextContent] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [cm].[ClientTextContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[cm].[RG_Recovery_2_ClientTextContent]', N'ClientTextContent', N'OBJECT'
EXEC sp_rename N'[cm].[ClientTextContent].[RG_Recovery_600f_PK_ClientTextContent]', N'PK_ClientTextContent', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ClientTextContent] on [cm].[ClientTextContent]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ClientTextContent] ON [cm].[ClientTextContent] ([ClientID], [TextContentKey]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [cm].[DatabaseDiagrams]'
GO
CREATE TABLE [cm].[RG_Recovery_3_DatabaseDiagrams]
(
[Name] [sys].[sysname] NOT NULL,
[PrincipalId] [int] NOT NULL,
[DiagramId] [int] NOT NULL,
[Version] [int] NULL,
[Definition] [varbinary] (max) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_726a_PK_tbl_sysdiagrams] on [cm].[RG_Recovery_3_DatabaseDiagrams]'
GO
ALTER TABLE [cm].[RG_Recovery_3_DatabaseDiagrams] ADD CONSTRAINT [RG_Recovery_726a_PK_tbl_sysdiagrams] PRIMARY KEY CLUSTERED  ([DiagramId]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [cm].[RG_Recovery_3_DatabaseDiagrams]([Name], [PrincipalId], [DiagramId], [Version], [Definition]) SELECT [Name], [PrincipalId], [DiagramId], [Version], [Definition] FROM [cm].[DatabaseDiagrams]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [cm].[DatabaseDiagrams]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[cm].[RG_Recovery_3_DatabaseDiagrams]', N'DatabaseDiagrams', N'OBJECT'
EXEC sp_rename N'[cm].[DatabaseDiagrams].[RG_Recovery_726a_PK_tbl_sysdiagrams]', N'PK_tbl_sysdiagrams', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [cm].[ClientEnumerationEnumerationItem]'
GO
CREATE TABLE [cm].[RG_Recovery_4_ClientEnumerationEnumerationItem]
(
[ClientEnumerationID] [int] NOT NULL,
[EnumerationItemKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayOrder] [int] NOT NULL,
[IsDefault] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_afbd_PK_ClientEnumerationEnumerationItem] on [cm].[RG_Recovery_4_ClientEnumerationEnumerationItem]'
GO
ALTER TABLE [cm].[RG_Recovery_4_ClientEnumerationEnumerationItem] ADD CONSTRAINT [RG_Recovery_afbd_PK_ClientEnumerationEnumerationItem] PRIMARY KEY CLUSTERED  ([ClientEnumerationID], [EnumerationItemKey]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [cm].[RG_Recovery_4_ClientEnumerationEnumerationItem]([ClientEnumerationID], [EnumerationItemKey], [DisplayOrder], [IsDefault]) SELECT [ClientEnumerationID], [EnumerationItemKey], [DisplayOrder], [IsDefault] FROM [cm].[ClientEnumerationEnumerationItem]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [cm].[ClientEnumerationEnumerationItem]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[cm].[RG_Recovery_4_ClientEnumerationEnumerationItem]', N'ClientEnumerationEnumerationItem', N'OBJECT'
EXEC sp_rename N'[cm].[ClientEnumerationEnumerationItem].[RG_Recovery_afbd_PK_ClientEnumerationEnumerationItem]', N'PK_ClientEnumerationEnumerationItem', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [cm].[ClientProfileDefault]'
GO
CREATE TABLE [cm].[RG_Recovery_5_ClientProfileDefault]
(
[ClientProfileDefaultID] [int] NOT NULL IDENTITY(1, 1),
[ProfileDefaultKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ClientProfileDefault_ProfileDefaultKey] DEFAULT ('profiledefault.house.totalarea'),
[ProfileDefaultCollectionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProfileAttributeKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultValue] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_ClientProfileDefault_DateUpdated] DEFAULT (getdate()),
[ClientId] [int] NOT NULL CONSTRAINT [DF_ClientID] DEFAULT ((0))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_6598_PK_ClientProfileDefault] on [cm].[RG_Recovery_5_ClientProfileDefault]'
GO
ALTER TABLE [cm].[RG_Recovery_5_ClientProfileDefault] ADD CONSTRAINT [RG_Recovery_6598_PK_ClientProfileDefault] PRIMARY KEY CLUSTERED  ([ClientProfileDefaultID]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_5_ClientProfileDefault] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [cm].[RG_Recovery_5_ClientProfileDefault]([ClientProfileDefaultID], [ProfileDefaultKey], [ProfileDefaultCollectionKey], [ProfileAttributeKey], [DefaultKey], [DefaultValue], [DateUpdated], [ClientId]) SELECT [ClientProfileDefaultID], [ProfileDefaultKey], [ProfileDefaultCollectionKey], [ProfileAttributeKey], [DefaultKey], [DefaultValue], [DateUpdated], [ClientId] FROM [cm].[ClientProfileDefault]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_5_ClientProfileDefault] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [cm].[ClientProfileDefault]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[cm].[RG_Recovery_5_ClientProfileDefault]', N'ClientProfileDefault', N'OBJECT'
EXEC sp_rename N'[cm].[ClientProfileDefault].[RG_Recovery_6598_PK_ClientProfileDefault]', N'PK_ClientProfileDefault', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ClientProfileDefault] on [cm].[ClientProfileDefault]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ClientProfileDefault] ON [cm].[ClientProfileDefault] ([ProfileDefaultCollectionKey], [ProfileAttributeKey], [ClientId]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[GetComponentConfiguration]'
GO



-- =============================================
-- Author:		Ubaid
-- Create date: 11/12/2014
-- Description:	Script to return configurations for SSIS packages
-- =============================================
/*
BEGIN TRANSACTION

EXEC [dbo].[GetComponentConfiguration] @ClientId = 210,
    @ComponentName = N'csvtoxml'

select * from cm.ClientConfiguration

ROLLBACK TRANSACTION
*/
-- =============================================
ALTER PROCEDURE [dbo].[GetComponentConfiguration]
    @ClientId AS INT ,
    @ComponentName AS VARCHAR(256) = ''
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @EnvironmentKey AS VARCHAR(256);

        SELECT  @EnvironmentKey = EnvironmentKey
        FROM    cm.EnvironmentConfiguration;

        WITH    ComponentConfig
                  AS ( SELECT   ClientID ,
                                SUBSTRING(ConfigurationKey,
                                          CHARINDEX('.', ConfigurationKey, 9)
                                          + 1, 100) AS ConfigurationKey ,
                                Value ,
                                IIF(ConfigurationKey LIKE 'package.global.%', 0, 1) AS ConfigPriority ,
                                IIF(EnvironmentKey = @EnvironmentKey, 1, 0) AS EnvironmentPriority
                       FROM     [cm].[ClientConfiguration]
                       WHERE    CategoryKey = 'system'
                                AND ( ConfigurationKey LIKE 'package.'
                                      + @ComponentName + '.%'
                                      OR ConfigurationKey LIKE 'package.global.%'
                                    )
                                AND ( ClientID = 0
                                      OR ClientID = @ClientId
                                    )
                                AND ( EnvironmentKey = @EnvironmentKey
                                      OR EnvironmentKey = 'prod'
                                    )
                     )
            SELECT  ConfigurationKey ,
                    Value
            INTO    #Result
            FROM    ( SELECT    ClientID ,
                                ConfigurationKey ,
                                Value ,
                                ConfigPriority ,
                                ROW_NUMBER() OVER ( PARTITION BY ConfigurationKey ORDER BY EnvironmentPriority DESC, ClientID DESC, ConfigPriority DESC ) AS SortId
                      FROM      ComponentConfig
                    ) c
            WHERE   SortId = 1

        DECLARE @Columns AS VARCHAR(MAX);
        DECLARE @Script AS VARCHAR(MAX);

        SELECT  @Columns = COALESCE(@Columns + ', ', '')
                + QUOTENAME(ConfigurationKey)
        FROM    ( SELECT DISTINCT
                            ConfigurationKey
                  FROM      #Result
                ) AS mos 

        SET @Script = 'SELECT  *
		FROM    ( SELECT    *
					FROM      #Result
				) a PIVOT
		( MAX(Value) FOR ConfigurationKey IN ( ' + @Columns + ' ) ) AS PivotTable;'

        EXEC(@Script);

        DROP TABLE #Result


    END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [cm].[ClientAction]'
GO
CREATE TABLE [cm].[RG_Recovery_6_ClientAction]
(
[ClientActionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ActionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionTypeKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Hide] [bit] NOT NULL,
[Disable] [bit] NOT NULL,
[CommodityKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DifficultyKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HabitIntervalKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NameKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DescriptionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AnnualCost] [decimal] (18, 2) NOT NULL,
[UpfrontCost] [decimal] (18, 2) NOT NULL,
[AnnualSavingsEstimate] [decimal] (18, 2) NOT NULL,
[AnnualUsageSavingsEstimate] [decimal] (18, 2) NULL,
[AnnualUsageSavingsUomKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CostVariancePercent] [decimal] (18, 2) NOT NULL,
[PaybackTime] [int] NOT NULL,
[ROI] [decimal] (18, 2) NOT NULL,
[RebateAvailable] [bit] NOT NULL,
[RebateAmount] [decimal] (18, 2) NULL,
[RebateUrl] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RebateImageKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NextStepLinkText] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImageKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VideoKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_ClientAction_DateUpdated] DEFAULT (getdate()),
[ActionPriority] [int] NULL,
[SavingsCalcMethod] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ClientAction_SavingsCalcMethod] DEFAULT ('None'),
[SavingsAmount] [decimal] (18, 2) NULL,
[CostExpression] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RebateExpression] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NextStepLink] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NextStepLinkType] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tags] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IconClass] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_2734_PK_ClientAction] on [cm].[RG_Recovery_6_ClientAction]'
GO
ALTER TABLE [cm].[RG_Recovery_6_ClientAction] ADD CONSTRAINT [RG_Recovery_2734_PK_ClientAction] PRIMARY KEY CLUSTERED  ([ClientActionID]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_6_ClientAction] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [cm].[RG_Recovery_6_ClientAction]([ClientActionID], [ClientID], [ActionKey], [ActionTypeKey], [Hide], [Disable], [CommodityKey], [DifficultyKey], [HabitIntervalKey], [NameKey], [DescriptionKey], [AnnualCost], [UpfrontCost], [AnnualSavingsEstimate], [AnnualUsageSavingsEstimate], [AnnualUsageSavingsUomKey], [CostVariancePercent], [PaybackTime], [ROI], [RebateAvailable], [RebateAmount], [RebateUrl], [RebateImageKey], [NextStepLinkText], [ImageKey], [VideoKey], [Comments], [DateUpdated], [ActionPriority], [SavingsCalcMethod], [SavingsAmount], [CostExpression], [RebateExpression]) SELECT [ClientActionID], [ClientID], [ActionKey], [ActionTypeKey], [Hide], [Disable], [CommodityKey], [DifficultyKey], [HabitIntervalKey], [NameKey], [DescriptionKey], [AnnualCost], [UpfrontCost], [AnnualSavingsEstimate], [AnnualUsageSavingsEstimate], [AnnualUsageSavingsUomKey], [CostVariancePercent], [PaybackTime], [ROI], [RebateAvailable], [RebateAmount], [RebateUrl], [RebateImageKey], [IconKey], [ImageKey], [VideoKey], [Comments], [DateUpdated], [ActionPriority], [SavingsCalcMethod], [SavingsAmount], [CostExpression], [RebateExpression] FROM [cm].[ClientAction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_6_ClientAction] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [cm].[ClientAction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[cm].[RG_Recovery_6_ClientAction]', N'ClientAction', N'OBJECT'
EXEC sp_rename N'[cm].[ClientAction].[RG_Recovery_2734_PK_ClientAction]', N'PK_ClientAction', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ClientAction] on [cm].[ClientAction]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ClientAction] ON [cm].[ClientAction] ([ClientID], [ActionKey], [Disable])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [cm].[vActions]'
GO



ALTER VIEW [cm].[vActions]
AS
    SELECT  [ClientID] ,
            [ta].[ActionKey] ,
            [NameKey] ,
            ISNULL([cm].[fnGetActionConditions](ClientActionID), '') AS ConditionKeys ,
            [DescriptionKey] ,
            [Disable] ,
            [Hide] ,
            ISNULL([VideoKey], '') AS VideoKey ,
            ISNULL([ImageKey], '') AS Images ,
            ISNULL([IconClass], '') AS IconClass ,
            [CommodityKey] ,
            [ActionTypeKey] ,
            ISNULL([cm].[fnGetActionAppliances](ClientActionID), '') AS ApplianceKeys ,
            [UpfrontCost] ,
            [AnnualCost] ,
            [CostVariancePercent] ,
            [DifficultyKey] AS Difficulty ,
            [HabitIntervalKey] AS HabitInterval ,
			--[SavingsEstimateQualityKey] AS DefaultSavingsEstimateQuality ,
            [AnnualSavingsEstimate] AS DefaultAnnualSavingsEstimate ,
            ISNULL(CONVERT(VARCHAR(10), [AnnualUsageSavingsEstimate]), '') AS DefaultAnnualUsageSavingsEstimate ,
            ISNULL([AnnualUsageSavingsUomKey], '') AS AnnualUsageSavingsUomKey ,
            [PaybackTime] AS DefaultPaybackTime ,
            [ROI] AS DefaultROI ,
            [RebateAvailable] ,
            ISNULL([RebateUrl], '') AS RebateUrl ,
            ISNULL([RebateImageKey], '') AS RebateImageKey ,
            ISNULL(CONVERT(VARCHAR(10), [RebateAmount]), '') AS RebateAmount ,
            ISNULL([cm].[fnGetActionSeasons](ClientActionID), '') AS SeasonKeys ,
            ISNULL([Comments], '') AS Comments ,
            ca.ActionPriority ,
            ca.[SavingsCalcMethod] ,
            ca.[SavingsAmount] ,
            ISNULL([cm].[fnGetActionWhatIfData](ClientActionID), '') AS WhatIfDataKeys,
            ca.[CostExpression] ,
            ca.[RebateExpression] ,
			ISNULL(ca.[NextStepLink], '') AS NextStepLink ,
			ISNULL(ca.[NextStepLinkText], '') AS NextStepLinkText ,
			ISNULL(ca.[NextStepLinkType], '') AS NextStepLinkType ,
			ISNULL(ca.[Tags], '') AS Tags
    FROM    [cm].[ClientAction] AS ca WITH ( NOLOCK )
            RIGHT JOIN [cm].[TypeAction] AS ta WITH ( NOLOCK ) ON ta.ActionKey = ca.ActionKey;



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [cm].[ClientActionSavings]'
GO
CREATE TABLE [cm].[RG_Recovery_7_ClientActionSavings]
(
[ClientActionSavingsID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ActionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Rank] [int] NOT NULL,
[Cost] [decimal] (18, 2) NOT NULL,
[AnnualSavingsEstimate] [decimal] (18, 2) NOT NULL,
[AnnualUsageSavingsEstimate] [decimal] (18, 2) NOT NULL,
[Comments] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_ClientActionSavings_DateUpdated] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_607e_PK_ClientActionSavings] on [cm].[RG_Recovery_7_ClientActionSavings]'
GO
ALTER TABLE [cm].[RG_Recovery_7_ClientActionSavings] ADD CONSTRAINT [RG_Recovery_607e_PK_ClientActionSavings] PRIMARY KEY CLUSTERED  ([ClientActionSavingsID]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_7_ClientActionSavings] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [cm].[RG_Recovery_7_ClientActionSavings]([ClientActionSavingsID], [ClientID], [ActionKey], [Rank], [Cost], [AnnualSavingsEstimate], [AnnualUsageSavingsEstimate], [Comments], [DateUpdated]) SELECT [ClientActionSavingsID], [ClientID], [ActionKey], [Rank], [Cost], [AnnualSavingsEstimate], [AnnualUsageSavingsEstimate], [Comments], [DateUpdated] FROM [cm].[ClientActionSavings]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_7_ClientActionSavings] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [cm].[ClientActionSavings]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[cm].[RG_Recovery_7_ClientActionSavings]', N'ClientActionSavings', N'OBJECT'
EXEC sp_rename N'[cm].[ClientActionSavings].[RG_Recovery_607e_PK_ClientActionSavings]', N'PK_ClientActionSavings', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ClientActionSavings] on [cm].[ClientActionSavings]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ClientActionSavings] ON [cm].[ClientActionSavings] ([ClientID], [ActionKey], [Rank]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [cm].[vActionSavings]'
GO
EXEC sp_refreshview N'[cm].[vActionSavings]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [cm].[vAppliances]'
GO
EXEC sp_refreshview N'[cm].[vAppliances]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [cm].[ClientAsset]'
GO
CREATE TABLE [cm].[RG_Recovery_8_ClientAsset]
(
[ClientAssetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[AssetKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_ClientAsset_DateUpdated] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_5adc_PK_ClientAsset] on [cm].[RG_Recovery_8_ClientAsset]'
GO
ALTER TABLE [cm].[RG_Recovery_8_ClientAsset] ADD CONSTRAINT [RG_Recovery_5adc_PK_ClientAsset] PRIMARY KEY CLUSTERED  ([ClientAssetID]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_8_ClientAsset] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [cm].[RG_Recovery_8_ClientAsset]([ClientAssetID], [ClientID], [AssetKey], [DateUpdated]) SELECT [ClientAssetID], [ClientID], [AssetKey], [DateUpdated] FROM [cm].[ClientAsset]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_8_ClientAsset] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [cm].[ClientAsset]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[cm].[RG_Recovery_8_ClientAsset]', N'ClientAsset', N'OBJECT'
EXEC sp_rename N'[cm].[ClientAsset].[RG_Recovery_5adc_PK_ClientAsset]', N'PK_ClientAsset', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ClientAsset] on [cm].[ClientAsset]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ClientAsset] ON [cm].[ClientAsset] ([ClientID], [AssetKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [cm].[vAsset]'
GO


ALTER VIEW [cm].[vAsset]
WITH SCHEMABINDING
AS
    SELECT  ca.[ClientID],
			ca.AssetKey AS 'Title',
            calc.[Description] ,
            calc.[FileContentType] , 
	        calc.[FileName] ,
	        calc.[FileUrl]
    FROM    [cm].ClientAsset AS ca 
            INNER JOIN [cm].ClientAssetLocaleContent AS calc WITH ( NOLOCK ) ON ca.ClientAssetID = calc.ClientAssetID
    WHERE   calc.LocaleKey = 'en-US'



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [cm].[ClientEnumerationItem]'
GO
CREATE TABLE [cm].[RG_Recovery_9_ClientEnumerationItem]
(
[ClientEnumerationItemID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EnumerationItemKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NameKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_ClientEnumerationItem_DateUpdated] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_9d7e_PK_ClientEnumerationItem] on [cm].[RG_Recovery_9_ClientEnumerationItem]'
GO
ALTER TABLE [cm].[RG_Recovery_9_ClientEnumerationItem] ADD CONSTRAINT [RG_Recovery_9d7e_PK_ClientEnumerationItem] PRIMARY KEY CLUSTERED  ([ClientEnumerationItemID]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_9_ClientEnumerationItem] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [cm].[RG_Recovery_9_ClientEnumerationItem]([ClientEnumerationItemID], [ClientID], [EnumerationItemKey], [NameKey], [DateUpdated]) SELECT [ClientEnumerationItemID], [ClientID], [EnumerationItemKey], [NameKey], [DateUpdated] FROM [cm].[ClientEnumerationItem]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_9_ClientEnumerationItem] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [cm].[ClientEnumerationItem]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[cm].[RG_Recovery_9_ClientEnumerationItem]', N'ClientEnumerationItem', N'OBJECT'
EXEC sp_rename N'[cm].[ClientEnumerationItem].[RG_Recovery_9d7e_PK_ClientEnumerationItem]', N'PK_ClientEnumerationItem', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [cm].[vEnumerationItems]'
GO
EXEC sp_refreshview N'[cm].[vEnumerationItems]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [cm].[vEnumerations]'
GO
EXEC sp_refreshview N'[cm].[vEnumerations]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [audit].[BillImportErrors]'
GO
CREATE TABLE [audit].[RG_Recovery_10_BillImportErrors]
(
[BillImportErrorId] [int] NOT NULL IDENTITY(1, 1),
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ErrorDateTime] [datetime] NOT NULL,
[ClientId] [int] NOT NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServicePointId] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EndDate] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageText] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_6d37_PK_BillImportImportErrors] on [audit].[RG_Recovery_10_BillImportErrors]'
GO
ALTER TABLE [audit].[RG_Recovery_10_BillImportErrors] ADD CONSTRAINT [RG_Recovery_6d37_PK_BillImportImportErrors] PRIMARY KEY CLUSTERED  ([BillImportErrorId]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [audit].[RG_Recovery_10_BillImportErrors] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [audit].[RG_Recovery_10_BillImportErrors]([BillImportErrorId], [TrackingId], [ErrorDateTime], [ClientId], [AccountId], [PremiseId], [ServicePointId], [StartDate], [EndDate], [MessageText]) SELECT [BillImportErrorId], [TrackingId], [ErrorDateTime], [ClientId], [AccountId], [PremiseId], [ServicePointId], [StartDate], [EndDate], [MessageText] FROM [audit].[BillImportErrors]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [audit].[RG_Recovery_10_BillImportErrors] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [audit].[BillImportErrors]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[audit].[RG_Recovery_10_BillImportErrors]', N'BillImportErrors', N'OBJECT'
EXEC sp_rename N'[audit].[BillImportErrors].[RG_Recovery_6d37_PK_BillImportImportErrors]', N'PK_BillImportImportErrors', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [cm].[ClientExpression]'
GO
CREATE TABLE [cm].[RG_Recovery_11_ClientExpression]
(
[ClientExpressionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ExpressionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Formula] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Comments] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_ClientExpression_DateUpdated] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_bcf4_PK_ClientExpression] on [cm].[RG_Recovery_11_ClientExpression]'
GO
ALTER TABLE [cm].[RG_Recovery_11_ClientExpression] ADD CONSTRAINT [RG_Recovery_bcf4_PK_ClientExpression] PRIMARY KEY CLUSTERED  ([ClientExpressionID]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_11_ClientExpression] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [cm].[RG_Recovery_11_ClientExpression]([ClientExpressionID], [ClientID], [ExpressionKey], [Name], [Description], [Formula], [Comments], [DateUpdated]) SELECT [ClientExpressionID], [ClientID], [ExpressionKey], [Name], [Description], [Formula], [Comments], [DateUpdated] FROM [cm].[ClientExpression]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_11_ClientExpression] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [cm].[ClientExpression]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[cm].[RG_Recovery_11_ClientExpression]', N'ClientExpression', N'OBJECT'
EXEC sp_rename N'[cm].[ClientExpression].[RG_Recovery_bcf4_PK_ClientExpression]', N'PK_ClientExpression', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ClientExpression] on [cm].[ClientExpression]'
GO
CREATE NONCLUSTERED INDEX [IX_ClientExpression] ON [cm].[ClientExpression] ([ClientID], [ExpressionKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [cm].[vExpressions]'
GO

ALTER  VIEW [cm].[vExpressions]
WITH SCHEMABINDING
AS

	SELECT [ClientID]
		  ,te.[ExpressionKey]
		  ,[Name]
		  ,[Description]
		  ,[Formula]
		  ,[Comments]
	  FROM [cm].[ClientExpression] AS ce WITH (NOLOCK)
	  RIGHT JOIN [cm].[TypeExpression] AS te WITH (NOLOCK) ON te.ExpressionKey = ce.ExpressionKey


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [cm].[vFileContent]'
GO




ALTER VIEW [cm].[vFileContent]
WITH SCHEMABINDING
AS
    SELECT DISTINCT cfc.[ClientID] ,
            cfc.[FileContentKey] ,
			cfc.[Type] ,
			cfc.[CategoryKey] ,
			cfc.[SmallFile] ,
			cfc.[MediumFile] ,
			cfc.[LargeFile]
			 --,
    --        cflc.[Title] ,
    --        cflc.[Description] ,
    --        cflc.[FileName] ,
    --        cflc.[FileContentType] ,
    --        cflc.[FileUrl] ,
    --        cflc.[FileSize]
    FROM    [cm].[ClientFileContent] AS cfc WITH ( NOLOCK )
			INNER JOIN [cm].ClientAsset AS ca WITH ( NOLOCK ) ON (cfc.SmallFile = ca.AssetKey OR cfc.MediumFile = ca.AssetKey OR cfc.LargeFile = ca.AssetKey)
            INNER JOIN [cm].ClientAssetLocaleContent AS cflc WITH ( NOLOCK ) ON ca.ClientAssetID = cflc.ClientAssetID
    WHERE   cflc.LocaleKey = 'en-US'




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [cm].[TypeProfileAttribute]'
GO
CREATE TABLE [cm].[RG_Recovery_12_TypeProfileAttribute]
(
[ProfileAttributeKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProfileAttributeID] [int] NOT NULL,
[ProfileAttributeTypeKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EntityLevelKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_TypeProfileAttribute_DateUpdated] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_4efc_PK_TypeProfileAttribute] on [cm].[RG_Recovery_12_TypeProfileAttribute]'
GO
ALTER TABLE [cm].[RG_Recovery_12_TypeProfileAttribute] ADD CONSTRAINT [RG_Recovery_4efc_PK_TypeProfileAttribute] PRIMARY KEY CLUSTERED  ([ProfileAttributeKey]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [cm].[RG_Recovery_12_TypeProfileAttribute]([ProfileAttributeKey], [ProfileAttributeID], [ProfileAttributeTypeKey], [EntityLevelKey], [Description], [DateUpdated]) SELECT [ProfileAttributeKey], [ProfileAttributeID], [ProfileAttributeTypeKey], [EntityLevelKey], [Description], [DateUpdated] FROM [cm].[TypeProfileAttribute]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [cm].[TypeProfileAttribute]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[cm].[RG_Recovery_12_TypeProfileAttribute]', N'TypeProfileAttribute', N'OBJECT'
EXEC sp_rename N'[cm].[TypeProfileAttribute].[RG_Recovery_4efc_PK_TypeProfileAttribute]', N'PK_TypeProfileAttribute', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TypeProfileAttribute] on [cm].[TypeProfileAttribute]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TypeProfileAttribute] ON [cm].[TypeProfileAttribute] ([ProfileAttributeID]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [cm].[ClientProfileAttribute]'
GO
CREATE TABLE [cm].[RG_Recovery_13_ClientProfileAttribute]
(
[ClientProfileAttributeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ProfileAttributeKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MinValue] [int] NULL,
[MaxValue] [int] NULL,
[QuestionTextKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ClientProfileAttribute_QuestionTextKey] DEFAULT ('common.undefined'),
[DoNotDefault] [bit] NOT NULL,
[Disable] [bit] NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_ClientProfileAttribute_DateUpdated] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_2f26_PK_ClientProfileAttribute] on [cm].[RG_Recovery_13_ClientProfileAttribute]'
GO
ALTER TABLE [cm].[RG_Recovery_13_ClientProfileAttribute] ADD CONSTRAINT [RG_Recovery_2f26_PK_ClientProfileAttribute] PRIMARY KEY CLUSTERED  ([ClientProfileAttributeID]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_13_ClientProfileAttribute] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [cm].[RG_Recovery_13_ClientProfileAttribute]([ClientProfileAttributeID], [ClientID], [ProfileAttributeKey], [MinValue], [MaxValue], [QuestionTextKey], [DoNotDefault], [Disable], [DateUpdated]) SELECT [ClientProfileAttributeID], [ClientID], [ProfileAttributeKey], [MinValue], [MaxValue], [QuestionTextKey], [DoNotDefault], [Disable], [DateUpdated] FROM [cm].[ClientProfileAttribute]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [cm].[RG_Recovery_13_ClientProfileAttribute] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [cm].[ClientProfileAttribute]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[cm].[RG_Recovery_13_ClientProfileAttribute]', N'ClientProfileAttribute', N'OBJECT'
EXEC sp_rename N'[cm].[ClientProfileAttribute].[RG_Recovery_2f26_PK_ClientProfileAttribute]', N'PK_ClientProfileAttribute', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ClientProfileAttribute] on [cm].[ClientProfileAttribute]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ClientProfileAttribute] ON [cm].[ClientProfileAttribute] ([ClientID], [ProfileAttributeKey]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [cm].[vProfileAttributes]'
GO
EXEC sp_refreshview N'[cm].[vProfileAttributes]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [cm].[vProfileDefaults]'
GO



ALTER  VIEW [cm].[vProfileDefaults]
WITH SCHEMABINDING
AS
    SELECT  cpdc.[ClientID] ,
			tfd.[ProfileDefaultKey] ,
            cfd.[ProfileDefaultCollectionKey] ,
            cfd.[ProfileAttributeKey] ,
            cfd.[DefaultKey] ,
            ISNULL(cfd.[DefaultValue], '') AS DefaultValue
    FROM    [cm].[ClientProfileDefault] AS cfd WITH ( NOLOCK )
            RIGHT JOIN [cm].[TypeProfileDefault] AS tfd WITH ( NOLOCK ) ON tfd.ProfileDefaultKey = cfd.ProfileDefaultKey
			INNER JOIN [cm].[ClientProfileDefaultCollection] AS cpdc WITH (NOLOCK) ON cpdc.ProfileDefaultCollectionKey = cfd.ProfileDefaultCollectionKey AND cpdc.clientid = cfd.clientid



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [cm].[TypeProfileOption]'
GO
CREATE TABLE [cm].[RG_Recovery_14_TypeProfileOption]
(
[ProfileOptionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProfileOptionID] [int] NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_TypeProfileOption_DateUpdated] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_8ea6_PK_TypeProfileOption] on [cm].[RG_Recovery_14_TypeProfileOption]'
GO
ALTER TABLE [cm].[RG_Recovery_14_TypeProfileOption] ADD CONSTRAINT [RG_Recovery_8ea6_PK_TypeProfileOption] PRIMARY KEY CLUSTERED  ([ProfileOptionKey]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [cm].[RG_Recovery_14_TypeProfileOption]([ProfileOptionKey], [ProfileOptionID], [DateUpdated]) SELECT [ProfileOptionKey], [ProfileOptionID], [DateUpdated] FROM [cm].[TypeProfileOption]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [cm].[TypeProfileOption]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[cm].[RG_Recovery_14_TypeProfileOption]', N'TypeProfileOption', N'OBJECT'
EXEC sp_rename N'[cm].[TypeProfileOption].[RG_Recovery_8ea6_PK_TypeProfileOption]', N'PK_TypeProfileOption', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [cm].[vProfileOptions]'
GO


ALTER VIEW [cm].[vProfileOptions]
WITH SCHEMABINDING
AS
    SELECT  cpo.[ClientID] ,
            tpo.[ProfileOptionKey] ,
            cpo.[NameKey] ,
            cpo.[Description],
			cpo.[ProfileOptionValue] 
    FROM    [cm].[ClientProfileOption] AS cpo WITH ( NOLOCK )
	RIGHT JOIN [cm].[TypeProfileOption] AS tpo WITH ( NOLOCK ) ON tpo.ProfileOptionKey = cpo.ProfileOptionKey







GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [cm].[vProfileSections]'
GO
EXEC sp_refreshview N'[cm].[vProfileSections]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [cm].[vTabs]'
GO
EXEC sp_refreshview N'[cm].[vTabs]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [cm].[vTextContent]'
GO



ALTER VIEW [cm].[vTextContent]
WITH SCHEMABINDING
AS
    SELECT  ctc.[ClientID] ,
            ttc.[TextContentKey] ,
            ctc.[CategoryKey] ,
            ctclc.[ShortText] ,
            ctclc.[MediumText] ,
            ctclc.[LongText] ,
			ctclc.[RequireVariableSubstitution]
    FROM    [cm].[ClientTextContent] AS ctc WITH ( NOLOCK )
			RIGHT JOIN [cm].TypeTextContent AS ttc WITH ( NOLOCK ) ON ttc.TextContentKey = ctc.TextContentKey
            LEFT JOIN [cm].ClientTextContentLocaleContent AS ctclc WITH ( NOLOCK ) ON ctclc.ClientTextContentID = ctc.ClientTextContentID
    WHERE   ctclc.LocaleKey = 'en-US'








GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [cm].[vWidgets]'
GO
EXEC sp_refreshview N'[cm].[vWidgets]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [cm].[TypeAsset]'
GO
CREATE TABLE [cm].[RG_Recovery_15_TypeAsset]
(
[AssetKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AssetID] [int] NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_TypeAsset_DateUpdated] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_364f_PK_Asset] on [cm].[RG_Recovery_15_TypeAsset]'
GO
ALTER TABLE [cm].[RG_Recovery_15_TypeAsset] ADD CONSTRAINT [RG_Recovery_364f_PK_Asset] PRIMARY KEY CLUSTERED  ([AssetKey]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [cm].[RG_Recovery_15_TypeAsset]([AssetKey], [AssetID], [DateUpdated]) SELECT [AssetKey], [AssetID], [DateUpdated] FROM [cm].[TypeAsset]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [cm].[TypeAsset]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[cm].[RG_Recovery_15_TypeAsset]', N'TypeAsset', N'OBJECT'
EXEC sp_rename N'[cm].[TypeAsset].[RG_Recovery_364f_PK_Asset]', N'PK_Asset', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[vDimActionItem]'
GO
EXEC sp_refreshview N'[dbo].[vDimActionItem]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [cm].[TypeConfigurationBulk]'
GO
CREATE TABLE [cm].[RG_Recovery_16_TypeConfigurationBulk]
(
[ConfigurationBulkKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConfigurationID] [int] NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_TypeConfigurationBulk_DateUpdated] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [RG_Recovery_4f0b_PK_ConfigurationBulk] on [cm].[RG_Recovery_16_TypeConfigurationBulk]'
GO
ALTER TABLE [cm].[RG_Recovery_16_TypeConfigurationBulk] ADD CONSTRAINT [RG_Recovery_4f0b_PK_ConfigurationBulk] PRIMARY KEY CLUSTERED  ([ConfigurationBulkKey]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [cm].[RG_Recovery_16_TypeConfigurationBulk]([ConfigurationBulkKey], [ConfigurationID], [DateUpdated]) SELECT [ConfigurationBulkKey], [ConfigurationID], [DateUpdated] FROM [cm].[TypeConfigurationBulk]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [cm].[TypeConfigurationBulk]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[cm].[RG_Recovery_16_TypeConfigurationBulk]', N'TypeConfigurationBulk', N'OBJECT'
EXEC sp_rename N'[cm].[TypeConfigurationBulk].[RG_Recovery_4f0b_PK_ConfigurationBulk]', N'PK_ConfigurationBulk', N'INDEX'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[fnConvertJsonToXml]'
GO
CREATE FUNCTION [dbo].[fnConvertJsonToXml] ( @json VARCHAR(MAX) )
RETURNS XML
AS
    BEGIN;
        DECLARE @output VARCHAR(MAX) ,
            @key VARCHAR(MAX) ,
            @value VARCHAR(MAX) ,
            @recursion_counter INT ,
            @offset INT ,
            @nested BIT ,
            @array BIT ,
            @tab CHAR(1)= CHAR(9) ,
            @cr CHAR(1)= CHAR(13) ,
            @lf CHAR(1)= CHAR(10);

		--- Clean up the JSON syntax by removing line breaks and tabs and
		--- trimming the results of leading and trailing spaces:
        SET @json = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@json, @cr, ''), @lf,
                                                ''), @tab, '')));

		--- Sanity check: If this is not valid JSON syntax, exit here.
        IF ( LEFT(@json, 1) != '{'
             OR RIGHT(@json, 1) != '}'
           )
            RETURN '';

		--- Because the first and last characters will, by definition, be
		--- curly brackets, we can remove them here, and trim the result.
        SET @json = LTRIM(RTRIM(SUBSTRING(@json, 2, LEN(@json) - 2)));

        SELECT  @output = '';
        WHILE ( @json != '' )
            BEGIN;

				--- Look for the first key which should start with a quote.
                IF ( LEFT(@json, 1) != '"' )
                    RETURN 'Expected quote (start of key name). Found "'+
                LEFT(@json, 1)+'"';

				--- .. and end with the next quote (that isn't escaped with
				--- and backslash).
                SET @key = SUBSTRING(@json, 2,
                                     PATINDEX('%[^\\]"%',
                                              SUBSTRING(@json, 2, LEN(@json))
                                              + ' "'));

				--- Truncate @json with the length of the key.
                SET @json = LTRIM(SUBSTRING(@json, LEN(@key) + 3, LEN(@json)));

				--- The next character should be a colon.
                IF ( LEFT(@json, 1) != ':' )
                    RETURN 'Expected ":" after key name, found "'+
                LEFT(@json, 1)+'"!';

				--- Truncate @json to skip past the colon:
                SET @json = LTRIM(SUBSTRING(@json, 2, LEN(@json)));

				--- If the next character is an angle bracket, this is an array.
                IF ( LEFT(@json, 1) = '[' )
                    SELECT  @array = 1 ,
                            @json = LTRIM(SUBSTRING(@json, 2, LEN(@json)));

                IF ( @array IS NULL )
                    SET @array = 0;
                WHILE ( @array IS NOT NULL )
                    BEGIN;

                        SELECT  @value = NULL ,
                                @nested = 0;
						--- The first character of the remainder of @json indicates
						--- what type of value this is.

						--- Set @value, depending on what type of value we're looking at:
						---
						--- 1. A new JSON object:
						---    To be sent recursively back into the parser:
                        IF ( @value IS NULL
                             AND LEFT(@json, 1) = '{'
                           )
                            BEGIN;
                                SELECT  @recursion_counter = 1 ,
                                        @offset = 1;
                                WHILE ( @recursion_counter != 0
                                        AND @offset < LEN(@json)
                                      )
                                    BEGIN;
                                        SET @offset = @offset
                                            + PATINDEX('%[{}]%',
                                                       SUBSTRING(@json,
                                                              @offset + 1,
                                                              LEN(@json)));
                                        SET @recursion_counter = @recursion_counter
                                            + ( CASE SUBSTRING(@json, @offset,
                                                              1)
                                                  WHEN '{' THEN 1
                                                  WHEN '}' THEN -1
                                                END );
                                    END;

                                SET @value = CAST(dbo.fnConvertJsonToXml(LEFT(@json,
                                                              @offset)) AS VARCHAR(MAX));
                                SET @json = SUBSTRING(@json, @offset + 1,
                                                      LEN(@json));
                                SET @nested = 1;
                            END;

						--- 2a. Blank text (quoted)
                        IF ( @value IS NULL
                             AND LEFT(@json, 2) = '""'
                           )
                            SELECT  @value = '' ,
                                    @json = LTRIM(SUBSTRING(@json, 3,
                                                            LEN(@json)));

						--- 2b. Other text (quoted, but not blank)
                        IF ( @value IS NULL
                             AND LEFT(@json, 1) = '"'
                           )
                            BEGIN;
                                SET @value = SUBSTRING(@json, 2,
                                                       PATINDEX('%[^\\]"%',
                                                              SUBSTRING(@json,
                                                              2, LEN(@json))
                                                              + ' "'));
                                SET @json = LTRIM(SUBSTRING(@json,
                                                            LEN(@value) + 3,
                                                            LEN(@json)));
                            END;

						--- 3. Blank (not quoted)
                        IF ( @value IS NULL
                             AND LEFT(@json, 1) = ','
                           )
                            SET @value = '';

						--- 4. Or unescaped numbers or text.
                        IF ( @value IS NULL )
                            BEGIN;
                                SET @value = LEFT(@json,
                                                  PATINDEX('%[,}]%',
                                                           REPLACE(@json, ']',
                                                              '}') + '}') - 1);
                                SET @json = SUBSTRING(@json, LEN(@value) + 1,
                                                      LEN(@json));
                            END;

						--- Append @key and @value to @output:
                        SET @output = @output + @lf + @cr + REPLICATE(@tab,
                                                              @@NESTLEVEL - 1)
                            + '<' + @key + '>' + ISNULL(REPLACE(REPLACE(@value,
                                                              '\"', '"'), '\\',
                                                              '\'), '')
                            + ( CASE WHEN @nested = 1
                                     THEN @lf + @cr + REPLICATE(@tab,
                                                              @@NESTLEVEL - 1)
                                     ELSE ''
                                END ) + '</' + @key + '>';

						--- And again, error checks:
						---
						--- 1. If these are multiple values, the next character
						---    should be a comma:
                        IF ( @array = 0
                             AND @json != ''
                             AND LEFT(@json, 1) != ','
                           )
                            RETURN @output+'Expected "," after value, found "'+
                    LEFT(@json, 1)+'"!';

						--- 2. .. or, if this is an array, the next character
						--- should be a comma or a closing angle bracket:
                        IF ( @array = 1
                             AND LEFT(@json, 1) NOT IN ( ',', ']' )
                           )
                            RETURN @output+'In array, expected "]" or "," after '+
                    'value, found "'+LEFT(@json, 1)+'"!';

						--- If this is where the array is closed (i.e. if it's a
						--- closing angle bracket)..
                        IF ( @array = 1
                             AND LEFT(@json, 1) = ']'
                           )
                            BEGIN;
                                SET @array = NULL;
                                SET @json = LTRIM(SUBSTRING(@json, 2,
                                                            LEN(@json)));

								--- After a closed array, there should be a comma:
                                IF ( LEFT(@json, 1) NOT IN ( '', ',' ) )
                                    BEGIN
                                        RETURN 'Closed array, expected ","!';
                                    END;
                            END;

                        SET @json = LTRIM(SUBSTRING(@json, 2, LEN(@json) + 1));
                        IF ( @array = 0 )
                            SET @array = NULL;

                    END;
            END;

		--- Return the output:
        RETURN CAST(@output AS XML);
    END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [cm].[vConfigurationBulks]'
GO

Create VIEW [cm].[vConfigurationBulks]
AS
    SELECT  [ClientID] ,
            tc.[ConfigurationBulkKey] ,
			[Name] ,
            [CategoryKey] ,
            [Description] ,
			JSONConfiguration ,
			ISNULL(dbo.[fnConvertJsonToXml](JSONConfiguration), '') AS JSONConfigurationXML ,
            [EnvironmentKey]
    FROM    [cm].[ClientConfigurationBulk] AS cc WITH (NOLOCK)
	RIGHT JOIN [cm].[TypeConfigurationBulk] AS tc WITH (NOLOCK) ON tc.ConfigurationBulkKey = cc.ConfigurationBulkKey



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [cm].[uspUpdateClientAction]'
GO







-- =============================================
-- Author:		Srini Ambati
-- Create date: 01/01/2015
-- Description:	This table merges the data from contentful into SQL tables for use by BI
-- Usage Example:
/*
BEGIN TRANSACTION

	declare @p1 cm.ClientActionTable
	insert into @p1 values(0,N'getpoolcover',N'efficiency',0,0,N'gas',N'low',N'onetime', N'action.getpoolcover.name', N'action.getpoolcover.desc', 0.00, 0.00, 20.00, 16.00, N'therms', 0.00,0, 0.00, 0, 0.00, N'', N'', N'', N'', N'', NULL, N'pool.yes', N'pool', N'', 0, N'WhatIf', 0.55, N'test1')
	EXEC [cm].[uspUpdateClientAction] @clientActionTable=@p1

	SELECT * FROM cm.TypeAction
	SELECT * FROM [cm].[ClientAction]
	SELECT * FROM cm.ClientActionWhatIfData

ROLLBACK TRANSACTION
*/
-- =============================================
CREATE PROCEDURE [cm].[uspUpdateClientAction]
    (
      @clientActionTable [cm].[ClientActionTable] READONLY 
    )
AS
    BEGIN

	-- Step 1: Declara local variables and assign values
        DECLARE @maxTextContentID INT;
        DECLARE @maxActionID INT;
        DECLARE @currentDateTime DATETIME;

        SELECT  @maxTextContentID = ISNULL(MAX(TextContentID), 0) + 1
        FROM    [cm].[TypeTextContent] WITH ( NOLOCK );

        SELECT  @maxActionID = ISNULL(MAX(ActionID), 0) + 1
        FROM    [cm].[TypeAction] WITH ( NOLOCK );
		
        SET @currentDateTime = GETDATE();

        BEGIN TRANSACTION

        BEGIN TRY

			-- Step 2: Insert new TextContent keys 
            WITH    NewTextContent ( TextContentKey )
                      AS ( SELECT DISTINCT
                                    NameKey
                           FROM     @clientActionTable
                         )
                INSERT  INTO [cm].[TypeTextContent]
                        ( TextContentKey ,
                          TextContentID ,
                          DateUpdated
						)
                        SELECT  na.TextContentKey ,
                                ROW_NUMBER() OVER ( ORDER BY na.TextContentKey )
                                + @maxTextContentID ,
                                @currentDateTime
                        FROM    NewTextContent AS na WITH ( NOLOCK )
                                LEFT OUTER JOIN [cm].[TypeTextContent] AS ta
                                WITH ( NOLOCK ) ON na.TextContentKey = ta.TextContentKey
                        WHERE   ta.TextContentKey IS NULL

			-- Step 3: Insert new Action keys 
			;
            WITH    NewAction ( ActionKey )
                      AS ( SELECT DISTINCT
                                    ActionKey
                           FROM     @clientActionTable
                         )
                INSERT  INTO [cm].[TypeAction]
                        ( ActionKey ,
                          ActionID ,
                          DateUpdated
						)
                        SELECT  na.ActionKey ,
                                ROW_NUMBER() OVER ( ORDER BY na.ActionKey )
                                + @maxActionID ,
                                @currentDateTime
                        FROM    NewAction AS na WITH ( NOLOCK )
                                LEFT OUTER JOIN [cm].[TypeAction] AS ta WITH ( NOLOCK ) ON na.ActionKey = ta.ActionKey
                        WHERE   ta.ActionKey IS NULL

		-- Step 4: Merge Client Actions
            MERGE ClientAction AS TARGET
            USING @clientActionTable AS SOURCE
            ON ( TARGET.ClientID = SOURCE.ClientID
                 AND TARGET.ActionKey = SOURCE.ActionKey
               )
            WHEN MATCHED THEN
                UPDATE SET
					TARGET.[ActionTypeKey] = SOURCE.[ActionTypeKey] ,
					TARGET.[Hide] = SOURCE.[Hide] ,
					TARGET.[Disable] = SOURCE.[Disable] ,
					TARGET.[CommodityKey] = SOURCE.[CommodityKey] ,
					TARGET.[DifficultyKey] = SOURCE.[DifficultyKey] ,
					TARGET.[HabitIntervalKey] = SOURCE.[HabitIntervalKey] ,
					TARGET.[NameKey] = SOURCE.[NameKey] ,
					TARGET.[DescriptionKey] = SOURCE.[DescriptionKey] ,
					TARGET.[AnnualCost] = SOURCE.[AnnualCost] ,
					TARGET.[UpfrontCost] = SOURCE.[UpfrontCost] ,
					TARGET.[AnnualSavingsEstimate] = SOURCE.[AnnualSavingsEstimate] ,
					TARGET.[AnnualUsageSavingsEstimate] = SOURCE.[AnnualUsageSavingsEstimate] ,
					TARGET.[AnnualUsageSavingsUomKey] = SOURCE.[AnnualUsageSavingsUomKey] ,
					TARGET.[CostVariancePercent] = SOURCE.[CostVariancePercent] ,
					TARGET.[PaybackTime] = SOURCE.[PaybackTime] ,
					TARGET.[ROI] = SOURCE.[ROI] ,
					TARGET.[RebateAvailable] = SOURCE.[RebateAvailable] ,
					TARGET.[RebateAmount] = SOURCE.[RebateAmount] ,
					TARGET.[RebateUrl] = SOURCE.[RebateUrl] ,
					TARGET.[RebateImageKey] = SOURCE.[RebateImageKey] ,
					TARGET.[IconClass] = SOURCE.[IconClass] ,
					TARGET.[ImageKey] = SOURCE.[ImageKey] ,
					TARGET.[VideoKey] = SOURCE.[VideoKey] ,
					TARGET.[Comments] = SOURCE.[Comments] ,
					TARGET.[ActionPriority] = SOURCE.[ActionPriority] ,
					TARGET.[SavingsCalcMethod] = SOURCE.[SavingsCalcMethod] ,
					TARGET.[SavingsAmount] = SOURCE.[SavingsAmount] ,
					TARGET.[CostExpression] = SOURCE.[CostExpression] ,
					TARGET.[RebateExpression] = SOURCE.[RebateExpression] ,
					TARGET.[NextStepLink] = SOURCE.[NextStepLink] ,
					TARGET.[NextStepLinkText] = SOURCE.[NextStepLinkText] , 
					TARGET.[NextStepLinkType] = SOURCE.[NextStepLinkType] ,
					TARGET.[Tags] = SOURCE.[Tags]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( [ClientID] ,
                         [ActionKey] ,
                         [ActionTypeKey]	,
						[Hide]	,
						[Disable]	,
						[CommodityKey]	,
						[DifficultyKey]	,
						[HabitIntervalKey]	,
						[NameKey]	,
						[DescriptionKey]	,
						[AnnualCost]	,
						[UpfrontCost]	,
						[AnnualSavingsEstimate]	,
						[AnnualUsageSavingsEstimate]	,
						[AnnualUsageSavingsUomKey]	,
						[CostVariancePercent]	,
						[PaybackTime]	,
						[ROI]	,
						[RebateAvailable]	,
						[RebateAmount]	,
						[RebateUrl]	,
						[RebateImageKey]	,
						[IconClass]	,
						[ImageKey]	,
						[VideoKey]	,
						[Comments] ,
						[DateUpdated] , 
						[ActionPriority] ,
						[SavingsCalcMethod] ,
						[SavingsAmount] ,
						[CostExpression] ,
						[RebateExpression] ,
						[NextStepLink] ,
						[NextStepLinkText] , 
						[NextStepLinkType] ,
						[Tags]
					   )
                VALUES ( SOURCE.ClientID ,
                         SOURCE.ActionKey ,
                         SOURCE.[ActionTypeKey],
						SOURCE.[Hide],
						SOURCE.[Disable],
						SOURCE.[CommodityKey],
						SOURCE.[DifficultyKey],
						SOURCE.[HabitIntervalKey],
						SOURCE.[NameKey],
						SOURCE.[DescriptionKey],
						SOURCE.[AnnualCost],
						SOURCE.[UpfrontCost],
						SOURCE.[AnnualSavingsEstimate],
						SOURCE.[AnnualUsageSavingsEstimate],
						SOURCE.[AnnualUsageSavingsUomKey],
						SOURCE.[CostVariancePercent],
						SOURCE.[PaybackTime],
						SOURCE.[ROI],
						SOURCE.[RebateAvailable],
						SOURCE.[RebateAmount],
						SOURCE.[RebateUrl],
						SOURCE.[RebateImageKey],
						SOURCE.[IconClass],
						SOURCE.[ImageKey],
						SOURCE.[VideoKey],
						SOURCE.[Comments],
                         @currentDateTime,
						SOURCE.[ActionPriority] ,
						SOURCE.[SavingsCalcMethod] ,
						SOURCE.[SavingsAmount] ,
						SOURCE.[CostExpression] ,
						SOURCE.[RebateExpression] ,
						SOURCE.[NextStepLink] ,
						SOURCE.[NextStepLinkText] , 
						SOURCE.[NextStepLinkType] ,
						SOURCE.[Tags]
					   );

            INSERT  INTO cm.ContentOrphanLog
                    ( ClientID ,
                      Typename ,
                      [Key] ,
                      KeyValue
                    )
                    ( SELECT    ClientID ,
                                'Action' ,
                                'ActionKey' ,
                                ActionKey
                      FROM      cm.ClientAction
                      WHERE     ClientID = 0
                                AND ActionKey NOT IN (
                                SELECT  ActionKey
                                FROM    @clientActionTable )
                    );


		-- Step 5: Create temp tables

            CREATE TABLE #ActionConditions
                (
                  [ClientActionID] INT NOT NULL ,
                  [ConditionKey] VARCHAR(256) NOT NULL ,
                  [DisplayOrder] INT NOT NULL
                )
            ON  [PRIMARY]

            CREATE TABLE #ActionAppliances
                (
                  [ClientActionID] INT NOT NULL ,
                  [ApplianceKey] VARCHAR(256) NOT NULL ,
                  [DisplayOrder] INT NOT NULL
                )
            ON  [PRIMARY]

            CREATE TABLE #ActionSeason
                (
                  [ClientActionID] INT NOT NULL ,
                  [SeasonKey] VARCHAR(256) NOT NULL ,
                  [DisplayOrder] INT NOT NULL
                )
            ON  [PRIMARY]

            CREATE TABLE #ActionWhatIfData
                (
                  [ClientActionID] INT NOT NULL ,
                  [WhatIfDataKey] VARCHAR(256) NOT NULL ,
                  [DisplayOrder] INT NOT NULL
                )
            ON  [PRIMARY]

            DECLARE @Action_Cursor CURSOR
            DECLARE @clientID INT
            DECLARE @ActionKey VARCHAR(256)
            DECLARE @ConditionKeys VARCHAR(4000)
            DECLARE @ApplianceKeys VARCHAR(4000)
            DECLARE @SeasonKeys VARCHAR(4000)
			DECLARE @WhatIfDataKeys VARCHAR(4000)

            DECLARE @ConditionKey VARCHAR(256)
            DECLARE @Condition_Cursor CURSOR

            DECLARE @ApplianceKey VARCHAR(256)
            DECLARE @Appliance_Cursor CURSOR

            DECLARE @SeasonKey VARCHAR(256)
            DECLARE @Season_Cursor CURSOR

            DECLARE @WhatIfDataKey VARCHAR(256)
            DECLARE @WhatIfData_Cursor CURSOR

            DECLARE @DisplayOrder INT 
			--DECLARE @ClientActionId INT

            SET @Action_Cursor = CURSOR FOR
			SELECT ClientID, ActionKey, ConditionKeys, ApplianceKeys, SeasonKeys, WhatIfDataKeys FROM @clientActionTable
            OPEN @Action_Cursor
            FETCH NEXT
			FROM @Action_Cursor INTO @clientID, @ActionKey, @ConditionKeys,@ApplianceKeys, @SeasonKeys, @WhatIfDataKeys
            WHILE @@FETCH_STATUS = 0
                BEGIN
					--SET @ClientActionId = [cm].[fnGetClientActionID](@clientID,@ActionKey)
					
                    IF ( @ConditionKeys <> '' )
                        BEGIN 
						--SELECT @ConditionKeys
						-- insert into Condition temp table
                            SET @Condition_Cursor = CURSOR FOR
							SELECT [Data], [Id] FROM [cm].[fnSplitStringAsTable](@ConditionKeys, ';')
                            OPEN @Condition_Cursor 
                            FETCH NEXT
							FROM @Condition_Cursor INTO @ConditionKey, @DisplayOrder
                            WHILE @@FETCH_STATUS = 0
                                BEGIN
                                    IF ( LTRIM(RTRIM(@ConditionKey)) <> '' )
                                        BEGIN 
                                            INSERT  INTO #ActionConditions
                                                    ( [ClientActionID] ,
                                                      [ConditionKey] ,
                                                      [DisplayOrder]
													)
                                            VALUES  ( [cm].[fnGetClientActionID](@clientID,@ActionKey) ,
                                                      LTRIM(RTRIM(@ConditionKey)) ,
                                                      @DisplayOrder
													)
                                        END 
                                    FETCH NEXT
								FROM @Condition_Cursor INTO @ConditionKey, @DisplayOrder
                                END
                            CLOSE @Condition_Cursor
                            DEALLOCATE @Condition_Cursor
                        END 
					ELSE
						BEGIN
							DELETE FROM [cm].ClientActionCondition WHERE ClientActionID = [cm].[fnGetClientActionID](@clientID,@ActionKey)
						END

                    IF ( @ApplianceKeys <> '' )
                        BEGIN 
						--SELECT @ApplianceKeys
						-- insert into Appliance temp table
                            SET @Appliance_Cursor = CURSOR FOR
							SELECT [Data], [Id] FROM [cm].[fnSplitStringAsTable](@ApplianceKeys, ';')
                            OPEN @Appliance_Cursor
                            FETCH NEXT
							FROM @Appliance_Cursor INTO @ApplianceKey,@DisplayOrder
                            WHILE @@FETCH_STATUS = 0
                                BEGIN
                                    IF ( LTRIM(RTRIM(@ApplianceKey)) <> '' )
                                        BEGIN 
                                            INSERT  INTO #ActionAppliances
                                                    ( [ClientActionID] ,
                                                      [ApplianceKey] ,
                                                      [DisplayOrder]
													)
                                            VALUES  ( [cm].[fnGetClientActionID](@clientID,@ActionKey) ,
                                                      LTRIM(RTRIM(@ApplianceKey)) ,
                                                      @DisplayOrder
													)
                                        END 
                                    FETCH NEXT
								FROM @Appliance_Cursor INTO @ApplianceKey, @DisplayOrder
                                END
                            CLOSE @Appliance_Cursor
                            DEALLOCATE @Appliance_Cursor
                        END 
					ELSE
						BEGIN
							DELETE FROM [cm].ClientActionAppliance WHERE ClientActionID = [cm].[fnGetClientActionID](@clientID,@ActionKey)
						END

                    IF ( @SeasonKeys <> '' )
                        BEGIN 
						--SELECT @SeasonKeys
						-- insert into Season temp table
                            SET @Season_Cursor = CURSOR FOR
							SELECT [Data], [Id] FROM [cm].[fnSplitStringAsTable](@SeasonKeys, ';')
                            OPEN @Season_Cursor
                            FETCH NEXT
							FROM @Season_Cursor INTO @SeasonKey, @DisplayOrder
                            WHILE @@FETCH_STATUS = 0
                                BEGIN
                                    IF ( LTRIM(RTRIM(@SeasonKey)) <> '' )
                                        BEGIN 
                                            INSERT  INTO #ActionSeason
                                                    ( [ClientActionID] ,
                                                      [SeasonKey] ,
                                                      [DisplayOrder]
													)
                                            VALUES  ( [cm].[fnGetClientActionID](@clientID, @ActionKey) ,
                                                      LTRIM(RTRIM(@SeasonKey)) ,
                                                      @DisplayOrder
													)
                                        END 
                                    FETCH NEXT
								FROM @Season_Cursor INTO @SeasonKey, @DisplayOrder
                                END
                            CLOSE @Season_Cursor
                            DEALLOCATE @Season_Cursor
                        END 
					ELSE
						BEGIN
							DELETE FROM [cm].ClientActionSeason WHERE ClientActionID = [cm].[fnGetClientActionID](@clientID,@ActionKey)
						END

					 IF ( @WhatIfDataKeys <> '' )
                        BEGIN 
						--SELECT @WhatIfDataKeys
						-- insert into WhatIfData temp table
                            SET @WhatIfData_Cursor = CURSOR FOR
							SELECT [Data], [Id] FROM [cm].[fnSplitStringAsTable](@WhatIfDataKeys, ';')
                            OPEN @WhatIfData_Cursor
                            FETCH NEXT
							FROM @WhatIfData_Cursor INTO @WhatIfDataKey, @DisplayOrder
                            WHILE @@FETCH_STATUS = 0
                                BEGIN
                                    IF ( LTRIM(RTRIM(@WhatIfDataKey)) <> '' )
                                        BEGIN 
                                            INSERT  INTO #ActionWhatIfData
                                                    ( [ClientActionID] ,
                                                      [WhatIfDataKey] ,
                                                      [DisplayOrder]
													)
                                            VALUES  ( [cm].[fnGetClientActionID](@clientID, @ActionKey) ,
                                                      LTRIM(RTRIM(@WhatIfDataKey)) ,
                                                      @DisplayOrder
													)
                                        END 
                                    FETCH NEXT
								FROM @WhatIfData_Cursor INTO @WhatIfDataKey, @DisplayOrder
                                END
                            CLOSE @WhatIfData_Cursor
                            DEALLOCATE @WhatIfData_Cursor
                        END 
					ELSE
						BEGIN
							DELETE FROM [cm].ClientActionWhatIfData WHERE ClientActionID = [cm].[fnGetClientActionID](@clientID,@ActionKey)
						END


                    FETCH NEXT
					FROM @Action_Cursor INTO @clientID, @ActionKey, @ConditionKeys, @ApplianceKeys, @SeasonKeys, @WhatIfDataKeys
                END
            CLOSE @Action_Cursor
            DEALLOCATE @Action_Cursor
		
            SELECT  [ClientActionID] ,
                    [ConditionKey] AS MissingConditionKey
            FROM    #ActionConditions
            WHERE   ConditionKey NOT IN ( SELECT    ConditionKey
                                          FROM      cm.TypeCondition )

			-- Step 6: Merge Client Action Profiles Conditions
            MERGE [cm].ClientActionCondition AS t
            USING
                ( SELECT DISTINCT
                            ClientActionID ,
                            ConditionKey ,
                            [DisplayOrder]
                  FROM      #ActionConditions
                ) AS s
            ON ( t.ClientActionID = s.ClientActionID
                 AND t.ConditionKey = s.ConditionKey
               )
            WHEN MATCHED THEN
                UPDATE SET
                         t.[DisplayOrder] = s.[DisplayOrder]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( ClientActionID ,
                         ConditionKey 
					   )
                VALUES ( s.ClientActionID ,
                         s.ConditionKey 
					   )
            WHEN NOT MATCHED BY SOURCE AND t.ClientActionID IN (SELECT ClientActionID FROM #ActionConditions) THEN
                DELETE;

            SELECT  [ClientActionID] ,
                    [ApplianceKey] AS MissingApplianceKey
            FROM    #ActionAppliances
            WHERE   ApplianceKey NOT IN ( SELECT    ApplianceKey
                                              FROM      cm.TypeAppliance )

			-- Step 7: Merge Client Action Profiles Appliances
            MERGE [cm].ClientActionAppliance AS t
            USING
                ( SELECT DISTINCT
                            ClientActionID ,
                            ApplianceKey ,
                            [DisplayOrder]
                  FROM      #ActionAppliances
                ) AS s
            ON ( t.ClientActionID = s.ClientActionID
                 AND t.ApplianceKey = s.ApplianceKey
               )
            WHEN MATCHED THEN
                UPDATE SET
                         t.[DisplayOrder] = s.[DisplayOrder]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( ClientActionID ,
                         ApplianceKey 
					   )
                VALUES ( s.ClientActionID ,
                         s.ApplianceKey 
					   )
            WHEN NOT MATCHED BY SOURCE AND t.ClientActionID IN (SELECT ClientActionID FROM #ActionAppliances) THEN
                DELETE;

            SELECT  [ClientActionID] ,
                    [SeasonKey] AS MissingSeasonKey
            FROM    #ActionSeason
            WHERE   SeasonKey NOT IN ( SELECT  SeasonKey
                                            FROM    cm.TypeSeason )

			-- Step 8: Merge Client Action Profiles Seasons
            MERGE [cm].ClientActionSeason AS t
            USING
                ( SELECT DISTINCT
                            ClientActionID ,
                            SeasonKey ,
                            [DisplayOrder]
                  FROM      #ActionSeason
                ) AS s
            ON ( t.ClientActionID = s.ClientActionID
                 AND t.SeasonKey = s.SeasonKey
               )
            WHEN MATCHED THEN
                UPDATE SET
                         t.[DisplayOrder] = s.[DisplayOrder]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( ClientActionID ,
                         SeasonKey 
					   )
                VALUES ( s.ClientActionID ,
                         s.SeasonKey 
					   )
            WHEN NOT MATCHED BY SOURCE AND t.ClientActionID IN (SELECT ClientActionID FROM #ActionSeason) THEN
                DELETE;

			SELECT  [ClientActionID] ,
                    [WhatIfDataKey] AS MissingWhatIfDataKey
            FROM    #ActionWhatIfData
            WHERE   WhatIfDataKey NOT IN ( SELECT  WhatIfDataKey
                                            FROM    cm.TypeWhatIfData )

			-- Step 8: Merge ClientActionWhatIfData
            MERGE [cm].ClientActionWhatIfData AS t
            USING
                ( SELECT DISTINCT
                            ClientActionID ,
                            WhatIfDataKey ,
                            [DisplayOrder]
                  FROM      #ActionWhatIfData
                ) AS s
            ON ( t.ClientActionID = s.ClientActionID
                 AND t.WhatIfDataKey = s.WhatIfDataKey
               )
            WHEN MATCHED THEN
                UPDATE SET
                         t.[DisplayOrder] = s.[DisplayOrder]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( ClientActionID ,
                         WhatIfDataKey 
					   )
                VALUES ( s.ClientActionID ,
                         s.WhatIfDataKey 
					   )
            WHEN NOT MATCHED BY SOURCE AND t.ClientActionID IN (SELECT ClientActionID FROM #ActionWhatIfData) THEN
                DELETE;

        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                ROLLBACK TRANSACTION;

            THROW;

        END CATCH;

        IF @@TRANCOUNT > 0
            COMMIT TRANSACTION;

    END






GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [cm].[uspSelectContentClientAction]'
GO

-- =============================================
-- Author:		Srini Ambati
-- Create date: 07/09/2014
-- Description:	This proc returns Action Content specific to client
-- Usage Example:
/*
BEGIN TRANSACTION

EXEC [cm].[uspSelectContentClientAction] 87

select * from cm.ClientAction

ROLLBACK TRANSACTION
*/
-- =============================================

ALTER PROCEDURE [cm].[uspSelectContentClientAction]
    (
      @clientID INT
    )
AS
    BEGIN
        SELECT  ca.ActionKey AS [Key],
				ca.NameKey,
                ISNULL([cm].[fnGetActionFieldValues](ca.ClientActionID, 'ClientActionCondition'), '') AS ConditionKeys ,
				ca.DescriptionKey ,
                ca.Hide ,
                ISNULL(ca.VideoKey, '') AS VideoKey ,
                ISNULL(ca.ImageKey, '') AS ImageKey ,
                ISNULL(ca.IconClass, '') AS IconClass ,
                ca.CommodityKey ,
                ca.ActionTypeKey AS [Type] ,
                [cm].[fnGetActionFieldValues](ca.ClientActionID, 'ClientActionAppliance') AS ApplianceKeys ,
                ca.UpfrontCost AS DefaultUpfrontCost,
                ca.AnnualCost AS DefaultAnnualCost,
                ca.CostVariancePercent AS DefaultCostVariancePercent,
                ca.DifficultyKey AS Difficulty ,
                ca.HabitIntervalKey AS HabitInterval ,
                ca.AnnualSavingsEstimate AS DefaultAnnualSavingsEstimate,
                ca.AnnualUsageSavingsEstimate AS DefaultAnnualUsageSavingsEstimate ,
                ca.AnnualUsageSavingsUomKey AS DefaultAnnualUsageSavingsUomKey,
                ca.PaybackTime AS DefaultPaybackTime,
                ca.ROI AS [DefaultRoi],
                ca.RebateAvailable ,
                ISNULL(ca.RebateUrl, '') AS RebateUrl , 
                ISNULL(ca.RebateImageKey, '') AS RebateImageKey ,
                ca.RebateAmount ,
                ISNULL([cm].[fnGetActionFieldValues](ca.ClientActionID, 'ClientActionSeason'), '') AS SeasonKeys ,
				ca.ActionPriority , 
				ca.SavingsCalcMethod , 
				SavingsAmount ,
				ISNULL([cm].[fnGetActionFieldValues](ca.ClientActionID, 'ClientActionWhatIfData'), '') AS WhatIfDataKeys , 
				ca.CostExpression ,
				ca.RebateExpression ,
				ISNULL(ca.[NextStepLink], '') AS NextStepLink ,
				ISNULL(ca.[NextStepLinkText], '') AS NextStepLinkText ,
				ISNULL(ca.[NextStepLinkType], '') AS NextStepLinkType ,
				ISNULL(ca.[Tags], '') AS Tags
        FROM    cm.ClientAction AS ca WITH ( NOLOCK )
                INNER JOIN ( SELECT MAX(clientid) AS clientid ,
                                    ActionKey
                             FROM   cm.ClientAction WITH ( NOLOCK )
                             WHERE  clientid IN ( 0, @clientID )
                             GROUP BY ActionKey
                           ) AS bw ON bw.clientid = ca.ClientID
                                      AND bw.ActionKey = ca.ActionKey
        WHERE   [Disable] = 0
        ORDER BY ca.[ActionKey]

    END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[getETFilesToImport]'
GO

-- =============================================
-- Author:		ECormier
-- Description:	gets configurations for Exact Target import files for ImportExactTarget_Daily ssis package
-- Create date:	7/2015
-- Update date: 8/4/205  ECormier - add paasPassword column to @tmpFilesToImport temp table because that has been added to GetComponentConfiguration sp
-- =============================================

ALTER procedure [dbo].[getETFilesToImport] @clientID int
as

set nocount on

/*
-- for debug
declare @clientID int
select @clientID = 101
*/

-- 7/21/2015 - move setting date and appending date to file names out of ssis package in into sp
declare @dateToUse date
select @dateToUse = dateadd(dd,-1,getdate())


declare @addDateExt varchar(20)
declare @AddYear varchar(4)
declare @AddMonth varchar(3)
declare @AddDay varchar(3)

-- set year string
select @AddYear = (select convert(varchar(4),year(@dateToUse)))

-- set month string
select @AddMonth = (select convert(varchar(2),month(@dateToUse)))
if len(@AddMonth) = 1 select @AddMonth = '0' + @AddMonth

-- set day string
select @AddDay = (select convert(varchar(2),day(@dateToUse)))
if len(@AddDay) = 1 select @AddDay = '0' + @AddDay

-- put them together to get the date suffix to add to file names
select @addDateExt = @AddYear + @AddMonth + @AddDay

-- create a temp table to hold the configurations for files to be imported
	declare @tmpFilesToImport table
	(ETEventsArchiveFolder varchar(255),
	ETEventsDestExtension varchar(10),	
	ETEventsDestFileName varchar(50),	
	ETEventsDestFilePath varchar(255),	
	ETEventsDestTable varchar(25),	
	ETEventsSrcExtension varchar(10),	
	ETEventsSrcFileName varchar(50),	
	ETEventsSrcFilePath varchar(255),	
	ETEventsSrcPassword varchar(25),	
	ETEventsSrcServer varchar(50),	
	ETEventsSrcUserName varchar(25),
	ETEventsTransExtension varchar(10),
	ETEventsTransFileName varchar(50),
	ETEventsTransFilePath varchar(255),
	paasPassword varchar(50),		
	reportmonth varchar(5), 	
	reportyear varchar(5),	
	sharedlocation varchar(255))

	-- get the configurations for the ClickOpen Events file
	insert into @tmpFilesToImport 
	EXECUTE [dbo].[GetComponentConfiguration] @clientID, 'ImportETEventsClickOpen'

	-- get the configurations for the BouncOptout Events file
	insert into @tmpFilesToImport 
	EXECUTE [dbo].[GetComponentConfiguration] @clientID, 'ImportETEventsBounce'

	-- update the file names to add the date extentions where needed
	update @tmpFilesToImport
	set ETEventsSrcFileName = case when ETEventsDestTable = 'ETClickOpen' then ETEventsSrcFileName + @addDateExt else ETEventsSrcFileName end,
		ETEventsDestFileName = ETEventsDestFileName + @addDateExt + '_' + convert(varchar(10),@clientID),
		ETEventsTransFileName = ETEventsTransFileName + @addDateExt + '_' + convert(varchar(10),@clientID)

	-- return the results of query to ssis package 
	select * from @tmpFilesToImport 

set nocount off
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [cm].[TypeNextStepLinkType]'
GO
CREATE TABLE [cm].[TypeNextStepLinkType]
(
[NextStepLinkTypeKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NextStepLinkTypeID] [int] NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_TypeNextStepLinkType_DateUpdated] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_TypeNextStepLinkType] on [cm].[TypeNextStepLinkType]'
GO
ALTER TABLE [cm].[TypeNextStepLinkType] ADD CONSTRAINT [PK_TypeNextStepLinkType] PRIMARY KEY CLUSTERED  ([NextStepLinkTypeKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_INV_FactPremiseAttribute_3] on [ETL].[INV_FactPremiseAttribute]'
GO
CREATE NONCLUSTERED INDEX [IX_INV_FactPremiseAttribute_3] ON [ETL].[INV_FactPremiseAttribute] ([TrackingDate]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientActionSavings]'
GO
ALTER TABLE [cm].[ClientActionSavings] WITH NOCHECK  ADD CONSTRAINT [FK_ClientActionSavings_TypeAction] FOREIGN KEY ([ActionKey]) REFERENCES [cm].[TypeAction] ([ActionKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientActionAppliance]'
GO
ALTER TABLE [cm].[ClientActionAppliance] WITH NOCHECK  ADD CONSTRAINT [FK_ClientActionAppliance_ClientAction] FOREIGN KEY ([ClientActionID]) REFERENCES [cm].[ClientAction] ([ClientActionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientActionCondition]'
GO
ALTER TABLE [cm].[ClientActionCondition] WITH NOCHECK  ADD CONSTRAINT [FK_ClientActionCondition_ClientAction] FOREIGN KEY ([ClientActionID]) REFERENCES [cm].[ClientAction] ([ClientActionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientActionSeason]'
GO
ALTER TABLE [cm].[ClientActionSeason] WITH NOCHECK  ADD CONSTRAINT [FK_ClientActionSeasonality_ClientAction] FOREIGN KEY ([ClientActionID]) REFERENCES [cm].[ClientAction] ([ClientActionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientActionWhatIfData]'
GO
ALTER TABLE [cm].[ClientActionWhatIfData] WITH NOCHECK  ADD CONSTRAINT [FK_ClientActionWhatIfData_ClientAction] FOREIGN KEY ([ClientActionID]) REFERENCES [cm].[ClientAction] ([ClientActionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientCondition]'
GO
ALTER TABLE [cm].[ClientCondition] WITH NOCHECK  ADD CONSTRAINT [FK_ClientCondition_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientConfigurationBulk]'
GO
ALTER TABLE [cm].[ClientConfigurationBulk] WITH NOCHECK  ADD CONSTRAINT [FK_ClientConfigurationBulk_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientConfiguration]'
GO
ALTER TABLE [cm].[ClientConfiguration] WITH NOCHECK  ADD CONSTRAINT [FK_ClientConfiguration_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientFileContent]'
GO
ALTER TABLE [cm].[ClientFileContent] WITH NOCHECK  ADD CONSTRAINT [FK_ClientFileContent_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientProfileAttribute]'
GO
ALTER TABLE [cm].[ClientProfileAttribute] WITH NOCHECK  ADD CONSTRAINT [FK_ClientProfileAttribute_TypeTextContent] FOREIGN KEY ([QuestionTextKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientProfileOption]'
GO
ALTER TABLE [cm].[ClientProfileOption] WITH NOCHECK  ADD CONSTRAINT [FK_ClientProfileOption_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientProfileSection]'
GO
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK  ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent] FOREIGN KEY ([DescriptionKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK  ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent3] FOREIGN KEY ([IntroTextKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK  ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent2] FOREIGN KEY ([SubTitleKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK  ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent1] FOREIGN KEY ([TitleKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientSeason]'
GO
ALTER TABLE [cm].[ClientSeason] WITH NOCHECK  ADD CONSTRAINT [FK_ClientSeason_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientTabAction]'
GO
ALTER TABLE [cm].[ClientTabAction] WITH NOCHECK  ADD CONSTRAINT [FK_ClientTabAction_TypeAction] FOREIGN KEY ([ActionKey]) REFERENCES [cm].[TypeAction] ([ActionKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientTab]'
GO
ALTER TABLE [cm].[ClientTab] WITH NOCHECK  ADD CONSTRAINT [FK_ClientTab_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientWidgetCondition]'
GO
ALTER TABLE [cm].[ClientWidgetCondition] WITH NOCHECK  ADD CONSTRAINT [FK_ClientWidgetConditionality_ClientWidget] FOREIGN KEY ([ClientWidgetID]) REFERENCES [cm].[ClientWidget] ([ClientWidgetID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientWidgetTextContent]'
GO
ALTER TABLE [cm].[ClientWidgetTextContent] WITH NOCHECK  ADD CONSTRAINT [FK_ClientWidgetTextContentality_ClientWidget] FOREIGN KEY ([ClientWidgetID]) REFERENCES [cm].[ClientWidget] ([ClientWidgetID])
ALTER TABLE [cm].[ClientWidgetTextContent] WITH NOCHECK  ADD CONSTRAINT [FK_ClientWidgetTextContentality_TypeTextContentality] FOREIGN KEY ([TextContentKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientWidget]'
GO
ALTER TABLE [cm].[ClientWidget] WITH NOCHECK  ADD CONSTRAINT [FK_ClientWidget_TypeTextContent] FOREIGN KEY ([IntroTextKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[TypeEnumeration]'
GO
ALTER TABLE [cm].[TypeEnumeration] WITH NOCHECK  ADD CONSTRAINT [FK_TypeEnumeration_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientAppliance]'
GO
ALTER TABLE [cm].[ClientAppliance] WITH NOCHECK  ADD CONSTRAINT [FK_ClientAppliance_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientCommodity]'
GO
ALTER TABLE [cm].[ClientCommodity] WITH NOCHECK  ADD CONSTRAINT [FK_ClientCommodity_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientCurrency]'
GO
ALTER TABLE [cm].[ClientCurrency] WITH NOCHECK  ADD CONSTRAINT [FK_ClientCurrency_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientCurrency] WITH NOCHECK  ADD CONSTRAINT [FK_ClientCurrency_TypeTextContent1] FOREIGN KEY ([SymbolKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientEnduse]'
GO
ALTER TABLE [cm].[ClientEnduse] WITH NOCHECK  ADD CONSTRAINT [FK_ClientEnduse_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientMeasurement]'
GO
ALTER TABLE [cm].[ClientMeasurement] WITH NOCHECK  ADD CONSTRAINT [FK_ClientMeasurement_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientTabTextContent]'
GO
ALTER TABLE [cm].[ClientTabTextContent] WITH NOCHECK  ADD CONSTRAINT [FK_ClientTabTextContentality_TypeTextContentality] FOREIGN KEY ([TextContentKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientUOM]'
GO
ALTER TABLE [cm].[ClientUOM] WITH NOCHECK  ADD CONSTRAINT [FK_ClientUOM_ClientUOM1] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientActionSavings]'
GO
ALTER TABLE [cm].[ClientActionSavings] ADD CONSTRAINT [FK_ClientActionSavings_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientActionSavingsCondition]'
GO
ALTER TABLE [cm].[ClientActionSavingsCondition] ADD CONSTRAINT [FK_ClientActionSavingsCondition_ClientActionSavings] FOREIGN KEY ([ClientActionSavingsID]) REFERENCES [cm].[ClientActionSavings] ([ClientActionSavingsID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientAction]'
GO
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeAction] FOREIGN KEY ([ActionKey]) REFERENCES [cm].[TypeAction] ([ActionKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeActionType] FOREIGN KEY ([ActionTypeKey]) REFERENCES [cm].[TypeActionType] ([ActionTypeKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeUom] FOREIGN KEY ([AnnualUsageSavingsUomKey]) REFERENCES [cm].[TypeUom] ([UomKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeUom1] FOREIGN KEY ([AnnualUsageSavingsUomKey]) REFERENCES [cm].[TypeUom] ([UomKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeCommodity] FOREIGN KEY ([CommodityKey]) REFERENCES [cm].[TypeCommodity] ([CommodityKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeExpression] FOREIGN KEY ([CostExpression]) REFERENCES [cm].[TypeExpression] ([ExpressionKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeDifficulty] FOREIGN KEY ([DifficultyKey]) REFERENCES [cm].[TypeDifficulty] ([DifficultyKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeHabitInterval] FOREIGN KEY ([HabitIntervalKey]) REFERENCES [cm].[TypeHabitInterval] ([HabitIntervalKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_ClientFileContent2] FOREIGN KEY ([ImageKey]) REFERENCES [cm].[TypeFileContent] ([FileContentKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeTextContent1] FOREIGN KEY ([NextStepLinkText]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeNextStepLinkType] FOREIGN KEY ([NextStepLinkType]) REFERENCES [cm].[TypeNextStepLinkType] ([NextStepLinkTypeKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeExpression1] FOREIGN KEY ([RebateExpression]) REFERENCES [cm].[TypeExpression] ([ExpressionKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_ClientFileContent] FOREIGN KEY ([RebateImageKey]) REFERENCES [cm].[TypeFileContent] ([FileContentKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_ClientFileContent3] FOREIGN KEY ([VideoKey]) REFERENCES [cm].[TypeFileContent] ([FileContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientApplianceExpression]'
GO
ALTER TABLE [cm].[ClientApplianceExpression] ADD CONSTRAINT [FK_ClientApplianceExpression_TypeExpression] FOREIGN KEY ([ExpressionKey]) REFERENCES [cm].[TypeExpression] ([ExpressionKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientApplianceProfileAttribute]'
GO
ALTER TABLE [cm].[ClientApplianceProfileAttribute] ADD CONSTRAINT [FK_ClientApplianceProfileAttribute_TypeProfileAttribute] FOREIGN KEY ([ProfileAttributeKey]) REFERENCES [cm].[TypeProfileAttribute] ([ProfileAttributeKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientAsset]'
GO
ALTER TABLE [cm].[ClientAsset] ADD CONSTRAINT [FK_ClientAsset_TypeAsset] FOREIGN KEY ([AssetKey]) REFERENCES [cm].[TypeAsset] ([AssetKey])
ALTER TABLE [cm].[ClientAsset] ADD CONSTRAINT [FK_ClientAsset_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientAssetLocaleContent]'
GO
ALTER TABLE [cm].[ClientAssetLocaleContent] ADD CONSTRAINT [FK_ClientAssetLocaleContent_ClientAsset] FOREIGN KEY ([ClientAssetID]) REFERENCES [cm].[ClientAsset] ([ClientAssetID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientEnumerationEnumerationItem]'
GO
ALTER TABLE [cm].[ClientEnumerationEnumerationItem] ADD CONSTRAINT [FK_ClientEnumerationEnumerationItem_ClientEnumeration] FOREIGN KEY ([ClientEnumerationID]) REFERENCES [cm].[ClientEnumeration] ([ClientEnumerationID])
ALTER TABLE [cm].[ClientEnumerationEnumerationItem] ADD CONSTRAINT [FK_ClientEnumerationEnumerationItem_TypeEnumerationItem] FOREIGN KEY ([EnumerationItemKey]) REFERENCES [cm].[TypeEnumerationItem] ([EnumerationItemKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientEnumerationItem]'
GO
ALTER TABLE [cm].[ClientEnumerationItem] ADD CONSTRAINT [FK_ClientEnumerationItem_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientEnumerationItem] ADD CONSTRAINT [FK_ClientEnumerationItem_TypeEnumerationItem] FOREIGN KEY ([EnumerationItemKey]) REFERENCES [cm].[TypeEnumerationItem] ([EnumerationItemKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientExpression]'
GO
ALTER TABLE [cm].[ClientExpression] ADD CONSTRAINT [FK_ClientExpression_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientExpression] ADD CONSTRAINT [FK_ClientExpression_TypeExpression] FOREIGN KEY ([ExpressionKey]) REFERENCES [cm].[TypeExpression] ([ExpressionKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientProfileAttribute]'
GO
ALTER TABLE [cm].[ClientProfileAttribute] ADD CONSTRAINT [FK_ClientProfileAttribute_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientProfileAttribute] ADD CONSTRAINT [FK_ClientProfileAttribute_TypeProfileAttribute] FOREIGN KEY ([ProfileAttributeKey]) REFERENCES [cm].[TypeProfileAttribute] ([ProfileAttributeKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientProfileAttributeCondition]'
GO
ALTER TABLE [cm].[ClientProfileAttributeCondition] ADD CONSTRAINT [FK_ClientProfileAttributeCondition_ClientProfileAttribute] FOREIGN KEY ([ClientProfileAttributeID]) REFERENCES [cm].[ClientProfileAttribute] ([ClientProfileAttributeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientProfileAttributeProfileOption]'
GO
ALTER TABLE [cm].[ClientProfileAttributeProfileOption] ADD CONSTRAINT [FK_ClientProfileAttributeProfileOption_ClientProfileAttribute] FOREIGN KEY ([ClientProfileAttributeID]) REFERENCES [cm].[ClientProfileAttribute] ([ClientProfileAttributeID])
ALTER TABLE [cm].[ClientProfileAttributeProfileOption] ADD CONSTRAINT [FK_ClientProfileAttributeProfileOption_TypeProfileOption] FOREIGN KEY ([ProfileOptionKey]) REFERENCES [cm].[TypeProfileOption] ([ProfileOptionKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientProfileDefault]'
GO
ALTER TABLE [cm].[ClientProfileDefault] ADD CONSTRAINT [FK_ClientProfileDefault_TypeProfileOption] FOREIGN KEY ([DefaultKey]) REFERENCES [cm].[TypeProfileOption] ([ProfileOptionKey])
ALTER TABLE [cm].[ClientProfileDefault] ADD CONSTRAINT [FK_ClientProfileDefault_TypeProfileAttribute] FOREIGN KEY ([ProfileAttributeKey]) REFERENCES [cm].[TypeProfileAttribute] ([ProfileAttributeKey])
ALTER TABLE [cm].[ClientProfileDefault] ADD CONSTRAINT [FK_ClientProfileDefault_TypeProfileDefaultCollection] FOREIGN KEY ([ProfileDefaultCollectionKey]) REFERENCES [cm].[TypeProfileDefaultCollection] ([ProfileDefaultCollectionKey])
ALTER TABLE [cm].[ClientProfileDefault] ADD CONSTRAINT [FK_ClientProfileDefault_TypeProfileDefault] FOREIGN KEY ([ProfileDefaultKey]) REFERENCES [cm].[TypeProfileDefault] ([ProfileDefaultKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientTabCondition]'
GO
ALTER TABLE [cm].[ClientTabCondition] ADD CONSTRAINT [FK_ClientTabConditionality_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])
ALTER TABLE [cm].[ClientTabCondition] ADD CONSTRAINT [FK_ClientTabConditionality_TypeConditionality] FOREIGN KEY ([ConditionKey]) REFERENCES [cm].[TypeCondition] ([ConditionKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientTabConfiguration]'
GO
ALTER TABLE [cm].[ClientTabConfiguration] ADD CONSTRAINT [FK_ClientTabConfigurationality_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientTabTextContent]'
GO
ALTER TABLE [cm].[ClientTabTextContent] ADD CONSTRAINT [FK_ClientTabTextContentality_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientTextContent]'
GO
ALTER TABLE [cm].[ClientTextContent] ADD CONSTRAINT [FK_ClientTextContent_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
ALTER TABLE [cm].[ClientTextContent] ADD CONSTRAINT [FK_ClientTextContent_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientTextContent] ADD CONSTRAINT [FK_ClientTextContent_TypeTextContent] FOREIGN KEY ([TextContentKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientTextContentLocaleContent]'
GO
ALTER TABLE [cm].[ClientTextContentLocaleContent] ADD CONSTRAINT [FK_ClientTextContentLocaleContent_ClientTextContent] FOREIGN KEY ([ClientTextContentID]) REFERENCES [cm].[ClientTextContent] ([ClientTextContentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientFileContent]'
GO
ALTER TABLE [cm].[ClientFileContent] ADD CONSTRAINT [FK_ClientFileContentLargeFile_TypeAsset] FOREIGN KEY ([LargeFile]) REFERENCES [cm].[TypeAsset] ([AssetKey])
ALTER TABLE [cm].[ClientFileContent] ADD CONSTRAINT [FK_ClientFileContentMediumFile_TypeAsset] FOREIGN KEY ([MediumFile]) REFERENCES [cm].[TypeAsset] ([AssetKey])
ALTER TABLE [cm].[ClientFileContent] ADD CONSTRAINT [FK_ClientFileContentSmallFile_TypeAsset] FOREIGN KEY ([SmallFile]) REFERENCES [cm].[TypeAsset] ([AssetKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientConfigurationBulk]'
GO
ALTER TABLE [cm].[ClientConfigurationBulk] ADD CONSTRAINT [FK_ClientConfigurationBulk_TypeConfigurationBulk] FOREIGN KEY ([ConfigurationBulkKey]) REFERENCES [cm].[TypeConfigurationBulk] ([ConfigurationBulkKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[TypeProfileAttribute]'
GO
ALTER TABLE [cm].[TypeProfileAttribute] ADD CONSTRAINT [FK_TypeProfileAttribute_TypeEntityLevel] FOREIGN KEY ([EntityLevelKey]) REFERENCES [cm].[TypeEntityLevel] ([EntityLevelKey])
ALTER TABLE [cm].[TypeProfileAttribute] ADD CONSTRAINT [FK_TypeProfileAttribute_TypeProfileAttributeType] FOREIGN KEY ([ProfileAttributeTypeKey]) REFERENCES [cm].[TypeProfileAttributeType] ([ProfileAttributeTypeKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientCondition]'
GO
ALTER TABLE [cm].[ClientCondition] ADD CONSTRAINT [FK_ClientCondition_TypeProfileAttribute] FOREIGN KEY ([ProfileAttributeKey]) REFERENCES [cm].[TypeProfileAttribute] ([ProfileAttributeKey])
ALTER TABLE [cm].[ClientCondition] ADD CONSTRAINT [FK_ClientCondition_TypeProfileOption] FOREIGN KEY ([ProfileOptionKey]) REFERENCES [cm].[TypeProfileOption] ([ProfileOptionKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientProfileSectionAttribute]'
GO
ALTER TABLE [cm].[ClientProfileSectionAttribute] ADD CONSTRAINT [FK_ClientProfileSectionAttribute_TypeProfileAttribute] FOREIGN KEY ([ProfileAttributeKey]) REFERENCES [cm].[TypeProfileAttribute] ([ProfileAttributeKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientWhatIfData]'
GO
ALTER TABLE [cm].[ClientWhatIfData] ADD CONSTRAINT [FK_ClientWhatIfData_TypeProfileAttribute] FOREIGN KEY ([ProfileAttributeKey]) REFERENCES [cm].[TypeProfileAttribute] ([ProfileAttributeKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[ClientProfileOption]'
GO
ALTER TABLE [cm].[ClientProfileOption] ADD CONSTRAINT [FK_ClientProfileOption_TypeProfileOption] FOREIGN KEY ([ProfileOptionKey]) REFERENCES [cm].[TypeProfileOption] ([ProfileOptionKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
