﻿/*** Run against all InsightsDW_xxx shards ***/

/*** Add the DimServiceContractType types from Aclara One ***/
IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
       WHERE TABLE_SCHEMA = 'dbo'  AND  TABLE_NAME = 'DimServiceContractType'))
BEGIN

            INSERT INTO [dbo].[DimServiceContractType] ([SecviceContractTypeDesciption] ,[SecviceContractTypeName])
            VALUES ('tv total','tv_total')

            INSERT INTO [dbo].[DimServiceContractType] ([SecviceContractTypeDesciption] ,[SecviceContractTypeName])
            VALUES ('fire total','fire_total')

            INSERT INTO [dbo].[DimServiceContractType] ([SecviceContractTypeDesciption] ,[SecviceContractTypeName])
            VALUES ('transportation total','transportation_total')

            INSERT INTO [dbo].[DimServiceContractType] ([SecviceContractTypeDesciption] ,[SecviceContractTypeName])
            VALUES ('oil total','oil_total')

            INSERT INTO [dbo].[DimServiceContractType] ([SecviceContractTypeDesciption] ,[SecviceContractTypeName])
            VALUES ('propane total','propane_total')

            INSERT INTO [dbo].[DimServiceContractType] ([SecviceContractTypeDesciption] ,[SecviceContractTypeName])
            VALUES ('other total','other_total')

END