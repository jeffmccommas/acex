


/*** Run only against Insights ***/






/*** Add the EmailAddress and PhoneNumber columns to the [wh].[ProfileCustomerAccount] in the Insights database ***/

IF NOT EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[wh].[ProfileCustomerAccount]') AND name = 'EmailAddress')
BEGIN
    ALTER TABLE [wh].[ProfileCustomerAccount] ADD EmailAddress nvarchar(255) NULL
END

IF NOT EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[wh].[ProfileCustomerAccount]') AND name = 'PhoneNumber')
BEGIN
    ALTER TABLE [wh].[ProfileCustomerAccount] ADD PhoneNumber nvarchar(15) NULL
END