


/*** Run against all InsightsDW_xxx shards ***/



/****** Object:  StoredProcedure [ETL].[U_FactBilling]    Script Date: 10/27/2016 1:26:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 7/2/2014
-- Description:
--
-- 10/27/2016 -- Phil Victor -- Added IsFault
-- =============================================
ALTER PROCEDURE [ETL].[U_FactBilling] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @TrackingDate DATETIME;
        SET @TrackingDate = GETUTCDATE();

        UPDATE  dbo.FactServicePointBilling
        SET     TotalUsage = b.TotalUnits ,
                CostOfUsage = b.TotalCost ,
                BillDays = b.BillDays ,
                RateClassKey1 = drc.RateClassKey ,
                ReadQuality = b.ReadQuality ,
                ReadDate = b.ReadDate ,
                DueDate = b.DueDate ,
                UpdateDate = @TrackingDate ,
                TrackingId = b.TrackingId ,
                SourceKey = ds.SourceKey ,
                SourceId = b.SourceId ,
                TrackingDate = b.TrackingDate,
                IsFault = b.IsFault
        FROM    ETL.T1U_FactBilling b WITH ( NOLOCK )
                INNER JOIN dbo.FactServicePointBilling fb WITH ( NOLOCK ) ON fb.ServiceContractKey = b.ServiceContractKey
                                                              AND fb.BillPeriodStartDateKey = b.BillPeriodStartDateKey
                                                              AND fb.BillPeriodEndDateKey = b.BillPeriodEndDateKey
                INNER JOIN dbo.DimSource ds ON ds.SourceId = b.SourceId
                LEFT JOIN dbo.DimRateClass drc ON drc.ClientId = @ClientID
                                                  AND drc.RateClassName = b.RateClass
        WHERE   b.ClientID = @ClientID;


    END;





