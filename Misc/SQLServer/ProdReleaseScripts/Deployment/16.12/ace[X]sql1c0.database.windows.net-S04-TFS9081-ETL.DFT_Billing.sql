
/*** Run against all InsightsDW_xxx shards ***/



/****** Object:  StoredProcedure [ETL].[DFT_Billing]    Script Date: 10/28/2016 8:51:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      <Author,,Name>
-- Create date: <Create Date,,>
-- Description: <Description,,>
--
--- 10/27/2016 - Phil Victor -- Added IsFault Check and RateClassKey check
-- =============================================
ALTER PROCEDURE [ETL].[DFT_Billing]
    @ClientId AS INT ,
    @EtlLogId AS INT
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #tblSrcBilling
            (
              ClientId INT ,
              AccountId VARCHAR(50) ,
              PremiseId VARCHAR(50) ,
              ServiceContractId VARCHAR(50) ,
              ServicePointId VARCHAR(50) ,
              MeterId VARCHAR(50) ,
              StartDate DATETIME ,
              EndDate DATETIME ,
              BillDays INT ,
              TotalUnits DECIMAL(18, 2) ,
              TotalCost DECIMAL(18, 2) ,
              CommodityId INT ,
              BillPeriodTypeId INT ,
              BillCycleScheduleId VARCHAR(50) ,
              UOMId INT ,
              RateClass VARCHAR(50) ,
              RateClassKey INT,
              DueDate DATETIME ,
              ReadDate DATETIME ,
              ReadQuality VARCHAR(50) ,
              SourceId INT ,
              TrackingId VARCHAR(50) ,
              TrackingDate DATETIME
            );

        CREATE TABLE #tblSrcFactBilling
            (
              ServiceContractKey INT ,
              BillPeriodStartDateKey INT ,
              BillPeriodEndDateKey INT ,
              ClientId INT ,
              PremiseId VARCHAR(50) ,
              AccountId VARCHAR(50) ,
              ServiceContractId VARCHAR(50) ,
              StartDate DATETIME ,
              EndDate DATETIME ,
              CommodityId INT ,
              TotalUsage DECIMAL(18, 2) ,
              CostOfUsage DECIMAL(18, 2) ,
              SourceKey INT ,
              BillDays INT,
              RateClassKey INT
            );

        INSERT  INTO #tblSrcBilling
                EXEC [ETL].[S_Billing] @ClientID = @ClientId;  /** Holding Table contents **/

        INSERT  INTO #tblSrcFactBilling
                EXEC [ETL].[S_FactBilling] @ClientID = @ClientId;    /** Current database contents **/

        INSERT  INTO ETL.INS_FactBilling
                SELECT  @ClientId ,
                        b.PremiseId ,
                        b.AccountId ,
                        b.ServiceContractId ,
                        b.StartDate ,
                        b.EndDate ,
                        b.CommodityId ,
                        b.BillPeriodTypeId ,
                        b.BillCycleScheduleId ,
                        b.UOMId ,
                        b.SourceId ,
                        b.TotalUnits ,
                        b.TotalCost ,
                        b.BillDays ,
                        b.RateClass ,
                        b.ReadQuality ,
                        b.ReadDate ,
                        b.DueDate ,
                        @EtlLogId ,
                        b.TrackingId ,
                        b.TrackingDate
                FROM    #tblSrcBilling b
                        LEFT JOIN #tblSrcFactBilling fb ON fb.ClientId = b.ClientId
                                                           AND fb.PremiseId = b.PremiseId
                                                           AND fb.AccountId = b.AccountId
                                                           AND fb.ServiceContractId = b.ServiceContractId
                                                           AND fb.StartDate = b.StartDate
                                                           AND fb.EndDate = b.EndDate
                                                           AND fb.CommodityId = b.CommodityId
                WHERE   fb.ServiceContractKey IS NULL;

        INSERT  INTO ETL.T1U_FactBilling
                SELECT  fb.ServiceContractKey ,
                        fb.BillPeriodStartDateKey ,
                        fb.BillPeriodEndDateKey ,
                        b.CommodityId ,
                        b.SourceId ,
                        @ClientId ,
                        b.TotalUnits ,
                        b.TotalCost ,
                        b.BillDays ,
                        b.RateClass ,
                        b.ReadQuality ,
                        b.ReadDate ,
                        b.DueDate ,
                        @EtlLogId ,
                        b.TrackingId ,
                        b.TrackingDate,
                        IIF(b.RateClassKey != fb.RateClassKey, 1,0) AS IsFault
                FROM    #tblSrcBilling b
                        INNER JOIN #tblSrcFactBilling fb ON fb.ClientId = b.ClientId
                                                            AND fb.PremiseId = b.PremiseId
                                                            AND fb.AccountId = b.AccountId
                                                            AND fb.ServiceContractId = b.ServiceContractId
                                                            AND fb.StartDate = b.StartDate
                                                            AND fb.EndDate = b.EndDate
                                                            AND fb.CommodityId = b.CommodityId
                LEFT JOIN dbo.DimRateClass drc ON drc.ClientId = b.ClientId
                                                  AND drc.RateClassName = b.RateClass
                WHERE   ( ISNULL(b.TotalUnits, 0) != ISNULL(fb.TotalUsage, 0)
                          OR ISNULL(b.TotalCost, 0) != ISNULL(fb.CostOfUsage, 0)
                          OR ISNULL(b.BillDays, 0) != ISNULL(fb.BillDays, 0)
                          OR ISNULL(b.RateClassKey, 0) != ISNULL(fb.RateClassKey, 0));

        DROP    TABLE #tblSrcBilling;
        DROP     TABLE #tblSrcFactBilling;

    END;

