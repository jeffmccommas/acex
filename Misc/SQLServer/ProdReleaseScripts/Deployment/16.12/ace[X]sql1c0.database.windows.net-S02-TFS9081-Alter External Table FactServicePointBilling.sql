﻿
/*** RUN ONLY ON InsightsDW database ***/

/*** TO Alter (i.e. add a column) an External table it must be dropped and recreated ***/
IF EXISTS (SELECT * from sys.external_tables  WHERE name LIKE 'FactServicePointBilling')
BEGIN
    DROP EXTERNAL TABLE [dbo].[FactServicePointBilling]
END

CREATE EXTERNAL TABLE [dbo].[FactServicePointBilling](
        [ServiceContractKey] [int] NOT NULL,
        [BillPeriodStartDateKey] [int] NOT NULL,
        [BillPeriodEndDateKey] [int] NOT NULL,
        [UOMKey] [int] NOT NULL,
        [BillPeriodTypeKey] [int] NOT NULL,
        [ClientId] [int] NOT NULL,
        [PremiseId] [varchar](50) NOT NULL,
        [AccountId] [varchar](50) NOT NULL,
        [CommodityId] [int] NOT NULL,
        [BillPeriodTypeId] [int] NOT NULL,
        [UOMId] [int] NOT NULL,
        [StartDate] [datetime] NOT NULL,
        [EndDate] [datetime] NOT NULL,
        [TotalUsage] [decimal](18, 2) NOT NULL,
        [CostOfUsage] [decimal](18, 2) NOT NULL,
        [OtherCost] [decimal](18, 2) NULL,
        [RateClassKey1] [int] NULL,
        [RateClassKey2] [int] NULL,
        [NextReadDate] [date] NULL,
        [ReadDate] [datetime] NULL,
        [CreateDate] [datetime] NULL,
        [UpdateDate] [datetime] NULL,
        [BillCycleScheduleId] [varchar](50) NULL,
        [BillDays] [int] NOT NULL,
        [ETL_LogId] [int] NOT NULL,
        [TrackingId] [varchar](50) NOT NULL,
        [TrackingDate] [datetime] NOT NULL,
        [SourceKey] [int] NOT NULL,
        [SourceId] [int] NOT NULL,
        [ReadQuality] [varchar](50) NULL,
        [DueDate] [date] NULL,
        [IsFault] [bit] NOT NULL
    )WITH(DATA_SOURCE = InsightsDWElasticDBQueryDataSrc,DISTRIBUTION=SHARDED(ClientID))

    /*** Check to make sure the External table created successfully ***/
    SELECT   e.object_id , '[' + s.name + '].[' + e.name + ']' AS TableName, *
    FROM     sys.external_tables e
    INNER JOIN sys.schemas s ON s.schema_id = e.schema_id
    WHERE e.name LIKE 'FactServicePointBilling'

    SELECT * FROM [sys].[external_data_sources]


IF EXISTS (SELECT * from sys.external_tables  WHERE name LIKE 'T1U_FactBilling')
BEGIN
    DROP EXTERNAL TABLE [ETL].[T1U_FactBilling]
END

    CREATE EXTERNAL TABLE [ETL].[T1U_FactBilling](
    [ServiceContractKey] [int] NOT NULL,
    [BillPeriodStartDateKey] [int] NOT NULL,
    [BillPeriodEndDateKey] [int] NOT NULL,
    [CommodityKey] [int] NOT NULL,
    [SourceId] [int] NOT NULL,
    [ClientID] [int] NOT NULL,
    [TotalUnits] [decimal](18, 2) NOT NULL,
    [TotalCost] [decimal](18, 2) NOT NULL,
    [BillDays] [int] NOT NULL,
    [RateClass] [varchar](256) NULL,
    [ReadQuality] [varchar](50) NULL,
    [ReadDate] [datetime] NULL,
    [DueDate] [date] NULL,
    [ETL_LogId] [int] NOT NULL,
    [TrackingId] [varchar](50) NOT NULL,
    [TrackingDate] [datetime] NOT NULL,
    [IsFault] [bit] NOT NULL
    )WITH(DATA_SOURCE = InsightsDWElasticDBQueryDataSrc,DISTRIBUTION=SHARDED(ClientID))

    /*** Check to make sure the External table created successfully ***/
    SELECT   e.object_id , '[' + s.name + '].[' + e.name + ']' AS TableName, *
    FROM     sys.external_tables e
    INNER JOIN sys.schemas s ON s.schema_id = e.schema_id
    WHERE e.name LIKE 'T1U_FactBilling'

    SELECT * FROM [sys].[external_data_sources]
