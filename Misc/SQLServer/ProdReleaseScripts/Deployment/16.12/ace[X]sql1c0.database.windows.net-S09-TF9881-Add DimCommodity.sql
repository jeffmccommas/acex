


/*** Run against all InsightsDW_xxx shards ***/

/*** Add the commodity types from Aclara One ***/
IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
       WHERE TABLE_SCHEMA = 'dbo'  AND  TABLE_NAME = 'DimCommodity'))
BEGIN

            INSERT INTO [dbo].[DimCommodity]  ([CommodityDesc] ,[CommodityId])
            VALUES ('tv',6)

            INSERT INTO [dbo].[DimCommodity]   ([CommodityDesc] ,[CommodityId])
            VALUES ('fire',7)

            INSERT INTO [dbo].[DimCommodity]   ([CommodityDesc] ,[CommodityId])
            VALUES ('transportation',8)

            INSERT INTO [dbo].[DimCommodity]   ([CommodityDesc] ,[CommodityId])
            VALUES ('oil',9)

            INSERT INTO [dbo].[DimCommodity]   ([CommodityDesc] ,[CommodityId])
            VALUES ('propane',10)

            INSERT INTO [dbo].[DimCommodity]   ([CommodityDesc] ,[CommodityId])
            VALUES ('other',11)

END