﻿/*** Run against all InsightsDW_xxx shards ***/

/*** Add the UOM types from Aclara One ***/
IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
       WHERE TABLE_SCHEMA = 'dbo'  AND  TABLE_NAME = 'DimUOM'))
BEGIN

                INSERT INTO [dbo].[DimUOM] ([UOMKey] ,[UOMId] ,[UOMDesc])
                     VALUES (99 ,99 ,'other')

                INSERT INTO [dbo].[DimUOM] ([UOMKey] ,[UOMId] ,[UOMDesc])
                     VALUES (9 ,9 ,'cf')

                INSERT INTO [dbo].[DimUOM] ([UOMKey] ,[UOMId] ,[UOMDesc])
                     VALUES (10 ,10 ,'cgal')

                INSERT INTO [dbo].[DimUOM] ([UOMKey] ,[UOMId] ,[UOMDesc])
                     VALUES (11 ,11 ,'hgal')

                INSERT INTO [dbo].[DimUOM] ([UOMKey] ,[UOMId] ,[UOMDesc])
                     VALUES (12 ,12 ,'kgal')

                INSERT INTO [dbo].[DimUOM] ([UOMKey] ,[UOMId] ,[UOMDesc])
                     VALUES (13 ,13 ,'cm')

END