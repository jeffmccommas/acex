﻿
/*** Add the isFault column to all InsightsDW_xxx Shards ***/

IF NOT EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[FactServicePointBilling]') AND name = 'IsFault')
BEGIN
    ALTER TABLE [dbo].[FactServicePointBilling] ADD IsFault bit  NOT NULL DEFAULT 0
END


IF NOT EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[ETL].[T1U_FactBilling]') AND name = 'IsFault')
BEGIN
    ALTER TABLE [ETL].[T1U_FactBilling] ADD IsFault bit  NOT NULL DEFAULT 0
END