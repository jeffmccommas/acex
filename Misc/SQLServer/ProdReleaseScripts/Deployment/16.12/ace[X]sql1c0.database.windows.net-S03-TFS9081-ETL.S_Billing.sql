
/*** Run against all InsightsDW_xxx shards ***/



/****** Object:  StoredProcedure [ETL].[S_Billing]    Script Date: 10/28/2016 9:02:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 7/21/2014
-- Description:
--
-- 10/28/2016 -- Phil Victor -- Added RateClassKey
-- =============================================
ALTER PROCEDURE [ETL].[S_Billing]
                @ClientID INT
AS

BEGIN

        SET NOCOUNT ON;


SELECT  ClientId ,
                AccountId ,
                PremiseId ,
                ServiceContractId ,
                ServicePointId ,
                MeterId ,
                StartDate ,
                EndDate ,
                BillDays ,
                TotalUnits ,
                TotalCost ,
                CommodityId ,
                BillPeriodTypeId ,
                BillCycleScheduleId ,
                UOMId ,
                RateClass ,
                RateClassKey,
                DueDate ,
                ReadDate ,
                ReadQuality ,
                SourceId ,
                TrackingId ,
                TrackingDate
        FROM    ( SELECT    hb.ClientId ,
                            AccountId ,
                            PremiseId ,
                            ServiceContractId ,
                            ServicePointId ,
                            MeterId ,
                            StartDate ,
                            EndDate ,
                            BillDays ,
                            TotalUnits ,
                            TotalCost ,
                            CommodityId ,
                            BillPeriodTypeId ,
                            BillCycleScheduleId ,
                            UOMId ,
                            RateClass ,
                            drc.RateClassKey,
                            DueDate ,
                            ReadDate ,
                            ReadQuality ,
                            SourceId ,
                            TrackingId ,
                            TrackingDate ,
                            ROW_NUMBER() OVER ( PARTITION BY hb.ClientId,
                                                AccountId, PremiseId,
                                                ServiceContractId, StartDate,
                                                EndDate, CommodityId ORDER BY TrackingDate DESC ) AS SortId
                  FROM      Holding.v_Billing hb
                  LEFT JOIN dbo.DimRateClass drc ON drc.ClientId = hb.ClientId
                                                  AND drc.RateClassName = hb.RateClass
                  WHERE  hb.ClientId = @ClientID
                ) b
        WHERE   SortId = 1
        ORDER BY ClientId ,
                PremiseId ,
                AccountId ,
                ServiceContractId ,
                StartDate ,
                EndDate ,
                CommodityId;


    END;







