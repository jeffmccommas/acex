/*** Run against InsightsDW_101 and InsightsDW_224 shards ***/

/****** Object:  Table [dbo].[RawWeatherEnrollmentTemp]    Script Date: 11/10/2016 10:36:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'RawWeatherEnrollmentTemp'))
BEGIN
            CREATE TABLE [dbo].[RawWeatherEnrollmentTemp](
                [customer_id] [varchar](50) NULL,
                [account_id] [varchar](50) NULL,
                [premise_id] [varchar](50) NULL,
                [cellid] [varchar](50) NULL
            )
END
GO

SET ANSI_PADDING OFF
GO

