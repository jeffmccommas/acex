/*** Run against InsightsDW_101 and InsightsDW_224 shards ***/

/****** Object:  StoredProcedure [dbo].[ConvertWeatherEnrollmentToXML]    Script Date: 11/10/2016 10:34:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT  *  FROM    sys.objects  WHERE   object_id = OBJECT_ID(N'[dbo].[ConvertWeatherEnrollmentToXML]'))
BEGIN


EXEC(
    'CREATE PROCEDURE [dbo].[ConvertWeatherEnrollmentToXML] @ClientId AS INT
    AS
        BEGIN

            SET NOCOUNT ON;

            BEGIN TRY

                        WITH XMLNAMESPACES (DEFAULT ''Aclara:Insights'')

                        SELECT (    SELECT
                                    customer_id AS ''@CustomerId'', -- Customer attributes
                                    account_id  AS ''Account/@AccountId'',  -- Account attributes
                                    premise_id  AS ''Account/Premise/@PremiseId''   -- Premise attributes


                --***************************************************
                --          treatment group enrollment assignment
                --***************************************************

                                    ,CASE WHEN cellid LIKE ''T-3%'' -- for WS
                                     THEN
                                     -- ProfileItem attributes for treatment enroll
                                    (SELECT
                                             ''ws2016.treatmentgroup.enrollmentstatus'' AS ''ProfileItem/@AttributeKey'',
                                            ''ws2016.treatmentgroup.enrollmentstatus.enrolled'' AS ''ProfileItem/@AttributeValue'',
                                            GETDATE()   AS ''ProfileItem/@CreateDate'',
                                            GETDATE()   AS ''ProfileItem/@ModifiedDate'',
                                            ''utility'' AS ''ProfileItem/@Source''
                                                            FOR XML PATH(''''), TYPE, ELEMENTS)
                                                            ELSE NULL
                                            END AS ''Account/Premise''

                                    ,CASE WHEN cellid LIKE ''T-4%'' -- for HER
                                     THEN
                                     -- ProfileItem attributes for treatment enroll
                                    (SELECT
                                                 ''heu2016.treatmentgroup.enrollmentstatus''            AS ''ProfileItem/@AttributeKey'',
                                                ''heu2016.treatmentgroup.enrollmentstatus.enrolled''    AS ''ProfileItem/@AttributeValue'',
                                                 GETDATE()  AS ''ProfileItem/@CreateDate'',
                                                 GETDATE()  AS ''ProfileItem/@ModifiedDate'',
                                                ''utility'' AS ''ProfileItem/@Source''
                                                    FOR XML PATH(''''), TYPE, ELEMENTS)
                                                    ELSE NULL
                                    END AS ''Account/Premise''


                --***************************************************
                --          treatment group number assignment
                --***************************************************

                                    ,CASE WHEN cellid LIKE ''T-3%''
                                     THEN
                                     -- ProfileItem attributes for treatment group 1 enroll
                                    (SELECT
                                             ''ws2016.treatmentgroup.groupnumber''  AS ''ProfileItem/@AttributeKey''
                                            ,cellid AS ''ProfileItem/@AttributeValue''
                                            , GETDATE()  AS ''ProfileItem/@CreateDate''
                                            , GETDATE()  AS ''ProfileItem/@ModifiedDate''
                                            , ''utility'' AS ''ProfileItem/@Source''
                                                            FOR XML PATH(''''), TYPE, ELEMENTS)
                                                            ELSE NULL
                                            END AS ''Account/Premise''


                                    ,CASE WHEN cellid LIKE ''T-4%''
                                     THEN
                                     -- ProfileItem attributes for treatment group 1 enroll
                                    (SELECT
                                             ''heu2016.treatmentgroup.groupnumber'' AS ''ProfileItem/@AttributeKey''
                                            ,cellid AS ''ProfileItem/@AttributeValue''
                                            , GETDATE()  AS ''ProfileItem/@CreateDate''
                                            , GETDATE()  AS ''ProfileItem/@ModifiedDate''
                                            , ''utility'' AS ''ProfileItem/@Source''
                                                            FOR XML PATH(''''), TYPE, ELEMENTS)
                                                            ELSE NULL
                                            END AS ''Account/Premise''

                --***************************************************
                --          benchmark group enrollment assignment for HEU
                --***************************************************

                                    ,CASE WHEN cellid LIKE ''B%'' -- for HEU
                                     THEN
                                     -- ProfileItem attributes for treatment enroll
                                    (SELECT
                                     ''heu2016.benchmarkgroup.enrollmentstatus''            AS ''ProfileItem/@AttributeKey''
                                    ,''heu2016.benchmarkgroup.enrollmentstatus.enrolled''   AS ''ProfileItem/@AttributeValue''
                                    , GETDATE()         AS ''ProfileItem/@CreateDate''
                                    , GETDATE()         AS ''ProfileItem/@ModifiedDate''
                                    , ''utility'' AS ''ProfileItem/@Source''
                                                    FOR XML PATH(''''), TYPE, ELEMENTS)
                                                    ELSE NULL
                                    END AS ''Account/Premise''

                --***************************************************
                --          benchmark group number assignment
                --***************************************************

                                    ,CASE WHEN cellid LIKE ''B%''
                                     THEN
                                     -- ProfileItem attributes for treatment group 1 enroll
                                    (SELECT
                                     ''heu2016.benchmarkgroup.groupnumber''         AS ''ProfileItem/@AttributeKey''
                                    ,cellid                                                 AS ''ProfileItem/@AttributeValue''
                                    , GETDATE()         AS ''ProfileItem/@CreateDate''
                                    , GETDATE()         AS ''ProfileItem/@ModifiedDate''
                                    , ''utility'' AS ''ProfileItem/@Source''
                                                    FOR XML PATH(''''), TYPE, ELEMENTS)
                                                    ELSE NULL
                                    END AS ''Account/Premise''

                --***************************************************
                --              control group enrollment assignment
                --***************************************************

                                    ,CASE WHEN cellid LIKE ''C-15%'' --one of the control groups.  This control group is only for Weather Sensitivities
                                     THEN
                                        -- ProfileItem attributes for control group enroll
                                    (SELECT
                                    ''ws2016.controlgroup.enrollmentstatus''            AS ''ProfileItem/@AttributeKey'',
                                    ''ws2016.controlgroup.enrollmentstatus.enrolled''   AS ''ProfileItem/@AttributeValue'',
                                    GETDATE()           AS ''ProfileItem/@CreateDate'',
                                    GETDATE()           AS ''ProfileItem/@ModifiedDate'',
                                    ''utility'' AS ''ProfileItem/@Source''
                                                    FOR XML PATH(''''), TYPE, ELEMENTS)
                                    ELSE NULL
                                    END AS ''Account/Premise''

                                    ,CASE WHEN cellid LIKE ''C-17%'' --one of the control groups.  This control group is for both WS and HEU
                                     THEN
                                        -- ProfileItem attributes for analysis group enroll
                                    (SELECT
                                     ''ws2016.controlgroup.enrollmentstatus''           AS ''ProfileItem/@AttributeKey'',
                                     ''ws2016.controlgroup.enrollmentstatus.enrolled''  AS ''ProfileItem/@AttributeValue'',
                                      GETDATE()         AS ''ProfileItem/@CreateDate'',
                                      GETDATE()         AS ''ProfileItem/@ModifiedDate'',
                                      ''utility'' AS ''ProfileItem/@Source''
                                                    FOR XML PATH(''''), TYPE, ELEMENTS)
                                    ELSE NULL
                                    END AS ''Account/Premise''

                                    ,CASE WHEN cellid LIKE ''C-17%'' --one of the control groups.  This control group is for both WS and HEU
                                     THEN
                                        -- ProfileItem attributes for analysis group enroll
                                    (SELECT
                                     ''heu2016.controlgroup.enrollmentstatus''          AS ''ProfileItem/@AttributeKey'',
                                     ''heu2016.controlgroup.enrollmentstatus.enrolled'' AS ''ProfileItem/@AttributeValue'',
                                      GETDATE()         AS ''ProfileItem/@CreateDate'',
                                      GETDATE()         AS ''ProfileItem/@ModifiedDate'',
                                      ''utility'' AS ''ProfileItem/@Source''
                                                    FOR XML PATH(''''), TYPE, ELEMENTS)
                                    ELSE NULL
                                    END AS ''Account/Premise''

                --***************************************************
                --          conbtrol group number assignment
                --***************************************************

                                    ,CASE WHEN cellid LIKE ''C-15%''
                                     THEN
                                     -- ProfileItem attributes for control group C-15 enroll.  This control group is for WS only
                                    (SELECT
                                     ''ws2016.controlgroup.groupnumber''            AS ''ProfileItem/@AttributeKey''
                                    ,cellid                                             AS ''ProfileItem/@AttributeValue''
                                    , GETDATE()         AS ''ProfileItem/@CreateDate''
                                    , GETDATE()         AS ''ProfileItem/@ModifiedDate''
                                    , ''utility'' AS ''ProfileItem/@Source''
                                                    FOR XML PATH(''''), TYPE, ELEMENTS)
                                                    ELSE NULL
                                    END AS ''Account/Premise''

                                    ,CASE WHEN cellid LIKE ''C-17%''
                                     THEN
                                     -- ProfileItem attributes for control group C-17 enroll.  This control group is for WS and HEU
                                    (SELECT
                                     ''ws2016.controlgroup.groupnumber''            AS ''ProfileItem/@AttributeKey'',
                                     cellid                                             AS ''ProfileItem/@AttributeValue'',
                                    GETDATE()           AS ''ProfileItem/@CreateDate'',
                                    GETDATE()           AS ''ProfileItem/@ModifiedDate'',
                                    ''utility'' AS ''ProfileItem/@Source''
                                                    FOR XML PATH(''''), TYPE, ELEMENTS)
                                                    ELSE NULL
                                    END AS ''Account/Premise''

                                    ,CASE WHEN cellid LIKE ''C-17%''
                                     THEN
                                     -- ProfileItem attributes for control group C-17 enroll.  This control group is for WS and HEU
                                    (SELECT
                                     ''heu2016.controlgroup.groupnumber''           AS ''ProfileItem/@AttributeKey'',
                                     cellid                                             AS ''ProfileItem/@AttributeValue'',
                                    GETDATE()           AS ''ProfileItem/@CreateDate'',
                                    GETDATE()           AS ''ProfileItem/@ModifiedDate'',
                                    ''utility'' AS ''ProfileItem/@Source''
                                                    FOR XML PATH(''''), TYPE, ELEMENTS)
                                                    ELSE NULL
                                    END AS ''Account/Premise''

                ---------------------------------------------------------------
                --***************************************************
                --              channel assignment for treatment and control
                --***************************************************

                                    ,CASE WHEN cellid LIKE ''T-3%'' OR cellid LIKE ''C-15%'' --all cells are print only.  this is for WS only
                                     THEN
                                    -- ProfileItem attributes for channel -printonly
                                    (SELECT
                                    ''ws2016.channel''                  AS ''ProfileItem/@AttributeKey'',
                                    ''ws2016.channel.printonly''    AS ''ProfileItem/@AttributeValue'',
                                    GETDATE()           AS ''ProfileItem/@CreateDate'',
                                    GETDATE()           AS ''ProfileItem/@ModifiedDate'',
                                    ''utility'' AS ''ProfileItem/@Source''
                                                    FOR XML PATH(''''), TYPE, ELEMENTS)
                                                    ELSE NULL
                                    END AS ''Account/Premise''


                                    ,CASE WHEN cellid LIKE ''T-4%'' OR cellid LIKE ''B%'' --HEU only
                                     THEN
                                    --ProfileItem attributes for channel -printonly
                                    (SELECT
                                    ''heu2016.channel''         AS ''ProfileItem/@AttributeKey'',
                                    ''heu2016.channel.printonly''   AS ''ProfileItem/@AttributeValue'',
                                    GETDATE()           AS ''ProfileItem/@CreateDate'',
                                    GETDATE()           AS ''ProfileItem/@ModifiedDate'',
                                    ''utility'' AS ''ProfileItem/@Source''
                                                FOR XML PATH(''''), TYPE, ELEMENTS)
                                                    ELSE NULL
                                    END AS ''Account/Premise''

                                    ,CASE WHEN cellid LIKE ''C-17%''    --this control group is both ws and heu
                                     THEN
                                    --ProfileItem attributes for channel -printonly
                                    (SELECT
                                    ''ws2016.channel''          AS ''ProfileItem/@AttributeKey'',
                                    ''ws2016.channel.printonly''    AS ''ProfileItem/@AttributeValue'',
                                    GETDATE()           AS ''ProfileItem/@CreateDate'',
                                    GETDATE()           AS ''ProfileItem/@ModifiedDate'',
                                    ''utility'' AS ''ProfileItem/@Source''
                                                FOR XML PATH(''''), TYPE, ELEMENTS)
                                                    ELSE NULL
                                    END AS ''Account/Premise''

                                    ,CASE WHEN cellid LIKE ''C-17%''    --this control group is both ws and heu
                                     THEN
                                    --ProfileItem attributes for channel -printonly
                                    (SELECT
                                    ''heu2016.channel''         AS ''ProfileItem/@AttributeKey'',
                                    ''heu2016.channel.printonly''   AS ''ProfileItem/@AttributeValue'',
                                    GETDATE()           AS ''ProfileItem/@CreateDate'',
                                    GETDATE()           AS ''ProfileItem/@ModifiedDate'',
                                    ''utility'' AS ''ProfileItem/@Source''
                                                FOR XML PATH(''''), TYPE, ELEMENTS)
                                                    ELSE NULL
                                    END AS ''Account/Premise''
                ---------------------------------
                --***************************************************
                --  analysis group enrollment assignment (all)
                --***************************************************

                                    ,CASE WHEN cellid LIKE ''T-3%'' OR cellid LIKE''C-15%''--one of the treatment groups or  --one of the control groups
                                     THEN
                                        -- ProfileItem attributes for analysis group enroll ws only
                                    (SELECT
                                     ''ws2016.analysisgroup.enrollmentstatus''          AS ''ProfileItem/@AttributeKey''
                                    ,''ws2016.analysisgroup.enrollmentstatus.enrolled'' AS ''ProfileItem/@AttributeValue''
                                    , GETDATE()         AS ''ProfileItem/@CreateDate''
                                    , GETDATE()         AS ''ProfileItem/@ModifiedDate''
                                    , ''utility'' AS ''ProfileItem/@Source''
                                                    FOR XML PATH(''''), TYPE, ELEMENTS)
                                    ELSE NULL
                                    END AS ''Account/Premise''

                                    ,CASE WHEN cellid LIKE ''T-4%'' OR cellid LIKE''B%'' --  one of the treatment groups or  --one of the control groups
                                     THEN
                                        -- ProfileItem attributes for analysis group enroll heu only
                                    (SELECT
                                     ''heu2016.analysisgroup.enrollmentstatus''         AS ''ProfileItem/@AttributeKey''
                                    ,''heu2016.analysisgroup.enrollmentstatus.enrolled''    AS ''ProfileItem/@AttributeValue''
                                    , GETDATE()         AS ''ProfileItem/@CreateDate''
                                    , GETDATE()         AS ''ProfileItem/@ModifiedDate''
                                    , ''utility'' AS ''ProfileItem/@Source''
                                                    FOR XML PATH(''''), TYPE, ELEMENTS)
                                    ELSE NULL
                                    END AS ''Account/Premise''

                                    ,CASE WHEN cellid LIKE ''C-17%'' --  C-17 is a control group for ws and heu
                                     THEN
                                        -- ProfileItem attributes for analysis group enroll
                                    (SELECT
                                    ''heu2016.analysisgroup.enrollmentstatus''          AS ''ProfileItem/@AttributeKey'',
                                    ''heu2016.analysisgroup.enrollmentstatus.enrolled'' AS ''ProfileItem/@AttributeValue'',
                                    GETDATE()           AS ''ProfileItem/@CreateDate'',
                                    GETDATE()           AS ''ProfileItem/@ModifiedDate'',
                                    ''utility'' AS ''ProfileItem/@Source''
                                                    FOR XML PATH(''''), TYPE, ELEMENTS)
                                    ELSE NULL
                                    END AS ''Account/Premise''

                                    ,CASE WHEN cellid LIKE ''C-17%'' --  C-17 is a control group for ws and heu
                                     THEN
                                        -- ProfileItem attributes for analysis group enroll
                                    (SELECT
                                    ''ws2016.analysisgroup.enrollmentstatus''           AS ''ProfileItem/@AttributeKey'',
                                    ''ws2016.analysisgroup.enrollmentstatus.enrolled''  AS ''ProfileItem/@AttributeValue'',
                                    GETDATE()           AS ''ProfileItem/@CreateDate'',
                                    GETDATE()           AS ''ProfileItem/@ModifiedDate'',
                                    ''utility'' AS ''ProfileItem/@Source''
                                                    FOR XML PATH(''''), TYPE, ELEMENTS)
                                    ELSE NULL
                                    END AS ''Account/Premise''
                ---------------------------------
                        FROM dbo.RawWeatherEnrollmentTemp
                        WHERE   cellid IS NOT NULL AND cellid !='''' AND cellid !=''SU''
                        ORDER BY customer_id, account_id, premise_id
                    FOR XML PATH(''Customer''),ELEMENTS, ROOT(''Customers'')) AS XmlData ,
                    ''Profile'' AS XmlType;

            END TRY
            BEGIN CATCH
                THROW;
            END CATCH;
        END;'
)

END
GO



