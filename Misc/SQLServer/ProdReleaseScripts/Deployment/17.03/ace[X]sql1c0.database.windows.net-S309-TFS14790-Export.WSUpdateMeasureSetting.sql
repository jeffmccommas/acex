﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

 /****** Object:  StoredProcedure [Export].[WSUpdateMeasureSetting]    Script Date: 2/8/2017 1:59:18 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[Export].[WSUpdateMeasureSetting]')
                    AND type IN ( N'P', N'PC' ) )
    DROP PROCEDURE [Export].[WSUpdateMeasureSetting];
GO

/****** Object:  StoredProcedure [Export].[WSUpdateMeasureSetting]    Script Date: 2/8/2017 1:59:18 PM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[Export].[WSUpdateMeasureSetting]')
                        AND type IN ( N'P', N'PC' ) )
    BEGIN
        EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WSUpdateMeasureSetting] AS';
    END;
GO

ALTER PROCEDURE [Export].[WSUpdateMeasureSetting]
    @clientId [INT] = 224 ,
    @profileName [VARCHAR](100) ,
    @settingName [VARCHAR](100) ,
    @settingValue [VARCHAR](4000) ,
    @allowAdd [BIT] = 0
AS -------------------------------------------------------------------------------
--  Name:               WSUpdateMeasureSetting
--  Author:             Philip Victor
--  Description :     Update a setting for a weather sensitivity measure
--  Notes:
--
--
--  Parameters:
--      @clientId       - identifies the client
--      @profileName    - identifies the profile
--      @settingName    - the name of the setting to modify
--      @settingValue   - the new value of the setting
--      @allowAdd       - set this value to 1 to allow the addition of a new setting
--
--
--  Examples:
--
--      update property for profile EMAIL.201511.1.1.M1
--          [export].[UpdateMeasureSetting]
--              @profileName = 'EMAIL.201511.1.1.M1',
--              @settingName = 'actions',
--              @settingValue = 'good, bad, ugly'

--  Change History:
--
-----------------------------------------------------------------------------

    BEGIN
        SET NOCOUNT ON;

    -- perform some validation
        DECLARE @err_msg [VARCHAR](1000) ,
            @current_setting_value [VARCHAR](4000);

        IF @allowAdd = 0
            BEGIN

        -- we are not allowing the addition of a new setting...therefore ensure we have the setting
                IF NOT EXISTS ( SELECT  *
                                FROM    [Export].[WSMeasureSetting]
                                WHERE   [ProfileName] = @profileName
                                        AND [Name] = @settingName
                                        AND [ClientId] = @clientId )
                    BEGIN
                        SET @err_msg = 'Weather Sensitivity settingName = '''
                            + @settingName
                            + ''' does not exist.  Set @allowAdd=1 if you really want to add a new setting';
                    END;
            END;

        IF @err_msg IS NOT NULL
            BEGIN
                RAISERROR(@err_msg , 11 , 1);
            END;
        ELSE
            BEGIN

                SELECT  @current_setting_value = [Value]
                FROM    [Export].[WSMeasureSetting]
                WHERE   [ProfileName] = @profileName
                        AND [Name] = @settingName
                        AND [ClientId] = @clientId;

                MERGE INTO [Export].[WSMeasureSetting] AS [current]
                USING
                    ( SELECT    @profileName AS [ProfileName] ,
                                @clientId AS [ClientId] ,
                                @settingName AS [Name] ,
                                @settingValue AS [Value]
                    ) AS [new]
                ON [current].[ProfileName] = [new].[ProfileName]
                    AND [current].[ClientId] = [new].[ClientId]
                    AND [current].[Name] = [new].[Name]
                WHEN MATCHED THEN
                -- replace the existing row
                    UPDATE SET [current].[Value] = [new].[Value]
                WHEN NOT MATCHED BY TARGET THEN
                -- create a new row
                    INSERT ( [ProfileName] ,
                             [ClientId] ,
                             [Name] ,
                             [Value] ,
                             [description]
                           )
                    VALUES ( [new].[ProfileName] ,
                             [new].[ClientId] ,
                             [new].[Name] ,
                             [new].[Value] ,
                             ''
                           );

                PRINT 'Weather Sensitivity Profile: ''' + @profileName
                    + '''; setting: ''' + @settingName + '''';
                PRINT '            oldValue: '''
                    + ISNULL(@current_setting_value, '') + '''';
                PRINT '            newValue: ''' + @settingValue + '''';
                PRINT '---------------------';

            END;
    END;

GO
