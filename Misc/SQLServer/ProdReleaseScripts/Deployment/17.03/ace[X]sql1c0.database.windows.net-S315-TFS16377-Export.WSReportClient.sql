﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  Table [Export].[WSReportClient]    Script Date: 3/13/2017 2:03:02 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSReportClient]') AND type in (N'U'))
    DROP TABLE [Export].[WSReportClient]
GO

/****** Object:  Table [Export].[WSReportClient]    Script Date: 3/13/2017 2:03:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSReportClient]') AND type in (N'U'))
BEGIN
        CREATE TABLE [Export].[WSReportClient](
            [FieldName] [varchar](255) NOT NULL,
            [ClientId] int NOT NULL,
            CONSTRAINT [PK_WSReportClient] PRIMARY KEY CLUSTERED
            (
                [FieldName] ASC
            )WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
        )
END
GO
