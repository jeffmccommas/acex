﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  Table [Export].[WSMeasureProfile]    Script Date: 2/8/2017 1:28:50 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSMeasureProfile]') AND type in (N'U'))
DROP TABLE [Export].[WSMeasureProfile]
GO

/****** Object:  Table [Export].[WSMeasureProfile]    Script Date: 2/8/2017 1:28:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSMeasureProfile]') AND type in (N'U'))
BEGIN
CREATE TABLE [Export].[WSMeasureProfile](
    [ProfileName] [varchar](50) NOT NULL,
    [ClientId] [int] NULL,
    [Description] [varchar](1000) NULL,
    [EnduseList] [varchar](1000) NULL,
    [Type] [varchar](50) NOT NULL,
    [OrderedEnduseList] [varchar](1000) NULL,
    [CriteriaId1] [varchar](50) NOT NULL CONSTRAINT [DF_WSMeasureProfile_CriteriaId1]  DEFAULT ('NOVALUE'),
    [CriteriaDetail1] [varchar](255) NULL,
    [CriteriaSort1] [varchar](20) NULL CONSTRAINT [DF_WSMeasureProfile_CriteriaSort1]  DEFAULT (''),
    [CriteriaId2] [varchar](50) NOT NULL CONSTRAINT [DF_WSMeasureProfile_CriteriaId2]  DEFAULT ('NOVALUE'),
    [CriteriaDetail2] [varchar](255) NULL,
    [CriteriaSort2] [varchar](20) NULL CONSTRAINT [DF_WSMeasureProfile_CriteriaSort2]  DEFAULT (''),
    [CriteriaId3] [varchar](50) NOT NULL CONSTRAINT [DF_WSMeasureProfile_CriteriaId3]  DEFAULT ('NOVALUE'),
    [CriteriaDetail3] [varchar](255) NULL,
    [CriteriaSort3] [varchar](20) NULL CONSTRAINT [DF_WSMeasureProfile_CriteriaSort3]  DEFAULT (''),
    [CriteriaId4] [varchar](50) NOT NULL CONSTRAINT [DF_WSMeasureProfile_CriteriaId4]  DEFAULT ('NOVALUE'),
    [CriteriaDetail4] [varchar](255) NULL,
    [CriteriaSort4] [varchar](20) NULL CONSTRAINT [DF_WSMeasureProfile_CriteriaSort4]  DEFAULT (''),
    [CriteriaId5] [varchar](50) NOT NULL CONSTRAINT [DF_WSMeasureProfile_CriteriaId5]  DEFAULT ('NOVALUE'),
    [CriteriaDetail5] [varchar](255) NULL,
    [CriteriaSort5] [varchar](20) NULL CONSTRAINT [DF_WSMeasureProfile_CriteriaSort5]  DEFAULT (''),
    [CriteriaId6] [varchar](50) NOT NULL CONSTRAINT [DF_WSMeasureProfile_CriteriaId6]  DEFAULT ('NOVALUE'),
    [CriteriaDetail6] [varchar](255) NULL,
    [CriteriaSort6] [varchar](20) NULL CONSTRAINT [DF_WSMeasureProfile_CriteriaSort6]  DEFAULT (''),
 CONSTRAINT [PK_WSMeasureProfile] PRIMARY KEY CLUSTERED
(
    [ProfileName] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO

