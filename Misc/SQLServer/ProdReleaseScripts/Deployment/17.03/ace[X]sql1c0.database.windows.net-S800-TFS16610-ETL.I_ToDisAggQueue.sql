﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[I_BillsToDisAggQueue]    Script Date: 3/20/2017 11:37:42 AM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[ETL].[I_BillsToDisAggQueue]')
                    AND type IN ( N'P', N'PC' ) )
    DROP PROCEDURE [ETL].[I_BillsToDisAggQueue];
GO

/****** Object:  StoredProcedure [ETL].[I_AttribsToDisAggQueue]    Script Date: 3/20/2017 11:37:42 AM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[ETL].[I_AttribsToDisAggQueue]')
                    AND type IN ( N'P', N'PC' ) )
    DROP PROCEDURE [ETL].[I_AttribsToDisAggQueue];
GO

/****** Object:  StoredProcedure [ETL].[I_AttribsToDisAggQueue]    Script Date: 3/20/2017 11:37:42 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[ETL].[I_AttribsToDisAggQueue]')
                        AND type IN ( N'P', N'PC' ) )
    BEGIN
        EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[I_AttribsToDisAggQueue] AS';
    END;
GO


-- =============================================
-- Author:      Wayne
-- Create date: 4/6/2015
--
-- =============================================
ALTER PROCEDURE [ETL].[I_AttribsToDisAggQueue] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        MERGE INTO holding.[DisAggReCalcQueue] AS T
        USING
            ( SELECT    a.ClientId ,
                        a.PremiseId ,
                        a.AccountId ,
                        a.CustomerId ,
                        a.SourceId ,
                        a.TrackingId ,
                        a.TrackingDate,
                        a.Latest
              FROM      ( SELECT DISTINCT
                                    dp.PremiseKey ,
                                    dp.ClientId ,
                                    dp.PremiseId ,
                                    dp.AccountId ,
                                    c.CustomerId ,
                                    p.SourceId ,
                                    p.TrackingId ,
                                    p.TrackingDate ,
                                    ROW_NUMBER() OVER ( PARTITION BY dp.PremiseId,
                                                        dp.AccountId,
                                                        c.CustomerId ORDER BY p.TrackingDate DESC ) AS latest
                          FROM      holding.[v_PremiseAttributes] p
                                    INNER JOIN dbo.DimPremise dp ON dp.ClientId = p.ClientId
                                                              AND dp.PremiseId = p.PremiseId
                                                              AND dp.AccountId = p.AccountId
                                    INNER JOIN ( SELECT PremiseKey ,
                                                        CustomerKey ,
                                                        ROW_NUMBER() OVER ( PARTITION BY premisekey ORDER BY datecreated DESC ) AS rnk
                                                 FROM   dbo.[FactCustomerPremise] fcp
                                               ) AS fcp ON dp.PremiseKey = fcp.premisekey
                                                           AND rnk = 1
                                    INNER JOIN dbo.[DimCustomer] c ON c.customerkey = fcp.customerkey
                                                              AND rnk = 1
                          WHERE     p.ClientId = @ClientID  --AND p.SourceId IN (4,5,6)
                        ) AS a
                    WHERE a.Latest = 1
            ) AS s
        ON s.ClientId = T.clientid
            AND s.PremiseId = T.PremiseId
            AND s.AccountId = T.AccountId
        WHEN MATCHED THEN
            UPDATE SET
                    SourceId = s.SourceId ,
                    TrackingId = s.TrackingId ,
                    TrackingDate = s.TrackingDate
        WHEN NOT MATCHED THEN
            INSERT ( [ClientId] ,
                     [PremiseId] ,
                     [AccountId] ,
                     [CustomerId] ,
                     [SourceId] ,
                     [TrackingId] ,
                     [TrackingDate]

                   )
            VALUES ( s.ClientId ,
                     s.PremiseId ,
                     s.AccountId ,
                     s.CustomerId ,
                     s.SourceId ,
                     s.TrackingId ,
                     s.TrackingDate

                   );
    END;

GO

/****** Object:  StoredProcedure [ETL].[I_BillsToDisAggQueue]    Script Date: 3/20/2017 11:37:42 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[ETL].[I_BillsToDisAggQueue]')
                        AND type IN ( N'P', N'PC' ) )
    BEGIN
        EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[I_BillsToDisAggQueue] AS';
    END;
GO


/*
-- =============================================
-- Author:      Wayne
-- Create date: 3/30/2015
--
-- =============================================
*/

ALTER PROCEDURE [ETL].[I_BillsToDisAggQueue] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        MERGE INTO [Holding].[DisAggReCalcQueue] AS T
        USING
            ( SELECT    a.ClientId ,
                        a.PremiseId ,
                        a.AccountId ,
                        a.CustomerId ,
                        a.SourceId ,
                        a.TrackingId ,
                        a.TrackingDate,
                        a.Latest
              FROM      ( SELECT DISTINCT
                                    dp.PremiseKey ,
                                    dp.ClientId ,
                                    dp.PremiseId ,
                                    dp.AccountId ,
                                    c.CustomerId ,
                                    b.SourceId ,
                                    b.TrackingId ,
                                    b.TrackingDate ,
                                    ROW_NUMBER() OVER ( PARTITION BY dp.PremiseId,
                                                        dp.AccountId,
                                                        c.CustomerId ORDER BY b.TrackingDate DESC ) AS latest
                          FROM      [Holding].[v_Billing] b
                                    INNER JOIN [dbo].[DimPremise] dp ON dp.ClientId = b.ClientId
                                                              AND dp.PremiseId = b.PremiseId
                                                              AND dp.AccountId = b.AccountId
                                    INNER JOIN ( SELECT PremiseKey ,
                                                        CustomerKey ,
                                                        ROW_NUMBER() OVER ( PARTITION BY PremiseKey ORDER BY DateCreated DESC ) AS rnk
                                                 FROM   [dbo].[FactCustomerPremise] fcp
                                               ) AS fcp ON dp.PremiseKey = fcp.PremiseKey
                                                           AND rnk = 1
                                    INNER JOIN dbo.[DimCustomer] c ON c.CustomerKey = fcp.CustomerKey
                                                              AND rnk = 1
                          WHERE     b.ClientId = @ClientID
                        ) AS a
              WHERE a.Latest = 1
            ) AS s
        ON s.ClientId = T.ClientId
            AND s.PremiseId = T.PremiseId
            AND s.AccountId = T.AccountId
        WHEN MATCHED THEN
            UPDATE SET
                    SourceId = s.SourceId ,
                    TrackingId = s.TrackingId ,
                    TrackingDate = s.TrackingDate
        WHEN NOT MATCHED THEN
            INSERT ( [ClientId] ,
                     [PremiseId] ,
                     [AccountId] ,
                     [CustomerId] ,
                     [SourceId] ,
                     [TrackingId] ,
                     [TrackingDate]

                   )
            VALUES ( s.ClientId ,
                     s.PremiseId ,
                     s.AccountId ,
                     s.CustomerId ,
                     s.SourceId ,
                     s.TrackingId ,
                     s.TrackingDate

                   );

    END;



GO

