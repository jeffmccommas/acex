﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSMETADATA DATABASE ***/
/********************************************************/

IF NOT EXISTS ( SELECT  1
                FROM    [cm].[TypeConfiguration]
                WHERE   [ConfigurationKey] = 'package.insightsdw.disableforhistoricalloading' )
    BEGIN
        INSERT  INTO [cm].[TypeConfiguration]
                ( [ConfigurationKey] ,
                  [ConfigurationID] ,
                  [DateUpdated]
                )
        VALUES  ( 'package.insightsdw.disableforhistoricalloading' ,
                  2506 ,
                  '2017-03-24 16:18:24.520'
                );
    END;
GO

IF NOT EXISTS ( SELECT  1
                FROM    [cm].[ClientConfiguration]
                WHERE   [ConfigurationKey] = 'package.insightsdw.disableforhistoricalloading'
                        AND ClientID = 0
                        AND [EnvironmentKey] = 'prod'
                        AND [CategoryKey] = 'system' )
    BEGIN
        INSERT  INTO [cm].[ClientConfiguration]
                ( [ClientID] ,
                  [EnvironmentKey] ,
                  [ConfigurationKey] ,
                  [CategoryKey] ,
                  [Description] ,
                  [Value] ,
                  [DateUpdated]
                )
        VALUES  ( 0 ,
                  'prod' ,
                  'package.insightsdw.disableforhistoricalloading' ,
                  'system' ,
                  'Set to true when loading historical data' ,
                  'false' ,
                  '2017-03-24 16:18:24.520'
                );
    END;
GO


IF NOT EXISTS ( SELECT  1
                FROM    [cm].[ClientConfiguration]
                WHERE   [ConfigurationKey] = 'package.insightsdw.disableforhistoricalloading'
                        AND ClientID = 210
                        AND [EnvironmentKey] = 'prod'
                        AND [CategoryKey] = 'system' )
    BEGIN
        INSERT  INTO [cm].[ClientConfiguration]
                ( [ClientID] ,
                  [EnvironmentKey] ,
                  [ConfigurationKey] ,
                  [CategoryKey] ,
                  [Description] ,
                  [Value] ,
                  [DateUpdated]
                )
        VALUES  ( 210 ,
                  'prod' ,
                  'package.insightsdw.disableforhistoricalloading' ,
                  'system' ,
                  'Set to true when loading historical data' ,
                  'true' ,
                  '2017-03-24 16:18:24.520'
                );
    END;
GO