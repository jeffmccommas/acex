﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTS DATABASE ***/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[D_STG_U_DisAggBills_API]    Script Date: 3/21/2017 2:17:51 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[D_STG_U_DisAggBills_API]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [ETL].[D_STG_U_DisAggBills_API]
GO

/****** Object:  StoredProcedure [ETL].[D_STG_U_DisAggBills_API]    Script Date: 3/21/2017 2:17:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[D_STG_U_DisAggBills_API]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[D_STG_U_DisAggBills_API] AS'
END
GO


-- =============================================
-- Author: Philip Victor
-- Create date: 12/7/2016
-- Description: Empty the U_DisAggBills_API table.
-- =============================================
ALTER PROCEDURE [ETL].[D_STG_U_DisAggBills_API]
AS

BEGIN

    SET NOCOUNT ON;

    TRUNCATE TABLE  [ETL].[U_DisAggBills_API]

END


GO
