﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [Export].[WSDisplayReportProfile]    Script Date: 2/8/2017 10:49:52 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSDisplayReportProfile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Export].[WSDisplayReportProfile]
GO

/****** Object:  StoredProcedure [Export].[WSDisplayReportProfile]    Script Date: 2/8/2017 10:49:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSDisplayReportProfile]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WSDisplayReportProfile] AS'
END
GO

ALTER procedure [Export].[WSDisplayReportProfile]
    @profileName [VARCHAR](100) = null,
    @clientId [INT] = 224
as
-------------------------------------------------------------------------------
--  Name:             WSDisplayReportProfile
--  Author:           Philip Victor
--  Description :     Display the settings for a weather sensitivity report profile
--  Notes:
--
--  Parameters:
--      @profileName        -   the name of the profile.  If this is null all profiles will be displayed.
--      @clientId               -   identifies the client
--
--  Examples:
--      [export].[WSDisplayReportProfile] @profileName = 'EMAIL.201601.2.1', @clientId = 224;
--
--  Change History:

-----------------------------------------------------------------------------

begin
    declare @rpt_profile    table ([profile_name] [VARCHAR](100));

    -- mapping of the report profile to measure profile
    declare @rpt_measure_map    table   (
                                            [sort_idx] [INT],
                                            [rpt_profile_name] [VARCHAR](100),
                                            [measure_number] [INT],
                                            [measure_profile_name] [VARCHAR](100))

    declare @rpt_profile_name [VARCHAR](100),
            @setting_name [VARCHAR](100),
            @setting_value   [VARCHAR](4000),
            @setting_description [VARCHAR](4000),
            @ordered_enduse_list [VARCHAR](1024),
            @type [VARCHAR](50),
            @measure_profile_list [VARCHAR](100),
            @measure_profile_name [VARCHAR](100),
            @measure_number  [INT],
            @criteria_id [VARCHAR](50),
            @criteria_detail [VARCHAR](255),
            @criteria_sort [VARCHAR](255),
            @tab_char [CHAR](1) = char(9);

    set nocount on;

    -- Confirm that the profile name is valid.
    insert into @rpt_profile([profile_name])
        select [ProfileName]
            from [Export].[WSReportProfile]
            where
                (
                    isnull( ltrim(rtrim(@profileName)), '' ) = '' or
                    [ProfileName] = @profileName
                ) and
                [ClientId] = @clientId
            order by [ProfileName];

    -- If not profile not found state so.
    if @@rowcount = 0 begin
        print 'Weather Sensitivity Profile ''' + @profileName +''' NOT found.';
    end
    -- Cycle through the report settings.
    else begin
        declare rpt_profile_cursor cursor local static forward_only for
            select [profile_name] from @rpt_profile;

        open rpt_profile_cursor;

        fetch next from rpt_profile_cursor into @rpt_profile_name;
        while @@fetch_status = 0 begin

                print @rpt_profile_name;

                declare setting_cursor cursor local static forward_only for
                    select [Name], [Value], [Description]
                        from [Export].[WSReportSetting]
                        where [ProfileName] = @rpt_profile_name
                        order by [Name];

                open setting_cursor;

                fetch next from setting_cursor into @setting_name, @setting_value, @setting_description;
                while @@fetch_status = 0 begin

                    print @tab_char + @tab_char + cast(@setting_name as char(30)) + ' : ''' + @setting_value + '''';

                    fetch next from setting_cursor into @setting_name, @setting_value, @setting_description;
                end
                close setting_cursor;
                deallocate setting_cursor;


            -- iterate over the measures associated with this report and display their details
            delete from @rpt_measure_map;
            insert @rpt_measure_map([sort_idx], [rpt_profile_name], [measure_number], [measure_profile_name])
                exec [Export].[WSFetchMeasureProfilesForReport]
                        @clientId = @clientId,
                        @reportProfileName = @rpt_profile_name;


            -- iterate over the measure profiles for the measure
            declare measure_profile_cursor cursor local static forward_only for
                select [measure_number], [measure_profile_name]
                    from @rpt_measure_map
                    where [rpt_profile_name] = @rpt_profile_name
                    order by [sort_idx];

            open measure_profile_cursor;

            fetch next from measure_profile_cursor into @measure_number, @measure_profile_name;

            while @@fetch_status = 0 begin

                print '';

                print @tab_char + @tab_char + cast('Measure #' + cast(@measure_number as [varchar]) as char(33)) + cast('MeasureProfileName' as char(30))+ ' : ''' + @measure_profile_name + '''';

                --  dump the soft modelled settings
                declare setting_cursor cursor local static forward_only for
                    select [Name], [Value], [Description]
                        from [Export].[WSMeasureSetting]
                        where [ProfileName] = @measure_profile_name
                        order by [Name];

                open setting_cursor;

                fetch next from setting_cursor into @setting_name, @setting_value, @setting_description;
                while @@fetch_status = 0 begin

                    print @tab_char + @tab_char + cast('' as char(33)) + cast(@setting_name as char(30)) + ' : ''' + @setting_value + '''';

                    fetch next from setting_cursor into @setting_name, @setting_value, @setting_description;
                end
                close setting_cursor;
                deallocate setting_cursor;

                -- dump the hard modelled settings
                set @setting_value = '';
                select
                    @setting_value = isnull([Type], '')
                    from [Export].[WSMeasureProfile]
                    where [ProfileName] = @measure_profile_name;
                print @tab_char + @tab_char + cast('' as char(33)) + cast('Type' as char(30))+ ' : ''' + @setting_value + '''';

                set @setting_value = '';
                select
                    @setting_value = isnull([Description], '')
                    from [Export].[WSMeasureProfile]
                    where [ProfileName] = @measure_profile_name;
                print @tab_char + @tab_char + cast('' as char(33)) + cast('Description' as char(30))+ ' : ''' + @setting_value + '''';

                set @ordered_enduse_list = '';
                select
                    @ordered_enduse_list =  isnull([OrderedEnduseList], '')
                    from [Export].[WSMeasureProfile]
                    where [ProfileName] = @measure_profile_name;
                print @tab_char + @tab_char + cast('' as char(33)) + cast('OrderedEnduseList' as char(30))+ ' : ''' + @ordered_enduse_list + '''';

                select @criteria_id = '', @criteria_detail = '', @criteria_sort = '';
                select
                    @criteria_id = isnull([CriteriaId1], ''),       @criteria_detail = isnull([CriteriaDetail1], ''),       @criteria_sort = isnull([CriteriaSort1], '')
                    from [Export].[WSMeasureProfile]
                    where [ProfileName] = @measure_profile_name;
                print @tab_char + @tab_char + cast('' as char(33)) + cast('CriteriaId1{id; detail; sort}' as char(30)) + ' : ''' + @criteria_id + '''; ''' + @criteria_detail + '''; ''' + @criteria_sort+ '''';

                select @criteria_id = '', @criteria_detail = '', @criteria_sort = '';
                select
                    @criteria_id = isnull([CriteriaId2], ''),       @criteria_detail = isnull([CriteriaDetail2], ''),       @criteria_sort = isnull([CriteriaSort2], '')
                    from [Export].[WSMeasureProfile]
                    where [ProfileName] = @measure_profile_name;
                print @tab_char + @tab_char + cast('' as char(33)) + cast('CriteriaId2{id; detail; sort}' as char(30)) + ' : ''' + @criteria_id + '''; ''' + @criteria_detail + '''; ''' + @criteria_sort+ '''';

                select @criteria_id = '', @criteria_detail = '', @criteria_sort = '';
                select
                    @criteria_id = isnull([CriteriaId3], ''),       @criteria_detail = isnull([CriteriaDetail3], ''),       @criteria_sort = isnull([CriteriaSort3], '')
                    from [Export].[WSMeasureProfile]
                    where [ProfileName] = @measure_profile_name;
                print @tab_char + @tab_char + cast('' as char(33)) + cast('CriteriaId3{id; detail; sort}' as char(30)) + ' : ''' + @criteria_id + '''; ''' + @criteria_detail + '''; ''' + @criteria_sort+ '''';

                select @criteria_id = '', @criteria_detail = '', @criteria_sort = '';
                select
                    @criteria_id = isnull([CriteriaId4], ''),       @criteria_detail = isnull([CriteriaDetail4], ''),       @criteria_sort = isnull([CriteriaSort4], '')
                    from [Export].[WSMeasureProfile]
                    where [ProfileName] = @measure_profile_name;
                print @tab_char + @tab_char + cast('' as char(33)) + cast('CriteriaId4{id; detail; sort}' as char(30)) + ' : ''' + @criteria_id + '''; ''' + @criteria_detail + '''; ''' + @criteria_sort+ '''';

                select @criteria_id = '', @criteria_detail = '', @criteria_sort = '';
                select
                    @criteria_id = isnull([CriteriaId5], ''),       @criteria_detail = isnull([CriteriaDetail5], ''),       @criteria_sort = isnull([CriteriaSort5], '')
                    from [Export].[WSMeasureProfile]
                    where [ProfileName] = @measure_profile_name;
                print @tab_char + @tab_char + cast('' as char(33)) + cast('CriteriaId5{id; detail; sort}' as char(30)) + ' : ''' + @criteria_id + '''; ''' + @criteria_detail + '''; ''' + @criteria_sort+ '''';

                select @criteria_id = '', @criteria_detail = '', @criteria_sort = '';
                select
                    @criteria_id = isnull([CriteriaId6], ''),       @criteria_detail = isnull([CriteriaDetail6], ''),       @criteria_sort = isnull([CriteriaSort6], '')
                    from [Export].[WSMeasureProfile]
                    where [ProfileName] = @measure_profile_name;
                print @tab_char + @tab_char + cast('' as char(33)) + cast('CriteriaId6{id; detail; sort}' as char(30)) + ' : ''' + @criteria_id + '''; ''' + @criteria_detail + '''; ''' + @criteria_sort+ '''';

                fetch next from measure_profile_cursor into @measure_number, @measure_profile_name;
            end

            close measure_profile_cursor;
            deallocate measure_profile_cursor;

            print '-------------------------------------------------------------------------------';
            print '';

            fetch next from rpt_profile_cursor into @rpt_profile_name;
        end

        close rpt_profile_cursor;
        deallocate rpt_profile_cursor;

    end
end

GO