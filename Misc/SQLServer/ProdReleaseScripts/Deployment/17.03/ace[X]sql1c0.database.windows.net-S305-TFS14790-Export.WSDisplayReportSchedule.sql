﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [Export].[WSDisplayReportSchedule]    Script Date: 2/8/2017 10:49:52 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSDisplayReportSchedule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Export].[WSDisplayReportSchedule]
GO

/****** Object:  StoredProcedure [Export].[WSDisplayReportSchedule]    Script Date: 2/8/2017 10:49:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSDisplayReportSchedule]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WSDisplayReportSchedule] AS'
END
GO

ALTER procedure [Export].[WSDisplayReportSchedule]
    @clientId       [int] = 224
as
-------------------------------------------------------------------------------
--  Name:                   WSDisplayReportSchedule
--  Author:                 Philip Victor
--  Description :       Display the schedule for the weather sensitvity reports
--  Notes:
--
--  Parameters:
--      @clientId           -   identifies the client
--
--
--  Examples:
--      [export].[WSDisplayReportSchedule]
--
--  Change History:
--
-----------------------------------------------------------------------------

begin

set nocount on;

    select
            [profile_start].[effective_date] as [Start],
            [profile_end].[effective_date] as [End],
            [profile_start].[profile_name] as [Profile Name],
            [report_number].[report_number] as [Report #],
            [channel].[channel] as [Channel]
            from
                (
                    -- effective start date for a report profile
                    select
                        ProfileName as [profile_name],
                        cast([Value] as date) as [effective_date]
                        from
                            [Export].[WSReportSetting]
                        where
                            [Name] = 'effective_start' and
                            [ClientId] = @clientId
                ) as [profile_start]
                inner join
                (
                    -- effective end date for a report profile
                    select
                        ProfileName as [profile_name],
                        cast([Value] as date) as [effective_date]
                        from
                            [Export].[WSReportSetting]
                        where
                            [Name] = 'effective_end' and
                            [ClientId] = @clientId
                ) as [profile_end] on
                    [profile_start].[profile_name] = [profile_end].[profile_name]
                inner join
                (
                    -- channel
                    select
                        ProfileName as [profile_name],
                        [Value] as [channel]
                        from
                            [Export].[WSReportSetting]
                        where
                            [Name] = 'channel' and
                            [ClientId] = @clientId
                ) as [channel] on
                    [profile_start].[profile_name] = [channel].[profile_name]
                inner join
                (
                    -- report number
                    select
                        ProfileName as [profile_name],
                        [Value] as [report_number]
                        from
                            [Export].[WSReportSetting]
                        where
                            [Name] = 'report_number' and
                            [ClientId] = @clientId
                ) as [report_number] on
                    [profile_start].[profile_name] = [report_number].[profile_name]
                inner join [export].[HomeEnergyReportProfile] as [report_profile] on
                    [profile_start].[profile_name] = [report_profile].[ProfileName]

    order by
        [channel].[channel],
        [profile_start].[effective_date],
        [report_number].[report_number]
end

GO