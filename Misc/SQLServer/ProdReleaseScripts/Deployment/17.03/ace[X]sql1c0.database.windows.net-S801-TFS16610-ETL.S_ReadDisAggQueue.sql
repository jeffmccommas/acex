﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[S_ReadDisAggQueue]    Script Date: 3/20/2017 1:09:45 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_ReadDisAggQueue]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [ETL].[S_ReadDisAggQueue]
GO

/****** Object:  StoredProcedure [ETL].[S_ReadDisAggQueue]    Script Date: 3/20/2017 1:09:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_ReadDisAggQueue]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[S_ReadDisAggQueue] AS'
END
GO
-- =============================================
-- Author:  Philip Victor
-- Create date: 03/20/2017
-- Description:
-- =============================================
ALTER PROCEDURE [ETL].[S_ReadDisAggQueue]
    @ClientID INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

            SELECT    a.ClientId ,
                        a.PremiseId ,
                        a.AccountId ,
                        a.CustomerId ,
                        a.SourceId ,
                        a.TrackingId ,
                        a.TrackingDate
              FROM    (SELECT daq.*,
                        ROW_NUMBER() OVER ( PARTITION BY daq.PremiseId,
                        daq.AccountId, daq.CustomerId ORDER BY daq.TrackingDate DESC ) AS Latest
             FROM [Holding].[DisAggReCalcQueue] daq
             INNER JOIN [dbo].[DimPremise] dp ON dp.ClientId = daq.ClientId
                                                              AND dp.PremiseId = daq.PremiseId
                                                              AND dp.AccountId = daq.AccountId
            INNER JOIN dbo.[vcalculateanalyticsFlagTrue] d ON d.PremiseKey = dp.PremiseKey
            WHERE daq.[ClientId]=@ClientID) AS a
            WHERE a.Latest  = 1
END

GO

