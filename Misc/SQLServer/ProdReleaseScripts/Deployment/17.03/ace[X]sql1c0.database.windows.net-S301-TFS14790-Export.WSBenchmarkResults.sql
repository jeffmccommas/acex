﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  Table [Export].[WSBenchmarkResults]    Script Date: 2/7/2017 11:07:47 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[DF_WSBenchmarkResults_CreateDate]') AND type = 'D')
BEGIN
    ALTER TABLE [Export].[WSBenchmarkResults] DROP CONSTRAINT [DF_WSBenchmarkResults_CreateDate]
END

GO

/****** Object:  Table [Export].[WSBenchmarkResults]    Script Date: 2/22/2017 11:10:41 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSBenchmarkResults]') AND type in (N'U'))
    DROP TABLE [Export].[WSBenchmarkResults]
GO

/****** Object:  Table [Export].[WSBenchmarkResults]    Script Date: 2/22/2017 11:10:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSBenchmarkResults]') AND type in (N'U'))
BEGIN
    CREATE TABLE [Export].[WSBenchmarkResults](
        [IsActive] [bit] NOT NULL DEFAULT 1,
        [ClientId] [int] NOT NULL,
        [ReportProfileName] [varchar](50) NOT NULL,
        [CreateDate] [datetime] NOT NULL,
        [PremiseId] [varchar](50) NOT NULL,
        [CustomerId] [varchar](50) NOT NULL,
        [AccountId] [varchar](50) NOT NULL,
        [CommodityDesc] [varchar](20) NOT NULL,
        [UOMDesc] [varchar](50) NOT NULL,
        [GroupId] [int] NOT NULL,
        [ExtremeDay_MyUsage] [decimal](18, 2) NOT NULL,
        [ExtremeDay_AverageHomeUse] [decimal](18, 2) NOT NULL,
        [ExtremeDay_EfficientHomeUse] [decimal](18, 2) NOT NULL,
        [ExtremeDay_PcntPeerBenchmarkValue] [varchar](5) NOT NULL,
        [ExtremeDay_PcntPeerBenchmarkText] [varchar](50) NOT NULL,
        [Self_MyExtremeDayUse] [decimal](18, 2) NOT NULL,
        [Self_MyNormalDayUse] [decimal](18, 2) NOT NULL,
        [Self_PcntBenchmarkValue] [varchar](5) NOT NULL,
        [Self_PcntBenchmarkText] [varchar](50) NOT NULL,
        [ReferrerId] [varchar](50) NULL,
        [Subscriber_Key] [varchar](50) NULL,
     CONSTRAINT [PK_WSBenchmarkResults] PRIMARY KEY CLUSTERED
    (
        [CustomerId] ASC,
        [AccountId] ASC,
        [PremiseId] ASC,
        [CreateDate] ASC
    )WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
    )
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[DF_WSBenchmarkResults_CreateDate]') AND type = 'D')
BEGIN
    ALTER TABLE [Export].[WSBenchmarkResults] ADD  CONSTRAINT [DF_WSBenchmarkResults_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
END

GO
