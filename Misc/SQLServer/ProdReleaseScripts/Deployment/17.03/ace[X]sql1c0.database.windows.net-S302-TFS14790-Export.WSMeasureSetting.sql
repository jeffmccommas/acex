﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  Table [Export].[WSMeasureSetting]    Script Date: 2/8/2017 1:28:50 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSMeasureSetting]') AND type in (N'U'))
DROP TABLE [Export].[WSMeasureSetting]
GO


/****** Object:  Table [Export].[WSMeasureSetting]    Script Date: 2/8/2017 1:28:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSMeasureSetting]') AND type in (N'U'))
BEGIN
CREATE TABLE [Export].[WSMeasureSetting](
    [ProfileName] [varchar](50) NOT NULL,
    [ClientId] [int] NOT NULL,
    [Name] [varchar](100) NOT NULL,
    [Value] [varchar](4000) NOT NULL,
    [Description] [varchar](4000) NULL,
PRIMARY KEY CLUSTERED
(
    [ProfileName] ASC,
    [ClientId] ASC,
    [Name] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO
