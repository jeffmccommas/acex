﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [Export].[WSUpdateReportSetting]    Script Date: 2/8/2017 10:49:52 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSUpdateReportSetting]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Export].[WSUpdateReportSetting]
GO


/****** Object:  StoredProcedure [Export].[WSUpdateReportSetting]    Script Date: 2/8/2017 10:49:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSUpdateReportSetting]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WSUpdateReportSetting] AS'
END
GO

ALTER procedure [Export].[WSUpdateReportSetting]
    @clientId [INT] = 224,
    @profileName [VARCHAR](100) = null,
    @channel [VARCHAR](20) = null,
    @reportNumber [INT] = null,
    @settingName [VARCHAR](100),
    @settingValue [VARCHAR](4000),
    @allowAdd [BIT] = 0
as
-------------------------------------------------------------------------------
--  Name:                   WSUpdateReportSetting
--  Author:                 Philip Victor
--  Description :       Update a setting for a report
--  Notes:
--      You can either update a setting for a channel/report# or for a specific profile.
--      When you update a setting for a channe/report# you specify the @channel and @reportNumber parameters.
--          The sproc will use reportNumber / channel to determine the profile to modify.  For UIL we have setup a profile for each
--          reportNumber / channel combination.  However we wish to "hide" the profile name from the user so that they only know about reportNumber /channel.
--          This is for convenience when you want to modify a setting for all profile(s) for the channel/report#...however be careful because some parameters,
--          such as segment are report profile specific.
--
--      When you want to update a setting for a specific report you specify a value for the @reportProfile parameter.
--
--
--
--  Parameters:
--      @clientId       - identifies the client
--      @profileName    - identifies the profile
--      @channel        - identifies the channel {email, print}
--      @reportNumber   - the report # {1..6 for email, 1..4 for print}
--      @settingName    - the name of the setting to modify
--      @settingValue   - the new value of the setting
--      @allowAdd       - set this value to 1 to allow the addition of a new setting
--
--
--  Examples:
--      update property for email report #3
--          [export].[WSUpdateReportSetting]
--              @channel = 'email',
--              @reportNumber = 3,
--              @settingName = 'is_trial_run',
--              @settingValue = '0'
--
--      update property for profile EMAIL.201511.1.1
--          [export].[WSUpdateReportSetting]
--              @profileName = 'EMAIL.201511.1.1',
--              @settingName = 'segments',
--              @settingValue = 'T-2-1'

--  Change History:
--
-----------------------------------------------------------------------------

begin

    set nocount on;

    -- perform some validation
    declare @err_msg [VARCHAR](1000),
            @current_setting_value [VARCHAR](4000);

    declare @profile TABLE ([ProfileName] [VARCHAR](100));

    if @channel not in ('email', 'print') begin
        set @err_msg = 'channel must be ''email'' or ''print''';

    end
    else if (@profileName is not null and (@channel is not null or @reportNumber is not null)) begin
        set @err_msg = 'specify either profile name or channel/report#...not both'

    end
    else if (@profileName is null and (@channel is null or @reportNumber is null)) begin
        set @err_msg = 'specify profile name or channel/report#'

    end
    else begin

        if (@profileName is null) begin

            -- find our profiles based on channel / report #
            insert into @profile([ProfileName])
                select distinct [report_number].[ProfileName]
                    from
                            (
                                select [ProfileName]
                                    from [Export].[WSReportSetting]
                                    where [Name] = 'report_number' and [Value] = cast(@reportNumber as varchar) and [ClientId] = @clientId
                            ) as [report_number]
                        inner join
                            (
                                select [ProfileName]
                                    from [Export].[WSReportSetting]
                                    where [Name] = 'channel' and [Value] = @channel and [ClientId] = @clientId
                            ) as [channel]
                            on [channel].[ProfileName] = [report_number].[ProfileName]
        end
        else begin
            insert into @profile([ProfileName]) values (@profileName);
        end

        -- If profile does not exist say so
        if not exists (select * from @profile) begin
            set @err_msg = 'there are no report profiles for channel=''' + @channel + ''' and report number = ' + cast(@reportNumber as varchar);

        end
        else if @allowAdd = 0 begin

            -- we are not allowing the addition of a new setting...therefore ensure we have the setting
            if not exists   (
                                select *  FROM
                                        @profile as [profile]
                                        inner join
                                            (
                                                select [ProfileName]
                                                    from [Export].[WSReportSetting]
                                                    where [Name] = @settingName and [ClientId] = @clientId
                                            ) as [setting_to_change]
                                            on [setting_to_change].[ProfileName] = [profile].[ProfileName]
                            ) begin
                set @err_msg =  'settingName = ''' + @settingName + ''' does not exist.  Set @allowAdd=1 if you really want to add a new setting';
            end
        end
    end

    -- Raise the error if it exists
    if @err_msg is not null begin
        raiserror(@err_msg , 11 , 1);
    end
    else BEGIN

        -- okay...lets update the values (we will use a cursor so we can print a friendly oldvalue / newvalue message)
        declare profile_cursor cursor local static forward_only for
            select [ProfileName] from @profile;

        open profile_cursor;

        fetch next from profile_cursor into @profileName;

        while @@fetch_status = 0 begin

            select @current_setting_value = [Value]
                from [Export].[WSReportSetting]
                where [ProfileName] = @profileName and [Name] = @settingName and [ClientId] = @clientId;

            merge into [Export].[WSReportSetting]  as [current]
                using
                    (
                        select @profileName as [ProfileName], @clientId as [ClientId], @settingName as [Name], @settingValue as [Value]
                    ) as [new]
                on
                    [current].[ProfileName] = [new].[ProfileName] and
                    [current].[ClientId]        = [new].[ClientId] and
                    [current].[Name]            = [new].[Name]

                when matched then
                    -- replace the existing row
                    update set
                        [current].[Value]                       = [new].[Value]

                when not matched by target then
                    -- create a new row
                    insert
                        (
                            [ProfileName],      [ClientId],         [Name],         [Value],        [description]
                        )
                        values
                        (
                            [new].[ProfileName],    [new].[ClientId],   [new].[Name],   [new].[Value],  ''
                        );

            print 'Weather Sensitivity Profile: ''' + @profileName + '''; setting: ''' + @settingName + '''';
            print '            oldValue: ''' + isnull(@current_setting_value, '') + '''';
            print '            newValue: ''' + @settingValue + '''';
            print '---------------------';

            fetch next from profile_cursor into @profileName;

        end

        close profile_cursor;
        deallocate profile_cursor;

    end
end

GO
