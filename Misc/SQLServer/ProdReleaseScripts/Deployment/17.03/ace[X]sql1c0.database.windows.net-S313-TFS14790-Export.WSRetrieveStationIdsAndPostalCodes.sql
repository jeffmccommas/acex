﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [Export].[WSRetrieveStationIdsAndPostalCodes]    Script Date: 3/6/2017 10:49:54 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSRetrieveStationIdsAndPostalCodes]') AND type in (N'P', N'PC'))
        DROP PROCEDURE [Export].[WSRetrieveStationIdsAndPostalCodes]
GO

/****** Object:  StoredProcedure [Export].[WSRetrieveStationIdsAndPostalCodes]    Script Date: 3/6/2017 10:49:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSRetrieveStationIdsAndPostalCodes]') AND type in (N'P', N'PC'))
    BEGIN
                EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WSRetrieveStationIdsAndPostalCodes] AS'
    END
GO

-- =============================================
-- Author:      Philip Victor
-- Create date: 3/6/2017
-- Description: Query to retrieve the weather stationIds and their associate zipcodes
-- =============================================
USE [InsightsDW_224_2017-02-14T21-44Z]
GO
/****** Object:  StoredProcedure [Export].[WSRetrieveStationIdsAndPostalCodes]    Script Date: 3/10/2017 4:24:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Philip Victor
-- Create date: 3/6/2017
-- Description: Query to retrieve the weather stationIds and their associate zipcodes
-- =============================================
ALTER PROCEDURE [Export].[WSRetrieveStationIdsAndPostalCodes]
    -- Add the parameters for the stored procedure here
    @ClientId INT ,
    @GroupId INT
AS
    BEGIN

            -- SET NOCOUNT ON added to prevent extra result sets from
            -- interfering with SELECT statements
                SET NOCOUNT ON;

            -- Place the content into a table variable so an index can be applied.
                DECLARE @EmZipCodes TABLE(
                    StationIdDaily  VARCHAR(10) INDEX IX1 CLUSTERED,
                    ZipCode VARCHAR(12) INDEX IX2 NONCLUSTERED([StationIdDaily],[ZipCode])
                );

                INSERT INTO @EmZipCodes
                    SELECT StationIdDaily, ZipCode FROM [cm].[EMZipcode];

                -- Get needed postal codes from benchmark groups
                DECLARE @PostalCodes TABLE (
                    PostalCode  VARCHAR(12) INDEX IX2 CLUSTERED,
                    GroupId INT INDEX IX3 NONCLUSTERED(GroupId,PostalCode)
                    );

                INSERT INTO @PostalCodes
                        SELECT SUBSTRING([dp].[PostalCode],1,5) AS PostalCode, [wsbmg].[GroupID]
                                FROM [Export].[WSBenchmarkGroups] wsbmg
                                    INNER JOIN  [dbo].[DimPremise] dp
                                     ON [dp].[ClientId] = [wsbmg].[ClientID] AND [dp].[PremiseKey] = [wsbmg].[PremiseKey]
                                WHERE  [wsbmg].[ClientID] = @ClientId  AND [dp].[PostalCode] IS NOT NULL
                        GROUP BY SUBSTRING([dp].[PostalCode],1,5), [wsbmg].[GroupID]

                ---- Determine the stationIds and ZipCodes.
                IF(@GroupId IS NOT NULL)
                        BEGIN

                                DELETE FROM @PostalCodes WHERE [GroupId] != @GroupId

                                SELECT [emz].[StationIdDaily],
                                                        CONVERT(VARCHAR(MAX), ISNULL(STUFF(( SELECT ', '
                                                                + LTRIM(RTRIM([emza].[ZipCode]))
                                                                FROM   @EmZipCodes emza
                                                                WHERE  [emza].[StationIdDaily] = [emz].[StationIdDaily]
                                                                FOR
                                                                    XML PATH('')
                                                                        ), 1, 1, ''), '')) AS PostalCodes
                                                FROM @EmZipCodes emz INNER JOIN
                                                @PostalCodes pc ON [emz].[ZipCode] = [pc].[PostalCode]

                            END
                      ELSE
                            BEGIN
                                        SELECT [emz].[StationIdDaily],
                                                                CONVERT(VARCHAR(MAX), ISNULL(STUFF(( SELECT ', '
                                                                    + LTRIM(RTRIM([emza].[ZipCode]))
                                                                    FROM   @EmZipCodes emza
                                                                    WHERE  [emza].[StationIdDaily] = [emz].[StationIdDaily]
                                                                    FOR
                                                                        XML PATH('')
                                                                            ), 1, 1, ''), '')) AS PostalCodes
                                                        FROM @EmZipCodes emz  WHERE  emz.[ZipCode]
                                                                            IN ( SELECT PostalCode FROM @PostalCodes )
                                                                                GROUP BY  [emz].[StationIdDaily]
                    END
        END


GO
