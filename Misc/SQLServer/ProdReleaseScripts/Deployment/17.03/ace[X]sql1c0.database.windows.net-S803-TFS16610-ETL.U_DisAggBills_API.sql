﻿
/**************************************************/
/***** RUN ONLY ON THE INSIGHTS DATABASE ***/
/*************************************************/

/****** Object:  Table [ETL].[U_DisAggBills_API]    Script Date: 3/20/2017 2:29:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[U_DisAggBills_API]') AND type in (N'U'))
        DROP TABLE [ETL].[U_DisAggBills_API]
GO

/****** Object:  Table [ETL].[U_DisAggBills_API]    Script Date: 3/20/2017 2:29:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[U_DisAggBills_API]') AND type in (N'U'))
BEGIN
        CREATE TABLE [ETL].[U_DisAggBills_API](
                            ClientId INT NOT NULL,
                            CustomerId VARCHAR(50) NOT NULL,
                            AccountId VARCHAR(50) NOT NULL,
                            PremiseId VARCHAR(50) NOT NULL,
                            StartDate DATETIME NOT NULL,
                            EndDate DATETIME NOT NULL,
                            TotalUnits DECIMAL(18,2) NOT NULL ,
                            TotalCost DECIMAL(18,2) NOT NULL,
                            CommodityKey VARCHAR(20) NULL,
                            UomKey VARCHAR(50) NOT NULL,
                            BillDays INT NOT NULL
        )
END
GO

