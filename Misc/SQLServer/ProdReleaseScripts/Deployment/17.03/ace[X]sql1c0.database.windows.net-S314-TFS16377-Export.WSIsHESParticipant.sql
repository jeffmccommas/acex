﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [Export].[WSRetrieveStationIdsAndPostalCodes]    Script Date: 3/6/2017 10:49:54 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSIsHESParticipant]') AND type in (N'P', N'PC'))
        DROP PROCEDURE [Export].[WSIsHESParticipant]
GO

/****** Object:  StoredProcedure [Export].[WSIsHESParticipant]    Script Date: 3/8/2017 14:26:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Export].[WSIsHESParticipant]') AND type in (N'P', N'PC'))
    BEGIN
                EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WSIsHESParticipant] AS'
    END
GO

-- =============================================
-- Author:      Muazzam Ali
-- Create date: 3/8/2017
-- Description: Query to find out if premise and account is HES participant
-- =============================================
ALTER PROCEDURE [Export].[WSIsHESParticipant]
    -- Add the parameters for the stored procedure here
    @AccountId VARCHAR(50) ,
    @PremiseId VARCHAR(50) 
AS
    
	BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements
        SET NOCOUNT ON;

		WITH home_energy_audit AS
							(SELECT DISTINCT
                                    PremiseID ,
                                    AccountID ,
                                    StatusKey ,
                                    ROW_NUMBER() OVER ( PARTITION BY PremiseID,
                                                        AccountID ORDER BY CreateDate DESC ) AS [rank]  -- most recent
                             FROM   [dbo].[factAction]
                             WHERE  ActionKey = 'audithome'
                           ) 
                SELECT * FROM home_energy_audit    WHERE @PremiseId = home_energy_audit.PremiseID
                                                     AND @AccountId = home_energy_audit.AccountID
                                                     AND home_energy_audit.StatusKey = 2   -- completed
                                                     AND home_energy_audit.rank = 1     -- most recent


SELECT @@rowcount

END

GO
