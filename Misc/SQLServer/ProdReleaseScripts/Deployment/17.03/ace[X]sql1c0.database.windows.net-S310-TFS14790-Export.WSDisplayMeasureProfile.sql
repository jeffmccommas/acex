﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  StoredProcedure [Export].[WSDisplayMeasureProfile]    Script Date: 2/8/2017 1:59:18 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[Export].[WSDisplayMeasureProfile]')
                    AND type IN ( N'P', N'PC' ) )
    DROP PROCEDURE [Export].[WSDisplayMeasureProfile];
GO

/****** Object:  StoredProcedure [Export].[WSDisplayMeasureProfile]    Script Date: 2/8/2017 1:59:18 PM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[Export].[WSDisplayMeasureProfile]')
                        AND type IN ( N'P', N'PC' ) )
    BEGIN
        EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WSDisplayMeasureProfile] AS';
    END;
GO

ALTER PROCEDURE [Export].[WSDisplayMeasureProfile]
    @profileName [VARCHAR](100) = NULL ,
    @clientId [INT] = 224
AS -------------------------------------------------------------------------------
--  Name:                   WSDisplayMeasureProfile
--  Author:                 Philip Victor
--  Description :       Display the settings for a weather sensitivity measure profile
--  Notes:
--
--  Parameters:
--      @profileName        -   the name of the profile.  If this is null all profiles will be displayed.
--      @clientId           -   identifies the client
--
--
--  Examples:
--      [export].[WSDisplayMeasureProfile] @profileName = 'EMAIL.201601.2.1.M1', @clientId = 224;
--
--  Change History:
--
-----------------------------------------------------------------------------

    BEGIN
        DECLARE @measure_profile TABLE
            (
              [profile_name] [VARCHAR](100)
            );


        DECLARE @setting_name [VARCHAR](100) ,
            @setting_value [VARCHAR](4000) ,
            @setting_description [VARCHAR](4000) ,
            @ordered_enduse_list [VARCHAR](1024) ,
            @profile_name [VARCHAR](100) ,
            @criteria_id [VARCHAR](50) ,
            @criteria_detail [VARCHAR](255) ,
            @criteria_sort [VARCHAR](255) ,
            @tab_char [CHAR](1) = CHAR(9);

        SET NOCOUNT ON;

        INSERT  INTO @measure_profile
                ( [profile_name]
                )
                SELECT DISTINCT
                        [ProfileName]
                FROM    [Export].[WSMeasureProfile]
                WHERE   ( ISNULL(LTRIM(RTRIM(@profileName)), '') = ''
                          OR [ProfileName] = @profileName
                        )
                        AND [ClientId] = @clientId
                ORDER BY [ProfileName];

        IF @@rowcount = 0
            BEGIN
                PRINT 'Weather Sensitivity Profile ''' + @profileName
                    + ''' NOT found.';
            END;
        ELSE
            BEGIN

                DECLARE profile_cursor CURSOR LOCAL STATIC FORWARD_ONLY
                FOR
                    SELECT  [profile_name]
                    FROM    @measure_profile;

                OPEN profile_cursor;

                FETCH NEXT FROM profile_cursor INTO @profile_name;

                WHILE @@fetch_status = 0
                    BEGIN

                        PRINT @profile_name;

            --  dump the soft modelled settings
                        DECLARE setting_cursor CURSOR LOCAL STATIC FORWARD_ONLY
                        FOR
                            SELECT  [Name] ,
                                    [Value] ,
                                    [Description]
                            FROM    [Export].[WSMeasureSetting]
                            WHERE   [ProfileName] = @profile_name
                            ORDER BY [Name];

                        OPEN setting_cursor;

                        FETCH NEXT FROM setting_cursor INTO @setting_name,
                            @setting_value, @setting_description;
                        WHILE @@fetch_status = 0
                            BEGIN

                                PRINT @tab_char
                                    + CAST(@setting_name AS CHAR(30))
                                    + ' : ''' + @setting_value + '''';

                                FETCH NEXT FROM setting_cursor INTO @setting_name,
                                    @setting_value, @setting_description;
                            END;
                        CLOSE setting_cursor;
                        DEALLOCATE setting_cursor;

            -- dump the hard modelled settings
                        SELECT  @setting_value = ISNULL([Type], '')
                        FROM    [Export].[WSMeasureProfile]
                        WHERE   [ProfileName] = @profile_name;
                        PRINT @tab_char + CAST('Type' AS CHAR(30)) + ' : '''
                            + @setting_value + '''';

                        SELECT  @setting_value = ISNULL([Description], '')
                        FROM    [Export].[WSMeasureProfile]
                        WHERE   [ProfileName] = @profile_name;
                        PRINT @tab_char + CAST('Description' AS CHAR(30))
                            + ' : ''' + @setting_value + '''';

                        SELECT  @ordered_enduse_list = ISNULL([OrderedEnduseList],
                                                              '')
                        FROM    [Export].[WSMeasureProfile]
                        WHERE   [ProfileName] = @profile_name;
                        PRINT @tab_char
                            + CAST('OrderedEnduseList' AS CHAR(30)) + ' : '''
                            + @ordered_enduse_list + '''';

                        SELECT  @criteria_id = ISNULL([CriteriaId1], '') ,
                                @criteria_detail = ISNULL([CriteriaDetail1],
                                                          '') ,
                                @criteria_sort = ISNULL([CriteriaSort1], '')
                        FROM    [Export].[WSMeasureProfile]
                        WHERE   [ProfileName] = @profile_name;
                        PRINT @tab_char
                            + CAST('CriteriaId1{id; detail; sort}' AS CHAR(30))
                            + ' : ''' + @criteria_id + '''; '''
                            + @criteria_detail + '''; ''' + @criteria_sort
                            + '''';

                        SELECT  @criteria_id = ISNULL([CriteriaId2], '') ,
                                @criteria_detail = ISNULL([CriteriaDetail2],
                                                          '') ,
                                @criteria_sort = ISNULL([CriteriaSort2], '')
                        FROM    [Export].[WSMeasureProfile]
                        WHERE   [ProfileName] = @profile_name;
                        PRINT @tab_char
                            + CAST('CriteriaId2{id; detail; sort}' AS CHAR(30))
                            + ' : ''' + @criteria_id + '''; '''
                            + @criteria_detail + '''; ''' + @criteria_sort
                            + '''';

                        SELECT  @criteria_id = ISNULL([CriteriaId3], '') ,
                                @criteria_detail = ISNULL([CriteriaDetail3],
                                                          '') ,
                                @criteria_sort = ISNULL([CriteriaSort3], '')
                        FROM    [Export].[WSMeasureProfile]
                        WHERE   [ProfileName] = @profile_name;
                        PRINT @tab_char
                            + CAST('CriteriaId3{id; detail; sort}' AS CHAR(30))
                            + ' : ''' + @criteria_id + '''; '''
                            + @criteria_detail + '''; ''' + @criteria_sort
                            + '''';

                        SELECT  @criteria_id = ISNULL([CriteriaId4], '') ,
                                @criteria_detail = ISNULL([CriteriaDetail4],
                                                          '') ,
                                @criteria_sort = ISNULL([CriteriaSort4], '')
                        FROM    [Export].[WSMeasureProfile]
                        WHERE   [ProfileName] = @profile_name;
                        PRINT @tab_char
                            + CAST('CriteriaId4{id; detail; sort}' AS CHAR(30))
                            + ' : ''' + @criteria_id + '''; '''
                            + @criteria_detail + '''; ''' + @criteria_sort
                            + '''';

                        SELECT  @criteria_id = ISNULL([CriteriaId5], '') ,
                                @criteria_detail = ISNULL([CriteriaDetail5],
                                                          '') ,
                                @criteria_sort = ISNULL([CriteriaSort5], '')
                        FROM    [Export].[WSMeasureProfile]
                        WHERE   [ProfileName] = @profile_name;
                        PRINT @tab_char
                            + CAST('CriteriaId5{id; detail; sort}' AS CHAR(30))
                            + ' : ''' + @criteria_id + '''; '''
                            + @criteria_detail + '''; ''' + @criteria_sort
                            + '''';

                        SELECT  @criteria_id = ISNULL([CriteriaId6], '') ,
                                @criteria_detail = ISNULL([CriteriaDetail6],
                                                          '') ,
                                @criteria_sort = ISNULL([CriteriaSort6], '')
                        FROM    [Export].[WSMeasureProfile]
                        WHERE   [ProfileName] = @profile_name;
                        PRINT @tab_char
                            + CAST('CriteriaId6{id; detail; sort}' AS CHAR(30))
                            + ' : ''' + @criteria_id + '''; '''
                            + @criteria_detail + '''; ''' + @criteria_sort
                            + '''';


                        PRINT '-------------------------------------------------------------------------------';
                        PRINT '';

                        FETCH NEXT FROM profile_cursor INTO @profile_name;
                    END;

                CLOSE profile_cursor;
                DEALLOCATE profile_cursor;

            END;
    END;

GO