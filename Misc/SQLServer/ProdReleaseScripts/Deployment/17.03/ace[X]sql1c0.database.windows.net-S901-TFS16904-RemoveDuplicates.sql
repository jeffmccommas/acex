﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  View [Holding].[v_ServicePoint]    Script Date: 3/24/2017 6:09:50 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Holding].[v_ServicePoint]'))
DROP VIEW [Holding].[v_ServicePoint]
GO

/****** Object:  View [Holding].[v_PremiseAttributes]    Script Date: 3/24/2017 6:09:50 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Holding].[v_PremiseAttributes]'))
DROP VIEW [Holding].[v_PremiseAttributes]
GO

/****** Object:  View [Holding].[v_Premise]    Script Date: 3/24/2017 6:09:50 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Holding].[v_Premise]'))
DROP VIEW [Holding].[v_Premise]
GO

/****** Object:  View [Holding].[v_Meter]    Script Date: 3/24/2017 6:09:50 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Holding].[v_Meter]'))
DROP VIEW [Holding].[v_Meter]
GO

/****** Object:  View [Holding].[v_Customer]    Script Date: 3/24/2017 6:09:50 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Holding].[v_Customer]'))
DROP VIEW [Holding].[v_Customer]
GO

/****** Object:  View [Holding].[v_Billing]    Script Date: 3/24/2017 6:09:50 PM ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Holding].[v_Billing]'))
DROP VIEW [Holding].[v_Billing]
GO

/****** Object:  View [Holding].[v_Billing]    Script Date: 3/24/2017 6:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Holding].[v_Billing]'))
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:      Jason Khourie
-- Create date: 613/2014
-- =============================================

CREATE VIEW [Holding].[v_Billing]
AS
    SELECT  theSelected.[ClientId] ,
            theSelected.[CustomerId] ,
            theSelected.[AccountId] ,
            theSelected.[PremiseId] ,
            theSelected.[ServiceContractId] ,
            theSelected.[ServicePointId] ,
            theSelected.[MeterId] ,
            theSelected.[RateClass] ,
            theSelected.[StartDate] ,
            theSelected.[EndDate] ,
            theSelected.[BillDays] ,
            theSelected.[TotalUnits] ,
            theSelected.[TotalCost] ,
            theSelected.[CommodityId] ,
            theSelected.[BillPeriodTypeId] ,
            theSelected.[BillCycleScheduleId] ,
            theSelected.[UOMId] ,
            theSelected.[DueDate] ,
            theSelected.[ReadDate] ,
            theSelected.[ReadQuality] ,
            theSelected.[SourceId] ,
            theSelected.TrackingId ,
            theSelected.TrackingDate ,
            theSelected.[BillingCostDetailsKey] ,
            theSelected.[ServiceCostDetailsKey]
    FROM    ( SELECT    b.[ClientId] ,
                        b.[CustomerId] ,
                        b.[AccountId] ,
                        b.[PremiseId] ,
                        b.[ServiceContractId] ,
                        b.[ServicePointId] ,
                        b.[MeterId] ,
                        b.[RateClass] ,
                        b.[BillStartDate] AS StartDate ,
                        b.[BillEndDate] AS EndDate ,
                        b.[BillDays] ,
                        b.[TotalUnits] ,
                        b.[TotalCost] ,
                        b.[CommodityId] ,
                        b.[BillPeriodTypeId] ,
                        b.[BillCycleScheduleId] ,
                        b.[UOMId] ,
                        b.[DueDate] ,
                        b.[ReadDate] ,
                        b.[ReadQuality] ,
                        b.[SourceId] ,
                        b.TrackingId ,
                        b.TrackingDate ,
                        b.[BillingCostDetailsKey] ,
                        b.[ServiceCostDetailsKey] ,
                        ROW_NUMBER() OVER ( PARTITION BY b.[ClientId],
                                            b.[PremiseId], b.[AccountId],
                                            b.[ServiceContractId],
                                            b.[BillStartDate], b.[BillEndDate],
                                            b.[CommodityId] ORDER BY b.TrackingDate DESC ) AS theRank
              FROM      Holding.Billing b WITH ( NOLOCK )
                        INNER JOIN Holding.Client cl ON cl.ClientId = b.ClientId
            ) theSelected
    WHERE   theSelected.theRank = 1;'
GO

/****** Object:  View [Holding].[v_Customer]    Script Date: 3/24/2017 6:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Holding].[v_Customer]'))
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:      Jason Khourie
-- Create date: 4/2/2014
-- Description: View of hld_User with Business and Unauthenticated users filtered out
-- =============================================

CREATE VIEW [Holding].[v_Customer]
AS
    SELECT  theSelected.ClientID ,
            theSelected.CustomerID ,
            theSelected.FirstName ,
            theSelected.LastName ,
            theSelected.Street1 ,
            theSelected.Street2 ,
            theSelected.City ,
            theSelected.[StateProvince] ,
            theSelected.[Country] ,
            theSelected.postalcode ,
            theSelected.MobilePhoneNumber ,
            theSelected.PhoneNumber ,
            theSelected.EmailAddress ,
            theSelected.AlternateEmailAddress ,
            theSelected.SourceId ,
            theSelected.AuthenticationTypeId ,
            theSelected.[TrackingId] ,
            theSelected.[TrackingDate]
    FROM    ( SELECT    c.ClientID ,
                        c.CustomerID ,
                        c.FirstName ,
                        c.LastName ,
                        c.Street1 ,
                        c.Street2 ,
                        c.City ,
                        c.[State] AS StateProvince ,
                        c.Country AS Country ,
                        c.postalcode ,
                        c.MobilePhoneNumber ,
                        c.PhoneNumber ,
                        c.EmailAddress ,
                        c.AlternateEmailAddress ,
                        c.SourceId ,
                        1 AS AuthenticationTypeId ,
                        c.[TrackingId] ,
                        c.[TrackingDate] ,
                        ROW_NUMBER() OVER ( PARTITION BY c.[ClientID],
                                            c.CustomerID ORDER BY c.TrackingDate DESC ) AS theRank
              FROM      Holding.Customer c WITH ( NOLOCK )
                        INNER JOIN Holding.Client cl ON cl.ClientId = c.ClientID
            ) theSelected
    WHERE   theSelected.theRank = 1;'
GO

/****** Object:  View [Holding].[v_Meter]    Script Date: 3/24/2017 6:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Holding].[v_Meter]'))
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:      Jason Khourie
-- Create date: 7/1/2014
-- =============================================

CREATE VIEW [Holding].[v_Meter]
AS
    SELECT  theSelected.[ClientId] ,
                        theSelected.[ServicePointId] ,
                        theSelected.[MeterId] ,
                        theSelected.[RateClass] ,
                        theSelected.[SourceId] ,
                        theSelected.TrackingId ,
                        theSelected.TrackingDate
    FROM    ( SELECT hb.[ClientId] ,
                        hb.[ServicePointId] ,
                        hb.[MeterId] ,
                        hb.[RateClass] ,
                        hb.[SourceId] ,
                        hb.TrackingId ,
                        hb.TrackingDate ,
                        ROW_NUMBER() OVER ( PARTITION BY hb.[ClientId],
                                                                                            hb.[ServicePointId],
                                                                                            hb.[MeterId],
                                                                                            hb.[RateClass]
                                            ORDER BY hb.TrackingDate DESC ) AS theRank
              FROM      Holding.Billing hb WITH ( NOLOCK )
                        INNER JOIN Holding.Client cl ON cl.ClientId = hb.ClientId
            ) theSelected
    WHERE   theSelected.theRank = 1;'
GO

/****** Object:  View [Holding].[v_Premise]    Script Date: 3/24/2017 6:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Holding].[v_Premise]'))
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:      Jason Khourie
-- Create date: 4/21/2014
-- Description: View of hld_User with Business and Unauthenticated users filtered out
-- =============================================

CREATE VIEW [Holding].[v_Premise]
AS
    SELECT  theSelected.ClientID ,
            theSelected.PremiseID ,
            theSelected.AccountId ,
            theSelected.Street1 ,
            theSelected.Street2 ,
            theSelected.City ,
            theSelected.StateProvince ,
            theSelected.Country ,
            theSelected.postalcode ,
            theSelected.GasService ,
            theSelected.ElectricService ,
            theSelected.WaterService ,
            theSelected.SourceId ,
            theSelected.[TrackingID] ,
            theSelected.[TrackingDate]
    FROM    ( SELECT    p.ClientID ,
                        p.PremiseID ,
                        p.AccountID AS AccountId ,
                        p.Street1 ,
                        p.Street2 ,
                        p.City ,
                        p.[State] AS StateProvince ,
                        p.Country ,
                        p.postalcode ,
                        GasService ,
                        ElectricService ,
                        WaterService ,
                        p.SourceId ,
                        p.[TrackingID] ,
                        p.[TrackingDate] ,
                        ROW_NUMBER() OVER ( PARTITION BY p.[PremiseID],
                                            p.[AccountID], p.[ClientID] ORDER BY p.TrackingDate DESC ) AS theRank
              FROM      Holding.Premise p WITH ( NOLOCK )
                        INNER JOIN Holding.Client cl ON cl.ClientId = p.ClientID
            ) theSelected
    WHERE   theSelected.theRank = 1;'
GO

/****** Object:  View [Holding].[v_PremiseAttributes]    Script Date: 3/24/2017 6:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Holding].[v_PremiseAttributes]'))
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:      Jason Khourie
-- Create date: 6/24/2014
-- =============================================

CREATE VIEW [Holding].[v_PremiseAttributes]
AS
    SELECT  theSelected.ClientID ,
            theSelected.AccountID ,
            theSelected.CustomerID ,
            theSelected.PremiseID ,
            theSelected.AttributeId ,
            theSelected.AttributeKey ,
            theSelected.OptionId ,
            theSelected.OptionValue ,
            theSelected.SourceId ,
            theSelected.TrackingID ,
            theSelected.TrackingDate
    FROM    ( SELECT    pa.ClientID ,
                        pa.AccountID ,
                        pa.CustomerID ,
                        pa.PremiseID ,
                        pa.AttributeId ,
                        pa.AttributeKey ,
                        pa.OptionId ,
                        pa.OptionValue ,
                        pa.SourceId ,
                        pa.TrackingID ,
                        pa.TrackingDate ,
                        ROW_NUMBER() OVER ( PARTITION BY pa.ClientID,
                                            pa.AccountID, pa.CustomerID,
                                            pa.PremiseID, pa.AttributeId ORDER BY pa.TrackingDate DESC ) AS theRank
              FROM      Holding.PremiseAttributes pa WITH ( NOLOCK )
                        INNER JOIN Holding.Client cl ON cl.ClientId = pa.ClientID
            ) theSelected
    WHERE   theSelected.theRank = 1;'
GO

/****** Object:  View [Holding].[v_ServicePoint]    Script Date: 3/24/2017 6:09:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Holding].[v_ServicePoint]'))
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:      Jason Khourie
-- Create date: 7/1/2014
-- =============================================

CREATE VIEW [Holding].[v_ServicePoint]
AS
    SELECT  theSelected.[ClientId] ,
            theSelected.[CustomerId] ,
            theSelected.[AccountId] ,
            theSelected.[PremiseId] ,
            theSelected.[ServicePointId] ,
            theSelected.[SourceId] ,
            theSelected.TrackingId ,
            theSelected.TrackingDate
    FROM    ( SELECT    cl.[ClientId] ,
                        hb.[CustomerId] ,
                        hb.[AccountId] ,
                        hb.[PremiseId] ,
                        hb.[ServicePointId] ,
                        hb.[SourceId] ,
                        hb.TrackingId ,
                        hb.TrackingDate,
                        ROW_NUMBER() OVER ( PARTITION BY hb.[ClientId],
                                                                                            hb.[CustomerId],
                                                                                            hb.[AccountId],
                                                                                            hb.[PremiseId],
                                                                                            hb.[ServicePointId]
                                            ORDER BY hb.TrackingDate DESC ) AS theRank
              FROM  Holding.Billing hb WITH ( NOLOCK )
                        INNER JOIN Holding.Client cl ON cl.ClientId = hb.ClientId
            ) theSelected
    WHERE   theSelected.theRank = 1;'
GO
