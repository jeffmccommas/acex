/*
Run this script on: 
		UAT Environment - Azure SQL
		
        aceusql1c0.database.windows.net.InsightsMetaData    <--  This database will be modified

to synchronize it with:
		DEV and QA environments - Azure SQL
		
        aceqsql1c0.database.windows.net.InsightsMetaData

Be sure to have admin privileges to execute successfully

*/
CREATE SCHEMA [rateengine]
GO


SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO


PRINT N'Creating [rateengine].[UseCharge]'
GO
CREATE TABLE [rateengine].[UseCharge]
(
[RateDefID] [int] NOT NULL,
[TierID] [tinyint] NOT NULL,
[TouID] [tinyint] NOT NULL,
[SeasonID] [tinyint] NOT NULL,
[CostTypeID] [int] NOT NULL,
[PartTypeID] [tinyint] NOT NULL CONSTRAINT [DF_UseCharge_PtID] DEFAULT ((0)),
[ChargeValue] [float] NOT NULL,
[RgID] [int] NULL,
[Description] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RTPgId] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_UseCharge] on [rateengine].[UseCharge]'
GO
ALTER TABLE [rateengine].[UseCharge] ADD CONSTRAINT [PK_UseCharge] PRIMARY KEY CLUSTERED  ([RateDefID], [TierID], [TouID], [SeasonID], [CostTypeID], [PartTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_UseCharge_a] on [rateengine].[UseCharge]'
GO
CREATE NONCLUSTERED INDEX [IX_UseCharge_a] ON [rateengine].[UseCharge] ([RateDefID], [SeasonID], [TouID], [TierID]) INCLUDE ([ChargeValue], [CostTypeID], [Description], [PartTypeID], [RgID], [RTPgId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[TimeOfUseBoundary]'
GO
CREATE TABLE [rateengine].[TimeOfUseBoundary]
(
[RateDefID] [int] NOT NULL,
[TouID] [tinyint] NOT NULL,
[SeasonID] [tinyint] NOT NULL,
[DayTypeID] [tinyint] NOT NULL,
[StartTime] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EndTime] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TimeOfUseBoundary] on [rateengine].[TimeOfUseBoundary]'
GO
ALTER TABLE [rateengine].[TimeOfUseBoundary] ADD CONSTRAINT [PK_TimeOfUseBoundary] PRIMARY KEY CLUSTERED  ([RateDefID], [TouID], [SeasonID], [DayTypeID], [StartTime], [EndTime])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[TierBoundary]'
GO
CREATE TABLE [rateengine].[TierBoundary]
(
[RateDefID] [int] NOT NULL,
[TierID] [tinyint] NOT NULL,
[TouID] [tinyint] NOT NULL,
[SeasonID] [tinyint] NOT NULL,
[BoundaryValue] [int] NOT NULL,
[SecondaryBoundaryValue] [int] NOT NULL CONSTRAINT [DF_TierBoundary_SecondaryBoundaryValue] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TierBoundary] on [rateengine].[TierBoundary]'
GO
ALTER TABLE [rateengine].[TierBoundary] ADD CONSTRAINT [PK_TierBoundary] PRIMARY KEY CLUSTERED  ([RateDefID], [TierID], [TouID], [SeasonID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[TerritoryBaseRateClass]'
GO
CREATE TABLE [rateengine].[TerritoryBaseRateClass]
(
[RateDefID] [int] NOT NULL,
[LgID] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TerritoryBaseRateClass] on [rateengine].[TerritoryBaseRateClass]'
GO
ALTER TABLE [rateengine].[TerritoryBaseRateClass] ADD CONSTRAINT [PK_TerritoryBaseRateClass] PRIMARY KEY CLUSTERED  ([RateDefID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[TaxCharge]'
GO
CREATE TABLE [rateengine].[TaxCharge]
(
[RateDefID] [int] NOT NULL,
[CostTypeID] [int] NOT NULL,
[ChargeValue] [float] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TaxCharge] on [rateengine].[TaxCharge]'
GO
ALTER TABLE [rateengine].[TaxCharge] ADD CONSTRAINT [PK_TaxCharge] PRIMARY KEY CLUSTERED  ([RateDefID], [CostTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[StepBoundary]'
GO
CREATE TABLE [rateengine].[StepBoundary]
(
[RateDefID] [int] NOT NULL,
[StepID] [tinyint] NOT NULL,
[SeasonID] [tinyint] NOT NULL,
[BoundaryValue] [float] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_StepBoundary] on [rateengine].[StepBoundary]'
GO
ALTER TABLE [rateengine].[StepBoundary] ADD CONSTRAINT [PK_StepBoundary] PRIMARY KEY CLUSTERED  ([RateDefID], [StepID], [SeasonID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[ServiceCharge]'
GO
CREATE TABLE [rateengine].[ServiceCharge]
(
[RateDefID] [int] NOT NULL,
[CalcTypeID] [tinyint] NOT NULL,
[CostTypeID] [int] NOT NULL,
[ChargeValue] [float] NOT NULL,
[StepID] [tinyint] NOT NULL CONSTRAINT [DF_ServiceCharge_StepID] DEFAULT ((0)),
[SeasonID] [tinyint] NOT NULL CONSTRAINT [DF_ServiceCharge_SeasonID] DEFAULT ((0)),
[RebateClassID] [tinyint] NOT NULL CONSTRAINT [DF_ServiceCharge_RebateClassID] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ServiceCharge] on [rateengine].[ServiceCharge]'
GO
ALTER TABLE [rateengine].[ServiceCharge] ADD CONSTRAINT [PK_ServiceCharge] PRIMARY KEY CLUSTERED  ([RateDefID], [StepID], [SeasonID], [CalcTypeID], [CostTypeID], [RebateClassID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SeasonBoundary]'
GO
CREATE TABLE [rateengine].[SeasonBoundary]
(
[RateDefID] [int] NOT NULL,
[SeasonID] [tinyint] NOT NULL,
[StartDay] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EndDay] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SeasonBoundary] on [rateengine].[SeasonBoundary]'
GO
ALTER TABLE [rateengine].[SeasonBoundary] ADD CONSTRAINT [PK_SeasonBoundary] PRIMARY KEY CLUSTERED  ([RateDefID], [SeasonID], [StartDay], [EndDay])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RateDefinition]'
GO
CREATE TABLE [rateengine].[RateDefinition]
(
[RateDefID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[RdRateMasterID] [int] NOT NULL,
[RdStartDate] [datetime] NOT NULL,
[RdEditDate] [datetime] NULL,
[RdDifferenceNote] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RdSeasonalInd] [bit] NOT NULL CONSTRAINT [DF_RateDefinition_RdSeasonalInd] DEFAULT ((0)),
[RdTieredInd] [bit] NOT NULL CONSTRAINT [DF_RateDefinition_RdTieredInd] DEFAULT ((0)),
[RdTimeOfUseInd] [bit] NOT NULL CONSTRAINT [DF_RateDefinition_RdTimeOfUseInd] DEFAULT ((0)),
[RdDemandInd] [bit] NOT NULL CONSTRAINT [DF_RateDefinition_RdDemandInd] DEFAULT ((0)),
[RdFuelAdjustmentInd] [bit] NOT NULL CONSTRAINT [DF_RateDefinition_RdFuelAdjustmentInd] DEFAULT ((0)),
[RdBasicInd] [bit] NOT NULL CONSTRAINT [DF_RateDefinition_RdBasicInd] DEFAULT ((0)),
[RdTsID] [int] NOT NULL CONSTRAINT [DF_RateDefinition_RdTsID] DEFAULT ((0)),
[RdDsID] [int] NOT NULL CONSTRAINT [DF_RateDefinition_RdDsID] DEFAULT ((0)),
[RdSeasonNum] [tinyint] NOT NULL CONSTRAINT [DF_RateDefinition_RdSeasonNum] DEFAULT ((0)),
[RdTierNum] [tinyint] NOT NULL CONSTRAINT [DF_RateDefinition_RdTierNum] DEFAULT ((0)),
[RdTOUNum] [tinyint] NOT NULL CONSTRAINT [DF_RateDefinition_RdTierNum1] DEFAULT ((0)),
[RdDemandNum] [tinyint] NOT NULL CONSTRAINT [DF_RateDefinition_RdTierNum1_1] DEFAULT ((0)),
[RdStartDateValidInd] [bit] NOT NULL CONSTRAINT [DF_RateDefinition_RdStartDateValidInd] DEFAULT ((0)),
[RdEnableInd] [tinyint] NOT NULL CONSTRAINT [DF_RateDefinition_RdEnableInd] DEFAULT ((0)),
[RdSpID] [tinyint] NOT NULL CONSTRAINT [DF_RateDefinition_RdSpID] DEFAULT ((3)),
[RdServiceStepsInd] [tinyint] NOT NULL CONSTRAINT [DF_RateDefinition_RdServiceStepsInd] DEFAULT ((0)),
[RdStepNum] [tinyint] NOT NULL CONSTRAINT [DF_RateDefinition_RdStepNum] DEFAULT ((0)),
[RdTOUBoundariesConsistent] [tinyint] NOT NULL CONSTRAINT [DF_RateDefinition_RdTOUBoundariesConsistent] DEFAULT ((1)),
[RdRTPInd] [bit] NULL,
[RdRTPStreamId] [tinyint] NULL,
[RdDisableOnProdInd] [bit] NOT NULL CONSTRAINT [DF_RateDefinition_RdDisableOnProdInd] DEFAULT ((0)),
[RdBrID] [tinyint] NOT NULL CONSTRAINT [DF_RateDefinition_RdBrID] DEFAULT ((0)),
[RdNmID] [tinyint] NOT NULL CONSTRAINT [DF_RateDefinition_RdNmID] DEFAULT ((0)),
[RdNetMeteringInd] [bit] NOT NULL CONSTRAINT [DF_RateDefinition_RdNetMeteringInd] DEFAULT ((0)),
[RdSdpID] [tinyint] NOT NULL CONSTRAINT [DF_RateDefinition_RdSdpID] DEFAULT ((1)),
[RdTierAccumByTOUInd] [bit] NOT NULL CONSTRAINT [DF_RateDefinition_RdTierAccumByTOUInd] DEFAULT ((0)),
[RdCapResInd] [bit] NOT NULL CONSTRAINT [DF_RateDefinition_RdCapResInd] DEFAULT ((0)),
[RdCPEventUsageAdderInd] [bit] NOT NULL CONSTRAINT [DF_RateDefinition_RdCPEventUsageAdderInd] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RateDefinition] on [rateengine].[RateDefinition]'
GO
ALTER TABLE [rateengine].[RateDefinition] ADD CONSTRAINT [PK_RateDefinition] PRIMARY KEY CLUSTERED  ([RateDefID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_RdDisableOnProdInd] on [rateengine].[RateDefinition]'
GO
CREATE NONCLUSTERED INDEX [IX_RdDisableOnProdInd] ON [rateengine].[RateDefinition] ([RdDisableOnProdInd], [RateDefID], [RdEnableInd]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_RateDefinition_b] on [rateengine].[RateDefinition]'
GO
CREATE NONCLUSTERED INDEX [IX_RateDefinition_b] ON [rateengine].[RateDefinition] ([RdRateMasterID], [RdEnableInd], [RdStartDate], [RateDefID], [RdSeasonalInd], [RdTieredInd], [RdTimeOfUseInd], [RdDemandInd], [RdFuelAdjustmentInd], [RdBasicInd], [RdServiceStepsInd], [RdTsID], [RdDsID], [RdSpID], [RdDifferenceNote], [RdTOUBoundariesConsistent]) INCLUDE ([RdRTPInd], [RdRTPStreamId]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_RD1] on [rateengine].[RateDefinition]'
GO
CREATE NONCLUSTERED INDEX [IX_RD1] ON [rateengine].[RateDefinition] ([RdRateMasterID], [RdStartDate]) INCLUDE ([RateDefID], [RdBasicInd], [RdDemandInd], [RdDifferenceNote], [RdDsID], [RdFuelAdjustmentInd], [RdRTPInd], [RdRTPStreamId], [RdSeasonalInd], [RdSpID], [RdTieredInd], [RdTimeOfUseInd], [RdTOUBoundariesConsistent], [RdTsID]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_RateDefinition_a] on [rateengine].[RateDefinition]'
GO
CREATE NONCLUSTERED INDEX [IX_RateDefinition_a] ON [rateengine].[RateDefinition] ([RdStartDate] DESC, [RdEnableInd], [RateDefID], [RdRateMasterID], [RdSeasonalInd], [RdTieredInd], [RdTimeOfUseInd], [RdDemandInd], [RdFuelAdjustmentInd], [RdBasicInd], [RdServiceStepsInd], [RdTsID], [RdDsID], [RdSpID], [RdDifferenceNote], [RdTOUBoundariesConsistent]) INCLUDE ([RdRTPInd], [RdRTPStreamId]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [NC_RateDefinition_RdEnableInd_RdStartDate] on [rateengine].[RateDefinition]'
GO
CREATE NONCLUSTERED INDEX [NC_RateDefinition_RdEnableInd_RdStartDate] ON [rateengine].[RateDefinition] ([RdEnableInd], [RdStartDate]) INCLUDE ([RdRateMasterID]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_RD2] on [rateengine].[RateDefinition]'
GO
CREATE NONCLUSTERED INDEX [IX_RD2] ON [rateengine].[RateDefinition] ([RdStartDate], [RdEnableInd], [RdRateMasterID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[MinimumCharge]'
GO
CREATE TABLE [rateengine].[MinimumCharge]
(
[RateDefID] [int] NOT NULL,
[CalcTypeID] [tinyint] NOT NULL,
[CostTypeID] [int] NOT NULL,
[ChargeValue] [float] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MinimumCharge] on [rateengine].[MinimumCharge]'
GO
ALTER TABLE [rateengine].[MinimumCharge] ADD CONSTRAINT [PK_MinimumCharge] PRIMARY KEY CLUSTERED  ([RateDefID], [CalcTypeID], [CostTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthCloneRateDefinition]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


CREATE procedure [rateengine].[AuthCloneRateDefinition] ( @sourceRdID int, @sourceRmID int, @RateMasterID int, @StartDate As Datetime, @EditDate As Datetime, @Enabled As tinyint, @StartDateValid As tinyint) AS
	
set NOCOUNT on 
	
	declare @newRdID As Int		
	  
	BEGIN TRAN

	INSERT INTO RateDefinition
	(RdRateMasterID, RdStartDate, RdEditDate, RdSeasonalInd, RdTieredInd, RdTimeOfUseInd, RdDemandInd,
		RdFuelAdjustmentInd, RdServiceStepsInd, RdBasicInd, RdTsID, RdDsID, RdSpID, RdSdpID, RdEnableInd, RdSeasonNum, RdTierNum, RdTOUNum, RdDemandNum, RdStepNum, RdStartDateValidInd, RdTOUBoundariesConsistent, RdRTPInd, RdRTPStreamId, RdBrID, RdNmID, RdNetMeteringInd, RdCapResInd, RdCPEventUsageAdderInd)
	SELECT @RateMasterID As RdRateMasterID, @StartDate As RdStartDate, @EditDate As RdEditDate, RdSeasonalInd, RdTieredInd, RdTimeOfUseInd, RdDemandInd,
		RdFuelAdjustmentInd, RdServiceStepsInd, RdBasicInd, RdTsID, RdDsID, RdSpID, RdSdpID, @Enabled As RdEnableInd, RdSeasonNum, RdTierNum, RdTOUNum, RdDemandNum, RdStepNum, @StartDateValid As RdStartDateValidInd, RdTOUBoundariesConsistent, RdRTPInd, RdRTPStreamId, RdBrID, RdNmID, RdNetMeteringInd, RdCapResInd, RdCPEventUsageAdderInd FROM RateDefinition WHERE
		RateDefID = @sourceRdID
	;Select @newRdID = SCOPE_IDENTITY()

	INSERT INTO UseCharge
	(RateDefID, TierID, TouID, SeasonID, CostTypeID, PartTypeID, ChargeValue, RgID, [Description], RTPgId)
	SELECT @newRdID As RateDefID, TierID, TouID, SeasonID, CostTypeID, PartTypeID, ChargeValue, RgID, [Description], RTPgId FROM UseCharge 
		WHERE RateDefID = @sourceRdID

	INSERT INTO ServiceCharge
	(RateDefID, CalcTypeID, CostTypeID, StepID, SeasonID, ChargeValue, RebateClassID) 
	SELECT @newRdID As RateDefID, CalcTypeID, CostTypeID, StepID, SeasonID, ChargeValue, RebateClassID FROM ServiceCharge
		WHERE RateDefID = @sourceRdID

	INSERT INTO MinimumCharge
	(RateDefID, CalcTypeID, CostTypeID, ChargeValue)
	SELECT @newRdID As RateDefID, CalcTypeID, CostTypeID, ChargeValue FROM MinimumCharge
		WHERE RateDefID = @sourceRdID

	INSERT INTO TaxCharge
	(RateDefID, CostTypeID, ChargeValue)
	SELECT @newRdID As RateDefID, CostTypeID, ChargeValue FROM TaxCharge
		WHERE RateDefID = @sourceRdID

	INSERT INTO TierBoundary
	(RateDefID, TierID, TouID, SeasonID, BoundaryValue, SecondaryBoundaryValue)
	SELECT @newRdID As RateDefID, TierID, TouID, SeasonID, BoundaryValue, SecondaryBoundaryValue FROM TierBoundary
		WHERE RateDefID = @sourceRdID

	INSERT INTO TimeOfUseBoundary
	(RateDefID, TouID, SeasonID, DayTypeID, StartTime, EndTime)
	SELECT @newRdID As RateDefID, TouID, SeasonID, DayTypeID, StartTime, EndTime FROM TimeOfUseBoundary
		WHERE RateDefID = @sourceRdID

	INSERT INTO SeasonBoundary
	(RateDefID, SeasonID, StartDay, EndDay)
	SELECT @newRdID As RateDefID, SeasonID, StartDay, EndDay FROM SeasonBoundary
		WHERE RateDefID = @sourceRdID

	INSERT INTO StepBoundary
	(RateDefID, StepID, SeasonID, BoundaryValue)
	SELECT @newRdID As RateDefID, StepID, SeasonID, BoundaryValue FROM StepBoundary
		WHERE RateDefID = @sourceRdID
		
	INSERT INTO TerritoryBaseRateClass
	(RateDefID, LgID)
	SELECT @newRdID As RateDefID, LgID FROM TerritoryBaseRateClass
		WHERE RateDefID = @sourceRdID
	

	if( @@ERROR = 0 ) begin
		COMMIT TRAN
	end
	else begin 
		ROLLBACK TRAN	
	end

	SELECT @newRdID As newRdID

set NOCOUNT off




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RateMasterChildren]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[RateMasterChildren]
(
[RateMasterID] [int] NOT NULL,
[RateMasterIDchild] [int] NOT NULL,
[Description] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RateMasterChildren] on [rateengine].[RateMasterChildren]'
GO
ALTER TABLE [rateengine].[RateMasterChildren] ADD CONSTRAINT [PK_RateMasterChildren] PRIMARY KEY CLUSTERED  ([RateMasterID], [RateMasterIDchild])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthCopyRateMasterChildren]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

create procedure [rateengine].AuthCopyRateMasterChildren( @RateMasterID int, @sourceRateMasterID int ) AS
		
set NOCOUNT on 
	
		INSERT INTO RateMasterChildren
			(RateMasterID, RateMasterIDchild)
		SELECT @RateMasterID As RateMasterID, RateMasterIDchild FROM RateMasterChildren
			WHERE RateMasterID = @sourceRateMasterID

set NOCOUNT off
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthDelChildRateClass]'
GO

create procedure [rateengine].[AuthDelChildRateClass] @RateMasterID As int, @RateMasterIDchild int  AS

	DELETE FROM RateMasterChildren WHERE RateMasterID = @RateMasterID AND RateMasterIDchild = @RateMasterIDchild

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RateMasterText]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[RateMasterText]
(
[RateMasterID] [int] NOT NULL,
[LaID] [int] NOT NULL,
[Name] [nvarchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Eligibility] [nvarchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RateMasterText] on [rateengine].[RateMasterText]'
GO
ALTER TABLE [rateengine].[RateMasterText] ADD CONSTRAINT [PK_RateMasterText] PRIMARY KEY CLUSTERED  ([RateMasterID], [LaID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_RateMasterText_a] on [rateengine].[RateMasterText]'
GO
CREATE NONCLUSTERED INDEX [IX_RateMasterText_a] ON [rateengine].[RateMasterText] ([RateMasterID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_RateMasterText_b] on [rateengine].[RateMasterText]'
GO
CREATE NONCLUSTERED INDEX [IX_RateMasterText_b] ON [rateengine].[RateMasterText] ([RateMasterID], [Name], [Description], [Eligibility])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RateMasterServiceTerritory]'
GO
CREATE TABLE [rateengine].[RateMasterServiceTerritory]
(
[RateMasterID] [int] NOT NULL,
[CompanyID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RateMasterServiceTerritory] on [rateengine].[RateMasterServiceTerritory]'
GO
ALTER TABLE [rateengine].[RateMasterServiceTerritory] ADD CONSTRAINT [PK_RateMasterServiceTerritory] PRIMARY KEY CLUSTERED  ([RateMasterID], [CompanyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RateMaster]'
GO
CREATE TABLE [rateengine].[RateMaster]
(
[RateMasterID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[RmClientRateID] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RmRateCompanyID] [int] NOT NULL,
[RmFuID] [tinyint] NOT NULL,
[RmSvID] [tinyint] NOT NULL,
[RmCtID] [tinyint] NOT NULL,
[RmDefault] [tinyint] NOT NULL CONSTRAINT [DF_RateProductMaster_RPDefault] DEFAULT ((0)),
[RmUnID] [tinyint] NOT NULL,
[RmPrID] [tinyint] NOT NULL,
[RmPoID] [int] NOT NULL CONSTRAINT [DF_RateMaster_RmPoID] DEFAULT ((0)),
[RmEnableInd] [tinyint] NOT NULL,
[RmLtID] [tinyint] NOT NULL,
[RmChildrenInd] [bit] NOT NULL,
[RmBdID] [tinyint] NOT NULL CONSTRAINT [DF_RateMaster_RmBdID] DEFAULT ((0)),
[RmHgID] [int] NULL,
[RmDPrID] [tinyint] NOT NULL CONSTRAINT [DF_RateMaster_RmDPrID] DEFAULT ((1))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RateProductMaster] on [rateengine].[RateMaster]'
GO
ALTER TABLE [rateengine].[RateMaster] ADD CONSTRAINT [PK_RateProductMaster] PRIMARY KEY CLUSTERED  ([RateMasterID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_RateMaster_b] on [rateengine].[RateMaster]'
GO
CREATE NONCLUSTERED INDEX [IX_RateMaster_b] ON [rateengine].[RateMaster] ([RateMasterID], [RmPoID], [RmRateCompanyID], [RmClientRateID], [RmFuID], [RmSvID], [RmCtID], [RmUnID], [RmPrID], [RmDefault], [RmChildrenInd], [RmLtID], [RmBdID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_RateMaster_c] on [rateengine].[RateMaster]'
GO
CREATE NONCLUSTERED INDEX [IX_RateMaster_c] ON [rateengine].[RateMaster] ([RmEnableInd], [RateMasterID], [RmRateCompanyID], [RmClientRateID], [RmPoID], [RmFuID], [RmSvID], [RmCtID], [RmUnID], [RmPrID], [RmDefault], [RmChildrenInd], [RmLtID], [RmBdID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_RM1] on [rateengine].[RateMaster]'
GO
CREATE NONCLUSTERED INDEX [IX_RM1] ON [rateengine].[RateMaster] ([RmRateCompanyID], [RmEnableInd], [RmPoID], [RateMasterID]) INCLUDE ([RmClientRateID], [RmCtID], [RmDefault], [RmFuID], [RmPrID], [RmSvID], [RmUnID]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_RateMaster_a] on [rateengine].[RateMaster]'
GO
CREATE NONCLUSTERED INDEX [IX_RateMaster_a] ON [rateengine].[RateMaster] ([RmClientRateID], [RmRateCompanyID], [RmEnableInd], [RmPoID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[Difference]'
GO
CREATE TABLE [rateengine].[Difference]
(
[RateDefID] [int] NOT NULL,
[DfID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RateDefinitionDifference] on [rateengine].[Difference]'
GO
ALTER TABLE [rateengine].[Difference] ADD CONSTRAINT [PK_RateDefinitionDifference] PRIMARY KEY CLUSTERED  ([RateDefID], [DfID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthDeleteRateDefinition]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
create procedure [rateengine].AuthDeleteRateDefinition ( @RateDefID int ) AS
		
set NOCOUNT on 
	
	declare @RdRateMasterID As Int		
	declare @num As Int
	  
	BEGIN TRAN

	SELECT @RdRateMasterID = RdRateMasterID FROM RateDefinition WHERE RateDefID = @RateDefID

	SELECT @num = count(*) FROM RateDefinition WHERE RdRateMasterID = @RdRateMasterID

	DELETE FROM SeasonBoundary WHERE RateDefID = @RateDefID
	
	DELETE FROM TimeOfUseBoundary WHERE RateDefID = @RateDefID
	
	DELETE FROM TierBoundary WHERE RateDefID = @RateDefID
	
	DELETE FROM TaxCharge WHERE RateDefID = @RateDefID
	
	DELETE FROM MinimumCharge WHERE RateDefID = @RateDefID

	DELETE FROM StepBoundary WHERE RateDefID = @RateDefID
		
	DELETE FROM ServiceCharge WHERE RateDefID = @RateDefID
	
	DELETE FROM UseCharge WHERE RateDefID = @RateDefID

	DELETE FROM [Difference] WHERE RateDefID = @RateDefID
		
	DELETE FROM TerritoryBaseRateClass WHERE RateDefID = @RateDefID
		
	DELETE FROM RateDefinition WHERE RateDefID = @RateDefID

	-- It is last RateDef, so also delete the master
	if( @num = 1 ) begin
		DELETE FROM RateMasterText WHERE RateMasterID = @RdRateMasterID

		DELETE FROM RateMasterServiceTerritory WHERE RateMasterID = @RdRateMasterID

		DELETE FROM RateMasterChildren WHERE RateMasterID = @RdRateMasterID

		DELETE FROM RateMaster WHERE RateMasterID = @RdRateMasterID
	end

	if( @@ERROR = 0 ) begin
		COMMIT TRAN
	end
	else begin 
		ROLLBACK TRAN	
	end


set NOCOUNT off
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RTPPrelim]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[RTPPrelim]
(
[RTPgId] [int] NOT NULL,
[TimeStart] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TimeEnd] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Price] [float] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RTPPrelim] on [rateengine].[RTPPrelim]'
GO
ALTER TABLE [rateengine].[RTPPrelim] ADD CONSTRAINT [PK_RTPPrelim] PRIMARY KEY CLUSTERED  ([RTPgId], [TimeStart], [TimeEnd])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RTPFinal]'
GO
CREATE TABLE [rateengine].[RTPFinal]
(
[RTPgId] [int] NOT NULL,
[TimeStart] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TimeEnd] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Price] [float] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RTPFinal] on [rateengine].[RTPFinal]'
GO
ALTER TABLE [rateengine].[RTPFinal] ADD CONSTRAINT [PK_RTPFinal] PRIMARY KEY CLUSTERED  ([RTPgId], [TimeStart], [TimeEnd])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RTPDayAhead]'
GO
CREATE TABLE [rateengine].[RTPDayAhead]
(
[RTPgId] [int] NOT NULL,
[TimeStart] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TimeEnd] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Price] [float] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RTPDayAhead] on [rateengine].[RTPDayAhead]'
GO
ALTER TABLE [rateengine].[RTPDayAhead] ADD CONSTRAINT [PK_RTPDayAhead] PRIMARY KEY CLUSTERED  ([RTPgId], [TimeStart], [TimeEnd])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RTPCriticalPeak]'
GO
CREATE TABLE [rateengine].[RTPCriticalPeak]
(
[RTPgId] [int] NOT NULL,
[TimeStart] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TimeEnd] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Price] [float] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RTPCriticalPeak] on [rateengine].[RTPCriticalPeak]'
GO
ALTER TABLE [rateengine].[RTPCriticalPeak] ADD CONSTRAINT [PK_RTPCriticalPeak] PRIMARY KEY CLUSTERED  ([RTPgId], [TimeStart], [TimeEnd])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetRealTimePricingNoRateClass]'
GO




create procedure [rateengine].[GetRealTimePricingNoRateClass]
@RateCompanyId as int = 100,
@StartDate as datetime,
@EndDate as datetime

AS

Set nocount on

Declare @RTPStreamId As int
Declare @RateDefId As int
DecLare @RTPgId As int

SELECT Top 1 @RateDefId = RateDefId, @RTPStreamId = RdRTPStreamId
FROM RateDefinition
	INNER JOIN RateMaster ON RateDefinition.RdRateMasterID = RateMaster.RateMasterID
	WHERE RateMaster.RmRateCompanyID = @RateCompanyID
		AND RateMaster.RmEnableInd = 1
		AND RateDefinition.RdEnableInd = 1
		AND RateDefinition.RdRTPInd = 1
	   	AND ( ( RateDefinition.RdStartDate >= @StartDate AND RateDefinition.RdStartDate <= @EndDate ) OR ( RateDefinition.RdStartDate < @StartDate ) )
ORDER BY RateDefinition.RdStartDate DESC

SELECT @RTPgId = RTPgId
FROM UseCharge
WHERE UseCharge.RateDefId = @RateDefId
	AND RTPgId IS NOT NULL

-- Create empty temp price table
Select Convert(DateTime, TimeStart) as TimeStart, Price, 0 as ActualStream into #TempPrice  from RTPDayAhead where  1=2

Declare @TempMaxDate as DateTime

If @RTPgId IS NOT NULL

BEGIN

-- REAL TIME ONLY
	If @RTPStreamId = 1

	BEGIN
		INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 3 From RTPFinal
		WHERE Convert(DateTime, TimeStart) >= @StartDate AND
		Convert(DateTime, TimeStart) <= @EndDate AND
		RTPgId = @RTPgId
		
		SELECT @TempMaxDate = Max(TimeStart) From #TempPrice

		If @TempMaxDate is Null
		BEGIN
			Select @TempMaxDate = DateAdd(hh, -1, @Startdate)
		END

		If @TempMaxDate < @EndDate
		BEGIN
			INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 2 From RTPPrelim
			WHERE Convert(DateTime, TimeStart) > @TempMaxDate AND
			Convert(DateTime, TimeStart) <= @EndDate AND
			RTPgId = @RTPgId
			
		END


	END


-- DAY AHEAD ONLY
	If @RTPStreamId = 2

	BEGIN
		INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 1 From RTPDayAhead
		WHERE Convert(DateTime, TimeStart) >= @StartDate AND
		Convert(DateTime, TimeStart) <= @EndDate AND
		RTPgId = @RTPgId
		
	END


-- REAL TIME AND DAY AHEAD
	If @RTPStreamId = 3

	BEGIN
		INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 3 From RTPFinal
		WHERE Convert(DateTime, TimeStart) >= @StartDate AND
		Convert(DateTime, TimeStart) <= @EndDate AND
		RTPgId = @RTPgId
		

		SELECT @TempMaxDate = Max(TimeStart) From #TempPrice
		
		If @TempMaxDate is Null
		BEGIN
			Select @TempMaxDate = DateAdd(hh, -1, @Startdate)
		END

		If @TempMaxDate is Null OR @TempMaxDate < @EndDate
		BEGIN
			INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 2 From RTPPrelim
			WHERE Convert(DateTime, TimeStart) > @TempMaxDate AND
			Convert(DateTime, TimeStart) <= @EndDate AND
			RTPgId = @RTPgId
			
			SELECT @TempMaxDate = Max(TimeStart) From #TempPrice

			If @TempMaxDate is Null
			BEGIN
				Select @TempMaxDate = DateAdd(hh, -1, @Startdate)
			END

			If @TempMaxDate is Null OR @TempMaxDate < @EndDate
			BEGIN
				INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 1 From RTPDayAhead
				WHERE Convert(DateTime, TimeStart) > @TempMaxDate AND
				Convert(DateTime, TimeStart) <= @EndDate AND
				RTPgId = @RTPgId
				

			END
		END	

	END
						

-- CRITICAL PEAK ONLY or PEAK TIME REBATE ONLY (5), treat this as if it were CPP
	If @RTPStreamId = 4 OR @RTPStreamId = 5

	BEGIN
		INSERT #TempPrice 
		SELECT CONVERT(DATETIME, TimeStart), 
			   Price, 
			   4 
		FROM RTPCriticalPeak
		WHERE CONVERT(DATETIME, TimeStart) >= @StartDate 
						AND CONVERT(DATETIME, TimeStart) <= @EndDate 
						AND RTPgId = @RTPgId
		
	END

END

SELECT * FROM #TempPrice
ORDER BY TimeStart

Drop Table #TempPrice

Set nocount off




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetRealTimePricingRTPgID]'
GO



create procedure [rateengine].[GetRealTimePricingRTPgID]
@RateCompanyId as int = 100,
@RateClass as varchar(16),
@StartDate as datetime,
@EndDate as datetime

AS

Set nocount on

Declare @RTPStreamId As int
Declare @RateDefId As int
DecLare @RTPgId As int

SELECT Top 1 @RateDefId = RateDefId, @RTPStreamId = RdRTPStreamId
FROM RateDefinition
	INNER JOIN RateMaster ON RateDefinition.RdRateMasterID = RateMaster.RateMasterID
	WHERE RateMaster.RmRateCompanyID = @RateCompanyID
		AND RateMaster.RmClientRateID = @RateClass
		AND RateMaster.RmEnableInd = 1
		AND RateDefinition.RdEnableInd = 1
		AND RateDefinition.RdRTPInd = 1
	   	AND ( ( RateDefinition.RdStartDate >= @StartDate AND RateDefinition.RdStartDate <= @EndDate ) OR ( RateDefinition.RdStartDate < @StartDate ) )
ORDER BY RateDefinition.RdStartDate DESC

SELECT @RTPgId = RTPgId
FROM UseCharge
WHERE UseCharge.RateDefId = @RateDefId
	AND RTPgId IS NOT NULL

select @RTPgId as RTPGid

Set nocount off



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetFuelTypebyRateClassRateCompanyID]'
GO


create procedure [rateengine].[GetFuelTypebyRateClassRateCompanyID] ( @RateCompanyID int, @RateClass varchar(16)) AS


	SELECT RmFuID  
	FROM RateMaster 
	WHERE RmRateCompanyID = @RateCompanyID AND RmClientRateID = @RateClass 



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[Log]'
GO
CREATE TABLE [rateengine].[Log]
(
[LoID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[LoSessionID] [uniqueidentifier] NOT NULL,
[LoText] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LoDate] [datetime] NOT NULL,
[LoBinary] [varbinary] (max) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Log] on [rateengine].[Log]'
GO
ALTER TABLE [rateengine].[Log] ADD CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED  ([LoID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[InsertLogOfReadings]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

create procedure [rateengine].[InsertLogOfReadings] ( @LoSessionID uniqueidentifier, @LoText varchar(256), @LoDate datetime,  @LoBinary varbinary(max) ) AS

	INSERT INTO [Log] ( LoSessionID, LoText, LoDate, LoBinary )
		VALUES ( @LoSessionID, @LoText, @LoDate, @LoBinary ) 

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[FuelCostRecoveryGroup]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[FuelCostRecoveryGroup]
(
[RgID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[RgName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RgRateCompanyID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_FuelCostRecoveryGroup] on [rateengine].[FuelCostRecoveryGroup]'
GO
ALTER TABLE [rateengine].[FuelCostRecoveryGroup] ADD CONSTRAINT [PK_FuelCostRecoveryGroup] PRIMARY KEY CLUSTERED  ([RgID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[InsertFuelCostAdjustmentByRateCompanyID]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

create procedure [rateengine].[InsertFuelCostAdjustmentByRateCompanyID] ( @rateCompanyID int, @RateGroupName varchar(1000)) AS

--insert
insert into [rateengine].[FuelCostRecoveryGroup]([RgName],[RgRateCompanyID])
values(@RateGroupName, @rateCompanyID)





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetLoggedReadings]'
GO


create procedure [rateengine].[GetLoggedReadings] ( @LoSessionID uniqueidentifier ) AS

	SELECT [Log].LoID, [Log].LoText, [Log].LoBinary FROM 
		[Log] WHERE  LoSessionID = @LoSessionID AND LoText = 'binary data'


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[UnitOfMeasure]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[UnitOfMeasure]
(
[UnID] [tinyint] NOT NULL,
[UnFuID] [tinyint] NOT NULL,
[UnName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnShortName] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_UnitOfMeasure] on [rateengine].[UnitOfMeasure]'
GO
ALTER TABLE [rateengine].[UnitOfMeasure] ADD CONSTRAINT [PK_UnitOfMeasure] PRIMARY KEY CLUSTERED  ([UnID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetUnitOfMeasureByID]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

create procedure [rateengine].[GetUnitOfMeasureByID] ( @uomID integer ) AS

	SELECT [UnName] FROM UnitOfMeasure WHERE UnID=@uomID 

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetRateClassInfo]'
GO

create procedure [rateengine].GetRateClassInfo ( @RateCompanyID int, @RateClass varchar(16), @Language int ) AS

	SELECT RateMasterText.[Name], RateMasterText.[Description], RateMasterText.Eligibility 
	FROM RateMaster INNER JOIN
		RateMasterText ON RateMaster.RateMasterID = RateMasterText.RateMasterID
	WHERE RateMaster.RmRateCompanyID = @RateCompanyID AND RateMaster.RmClientRateID = @RateClass 
		AND RateMasterText.LaID = @Language

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[TerritoryLookupGroup]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[TerritoryLookupGroup]
(
[LgID] [tinyint] NOT NULL,
[LgName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_LookupGroup] on [rateengine].[TerritoryLookupGroup]'
GO
ALTER TABLE [rateengine].[TerritoryLookupGroup] ADD CONSTRAINT [PK_LookupGroup] PRIMARY KEY CLUSTERED  ([LgID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[TerritoryLookup]'
GO
CREATE TABLE [rateengine].[TerritoryLookup]
(
[TlLgID] [tinyint] NOT NULL,
[TlCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TlSeasonID] [tinyint] NOT NULL,
[TlTerritory] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TlValue] [float] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TerritoryLookup] on [rateengine].[TerritoryLookup]'
GO
ALTER TABLE [rateengine].[TerritoryLookup] ADD CONSTRAINT [PK_TerritoryLookup] PRIMARY KEY CLUSTERED  ([TlLgID], [TlCode], [TlSeasonID], [TlTerritory])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectBaselineTerritory]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

create procedure [rateengine].SelectBaselineTerritory ( @RateDefID int, @Code varchar(2), @Territory varchar(2), @SeasonID int ) AS

	SELECT TerritoryLookup.TlValue
	FROM TerritoryLookup INNER JOIN
    	TerritoryLookupGroup ON TerritoryLookup.TlLgID = TerritoryLookupGroup.LgID INNER JOIN
    	TerritoryBaseRateClass ON TerritoryLookupGroup.LgID = TerritoryBaseRateClass.LgID
	WHERE (TerritoryBaseRateClass.RateDefID = @RateDefID) AND 
		(TerritoryLookup.TlSeasonID = @SeasonID) AND 
		(TerritoryLookup.TlTerritory = @Territory) AND 
    	(TerritoryLookup.TlCode = @Code)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthInsertFuelCostAdjustmentByRateCompanyID]'
GO


create procedure [rateengine].[AuthInsertFuelCostAdjustmentByRateCompanyID] ( @rateCompanyID int, @RateGroupName varchar(1000)) AS

--insert
insert into [rateengine].[FuelCostRecoveryGroup]([RgName],[RgRateCompanyID])
values(@RateGroupName, @rateCompanyID)



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[InsertLog]'
GO

create procedure [rateengine].InsertLog ( @LoSessionID uniqueidentifier, @LoText varchar(256), @LoDate datetime ) AS

	INSERT INTO [Log] ( LoSessionID, LoText, LoDate )
		VALUES ( @LoSessionID, @LoText, @LoDate ) 

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthRefreshTerritoryBaseRateClass]'
GO

create procedure [rateengine].AuthRefreshTerritoryBaseRateClass ( @RateDefID int, @LgID int ) AS
		
set NOCOUNT on 
	
	BEGIN TRAN

	DELETE FROM TerritoryBaseRateClass WHERE RateDefID = @RateDefID

	INSERT INTO TerritoryBaseRateClass (RateDefID, LgID ) Values ( @RateDefID, @LgID )

	if( @@ERROR = 0 ) begin
		COMMIT TRAN
	end
	else begin 
		ROLLBACK TRAN	
	end

set NOCOUNT off

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[HolidayGroup]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[HolidayGroup]
(
[HgID] [int] NOT NULL IDENTITY(1, 1),
[HgName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HgRateCompanyID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_HolidayGroup] on [rateengine].[HolidayGroup]'
GO
ALTER TABLE [rateengine].[HolidayGroup] ADD CONSTRAINT [PK_HolidayGroup] PRIMARY KEY CLUSTERED  ([HgID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthUpdHolidayGroup]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

create procedure [rateengine].[AuthUpdHolidayGroup] @HgId int, @HgName varchar(32) AS

	UPDATE HolidayGroup SET HgName=@HgName
	WHERE HgID = @HgID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthDelHolidayGroup]'
GO

create procedure [rateengine].[AuthDelHolidayGroup] @HgID int AS

	DELETE HolidayGroup
	WHERE HgID = @HgID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthInsHolidayGroup]'
GO

create procedure [rateengine].[AuthInsHolidayGroup] @HgName varchar(32), @HgRateCompanyID int AS

	INSERT INTO HolidayGroup (HgName, HgRateCompanyID)
	VALUES (@HgName, @HgRateCompanyID )

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[Holiday]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[Holiday]
(
[HoID] [int] NOT NULL IDENTITY(1, 1),
[HoHgID] [int] NOT NULL,
[HoName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HoDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Holiday] on [rateengine].[Holiday]'
GO
ALTER TABLE [rateengine].[Holiday] ADD CONSTRAINT [PK_Holiday] PRIMARY KEY CLUSTERED  ([HoID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthUpdHoliday]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

create procedure [rateengine].[AuthUpdHoliday] @HoID int, @HoDate datetime, @HoName varchar(32) AS

	UPDATE Holiday
	SET HoDate=@HoDate, HoName=@HoName
	WHERE HoId = @HoID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthDelHoliday]'
GO

create procedure [rateengine].[AuthDelHoliday] @HoID int AS

	DELETE HOLIDAY
	WHERE HoID = @HoID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthInsHoliday]'
GO

create procedure [rateengine].[AuthInsHoliday] @HoHgID int, @HoDate datetime, @HoName varchar(32) AS

	INSERT Holiday (HoHgID, HoDate, HoName)
	VALUES (@HoHgID, @HoDate, @HoName)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectHolidayList]'
GO

create procedure [rateengine].[SelectHolidayList] ( @HolidayGroupID Int ) AS

	SELECT HoID, HoHgID, HoName, HoDate
	FROM Holiday WHERE HoHgID = @HolidayGroupID
	ORDER BY HoDate

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[TimeOfUse]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[TimeOfUse]
(
[TouID] [tinyint] NOT NULL,
[TuName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TuSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TimeOfUse] on [rateengine].[TimeOfUse]'
GO
ALTER TABLE [rateengine].[TimeOfUse] ADD CONSTRAINT [PK_TimeOfUse] PRIMARY KEY CLUSTERED  ([TouID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[Tier]'
GO
CREATE TABLE [rateengine].[Tier]
(
[TierID] [tinyint] NOT NULL,
[TiName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TiSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Teir] on [rateengine].[Tier]'
GO
ALTER TABLE [rateengine].[Tier] ADD CONSTRAINT [PK_Teir] PRIMARY KEY CLUSTERED  ([TierID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[Season]'
GO
CREATE TABLE [rateengine].[Season]
(
[SeasonID] [tinyint] NOT NULL,
[SeName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SeSortOrder] [tinyint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Season] on [rateengine].[Season]'
GO
ALTER TABLE [rateengine].[Season] ADD CONSTRAINT [PK_Season] PRIMARY KEY CLUSTERED  ([SeasonID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RTPGroup]'
GO
CREATE TABLE [rateengine].[RTPGroup]
(
[RTPgId] [int] NOT NULL IDENTITY(1, 1),
[RTPGroupName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RTPgRateCompanyId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RealTimePriceGroup] on [rateengine].[RTPGroup]'
GO
ALTER TABLE [rateengine].[RTPGroup] ADD CONSTRAINT [PK_RealTimePriceGroup] PRIMARY KEY CLUSTERED  ([RTPgId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[PartType]'
GO
CREATE TABLE [rateengine].[PartType]
(
[PartTypeID] [tinyint] NOT NULL,
[PtName] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PtSortOrder] [tinyint] NOT NULL CONSTRAINT [DF_PartType_PtSortOrder] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PartType] on [rateengine].[PartType]'
GO
ALTER TABLE [rateengine].[PartType] ADD CONSTRAINT [PK_PartType] PRIMARY KEY CLUSTERED  ([PartTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[CostType]'
GO
CREATE TABLE [rateengine].[CostType]
(
[CostTypeID] [int] NOT NULL,
[CtName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CtDescription] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CtSortOrder] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CostType] on [rateengine].[CostType]'
GO
ALTER TABLE [rateengine].[CostType] ADD CONSTRAINT [PK_CostType] PRIMARY KEY CLUSTERED  ([CostTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthGetUseCharges]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


create procedure [rateengine].AuthGetUseCharges ( @RateDefID int ) AS

	-- Using a UNION query to support requirements of exact sort and to provide
	-- the category parameter for use by calling Datagrid

	-- Get FCR Use Charges
	SELECT UseCharge.RateDefID As RdID, UseCharge.TierID AS TiID, UseCharge.TouID AS TuID, UseCharge.SeasonID AS SeID,
	UseCharge.CostTypeID AS CtID, UseCharge.PartTypeID AS PtID, UseCharge.ChargeValue, UseCharge.RgID, UseCharge.RTPgId, [Description], 
	TiName, TuName, SeName, CtName, PtName, RgName, RTPGroupName, 1 As Category
	FROM UseCharge INNER JOIN 
	Tier ON UseCharge.TierID = Tier.TierID INNER JOIN 
	TimeOfUse ON UseCharge.TouID = TimeOfUse.TouID INNER JOIN 
	Season ON UseCharge.SeasonID = Season.SeasonID INNER JOIN  
	CostType ON UseCharge.CostTypeID = CostType.CostTypeID INNER JOIN 
	PartType ON UseCharge.PartTypeID = PartType.PartTypeID LEFT OUTER JOIN 
	FuelCostRecoveryGroup ON UseCharge.RgID = FuelCostRecoveryGroup.RgID LEFT OUTER JOIN
	RTPGroup ON UseCharge.RTPgId = RTPGroup.RTPgId
	WHERE RateDefID = @RateDefID AND UseCharge.RgID IS NOT NULL And UseCharge.RTPgId IS NULL

	UNION

	-- Get Demand Use Charges
	SELECT UseCharge.RateDefID As RdID, UseCharge.TierID AS TiID, UseCharge.TouID AS TuID, UseCharge.SeasonID AS SeID,
	UseCharge.CostTypeID AS CtID, UseCharge.PartTypeID AS PtID, UseCharge.ChargeValue, UseCharge.RgID, UseCharge.RTPgId, [Description], 
	TiName, TuName, SeName, CtName, PtName, RgName, RTPGroupName, 2 As Category
	FROM UseCharge INNER JOIN 
	Tier ON UseCharge.TierID = Tier.TierID INNER JOIN 
	TimeOfUse ON UseCharge.TouID = TimeOfUse.TouID INNER JOIN 
	Season ON UseCharge.SeasonID = Season.SeasonID INNER JOIN  
	CostType ON UseCharge.CostTypeID = CostType.CostTypeID INNER JOIN 
	PartType ON UseCharge.PartTypeID = PartType.PartTypeID LEFT OUTER JOIN 
	FuelCostRecoveryGroup ON UseCharge.RgID = FuelCostRecoveryGroup.RgID LEFT OUTER JOIN
	RTPGroup ON UseCharge.RTPgId = RTPGroup.RTPgId
	WHERE RateDefID = @RateDefID AND UseCharge.RgID IS NULL  And UseCharge.RTPgId IS NULL AND Tier.TierID IN ( 11,12 )

	UNION

	-- Get rest of regular UseCharges
	SELECT UseCharge.RateDefID As RdID, UseCharge.TierID AS TiID, UseCharge.TouID AS TuID, UseCharge.SeasonID AS SeID,
	UseCharge.CostTypeID AS CtID, UseCharge.PartTypeID AS PtID, UseCharge.ChargeValue, UseCharge.RgID, UseCharge.RTPgId, [Description], 
	TiName, TuName, SeName, CtName, PtName, RgName, RTPGroupName, 3 As Category
	FROM UseCharge INNER JOIN 
	Tier ON UseCharge.TierID = Tier.TierID INNER JOIN 
	TimeOfUse ON UseCharge.TouID = TimeOfUse.TouID INNER JOIN 
	Season ON UseCharge.SeasonID = Season.SeasonID INNER JOIN  
	CostType ON UseCharge.CostTypeID = CostType.CostTypeID INNER JOIN 
	PartType ON UseCharge.PartTypeID = PartType.PartTypeID LEFT OUTER JOIN 
	FuelCostRecoveryGroup ON UseCharge.RgID = FuelCostRecoveryGroup.RgID LEFT OUTER JOIN
	RTPGroup ON UseCharge.RTPgId = RTPGroup.RTPgId
	WHERE RateDefID = @RateDefID AND UseCharge.RgID IS NULL  And UseCharge.RTPgId IS NULL AND Tier.TierID IN ( 0,1,2,3,4,5,6,7,8,9,10 )

	UNION

	-- Get RTP Use Charges
	SELECT UseCharge.RateDefID As RdID, UseCharge.TierID AS TiID, UseCharge.TouID AS TuID, UseCharge.SeasonID AS SeID,
	UseCharge.CostTypeID AS CtID, UseCharge.PartTypeID AS PtID, UseCharge.ChargeValue, UseCharge.RgID, UseCharge.RTPgId, [Description], 
	TiName, TuName, SeName, CtName, PtName, RgName, RTPGroupName, 4 As Category
	FROM UseCharge INNER JOIN 
	Tier ON UseCharge.TierID = Tier.TierID INNER JOIN 
	TimeOfUse ON UseCharge.TouID = TimeOfUse.TouID INNER JOIN 
	Season ON UseCharge.SeasonID = Season.SeasonID INNER JOIN  
	CostType ON UseCharge.CostTypeID = CostType.CostTypeID INNER JOIN 
	PartType ON UseCharge.PartTypeID = PartType.PartTypeID LEFT OUTER JOIN 
	FuelCostRecoveryGroup ON UseCharge.RgID = FuelCostRecoveryGroup.RgID LEFT OUTER JOIN
	RTPGroup ON UseCharge.RTPgId = RTPGroup.RTPgId
	WHERE RateDefID = @RateDefID AND UseCharge.RgID IS NULL And UseCharge.RTPgId IS NOT NULL

	ORDER BY Category, UseCharge.SeasonID, UseCharge.TouID, UseCharge.TierID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectHolidayListB]'
GO

create procedure [rateengine].[SelectHolidayListB] ( @HolidayGroupID Int, @StartDate datetime, @EndDate  datetime ) AS

	SELECT HoID, HoHgID, HoName, HoDate
	FROM Holiday WHERE HoHgID = @HolidayGroupID AND 
		( HoDate >= CONVERT(DATETIME, @StartDate, 102) AND HoDate <= CONVERT(DATETIME, @EndDate, 102) )
	ORDER BY HoDate

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthSelRTPGroup]'
GO

create procedure [rateengine].[AuthSelRTPGroup]  @RateCompanyId int AS

	SELECT RTPgId, RTPGroupName
	FROM RTPGroup
	WHERE RTPgRateCompanyId = @RateCompanyId
	ORDER BY RTPGroupName
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[updRTPPrelim]'
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [rateengine].[updRTPPrelim] 

@RateCompanyId int,
@RTPGroupName varchar(50),
@TimeStart varchar(20),
@TimeEnd varchar(20),
@Price float

AS

Set NOCOUNT ON

Declare @RTPgID int

Select @RTPgId = RTPgId From RTPGroup  Where RTPgRateCompanyID = @RateCompanyID and
RTPGroupName = @RTPGroupName


If @RTPgId = null
BEGIN
	Insert RTPGroup values (@RTPGroupName, @RateCompanyId)
	Select @RTPgId = RTPgId From RTPGroup  Where RTPgRateCompanyID = @RateCompanyID and
RTPGroupName = @RTPGroupName
END


If (
Select count(*) From RTPPrelim 
Where 
RTPgId = @RTPgId and
TimeStart >= @TimeStart and
TimeEnd <= @TimeEnd
) > 0

BEGIN

Delete RTPPrelim
where 
RTPgId = @RTPgId and
TimeStart >= @TimeStart and
TimeEnd <= @TimeEnd

Insert RTPPrelim values (@RTPgId, @TimeStart, @TimeEnd, @Price)

END

ELSE

BEGIN

Insert RTPPrelim values (@RTPgId, @TimeStart, @TimeEnd, @Price)

END

Set NOCOUNT OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[updRTPFinal]'
GO

create procedure [rateengine].[updRTPFinal] 

@RateCompanyId int,
@RTPGroupName varchar(50),
@TimeStart varchar(20),
@TimeEnd varchar(20),
@Price float

AS

Set NOCOUNT ON

Declare @RTPgID int

Select @RTPgId = RTPgId From RTPGroup  Where RTPgRateCompanyID = @RateCompanyID and
RTPGroupName = @RTPGroupName


If @RTPgId = null
BEGIN
	Insert RTPGroup values (@RTPGroupName, @RateCompanyId)
	Select @RTPgId = RTPgId From RTPGroup  Where RTPgRateCompanyID = @RateCompanyID and
RTPGroupName = @RTPGroupName
END


If (
Select count(*) From RTPFinal 
Where 
RTPgId = @RTPgId and
TimeStart >= @TimeStart and
TimeEnd <= @TimeEnd
) > 0

BEGIN

Delete RTPFinal
where 
RTPgId = @RTPgId and
TimeStart >= @TimeStart and
TimeEnd <= @TimeEnd

Insert RTPFinal values (@RTPgId, @TimeStart, @TimeEnd, @Price)

END

ELSE

BEGIN

Insert RTPFinal values (@RTPgId, @TimeStart, @TimeEnd, @Price)

END

Set NOCOUNT OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[updRTPDayAhead]'
GO

create procedure [rateengine].[updRTPDayAhead] 

@RateCompanyId int,
@RTPGroupName varchar(50),
@TimeStart varchar(20),
@TimeEnd varchar(20),
@Price float

AS

Set NOCOUNT ON

Declare @RTPgID int

Select @RTPgId = RTPgId From RTPGroup  Where RTPgRateCompanyID = @RateCompanyID and
RTPGroupName = @RTPGroupName


If @RTPgId = null
BEGIN
	Insert RTPGroup values (@RTPGroupName, @RateCompanyId)
	Select @RTPgId = RTPgId From RTPGroup  Where RTPgRateCompanyID = @RateCompanyID and
RTPGroupName = @RTPGroupName
END


If (
Select count(*) From RTPDayAhead 
Where 
RTPgId = @RTPgId and
TimeStart >= @TimeStart and
TimeEnd <= @TimeEnd
) > 0

BEGIN

Delete RTPDayAhead
where 
RTPgId = @RTPgId and
TimeStart >= @TimeStart and
TimeEnd <= @TimeEnd

Insert RTPDayAhead values (@RTPgId, @TimeStart, @TimeEnd, @Price)

END

ELSE

BEGIN

Insert RTPDayAhead values (@RTPgId, @TimeStart, @TimeEnd, @Price)

END

Set NOCOUNT OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectUseCharges]'
GO
SET QUOTED_IDENTIFIER OFF
GO


create procedure [rateengine].SelectUseCharges ( @RateDefinitionID Int ) AS

	SELECT TierID, TouID, SeasonID, CostTypeID, PartTypeID, ChargeValue, RgID, RTPgId, [Description]
		FROM UseCharge
	WHERE RateDefID = @RateDefinitionID
	ORDER BY SeasonID, TouID, TierID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetRealTimePricing]'
GO
SET QUOTED_IDENTIFIER ON
GO




create procedure [rateengine].[GetRealTimePricing]
@RateCompanyId as int = 100,
@RateClass as varchar(16),
@StartDate as datetime,
@EndDate as datetime

AS

Set nocount on

Declare @RTPStreamId As int
Declare @RateDefId As int
DecLare @RTPgId As int

SELECT Top 1 @RateDefId = RateDefId, @RTPStreamId = RdRTPStreamId
FROM RateDefinition
	INNER JOIN RateMaster ON RateDefinition.RdRateMasterID = RateMaster.RateMasterID
	WHERE RateMaster.RmRateCompanyID = @RateCompanyID
		AND RateMaster.RmClientRateID = @RateClass
		AND RateMaster.RmEnableInd = 1
		AND RateDefinition.RdEnableInd = 1
		AND RateDefinition.RdRTPInd = 1
	   	AND ( ( RateDefinition.RdStartDate >= @StartDate AND RateDefinition.RdStartDate <= @EndDate ) OR ( RateDefinition.RdStartDate < @StartDate ) )
ORDER BY RateDefinition.RdStartDate DESC

SELECT @RTPgId = RTPgId
FROM UseCharge
WHERE UseCharge.RateDefId = @RateDefId
	AND RTPgId IS NOT NULL

-- Create empty temp price table
Select Convert(DateTime, TimeStart) as TimeStart, Price, 0 as ActualStream into #TempPrice  from RTPDayAhead where  1=2

Declare @TempMaxDate as DateTime

If @RTPgId IS NOT NULL

BEGIN

-- REAL TIME ONLY
	If @RTPStreamId = 1

	BEGIN
		INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 3 From RTPFinal
		WHERE Convert(DateTime, TimeStart) >= @StartDate AND
		Convert(DateTime, TimeStart) <= @EndDate AND
		RTPgId = @RTPgId
		
		SELECT @TempMaxDate = Max(TimeStart) From #TempPrice

		If @TempMaxDate is Null
		BEGIN
			Select @TempMaxDate = DateAdd(hh, -1, @Startdate)
		END

		If @TempMaxDate < @EndDate
		BEGIN
			INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 2 From RTPPrelim
			WHERE Convert(DateTime, TimeStart) > @TempMaxDate AND
			Convert(DateTime, TimeStart) <= @EndDate AND
			RTPgId = @RTPgId
			
		END


	END


-- DAY AHEAD ONLY
	If @RTPStreamId = 2

	BEGIN
		INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 1 From RTPDayAhead
		WHERE Convert(DateTime, TimeStart) >= @StartDate AND
		Convert(DateTime, TimeStart) <= @EndDate AND
		RTPgId = @RTPgId
		
	END


-- REAL TIME AND DAY AHEAD
	If @RTPStreamId = 3

	BEGIN
		INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 3 From RTPFinal
		WHERE Convert(DateTime, TimeStart) >= @StartDate AND
		Convert(DateTime, TimeStart) <= @EndDate AND
		RTPgId = @RTPgId
		

		SELECT @TempMaxDate = Max(TimeStart) From #TempPrice
		
		If @TempMaxDate is Null
		BEGIN
			Select @TempMaxDate = DateAdd(hh, -1, @Startdate)
		END

		If @TempMaxDate is Null OR @TempMaxDate < @EndDate
		BEGIN
			INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 2 From RTPPrelim
			WHERE Convert(DateTime, TimeStart) > @TempMaxDate AND
			Convert(DateTime, TimeStart) <= @EndDate AND
			RTPgId = @RTPgId
			
			SELECT @TempMaxDate = Max(TimeStart) From #TempPrice

			If @TempMaxDate is Null
			BEGIN
				Select @TempMaxDate = DateAdd(hh, -1, @Startdate)
			END

			If @TempMaxDate is Null OR @TempMaxDate < @EndDate
			BEGIN
				INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 1 From RTPDayAhead
				WHERE Convert(DateTime, TimeStart) > @TempMaxDate AND
				Convert(DateTime, TimeStart) <= @EndDate AND
				RTPgId = @RTPgId
				

			END
		END	

	END
		

-- CRITICAL PEAK ONLY (4) or PEAK TIME REBATE ONLY (5), treat this as if it were CPP
	If @RTPStreamId = 4 OR @RTPStreamId = 5

	BEGIN
		INSERT #TempPrice 
		SELECT CONVERT(DATETIME, TimeStart), 
			   Price, 
			   4 
		FROM RTPCriticalPeak
		WHERE CONVERT(DATETIME, TimeStart) >= @StartDate 
						AND CONVERT(DATETIME, TimeStart) <= @EndDate 
						AND RTPgId = @RTPgId
		
	END

END

SELECT * FROM #TempPrice
ORDER BY TimeStart

Drop Table #TempPrice

Set nocount off




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectStepBoundaries]'
GO
SET QUOTED_IDENTIFIER OFF
GO

create procedure [rateengine].SelectStepBoundaries ( @RateDefinitionID Int ) AS

	SELECT StepID, SeasonID, BoundaryValue
		FROM StepBoundary
	WHERE RateDefID = @RateDefinitionID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[FuelCostRecovery]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[FuelCostRecovery]
(
[FrID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[FrRgID] [int] NOT NULL,
[FrStartDate] [datetime] NOT NULL,
[FrChargeValue] [float] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_FuelCostRecovery] on [rateengine].[FuelCostRecovery]'
GO
ALTER TABLE [rateengine].[FuelCostRecovery] ADD CONSTRAINT [PK_FuelCostRecovery] PRIMARY KEY CLUSTERED  ([FrID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_FuelCostRecovery_a] on [rateengine].[FuelCostRecovery]'
GO
CREATE NONCLUSTERED INDEX [IX_FuelCostRecovery_a] ON [rateengine].[FuelCostRecovery] ([FrRgID], [FrStartDate], [FrID]) WITH (STATISTICS_NORECOMPUTE=ON)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_FuelCostRecovery_b] on [rateengine].[FuelCostRecovery]'
GO
CREATE NONCLUSTERED INDEX [IX_FuelCostRecovery_b] ON [rateengine].[FuelCostRecovery] ([FrStartDate] DESC, [FrRgID], [FrChargeValue], [FrID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetFuelCostRecoveryList]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

create procedure [rateengine].GetFuelCostRecoveryList ( @RgID int, @StartDate datetime, @EndDate datetime ) AS


	SELECT DISTINCT FuelCostRecovery.FrStartDate, FuelCostRecovery.FrChargeValue
	FROM FuelCostRecovery INNER JOIN
	   FuelCostRecoveryGroup ON FuelCostRecovery.FrRgID = FuelCostRecoveryGroup.RgID
	WHERE (FuelCostRecoveryGroup.RgID = @RgID) AND (FuelCostRecovery.FrStartDate >= CONVERT(DATETIME, @StartDate, 102)) AND 
	 (FuelCostRecovery.FrStartDate <= CONVERT(DATETIME, @EndDate, 102))

	UNION

	SELECT f.FrStartDate, f.FrChargeValue FROM (
	SELECT TOP 1 Max(FuelCostRecovery.FrID) as FrID, FrStartDate FROM FuelCostRecovery INNER JOIN
	   FuelCostRecoveryGroup ON FuelCostRecovery.FrRgID = FuelCostRecoveryGroup.RgID
	WHERE (FuelCostRecoveryGroup.RgID = @RgID) and 
	(FuelCostRecovery.FrStartDate <= CONVERT(DATETIME, @StartDate, 102))  GROUP BY FrStartDate ORDER BY FuelCostRecovery.FrStartDate DESC   ) x

	INNER JOIN FuelCostRecovery f on x.FrID = f.FrID
	ORDER BY FuelCostRecovery.FrStartDate DESC


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectServiceCharges]'
GO

create procedure [rateengine].[SelectServiceCharges] ( @RateDefinitionID Int ) AS

	SELECT CalcTypeID, CostTypeID, StepID, SeasonID, ChargeValue, RebateClassID
		FROM ServiceCharge
	WHERE RateDefID = @RateDefinitionID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectTOUBoundaries]'
GO

create procedure [rateengine].SelectTOUBoundaries ( @RateDefinitionID Int ) AS

	SELECT TouID, SeasonID, DayTypeID, StartTime, EndTime
		FROM TimeOfUseBoundary
	WHERE RateDefID = @RateDefinitionID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectTierBoundaries]'
GO

create procedure [rateengine].SelectTierBoundaries ( @RateDefinitionID Int ) AS

	SELECT TierID, TouID, SeasonID, BoundaryValue, SecondaryBoundaryValue
		FROM TierBoundary
	WHERE RateDefID = @RateDefinitionID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectTaxCharges]'
GO

create procedure [rateengine].SelectTaxCharges ( @RateDefinitionID Int ) AS

	SELECT CostTypeID, ChargeValue
		FROM TaxCharge
	WHERE RateDefID = @RateDefinitionID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectSeasonBoundaries]'
GO

create procedure [rateengine].SelectSeasonBoundaries ( @RateDefinitionID Int ) AS

	SELECT SeasonID, StartDay, EndDay
		FROM SeasonBoundary
	WHERE RateDefID = @RateDefinitionID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectMinimumCharges]'
GO

create procedure [rateengine].SelectMinimumCharges ( @RateDefinitionID Int ) AS

	SELECT CalcTypeID, CostTypeID, ChargeValue
		FROM MinimumCharge
	WHERE RateDefID = @RateDefinitionID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[DifferenceType]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[DifferenceType]
(
[DfID] [int] NOT NULL,
[DfDcID] [tinyint] NOT NULL,
[DfName] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DfSortOrder] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DifferenceType] on [rateengine].[DifferenceType]'
GO
ALTER TABLE [rateengine].[DifferenceType] ADD CONSTRAINT [PK_DifferenceType] PRIMARY KEY CLUSTERED  ([DfID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[DifferenceCategory]'
GO
CREATE TABLE [rateengine].[DifferenceCategory]
(
[DcID] [tinyint] NOT NULL,
[DcName] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DcSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DifferenceCategory] on [rateengine].[DifferenceCategory]'
GO
ALTER TABLE [rateengine].[DifferenceCategory] ADD CONSTRAINT [PK_DifferenceCategory] PRIMARY KEY CLUSTERED  ([DcID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectDifferences]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

create procedure [rateengine].SelectDifferences ( @RateDefinitionID Int ) AS

	SELECT DifferenceType.DfID, DifferenceType.DfName, DifferenceCategory.DcID, DifferenceCategory.DcName
	FROM Difference INNER JOIN
    	DifferenceType ON Difference.DfID = DifferenceType.DfID INNER JOIN
    	DifferenceCategory ON DifferenceType.DfDcID = DifferenceCategory.DcID
    WHERE Difference.RateDefID = @RateDefinitionID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectAllChargesAndBoundariesAndOther]'
GO

create procedure [rateengine].SelectAllChargesAndBoundariesAndOther ( @RateDefinitionID Int ) AS

	BEGIN 
		EXEC [rateengine].[SelectUseCharges] @RateDefinitionID 
		EXEC [rateengine].[SelectServiceCharges] @RateDefinitionID
		EXEC [rateengine].[SelectMinimumCharges] @RateDefinitionID
		EXEC [rateengine].[SelectTaxCharges] @RateDefinitionID
		EXEC [rateengine].[SelectTierBoundaries] @RateDefinitionID
		EXEC [rateengine].[SelectTOUBoundaries] @RateDefinitionID
		EXEC [rateengine].[SelectSeasonBoundaries] @RateDefinitionID
		EXEC [rateengine].[SelectStepBoundaries] @RateDefinitionID
		EXEC [rateengine].[SelectDifferences] @RateDefinitionID
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetLogHistory]'
GO

create procedure [rateengine].GetLogHistory ( @LoSessionID uniqueidentifier ) AS

	SELECT [Log].LoID, [Log].LoText FROM 
		[Log] WHERE LoSessionID = @LoSessionID ORDER BY LoID ASC

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[delRTPCriticalPeak]'
GO
create procedure [rateengine].[delRTPCriticalPeak] 
					@RateCompanyId INT,
					@RTPGroupName VARCHAR(50),
					@TimeStart VARCHAR(20),
					@TimeEnd VARCHAR(20),
					@Price FLOAT
AS

SET NOCOUNT ON

DECLARE @RTPgID INT

SELECT @RTPgId = RTPgId 
FROM RTPGroup  
WHERE RTPgRateCompanyID = @RateCompanyID 
		AND RTPGroupName = @RTPGroupName

IF @RTPgID <> null and (SELECT COUNT(*) 
	FROM RTPCriticalPeak 
	WHERE RTPgId = @RTPgId 
			AND TimeStart >= @TimeStart 
			AND TimeEnd <= @TimeEnd) > 0
BEGIN

	DELETE RTPCriticalPeak 
	WHERE RTPgId = @RTPgId 
			AND TimeStart >= @TimeStart 
			AND TimeEnd <= @TimeEnd

END

SET NOCOUNT OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RateMaintData]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[RateMaintData]
(
[SessionID] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RateMaintID] [int] NOT NULL IDENTITY(1, 1),
[UploadDateTime] [datetime] NOT NULL,
[RateCompanyID] [int] NULL,
[RateMasterID] [int] NULL,
[SourceTypeID] [tinyint] NULL,
[SourceID] [int] NULL,
[ClientRateID] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateDefID] [int] NULL,
[EffectiveDate] [datetime] NULL,
[ChargeType] [tinyint] NULL,
[Tier] [tinyint] NULL,
[TimeOfUse] [tinyint] NULL,
[SeasonID] [int] NULL,
[StepID] [int] NULL,
[PartTypeID] [tinyint] NULL,
[CostTypeID] [int] NULL,
[ChargeValue] [float] NULL,
[CalcTypeID] [tinyint] NULL,
[ErrorText] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImportStatus] [tinyint] NULL,
[ImportDateTime] [datetime] NULL,
[originalRateCompanyID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[originalSourceType] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[originalSourceID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[originalSourceName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[originalRateClass] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[originalEffectiveDate] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[originalTypeOfCharge] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[originalTier] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[originalTimeOfUse] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[originalSeason] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[originalPartType] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[originalCostType] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[originalChargeValue] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[originalCalcType] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[originalStep] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RebateClassID] [int] NULL,
[originalRebateClassID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RateMaintData] on [rateengine].[RateMaintData]'
GO
ALTER TABLE [rateengine].[RateMaintData] ADD CONSTRAINT [PK_RateMaintData] PRIMARY KEY CLUSTERED  ([RateMaintID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[DeleteRateMaintDataSession]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

create procedure [rateengine].[DeleteRateMaintDataSession] ( @SessionID varchar(36) ) AS

	DELETE 
		FROM [rateengine].[RateMaintData]
		WHERE SessionID = @SessionID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[ChargeCalcType]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[ChargeCalcType]
(
[CalcTypeID] [tinyint] NOT NULL,
[CaName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CaSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ChargeCalcType] on [rateengine].[ChargeCalcType]'
GO
ALTER TABLE [rateengine].[ChargeCalcType] ADD CONSTRAINT [PK_ChargeCalcType] PRIMARY KEY CLUSTERED  ([CalcTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetChargeCalcTypeList]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

create procedure [rateengine].[GetChargeCalcTypeList] AS

SELECT CalcTypeID,
       CaName,
       CaSortOrder
  FROM dbo.ChargeCalcType
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetCostTypeList]'
GO

create procedure [rateengine].[GetCostTypeList] AS

	SELECT	CostTypeID,
			CtName,
			CtDescription,
			CtSortOrder 
	FROM	CostType
    ORDER BY CtName

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetMinimumCharge]'
GO

create procedure [rateengine].[GetMinimumCharge] (@RateDefID AS int, 
											   @CalcTypeID AS tinyint,
											   @CostTypeID AS int) AS

	SELECT	MinimumCharge.RateDefID AS RdID, 
			MinimumCharge.CalcTypeID AS CaID, 
			MinimumCharge.CostTypeID AS CtID, 
			MinimumCharge.ChargeValue, 
			CaName, 
			CtName 

	FROM	MinimumCharge 
			INNER JOIN ChargeCalcType ON MinimumCharge.CalcTypeID = ChargeCalcType.CalcTypeID 
			INNER JOIN CostType ON MinimumCharge.CostTypeID = CostType.CostTypeID 

	WHERE	MinimumCharge.RateDefID = @RateDefID AND 
		    MinimumCharge.CalcTypeID = @CalcTypeID AND
		    MinimumCharge.CostTypeID = @CostTypeID AND
			ChargeCalcType.CalcTypeID IN (0,1,2,3,4,5)

	ORDER BY	MinimumCharge.RateDefID  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetNewestRateDefinition]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [rateengine].[GetNewestRateDefinition] ( 
									@RateMasterID AS int,
									@EndDate AS datetime) AS

	SELECT	RateDefID, 
			r.RdRateMasterID AS RdRmID, 
			r.RdStartDate, 
			r.RdSeasonalInd, 
			r.RdTieredInd, 
			r.RdTimeOfUseInd, 
			r.RdDemandInd,
			r.RdFuelAdjustmentInd, 
			r.RdBasicInd,
			r.RdTsID, 
			r.RdDsID, 
			r.RdDifferenceNote, 
			r.RdEnableInd,
			r.RdBrID,
			r.RdNmID,
			r.RdNetMeteringInd,
			r.RdTierAccumByTOUInd,			
			m.RmFuID, 
			m.RmSvID, 
			m.RmCtID, 
			m.RmUnID, 
			m.RmPrID,
			m.RmDPrID,
			m.RmClientRateID, 
			m.RmRateCompanyID, 
			m.RmDefault,
			m.RmChildrenInd, 
			m.RmLtID,
			m.RmBdID,
			m.RmHgID
	FROM 
			(SELECT	RateDefinition.RdRateMasterID,
					MAX(RateDefinition.RdStartDate) AS StartDate 
			 FROM RateDefinition 
			 WHERE (RateDefinition.RdRateMasterID = @RateMasterID) AND 
				  (RateDefinition.RdStartDate < CONVERT(DATETIME, @EndDate, 102)) 
			 GROUP BY RdRateMasterID) x 

			INNER JOIN RateDefinition r ON	x.RdRateMasterID = r.RdRateMasterID AND 
											x.StartDate = r.RdStartDate 
			INNER JOIN RateMaster m ON	x.RdRateMasterID = m.RateMasterID 

	WHERE	m.RateMasterID = @RateMasterID AND 
			m.RmEnableInd >= 0 


  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[tmpEguideRatesToMove]'
GO
CREATE TABLE [rateengine].[tmpEguideRatesToMove]
(
[RateCompanyID] [int] NULL,
[RateMasterID] [int] NULL,
[RateDefID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO


PRINT N'Creating [rateengine].[GetPartTypeList]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

create procedure [rateengine].[GetPartTypeList] AS

SELECT PartTypeID,
       PtName,
       PtSortOrder
  FROM dbo.PartType
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetRateDefinitionByStartDate]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [rateengine].[GetRateDefinitionByStartDate] ( 
									@RateMasterID AS int,
									@StartDate AS datetime) AS

	SELECT	r.RateDefID, 
			r.RdRateMasterID As RdRmID,
			r.RdStartDate, 
			r.RdSeasonalInd, 
			r.RdTieredInd, 
			r.RdTimeOfUseInd, 
			r.RdDemandInd,
			r.RdFuelAdjustmentInd, 
			r.RdBasicInd,
			r.RdTsID, 
			r.RdDsID, 
			r.RdDifferenceNote, 
			r.RdEnableInd,
			r.RdBrID,
			r.RdNmID,
			r.RdNetMeteringInd,	
			r.RdTierAccumByTOUInd,		
			m.RmFuID, 
			m.RmSvID, 
			m.RmCtID, 
			m.RmUnID, 
			m.RmPrID,
			m.RmDPrID,
			m.RmClientRateID, 
			m.RmRateCompanyID, 
			m.RmDefault,
			m.RmChildrenInd, 
			m.RmLtID,
			m.RmBdID,
			m.RmHgID
	FROM
			(SELECT RateDefinition.RdRateMasterID,
					RateDefinition.RDStartDate
			 FROM	RateDefinition
			 WHERE	(RateDefinition.RdRateMasterID = @RateMasterID) AND
					(RateDefinition.RdStartDate = CONVERT(DATETIME, @StartDate, 102))) x
			INNER JOIN RateDefinition r ON x.RdRateMasterID = r.RdRateMasterID AND 
										   x.RDStartDate = r.RdStartDate
			INNER JOIN RateMaster m ON x.RdRateMasterID = m.RateMasterID

	WHERE	m.RateMasterID = @RateMasterID AND 
			m.RmEnableInd >= 0
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetRateMaintDataSession]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

create procedure [rateengine].[GetRateMaintDataSession] ( @SessionID AS varchar(36) ) AS

	SELECT	SessionID,
		    RateMaintID,
			UploadDateTime,
		    RateCompanyID,
			RateMasterID,
		    SourceTypeID,
		    SourceID,
		    ClientRateID,
		    RateDefID,
		    EffectiveDate,
		    ChargeType,
		    Tier,
		    TimeOfUse,
		    SeasonID,
			StepID,
		    PartTypeID,
		    CostTypeID,
		    ChargeValue,
		    CalcTypeID,
		    ErrorText,
		    ImportStatus,
		    ImportDateTime,
		    originalRateCompanyID,
		    originalSourceType,
		    originalSourceID,
		    originalSourceName,
		    originalRateClass,
		    originalEffectiveDate,
		    originalTypeOfCharge,
		    originalTier,
		    originalTimeOfUse,
		    originalSeason,
		    originalPartType,
		    originalCostType,
		    originalChargeValue,
		    originalCalcType,
			originalStep, 
			RebateClassID, 
			originalRebateClassID

	  FROM	[rateengine].[RateMaintData]

	  WHERE	SessionID = @SessionID

	  ORDER BY	RateMaintID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetSeasonList]'
GO

create procedure [rateengine].[GetSeasonList] AS

SELECT SeasonID,
       SeName,
       SeSortOrder
  FROM dbo.Season
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetOldestStartDate]'
GO

create procedure [rateengine].GetOldestStartDate ( @RateMasterID int ) AS

	SELECT TOP 1 RateDefinition.RdStartDate AS OldestEffectiveDate
	FROM RateDefinition INNER JOIN
	     RateMaster ON RateDefinition.RdRateMasterID = RateMaster.RateMasterID
	WHERE (RateMaster.RmEnableInd = 1) AND (RateDefinition.RdEnableInd = 1) AND (RateDefinition.RdRateMasterID = @RateMasterID)
	ORDER BY RateDefinition.RdStartDate

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[Step]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[Step]
(
[StepID] [tinyint] NOT NULL,
[StName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Step] on [rateengine].[Step]'
GO
ALTER TABLE [rateengine].[Step] ADD CONSTRAINT [PK_Step] PRIMARY KEY CLUSTERED  ([StepID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RebateClass]'
GO
CREATE TABLE [rateengine].[RebateClass]
(
[RebateClassID] [tinyint] NOT NULL,
[RCName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RCSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RebateClass] on [rateengine].[RebateClass]'
GO
ALTER TABLE [rateengine].[RebateClass] ADD CONSTRAINT [PK_RebateClass] PRIMARY KEY CLUSTERED  ([RebateClassID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetServiceCharge]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

create procedure [rateengine].[GetServiceCharge] (@RateDefID AS int,
  											   @CalcTypeID AS tinyint,
											   @CostTypeID AS int, 
											   @StepID AS tinyint,
											   @SeasonID AS tinyint, 
											   @RebateClassID AS tinyint) AS

	SELECT	ServiceCharge.RateDefID AS RdID, 
			ServiceCharge.CalcTypeID AS CaID, 
			ServiceCharge.StepID AS StID, 
			ServiceCharge.SeasonID AS SeID, 
			ServiceCharge.CostTypeID AS CtID, 
			ServiceCharge.RebateClassID AS RcID,
			ServiceCharge.ChargeValue, 
			CaName, 
			StName, 
			SeName, 
			CtName, 
			RCName

	FROM ServiceCharge 

		INNER JOIN ChargeCalcType ON ServiceCharge.CalcTypeID = ChargeCalcType.CalcTypeID 
		INNER JOIN Step ON ServiceCharge.StepID = Step.StepID 
		INNER JOIN Season ON ServiceCharge.SeasonID = Season.SeasonID 
		INNER JOIN CostType ON ServiceCharge.CostTypeID = CostType.CostTypeID 
		INNER JOIN RebateClass On ServiceCharge.RebateClassID = RebateClass.RebateClassID

	WHERE ServiceCharge.RateDefID = @RateDefID AND
		  ServiceCharge.CalcTypeID = @CalcTypeID AND
		  ServiceCharge.CostTypeID = @CostTypeID AND
		  ServiceCharge.StepID = @StepID AND
		  ServiceCharge.SeasonID = @SeasonID AND
		  ServiceCharge.RebateClassID = @RebateClassID

	ORDER BY ServiceCharge.RateDefID 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetStepList]'
GO

create procedure [rateengine].[GetStepList] AS

SELECT StepID,
       StName,
       StSortOrder
  FROM dbo.Step
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthUpdFuelCostRecovery]'
GO

create procedure [rateengine].[AuthUpdFuelCostRecovery] @FrId int, @StartDate datetime, @ChargeValue  float AS

	UPDATE FuelCostRecovery
	SET FrStartDate=@StartDate, FrChargeValue=@ChargeValue
	WHERE FrId = @FRid

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetTaxCharge]'
GO

create procedure [rateengine].[GetTaxCharge] (@RateDefID AS int, 
										   @CostTypeID AS int) AS

	SELECT	TaxCharge.RateDefID AS RdID, 
			TaxCharge.CostTypeID AS CtID, 
			TaxCharge.ChargeValue, 
			CtName 

	FROM	TaxCharge 
			INNER JOIN CostType ON TaxCharge.CostTypeID = CostType.CostTypeID 

	WHERE	TaxCharge.RateDefID = @RateDefID AND
			TaxCharge.CostTypeID = @CostTypeID

	ORDER BY	TaxCharge.CostTypeID 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthSelTerritoryLookupGroup]'
GO

create procedure [rateengine].[AuthSelTerritoryLookupGroup] AS

	SELECT LgID, LgName FROM TerritoryLookupGroup ORDER BY LgName

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetTierList]'
GO

create procedure [rateengine].[GetTierList] AS

SELECT TierID,
       TiName,
       TiSortOrder
  FROM dbo.Tier
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthSelRateMasterServiceTerritories]'
GO

create procedure [rateengine].[AuthSelRateMasterServiceTerritories]  @RateMasterID int  AS

	--SELECT HSCM.CompanyId AS CompanyId, HSCM.CompanyName AS CompanyName FROM HubStatic..CompanyMaster HSCM, RateMasterServiceTerritory RMST
	--WHERE RMST.RateMasterID = @RateMasterID AND HSCM.CompanyId = RMST.CompanyId
	--ORDER BY CompanyName

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetTimeOfUseList]'
GO

create procedure [rateengine].[GetTimeOfUseList] AS

SELECT TouID,
       TuName,
       TuSortOrder
  FROM dbo.TimeOfUse
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthSelRateDefStartDate]'
GO

create procedure [rateengine].[AuthSelRateDefStartDate] @RateDefID int AS

	SELECT RdStartDate, RdStartDateValidInd FROM RateDefinition
	WHERE RateDefID = @RateDefID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetRebateClassList]'
GO

create procedure [rateengine].[GetRebateClassList] AS

SELECT RebateClassID,
       RCName,
       RCSortOrder
  FROM dbo.RebateClass

-- Grant execute permissions.

	GRANT EXECUTE ON [rateengine].[GetRebateClassList] TO epuser
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetUseCharge]'
GO

create procedure [rateengine].[GetUseCharge] (@RateDefID AS int,
										   @TierID AS tinyint,
										   @TouID AS tinyint,
										   @SeasonID AS tinyint,
										   @CostTypeID AS int,
										   @PartTypeID AS tinyint) AS

	SELECT	RateDefID,
		    TierID,
		    TouID,
		    SeasonID,
		    CostTypeID,
		    PartTypeID,
		    ChargeValue,
		    RgID,
		    [Description],
		    RTPgId

	  FROM  UseCharge

	WHERE RateDefID = @RateDefID AND
		  TierID = @TierID AND
		  TouID = @TouID AND
		  SeasonID = @SeasonID AND
		  CostTypeID = @CostTypeID AND
		  PartTypeID = @PartTypeID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthSelRateClassName]'
GO

create procedure [rateengine].[AuthSelRateClassName] @RateMasterID int AS

	SELECT RmClientRateID FROM RateMaster
	WHERE RateMasterID = @RateMasterID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[InsertMinimumCharge]'
GO

create procedure [rateengine].[InsertMinimumCharge] (
							@RateDefID AS int,
							@CalcTypeID AS tinyint,
							@CostTypeID AS int,
							@ChargeValue AS float) AS

	INSERT INTO MinimumCharge ( RateDefID, 
								CalcTypeID, 
								CostTypeID, 
								ChargeValue) 
	VALUES (	@RateDefID,
				@CalcTypeID,
				@CostTypeID,
				@ChargeValue  )
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthSelFCRGroup]'
GO

create procedure [rateengine].[AuthSelFCRGroup] @RateCompanyId int AS

	SELECT RgId, RgName
	FROM FuelCostRecoveryGroup
	WHERE RgRateCompanyId = @RateCompanyId
	ORDER BY RgName

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[InsertRateMaintData]'
GO

create procedure [rateengine].[InsertRateMaintData] ( 
						@SessionID varchar(36),
						@UploadDateTime datetime,
						@RateCompanyID int,
						@RateMasterID int,
						@SourceTypeID tinyint,
						@SourceID int,
						@ClientRateID AS varchar(16),
						@RateDefID int,
						@EffectiveDate datetime,
						@ChargeType tinyint,
						@Tier tinyint,
						@TimeOfUse tinyint,
						@SeasonID int,
						@StepID tinyint,
						@PartTypeID int,
						@CostTypeID int,
						@ChargeValue float,
						@CalcTypeID int,
						@ErrorText varchar(256) ,
						@ImportStatus tinyint,
						@ImportDateTime datetime,
						@originalRateCompanyID varchar(32),
						@originalSourceType varchar(32),
						@originalSourceID varchar(32),
						@originalSourceName varchar(32),
						@originalRateClass varchar(32),
						@originalEffectiveDate varchar(32),
						@originalTypeOfCharge varchar(32),
						@originalTier varchar(32),
						@originalTimeOfUse varchar(32),
						@originalSeason varchar(32),
						@originalPartType varchar(32),
						@originalCostType varchar(32),
						@originalChargeValue varchar(32),
						@originalCalcType varchar(32),
						@originalStep varchar(32), 
						@RebateClassID int,
						@originalRebateClassID varchar(32)) AS

-- Validation: Effective Date is not valid.
	IF NOT IsDate(@EffectiveDate) = 1
		SET @EffectiveDate = NULL

-- Validation: Import Date/Time is not valid.
	IF NOT IsDate(@ImportDateTime) = 1
		SET @ImportDateTime = NULL

	INSERT INTO [rateengine].[RateMaintData]
			   (SessionID,
				UploadDateTime,
				RateCompanyID,
				RateMasterID,
				SourceTypeID,
				SourceID,
				ClientRateID,
				RateDefID,
				EffectiveDate,
				ChargeType,
				Tier,
				TimeOfUse,
				SeasonID,
			    StepID,
				PartTypeID,
				CostTypeID,
				ChargeValue,
				CalcTypeID,
				ErrorText,
				ImportStatus,
				ImportDateTime,
				originalRateCompanyID,
				originalSourceType,
				originalSourceID,
				originalSourceName,
				originalRateClass,
				originalEffectiveDate,
				originalTypeOfCharge,
				originalTier,
				originalTimeOfUse,
				originalSeason,
				originalPartType,
				originalCostType,
				originalChargeValue,
				originalCalcType,
				originalStep,
				RebateClassID,
				originalRebateClassID)

	VALUES (	@SessionID,
				@UploadDateTime,
				@RateCompanyID,
				@RateMasterID,
				@SourceTypeID,
				@SourceID,
				@ClientRateID,
				@RateDefID,
				@EffectiveDate,
				@ChargeType,
				@Tier,
				@TimeOfUse,
				@SeasonID,
			    @StepID,
				@PartTypeID,
				@CostTypeID,
				@ChargeValue,
				@CalcTypeID,
				@ErrorText,
				@ImportStatus,
				@ImportDateTime,
				@originalRateCompanyID,
				@originalSourceType,
				@originalSourceID,
				@originalSourceName,
				@originalRateClass,
				@originalEffectiveDate,
				@originalTypeOfCharge,
				@originalTier,
				@originalTimeOfUse,
				@originalSeason,
				@originalPartType,
				@originalCostType,
				@originalChargeValue,
				@originalCalcType,
				@originalStep,
				@RebateClassID,
				@originalRebateClassID)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[InsertServiceCharge]'
GO

create procedure [rateengine].[InsertServiceCharge] (	@RateDefID AS int, 
												@CalcTypeID AS tinyint,
												@CostTypeID AS int,
												@ChargeValue AS float,
												@StepID AS tinyint,
												@SeasonID AS tinyint,
												@RebateClassID AS tinyint) AS

	INSERT INTO ServiceCharge (		RateDefID, 
									CalcTypeID, 
									CostTypeID,
									ChargeValue,
									StepID, 
									SeasonID, 
									RebateClassID)
	VALUES (	@RateDefID,
				@CalcTypeID,
				@CostTypeID,
				@ChargeValue,
				@StepID,
				@SeasonID,
				@RebateClassID)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthInsRateMasterServiceTerritory]'
GO

create procedure [rateengine].[AuthInsRateMasterServiceTerritory] @RateMasterID int, @CompanyID varchar(32) AS

	INSERT RateMasterServiceTerritory
	VALUES ( @RateMasterID, @CompanyID )

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetCriticalPeakEvents]'
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [rateengine].[GetCriticalPeakEvents]
    @RateCompanyId AS INT = 100 ,
    @BaseRateClass AS VARCHAR(16) ,
    @StartDate AS DATETIME ,
    @EndDate AS DATETIME
AS 
    SET nocount ON

    DECLARE @RTPStreamId AS INT
    DECLARE @RateDefId AS INT
    DECLARE @RTPgId AS INT

    SELECT TOP 1
            @RateDefId = RateDefId ,
            @RTPStreamId = RdRTPStreamId
    FROM    RateDefinition WITH ( NOLOCK )
            INNER JOIN RateMaster WITH ( NOLOCK ) ON RateDefinition.RdRateMasterID = RateMaster.RateMasterID
    WHERE   RateMaster.RmRateCompanyID = @RateCompanyID
            AND RateMaster.RmClientRateID = @BaseRateClass
            AND RateMaster.RmEnableInd = 1
            AND RateDefinition.RdEnableInd = 1
            AND RateDefinition.RdRTPInd = 1
            AND ( ( RateDefinition.RdStartDate >= @StartDate
                    AND RateDefinition.RdStartDate <= @EndDate
                  )
                  OR ( RateDefinition.RdStartDate < @StartDate )
                )
    ORDER BY RateDefinition.RdStartDate DESC

    SELECT  @RTPgId = RTPgId
    FROM    UseCharge
    WHERE   UseCharge.RateDefId = @RateDefId
            AND RTPgId IS NOT NULL

    IF @RTPgId IS NOT NULL 
        BEGIN

              -- CRITICAL PEAK ONLY (4) or PEAK TIME REBATE ONLY (5), treat this as if it were CPP
            IF @RTPStreamId = 4
                OR @RTPStreamId = 5 
                BEGIN
              
                    SELECT  MIN(CONVERT(DATETIME, TimeStart)) AS TimeStart ,
                            DATEADD(HOUR, -1, MAX(CONVERT(DATETIME, TimeEnd))) AS TimeEnd ,
                            SUM(Price) ,
                            MIN(4)
                    FROM    RTPCriticalPeak
                    WHERE   CONVERT(DATETIME, TimeStart) >= @StartDate
                            AND CONVERT(DATETIME, TimeStart) <= @EndDate
                            AND RTPgId = @RTPgId
                    GROUP BY DATEPART(DD, TimeStart) ,
                            DATEPART(MM, TimeStart) ,
                            DATEPART(YY, TimeStart)
                    ORDER BY TimeStart          
                                         
                END

        END

    SET nocount OFF



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[InsertTaxCharge]'
GO
SET QUOTED_IDENTIFIER OFF
GO

create procedure [rateengine].[InsertTaxCharge] (
							@RateDefID AS int, 
							@CostTypeID AS int,
							@ChargeValue AS float) AS

	INSERT INTO	TaxCharge (	RateDefID, 
							CostTypeID, 
							ChargeValue ) 
	VALUES (	@RateDefID,
				@CostTypeID,
				@ChargeValue )
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthInsFuelCostRecovery]'
GO

create procedure [rateengine].[AuthInsFuelCostRecovery] @FrRgID int, @StartDate datetime, @ChargeValue  float AS

	INSERT FuelCostRecovery
	VALUES (@FrRgID, @StartDate, @ChargeValue)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[InsertUseCharge]'
GO

create procedure [rateengine].[InsertUseCharge] (
									@RateDefID AS int,
									@TierID AS tinyint,
									@TouID AS tinyint,
									@SeasonID AS tinyint,
									@CostTypeID AS int,
									@PartTypeID AS tinyint,
									@ChargeValue AS float,
									@RgID AS int,
									@Description AS varchar(32),
									@RTPgID AS int) AS

	INSERT INTO UseCharge (	
							RateDefID, 
							TierID,
							TouID,
							SeasonID,
							CostTypeID,
							PartTypeID, 
							ChargeValue,
							RgID,
							[Description],
							RTPgID)
	VALUES (	@RateDefID,
				@TierID,
				@TouID,
				@SeasonID,
				@CostTypeID,
				@PartTypeID, 
				@ChargeValue,
				@RgID,
				@Description,
				@RTPgID  )
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthDelRateMasterServiceTerritory]'
GO

create procedure [rateengine].[AuthDelRateMasterServiceTerritory] @RateMasterID int, @CompanyId varchar(32) AS

	DELETE RateMasterServiceTerritory
	WHERE RateMasterID = @RateMasterID AND CompanyId = @CompanyId

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[UpdateMinimumCharge]'
GO

create procedure [rateengine].UpdateMinimumCharge ( 
						@RateDefID AS int,
						@CalcTypeID AS tinyint,
						@CostTypeID AS int,
						@CalcTypeID_New AS tinyint,
						@CostTypeID_New AS int,
						@ChargeValue_New AS float) AS

	SET NOCOUNT ON

	UPDATE [rateengine].[MinimumCharge]

	SET		CalcTypeID = @CalcTypeID_New,
			CostTypeID = @CostTypeID_New,
			ChargeValue = @ChargeValue_New

	WHERE	RateDefID = @RateDefID AND
			CalcTypeID = @CalcTypeID AND
			CostTypeID = @CostTypeID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RTPStream]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[RTPStream]
(
[RTPStreamId] [tinyint] NOT NULL,
[RTPStream] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RTPStream] on [rateengine].[RTPStream]'
GO
ALTER TABLE [rateengine].[RTPStream] ADD CONSTRAINT [PK_RTPStream] PRIMARY KEY CLUSTERED  ([RTPStreamId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthSelRTPStream]'
GO
SET ANSI_NULLS OFF
GO
create procedure [rateengine].[AuthSelRTPStream] AS

Select * from RTPStream
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthDelFuelCostRecovery]'
GO
SET QUOTED_IDENTIFIER OFF
GO

create procedure [rateengine].[AuthDelFuelCostRecovery] @FrId int AS

	DELETE FuelCostRecovery
	WHERE FrId = @FrId

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[UpdateRateMaintData]'
GO

create procedure [rateengine].[UpdateRateMaintData] ( 
						@RateMaintID AS int,
						@SessionID_New AS varchar(36),
						@UploadDateTime_New AS datetime,
						@RateCompanyID_New AS int,
						@RateMasterID_New AS int,
						@SourceTypeID_New AS tinyint,
						@SourceID_New AS int,
						@ClientRateID_New AS varchar(16),
						@RateDefID_New AS int,
						@EffectiveDate_New AS datetime,
						@ChargeType_New AS tinyint,
						@Tier_New AS tinyint,
						@TimeOfUse_New AS tinyint,
						@SeasonID_New AS int,
						@StepID_New As int,
						@PartTypeID_New AS int,
						@CostTypeID_New AS int,
						@ChargeValue_New AS float,
						@CalcTypeID_New AS int,
						@ErrorText_New AS varchar(256),
						@ImportStatus_New AS tinyint,
						@ImportDateTime_New AS datetime,
						@originalRateCompanyID_New AS varchar(32),
						@originalSourceType_New AS varchar(32),
						@originalSourceID_New AS varchar(32),
						@originalSourceName_New AS varchar(32),
						@originalRateClass_New AS varchar(32),
						@originalEffectiveDate_New AS varchar(32),
						@originalTypeOfCharge_New AS varchar(32),
						@originalTier_New AS varchar(32),
						@originalTimeOfUse_New AS varchar(32),
						@originalSeason_New AS varchar(32),
						@originalPartType_New AS varchar(32),
						@originalCostType_New AS varchar (32),
						@originalChargeValue_New AS varchar (32),
						@originalCalcType_New AS varchar (32),
						@originalStep_New AS varchar (32), 
						@RebateClassID_New AS int,
						@originalRebateClassID_New AS varchar(32)) AS

	SET NOCOUNT ON

	UPDATE [rateengine].[RateMaintData]

	SET
			SessionID = @SessionID_New,
            UploadDateTime = @UploadDateTime_New,
			RateCompanyID = @RateCompanyID_New,
			RateMasterID = @RateMasterID_New,
			SourceTypeID = @SourceTypeID_New,
			SourceID = @SourceID_New,
			ClientRateID = @ClientRateID_New,
			RateDefID = @RateDefID_New,
			EffectiveDate = @EffectiveDate_New,
			ChargeType = @ChargeType_New,
			Tier = @Tier_New,
			TimeOfUse = @TimeOfUse_New,
			SeasonID = @SeasonID_New,
			StepID = @StepID_New,
			PartTypeID = @PartTypeID_New,
			CostTypeID = @CostTypeID_New,
			ChargeValue = @ChargeValue_New,
			CalcTypeID = @CalcTypeID_New,
			ErrorText = @ErrorText_New,
			ImportStatus = @ImportStatus_New,
			ImportDateTime = @ImportDateTime_New,
			originalRateCompanyID = @originalRateCompanyID_New,
			originalSourceType = @originalSourceType_New,
			originalSourceID = @originalSourceID_New,
			originalSourceName = @originalSourceName_New,
			originalRateClass = @originalRateClass_New,
			originalEffectiveDate = @originalEffectiveDate_New,
			originalTypeOfCharge = @originalTypeOfCharge_New,
			originalTier = @originalTier_New,
			originalTimeOfUse = @originalTimeOfUse_New,
			originalSeason = @originalSeason_New,
			originalPartType = @originalPartType_New,
			originalCostType = @originalCostType_New,
			originalChargeValue = @originalChargeValue_New,
			originalCalcType = @originalCalcType_New,
            originalStep = @originalStep_New, 
            originalRebateClassID = @originalRebateClassID_New,
            RebateClassID = @RebateClassID_New

	WHERE	RateMaintID = @RateMaintID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[UpdateServiceCharge]'
GO

create procedure [rateengine].[UpdateServiceCharge] ( 
						@RateDefID AS int,
						@CalcTypeID AS tinyint,
						@CostTypeID AS int,
						@StepID AS tinyint,
						@SeasonID AS tinyint, 
						@RebateClassID as tinyint,
						@CalcTypeID_New AS tinyint,
						@CostTypeID_New AS int,
						@ChargeValue_New AS float,
						@StepID_New AS tinyint,
						@SeasonID_New AS tinyint,
						@RebateClassID_New as tinyint) AS

	SET NOCOUNT ON

	UPDATE [rateengine].[ServiceCharge]
	SET		CalcTypeID = @CalcTypeID_New,
			CostTypeID = @CostTypeID_New,
			ChargeValue = @ChargeValue_New,
			StepID = @StepID_New,
			SeasonID = @SeasonID_New,
			RebateClassID = @RebateClassID_New
	WHERE	RateDefID = @RateDefID AND
			CalcTypeID = @CalcTypeID AND
			CostTypeID = @CostTypeID AND
			StepID = @StepID AND
			SeasonID = @SeasonID AND
			RebateClassID = @RebateClassID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthCopyRateMasterServiceTerritory]'
GO

create procedure [rateengine].AuthCopyRateMasterServiceTerritory( @RateMasterID int, @sourceRateMasterID int ) AS
		
set NOCOUNT on 
	
		INSERT INTO RateMasterServiceTerritory
			(RateMasterID, CompanyID)
		SELECT @RateMasterID As RateMasterID, CompanyID FROM RateMasterServiceTerritory
			WHERE RateMasterID = @sourceRateMasterID

set NOCOUNT off

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[UpdateTaxCharge]'
GO

create procedure [rateengine].[UpdateTaxCharge] (
							@RateDefID AS int, 
							@CostTypeID AS int,
							@CostTypeID_New AS int,
							@ChargeValue_New AS float) AS

	UPDATE TaxCharge 

	SET	CostTypeID = @CostTypeID_New, 
		ChargeValue = @ChargeValue_New

	WHERE	RateDefID = @RateDefID AND
			CostTypeID = @CostTypeID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[UpdateUseCharge]'
GO

create procedure [rateengine].UpdateUseCharge ( 
							@RateDefID AS int,
							@TierID AS tinyint,
							@TouID AS tinyint,
							@SeasonID AS tinyint,
							@CostTypeID AS int,
							@PartTypeID AS tinyint,
							@TierID_New AS tinyint,
							@TouID_New AS tinyint,
							@SeasonID_New AS tinyint,
							@CostTypeID_New AS int,
							@PartTypeID_New AS tinyint,
							@ChargeValue_New AS float,
							@RgID_New AS int,
							@Description_New AS varchar(32),
                            @RTPgID_New AS int) AS

SET NOCOUNT ON

	UPDATE [rateengine].[UseCharge]

	SET		
			TierID = @TierID_New,
			TouID = @TouID_New,
			SeasonID = @SeasonID_New,
			CostTypeID = @CostTypeID_New,
			PartTypeID = @PartTypeID_New,
			ChargeValue = @ChargeValue_New,
			RgID = @RgID_New,
			[Description] = @Description_New,
            RTPgID = @RTPgID_New

	WHERE	RateDefID = @RateDefID AND
			TierID = @TierID AND
			TouID = @TouID AND
			SeasonID = @SeasonID AND
			CostTypeID = @CostTypeID AND
			PartTypeID = @PartTypeID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthSelServiceCompaniesByRefId]'
GO

create procedure [rateengine].[AuthSelServiceCompaniesByRefId] @RateCompanyId int, @RateMasterID int  AS

/*
This Store Procedure will work only for REFERRERs, not for Suppliers.
For Suppliers, we'll have to add a UNION query to find Companies by Suppliers to the existing query.
*/
	--SELECT HSCM.CompanyId AS CompanyId, HSCM.CompanyName AS CompanyName FROM HubStatic..RateCompanyMap HSRM, HubStatic..CompanyMaster HSCM, HubStatic..ReferrerToCompany HSRC, RateMaster RM
	--WHERE HSRM.RateCompanyId = @RateCompanyId AND HSRC.ReferrerId = HSRM.SourceId AND RM.RateMasterID = @RateMasterID AND HSRC.FuelType = RM.RmFuId AND HSCM.CompanyId = HSRC.CompanyId AND HSRC.CompanyId Not In (SELECT CompanyId FROM RateMasterServiceTerritory WHERE RateMasterID = @RateMasterID)
	--ORDER BY CompanyName

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetCriticalPeakEventsByRTPGroupId]'
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [rateengine].[GetCriticalPeakEventsByRTPGroupId]
@RTPgId as int,
@StartDate as datetime,
@EndDate as datetime

AS
	SET nocount on
		
	SELECT MIN(CONVERT(DATETIME, TimeStart)) As TimeStart , 
			DATEADD(HOUR, -1, MAX(CONVERT(DATETIME, TimeEnd))) AS TimeEnd,
			Sum(Price), MIN(4)
	FROM RTPCriticalPeak
	WHERE CONVERT(DATETIME, TimeStart) >= @StartDate
					AND CONVERT(DATETIME, TimeStart) <= @EndDate 
					AND RTPgId = @RTPgId
	GROUP BY DATEPART(DD,TimeStart), DATEPART(MM,TimeStart), DATEPART(YY,TimeStart)	
	Order By TimeStart		
						
	SET nocount off

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[updRTPCriticalPeak]'
GO

create procedure [rateengine].[updRTPCriticalPeak] 
					@RateCompanyId INT,
					@RTPGroupName VARCHAR(50),
					@TimeStart VARCHAR(20),
					@TimeEnd VARCHAR(20),
					@Price FLOAT
AS

SET NOCOUNT ON

DECLARE @RTPgID INT

SELECT @RTPgId = RTPgId 
FROM RTPGroup  
WHERE RTPgRateCompanyID = @RateCompanyID 
		AND RTPGroupName = @RTPGroupName


If  @RTPgId is null 
BEGIN
	declare @errMsg varchar(max)
	set @errMsg = 'NULL Group ID from rtpGroupname =' + @RTPGroupName + ' from companyID=' + CONVERT(varchar(20),@RateCompanyID) + ' At Time = ' + convert(varchar(20),getdate())
	RAISERROR (@errMsg, 16, 1)
	RETURN
END

IF (SELECT COUNT(*) 
	FROM RTPCriticalPeak 
	WHERE RTPgId = @RTPgId 
			AND TimeStart >= @TimeStart 
			AND TimeEnd <= @TimeEnd) > 0
BEGIN

	DELETE RTPCriticalPeak 
	WHERE RTPgId = @RTPgId 
			AND TimeStart >= @TimeStart 
			AND TimeEnd <= @TimeEnd

	INSERT RTPCriticalPeak values (@RTPgId, @TimeStart, @TimeEnd, @Price)

END
ELSE
 
BEGIN

	INSERT RTPCriticalPeak values (@RTPgId, @TimeStart, @TimeEnd, @Price)

END
SET NOCOUNT OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetRealTimePricing1]'
GO


create procedure [rateengine].[GetRealTimePricing1]
@RateCompanyId as int = 100,
@RateClass as varchar(16) = 'ETOU-B1',
@StartDate as datetime = '1/1/2000',
@EndDate as datetime = '1/1/2010'

AS

Set nocount on

Declare @RTPStreamId As int
Declare @RateDefId As int
DecLare @RTPgId As int

SELECT Top 1 @RateDefId = RateDefId, @RTPStreamId = RdRTPStreamId
FROM RateDefinition
	INNER JOIN RateMaster ON RateDefinition.RdRateMasterID = RateMaster.RateMasterID
	WHERE RateMaster.RmRateCompanyID = @RateCompanyID
		AND RateMaster.RmClientRateID = @RateClass
		AND RateMaster.RmEnableInd = 1
		AND RateDefinition.RdEnableInd = 1
		AND RateDefinition.RdRTPInd = 1
	   	AND ( ( RateDefinition.RdStartDate >= @StartDate AND RateDefinition.RdStartDate <= @EndDate ) OR ( RateDefinition.RdStartDate < @StartDate ) )
ORDER BY RateDefinition.RdStartDate DESC

SELECT @RTPgId = RTPgId
FROM UseCharge
WHERE UseCharge.RateDefId = @RateDefId
	AND RTPgId IS NOT NULL

-- Create empty temp price table
Select Convert(DateTime, TimeStart) as TimeStart, Price, 0 as ActualStream into #TempPrice  from RTPDayAhead where  1=2

Declare @TempMaxDate as DateTime

If @RTPgId IS NOT NULL

BEGIN

-- REAL TIME ONLY
	If @RTPStreamId = 1

	BEGIN
		INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 3 From RTPFinal
		WHERE Convert(DateTime, TimeStart) >= @StartDate AND
		Convert(DateTime, TimeStart) <= @EndDate AND
		RTPgId = @RTPgId
		
		SELECT @TempMaxDate = Max(TimeStart) From #TempPrice

		If @TempMaxDate is Null
		BEGIN
			Select @TempMaxDate = DateAdd(hh, -1, @Startdate)
		END

		If @TempMaxDate < @EndDate
		BEGIN
			INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 2 From RTPPrelim
			WHERE Convert(DateTime, TimeStart) > @TempMaxDate AND
			Convert(DateTime, TimeStart) <= @EndDate AND
			RTPgId = @RTPgId
			
		END


	END


-- DAY AHEAD ONLY
	If @RTPStreamId = 2

	BEGIN
		INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 1 From RTPDayAhead
		WHERE Convert(DateTime, TimeStart) >= @StartDate AND
		Convert(DateTime, TimeStart) <= @EndDate AND
		RTPgId = @RTPgId
		
	END


-- REAL TIME AND DAY AHEAD
	If @RTPStreamId = 3

	BEGIN
		INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 3 From RTPFinal
		WHERE Convert(DateTime, TimeStart) >= @StartDate AND
		Convert(DateTime, TimeStart) <= @EndDate AND
		RTPgId = @RTPgId
		

		SELECT @TempMaxDate = Max(TimeStart) From #TempPrice
		
		If @TempMaxDate is Null
		BEGIN
			Select @TempMaxDate = DateAdd(hh, -1, @Startdate)
		END

		If @TempMaxDate is Null OR @TempMaxDate < @EndDate
		BEGIN
			INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 2 From RTPPrelim
			WHERE Convert(DateTime, TimeStart) > @TempMaxDate AND
			Convert(DateTime, TimeStart) <= @EndDate AND
			RTPgId = @RTPgId
			
			SELECT @TempMaxDate = Max(TimeStart) From #TempPrice

			If @TempMaxDate is Null
			BEGIN
				Select @TempMaxDate = DateAdd(hh, -1, @Startdate)
			END

			If @TempMaxDate is Null OR @TempMaxDate < @EndDate
			BEGIN
				INSERT #TempPrice SELECT Convert(DateTime, TimeStart), Price, 1 From RTPDayAhead
				WHERE Convert(DateTime, TimeStart) > @TempMaxDate AND
				Convert(DateTime, TimeStart) <= @EndDate AND
				RTPgId = @RTPgId
				

			END
		END	

	END

-- Update RTP prices with critical peak prices if they exist
--	UPDATE #TempPrice
--	SET Price = cp.Price,
--		ActualStream = 4
--	FROM #TempPrice
--	INNER JOIN RTPCriticalPeak cp ON #TempPrice.TimeStart = CONVERT(DATETIME, cp.TimeStart)
--	WHERE RTPgId = @RTPgID
									

-- CRITICAL PEAK ONLY
	If @RTPStreamId = 4

	BEGIN
		INSERT #TempPrice 
		SELECT CONVERT(DATETIME, TimeStart), 
			   Price, 
			   4 
		FROM RTPCriticalPeak
		WHERE CONVERT(DATETIME, TimeStart) >= @StartDate 
						AND CONVERT(DATETIME, TimeStart) <= @EndDate 
						AND RTPgId = @RTPgId
		
	END

END

SELECT * FROM #TempPrice
ORDER BY TimeStart

Drop Table #TempPrice

Set nocount off


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RTPEventSimulateDuration]'
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[RTPEventSimulateDuration]
(
[RTPgId] [int] NOT NULL,
[SeasonID] [tinyint] NOT NULL,
[StartHour] [tinyint] NOT NULL,
[EndHour] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RTPEventSimulateDuration] on [rateengine].[RTPEventSimulateDuration]'
GO
ALTER TABLE [rateengine].[RTPEventSimulateDuration] ADD CONSTRAINT [PK_RTPEventSimulateDuration] PRIMARY KEY CLUSTERED  ([RTPgId], [SeasonID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetRTPSimulatedEventDurationByGroupId]'
GO
SET ANSI_NULLS OFF
GO


create procedure [rateengine].[GetRTPSimulatedEventDurationByGroupId]  
@RTPgId as int

AS 

	IF EXISTS(SELECT TOP 1 RTPgId FROM [rateengine].[RTPEventSimulateDuration] WHERE RTPgId = @RTPgId)
	BEGIN
		SELECT SeasonID, StartHour, EndHour FROM RTPEventSimulateDuration with(nolock) WHERE RTPgId = @RTPgId ORDER BY SeasonID, StartHour
	END
	ELSE
	BEGIN
		SELECT SeasonID, StartHour, EndHour FROM RTPEventSimulateDuration with(nolock) WHERE RTPgId = 0 ORDER BY SeasonID, StartHour
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthInsNewBasicRateClass]'
GO
SET QUOTED_IDENTIFIER OFF
GO

create procedure [rateengine].[AuthInsNewBasicRateClass] (
									     @RateClass varchar(16), 
										 @RateCompanyID int, 
										 @OneCompanyID varchar(32),
										 @Name varchar(64),
										 @Description varchar(64),
										 @Eligibility varchar(64),
										 @FuelID tinyint, 
										 @UnitOfMeasure tinyint, 
										 @ServiceType As tinyint, 
										 @CustomerType As tinyint,
										 @ProrateType As tinyint,
										 @DemandProrateType As tinyint,
										 @IsDefault As tinyint,
										 @IsEnabled As tinyint,
										 @EffectiveDate As Datetime, 
										 @TotalUseCharge As float,
										 @TotalServiceCharge As float
										 ) AS
	
set NOCOUNT on 
	
	declare @newRateMasterID As Int
	declare @newRdID As Int	
	declare @result As int


	BEGIN TRAN


	-- insert into MASTER
	INSERT INTO RateMaster 
		(RmClientRateID, RmRateCompanyID, RmFuID, RmUnID, RmSvID, RmCtID, 
		 RmPrID, RmDPrID, RmDefault, RmEnableInd )
	VALUES (@RateClass, @RateCompanyID, @FuelID, @UnitOfMeasure, @ServiceType, @CustomerType, 
			@ProrateType, @DemandProrateType, @IsDefault, @IsEnabled)
	;Select @newRateMasterID = SCOPE_IDENTITY()

	-- insert one TERRITORY/COMPANYID
	INSERT INTO RateMasterServiceTerritory (RateMasterID, CompanyID )
	VALUES ( @newRateMasterID, @OneCompanyID )

	-- insert one RateMasterText
	INSERT INTO RateMasterText (RateMasterID, LaID, [Name], Description, Eligibility )
	VALUES ( @newRateMasterID, 0, @Name, @Description, @Eligibility )


	-- insert into Rate definition for effective date;
	INSERT INTO RateDefinition (
		RdRateMasterID, 
		RdStartDate, 
		RdEditDate, 
		RdSeasonalInd, 
		RdTieredInd, 
		RdTimeOfUseInd, 
		RdDemandInd,
		RdFuelAdjustmentInd, 
		RdServiceStepsInd, 
		RdBasicInd, 
		RdTsID, 
		RdDsID, 
		RdSpID,
		RdSdpID, 
		RdEnableInd, 
		RdSeasonNum, 
		RdTierNum, 
		RdTOUNum, 
		RdDemandNum, 
		RdStepNum, 
		RdStartDateValidInd, 
		RdTOUBoundariesConsistent,
		RdBrID,
		RdNmID,
		RdNetMeteringInd 
		)
	VALUES (
		@newRateMasterID, 
		@EffectiveDate, 
		GETDATE(), 
		0, --RdSeasonalInd,
		0, --RdTieredInd, 
		0, --RdTimeOfUseInd, 
		0, --RdDemandInd,
		0, --RdFuelAdjustmentInd, 
		0, --RdServiceStepsInd, 
		1, --RdBasicInd, 
		0, --RdTsID, TierStructure
		0, --RdDsID, DemandStructure 
		3, --RdSpID, SeasonalProrate (3)
		1, --RdSdpID, SeasonalDemandProrate (1)
		1, --RdEnableInd, 
		0, --RdSeasonNum, 
		0, --RdTierNum, 
		0, --RdTOUNum, 
		0, --RdDemandNum, 
		0, --RdStepNum, 
		1, --RdStartDateValidInd, 
		0, --RdTOUBoundariesConsistent,
		0, --RdBrID, BaselineRule (0) 
		0, --RdNmID, NetMeteringType (0)
		0  --RdNetMeteringInd (0)
		) 
	;Select @newRdID = SCOPE_IDENTITY()


	-- USE CHARGE
	INSERT INTO UseCharge (	
		RateDefID, 
		TierID,
		TouID,
		SeasonID,
		CostTypeID,
		PartTypeID, 
		ChargeValue
		)
	VALUES 	(	
		@newRdID,
		10, --@TierID, 10=TotalServiceUse
		0, --@TouID,
		0, --@SeasonID,
		140, --@CostTypeID, 140 = STD_ENGY_ALLOTHER
		0, --@PartTypeID, 
		@TotalUseCharge
		)


	-- SERVICE CHARGE
	INSERT INTO ServiceCharge (		
		RateDefID, 
		CalcTypeID, 
		CostTypeID,
		ChargeValue,
		StepID, 
		SeasonID
		)
	VALUES (
		@newRdID,
		3, --@CalcTypeID, monthly
		1, --@CostTypeID, 1 = STD_CUST
		@TotalServiceCharge,
		0, --@StepID,
		0 --@SeasonID
		)


	if( @@ERROR = 0 ) begin
		COMMIT TRAN
		set @result = @newRateMasterID
	end
	else begin 
		ROLLBACK TRAN	
		set @result = 0
	end

	SELECT @result As Result

set NOCOUNT off


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RTPEventSimulate]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[RTPEventSimulate]
(
[RateCompanyID] [int] NOT NULL,
[Priority] [tinyint] NOT NULL,
[Month] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RTPEventSimulate] on [rateengine].[RTPEventSimulate]'
GO
ALTER TABLE [rateengine].[RTPEventSimulate] ADD CONSTRAINT [PK_RTPEventSimulate] PRIMARY KEY CLUSTERED  ([RateCompanyID], [Priority])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetRTPSimulatedEvents]'
GO
SET ANSI_NULLS OFF
GO


create procedure [rateengine].[GetRTPSimulatedEvents]  
@RateCompanyID as int,
@NumberOfEvents as int

AS 

	IF EXISTS(SELECT TOP 1 Priority FROM [rateengine].[RTPEventSimulate] WHERE RateCompanyID = @RateCompanyID)
	BEGIN
		SELECT TOP (@NumberOfEvents) Priority, [Month] FROM RTPEventSimulate with(nolock) WHERE RateCompanyID = @RateCompanyID ORDER BY Priority
	END
	ELSE
	BEGIN
		SELECT TOP (@NumberOfEvents) Priority, [Month] FROM RTPEventSimulate with(nolock) WHERE RateCompanyID = 0 ORDER BY Priority
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Creating [rateengine].[GetRateClassFromRateMasterID]'
GO

create procedure [rateengine].GetRateClassFromRateMasterID ( @RateMasterID int ) AS

	SELECT RmClientRateID 
		FROM RateMaster 
	WHERE RateMaster.RateMasterID = @RateMasterID 

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[DoesRateClassExist]'
GO

create procedure [rateengine].DoesRateClassExist ( @RateCompanyID int, @RateClass varchar(16), @Language int ) AS

	SELECT Count(*) As Count 
	FROM RateMaster INNER JOIN
		RateMasterText ON RateMaster.RateMasterID = RateMasterText.RateMasterID
	WHERE RateMaster.RmRateCompanyID = @RateCompanyID AND RateMaster.RmClientRateID = @RateClass 
		AND RateMasterText.LaID = @Language

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectAllCharges]'
GO

create procedure [rateengine].SelectAllCharges ( @RateDefinitionID Int ) AS

	BEGIN 
		EXEC [rateengine].[SelectUseCharges] @RateDefinitionID 
		EXEC [rateengine].[SelectServiceCharges] @RateDefinitionID
		EXEC [rateengine].[SelectMinimumCharges] @RateDefinitionID
		EXEC [rateengine].[SelectTaxCharges] @RateDefinitionID
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[Pollution]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[Pollution]
(
[PoID] [int] NOT NULL,
[PoDescription] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PoCO2] [float] NOT NULL CONSTRAINT [DF_Pollution_PoCO2] DEFAULT ((0)),
[PoSO2] [float] NOT NULL CONSTRAINT [DF_Pollution_PoSO2] DEFAULT ((0)),
[PoNOx] [float] NOT NULL CONSTRAINT [DF_Pollution_PoNOx] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Pollution] on [rateengine].[Pollution]'
GO
ALTER TABLE [rateengine].[Pollution] ADD CONSTRAINT [PK_Pollution] PRIMARY KEY CLUSTERED  ([PoID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectSimpleRateInformationA]'
GO



-- SelectSimpleRateInformationA
create procedure [rateengine].[SelectSimpleRateInformationA] ( @RateCompanyID Int, @RateClass varchar(16), @StartDate datetime, @EndDate datetime, @Language int ) AS

	SELECT RateDefID, RdRateMasterID, 
		RdStartDate, RdSeasonalInd, RdTieredInd, RdTimeOfUseInd, RdDemandInd,
		RdFuelAdjustmentInd, RdBasicInd, RdServiceStepsInd,
		RdTsID, RdDsID, RdSpID, RdSdpID, RdDifferenceNote, RdTOUBoundariesConsistent,
		RdRTPInd, RdRTPStreamId, RdBrID, RdNmID, RdNetMeteringInd, RdTierAccumByTOUInd, RdCapResInd, RdCPEventUsageAdderInd,
		RmFuID, RmSvID, RmCtID, RmUnID, RmPrID, RmDPrID,
		RmClientRateID, RmRateCompanyID, RmDefault, RmChildrenInd, RmLtID, RmBdID, RmHgID,
		[Name], [Description], [Eligibility],
		PoID, PoCO2, PoSO2, PoNOx
	FROM RateDefinition
	INNER JOIN RateMaster ON RateDefinition.RdRateMasterID = RateMaster.RateMasterID
	INNER JOIN RateMasterText ON RateMaster.RateMasterID = RateMasterText.RateMasterID
	INNER JOIN Pollution ON RateMaster.RmPoID = Pollution.PoID
	WHERE RateMaster.RmRateCompanyID = @RateCompanyID AND RateMaster.RmClientRateID = @RateClass 
		AND RateMaster.RmEnableInd = 1 AND RateMasterText.LaID = @Language
		AND RateDefinition.RdEnableInd = 1
	    AND ( RateDefinition.RdStartDate >= CONVERT(DATETIME, @StartDate, 102) AND 
	    	RateDefinition.RdStartDate <= CONVERT(DATETIME, @EndDate, 102) ) 
	UNION 

	SELECT d.RateDefID, d.RdRateMasterID, 
		d.RdStartDate, d.RdSeasonalInd, d.RdTieredInd, d.RdTimeOfUseInd, d.RdDemandInd, 
		d.RdFuelAdjustmentInd, d.RdBasicInd, RdServiceStepsInd,
		d.RdTsID, d.RdDsID, d.RdSpID, d.RdSdpID, d.RdDifferenceNote, d.RdTOUBoundariesConsistent,
		d.RdRTPInd, d.RdRTPStreamId, d.RdBrID, d.RdNmID, d.RdNetMeteringInd, d.RdTierAccumByTOUInd, d.RdCapResInd, d.RdCPEventUsageAdderInd,
		m.RmFuID, m.RmSvID, m.RmCtID, m.RmUnID, m.RmPrID, m.RmDPrID,
		m.RmClientRateID, m.RmRateCompanyID, m.RmDefault, m.RmChildrenInd, m.RmLtID, m.RmBdID, m.RmHgID,
		t.[Name], t.[Description], t.[Eligibility],
		p.PoID, p.PoCO2, p.PoSO2, p.PoNOx FROM (
	SELECT TOP 1 (RateDefID) As RateDefID, RdStartDate
	FROM RateDefinition
	INNER JOIN RateMaster ON RateDefinition.RdRateMasterID = RateMaster.RateMasterID
	INNER JOIN RateMasterText ON RateMaster.RateMasterID = RateMasterText.RateMasterID
	WHERE RateMaster.RmRateCompanyID = @RateCompanyID AND RateMaster.RmClientRateID = @RateClass 
		AND RateMaster.RmEnableInd = 1 AND RateMasterText.LaID = @Language
		AND RateDefinition.RdEnableInd = 1
	    AND RateDefinition.RdStartDate <= CONVERT(DATETIME, @StartDate, 102) ORDER BY RdStartDate DESC ) x

	INNER JOIN RateDefinition d on x.RateDefID = d.RateDefID
	INNER JOIN RateMaster m ON d.RdRateMasterID = m.RateMasterID
	INNER JOIN RateMasterText t ON m.RateMasterID = t.RateMasterID
	INNER JOIN Pollution p ON m.RmPoID = p.PoID

	ORDER BY RateDefinition.RdStartDate DESC
	
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectSimpleRateInformationB]'
GO






-- SelectSimpleRateInformationB
create procedure [rateengine].[SelectSimpleRateInformationB] ( @RateCompanyID Int, @RateClass varchar(16), @Language int ) AS

	SELECT RateDefID, RdRateMasterID, 
		RdStartDate, RdSeasonalInd, RdTieredInd, RdTimeOfUseInd, RdDemandInd,
		RdFuelAdjustmentInd, RdBasicInd, RdServiceStepsInd,
		RdTsID, RdDsID, RdSpID, RdSdpID, RdDifferenceNote, RdTOUBoundariesConsistent,
		RdRTPInd, RdRTPStreamId, RdBrID, RdNmID, RdNetMeteringInd, RdTierAccumByTOUInd, RdCapResInd, RdCPEventUsageAdderInd,
		RmFuID, RmSvID, RmCtID, RmUnID, RmPrID, RmDPrID,
		RmClientRateID, RmRateCompanyID, RmDefault, RmChildrenInd, RmLtID, RmBdID, RmHgID,
		[Name], [Description], [Eligibility],
		PoID, PoCO2, PoSO2, PoNOx
	FROM RateDefinition
	INNER JOIN RateMaster ON RateDefinition.RdRateMasterID = RateMaster.RateMasterID
	INNER JOIN RateMasterText ON RateMaster.RateMasterID = RateMasterText.RateMasterID
	INNER JOIN Pollution ON RateMaster.RmPoID = Pollution.PoID
	WHERE RateMaster.RmRateCompanyID = @RateCompanyID AND RateMaster.RmClientRateID = @RateClass 
		AND RateMaster.RmEnableInd = 1 AND RateMasterText.LaID = @Language
		AND RateDefinition.RdEnableInd = 1
	ORDER BY RateDefinition.RdStartDate DESC


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectSimpleRateInformationC]'
GO





--SelectSimpleRateInformationC
create procedure [rateengine].[SelectSimpleRateInformationC] ( @RateDefinitionID Int, @Language int ) AS

	SELECT RateDefID, RdRateMasterID, 
		RdStartDate, RdSeasonalInd, RdTieredInd, RdTimeOfUseInd, RdDemandInd,
		RdFuelAdjustmentInd, RdBasicInd, RdServiceStepsInd,
		RdTsID, RdDsID, RdSpID, RdSdpID, RdDifferenceNote, RdTOUBoundariesConsistent,
		RdRTPInd, RdRTPStreamId, RdBrID, RdNmID, RdNetMeteringInd, RdTierAccumByTOUInd, RdCapResInd, RdCPEventUsageAdderInd,
		RmFuID, RmSvID, RmCtID, RmUnID, RmPrID, RmDPrID,
		RmClientRateID, RmRateCompanyID, RmDefault, RmChildrenInd, RmLtID, RmBdID, RmHgID,
		[Name], [Description], [Eligibility],
		PoID, PoCO2, PoSO2, PoNOx
	FROM RateDefinition
	INNER JOIN RateMaster ON RateDefinition.RdRateMasterID = RateMaster.RateMasterID
	INNER JOIN RateMasterText ON RateMaster.RateMasterID = RateMasterText.RateMasterID
	INNER JOIN Pollution ON RateMaster.RmPoID = Pollution.PoID
	WHERE RateDefinition.RateDefID = @RateDefinitionID 
		AND RateMaster.RmEnableInd = 1 AND RateMasterText.LaID = @Language
		AND RateDefinition.RdEnableInd = 1
	ORDER BY RateDefinition.RdStartDate DESC


	
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[Setting]'
GO
CREATE TABLE [rateengine].[Setting]
(
[SettingID] [int] NOT NULL,
[Name] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SortOrder] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Setting] on [rateengine].[Setting]'
GO
ALTER TABLE [rateengine].[Setting] ADD CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED  ([SettingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[RateCompanySetting]'
GO
CREATE TABLE [rateengine].[RateCompanySetting]
(
[RateCompanyID] [int] NOT NULL,
[SettingID] [int] NOT NULL,
[SettingValue] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RateCompanySetting] on [rateengine].[RateCompanySetting]'
GO
ALTER TABLE [rateengine].[RateCompanySetting] ADD CONSTRAINT [PK_RateCompanySetting] PRIMARY KEY CLUSTERED  ([RateCompanyID], [SettingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetRateCompanySettings]'
GO
SET ANSI_NULLS OFF
GO


create procedure [rateengine].[GetRateCompanySettings]
@RateCompanyId as int

AS

    SET nocount ON;

    -- rk is the rank, which means the top setting if any are overridden by a client
    WITH summary AS (
        SELECT rcs.RateCompanyID,
                rcs.SettingID, 
				s.Name,
                rcs.SettingValue,
				ROW_NUMBER() OVER(PARTITION BY rcs.SettingID 
         ORDER BY rcs.RateCompanyID DESC) AS rk
		FROM RateCompanySetting rcs INNER JOIN 
        Setting s ON rcs.SettingID = s.SettingID
        WHERE RateCompanyID = 0 OR RateCompanyID = @RateCompanyId)
    SELECT sm.RateCompanyID, sm.SettingID, sm.Name, sm.SettingValue
        FROM summary sm
    WHERE sm.rk = 1

    SET nocount off;

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetFuelCostRecovery]'
GO
SET QUOTED_IDENTIFIER OFF
GO

create procedure [rateengine].GetFuelCostRecovery ( @FrID int, @RateCompanyID int ) AS

	-- RateCompanyID is technically not needed in query, but using it in case of
	-- a change to PK that contemplating in future
	SELECT FrChargeValue FROM FuelCostRecovery 
	WHERE FrID = @FrID AND @RateCompanyID = @RateCompanyID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectRateClassListA]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- SelectRateClassListA
create procedure [rateengine].[SelectRateClassListA] ( @RateCompanyID Int, @StartDate datetime, @EndDate datetime, @Language int ) AS
				
	SELECT RateDefID, r.RdRateMasterID, 
		r.RdStartDate, r.RdSeasonalInd, r.RdTieredInd, r.RdTimeOfUseInd, r.RdDemandInd,
		r.RdFuelAdjustmentInd, r.RdBasicInd,
		r.RdTsID, r.RdDsID, r.RdSpID, r.RdSdpID, r.RdDifferenceNote, r.RdTOUBoundariesConsistent,
		r.RdRTPInd, r.RdRTPStreamId, r.RdBrID, r.RdNmID, r.RdNetMeteringInd, r.RdTierAccumByTOUInd, r.RdCapResInd, r.RdCPEventUsageAdderInd,
		m.RmFuID, m.RmSvID, m.RmCtID, m.RmUnID, m.RmPrID, m.RmDPrID,
		m.RmClientRateID, m.RmRateCompanyID, m.RmDefault, m.RmChildrenInd, m.RmLtID, m.RmBdID, m.RmHgID,
		t.[Name], t.[Description], t.[Eligibility],
		p.PoID, p.PoCO2, p.PoSO2, p.PoNOx
	FROM
		(SELECT RateDefinition.RdRateMasterID,
		       MAX(RateDefinition.RdStartDate) As StartDate
		FROM RateDefinition
		WHERE (RateDefinition.RdEnableInd = 1) AND 
		      (RateDefinition.RdStartDate < CONVERT(DATETIME, @EndDate, 102))
		GROUP BY RdRateMasterID) x 
	INNER JOIN RateDefinition r ON x.RdRateMasterID = r.RdRateMasterID AND x.StartDate = r.RdStartDate
	INNER JOIN RateMaster m ON x.RdRateMasterID = m.RateMasterID
	INNER JOIN RateMasterText t ON m.RateMasterID = t.RateMasterID
	INNER JOIN Pollution p ON m.RmPoID = p.PoID
	WHERE m.RmRateCompanyID = @RateCompanyID AND m.RmEnableInd = 1 AND t.LaID = @Language
	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectRateClassListB]'
GO



--SelectRateClassListB
create procedure [rateengine].[SelectRateClassListB] ( @RateCompanyID Int, @StartDate datetime, @EndDate datetime, @Language int, @CustomerType int, @FuelType int, @Default tinyint ) AS
				
	SELECT RateDefID, r.RdRateMasterID, 
		r.RdStartDate, r.RdSeasonalInd, r.RdTieredInd, r.RdTimeOfUseInd, r.RdDemandInd,
		r.RdFuelAdjustmentInd, r.RdBasicInd,
		r.RdTsID, r.RdDsID, r.RdSpID, r.RdSdpID, r.RdDifferenceNote, r.RdTOUBoundariesConsistent,
		r.RdRTPInd, r.RdRTPStreamId, r.RdBrID, r.RdNmID, r.RdNetMeteringInd, r.RdTierAccumByTOUInd, r.RdCapResInd, r.RdCPEventUsageAdderInd,
		m.RmFuID, m.RmSvID, m.RmCtID, m.RmUnID, m.RmPrID, m.RmDPrID,
		m.RmClientRateID, m.RmRateCompanyID, m.RmDefault, m.RmChildrenInd, m.RmLtID, m.RmBdID, m.RmHgID,
		t.[Name], t.[Description], t.[Eligibility],
		p.PoID, p.PoCO2, p.PoSO2, p.PoNOx
	FROM
		(SELECT RateDefinition.RdRateMasterID,
		       MAX(RateDefinition.RdStartDate) As StartDate
		FROM RateDefinition
		WHERE (RateDefinition.RdEnableInd = 1) AND 
		      (RateDefinition.RdStartDate < CONVERT(DATETIME, @EndDate, 102))
		GROUP BY RdRateMasterID) x 
	INNER JOIN RateDefinition r ON x.RdRateMasterID = r.RdRateMasterID AND x.StartDate = r.RdStartDate
	INNER JOIN RateMaster m ON x.RdRateMasterID = m.RateMasterID
	INNER JOIN RateMasterText t ON m.RateMasterID = t.RateMasterID
	INNER JOIN Pollution p ON m.RmPoID = p.PoID
	WHERE m.RmRateCompanyID = @RateCompanyID 
		AND m.RmEnableInd = 1 
		AND t.LaID = @Language
		AND m.RmCtID = @CustomerType 
		AND m.RmFuID = @FuelType
		AND m.RmDefault >= @Default
		
		
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectRateClassListC]'
GO




-- SelectRateClassListC
create procedure [rateengine].[SelectRateClassListC] ( @ServiceTerritoryID varchar(32), @Language int ) AS
				
	SELECT RateDefID, r.RdRateMasterID, 
		r.RdStartDate, r.RdSeasonalInd, r.RdTieredInd, r.RdTimeOfUseInd, r.RdDemandInd,
		r.RdFuelAdjustmentInd, r.RdBasicInd,
		r.RdTsID, r.RdDsID, r.RdSpID, r.RdSdpID, r.RdDifferenceNote, r.RdTOUBoundariesConsistent,
		r.RdRTPInd, r.RdRTPStreamId, r.RdBrID, r.RdNmID, r.RdNetMeteringInd, r.RdTierAccumByTOUInd, r.RdCapResInd, r.RdCPEventUsageAdderInd,
		m.RmFuID, m.RmSvID, m.RmCtID, m.RmUnID, m.RmPrID, m.RmDPrID,
		m.RmClientRateID, m.RmRateCompanyID, m.RmDefault, m.RmChildrenInd, m.RmLtID, m.RmBdID, m.RmHgID,
		t.[Name], t.[Description], t.[Eligibility],
		p.PoID, p.PoCO2, p.PoSO2, p.PoNOx
	FROM
		(SELECT RateDefinition.RdRateMasterID,
		       MAX(RateDefinition.RdStartDate) As StartDate
		FROM RateDefinition
		WHERE (RateDefinition.RdEnableInd = 1) 
		GROUP BY RdRateMasterID) x 
	INNER JOIN RateDefinition r ON x.RdRateMasterID = r.RdRateMasterID AND x.StartDate = r.RdStartDate
	INNER JOIN RateMaster m ON x.RdRateMasterID = m.RateMasterID
	INNER JOIN RateMasterText t ON m.RateMasterID = t.RateMasterID
	INNER JOIN Pollution p ON m.RmPoID = p.PoID
	INNER JOIN RateMasterServiceTerritory s ON m.RateMasterID = s.RateMasterID
	WHERE s.CompanyID = @ServiceTerritoryID AND m.RmEnableInd = 1 AND t.LaID = @Language




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectRateClassListD]'
GO




-- SelectRateClassListD
create procedure [rateengine].[SelectRateClassListD] ( @ServiceTerritoryID varchar(32), @Language int, @CustomerType int, @FuelType int, @Default tinyint ) AS
				
	SELECT RateDefID, r.RdRateMasterID, 
		r.RdStartDate, r.RdSeasonalInd, r.RdTieredInd, r.RdTimeOfUseInd, r.RdDemandInd,
		r.RdFuelAdjustmentInd, r.RdBasicInd,
		r.RdTsID, r.RdDsID, r.RdSpID, r.RdSdpID, r.RdDifferenceNote, r.RdTOUBoundariesConsistent,
		r.RdRTPInd, r.RdRTPStreamId, r.RdBrID, r.RdNmID, r.RdNetMeteringInd, r.RdTierAccumByTOUInd, r.RdCapResInd, r.RdCPEventUsageAdderInd,
		m.RmFuID, m.RmSvID, m.RmCtID, m.RmUnID, m.RmPrID, m.RmDPrID,
		m.RmClientRateID, m.RmRateCompanyID, m.RmDefault, m.RmChildrenInd, m.RmLtID, m.RmBdID, m.RmHgID,
		t.[Name], t.[Description], t.[Eligibility],
		p.PoID, p.PoCO2, p.PoSO2, p.PoNOx
	FROM
		(SELECT RateDefinition.RdRateMasterID,
		       MAX(RateDefinition.RdStartDate) As StartDate
		FROM RateDefinition
		WHERE (RateDefinition.RdEnableInd = 1) 
		GROUP BY RdRateMasterID) x 
	INNER JOIN RateDefinition r ON x.RdRateMasterID = r.RdRateMasterID AND x.StartDate = r.RdStartDate
	INNER JOIN RateMaster m ON x.RdRateMasterID = m.RateMasterID
	INNER JOIN RateMasterText t ON m.RateMasterID = t.RateMasterID
	INNER JOIN Pollution p ON m.RmPoID = p.PoID
	INNER JOIN RateMasterServiceTerritory s ON m.RateMasterID = s.RateMasterID
	WHERE s.CompanyID = @ServiceTerritoryID 
		AND m.RmEnableInd = 1 
		AND t.LaID = @Language	 
		AND m.RmCtID = @CustomerType 
		AND m.RmFuID = @FuelType
		AND m.RmDefault >= @Default


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectRateClassListE]'
GO





-- SelectRateClassListE
create procedure [rateengine].[SelectRateClassListE] ( @RateCompanyID Int, @ServiceTerritoryID varchar(32), @Language int ) AS
				
	SELECT RateDefID, r.RdRateMasterID, 
		r.RdStartDate, r.RdSeasonalInd, r.RdTieredInd, r.RdTimeOfUseInd, r.RdDemandInd,
		r.RdFuelAdjustmentInd, r.RdBasicInd,
		r.RdTsID, r.RdDsID, r.RdSpID, r.RdSdpID, r.RdDifferenceNote, r.RdTOUBoundariesConsistent,
		r.RdRTPInd, r.RdRTPStreamId, r.RdBrID, r.RdNmID, r.RdNetMeteringInd, r.RdTierAccumByTOUInd, r.RdCapResInd, r.RdCPEventUsageAdderInd,
		m.RmFuID, m.RmSvID, m.RmCtID, m.RmUnID, m.RmPrID, m.RmDPrID,
		m.RmClientRateID, m.RmRateCompanyID, m.RmDefault, m.RmChildrenInd, m.RmLtID, m.RmBdID, m.RmHgID,
		t.[Name], t.[Description], t.[Eligibility],
		p.PoID, p.PoCO2, p.PoSO2, p.PoNOx
	FROM
		(SELECT RateDefinition.RdRateMasterID,
		       MAX(RateDefinition.RdStartDate) As StartDate
		FROM RateDefinition
		WHERE (RateDefinition.RdEnableInd = 1) 
		GROUP BY RdRateMasterID) x 
	INNER JOIN RateDefinition r ON x.RdRateMasterID = r.RdRateMasterID AND x.StartDate = r.RdStartDate
	INNER JOIN RateMaster m ON x.RdRateMasterID = m.RateMasterID
	INNER JOIN RateMasterText t ON m.RateMasterID = t.RateMasterID
	INNER JOIN RateMasterServiceTerritory s ON m.RateMasterID = s.RateMasterID
	INNER JOIN Pollution p ON m.RmPoID = p.PoID
	WHERE s.CompanyID = @ServiceTerritoryID AND m.RmRateCompanyID = @RateCompanyID AND m.RmEnableInd = 1 AND t.LaID = @Language
	
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectRateClassListF]'
GO


-- SelectRateClassListF
create procedure [rateengine].[SelectRateClassListF] ( @RateCompanyID Int, @ServiceTerritoryID varchar(32), @Language int, @CustomerType int, @FuelType int, @Default tinyint ) AS
				
	SELECT RateDefID, r.RdRateMasterID, 
		r.RdStartDate, r.RdSeasonalInd, r.RdTieredInd, r.RdTimeOfUseInd, r.RdDemandInd,
		r.RdFuelAdjustmentInd, r.RdBasicInd,
		r.RdTsID, r.RdDsID, r.RdSpID, r.RdSdpID, r.RdDifferenceNote, r.RdTOUBoundariesConsistent,
		r.RdRTPInd, r.RdRTPStreamId, r.RdBrID, r.RdNmID, r.RdNetMeteringInd, r.RdTierAccumByTOUInd, r.RdCapResInd, r.RdCPEventUsageAdderInd,
		m.RmFuID, m.RmSvID, m.RmCtID, m.RmUnID, m.RmPrID, m.RmDPrID,
		m.RmClientRateID, m.RmRateCompanyID, m.RmDefault, m.RmChildrenInd, m.RmLtID, m.RmBdID, m.RmHgID,
		t.[Name], t.[Description], t.[Eligibility],
		p.PoID, p.PoCO2, p.PoSO2, p.PoNOx
	FROM
		(SELECT RateDefinition.RdRateMasterID,
		       MAX(RateDefinition.RdStartDate) As StartDate
		FROM RateDefinition
		WHERE (RateDefinition.RdEnableInd = 1) 
		GROUP BY RdRateMasterID) x 
	INNER JOIN RateDefinition r ON x.RdRateMasterID = r.RdRateMasterID AND x.StartDate = r.RdStartDate
	INNER JOIN RateMaster m ON x.RdRateMasterID = m.RateMasterID
	INNER JOIN RateMasterText t ON m.RateMasterID = t.RateMasterID
	INNER JOIN Pollution p ON m.RmPoID = p.PoID
	INNER JOIN RateMasterServiceTerritory s ON m.RateMasterID = s.RateMasterID
	WHERE s.CompanyID = @ServiceTerritoryID 
		AND m.RmRateCompanyID = @RateCompanyID 
		AND m.RmEnableInd = 1 
		AND t.LaID = @Language
		AND m.RmCtID = @CustomerType 
		AND m.RmFuID = @FuelType	 
		AND m.RmDefault >= @Default


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[UseTypeMap]'
GO
CREATE TABLE [rateengine].[UseTypeMap]
(
[TierID] [tinyint] NOT NULL,
[TouID] [tinyint] NOT NULL,
[SeasonID] [tinyint] NOT NULL,
[UseName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LoadTypeID] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_UseTypeMap] on [rateengine].[UseTypeMap]'
GO
ALTER TABLE [rateengine].[UseTypeMap] ADD CONSTRAINT [PK_UseTypeMap] PRIMARY KEY CLUSTERED  ([TierID], [TouID], [SeasonID], [UseName])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[GetUseTypeMapByName]'
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

create procedure [rateengine].[GetUseTypeMapByName] ( @UseName varchar(32) ) AS

	SELECT TierID, TouID, SeasonID, UseName, LoadTypeID FROM UseTypeMap WHERE UseName = @UseName
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SelectMasterChildInformation]'
GO

create procedure [rateengine].[SelectMasterChildInformation] @RateMasterID int  AS

	SELECT  RateMasterChildren.RateMasterID, 
			RateMasterChildren.RateMasterIDchild, 
			RateMaster.RmClientRateID,
			RateMaster.RmLtID
	FROM RateMasterChildren INNER JOIN RateMaster ON RateMasterChildren.RateMasterIDchild = RateMaster.RateMasterID
	WHERE RateMasterChildren.RateMasterID = @RateMasterID
	ORDER BY RateMaster.RmClientRateID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthSelRateMasterRateClasses]'
GO

create procedure [rateengine].[AuthSelRateMasterRateClasses] @RateCompanyID int, @RateMasterID int  AS

	SELECT  RateMaster.RateMasterID, 
			RateMaster.RmClientRateID As RateClassName
	FROM RateMaster
	WHERE RateMaster.RmEnableInd = 1 AND
		  RateMaster.RMRateCompanyID = @RateCompanyID AND
		  RateMaster.RateMasterID != @RateMasterID AND 
		  RateMaster.RateMasterID NOT IN (
				SELECT RateMasterIDchild FROM RateMasterChildren WHERE RateMasterID = @RateMasterID )
	ORDER BY RateMaster.RmClientRateID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthSelMasterChildRateClasses]'
GO

create procedure [rateengine].[AuthSelMasterChildRateClasses] @RateMasterID int  AS

	SELECT  RateMasterChildren.RateMasterID, 
			RateMasterChildren.RateMasterIDchild, 
			RateMaster.RmClientRateID As RateClassName
	FROM RateMasterChildren INNER JOIN RateMaster ON RateMasterChildren.RateMasterIDchild = RateMaster.RateMasterID
	WHERE RateMasterChildren.RateMasterID = @RateMasterID
	ORDER BY RateMaster.RmClientRateID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[InsertFuelCostAdjustment]'
GO

create procedure [rateengine].[InsertFuelCostAdjustment] ( @referrerID int, @RateGroupName varchar(1000)) AS

declare @rateCompanyID int

/*
--check rate group exists
if(SELECT count([RateCompanyID])
      FROM [Hubstatic].[rateengine].[RateCompanyMap]
  where [SourceTypeID] = 1 and [SourceID] = @referrerID) > 0

  BEGIN
 set @rateCompanyID = (SELECT top 1 [RateCompanyID]
      FROM [Hubstatic].[rateengine].[RateCompanyMap]
  where [SourceTypeID] = 1 and [SourceID] = @referrerID)

  END

  ELSE
  BEGIN

  set @rateCompanyID = (SELECT max ([RateCompanyID])
      FROM [Hubstatic].[rateengine].[RateCompanyMap]
  where [SourceTypeID] = 1) + 1

  insert into [Hubstatic].[rateengine].[RateCompanyMap] ([SourceID],[SourceTypeID],[RateCompanyID],[SortOrder])
  VALUES(@referrerID,1,@rateCompanyID,@rateCompanyID)
  END
*/

--insert
insert into [rateengine].[FuelCostRecoveryGroup]([RgName],[RgRateCompanyID])
values(@RateGroupName, @rateCompanyID)





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthInsRateMasterChild]'
GO

create procedure [rateengine].[AuthInsRateMasterChild] @RateMasterID As int, @RateMasterIDchild int  AS

	INSERT INTO RateMasterChildren (RateMasterID, RateMasterIDchild)
	VALUES (@RateMasterID, @RateMasterIDchild)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[ServiceType]'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [rateengine].[ServiceType]
(
[SvID] [tinyint] NOT NULL,
[SvName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SvShortName] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SvSortOrder] [tinyint] NOT NULL CONSTRAINT [DF_ServiceType_SvSortOrder] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ServiceType] on [rateengine].[ServiceType]'
GO
ALTER TABLE [rateengine].[ServiceType] ADD CONSTRAINT [PK_ServiceType] PRIMARY KEY CLUSTERED  ([SvID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[Language]'
GO
CREATE TABLE [rateengine].[Language]
(
[LaID] [int] NOT NULL,
[LaPrefix] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LaName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LaSortOrder] [tinyint] NOT NULL CONSTRAINT [DF_Language_LaSortOrder] DEFAULT ((100))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Language] on [rateengine].[Language]'
GO
ALTER TABLE [rateengine].[Language] ADD CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED  ([LaID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[Fuel]'
GO
CREATE TABLE [rateengine].[Fuel]
(
[FuID] [tinyint] NOT NULL,
[FuName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FuShortName] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FuSortOrder] [tinyint] NOT NULL CONSTRAINT [DF_Fuel_FuSortOrder] DEFAULT ((100))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Fuel] on [rateengine].[Fuel]'
GO
ALTER TABLE [rateengine].[Fuel] ADD CONSTRAINT [PK_Fuel] PRIMARY KEY CLUSTERED  ([FuID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[DemandStructureType]'
GO
CREATE TABLE [rateengine].[DemandStructureType]
(
[DsID] [int] NOT NULL,
[DsName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DsDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DsSortOrder] [int] NOT NULL CONSTRAINT [DF_DemandStructureType_DsSortOrder] DEFAULT ((100))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DemandStructureType] on [rateengine].[DemandStructureType]'
GO
ALTER TABLE [rateengine].[DemandStructureType] ADD CONSTRAINT [PK_DemandStructureType] PRIMARY KEY CLUSTERED  ([DsID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[TierStructureType]'
GO
CREATE TABLE [rateengine].[TierStructureType]
(
[TsID] [int] NOT NULL,
[TsName] [varchar] (48) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TsDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TsSortOrder] [int] NOT NULL CONSTRAINT [DF_TierStructureType_TsSortOrder] DEFAULT ((100))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TierStructureType] on [rateengine].[TierStructureType]'
GO
ALTER TABLE [rateengine].[TierStructureType] ADD CONSTRAINT [PK_TierStructureType] PRIMARY KEY CLUSTERED  ([TsID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[ProrateType]'
GO
CREATE TABLE [rateengine].[ProrateType]
(
[PrID] [tinyint] NOT NULL CONSTRAINT [DF_ProrateType_PrID] DEFAULT ((1)),
[PrName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ProrateType] on [rateengine].[ProrateType]'
GO
ALTER TABLE [rateengine].[ProrateType] ADD CONSTRAINT [PK_ProrateType] PRIMARY KEY CLUSTERED  ([PrID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[BaselineRule]'
GO
CREATE TABLE [rateengine].[BaselineRule]
(
[BrID] [tinyint] NOT NULL,
[BrName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BrDescription] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BaselineRule] on [rateengine].[BaselineRule]'
GO
ALTER TABLE [rateengine].[BaselineRule] ADD CONSTRAINT [PK_BaselineRule] PRIMARY KEY CLUSTERED  ([BrID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[NetMeteringType]'
GO
CREATE TABLE [rateengine].[NetMeteringType]
(
[NmID] [tinyint] NOT NULL,
[NmName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NmDescription] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NmSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_NetMeteringType] on [rateengine].[NetMeteringType]'
GO
ALTER TABLE [rateengine].[NetMeteringType] ADD CONSTRAINT [PK_NetMeteringType] PRIMARY KEY CLUSTERED  ([NmID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SeasonalProrateType]'
GO
CREATE TABLE [rateengine].[SeasonalProrateType]
(
[SpID] [tinyint] NOT NULL,
[SpName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SpSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SeasonalProrateType] on [rateengine].[SeasonalProrateType]'
GO
ALTER TABLE [rateengine].[SeasonalProrateType] ADD CONSTRAINT [PK_SeasonalProrateType] PRIMARY KEY CLUSTERED  ([SpID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[BilledDemandType]'
GO
CREATE TABLE [rateengine].[BilledDemandType]
(
[BilledDemandTypeID] [tinyint] NOT NULL,
[BdName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BdSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BilledDemandType] on [rateengine].[BilledDemandType]'
GO
ALTER TABLE [rateengine].[BilledDemandType] ADD CONSTRAINT [PK_BilledDemandType] PRIMARY KEY CLUSTERED  ([BilledDemandTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[CustomerType]'
GO
CREATE TABLE [rateengine].[CustomerType]
(
[CtID] [tinyint] NOT NULL,
[CtName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CtShortName] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CtSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CustomerType] on [rateengine].[CustomerType]'
GO
ALTER TABLE [rateengine].[CustomerType] ADD CONSTRAINT [PK_CustomerType] PRIMARY KEY CLUSTERED  ([CtID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[LoadType]'
GO
CREATE TABLE [rateengine].[LoadType]
(
[LoadTypeID] [tinyint] NOT NULL,
[LtName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_LoadType] on [rateengine].[LoadType]'
GO
ALTER TABLE [rateengine].[LoadType] ADD CONSTRAINT [PK_LoadType] PRIMARY KEY CLUSTERED  ([LoadTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[DayType]'
GO
CREATE TABLE [rateengine].[DayType]
(
[DayTypeID] [tinyint] NOT NULL,
[DtName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DtSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DayType] on [rateengine].[DayType]'
GO
ALTER TABLE [rateengine].[DayType] ADD CONSTRAINT [PK_DayType] PRIMARY KEY CLUSTERED  ([DayTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[AuthAudit]'
GO
CREATE TABLE [rateengine].[AuthAudit]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[UserID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RateCompanyID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RateCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date] [datetime] NOT NULL,
[ChangeType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Action] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EffectiveDate] [date] NULL,
[ChangeDetails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AuthAudit] on [rateengine].[AuthAudit]'
GO
ALTER TABLE [rateengine].[AuthAudit] ADD CONSTRAINT [PK_AuthAudit] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[DemandProrateType]'
GO
CREATE TABLE [rateengine].[DemandProrateType]
(
[DPrID] [tinyint] NOT NULL,
[DPrName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DPrSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DemandProrateType] on [rateengine].[DemandProrateType]'
GO
ALTER TABLE [rateengine].[DemandProrateType] ADD CONSTRAINT [PK_DemandProrateType] PRIMARY KEY CLUSTERED  ([DPrID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[SeasonalDemandProrateType]'
GO
CREATE TABLE [rateengine].[SeasonalDemandProrateType]
(
[SdpID] [tinyint] NOT NULL,
[SdpName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SdpSortOrder] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SeasonalDemandProrateType] on [rateengine].[SeasonalDemandProrateType]'
GO
ALTER TABLE [rateengine].[SeasonalDemandProrateType] ADD CONSTRAINT [PK_SeasonalDemandProrateType] PRIMARY KEY CLUSTERED  ([SdpID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [rateengine].[tmpTOUExclude]'
GO
CREATE TABLE [rateengine].[tmpTOUExclude]
(
[Company] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Adding foreign keys to [rateengine].[UseCharge]'
GO
ALTER TABLE [rateengine].[UseCharge] WITH NOCHECK  ADD CONSTRAINT [FK_UseCharge_CostType] FOREIGN KEY ([CostTypeID]) REFERENCES [rateengine].[CostType] ([CostTypeID])
ALTER TABLE [rateengine].[UseCharge] WITH NOCHECK  ADD CONSTRAINT [FK_UseCharge_FuelCostRecoveryGroup] FOREIGN KEY ([RgID]) REFERENCES [rateengine].[FuelCostRecoveryGroup] ([RgID])
ALTER TABLE [rateengine].[UseCharge] WITH NOCHECK  ADD CONSTRAINT [FK_UseCharge_RateDefinition] FOREIGN KEY ([RateDefID]) REFERENCES [rateengine].[RateDefinition] ([RateDefID])
ALTER TABLE [rateengine].[UseCharge] WITH NOCHECK  ADD CONSTRAINT [FK_UseCharge_Season] FOREIGN KEY ([SeasonID]) REFERENCES [rateengine].[Season] ([SeasonID])
ALTER TABLE [rateengine].[UseCharge] WITH NOCHECK  ADD CONSTRAINT [FK_UseCharge_Tier] FOREIGN KEY ([TierID]) REFERENCES [rateengine].[Tier] ([TierID])
ALTER TABLE [rateengine].[UseCharge] WITH NOCHECK  ADD CONSTRAINT [FK_UseCharge_TimeOfUse] FOREIGN KEY ([TouID]) REFERENCES [rateengine].[TimeOfUse] ([TouID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[FuelCostRecovery]'
GO
ALTER TABLE [rateengine].[FuelCostRecovery] WITH NOCHECK  ADD CONSTRAINT [FK_FuelCostRecovery_FuelCostRecoveryGroup] FOREIGN KEY ([FrRgID]) REFERENCES [rateengine].[FuelCostRecoveryGroup] ([RgID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[RateMaster]'
GO
ALTER TABLE [rateengine].[RateMaster] WITH NOCHECK  ADD CONSTRAINT [FK_RateMaster_Fuel] FOREIGN KEY ([RmFuID]) REFERENCES [rateengine].[Fuel] ([FuID])
ALTER TABLE [rateengine].[RateMaster] WITH NOCHECK  ADD CONSTRAINT [FK_RateMaster_ProrateType] FOREIGN KEY ([RmPrID]) REFERENCES [rateengine].[ProrateType] ([PrID])
ALTER TABLE [rateengine].[RateMaster] WITH NOCHECK  ADD CONSTRAINT [FK_RateMaster_ServiceType] FOREIGN KEY ([RmSvID]) REFERENCES [rateengine].[ServiceType] ([SvID])
ALTER TABLE [rateengine].[RateMaster] WITH NOCHECK  ADD CONSTRAINT [FK_RateMaster_CustomerType] FOREIGN KEY ([RmCtID]) REFERENCES [rateengine].[CustomerType] ([CtID])
ALTER TABLE [rateengine].[RateMaster] WITH NOCHECK  ADD CONSTRAINT [FK_RateMaster_UnitOfMeasure] FOREIGN KEY ([RmUnID]) REFERENCES [rateengine].[UnitOfMeasure] ([UnID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[MinimumCharge]'
GO
ALTER TABLE [rateengine].[MinimumCharge] WITH NOCHECK  ADD CONSTRAINT [FK_MinimumCharge_RateDefinition] FOREIGN KEY ([RateDefID]) REFERENCES [rateengine].[RateDefinition] ([RateDefID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[RateMasterText]'
GO
ALTER TABLE [rateengine].[RateMasterText] WITH NOCHECK  ADD CONSTRAINT [FK_RateMasterText_Language] FOREIGN KEY ([LaID]) REFERENCES [rateengine].[Language] ([LaID])
ALTER TABLE [rateengine].[RateMasterText] WITH NOCHECK  ADD CONSTRAINT [FK_RateMasterText_RateMaster] FOREIGN KEY ([RateMasterID]) REFERENCES [rateengine].[RateMaster] ([RateMasterID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[RateMasterServiceTerritory]'
GO
ALTER TABLE [rateengine].[RateMasterServiceTerritory] WITH NOCHECK  ADD CONSTRAINT [FK_RateMasterServiceTerritory_RateMaster] FOREIGN KEY ([RateMasterID]) REFERENCES [rateengine].[RateMaster] ([RateMasterID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[RateDefinition]'
GO
ALTER TABLE [rateengine].[RateDefinition] WITH NOCHECK  ADD CONSTRAINT [FK_RateDefinition_RateMaster] FOREIGN KEY ([RdRateMasterID]) REFERENCES [rateengine].[RateMaster] ([RateMasterID])
ALTER TABLE [rateengine].[RateDefinition] WITH NOCHECK  ADD CONSTRAINT [FK_RateDefinition_TierStructureType] FOREIGN KEY ([RdTsID]) REFERENCES [rateengine].[TierStructureType] ([TsID])
ALTER TABLE [rateengine].[RateDefinition] WITH NOCHECK  ADD CONSTRAINT [FK_RateDefinition_DemandStructureType] FOREIGN KEY ([RdDsID]) REFERENCES [rateengine].[DemandStructureType] ([DsID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[Difference]'
GO
ALTER TABLE [rateengine].[Difference] WITH NOCHECK  ADD CONSTRAINT [FK_Difference_RateDefinition] FOREIGN KEY ([RateDefID]) REFERENCES [rateengine].[RateDefinition] ([RateDefID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[SeasonBoundary]'
GO
ALTER TABLE [rateengine].[SeasonBoundary] WITH NOCHECK  ADD CONSTRAINT [FK_SeasonBoundary_RateDefinition] FOREIGN KEY ([RateDefID]) REFERENCES [rateengine].[RateDefinition] ([RateDefID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[TaxCharge]'
GO
ALTER TABLE [rateengine].[TaxCharge] WITH NOCHECK  ADD CONSTRAINT [FK_TaxCharge_RateDefinition] FOREIGN KEY ([RateDefID]) REFERENCES [rateengine].[RateDefinition] ([RateDefID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[TierBoundary]'
GO
ALTER TABLE [rateengine].[TierBoundary] WITH NOCHECK  ADD CONSTRAINT [FK_TierBoundary_RateDefinition] FOREIGN KEY ([RateDefID]) REFERENCES [rateengine].[RateDefinition] ([RateDefID])
ALTER TABLE [rateengine].[TierBoundary] WITH NOCHECK  ADD CONSTRAINT [FK_TierBoundary_Season] FOREIGN KEY ([SeasonID]) REFERENCES [rateengine].[Season] ([SeasonID])
ALTER TABLE [rateengine].[TierBoundary] WITH NOCHECK  ADD CONSTRAINT [FK_TierBoundary_Tier] FOREIGN KEY ([TierID]) REFERENCES [rateengine].[Tier] ([TierID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[TimeOfUseBoundary]'
GO
ALTER TABLE [rateengine].[TimeOfUseBoundary] WITH NOCHECK  ADD CONSTRAINT [FK_TimeOfUseBoundary_RateDefinition] FOREIGN KEY ([RateDefID]) REFERENCES [rateengine].[RateDefinition] ([RateDefID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[MinimumCharge]'
GO
ALTER TABLE [rateengine].[MinimumCharge] ADD CONSTRAINT [FK_MinimumCharge_CostType] FOREIGN KEY ([CostTypeID]) REFERENCES [rateengine].[CostType] ([CostTypeID])
ALTER TABLE [rateengine].[MinimumCharge] ADD CONSTRAINT [FK_MinimumCharge_ChargeCalcType] FOREIGN KEY ([CalcTypeID]) REFERENCES [rateengine].[ChargeCalcType] ([CalcTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[ServiceCharge]'
GO
ALTER TABLE [rateengine].[ServiceCharge] ADD CONSTRAINT [FK_ServiceCharge_CostType] FOREIGN KEY ([CostTypeID]) REFERENCES [rateengine].[CostType] ([CostTypeID])
ALTER TABLE [rateengine].[ServiceCharge] ADD CONSTRAINT [FK_ServiceCharge_RebateClass] FOREIGN KEY ([RebateClassID]) REFERENCES [rateengine].[RebateClass] ([RebateClassID])
ALTER TABLE [rateengine].[ServiceCharge] ADD CONSTRAINT [FK_ServiceCharge_RateDefinition] FOREIGN KEY ([RateDefID]) REFERENCES [rateengine].[RateDefinition] ([RateDefID])
ALTER TABLE [rateengine].[ServiceCharge] ADD CONSTRAINT [FK_ServiceCharge_Season] FOREIGN KEY ([SeasonID]) REFERENCES [rateengine].[Season] ([SeasonID])
ALTER TABLE [rateengine].[ServiceCharge] ADD CONSTRAINT [FK_ServiceCharge_ChargeCalcType] FOREIGN KEY ([CalcTypeID]) REFERENCES [rateengine].[ChargeCalcType] ([CalcTypeID])
ALTER TABLE [rateengine].[ServiceCharge] ADD CONSTRAINT [FK_ServiceCharge_Step] FOREIGN KEY ([StepID]) REFERENCES [rateengine].[Step] ([StepID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[TaxCharge]'
GO
ALTER TABLE [rateengine].[TaxCharge] ADD CONSTRAINT [FK_TaxCharge_CostType] FOREIGN KEY ([CostTypeID]) REFERENCES [rateengine].[CostType] ([CostTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[RateDefinition]'
GO
ALTER TABLE [rateengine].[RateDefinition] ADD CONSTRAINT [FK_RateDefinition_BaselineRule] FOREIGN KEY ([RdBrID]) REFERENCES [rateengine].[BaselineRule] ([BrID])
ALTER TABLE [rateengine].[RateDefinition] ADD CONSTRAINT [FK_RateDefinition_NetMeteringType] FOREIGN KEY ([RdNmID]) REFERENCES [rateengine].[NetMeteringType] ([NmID])
ALTER TABLE [rateengine].[RateDefinition] ADD CONSTRAINT [FK_RateDefinition_SeasonalProrateType] FOREIGN KEY ([RdSpID]) REFERENCES [rateengine].[SeasonalProrateType] ([SpID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[RateMaster]'
GO
ALTER TABLE [rateengine].[RateMaster] ADD CONSTRAINT [FK_RateMaster_BilledDemandType] FOREIGN KEY ([RmBdID]) REFERENCES [rateengine].[BilledDemandType] ([BilledDemandTypeID])
ALTER TABLE [rateengine].[RateMaster] ADD CONSTRAINT [FK_RateMaster_HolidayGroup] FOREIGN KEY ([RmHgID]) REFERENCES [rateengine].[HolidayGroup] ([HgID])
ALTER TABLE [rateengine].[RateMaster] ADD CONSTRAINT [FK_RateMaster_LoadType] FOREIGN KEY ([RmLtID]) REFERENCES [rateengine].[LoadType] ([LoadTypeID])
ALTER TABLE [rateengine].[RateMaster] ADD CONSTRAINT [FK_RateMaster_Pollution] FOREIGN KEY ([RmPoID]) REFERENCES [rateengine].[Pollution] ([PoID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[Difference]'
GO
ALTER TABLE [rateengine].[Difference] ADD CONSTRAINT [FK_Difference_DifferenceType] FOREIGN KEY ([DfID]) REFERENCES [rateengine].[DifferenceType] ([DfID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[DifferenceType]'
GO
ALTER TABLE [rateengine].[DifferenceType] ADD CONSTRAINT [FK_DifferenceType_DifferenceCategory] FOREIGN KEY ([DfDcID]) REFERENCES [rateengine].[DifferenceCategory] ([DcID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[Holiday]'
GO
ALTER TABLE [rateengine].[Holiday] ADD CONSTRAINT [FK_Holiday_HolidayGroup] FOREIGN KEY ([HoHgID]) REFERENCES [rateengine].[HolidayGroup] ([HgID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[UnitOfMeasure]'
GO
ALTER TABLE [rateengine].[UnitOfMeasure] ADD CONSTRAINT [FK_UnitOfMeasure_Fuel] FOREIGN KEY ([UnFuID]) REFERENCES [rateengine].[Fuel] ([FuID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[UseCharge]'
GO
ALTER TABLE [rateengine].[UseCharge] ADD CONSTRAINT [FK_UseCharge_PartType] FOREIGN KEY ([PartTypeID]) REFERENCES [rateengine].[PartType] ([PartTypeID])
ALTER TABLE [rateengine].[UseCharge] ADD CONSTRAINT [FK_UseCharge_RTPGroup] FOREIGN KEY ([RTPgId]) REFERENCES [rateengine].[RTPGroup] ([RTPgId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[RateMasterChildren]'
GO
ALTER TABLE [rateengine].[RateMasterChildren] ADD CONSTRAINT [FK_RateMasterChildren_RateMaster] FOREIGN KEY ([RateMasterID]) REFERENCES [rateengine].[RateMaster] ([RateMasterID])
ALTER TABLE [rateengine].[RateMasterChildren] ADD CONSTRAINT [FK_RateMasterChildren_RateMaster1] FOREIGN KEY ([RateMasterIDchild]) REFERENCES [rateengine].[RateMaster] ([RateMasterID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[RTPCriticalPeak]'
GO
ALTER TABLE [rateengine].[RTPCriticalPeak] ADD CONSTRAINT [FK_RTPCriticalPeak] FOREIGN KEY ([RTPgId]) REFERENCES [rateengine].[RTPGroup] ([RTPgId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[RTPDayAhead]'
GO
ALTER TABLE [rateengine].[RTPDayAhead] ADD CONSTRAINT [FK_RTPDayAhead_RTPGroup] FOREIGN KEY ([RTPgId]) REFERENCES [rateengine].[RTPGroup] ([RTPgId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[RateCompanySetting]'
GO
ALTER TABLE [rateengine].[RateCompanySetting] ADD CONSTRAINT [FK_RateCompanySetting_Setting] FOREIGN KEY ([SettingID]) REFERENCES [rateengine].[Setting] ([SettingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[StepBoundary]'
GO
ALTER TABLE [rateengine].[StepBoundary] ADD CONSTRAINT [FK_StepBoundary_RateDefinition] FOREIGN KEY ([RateDefID]) REFERENCES [rateengine].[RateDefinition] ([RateDefID])
ALTER TABLE [rateengine].[StepBoundary] ADD CONSTRAINT [FK_StepBoundary_Season] FOREIGN KEY ([SeasonID]) REFERENCES [rateengine].[Season] ([SeasonID])
ALTER TABLE [rateengine].[StepBoundary] ADD CONSTRAINT [FK_StepBoundary_Step] FOREIGN KEY ([StepID]) REFERENCES [rateengine].[Step] ([StepID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[TerritoryBaseRateClass]'
GO
ALTER TABLE [rateengine].[TerritoryBaseRateClass] ADD CONSTRAINT [FK_TerritoryBaseRateClass_RateDefinition] FOREIGN KEY ([RateDefID]) REFERENCES [rateengine].[RateDefinition] ([RateDefID])
ALTER TABLE [rateengine].[TerritoryBaseRateClass] ADD CONSTRAINT [FK_BaseRateClassTerritory_LookupGroup] FOREIGN KEY ([LgID]) REFERENCES [rateengine].[TerritoryLookupGroup] ([LgID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[SeasonBoundary]'
GO
ALTER TABLE [rateengine].[SeasonBoundary] ADD CONSTRAINT [FK_SeasonBoundary_Season] FOREIGN KEY ([SeasonID]) REFERENCES [rateengine].[Season] ([SeasonID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[TerritoryLookup]'
GO
ALTER TABLE [rateengine].[TerritoryLookup] ADD CONSTRAINT [FK_TerritoryLookup_Season] FOREIGN KEY ([TlSeasonID]) REFERENCES [rateengine].[Season] ([SeasonID])
ALTER TABLE [rateengine].[TerritoryLookup] ADD CONSTRAINT [FK_TerritoryLookup_LookupGroup] FOREIGN KEY ([TlLgID]) REFERENCES [rateengine].[TerritoryLookupGroup] ([LgID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[TimeOfUseBoundary]'
GO
ALTER TABLE [rateengine].[TimeOfUseBoundary] ADD CONSTRAINT [FK_TimeOfUseBoundary_Season] FOREIGN KEY ([SeasonID]) REFERENCES [rateengine].[Season] ([SeasonID])
ALTER TABLE [rateengine].[TimeOfUseBoundary] ADD CONSTRAINT [FK_TimeOfUseBoundary_TimeOfUse] FOREIGN KEY ([TouID]) REFERENCES [rateengine].[TimeOfUse] ([TouID])
ALTER TABLE [rateengine].[TimeOfUseBoundary] ADD CONSTRAINT [FK_TimeOfUseBoundary_DayType] FOREIGN KEY ([DayTypeID]) REFERENCES [rateengine].[DayType] ([DayTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[UseTypeMap]'
GO
ALTER TABLE [rateengine].[UseTypeMap] ADD CONSTRAINT [FK_UseTypeMap_Season] FOREIGN KEY ([SeasonID]) REFERENCES [rateengine].[Season] ([SeasonID])
ALTER TABLE [rateengine].[UseTypeMap] ADD CONSTRAINT [FK_UseTypeMap_Tier] FOREIGN KEY ([TierID]) REFERENCES [rateengine].[Tier] ([TierID])
ALTER TABLE [rateengine].[UseTypeMap] ADD CONSTRAINT [FK_UseTypeMap_TimeOfUse] FOREIGN KEY ([TouID]) REFERENCES [rateengine].[TimeOfUse] ([TouID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[RTPFinal]'
GO
ALTER TABLE [rateengine].[RTPFinal] ADD CONSTRAINT [FK_RTPFinal_RTPGroup] FOREIGN KEY ([RTPgId]) REFERENCES [rateengine].[RTPGroup] ([RTPgId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[RTPPrelim]'
GO
ALTER TABLE [rateengine].[RTPPrelim] ADD CONSTRAINT [FK_RTPPrelim_RTPGroup] FOREIGN KEY ([RTPgId]) REFERENCES [rateengine].[RTPGroup] ([RTPgId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [rateengine].[TierBoundary]'
GO
ALTER TABLE [rateengine].[TierBoundary] ADD CONSTRAINT [FK_TierBoundary_TimeOfUse] FOREIGN KEY ([TouID]) REFERENCES [rateengine].[TimeOfUse] ([TouID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
