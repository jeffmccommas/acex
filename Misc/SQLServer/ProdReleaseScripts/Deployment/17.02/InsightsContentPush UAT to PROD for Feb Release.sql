/*
Run this script on:

acePsql1c0.database.windows.net.Insights    -  This database will be modified

to synchronize it with:

aceUsql1c0.database.windows.net.Insights

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 11.1.3 from Red Gate Software Ltd at 2/24/2017 3:22:29 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

PRINT(N'Drop constraints from [dbo].[UserRole]')
ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK_UserRole_Role]
ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK_UserRole_User]

PRINT(N'Drop constraints from [dbo].[UserEnvironment]')
ALTER TABLE [dbo].[UserEnvironment] DROP CONSTRAINT [FK_UserEnvironment_Environment]
ALTER TABLE [dbo].[UserEnvironment] DROP CONSTRAINT [FK_UserEnvironment_User]

PRINT(N'Drop constraints from [dbo].[UserEndpoint]')
ALTER TABLE [dbo].[UserEndpoint] DROP CONSTRAINT [FK_UserEndpoint_Endpoint]
ALTER TABLE [dbo].[UserEndpoint] DROP CONSTRAINT [FK_UserEndpoint_User]

PRINT(N'Drop constraints from [cp].[ControlPanelLoginClient]')
ALTER TABLE [cp].[ControlPanelLoginClient] DROP CONSTRAINT [FK_ControlPanelLoginClient_ControlPanelLogin]

PRINT(N'Drop constraints from [dbo].[User]')
ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_User_Client]

PRINT(N'Drop constraint FK_ClientProperty_Client from [dbo].[ClientProperty]')
ALTER TABLE [dbo].[ClientProperty] DROP CONSTRAINT [FK_ClientProperty_Client]

PRINT(N'Update rows in [cp].[ControlPanelLogin]')
UPDATE [cp].[ControlPanelLogin] SET [UpdDate]='2017-02-17 20:17:45.117' WHERE [CpID]=14
UPDATE [cp].[ControlPanelLogin] SET [EnableInd]=0, [UpdDate]='2017-02-20 14:07:56.447' WHERE [CpID]=19
UPDATE [cp].[ControlPanelLogin] SET [EnableInd]=0, [UpdDate]='2017-02-20 14:07:45.240' WHERE [CpID]=30
UPDATE [cp].[ControlPanelLogin] SET [EnableInd]=0, [UpdDate]='2017-02-20 14:08:11.933' WHERE [CpID]=33
UPDATE [cp].[ControlPanelLogin] SET [UpdDate]='2017-02-17 20:07:13.063' WHERE [CpID]=36
UPDATE [cp].[ControlPanelLogin] SET [UpdDate]='2017-02-17 20:07:28.367' WHERE [CpID]=44
UPDATE [cp].[ControlPanelLogin] SET [UpdDate]='2017-02-20 14:09:19.997' WHERE [CpID]=62
UPDATE [cp].[ControlPanelLogin] SET [UpdDate]='2017-01-24 19:12:33.467' WHERE [CpID]=63
PRINT(N'Operation applied to 8 rows out of 8')

PRINT(N'Add row to [dbo].[Client]')
INSERT INTO [dbo].[Client] ([ClientID], [Name], [Description], [RateCompanyID], [ReferrerID], [NewDate], [UpdDate], [AuthType], [EnableInd]) VALUES (174, 'Ameren', 'Ameren', 106, NULL, '2017-02-17 19:56:47.000', '2017-02-17 20:21:21.783', 1, 1)

PRINT(N'Add rows to [dbo].[User]')
SET IDENTITY_INSERT [dbo].[User] ON
INSERT INTO [dbo].[User] ([UserID], [ClientID], [ActorName], [CEAccessKeyID], [NewDate], [UpdDate], [EnableInd]) VALUES (41, 224, 'BIUILUser', 'bbbccb3ba49e634a67789412625959df5b73', '2017-02-13 20:28:20.557', '2017-02-13 20:48:48.937', 1)
INSERT INTO [dbo].[User] ([UserID], [ClientID], [ActorName], [CEAccessKeyID], [NewDate], [UpdDate], [EnableInd]) VALUES (42, 174, 'Web Users', '0edeff64fad50c4d773884401d7801cd20c4', '2017-02-17 19:59:11.197', '2017-02-17 20:29:03.290', 1)
SET IDENTITY_INSERT [dbo].[User] OFF
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Add rows to [cp].[ControlPanelLoginClient]')
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (14, 174)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (36, 174)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (44, 174)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (62, 61)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (62, 80)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (62, 174)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (62, 210)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (62, 224)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (62, 290)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 4)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 5)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 61)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 80)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 101)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 118)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 210)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 224)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 233)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 276)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 290)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 766)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 1234)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 6539)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 54654)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (63, 122469)
PRINT(N'Operation applied to 25 rows out of 25')

PRINT(N'Add rows to [dbo].[UserEndpoint]')
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (41, 1, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (41, 12, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (41, 15, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (41, 16, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (41, 17, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (41, 19, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 1, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 2, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 3, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 4, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 5, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 6, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 7, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 8, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 9, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 10, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 11, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 12, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 13, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 14, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 15, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 16, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 17, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 18, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 19, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 20, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 21, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 22, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 23, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (42, 24, 1)
PRINT(N'Operation applied to 30 rows out of 30')

PRINT(N'Add rows to [dbo].[UserEnvironment]')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (41, 0, 'DemoBasicKeyBIProd224', 'DemoBasicKeyBIProd224', '2017-02-13 20:30:34.217')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (41, 1, 'DemoBasicKeyBI224', 'DemoBasicKeyBI224', '2017-02-13 20:30:34.217')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (41, 2, 'DemoBasicKeyBI224', 'DemoBasicKeyBI224', '2017-02-13 20:30:34.217')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (41, 3, 'DemoBasicKeyBI224', 'DemoBasicKeyBI224', '2017-02-13 20:30:34.217')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (41, 4, 'DemoBasicKeyBI224', 'DemoBasicKeyBI224', '2017-02-13 20:30:34.217')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (42, 0, 'AmerenBasicKey174Prod', 'AmerenBasicKey174Prod', '2017-02-17 20:22:43.570')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (42, 1, 'AmerenBasicKey174', 'AmerenBasicKey174', '2017-02-17 20:22:43.570')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (42, 2, 'AmerenBasicKey174', 'AmerenBasicKey174', '2017-02-17 20:22:43.570')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (42, 3, 'AmerenBasicKey174', 'AmerenBasicKey174', '2017-02-17 20:22:43.570')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (42, 4, 'AmerenBasicKey174', 'AmerenBasicKey174', '2017-02-17 20:22:43.570')
PRINT(N'Operation applied to 10 rows out of 10')

PRINT(N'Add rows to [dbo].[UserRole]')
INSERT INTO [dbo].[UserRole] ([UserID], [RoleID], [EnableInd]) VALUES (41, 2, 1)
INSERT INTO [dbo].[UserRole] ([UserID], [RoleID], [EnableInd]) VALUES (42, 2, 1)
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Add constraints to [dbo].[UserRole]')
ALTER TABLE [dbo].[UserRole] ADD CONSTRAINT [FK_UserRole_Role] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role] ([RoleID])
ALTER TABLE [dbo].[UserRole] WITH NOCHECK ADD CONSTRAINT [FK_UserRole_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])

PRINT(N'Add constraints to [dbo].[UserEnvironment]')
ALTER TABLE [dbo].[UserEnvironment] ADD CONSTRAINT [FK_UserEnvironment_Environment] FOREIGN KEY ([EnvID]) REFERENCES [dbo].[Environment] ([EnvID])
ALTER TABLE [dbo].[UserEnvironment] WITH NOCHECK ADD CONSTRAINT [FK_UserEnvironment_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])

PRINT(N'Add constraints to [dbo].[UserEndpoint]')
ALTER TABLE [dbo].[UserEndpoint] WITH NOCHECK ADD CONSTRAINT [FK_UserEndpoint_Endpoint] FOREIGN KEY ([EndpointID]) REFERENCES [dbo].[Endpoint] ([EndpointID])
ALTER TABLE [dbo].[UserEndpoint] WITH NOCHECK ADD CONSTRAINT [FK_UserEndpoint_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])

PRINT(N'Add constraints to [cp].[ControlPanelLoginClient]')
ALTER TABLE [cp].[ControlPanelLoginClient] ADD CONSTRAINT [FK_ControlPanelLoginClient_ControlPanelLogin] FOREIGN KEY ([CpID]) REFERENCES [cp].[ControlPanelLogin] ([CpID])

PRINT(N'Add constraints to [dbo].[User]')
ALTER TABLE [dbo].[User] WITH NOCHECK ADD CONSTRAINT [FK_User_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])

PRINT(N'Add constraint FK_ClientProperty_Client to [dbo].[ClientProperty]')
ALTER TABLE [dbo].[ClientProperty] WITH NOCHECK ADD CONSTRAINT [FK_ClientProperty_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
COMMIT TRANSACTION
GO
