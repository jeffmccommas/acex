﻿/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  View [Holding].[v_Billing]    Script Date: 1/10/2017 12:45:09 PM ******/
DROP VIEW [Holding].[v_Billing]
GO

/****** Object:  View [Holding].[v_Billing]    Script Date: 1/10/2017 12:45:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Jason Khourie
-- Create date: 613/2014
-- =============================================

CREATE VIEW [Holding].[v_Billing]
AS
    SELECT  b.[ClientId] ,
            b.[CustomerId] ,
            b.[AccountId] ,
            b.[PremiseId] ,
            b.[ServiceContractId] ,
            b.[ServicePointId] ,
            b.[MeterId] ,
            b.[RateClass] ,
            b.[BillStartDate] AS StartDate ,
            b.[BillEndDate] AS EndDate ,
            b.[BillDays] ,
            b.[TotalUnits] ,
            b.[TotalCost] ,
            b.[CommodityId] ,
            b.[BillPeriodTypeId] ,
            b.[BillCycleScheduleId] ,
            b.[UOMId] ,
            b.[DueDate] ,
            b.[ReadDate] ,
            b.[ReadQuality] ,
            b.[SourceId] ,
            b.TrackingId ,
            b.TrackingDate,
            b.[BillingCostDetailsKey],
            b.[ServiceCostDetailsKey]
    FROM    Holding.Billing b WITH ( NOLOCK )
            INNER JOIN Holding.Client cl ON cl.ClientId = b.ClientId;
GO
