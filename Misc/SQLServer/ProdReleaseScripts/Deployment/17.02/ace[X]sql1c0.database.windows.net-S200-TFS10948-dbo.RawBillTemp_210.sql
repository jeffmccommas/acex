﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_210 DATABASE SHARD ***/
/********************************************************/

/****** Object:  Table [dbo].[RawBillTemp_210]    Script Date: 12/15/2016 12:57:10 PM ******/
DROP TABLE [dbo].[RawBillTemp_210]
GO

/****** Object:  Table [dbo].[RawBillTemp_210]    Script Date: 12/15/2016 12:57:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RawBillTemp_210](
    [customer_id] [NVARCHAR](255) NULL,
    [premise_id] [NVARCHAR](255) NULL,
    [mail_address_line_1] [NVARCHAR](255) NULL,
    [mail_address_line_2] [NVARCHAR](255) NULL,
    [mail_address_line_3] [NVARCHAR](255) NULL,
    [mail_city] [NVARCHAR](255) NULL,
    [mail_state] [NVARCHAR](255) NULL,
    [mail_zip_code] [NVARCHAR](255) NULL,
    [first_name] [NVARCHAR](255) NULL,
    [last_name] [NVARCHAR](255) NULL,
    [phone_1] [NVARCHAR](255) NULL,
    [phone_2] [NVARCHAR](255) NULL,
    [email] [NVARCHAR](255) NULL,
    [customer_type] [NVARCHAR](255) NULL,
    [account_id] [NVARCHAR](255) NULL,
    [active_date] [NVARCHAR](255) NULL,
    [inactive_date] [NVARCHAR](255) NULL,
    [read_cycle] [NVARCHAR](255) NULL,
    [rate_code] [NVARCHAR](255) NULL,
    [service_point_id] [NVARCHAR](255) NULL,
    [service_house_number] [NVARCHAR](255) NULL,
    [service_street_name] [NVARCHAR](255) NULL,
    [service_unit] [NVARCHAR](255) NULL,
    [service_city] [NVARCHAR](255) NULL,
    [service_state] [NVARCHAR](255) NULL,
    [service_zip_code] [NVARCHAR](255) NULL,
    [meter_type] [NVARCHAR](255) NULL,
    [meter_units] [NVARCHAR](255) NULL,
    [usage_value] [NVARCHAR](255) NULL,
    [bill_enddate] [NVARCHAR](255) NULL,
    [bill_days] [NVARCHAR](255) NULL,
    [is_estimate] [NVARCHAR](255) NULL,
    [usage_charge] [NVARCHAR](255) NULL,
    [clientId] [INT] NOT NULL,
    [programs] [NVARCHAR](MAX) NULL,
    [service_commodity] [NVARCHAR](255) NULL,
    [account_structure_type] [NVARCHAR](255) NULL,
    [meter_id] [NVARCHAR](255) NULL,
    [meter_replaces_meterid] [NVARCHAR](255) NULL,
    [service_read_date] [NVARCHAR](255) NULL,
    [basic_charge] [NVARCHAR](255) NULL,
    [bill_taxes] [NVARCHAR](255) NULL,
    [demand_unit] [NVARCHAR](255) NULL,
    [demand_quantity] [NVARCHAR](255) NULL,
    [demand_charge] [NVARCHAR](255) NULL
)

GO


