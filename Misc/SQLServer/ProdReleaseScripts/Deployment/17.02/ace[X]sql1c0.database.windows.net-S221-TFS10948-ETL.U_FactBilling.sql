﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[U_FactBilling]    Script Date: 1/13/2017 8:00:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 7/2/2014
-- Description:
--
-- 10/27/2016 -- Phil Victor -- Added IsFault
-- 01/10/2017 -- Phil Victor -- Added CostDetailsKeys
-- =============================================
ALTER PROCEDURE [ETL].[U_FactBilling] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @TrackingDate DATETIME;
        SET @TrackingDate = GETUTCDATE();

        UPDATE [dbo].[FactServicePointBilling]
        SET     TotalUsage = b.TotalUnits ,
                CostOfUsage = b.TotalCost ,
                BillDays = b.BillDays ,
                RateClassKey1 = drc.RateClassKey ,
                ReadQuality = b.ReadQuality ,
                ReadDate = b.ReadDate ,
                DueDate = b.DueDate ,
                UpdateDate = @TrackingDate ,
                TrackingId = b.TrackingId ,
                SourceKey = ds.SourceKey ,
                SourceId = b.SourceId ,
                TrackingDate = b.TrackingDate,
                IsFault = b.IsFault
        FROM [ETL].[T1U_FactBilling] b WITH ( NOLOCK )
                INNER JOIN [dbo].[FactServicePointBilling] fb WITH ( NOLOCK ) ON fb.ServiceContractKey = b.ServiceContractKey
                                                              AND fb.BillPeriodStartDateKey = b.BillPeriodStartDateKey
                                                              AND fb.BillPeriodEndDateKey = b.BillPeriodEndDateKey
                INNER JOIN [dbo].[DimSource] ds ON ds.SourceId = b.SourceId
                LEFT JOIN [dbo].[DimRateClass] drc ON drc.ClientId = @ClientID
                                                  AND drc.RateClassName = b.RateClass
        WHERE   b.ClientID = @ClientID;

        /* Update BillCostDetails - can only do value */
        UPDATE [dbo].[FactServicePointBilling_BillCostDetails]
        SET [BillingCostDetailValue] = b.[BillingCostDetailValue]
        FROM [ETL].[T1U_FactBilling_BillCostDetails] b WITH ( NOLOCK )
            INNER JOIN [dbo].[FactServicePointBilling_BillCostDetails] fb WITH ( NOLOCK ) ON fb.[BillingCostDetailsKey] = b.[BillingCostDetailsKey]
                    AND fb.[BillingCostDetailName] = b.[BillingCostDetailName]

        /* Update ServiceCostDetails - can only do value */
        UPDATE [dbo].[FactServicePointBilling_ServiceCostDetails]
        SET    [ServiceCostDetailValue] = b.[ServiceCostDetailValue]
        FROM [ETL].[T1U_FactBilling_ServiceCostDetails] b WITH ( NOLOCK )
            INNER JOIN [dbo].[FactServicePointBilling_ServiceCostDetails] fb WITH ( NOLOCK ) ON fb.[ServiceCostDetailsKey] = b.[ServiceCostDetailsKey]
                AND fb.[ServiceCostDetailName] = b.[ServiceCostDetailName]

    END;





