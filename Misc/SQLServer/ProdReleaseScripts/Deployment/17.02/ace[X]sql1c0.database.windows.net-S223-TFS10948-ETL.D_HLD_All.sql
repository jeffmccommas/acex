﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[D_HLD_All]    Script Date: 1/13/2017 10:15:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 8/8/2014
-- Description:
--
-- Phil Victor - 01/12/2017 - Added CostDetails
-- =============================================
ALTER PROCEDURE [ETL].[D_HLD_All]
                 @ClientID INT
AS

BEGIN

    SET NOCOUNT ON;

    TRUNCATE TABLE [Holding].[Billing]
    TRUNCATE TABLE [Holding].[Billing_BillCostDetails]
    TRUNCATE TABLE [Holding].[Billing_ServiceCostDetails]

    TRUNCATE TABLE Holding.Customer
    TRUNCATE TABLE Holding.[Events]
    TRUNCATE TABLE Holding.Premise
    TRUNCATE TABLE Holding.PremiseAttributes
    TRUNCATE TABLE Holding.ActionItem

END


