﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[S_Billing]    Script Date: 1/10/2017 12:53:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 7/21/2014
-- Description:
--
-- 10/28/2016 -- Phil Victor -- Added RateClassKey
-- 01/10/2017 -- Phil Victor -- Added CostDetailsKey
-- =============================================
ALTER PROCEDURE [ETL].[S_Billing]
                @ClientID INT
AS

BEGIN

        SET NOCOUNT ON;


        SELECT  ClientId ,
                AccountId ,
                PremiseId ,
                ServiceContractId ,
                ServicePointId ,
                MeterId ,
                StartDate ,
                EndDate ,
                BillDays ,
                TotalUnits ,
                TotalCost ,
                CommodityId ,
                BillPeriodTypeId ,
                BillCycleScheduleId ,
                UOMId ,
                RateClass ,
                RateClassKey,
                DueDate ,
                ReadDate ,
                ReadQuality ,
                SourceId ,
                TrackingId ,
                TrackingDate,
                [BillingCostDetailsKey],
                [ServiceCostDetailsKey]
        FROM    ( SELECT    hb.ClientId ,
                            AccountId ,
                            PremiseId ,
                            ServiceContractId ,
                            ServicePointId ,
                            MeterId ,
                            StartDate ,
                            EndDate ,
                            BillDays ,
                            TotalUnits ,
                            TotalCost ,
                            CommodityId ,
                            BillPeriodTypeId ,
                            BillCycleScheduleId ,
                            UOMId ,
                            RateClass ,
                            drc.RateClassKey,
                            DueDate ,
                            ReadDate ,
                            ReadQuality ,
                            SourceId ,
                            TrackingId ,
                            TrackingDate,
                            [BillingCostDetailsKey],
                            [ServiceCostDetailsKey],
                            ROW_NUMBER() OVER ( PARTITION BY hb.ClientId,
                                                AccountId, PremiseId,
                                                ServiceContractId, StartDate,
                                                EndDate, CommodityId ORDER BY TrackingDate DESC ) AS SortId
                  FROM Holding.v_Billing hb
                  LEFT JOIN dbo.DimRateClass drc ON drc.ClientId = hb.ClientId
                                                  AND drc.RateClassName = hb.RateClass
                  WHERE  hb.ClientId = @ClientID
                ) b
        WHERE   SortId = 1
        ORDER BY ClientId ,
                PremiseId ,
                AccountId ,
                ServiceContractId ,
                StartDate ,
                EndDate ,
                CommodityId;


    END;
