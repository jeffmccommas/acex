﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [Holding].[InsertBilling_BillCostDetailData]    Script Date: 1/9/2017 11:04:18 AM ******/
DROP PROCEDURE [Holding].[InsertBilling_BillCostDetailData]
GO
/****** Object:  UserDefinedTableType [Holding].[Billing_BillCostDetailsTableType]    Script Date: 1/9/2017 11:04:18 AM ******/
DROP TYPE [Holding].[Billing_BillCostDetailsTableType]
GO

/****** Object:  UserDefinedTableType [Holding].[Billing_BillCostDetailsTableType]    Script Date: 1/9/2017 11:04:18 AM ******/
CREATE TYPE [Holding].[Billing_BillCostDetailsTableType] AS TABLE(
            [BillingCostDetailsKey] [uniqueidentifier] NOT NULL,
            [BillingCostDetailName] [VARCHAR](MAX) NOT NULL,
            [BillingCostDetailValue] [VARCHAR](MAX) NOT NULL
)
GO

/****** Object:  StoredProcedure [Holding].[InsertBilling_BillCostDetailData]    Script Date: 1/9/2017 11:04:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [Holding].[InsertBilling_BillCostDetailData]
    @Tbl AS [Holding].[Billing_BillCostDetailsTableType] READONLY
AS /**********************************************************************************************************
* SP Name:
*       [Holding].[InsertBilling_BillCostDetailData]
* Parameters:
*       [BillingCostDetailsTableType]
* Purpose:  This stored procedure insertsrows into the billing_billcostdetails holding tables
* ---------------------------------------------------------------------------------------------------------------------------
* Author:  Philip Victor - 1/9/2017
**********************************************************************************************************/
    BEGIN
        SET NOCOUNT ON;
        DECLARE @servicescount INT;

        INSERT INTO [Holding].[Billing_BillCostDetails]
                   ([BillingCostDetailsKey]
                   ,[BillingCostDetailName]
                   ,[BillingCostDetailValue])
                SELECT  [BillingCostDetailsKey] ,
                        [BillingCostDetailName] ,
                        [BillingCostDetailValue]
                FROM    @Tbl;





        SELECT  @@Rowcount;
    END; --proc

GO