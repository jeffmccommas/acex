﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[S_FactBilling]    Script Date: 1/13/2017 2:13:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 7/21/2014
-- Description:
--
-- 10/27/2016 -- Phil Victor -- Added RateClassKey
-- 01/12/2017 -- Phil Victor -- Added CostDetailKeys
-- =============================================
ALTER PROCEDURE [ETL].[S_FactBilling] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT  fspb.ServiceContractKey ,
                BillPeriodStartDateKey ,
                BillPeriodEndDateKey ,
                fspb.ClientId ,
                fspb.PremiseId ,
                fspb.AccountId ,
                IIF(dsc.IsInferred = 1, '', dsc.ServiceContractId) AS ServiceContractId ,
                fspb.StartDate ,
                fspb.EndDate ,
                fspb.CommodityId ,
                fspb.TotalUsage ,
                fspb.CostOfUsage ,
                fspb.SourceKey ,
                fspb.BillDays,
                fspb.RateClassKey1 AS RateClassKey,
                IIF(fspb.[BillingCostDetailsKey] IS NULL , kfb.[BillingCostDetailsKey], fspb.[BillingCostDetailsKey]) AS BillingCostDetailKey,
                IIF(fspb.[ServiceCostDetailsKey] IS NULL , kfb.[ServiceCostDetailsKey], fspb.[ServiceCostDetailsKey]) AS ServiceCostDetailsKey
        FROM    ETL.KEY_FactBilling kfb WITH ( NOLOCK )
                INNER JOIN dbo.DimPremise p WITH ( NOLOCK ) ON p.ClientId = kfb.ClientId
                                                              AND p.AccountId = kfb.AccountId
                                                              AND p.PremiseId = kfb.PremiseId
                INNER JOIN dbo.DimServiceContract dsc WITH ( NOLOCK ) ON dsc.ClientId = kfb.ClientId
                                                              AND dsc.PremiseKey = p.PremiseKey
                                                              AND kfb.CommodityId = dsc.CommodityKey
                                                              AND kfb.ServiceContractId = dsc.ServiceContractId
                INNER JOIN dbo.FactServicePointBilling fspb WITH ( NOLOCK ) ON fspb.ClientId = kfb.ClientId
                                                              AND fspb.ServiceContractKey = dsc.ServiceContractKey
                                                              AND kfb.StartDate = fspb.StartDate
                                                              AND kfb.EndDate = fspb.EndDate
        WHERE   kfb.ClientId = @ClientID
        ORDER BY fspb.ClientId ,
                fspb.PremiseId ,
                fspb.AccountId ,
                dsc.ServiceContractId ,
                fspb.StartDate ,
                fspb.EndDate ,
                fspb.CommodityId;

    END;
