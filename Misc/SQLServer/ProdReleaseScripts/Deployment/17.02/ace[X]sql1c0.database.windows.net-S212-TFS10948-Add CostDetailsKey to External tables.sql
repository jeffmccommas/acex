﻿
/********************************************************/
/*** RUN ONLY ON InsightsDW database ***/
/********************************************************/

/*** TO Alter (i.e. add a column) an External table it must be dropped and recreated ***/
IF EXISTS (SELECT * from sys.external_tables  WHERE name LIKE 'FactServicePointBilling')
BEGIN
    DROP EXTERNAL TABLE [dbo].[FactServicePointBilling]
END

CREATE EXTERNAL TABLE [dbo].[FactServicePointBilling](
        [ServiceContractKey] [int] NOT NULL,
        [BillPeriodStartDateKey] [int] NOT NULL,
        [BillPeriodEndDateKey] [int] NOT NULL,
        [UOMKey] [int] NOT NULL,
        [BillPeriodTypeKey] [int] NOT NULL,
        [ClientId] [int] NOT NULL,
        [PremiseId] [varchar](50) NOT NULL,
        [AccountId] [varchar](50) NOT NULL,
        [CommodityId] [int] NOT NULL,
        [BillPeriodTypeId] [int] NOT NULL,
        [UOMId] [int] NOT NULL,
        [StartDate] [datetime] NOT NULL,
        [EndDate] [datetime] NOT NULL,
        [TotalUsage] [decimal](18, 2) NOT NULL,
        [CostOfUsage] [decimal](18, 2) NOT NULL,
        [OtherCost] [decimal](18, 2) NULL,
        [RateClassKey1] [int] NULL,
        [RateClassKey2] [int] NULL,
        [NextReadDate] [date] NULL,
        [ReadDate] [datetime] NULL,
        [CreateDate] [datetime] NULL,
        [UpdateDate] [datetime] NULL,
        [BillCycleScheduleId] [varchar](50) NULL,
        [BillDays] [int] NOT NULL,
        [ETL_LogId] [int] NOT NULL,
        [TrackingId] [varchar](50) NOT NULL,
        [TrackingDate] [datetime] NOT NULL,
        [SourceKey] [int] NOT NULL,
        [SourceId] [int] NOT NULL,
        [ReadQuality] [varchar](50) NULL,
        [DueDate] [date] NULL,
        [IsFault] [bit] NOT NULL,
        [BillingCostDetailsKey] [uniqueidentifier] NULL,
        [ServiceCostDetailsKey] [uniqueidentifier] NULL
    )WITH(DATA_SOURCE = InsightsDWElasticDBQueryDataSrc,DISTRIBUTION=SHARDED(ClientID))

    /*** Check to make sure the External table created successfully ***/
    SELECT   e.object_id , '[' + s.name + '].[' + e.name + ']' AS TableName, *
    FROM     sys.external_tables e
    INNER JOIN sys.schemas s ON s.schema_id = e.schema_id
    WHERE e.name LIKE 'FactServicePointBilling'

    --SELECT * FROM [sys].[external_data_sources]

/*****************************************************************************************/

    IF EXISTS (SELECT * from sys.external_tables  WHERE name LIKE 'INV_FactBilling')
    BEGIN
        DROP EXTERNAL TABLE [ETL].[INV_FactBilling]
    END

    CREATE EXTERNAL TABLE [ETL].[INV_FactBilling](
            [ClientId] [INT] NULL,
            [PremiseId] [VARCHAR](50) NULL,
            [AccountId] [VARCHAR](50) NULL,
            [ServiceContractId] [VARCHAR](64) NULL,
            [ServicePointId] [VARCHAR](64) NULL,
            [MeterId] [VARCHAR](64) NULL,
            [CustomerId] [VARCHAR](50) NULL,
            [CommodityId] [INT] NULL,
            [BillPeriodTypeId] [INT] NULL,
            [UOMId] [INT] NULL,
            [SourceID] [INT] NULL,
            [StartDate] [DATETIME] NULL,
            [EndDate] [DATETIME] NULL,
            [TotalUnits] [DECIMAL](18, 2) NULL,
            [TotalCost] [DECIMAL](18, 2) NULL,
            [BillDays] [INT] NULL,
            [ETL_LogId] [INT] NOT NULL,
            [TrackingId] [VARCHAR](50) NOT NULL,
            [TrackingDate] [DATETIME] NOT NULL ,
            [BillingCostDetailsKey] [uniqueidentifier] NULL,
            [ServiceCostDetailsKey] [uniqueidentifier] NULL
            )WITH(DATA_SOURCE = InsightsDWElasticDBQueryDataSrc,DISTRIBUTION=SHARDED(ClientID))

    /*** Check to make sure the External table created successfully ***/
    SELECT   e.object_id , '[' + s.name + '].[' + e.name + ']' AS TableName, *
    FROM     sys.external_tables e
    INNER JOIN sys.schemas s ON s.schema_id = e.schema_id
    WHERE e.name LIKE 'INV_FactBilling'

    --SELECT * FROM [sys].[external_data_sources]

    /*****************************************************************************************/

     IF EXISTS (SELECT e.object_id , '[' + s.name + '].[' + e.name + ']' AS TableName, *
                    FROM sys.external_tables e
                    INNER JOIN sys.schemas s ON s.schema_id = e.schema_id
                    WHERE e.name LIKE 'INV_FactBilling')
    BEGIN
        DROP EXTERNAL TABLE [ETL].[INS_FactBilling]
    END

    CREATE EXTERNAL TABLE [ETL].[INS_FactBilling](
            [ClientId] [INT] NOT NULL,
            [PremiseId] [VARCHAR](50) NOT NULL,
            [AccountId] [VARCHAR](50) NOT NULL,
            [ServiceContractId] [VARCHAR](64) NOT NULL,
            [StartDate] [DATETIME] NOT NULL,
            [EndDate] [DATETIME] NOT NULL,
            [CommodityId] [INT] NOT NULL,
            [BillPeriodTypeId] [INT] NOT NULL,
            [BillCycleScheduleId] [VARCHAR](50) NULL,
            [UOMId] [INT] NOT NULL,
            [SourceId] [INT] NOT NULL,
            [TotalUnits] [DECIMAL](18, 2) NOT NULL,
            [TotalCost] [DECIMAL](18, 2) NOT NULL,
            [BillDays] [INT] NOT NULL,
            [RateClass] [VARCHAR](256) NULL,
            [ReadQuality] [VARCHAR](50) NULL,
            [ReadDate] [DATETIME] NULL,
            [DueDate] [DATE] NULL,
            [ETL_LogId] [INT] NOT NULL,
            [TrackingId] [VARCHAR](50) NOT NULL,
            [TrackingDate] [DATETIME] NOT NULL,
            [BillingCostDetailsKey] [uniqueidentifier] NULL,
            [ServiceCostDetailsKey] [uniqueidentifier] NULL
            )WITH(DATA_SOURCE = InsightsDWElasticDBQueryDataSrc,DISTRIBUTION=SHARDED(ClientID))

    /*** Check to make sure the External table created successfully ***/
    SELECT   e.object_id , '[' + s.name + '].[' + e.name + ']' AS TableName, *
    FROM     sys.external_tables e
    INNER JOIN sys.schemas s ON s.schema_id = e.schema_id
    WHERE e.name LIKE 'INS_FactBilling'

    /*****************************************************************************************/

        IF EXISTS (SELECT * from sys.external_tables  WHERE name LIKE 'KEY_FactBilling')
    BEGIN
        DROP EXTERNAL TABLE [ETL].[KEY_FactBilling]
    END

    CREATE EXTERNAL TABLE [ETL].[KEY_FactBilling](
            [ClientId] [INT] NOT NULL,
            [PremiseId] [VARCHAR](50) NOT NULL,
            [AccountId] [VARCHAR](50) NOT NULL,
            [CustomerId] [VARCHAR](50) NOT NULL,
            [ServiceContractId] [VARCHAR](64) NOT NULL,
            [ServicePointId] [VARCHAR](64) NULL,
            [MeterId] [VARCHAR](64) NULL,
            [MeterType] [VARCHAR](64) NULL,
            [ReplacedMeterId] [VARCHAR](64) NULL,
            [RateClass] [VARCHAR](256) NULL,
            [CommodityId] [INT] NOT NULL,
            [AMIStartDate] [DATETIME] NULL,
            [AMIEndDate] [DATETIME] NULL,
            [StartDate] [DATETIME] NOT NULL,
            [EndDate] [DATETIME] NOT NULL,
            [SourceId] [INT] NOT NULL,
            [TrackingId] [VARCHAR](50) NOT NULL,
            [TrackingDate] [DATETIME] NOT NULL,
            [BillingCostDetailsKey] [uniqueidentifier] NULL,
            [ServiceCostDetailsKey] [uniqueidentifier] NULL
            )WITH(DATA_SOURCE = InsightsDWElasticDBQueryDataSrc,DISTRIBUTION=SHARDED(ClientID))

    /*** Check to make sure the External table created successfully ***/
    SELECT   e.object_id , '[' + s.name + '].[' + e.name + ']' AS TableName, *
    FROM     sys.external_tables e
    INNER JOIN sys.schemas s ON s.schema_id = e.schema_id
    WHERE e.name LIKE 'KEY_FactBilling'

    --SELECT * FROM [sys].[external_data_sources]

    /*****************************************************************************************/

    IF EXISTS (SELECT * from sys.external_tables  WHERE name LIKE 'T1U_FactBilling')
    BEGIN
        DROP EXTERNAL TABLE [ETL].[T1U_FactBilling]
    END

    CREATE EXTERNAL TABLE [ETL].[T1U_FactBilling](
            [ServiceContractKey] [INT] NOT NULL,
            [BillPeriodStartDateKey] [INT] NOT NULL,
            [BillPeriodEndDateKey] [INT] NOT NULL,
            [CommodityKey] [INT] NOT NULL,
            [SourceId] [INT] NOT NULL,
            [ClientID] [INT] NOT NULL,
            [TotalUnits] [DECIMAL](18, 2) NOT NULL,
            [TotalCost] [DECIMAL](18, 2) NOT NULL,
            [BillDays] [INT] NOT NULL,
            [RateClass] [VARCHAR](256) NULL,
            [ReadQuality] [VARCHAR](50) NULL,
            [ReadDate] [DATETIME] NULL,
            [DueDate] [DATE] NULL,
            [ETL_LogId] [INT] NOT NULL,
            [TrackingId] [VARCHAR](50) NOT NULL,
            [TrackingDate] [DATETIME] NOT NULL,
            [IsFault] [BIT] NOT NULL,
            [BillingCostDetailsKey] [uniqueidentifier] NULL,
            [ServiceCostDetailsKey] [uniqueidentifier] NULL
            )WITH(DATA_SOURCE = InsightsDWElasticDBQueryDataSrc,DISTRIBUTION=SHARDED(ClientID))

    /*** Check to make sure the External table created successfully ***/
    SELECT   e.object_id , '[' + s.name + '].[' + e.name + ']' AS TableName, *
    FROM     sys.external_tables e
    INNER JOIN sys.schemas s ON s.schema_id = e.schema_id
    WHERE e.name LIKE 'T1U_FactBilling'

    --SELECT * FROM [sys].[external_data_sources]

   /*****************************************************************************************/
    IF EXISTS (SELECT * from sys.external_tables  WHERE name LIKE 'FactServicePointBilling_BillCostDetails')
    BEGIN
        DROP EXTERNAL TABLE [dbo].[FactServicePointBilling_BillCostDetails]
    END

    CREATE EXTERNAL TABLE [dbo].[FactServicePointBilling_BillCostDetails](
            [BillingCostDetailsKey] [UNIQUEIDENTIFIER] NOT NULL,
            [BillingCostDetailName] [VARCHAR](MAX) NOT NULL,
            [BillingCostDetailValue] [VARCHAR](MAX) NOT NULL
            )WITH(DATA_SOURCE = InsightsDWElasticDBQueryDataSrc,DISTRIBUTION=SHARDED(BillingCostDetailsKey))

    /*** Check to make sure the External table created successfully ***/
    SELECT   e.object_id , '[' + s.name + '].[' + e.name + ']' AS TableName, *
    FROM     sys.external_tables e
    INNER JOIN sys.schemas s ON s.schema_id = e.schema_id
    WHERE e.name LIKE 'FactServicePointBilling_BillCostDetails'

    --SELECT * FROM [sys].[external_data_sources]

    /*****************************************************************************************/
    IF EXISTS (SELECT * from sys.external_tables  WHERE name LIKE 'FactServicePointBilling_ServiceCostDetails')
    BEGIN
        DROP EXTERNAL TABLE [dbo].[FactServicePointBilling_ServiceCostDetails]
    END

    CREATE EXTERNAL TABLE [dbo].[FactServicePointBilling_ServiceCostDetails](
                [ServiceCostDetailsKey] [UNIQUEIDENTIFIER] NOT NULL,
                [ServiceCostDetailName] [VARCHAR](MAX) NOT NULL,
                [ServiceCostDetailValue] [VARCHAR](MAX) NOT NULL
            )WITH(DATA_SOURCE = InsightsDWElasticDBQueryDataSrc,DISTRIBUTION=SHARDED(ServiceCostDetailsKey))

    /*** Check to make sure the External table created successfully ***/
    SELECT   e.object_id , '[' + s.name + '].[' + e.name + ']' AS TableName, *
    FROM     sys.external_tables e
    INNER JOIN sys.schemas s ON s.schema_id = e.schema_id
    WHERE e.name LIKE 'FactServicePointBilling_ServiceCostDetails'

    SELECT * FROM [sys].[external_data_sources]