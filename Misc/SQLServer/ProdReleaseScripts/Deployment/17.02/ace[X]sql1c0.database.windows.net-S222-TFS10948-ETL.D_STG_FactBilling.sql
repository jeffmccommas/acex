﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[D_STG_FactBilling]    Script Date: 1/13/2017 9:22:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 7/16/2014
-- Description:
--
-- Phil Victor - 01/12/2017 - Added CostDetails.
-- =============================================
ALTER PROCEDURE [ETL].[D_STG_FactBilling]
                 @ClientID INT
AS

BEGIN

    SET NOCOUNT ON;

    TRUNCATE TABLE ETL.INS_FactBilling
    TRUNCATE TABLE ETL.[INS_FactBilling_BillCostDetails]
    TRUNCATE TABLE ETL.[INS_FactBilling_ServiceCostDetails]

    TRUNCATE TABLE ETL.T1U_FactBilling
    TRUNCATE TABLE ETL.[T1U_FactBilling_BillCostDetails]
    TRUNCATE TABLE ETL.[T1U_FactBilling_ServiceCostDetails]

END