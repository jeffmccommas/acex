﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [Holding].[InsertBilling_ServiceCostDetailData]    Script Date: 1/9/2017 11:04:18 AM ******/
DROP PROCEDURE [Holding].[InsertBilling_ServiceCostDetailData]
GO
/****** Object:  UserDefinedTableType [Holding].[Billing_ServiceCostDetailsTableType]    Script Date: 1/9/2017 11:04:18 AM ******/
DROP TYPE [Holding].[Billing_ServiceCostDetailsTableType]
GO

/****** Object:  UserDefinedTableType [Holding].[Billing_ServiceCostDetailsTableType]    Script Date: 1/9/2017 11:04:18 AM ******/
CREATE TYPE [Holding].[Billing_ServiceCostDetailsTableType] AS TABLE(
        [ServiceCostDetailsKey] [uniqueidentifier] NOT NULL,
        [ServiceCostDetailName] [VARCHAR](MAX) NOT NULL,
        [ServiceCostDetailValue] [VARCHAR](MAX) NOT NULL
)
GO

/****** Object:  StoredProcedure [Holding].[InsertBilling_ServiceCostDetailData]    Script Date: 1/9/2017 11:04:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [Holding].[InsertBilling_ServiceCostDetailData]
    @Tbl AS [Holding].[Billing_ServiceCostDetailsTableType] READONLY
AS /**********************************************************************************************************
* SP Name:
*       [Holding].[InsertBilling_ServiceCostDetailData]
* Parameters:
*       [Billing_ServiceCostDetailsTableType]
* Purpose:  This stored procedure insertsrows into the billing_servicecostdetails holding tables
* ---------------------------------------------------------------------------------------------------------------------------
* Author:  Philip Victor - 1/9/2017
**********************************************************************************************************/
    BEGIN
        SET NOCOUNT ON;
        DECLARE @servicescount INT;

        INSERT INTO [Holding].[Billing_ServiceCostDetails]
                   ([ServiceCostDetailsKey]
                   ,[ServiceCostDetailName]
                   ,[ServiceCostDetailValue])
                SELECT  [ServiceCostDetailsKey] ,
                        [ServiceCostDetailName] ,
                        [ServiceCostDetailValue]
                FROM    @Tbl;


        SELECT  @@Rowcount;
    END; --proc

GO
