﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

-- =============================================
-- Author:      <Author,,Name>
-- Create date: <Create Date,,>
-- Description: <Description,,>
--
--- 10/27/2016 - Phil Victor -- Added IsFault Check and RateClassKey check
--- 01/11/2017 - Phil Victor -- Added CostDetailsKey
-- =============================================
ALTER PROCEDURE [ETL].[DFT_Billing]
    @ClientId AS INT ,
    @EtlLogId AS INT
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #tblSrcBilling
            (
              ClientId INT ,
              AccountId VARCHAR(50) ,
              PremiseId VARCHAR(50) ,
              ServiceContractId VARCHAR(50) ,
              ServicePointId VARCHAR(50) ,
              MeterId VARCHAR(50) ,
              StartDate DATETIME ,
              EndDate DATETIME ,
              BillDays INT ,
              TotalUnits DECIMAL(18, 2) ,
              TotalCost DECIMAL(18, 2) ,
              CommodityId INT ,
              BillPeriodTypeId INT ,
              BillCycleScheduleId VARCHAR(50) ,
              UOMId INT ,
              RateClass VARCHAR(50) ,
              RateClassKey INT ,
              DueDate DATETIME ,
              ReadDate DATETIME ,
              ReadQuality VARCHAR(50) ,
              SourceId INT ,
              TrackingId VARCHAR(50) ,
              TrackingDate DATETIME ,
              BillingCostDetailsKey UNIQUEIDENTIFIER ,
              ServiceCostDetailsKey UNIQUEIDENTIFIER
            );

        CREATE TABLE #tblSrcFactBilling
            (
              ServiceContractKey INT ,
              BillPeriodStartDateKey INT ,
              BillPeriodEndDateKey INT ,
              ClientId INT ,
              PremiseId VARCHAR(50) ,
              AccountId VARCHAR(50) ,
              ServiceContractId VARCHAR(50) ,
              StartDate DATETIME ,
              EndDate DATETIME ,
              CommodityId INT ,
              TotalUsage DECIMAL(18, 2) ,
              CostOfUsage DECIMAL(18, 2) ,
              SourceKey INT ,
              BillDays INT ,
              RateClassKey INT ,
              BillingCostDetailsKey UNIQUEIDENTIFIER ,
              ServiceCostDetailsKey UNIQUEIDENTIFIER
            );

        /* Insert data into Temp tables */
        INSERT  INTO #tblSrcBilling
                EXEC [ETL].[S_Billing] @ClientID = @ClientId;  /** Holding Table contents **/

        INSERT  INTO #tblSrcFactBilling
                EXEC [ETL].[S_FactBilling] @ClientID = @ClientId;    /** Current database contents **/

        /** Handle inserts **/
        INSERT  INTO ETL.INS_FactBilling
                SELECT  @ClientId ,
                        b.PremiseId ,
                        b.AccountId ,
                        b.ServiceContractId ,
                        b.StartDate ,
                        b.EndDate ,
                        b.CommodityId ,
                        b.BillPeriodTypeId ,
                        b.BillCycleScheduleId ,
                        b.UOMId ,
                        b.SourceId ,
                        b.TotalUnits ,
                        b.TotalCost ,
                        b.BillDays ,
                        b.RateClass ,
                        b.ReadQuality ,
                        b.ReadDate ,
                        b.DueDate ,
                        @EtlLogId ,
                        b.TrackingId ,
                        b.TrackingDate ,
                        b.[BillingCostDetailsKey] ,
                        b.[ServiceCostDetailsKey]
                FROM    #tblSrcBilling b
                        LEFT JOIN #tblSrcFactBilling fb ON fb.ClientId = b.ClientId
                                                           AND fb.PremiseId = b.PremiseId
                                                           AND fb.AccountId = b.AccountId
                                                           AND fb.ServiceContractId = b.ServiceContractId
                                                           AND fb.StartDate = b.StartDate
                                                           AND fb.EndDate = b.EndDate
                                                           AND fb.CommodityId = b.CommodityId
                WHERE   fb.ServiceContractKey IS NULL;

        /* Handle Updates */
        INSERT  INTO ETL.T1U_FactBilling
                SELECT  fb.ServiceContractKey ,
                        fb.BillPeriodStartDateKey ,
                        fb.BillPeriodEndDateKey ,
                        b.CommodityId ,
                        b.SourceId ,
                        @ClientId ,
                        b.TotalUnits ,
                        b.TotalCost ,
                        b.BillDays ,
                        b.RateClass ,
                        b.ReadQuality ,
                        b.ReadDate ,
                        b.DueDate ,
                        @EtlLogId ,
                        b.TrackingId ,
                        b.TrackingDate ,
                        IIF(b.RateClassKey != fb.RateClassKey, 1, 0) AS IsFault ,
                        fb.BillingCostDetailsKey ,
                        fb.ServiceCostDetailsKey
                FROM    #tblSrcBilling b
                        INNER JOIN #tblSrcFactBilling fb ON fb.ClientId = b.ClientId
                                                            AND fb.PremiseId = b.PremiseId
                                                            AND fb.AccountId = b.AccountId
                                                            AND fb.ServiceContractId = b.ServiceContractId
                                                            AND fb.StartDate = b.StartDate
                                                            AND fb.EndDate = b.EndDate
                                                            AND fb.CommodityId = b.CommodityId
                        LEFT JOIN dbo.DimRateClass drc WITH ( NOLOCK ) ON drc.ClientId = b.ClientId
                                                              AND drc.RateClassName = b.RateClass
                WHERE   ( ISNULL(b.TotalUnits, 0) != ISNULL(fb.TotalUsage, 0)
                          OR ISNULL(b.TotalCost, 0) != ISNULL(fb.CostOfUsage,
                                                              0)
                          OR ISNULL(b.BillDays, 0) != ISNULL(fb.BillDays, 0)
                          OR ISNULL(b.RateClassKey, 0) != ISNULL(fb.RateClassKey,
                                                              0)
                        );

/******************************************************************************************************************************/
 ----   /* Handle New BillCostDetails Insert */
        INSERT  INTO [ETL].[INS_FactBilling_BillCostDetails]
                SELECT  fb.[BillingCostDetailsKey] ,
                        bcd.[BillingCostDetailName] ,
                        bcd.[BillingCostDetailValue]
                FROM    [ETL].[INS_FactBilling] fb WITH ( NOLOCK )
                        INNER JOIN [Holding].[Billing_BillCostDetails] bcd
                        WITH ( NOLOCK ) ON fb.[BillingCostDetailsKey] = bcd.[BillingCostDetailsKey];

 ----   /* Handle New  ServiceCostDetails Insert */
        INSERT  INTO ETL.INS_FactBilling_ServiceCostDetails
                SELECT  fb.[ServiceCostDetailsKey] ,
                        scd.[ServiceCostDetailName] ,
                        scd.[ServiceCostDetailValue]
                FROM    [ETL].[INS_FactBilling] fb WITH ( NOLOCK )
                        INNER JOIN [Holding].[Billing_ServiceCostDetails] scd
                        WITH ( NOLOCK ) ON fb.[ServiceCostDetailsKey] = scd.[ServiceCostDetailsKey];

 ----    /* Update the CostDetailKeys to ones already present in the database */
        UPDATE  bcd
        SET     bcd.BillingCostDetailsKey = fspb.[BillingCostDetailsKey]
        FROM    [Holding].[Billing_BillCostDetails] bcd
                INNER JOIN [ETL].[KEY_FactBilling] kfb ON [kfb].[BillingCostDetailsKey] = [bcd].[BillingCostDetailsKey]
                INNER JOIN [dbo].[DimPremise] p WITH ( NOLOCK ) ON p.ClientId = kfb.ClientId
                                                              AND p.AccountId = kfb.AccountId
                                                              AND p.PremiseId = kfb.PremiseId
                INNER JOIN [dbo].[DimServiceContract] dsc WITH ( NOLOCK ) ON dsc.ClientId = kfb.ClientId
                                                              AND dsc.PremiseKey = p.PremiseKey
                                                              AND kfb.CommodityId = dsc.CommodityKey
                                                              AND kfb.ServiceContractId = dsc.ServiceContractId
                INNER JOIN [dbo].[FactServicePointBilling] fspb WITH ( NOLOCK ) ON fspb.ClientId = kfb.ClientId
                                                              AND fspb.ServiceContractKey = dsc.ServiceContractKey
                                                              AND kfb.StartDate = fspb.StartDate
                                                              AND kfb.EndDate = fspb.EndDate
        WHERE   fspb.[BillingCostDetailsKey] IS NOT NULL
                AND fspb.[BillingCostDetailsKey] != kfb.[BillingCostDetailsKey];

        UPDATE  scd
        SET     [scd].[ServiceCostDetailsKey] = [fspb].[ServiceCostDetailsKey]
        FROM    [Holding].[Billing_ServiceCostDetails] scd
                INNER JOIN [ETL].[KEY_FactBilling] kfb ON [kfb].[ServiceCostDetailsKey] = [scd].[ServiceCostDetailsKey]
                INNER JOIN [dbo].[DimPremise] p WITH ( NOLOCK ) ON p.ClientId = kfb.ClientId
                                                              AND p.AccountId = kfb.AccountId
                                                              AND p.PremiseId = kfb.PremiseId
                INNER JOIN [dbo].[DimServiceContract] dsc WITH ( NOLOCK ) ON dsc.ClientId = kfb.ClientId
                                                              AND dsc.PremiseKey = p.PremiseKey
                                                              AND kfb.CommodityId = dsc.CommodityKey
                                                              AND kfb.ServiceContractId = dsc.ServiceContractId
                INNER JOIN [dbo].[FactServicePointBilling] fspb WITH ( NOLOCK ) ON fspb.ClientId = kfb.ClientId
                                                              AND fspb.ServiceContractKey = dsc.ServiceContractKey
                                                              AND kfb.StartDate = fspb.StartDate
                                                              AND kfb.EndDate = fspb.EndDate
        WHERE   fspb.[ServiceCostDetailsKey] IS NOT NULL
                AND fspb.[ServiceCostDetailsKey] != kfb.[ServiceCostDetailsKey];

        ----    /* If a new BillCostDetail has been added */
        INSERT  INTO [ETL].[INS_FactBilling_BillCostDetails]
                SELECT  bcd.[BillingCostDetailsKey] ,
                        bcd.[BillingCostDetailName] ,
                        bcd.[BillingCostDetailValue]
                FROM    [Holding].[Billing_BillCostDetails] bcd WITH ( NOLOCK )
                WHERE   bcd.[BillingCostDetailName] NOT IN (
                        SELECT  fbcd.[BillingCostDetailName]
                        FROM    [Holding].[Billing_BillCostDetails] fb WITH ( NOLOCK )
                                INNER JOIN [dbo].[FactServicePointBilling_BillCostDetails] fbcd
                                WITH ( NOLOCK ) ON fb.[BillingCostDetailsKey] = fbcd.[BillingCostDetailsKey] )
                        AND NOT EXISTS ( SELECT 1
                                         FROM   [ETL].[INS_FactBilling_BillCostDetails] ifb
                                                INNER JOIN [Holding].[Billing_BillCostDetails] hbcd ON ifb.[BillingCostDetailsKey] = hbcd.[BillingCostDetailsKey]
                                                              AND ifb.[BillingCostDetailName] = hbcd.[BillingCostDetailName] );

            /* If a new ServiceCostDetail has been added */
        INSERT  INTO ETL.[INS_FactBilling_ServiceCostDetails]
                SELECT  scd.[ServiceCostDetailsKey] ,
                        scd.[ServiceCostDetailName] ,
                        scd.[ServiceCostDetailValue]
                FROM    [Holding].[Billing_ServiceCostDetails] scd WITH ( NOLOCK )
                WHERE   scd.[ServiceCostDetailName] NOT IN (
                        SELECT  fscd.[ServiceCostDetailName]
                        FROM    [Holding].[Billing_ServiceCostDetails] fb WITH ( NOLOCK )
                                INNER JOIN [dbo].[FactServicePointBilling_ServiceCostDetails] fscd
                                WITH ( NOLOCK ) ON fb.[ServiceCostDetailsKey] = fscd.[ServiceCostDetailsKey] )
                        AND NOT EXISTS ( SELECT 1
                                         FROM   [ETL].[INS_FactBilling_ServiceCostDetails] ifsb
                                                INNER JOIN [Holding].[Billing_ServiceCostDetails] sbcd ON ifsb.[ServiceCostDetailsKey] = sbcd.[ServiceCostDetailsKey]
                                                              AND ifsb.[ServiceCostDetailName] = sbcd.[ServiceCostDetailName] );

        -- /* If the value changed in the BillCostDetails */
        INSERT  INTO ETL.T1U_FactBilling_BillCostDetails
                SELECT  hbcd.[BillingCostDetailsKey] ,
                        hbcd.[BillingCostDetailName] ,
                        hbcd.[BillingCostDetailValue]
                FROM    [dbo].[FactServicePointBilling_BillCostDetails] fbcd
                        WITH ( NOLOCK )
                        INNER JOIN [Holding].[Billing_BillCostDetails] hbcd
                        WITH ( NOLOCK ) ON fbcd.[BillingCostDetailsKey] = hbcd.[BillingCostDetailsKey]
                WHERE   fbcd.[BillingCostDetailName] = hbcd.[BillingCostDetailName]
                        AND [fbcd].[BillingCostDetailValue] != hbcd.[BillingCostDetailValue];

        -- /* If the value changed in the ServiceCostDetails */
        INSERT  INTO ETL.T1U_FactBilling_ServiceCostDetails
                SELECT  hscd.[ServiceCostDetailsKey] ,
                        hscd.[ServiceCostDetailName] ,
                        hscd.[ServiceCostDetailValue]
                FROM    [dbo].[FactServicePointBilling_ServiceCostDetails] fscd
                        WITH ( NOLOCK )
                        INNER JOIN [Holding].[Billing_ServiceCostDetails] hscd
                        WITH ( NOLOCK ) ON fscd.[ServiceCostDetailsKey] = hscd.[ServiceCostDetailsKey]
                WHERE   fscd.[ServiceCostDetailName] = hscd.[ServiceCostDetailName]
                        AND fscd.[ServiceCostDetailValue] != hscd.[ServiceCostDetailValue];


        DROP    TABLE #tblSrcBilling;
        DROP     TABLE #tblSrcFactBilling;

    END;
