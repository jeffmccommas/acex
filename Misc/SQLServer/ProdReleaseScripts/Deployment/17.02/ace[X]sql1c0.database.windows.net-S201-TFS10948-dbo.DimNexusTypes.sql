﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  Table [dbo].[DimNexusTypes]    Script Date: 2/12/2017 2:37:56 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DimNexusTypes]') AND type in (N'U'))
DROP TABLE [dbo].[DimNexusTypes]
GO

/****** Object:  Table [dbo].[DimNexusTypes]    Script Date: 2/12/2017 2:37:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DimNexusTypes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DimNexusTypes](
    [NexusTypeKey] [int] IDENTITY(1,1) NOT NULL,
    [NexusType] [nvarchar](50) NOT NULL,
    [NexusTypeDesc] [nvarchar](max) NULL,
    [NexusTypeCat] [nvarchar](max) NOT NULL,
    [Validation] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_NexusTypeKey] PRIMARY KEY CLUSTERED
(
    [NexusTypeKey] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO

SET IDENTITY_INSERT [dbo].[DimNexusTypes] ON
GO

INSERT [dbo].[DimNexusTypes] ([NexusTypeKey], [NexusType], [NexusTypeDesc], [NexusTypeCat], [Validation]) VALUES (1, N'TOTAL', N'Total usage', N'Measurement', N'decimal')
GO
INSERT [dbo].[DimNexusTypes] ([NexusTypeKey], [NexusType], [NexusTypeDesc], [NexusTypeCat], [Validation]) VALUES (2, N'TOTAL_UOM', N'UOM for the total usage', N'Measurement', N'string')
GO
INSERT [dbo].[DimNexusTypes] ([NexusTypeKey], [NexusType], [NexusTypeDesc], [NexusTypeCat], [Validation]) VALUES (3, N'TOTAL_TIMESTAMP', N'Timestamp for the total usage', N'Measurement', N'DateTime')
GO
INSERT [dbo].[DimNexusTypes] ([NexusTypeKey], [NexusType], [NexusTypeDesc], [NexusTypeCat], [Validation]) VALUES (4, N'MAX_DMND', N'Demand value', N'Measurement', N'decimal')
GO
INSERT [dbo].[DimNexusTypes] ([NexusTypeKey], [NexusType], [NexusTypeDesc], [NexusTypeCat], [Validation]) VALUES (5, N'MAX_DMND_TIMESTAMP', N'Timestamp for the demand value', N'Measurement', N'DateTime')
GO
INSERT [dbo].[DimNexusTypes] ([NexusTypeKey], [NexusType], [NexusTypeDesc], [NexusTypeCat], [Validation]) VALUES (6, N'MAX_DMND_UOM', N'UOM for the demand value', N'Measurement', N'string')
GO
INSERT [dbo].[DimNexusTypes] ([NexusTypeKey], [NexusType], [NexusTypeDesc], [NexusTypeCat], [Validation]) VALUES (7, N'STD_CUST', N'Customer charge', N'Charge', N'decimal')
GO
INSERT [dbo].[DimNexusTypes] ([NexusTypeKey], [NexusType], [NexusTypeDesc], [NexusTypeCat], [Validation]) VALUES (8, N'GOVT_TAX_MUNI', N'Taxes', N'Charge', N'decimal')
GO
INSERT [dbo].[DimNexusTypes] ([NexusTypeKey], [NexusType], [NexusTypeDesc], [NexusTypeCat], [Validation]) VALUES (9, N'STD_MAX_DMND', N'Demand charge', N'Charge', N'decimal')
GO
INSERT [dbo].[DimNexusTypes] ([NexusTypeKey], [NexusType], [NexusTypeDesc], [NexusTypeCat], [Validation]) VALUES (10, N'STD_ENGY', N'Energy charge', N'Charge', N'decimal')
GO



SET IDENTITY_INSERT [dbo].[DimNexusTypes] OFF
GO
