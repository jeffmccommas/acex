﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

IF EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[FactServicePointBilling]') AND name = 'FactServicePointBillingKey')
BEGIN
    ALTER TABLE [dbo].[FactServicePointBilling] DROP COLUMN  FactServicePointBillingKey
END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[FactServicePointBilling]') AND name = 'BillingCostDetailsKey')
BEGIN
    ALTER TABLE [dbo].[FactServicePointBilling] ADD BillingCostDetailsKey uniqueidentifier NULL;
END

IF NOT EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[FactServicePointBilling]') AND name = 'ServiceCostDetailsKey')
BEGIN
    ALTER TABLE [dbo].[FactServicePointBilling] ADD ServiceCostDetailsKey uniqueidentifier NULL;
END

/********************************************************************************************************************/

IF EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[ETL].[INV_FactBilling]') AND name = 'CostDetailsKey')
BEGIN
    ALTER TABLE [ETL].[INS_FactBilling] DROP COLUMN  CostDetailsKey;
END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[ETL].[INV_FactBilling]') AND name = 'BillingCostDetailsKey')
BEGIN
    ALTER TABLE [ETL].[INV_FactBilling] ADD BillingCostDetailsKey uniqueidentifier NULL;
END

IF NOT EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[ETL].[INV_FactBilling]') AND name = 'ServiceCostDetailsKey')
BEGIN
    ALTER TABLE [ETL].[INV_FactBilling] ADD ServiceCostDetailsKey uniqueidentifier NULL;
END

/********************************************************************************************************************/

IF EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[ETL].[INS_FactBilling]') AND name = 'CostDetailsKey')
BEGIN
    ALTER TABLE [ETL].[INS_FactBilling] DROP COLUMN  CostDetailsKey;
END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[ETL].[INS_FactBilling]') AND name = 'BillingCostDetailsKey')
BEGIN
    ALTER TABLE [ETL].[INS_FactBilling] ADD BillingCostDetailsKey uniqueidentifier NULL;
END

IF NOT EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[ETL].[INS_FactBilling]') AND name = 'ServiceCostDetailsKey')
BEGIN
    ALTER TABLE [ETL].[INS_FactBilling] ADD ServiceCostDetailsKey uniqueidentifier NULL;
END

/********************************************************************************************************************/

IF EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND name = 'CostDetailsKey')
BEGIN
    ALTER TABLE [ETL].[KEY_FactBilling] DROP COLUMN CostDetailsKey;
END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND name = 'BillingCostDetailsKey')
BEGIN
    ALTER TABLE [ETL].[KEY_FactBilling] ADD BillingCostDetailsKey uniqueidentifier NULL;
END

IF NOT EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND name = 'ServiceCostDetailsKey')
BEGIN
    ALTER TABLE [ETL].[KEY_FactBilling] ADD ServiceCostDetailsKey uniqueidentifier NULL;
END

/********************************************************************************************************************/

IF EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[ETL].[T1U_FactBilling]') AND name = 'CostDetailsKey')
BEGIN
    ALTER TABLE [ETL].[T1U_FactBilling] DROP COLUMN CostDetailsKey;
END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[ETL].[T1U_FactBilling]') AND name = 'BillingCostDetailsKey')
BEGIN
    ALTER TABLE [ETL].[T1U_FactBilling] ADD BillingCostDetailsKey uniqueidentifier NULL;
END

IF NOT EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[ETL].[T1U_FactBilling]') AND name = 'ServiceCostDetailsKey')
BEGIN
    ALTER TABLE [ETL].[T1U_FactBilling] ADD ServiceCostDetailsKey uniqueidentifier NULL;
END