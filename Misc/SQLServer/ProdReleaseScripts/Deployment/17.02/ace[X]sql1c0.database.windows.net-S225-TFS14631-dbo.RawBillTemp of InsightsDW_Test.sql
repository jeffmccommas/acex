﻿ /********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_TEST DATABASE SHARD ***/
/********************************************************/

/****** Object:  Table [dbo].[RawBillTemp_87]    Script Date: 1/27/2017 9:21:20 AM ******/
DROP TABLE [dbo].[RawBillTemp_87]
GO

/****** Object:  Table [dbo].[RawBillTemp_87]    Script Date: 1/27/2017 9:21:20 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RawBillTemp_87](
    [customer_id] [VARCHAR](255) NULL,
    [premise_id] [VARCHAR](255) NULL,
    [mail_address_line_1] [VARCHAR](255) NULL,
    [mail_address_line_2] [VARCHAR](255) NULL,
    [mail_address_line_3] [VARCHAR](255) NULL,
    [mail_city] [VARCHAR](255) NULL,
    [mail_state] [VARCHAR](255) NULL,
    [mail_zip_code] [VARCHAR](255) NULL,
    [first_name] [VARCHAR](255) NULL,
    [last_name] [VARCHAR](255) NULL,
    [phone_1] [VARCHAR](255) NULL,
    [phone_2] [VARCHAR](255) NULL,
    [email] [VARCHAR](255) NULL,
    [customer_type] [VARCHAR](255) NULL,
    [account_id] [VARCHAR](255) NULL,
    [active_date] [VARCHAR](255) NULL,
    [inactive_date] [VARCHAR](255) NULL,
    [read_cycle] [VARCHAR](255) NULL,
    [rate_code] [VARCHAR](255) NULL,
    [service_house_number] [VARCHAR](255) NULL,
    [service_street_name] [VARCHAR](255) NULL,
    [service_unit] [VARCHAR](255) NULL,
    [service_city] [VARCHAR](255) NULL,
    [service_state] [VARCHAR](255) NULL,
    [service_zip_code] [VARCHAR](255) NULL,
    [meter_type] [VARCHAR](255) NULL,
    [meter_units] [VARCHAR](255) NULL,
    [bldg_sq_foot] [VARCHAR](255) NULL,
    [year_built] [VARCHAR](255) NULL,
    [bedrooms] [VARCHAR](255) NULL,
    [assess_value] [VARCHAR](255) NULL,
    [usage_value] [VARCHAR](255) NULL,
    [bill_enddate] [VARCHAR](255) NULL,
    [bill_days] [VARCHAR](255) NULL,
    [is_estimate] [VARCHAR](255) NULL,
    [usage_charge] [VARCHAR](255) NULL,
    [ClientId] [INT] NOT NULL,
    [Programs] [VARCHAR](MAX) NULL,
    [service_commodity] [VARCHAR](255) NULL,
    [account_structure_type] [VARCHAR](255) NULL,
    [meter_id] [VARCHAR](255) NULL,
    [billperiod_type] [VARCHAR](255) NULL,
    [meter_replaces_meterid] [VARCHAR](255) NULL,
    [service_read_date] [VARCHAR](255) NULL,
    [service_read_startdate] [VARCHAR](255) NULL,
    /** BASIC_CHARGE|BILL_TAXES|DEMAND_UNIT|DEMAND_QUANTITY|ENERGY_CHARGE|TOTAL_USAGE|DEMAND_CHARGE|UOM_TOTAL_USAGE|TIMESTAMP_TOTAL_USAGE|TIMESTAMP_DEMAND_VALUE **/
    [basic_charge] [NVARCHAR](255) NULL,
    [bill_taxes] [NVARCHAR](255) NULL,
    [demand_unit] [NVARCHAR](255) NULL,
    [demand_quantity] [NVARCHAR](255) NULL,
    [energy_charge] [NVARCHAR](255) NULL,
    [total_usage] [NVARCHAR](255) NULL,
    [demand_charge] [NVARCHAR](255) NULL,
    [uom_total_usage] [NVARCHAR](255) NULL,
    [timestamp_total_usage] [NVARCHAR](255) NULL,
    [timestamp_demand_value] [NVARCHAR](255) NULL
)

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[RawBillTemp_224]    Script Date: 1/27/2017 9:31:29 AM ******/
DROP TABLE [dbo].[RawBillTemp_224]
GO

/****** Object:  Table [dbo].[RawBillTemp_224]    Script Date: 1/27/2017 9:31:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RawBillTemp_224](
    [customer_id] [VARCHAR](255) NULL,
    [premise_id] [VARCHAR](255) NULL,
    [mail_address_line_1] [VARCHAR](255) NULL,
    [mail_address_line_2] [VARCHAR](255) NULL,
    [mail_address_line_3] [VARCHAR](255) NULL,
    [mail_city] [VARCHAR](255) NULL,
    [mail_state] [VARCHAR](255) NULL,
    [mail_zip_code] [VARCHAR](255) NULL,
    [first_name] [VARCHAR](255) NULL,
    [last_name] [VARCHAR](255) NULL,
    [phone_1] [VARCHAR](255) NULL,
    [phone_2] [VARCHAR](255) NULL,
    [email] [VARCHAR](255) NULL,
    [customer_type] [VARCHAR](255) NULL,
    [account_id] [VARCHAR](255) NULL,
    [active_date] [VARCHAR](255) NULL,
    [inactive_date] [VARCHAR](255) NULL,
    [read_cycle] [VARCHAR](255) NULL,
    [rate_code] [VARCHAR](255) NULL,
    [service_house_number] [VARCHAR](255) NULL,
    [service_street_name] [VARCHAR](255) NULL,
    [service_unit] [VARCHAR](255) NULL,
    [service_city] [VARCHAR](255) NULL,
    [service_state] [VARCHAR](255) NULL,
    [service_zip_code] [VARCHAR](255) NULL,
    [meter_type] [VARCHAR](255) NULL,
    [meter_units] [VARCHAR](255) NULL,
    [bldg_sq_foot] [VARCHAR](255) NULL,
    [year_built] [VARCHAR](255) NULL,
    [bedrooms] [VARCHAR](255) NULL,
    [assess_value] [VARCHAR](255) NULL,
    [usage_value] [VARCHAR](255) NULL,
    [bill_enddate] [VARCHAR](255) NULL,
    [bill_days] [VARCHAR](255) NULL,
    [is_estimate] [VARCHAR](255) NULL,
    [usage_charge] [VARCHAR](255) NULL,
    [ClientId] [INT] NOT NULL,
    [Programs] [VARCHAR](MAX) NULL,
    [service_commodity] [VARCHAR](255) NULL,
    [account_structure_type] [VARCHAR](255) NULL,
    [meter_id] [VARCHAR](255) NULL,
    [billperiod_type] [VARCHAR](255) NULL,
    [meter_replaces_meterid] [VARCHAR](255) NULL,
    [service_read_date] [VARCHAR](255) NULL,
    [service_read_startdate] [VARCHAR](255) NULL,
    /** BASIC_CHARGE|BILL_TAXES|DEMAND_UNIT|DEMAND_QUANTITY|ENERGY_CHARGE|TOTAL_USAGE|DEMAND_CHARGE|UOM_TOTAL_USAGE|TIMESTAMP_TOTAL_USAGE|TIMESTAMP_DEMAND_VALUE **/
    [basic_charge] [NVARCHAR](255) NULL,
    [bill_taxes] [NVARCHAR](255) NULL,
    [demand_unit] [NVARCHAR](255) NULL,
    [demand_quantity] [NVARCHAR](255) NULL,
    [energy_charge] [NVARCHAR](255) NULL,
    [total_usage] [NVARCHAR](255) NULL,
    [demand_charge] [NVARCHAR](255) NULL,
    [uom_total_usage] [NVARCHAR](255) NULL,
    [timestamp_total_usage] [NVARCHAR](255) NULL,
    [timestamp_demand_value] [NVARCHAR](255) NULL
)

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[RawBillTemp_256]    Script Date: 1/27/2017 9:32:56 AM ******/
DROP TABLE [dbo].[RawBillTemp_256]
GO

/****** Object:  Table [dbo].[RawBillTemp_256]    Script Date: 1/27/2017 9:32:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RawBillTemp_256](
    [customer_id] [VARCHAR](255) NULL,
    [premise_id] [VARCHAR](255) NULL,
    [mail_address_line_1] [VARCHAR](255) NULL,
    [mail_address_line_2] [VARCHAR](255) NULL,
    [mail_address_line_3] [VARCHAR](255) NULL,
    [mail_city] [VARCHAR](255) NULL,
    [mail_state] [VARCHAR](255) NULL,
    [mail_zip_code] [VARCHAR](255) NULL,
    [first_name] [VARCHAR](255) NULL,
    [last_name] [VARCHAR](255) NULL,
    [phone_1] [VARCHAR](255) NULL,
    [phone_2] [VARCHAR](255) NULL,
    [email] [VARCHAR](255) NULL,
    [customer_type] [VARCHAR](255) NULL,
    [account_id] [VARCHAR](255) NULL,
    [active_date] [VARCHAR](255) NULL,
    [inactive_date] [VARCHAR](255) NULL,
    [read_cycle] [VARCHAR](255) NULL,
    [rate_code] [VARCHAR](255) NULL,
    [service_house_number] [VARCHAR](255) NULL,
    [service_street_name] [VARCHAR](255) NULL,
    [service_unit] [VARCHAR](255) NULL,
    [service_city] [VARCHAR](255) NULL,
    [service_state] [VARCHAR](255) NULL,
    [service_zip_code] [VARCHAR](255) NULL,
    [meter_type] [VARCHAR](255) NULL,
    [meter_units] [VARCHAR](255) NULL,
    [bldg_sq_foot] [VARCHAR](255) NULL,
    [year_built] [VARCHAR](255) NULL,
    [bedrooms] [VARCHAR](255) NULL,
    [assess_value] [VARCHAR](255) NULL,
    [usage_value] [VARCHAR](255) NULL,
    [bill_enddate] [VARCHAR](255) NULL,
    [bill_days] [VARCHAR](255) NULL,
    [is_estimate] [VARCHAR](255) NULL,
    [usage_charge] [VARCHAR](255) NULL,
    [ClientId] [INT] NOT NULL,
    [Programs] [VARCHAR](MAX) NULL,
    [service_commodity] [VARCHAR](255) NULL,
    [account_structure_type] [VARCHAR](255) NULL,
    [meter_id] [VARCHAR](255) NULL,
    [billperiod_type] [VARCHAR](255) NULL,
    [meter_replaces_meterid] [VARCHAR](255) NULL,
    [service_read_date] [VARCHAR](255) NULL,
    [service_read_startdate] [VARCHAR](255) NULL,
    /** BASIC_CHARGE|BILL_TAXES|DEMAND_UNIT|DEMAND_QUANTITY|ENERGY_CHARGE|TOTAL_USAGE|DEMAND_CHARGE|UOM_TOTAL_USAGE|TIMESTAMP_TOTAL_USAGE|TIMESTAMP_DEMAND_VALUE **/
    [basic_charge] [NVARCHAR](255) NULL,
    [bill_taxes] [NVARCHAR](255) NULL,
    [demand_unit] [NVARCHAR](255) NULL,
    [demand_quantity] [NVARCHAR](255) NULL,
    [energy_charge] [NVARCHAR](255) NULL,
    [total_usage] [NVARCHAR](255) NULL,
    [demand_charge] [NVARCHAR](255) NULL,
    [uom_total_usage] [NVARCHAR](255) NULL,
    [timestamp_total_usage] [NVARCHAR](255) NULL,
    [timestamp_demand_value] [NVARCHAR](255) NULL
)

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[RawBillTemp_276]    Script Date: 1/27/2017 9:33:44 AM ******/
DROP TABLE [dbo].[RawBillTemp_276]
GO

/****** Object:  Table [dbo].[RawBillTemp_276]    Script Date: 1/27/2017 9:33:44 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RawBillTemp_276](
    [customer_id] [VARCHAR](255) NULL,
    [premise_id] [VARCHAR](255) NULL,
    [mail_address_line_1] [VARCHAR](255) NULL,
    [mail_address_line_2] [VARCHAR](255) NULL,
    [mail_address_line_3] [VARCHAR](255) NULL,
    [mail_city] [VARCHAR](255) NULL,
    [mail_state] [VARCHAR](255) NULL,
    [mail_zip_code] [VARCHAR](255) NULL,
    [first_name] [VARCHAR](255) NULL,
    [last_name] [VARCHAR](255) NULL,
    [phone_1] [VARCHAR](255) NULL,
    [phone_2] [VARCHAR](255) NULL,
    [email] [VARCHAR](255) NULL,
    [customer_type] [VARCHAR](255) NULL,
    [account_id] [VARCHAR](255) NULL,
    [active_date] [VARCHAR](255) NULL,
    [inactive_date] [VARCHAR](255) NULL,
    [read_cycle] [VARCHAR](255) NULL,
    [rate_code] [VARCHAR](255) NULL,
    [service_house_number] [VARCHAR](255) NULL,
    [service_street_name] [VARCHAR](255) NULL,
    [service_unit] [VARCHAR](255) NULL,
    [service_city] [VARCHAR](255) NULL,
    [service_state] [VARCHAR](255) NULL,
    [service_zip_code] [VARCHAR](255) NULL,
    [meter_type] [VARCHAR](255) NULL,
    [meter_units] [VARCHAR](255) NULL,
    [bldg_sq_foot] [VARCHAR](255) NULL,
    [year_built] [VARCHAR](255) NULL,
    [bedrooms] [VARCHAR](255) NULL,
    [assess_value] [VARCHAR](255) NULL,
    [usage_value] [VARCHAR](255) NULL,
    [bill_enddate] [VARCHAR](255) NULL,
    [bill_days] [VARCHAR](255) NULL,
    [is_estimate] [VARCHAR](255) NULL,
    [usage_charge] [VARCHAR](255) NULL,
    [ClientId] [INT] NOT NULL,
    [Programs] [VARCHAR](MAX) NULL,
    [service_commodity] [VARCHAR](255) NULL,
    [account_structure_type] [VARCHAR](255) NULL,
    [meter_id] [VARCHAR](255) NULL,
    [billperiod_type] [VARCHAR](255) NULL,
    [meter_replaces_meterid] [VARCHAR](255) NULL,
    [service_read_date] [VARCHAR](255) NULL,
    [service_read_startdate] [VARCHAR](255) NULL,
    /** BASIC_CHARGE|BILL_TAXES|DEMAND_UNIT|DEMAND_QUANTITY|ENERGY_CHARGE|TOTAL_USAGE|DEMAND_CHARGE|UOM_TOTAL_USAGE|TIMESTAMP_TOTAL_USAGE|TIMESTAMP_DEMAND_VALUE **/
    [basic_charge] [NVARCHAR](255) NULL,
    [bill_taxes] [NVARCHAR](255) NULL,
    [demand_unit] [NVARCHAR](255) NULL,
    [demand_quantity] [NVARCHAR](255) NULL,
    [energy_charge] [NVARCHAR](255) NULL,
    [total_usage] [NVARCHAR](255) NULL,
    [demand_charge] [NVARCHAR](255) NULL,
    [uom_total_usage] [NVARCHAR](255) NULL,
    [timestamp_total_usage] [NVARCHAR](255) NULL,
    [timestamp_demand_value] [NVARCHAR](255) NULL
)

GO

SET ANSI_PADDING OFF
GO





