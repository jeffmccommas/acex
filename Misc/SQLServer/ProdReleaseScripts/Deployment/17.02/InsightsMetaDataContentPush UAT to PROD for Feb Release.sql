﻿/*
Run this script on:

acePsql1c0.database.windows.net.InsightsMetadata    -  This database will be modified

to synchronize it with:

aceUsql1c0.database.windows.net.InsightsMetadata

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 11.1.3 from Red Gate Software Ltd at 2/24/2017 3:18:39 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

PRINT(N'Drop constraints from [cm].[ClientWidgetTextContent]')
ALTER TABLE [cm].[ClientWidgetTextContent] DROP CONSTRAINT [FK_ClientWidgetTextContentality_ClientWidget]
ALTER TABLE [cm].[ClientWidgetTextContent] DROP CONSTRAINT [FK_ClientWidgetTextContentality_TypeTextContentality]

PRINT(N'Drop constraints from [cm].[ClientTextContentLocaleContent]')
ALTER TABLE [cm].[ClientTextContentLocaleContent] DROP CONSTRAINT [FK_ClientTextContentLocaleContent_ClientTextContent]
ALTER TABLE [cm].[ClientTextContentLocaleContent] DROP CONSTRAINT [FK_ClientTextContentLocaleContent_TypeLocale]

PRINT(N'Drop constraints from [cm].[ClientTabProfileSection]')
ALTER TABLE [cm].[ClientTabProfileSection] DROP CONSTRAINT [FK_ClientTabProfileSection_ClientTab]
ALTER TABLE [cm].[ClientTabProfileSection] DROP CONSTRAINT [FK_ClientTabProfileSection_TypeProfileSection]

PRINT(N'Drop constraints from [cm].[ClientTabCondition]')
ALTER TABLE [cm].[ClientTabCondition] DROP CONSTRAINT [FK_ClientTabConditionality_ClientTab]
ALTER TABLE [cm].[ClientTabCondition] DROP CONSTRAINT [FK_ClientTabConditionality_TypeConditionality]

PRINT(N'Drop constraints from [cm].[ClientTabChildTab]')
ALTER TABLE [cm].[ClientTabChildTab] DROP CONSTRAINT [FK_ClientTabChildTab_TypeTab]
ALTER TABLE [cm].[ClientTabChildTab] DROP CONSTRAINT [FK_ClientTabChildTab_ClientTab]

PRINT(N'Drop constraints from [cm].[ClientTabAction]')
ALTER TABLE [cm].[ClientTabAction] DROP CONSTRAINT [FK_ClientTabAction_TypeAction]
ALTER TABLE [cm].[ClientTabAction] DROP CONSTRAINT [FK_ClientTabAction_ClientTab]

PRINT(N'Drop constraints from [cm].[ClientProfileSectionCondition]')
ALTER TABLE [cm].[ClientProfileSectionCondition] DROP CONSTRAINT [FK_ClientProfileSectionCondition_ClientProfileSection]
ALTER TABLE [cm].[ClientProfileSectionCondition] DROP CONSTRAINT [FK_ClientProfileSectionCondition_TypeCondition]

PRINT(N'Drop constraints from [cm].[ClientProfileSectionAttribute]')
ALTER TABLE [cm].[ClientProfileSectionAttribute] DROP CONSTRAINT [FK_ClientProfileSectionAttribute_ClientProfileSection]
ALTER TABLE [cm].[ClientProfileSectionAttribute] DROP CONSTRAINT [FK_ClientProfileSectionAttribute_TypeProfileAttribute]

PRINT(N'Drop constraints from [cm].[ClientProfileAttributeProfileOption]')
ALTER TABLE [cm].[ClientProfileAttributeProfileOption] DROP CONSTRAINT [FK_ClientProfileAttributeProfileOption_ClientProfileAttribute]
ALTER TABLE [cm].[ClientProfileAttributeProfileOption] DROP CONSTRAINT [FK_ClientProfileAttributeProfileOption_TypeProfileOption]

PRINT(N'Drop constraints from [cm].[ClientProfileAttributeCondition]')
ALTER TABLE [cm].[ClientProfileAttributeCondition] DROP CONSTRAINT [FK_ClientProfileAttributeCondition_ClientProfileAttribute]
ALTER TABLE [cm].[ClientProfileAttributeCondition] DROP CONSTRAINT [FK_ClientProfileAttributeCondition_TypeCondition]

PRINT(N'Drop constraints from [cm].[ClientActionCondition]')
ALTER TABLE [cm].[ClientActionCondition] DROP CONSTRAINT [FK_ClientActionCondition_ClientAction]
ALTER TABLE [cm].[ClientActionCondition] DROP CONSTRAINT [FK_ClientActionCondition_TypeCondition]

PRINT(N'Drop constraints from [cm].[ClientActionAppliance]')
ALTER TABLE [cm].[ClientActionAppliance] DROP CONSTRAINT [FK_ClientActionAppliance_TypeAppliance]
ALTER TABLE [cm].[ClientActionAppliance] DROP CONSTRAINT [FK_ClientActionAppliance_ClientAction]

PRINT(N'Drop constraints from [cm].[ClientWidget]')
ALTER TABLE [cm].[ClientWidget] DROP CONSTRAINT [FK_ClientWidget_TypeTextContent]
ALTER TABLE [cm].[ClientWidget] DROP CONSTRAINT [FK_ClientWidget_TypeTab]
ALTER TABLE [cm].[ClientWidget] DROP CONSTRAINT [FK_ClientWidget_TypeWidget]
ALTER TABLE [cm].[ClientWidget] DROP CONSTRAINT [FK_ClientWidget_TypeWidgetType]

PRINT(N'Drop constraint FK_ClientWidgetConditionality_ClientWidget from [cm].[ClientWidgetCondition]')
ALTER TABLE [cm].[ClientWidgetCondition] DROP CONSTRAINT [FK_ClientWidgetConditionality_ClientWidget]

PRINT(N'Drop constraint FK_ClientWidgetConfigurationality_ClientWidget from [cm].[ClientWidgetConfiguration]')
ALTER TABLE [cm].[ClientWidgetConfiguration] DROP CONSTRAINT [FK_ClientWidgetConfigurationality_ClientWidget]

PRINT(N'Drop constraints from [cm].[ClientTextContent]')
ALTER TABLE [cm].[ClientTextContent] DROP CONSTRAINT [FK_ClientTextContent_TypeCategory]
ALTER TABLE [cm].[ClientTextContent] DROP CONSTRAINT [FK_ClientTextContent_Client]
ALTER TABLE [cm].[ClientTextContent] DROP CONSTRAINT [FK_ClientTextContent_TypeTextContent]

PRINT(N'Drop constraints from [cm].[ClientTab]')
ALTER TABLE [cm].[ClientTab] DROP CONSTRAINT [FK_ClientTab_TypeLayout]
ALTER TABLE [cm].[ClientTab] DROP CONSTRAINT [FK_ClientTab_TypeTextContent]
ALTER TABLE [cm].[ClientTab] DROP CONSTRAINT [FK_ClientTab_TypeTab]
ALTER TABLE [cm].[ClientTab] DROP CONSTRAINT [FK_ClientTab_TypeTabType]

PRINT(N'Drop constraint FK_ClientTabConfigurationality_ClientTab from [cm].[ClientTabConfiguration]')
ALTER TABLE [cm].[ClientTabConfiguration] DROP CONSTRAINT [FK_ClientTabConfigurationality_ClientTab]

PRINT(N'Drop constraint FK_ClientTabTextContentality_ClientTab from [cm].[ClientTabTextContent]')
ALTER TABLE [cm].[ClientTabTextContent] DROP CONSTRAINT [FK_ClientTabTextContentality_ClientTab]

PRINT(N'Drop constraints from [cm].[ClientProfileSection]')
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_Client]
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent]
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent3]
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeProfileSection]
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent2]
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent1]

PRINT(N'Drop constraints from [cm].[ClientProfileOption]')
ALTER TABLE [cm].[ClientProfileOption] DROP CONSTRAINT [FK_ClientProfileOption_Client]
ALTER TABLE [cm].[ClientProfileOption] DROP CONSTRAINT [FK_ClientProfileOption_TypeTextContent]
ALTER TABLE [cm].[ClientProfileOption] DROP CONSTRAINT [FK_ClientProfileOption_TypeProfileOption]

PRINT(N'Drop constraints from [cm].[ClientProfileAttribute]')
ALTER TABLE [cm].[ClientProfileAttribute] DROP CONSTRAINT [FK_ClientProfileAttribute_Client]
ALTER TABLE [cm].[ClientProfileAttribute] DROP CONSTRAINT [FK_ClientProfileAttribute_TypeProfileAttribute]
ALTER TABLE [cm].[ClientProfileAttribute] DROP CONSTRAINT [FK_ClientProfileAttribute_TypeTextContent]

PRINT(N'Drop constraints from [cm].[ClientConfigurationBulk]')
ALTER TABLE [cm].[ClientConfigurationBulk] DROP CONSTRAINT [FK_ClientConfigurationBulk_TypeCategory]
ALTER TABLE [cm].[ClientConfigurationBulk] DROP CONSTRAINT [FK_ClientConfigurationBulk_Client]
ALTER TABLE [cm].[ClientConfigurationBulk] DROP CONSTRAINT [FK_ClientConfigurationBulk_TypeConfigurationBulk]
ALTER TABLE [cm].[ClientConfigurationBulk] DROP CONSTRAINT [FK_ClientConfigurationBulk_TypeEnvironment]

PRINT(N'Drop constraints from [cm].[ClientConfiguration]')
ALTER TABLE [cm].[ClientConfiguration] DROP CONSTRAINT [FK_ClientConfiguration_TypeCategory]
ALTER TABLE [cm].[ClientConfiguration] DROP CONSTRAINT [FK_ClientConfiguration_Client]
ALTER TABLE [cm].[ClientConfiguration] DROP CONSTRAINT [FK_ClientConfiguration_TypeConfiguration]
ALTER TABLE [cm].[ClientConfiguration] DROP CONSTRAINT [FK_ClientConfiguration_TypeEnvironment]

PRINT(N'Drop constraints from [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeAction]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeActionType]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeUom]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeUom1]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_Client]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeCommodity]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeExpression]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeDifficulty]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeHabitInterval]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_ClientFileContent2]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeTextContent]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeTextContent1]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeNextStepLinkType]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeExpression1]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_ClientFileContent]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_ClientFileContent3]

PRINT(N'Drop constraint FK_ClientActionSeasonality_ClientAction from [cm].[ClientActionSeason]')
ALTER TABLE [cm].[ClientActionSeason] DROP CONSTRAINT [FK_ClientActionSeasonality_ClientAction]

PRINT(N'Drop constraint FK_ClientActionWhatIfData_ClientAction from [cm].[ClientActionWhatIfData]')
ALTER TABLE [cm].[ClientActionWhatIfData] DROP CONSTRAINT [FK_ClientActionWhatIfData_ClientAction]

PRINT(N'Drop constraint FK_ClientAppliance_TypeTextContent from [cm].[ClientAppliance]')
ALTER TABLE [cm].[ClientAppliance] DROP CONSTRAINT [FK_ClientAppliance_TypeTextContent]

PRINT(N'Drop constraint FK_ClientCommodity_TypeTextContent from [cm].[ClientCommodity]')
ALTER TABLE [cm].[ClientCommodity] DROP CONSTRAINT [FK_ClientCommodity_TypeTextContent]

PRINT(N'Drop constraint FK_ClientCurrency_TypeTextContent from [cm].[ClientCurrency]')
ALTER TABLE [cm].[ClientCurrency] DROP CONSTRAINT [FK_ClientCurrency_TypeTextContent]

PRINT(N'Drop constraint FK_ClientCurrency_TypeTextContent1 from [cm].[ClientCurrency]')
ALTER TABLE [cm].[ClientCurrency] DROP CONSTRAINT [FK_ClientCurrency_TypeTextContent1]

PRINT(N'Drop constraint FK_ClientEnduse_TypeTextContent from [cm].[ClientEnduse]')
ALTER TABLE [cm].[ClientEnduse] DROP CONSTRAINT [FK_ClientEnduse_TypeTextContent]

PRINT(N'Drop constraint FK_ClientMeasurement_TypeTextContent from [cm].[ClientMeasurement]')
ALTER TABLE [cm].[ClientMeasurement] DROP CONSTRAINT [FK_ClientMeasurement_TypeTextContent]

PRINT(N'Drop constraint FK_ClientSeason_TypeTextContent from [cm].[ClientSeason]')
ALTER TABLE [cm].[ClientSeason] DROP CONSTRAINT [FK_ClientSeason_TypeTextContent]

PRINT(N'Drop constraint FK_ClientTabTextContentality_TypeTextContentality from [cm].[ClientTabTextContent]')
ALTER TABLE [cm].[ClientTabTextContent] DROP CONSTRAINT [FK_ClientTabTextContentality_TypeTextContentality]

PRINT(N'Drop constraint FK_ClientUOM_ClientUOM1 from [cm].[ClientUOM]')
ALTER TABLE [cm].[ClientUOM] DROP CONSTRAINT [FK_ClientUOM_ClientUOM1]

PRINT(N'Drop constraint FK_ClientCondition_TypeProfileOption from [cm].[ClientCondition]')
ALTER TABLE [cm].[ClientCondition] DROP CONSTRAINT [FK_ClientCondition_TypeProfileOption]

PRINT(N'Drop constraint FK_ClientProfileDefault_TypeProfileOption from [cm].[ClientProfileDefault]')
ALTER TABLE [cm].[ClientProfileDefault] DROP CONSTRAINT [FK_ClientProfileDefault_TypeProfileOption]

PRINT(N'Drop constraints from [cm].[TypeProfileAttribute]')
ALTER TABLE [cm].[TypeProfileAttribute] DROP CONSTRAINT [FK_TypeProfileAttribute_TypeEntityLevel]
ALTER TABLE [cm].[TypeProfileAttribute] DROP CONSTRAINT [FK_TypeProfileAttribute_TypeProfileAttributeType]

PRINT(N'Drop constraint FK_ClientApplianceProfileAttribute_TypeProfileAttribute from [cm].[ClientApplianceProfileAttribute]')
ALTER TABLE [cm].[ClientApplianceProfileAttribute] DROP CONSTRAINT [FK_ClientApplianceProfileAttribute_TypeProfileAttribute]

PRINT(N'Drop constraint FK_ClientCondition_TypeProfileAttribute from [cm].[ClientCondition]')
ALTER TABLE [cm].[ClientCondition] DROP CONSTRAINT [FK_ClientCondition_TypeProfileAttribute]

PRINT(N'Drop constraint FK_ClientProfileDefault_TypeProfileAttribute from [cm].[ClientProfileDefault]')
ALTER TABLE [cm].[ClientProfileDefault] DROP CONSTRAINT [FK_ClientProfileDefault_TypeProfileAttribute]

PRINT(N'Drop constraint FK_ClientWhatIfData_TypeProfileAttribute from [cm].[ClientWhatIfData]')
ALTER TABLE [cm].[ClientWhatIfData] DROP CONSTRAINT [FK_ClientWhatIfData_TypeProfileAttribute]

PRINT(N'Drop constraint FK_ClientTabConfigurationality_TypeConfigurationality from [cm].[ClientTabConfiguration]')
ALTER TABLE [cm].[ClientTabConfiguration] DROP CONSTRAINT [FK_ClientTabConfigurationality_TypeConfigurationality]

PRINT(N'Drop constraint FK_ClientWidgetConfigurationality_TypeConfigurationality from [cm].[ClientWidgetConfiguration]')
ALTER TABLE [cm].[ClientWidgetConfiguration] DROP CONSTRAINT [FK_ClientWidgetConfigurationality_TypeConfigurationality]

PRINT(N'Drop constraint FK_ClientCondition_TypeCategory from [cm].[ClientCondition]')
ALTER TABLE [cm].[ClientCondition] DROP CONSTRAINT [FK_ClientCondition_TypeCategory]

PRINT(N'Drop constraint FK_ClientFileContent_TypeCategory from [cm].[ClientFileContent]')
ALTER TABLE [cm].[ClientFileContent] DROP CONSTRAINT [FK_ClientFileContent_TypeCategory]

PRINT(N'Drop constraint FK_TypeEnumeration_TypeCategory from [cm].[TypeEnumeration]')
ALTER TABLE [cm].[TypeEnumeration] DROP CONSTRAINT [FK_TypeEnumeration_TypeCategory]

PRINT(N'Delete rows from [cm].[ClientTextContentLocaleContent]')
DELETE FROM [cm].[ClientTextContentLocaleContent] WHERE [ClientTextContentID]=397 AND [LocaleKey]='en-US'
DELETE FROM [cm].[ClientTextContentLocaleContent] WHERE [ClientTextContentID]=397 AND [LocaleKey]='es-ES'
DELETE FROM [cm].[ClientTextContentLocaleContent] WHERE [ClientTextContentID]=397 AND [LocaleKey]='ru-RU'
DELETE FROM [cm].[ClientTextContentLocaleContent] WHERE [ClientTextContentID]=398 AND [LocaleKey]='en-US'
DELETE FROM [cm].[ClientTextContentLocaleContent] WHERE [ClientTextContentID]=398 AND [LocaleKey]='es-ES'
DELETE FROM [cm].[ClientTextContentLocaleContent] WHERE [ClientTextContentID]=398 AND [LocaleKey]='ru-RU'
PRINT(N'Operation applied to 6 rows out of 6')

PRINT(N'Delete rows from [cm].[ClientTabProfileSection]')
DELETE FROM [cm].[ClientTabProfileSection] WHERE [ClientTabID]=1266 AND [ProfileSectionKey]='profilesection.buscooling'
DELETE FROM [cm].[ClientTabProfileSection] WHERE [ClientTabID]=1266 AND [ProfileSectionKey]='profilesection.busheating'
DELETE FROM [cm].[ClientTabProfileSection] WHERE [ClientTabID]=1266 AND [ProfileSectionKey]='profilesection.insulation'
PRINT(N'Operation applied to 3 rows out of 3')

PRINT(N'Delete row from [cm].[ClientTabCondition]')
DELETE FROM [cm].[ClientTabCondition] WHERE [ClientTabID]=1254 AND [ConditionKey]='premisetype.residential'

PRINT(N'Delete row from [cm].[ClientTabAction]')
DELETE FROM [cm].[ClientTabAction] WHERE [ClientTabID]=1122 AND [ActionKey]='shortershowers'

PRINT(N'Delete rows from [cm].[ClientProfileSectionCondition]')
DELETE FROM [cm].[ClientProfileSectionCondition] WHERE [ClientProfileSectionID]=188 AND [ConditionKey]='centralac.yes'
DELETE FROM [cm].[ClientProfileSectionCondition] WHERE [ClientProfileSectionID]=191 AND [ConditionKey]='secondaryheating.yes'
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Delete rows from [cm].[ClientActionCondition]')
DELETE FROM [cm].[ClientActionCondition] WHERE [ClientActionID]=1177 AND [ConditionKey]='waterheater.gas'
DELETE FROM [cm].[ClientActionCondition] WHERE [ClientActionID]=1246 AND [ConditionKey]='premisetype.residential'
DELETE FROM [cm].[ClientActionCondition] WHERE [ClientActionID]=7275 AND [ConditionKey]='premisetype.residential'
DELETE FROM [cm].[ClientActionCondition] WHERE [ClientActionID]=7275 AND [ConditionKey]='waterheater.gas'
DELETE FROM [cm].[ClientActionCondition] WHERE [ClientActionID]=7451 AND [ConditionKey]='premisetype.residential'
DELETE FROM [cm].[ClientActionCondition] WHERE [ClientActionID]=7451 AND [ConditionKey]='waterheater.gas'
PRINT(N'Operation applied to 6 rows out of 6')

PRINT(N'Delete rows from [cm].[ClientActionAppliance]')
DELETE FROM [cm].[ClientActionAppliance] WHERE [ClientActionID]=1177 AND [ApplianceKey]='waterheater'
DELETE FROM [cm].[ClientActionAppliance] WHERE [ClientActionID]=1246 AND [ApplianceKey]='heatsystem'
DELETE FROM [cm].[ClientActionAppliance] WHERE [ClientActionID]=7275 AND [ApplianceKey]='waterheater'
DELETE FROM [cm].[ClientActionAppliance] WHERE [ClientActionID]=7451 AND [ApplianceKey]='waterheater'
PRINT(N'Operation applied to 4 rows out of 4')

PRINT(N'Delete rows from [cm].[ClientTextContent]')
DELETE FROM [cm].[ClientTextContent] WHERE [ClientTextContentID]=397
DELETE FROM [cm].[ClientTextContent] WHERE [ClientTextContentID]=398
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Delete rows from [cm].[ClientAction]')
DELETE FROM [cm].[ClientAction] WHERE [ClientActionID]=1177
DELETE FROM [cm].[ClientAction] WHERE [ClientActionID]=7275
DELETE FROM [cm].[ClientAction] WHERE [ClientActionID]=7451
PRINT(N'Operation applied to 3 rows out of 3')

PRINT(N'Delete rows from [admin].[ETLBulkConfiguration]')
DELETE FROM [admin].[ETLBulkConfiguration] WHERE [ClientID]=87 AND [Environment]='prod' AND [ClientComponent]='InsightsDW.ImportData'
DELETE FROM [admin].[ETLBulkConfiguration] WHERE [ClientID]=101 AND [Environment]='prod' AND [ClientComponent]='InsightsDW.ImportData'
DELETE FROM [admin].[ETLBulkConfiguration] WHERE [ClientID]=224 AND [Environment]='prod' AND [ClientComponent]='InsightsDW.ImportData'
DELETE FROM [admin].[ETLBulkConfiguration] WHERE [ClientID]=256 AND [Environment]='prod' AND [ClientComponent]='InsightsDW.ImportData'
DELETE FROM [admin].[ETLBulkConfiguration] WHERE [ClientID]=276 AND [Environment]='prod' AND [ClientComponent]='InsightsDW.ImportData'
PRINT(N'Operation applied to 5 rows out of 5')

PRINT(N'Update rows in [cm].[ClientTextContentLocaleContent]')
UPDATE [cm].[ClientTextContentLocaleContent] SET [ShortText]=N'2006 - 2010', [MediumText]=N'2006 - 2010', [LongText]=N'2006 - 2010' WHERE [ClientTextContentID]=4900 AND [LocaleKey]='en-US'
UPDATE [cm].[ClientTextContentLocaleContent] SET [MediumText]=N'Bill and usage insights', [LongText]=N'Bill and usage insights' WHERE [ClientTextContentID]=6952 AND [LocaleKey]='en-US'
UPDATE [cm].[ClientTextContentLocaleContent] SET [MediumText]=N'Proyecto de ley y uso de ideas', [LongText]=N'Proyecto de ley y uso de ideas' WHERE [ClientTextContentID]=6952 AND [LocaleKey]='es-ES'
UPDATE [cm].[ClientTextContentLocaleContent] SET [MediumText]=N'Билл и использование идеи', [LongText]=N'Билл и использование идеи' WHERE [ClientTextContentID]=6952 AND [LocaleKey]='ru-RU'
PRINT(N'Operation applied to 4 rows out of 4')

PRINT(N'Update rows in [cm].[ClientTabProfileSection]')
UPDATE [cm].[ClientTabProfileSection] SET [Rank]=3 WHERE [ClientTabID]=1266 AND [ProfileSectionKey]='profilesection.centralac'
UPDATE [cm].[ClientTabProfileSection] SET [Rank]=2 WHERE [ClientTabID]=1266 AND [ProfileSectionKey]='profilesection.secondaryheating'
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Update rows in [cm].[ClientTabAction]')
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=54 WHERE [ClientTabID]=1122 AND [ActionKey]='clotheswasherpartialload'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=51 WHERE [ClientTabID]=1122 AND [ActionKey]='gardensoakerhose'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=53 WHERE [ClientTabID]=1122 AND [ActionKey]='reducelawnwatering'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=50 WHERE [ClientTabID]=1122 AND [ActionKey]='repairleakytoilet'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=39 WHERE [ClientTabID]=1122 AND [ActionKey]='showerstartupvalve'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=40 WHERE [ClientTabID]=1122 AND [ActionKey]='solarpoolheater'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=41 WHERE [ClientTabID]=1122 AND [ActionKey]='solarwaterheater'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=42 WHERE [ClientTabID]=1122 AND [ActionKey]='takeshortershowers'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=55 WHERE [ClientTabID]=1122 AND [ActionKey]='washclothesincold'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=43 WHERE [ClientTabID]=1122 AND [ActionKey]='washdishesturnoffwater'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=44 WHERE [ClientTabID]=1122 AND [ActionKey]='waterheaterheatrecovery'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=45 WHERE [ClientTabID]=1122 AND [ActionKey]='waterheaterheattrap'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=46 WHERE [ClientTabID]=1122 AND [ActionKey]='waterheaterinsulateelec'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=47 WHERE [ClientTabID]=1122 AND [ActionKey]='waterheaterinsulategas'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=48 WHERE [ClientTabID]=1122 AND [ActionKey]='waterheatertimerelec'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=49 WHERE [ClientTabID]=1122 AND [ActionKey]='waterheatertimergas'
UPDATE [cm].[ClientTabAction] SET [DisplayOrder]=52 WHERE [ClientTabID]=1122 AND [ActionKey]='xeroscapegrass'
PRINT(N'Operation applied to 17 rows out of 17')

PRINT(N'Update rows in [cm].[ClientProfileSectionCondition]')
UPDATE [cm].[ClientProfileSectionCondition] SET [DisplayOrder]=1 WHERE [ClientProfileSectionID]=188 AND [ConditionKey]='premisetype.residential'
UPDATE [cm].[ClientProfileSectionCondition] SET [DisplayOrder]=1 WHERE [ClientProfileSectionID]=191 AND [ConditionKey]='premisetype.residential'
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Update rows in [cm].[ClientWidget]')
UPDATE [cm].[ClientWidget] SET [Disable]=0 WHERE [ClientWidgetID]=459
UPDATE [cm].[ClientWidget] SET [Disable]=0 WHERE [ClientWidgetID]=469
UPDATE [cm].[ClientWidget] SET [Disable]=0 WHERE [ClientWidgetID]=471
PRINT(N'Operation applied to 3 rows out of 3')

PRINT(N'Update rows in [cm].[ClientTab]')
UPDATE [cm].[ClientTab] SET [MenuOrder]=1, [LayoutKey]='layout.2rows1column', [TabTypeKey]='sub', [URL]='C12', [TabIconClass]=NULL WHERE [ClientTabID]=1256
UPDATE [cm].[ClientTab] SET [MenuOrder]=2, [URL]='C2', [TabIconClass]='NULL', [Disable]=0 WHERE [ClientTabID]=1258
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Update row in [cm].[ClientProfileOption]')
UPDATE [cm].[ClientProfileOption] SET [Description]='2006 - 2010' WHERE [ClientProfileOptionID]=1164

PRINT(N'Update rows in [cm].[ClientConfigurationBulk]')
UPDATE [cm].[ClientConfigurationBulk] SET [JSONConfiguration]=N'{
 "ClientId": 290,
 "TimeZone": "CST",
 "IsAmiIntervalStart": true,
 "IsDstHandled": true,
 "CalculationScheduleTime": "",
 "NotificationReportScheduleTime": "05:00",
 "InsightReportScheduleTime": "06:00",
 "ForgotPasswordEmailTemplateId": "5917e8eb-d034-4558-a64e-f84e95bcce09",
 "RegistrationConfirmationEmailTemplateId": "63174e2c-99e9-4c1a-a363-c40171855b06",
 "OptInEmailConfirmationTemplateId": "957ab6ff-4028-4b7d-b5eb-47867b9b8777",
 "OptInSmsConfirmationTemplateId": "290_12",
 "OptInEmailUserName": "lusprod",
 "OptInEmailPassword": "Xlusprod2311X",
 "OptInEmailFrom": "myaccount@lus.org",
 "TrumpiaShortCode": "33221",
 "TrumpiaApiKey": "a42cab7d78472015f921d2af1a27bd21",
 "TrumpiaUserName": "aclaraprod",
 "TrumpiaContactList": "LUSProd",
 "UnmaskedAccountIdEndingDigit": "6",
 "Programs": [{
  "ProgramName": "Program1",
  "UtilityProgramName": "Program1",
  "FileExportRequired": true,
  "DoubleOptInRequired": true,
  "TrumpiaKeyword": "abagdasarian2",
  "Insights": [
   {
    "InsightName": "BillToDate",
    "UtilityInsightName": "BillToDate",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Daily",
    "DefaultValue": "",
    "EmailTemplateId": "d23f0fa7-d533-4d82-9687-7426ce2211d0",
    "SMSTemplateId": "290_1",
    "NotifyTime": "14:00",
    "NotificationDay": "Tuesday",
    "TrumpiaKeyword": "",
    "Level": "Account",
    "CommodityType": "",
    "ThresholdType": "",
    "ThresholdMin": "",
    "ThresholdMax": ""
   }, {
    "InsightName": "AccountLevelCostThreshold",
    "UtilityInsightName": "AccountLevelCostThreshold",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Immediate",
    "DefaultValue": "100",
    "EmailTemplateId": "0eb24acc-45f1-4ee9-9a54-5736255530dc",
    "SMSTemplateId": "290_6",
    "NotifyTime": "14:00",
    "NotificationDay": "",
    "TrumpiaKeyword": "",
    "Level": "Account",
    "CommodityType": "",
    "ThresholdType": "Cost",
    "ThresholdMin": "1",
    "ThresholdMax": "999999999"
   }, {
    "InsightName": "ServiceLevelCostThreshold",
    "UtilityInsightName": "ServiceLevelCostThreshold",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Immediate",
    "DefaultValue": "25,25",
    "EmailTemplateId": "0e41b8e1-0df2-4328-a4be-24d034f80a84",
    "SMSTemplateId": "290_8",
    "NotifyTime": "14:00",
    "NotificationDay": "",
    "TrumpiaKeyword": "",
    "Level": "Service",
    "CommodityType": "Electric,Water",
    "ThresholdType": "Cost",
    "ThresholdMin": "1",
    "ThresholdMax": "999999999"
   }, {
    "InsightName": "ServiceLevelUsageThreshold",
    "UtilityInsightName": "ServiceLevelUsageThreshold",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Immediate",
    "DefaultValue": "500,5",
    "EmailTemplateId": "2c7b7ff3-0be6-4e0a-acf6-79a07519cae1",
    "SMSTemplateId": "290_7",
    "NotifyTime": "14:00",
    "NotificationDay": "",
    "TrumpiaKeyword": "",
    "Level": "Service",
    "CommodityType": "Electric,Water",
    "ThresholdType": "Usage",
    "ThresholdMin": "0.1",
    "ThresholdMax": "999999999"
   }
  ]
 }]
}' WHERE [ClientConfigurationBulkID]=48
EXEC(N'UPDATE [cm].[ClientConfigurationBulk] SET [JSONConfiguration]=N''{
  "ClientId": 87,
  "TimeZone": "EST",
  "IsAmiIntervalStart": false,
  "IsDstHandled": false,
  "CalculationScheduleTime": "",
  "NotificationReportScheduleTime": "05:00",
  "InsightReportScheduleTime": "06:00",
  "ForgotPasswordEmailTemplateId": "dc2843e8-54ee-42de-8341-fe07716f7943",
  "RegistrationConfirmationEmailTemplateId": "72d3b00f-6b15-4465-89be-a50773a7aecd",
  "OptInEmailConfirmationTemplateId": "9aa83e4d-74d9-4447-8602-6f5e7d42f8c5",
  "OptInSmsConfirmationTemplateId": "87_12",
  "OptInEmailUserName": "aclarauat",
  "OptInEmailPassword": "Xaclarauat2311X",
  "OptInEmailFrom": "support@aclarax.com",
  "TrumpiaShortCode": "99000",
  "TrumpiaApiKey": "e8a29380eace0f25b22173da353ff1a3",
  "TrumpiaUserName": "aclarauat",
  "TrumpiaContactList": "AclaraDemoUat",
  "UnmaskedAccountIdEndingDigit": "4",
  "Programs": [
    {
      "ProgramName": "Program1",
      "UtilityProgramName": "Program1",
      "FileExportRequired": true,
      "DoubleOptInRequired": true,
      "TrumpiaKeyword": "abagdasarian2",
      "Insights": [
        {
          "InsightName": "BillToDate",
          "UtilityInsightName": "BillToDate",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Daily",
          "DefaultValue": "",
          "EmailTemplateId": "99b8da88-6d0e-44b9-b5d8-43b7716e7b02",
          "SMSTemplateId": "87_1",
          "NotifyTime": "20:00",
          "NotificationDay": "Monday",
          "TrumpiaKeyword": "",
          "Level": "Account",
          "CommodityType": "",
          "ThresholdType": "",
          "ThresholdMin": "",
          "ThresholdMax": ""
        },
        {
          "InsightName": "AccountProjectedCost",
          "UtilityInsightName": "AccountProjectedCost",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Daily",
          "DefaultValue": "",
          "EmailTemplateId": "c5faaa2b-ecb8-42b2-a002-db22540f8f64",
          "SMSTemplateId": "87_3",
          "NotifyTime": "20:00",
          "NotificationDay": "Monday",
          "TrumpiaKeyword": "",
          "Level": "Account",
          "CommodityType": "",
          "ThresholdType": "",
          "ThresholdMin": "",
          "ThresholdMax": ""
        },
        {
          "InsightName": "AccountLevelCostThreshold",
          "UtilityInsightName": "AccountLevelCostThreshold",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Immediate",
          "DefaultValue": "100",
          "EmailTemplateId": "62a4af14-6522-4da0-9e92-26eeec1f1eb1",
          "SMSTemplateId": "87_6",
          "NotifyTime": "",
          "NotificationDay": "",
          "TrumpiaKeyword": "",
          "Level": "Account",
          "CommodityType": "",
          "ThresholdType": "Cost",
          "ThresholdMin": "1",
          "ThresholdMax": "1000000"
        },
        {
          "InsightName": "ServiceLevelCostThreshold",
          "UtilityInsightName": "ServiceLevelCostThreshold",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Immediate",
          "DefaultValue": "25,25",
          "EmailTemplateId": "893a5ccc-9e9e-4c71-b93e-0839b37bf76c",
          "SMSTemplateId": "87_8",
          "NotifyTime": "",
          "NotificationDay": "",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Gas,Electric",
          "ThresholdType": "Cost",
          "ThresholdMin": "1",
          "ThresholdMax": "1000000"
        },
        {
          "InsightName": "ServiceLevelUsageThreshold",
          "UtilityInsightName": "ServiceLevelUsageThresho'' WHERE [ClientConfigurationBulkID]=51')
EXEC(N'UPDATE [cm].[ClientConfigurationBulk] SET [JSONConfiguration].WRITE(N''ld",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Immediate",
          "DefaultValue": "25,500",
          "EmailTemplateId": "e18510f6-71db-4529-9bb8-660acd35e313",
          "SMSTemplateId": "87_7",
          "NotifyTime": "",
          "NotificationDay": "",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Gas,Electric",
          "ThresholdType": "Usage",
          "ThresholdMin": "0.1",
          "ThresholdMax": "1000000"
        },
        {
          "InsightName": "CostToDate",
          "UtilityInsightName": "CostToDate",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Weekly",
          "DefaultValue": "",
          "EmailTemplateId": "10d66d92-780a-47a2-914e-997b9dfcbecf",
          "SMSTemplateId": "87_2",
          "NotifyTime": "20:00",
          "NotificationDay": "Monday",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Gas,Electric",
          "ThresholdType": "",
          "ThresholdMin": "",
          "ThresholdMax": ""
        },
        {
          "InsightName": "ServiceProjectedCost",
          "UtilityInsightName": "ServiceProjectedCost",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Daily",
          "DefaultValue": "",
          "EmailTemplateId": "a8f0164a-e83b-4bd9-b1d9-a03ece473f2a",
          "SMSTemplateId": "87_4",
          "NotifyTime": "20:00",
          "NotificationDay": "Monday",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Gas,Electric",
          "ThresholdType": "",
          "ThresholdMin": "",
          "ThresholdMax": ""
        },
        {
          "InsightName": "Usage",
          "UtilityInsightName": "Usage",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Daily",
          "DefaultValue": "",
          "EmailTemplateId": "b573e286-5c12-4664-977c-cf8f33ccd954",
          "SMSTemplateId": "87_5",
          "NotifyTime": "20:00",
          "NotificationDay": "Monday",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Gas,Electric",
          "ThresholdType": "",
          "ThresholdMin": "",
          "ThresholdMax": ""
        },
        {
          "InsightName": "DayThreshold",
          "UtilityInsightName": "DayThreshold",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Immediate",
          "DefaultValue": "1,16",
          "EmailTemplateId": "34982ac6-adb7-4c60-9fb0-fbe29a908888",
          "SMSTemplateId": "87_9",
          "NotifyTime": "",
          "NotificationDay": "",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Gas,Electric",
          "ThresholdType": "Usage",
          "ThresholdMin": "0.1",
          "ThresholdMax": "1000000"
        },
        {
          "InsightName": "ServiceLevelTieredThresholdApproaching",
          "UtilityInsightName": "ServiceLevelTieredThresholdApproaching",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Immediate",
          "DefaultValue": "20,20",
          "EmailTemplateId": "560115e4-1b98-46c1-bfc0-24c962a6dcee",
          "SMSTemplateId": "87_10",
          "NotifyTime": "",
          "NotificationDay": "",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Gas,Ele'',NULL,NULL) WHERE [ClientConfigurationBulkID]=51
UPDATE [cm].[ClientConfigurationBulk] SET [JSONConfiguration].WRITE(N''ctric",
          "ThresholdType": "Percent",
          "ThresholdMin": "1",
          "ThresholdMax": "100"
        },
        {
          "InsightName": "ServiceLevelTieredThresholdExceed",
          "UtilityInsightName": "ServiceLevelTieredThresholdExceed",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Immediate",
          "DefaultValue": "",
          "EmailTemplateId": "37df6357-84dd-476d-9ba8-62388866bd35",
          "SMSTemplateId": "87_11",
          "NotifyTime": "",
          "NotificationDay": "",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Gas,Electric",
          "ThresholdType": "",
          "ThresholdMin": "",
          "ThresholdMax": ""
        }
      ]
    },
    {
      "ProgramName": "Program2",
      "UtilityProgramName": "Program2",
      "FileExportRequired": false,
      "DoubleOptInRequired": true,
      "TrumpiaKeyword": "",
      "Insights": [
        {
          "InsightName": "ServiceLevelCostThreshold",
          "UtilityInsightName": "ServiceLevelCostThreshold",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Immediate",
          "DefaultValue": "25",
          "EmailTemplateId": "893a5ccc-9e9e-4c71-b93e-0839b37bf76c",
          "SMSTemplateId": "87_8",
          "NotifyTime": "",
          "NotificationDay": "",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Water",
          "ThresholdType": "Cost",
          "ThresholdMin": "1",
          "ThresholdMax": "1000000"
        },
        {
          "InsightName": "ServiceLevelUsageThreshold",
          "UtilityInsightName": "ServiceLevelUsageThreshold",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Immediate",
          "DefaultValue": "5",
          "EmailTemplateId": "e18510f6-71db-4529-9bb8-660acd35e313",
          "SMSTemplateId": "87_7",
          "NotifyTime": "",
          "NotificationDay": "",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Water",
          "ThresholdType": "Usage",
          "ThresholdMin": "0.1",
          "ThresholdMax": "1000000"
        },
        {
          "InsightName": "CostToDate",
          "UtilityInsightName": "CostToDate",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Weekly",
          "DefaultValue": "",
          "EmailTemplateId": "10d66d92-780a-47a2-914e-997b9dfcbecf",
          "SMSTemplateId": "87_2",
          "NotifyTime": "20:00",
          "NotificationDay": "Monday",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Water",
          "ThresholdType": "",
          "ThresholdMin": "",
          "ThresholdMax": ""
        },
        {
          "InsightName": "ServiceProjectedCost",
          "UtilityInsightName": "ServiceProjectedCost",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Daily",
          "DefaultValue": "",
          "EmailTemplateId": "a8f0164a-e83b-4bd9-b1d9-a03ece473f2a",
          "SMSTemplateId": "87_4",
          "NotifyTime": "20:00",
          "NotificationDay": "Monday",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Water",
          "ThresholdType": "",
          "ThresholdMin": "",
          "ThresholdMax": ""
        },
        {
          "InsightName": "Usage",
          "UtilityInsightName": "Usage",
          "AllowedCommunication'',NULL,NULL) WHERE [ClientConfigurationBulkID]=51
UPDATE [cm].[ClientConfigurationBulk] SET [JSONConfiguration].WRITE(N''Channel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Daily",
          "DefaultValue": "",
          "EmailTemplateId": "b573e286-5c12-4664-977c-cf8f33ccd954",
          "SMSTemplateId": "87_5",
          "NotifyTime": "20:00",
          "NotificationDay": "Monday",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Water",
          "ThresholdType": "",
          "ThresholdMin": "",
          "ThresholdMax": ""
        },
        {
          "InsightName": "DayThreshold",
          "UtilityInsightName": "DayThreshold",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Immediate",
          "DefaultValue": "0.15",
          "EmailTemplateId": "34982ac6-adb7-4c60-9fb0-fbe29a908888",
          "SMSTemplateId": "87_9",
          "NotifyTime": "",
          "NotificationDay": "",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Water",
          "ThresholdType": "Usage",
          "ThresholdMin": "0.1",
          "ThresholdMax": "1000000"
        },
        {
          "InsightName": "ServiceLevelTieredThresholdApproaching",
          "UtilityInsightName": "ServiceLevelTieredThresholdApproaching",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Immediate",
          "DefaultValue": "20",
          "EmailTemplateId": "560115e4-1b98-46c1-bfc0-24c962a6dcee",
          "SMSTemplateId": "87_10",
          "NotifyTime": "",
          "NotificationDay": "",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Water",
          "ThresholdType": "Percent",
          "ThresholdMin": "1",
          "ThresholdMax": "100"
        },
        {
          "InsightName": "ServiceLevelTieredThresholdExceed",
          "UtilityInsightName": "ServiceLevelTieredThresholdExceed",
          "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
          "DefaultCommunicationChannel": "Email",
          "DefaultFrequency": "Immediate",
          "DefaultValue": "",
          "EmailTemplateId": "37df6357-84dd-476d-9ba8-62388866bd35",
          "SMSTemplateId": "87_11",
          "NotifyTime": "",
          "NotificationDay": "",
          "TrumpiaKeyword": "",
          "Level": "Service",
          "CommodityType": "Water",
          "ThresholdType": "",
          "ThresholdMin": "",
          "ThresholdMax": ""
        }
      ]
    }
  ],
  "ExtendedAttributes": "normalDecimalFormat=0.00,altNumericFormat=0",
  "AlterInformation": "apply changes indicated"
}'',NULL,NULL) WHERE [ClientConfigurationBulkID]=51
')
UPDATE [cm].[ClientConfigurationBulk] SET [JSONConfiguration]=N'{
 "ClientId": 290,
 "TimeZone": "CST",
 "IsAmiIntervalStart": true,
 "IsDstHandled": true,
 "CalculationScheduleTime": "",
 "NotificationReportScheduleTime": "05:00",
 "InsightReportScheduleTime": "06:00",
 "ForgotPasswordEmailTemplateId": "af5c0251-486f-4100-a4cf-2d91a905f108",
 "RegistrationConfirmationEmailTemplateId": "7c0657cd-1020-42a4-a281-87db82a7f6c4",
 "OptInEmailConfirmationTemplateId": "777ca9f8-bb03-490b-946b-b1e6991b2b15",
 "OptInSmsConfirmationTemplateId": "290_12",
 "OptInEmailUserName": "aclaradev",
 "OptInEmailPassword": "Xaclaradev2311X",
 "OptInEmailFrom": "support@aclarax.com",
 "TrumpiaShortCode": "33221",
 "TrumpiaApiKey": "b4c1971153b3509b6ec0d8a24a33454c",
 "TrumpiaUserName": "aclaradev",
 "TrumpiaContactList": "LUSDev",
 "UnmaskedAccountIdEndingDigit": "6",
 "Programs": [{
  "ProgramName": "Program1",
  "UtilityProgramName": "Program1",
  "FileExportRequired": true,
  "DoubleOptInRequired": true,
  "TrumpiaKeyword": "abagdasarian2",
  "Insights": [
   {
    "InsightName": "BillToDate",
    "UtilityInsightName": "BillToDate",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Daily",
    "DefaultValue": "",
    "EmailTemplateId": "c8d5e21f-1150-4b3d-b4b8-ad869d89a040",
    "SMSTemplateId": "290_1",
    "NotifyTime": "20:00",
    "NotificationDay": "Monday",
    "TrumpiaKeyword": "",
    "Level": "Account",
    "CommodityType": "",
    "ThresholdType": "",
    "ThresholdMin": "",
    "ThresholdMax": ""
   }, {
    "InsightName": "AccountLevelCostThreshold",
    "UtilityInsightName": "AccountLevelCostThreshold",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Immediate",
    "DefaultValue": "100",
    "EmailTemplateId": "7b3f9981-ae52-4fd9-b223-8ae516f6f82f",
    "SMSTemplateId": "290_6",
    "NotifyTime": "",
    "NotificationDay": "",
    "TrumpiaKeyword": "",
    "Level": "Account",
    "CommodityType": "",
    "ThresholdType": "Cost",
    "ThresholdMin": "1",
    "ThresholdMax": "999999999"
   }, {
    "InsightName": "ServiceLevelCostThreshold",
    "UtilityInsightName": "ServiceLevelCostThreshold",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Immediate",
    "DefaultValue": "25,25",
    "EmailTemplateId": "87791eaa-0342-45d5-85d4-4645f7a85f7d",
    "SMSTemplateId": "290_8",
    "NotifyTime": "",
    "NotificationDay": "",
    "TrumpiaKeyword": "",
    "Level": "Service",
    "CommodityType": "Electric,Water",
    "ThresholdType": "Cost",
    "ThresholdMin": "1",
    "ThresholdMax": "999999999"
   }, {
    "InsightName": "ServiceLevelUsageThreshold",
    "UtilityInsightName": "ServiceLevelUsageThreshold",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Immediate",
    "DefaultValue": "500,5",
    "EmailTemplateId": "f7791cc2-9665-4d4b-8a7f-9421daf55e29",
    "SMSTemplateId": "290_7",
    "NotifyTime": "",
    "NotificationDay": "",
    "TrumpiaKeyword": "",
    "Level": "Service",
    "CommodityType": "Electric,Water",
    "ThresholdType": "Usage",
    "ThresholdMin": "0.1",
    "ThresholdMax": "999999999"
   }
  ]
 }]
}' WHERE [ClientConfigurationBulkID]=58
UPDATE [cm].[ClientConfigurationBulk] SET [JSONConfiguration]=N'{
 "ClientId": 290,
 "TimeZone": "CST",
 "IsAmiIntervalStart": true,
 "IsDstHandled": true,
 "CalculationScheduleTime": "",
 "NotificationReportScheduleTime": "05:00",
 "InsightReportScheduleTime": "06:00",
 "ForgotPasswordEmailTemplateId": "ba32bfe8-5d14-4310-ac2f-ca50d51af988",
 "RegistrationConfirmationEmailTemplateId": "ab52d679-306a-4bd8-a968-94840da1a03d",
 "OptInEmailConfirmationTemplateId": "8607c228-8be5-4d70-bb77-d8a235da6b58",
 "OptInSmsConfirmationTemplateId": "290_12",
 "OptInEmailUserName": "aclaraqa",
 "OptInEmailPassword": "Xaclaraqa2311X",
 "OptInEmailFrom": "support@aclarax.com",
 "TrumpiaShortCode": "33221",
 "TrumpiaApiKey": "6b1ea603b836084b30b5bdedc72769ce",
 "TrumpiaUserName": "aclaraqa",
 "TrumpiaContactList": "LUSQa",
 "UnmaskedAccountIdEndingDigit": "6",
 "Programs": [{
  "ProgramName": "Program1",
  "UtilityProgramName": "Program1",
  "FileExportRequired": true,
  "DoubleOptInRequired": true,
  "TrumpiaKeyword": "abagdasarian2",
  "Insights": [
   {
    "InsightName": "BillToDate",
    "UtilityInsightName": "BillToDate",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Daily",
    "DefaultValue": "",
    "EmailTemplateId": "84b5415e-26b5-4216-961a-92bce5d830ff",
    "SMSTemplateId": "290_1",
    "NotifyTime": "20:00",
    "NotificationDay": "Tuesday",
    "TrumpiaKeyword": "",
    "Level": "Account",
    "CommodityType": "",
    "ThresholdType": "",
    "ThresholdMin": "",
    "ThresholdMax": ""
   }, {
    "InsightName": "AccountLevelCostThreshold",
    "UtilityInsightName": "AccountLevelCostThreshold",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Immediate",
    "DefaultValue": "100",
    "EmailTemplateId": "2dd1d286-19f8-4881-a5b6-939629db8174",
    "SMSTemplateId": "290_6",
    "NotifyTime": "",
    "NotificationDay": "",
    "TrumpiaKeyword": "",
    "Level": "Account",
    "CommodityType": "",
    "ThresholdType": "Cost",
    "ThresholdMin": "1",
    "ThresholdMax": "999999999"
   }, {
    "InsightName": "ServiceLevelCostThreshold",
    "UtilityInsightName": "ServiceLevelCostThreshold",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Immediate",
    "DefaultValue": "25,25",
    "EmailTemplateId": "f5369cb1-8892-466b-959f-15b95e4b22a4",
    "SMSTemplateId": "290_8",
    "NotifyTime": "",
    "NotificationDay": "",
    "TrumpiaKeyword": "",
    "Level": "Service",
    "CommodityType": "Electric,Water",
    "ThresholdType": "Cost",
    "ThresholdMin": "1",
    "ThresholdMax": "999999999"
   }, {
    "InsightName": "ServiceLevelUsageThreshold",
    "UtilityInsightName": "ServiceLevelUsageThreshold",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Immediate",
    "DefaultValue": "500,5",
    "EmailTemplateId": "86bb0d05-5409-41df-bf8a-6e1947aa252e",
    "SMSTemplateId": "290_7",
    "NotifyTime": "",
    "NotificationDay": "",
    "TrumpiaKeyword": "",
    "Level": "Service",
    "CommodityType": "Electric,Water",
    "ThresholdType": "Usage",
    "ThresholdMin": "0.1",
    "ThresholdMax": "999999999"
   }
  ]
 }]
}' WHERE [ClientConfigurationBulkID]=59
UPDATE [cm].[ClientConfigurationBulk] SET [JSONConfiguration]=N'{
 "ClientId": 290,
 "TimeZone": "CST",
 "IsAmiIntervalStart": true,
 "IsDstHandled": true,
 "CalculationScheduleTime": "",
 "NotificationReportScheduleTime": "05:00",
 "InsightReportScheduleTime": "06:00",
 "ForgotPasswordEmailTemplateId": "2c11264a-853c-4579-b5cc-e98502e01fa5",
 "RegistrationConfirmationEmailTemplateId": "e3c1725f-3547-42f4-bebf-afbf1646bcdf",
 "OptInEmailConfirmationTemplateId": "f90751ec-a1cb-4447-b9e3-9e5f637d97c4",
 "OptInSmsConfirmationTemplateId": "290_12",
 "OptInEmailUserName": "lusuat",
 "OptInEmailPassword": "Xlusuat2311X",
 "OptInEmailFrom": "myaccount@lus.org",
 "TrumpiaShortCode": "33221",
 "TrumpiaApiKey": "e8a29380eace0f25b22173da353ff1a3",
 "TrumpiaUserName": "aclarauat",
 "TrumpiaContactList": "LUSUat",
 "UnmaskedAccountIdEndingDigit": "6",
 "Programs": [{
  "ProgramName": "Program1",
  "UtilityProgramName": "Program1",
  "FileExportRequired": true,
  "DoubleOptInRequired": true,
  "TrumpiaKeyword": "abagdasarian2",
  "Insights": [
   {
    "InsightName": "BillToDate",
    "UtilityInsightName": "BillToDate",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Daily",
    "DefaultValue": "",
    "EmailTemplateId": "99974fb2-3b36-4893-bc89-ee5d04b29a56",
    "SMSTemplateId": "290_1",
    "NotifyTime": "14:00",
    "NotificationDay": "Tuesday",
    "TrumpiaKeyword": "",
    "Level": "Account",
    "CommodityType": "",
    "ThresholdType": "",
    "ThresholdMin": "",
    "ThresholdMax": ""
   }, {
    "InsightName": "AccountLevelCostThreshold",
    "UtilityInsightName": "AccountLevelCostThreshold",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Immediate",
    "DefaultValue": "100",
    "EmailTemplateId": "345df79d-d689-4674-a477-ded90b7008a7",
    "SMSTemplateId": "290_6",
    "NotifyTime": "14:00",
    "NotificationDay": "",
    "TrumpiaKeyword": "",
    "Level": "Account",
    "CommodityType": "",
    "ThresholdType": "Cost",
    "ThresholdMin": "1",
    "ThresholdMax": "999999999"
   }, {
    "InsightName": "ServiceLevelCostThreshold",
    "UtilityInsightName": "ServiceLevelCostThreshold",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Immediate",
    "DefaultValue": "25,25",
    "EmailTemplateId": "a8316040-2ae9-459b-b062-b3c7d5937af0",
    "SMSTemplateId": "290_8",
    "NotifyTime": "14:00",
    "NotificationDay": "",
    "TrumpiaKeyword": "",
    "Level": "Service",
    "CommodityType": "Electric,Water",
    "ThresholdType": "Cost",
    "ThresholdMin": "1",
    "ThresholdMax": "999999999"
   }, {
    "InsightName": "ServiceLevelUsageThreshold",
    "UtilityInsightName": "ServiceLevelUsageThreshold",
    "AllowedCommunicationChannel": "File,Email,SMS,EmailandSMS",
    "DefaultCommunicationChannel": "Email",
    "DefaultFrequency": "Immediate",
    "DefaultValue": "500,5",
    "EmailTemplateId": "12c8ea14-a916-433b-afe9-3efcb894ff0d",
    "SMSTemplateId": "290_7",
    "NotifyTime": "14:00",
    "NotificationDay": "",
    "TrumpiaKeyword": "",
    "Level": "Service",
    "CommodityType": "Electric,Water",
    "ThresholdType": "Usage",
    "ThresholdMin": "0.1",
    "ThresholdMax": "999999999"
   }
  ]
 }]
}' WHERE [ClientConfigurationBulkID]=60
PRINT(N'Operation applied to 5 rows out of 5')

PRINT(N'Update rows in [cm].[ClientConfiguration]')
UPDATE [cm].[ClientConfiguration] SET [Description]='The color to use on the line or bar for the most recent 12 months of usage' WHERE [ClientConfigurationID]=1583
UPDATE [cm].[ClientConfiguration] SET [Description]='The color to use on the line or bar for the previous 12 months of usage.' WHERE [ClientConfigurationID]=1584
UPDATE [cm].[ClientConfiguration] SET [Value]='true' WHERE [ClientConfigurationID]=2942
UPDATE [cm].[ClientConfiguration] SET [Description]='The color to use on the line or bar for the most recent 12 months of usage' WHERE [ClientConfigurationID]=2965
UPDATE [cm].[ClientConfiguration] SET [Description]='The color to use on the line or bar for the previous 12 months of usage.' WHERE [ClientConfigurationID]=2966
UPDATE [cm].[ClientConfiguration] SET [Value]='.csv' WHERE [ClientConfigurationID]=3139
UPDATE [cm].[ClientConfiguration] SET [Value]='.csv' WHERE [ClientConfigurationID]=3140
PRINT(N'Operation applied to 7 rows out of 7')

PRINT(N'Update row in [cm].[ClientAction]')
UPDATE [cm].[ClientAction] SET [RebateExpression]=NULL, [IconClass]='icon-money' WHERE [ClientActionID]=1246

PRINT(N'Update rows in [cm].[TypeTextContent]')
UPDATE [cm].[TypeTextContent] SET [TextContentID]=4230 WHERE [TextContentKey]='action.shortershowers.desc'
UPDATE [cm].[TypeTextContent] SET [TextContentID]=4231 WHERE [TextContentKey]='action.shortershowers.name'
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Add row to [cm].[TypeCategory]')
INSERT INTO [cm].[TypeCategory] ([CategoryKey], [CategoryID]) VALUES ('wsbenchmark', 28)

PRINT(N'Add rows to [cm].[TypeConfiguration]')
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.chartanchortype', 2407)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.chartlinecolor1', 2408)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.chartlinecolor2', 2409)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.chartlinecolor3', 2410)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.chartmonthformat', 2411)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.charttype', 2412)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.commodities', 2413)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.currencyformat', 2414)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.enableweather', 2415)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.footerlink', 2416)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.footerlinktype', 2417)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.hoverdateformat', 2418)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.hoverelectricusageformat', 2419)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.hovergasusageformat', 2420)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.hoverwaterusageformat', 2421)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.linethickness', 2422)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.showcommoditylabels', 2489)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.showfooterlink', 2423)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.showfootertext', 2424)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.showintro', 2425)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.showsubtitle', 2426)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.showtitle', 2427)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusage.billedusagechart.showweatherbydefault', 2428)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.chartanchortype', 2464)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.chartlinecolor1', 2465)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.chartlinecolor2', 2466)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.chartlinecolor3', 2467)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.chartmonthformat', 2468)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.charttype', 2469)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.commodities', 2470)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.currencyformat', 2471)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.enableweather', 2472)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.footerlink', 2473)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.footerlinktype', 2474)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.hoverdateformat', 2475)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.hoverelectricusageformat', 2476)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.hovergasusageformat', 2477)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.hoverwaterusageformat', 2478)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.linethickness', 2479)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.showcommoditylabels', 2490)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.showfooterlink', 2480)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.showfootertext', 2481)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.showintro', 2482)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.showsubtitle', 2483)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.showtitle', 2484)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechart.showweatherbydefault', 2485)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechartchartanchortype', 2429)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechartchartlinecolor1', 2430)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechartchartlinecolor2', 2431)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechartchartmonthformat', 2432)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechartcommodities', 2433)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechartcurrencyformat', 2434)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechartfooterlink', 2435)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechartfooterlinktype', 2436)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagecharthoverdateformat', 2437)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagecharthoverelectricusageformat', 2438)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagecharthovergasusageformat', 2439)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagecharthoverwaterusageformat', 2440)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechartlinethickness', 2441)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechartshowfooterlink', 2442)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechartshowfootertext', 2443)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.billedusagebusiness.billedusagechartshowtitle', 2444)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.myusage.greenbutton.hidebilldownload', 2404)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.myusage.greenbutton.hideusagedownload', 2405)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofile.profile.footerlink', 2446)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofile.profile.footerlinktype', 2447)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofile.profile.longprofilelink', 2448)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofile.profile.showfooterlink', 2449)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofile.profile.showfootertext', 2450)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofile.profile.showlongprofilelink', 2451)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofile.profile.showrequiredkey', 2452)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofile.profile.showtitle', 2453)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofilebusiness.profile.footerlink', 2454)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofilebusiness.profile.footerlinktype', 2455)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofilebusiness.profile.longprofilelink', 2456)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofilebusiness.profile.showfooterlink', 2457)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofilebusiness.profile.showfootertext', 2458)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofilebusiness.profile.showintro', 2459)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofilebusiness.profile.showlongprofilelink', 2460)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofilebusiness.profile.showrequiredkey', 2461)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('tab.shortenergyprofilebusiness.profile.showtitle', 2462)
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('weather.sensitivity.shardname', 2487)
PRINT(N'Operation applied to 82 rows out of 82')

PRINT(N'Add row to [cm].[TypeProfileAttribute]')
INSERT INTO [cm].[TypeProfileAttribute] ([ProfileAttributeKey], [ProfileAttributeID], [ProfileAttributeTypeKey], [EntityLevelKey], [Description]) VALUES ('house.floors.insulated', 637, 'boolean', 'premise', 'Are your walls insulated?')

PRINT(N'Add rows to [cm].[TypeProfileOption]')
INSERT INTO [cm].[TypeProfileOption] ([ProfileOptionKey], [ProfileOptionID]) VALUES ('house.floorsinsulated.no', 1882)
INSERT INTO [cm].[TypeProfileOption] ([ProfileOptionKey], [ProfileOptionID]) VALUES ('house.floorsinsulated.yes', 1883)
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Add row to [cm].[TypeProfileSection]')
INSERT INTO [cm].[TypeProfileSection] ([ProfileSectionKey], [ProfileSectionID]) VALUES ('profilesection.otherappliance', 103)

PRINT(N'Add rows to [cm].[TypeTab]')
INSERT INTO [cm].[TypeTab] ([TabKey], [TabID]) VALUES ('tab.billedusage', 94)
INSERT INTO [cm].[TypeTab] ([TabKey], [TabID]) VALUES ('tab.billedusagebusiness', 95)
INSERT INTO [cm].[TypeTab] ([TabKey], [TabID]) VALUES ('tab.billedusagechart', 101)
INSERT INTO [cm].[TypeTab] ([TabKey], [TabID]) VALUES ('tab.billedusagechartbusiness', 102)
INSERT INTO [cm].[TypeTab] ([TabKey], [TabID]) VALUES ('tab.insulationwindowdoor', 92)
INSERT INTO [cm].[TypeTab] ([TabKey], [TabID]) VALUES ('tab.mybilledusage', 96)
INSERT INTO [cm].[TypeTab] ([TabKey], [TabID]) VALUES ('tab.myshortprofile', 97)
INSERT INTO [cm].[TypeTab] ([TabKey], [TabID]) VALUES ('tab.shortenergyprofile', 98)
INSERT INTO [cm].[TypeTab] ([TabKey], [TabID]) VALUES ('tab.shortenergyprofilebusiness', 99)
PRINT(N'Operation applied to 9 rows out of 9')

PRINT(N'Add rows to [cm].[TypeTextContent]')
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('billedusage.billedusagechart.title', 4204)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('billedusagebusiness.billedusagechart.title', 4205)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('house.floorsinsulated.question', 4285)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('profilesection.fireplace.desc', 4286)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('profilesection.fireplace.subtitle', 4287)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('profilesection.fireplace.title', 4288)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('profilesection.otherappliance.desc', 4289)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('profilesection.otherappliance.subtitle', 4290)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('profilesection.otherappliance.title', 4291)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('shortenergyprofile.profile.title', 4206)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('shortenergyprofilebusiness.profile.title', 4207)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.billedusage.name', 4208)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.billedusagebusiness.name', 4209)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.billedusagechart', 4215)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.billedusagechart.name', 4218)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.billedusagechartbusiness', 4216)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.billedusagechartbusiness.name', 4219)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmcontacts.contacts.acceptedaccounts', 4233)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmcontacts.contacts.footer', 4234)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmcontacts.contacts.loading', 4235)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmcontacts.contacts.norecordsfound', 4236)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmcontacts.contacts.noselection', 4237)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmcontacts.contacts.pendingaccounts', 4238)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmcontacts.contacts.rejectconfirmationcancel', 4239)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmcontacts.contacts.rejectconfirmationconfirm', 4240)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmcontacts.contacts.rejectconfirmationmessage', 4241)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmcontacts.contacts.rejectconfirmationtitle', 4242)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.acceptedmeters', 4243)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.acceptmeters', 4244)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.accountaddress', 4245)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.accountid', 4246)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.accountjobtitle', 4247)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.accountname', 4248)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.accountphone', 4249)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.customfield1column', 4250)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.customfield2column', 4251)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.firstbilldate', 4252)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.footer', 4253)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.inuse', 4254)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.loading', 4255)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.metered', 4256)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.meterid', 4257)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.metername', 4258)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.metersintrotext', 4259)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.meterstitle', 4260)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.metertype', 4261)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.norecordsfound', 4262)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.noselection', 4263)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.pendingmeters', 4264)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.propertyaddress', 4265)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.propertybuildings', 4266)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.propertyconstructionstatus', 4267)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.propertyearbuilt', 4268)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.propertyfloorarea', 4269)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.propertyid', 4270)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.propertyisfederal', 4271)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.propertyname', 4272)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.propertyoccupancypercent', 4273)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.propertyprimaryfunction', 4274)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.rejectcancel', 4275)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.rejectconfirm', 4276)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.rejectconfirmationmessage', 4277)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.rejectconfirmationtitle', 4278)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.rejectmeters', 4279)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.timestamp', 4280)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmmeters.meters.unitofmeasure', 4281)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmportals.tab.contacts', 4282)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.espmportals.tab.meters', 4283)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.insulationwindowdoor.name', 4202)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.mybilledusage.name', 4210)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.myshortprofile.name', 4211)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.shortenergyprofile', 4221)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.shortenergyprofile.name', 4224)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.shortenergyprofile.profile.nextbuttonlabel', 4227)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.shortenergyprofilebusiness', 4222)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.shortenergyprofilebusiness.name', 4225)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.shortenergyprofilebusiness.profile.nextbuttonlabel', 4228)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.shortprofile.name', 4212)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('tab.shortprofilebusiness.name', 4213)
PRINT(N'Operation applied to 79 rows out of 79')

PRINT(N'Add rows to [cm].[TypeWidget]')
INSERT INTO [cm].[TypeWidget] ([WidgetKey], [WidgetID]) VALUES ('billedusage.billedusagechart', 134)
INSERT INTO [cm].[TypeWidget] ([WidgetKey], [WidgetID]) VALUES ('billedusagebusiness.billedusagechart', 135)
INSERT INTO [cm].[TypeWidget] ([WidgetKey], [WidgetID]) VALUES ('shortenergyprofile.profile', 136)
INSERT INTO [cm].[TypeWidget] ([WidgetKey], [WidgetID]) VALUES ('shortenergyprofilebusiness.profile', 137)
PRINT(N'Operation applied to 4 rows out of 4')

PRINT(N'Add rows to [admin].[ETLBulkConfiguration]')
INSERT INTO [admin].[ETLBulkConfiguration] ([ClientID], [Environment], [ClientComponent], [XMLConfiguration]) VALUES (87, 'production', 'InsightsDW.ImportData', CONVERT(xml,N'<InsightsDW.ImportData><Shard>InsightsDW_Test</Shard><ETLAvoidanceStartTime>23:00:00.000</ETLAvoidanceStartTime><ETLAvoidanceEndTime>24:00:00.000</ETLAvoidanceEndTime><ErrorLogFile>\\azuPfil001\PaaS_CloudFileDrop\87\Import\BI\InsightsDW.ImportData.Errors.csv</ErrorLogFile><ImportSpecificLoggingOn>true</ImportSpecificLoggingOn><FileOutputExtension>log</FileOutputExtension><CustomerFilePath>\\azuPfil001\PaaS_CloudFileDrop\87\Import\BI\CustomerData\</CustomerFilePath><CustomerFileSearchPattern>customer*.xml</CustomerFileSearchPattern><CustomerArchivePath>\\azuPfil001\PaaS_CloudFileDrop\87\Import\BI\CustomerData\archive\</CustomerArchivePath><CustomerXSD>customerinfo.xsd</CustomerXSD><BillFilePath>\\azuPfil001\PaaS_CloudFileDrop\87\Import\BI\BillingData\</BillFilePath><BillFileSearchPattern>bill*.xml</BillFileSearchPattern><BillArchivePath>\\azuPfil001\PaaS_CloudFileDrop\87\Import\BI\BillingData\archive\</BillArchivePath><BillingXSD>billingdata.xsd</BillingXSD><ProfileArchivePath>\\azuPfil001\PaaS_CloudFileDrop\87\Import\BI\ProfileData\archive\</ProfileArchivePath><ProfileFilePath>\\azuPfil001\PaaS_CloudFileDrop\87\Import\BI\ProfileData\</ProfileFilePath><ProfileFileSearchPattern>profile*.xml</ProfileFileSearchPattern><ProfileXSD>profile.xsd</ProfileXSD><ActionItemArchivePath>\\azuPfil001\PaaS_CloudFileDrop\87\Import\BI\ActionItemData\archive\</ActionItemArchivePath><ActionItemFilePath>\\azuPfil001\PaaS_CloudFileDrop\87\Import\BI\Actionitemdata\</ActionItemFilePath><ActionItemFileSearchPattern>actionitem*.xml</ActionItemFileSearchPattern><ActionItemXSD>actionitems.xsd</ActionItemXSD></InsightsDW.ImportData>',1))
INSERT INTO [admin].[ETLBulkConfiguration] ([ClientID], [Environment], [ClientComponent], [XMLConfiguration]) VALUES (101, 'production', 'InsightsDW.ImportData', CONVERT(xml,N'<InsightsDW.ImportData><Shard>InsightsDW_101</Shard><ETLAvoidanceStartTime>23:00:00.000</ETLAvoidanceStartTime><ETLAvoidanceEndTime>24:00:00.000</ETLAvoidanceEndTime><FileOutputExtension>log</FileOutputExtension><ErrorLogFile>\\azuPfil001\PaaS_CloudFileDrop\101\Import\BI\InsightsDW.ImportData.Errors.csv</ErrorLogFile><ImportSpecificLoggingOn>true</ImportSpecificLoggingOn><CustomerFilePath>\\azuPfil001\PaaS_CloudFileDrop\101\Import\BI\CustomerData\</CustomerFilePath><CustomerFileSearchPattern>customer*.xml</CustomerFileSearchPattern><CustomerArchivePath>\\azuPfil001\PaaS_CloudFileDrop\101\Import\BI\CustomerData\archive\</CustomerArchivePath><CustomerXSD>customerinfo.xsd</CustomerXSD><BillFilePath>\\azuPfil001\PaaS_CloudFileDrop\101\Import\BI\BillingData\</BillFilePath><BillFileSearchPattern>bill*.xml</BillFileSearchPattern><BillArchivePath>\\azuPfil001\PaaS_CloudFileDrop\101\Import\BI\BillingData\archive\</BillArchivePath><BillingXSD>billingdata.xsd</BillingXSD><ProfileArchivePath>\\azuPfil001\PaaS_CloudFileDrop\101\Import\BI\ProfileData\archive\</ProfileArchivePath><ProfileFilePath>\\azuPfil001\PaaS_CloudFileDrop\101\Import\BI\ProfileData\</ProfileFilePath><ProfileFileSearchPattern>profile*.xml</ProfileFileSearchPattern><ProfileXSD>profile.xsd</ProfileXSD></InsightsDW.ImportData>',1))
INSERT INTO [admin].[ETLBulkConfiguration] ([ClientID], [Environment], [ClientComponent], [XMLConfiguration]) VALUES (224, 'production', 'InsightsDW.ImportData', CONVERT(xml,N'<InsightsDW.ImportData><Shard>InsightsDW_224</Shard><ETLAvoidanceStartTime>23:00:00.000</ETLAvoidanceStartTime><ETLAvoidanceEndTime>24:00:00.000</ETLAvoidanceEndTime><FileOutputExtension>log</FileOutputExtension><ErrorLogFile>\\azuPfil001\PaaS_CloudFileDrop\224\Import\BI\InsightsDW.ImportData.Errors.csv</ErrorLogFile><ImportSpecificLoggingOn>true</ImportSpecificLoggingOn><CustomerFilePath>\\azuPfil001\PaaS_CloudFileDrop\224\Import\BI\CustomerData\</CustomerFilePath><CustomerArchivePath>\\azuPfil001\PaaS_CloudFileDrop\224\Import\BI\CustomerData\archive\</CustomerArchivePath><CustomerFileSearchPattern>aclara_cdw_customer*.xml</CustomerFileSearchPattern><CustomerXSD>customerinfo.xsd</CustomerXSD><BillFilePath>\\azuPfil001\PaaS_CloudFileDrop\224\Import\BI\BillingData\</BillFilePath><BillArchivePath>\\azuPfil001\PaaS_CloudFileDrop\224\Import\BI\BillingData\archive\</BillArchivePath><BillFileSearchPattern>aclara_cdw_sap*.xml</BillFileSearchPattern><BillingXSD>billingdata.xsd</BillingXSD><ProfileArchivePath>\\azuPfil001\PaaS_CloudFileDrop\224\Import\BI\ProfileData\archive\</ProfileArchivePath><ProfileFilePath>\\azuPfil001\PaaS_CloudFileDrop\224\Import\BI\ProfileData\</ProfileFilePath><ProfileFileSearchPattern>aclara_cdw_profile*.xml</ProfileFileSearchPattern><ProfileXSD>profile.xsd</ProfileXSD><ActionItemArchivePath>\\azuPfil001\PaaS_CloudFileDrop\224\Import\BI\ActionItemData\archive\</ActionItemArchivePath><ActionItemFilePath>\\azuPfil001\PaaS_CloudFileDrop\224\Import\BI\Actionitemdata\</ActionItemFilePath><ActionItemFileSearchPattern>aclara_cdw_actions*.xml</ActionItemFileSearchPattern><ActionItemXSD>actionitems.xsd</ActionItemXSD></InsightsDW.ImportData>',1))
INSERT INTO [admin].[ETLBulkConfiguration] ([ClientID], [Environment], [ClientComponent], [XMLConfiguration]) VALUES (256, 'production', 'InsightsDW.ImportData', CONVERT(xml,N'<InsightsDW.ImportData><Shard>InsightsDW_Test</Shard><ETLAvoidanceStartTime>23:00:00.000</ETLAvoidanceStartTime><ETLAvoidanceEndTime>24:00:00.000</ETLAvoidanceEndTime><FileOutputExtension>log</FileOutputExtension><ErrorLogFile>\\azuPfil001\PaaS_CloudFileDrop\256\Import\BI\InsightsDW.ImportData.Errors.csv</ErrorLogFile><ImportSpecificLoggingOn>true</ImportSpecificLoggingOn><CustomerFilePath>\\azuPfil001\PaaS_CloudFileDrop\256\Import\BI\CustomerData\</CustomerFilePath><CustomerArchivePath>\\azuPfil001\PaaS_CloudFileDrop\256\Import\BI\CustomerData\archive\</CustomerArchivePath><CustomerFileSearchPattern>customer*.xml</CustomerFileSearchPattern><CustomerXSD>customerinfo.xsd</CustomerXSD><BillFilePath>\\azuPfil001\PaaS_CloudFileDrop\256\Import\BI\BillingData\</BillFilePath><BillArchivePath>\\azuPfil001\PaaS_CloudFileDrop\256\Import\BI\BillingData\archive\</BillArchivePath><BillFileSearchPattern>bill*.xml</BillFileSearchPattern><BillingXSD>billingdata.xsd</BillingXSD><ProfileArchivePath>\\azuPfil001\PaaS_CloudFileDrop\256\Import\BI\ProfileData\archive\</ProfileArchivePath><ProfileFilePath>\\azuPfil001\PaaS_CloudFileDrop\256\Import\BI\ProfileData\</ProfileFilePath><ProfileFileSearchPattern>profile*.xml</ProfileFileSearchPattern><ProfileXSD>profile.xsd</ProfileXSD><ActionItemArchivePath>\\azuPfil001\PaaS_CloudFileDrop\256\Import\BI\ActionItemData\archive\</ActionItemArchivePath><ActionItemFilePath>\\azuPfil001\PaaS_CloudFileDrop\256\Import\BI\ActionItemData\</ActionItemFilePath><ActionItemFileSearchPattern>actionitem*.xml</ActionItemFileSearchPattern><ActionItemXSD>actionitems.xsd</ActionItemXSD></InsightsDW.ImportData>',1))
INSERT INTO [admin].[ETLBulkConfiguration] ([ClientID], [Environment], [ClientComponent], [XMLConfiguration]) VALUES (276, 'production', 'InsightsDW.ImportData', CONVERT(xml,N'<InsightsDW.ImportData><Shard>InsightsDW_276</Shard><ETLAvoidanceStartTime>23:00:00.000</ETLAvoidanceStartTime><ETLAvoidanceEndTime>24:00:00.000</ETLAvoidanceEndTime><FileOutputExtension>log</FileOutputExtension><ErrorLogFile>\\azuPfil001\PaaS_CloudFileDrop\276\Import\BI\InsightsDW.ImportData.Errors.csv</ErrorLogFile><ImportSpecificLoggingOn>true</ImportSpecificLoggingOn><CustomerFilePath>\\azuPfil001\PaaS_CloudFileDrop\276\Import\BI\CustomerData\</CustomerFilePath><CustomerArchivePath>\\azuPfil001\PaaS_CloudFileDrop\276\Import\BI\CustomerData\archive\</CustomerArchivePath><CustomerFileSearchPattern>customer*.xml</CustomerFileSearchPattern><CustomerXSD>customerinfo.xsd</CustomerXSD><BillFilePath>\\azuPfil001\PaaS_CloudFileDrop\276\Import\BI\BillingData\</BillFilePath><BillArchivePath>\\azuPfil001\PaaS_CloudFileDrop\276\Import\BI\BillingData\archive\</BillArchivePath><BillFileSearchPattern>bill*.xml</BillFileSearchPattern><BillingXSD>billingdata.xsd</BillingXSD><ProfileArchivePath>\\azuPfil001\PaaS_CloudFileDrop\276\Import\BI\ProfileData\archive\</ProfileArchivePath><ProfileFilePath>\\azuPfil001\PaaS_CloudFileDrop\276\Import\BI\ProfileData\</ProfileFilePath><ProfileFileSearchPattern>profile*.xml</ProfileFileSearchPattern><ProfileXSD>profile.xsd</ProfileXSD><ActionItemArchivePath>\\azuPfil001\PaaS_CloudFileDrop\276\Import\BI\ActionItemData\archive\</ActionItemArchivePath><ActionItemFilePath>\\azuPfil001\PaaS_CloudFileDrop\276\Import\BI\Actionitemdata\</ActionItemFilePath><ActionItemFileSearchPattern>actionitem*.xml</ActionItemFileSearchPattern><ActionItemXSD>actionitems.xsd</ActionItemXSD></InsightsDW.ImportData>',1))
PRINT(N'Operation applied to 5 rows out of 5')

PRINT(N'Add row to [cm].[ClientAction]')
SET IDENTITY_INSERT [cm].[ClientAction] ON
INSERT INTO [cm].[ClientAction] ([ClientActionID], [ClientID], [ActionKey], [ActionTypeKey], [Hide], [Disable], [CommodityKey], [DifficultyKey], [HabitIntervalKey], [NameKey], [DescriptionKey], [AnnualCost], [UpfrontCost], [AnnualSavingsEstimate], [AnnualUsageSavingsEstimate], [AnnualUsageSavingsUomKey], [CostVariancePercent], [PaybackTime], [ROI], [RebateAvailable], [RebateAmount], [RebateUrl], [RebateImageKey], [NextStepLinkText], [ImageKey], [VideoKey], [Comments], [ActionPriority], [SavingsCalcMethod], [SavingsAmount], [CostExpression], [RebateExpression], [NextStepLink], [NextStepLinkType], [Tags], [IconClass], [MinimumSavingsAmt], [MaximumSavingsAmt]) VALUES (7533, 210, 'insight.setgoal', 'insights', 0, 0, 'all', 'low', 'onetime', 'insight.setgoal.name', 'common.undefined', 0.00, 0.00, 0.00, 0.00, 'kwh', 0.00, 0, 0.00, 0, 0.00, NULL, NULL, 'insight.setgoal.linktext', 'laptopman.image', NULL, NULL, 50, 'None', 0.00, NULL, NULL, 'id=2&sid=11&dlw=setgoal', 'internal', '#setgoal', 'icon-money', 0.00, 0.00)
SET IDENTITY_INSERT [cm].[ClientAction] OFF

PRINT(N'Add rows to [cm].[ClientConfiguration]')
SET IDENTITY_INSERT [cm].[ClientConfiguration] ON
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3145, 80, 'prod', 'tab.dashboard.profile.showlongprofilelink', 'profile', 'If true then show the link to the long profile widget. Else do not show.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3147, 80, 'prod', 'premiseselect.enablepremiseselect', 'common', 'enable premise select', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3148, 80, 'prod', 'premiseselect.showsinglepremise', 'common', 'If true then display the premise select even when there is only one premise available. If false then hide the premise select when there is only one premise.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3149, 4, 'prod', 'premiseselect.enablepremiseselect', 'common', 'enable premise select', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3150, 4, 'prod', 'premiseselect.showsinglepremise', 'common', 'If true then display the premise select even when there is only one premise available. If false then hide the premise select when there is only one premise.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3151, 0, 'prod', 'tab.myusage.greenbutton.hidebilldownload', 'greenbutton', 'If true then hide the "download my bills" feature. Else show it.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3152, 0, 'prod', 'tab.myusage.greenbutton.hideusagedownload', 'greenbutton', 'If true then hide the "download my usage" feature. Else show it.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3153, 210, 'prod', 'tab.myusage.greenbutton.hidebilldownload', 'greenbutton', 'If true then hide the "download my bills" feature. Else show it.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3154, 210, 'prod', 'tab.myusage.greenbutton.hideusagedownload', 'greenbutton', 'If true then hide the "download my usage" feature. Else show it.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3155, 210, 'prod', 'tab.billedusage.billedusagechart.chartanchortype', 'bill', 'The anchor type, or symbol, to use in the chart', 'circle')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3156, 210, 'prod', 'tab.billedusage.billedusagechart.chartlinecolor1', 'bill', 'The line color to use on the line for the most recent 12 months of usage', '#00c5ff')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3157, 210, 'prod', 'tab.billedusage.billedusagechart.chartlinecolor2', 'bill', 'The color to use on the line for the previous 12 months of usage.', '#f9b129')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3158, 210, 'prod', 'tab.billedusage.billedusagechart.chartlinecolor3', 'bill', 'The color to use on the line for weather.', '#EB5A6C')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3159, 210, 'prod', 'tab.billedusage.billedusagechart.chartmonthformat', 'bill', 'The date format to use for the months in the chart axis', 'MMM')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3160, 210, 'prod', 'tab.billedusage.billedusagechart.charttype', 'bill', 'The type of chart: line or bar. Defaults to bar.', 'column')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3161, 210, 'prod', 'tab.billedusage.billedusagechart.commodities', 'bill', 'A comma-separated list of commodities for which the widget will display bills. The first one is the default.', 'gas,electric,water')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3162, 210, 'prod', 'tab.billedusage.billedusagechart.currencyformat', 'bill', 'Number format to use for currency. Using Microsoft Standard Numeric Format Strings.', 'C0')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3163, 210, 'prod', 'tab.billedusage.billedusagechart.enableweather', 'bill', 'This is used to enable weather series on chart.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3164, 210, 'prod', 'tab.billedusage.billedusagechart.footerlink', 'bill', 'The link to use in the widget footer.', 'http://www.aclara.com/')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3165, 210, 'prod', 'tab.billedusage.billedusagechart.footerlinktype', 'bill', 'The link to use in the widget footer.', 'external')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3166, 210, 'prod', 'tab.billedusage.billedusagechart.hoverdateformat', 'bill', 'The date format to use on the hover text.', 'MMM yyyy')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3167, 210, 'prod', 'tab.billedusage.billedusagechart.hoverelectricusageformat', 'bill', 'Number format to use for the electric usage in the hover text', 'N0')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3168, 210, 'prod', 'tab.billedusage.billedusagechart.hovergasusageformat', 'bill', 'Number format to use for the gas usage in the hover text', 'N2')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3169, 210, 'prod', 'tab.billedusage.billedusagechart.hoverwaterusageformat', 'bill', 'Number format to use for the water usage in the hover text', 'N2')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3170, 210, 'prod', 'tab.billedusage.billedusagechart.linethickness', 'bill', 'The line thickness to use in the chart', '2')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3171, 210, 'prod', 'tab.billedusage.billedusagechart.showfooterlink', 'bill', 'If true then show the link at the bottom of the widget. Else do not show the link.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3172, 210, 'prod', 'tab.billedusage.billedusagechart.showfootertext', 'bill', 'If true then show the footer text at the bottom of the widget. Else do not show the footer text.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3173, 210, 'prod', 'tab.billedusage.billedusagechart.showintro', 'bill', 'If true then show the widget intro text. Else omit the intro text.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3174, 210, 'prod', 'tab.billedusage.billedusagechart.showsubtitle', 'bill', 'If true then show the widget subtitle. Else omit the subtitle.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3175, 210, 'prod', 'tab.billedusage.billedusagechart.showtitle', 'bill', 'If true then show the widget title. Else omit the title.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3176, 210, 'prod', 'tab.billedusage.billedusagechart.showweatherbydefault', 'bill', 'This is used to show weather series by default.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3177, 210, 'prod', 'tab.billedusagebusiness.billedusagechartchartanchortype', 'bill', 'The anchor type, or symbol, to use in the chart', 'circle')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3178, 210, 'prod', 'tab.billedusagebusiness.billedusagechartchartlinecolor1', 'bill', 'The line color to use on the line for the most recent 12 months of usage', '#00c5ff')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3179, 210, 'prod', 'tab.billedusagebusiness.billedusagechartchartlinecolor2', 'bill', 'The color to use on the line for the previous 12 months of usage.', '#f9b129')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3180, 210, 'prod', 'tab.billedusagebusiness.billedusagechartchartmonthformat', 'bill', 'The date format to use for the months in the chart axis', 'MMM')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3181, 210, 'prod', 'tab.billedusagebusiness.billedusagechartcommodities', 'bill', 'A comma-separated list of commodities for which the widget will display bills. The first one is the default.', 'gas,electric,water')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3182, 210, 'prod', 'tab.billedusagebusiness.billedusagechartcurrencyformat', 'bill', 'Number format to use for currency. Using Microsoft Standard Numeric Format Strings.', 'C0')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3183, 210, 'prod', 'tab.billedusagebusiness.billedusagechartfooterlink', 'bill', 'The link to use in the widget footer.', 'http://www.aclara.com/')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3184, 210, 'prod', 'tab.billedusagebusiness.billedusagechartfooterlinktype', 'bill', 'The link to use in the widget footer.', 'external')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3185, 210, 'prod', 'tab.billedusagebusiness.billedusagecharthoverdateformat', 'bill', 'The date format to use on the hover text.', 'MMM yyyy')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3186, 210, 'prod', 'tab.billedusagebusiness.billedusagecharthoverelectricusageformat', 'bill', 'Number format to use for the electric usage in the hover text', 'N0')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3187, 210, 'prod', 'tab.billedusagebusiness.billedusagecharthovergasusageformat', 'bill', 'Number format to use for the gas usage in the hover text', 'N2')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3188, 210, 'prod', 'tab.billedusagebusiness.billedusagecharthoverwaterusageformat', 'bill', 'Number format to use for the water usage in the hover text', 'N2')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3189, 210, 'prod', 'tab.billedusagebusiness.billedusagechartlinethickness', 'bill', 'The line thickness to use in the chart', '2')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3190, 210, 'prod', 'tab.billedusagebusiness.billedusagechartshowfooterlink', 'bill', 'If true then show the link at the bottom of the widget. Else do not show the link.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3191, 210, 'prod', 'tab.billedusagebusiness.billedusagechartshowfootertext', 'bill', 'If true then show the footer text at the bottom of the widget. Else do not show the footer text.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3192, 210, 'prod', 'tab.billedusagebusiness.billedusagechartshowtitle', 'bill', 'If true then show the widget title. Else omit the title.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3193, 210, 'prod', 'tab.shortenergyprofile.profile.footerlink', 'profile', 'The link to use in the widget footer.', 'http://www.aclara.com/')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3194, 210, 'prod', 'tab.shortenergyprofile.profile.footerlinktype', 'profile', 'The link to use in the widget footer.', 'external')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3195, 210, 'prod', 'tab.shortenergyprofile.profile.longprofilelink', 'profile', 'The link to the long profile widget', 'id=5&sid=12')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3196, 210, 'prod', 'tab.shortenergyprofile.profile.showfooterlink', 'profile', 'If true then show the link at the bottom of the widget. Else do not show the link.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3197, 210, 'prod', 'tab.shortenergyprofile.profile.showfootertext', 'profile', 'If true then show the footer text at the bottom of the widget. Else do not show the footer text.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3198, 210, 'prod', 'tab.shortenergyprofile.profile.showlongprofilelink', 'profile', 'If true then show the link to the long profile widget. Else do not show.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3199, 210, 'prod', 'tab.shortenergyprofile.profile.showrequiredkey', 'profile', 'If true then show the requiredkey text. Else do not show.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3200, 210, 'prod', 'tab.shortenergyprofile.profile.showtitle', 'profile', 'If true then show the title at the top of the widget. Else do not show the title.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3201, 210, 'prod', 'tab.shortenergyprofilebusiness.profile.footerlink', 'profile', 'The link to use in the widget footer.', 'http://www.aclara.com/')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3202, 210, 'prod', 'tab.shortenergyprofilebusiness.profile.footerlinktype', 'profile', 'The link to use in the widget footer.', 'external')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3203, 210, 'prod', 'tab.shortenergyprofilebusiness.profile.longprofilelink', 'profile', 'The link to the long profile widget', 'id=5&sid=12')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3204, 210, 'prod', 'tab.shortenergyprofilebusiness.profile.showfooterlink', 'profile', 'If true then show the link at the bottom of the widget. Else do not show the link.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3205, 210, 'prod', 'tab.shortenergyprofilebusiness.profile.showfootertext', 'profile', 'If true then show the footer text at the bottom of the widget. Else do not show the footer text.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3206, 210, 'prod', 'tab.shortenergyprofilebusiness.profile.showintro', 'profile', 'If true then show the intro at the top of the widget. Else do not show the intro.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3207, 210, 'prod', 'tab.shortenergyprofilebusiness.profile.showlongprofilelink', 'profile', 'If true then show the link to the long profile widget. Else do not show.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3208, 210, 'prod', 'tab.shortenergyprofilebusiness.profile.showrequiredkey', 'profile', 'If true then show the requiredkey text. Else do not show.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3209, 210, 'prod', 'tab.shortenergyprofilebusiness.profile.showtitle', 'profile', 'If true then show the title at the top of the widget. Else do not show the title.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3210, 256, 'prod', 'tab.myusage.greenbutton.hidebilldownload', 'greenbutton', 'If true then hide the "download my bills" feature. Else show it.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3211, 256, 'prod', 'tab.myusage.greenbutton.hideusagedownload', 'greenbutton', 'If true then hide the "download my usage" feature. Else show it.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3213, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.chartanchortype', 'bill', 'The anchor type, or symbol, to use in the chart', 'circle')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3214, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.chartlinecolor1', 'bill', 'The line color to use on the line for the most recent 12 months of usage', '#00c5ff')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3215, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.chartlinecolor2', 'bill', 'The color to use on the line for the previous 12 months of usage.', '#f9b129')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3216, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.chartlinecolor3', 'bill', 'The color to use on the line for weather.', '#EB5A6C')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3217, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.chartmonthformat', 'bill', 'The date format to use for the months in the chart axis', 'MMM')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3218, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.charttype', 'bill', 'The type of chart: line or bar. Defaults to bar.', 'column')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3219, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.commodities', 'bill', 'A comma-separated list of commodities for which the widget will display bills. The first one is the default.', 'gas,electric,water')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3220, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.currencyformat', 'bill', 'Number format to use for currency. Using Microsoft Standard Numeric Format Strings.', 'C0')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3221, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.enableweather', 'bill', 'This is used to enable weather series on chart.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3222, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.footerlink', 'bill', 'The link to use in the widget footer.', 'http://www.aclara.com/')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3223, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.footerlinktype', 'bill', 'The link to use in the widget footer.', 'external')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3224, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.hoverdateformat', 'bill', 'The date format to use on the hover text.', 'MMM yyyy')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3225, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.hoverelectricusageformat', 'bill', 'Number format to use for the electric usage in the hover text', 'N0')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3226, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.hovergasusageformat', 'bill', 'Number format to use for the gas usage in the hover text', 'N2')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3227, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.hoverwaterusageformat', 'bill', 'Number format to use for the water usage in the hover text', 'N2')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3228, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.linethickness', 'bill', 'The line thickness to use in the chart', '2')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3229, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.showfooterlink', 'bill', 'If true then show the link at the bottom of the widget. Else do not show the link.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3230, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.showfootertext', 'bill', 'If true then show the footer text at the bottom of the widget. Else do not show the footer text.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3231, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.showintro', 'bill', 'If true then show the widget intro text. Else omit the intro text.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3232, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.showsubtitle', 'bill', 'If true then show the widget subtitle. Else omit the subtitle.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3233, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.showtitle', 'bill', 'If true then show the widget title. Else omit the title.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3234, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.showweatherbydefault', 'bill', 'This is used to show weather series by default.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3235, 210, 'prod', 'tab.billcomparison.billcomparison.showfooterlink', 'bill', 'If true then show the link at the bottom of the widget. Else do not show the link.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3236, 210, 'prod', 'tab.billcomparison.billcomparison.showfootertext', 'bill', 'If true then show the footer text at the bottom of the widget. Else do not show the footer text.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3237, 210, 'prod', 'tab.billcomparison.billcomparison.showintro', 'bill', 'If true then show the widget intro text. Else omit the intro text.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3238, 210, 'prod', 'tab.billcomparison.billcomparison.showtitle', 'bill', 'If true then show the widget title. Else omit the title.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3239, 210, 'prod', 'tab.dashboard.billcomparison.showfooterlink', 'bill', 'If true then show the link at the bottom of the widget. Else do not show the link.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3240, 210, 'prod', 'tab.dashboard.billcomparison.showfootertext', 'bill', 'If true then show the footer text at the bottom of the widget. Else do not show the footer text.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3241, 210, 'prod', 'tab.dashboard.billcomparison.showintro', 'bill', 'If true then show the widget intro text. Else omit the intro text.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3242, 210, 'prod', 'tab.dashboard.billdisagg.combinedview', 'billdisagg', 'This is used to show textual labels next to the commodity icons.', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3243, 0, 'prod', 'weather.sensitivity.shardname', 'wsbenchmark', 'The Shard Name to run for the Weather Sensitivity report', 'InsightsDW_Test')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3244, 224, 'prod', 'weather.sensitivity.shardname', 'wsbenchmark', 'The Shard Name to run for the Weather Sensitivity report', 'InsightsDW_224')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3245, 210, 'prod', 'tab.billedusage.billedusagechart.showcommoditylabels', 'bill', 'If true then show the text labels next to the commodity icons. If not true then show the commodity icon only. The commodity labels are stored in commodity.gas.name, commodity.electric.name and commodity.water.name..', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3246, 210, 'prod', 'tab.billedusagebusiness.billedusagechart.showcommoditylabels', 'bill', 'If true then show the text labels next to the commodity icons. If not true then show the commodity icon only. The commodity labels are stored in commodity.gas.name, commodity.electric.name and commodity.water.name..', 'true')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3247, 210, 'prod', 'tab.dashboard.billedusagechart.showfooterlink', 'bill', 'If true then show the link at the bottom of the widget. Else do not show the link.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3248, 210, 'prod', 'tab.dashboard.billedusagechart.showfootertext', 'bill', 'If true then show the footer text at the bottom of the widget. Else do not show the footer text.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3249, 210, 'prod', 'tab.dashboard.billedusagechart.showintro', 'bill', 'If true then show the widget intro text. Else omit the intro text.', 'false')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (3250, 210, 'prod', 'tab.myusage.greenbutton.showsubtitle', 'greenbutton', 'If true then show the subtitle. Else omit the subtitle', 'true')
SET IDENTITY_INSERT [cm].[ClientConfiguration] OFF
PRINT(N'Operation applied to 104 rows out of 104')

PRINT(N'Add rows to [cm].[ClientProfileAttribute]')
SET IDENTITY_INSERT [cm].[ClientProfileAttribute] ON
INSERT INTO [cm].[ClientProfileAttribute] ([ClientProfileAttributeID], [ClientID], [ProfileAttributeKey], [MinValue], [MaxValue], [QuestionTextKey], [DoNotDefault], [Disable]) VALUES (2074, 210, 'fireplace.fuel', NULL, NULL, 'fireplace.fuel.question', 0, 0)
INSERT INTO [cm].[ClientProfileAttribute] ([ClientProfileAttributeID], [ClientID], [ProfileAttributeKey], [MinValue], [MaxValue], [QuestionTextKey], [DoNotDefault], [Disable]) VALUES (2075, 210, 'heatsystem.style', NULL, NULL, 'heatsystem.style.question', 0, 0)
INSERT INTO [cm].[ClientProfileAttribute] ([ClientProfileAttributeID], [ClientID], [ProfileAttributeKey], [MinValue], [MaxValue], [QuestionTextKey], [DoNotDefault], [Disable]) VALUES (2076, 210, 'house.basementstyle', NULL, NULL, 'house.basementstyle.question', 0, 0)
INSERT INTO [cm].[ClientProfileAttribute] ([ClientProfileAttributeID], [ClientID], [ProfileAttributeKey], [MinValue], [MaxValue], [QuestionTextKey], [DoNotDefault], [Disable]) VALUES (2077, 210, 'house.floors.insulated', NULL, NULL, 'house.floorsinsulated.question', 0, 0)
INSERT INTO [cm].[ClientProfileAttribute] ([ClientProfileAttributeID], [ClientID], [ProfileAttributeKey], [MinValue], [MaxValue], [QuestionTextKey], [DoNotDefault], [Disable]) VALUES (2078, 210, 'house.levels', 1, 4, 'house.levels.question', 0, 0)
INSERT INTO [cm].[ClientProfileAttribute] ([ClientProfileAttributeID], [ClientID], [ProfileAttributeKey], [MinValue], [MaxValue], [QuestionTextKey], [DoNotDefault], [Disable]) VALUES (2079, 210, 'house.style', NULL, NULL, 'house.style.question', 0, 0)
SET IDENTITY_INSERT [cm].[ClientProfileAttribute] OFF
PRINT(N'Operation applied to 6 rows out of 6')

PRINT(N'Add rows to [cm].[ClientProfileOption]')
SET IDENTITY_INSERT [cm].[ClientProfileOption] ON
INSERT INTO [cm].[ClientProfileOption] ([ClientProfileOptionID], [ClientID], [ProfileOptionKey], [NameKey], [Description], [ProfileOptionValue]) VALUES (1860, 210, 'house.floorsinsulated.no', 'profile.option.no.name', 'floors are not insulated', 0.0000)
INSERT INTO [cm].[ClientProfileOption] ([ClientProfileOptionID], [ClientID], [ProfileOptionKey], [NameKey], [Description], [ProfileOptionValue]) VALUES (1861, 210, 'house.floorsinsulated.yes', 'profile.option.yes.name', 'floors are insulated', 0.0000)
SET IDENTITY_INSERT [cm].[ClientProfileOption] OFF
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Add rows to [cm].[ClientProfileSection]')
SET IDENTITY_INSERT [cm].[ClientProfileSection] ON
INSERT INTO [cm].[ClientProfileSection] ([ClientProfileSectionID], [ClientID], [ProfileSectionKey], [Name], [DescriptionKey], [TitleKey], [SubTitleKey], [Rank], [Images], [Disable], [IntroTextKey]) VALUES (195, 4, 'profilesection.centralac', 'Central AC', 'profilesection.centralac.desc', 'profilesection.centralac.title', 'profilesection.centralac.subtitle', 90, 'image.284341565centralacoutdoorunits', 0, NULL)
INSERT INTO [cm].[ClientProfileSection] ([ClientProfileSectionID], [ClientID], [ProfileSectionKey], [Name], [DescriptionKey], [TitleKey], [SubTitleKey], [Rank], [Images], [Disable], [IntroTextKey]) VALUES (196, 4, 'profilesection.shortcentralac', 'centralac', 'profilesection.centralac.desc', 'profilesection.centralac.title', 'profilesection.centralac.subtitle', 90, 'image.284341565centralacoutdoorunits', 0, NULL)
INSERT INTO [cm].[ClientProfileSection] ([ClientProfileSectionID], [ClientID], [ProfileSectionKey], [Name], [DescriptionKey], [TitleKey], [SubTitleKey], [Rank], [Images], [Disable], [IntroTextKey]) VALUES (197, 210, 'profilesection.home1', 'Most important home information', 'profilesection.home1.desc', 'profilesection.home1.title', 'profilesection.home1.subtitle', 10, 'laptopman.image', 0, NULL)
INSERT INTO [cm].[ClientProfileSection] ([ClientProfileSectionID], [ClientID], [ProfileSectionKey], [Name], [DescriptionKey], [TitleKey], [SubTitleKey], [Rank], [Images], [Disable], [IntroTextKey]) VALUES (214, 210, 'profilesection.otherappliance', 'Appliances: Other', 'profilesection.otherappliance.desc', 'profilesection.otherappliance.title', 'profilesection.otherappliance.subtitle', 250, 'Outlet.image ', 0, NULL)
INSERT INTO [cm].[ClientProfileSection] ([ClientProfileSectionID], [ClientID], [ProfileSectionKey], [Name], [DescriptionKey], [TitleKey], [SubTitleKey], [Rank], [Images], [Disable], [IntroTextKey]) VALUES (215, 210, 'profilesection.faucetstoilets', 'Faucets and Toilets', 'profilesection.faucetstoilets.desc', 'profilesection.faucetstoilets.title', 'profilesection.faucetstoilets.subtitle', 360, 'image.152121869toiletwithgreenbackground', 0, NULL)
SET IDENTITY_INSERT [cm].[ClientProfileSection] OFF
PRINT(N'Operation applied to 5 rows out of 5')

PRINT(N'Add rows to [cm].[ClientTab]')
SET IDENTITY_INSERT [cm].[ClientTab] ON
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1267, 210, 'tab.lightingbusiness', 'Energy Profile Business: Lighting', 530, 'layout.2rows1column', 'widget', 'tab.lightingbusiness.name', 'C530', 'energyprofilebusiness.longprofile', 1, 'NULL', 'icon-light-bulb', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1268, 210, 'tab.yourhome', 'Energy Profile: Your Home', 10, 'layout.2rows1column', 'widget', 'tab.yourhome.name', 'C10', 'energyprofile.longprofile', 1, 'NULL', 'icon-house', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1269, 210, 'tab.appliances', 'Energy Profile: Appliances', 40, 'layout.2rows1column', 'widget', 'tab.appliances.name', 'C40', 'energyprofile.longprofile', 1, 'NULL', 'icon-refrigerator', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1270, 210, 'tab.equipmentbusiness', 'Energy Profile Business: Equipment', 540, 'layout.2rows1column', 'widget', 'tab.equipmentbusiness.name', 'C540', 'energyprofilebusiness.longprofile', 1, 'NULL', 'icon-monitor', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1271, 210, 'tab.heatingandcoolingbusiness', 'Energy Profile Business: Heating and Cooling', 520, 'layout.2rows1column', 'widget', 'tab.heatingandcoolingbusiness.name', 'C520', 'energyprofilebusiness.longprofile', 1, 'NULL', 'icon-waves', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1272, 210, 'tab.insulationwindowdoor', 'Energy Profile:  Insulation,Windows and Doors', 20, 'layout.2rows1column', 'widget', 'tab.insulationwindowdoor.name', 'C20', 'energyprofile.longprofile', 1, 'NULL', 'icon-house', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1273, 210, 'tab.lighting', 'Energy Profile: Lighting', 50, 'layout.2rows1column', 'widget', 'tab.lighting.name', 'C50', 'energyprofile.longprofile', 1, 'NULL', 'icon-light-bulb', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1274, 210, 'tab.misc', 'Energy profile: Misc', 70, 'layout.2rows1column', 'widget', 'tab.misc.name', 'C70', 'energyprofile.longprofile', 1, 'NULL', 'icon-monitor', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1275, 210, 'tab.overview', 'Energy Profile: Overview', 5, 'layout.2rows1column', 'widget', 'tab.overview.name', 'C5', 'energyprofile.longprofile', 1, 'NULL', 'icon-menu', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1276, 210, 'tab.water', 'Energy profile: Water', 60, 'layout.2rows1column', 'widget', 'tab.water.name', 'C60', 'energyprofile.longprofile', 1, 'NULL', 'icon-drop', 'NULL', 1)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1277, 210, 'tab.waterbusiness', 'Energy Profile Business: Water', 550, 'layout.2rows1column', 'widget', 'tab.waterbusiness.name', 'C550', 'energyprofilebusiness.longprofile', 1, 'NULL', 'icon-drop', 'NULL', 1)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1278, 210, 'tab.yourbusiness', 'Energy Profile Business: Your Business', 510, 'layout.2rows1column', 'widget', 'tab.yourbusiness.name', 'C510', 'energyprofilebusiness.longprofile', 1, 'NULL', 'icon-house', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1279, 210, 'tab.energyprofilebusiness', 'Energy Profile Business', 1, 'layout.2rows1column', 'sub', 'tab.energyprofilebusiness.name', 'C12', NULL, 1, 'NULL', NULL, 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1280, 210, 'tab.myplan', 'My Plan', 3, 'layout.1row2columnslarge', 'sub', 'tab.myplan.name', 'C13', NULL, 1, 'NULL', NULL, 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1281, 210, 'tab.waystosave', 'Ways to Save', 2, 'layout.1row2columnslarge', 'sub', 'tab.waystosave.name', 'C11', NULL, 1, 'NULL', NULL, 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1282, 210, 'tab.waystosavebusiness', 'Ways to Save Business', 2, 'layout.1row2columnslarge', 'sub', 'tab.waystosavebusiness.name', 'C11', NULL, 1, 'NULL', NULL, 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1291, 210, 'tab.billedusage', 'Billed Usage Residential', 1, 'layout.1row1column', 'sub', 'tab.billedusage.name', 'C90', 'billedusage.billedusagechart', 1, 'NULL', 'NULL', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1292, 210, 'tab.billedusagebusiness', 'Billed Usage Business', 2, 'layout.1row1column', 'sub', 'tab.billedusagebusiness.name', 'C90', 'billedusagebusiness.billedusagechart', 1, 'NULL', 'NULL', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1293, 210, 'tab.mybilledusage', 'My Billed Usage', 6, 'layout.1row1column', 'main', 'tab.mybilledusage.name', 'C6', NULL, 1, 'NULL', 'NULL', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1294, 210, 'tab.myshortprofile', 'My profile', 5, 'layout.1row1column', 'main', 'tab.myshortprofile.name', 'C5', NULL, 1, 'NULL', 'NULL', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1295, 210, 'tab.shortenergyprofile', 'Energy Profile', 1, 'layout.1row1column', 'sub', 'tab.energyprofile.name', 'C130', 'shortenergyprofile.profile', 1, 'NULL', 'NULL', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1296, 210, 'tab.shortenergyprofilebusiness', 'Energy Profile Business', 2, 'layout.1row1column', 'sub', 'tab.energyprofilebusiness.name', 'C130', 'shortenergyprofilebusiness.profile', 1, 'NULL', 'NULL', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1297, 210, 'tab.shortprofile', 'Short Profile: All', 1, 'layout.1row1column', 'widget', 'tab.shortprofile.name', 'C140', 'shortenergyprofile.profile', 1, 'NULL', 'NULL', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1298, 210, 'tab.shortprofilebusiness', 'Short Profile Business: All', 2, 'layout.1row1column', 'widget', 'tab.shortprofilebusiness.name', 'C150', 'shortenergyprofilebusiness.profile', 1, 'NULL', 'NULL', 'NULL', 0)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1299, 210, 'tab.billedusagechart', 'Billed Usage Residential', 1, 'layout.1row1column', 'widget', 'tab.billedusagechart.name', 'C90', 'billedusage.billedusagechart', 1, 'NULL', 'NULL', 'NULL', 1)
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1300, 210, 'tab.billedusagechartbusiness', 'Billed Usage Business', 2, 'layout.1row1column', 'widget', 'tab.billedusagechartbusiness.name', 'C90', 'billedusagebusiness.billedusagechart', 1, 'NULL', 'NULL', 'NULL', 1)
SET IDENTITY_INSERT [cm].[ClientTab] OFF
PRINT(N'Operation applied to 26 rows out of 26')

PRINT(N'Add rows to [cm].[ClientTextContent]')
SET IDENTITY_INSERT [cm].[ClientTextContent] ON
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7073, 210, 'house.rented.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7074, 210, 'house.rooms.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7075, 210, 'house.roomsize.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7076, 210, 'house.style.apartment.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7077, 210, 'house.style.duplex.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7078, 210, 'house.style.mobile.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7079, 210, 'house.style.multifamily.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7080, 210, 'house.style.singlefamily.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7081, 210, 'house.style.townhouse.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7082, 210, 'sink.faucetleakrate.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7083, 210, 'sink.leakingfaucetsqty.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7084, 210, 'house.atticinsulated.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7085, 210, 'house.wallsinsulated.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7086, 210, 'house.basementstyle.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7087, 210, 'house.crawlinsulation.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7088, 0, 'tab.insulationwindowdoor.name', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7089, 210, 'common.fatalerrormessage', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7090, 210, 'common.partialbrowsermessage', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7091, 210, 'common.unsupportedbrowsermessage', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7092, 210, 'tab.dashboard.billcomparison.footerlinktext', 'bill')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7093, 210, 'tab.dashboard.billcomparison.onebill', 'bill')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7094, 210, 'tab.dashboard.billdisagg.footer', 'billdisagg')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7095, 210, 'tab.dashboard.billdisagg.nomodelledusage', 'billdisagg')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7096, 210, 'tab.dashboard.billedusagechart.nobills', 'bill')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7097, 210, 'tab.dashboard.profile.nextbuttonlabel', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7098, 210, 'tab.dashboard.profile.notcompletemessage', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7099, 210, 'tab.dashboardbusiness.billedusagechart.nobills', 'bill')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7100, 210, 'tab.dashboardbusiness.insights.title', 'insights')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7101, 210, 'tab.dashboardbusiness.profile.nextbuttonlabel', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7102, 210, 'tab.dashboardbusiness.profile.notcompletemessage', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7103, 210, 'tab.myplan.actionplan.introtext', 'action')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7104, 210, 'tab.myusage.greenbutton.bannertext', 'greenbutton')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7105, 210, 'tab.myusage.greenbutton.footer', 'greenbutton')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7106, 210, 'tab.myusage.greenbutton.footerlinktext', 'greenbutton')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7107, 210, 'tab.myusage.greenbutton.subtitle', 'greenbutton')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7108, 210, 'tab.waystosave.actions.introtext', 'action')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7109, 210, 'tab.waystosavebusiness.actions.introtext', 'action')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7110, 210, 'billedusage.billedusagechart.title', 'bill')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7111, 210, 'billedusagebusiness.billedusagechart.title', 'bill')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7112, 210, 'shortenergyprofile.profile.title', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7113, 210, 'shortenergyprofilebusiness.profile.title', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7114, 210, 'tab.billedusage.name', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7115, 210, 'tab.billedusagebusiness.name', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7116, 210, 'tab.energyprofile.name', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7117, 210, 'tab.energyprofilebusiness.name', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7118, 210, 'tab.mybilledusage.name', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7119, 210, 'tab.myshortprofile.name', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7120, 210, 'tab.shortprofile.name', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7121, 210, 'tab.shortprofilebusiness.name', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7122, 210, 'tab.billedusagechart', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7123, 210, 'tab.billedusagechartbusiness', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7124, 210, 'tab.shortenergyprofile', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7125, 210, 'tab.shortenergyprofilebusiness', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7126, 210, 'tab.shortenergyprofile.name', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7127, 210, 'tab.shortenergyprofilebusiness.name', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7128, 210, 'tab.shortenergyprofile.profile.nextbuttonlabel', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7129, 210, 'tab.shortenergyprofilebusiness.profile.nextbuttonlabel', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7130, 0, 'action.shortershowers.desc', 'action')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7131, 0, 'action.shortershowers.name', 'action')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7132, 210, 'tab.billcomparison.billcomparison.billdays', 'bill')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7133, 210, 'tab.billcomparison.billcomparison.totalamountdue', 'bill')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7134, 210, 'tab.myusage.greenbutton.introtext', 'greenbutton')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7135, 224, 'tab.espmcontacts.contacts.acceptedaccounts', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7136, 224, 'tab.espmcontacts.contacts.footer', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7137, 224, 'tab.espmcontacts.contacts.loading', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7138, 224, 'tab.espmcontacts.contacts.norecordsfound', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7139, 224, 'tab.espmcontacts.contacts.noselection', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7140, 224, 'tab.espmcontacts.contacts.pendingaccounts', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7141, 224, 'tab.espmcontacts.contacts.rejectconfirmationcancel', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7142, 224, 'tab.espmcontacts.contacts.rejectconfirmationconfirm', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7143, 224, 'tab.espmcontacts.contacts.rejectconfirmationmessage', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7144, 224, 'tab.espmcontacts.contacts.rejectconfirmationtitle', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7145, 224, 'tab.espmmeters.meters.acceptedmeters', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7146, 224, 'tab.espmmeters.meters.acceptmeters', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7147, 224, 'tab.espmmeters.meters.accountaddress', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7148, 224, 'tab.espmmeters.meters.accountid', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7149, 224, 'tab.espmmeters.meters.accountjobtitle', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7150, 224, 'tab.espmmeters.meters.accountname', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7151, 224, 'tab.espmmeters.meters.accountphone', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7152, 224, 'tab.espmmeters.meters.customfield1column', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7153, 224, 'tab.espmmeters.meters.customfield2column', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7154, 224, 'tab.espmmeters.meters.firstbilldate', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7155, 224, 'tab.espmmeters.meters.footer', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7156, 224, 'tab.espmmeters.meters.inuse', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7157, 224, 'tab.espmmeters.meters.loading', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7158, 224, 'tab.espmmeters.meters.metered', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7159, 224, 'tab.espmmeters.meters.meterid', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7160, 224, 'tab.espmmeters.meters.metername', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7161, 224, 'tab.espmmeters.meters.metersintrotext', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7162, 224, 'tab.espmmeters.meters.meterstitle', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7163, 224, 'tab.espmmeters.meters.metertype', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7164, 224, 'tab.espmmeters.meters.norecordsfound', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7165, 224, 'tab.espmmeters.meters.noselection', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7166, 224, 'tab.espmmeters.meters.pendingmeters', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7167, 224, 'tab.espmmeters.meters.propertyaddress', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7168, 224, 'tab.espmmeters.meters.propertybuildings', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7169, 224, 'tab.espmmeters.meters.propertyconstructionstatus', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7170, 224, 'tab.espmmeters.meters.propertyearbuilt', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7171, 224, 'tab.espmmeters.meters.propertyfloorarea', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7172, 224, 'tab.espmmeters.meters.propertyid', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7173, 224, 'tab.espmmeters.meters.propertyisfederal', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7174, 224, 'tab.espmmeters.meters.propertyname', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7175, 224, 'tab.espmmeters.meters.propertyoccupancypercent', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7176, 224, 'tab.espmmeters.meters.propertyprimaryfunction', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7177, 224, 'tab.espmmeters.meters.rejectcancel', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7178, 224, 'tab.espmmeters.meters.rejectconfirm', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7179, 224, 'tab.espmmeters.meters.rejectconfirmationmessage', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7180, 224, 'tab.espmmeters.meters.rejectconfirmationtitle', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7181, 224, 'tab.espmmeters.meters.rejectmeters', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7182, 224, 'tab.espmmeters.meters.timestamp', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7183, 224, 'tab.espmmeters.meters.unitofmeasure', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7184, 224, 'tab.espmportals.tab.contacts', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7185, 224, 'tab.espmportals.tab.meters', 'espm')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7186, 210, 'clotheswasher.loadused.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7187, 210, 'clotheswasher.style.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7188, 210, 'heatsystem.style.geothermalheatpump.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7189, 210, 'house.floorsinsulated.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7190, 210, 'house.levels.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7191, 210, 'house.totalarearange.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7192, 210, 'profilesection.fireplace.desc', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7193, 210, 'profilesection.fireplace.subtitle', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7194, 210, 'profilesection.fireplace.title', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7195, 210, 'profilesection.otherappliance.desc', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7196, 210, 'profilesection.otherappliance.subtitle', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7197, 210, 'profilesection.otherappliance.title', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7198, 210, 'refrigerator.location.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7199, 210, 'refrigerator.thrudoorice.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7200, 210, 'refrigerator.year.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7201, 210, 'house.totalarea.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7202, 210, 'house.floorinsulation.question', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7203, 210, 'house.yearbuiltrange.1955.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7204, 210, 'house.yearbuiltrange.1965.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7205, 210, 'house.yearbuiltrange.1975.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7206, 210, 'house.yearbuiltrange.1985.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7207, 210, 'house.yearbuiltrange.1995.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7208, 210, 'house.yearbuiltrange.2000.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7209, 210, 'house.yearbuiltrange.2005.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7210, 210, 'house.yearbuiltrange.2010.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7211, 210, 'house.yearbuiltrange.2015.name', 'profile')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (7212, 210, 'house.yearbuiltrange.before1950.name', 'profile')
SET IDENTITY_INSERT [cm].[ClientTextContent] OFF
PRINT(N'Operation applied to 140 rows out of 140')

PRINT(N'Add rows to [cm].[ClientWidget]')
SET IDENTITY_INSERT [cm].[ClientWidget] ON
INSERT INTO [cm].[ClientWidget] ([ClientWidgetID], [ClientID], [WidgetKey], [Name], [TabKey], [WidgetTypeKey], [WidgetColumn], [WidgetOrder], [WidgetRow], [Disable], [IntroTextKey]) VALUES (532, 210, 'billedusage.billedusagechart', 'My Billed Usage Chart', 'tab.billedusage', 'billedusagechart', 1, 1, 1, 0, 'common.undefined')
INSERT INTO [cm].[ClientWidget] ([ClientWidgetID], [ClientID], [WidgetKey], [Name], [TabKey], [WidgetTypeKey], [WidgetColumn], [WidgetOrder], [WidgetRow], [Disable], [IntroTextKey]) VALUES (533, 210, 'billedusagebusiness.billedusagechart', 'My Billed Isage Business Chart', 'tab.billedusagebusiness', 'billedusagechart', 1, 1, 1, 0, 'common.undefined')
INSERT INTO [cm].[ClientWidget] ([ClientWidgetID], [ClientID], [WidgetKey], [Name], [TabKey], [WidgetTypeKey], [WidgetColumn], [WidgetOrder], [WidgetRow], [Disable], [IntroTextKey]) VALUES (534, 210, 'shortenergyprofile.profile', 'Dashboard Profile', 'tab.shortenergyprofile', 'profile', 1, 1, 1, 0, 'common.undefined')
INSERT INTO [cm].[ClientWidget] ([ClientWidgetID], [ClientID], [WidgetKey], [Name], [TabKey], [WidgetTypeKey], [WidgetColumn], [WidgetOrder], [WidgetRow], [Disable], [IntroTextKey]) VALUES (535, 210, 'shortenergyprofilebusiness.profile', 'Dashboard Profile', 'tab.shortenergyprofilebusiness', 'profile', 1, 1, 1, 0, 'common.undefined')
SET IDENTITY_INSERT [cm].[ClientWidget] OFF
PRINT(N'Operation applied to 4 rows out of 4')

PRINT(N'Add row to [cm].[ClientActionCondition]')
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (7533, 'premisetype.residential', 0)

PRINT(N'Add row to [cm].[ClientProfileAttributeCondition]')
INSERT INTO [cm].[ClientProfileAttributeCondition] ([ClientProfileAttributeID], [ConditionKey], [DisplayOrder]) VALUES (2075, 'heatingsystem.no', 1)

PRINT(N'Add rows to [cm].[ClientProfileAttributeProfileOption]')
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2074, 'fireplace.fuel.electric', 4)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2074, 'fireplace.fuel.gas', 2)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2074, 'fireplace.fuel.propane', 3)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2074, 'fireplace.fuel.wood', 1)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2075, 'heatsystem.style.airsourceheatpump', 4)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2075, 'heatsystem.style.baseboardresistance', 6)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2075, 'heatsystem.style.conventionaloil', 5)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2075, 'heatsystem.style.forcedairfurnace', 1)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2075, 'heatsystem.style.freestandingstove', 10)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2075, 'heatsystem.style.geothermalheatpump', 7)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2075, 'heatsystem.style.groundsourceheatpump', 8)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2075, 'heatsystem.style.radiant', 9)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2075, 'heatsystem.style.steamboiler', 2)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2075, 'heatsystem.style.waterboiler', 3)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2076, 'house.basementstyle.conditioned', 1)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2076, 'house.basementstyle.slabongrade', 3)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2076, 'house.basementstyle.unconditioned', 2)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2077, 'house.floorsinsulated.no', 2)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2077, 'house.floorsinsulated.yes', 1)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2078, 'house.levels.value', 1)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2079, 'house.style.apartment', 2)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2079, 'house.style.duplex', 3)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2079, 'house.style.mobile', 4)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2079, 'house.style.multifamily', 5)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2079, 'house.style.singlefamily', 1)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (2079, 'house.style.townhouse', 6)
PRINT(N'Operation applied to 26 rows out of 26')

PRINT(N'Add rows to [cm].[ClientProfileSectionAttribute]')
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (190, 'house.floorinsulation', 4)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (195, 'centralac.servicefrequency', 3)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (195, 'centralac.style', 1)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (195, 'centralac.year', 2)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (196, 'centralac.servicefrequency', 3)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (196, 'centralac.style', 1)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (196, 'centralac.year', 2)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (196, 'roomac.count', 4)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (197, 'house.attic', 7)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (197, 'house.basementstyle', 6)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (197, 'house.daytimeoccupancy', 9)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (197, 'house.levels', 2)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (197, 'house.people', 5)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (197, 'house.rented', 8)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (197, 'house.rooms', 10)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (197, 'house.style', 1)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (197, 'house.totalarea', 4)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (197, 'house.yearbuiltrange', 3)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (214, 'o2concentrator.count', 1)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (214, 'wellpump.count', 2)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (215, 'sink.faucetleakrate', 2)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (215, 'sink.leakingfaucetsqty', 1)
PRINT(N'Operation applied to 22 rows out of 22')

PRINT(N'Add rows to [cm].[ClientTabChildTab]')
INSERT INTO [cm].[ClientTabChildTab] ([ClientTabID], [ChildTabKey], [Rank]) VALUES (1258, 'tab.energyprofile', 1)
INSERT INTO [cm].[ClientTabChildTab] ([ClientTabID], [ChildTabKey], [Rank]) VALUES (1258, 'tab.energyprofilebusiness', 2)
INSERT INTO [cm].[ClientTabChildTab] ([ClientTabID], [ChildTabKey], [Rank]) VALUES (1258, 'tab.myplan', 5)
INSERT INTO [cm].[ClientTabChildTab] ([ClientTabID], [ChildTabKey], [Rank]) VALUES (1258, 'tab.waystosave', 3)
INSERT INTO [cm].[ClientTabChildTab] ([ClientTabID], [ChildTabKey], [Rank]) VALUES (1258, 'tab.waystosavebusiness', 4)
INSERT INTO [cm].[ClientTabChildTab] ([ClientTabID], [ChildTabKey], [Rank]) VALUES (1293, 'tab.billedusage', 1)
INSERT INTO [cm].[ClientTabChildTab] ([ClientTabID], [ChildTabKey], [Rank]) VALUES (1293, 'tab.billedusagebusiness', 2)
INSERT INTO [cm].[ClientTabChildTab] ([ClientTabID], [ChildTabKey], [Rank]) VALUES (1294, 'tab.shortenergyprofile', 1)
INSERT INTO [cm].[ClientTabChildTab] ([ClientTabID], [ChildTabKey], [Rank]) VALUES (1294, 'tab.shortenergyprofilebusiness', 2)
PRINT(N'Operation applied to 9 rows out of 9')

PRINT(N'Add rows to [cm].[ClientTabCondition]')
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1266, 'premisetype.residential', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1267, 'premisetype.business', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1268, 'premisetype.residential', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1269, 'premisetype.residential', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1270, 'premisetype.business', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1271, 'premisetype.business', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1272, 'premisetype.residential', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1273, 'premisetype.residential', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1274, 'premisetype.residential', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1275, 'premisetype.residential', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1276, 'premisetype.residential', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1277, 'premisetype.business', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1278, 'premisetype.business', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1279, 'premisetype.business', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1281, 'premisetype.residential', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1282, 'premisetype.business', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1291, 'premisetype.residential', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1292, 'premisetype.business', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1295, 'premisetype.residential', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1296, 'premisetype.business', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1297, 'premisetype.residential', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1298, 'premisetype.business', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1299, 'premisetype.residential', 1)
INSERT INTO [cm].[ClientTabCondition] ([ClientTabID], [ConditionKey], [Rank]) VALUES (1300, 'premisetype.business', 1)
PRINT(N'Operation applied to 24 rows out of 24')

PRINT(N'Add rows to [cm].[ClientTabProfileSection]')
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1266, 'profilesection.centralactemps', 4)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1266, 'profilesection.heatingtemps', 5)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1267, 'profilesection.buslighting', 1)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1268, 'profilesection.home1', 1)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1269, 'profilesection.clotheswasher', 3)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1269, 'profilesection.computer', 9)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1269, 'profilesection.cooking', 5)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1269, 'profilesection.dishwasher', 7)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1269, 'profilesection.dryer', 4)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1269, 'profilesection.otherappliance', 10)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1269, 'profilesection.refrigerator', 2)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1269, 'profilesection.shortcooktop', 6)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1269, 'profilesection.tv', 8)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1269, 'profilesection.waterheater', 1)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1270, 'profilesection.buscooking', 1)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1270, 'profilesection.buselectronics', 4)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1270, 'profilesection.busfreezer', 2)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1270, 'profilesection.buslaundry', 3)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1270, 'profilesection.busrefrigeration', 5)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1271, 'profilesection.buscooling', 2)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1271, 'profilesection.busheating', 1)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1272, 'profilesection.insulation', 1)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1273, 'profilesection.lighting1', 1)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1274, 'profilesection.faucetstoilets', 4)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1274, 'profilesection.misccomfort', 2)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1274, 'profilesection.misckitchen', 1)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1274, 'profilesection.miscother', 3)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1276, 'profilesection.faucetstoilets', 9)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1276, 'profilesection.garden', 2)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1276, 'profilesection.hottub', 6)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1276, 'profilesection.lawn', 1)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1276, 'profilesection.pool', 4)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1276, 'profilesection.poolheater', 5)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1276, 'profilesection.showers', 7)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1276, 'profilesection.waterheater', 3)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1276, 'profilesection.wellpump', 8)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1277, 'profilesection.buswaterheater', 1)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1278, 'profilesection.busbuilding', 2)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1278, 'profilesection.buslong1', 1)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1297, 'profilesection.shortappliances', 4)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1297, 'profilesection.shortcooktop', 9)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1297, 'profilesection.shortdryer', 5)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1297, 'profilesection.shortheating', 2)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1297, 'profilesection.shorthome1', 1)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1297, 'profilesection.shorthottub', 7)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1297, 'profilesection.shortoven', 8)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1297, 'profilesection.shortpoolheater', 6)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1297, 'profilesection.shortwaterheater', 3)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1298, 'profilesection.buswizard1', 1)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1298, 'profilesection.buswizard2', 2)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (1298, 'profilesection.buswizardlight', 3)
PRINT(N'Operation applied to 51 rows out of 51')

PRINT(N'Add rows to [cm].[ClientTextContentLocaleContent]')
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7073, 'en-US', N'Do you rent your home?', N'Do you rent your home?', N'Do you rent your home?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7073, 'es-ES', N'¿Alquilas tu casa?', N'¿Alquilas tu casa?', N'¿Alquilas tu casa?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7073, 'ru-RU', N'Вы арендуете ваш дом?', N'Вы арендуете ваш дом?', N'Вы арендуете ваш дом?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7074, 'en-US', N'Excluding bathrooms and hallways, how many rooms are in your home?', N'Excluding bathrooms and hallways, how many rooms are in your home?', N'Excluding bathrooms and hallways, how many rooms are in your home?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7074, 'es-ES', N'¿Excluyendo baños y pasillos, cómo muchas de ellas están en su hogar?', N'¿Excluyendo baños y pasillos, cómo muchas de ellas están en su hogar?', N'¿Excluyendo baños y pasillos, cómo muchas de ellas están en su hogar?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7074, 'ru-RU', N'За исключением ванных комнат и прихожих сколько комнат находятся в вашем доме?', N'За исключением ванных комнат и прихожих сколько комнат находятся в вашем доме?', N'За исключением ванных комнат и прихожих сколько комнат находятся в вашем доме?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7075, 'en-US', N'What would you say the size of your rooms is...', N'What would you say the size of your rooms is...', N'What would you say the size of your rooms is...', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7075, 'es-ES', N'Qué dirías que es el tamaño de sus habitaciones...', N'Qué dirías que es el tamaño de sus habitaciones...', N'Qué dirías que es el tamaño de sus habitaciones...', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7075, 'ru-RU', N'Что бы вы сказали, что размер вашего номера...', N'Что бы вы сказали, что размер вашего номера...', N'Что бы вы сказали, что размер вашего номера...', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7076, 'en-US', N'Apartment/Condo', N'Apartment/Condo', N'Apartment/Condo', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7076, 'es-ES', N'Apartamento/Condo', N'Apartamento/Condo', N'Apartamento/Condo', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7076, 'ru-RU', N'Квартира/кондоминиум', N'Квартира/кондоминиум', N'Квартира/кондоминиум', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7077, 'en-US', N'Semi-detached Duplex', N'Semi-detached Duplex', N'Semi-detached Duplex', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7077, 'es-ES', N'Duplex semi-adosado', N'Duplex semi-adosado', N'Duplex semi-adosado', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7077, 'ru-RU', N'Смежный дублекс', N'Смежный дублекс', N'Смежный дублекс', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7078, 'en-US', N'Mobile', N'Mobile', N'Mobile', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7078, 'es-ES', N'Movil', N'Movil', N'Movil', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7078, 'ru-RU', N'Для мобильных :', N'Для мобильных :', N'Для мобильных :', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7079, 'en-US', N'Other Multifamily', N'Other Multifamily', N'Other Multifamily', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7079, 'es-ES', N'Otros multifamiliares', N'Otros multifamiliares', N'Otros multifamiliares', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7079, 'ru-RU', N'Другие многоквартирные', N'Другие многоквартирные', N'Другие многоквартирные', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7080, 'en-US', N'Detached Single Family', N'Detached Single Family', N'Detached Single Family', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7080, 'es-ES', N'Chalet unifamiliar', N'Chalet unifamiliar', N'Chalet unifamiliar', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7080, 'ru-RU', N'Отдельный для одной семьи', N'Отдельный для одной семьи', N'Отдельный для одной семьи', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7081, 'en-US', N'Townhouse', N'Townhouse', N'Townhouse', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7081, 'es-ES', N'Casa de Ciudad', N'Casa de Ciudad', N'Casa de Ciudad', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7081, 'ru-RU', N'Таунхаус', N'Таунхаус', N'Таунхаус', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7082, 'en-US', N'How leaky are the they?', N'How leaky are the they?', N'How leaky are the they?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7082, 'es-ES', N'Con fugas ¿cómo son los?', N'Con fugas ¿cómo son los?', N'Con fugas ¿cómo son los?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7082, 'ru-RU', N'Как протекающая являются ли они?', N'Как протекающая являются ли они?', N'Как протекающая являются ли они?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7083, 'en-US', N'How many leaky faucets in your home?', N'How many leaky faucets in your home?', N'How many leaky faucets in your home?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7083, 'es-ES', N'¿Cuántos grifos con fugas en su casa?', N'¿Cuántos grifos con fugas en su casa?', N'¿Cuántos grifos con fugas en su casa?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7083, 'ru-RU', N'Сколько вытекающей смесители в вашем доме?', N'Сколько вытекающей смесители в вашем доме?', N'Сколько вытекающей смесители в вашем доме?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7084, 'en-US', N'Is your attic insulated?', N'Is your attic insulated?', N'Is your attic insulated?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7084, 'es-ES', N'¿Es aislamiento de su ático?', N'¿Es aislamiento de su ático?', N'¿Es aislamiento de su ático?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7084, 'ru-RU', N'Ваш чердак изолирован?', N'Ваш чердак изолирован?', N'Ваш чердак изолирован?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7085, 'en-US', N'Are your walls insulated?', N'Are your walls insulated?', N'Are your walls insulated?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7085, 'es-ES', N'¿Están aisladas las paredes?', N'¿Están aisladas las paredes?', N'¿Están aisladas las paredes?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7085, 'ru-RU', N'Ваши стены изолированные?', N'Ваши стены изолированные?', N'Ваши стены изолированные?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7086, 'en-US', N'Is your basement heated?', N'Is your basement heated?', N'Is your basement heated?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7086, 'es-ES', N'¿Se calienta su sótano?', N'¿Se calienta su sótano?', N'¿Se calienta su sótano?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7086, 'ru-RU', N'Ваш подвал с подогревом?', N'Ваш подвал с подогревом?', N'Ваш подвал с подогревом?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7087, 'en-US', N'Do you have a crawlspace?', N'Do you have a crawlspace?', N'Do you have a crawlspace?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7087, 'es-ES', N'¿Tienes un crawlspace?', N'¿Tienes un crawlspace?', N'¿Tienes un crawlspace?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7087, 'ru-RU', N'Есть ли у вас crawlspace?', N'Есть ли у вас crawlspace?', N'Есть ли у вас crawlspace?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7088, 'en-US', N'Insulation, Windows & Doors', N'Insulation, Windows & Doors', N'Insulation, Windows & Doors', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7088, 'es-ES', N'Insulation, Windows & Doors', N'Insulation, Windows & Doors', N'Insulation, Windows & Doors', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7088, 'ru-RU', N'Insulation, Windows & Doors', N'Insulation, Windows & Doors', N'Insulation, Windows & Doors', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7089, 'en-US', N'We�re sorry. There was a problem loading your information.', N'We�re sorry. There was a problem loading your information.', N'We�re sorry. There was a problem loading your information.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7089, 'es-ES', N'Que lo siento. Hubo un problema cargando su información.', N'Que lo siento. Hubo un problema cargando su información.', N'Que lo siento. Hubo un problema cargando su información.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7089, 'ru-RU', N'Мы повторно Извините. Существует проблема с загрузкой вашей информации.', N'Мы повторно Извините. Существует проблема с загрузкой вашей информации.', N'Мы повторно Извините. Существует проблема с загрузкой вашей информации.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7090, 'en-US', N'You are using an outdated version of your browser. Please upgrade to a current version. An upgrade is not required, but it is strongly recommended so that you can access all of the features.', N'You are using an outdated version of your browser. Please upgrade to a current version. An upgrade is not required, but it is strongly recommended so that you can access all of the features.', N'You are using an outdated version of your browser. Please upgrade to a current version. An upgrade is not required, but it is strongly recommended so that you can access all of the features.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7090, 'es-ES', N'Está utilizando una versión desactualizada de su navegador. Por favor actualice a una versión actual. Una actualización no es necesaria, pero es muy recomendable para que usted pueda acceder a todas las características.', N'Está utilizando una versión desactualizada de su navegador. Por favor actualice a una versión actual. Una actualización no es necesaria, pero es muy recomendable para que usted pueda acceder a todas las características.', N'Está utilizando una versión desactualizada de su navegador. Por favor actualice a una versión actual. Una actualización no es necesaria, pero es muy recomendable para que usted pueda acceder a todas las características.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7090, 'ru-RU', N'Вы используете устаревшую версию браузера. Обновите до текущей версии. Обновление не является обязательным, но настоятельно рекомендуется, так что вы можете получить доступ ко всем функции.', N'Вы используете устаревшую версию браузера. Обновите до текущей версии. Обновление не является обязательным, но настоятельно рекомендуется, так что вы можете получить доступ ко всем функции.', N'Вы используете устаревшую версию браузера. Обновите до текущей версии. Обновление не является обязательным, но настоятельно рекомендуется, так что вы можете получить доступ ко всем функции.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7091, 'en-US', N'You are using an outdated version of your browser. Please upgrade to an up-to-date browser so that you can access our site. We support the current and previous major browser releases.', N'You are using an outdated version of your browser. Please upgrade to an up-to-date browser so that you can access our site. We support the current and previous major browser releases.', N'You are using an outdated version of your browser. Please upgrade to an up-to-date browser so that you can access our site. We support the current and previous major browser releases.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7091, 'es-ES', N'Está utilizando una versión desactualizada de su navegador. Actualice a un navegador actualizado para que usted pueda acceder nuestro sitio. Apoyamos a las versiones de navegador principales actual y anterior.', N'Está utilizando una versión desactualizada de su navegador. Actualice a un navegador actualizado para que usted pueda acceder nuestro sitio. Apoyamos a las versiones de navegador principales actual y anterior.', N'Está utilizando una versión desactualizada de su navegador. Actualice a un navegador actualizado para que usted pueda acceder nuestro sitio. Apoyamos a las versiones de navegador principales actual y anterior.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7091, 'ru-RU', N'Вы используете устаревшую версию браузера. Обновите до современный браузер так, что вы можете получить доступ к сайту. Мы поддерживаем релизы текущих и предыдущих основных браузеров.', N'Вы используете устаревшую версию браузера. Обновите до современный браузер так, что вы можете получить доступ к сайту. Мы поддерживаем релизы текущих и предыдущих основных браузеров.', N'Вы используете устаревшую версию браузера. Обновите до современный браузер так, что вы можете получить доступ к сайту. Мы поддерживаем релизы текущих и предыдущих основных браузеров.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7092, 'en-US', N'See More', N'See More', N'See More', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7092, 'es-ES', N'Ver Más', N'Ver Más', N'Ver Más', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7092, 'ru-RU', N'Показать еще', N'Показать еще', N'Показать еще', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7093, 'en-US', N'Two or more bills required for comparison.  Please check back.', N'Two or more bills required for comparison.  Please check back.', N'Two or more bills required for comparison.  Please check back.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7093, 'es-ES', N'Dos o más proyectos de ley necesarios para la comparación.  Compruebe por favor detrás.', N'Dos o más proyectos de ley necesarios para la comparación.  Compruebe por favor detrás.', N'Dos o más proyectos de ley necesarios para la comparación.  Compruebe por favor detrás.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7093, 'ru-RU', N'Два или несколько законопроектов, необходимых для сравнения.  Пожалуйста, проверьте обратно.', N'Два или несколько законопроектов, необходимых для сравнения.  Пожалуйста, проверьте обратно.', N'Два или несколько законопроектов, необходимых для сравнения.  Пожалуйста, проверьте обратно.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7094, 'en-US', N'These estimates are based on information in your Energy Profile and your billed usage.', N'These estimates are based on information in your Energy Profile and your billed usage.', N'These estimates are based on information in your Energy Profile and your billed usage.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7094, 'es-ES', N'Estas estimaciones se basan en información en su perfil de energía y su uso de la factura.', N'Estas estimaciones se basan en información en su perfil de energía y su uso de la factura.', N'Estas estimaciones se basan en información en su perfil de energía y su uso de la factura.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7094, 'ru-RU', N'Эти оценки основаны на информации в вашем профиле энергии и вашего счета использования.', N'Эти оценки основаны на информации в вашем профиле энергии и вашего счета использования.', N'Эти оценки основаны на информации в вашем профиле энергии и вашего счета использования.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7095, 'en-US', N'Please update your energy profile so that we can show you the breakdown of your energy use by category.', N'Please update your energy profile so that we can show you the breakdown of your energy use by category.', N'Please update your energy profile so that we can show you the breakdown of your energy use by category.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7095, 'es-ES', N'Por favor, actualice su perfil de energía que podemos ver el desglose de tu consumo de energía por categoría.', N'Por favor, actualice su perfil de energía que podemos ver el desglose de tu consumo de energía por categoría.', N'Por favor, actualice su perfil de energía que podemos ver el desglose de tu consumo de energía por categoría.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7095, 'ru-RU', N'Пожалуйста, обновите свой профиль энергии так, что мы можем показать вам разбивка вашего использования энергии по категориям.', N'Пожалуйста, обновите свой профиль энергии так, что мы можем показать вам разбивка вашего использования энергии по категориям.', N'Пожалуйста, обновите свой профиль энергии так, что мы можем показать вам разбивка вашего использования энергии по категориям.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7096, 'en-US', N'No billed usage is available.  Please try again when you receive your next bill.', N'No billed usage is available.  Please try again when you receive your next bill.', N'No billed usage is available.  Please try again when you receive your next bill.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7096, 'es-ES', N'No uso factura está disponible.  Por favor vuelva a intentarlo cuando reciba la próxima factura.', N'No uso factura está disponible.  Por favor vuelva a intentarlo cuando reciba la próxima factura.', N'No uso factura está disponible.  Por favor vuelva a intentarlo cuando reciba la próxima factura.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7096, 'ru-RU', N'Использование не счета доступны.  Пожалуйста, попробуйте еще раз, когда вы получите ваш следующий счет.', N'Использование не счета доступны.  Пожалуйста, попробуйте еще раз, когда вы получите ваш следующий счет.', N'Использование не счета доступны.  Пожалуйста, попробуйте еще раз, когда вы получите ваш следующий счет.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7097, 'en-US', N'Save & Next', N'Save & Next', N'Save & Next', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7097, 'es-ES', N'Guardar', N'Guardar', N'Guardar', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7097, 'ru-RU', N'Сохранить', N'Сохранить', N'Сохранить', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7098, 'en-US', N'Please complete required fields', N'Please complete required fields', N'Please complete required fields', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7098, 'es-ES', N'Por favor rellene todos los campos requeridos.', N'Por favor rellene todos los campos requeridos.', N'Por favor rellene todos los campos requeridos.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7098, 'ru-RU', N'Пожалуйста, заполните все обязательные поля.', N'Пожалуйста, заполните все обязательные поля.', N'Пожалуйста, заполните все обязательные поля.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7099, 'en-US', N'No billed usage is available.  Please try again when you receive your next bill.', N'No billed usage is available.  Please try again when you receive your next bill.', N'No billed usage is available.  Please try again when you receive your next bill.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7099, 'es-ES', N'No uso factura está disponible.  Por favor vuelva a intentarlo cuando reciba la próxima factura.', N'No uso factura está disponible.  Por favor vuelva a intentarlo cuando reciba la próxima factura.', N'No uso factura está disponible.  Por favor vuelva a intentarlo cuando reciba la próxima factura.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7099, 'ru-RU', N'Использование не счета доступны.  Пожалуйста, попробуйте еще раз, когда вы получите ваш следующий счет.', N'Использование не счета доступны.  Пожалуйста, попробуйте еще раз, когда вы получите ваш следующий счет.', N'Использование не счета доступны.  Пожалуйста, попробуйте еще раз, когда вы получите ваш следующий счет.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7100, 'en-US', N'Bill and usage insights', N'Bill and usage insights', N'Bill and usage insights', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7100, 'es-ES', N'Proyecto de ley y uso de ideas', N'Proyecto de ley y uso de ideas', N'Proyecto de ley y uso de ideas', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7100, 'ru-RU', N'Билл и использование идеи', N'Билл и использование идеи', N'Билл и использование идеи', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7101, 'en-US', N'Save & Next', N'Save & Next', N'Save & Next', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7101, 'es-ES', N'Guardar', N'Guardar', N'Guardar', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7101, 'ru-RU', N'Сохранить', N'Сохранить', N'Сохранить', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7102, 'en-US', N'Please complete required fields', N'Please complete required fields', N'Please complete required fields', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7102, 'es-ES', N'Por favor rellene todos los campos requeridos.', N'Por favor rellene todos los campos requeridos.', N'Por favor rellene todos los campos requeridos.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7102, 'ru-RU', N'Пожалуйста, заполните все обязательные поля.', N'Пожалуйста, заполните все обязательные поля.', N'Пожалуйста, заполните все обязательные поля.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7103, 'en-US', N'## Review and Update Savings Actions in Your Plan', N'## Review and Update Savings Actions in Your Plan', N'## Review and Update Savings Actions in Your Plan', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7103, 'es-ES', N'NO QUERY SPECIFIED. EXAMPLE REQUEST: GET?Q=HELLO&LANGPAIR=EN|IT', N'NO QUERY SPECIFIED. EXAMPLE REQUEST: GET?Q=HELLO&LANGPAIR=EN|IT', N'NO QUERY SPECIFIED. EXAMPLE REQUEST: GET?Q=HELLO&LANGPAIR=EN|IT', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7103, 'ru-RU', N'NO QUERY SPECIFIED. EXAMPLE REQUEST: GET?Q=HELLO&LANGPAIR=EN|IT', N'NO QUERY SPECIFIED. EXAMPLE REQUEST: GET?Q=HELLO&LANGPAIR=EN|IT', N'NO QUERY SPECIFIED. EXAMPLE REQUEST: GET?Q=HELLO&LANGPAIR=EN|IT', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7104, 'en-US', N'Benchmark your usage against similar homes.', N'Benchmark your usage against similar homes.', N'Benchmark your usage against similar homes.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7104, 'es-ES', N'Su uso contra hogares similares de referencia.', N'Su uso contra hogares similares de referencia.', N'Su uso contra hogares similares de referencia.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7104, 'ru-RU', N'Тест вашего использования против подобных домов.', N'Тест вашего использования против подобных домов.', N'Тест вашего использования против подобных домов.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7105, 'en-US', N'Often a third-party app is needed to open zip files, so we recommend using your desktop/laptop computer for green button download.', N'Often a third-party app is needed to open zip files, so we recommend using your desktop/laptop computer for green button download.', N'Often a third-party app is needed to open zip files, so we recommend using your desktop/laptop computer for green button download.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7105, 'es-ES', N'Con frecuencia se requiere una aplicación de terceros para abrir archivos zip, por lo que se recomienda usar su computadora de escritorio o portátil para descargar botón verde.', N'Con frecuencia se requiere una aplicación de terceros para abrir archivos zip, por lo que se recomienda usar su computadora de escritorio o portátil para descargar botón verde.', N'Con frecuencia se requiere una aplicación de terceros para abrir archivos zip, por lo que se recomienda usar su computadora de escritorio o portátil para descargar botón verde.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7105, 'ru-RU', N'Часто приложение сторонних требуется для открытия zip-файлов, поэтому мы рекомендуем использовать ваш desktop/портативный компьютер для загрузки зеленую кнопку.', N'Часто приложение сторонних требуется для открытия zip-файлов, поэтому мы рекомендуем использовать ваш desktop/портативный компьютер для загрузки зеленую кнопку.', N'Часто приложение сторонних требуется для открытия zip-файлов, поэтому мы рекомендуем использовать ваш desktop/портативный компьютер для загрузки зеленую кнопку.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7106, 'en-US', N'Energy Star Yardstick', N'Energy Star Yardstick', N'Energy Star Yardstick', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7106, 'es-ES', N'Criterio de estrella de energía', N'Criterio de estrella de energía', N'Criterio de estrella de energía', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7106, 'ru-RU', N'Энергия Star Yardstick', N'Энергия Star Yardstick', N'Энергия Star Yardstick', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7107, 'en-US', N'Download your usage data to use with Energy Star�s Yardstick tool', N'Download your usage data to use with Energy Star�s Yardstick tool', N'Download your usage data to use with Energy Star�s Yardstick tool', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7107, 'es-ES', N'Descargar sus datos de uso para utilizar con la herramienta criterio Energy Star', N'Descargar sus datos de uso para utilizar con la herramienta criterio Energy Star', N'Descargar sus datos de uso para utilizar con la herramienta criterio Energy Star', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7107, 'ru-RU', N'Загрузить данные об использовании для использования с Energy Star s Yardstick инструмент', N'Загрузить данные об использовании для использования с Energy Star s Yardstick инструмент', N'Загрузить данные об использовании для использования с Energy Star s Yardstick инструмент', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7108, 'en-US', N'### Create a personal savings plan!Keep track of your energy saving projects in __My Plan__. There you can create a __to-do list__ of energy saving actions you plan to do, mark off which tasks you''ve __completed__, and see estimates of how much your efforts can save.', N'### Create a personal savings plan!Keep track of your energy saving projects in __My Plan__. There you can create a __to-do list__ of energy saving actions you plan to do, mark off which tasks you''ve __completed__, and see estimates of how much your efforts can save.', N'### Create a personal savings plan!Keep track of your energy saving projects in __My Plan__. There you can create a __to-do list__ of energy saving actions you plan to do, mark off which tasks you''ve __completed__, and see estimates of how much your efforts can save.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7108, 'es-ES', N'###Crear un plan de ahorros personal! El seguimiento de su proyectos de ahorro en Plan__ __My. Se puede crear un __to-do list__ de planea, la marca de que tareas has __completed__ y ver las estimaciones de cuánto sus esfuerzos pueden guardar acciones de ahorro de energía.', N'###Crear un plan de ahorros personal! El seguimiento de su proyectos de ahorro en Plan__ __My. Se puede crear un __to-do list__ de planea, la marca de que tareas has __completed__ y ver las estimaciones de cuánto sus esfuerzos pueden guardar acciones de ahorro de energía.', N'###Crear un plan de ahorros personal! El seguimiento de su proyectos de ahorro en Plan__ __My. Se puede crear un __to-do list__ de planea, la marca de que tareas has __completed__ y ver las estimaciones de cuánto sus esfuerzos pueden guardar acciones de ahorro de energía.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7108, 'ru-RU', N'###Создать план личных сбережений! Keep track of ваши энергосберегающих проектов в __My Plan__. Там вы можете создать __to-do list__ действий, которые планируется Отметьте какие задачи вы __completed__ и увидеть оценки сколько ваши усилия могут спасти энергосберегающего.', N'###Создать план личных сбережений! Keep track of ваши энергосберегающих проектов в __My Plan__. Там вы можете создать __to-do list__ действий, которые планируется Отметьте какие задачи вы __completed__ и увидеть оценки сколько ваши усилия могут спасти энергосберегающего.', N'###Создать план личных сбережений! Keep track of ваши энергосберегающих проектов в __My Plan__. Там вы можете создать __to-do list__ действий, которые планируется Отметьте какие задачи вы __completed__ и увидеть оценки сколько ваши усилия могут спасти энергосберегающего.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7109, 'en-US', N'### Create a personal savings plan!Keep track of your energy saving projects in __My Plan__. There you can create a __to-do list__ of energy saving actions you plan to do, mark off which tasks you''ve __completed__, and see estimates of how much your efforts can save.', N'### Create a personal savings plan!Keep track of your energy saving projects in __My Plan__. There you can create a __to-do list__ of energy saving actions you plan to do, mark off which tasks you''ve __completed__, and see estimates of how much your efforts can save.', N'### Create a personal savings plan!Keep track of your energy saving projects in __My Plan__. There you can create a __to-do list__ of energy saving actions you plan to do, mark off which tasks you''ve __completed__, and see estimates of how much your efforts can save.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7109, 'es-ES', N'###Crear un plan de ahorros personal! El seguimiento de su proyectos de ahorro en Plan__ __My. Se puede crear un __to-do list__ de planea, la marca de que tareas has __completed__ y ver las estimaciones de cuánto sus esfuerzos pueden guardar acciones de ahorro de energía.', N'###Crear un plan de ahorros personal! El seguimiento de su proyectos de ahorro en Plan__ __My. Se puede crear un __to-do list__ de planea, la marca de que tareas has __completed__ y ver las estimaciones de cuánto sus esfuerzos pueden guardar acciones de ahorro de energía.', N'###Crear un plan de ahorros personal! El seguimiento de su proyectos de ahorro en Plan__ __My. Se puede crear un __to-do list__ de planea, la marca de que tareas has __completed__ y ver las estimaciones de cuánto sus esfuerzos pueden guardar acciones de ahorro de energía.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7109, 'ru-RU', N'###Создать план личных сбережений! Keep track of ваши энергосберегающих проектов в __My Plan__. Там вы можете создать __to-do list__ действий, которые планируется Отметьте какие задачи вы __completed__ и увидеть оценки сколько ваши усилия могут спасти энергосберегающего.', N'###Создать план личных сбережений! Keep track of ваши энергосберегающих проектов в __My Plan__. Там вы можете создать __to-do list__ действий, которые планируется Отметьте какие задачи вы __completed__ и увидеть оценки сколько ваши усилия могут спасти энергосберегающего.', N'###Создать план личных сбережений! Keep track of ваши энергосберегающих проектов в __My Plan__. Там вы можете создать __to-do list__ действий, которые планируется Отметьте какие задачи вы __completed__ и увидеть оценки сколько ваши усилия могут спасти энергосберегающего.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7110, 'en-US', N'Your Billed Usage', N'Your Billed Usage', N'Your Billed Usage', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7110, 'es-ES', N'Su uso de la factura', N'Su uso de la factura', N'Su uso de la factura', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7110, 'ru-RU', N'Использование счета', N'Использование счета', N'Использование счета', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7111, 'en-US', N'Your Billed Usage', N'Your Billed Usage', N'Your Billed Usage', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7111, 'es-ES', N'Su uso de la factura', N'Su uso de la factura', N'Su uso de la factura', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7111, 'ru-RU', N'Использование счета', N'Использование счета', N'Использование счета', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7112, 'en-US', N'Energy Profile', N'Energy Profile', N'Energy Profile', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7112, 'es-ES', N'Perfil energético', N'Perfil energético', N'Perfil energético', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7112, 'ru-RU', N'Профиль энергии', N'Профиль энергии', N'Профиль энергии', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7113, 'en-US', N'Energy Profile', N'Energy Profile', N'Energy Profile', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7113, 'es-ES', N'Perfil energético', N'Perfil energético', N'Perfil energético', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7113, 'ru-RU', N'Профиль энергии', N'Профиль энергии', N'Профиль энергии', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7114, 'en-US', N'Billed Usage for My Home', N'Billed Usage for My Home', N'Billed Usage for My Home', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7114, 'es-ES', N'Uso facturado para mi hogar', N'Uso facturado para mi hogar', N'Uso facturado para mi hogar', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7114, 'ru-RU', N'Использование счета для моего дома', N'Использование счета для моего дома', N'Использование счета для моего дома', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7115, 'en-US', N'Billed Usage for My Business', N'Billed Usage for My Business', N'Billed Usage for My Business', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7115, 'es-ES', N'Uso de facturado para mi empresa', N'Uso de facturado para mi empresa', N'Uso de facturado para mi empresa', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7115, 'ru-RU', N'Использование счета для моего бизнеса', N'Использование счета для моего бизнеса', N'Использование счета для моего бизнеса', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7116, 'en-US', N'Residential Profile', N'Residential Profile', N'Residential Profile', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7116, 'es-ES', N'Perfil residencial', N'Perfil residencial', N'Perfil residencial', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7116, 'ru-RU', N'Жилой профиль', N'Жилой профиль', N'Жилой профиль', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7117, 'en-US', N'Business Profile', N'Business Profile', N'Business Profile', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7117, 'es-ES', N'Perfil empresarial', N'Perfil empresarial', N'Perfil empresarial', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7117, 'ru-RU', N'Редактировать бизнес профиль', N'Редактировать бизнес профиль', N'Редактировать бизнес профиль', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7118, 'en-US', N'Billed Usage', N'Billed Usage', N'Billed Usage', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7118, 'es-ES', N'Uso de factura', N'Uso de factura', N'Uso de factura', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7118, 'ru-RU', N'Использование счета', N'Использование счета', N'Использование счета', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7119, 'en-US', N'My Profile', N'My Profile', N'My Profile', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7119, 'es-ES', N'Mi Perfil', N'Mi Perfil', N'Mi Perfil', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7119, 'ru-RU', N'Профиль', N'Профиль', N'Профиль', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7120, 'en-US', N'Residential Short Profile', N'Residential Short Profile', N'Residential Short Profile', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7120, 'es-ES', N'Residencial perfil corto', N'Residencial perfil corto', N'Residencial perfil corto', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7120, 'ru-RU', N'Жилой короткий профиль', N'Жилой короткий профиль', N'Жилой короткий профиль', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7121, 'en-US', N'Business Short Profile', N'Business Short Profile', N'Business Short Profile', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7121, 'es-ES', N'Perfil breve del negocio', N'Perfil breve del negocio', N'Perfil breve del negocio', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7121, 'ru-RU', N'Короткий профиль бизнес', N'Короткий профиль бизнес', N'Короткий профиль бизнес', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7122, 'en-US', N'Billed Usage for My Home', N'Billed Usage for My Home', N'Billed Usage for My Home', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7122, 'es-ES', N'Uso facturado para mi hogar', N'Uso facturado para mi hogar', N'Uso facturado para mi hogar', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7122, 'ru-RU', N'Использование счета для моего дома', N'Использование счета для моего дома', N'Использование счета для моего дома', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7123, 'en-US', N'Billed Usage for My Business', N'Billed Usage for My Business', N'Billed Usage for My Business', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7123, 'es-ES', N'Uso de facturado para mi empresa', N'Uso de facturado para mi empresa', N'Uso de facturado para mi empresa', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7123, 'ru-RU', N'Использование счета для моего бизнеса', N'Использование счета для моего бизнеса', N'Использование счета для моего бизнеса', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7124, 'en-US', N'Residential Short Profile', N'Residential Short Profile', N'Residential Short Profile', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7124, 'es-ES', N'Residencial perfil corto', N'Residencial perfil corto', N'Residencial perfil corto', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7124, 'ru-RU', N'Жилой короткий профиль', N'Жилой короткий профиль', N'Жилой короткий профиль', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7125, 'en-US', N'Business Short Profile', N'Business Short Profile', N'Business Short Profile', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7125, 'es-ES', N'Perfil breve del negocio', N'Perfil breve del negocio', N'Perfil breve del negocio', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7125, 'ru-RU', N'Короткий профиль бизнес', N'Короткий профиль бизнес', N'Короткий профиль бизнес', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7126, 'en-US', N'My Home Profile', N'My Home Profile', N'My Home Profile', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7126, 'es-ES', N'Mi perfil Inicio', N'Mi perfil Inicio', N'Mi perfil Inicio', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7126, 'ru-RU', N'Мой домашний профиль', N'Мой домашний профиль', N'Мой домашний профиль', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7127, 'en-US', N'My Business Profile', N'My Business Profile', N'My Business Profile', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7127, 'es-ES', N'Perfil de negocio', N'Perfil de negocio', N'Perfil de negocio', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7127, 'ru-RU', N'Мой бизнес-профиль', N'Мой бизнес-профиль', N'Мой бизнес-профиль', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7128, 'en-US', N'Next', N'Next', N'Next', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7128, 'es-ES', N'Siguiente', N'Siguiente', N'Siguiente', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7128, 'ru-RU', N'В будущее', N'В будущее', N'В будущее', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7129, 'en-US', N'Next', N'Next', N'Next', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7129, 'es-ES', N'Siguiente', N'Siguiente', N'Siguiente', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7129, 'ru-RU', N'В будущее', N'В будущее', N'В будущее', 0)
EXEC(N'INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7130, ''en-US'', N''Taking shorter showers really saves money: Every minute is 1 to 5 gallons down the drain.'', N''###OVERVIEW  
Standby power is the power used by your electronic devices when they are not being actively used. Experts estimate that the average home has _over 40 devices_ using standby power that account for almost 10% of the total electricity used in that home. Sometimes standby power serves a purpose -- to power the digital clock on your microwave or to make it possible for your television to turn on by remote. But sometimes, this standby power is serving no useful purpose -- and it''''s still costing you money! Turning devices off doesn''''t help. The only way to cut this invisible power drain is to cut off the power supply to the devices by unplugging them or using power strips.  

###WHAT YOU''''LL NEED  
* Power strip(s)
* Television and related devices (optional)
* Computer and related devices (optional)

###HOW TO DO IT  
Using a power strip for a television and related devices: 
1. Plug your television and related devices into one or more power strips. This should include the television itself plus any speakers, DVD/Blu-ray players, gaming devices, etc.
 * A special note about set-top (cable) boxes and DVRs: Cutting standby power to these devices can result in significant energy savings. Be aware that programs will not record on the DVR while it''''s power is off. In addition, some set-top boxes may temporarily lose programming information, but this is typically restored within 30 minutes of powering on.
2. Plug the power strip into the wall and put the switch in the **ON** position when you are using your television and related devices. 
3. When you are done using your television and related devices, turn everything off and turn the power strip to the **OFF** position. 
 
Using a power strip for a computer and related devices: 
1. Plug all your computer-related devices into one or more power strips. This should include computers, monitors, speakers, printers, and any other computing devices.
2. Plug the power strip into the wall and put the switch in the **ON** position when you are using your computer. 
3. When you are done using your computer and related devices, turn everything off and turn the power strip to the **OFF** position.  '', N''###OVERVIEW  
Standby power is the power used by your electronic devices when they are not being actively used. Experts estimate that the average home has _over 40 devices_ using standby power that account for almost 10% of the total electricity used in that home. Sometimes standby power serves a purpose -- to power the digital clock on your microwave or to make it possible for your television to turn on by remote. But sometimes, this standby power is serving no useful purpose -- and it''''s still costing you money! Turning devices off doesn''''t help. The only way to cut this invisible power drain is to cut off the power supply to the devices by unplugging them or using power strips.  

###WHAT YOU''''LL NEED  
* Power strip(s)
* Television and related devices (optional)
* Computer and related devices (optional)

###HOW TO DO IT  
Using a power strip for a television and related devices: 
1. Plug your television and related devices into one or more power strips. This should include the television itself plus any speakers, DVD/Blu-ray players, gaming devices, etc.
 * A special note about set-top (cable) boxes and DVRs: Cutting standby power to these devices can result in significant energy savings. Be aware that programs will not record on the DVR while it''''s power is off. In addition, some set-top boxes may temporarily lose programming information, but this is typically restored within 30 minutes of powering on.
2. Plug the power strip into the wall and put the switch in the **ON** position when you are using your television and related devices. 
3. When you are done using your television and related devices, turn everything off and turn the power strip to the **OFF** position. 
 
Using a power strip for a computer and related devices: 
1. Plug all your computer-related devices into one or more power strips. This should include computers, monitors, speakers, printers, and any other computing devices.
2. Plug the power strip into the wall and put the switch in the **ON** position when you are using your computer. 
3. When you are done using your computer and related devices, turn everything off and turn the power strip to the **OFF** position.  '', 0)')
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7130, 'es-ES', N'Tomar duchas más cortas realmente ahorra dinero: Cada minuto es de 1 a 5 galones por el desagüe.', N'Tomar duchas más cortas realmente ahorra dinero: Cada minuto es de 1 a 5 galones por el desagüe.', N'Tomar duchas más cortas realmente ahorra dinero: Cada minuto es de 1 a 5 galones por el desagüe.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7130, 'ru-RU', N'Принимая более короткий душ действительно экономит деньги: Каждую минуту от 1 до 5 галлонов коту под хвост.', N'Принимая более короткий душ действительно экономит деньги: Каждую минуту от 1 до 5 галлонов коту под хвост.', N'Принимая более короткий душ действительно экономит деньги: Каждую минуту от 1 до 5 галлонов коту под хвост.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7131, 'en-US', N'Take Shorter Showers', N'Take Shorter Showers', N'Take Shorter Showers', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7131, 'es-ES', N'Tomar duchas más cortas', N'Tomar duchas más cortas', N'Tomar duchas más cortas', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7131, 'ru-RU', N'Возьмите короткий душ', N'Возьмите короткий душ', N'Возьмите короткий душ', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7132, 'en-US', N'Billing Days', N'Billing Days', N'Billing Days', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7132, 'es-ES', N'Facturación', N'Facturación', N'Facturación', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7132, 'ru-RU', N'Адрес Биллинга', N'Адрес Биллинга', N'Адрес Биллинга', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7133, 'en-US', N'Cost', N'Cost', N'Cost', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7133, 'es-ES', N'Coste', N'Coste', N'Coste', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7133, 'ru-RU', N'Цена', N'Цена', N'Цена', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7134, 'en-US', N'Compare how your home uses energy to similar homes across the US.  
The green button download generates a zip file containing your billing data XML file(s) and a .xslt stylesheet file to help view these files.
After you click the green button, save the zip file to a location on your computer that you will remember. Then go there and extract the file(s) from the zip file. Next, go to Energy Star�s Yardstick tool and follow the instructions to upload your electric and/or natural gas files and enter in other basic information about your home.  
Note: Units for electricity are in kWh and units for natural gas are in therms.', N'Compare how your home uses energy to similar homes across the US.  
The green button download generates a zip file containing your billing data XML file(s) and a .xslt stylesheet file to help view these files.
After you click the green button, save the zip file to a location on your computer that you will remember. Then go there and extract the file(s) from the zip file. Next, go to Energy Star�s Yardstick tool and follow the instructions to upload your electric and/or natural gas files and enter in other basic information about your home.  
Note: Units for electricity are in kWh and units for natural gas are in therms.', N'Compare how your home uses energy to similar homes across the US.  
The green button download generates a zip file containing your billing data XML file(s) and a .xslt stylesheet file to help view these files.
After you click the green button, save the zip file to a location on your computer that you will remember. Then go there and extract the file(s) from the zip file. Next, go to Energy Star�s Yardstick tool and follow the instructions to upload your electric and/or natural gas files and enter in other basic information about your home.  
Note: Units for electricity are in kWh and units for natural gas are in therms.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7134, 'es-ES', N'Comparar cómo su hogar utiliza energía a casas similares en los Estados Unidos.  
La descarga de enlace genera un archivo zip que contiene sus archivos XML de datos facturación y un archivo de hoja de estilos .xslt para ver estos archivos.
Después de hacer clic en el botón verde, guarde el archivo zip a una ubicación en tu computadora que recordará. Luego ir allí y extraer los archivos desde el archivo zip. A continuación, vaya a la herramienta criterio Energy Star y siga las instrucciones para subir sus archivos eléctrico o gas natural y ente', N'Comparar cómo su hogar utiliza energía a casas similares en los Estados Unidos.  
La descarga de enlace genera un archivo zip que contiene sus archivos XML de datos facturación y un archivo de hoja de estilos .xslt para ver estos archivos.
Después de hacer clic en el botón verde, guarde el archivo zip a una ubicación en tu computadora que recordará. Luego ir allí y extraer los archivos desde el archivo zip. A continuación, vaya a la herramienta criterio Energy Star y siga las instrucciones para subir sus archivos eléctrico o gas natural y ente', N'Comparar cómo su hogar utiliza energía a casas similares en los Estados Unidos.  
La descarga de enlace genera un archivo zip que contiene sus archivos XML de datos facturación y un archivo de hoja de estilos .xslt para ver estos archivos.
Después de hacer clic en el botón verde, guarde el archivo zip a una ubicación en tu computadora que recordará. Luego ir allí y extraer los archivos desde el archivo zip. A continuación, vaya a la herramienta criterio Energy Star y siga las instrucciones para subir sus archivos eléctrico o gas natural y ente', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7134, 'ru-RU', N'Сравните, как ваш дом использует энергию для аналогичных домов в США.  
Зеленая кнопка Скачать генерирует файл zip, содержащий ваш платежный данных XML файлы и файл стилей .xslt для просмотра этих файлов.
После того, как вы нажмите зеленую кнопку, сохраните zip-файл в папку на вашем компьютере, который вы будете помнить. Затем туда и извлекать файлы из zip-файла. Затем перейдите к инструменту Yardstick s Energy Star и следуйте инструкциям, чтобы загрузить ваши электрические и/или природный газ файлы и ente', N'Сравните, как ваш дом использует энергию для аналогичных домов в США.  
Зеленая кнопка Скачать генерирует файл zip, содержащий ваш платежный данных XML файлы и файл стилей .xslt для просмотра этих файлов.
После того, как вы нажмите зеленую кнопку, сохраните zip-файл в папку на вашем компьютере, который вы будете помнить. Затем туда и извлекать файлы из zip-файла. Затем перейдите к инструменту Yardstick s Energy Star и следуйте инструкциям, чтобы загрузить ваши электрические и/или природный газ файлы и ente', N'Сравните, как ваш дом использует энергию для аналогичных домов в США.  
Зеленая кнопка Скачать генерирует файл zip, содержащий ваш платежный данных XML файлы и файл стилей .xslt для просмотра этих файлов.
После того, как вы нажмите зеленую кнопку, сохраните zip-файл в папку на вашем компьютере, который вы будете помнить. Затем туда и извлекать файлы из zip-файла. Затем перейдите к инструменту Yardstick s Energy Star и следуйте инструкциям, чтобы загрузить ваши электрические и/или природный газ файлы и ente', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7135, 'en-US', N'Accepted Accounts', N'Accepted Accounts', N'Accepted Accounts', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7135, 'es-ES', N'2017 Aclara Technologies LLC. Todos los derechos reservados. | Con tecnología de plataforma AclaraOne | 19984 Revisión v0.5.5835', N'2017 Aclara Technologies LLC. Todos los derechos reservados. | Con tecnología de plataforma AclaraOne | 19984 Revisión v0.5.5835', N'2017 Aclara Technologies LLC. Todos los derechos reservados. | Con tecnología de plataforma AclaraOne | 19984 Revisión v0.5.5835', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7135, 'ru-RU', N'2017 Aclara Technologies LLC. Все права защищены. | Работает на платформе AclaraOne | v0.5.5835 Редакция 19984', N'2017 Aclara Technologies LLC. Все права защищены. | Работает на платформе AclaraOne | v0.5.5835 Редакция 19984', N'2017 Aclara Technologies LLC. Все права защищены. | Работает на платформе AclaraOne | v0.5.5835 Редакция 19984', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7136, 'en-US', N'? 2017 Aclara Technologies LLC. All rights reserved. | Powered by AclaraOne Platform | v0.5.5835 Revision 19984', N'? 2017 Aclara Technologies LLC. All rights reserved. | Powered by AclaraOne Platform | v0.5.5835 Revision 19984', N'? 2017 Aclara Technologies LLC. All rights reserved. | Powered by AclaraOne Platform | v0.5.5835 Revision 19984', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7136, 'es-ES', N'rechazar Contactos', N'rechazar Contactos', N'rechazar Contactos', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7136, 'ru-RU', N'Отклонить Контакты', N'Отклонить Контакты', N'Отклонить Контакты', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7137, 'en-US', N'Loading Contacts', N'Loading Contacts', N'Loading Contacts', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7137, 'es-ES', N'No se han encontrado registros', N'No se han encontrado registros', N'No se han encontrado registros', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7137, 'ru-RU', N'Записей не найдено', N'Записей не найдено', N'Записей не найдено', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7138, 'en-US', N'No Records Found', N'No Records Found', N'No Records Found', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7138, 'es-ES', N'Contactos', N'Contactos', N'Contactos', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7138, 'ru-RU', N'Контакты', N'Контакты', N'Контакты', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7139, 'en-US', N'Please select at least one contact to accept/reject', N'Please select at least one contact to accept/reject', N'Please select at least one contact to accept/reject', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7139, 'es-ES', N'Cargando los contactos...', N'Cargando los contactos...', N'Cargando los contactos...', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7139, 'ru-RU', N'Загрузка контактов.', N'Загрузка контактов.', N'Загрузка контактов.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7140, 'en-US', N'Pending Accounts', N'Pending Accounts', N'Pending Accounts', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7140, 'es-ES', N'Cuentas aceptados', N'Cuentas aceptados', N'Cuentas aceptados', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7140, 'ru-RU', N'Принятые Счета', N'Принятые Счета', N'Принятые Счета', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7141, 'en-US', N'Cancel', N'cancel', N'Cancel', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7141, 'es-ES', N'Por favor, seleccione al menos un contacto para aceptar / rechazar', N'Por favor, seleccione al menos un contacto para aceptar / rechazar', N'Por favor, seleccione al menos un contacto para aceptar / rechazar', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7141, 'ru-RU', N'Пожалуйста, выберите хотя бы один контакт, чтобы принять / отклонить', N'Пожалуйста, выберите хотя бы один контакт, чтобы принять / отклонить', N'Пожалуйста, выберите хотя бы один контакт, чтобы принять / отклонить', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7142, 'en-US', N'Confirm', N'Confirm', N'Confirm', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7142, 'es-ES', N'Cancelar', N'Cancelar', N'Cancelar', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7142, 'ru-RU', N'Отмена', N'Отмена', N'Отмена', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7143, 'en-US', N'Are you sure you want to reject selected contacts ?', N'Are you sure you want to reject selected contacts ?', N'Are you sure you want to reject selected contacts ?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7143, 'es-ES', N'Confirmar', N'Confirmar', N'Confirmar', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7143, 'ru-RU', N'Подтвердить', N'Подтвердить', N'Подтвердить', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7144, 'en-US', N'Reject Contacts', N'Reject Contacts', N'Reject Contacts', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7144, 'es-ES', N'¿Está seguro de que desea rechazar los contactos seleccionados?', N'¿Está seguro de que desea rechazar los contactos seleccionados?', N'¿Está seguro de que desea rechazar los contactos seleccionados?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7144, 'ru-RU', N'Вы уверены, что хотите отклонить выбранные контакты?', N'Вы уверены, что хотите отклонить выбранные контакты?', N'Вы уверены, что хотите отклонить выбранные контакты?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7145, 'en-US', N'Connected Meters', N'Connected Meters', N'Connected Meters', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7145, 'es-ES', N'Metros conectados', N'Metros conectados', N'Metros conectados', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7145, 'ru-RU', N'Подключенные Счетчики', N'Подключенные Счетчики', N'Подключенные Счетчики', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7146, 'en-US', N'Accept', N'Accept', N'Accept', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7146, 'es-ES', N'Aceptar', N'Aceptar', N'Aceptar', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7146, 'ru-RU', N'Принять', N'Принять', N'Принять', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7147, 'en-US', N'ADDRESS:', N'ADDRESS:', N'ADDRESS:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7147, 'es-ES', N'DIRECCIÓN:', N'DIRECCIÓN:', N'DIRECCIÓN:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7147, 'ru-RU', N'АДРЕС:', N'АДРЕС:', N'АДРЕС:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7148, 'en-US', N'Account', N'Account', N'Account', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7148, 'es-ES', N'Cuenta', N'Cuenta', N'Cuenta', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7148, 'ru-RU', N'Аккаунт', N'Аккаунт', N'Аккаунт', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7149, 'en-US', N'TITLE:', N'TITLE:', N'TITLE:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7149, 'es-ES', N'TITULO:', N'TITULO:', N'TITULO:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7149, 'ru-RU', N'НАИМЕНОВАНИЕ:', N'НАИМЕНОВАНИЕ:', N'НАИМЕНОВАНИЕ:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7150, 'en-US', N'NAME:', N'NAME:', N'NAME:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7150, 'es-ES', N'NOMBRE:', N'NOMBRE:', N'NOMBRE:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7150, 'ru-RU', N'НАЗВАНИЕ:', N'НАЗВАНИЕ:', N'НАЗВАНИЕ:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7151, 'en-US', N'PHONE:', N'PHONE:', N'PHONE:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7151, 'es-ES', N'TELÉFONO:', N'TELÉFONO:', N'TELÉFONO:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7151, 'ru-RU', N'ТЕЛЕФОН:', N'ТЕЛЕФОН:', N'ТЕЛЕФОН:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7152, 'en-US', N'Custom Field 1', N'Custom Field 1', N'Custom Field 1', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7152, 'es-ES', N'Campo personalizado 1', N'Campo personalizado 1', N'Campo personalizado 1', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7152, 'ru-RU', N'Пользовательское Поле', N'Пользовательское Поле', N'Пользовательское Поле', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7153, 'en-US', N'Custom Field 2', N'Custom Field 2', N'Custom Field 2', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7153, 'es-ES', N'Campo personalizado 2', N'Campo personalizado 2', N'Campo personalizado 2', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7153, 'ru-RU', N'Пользовательские поля #2', N'Пользовательские поля #2', N'Пользовательские поля #2', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7154, 'en-US', N'First Bill Date', N'First Bill Date', N'First Bill Date', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7154, 'es-ES', N'En primer lugar, Bill Fecha', N'En primer lugar, Bill Fecha', N'En primer lugar, Bill Fecha', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7154, 'ru-RU', N'Во-первых Билл Дата', N'Во-первых Билл Дата', N'Во-первых Билл Дата', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7155, 'en-US', N'2017 Aclara Technologies LLC. All rights reserved. | Powered by AclaraOne Platform | v0.5.5835 Revision 19984', N'2017 Aclara Technologies LLC. All rights reserved. | Powered by AclaraOne Platform | v0.5.5835 Revision 19984', N'2017 Aclara Technologies LLC. All rights reserved. | Powered by AclaraOne Platform | v0.5.5835 Revision 19984', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7155, 'es-ES', N'2017 Aclara Technologies LLC. Todos los derechos reservados. | Con tecnología de plataforma AclaraOne | 19984 Revisión v0.5.5835', N'2017 Aclara Technologies LLC. Todos los derechos reservados. | Con tecnología de plataforma AclaraOne | 19984 Revisión v0.5.5835', N'2017 Aclara Technologies LLC. Todos los derechos reservados. | Con tecnología de plataforma AclaraOne | 19984 Revisión v0.5.5835', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7155, 'ru-RU', N'2017 Aclara Technologies LLC. Все права защищены. | Работает на платформе AclaraOne | v0.5.5835 Редакция 19984', N'2017 Aclara Technologies LLC. Все права защищены. | Работает на платформе AclaraOne | v0.5.5835 Редакция 19984', N'2017 Aclara Technologies LLC. Все права защищены. | Работает на платформе AclaraOne | v0.5.5835 Редакция 19984', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7156, 'en-US', N'In Use', N'In Use', N'In Use', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7156, 'es-ES', N'En uso', N'En uso', N'En uso', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7156, 'ru-RU', N'В использовании', N'В использовании', N'В использовании', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7157, 'en-US', N'Loading Meters', N'Loading Meters', N'Loading Meters', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7157, 'es-ES', N'Cargando Metros', N'Cargando Metros', N'Cargando Metros', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7157, 'ru-RU', N'Загрузка Счетчики', N'Загрузка Счетчики', N'Загрузка Счетчики', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7158, 'en-US', N'Metered', N'Metered', N'Metered', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7158, 'es-ES', N'Instrumento de medición', N'Instrumento de medición', N'Instrumento de medición', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7158, 'ru-RU', N'Измерительный прибор', N'Измерительный прибор', N'Измерительный прибор', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7159, 'en-US', N'Meter Id', N'Meter Id', N'Meter Id', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7159, 'es-ES', N'ID de la UdM de la métrica', N'ID de la UdM de la métrica', N'ID de la UdM de la métrica', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7159, 'ru-RU', N'метр', N'метр', N'метр', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7160, 'en-US', N'Meter Name', N'Meter Name', N'Meter Name', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7160, 'es-ES', N'Nombre de la métrica', N'Nombre de la métrica', N'Nombre de la métrica', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7160, 'ru-RU', N'метр', N'метр', N'метр', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7161, 'en-US', N'This screen shows pending and connected Portfolio Manager Meters', N'This screen shows pending and connected Portfolio Manager Meters', N'This screen shows pending and connected Portfolio Manager Meters', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7161, 'es-ES', N'Esta pantalla muestra la espera y conectado Metros gestor de la cartera', N'Esta pantalla muestra la espera y conectado Metros gestor de la cartera', N'Esta pantalla muestra la espera y conectado Metros gestor de la cartera', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7161, 'ru-RU', N'На этом экране отображается в ожидании и подключен портфолио Счетчики диспетчера', N'На этом экране отображается в ожидании и подключен портфолио Счетчики диспетчера', N'На этом экране отображается в ожидании и подключен портфолио Счетчики диспетчера', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7162, 'en-US', N'Meters', N'Meters', N'Meters', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7162, 'es-ES', N'Medidores', N'Medidores', N'Medidores', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7162, 'ru-RU', N'Метры', N'Метры', N'Метры', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7163, 'en-US', N'Meter Type', N'Meter Type', N'Meter Type', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7163, 'es-ES', N'Medidor', N'Medidor', N'Medidor', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7163, 'ru-RU', N'метр', N'метр', N'метр', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7164, 'en-US', N'No Records Found', N'No Records Found', N'No Records Found', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7164, 'es-ES', N'No se han encontrado registros', N'No se han encontrado registros', N'No se han encontrado registros', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7164, 'ru-RU', N'Записей не найдено', N'Записей не найдено', N'Записей не найдено', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7165, 'en-US', N'Please select at least one meter to accept/reject', N'Please select at least one meter to accept/reject', N'Please select at least one meter to accept/reject', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7165, 'es-ES', N'Por favor, seleccione al menos un metro de aceptar / rechazar', N'Por favor, seleccione al menos un metro de aceptar / rechazar', N'Por favor, seleccione al menos un metro de aceptar / rechazar', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7165, 'ru-RU', N'Пожалуйста, выберите хотя бы один метр, чтобы принять / отклонить', N'Пожалуйста, выберите хотя бы один метр, чтобы принять / отклонить', N'Пожалуйста, выберите хотя бы один метр, чтобы принять / отклонить', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7166, 'en-US', N'Pending Meters', N'Pending Meters', N'Pending Meters', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7166, 'es-ES', N'Metros pendientes', N'Metros pendientes', N'Metros pendientes', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7166, 'ru-RU', N'Pending Счетчики', N'Pending Счетчики', N'Pending Счетчики', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7167, 'en-US', N'ADDRESS:', N'ADDRESS:', N'ADDRESS:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7167, 'es-ES', N'DIRECCIÓN:', N'DIRECCIÓN:', N'DIRECCIÓN:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7167, 'ru-RU', N'АДРЕС:', N'АДРЕС:', N'АДРЕС:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7168, 'en-US', N'NUMBER OF BUILDINGS:', N'NUMBER OF BUILDINGS:', N'NUMBER OF BUILDINGS:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7168, 'es-ES', N'Total de edificios nuevos', N'Total de edificios nuevos', N'Total de edificios nuevos', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7168, 'ru-RU', N'Количество зданий:', N'Количество зданий:', N'Количество зданий:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7169, 'en-US', N'CONSTRUCTION STATUS:', N'CONSTRUCTION STATUS:', N'CONSTRUCTION STATUS:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7169, 'es-ES', N'CONSTRUCCIÓN DE ESTADO:', N'CONSTRUCCIÓN DE ESTADO:', N'CONSTRUCCIÓN DE ESTADO:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7169, 'ru-RU', N'Статус строительства', N'Статус строительства', N'Статус строительства', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7170, 'en-US', N'YEAR BUILT:', N'YEAR BUILT:', N'YEAR BUILT:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7170, 'es-ES', N'Año de construcción:', N'Año de construcción:', N'Año de construcción:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7170, 'ru-RU', N'Год постройки:', N'Год постройки:', N'Год постройки:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7171, 'en-US', N'FLOOR AREA:', N'FLOOR AREA:', N'FLOOR AREA:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7171, 'es-ES', N'Área del suelo', N'Área del suelo', N'Área del suelo', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7171, 'ru-RU', N'Общая площадь:', N'Общая площадь:', N'Общая площадь:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7172, 'en-US', N'Property', N'Property', N'Property', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7172, 'es-ES', N'Propiedad', N'Propiedad', N'Propiedad', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7172, 'ru-RU', N'Собственность', N'Собственность', N'Собственность', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7173, 'en-US', N'IS FEDERAL:', N'IS FEDERAL:', N'IS FEDERAL:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7173, 'es-ES', N'Es federal:', N'Es federal:', N'Es federal:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7173, 'ru-RU', N'IS ФЕДЕРАЛЬНОЕ:', N'IS ФЕДЕРАЛЬНОЕ:', N'IS ФЕДЕРАЛЬНОЕ:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7174, 'en-US', N'Property Name', N'Property Name', N'Property Name', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7174, 'es-ES', N'Nombre de la Propiedad', N'Nombre de la Propiedad', N'Nombre de la Propiedad', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7174, 'ru-RU', N'Название объекта', N'Название объекта', N'Название объекта', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7175, 'en-US', N'OCCUPANCY PERCENT:', N'OCCUPANCY PERCENT:', N'OCCUPANCY PERCENT:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7175, 'es-ES', N'OCUPACIÓN POR CIENTO:', N'OCUPACIÓN POR CIENTO:', N'OCUPACIÓN POR CIENTO:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7175, 'ru-RU', N'ЗАНЯТОСТЬ ПРОЦЕНТОВ:', N'ЗАНЯТОСТЬ ПРОЦЕНТОВ:', N'ЗАНЯТОСТЬ ПРОЦЕНТОВ:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7176, 'en-US', N'PRIMARY FUNCTION:', N'PRIMARY FUNCTION:', N'PRIMARY FUNCTION:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7176, 'es-ES', N'FUNCIÓN PRIMARIA:', N'FUNCIÓN PRIMARIA:', N'FUNCIÓN PRIMARIA:', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7176, 'ru-RU', N'Основная функция', N'Основная функция', N'Основная функция', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7177, 'en-US', N'Cancel', N'Cancel', N'Cancel', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7177, 'es-ES', N'Cancelar', N'Cancelar', N'Cancelar', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7177, 'ru-RU', N'Отмена', N'Отмена', N'Отмена', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7178, 'en-US', N'Confirm', N'Confirm', N'Confirm', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7178, 'es-ES', N'Confirmar', N'Confirmar', N'Confirmar', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7178, 'ru-RU', N'Подтвердить', N'Подтвердить', N'Подтвердить', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7179, 'en-US', N'Are you sure you want to reject the selected meters ?', N'Are you sure you want to reject the selected meters ?', N'Are you sure you want to reject the selected meters ?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7179, 'es-ES', N'¿Está seguro de que desea rechazar los contadores seleccionados?', N'¿Está seguro de que desea rechazar los contadores seleccionados?', N'¿Está seguro de que desea rechazar los contadores seleccionados?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7179, 'ru-RU', N'Вы уверены, что хотите отказаться от выбранных метров?', N'Вы уверены, что хотите отказаться от выбранных метров?', N'Вы уверены, что хотите отказаться от выбранных метров?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7180, 'en-US', N'Reject Meters', N'Reject Meters', N'Reject Meters', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7180, 'es-ES', N'rechazar Metros', N'rechazar Metros', N'rechazar Metros', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7180, 'ru-RU', N'Отклонить Метров', N'Отклонить Метров', N'Отклонить Метров', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7181, 'en-US', N'Reject', N'Reject', N'Reject', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7181, 'es-ES', N'Rechazar', N'Rechazar', N'Rechazar', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7181, 'ru-RU', N'Отклонить', N'Отклонить', N'Отклонить', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7182, 'en-US', N'TimeStamp', N'TimeStamp', N'TimeStamp', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7182, 'es-ES', N'Timestamp', N'Timestamp', N'Timestamp', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7182, 'ru-RU', N'Timestamp', N'Timestamp', N'Timestamp', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7183, 'en-US', N'Unit of Measure', N'Unit of Measure', N'Unit of Measure', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7183, 'es-ES', N'Unidad de medida', N'Unidad de medida', N'Unidad de medida', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7183, 'ru-RU', N'ед.изм', N'ед.изм', N'ед.изм', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7184, 'en-US', N'Meters', N'Meters', N'Meters', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7184, 'es-ES', N'Medidores', N'Medidores', N'Medidores', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7184, 'ru-RU', N'Метры', N'Метры', N'Метры', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7185, 'en-US', N'Meters', N'Meters', N'Meters', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7185, 'es-ES', N'Medidores', N'Medidores', N'Medidores', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7185, 'ru-RU', N'Метры', N'Метры', N'Метры', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7186, 'en-US', N'What size load setting do you typically use?', N'What size load setting do you typically use?', N'What size load setting do you typically use?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7186, 'es-ES', N'¿Qué carga de tamaño ajuste normalmente usas?', N'¿Qué carga de tamaño ajuste normalmente usas?', N'¿Qué carga de tamaño ajuste normalmente usas?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7186, 'ru-RU', N'Какой размер загрузки установка вы обычно используете?', N'Какой размер загрузки установка вы обычно используете?', N'Какой размер загрузки установка вы обычно используете?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7187, 'en-US', N'What style of washing machine do you have?', N'What style of washing machine do you have?', N'What style of washing machine do you have?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7187, 'es-ES', N'¿Qué estilo de lavadora tiene?', N'¿Qué estilo de lavadora tiene?', N'¿Qué estilo de lavadora tiene?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7187, 'ru-RU', N'Какой стиль Стиральная машина у вас?', N'Какой стиль Стиральная машина у вас?', N'Какой стиль Стиральная машина у вас?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7188, 'en-US', N'Ductless Heat Pump/Mini-Split', N'Ductless Heat Pump/Mini-Split', N'Ductless Heat Pump/Mini-Split', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7188, 'es-ES', N'Ducto/Mini-partió la bomba de calor', N'Ducto/Mini-partió la bomba de calor', N'Ducto/Mini-partió la bomba de calor', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7188, 'ru-RU', N'Выводного протока/мини-мультисплит теплового насоса', N'Выводного протока/мини-мультисплит теплового насоса', N'Выводного протока/мини-мультисплит теплового насоса', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7189, 'en-US', N'Is your floor insulated?', N'Is your floor insulated?', N'Is your floor insulated?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7189, 'es-ES', N'¿Se aísla el piso?', N'¿Se aísla el piso?', N'¿Se aísla el piso?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7189, 'ru-RU', N'Защищен ли ваш пол?', N'Защищен ли ваш пол?', N'Защищен ли ваш пол?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7190, 'en-US', N'How many levels does your home have, excluding basement and unfinished attic?', N'How many levels does your home have, excluding basement and unfinished attic?', N'How many levels does your home have, excluding basement and unfinished attic?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7190, 'es-ES', N'¿Cuántos niveles ¿tiene su hogar, excepto el sótano y buhardilla sin terminar?', N'¿Cuántos niveles ¿tiene su hogar, excepto el sótano y buhardilla sin terminar?', N'¿Cuántos niveles ¿tiene su hogar, excepto el sótano y buhardilla sin terminar?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7190, 'ru-RU', N'Сколько уровней вашего дома у, исключая подвал и чердак незавершенной?', N'Сколько уровней вашего дома у, исключая подвал и чердак незавершенной?', N'Сколько уровней вашего дома у, исключая подвал и чердак незавершенной?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7191, 'en-US', N'What is the square footage of your home?', N'What is the square footage of your home?', N'What is the square footage of your home?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7191, 'es-ES', N'¿Cuál es la cantidad cuadrada de su hogar?', N'¿Cuál es la cantidad cuadrada de su hogar?', N'¿Cuál es la cantidad cuadrada de su hogar?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7191, 'ru-RU', N'Какова площадь вашего дома?', N'Какова площадь вашего дома?', N'Какова площадь вашего дома?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7192, 'en-US', N'Fireplace', N'Fireplace', N'Fireplace', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7192, 'es-ES', N'Chimenea', N'Chimenea', N'Chimenea', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7192, 'ru-RU', N'Камин', N'Камин', N'Камин', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7193, 'en-US', N'Fireplace', N'Fireplace', N'Fireplace', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7193, 'es-ES', N'Chimenea', N'Chimenea', N'Chimenea', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7193, 'ru-RU', N'Камин', N'Камин', N'Камин', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7194, 'en-US', N'Fireplace', N'Fireplace', N'Fireplace', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7194, 'es-ES', N'Chimenea', N'Chimenea', N'Chimenea', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7194, 'ru-RU', N'Камин', N'Камин', N'Камин', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7195, 'en-US', N'Other', N'Other', N'Other', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7195, 'es-ES', N'Otro', N'Otro', N'Otro', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7195, 'ru-RU', N'Другое', N'Другое', N'Другое', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7196, 'en-US', N'Other', N'Other', N'Other', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7196, 'es-ES', N'Otro', N'Otro', N'Otro', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7196, 'ru-RU', N'Другое', N'Другое', N'Другое', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7197, 'en-US', N'Other', N'Other', N'Other', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7197, 'es-ES', N'Otro', N'Otro', N'Otro', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7197, 'ru-RU', N'Другое', N'Другое', N'Другое', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7198, 'en-US', N'Where is your refrigerator located?', N'Where is your refrigerator located?', N'Where is your refrigerator located?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7198, 'es-ES', N'¿Dónde está ubicado el refrigerador?', N'¿Dónde está ubicado el refrigerador?', N'¿Dónde está ubicado el refrigerador?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7198, 'ru-RU', N'Где находится ваш холодильник?', N'Где находится ваш холодильник?', N'Где находится ваш холодильник?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7199, 'en-US', N'Is there an ice dispenser in the door?', N'Is there an ice dispenser in the door?', N'Is there an ice dispenser in the door?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7199, 'es-ES', N'¿Hay un dispensador de hielo en la puerta?', N'¿Hay un dispensador de hielo en la puerta?', N'¿Hay un dispensador de hielo en la puerta?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7199, 'ru-RU', N'Есть диспенсер льда в дверь?', N'Есть диспенсер льда в дверь?', N'Есть диспенсер льда в дверь?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7200, 'en-US', N'How old is your refrigerator?', N'How old is your refrigerator?', N'How old is your refrigerator?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7200, 'es-ES', N'¿Cuántos años tiene tu refrigerador?', N'¿Cuántos años tiene tu refrigerador?', N'¿Cuántos años tiene tu refrigerador?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7200, 'ru-RU', N'Сколько лет ваш холодильник?', N'Сколько лет ваш холодильник?', N'Сколько лет ваш холодильник?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7201, 'en-US', N'What is the square footage of your home?', N'What is the square footage of your home?', N'What is the square footage of your home?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7201, 'es-ES', N'¿Cuál es la cantidad cuadrada de su hogar?', N'¿Cuál es la cantidad cuadrada de su hogar?', N'¿Cuál es la cantidad cuadrada de su hogar?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7201, 'ru-RU', N'Какова площадь вашего дома?', N'Какова площадь вашего дома?', N'Какова площадь вашего дома?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7202, 'en-US', N'Is your floor insulated?', N'Is your floor insulated?', N'Is your floor insulated?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7202, 'es-ES', N'¿Se aísla el piso?', N'¿Se aísla el piso?', N'¿Se aísla el piso?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7202, 'ru-RU', N'Защищен ли ваш пол?', N'Защищен ли ваш пол?', N'Защищен ли ваш пол?', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7203, 'en-US', N'1955 - 1964', N'1955 - 1964', N'1955 - 1964', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7203, 'es-ES', N'1955 - 1964', N'1955 - 1964', N'1955 - 1964', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7203, 'ru-RU', N'1955 - 1964', N'1955 - 1964', N'1955 - 1964', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7204, 'en-US', N'1965 - 1974', N'1965 - 1974', N'1965 - 1974', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7204, 'es-ES', N'1965 - 1974', N'1965 - 1974', N'1965 - 1974', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7204, 'ru-RU', N'1965 - 1974', N'1965 - 1974', N'1965 - 1974', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7205, 'en-US', N'1975 - 1974', N'1975 - 1974', N'1975 - 1974', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7205, 'es-ES', N'1975 - 1974', N'1975 - 1974', N'1975 - 1974', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7205, 'ru-RU', N'1975 - 1974', N'1975 - 1974', N'1975 - 1974', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7206, 'en-US', N'1985 - 1994', N'1985 - 1994', N'1985 - 1994', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7206, 'es-ES', N'1985 - 1994', N'1985 - 1994', N'1985 - 1994', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7206, 'ru-RU', N'1985 - 1994', N'1985 - 1994', N'1985 - 1994', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7207, 'en-US', N'1995 - 1999', N'1995 - 1999', N'1995 - 1999', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7207, 'es-ES', N'1995-1999', N'1995-1999', N'1995-1999', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7207, 'ru-RU', N'1995-1999', N'1995-1999', N'1995-1999', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7208, 'en-US', N'2000 - 2004', N'2000 - 2004', N'2000 - 2004', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7208, 'es-ES', N'2000 - 2004', N'2000 - 2004', N'2000 - 2004', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7208, 'ru-RU', N'2000 — 2004', N'2000 — 2004', N'2000 — 2004', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7209, 'en-US', N'2005 - 2009', N'2005 - 2009', N'2005 - 2009', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7209, 'es-ES', N'2005-2009', N'2005-2009', N'2005-2009', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7209, 'ru-RU', N'2005 – 2009 гг.', N'2005 – 2009 гг.', N'2005 – 2009 гг.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7210, 'en-US', N'2010 - 2014', N'2010 - 2014', N'2010 - 2014', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7210, 'es-ES', N'2010-2014', N'2010-2014', N'2010-2014', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7210, 'ru-RU', N'2010 - 2014 г.г.', N'2010 - 2014 г.г.', N'2010 - 2014 г.г.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7211, 'en-US', N'after 2014', N'after 2014', N'after 2014', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7211, 'es-ES', N'después de 2014', N'después de 2014', N'después de 2014', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7211, 'ru-RU', N'после 2014 года', N'после 2014 года', N'после 2014 года', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7212, 'en-US', N'before 1955', N'before 1955', N'before 1955', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7212, 'es-ES', N'antes de 1955', N'antes de 1955', N'antes de 1955', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (7212, 'ru-RU', N'до 1955 года', N'до 1955 года', N'до 1955 года', 0)
PRINT(N'Operation applied to 420 rows out of 420')

PRINT(N'Add rows to [cm].[ClientWidgetTextContent]')
INSERT INTO [cm].[ClientWidgetTextContent] ([ClientWidgetID], [TextContentKey]) VALUES (532, 'billedusage.billedusagechart.title')
INSERT INTO [cm].[ClientWidgetTextContent] ([ClientWidgetID], [TextContentKey]) VALUES (533, 'billedusagebusiness.billedusagechart.title')
INSERT INTO [cm].[ClientWidgetTextContent] ([ClientWidgetID], [TextContentKey]) VALUES (534, 'shortenergyprofile.profile.title')
INSERT INTO [cm].[ClientWidgetTextContent] ([ClientWidgetID], [TextContentKey]) VALUES (535, 'shortenergyprofilebusiness.profile.title')
PRINT(N'Operation applied to 4 rows out of 4')

PRINT(N'Add constraints to [cm].[ClientWidgetTextContent]')
ALTER TABLE [cm].[ClientWidgetTextContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidgetTextContentality_ClientWidget] FOREIGN KEY ([ClientWidgetID]) REFERENCES [cm].[ClientWidget] ([ClientWidgetID])
ALTER TABLE [cm].[ClientWidgetTextContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidgetTextContentality_TypeTextContentality] FOREIGN KEY ([TextContentKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraints to [cm].[ClientTextContentLocaleContent]')
ALTER TABLE [cm].[ClientTextContentLocaleContent] ADD CONSTRAINT [FK_ClientTextContentLocaleContent_ClientTextContent] FOREIGN KEY ([ClientTextContentID]) REFERENCES [cm].[ClientTextContent] ([ClientTextContentID])
ALTER TABLE [cm].[ClientTextContentLocaleContent] ADD CONSTRAINT [FK_ClientTextContentLocaleContent_TypeLocale] FOREIGN KEY ([LocaleKey]) REFERENCES [cm].[TypeLocale] ([LocaleKey])

PRINT(N'Add constraints to [cm].[ClientTabProfileSection]')
ALTER TABLE [cm].[ClientTabProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabProfileSection_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])
ALTER TABLE [cm].[ClientTabProfileSection] ADD CONSTRAINT [FK_ClientTabProfileSection_TypeProfileSection] FOREIGN KEY ([ProfileSectionKey]) REFERENCES [cm].[TypeProfileSection] ([ProfileSectionKey])

PRINT(N'Add constraints to [cm].[ClientTabCondition]')
ALTER TABLE [cm].[ClientTabCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabConditionality_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])
ALTER TABLE [cm].[ClientTabCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabConditionality_TypeConditionality] FOREIGN KEY ([ConditionKey]) REFERENCES [cm].[TypeCondition] ([ConditionKey])

PRINT(N'Add constraints to [cm].[ClientTabChildTab]')
ALTER TABLE [cm].[ClientTabChildTab] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabChildTab_TypeTab] FOREIGN KEY ([ChildTabKey]) REFERENCES [cm].[TypeTab] ([TabKey])
ALTER TABLE [cm].[ClientTabChildTab] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabChildTab_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])

PRINT(N'Add constraints to [cm].[ClientTabAction]')
ALTER TABLE [cm].[ClientTabAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabAction_TypeAction] FOREIGN KEY ([ActionKey]) REFERENCES [cm].[TypeAction] ([ActionKey])
ALTER TABLE [cm].[ClientTabAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabAction_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])

PRINT(N'Add constraints to [cm].[ClientProfileSectionCondition]')
ALTER TABLE [cm].[ClientProfileSectionCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSectionCondition_ClientProfileSection] FOREIGN KEY ([ClientProfileSectionID]) REFERENCES [cm].[ClientProfileSection] ([ClientProfileSectionID])
ALTER TABLE [cm].[ClientProfileSectionCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSectionCondition_TypeCondition] FOREIGN KEY ([ConditionKey]) REFERENCES [cm].[TypeCondition] ([ConditionKey])

PRINT(N'Add constraints to [cm].[ClientProfileSectionAttribute]')
ALTER TABLE [cm].[ClientProfileSectionAttribute] ADD CONSTRAINT [FK_ClientProfileSectionAttribute_ClientProfileSection] FOREIGN KEY ([ClientProfileSectionID]) REFERENCES [cm].[ClientProfileSection] ([ClientProfileSectionID])
ALTER TABLE [cm].[ClientProfileSectionAttribute] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSectionAttribute_TypeProfileAttribute] FOREIGN KEY ([ProfileAttributeKey]) REFERENCES [cm].[TypeProfileAttribute] ([ProfileAttributeKey])

PRINT(N'Add constraints to [cm].[ClientProfileAttributeProfileOption]')
ALTER TABLE [cm].[ClientProfileAttributeProfileOption] ADD CONSTRAINT [FK_ClientProfileAttributeProfileOption_ClientProfileAttribute] FOREIGN KEY ([ClientProfileAttributeID]) REFERENCES [cm].[ClientProfileAttribute] ([ClientProfileAttributeID])
ALTER TABLE [cm].[ClientProfileAttributeProfileOption] ADD CONSTRAINT [FK_ClientProfileAttributeProfileOption_TypeProfileOption] FOREIGN KEY ([ProfileOptionKey]) REFERENCES [cm].[TypeProfileOption] ([ProfileOptionKey])

PRINT(N'Add constraints to [cm].[ClientProfileAttributeCondition]')
ALTER TABLE [cm].[ClientProfileAttributeCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileAttributeCondition_ClientProfileAttribute] FOREIGN KEY ([ClientProfileAttributeID]) REFERENCES [cm].[ClientProfileAttribute] ([ClientProfileAttributeID])
ALTER TABLE [cm].[ClientProfileAttributeCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileAttributeCondition_TypeCondition] FOREIGN KEY ([ConditionKey]) REFERENCES [cm].[TypeCondition] ([ConditionKey])

PRINT(N'Add constraints to [cm].[ClientActionCondition]')
ALTER TABLE [cm].[ClientActionCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientActionCondition_ClientAction] FOREIGN KEY ([ClientActionID]) REFERENCES [cm].[ClientAction] ([ClientActionID])
ALTER TABLE [cm].[ClientActionCondition] ADD CONSTRAINT [FK_ClientActionCondition_TypeCondition] FOREIGN KEY ([ConditionKey]) REFERENCES [cm].[TypeCondition] ([ConditionKey])

PRINT(N'Add constraints to [cm].[ClientActionAppliance]')
ALTER TABLE [cm].[ClientActionAppliance] WITH NOCHECK ADD CONSTRAINT [FK_ClientActionAppliance_TypeAppliance] FOREIGN KEY ([ApplianceKey]) REFERENCES [cm].[TypeAppliance] ([ApplianceKey])
ALTER TABLE [cm].[ClientActionAppliance] WITH NOCHECK ADD CONSTRAINT [FK_ClientActionAppliance_ClientAction] FOREIGN KEY ([ClientActionID]) REFERENCES [cm].[ClientAction] ([ClientActionID])

PRINT(N'Add constraints to [cm].[ClientWidget]')
ALTER TABLE [cm].[ClientWidget] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidget_TypeTextContent] FOREIGN KEY ([IntroTextKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientWidget] ADD CONSTRAINT [FK_ClientWidget_TypeTab] FOREIGN KEY ([TabKey]) REFERENCES [cm].[TypeTab] ([TabKey])
ALTER TABLE [cm].[ClientWidget] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidget_TypeWidget] FOREIGN KEY ([WidgetKey]) REFERENCES [cm].[TypeWidget] ([WidgetKey])
ALTER TABLE [cm].[ClientWidget] ADD CONSTRAINT [FK_ClientWidget_TypeWidgetType] FOREIGN KEY ([WidgetTypeKey]) REFERENCES [cm].[TypeWidgetType] ([WidgetTypeKey])

PRINT(N'Add constraint FK_ClientWidgetConditionality_ClientWidget to [cm].[ClientWidgetCondition]')
ALTER TABLE [cm].[ClientWidgetCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidgetConditionality_ClientWidget] FOREIGN KEY ([ClientWidgetID]) REFERENCES [cm].[ClientWidget] ([ClientWidgetID])

PRINT(N'Add constraint FK_ClientWidgetConfigurationality_ClientWidget to [cm].[ClientWidgetConfiguration]')
ALTER TABLE [cm].[ClientWidgetConfiguration] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidgetConfigurationality_ClientWidget] FOREIGN KEY ([ClientWidgetID]) REFERENCES [cm].[ClientWidget] ([ClientWidgetID])

PRINT(N'Add constraints to [cm].[ClientTextContent]')
ALTER TABLE [cm].[ClientTextContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientTextContent_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
ALTER TABLE [cm].[ClientTextContent] ADD CONSTRAINT [FK_ClientTextContent_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientTextContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientTextContent_TypeTextContent] FOREIGN KEY ([TextContentKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraints to [cm].[ClientTab]')
ALTER TABLE [cm].[ClientTab] ADD CONSTRAINT [FK_ClientTab_TypeLayout] FOREIGN KEY ([LayoutKey]) REFERENCES [cm].[TypeLayout] ([LayoutKey])
ALTER TABLE [cm].[ClientTab] WITH NOCHECK ADD CONSTRAINT [FK_ClientTab_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientTab] ADD CONSTRAINT [FK_ClientTab_TypeTab] FOREIGN KEY ([TabKey]) REFERENCES [cm].[TypeTab] ([TabKey])
ALTER TABLE [cm].[ClientTab] ADD CONSTRAINT [FK_ClientTab_TypeTabType] FOREIGN KEY ([TabTypeKey]) REFERENCES [cm].[TypeTabType] ([TabTypeKey])

PRINT(N'Add constraint FK_ClientTabConfigurationality_ClientTab to [cm].[ClientTabConfiguration]')
ALTER TABLE [cm].[ClientTabConfiguration] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabConfigurationality_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])

PRINT(N'Add constraint FK_ClientTabTextContentality_ClientTab to [cm].[ClientTabTextContent]')
ALTER TABLE [cm].[ClientTabTextContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabTextContentality_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])

PRINT(N'Add constraints to [cm].[ClientProfileSection]')
ALTER TABLE [cm].[ClientProfileSection] ADD CONSTRAINT [FK_ClientProfileSection_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent] FOREIGN KEY ([DescriptionKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent3] FOREIGN KEY ([IntroTextKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientProfileSection] ADD CONSTRAINT [FK_ClientProfileSection_TypeProfileSection] FOREIGN KEY ([ProfileSectionKey]) REFERENCES [cm].[TypeProfileSection] ([ProfileSectionKey])
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent2] FOREIGN KEY ([SubTitleKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent1] FOREIGN KEY ([TitleKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraints to [cm].[ClientProfileOption]')
ALTER TABLE [cm].[ClientProfileOption] ADD CONSTRAINT [FK_ClientProfileOption_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientProfileOption] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileOption_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientProfileOption] ADD CONSTRAINT [FK_ClientProfileOption_TypeProfileOption] FOREIGN KEY ([ProfileOptionKey]) REFERENCES [cm].[TypeProfileOption] ([ProfileOptionKey])

PRINT(N'Add constraints to [cm].[ClientProfileAttribute]')
ALTER TABLE [cm].[ClientProfileAttribute] ADD CONSTRAINT [FK_ClientProfileAttribute_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientProfileAttribute] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileAttribute_TypeProfileAttribute] FOREIGN KEY ([ProfileAttributeKey]) REFERENCES [cm].[TypeProfileAttribute] ([ProfileAttributeKey])
ALTER TABLE [cm].[ClientProfileAttribute] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileAttribute_TypeTextContent] FOREIGN KEY ([QuestionTextKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraints to [cm].[ClientConfigurationBulk]')
ALTER TABLE [cm].[ClientConfigurationBulk] WITH NOCHECK ADD CONSTRAINT [FK_ClientConfigurationBulk_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
ALTER TABLE [cm].[ClientConfigurationBulk] ADD CONSTRAINT [FK_ClientConfigurationBulk_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientConfigurationBulk] WITH NOCHECK ADD CONSTRAINT [FK_ClientConfigurationBulk_TypeConfigurationBulk] FOREIGN KEY ([ConfigurationBulkKey]) REFERENCES [cm].[TypeConfigurationBulk] ([ConfigurationBulkKey])
ALTER TABLE [cm].[ClientConfigurationBulk] ADD CONSTRAINT [FK_ClientConfigurationBulk_TypeEnvironment] FOREIGN KEY ([EnvironmentKey]) REFERENCES [cm].[TypeEnvironment] ([EnvironmentKey])

PRINT(N'Add constraints to [cm].[ClientConfiguration]')
ALTER TABLE [cm].[ClientConfiguration] WITH NOCHECK ADD CONSTRAINT [FK_ClientConfiguration_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
ALTER TABLE [cm].[ClientConfiguration] ADD CONSTRAINT [FK_ClientConfiguration_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientConfiguration] WITH NOCHECK ADD CONSTRAINT [FK_ClientConfiguration_TypeConfiguration] FOREIGN KEY ([ConfigurationKey]) REFERENCES [cm].[TypeConfiguration] ([ConfigurationKey])
ALTER TABLE [cm].[ClientConfiguration] ADD CONSTRAINT [FK_ClientConfiguration_TypeEnvironment] FOREIGN KEY ([EnvironmentKey]) REFERENCES [cm].[TypeEnvironment] ([EnvironmentKey])

PRINT(N'Add constraints to [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeAction] FOREIGN KEY ([ActionKey]) REFERENCES [cm].[TypeAction] ([ActionKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeActionType] FOREIGN KEY ([ActionTypeKey]) REFERENCES [cm].[TypeActionType] ([ActionTypeKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeUom] FOREIGN KEY ([AnnualUsageSavingsUomKey]) REFERENCES [cm].[TypeUom] ([UomKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeUom1] FOREIGN KEY ([AnnualUsageSavingsUomKey]) REFERENCES [cm].[TypeUom] ([UomKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeCommodity] FOREIGN KEY ([CommodityKey]) REFERENCES [cm].[TypeCommodity] ([CommodityKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeExpression] FOREIGN KEY ([CostExpression]) REFERENCES [cm].[TypeExpression] ([ExpressionKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeDifficulty] FOREIGN KEY ([DifficultyKey]) REFERENCES [cm].[TypeDifficulty] ([DifficultyKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeHabitInterval] FOREIGN KEY ([HabitIntervalKey]) REFERENCES [cm].[TypeHabitInterval] ([HabitIntervalKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_ClientFileContent2] FOREIGN KEY ([ImageKey]) REFERENCES [cm].[TypeFileContent] ([FileContentKey])
ALTER TABLE [cm].[ClientAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientAction_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientAction_TypeTextContent1] FOREIGN KEY ([NextStepLinkText]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeNextStepLinkType] FOREIGN KEY ([NextStepLinkType]) REFERENCES [cm].[TypeNextStepLinkType] ([NextStepLinkTypeKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeExpression1] FOREIGN KEY ([RebateExpression]) REFERENCES [cm].[TypeExpression] ([ExpressionKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_ClientFileContent] FOREIGN KEY ([RebateImageKey]) REFERENCES [cm].[TypeFileContent] ([FileContentKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_ClientFileContent3] FOREIGN KEY ([VideoKey]) REFERENCES [cm].[TypeFileContent] ([FileContentKey])

PRINT(N'Add constraint FK_ClientActionSeasonality_ClientAction to [cm].[ClientActionSeason]')
ALTER TABLE [cm].[ClientActionSeason] WITH NOCHECK ADD CONSTRAINT [FK_ClientActionSeasonality_ClientAction] FOREIGN KEY ([ClientActionID]) REFERENCES [cm].[ClientAction] ([ClientActionID])

PRINT(N'Add constraint FK_ClientActionWhatIfData_ClientAction to [cm].[ClientActionWhatIfData]')
ALTER TABLE [cm].[ClientActionWhatIfData] WITH NOCHECK ADD CONSTRAINT [FK_ClientActionWhatIfData_ClientAction] FOREIGN KEY ([ClientActionID]) REFERENCES [cm].[ClientAction] ([ClientActionID])

PRINT(N'Add constraint FK_ClientAppliance_TypeTextContent to [cm].[ClientAppliance]')
ALTER TABLE [cm].[ClientAppliance] WITH NOCHECK ADD CONSTRAINT [FK_ClientAppliance_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientCommodity_TypeTextContent to [cm].[ClientCommodity]')
ALTER TABLE [cm].[ClientCommodity] WITH NOCHECK ADD CONSTRAINT [FK_ClientCommodity_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientCurrency_TypeTextContent to [cm].[ClientCurrency]')
ALTER TABLE [cm].[ClientCurrency] WITH NOCHECK ADD CONSTRAINT [FK_ClientCurrency_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientCurrency_TypeTextContent1 to [cm].[ClientCurrency]')
ALTER TABLE [cm].[ClientCurrency] WITH NOCHECK ADD CONSTRAINT [FK_ClientCurrency_TypeTextContent1] FOREIGN KEY ([SymbolKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientEnduse_TypeTextContent to [cm].[ClientEnduse]')
ALTER TABLE [cm].[ClientEnduse] WITH NOCHECK ADD CONSTRAINT [FK_ClientEnduse_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientMeasurement_TypeTextContent to [cm].[ClientMeasurement]')
ALTER TABLE [cm].[ClientMeasurement] WITH NOCHECK ADD CONSTRAINT [FK_ClientMeasurement_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientSeason_TypeTextContent to [cm].[ClientSeason]')
ALTER TABLE [cm].[ClientSeason] WITH NOCHECK ADD CONSTRAINT [FK_ClientSeason_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientTabTextContentality_TypeTextContentality to [cm].[ClientTabTextContent]')
ALTER TABLE [cm].[ClientTabTextContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabTextContentality_TypeTextContentality] FOREIGN KEY ([TextContentKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientUOM_ClientUOM1 to [cm].[ClientUOM]')
ALTER TABLE [cm].[ClientUOM] WITH NOCHECK ADD CONSTRAINT [FK_ClientUOM_ClientUOM1] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientCondition_TypeProfileOption to [cm].[ClientCondition]')
ALTER TABLE [cm].[ClientCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientCondition_TypeProfileOption] FOREIGN KEY ([ProfileOptionKey]) REFERENCES [cm].[TypeProfileOption] ([ProfileOptionKey])

PRINT(N'Add constraint FK_ClientProfileDefault_TypeProfileOption to [cm].[ClientProfileDefault]')
ALTER TABLE [cm].[ClientProfileDefault] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileDefault_TypeProfileOption] FOREIGN KEY ([DefaultKey]) REFERENCES [cm].[TypeProfileOption] ([ProfileOptionKey])

PRINT(N'Add constraints to [cm].[TypeProfileAttribute]')
ALTER TABLE [cm].[TypeProfileAttribute] ADD CONSTRAINT [FK_TypeProfileAttribute_TypeEntityLevel] FOREIGN KEY ([EntityLevelKey]) REFERENCES [cm].[TypeEntityLevel] ([EntityLevelKey])
ALTER TABLE [cm].[TypeProfileAttribute] ADD CONSTRAINT [FK_TypeProfileAttribute_TypeProfileAttributeType] FOREIGN KEY ([ProfileAttributeTypeKey]) REFERENCES [cm].[TypeProfileAttributeType] ([ProfileAttributeTypeKey])

PRINT(N'Add constraint FK_ClientApplianceProfileAttribute_TypeProfileAttribute to [cm].[ClientApplianceProfileAttribute]')
ALTER TABLE [cm].[ClientApplianceProfileAttribute] WITH NOCHECK ADD CONSTRAINT [FK_ClientApplianceProfileAttribute_TypeProfileAttribute] FOREIGN KEY ([ProfileAttributeKey]) REFERENCES [cm].[TypeProfileAttribute] ([ProfileAttributeKey])

PRINT(N'Add constraint FK_ClientCondition_TypeProfileAttribute to [cm].[ClientCondition]')
ALTER TABLE [cm].[ClientCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientCondition_TypeProfileAttribute] FOREIGN KEY ([ProfileAttributeKey]) REFERENCES [cm].[TypeProfileAttribute] ([ProfileAttributeKey])

PRINT(N'Add constraint FK_ClientProfileDefault_TypeProfileAttribute to [cm].[ClientProfileDefault]')
ALTER TABLE [cm].[ClientProfileDefault] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileDefault_TypeProfileAttribute] FOREIGN KEY ([ProfileAttributeKey]) REFERENCES [cm].[TypeProfileAttribute] ([ProfileAttributeKey])

PRINT(N'Add constraint FK_ClientWhatIfData_TypeProfileAttribute to [cm].[ClientWhatIfData]')
ALTER TABLE [cm].[ClientWhatIfData] WITH NOCHECK ADD CONSTRAINT [FK_ClientWhatIfData_TypeProfileAttribute] FOREIGN KEY ([ProfileAttributeKey]) REFERENCES [cm].[TypeProfileAttribute] ([ProfileAttributeKey])

PRINT(N'Add constraint FK_ClientTabConfigurationality_TypeConfigurationality to [cm].[ClientTabConfiguration]')
ALTER TABLE [cm].[ClientTabConfiguration] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabConfigurationality_TypeConfigurationality] FOREIGN KEY ([ConfigurationKey]) REFERENCES [cm].[TypeConfiguration] ([ConfigurationKey])

PRINT(N'Add constraint FK_ClientWidgetConfigurationality_TypeConfigurationality to [cm].[ClientWidgetConfiguration]')
ALTER TABLE [cm].[ClientWidgetConfiguration] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidgetConfigurationality_TypeConfigurationality] FOREIGN KEY ([ConfigurationKey]) REFERENCES [cm].[TypeConfiguration] ([ConfigurationKey])

PRINT(N'Add constraint FK_ClientCondition_TypeCategory to [cm].[ClientCondition]')
ALTER TABLE [cm].[ClientCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientCondition_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])

PRINT(N'Add constraint FK_ClientFileContent_TypeCategory to [cm].[ClientFileContent]')
ALTER TABLE [cm].[ClientFileContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientFileContent_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])

PRINT(N'Add constraint FK_TypeEnumeration_TypeCategory to [cm].[TypeEnumeration]')
ALTER TABLE [cm].[TypeEnumeration] WITH NOCHECK ADD CONSTRAINT [FK_TypeEnumeration_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
COMMIT TRANSACTION
GO
