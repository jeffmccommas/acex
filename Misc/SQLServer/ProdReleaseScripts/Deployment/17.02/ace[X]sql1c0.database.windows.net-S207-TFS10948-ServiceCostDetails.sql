﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

SET ANSI_NULLS ON
GO

SET ANSI_PADDING ON
GO

-- =============================================
-- Author: Phil Victor
-- Create date: 1/06/2017
-- =============================================
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
           WHERE TABLE_NAME = N'Billing_ServiceCostDetails' AND [TABLE_SCHEMA] = N'Holding')
BEGIN
            DROP TABLE [Holding].[Billing_ServiceCostDetails]
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
           WHERE TABLE_NAME = N'Billing_ServiceCostDetails' AND [TABLE_SCHEMA] = N'Holding')
BEGIN
        CREATE TABLE [Holding].[Billing_ServiceCostDetails](
                [ServiceCostDetailsKey] uniqueidentifier NOT NULL,
                [ServiceCostDetailName] VARCHAR(MAX) NOT NULL,
                [ServiceCostDetailValue] [VARCHAR](MAX) NOT NULL
)

END

-- =============================================
-- Author: Phil Victor
-- Create date: 1/06/2017
-- =============================================
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
           WHERE TABLE_NAME = N'FactServicePointBilling_ServiceCostDetails' AND [TABLE_SCHEMA] = N'dbo')
BEGIN
            DROP TABLE dbo.[FactServicePointBilling_ServiceCostDetails]
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
           WHERE TABLE_NAME = N'FactServicePointBilling_ServiceCostDetails' AND [TABLE_SCHEMA] = N'dbo')
BEGIN
        CREATE TABLE [dbo].[FactServicePointBilling_ServiceCostDetails](
                [ServiceCostDetailsKey] uniqueidentifier NOT NULL,
                [ServiceCostDetailName] VARCHAR(MAX) NOT NULL,
                [ServiceCostDetailValue] [VARCHAR](MAX) NOT NULL
)

END


-- =============================================
-- Author: Phil Victor
-- Create date: 1/06/2017
-- =============================================
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
           WHERE TABLE_NAME = N'T1U_FactBilling_ServiceCostDetails' AND [TABLE_SCHEMA] = N'ETL')
BEGIN
            DROP TABLE [ETL].[T1U_FactBilling_ServiceCostDetails]
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
           WHERE TABLE_NAME = N'T1U_FactBilling_ServiceCostDetails' AND [TABLE_SCHEMA] = N'ETL')
BEGIN
        CREATE TABLE [ETL].[T1U_FactBilling_ServiceCostDetails](
                [ServiceCostDetailsKey] uniqueidentifier NOT NULL,
                [ServiceCostDetailName] VARCHAR(MAX) NOT NULL,
                [ServiceCostDetailValue] [VARCHAR](MAX) NOT NULL
)

END


-- =============================================
-- Author: Phil Victor
-- Create date: 1/06/2017
-- =============================================
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
           WHERE TABLE_NAME = N'INS_FactBilling_ServiceCostDetails' AND [TABLE_SCHEMA] = N'ETL')
BEGIN
            DROP TABLE [ETL].[INS_FactBilling_ServiceCostDetails]
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
           WHERE TABLE_NAME = N'INS_FactBilling_ServiceCostDetails' AND [TABLE_SCHEMA] = N'ETL')
BEGIN
        CREATE TABLE [ETL].[INS_FactBilling_ServiceCostDetails](
                [ServiceCostDetailsKey] uniqueidentifier NOT NULL,
                [ServiceCostDetailName] VARCHAR(MAX) NOT NULL,
                [ServiceCostDetailValue] [VARCHAR](MAX) NOT NULL
)

END

SET ANSI_PADDING OFF
GO