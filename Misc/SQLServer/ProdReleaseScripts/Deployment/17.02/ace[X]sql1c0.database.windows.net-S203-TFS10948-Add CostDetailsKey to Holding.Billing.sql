﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

IF NOT EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[Holding].[Billing]') AND name = 'BillingCostDetailsKey')
BEGIN
    ALTER TABLE [Holding].[Billing] ADD BillingCostDetailsKey uniqueidentifier NULL
END

IF NOT EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[Holding].[Billing]') AND name = 'ServiceCostDetailsKey')
BEGIN
    ALTER TABLE [Holding].[Billing] ADD ServiceCostDetailsKey uniqueidentifier NULL
END


IF EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[Holding].[Billing]') AND name = 'CostDetailsKey')
BEGIN
    ALTER TABLE [Holding].[Billing] DROP COLUMN CostDetailsKey
END

IF EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'[Holding].[Billing]') AND name = 'BillCostDetailsKey')
BEGIN
    ALTER TABLE [Holding].[Billing] DROP COLUMN BillCostDetailsKey
END
