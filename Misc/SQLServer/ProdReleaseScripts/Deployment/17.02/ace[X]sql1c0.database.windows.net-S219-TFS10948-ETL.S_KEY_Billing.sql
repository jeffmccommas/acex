﻿/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[S_KEY_Billing]    Script Date: 1/10/2017 12:58:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 9/19/2014
-- Description:

-- 01/10/2017 -- Phil Victor -- Added CostDetailsKey
-- =============================================
ALTER PROCEDURE [ETL].[S_KEY_Billing] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT  ClientId ,
                AccountId ,
                PremiseId ,
                CustomerId ,
                ServiceContractId ,
                ServicePointId ,
                MeterId ,
                MeterType ,
                ReplacedMeterId ,
                RateClass ,
                StartDate ,
                EndDate ,
                CommodityId ,
                b.AMIStartDate ,
                b.AMIEndDate ,
                SourceId ,
                TrackingId ,
                TrackingDate,
                [BillingCostDetailsKey],
                [ServiceCostDetailsKey]
        FROM    ( SELECT    ClientId ,
                            AccountId ,
                            PremiseId ,
                            CustomerId ,
                            ServiceContractId ,
                            ServicePointId ,
                            MeterId ,
                            MeterType ,
                            ReplacedMeterId ,
                            RateClass ,
                            BillStartDate AS StartDate ,
                            BillEndDate EndDate ,
                            BillDays ,
                            TotalUnits ,
                            TotalCost ,
                            CommodityId ,
                            AMIStartDate ,
                            AMIEndDate ,
                            SourceId ,
                            TrackingId ,
                            TrackingDate ,
                            [BillingCostDetailsKey],
                            [ServiceCostDetailsKey],
                            ROW_NUMBER() OVER ( PARTITION BY ClientId,
                                                AccountId, PremiseId,
                                                CustomerId, ServiceContractId,
                                                BillStartDate, BillEndDate,
                                                CommodityId ORDER BY TrackingDate DESC ) AS SortId
                  FROM      Holding.Billing
                  WHERE     ClientId = @ClientID
                ) b
        WHERE   SortId = 1
        ORDER BY ClientId ,
                PremiseId ,
                AccountId ,
                CustomerId ,
                ServiceContractId ,
                StartDate ,
                EndDate ,
                CommodityId;


    END;