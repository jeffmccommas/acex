﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [Holding].[InsertBillingData]    Script Date: 1/9/2017 11:04:18 AM ******/
DROP PROCEDURE [Holding].[InsertBillingData]
GO
/****** Object:  UserDefinedTableType [Holding].[BillingTableType]    Script Date: 1/9/2017 11:04:18 AM ******/
DROP TYPE [Holding].[BillingTableType]
GO

/****** Object:  UserDefinedTableType [Holding].[BillingTableType]    Script Date: 1/9/2017 11:04:18 AM ******/
CREATE TYPE [Holding].[BillingTableType] AS TABLE(
    [ClientID] [int] NOT NULL,
    [CustomerID] [varchar](50) NOT NULL,
    [AccountID] [varchar](50) NOT NULL,
    [PremiseId] [varchar](50) NOT NULL,
    [ServiceContractId] [varchar](64) NOT NULL,
    [ServicePointId] [varchar](64) NULL,
    [MeterId] [varchar](64) NULL,
    [BillStartDate] [varchar](20) NOT NULL,
    [BillEndDate] [varchar](20) NOT NULL,
    [AMIStartDate] [varchar](20) NULL,
    [AMIEndDate] [varchar](20) NULL,
    [totalunits] [decimal](18, 2) NOT NULL,
    [totalcost] [decimal](18, 2) NOT NULL,
    [commodityid] [int] NOT NULL,
    [BillPeriodTypeId] [int] NOT NULL,
    [BillCycleScheduleId] [varchar](50) NULL,
    [UOM] [int] NOT NULL,
    [RateClass] [varchar](256) NULL,
    [MeterType] [varchar](64) NULL,
    [ReplacedMeterId] [varchar](64) NULL,
    [ReadQuality] [varchar](50) NULL,
    [ReadDate] [varchar](20) NULL,
    [DueDate] [varchar](20) NULL,
    [SourceId] [int] NOT NULL,
    [TrackingId] [varchar](50) NOT NULL,
    [TrackingDate] [varchar](20) NOT NULL,
    [BillingCostDetailsKey] [uniqueidentifier] NULL,
    [ServiceCostDetailsKey] [uniqueidentifier] NULL
)
GO

/****** Object:  StoredProcedure [Holding].[InsertBillingData]    Script Date: 1/9/2017 11:04:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [Holding].[InsertBillingData]
    @BillTbl AS Holding.BillingTableType READONLY
AS /**********************************************************************************************************
* SP Name:
*       holding.InsertBillingdata
* Parameters:
*       CustomerHoldingTable            BillingTableType
* Purpose:  This stored procedure insertsrows into the billing holding tables
*
* ---------------------------------------------------------------------------------------------------------------------------
* Philip Victor - 1/9/2017 - Added CostDetailsKey
**********************************************************************************************************/
    BEGIN
        SET NOCOUNT ON;
        DECLARE @servicescount INT;

        INSERT  INTO Holding.Billing
                ( ClientId ,
                  CustomerId ,
                  AccountId ,
                  PremiseId ,
                  ServiceContractId ,
                  ServicePointId ,
                  MeterId ,
                  BillStartDate ,
                  BillDays ,
                  BillEndDate ,
                  TotalUnits ,
                  TotalCost ,
                  CommodityId ,
                  BillPeriodTypeId ,
                  BillCycleScheduleId ,
                  UOMId ,
                  AMIStartDate ,
                  AMIEndDate ,
                  RateClass ,
                  MeterType ,
                  ReplacedMeterId ,
                  ReadQuality ,
                  ReadDate ,
                  DueDate ,
                  SourceId ,
                  TrackingId ,
                  TrackingDate,
                    [BillingCostDetailsKey],
                    [ServiceCostDetailsKey]
                )
                SELECT  ClientID ,
                        CustomerID ,
                        AccountID ,
                        PremiseId ,
                        ServiceContractId ,
                        ServicePointId ,
                        MeterId ,
                        BillStartDate ,
                        DATEDIFF(dd, BillStartDate, BillEndDate) ,
                        BillEndDate ,
                        totalunits ,
                        totalcost ,
                        commodityid ,
                        BillPeriodTypeId ,
                        BillCycleScheduleId ,
                        UOM ,
                        AMIStartDate ,
                        AMIEndDate ,
                        RateClass ,
                        MeterType ,
                        ReplacedMeterId ,
                        ReadQuality ,
                        ReadDate ,
                        DueDate ,
                        SourceId ,
                        TrackingId ,
                        TrackingDate,
                        [BillingCostDetailsKey],
                        [ServiceCostDetailsKey]
                FROM    @BillTbl;


        SELECT  @@Rowcount;
    END; --proc
GO