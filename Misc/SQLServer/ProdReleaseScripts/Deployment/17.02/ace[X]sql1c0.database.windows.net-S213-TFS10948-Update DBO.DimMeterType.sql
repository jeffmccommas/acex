﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


BEGIN
   IF NOT EXISTS ( SELECT * FROM [dbo].[DimMeterType]
                   WHERE [MeterTypeDescription] = 'REGULAR'
                   AND [MeterTypeName] = 'REGULAR')
   BEGIN
            INSERT INTO [dbo].[DimMeterType]  ([MeterTypeDescription] ,[MeterTypeName])
                 VALUES ('REGULAR','REGULAR')
   END
END


BEGIN
   IF NOT EXISTS ( SELECT * FROM [dbo].[DimMeterType]
                   WHERE [MeterTypeDescription] = 'AMI'
                   AND [MeterTypeName] = 'AMI')
   BEGIN
            INSERT INTO [dbo].[DimMeterType]  ([MeterTypeDescription] ,[MeterTypeName])
                 VALUES ('AMI','AMI')
   END
END


BEGIN
   IF NOT EXISTS ( SELECT * FROM [dbo].[DimMeterType]
                   WHERE [MeterTypeDescription] = 'AMR'
                   AND [MeterTypeName] = 'AMR')
   BEGIN
            INSERT INTO [dbo].[DimMeterType]  ([MeterTypeDescription] ,[MeterTypeName])
                 VALUES ('AMR','AMR')
   END
END