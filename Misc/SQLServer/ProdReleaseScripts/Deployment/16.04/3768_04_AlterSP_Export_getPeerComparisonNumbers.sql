USE [InsightsDW_224]
GO
/****** Object:  StoredProcedure [Export].[usp_HE_Report_Get_peer_comparison_numbers]    Script Date: 4/18/2016 4:35:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
-- =============================================
-- Description:	This proc gets the benchmarks customers with matching attributes 
--				for particular profile which is passed in as a param
--				and uses the information to UPDATE the the home_energy_report_staging_allcolumns table
-- Written by:	Sunil
-- =============================================
-- Changed by:	Wayne
-- Change date:	8.5.14		
-- Description:	v 1.0.0
--				Changed to work with new DW tables
--				Added some comments
--				Renamed vars FROM 'fuel' to 'commodity'
-- =============================================
-- Changed by:	Wayne	
-- Change date: 8.27.14
-- Description:	v 1.0.0
--			added counts for bills
-- =============================================
-- Changed by:	Wayne
-- Change date: 10.14.2014
-- Description:	v 1.10.0
--	
-- try to push to right		
-- =============================================
-- Changed by:	Wayne
-- Change date: 10.17.2014
-- Description:	v 1.14.0
--	
-- Waste goal should be based on monthly totals for Monthy report
-- and annual totals for Annual report	
-- =============================================
-- Changed by:	Wayne
-- Change date: 10.17.2014
-- Description:	v 1.16.0
--	
-- ComparetoEfficientGMontlyCostPercent & ComparetoAverageGMontlyCostPercent values 
-- should be calculated of values that have already been rounded
-- =============================================
-- Changed by:	Wayne
-- Change date: 11.14.2014
-- Description:	v 1.24.0
--	
-- Change the format of certain date columns based on channel param
-- The following columns were affected:
--		BillStartDate, 
--		BilEndDate, 
--		and all the month labels
-- these column need to be in format like Jan 01 2014 
-- when the channels is Email, but do not change existing format for Print
--
-- =============================================
-- Changed by:	Wayne
-- Change date: 11.17.2014
-- Description:	v 1.26.0    (part of multiple items)
--	
-- put in some checks for bad test data
-- =============================================
-- Changed by:	Wayne
-- Change date: 11.19.2014
-- Description:	v 1.28.0
--	
-- added logic for report number to
-- [Export].[usp_HE_Report_Get_peer_comparison_numbers]
-- if month 13 is null or <=0
-- then use a 1 otherwise use the parameter value
-- and removed a rdundant update being done 
-- to report number in 
-- [Export].[usp_HE_Report_Get_AMI]
-- =============================================
-- Changed by:	Wayne
-- Change date: 12.31.2014
-- Description:	v 1.33.0    
--	
-- put in some checks for bad test data dividing by zero
-- =============================================
-- Changed by:	Wayne
-- Change date: 12.31.2014
-- Description:	v 1.33.0 (same version)
--	
--  Removed the WITH (NOLOCK)'s because
--  they were causing race conditions
-- =============================================
-- Changed by:	Wayne
-- Change date:	8.3.2016
-- Version:		v 15.06.0 
--	
-- Description:	allowed for 2 character report number
--				
-- =============================================
-- Changed by:	Wayne
-- Change date:	08.26.2016
-- Version:		v 15.06.0 
--	
-- Description:	added a null check in the insert staement
--				for "load the monthly benchmarks into the temp table"
--				
-- =============================================
-- Changed by:	George
-- Change date:	10.09.2015
-- Version:		v 15.06.0 
--	
-- Description:	modified for UIL (224)
--				
-- =============================================
*/
ALTER procedure [Export].[usp_HE_Report_Get_peer_comparison_numbers]
	@profile_name		[varchar](50),
	@bill_year			[varchar](4),
	@bill_month			[varchar](2),
	@channel			[varchar](5),
	@report_number		[varchar](2),
	@run_id 			[uniqueidentifier]	= null
	with recompile			-- force generation of query plan`
as
begin

set nocount on

if @run_id is null set @run_id = newid();

declare 
		@energyObjectCategory varchar (50)

declare
		@clientID int, 
		@period varchar (10) , 
		@season_List varchar (100),
		@progress_msg 	varchar(max),
		@component 		varchar(400) = '[Export].[usp_HE_Report_Get_peer_comparison_numbers]',		
		@new_line 		char(2) =  char(13) + char(10),
		@benchmarkCTFeild int = 0,
		@benchmarkCTPeriod INT = 0;
		

/*
Go get the profile and 
populate the vars
*/
    SELECT  @clientID = ClientId ,
            @period = Period ,
            @season_List = SeasonList
    FROM    [Export].[HomeEnergyReportProfile]
    WHERE   [ProfileName] = @profile_name;

    SELECT  @benchmarkCTFeild = CONVERT(INT, ISNULL(Value, @benchmarkCTFeild))
    FROM    Export.HomeEnergyReportSetting
    WHERE   ProfileName = @profile_name
            AND Name = 'benchmark_comparison_field'; 

    SELECT  @benchmarkCTPeriod = CONVERT(INT, ISNULL(Value, @benchmarkCTPeriod))
    FROM    Export.HomeEnergyReportSetting
    WHERE   ProfileName = @profile_name
            AND Name = 'benchmark_comparison_period'; 

/*
Define some additional vars
*/
		declare
			@PremiseKey int,
			@BillDateKey int,
			@CommodityKey int,
			@MyUsage decimal(18, 2) ,
			@MyCost decimal(18, 2) ,
			@AverageUsage decimal(18, 2) ,
			@AverageCost decimal(18, 2) ,
			@EfficientUsage decimal(18, 2) ,
			@EfficientCost decimal(18, 2) ,
			@MonthLabel char (6) ,
			@row_id varchar (2) , 
			@sqlcmd varchar (2048),
			@CommodityDesc varchar (50),
			@column_month date,
			@count int = 1;

--================================================================= BENCHMARKS ====================================
/*
create a temp table
to hold monthly benchmarks
*/
		if object_id ('tempdb..#PeerBenchmarks') is not null drop table #PeerBenchmarks
		CREATE TABLE #PeerBenchmarks(
			[PremiseKey] [int] NOT NULL,
			EndDateKey [int] NOT NULL,
			StartDateKey [int] NOT NULL,
			CommodityKey [int] NOT NULL,
			[MyUsage] [decimal](18, 2) NULL,
			[MyCost] [decimal](18, 2) NULL,
			[AverageUsage] [decimal](18, 2) NULL,
			[AverageCost] [decimal](18, 2) NULL,
			[EfficientUsage] [decimal](18, 2) NULL,
			[EfficientCost] [decimal](18, 2) NULL,
			[row_id] int NULL,
			CommodityDesc varchar (20) NULL
		)
		
/*
	create a temp table
	to hold quarterly benchmarks
*/
		if object_id ('tempdb..#PeerBenchmarks_quarterly') is not null drop table #PeerBenchmarks_quarterly
		CREATE TABLE #PeerBenchmarks_quarterly(
			[PremiseKey] [int] NOT NULL,
			CommodityKey [int] NOT NULL,
			[MyUsage] [decimal](18, 2) NULL,
			[MyCost] [decimal](18, 2) NULL,
			[AverageUsage] [decimal](18, 2) NULL,
			[AverageCost] [decimal](18, 2) NULL,
			[EfficientUsage] [decimal](18, 2) NULL,
			[EfficientCost] [decimal](18, 2) NULL,
			[row_id] int NULL,
			CommodityDesc varchar (20) NULL
		)

/*
create a temp table
to hold annual benchmarks
*/
		if object_id ('tempdb..#PeerBenchmarks_annual') is not null drop table #PeerBenchmarks_annual
		CREATE TABLE #PeerBenchmarks_annual(
			[PremiseKey] [int] NOT NULL,
			CommodityKey [int] NOT NULL,
			[MyUsage] [decimal](18, 2) NULL,
			[MyCost] [decimal](18, 2) NULL,
			[AverageUsage] [decimal](18, 2) NULL,
			[AverageCost] [decimal](18, 2) NULL,
			[EfficientUsage] [decimal](18, 2) NULL,
			[EfficientCost] [decimal](18, 2) NULL,
			[row_id] int NULL,
			CommodityDesc varchar (20) NULL
		)


/*
the period @startrangedate to @endrangedate will be 13 months, !! dont !! do annual calculations on this range, just show these bills
the period @calcstartrange to @endrangedate will be 12 months, alway do annual calculations on this range,
*/


declare @number_of_months_for_report int = 24;				-- there are 24 months on the paper report.
if @channel = 'email' set @number_of_months_for_report = 2;	-- there are only 2 months on the email report...current and previous.

-- the end date range is the last day of the month based on the report date
DECLARE	@endrangedate date= DATEADD(DAY,-1,DATEADD(MONTH,1,(CAST(@bill_year+@bill_month+'01' AS DATE))))  

-- the start date range is the first day of the month based on the number of months in the report and the report date
DECLARE	@startrangedate date= DATEADD(MONTH, -(@number_of_months_for_report - 1) ,CAST(@bill_year+@bill_month+'01' AS DATE)) 

--this is same bill offset by 12 months we are using
DECLARE	@annual_cutoff_date date= DATEADD(MONTH, -12 ,CAST(@bill_year+@bill_month+'01' AS DATE)) 

DECLARE	@calcstartrange date=DATEADD(MONTH, 1, @startrangedate)

DECLARE @calcstartrangekey int=cast(convert(char(8), @calcstartrange, 112) as int)
DECLARE @startrangekey INT =cast(convert(char(8), @startrangedate, 112) as int) 
DECLARE @annual_cutoff_date_key INT =cast(convert(char(8), @annual_cutoff_date, 112) as int) 
DECLARE @endrangekey INT =cast(convert(char(8), @endrangedate, 112) as int) 

set @progress_msg = 'peer comparison dates   ' +
		'@number_of_months_for_report: ' + cast(@number_of_months_for_report as varchar) + '''; ' +
		'@startrangedate:''' + cast(@startrangedate as varchar) + '''; ' +
		'@endrangedate:''' + cast(@endrangedate as varchar) + '''; ' +
		'@calcstartrange:''' + cast(@calcstartrange as varchar) + '''; ' + 
		'@annual_cutoff_date:''' + cast(@annual_cutoff_date as varchar) + '''; ';
		
exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;


/*
load the possible monthly billing columns into a table
*/
if object_id ('tempdb..#ColumnDates') is not null drop table #ColumnDates
CREATE TABLE #ColumnDates
(
	rowid [INT] IDENTITY(1,1) NOT NULL,
	ColumnMonth date,
	ColumnMonthDateKey int
)
INSERT #ColumnDates (ColumnMonth, ColumnMonthDateKey)
	SELECT DISTINCT  
		d.month,
		cast(convert(char(8), d.month, 112) as int)  
		FROM 
			dbo.DimDate d
		WHERE 
			DateKey BETWEEN @startrangekey AND @endrangekey
		ORDER BY cast(convert(char(8), d.month, 112) as int) desc	

--================================================================= MONTHLY LOOP ====================================
/*
load the monthly benchmarks into the temp table
*/

	exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = 'start   populate #PeerBenchmarks';

	insert #PeerBenchmarks
		(
			  row_id
			, [PremiseKey]
			, EndDateKey
			, StartDateKey
			, [CommodityKey]
			, [MyUsage]
			, [MyCost]
			, [AverageUsage]
			, [AverageCost]
			, [EfficientUsage]
			, [EfficientCost]
			, CommodityDesc
		)
		select			
			  colid
			, [PremiseKey]
			, EndDateKey
			, StartDateKey
			, [CommodityKey]
			, [MyUsage]
			, [MyCost]
			, [AverageUsage]
			, [AverageCost]
			, [EfficientUsage]
			, [EfficientCost]
			, CommodityDesc
			FROM
				(
					SELECT * 
						from
							(
								SELECT  ColumnMonthDateKey, rowid AS colid  FROM 	 #columndates 
							) as f
							LEFT JOIN 
							(
								SELECT 
									d.month AS monthdate
								  , ROW_NUMBER () OVER (PARTITION BY fb.PremiseKey, fb.CommodityId ORDER BY fb.EndDate DESC) AS row_id
								  , fb.[PremiseKey]
								  , cast(convert(char(8), fb.EndDate, 112) as int) as EndDateKey
								  , cast(convert(char(8), fb.StartDate, 112) as int) as StartDateKey								  
								  , f.CommodityKey
								  , [MyUsage]
								  , [MyCost]
								  , [AverageUsage]
								  , [AverageCost]
								  , [EfficientUsage]
								  , [EfficientCost]
								  , f.CommodityDesc
								  ,cast(convert(char(8), d.month, 112) as int) monthdatekey
								  FROM [dbo].BillBenchmarkResults as  fb  
									INNER JOIN	[dbo].[DimDate] d	    										ON d.FullDateAlternateKey = fb.EndDate
									INNER JOIN	[dbo].[DimCommodity] f  										ON f.CommodityId = fb.CommodityId
									INNER JOIN	[dbo].[DimPremise] p1   										ON p1.PremiseKey = fb.PremiseKey
									-- we will only look at benchmarks for premises that will be on the report
									INNER JOIN  [export].[home_energy_report_staging_allcolumns] as [staging]	ON [staging].[PremiseId] = [p1].[PremiseId]	
									WHERE 
										p1.clientID = @clientID
										AND d.DateKey BETWEEN @startrangekey AND  @endrangekey
										and fb.StartDate is not null
										and fb.PeriodType = 1		-- monthly
							)	 AS a ON a.monthdatekey=f.ColumnMonthDateKey 
				) as fb2
			WHERE fb2.PremiseKey IS NOT null;
			
	exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = 'success populate #PeerBenchmarks';
	
	select @count = count(*) from #PeerBenchmarks;
	select @progress_msg = '#PeerBenchmarks count ' + cast(@count as varchar);
	exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
	
	--
	-- fill in the month labels for every month, regardless of whether or not there is data for a particular account
	--
	declare month_cursor cursor local static forward_only for
		select ColumnMonth, cast(rowid as varchar) from #ColumnDates order by rowid
	set	@sqlcmd = '		
			UPDATE	[export].[home_energy_report_staging_allcolumns]
			set	
				';			
	set @count = 0;
	
	open month_cursor
	fetch next from month_cursor into @column_month, @row_id	
	while @@fetch_status = 0 begin

		if @count != 0 set @sqlcmd = @sqlcmd + @new_line + ', ';			
			
		set	@sqlcmd = @sqlcmd + 'MonthLabel' + @row_id + ' = ''' + format(@column_month, 'MMM') + '''';
	
		fetch next from month_cursor into @column_month, @row_id;
		set @count = @count + 1;

	end
	close month_cursor;
	deallocate month_cursor;
	
	if @count > 0 begin
		set @progress_msg = 'start   @sqlcmd:' + @sqlcmd;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
	
		exec (@sqlcmd)
	
		set @progress_msg = 'success @sqlcmd:' + @sqlcmd;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
	end
	else begin
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = '#ColumnDates is empty';
	end


	--
	--	month labels filled in
	--
	

/*
create a cursor
*/
	DECLARE peerinfo_cursor CURSOR local forward_only FOR
		SELECT	df.CommodityDesc, cast(row_id as varchar)
			FROM	
				#PeerBenchmarks pbm  
				INNER JOIN [dbo].[DimCommodity] df  ON df.CommodityKey = pbm.CommodityKey
			GROUP BY df.CommodityDesc, row_id
			ORDER BY df.CommodityDesc, row_id;
	OPEN peerinfo_cursor
	FETCH NEXT FROM peerinfo_cursor INTO @CommodityDesc, @row_id
	SELECT	@count = 1

/*
loop thru and UPDATE the home_energy_report_staging_allcolumns table
with benchmark values
*/

	exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = 'start    pocess monthly benchmarks';
	
	WHILE @@fetch_status = 0 BEGIN

		select @progress_msg = 'start @CommodityDesc:''' + @CommodityDesc + '''; @row_id:' + cast(@row_id as varchar);
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
		
		SELECT	@sqlcmd = '
			UPDATE	he
			set	
				MyHome'			+ LEFT (@CommodityDesc, 1) + 'Usage'	+ @row_id + ' = b2.[MyUsage],
				MyHome'			+ LEFT (@CommodityDesc, 1) + 'Cost'		+ @row_id + ' = b2.[MyCost],
				AverageHome'	+ LEFT (@CommodityDesc, 1) + 'Usage'	+ @row_id + ' = b2.[AverageUsage],
				AverageHome'	+ LEFT (@CommodityDesc, 1) + 'Cost'		+ @row_id + ' = b2.[AverageCost],
				EfficientHome'	+ LEFT (@CommodityDesc, 1) + 'Usage'	+ @row_id + ' = b2.[EfficientUsage],
				EfficientHome'	+ LEFT (@CommodityDesc, 1) + 'Cost'		+ @row_id + ' = b2.[EfficientCost]
			FROM 
				#PeerBenchmarks b2  
				INNER JOIN DimPremise p2 on p2.PremiseKey = b2.PremiseKey
				INNER JOIN [export].[home_energy_report_staging_allcolumns] he on he.PremiseID = p2.PremiseId
				where 
					b2.row_id = ' + @row_id + ' and b2.CommodityDesc = ''' + @CommodityDesc + ''' and b2.premisekey is not null';
				


		select @progress_msg = 'start   @sqlcmd:' + @sqlcmd;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
	
		exec (@sqlcmd)
	
		select @progress_msg = 'success @sqlcmd:' + @sqlcmd;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
	

		IF @row_id = 1 BEGIN

			SELECT	@sqlcmd = '
					UPDATE	he
					set	
						CTEgasMonthlyCostPercent = 
						    case when CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.EfficientHomeGCost1)))  AS money) !=0 then
								((CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.MyHomeGCost1))) AS money) -CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.EfficientHomeGCost1))) AS money))
								/CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.EfficientHomeGCost1)))  AS money)
									   )  *100 else 0 end,
						CTAgasMonthlyCostPercent =  
						   case when CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.AverageHomeGCost1))) AS money) !=0 then
								 ((CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.MyHomeGCost1))) AS money) -CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.AverageHomeGCost1)))  AS money))
								 /CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.AverageHomeGCost1))) AS money)
                                           )   * 100 else 0 end,';
			SELECT @sqlcmd=@sqlcmd +  
					CASE WHEN @channel='email' THEN 
						' BillStartDate'+ @CommodityDesc + '= format(CAST(CAST(b2.StartDateKey as char(8)) AS datetime),''MMM dd yyyy'', ''en-US'' ) ,
						BillEndDate'+ @CommodityDesc + '=   format(CAST(CAST(b2.EndDateKey as char(8)) AS datetime),''MMM dd yyyy'', ''en-US'' ) '
					ELSE
						' BillStartDate'+ @CommodityDesc + '=  CAST(b2.StartDateKey AS VARCHAR(8)) ,
						BillEndDate'+ @CommodityDesc + '=   CAST(b2.EndDateKey AS VARCHAR(8)) '
					END;
					
			SELECT @sqlcmd=@sqlcmd +  
					' FROM #PeerBenchmarks b2  
						INNER JOIN DimPremise p2	 									
						on p2.PremiseKey = b2.PremiseKey
						INNER JOIN [export].[home_energy_report_staging_allcolumns] he	 
						on he.PremiseID = p2.PremiseId
						where row_id = ' + @row_id + ' and b2.CommodityDesc = ''' + @CommodityDesc + '''					
					';
	
			select @progress_msg = 'start   @sqlcmd:' + @sqlcmd;
			exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
	
			exec (@sqlcmd)	-- monthly calc
	
			select @progress_msg = 'success @sqlcmd:' + @sqlcmd;
			exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;			

		END

		--UPDATE the bill count
		SELECT	@sqlcmd = '
				UPDATE [export].[home_energy_report_staging_allcolumns]
					SET MyHomeBillCount' + LEFT (@CommodityDesc, 1) +  '= fb3.billedmonths
					FROM
						( SELECT PremiseId, COUNT(*) AS billedmonths FROM  (		
							SELECT 
								ROW_NUMBER () OVER (PARTITION BY fb.PremiseKey, fb.CommodityKey ORDER BY fb.EndDateKey DESC) AS row_id
								, p1.PremiseId				
							FROM [dbo].Benchmarks as  fb  
								INNER JOIN [dbo].[DimDate] d	 		
									ON d.DateKey = fb.EndDateKey
								INNER JOIN [dbo].[DimCommodity] f	 
									ON f.CommodityKey = fb.CommodityKey
								INNER JOIN [dbo].[DimPremise] p1	 	
									ON p1.PremiseKey = fb.PremiseKey
							WHERE p1.clientID = '  + CAST(@clientID AS VARCHAR(10))  + 'AND f.CommodityDesc= ''' + @CommodityDesc + '''
								AND fb.EndDateKey BETWEEN ''' + CAST(@calcstartrangekey AS VARCHAR(8)) + ''' AND ''' + CAST(@endrangekey AS VARCHAR(8))  +''' 
						) fb2
					WHERE fb2.row_id <= 13 
					GROUP BY fb2.Premiseid) fb3
					INNER JOIN [export].[home_energy_report_staging_allcolumns] s
					ON s.PremiseID=fb3.PremiseId';

			
		select @progress_msg = 'start   @sqlcmd:' + @sqlcmd;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
	
		exec (@sqlcmd);
	
		select @progress_msg = 'success @sqlcmd:' + @sqlcmd;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
		
		--
		SELECT	@count += 1

		select @progress_msg = 'success @CommodityDesc:''' + @CommodityDesc + '''; @row_id:' + cast(@row_id as varchar);
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;

		FETCH NEXT FROM peerinfo_cursor INTO @CommodityDesc, @row_id

	END
	
	exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = 'success pocess monthly benchmarks';
	CLOSE peerinfo_cursor
	DEALLOCATE peerinfo_cursor
	
	if @channel = 'email' begin
	
	--
	--	OK...at this point, for UIL email, we only have the current month and the previous month...
	--	the requirement dictate that we are going to use the previous month if the current month does not exist...therefore we are going to make it look like there
	--  is only 1 set of cost / usage columns...and we are going to set the values of all null cost/usage columns in month 1 to the value of month 20
	--	we will then remove month 2 values...
	--
		declare commodity_cursor cursor local forward_only for
			select distinct df.CommodityDesc
				from	
					#PeerBenchmarks pbm  
					inner join [dbo].[DimCommodity] df  on df.CommodityKey = pbm.CommodityKey			
				order by df.CommodityDesc;
		open commodity_cursor
		fetch next from commodity_cursor into @CommodityDesc;
		while @@fetch_status = 0 begin
			set	@sqlcmd = '
				update	[export].[home_energy_report_staging_allcolumns]
				set	
					MyHome'			+ left(@CommodityDesc, 1) + 'Usage1 = coalesce(MyHome'			+ left(@CommodityDesc, 1) + 'Usage1, MyHome'		+ left(@CommodityDesc, 1) + 'Usage2),
					MyHome'			+ left(@CommodityDesc, 1) + 'Cost1 = coalesce(MyHome'			+ left(@CommodityDesc, 1) + 'Cost1, MyHome'			+ left(@CommodityDesc, 1) + 'Cost2),
					AverageHome'	+ left(@CommodityDesc, 1) + 'Usage1 = coalesce(AverageHome'		+ left(@CommodityDesc, 1) + 'Usage1, AverageHome'	+ left(@CommodityDesc, 1) + 'Usage2),
					AverageHome'	+ left(@CommodityDesc, 1) + 'Cost1 = coalesce(AverageHome'		+ left(@CommodityDesc, 1) + 'Cost1, AverageHome'	+ left(@CommodityDesc, 1) + 'Cost2),
					EfficientHome'	+ left(@CommodityDesc, 1) + 'Usage1 = coalesce(EfficientHome'	+ left(@CommodityDesc, 1) + 'Usage1, EfficientHome'	+ left(@CommodityDesc, 1) + 'Usage2),
					EfficientHome'	+ left(@CommodityDesc, 1) + 'Cost1 = coalesce(EfficientHome'	+ left(@CommodityDesc, 1) + 'Cost1, EfficientHome'	+ left(@CommodityDesc, 1) + 'Cost2);';

			select @progress_msg = 'start   @sqlcmd:' + @sqlcmd;
			exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
	
			exec (@sqlcmd)
	
			select @progress_msg = 'success @sqlcmd:' + @sqlcmd;
			exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;

			fetch next from commodity_cursor into @CommodityDesc;
		end
		close commodity_cursor;
		deallocate commodity_cursor;
	end
	--=================================================== BENCHMARK COMPARISON TYPE ==============================================

    UPDATE  h
    SET     h.BenchmarkComparisonType = r.BenchmarkComparisonType
    FROM    [Export].[home_energy_report_staging_allcolumns] h
            INNER JOIN ( SELECT [bm].[ClientId] ,
                                premise.PremiseId ,
                                [bm].[CommodityId] ,
                                [bm].[EndDate] ,
                                [bm].[PeriodType] ,
                                IIF(@benchmarkCTFeild = 0, bm.MyUsage_ComparisonType, bm.MyCost_ComparisonType) AS BenchmarkComparisonType ,
                                ROW_NUMBER() OVER ( PARTITION BY [bm].[ClientId],
                                                    [bm].[PremiseKey],
                                                    [bm].[CommodityId] ORDER BY [bm].[EndDate] DESC ) AS [rank]
                         FROM   [dbo].[BillBenchmarkResults] AS [bm]
                                INNER JOIN [dbo].[DimPremise] AS [premise] ON [premise].PremiseKey = [bm].PremiseKey
                                INNER JOIN [Export].[home_energy_report_staging_allcolumns]
                                AS [staging] ON [staging].[PremiseID] = [premise].[PremiseId]
                         WHERE  ( ( @benchmarkCTPeriod = 0
                                    AND [bm].[PeriodType] = 1
                                  )
                                  OR ( @benchmarkCTPeriod = 1
                                       AND [bm].[PeriodType] = 3
                                     )
                                )
                                AND ( ( @benchmarkCTFeild = 0
                                        AND ISNULL(bm.MyUsage, 0) != 0
                                      )
                                      OR ( @benchmarkCTFeild = 1
                                           AND ISNULL(bm.MyCost, 0) != 0
                                         )
                                    )
                                AND bm.EndDate >= @startrangedate
                                AND [bm].[EndDate] < @endrangedate
                       ) r ON r.PremiseID = h.PremiseID
    WHERE   r.rank = 1;
	--================================================================= QUARTERLY LOOP ====================================
	/*
		load the quarterly benchmarks into the temp table
		we are loading the most recent quarterly measure for each premise/commodity
	*/

	exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = 'start   populate #PeerBenchmarks_quarterly';

	insert into #PeerBenchmarks_quarterly
		(
			 [PremiseKey]			,[CommodityKey]			,[MyUsage]				,[MyCost]
			,[AverageUsage]			,[AverageCost]			,[EfficientUsage]		,[EfficientCost]		,[CommodityDesc]
		)
		select 
			 [bm].[PremiseKey]		,[com].[CommodityKey]	,[bm].[MyUsage]			,[bm].[MyCost]
			,[bm].[AverageUsage]	,[bm].[AverageCost]		,[bm].[EfficientUsage]	,[bm].[EfficientCost]	,[com].[CommodityDesc]
			from
				[dbo].[BillBenchmarkResults] as [bm]
				inner join [dbo].[DimCommodity] as [com] on
					[com].[CommodityId] = [bm].[CommodityId]
				inner join
				(
					-- this is used to retrieve the most recent quarterly bill 
					select 
						[bm].[ClientId], [bm].[PremiseKey], [bm].[CommodityId], [bm].[EndDate], [bm].[PeriodType],
						row_number() over (partition by [bm].[ClientId], [bm].[PremiseKey], [bm].[CommodityId]  order by [bm].[EndDate] desc) as [rank]
						from
							[dbo].[BillBenchMarkResults] as [bm]
							inner join	[dbo].[DimPremise] as [premise] on
								[premise].PremiseKey = [bm].PremiseKey
							inner join  [export].[home_energy_report_staging_allcolumns] as [staging] on
								[staging].[PremiseId] = [premise].[PremiseId]	-- we will only look at benchmarks for premises that will be on the report
						where
							[bm].[PeriodType] = 3	and 		-- quarterly only 
							isnull([bm].[MyCost], 0) != 0 and	-- must have a quarterly cost
								
							-- date filtering
							[bm].[EndDate] < @endrangedate and
							
							-- only do this for email
							@channel = 'print'

				) as [most_recent_bm] on
					[most_recent_bm].[ClientId] = [bm].[ClientId] and
					[most_recent_bm].[PremiseKey] = [bm].[PremiseKey] and
					[most_recent_bm].[EndDate] = [bm].[EndDate] and
					[most_recent_bm].[CommodityId] = [bm].[CommodityId] and
					[most_recent_bm].[PeriodType] = [bm].[PeriodType] and
					[most_recent_bm].[rank] = 1
			where
				[bm].[PeriodType] = 3 and	-- quarterly
				@channel = 'print'			-- we are not using quarterly for the print report...we could let everything fall through; but from a performance perspective we will save time by not doing the quarterly

	select @count = count(*) from #PeerBenchmarks_quarterly;
	select @progress_msg = 'success populate #PeerBenchmarks_quarterly count ' + cast(@count as varchar);
	exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;

	


	/*
		create a cursor
	*/
	declare peerinfo_cursor_quarterly cursor local static forward_only for
		select distinct [CommodityDesc] from #PeerBenchmarks_quarterly;

	open peerinfo_cursor_quarterly;
	fetch next from peerinfo_cursor_quarterly INTO @CommodityDesc;
	
	/*
		loop thru and UPDATE the home_energy_report_staging_allcolumns table
		with benchmark values
		*/

	exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = 'start   pocess quaterly benchmarks';
	
	while @@fetch_status = 0 begin

		select @progress_msg = 'start   pocess quaterly benchmarks @CommodityDesc:''' + @CommodityDesc + '''';
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
		
		SELECT	@sqlcmd = '
			update	he
				set	
					MyHome'			+ LEFT (@CommodityDesc, 1) + 'CostQtr = quarterly_bm.MyCost,
					AverageHome'	+ LEFT (@CommodityDesc, 1) + 'CostQtr = quarterly_bm.AverageCost,				
					EfficientHome'	+ LEFT (@CommodityDesc, 1) + 'CostQtr = quarterly_bm.EfficientCost		
				from 
					#PeerBenchmarks_quarterly as [quarterly_bm]  
					inner join [dbo].[DimPremise] as [p] on
						[p].[PremiseKey] = [quarterly_bm].[PremiseKey]
					inner join [export].[home_energy_report_staging_allcolumns] as [he] on 
						[he].PremiseId = [p].[PremiseId]
					where
						[quarterly_bm].[CommodityDesc] = ''' + @CommodityDesc + '''
		';

		select @progress_msg = 'start   @sqlcmd:' + @sqlcmd;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
	
		exec (@sqlcmd)
	
		select @progress_msg = 'success @sqlcmd:' + @sqlcmd;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
	

		select @progress_msg = 'success pocess quaterly benchmarks @CommodityDesc:''' + @CommodityDesc + '''';;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;


		fetch next from peerinfo_cursor_quarterly INTO @CommodityDesc;

	END
	
	exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = 'success pocess quaterly benchmarks';
	close peerinfo_cursor_quarterly
	deallocate peerinfo_cursor_quarterly


--================================================================= ANNUAL LOOP ====================================
/*
load the annual benchmarks into the annual temp table
*/
	exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = 'start   populate #PeerBenchmarks_annual';
	INSERT #PeerBenchmarks_annual
		(	
			[PremiseKey]
			, CommodityKey
			, [MyUsage]
			, [MyCost]
			, [AverageUsage]
			, [AverageCost]
			, [EfficientUsage]
			, [EfficientCost]
			, CommodityDesc
		)
		SELECT	
			  [PremiseKey]
			, CommodityKey
			, SUM ([MyUsage])
			, SUM ([MyCost])
			, SUM ([AverageUsage])
			, SUM ([AverageCost])
			, SUM ([EfficientUsage])
			, SUM ([EfficientCost])
			, CommodityDesc
		FROM	#PeerBenchmarks    --this has 13 months
		where EndDateKey BETWEEN @calcstartrangekey AND  @annual_cutoff_date_key  --need 12 months
		GROUP BY [PremiseKey], CommodityKey, CommodityDesc;
	exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = 'success populate #PeerBenchmarks_annual';
		

/*
create a cursor for annual
*/

	DECLARE peerinfo_annual_cursor CURSOR FOR
	SELECT	CommodityDesc
	FROM	#PeerBenchmarks_annual pbm
	GROUP BY CommodityDesc
/*
loop thru and UPDATE the home_energy_report_staging_allcolumns table
with annual benchmark values
*/
	OPEN peerinfo_annual_cursor
	FETCH NEXT FROM peerinfo_annual_cursor INTO @CommodityDesc
		
	exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = 'stat   pocess annual benchmarks';

	WHILE @@fetch_status = 0 BEGIN

		select @progress_msg = 'start   processing annual for ' + @CommodityDesc;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;

		SELECT	@sqlcmd = '
			UPDATE	he
			set	MyHomeAnnual'		+ LEFT (@CommodityDesc, 1) + 'Cost = b2.[MyCost],
				AverageAnnual'		+ LEFT (@CommodityDesc, 1) + 'Cost = b2.[AverageCost],
				EfficientAnnual'	+ LEFT (@CommodityDesc, 1) + 'Cost = b2.[EfficientCost]
			FROM #PeerBenchmarks_annual b2  
				INNER JOIN DimPremise p2	  									
				on p2.PremiseKey = b2.PremiseKey
				INNER JOIN [export].[home_energy_report_staging_allcolumns] he	 
				ON he.PremiseID = p2.PremiseId
				where b2.CommodityDesc = ''' + @CommodityDesc + '''';

		

		select @progress_msg = 'start   @sqlcmd:' + @sqlcmd;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
	
		exec (@sqlcmd);	-- annual calc
	
		select @progress_msg = 'success @sqlcmd:' + @sqlcmd;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;

		SELECT	@sqlcmd = '
			UPDATE	he
			set	
				
					CTE' + @CommodityDesc + 'YTDCostPercent  = case when CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.EfficientHomeGCost1)))  AS money) !=0 then					
						  ((CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.MyHomeGCost1))) AS money) -CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.EfficientHomeGCost1))) AS money))/
						  CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.EfficientHomeGCost1)))  AS money)
                                           )  *100 else 0 end ,
					CTA' + @CommodityDesc + 'YTDCostPercent  =  case when CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.AverageHomeGCost1))) AS money) !=0 then
						  ((CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.MyHomeGCost1))) AS money) -CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.AverageHomeGCost1)))  AS money))/
						  CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.AverageHomeGCost1))) AS money)
                                           )   * 100 else 0 end,
				CompareToMyselfLessPct' + @CommodityDesc + '=CASE when cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage13 as decimal(18,2)) <> 0
				then((cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage13 as decimal(18,2)) - cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage1  as decimal(18,2)) 
															)/(cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage13 as decimal(18,2)))
															)*100 else 0 end,
				CompareToMyselfMorePct' + @CommodityDesc + '=CASE when cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage13 as decimal(18,2)) <> 0
				then((cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage1  as decimal(18,2)) - cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage13 as decimal(18,2))
															)/(	cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage13 as decimal(18,2))) 
															)*100 else 0 end
			FROM #PeerBenchmarks_annual b2  
				INNER JOIN DimPremise p2	 									
				on p2.PremiseKey = b2.PremiseKey
				INNER JOIN [export].[home_energy_report_staging_allcolumns] he  	
				on he.PremiseID = p2.PremiseId
				where b2.CommodityDesc = ''' + @CommodityDesc + '''';

				
		select @progress_msg = 'start   @sqlcmd:' + @sqlcmd;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
	
		exec (@sqlcmd);	-- annual calc
	
		select @progress_msg = 'success @sqlcmd:' + @sqlcmd;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;

			

		--UPDATE the bill count
		SELECT	@sqlcmd = '
			UPDATE [export].[home_energy_report_staging_allcolumns]
			SET MyHomeBillCount' + LEFT (@CommodityDesc, 1) +  '= fb3.billedmonths
				FROM
			( SELECT PremiseId, COUNT(*) AS billedmonths FROM  (		
				SELECT 
					   ROW_NUMBER () OVER (PARTITION BY fb.PremiseKey, fb.CommodityKey ORDER BY fb.EndDateKey DESC) AS row_id
					  , p1.PremiseId				
				  FROM [dbo].Benchmarks as  fb 
					INNER JOIN [dbo].[DimDate] d			ON d.DateKey = fb.EndDateKey
					INNER JOIN [dbo].[DimCommodity] f	ON f.CommodityKey = fb.CommodityKey
					INNER JOIN [dbo].[DimPremise] p1		ON p1.PremiseKey = fb.PremiseKey
				WHERE p1.clientID = '  + CAST(@clientID AS VARCHAR(10))  + 'AND f.CommodityDesc= ''' + @CommodityDesc + '''
				AND fb.EndDateKey BETWEEN ''' + CAST(@calcstartrangekey AS VARCHAR(8)) + ''' AND ''' + CAST(@endrangekey AS VARCHAR(8))  +'''
			) fb2
			WHERE fb2.row_id <= 13 
			GROUP BY fb2.Premiseid) fb3
			INNER JOIN [export].[home_energy_report_staging_allcolumns] s
			ON s.PremiseID=fb3.PremiseId';
			
		select @progress_msg = 'start   @sqlcmd:' + @sqlcmd;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;
	
		exec (@sqlcmd);	-- annual calc
	
		select @progress_msg = 'success @sqlcmd:' + @sqlcmd;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;

		--

		select @progress_msg = 'success processing annual for ' + @CommodityDesc;
		exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = @progress_msg;

		--

		FETCH NEXT FROM peerinfo_annual_cursor INTO @CommodityDesc

	END

	CLOSE peerinfo_annual_cursor
	DEALLOCATE peerinfo_annual_cursor

	exec [Export].[usp_HE_Report_Record_Progress] @runId = @run_id, @component = @component, @text = 'success pocess annual benchmarks';
		

	--at this point all values to be used in further calcs have already been pulled from tables
	UPDATE	he
		SET 
				-- <george> this logic was in the original SoCal implementation...I have no idea as to what is is trying to accomplish other that look at data a year ago and set the
				--		    report number accordingly.  For UIL there is no such requirement!
				--ProgramReportNumber =
				--	CASE 
				--		WHEN MyHomegUsage13 IS NULL OR CAST(MyHomegUsage13 AS decimal(18,2)) <=0.00 then 1
				--		ELSE @report_number
				--	end	,	
				MyHomeAnnualTotalCost			= CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0)),
				AverageAnnualTotalCost			= CONVERT (MONEY, ISNULL(AverageAnnualECost,0))		+ CONVERT (MONEY, ISNULL(AverageAnnualGCost,0))		+ CONVERT (MONEY, ISNULL(AverageAnnualWCost,0)),
				EfficientAnnualTotalCost		= CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)),
				AnnualSavDollarPotential10Pct	= CONVERT (MONEY, .10 * (CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0)) + CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0)) + CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0)))),
				AnnualSavDollarPotential15Pct	= CONVERT (MONEY, .15 * (CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0)) + CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0)) + CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0)))), 
				MonthlySavDollarPotential15Pct	= CONVERT (MONEY, .15 * ((
												  CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0)) 
												+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0)) 
												+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0)))
				/(CAST(ISNULL(MyHomeBillCountE,0) AS int) + CAST(ISNULL(MyHomeBillCountG,0) AS INT) + CAST(ISNULL(MyHomeBillCountW,0) AS INT))))
				,
WasteGoalResult= 
case When @period='annual' THEN
				
				CASE	
				When  -- customer uses less than efficient homes 
						(CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0)))<
						(CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)))
						THEN 'A'

				When --or uses less than 8% more than efficient homes.
						 ( CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))			+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0))) <
						 (
							(CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)))
							 +
							(((CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)))
							) * .08) 
						) THEN 'A'
---------------------------
				When  -- customer uses more than 8% of efficient homes 
						 ( CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))			+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0))) >
						 (
							(CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)))
							 +
							(((CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)))
							) * .08) 
						) THEN 'B'

				When --or is within 8% of average homes.
						 ( CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0))) <
						 (
							(CONVERT (MONEY, ISNULL(AverageAnnualECost,0))	+ CONVERT (MONEY, ISNULL(AverageAnnualGCost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualWCost,0)))
							 +
							(((CONVERT (MONEY, ISNULL(AverageAnnualECost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualGCost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualWCost,0)))
							) * .08) 
						) THEN 'B'
-----------------------------
				When --or uses more than 8%  than Average homes.
						 ( CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))			+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0))) >
						 (
							(CONVERT (MONEY, ISNULL(AverageAnnualECost,0))		+ CONVERT (MONEY, ISNULL(AverageAnnualGCost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualWCost,0)))
							 +
							(((CONVERT (MONEY, ISNULL(AverageAnnualECost,0))	+ CONVERT (MONEY, ISNULL(AverageAnnualGCost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualWCost,0)))
							) * .08) 
						) THEN 'C'


						ELSE 'D'
				END

ELSE
				CASE	
				When  -- customer uses less than efficient homes 
						(CONVERT (MONEY, ISNULL(MyHomeECost1,0))		+ CONVERT (MONEY, ISNULL(MyHomeGCost1,0))		+ CONVERT (MONEY, ISNULL(MyHomeWCost1,0)))<
						(CONVERT (MONEY, ISNULL(EfficientHomeECost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeGCost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeWCost1,0)))
						THEN 'A'

				When --or uses less than 8% more than efficient homes.
						 ( CONVERT (MONEY, ISNULL(MyHomeECost1,0))			+ CONVERT (MONEY, ISNULL(MyHomeGCost1,0))		+ CONVERT (MONEY, ISNULL(MyHomeWCost1,0))) <
						 (
							(CONVERT (MONEY, ISNULL(EfficientHomeECost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeGCost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeWCost1,0)))
							 +
							(((CONVERT (MONEY, ISNULL(EfficientHomeECost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeGCost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeWCost1,0)))
							) * .08) 
						) THEN 'A'
---------------------------
				When  -- customer uses more than 8% of efficient homes 
						 ( CONVERT (MONEY, ISNULL(MyHomeECost1,0))			+ CONVERT (MONEY, ISNULL(MyHomeGCost1,0))		+ CONVERT (MONEY, ISNULL(MyHomeWCost1,0))) >
						 (
							(CONVERT (MONEY, ISNULL(EfficientHomeECost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeGCost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeWCost1,0)))
							 +
							(((CONVERT (MONEY, ISNULL(EfficientHomeECost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeGCost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeWCost1,0)))
							) * .08) 
						) THEN 'B'

				When --or is within 8% of average homes.
						 ( CONVERT (MONEY, ISNULL(MyHomeECost1,0))		+ CONVERT (MONEY, ISNULL(MyHomeGCost1,0))	+ CONVERT (MONEY, ISNULL(MyHomeWCost1,0))) <
						 (
							(CONVERT (MONEY, ISNULL(AverageHomeECost1,0))	+ CONVERT (MONEY, ISNULL(AverageHomeGCost1,0)) + CONVERT (MONEY, ISNULL(AverageHomeWCost1,0)))
							 +
							(((CONVERT (MONEY, ISNULL(AverageHomeECost1,0)) + CONVERT (MONEY, ISNULL(AverageHomeGCost1,0)) + CONVERT (MONEY, ISNULL(AverageHomeWCost1,0)))
							) * .08) 
						) THEN 'B'
-----------------------------
				When --or uses more than 8%  than Average homes.
						 ( CONVERT (MONEY, ISNULL(MyHomeECost1,0))		+ CONVERT (MONEY, ISNULL(MyHomeGCost1,0))	+ CONVERT (MONEY, ISNULL(MyHomeWCost1,0))) >
						 (
							(CONVERT (MONEY, ISNULL(AverageHomeECost1,0))		+ CONVERT (MONEY, ISNULL(AverageHomeGCost1,0)) + CONVERT (MONEY, ISNULL(AverageHomeWCost1,0)))
							 +
							(((CONVERT (MONEY, ISNULL(AverageHomeECost1,0))	+ CONVERT (MONEY, ISNULL(AverageHomeGCost1,0)) + CONVERT (MONEY, ISNULL(AverageHomeWCost1,0)))
							) * .08) 
						) THEN 'C'


						ELSE 'D'
				END
END
,

			CTATotalYTDCostPercent = 
			(( 
					(CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))	+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0)))
				- (CONVERT (MONEY, ISNULL(AverageAnnualECost,0))	+ CONVERT (MONEY, ISNULL(AverageAnnualGCost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualWCost,0)))
				) / (CONVERT (MONEY, ISNULL(AverageAnnualECost,0))	+ CONVERT (MONEY, ISNULL(AverageAnnualGCost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualWCost,0)))) *100,
			CTETotalYTDCostPercent = 
			(( 
					(CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0)))
				- (CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))		+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)))
				) / (CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)))) *100,

			CTATotalMonthlyCostPercent = CTAGasMonthlyCostPercent,
			CTETotalMonthlyCostPercent = CTEGasMonthlyCostPercent

		FROM	[export].[home_energy_report_staging_allcolumns] he
		--next line to bypass bad or missing data
		WHERE 
			(CAST(ISNULL(MyHomeBillCountE,0) AS int) + CAST(ISNULL(MyHomeBillCountG,0) AS INT) + CAST(ISNULL(MyHomeBillCountW,0) AS INT)) !=0
			AND (CONVERT (MONEY, ISNULL(AverageAnnualECost,0))	+ CONVERT (MONEY, ISNULL(AverageAnnualGCost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualWCost,0))) !=0
			AND (CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0))) !=0
			
SET NOCOUNT OFF

END


