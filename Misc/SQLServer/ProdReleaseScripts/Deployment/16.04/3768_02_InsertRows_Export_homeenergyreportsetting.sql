;
WITH    Profiles
          AS ( SELECT DISTINCT
                        ProfileName
               FROM     Export.HomeEnergyReportSetting
             ),
        NewRecs
          AS ( SELECT   224 AS ClientId ,
                        'benchmark_comparison_field' AS Name ,
                        '0' AS Value ,
                        'indicates which field to use for benchmarks comparison type (0 for usage, 1 for cost)' AS Description
             )
    INSERT  INTO Export.HomeEnergyReportSetting
            SELECT  p.ProfileName ,
                    nr.*
            FROM    NewRecs nr
                    CROSS JOIN Profiles p;

					;
WITH    Profiles
          AS ( SELECT DISTINCT
                        ProfileName
               FROM     Export.HomeEnergyReportSetting
             ),
        NewRecs
          AS ( SELECT   224 AS ClientId ,
                        'benchmark_comparison_period' AS Name ,
                        '0' AS Value ,
                        'indicates period for benchmarks comparison type (0 for monthly, 1 for quarterly)' AS Description
             )
    INSERT  INTO Export.HomeEnergyReportSetting
            SELECT  p.ProfileName ,
                    nr.*
            FROM    NewRecs nr
                    CROSS JOIN Profiles p;