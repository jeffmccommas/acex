UPDATE  wh.ProfilePremiseAttribute
SET     AttributeValue = SUBSTRING(AttributeValue, 1, LEN(AttributeValue) - 9)
        + '2015'
WHERE   AttributeKey LIKE '%year%'
        AND AttributeValue LIKE '%after2010';