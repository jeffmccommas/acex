UPDATE  dbo.FactPremiseAttribute
SET     ContentOptionKey = SUBSTRING(ContentOptionKey, 1,
                                     LEN(ContentOptionKey) - 9) + '2015' ,
        Value = SUBSTRING(Value, 1, LEN(Value) - 9) + '2015'
WHERE   ContentAttributeKey LIKE '%year%'
        AND Value LIKE '%after2010';