USE [InsightsDW_Test]
GO
/****** Object:  Table [dbo].[BillBenchmarkGroups]    Script Date: 9/13/2016 11:27:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BillBenchmarkGroups](
	[PremiseKey] [int] NOT NULL,
	[ClientId] [smallint] NOT NULL,
	[KMCluster] [smallint] NOT NULL,
	[EquipSegment] [smallint] NOT NULL,
	[LocationId] [smallint] NOT NULL,
	[GroupId] [smallint] NOT NULL,
	[QualityCode] [varchar](50) NOT NULL
)

GO
SET ANSI_PADDING OFF
GO
