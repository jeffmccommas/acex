USE [Insights]
GO

/****** Object:  Table [dbo].[FileType]    Script Date: 9/12/2016 12:45:39 PM ******/
DROP TABLE IF EXISTS [dbo].[FileType]
GO

/****** Object:  Table [dbo].[FileType]    Script Date: 9/12/2016 12:45:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FileType](
	[TypeID] [int] IDENTITY(1,1) NOT NULL,
	[TypeExtension] [varchar](50) NOT NULL,
	[TypeDescription] [varchar](100) NULL,
 CONSTRAINT [PK_FileType] PRIMARY KEY CLUSTERED 
(
	[TypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

SET ANSI_PADDING OFF
GO

INSERT INTO [dbo].[FileType]
           ([TypeExtension]
           ,[TypeDescription])
     VALUES
           ('.pdf', 'Files of Type Pdf')
GO

INSERT INTO [dbo].[FileType]
           ([TypeExtension]
           ,[TypeDescription])
     VALUES
           ('.zip', 'Files of Type Zip')

GO

INSERT INTO [dbo].[FileType]
           ([TypeExtension]
           ,[TypeDescription])
     VALUES
           ('.csv', 'Files of Type Csv')

