USE [InsightsMetaData]
GO


/****** Object:  StoredProcedure [cm].[uspEMSelectDailyWeatherDetailForBills]    Script Date: 6/17/2016 12:02:01 PM ******/
DROP PROCEDURE [cm].[uspEMSelectDailyWeatherDetailForBills]
GO

/****** Object:  UserDefinedTableType [cm].[AccountBillAmountsTable]    Script Date: 6/17/2016 11:59:17 AM ******/
DROP TYPE [cm].[AccountBillAmountsTable]
GO

/****** Object:  UserDefinedTableType [cm].[AccountBillAmountsTable]    Script Date: 6/17/2016 11:59:17 AM ******/
CREATE TYPE [cm].[AccountBillAmountsTable] AS TABLE(
	[AccountId] [VARCHAR](50) NOT NULL,
	[PremiseId] [VARCHAR](50) NOT NULL,
	[ServiceId] [VARCHAR](50) NOT NULL,
	[BillDate] [VARCHAR](50) NOT NULL,
	[BillStartDate] [VARCHAR](50) NOT NULL,
	[BillEndDate] [VARCHAR](50) NOT NULL,
	[CommodityKey] [VARCHAR](50) NOT NULL,
	[UOMKey] [VARCHAR](50) NOT NULL,
	[BillDays] [INT] NOT NULL,
	[TotalServiceUse] [DECIMAL](18, 2) NOT NULL,
	[CostofUsage] [DECIMAL](18, 2) NOT NULL,
	[AdditionalServiceCost] [DECIMAL](18, 2) NOT NULL,
	[ZipCode] [VARCHAR](16) NOT NULL,
	[AvgTemp] [INT] NOT NULL,
	[RateClass] [VARCHAR](100)
	PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC,
	[PremiseId] ASC,
	[ServiceId] ASC,
	[BillDate] ASC,
	[CommodityKey] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO


