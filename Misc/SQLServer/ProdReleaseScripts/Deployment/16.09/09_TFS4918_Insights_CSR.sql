USE [Insights]
GO
/****** Object:  StoredProcedure [dbo].[uspMergeCustomerWebToken]    Script Date: 7/11/2016 9:06:16 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspMergeCustomerWebToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspMergeCustomerWebToken]
GO
/****** Object:  StoredProcedure [dbo].[uspInsertCustomerWebToken]    Script Date: 7/11/2016 9:06:16 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspInsertCustomerWebToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspInsertCustomerWebToken]
GO
/****** Object:  StoredProcedure [dbo].[uspGetCustomerWebToken]    Script Date: 7/11/2016 9:06:16 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspGetCustomerWebToken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspGetCustomerWebToken]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerWebToken_Client]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerWebToken]'))
ALTER TABLE [dbo].[CustomerWebToken] DROP CONSTRAINT [FK_CustomerWebToken_Client]
GO
/****** Object:  Table [dbo].[CustomerWebToken]    Script Date: 7/11/2016 9:06:16 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerWebToken]') AND type in (N'U'))
DROP TABLE [dbo].[CustomerWebToken]
GO
/****** Object:  Table [auth].[Role]    Script Date: 7/11/2016 9:06:16 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[auth].[Role]') AND type in (N'U'))
DROP TABLE [auth].[Role]
GO
/****** Object:  Table [auth].[GroupRole]    Script Date: 7/11/2016 9:06:16 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[auth].[GroupRole]') AND type in (N'U'))
DROP TABLE [auth].[GroupRole]
GO
/****** Object:  Table [auth].[Group]    Script Date: 7/11/2016 9:06:16 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[auth].[Group]') AND type in (N'U'))
DROP TABLE [auth].[Group]
GO
/****** Object:  Table [auth].[ClientTabConditions]    Script Date: 7/11/2016 9:06:16 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[auth].[ClientTabConditions]') AND type in (N'U'))
DROP TABLE [auth].[ClientTabConditions]
GO
/****** Object:  Schema [auth]    Script Date: 7/11/2016 9:06:16 AM ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'auth')
DROP SCHEMA [auth]
GO
/****** Object:  Schema [auth]    Script Date: 7/11/2016 9:06:16 AM ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'auth')
EXEC sys.sp_executesql N'CREATE SCHEMA [auth]'

GO
/****** Object:  Table [auth].[ClientTabConditions]    Script Date: 7/11/2016 9:06:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[auth].[ClientTabConditions]') AND type in (N'U'))
BEGIN
CREATE TABLE [auth].[ClientTabConditions](
	[ClientID] [int] NOT NULL,
	[TabID] [varchar](50) NOT NULL,
	[GroupID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
 CONSTRAINT [PK_ClientTabConditions] PRIMARY KEY CLUSTERED 
(
	[ClientID] ASC,
	[TabID] ASC,
	[GroupID] ASC,
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [auth].[Group]    Script Date: 7/11/2016 9:06:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[auth].[Group]') AND type in (N'U'))
BEGIN
CREATE TABLE [auth].[Group](
	[GroupID] [int] NOT NULL,
	[GroupName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Group_1] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [auth].[GroupRole]    Script Date: 7/11/2016 9:06:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[auth].[GroupRole]') AND type in (N'U'))
BEGIN
CREATE TABLE [auth].[GroupRole](
	[GroupID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
 CONSTRAINT [PK_GroupRole] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC,
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
/****** Object:  Table [auth].[Role]    Script Date: 7/11/2016 9:06:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[auth].[Role]') AND type in (N'U'))
BEGIN
CREATE TABLE [auth].[Role](
	[RoleID] [int] NOT NULL,
	[RoleName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Role_1] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CustomerWebToken]    Script Date: 7/11/2016 9:06:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerWebToken]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CustomerWebToken](
	[ClientID] [int] NOT NULL,
	[CustomerID] [varchar](50) NOT NULL,
	[WebToken] [varchar](50) NOT NULL,
	[AdditionalInfo] [varchar](max) NULL,
	[UtcDateTimeCreated] [datetime] NOT NULL,
	[UtcDateTimeUpdated] [datetime] NOT NULL,
	[UserID] [varchar](50) NULL,
	[GroupID] [int] NULL,
	[RoleID] [int] NULL,
 CONSTRAINT [PK_CustomerWebToken] PRIMARY KEY CLUSTERED 
(
	[ClientID] ASC,
	[CustomerID] ASC,
	[WebToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerWebToken_Client]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerWebToken]'))
ALTER TABLE [dbo].[CustomerWebToken]  WITH CHECK ADD  CONSTRAINT [FK_CustomerWebToken_Client] FOREIGN KEY([ClientID])
REFERENCES [dbo].[Client] ([ClientID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerWebToken_Client]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerWebToken]'))
ALTER TABLE [dbo].[CustomerWebToken] CHECK CONSTRAINT [FK_CustomerWebToken_Client]
GO
/****** Object:  StoredProcedure [dbo].[uspGetCustomerWebToken]    Script Date: 7/11/2016 9:06:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspGetCustomerWebToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[uspGetCustomerWebToken] AS' 
END
GO
-- =============================================
-- Author:		Srini Ambati
-- Create date: 01/09/2015
-- Description:	
-- Usage Example:
/*
BEGIN TRANSACTION

	EXEC [dbo].[uspGetCustomerWebToken] 256, 'D123456'
	EXEC [dbo].[uspGetCustomerWebToken] 256, 'wrongcustomerid'
	SELECT * FROM [dbo].[CustomerWebToken]
ROLLBACK TRANSACTION
*/
-- =============================================

ALTER PROCEDURE [dbo].[uspGetCustomerWebToken]
    (
      @clientID INT ,
      @customerId VARCHAR(50),
	  @userId VARCHAR(50) = NULL ,
	  @groupId INT = NULL,
	  @roleId INT = NULL
    )
AS
    BEGIN
		IF @userId IS NOT NULL
			BEGIN
				SELECT  WebToken ,
						AdditionalInfo ,
						UtcDateTimeUpdated
				FROM    [dbo].[CustomerWebToken] WITH (NOLOCK)
				WHERE   ClientID = @clientID
						AND CustomerID = @customerId
						AND UserID = @userId
						AND GroupID = @groupId
						AND RoleID = @roleId
			END
		ELSE
			BEGIN
				SELECT  WebToken ,
						AdditionalInfo ,
						UtcDateTimeUpdated
				FROM    [dbo].[CustomerWebToken] WITH (NOLOCK)
				WHERE   ClientID = @clientID
						AND CustomerID = @customerId
			END
    END

GO
/****** Object:  StoredProcedure [dbo].[uspInsertCustomerWebToken]    Script Date: 7/11/2016 9:06:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspInsertCustomerWebToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[uspInsertCustomerWebToken] AS' 
END
GO

-- =============================================
-- Author:		Srini Ambati
-- Create date: 01/09/2015
-- Description:	
-- Usage Example:
/*
BEGIN TRANSACTION

	EXEC [dbo].[uspInsertCustomerWebToken] 256, 'D123456', 'werwjwejweejlkjsodwee1', 'Some Additional Info', '2015-01-01'
	SELECT * FROM [dbo].[CustomerWebToken]
ROLLBACK TRANSACTION
*/
-- =============================================

ALTER PROCEDURE [dbo].[uspInsertCustomerWebToken]
    (
      @clientID INT ,
      @customerId VARCHAR(50) ,
      @webToken VARCHAR(50) ,
      @AdditionalInfo VARCHAR(MAX) = NULL ,
      @utcDateTimeCreated DATETIME,
	  @userId VARCHAR(50) = NULL ,
	  @groupId INT = NULL,
	  @roleId INT = NULL
    )
AS
    BEGIN

        IF EXISTS ( SELECT  ClientID
                    FROM    [dbo].[CustomerWebToken]
                    WHERE   ClientID = @clientID
                            AND CustomerID = @customerId
                            AND WebToken = @webToken )
            BEGIN 

                UPDATE  [dbo].[CustomerWebToken]
                SET     [AdditionalInfo] = @AdditionalInfo ,
                        [UtcDateTimeUpdated] = @utcDateTimeCreated
                WHERE   ClientID = @clientID
                        AND CustomerID = @customerId
                        AND WebToken = @webToken
            END 
        ELSE
            BEGIN 
                INSERT  INTO [dbo].[CustomerWebToken]
                        ( [ClientID] ,
                          [CustomerID] ,
                          [WebToken] ,
                          [AdditionalInfo] ,
                          [UtcDateTimeCreated] ,
                          [UtcDateTimeUpdated],
						  [UserID],
						  [GroupID],
						  [RoleID]
                        )
                VALUES  ( @clientID ,
                          @customerId ,
                          @webToken ,
                          @AdditionalInfo ,
                          @utcDateTimeCreated ,
                          @utcDateTimeCreated,
						  @userId,
						  @groupId,
						  @roleId
                        )
            END 
    END

GO
/****** Object:  StoredProcedure [dbo].[uspMergeCustomerWebToken]    Script Date: 7/11/2016 9:06:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspMergeCustomerWebToken]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[uspMergeCustomerWebToken] AS' 
END
GO

-- =============================================
-- Author:		Srini Ambati
-- Create date: 01/09/2015
-- Description:	
-- Usage Example:
/*
BEGIN TRANSACTION

	EXEC [dbo].[uspMergeCustomerWebToken] 256, 'D123456', 'werwjwejweejlkjsodwee2', 'Some Additional Info', '2015-01-01'
	SELECT * FROM [dbo].[CustomerWebToken]
ROLLBACK TRANSACTION
*/
-- =============================================

ALTER PROCEDURE [dbo].[uspMergeCustomerWebToken]
    (
      @clientID INT ,
      @customerId VARCHAR(50) ,
      @webToken VARCHAR(50) ,
      @AdditionalInfo VARCHAR(MAX) = NULL ,
      @utcDateTimeCreated DATETIME,
	  @userId VARCHAR(50) = NULL ,
	  @groupId INT = NULL,
	  @roleId INT = NULL
    )
AS
    BEGIN
		BEGIN TRANSACTION

			DELETE FROM [dbo].[CustomerWebToken] WHERE ClientID = @clientID AND CustomerID = @customerId
			EXEC [dbo].[uspInsertCustomerWebToken] @clientID,@customerId,@webToken,@AdditionalInfo,@utcDateTimeCreated,@userId,@groupId,@roleId 

		COMMIT TRANSACTION
	END 

GO
