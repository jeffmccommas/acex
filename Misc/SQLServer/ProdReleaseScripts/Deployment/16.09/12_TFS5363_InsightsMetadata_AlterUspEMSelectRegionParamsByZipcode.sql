﻿
/****** Object:  StoredProcedure [cm].[uspEMSelectRegionParamsByZipcode]    Script Date: 8/4/2016 1:28:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [cm].[uspEMSelectRegionParamsByZipcode]
    @Zipcode AS VARCHAR(12) = NULL
AS
    BEGIN

                SELECT TOP 1 [ZipCode]
                          ,[TemperatureRegionId]
                          ,[ApplianceRegionId]
                          ,[SolarRegionId]
                          ,[State]
                          ,[CountryCode]
                  FROM [cm].[EMZipcode]
                  WHERE [ZipCode] = @Zipcode

    END
