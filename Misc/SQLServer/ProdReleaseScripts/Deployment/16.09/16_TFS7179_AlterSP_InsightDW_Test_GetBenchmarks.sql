USE [InsightsDW_Test]
GO
/****** Object:  StoredProcedure [dbo].[GetBenchmarks]    Script Date: 9/13/2016 11:27:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.GetBenchmarks') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.GetBenchmarks AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE dbo.GetBenchmarks
    @ClientID AS INT ,
    @CustomerID AS VARCHAR(50) ,
    @AccountID AS VARCHAR(50) = NULL ,
    @PremiseID AS VARCHAR(50) = NULL ,
    @StartDate AS DATE ,
    @EndDate AS DATE ,
    @Count AS INT = NULL ,
    @Groups AS VARCHAR(1000) = 'default' ,
    @Measurements AS VARCHAR(50) = NULL
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @RETURN_VALUE INT;
        
        DECLARE @GroupsTable TABLE ( Value VARCHAR(50) );

        INSERT  INTO @GroupsTable
                EXEC @RETURN_VALUE = [dbo].[SplitIntoTable] @ValueList = @Groups;

        WITH    Premise
                  AS ( SELECT DISTINCT
                                r.ClientId ,
                                c.CustomerId ,
                                p.AccountId ,
                                p.PremiseId ,
                                p.PremiseKey
                       FROM     dbo.DimPremise p
                                INNER JOIN dbo.FactCustomerPremise cp ON cp.PremiseKey = p.PremiseKey
                                INNER JOIN dbo.DimCustomer c ON c.CustomerKey = cp.CustomerKey
                                INNER JOIN dbo.DimClient r ON r.ClientKey = c.ClientKey
                       WHERE    r.ClientId = @ClientID
                                AND c.CustomerId = @CustomerID
                                AND ( @AccountID IS NULL
                                      OR p.AccountId = @AccountID
                                    )
                                AND ( @PremiseID IS NULL
                                      OR p.PremiseId = @PremiseID
                                    )
                     )
            SELECT  p.ClientId ,
                    p.CustomerId ,
                    p.AccountId ,
                    p.PremiseId ,
                    pb.StartDate ,
                    pb.EndDate AS EndDate ,
                    pgt.BenchmarkGroupTypeName AS GroupKey ,
                    pb.CommodityId AS FuelKey ,
                    pb.MyUsage ,
                    pb.MyCost ,
                    pb.AverageUsage ,
                    pb.AverageCost ,
                    pb.EfficientUsage ,
                    pb.EfficientCost ,
                    gr.GroupCount AS GroupSize ,
                    ROW_NUMBER() OVER ( ORDER BY p.PremiseKey ) AS RowIdentifier ,
                    ROW_NUMBER() OVER ( PARTITION BY pb.PremiseKey,
                                        pb.CommodityId,
                                        pb.GroupId ORDER BY pb.EndDate DESC ) AS RowIndex
            INTO    #Result
            FROM    [dbo].[BillBenchmarkResults] pb
                    INNER JOIN dbo.BenchmarkGroupType pgt ON pgt.BenchmarkGroupTypeKey = pb.GroupId
                    INNER JOIN @GroupsTable g ON g.Value = pgt.BenchmarkGroupTypeName
                    INNER JOIN [dbo].[BillBenchmarkGroupResults] gr ON gr.GroupId = pb.GroupId
                                                              AND gr.EndDate = pb.EndDate
                    INNER JOIN Premise p ON pb.PremiseKey = p.PremiseKey
            WHERE   pb.PeriodType = 1
                    AND ( ( pb.EndDate BETWEEN @StartDate AND @EndDate )
                          OR ( pb.StartDate BETWEEN @StartDate AND @EndDate )
                        );
          
		    
        SELECT  r.RowIdentifier ,
                r.ClientId ,
                r.CustomerId ,
                r.AccountId ,
                r.PremiseId ,
                r.StartDate ,
                r.EndDate ,
                r.GroupKey ,
                f.CommodityDesc AS CommodityKey ,
                r.MyUsage ,
                r.MyCost ,
                r.AverageUsage ,
                r.AverageCost ,
                r.EfficientUsage ,
                r.EfficientCost ,
                'usd' AS CostCurrencyKey ,
                CASE WHEN r.FuelKey = 1 THEN 'kwh'
                     WHEN r.FuelKey = 2 THEN 'ccf'
                     ELSE 'gal'
                END AS UOMKey ,
                r.GroupSize AS GroupCount
        FROM    #Result r
                INNER JOIN [dbo].[DimCommodity] f ON f.CommodityKey = r.FuelKey
        WHERE   ( @Count IS NULL
                  OR RowIndex <= @Count
                )
        ORDER BY r.GroupKey ,
                EndDate DESC ,
                r.FuelKey;

        IF @Measurements IS NOT NULL
            BEGIN
                DECLARE @MeasurementsTable TABLE ( Value VARCHAR(50) );

                INSERT  INTO @MeasurementsTable
                        EXEC @RETURN_VALUE = [dbo].[SplitIntoTable] @ValueList = @Measurements;

                WITH    CalcMeasures
                          AS ( SELECT   * ,
                                        CASE WHEN FuelKey = 1
                                             THEN MyUsage / 1.52
                                             WHEN FuelKey = 2
                                             THEN MyUsage / 11.7
                                             WHEN FuelKey = 3 THEN 0
                                        END AS MyCo2 ,
                                        CASE WHEN FuelKey = 1
                                             THEN AverageUsage / 1.52
                                             WHEN FuelKey = 2
                                             THEN AverageUsage / 11.7
                                             WHEN FuelKey = 3 THEN 0
                                        END AS AverageCo2 ,
                                        CASE WHEN FuelKey = 1
                                             THEN EfficientUsage / 1.52
                                             WHEN FuelKey = 2
                                             THEN EfficientUsage / 11.7
                                             WHEN FuelKey = 3 THEN 0
                                        END AS EfficientCo2 ,
                                        CASE WHEN FuelKey = 1
                                             THEN ( MyUsage / 1.52 ) / 85.98
                                             WHEN FuelKey = 2
                                             THEN ( MyUsage / 11.7 ) / 85.98
                                             WHEN FuelKey = 3 THEN 0
                                        END AS MyTrees ,
                                        CASE WHEN FuelKey = 1
                                             THEN ( AverageUsage / 1.52 )
                                                  / 85.98
                                             WHEN FuelKey = 2
                                             THEN ( AverageUsage / 11.7 )
                                                  / 85.98
                                             WHEN FuelKey = 3 THEN 0
                                        END AS AverageTrees ,
                                        CASE WHEN FuelKey = 1
                                             THEN ( EfficientUsage / 1.52 )
                                                  / 85.98
                                             WHEN FuelKey = 2
                                             THEN ( EfficientUsage / 11.7 )
                                                  / 85.98
                                             WHEN FuelKey = 3 THEN 0
                                        END AS EfficientTrees ,
                                        100 AS MyPoints ,
                                        100 AS AveragePoints ,
                                        100 AS EfficientPoints
                               FROM     #Result
                               WHERE    ( @Count IS NULL
                                          OR RowIndex <= @Count
                                        )
                             )
                    SELECT  RowIdentifier ,
                            ClientId ,
                            CustomerId ,
                            AccountId ,
                            PremiseId ,
                            GroupKey ,
                            FuelKey ,
                            EndDate ,
                            m.Value AS [Key] ,
                            CASE WHEN m.Value = 'co2' THEN cm.MyCo2
                                 WHEN m.Value = 'trees' THEN cm.MyTrees
                                 WHEN m.Value = 'points' THEN cm.MyPoints
                            END AS MyQuantity ,
                            CASE WHEN m.Value = 'co2' THEN cm.AverageCo2
                                 WHEN m.Value = 'trees' THEN cm.AverageTrees
                                 WHEN m.Value = 'points' THEN cm.AveragePoints
                            END AS AverageQuantity ,
                            CASE WHEN m.Value = 'co2' THEN cm.EfficientCo2
                                 WHEN m.Value = 'trees' THEN cm.EfficientTrees
                                 WHEN m.Value = 'points'
                                 THEN cm.EfficientPoints
                            END AS EfficientQuantity ,
                            'lbs' AS UOMKey
                    FROM    CalcMeasures cm
                            CROSS JOIN @MeasurementsTable m
                    WHERE   m.Value IN ( 'co2', 'trees', 'points' );
            END;      

        DROP TABLE #Result;
    END;



GO
