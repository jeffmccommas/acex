USE [InsightsDW_Test]
GO
/****** Object:  Table [dbo].[BillBenchmarkGroupResults]    Script Date: 9/13/2016 11:27:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillBenchmarkGroupResults](
	[ClientId] [smallint] NOT NULL,
	[GroupId] [smallint] NOT NULL,
	[CommodityId] [tinyint] NOT NULL,
	[EndDate] [date] NOT NULL,
	[StartDate] [date] NOT NULL,
	[AverageUsage] [decimal](18, 2) NULL,
	[EfficientUsage] [decimal](18, 2) NULL,
	[AverageCost] [decimal](18, 2) NULL,
	[EfficientCost] [decimal](18, 2) NULL,
	[GroupCount] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_BillBenchmarkGroupResults_DateCreated]  DEFAULT (getdate()),
 CONSTRAINT [PK_BillBenchmarkGroupResults] PRIMARY KEY CLUSTERED 
(
	[ClientId] ASC,
	[GroupId] ASC,
	[CommodityId] ASC,
	[EndDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
