USE [Insights]
GO



/****** Object:  Table [dbo].[FileSource]    Script Date: 9/12/2016 12:24:30 PM ******/
DROP TABLE IF EXISTS [dbo].[FileSource]
GO

/****** Object:  Table [dbo].[FileSource]    Script Date: 9/12/2016 12:24:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FileSource](
	[SourceID] [int] IDENTITY(1,1) NOT NULL,
	[SourceName] [varchar](50) NOT NULL,
	[SourceDescription] [varchar](100) NULL,
 CONSTRAINT [PK_FileSource] PRIMARY KEY CLUSTERED 
(
	[SourceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

SET ANSI_PADDING OFF
GO

INSERT INTO [dbo].[FileSource]
           ([SourceName]
           ,[SourceDescription])
     VALUES
           ('ReportCreate', 'Report Create Widget')
GO

INSERT INTO [dbo].[FileSource]
           ([SourceName]
           ,[SourceDescription])
     VALUES
           ('Utility', 'Report Uploaded by Utility')

GO

INSERT INTO [dbo].[FileSource]
           ([SourceName]
           ,[SourceDescription])
     VALUES
           ('ThirdParty', 'Report Uploaded by ThirdParty')

