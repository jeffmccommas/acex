USE [InsightsDW_224]
GO

IF EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Mailing_Name'
      AND Object_ID = Object_ID(N'[export].[home_energy_report_staging_allcolumns]'))
BEGIN
    ALTER TABLE [export].[home_energy_report_staging_allcolumns]
	ALTER COLUMN Mailing_Name VARCHAR (100) 
END

IF EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Premise_Name'
      AND Object_ID = Object_ID(N'[export].[home_energy_report_staging_allcolumns]'))
BEGIN
    ALTER TABLE [export].[home_energy_report_staging_allcolumns]
	ALTER COLUMN Premise_Name VARCHAR (100) 
END