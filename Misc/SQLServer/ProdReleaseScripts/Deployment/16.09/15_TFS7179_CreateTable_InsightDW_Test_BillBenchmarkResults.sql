USE [InsightsDW_Test]
GO
/****** Object:  Table [dbo].[BillBenchmarkResults]    Script Date: 9/13/2016 11:27:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillBenchmarkResults](
	[PremiseKey] [int] NOT NULL,
	[ClientId] [smallint] NOT NULL,
	[CommodityId] [tinyint] NOT NULL,
	[EndDate] [date] NOT NULL,
	[PeriodType] [tinyint] NOT NULL,
	[GroupId] [smallint] NOT NULL,
	[StartDate] [date] NULL,
	[MyUsage] [decimal](18, 2) NULL,
	[AverageUsage] [decimal](18, 2) NULL,
	[EfficientUsage] [decimal](18, 2) NULL,
	[MyUsage_ComparisonValue] [decimal](18, 2) NULL,
	[MyUsage_ComparisonType] [tinyint] NULL,
	[UOMId] [tinyint] NULL,
	[MyCost] [decimal](18, 2) NULL,
	[AverageCost] [decimal](18, 2) NULL,
	[EfficientCost] [decimal](18, 2) NULL,
	[MyCost_ComparisonValue] [decimal](18, 2) NULL,
	[MyCost_ComparisonType] [tinyint] NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_BillBenchmarkResults_DateCreated]  DEFAULT (getdate()),
 CONSTRAINT [PK_BillBenchmarkResults] PRIMARY KEY CLUSTERED 
(
	[ClientId] ASC,
	[PremiseKey] ASC,
	[CommodityId] ASC,
	[EndDate] ASC,
	[PeriodType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
