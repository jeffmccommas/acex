USE [Insights]
GO

/****** Object:  Table [dbo].[FileDetail]    Script Date: 9/6/2016 2:53:36 PM ******/
DROP TABLE IF EXISTS [dbo].[FileDetail]
GO

/****** Object:  Table [dbo].[FileDetail]    Script Date: 9/6/2016 2:53:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FileDetail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int] NOT NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[AccountId] [varchar](50) NOT NULL,
	[PremiseId] [varchar](50) NOT NULL,
	[UserId] [int] NULL,
	[RoleId] [tinyint] NULL,
	[TypeId] [int] NOT NULL,
	[FileSourceId] [int] NOT NULL,
	[Name] [varchar](120) NOT NULL,
	[Title] [varchar](100) NULL,
	[Description] [varchar](500) NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_FileDetail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

SET ANSI_PADDING OFF
GO


