﻿
/****** Object:  StoredProcedure [cm].[uspEMSelectRegionParamsByClientZipcode]    Script Date: 8/4/2016 1:39:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [cm].[uspEMSelectRegionParamsByClientZipcode]
        @ClientID As INT,
    @Zipcode AS VARCHAR(12) = NULL
AS
    BEGIN

                SELECT TOP 1 [ZipCode]
                          ,[TemperatureRegionId]
                          ,[ApplianceRegionId]
                          ,[SolarRegionId]
                          ,[State]
                          ,[StationIdDaily]
                          ,[ClimateId]
                          ,[CountryCode]
                  FROM [cm].[EMZipcode] WITH (NOLOCK)
                  WHERE ([ClientID] = 0 OR [ClientID] = @ClientID) AND [ZipCode] = @Zipcode
                  ORDER BY [ClientID] DESC

    END