﻿

/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[DFT_Billing]    Script Date: 5/18/2017 8:15:09 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[DFT_Billing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [ETL].[DFT_Billing]
GO
/****** Object:  StoredProcedure [ETL].[DFT_Billing]    Script Date: 5/18/2017 8:15:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[DFT_Billing]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[DFT_Billing] AS'
END
GO

-- =============================================
-- Author:      <Author,,Name>
-- Create date: <Create Date,,>
-- Description: <Description,,>
--
--- 10/27/2016 - Phil Victor -- Added IsFault Check and RateClassKey check
--- 01/11/2017 - Phil Victor -- Added CostDetailsKey
-- =============================================
ALTER PROCEDURE [ETL].[DFT_Billing]
    @ClientId AS INT
   ,@EtlLogId AS INT
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE [#tblSrcBilling]
            (
             [ClientId] INT
            ,[AccountId] VARCHAR(50)
            ,[PremiseId] VARCHAR(50)
            ,[ServiceContractId] VARCHAR(50)
            ,[ServicePointId] VARCHAR(50)
            ,[MeterId] VARCHAR(50)
            ,[StartDate] DATETIME
            ,[EndDate] DATETIME
            ,[BillDays] INT
            ,[TotalUnits] DECIMAL(18, 2)
            ,[TotalCost] DECIMAL(18, 2)
            ,[CommodityId] INT
            ,[BillPeriodTypeId] INT
            ,[BillCycleScheduleId] VARCHAR(50)
            ,[UOMId] INT
            ,[RateClass] VARCHAR(50)
            ,[RateClassKey] INT
            ,[DueDate] DATETIME
            ,[ReadDate] DATETIME
            ,[ReadQuality] VARCHAR(50)
            ,[SourceId] INT
            ,[TrackingId] VARCHAR(50)
            ,[TrackingDate] DATETIME
            ,[BillingCostDetailsKey] UNIQUEIDENTIFIER
            ,[ServiceCostDetailsKey] UNIQUEIDENTIFIER
            ,[UtilityBillRecordId] [VARCHAR](64)
            ,[CanceledUtilityBillRecordId] [VARCHAR](64)
            );

        CREATE TABLE [#tblSrcFactBilling]
            (
             [ServiceContractKey] INT
            ,[BillPeriodStartDateKey] INT
            ,[BillPeriodEndDateKey] INT
            ,[ClientId] INT
            ,[PremiseId] VARCHAR(50)
            ,[AccountId] VARCHAR(50)
            ,[ServiceContractId] VARCHAR(50)
            ,[StartDate] DATETIME
            ,[EndDate] DATETIME
            ,[CommodityId] INT
            ,[TotalUsage] DECIMAL(18, 2)
            ,[CostOfUsage] DECIMAL(18, 2)
            ,[SourceKey] INT
            ,[BillDays] INT
            ,[RateClassKey] INT
            ,[BillingCostDetailsKey] UNIQUEIDENTIFIER
            ,[ServiceCostDetailsKey] UNIQUEIDENTIFIER
            ,[UtilityBillRecordId] [VARCHAR](64)
            ,[CanceledUtilityBillRecordId] [VARCHAR](64)
            );

        /* Insert data into Temp tables */
        INSERT  INTO [#tblSrcBilling]
                EXEC [ETL].[S_Billing] @ClientId = @ClientId;  /** Holding Table contents **/

        INSERT  INTO [#tblSrcFactBilling]
                EXEC [ETL].[S_FactBilling] @ClientId = @ClientId;    /** Current database contents **/

         /* Handled canceled Bills */
        IF EXISTS ( SELECT  1
                    FROM    [ETL].[KEY_FactBilling]
                    WHERE  [CanceledUtilityBillRecordId] IS NOT NULL AND Len([CanceledUtilityBillRecordId]) > 0 )
        BEGIN

             SELECT  [fsb].*  INTO [#RemovedFrom]
                    FROM    [dbo].[FactServicePointBilling] [fsb]
                            INNER JOIN [ETL].[KEY_FactBilling] [kfb] ON [fsb].[UtilityBillRecordId] = [kfb].[CanceledUtilityBillRecordId];

             INSERT  INTO [audit].[MissingCanceled_FactServicePointBilling]   ([ClientId],
                    [PremiseId],[AccountId],[CustomerId],[ServiceContractId],
                    [ServicePointId],[MeterId],[MeterType],[ReplacedMeterId],[RateClass],
                    [CommodityId],[AMIStartDate],[AMIEndDate],[StartDate],[EndDate],
                    [SourceId],[TrackingId],[TrackingDate],[BillingCostDetailsKey],
                    [ServiceCostDetailsKey],[UtilityBillRecordId],[CanceledUtilityBillRecordId] )
                SELECT [kfb].[ClientId], [kfb].[PremiseId],[kfb].[AccountId],[kfb].[CustomerId],[kfb].[ServiceContractId],
                [kfb].[ServicePointId],[kfb].[MeterId],[kfb].[MeterType],[kfb].[ReplacedMeterId],[kfb].[RateClass],
                [kfb].[CommodityId],[kfb].[AMIStartDate],[kfb].[AMIEndDate],[kfb].[StartDate],[kfb].[EndDate],
                [kfb].[SourceId],[kfb].[TrackingId],[kfb].[TrackingDate],[kfb].[BillingCostDetailsKey],
                [kfb].[ServiceCostDetailsKey],[kfb].[UtilityBillRecordId],[kfb].[CanceledUtilityBillRecordId]
                  FROM [ETL].[KEY_FactBilling] [kfb] LEFT JOIN [dbo].[FactServicePointBilling] [fsb] ON [fsb].[UtilityBillRecordId] = [kfb].[CanceledUtilityBillRecordId]
                  WHERE [kfb].[CanceledUtilityBillRecordId] IS NOT NULL AND [kfb].[UtilityBillRecordId] IS NOT NULL AND [fsb].[UtilityBillRecordId] IS NULL ;

            INSERT  INTO [audit].[RemovedFrom_FactServicePointBilling]   ([ServiceContractKey]
                  ,[BillPeriodStartDateKey],[BillPeriodEndDateKey],[UOMKey],[BillPeriodTypeKey]
                  ,[ClientId],[PremiseId],[AccountId],[CommodityId],[BillPeriodTypeId],[UOMId]
                  ,[StartDate],[EndDate],[TotalUsage],[CostOfUsage],[OtherCost],[RateClassKey1]
                  ,[RateClassKey2],[NextReadDate],[ReadDate],[CreateDate],[UpdateDate]
                  ,[BillCycleScheduleId],[BillDays],[ETL_LogId],[TrackingId],[TrackingDate]
                  ,[SourceKey],[SourceId],[ReadQuality],[DueDate],[IsFault]
                  ,[BillingCostDetailsKey],[ServiceCostDetailsKey]
                  ,[UtilityBillRecordId],[CanceledUtilityBillRecordId] )
                    SELECT  [rfsb].[ServiceContractKey],[rfsb].[BillPeriodStartDateKey],[rfsb].[BillPeriodEndDateKey]
                                  ,[rfsb].[UOMKey],[rfsb].[BillPeriodTypeKey],[rfsb].[ClientId],[rfsb].[PremiseId]
                                  ,[rfsb].[AccountId],[rfsb].[CommodityId],[rfsb].[BillPeriodTypeId],[rfsb].[UOMId]
                                  ,[rfsb].[StartDate],[rfsb].[EndDate],[rfsb].[TotalUsage],[rfsb].[CostOfUsage]
                                  ,[rfsb].[OtherCost],[rfsb].[RateClassKey1],[rfsb].[RateClassKey2]
                                  ,[rfsb].[NextReadDate],[rfsb].[ReadDate],[rfsb].[CreateDate]
                                  ,[rfsb].[UpdateDate],[rfsb].[BillCycleScheduleId],[rfsb].[BillDays]
                                  ,[rfsb].[ETL_LogId],[rfsb].[TrackingId],[rfsb].[TrackingDate]
                                  ,[rfsb].[SourceKey],[rfsb].[SourceId],[rfsb].[ReadQuality]
                                  ,[rfsb].[DueDate],[rfsb].[IsFault],[rfsb].[BillingCostDetailsKey],[rfsb].[ServiceCostDetailsKey]
                                  ,[rfsb].[UtilityBillRecordId],[rfsb].[CanceledUtilityBillRecordId]
                    FROM [#RemovedFrom] [rfsb];

            DELETE  [fsb]
            FROM    [dbo].[FactServicePointBilling] [fsb]
                INNER JOIN [ETL].[KEY_FactBilling] [kfb] ON [fsb].[UtilityBillRecordId] = [kfb].[CanceledUtilityBillRecordId];

            TRUNCATE TABLE [#tblSrcFactBilling];
             INSERT  INTO [#tblSrcFactBilling]
                EXEC [ETL].[S_FactBilling] @ClientId = @ClientId;    /** Current database contents **/

            DELETE  [fspbd]
            FROM    [dbo].[FactServicePointBilling_BillCostDetails] [fspbd]
                    INNER JOIN [#RemovedFrom] [rfspb] ON [rfspb].[BillingCostDetailsKey] = [fspbd].[BillingCostDetailsKey];

            DELETE  [fspbs]
            FROM    [dbo].[FactServicePointBilling_ServiceCostDetails] [fspbs]
                    INNER JOIN [#RemovedFrom] [rfspb] ON [rfspb].[ServiceCostDetailsKey] = [fspbs].[ServiceCostDetailsKey];

            DROP TABLE [#RemovedFrom];
        END

        /*** Handle new bills with a UtilityBillRecordId ***/
        IF EXISTS ( SELECT  1
                    FROM    [ETL].[KEY_FactBilling]
                    WHERE   [UtilityBillRecordId] IS NOT NULL AND Len([UtilityBillRecordId]) > 0)
        BEGIN

                SELECT
                        @ClientId AS ClientId
                       ,[b].[PremiseId]
                       ,[b].[AccountId]
                       ,[b].[ServiceContractId]
                       ,[b].[StartDate]
                       ,[b].[EndDate]
                       ,[b].[CommodityId]
                       ,[b].[BillPeriodTypeId]
                       ,[b].[BillCycleScheduleId]
                       ,[b].[UOMId]
                       ,[b].[SourceId]
                       ,[b].[TotalUnits]
                       ,[b].[TotalCost]
                       ,[b].[BillDays]
                       ,[b].[RateClass]
                       ,[b].[ReadQuality]
                       ,[b].[ReadDate]
                       ,[b].[DueDate]
                       ,[b].[TrackingId]
                       ,[b].[TrackingDate]
                       ,[b].[BillingCostDetailsKey]
                       ,[b].[ServiceCostDetailsKey]
                       ,[b].[UtilityBillRecordId]
                       ,[b].[CanceledUtilityBillRecordId]
                       ,[b].[RateClassKey]
                INTO [#AddedOrUpdated]
                FROM  [#tblSrcBilling] [b] INNER JOIN [ETL].[KEY_FactBilling] kfb
                        ON [kfb].[UtilityBillRecordId] = [b].[UtilityBillRecordId]
                WHERE ([kfb].[UtilityBillRecordId] IS NOT NULL AND Len([kfb].[UtilityBillRecordId]) > 0)
                    AND ([b].[UtilityBillRecordId] IS NOT NULL AND Len([b].[UtilityBillRecordId]) > 0)

             INSERT  INTO [ETL].[INS_FactBilling]
                        SELECT  @ClientId
                               ,[b].[PremiseId]
                               ,[b].[AccountId]
                               ,[b].[ServiceContractId]
                               ,[b].[StartDate]
                               ,[b].[EndDate]
                               ,[b].[CommodityId]
                               ,[b].[BillPeriodTypeId]
                               ,[b].[BillCycleScheduleId]
                               ,[b].[UOMId]
                               ,[b].[SourceId]
                               ,[b].[TotalUnits]
                               ,[b].[TotalCost]
                               ,[b].[BillDays]
                               ,[b].[RateClass]
                               ,[b].[ReadQuality]
                               ,[b].[ReadDate]
                               ,[b].[DueDate]
                               ,@EtlLogId
                               ,[b].[TrackingId]
                               ,[b].[TrackingDate]
                               ,[b].[BillingCostDetailsKey]
                               ,[b].[ServiceCostDetailsKey]
                               ,[b].[UtilityBillRecordId]
                               ,[b].[CanceledUtilityBillRecordId]
                        FROM  [#AddedOrUpdated] [b]
                                LEFT JOIN [#tblSrcFactBilling] [fb] ON [fb].[ClientId] = [b].[ClientId]
                                                                   AND [fb].[PremiseId] = [b].[PremiseId]
                                                                   AND [fb].[AccountId] = [b].[AccountId]
                                                                   AND [fb].[ServiceContractId] = [b].[ServiceContractId]
                                                                   AND [fb].[EndDate] = [b].[EndDate]
                                                                   AND [fb].[CommodityId] = [b].[CommodityId]
                                                                   AND [fb].[UtilityBillRecordId] = [b].[UtilityBillRecordId]
                        WHERE [fb].[UtilityBillRecordId] IS NULL  AND [b].[UtilityBillRecordId] IS NOT NULL

                         /* Handle Updates */
                        INSERT  INTO [ETL].[T1U_FactBilling]
                                SELECT  [fb].[ServiceContractKey]
                                       ,[fb].[BillPeriodStartDateKey]
                                       ,[fb].[BillPeriodEndDateKey]
                                       ,[b].[CommodityId]
                                       ,[b].[SourceId]
                                       ,@ClientId
                                       ,[b].[TotalUnits]
                                       ,[b].[TotalCost]
                                       ,[b].[StartDate]
                                       ,[b].[BillDays]
                                       ,[b].[RateClass]
                                       ,[b].[ReadQuality]
                                       ,[b].[ReadDate]
                                       ,[b].[DueDate]
                                       ,@EtlLogId
                                       ,[b].[TrackingId]
                                       ,[b].[TrackingDate]
                                       ,IIF([b].[RateClassKey] != [fb].[RateClassKey], 1, 0) AS [IsFault]
                                       ,[fb].[BillingCostDetailsKey]
                                       ,[fb].[ServiceCostDetailsKey]
                                       ,[b].[UtilityBillRecordId]
                                       ,[b].[CanceledUtilityBillRecordId]
                                FROM    [#AddedOrUpdated] [b]
                                        INNER JOIN [#tblSrcFactBilling] [fb] ON [fb].[ClientId] = [b].[ClientId]
                                                                            AND [fb].[PremiseId] = [b].[PremiseId]
                                                                            AND [fb].[AccountId] = [b].[AccountId]
                                                                            AND [fb].[ServiceContractId] = [b].[ServiceContractId]
                                                                            AND [fb].[UtilityBillRecordId] = [b].[UtilityBillRecordId]
                                                                            AND [fb].[EndDate] = [b].[EndDate]
                                                                            AND [fb].[CommodityId] = [b].[CommodityId]
                                        LEFT JOIN [dbo].[DimRateClass] [drc] WITH (NOLOCK) ON [drc].[ClientId] = [b].[ClientId]
                                                                                        AND [drc].[RateClassName] = [b].[RateClass]
                                WHERE [fb].[UtilityBillRecordId] IS NOT NULL  AND [b].[UtilityBillRecordId] IS NOT NULL AND
                                         (
                                         ISNULL([b].[TotalUnits], 0) != ISNULL([fb].[TotalUsage], 0)
                                         OR ISNULL([b].[TotalCost], 0) != ISNULL([fb].[CostOfUsage], 0)
                                         OR ISNULL([b].[BillDays], 0) != ISNULL([fb].[BillDays], 0)
                                         OR ISNULL([b].[RateClassKey], 0) != ISNULL([fb].[RateClassKey], 0)
                                        );

             DROP TABLE [#AddedOrUpdated];

        END

        /**** Old Path ****/
        IF EXISTS ( SELECT  1
                    FROM  [#tblSrcBilling]
                    WHERE ([UtilityBillRecordId] IS NULL OR Len([UtilityBillRecordId]) = 0)
                    AND ( [CanceledUtilityBillRecordId] IS NULL OR Len([CanceledUtilityBillRecordId]) = 0))
        BEGIN

                /** Handle inserts **/
                INSERT  INTO [ETL].[INS_FactBilling]
                        SELECT  @ClientId
                               ,[b].[PremiseId]
                               ,[b].[AccountId]
                               ,[b].[ServiceContractId]
                               ,[b].[StartDate]
                               ,[b].[EndDate]
                               ,[b].[CommodityId]
                               ,[b].[BillPeriodTypeId]
                               ,[b].[BillCycleScheduleId]
                               ,[b].[UOMId]
                               ,[b].[SourceId]
                               ,[b].[TotalUnits]
                               ,[b].[TotalCost]
                               ,[b].[BillDays]
                               ,[b].[RateClass]
                               ,[b].[ReadQuality]
                               ,[b].[ReadDate]
                               ,[b].[DueDate]
                               ,@EtlLogId
                               ,[b].[TrackingId]
                               ,[b].[TrackingDate]
                               ,[b].[BillingCostDetailsKey]
                               ,[b].[ServiceCostDetailsKey]
                               ,[b].[UtilityBillRecordId]
                               ,[b].[CanceledUtilityBillRecordId]
                        FROM  [#tblSrcBilling] [b]
                                LEFT JOIN [#tblSrcFactBilling] [fb] ON [fb].[ClientId] = [b].[ClientId]
                                                                   AND [fb].[PremiseId] = [b].[PremiseId]
                                                                   AND [fb].[AccountId] = [b].[AccountId]
                                                                   AND [fb].[ServiceContractId] = [b].[ServiceContractId]
                                                                   AND [fb].[EndDate] = [b].[EndDate]
                                                                   AND [fb].[CommodityId] = [b].[CommodityId]
                        WHERE ([fb].[ServiceContractKey] IS NULL OR LEN([fb].[ServiceContractKey]) = 0)
                                        AND ([fb].[UtilityBillRecordId] IS NULL AND [b].[UtilityBillRecordId] IS NULL)


                /* Handle Updates */
                INSERT  INTO [ETL].[T1U_FactBilling]
                        SELECT  [fb].[ServiceContractKey]
                               ,[fb].[BillPeriodStartDateKey]
                               ,[fb].[BillPeriodEndDateKey]
                               ,[b].[CommodityId]
                               ,[b].[SourceId]
                               ,@ClientId
                               ,[b].[TotalUnits]
                               ,[b].[TotalCost]
                               ,[b].[StartDate]
                               ,[b].[BillDays]
                               ,[b].[RateClass]
                               ,[b].[ReadQuality]
                               ,[b].[ReadDate]
                               ,[b].[DueDate]
                               ,@EtlLogId
                               ,[b].[TrackingId]
                               ,[b].[TrackingDate]
                               ,IIF([b].[RateClassKey] != [fb].[RateClassKey], 1, 0) AS [IsFault]
                               ,[fb].[BillingCostDetailsKey]
                               ,[fb].[ServiceCostDetailsKey]
                               ,[b].[UtilityBillRecordId]
                               ,[b].[CanceledUtilityBillRecordId]
                        FROM    [#tblSrcBilling] [b]
                                INNER JOIN [#tblSrcFactBilling] [fb] ON [fb].[ClientId] = [b].[ClientId]
                                                                    AND [fb].[PremiseId] = [b].[PremiseId]
                                                                    AND [fb].[AccountId] = [b].[AccountId]
                                                                    AND [fb].[ServiceContractId] = [b].[ServiceContractId]
                                                                    AND [fb].[EndDate] = [b].[EndDate]
                                                                    AND [fb].[CommodityId] = [b].[CommodityId]
                                LEFT JOIN [dbo].[DimRateClass] [drc] WITH (NOLOCK) ON [drc].[ClientId] = [b].[ClientId]
                                                                                AND [drc].[RateClassName] = [b].[RateClass]
                        WHERE  [fb].[UtilityBillRecordId] IS NULL  AND [b].[UtilityBillRecordId] IS NULL AND
                            (
                                 ISNULL([b].[TotalUnits], 0) != ISNULL([fb].[TotalUsage], 0)
                                 OR ISNULL([b].[TotalCost], 0) != ISNULL([fb].[CostOfUsage], 0)
                                 OR ISNULL([b].[BillDays], 0) != ISNULL([fb].[BillDays], 0)
                                 OR ISNULL([b].[RateClassKey], 0) != ISNULL([fb].[RateClassKey], 0)
                                );
            END

/******************************************************************************************************************************/
 ----   /* Handle New BillCostDetails Insert */
        INSERT  INTO [ETL].[INS_FactBilling_BillCostDetails]
                SELECT  [fb].[BillingCostDetailsKey]
                       ,[bcd].[BillingCostDetailName]
                       ,[bcd].[BillingCostDetailValue]
                FROM    [ETL].[INS_FactBilling] [fb] WITH (NOLOCK)
                        INNER JOIN [Holding].[Billing_BillCostDetails] [bcd] WITH (NOLOCK) ON [fb].[BillingCostDetailsKey] = [bcd].[BillingCostDetailsKey];

 ----   /* Handle New  ServiceCostDetails Insert */
        INSERT  INTO [ETL].[INS_FactBilling_ServiceCostDetails]
                SELECT  [fb].[ServiceCostDetailsKey]
                       ,[scd].[ServiceCostDetailName]
                       ,[scd].[ServiceCostDetailValue]
                FROM    [ETL].[INS_FactBilling] [fb] WITH (NOLOCK)
                        INNER JOIN [Holding].[Billing_ServiceCostDetails] [scd] WITH (NOLOCK) ON [fb].[ServiceCostDetailsKey] = [scd].[ServiceCostDetailsKey];

 ----    /* Update the CostDetailKeys to ones already present in the database */
        UPDATE  [bcd]
        SET     [bcd].[BillingCostDetailsKey] = [fspb].[BillingCostDetailsKey]
        FROM    [Holding].[Billing_BillCostDetails] [bcd]
                INNER JOIN [ETL].[KEY_FactBilling] [kfb] ON [kfb].[BillingCostDetailsKey] = [bcd].[BillingCostDetailsKey]
                INNER JOIN [dbo].[DimPremise] [p] WITH (NOLOCK) ON [p].[ClientId] = [kfb].[ClientId]
                                                                 AND [p].[AccountId] = [kfb].[AccountId]
                                                                 AND [p].[PremiseId] = [kfb].[PremiseId]
                INNER JOIN [dbo].[DimServiceContract] [dsc] WITH (NOLOCK) ON [dsc].[ClientId] = [kfb].[ClientId]
                                                                           AND [dsc].[PremiseKey] = [p].[PremiseKey]
                                                                           AND [kfb].[CommodityId] = [dsc].[CommodityKey]
                                                                           AND [kfb].[ServiceContractId] = [dsc].[ServiceContractId]
                INNER JOIN [dbo].[FactServicePointBilling] [fspb] WITH (NOLOCK) ON [fspb].[ClientId] = [kfb].[ClientId]
                                                                                 AND [fspb].[ServiceContractKey] = [dsc].[ServiceContractKey]
                                                                                 AND [kfb].[StartDate] = [fspb].[StartDate]
                                                                                 AND [kfb].[EndDate] = [fspb].[EndDate]
        WHERE   [fspb].[BillingCostDetailsKey] IS NOT NULL;

        UPDATE  [scd]
        SET     [scd].[ServiceCostDetailsKey] = [fspb].[ServiceCostDetailsKey]
        FROM    [Holding].[Billing_ServiceCostDetails] [scd]
                INNER JOIN [ETL].[KEY_FactBilling] [kfb] ON [kfb].[ServiceCostDetailsKey] = [scd].[ServiceCostDetailsKey]
                INNER JOIN [dbo].[DimPremise] [p] WITH (NOLOCK) ON [p].[ClientId] = [kfb].[ClientId]
                                                                 AND [p].[AccountId] = [kfb].[AccountId]
                                                                 AND [p].[PremiseId] = [kfb].[PremiseId]
                INNER JOIN [dbo].[DimServiceContract] [dsc] WITH (NOLOCK) ON [dsc].[ClientId] = [kfb].[ClientId]
                                                                           AND [dsc].[PremiseKey] = [p].[PremiseKey]
                                                                           AND [kfb].[CommodityId] = [dsc].[CommodityKey]
                                                                           AND [kfb].[ServiceContractId] = [dsc].[ServiceContractId]
                INNER JOIN [dbo].[FactServicePointBilling] [fspb] WITH (NOLOCK) ON [fspb].[ClientId] = [kfb].[ClientId]
                                                                                 AND [fspb].[ServiceContractKey] = [dsc].[ServiceContractKey]
                                                                                 AND [kfb].[StartDate] = [fspb].[StartDate]
                                                                                 AND [kfb].[EndDate] = [fspb].[EndDate]
        WHERE   [fspb].[ServiceCostDetailsKey] IS NOT NULL;

        ----    /* If a new BillCostDetail has been added */
        INSERT  INTO [ETL].[INS_FactBilling_BillCostDetails]
                SELECT  [bcd].[BillingCostDetailsKey]
                       ,[bcd].[BillingCostDetailName]
                       ,[bcd].[BillingCostDetailValue]
                FROM    [Holding].[Billing_BillCostDetails] [bcd] WITH (NOLOCK)
                WHERE   [bcd].[BillingCostDetailName] NOT IN (
                        SELECT  [fbcd].[BillingCostDetailName]
                        FROM    [Holding].[Billing_BillCostDetails] [fb] WITH (NOLOCK)
                                INNER JOIN [dbo].[FactServicePointBilling_BillCostDetails] [fbcd] WITH (NOLOCK) ON [fb].[BillingCostDetailsKey] = [fbcd].[BillingCostDetailsKey])
                        AND NOT EXISTS ( SELECT 1
                                         FROM   [ETL].[INS_FactBilling_BillCostDetails] [ifb]
                                                INNER JOIN [Holding].[Billing_BillCostDetails] [hbcd] ON [ifb].[BillingCostDetailsKey] = [hbcd].[BillingCostDetailsKey]
                                                                                                       AND [ifb].[BillingCostDetailName] = [hbcd].[BillingCostDetailName] );

            /* If a new ServiceCostDetail has been added */
        INSERT  INTO [ETL].[INS_FactBilling_ServiceCostDetails]
                SELECT  [scd].[ServiceCostDetailsKey]
                       ,[scd].[ServiceCostDetailName]
                       ,[scd].[ServiceCostDetailValue]
                FROM    [Holding].[Billing_ServiceCostDetails] [scd] WITH (NOLOCK)
                WHERE   [scd].[ServiceCostDetailName] NOT IN (
                        SELECT  [fscd].[ServiceCostDetailName]
                        FROM    [Holding].[Billing_ServiceCostDetails] [fb] WITH (NOLOCK)
                                INNER JOIN [dbo].[FactServicePointBilling_ServiceCostDetails] [fscd] WITH (NOLOCK) ON [fb].[ServiceCostDetailsKey] = [fscd].[ServiceCostDetailsKey])
                        AND NOT EXISTS ( SELECT 1
                                         FROM   [ETL].[INS_FactBilling_ServiceCostDetails] [ifsb]
                                                INNER JOIN [Holding].[Billing_ServiceCostDetails] [sbcd] ON [ifsb].[ServiceCostDetailsKey] = [sbcd].[ServiceCostDetailsKey]
                                                                                                        AND [ifsb].[ServiceCostDetailName] = [sbcd].[ServiceCostDetailName] );

        -- /* If the value changed in the BillCostDetails */
        INSERT  INTO [ETL].[T1U_FactBilling_BillCostDetails]
                SELECT  [hbcd].[BillingCostDetailsKey]
                       ,[hbcd].[BillingCostDetailName]
                       ,[hbcd].[BillingCostDetailValue]
                FROM    [dbo].[FactServicePointBilling_BillCostDetails] [fbcd] WITH (NOLOCK)
                        INNER JOIN [Holding].[Billing_BillCostDetails] [hbcd] WITH (NOLOCK) ON [fbcd].[BillingCostDetailsKey] = [hbcd].[BillingCostDetailsKey]
                WHERE   [fbcd].[BillingCostDetailName] = [hbcd].[BillingCostDetailName]
                        AND [fbcd].[BillingCostDetailValue] != [hbcd].[BillingCostDetailValue];

        -- /* If the value changed in the ServiceCostDetails */
        INSERT  INTO [ETL].[T1U_FactBilling_ServiceCostDetails]
                SELECT  [hscd].[ServiceCostDetailsKey]
                       ,[hscd].[ServiceCostDetailName]
                       ,[hscd].[ServiceCostDetailValue]
                FROM    [dbo].[FactServicePointBilling_ServiceCostDetails] [fscd] WITH (NOLOCK)
                        INNER JOIN [Holding].[Billing_ServiceCostDetails] [hscd] WITH (NOLOCK) ON [fscd].[ServiceCostDetailsKey] = [hscd].[ServiceCostDetailsKey]
                WHERE   [fscd].[ServiceCostDetailName] = [hscd].[ServiceCostDetailName]
                        AND [fscd].[ServiceCostDetailValue] != [hscd].[ServiceCostDetailValue];


        DROP    TABLE [#tblSrcBilling];
        DROP     TABLE [#tblSrcFactBilling];

    END;


GO
