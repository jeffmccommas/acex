﻿

/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  Table [Holding].[BillingExceptions]    Script Date: 5/12/2017 10:19:56 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Holding].[BillingExceptions]') AND type in (N'U'))
    DROP TABLE [Holding].[BillingExceptions]
GO

/****** Object:  Table [Holding].[Billing]    Script Date: 5/12/2017 10:19:56 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Holding].[Billing]') AND type in (N'U'))
        DROP TABLE [Holding].[Billing]
GO

/****** Object:  Table [Holding].[Billing]    Script Date: 5/12/2017 10:19:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Holding].[Billing]') AND type in (N'U'))
BEGIN
            CREATE TABLE [Holding].[Billing](
                [ClientId] [int] NOT NULL,
                [CustomerId] [varchar](50) NOT NULL,
                [AccountId] [varchar](50) NOT NULL,
                [PremiseId] [varchar](50) NOT NULL,
                [ServiceContractId] [varchar](64) NOT NULL,
                [ServicePointId] [varchar](64) NULL,
                [BillStartDate] [datetime] NOT NULL,
                [BillEndDate] [datetime] NOT NULL,
                [BillDays] [int] NOT NULL,
                [TotalUnits] [decimal](18, 2) NOT NULL,
                [TotalCost] [decimal](18, 2) NOT NULL,
                [CommodityId] [int] NOT NULL,
                [BillPeriodTypeId] [int] NOT NULL,
                [UOMId] [int] NOT NULL,
                [AMIStartDate] [datetime] NULL,
                [AMIEndDate] [datetime] NULL,
                [MeterId] [varchar](64) NULL,
                [MeterType] [varchar](64) NULL,
                [ReplacedMeterId] [varchar](64) NULL,
                [RateClass] [varchar](256) NULL,
                [DueDate] [date] NULL,
                [ReadDate] [datetime] NULL,
                [ReadQuality] [varchar](50) NULL,
                [BillCycleScheduleId] [varchar](50) NULL,
                [SourceId] [int] NOT NULL,
                [TrackingId] [varchar](50) NOT NULL,
                [TrackingDate] [datetime] NOT NULL,
                [BillingRowId] [int] IDENTITY(1,1) NOT NULL,
                [ServiceCostDetailsKey] [uniqueidentifier] NULL,
                [BillingCostDetailsKey] [uniqueidentifier] NULL,
                [UtilityBillRecordId] [VARCHAR](64) NULL,
                [CanceledUtilityBillRecordId] [VARCHAR](64) NULL
            )
            END
GO
/****** Object:  Table [Holding].[BillingExceptions]    Script Date: 5/12/2017 10:19:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Holding].[BillingExceptions]') AND type in (N'U'))
BEGIN
            CREATE TABLE [Holding].[BillingExceptions](
                [Bill_Id] [nvarchar](50) NULL,
                [StartDate] [nvarchar](50) NULL,
                [EndDate] [nvarchar](50) NULL,
                [DueDate] [nvarchar](50) NULL,
                [BillPeriodType] [nvarchar](50) NULL,
                [BillCycleScheduleId] [nvarchar](64) NULL,
                [Account_Id] [nvarchar](50) NULL,
                [Customer_Id] [nvarchar](50) NULL,
                [AccountId] [nvarchar](255) NULL,
                [CustomerId] [nvarchar](255) NULL,
                [Source] [nvarchar](255) NULL,
                [PremiseId] [nvarchar](255) NULL,
                [Premise_Id] [nvarchar](50) NULL,
                [RateClass] [nvarchar](255) NULL,
                [AMIEndDate] [nvarchar](50) NULL,
                [AMIStartDate] [nvarchar](50) NULL,
                [ServicePointId] [nvarchar](255) NULL,
                [UOM] [nvarchar](50) NULL,
                [Commodity] [nvarchar](50) NULL,
                [TotalCost] [nvarchar](50) NULL,
                [TotalUsage] [nvarchar](50) NULL,
                [Client_ID] [nvarchar](50) NULL,
                [servicecontarctID1] [nvarchar](50) NULL,
                [billdays1] [nvarchar](50) NULL,
                [sourceid1] [nvarchar](50) NULL,
                [SourceFile] [nvarchar](50) NULL,
                [ErrorCode] [nvarchar](50) NULL,
                [ErrorColumn] [nvarchar](50) NULL,
                [UtilityBillRecordId] [VARCHAR](64) NULL,
                [CanceledUtilityBillRecordId] [VARCHAR](64) NULL
            )
            END
GO
