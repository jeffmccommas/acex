﻿

/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/



/****** Object:  StoredProcedure [ETL].[S_FactBilling]    Script Date: 5/13/2017 7:58:10 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_FactBilling]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [ETL].[S_FactBilling]
GO

/****** Object:  StoredProcedure [ETL].[S_Billing]    Script Date: 5/13/2017 7:58:10 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_Billing]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [ETL].[S_Billing]
GO

/****** Object:  StoredProcedure [ETL].[S_Billing]    Script Date: 5/13/2017 7:58:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_Billing]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[S_Billing] AS'
END
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 7/21/2014
-- Description:
--
-- 10/28/2016 -- Phil Victor -- Added RateClassKey
-- 01/10/2017 -- Phil Victor -- Added CostDetailsKey
-- 05/13/2017 -- Phil Victor -- Added UtilityRecordIds
-- =============================================
ALTER PROCEDURE [ETL].[S_Billing] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT  [ClientId]
               ,[AccountId]
               ,[PremiseId]
               ,[ServiceContractId]
               ,[ServicePointId]
               ,[MeterId]
               ,[StartDate]
               ,[EndDate]
               ,[BillDays]
               ,[TotalUnits]
               ,[TotalCost]
               ,[CommodityId]
               ,[BillPeriodTypeId]
               ,[BillCycleScheduleId]
               ,[UOMId]
               ,[RateClass]
               ,[RateClassKey]
               ,[DueDate]
               ,[ReadDate]
               ,[ReadQuality]
               ,[SourceId]
               ,[TrackingId]
               ,[TrackingDate]
               ,[BillingCostDetailsKey]
               ,[ServiceCostDetailsKey]
               ,[UtilityBillRecordId]
               ,[CanceledUtilityBillRecordId]
        FROM    (SELECT [hb].[ClientId]
                       ,[AccountId]
                       ,[PremiseId]
                       ,[ServiceContractId]
                       ,[ServicePointId]
                       ,[MeterId]
                       ,[BillStartDate] AS StartDate
                       ,[BillEndDate] AS EndDate
                       ,[BillDays]
                       ,[TotalUnits]
                       ,[TotalCost]
                       ,[CommodityId]
                       ,[BillPeriodTypeId]
                       ,[BillCycleScheduleId]
                       ,[UOMId]
                       ,[RateClass]
                       ,[drc].[RateClassKey]
                       ,[DueDate]
                       ,[ReadDate]
                       ,[ReadQuality]
                       ,[SourceId]
                       ,[TrackingId]
                       ,[TrackingDate]
                       ,[BillingCostDetailsKey]
                       ,[ServiceCostDetailsKey]
                       ,[UtilityBillRecordId]
                       ,[CanceledUtilityBillRecordId]
                       ,ROW_NUMBER() OVER (PARTITION BY [hb].[ClientId], [AccountId], [PremiseId], [ServiceContractId],
                                           [BillEndDate], [CommodityId] ORDER BY [TrackingDate] DESC) AS [SortId]
                 FROM   [Holding].[Billing] [hb]
                        LEFT JOIN [dbo].[DimRateClass] [drc] ON [drc].[ClientId] = [hb].[ClientId]
                                                          AND [drc].[RateClassName] = [hb].[RateClass]
                 WHERE  [hb].[ClientId] = @ClientID
                ) [b]
        WHERE   [b].[SortId] = 1
        ORDER BY [ClientId]
               ,[PremiseId]
               ,[AccountId]
               ,[ServiceContractId]
               ,[EndDate]
               ,[CommodityId];


    END;


GO
/****** Object:  StoredProcedure [ETL].[S_FactBilling]    Script Date: 5/13/2017 7:58:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_FactBilling]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[S_FactBilling] AS'
END
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 7/21/2014
-- Description:
--
-- 10/27/2016 -- Phil Victor -- Added RateClassKey
-- 01/12/2017 -- Phil Victor -- Added CostDetailKeys
-- 05/13/2017 -- Phil Victor -- Added UtilityRecordIds
-- =============================================
ALTER PROCEDURE [ETL].[S_FactBilling] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT  [fspb].[ServiceContractKey]
               ,[BillPeriodStartDateKey]
               ,[BillPeriodEndDateKey]
               ,[fspb].[ClientId]
               ,[fspb].[PremiseId]
               ,[fspb].[AccountId]
               ,IIF([dsc].[IsInferred] = 1, '', [dsc].[ServiceContractId]) AS [ServiceContractId]
               ,[fspb].[StartDate]
               ,[fspb].[EndDate]
               ,[fspb].[CommodityId]
               ,[fspb].[TotalUsage]
               ,[fspb].[CostOfUsage]
               ,[fspb].[SourceKey]
               ,[fspb].[BillDays]
               ,[fspb].[RateClassKey1] AS [RateClassKey]
               ,IIF([fspb].[BillingCostDetailsKey] IS NULL, [kfb].[BillingCostDetailsKey], [fspb].[BillingCostDetailsKey]) AS [BillingCostDetailKey]
               ,IIF([fspb].[ServiceCostDetailsKey] IS NULL, [kfb].[ServiceCostDetailsKey], [fspb].[ServiceCostDetailsKey]) AS [ServiceCostDetailsKey]
               ,[fspb].[UtilityBillRecordId]
               ,[fspb].[CanceledUtilityBillRecordId]
        FROM    [ETL].[KEY_FactBilling] [kfb] WITH (NOLOCK)
                INNER JOIN [dbo].[DimPremise] [p] WITH (NOLOCK) ON [p].[ClientId] = [kfb].[ClientId]
                                                             AND [p].[AccountId] = [kfb].[AccountId]
                                                             AND [p].[PremiseId] = [kfb].[PremiseId]
                INNER JOIN [dbo].[DimServiceContract] [dsc] WITH (NOLOCK) ON [dsc].[ClientId] = [kfb].[ClientId]
                                                                       AND [dsc].[PremiseKey] = [p].[PremiseKey]
                                                                       AND [kfb].[CommodityId] = [dsc].[CommodityKey]
                                                                       AND [kfb].[ServiceContractId] = [dsc].[ServiceContractId]
                INNER JOIN [dbo].[FactServicePointBilling] [fspb] WITH (NOLOCK) ON [fspb].[ClientId] = [kfb].[ClientId]
                                                                             AND [fspb].[ServiceContractKey] = [dsc].[ServiceContractKey]
                                                                             AND [kfb].[EndDate] = [fspb].[EndDate]
        WHERE   [kfb].[ClientId] = @ClientID
        ORDER BY [fspb].[ClientId]
               ,[fspb].[PremiseId]
               ,[fspb].[AccountId]
               ,[dsc].[ServiceContractId]
               ,[fspb].[EndDate]
               ,[fspb].[CommodityId];

    END;

GO
