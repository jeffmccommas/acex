﻿
/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/

/***** May take a while because of re-Indexing **********/
/********************************************************/

/****** Object:  Table [audit].[RemovedFromFactServicePointBilling]    Script Date: 5/13/2017 10:58:28 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[audit].[RemovedFromFactServicePointBilling]') AND type in (N'U'))
        DROP TABLE [audit].[RemovedFromFactServicePointBilling]
GO

/****** Object:  Table [audit].[RemovedFrom_FactServicePointBilling]    Script Date: 5/13/2017 10:58:28 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[audit].[RemovedFrom_FactServicePointBilling]') AND type in (N'U'))
        DROP TABLE [audit].[RemovedFrom_FactServicePointBilling]
GO

/****** Object:  Table [audit].[MissingCanceled_FactServicePointBilling]    Script Date: 5/13/2017 10:58:28 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[audit].[MissingCanceled_FactServicePointBilling]') AND type in (N'U'))
        DROP TABLE [audit].[MissingCanceled_FactServicePointBilling]
GO

/****** Object:  Table [dbo].[FactServicePointBilling]    Script Date: 5/13/2017 10:58:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME =N'FactServicePointBilling' AND COLUMN_NAME = N'UtilityBillRecordId')
BEGIN
        ALTER TABLE [dbo].[FactServicePointBilling] ADD [UtilityBillRecordId] [VARCHAR](64) NULL
END;

IF NOT EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'FactServicePointBilling') AND name = 'CanceledUtilityBillRecordId')
BEGIN
        ALTER TABLE [dbo].[FactServicePointBilling] ADD [CanceledUtilityBillRecordId] [VARCHAR](64) NULL
END;

/****** Object:  Table [audit].[RemovedFrom_FactServicePointBilling]    Script Date: 5/13/2017 10:58:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[audit].[RemovedFrom_FactServicePointBilling]') AND type in (N'U'))
BEGIN
        CREATE TABLE [audit].[RemovedFrom_FactServicePointBilling](
            [ServiceContractKey] [int] NOT NULL,
            [BillPeriodStartDateKey] [int] NOT NULL,
            [BillPeriodEndDateKey] [int] NOT NULL,
            [UOMKey] [int] NOT NULL,
            [BillPeriodTypeKey] [int] NOT NULL,
            [ClientId] [int] NOT NULL,
            [PremiseId] [varchar](50) NOT NULL,
            [AccountId] [varchar](50) NOT NULL,
            [CommodityId] [int] NOT NULL,
            [BillPeriodTypeId] [int] NOT NULL,
            [UOMId] [int] NOT NULL,
            [StartDate] [datetime] NOT NULL,
            [EndDate] [datetime] NOT NULL,
            [TotalUsage] [decimal](18, 2) NOT NULL,
            [CostOfUsage] [decimal](18, 2) NOT NULL,
            [OtherCost] [decimal](18, 2) NULL,
            [RateClassKey1] [int] NULL,
            [RateClassKey2] [int] NULL,
            [NextReadDate] [date] NULL,
            [ReadDate] [datetime] NULL,
            [CreateDate] [datetime] NULL,
            [UpdateDate] [datetime] NULL,
            [BillCycleScheduleId] [varchar](50) NULL,
            [BillDays] [int] NOT NULL,
            [ETL_LogId] [int] NOT NULL,
            [TrackingId] [varchar](50) NOT NULL,
            [TrackingDate] [datetime] NOT NULL,
            [SourceKey] [int] NOT NULL,
            [SourceId] [int] NOT NULL,
            [ReadQuality] [varchar](50) NULL,
            [DueDate] [date] NULL,
            [IsFault] [bit] NOT NULL,
            [BillingCostDetailsKey] [uniqueidentifier] NULL,
            [ServiceCostDetailsKey] [uniqueidentifier] NULL,
            [UtilityBillRecordId] [VARCHAR](64)  NULL,
            [CanceledUtilityBillRecordId] [VARCHAR](64) NULL,
            [RemovedDate] DATETIME2(3) CONSTRAINT DF_RemovedFrom_FactServicePointBilling_RemovedDate DEFAULT (SYSDATETIME())
        )
END

GO

/****** Object:  Table [audit].[MissingCanceled_FactServicePointBilling]    Script Date: 5/13/2017 10:58:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[audit].[MissingCanceled_FactServicePointBilling]') AND type in (N'U'))
BEGIN
        CREATE TABLE [audit].[MissingCanceled_FactServicePointBilling](
             [ClientId] [int] NOT NULL,
             [PremiseId] [varchar](50) NOT NULL,
             [AccountId] [varchar](50) NOT NULL,
             [CustomerId] [varchar](50) NOT NULL,
             [ServiceContractId] [varchar](64) NOT NULL,
             [ServicePointId] [varchar](64) NULL,
             [MeterId] [varchar](64) NULL,
             [MeterType] [varchar](64) NULL,
             [ReplacedMeterId] [varchar](64) NULL,
             [RateClass] [varchar](256) NULL,
             [CommodityId] [int] NOT NULL,
             [AMIStartDate] [datetime] NULL,
             [AMIEndDate] [datetime] NULL,
             [StartDate] [datetime] NOT NULL,
             [EndDate] [datetime] NOT NULL,
             [SourceId] [int] NOT NULL,
             [TrackingId] [varchar](50) NOT NULL,
             [TrackingDate] [datetime] NOT NULL,
             [BillingCostDetailsKey] [uniqueidentifier] NULL,
             [ServiceCostDetailsKey] [uniqueidentifier] NULL,
             [UtilityBillRecordId] [varchar](64) NULL,
             [CanceledUtilityBillRecordId] [varchar](64) NULL,
            [FoundMissingDate] DATETIME2(3) CONSTRAINT DF_MissingCanceled_FactServicePointBilling_FoundMissingDate DEFAULT (SYSDATETIME())
        )
END

GO

