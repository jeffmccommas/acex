﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  StoredProcedure [ETL].[U_FactBilling]    Script Date: 5/13/2017 4:37:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[U_FactBilling]') AND type in (N'P', N'PC'))
        DROP PROCEDURE [ETL].[U_FactBilling]
GO

/****** Object:  StoredProcedure [ETL].[I_FactBilling]    Script Date: 5/13/2017 4:37:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[I_FactBilling]') AND type in (N'P', N'PC'))
        DROP PROCEDURE [ETL].[I_FactBilling]
GO

/****** Object:  StoredProcedure [ETL].[I_FactBilling]    Script Date: 5/13/2017 4:37:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[I_FactBilling]') AND type in (N'P', N'PC'))
BEGIN
        EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[I_FactBilling] AS'
END
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 6/13/2014
-- Description:
--
-- PjV - 11/15/2016 - Added IsFault flag handling.
-- 01/10/2017 -- Phil Victor -- Added CostDetailsKeys
-- =============================================
ALTER PROCEDURE [ETL].[I_FactBilling] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        UPDATE  ifb
        SET ServiceContractId = kfb.ServiceContractId
        FROM ETL.INS_FactBilling ifb WITH ( NOLOCK )
                INNER JOIN ETL.KEY_FactBilling kfb WITH ( NOLOCK ) ON kfb.ClientId = ifb.ClientId
                                                              AND kfb.AccountId = ifb.AccountId
                                                              AND kfb.PremiseId = ifb.PremiseId
                                                              AND kfb.CommodityId = ifb.CommodityId
        WHERE   ifb.ServiceContractId = '' OR [ifb].[ServiceContractId] IS NULL;

        INSERT INTO dbo.FactServicePointBilling
                ( ServiceContractKey ,
                  BillPeriodStartDateKey ,
                  BillPeriodEndDateKey ,
                  UOMKey ,
                  BillPeriodTypeKey ,
                  ClientId ,
                  PremiseId ,
                  AccountId ,
                  CommodityId ,
                  BillPeriodTypeId ,
                  UOMId ,
                  StartDate ,
                  EndDate ,
                  TotalUsage ,
                  CostOfUsage ,
                  OtherCost ,
                  RateClassKey1 ,
                  RateClassKey2 ,
                  DueDate ,
                  ReadDate ,
                  CreateDate ,
                  UpdateDate ,
                  BillCycleScheduleId ,
                  BillDays ,
                  ReadQuality ,
                  ETL_LogId ,
                  TrackingId ,
                  TrackingDate ,
                  SourceKey ,
                  SourceId,
                  [IsFault] ,
                  [BillingCostDetailsKey],
                  [ServiceCostDetailsKey],
                  [UtilityBillRecordId],
                  [CanceledUtilityBillRecordId]
                )
                SELECT  dsc.ServiceContractKey ,
                        dsd.DateKey ,
                        ded.DateKey ,
                        duom.UOMKey ,
                        dbpt.BillPeriodTypeKey ,
                        fb.ClientId ,
                        fb.PremiseId ,
                        fb.AccountId ,
                        fb.CommodityId ,
                        fb.BillPeriodTypeId ,
                        fb.UOMId ,
                        StartDate ,
                        EndDate ,
                        fb.TotalUnits ,
                        fb.TotalCost ,
                        NULL ,
                        drc.RateClassKey ,
                        NULL ,
                        fb.DueDate ,
                        fb.ReadDate ,
                        GETUTCDATE() ,
                        GETUTCDATE() ,
                        fb.BillCycleScheduleId ,
                        fb.BillDays ,
                        fb.ReadQuality ,
                        fb.ETL_LogId ,
                        fb.TrackingId ,
                        fb.TrackingDate ,
                        ds.SourceKey ,
                        fb.SourceId,
                        0,
                        fb.[BillingCostDetailsKey],
                        fb.[ServiceCostDetailsKey],
                        fb.[UtilityBillRecordId],
                        fb.[CanceledUtilityBillRecordId]
                FROM  [ETL].[INS_FactBilling] fb WITH ( NOLOCK )
                        INNER JOIN [dbo].[DimPremise] dp WITH ( NOLOCK ) ON dp.[PremiseId] = fb.[PremiseId]
                                                              AND dp.[AccountId] = fb.[AccountId]
                                                              AND dp.[ClientId] = fb.[ClientId]
                        INNER JOIN [dbo].[DimServiceContract] dsc WITH ( NOLOCK ) ON dsc.[PremiseKey] = dp.[PremiseKey]
                                                              AND dsc.[CommodityKey] = fb.[CommodityId]
                                                              AND dsc.[ServiceContractId] = fb.[ServiceContractId]
                        INNER JOIN [dbo].[DimUOM] duom WITH ( NOLOCK ) ON duom.UOMId = fb.UOMId
                        INNER JOIN [dbo].[DimBillPeriodType] dbpt WITH ( NOLOCK ) ON dbpt.[BillPeriodTypeId] = fb.[BillPeriodTypeId]
                        INNER JOIN [dbo].[DimDate] dsd WITH ( NOLOCK ) ON dsd.[FullDateAlternateKey] = fb.[StartDate]
                        INNER JOIN [dbo].[DimDate] ded WITH ( NOLOCK ) ON ded.[FullDateAlternateKey] = fb.[EndDate]
                        INNER JOIN [dbo].[DimSource] ds WITH ( NOLOCK ) ON ds.[SourceId] = fb.[SourceId]
                        LEFT JOIN [dbo].[DimRateClass] drc WITH ( NOLOCK ) ON drc.[ClientId] = @ClientID
                                                          AND drc.[RateClassName] = fb.[RateClass]
                WHERE   fb.[ClientId] = @ClientID;


                -- Update the FactServicePoint isFault flag if the Meter has been deleted.
                UPDATE fspb
                SET [IsFault] = 1
                FROM [ETL].[KEY_FactBilling]  kfb WITH ( NOLOCK )
                        INNER JOIN [dbo].[DimMeter] dm WITH ( NOLOCK ) ON dm.[ClientId] =  @ClientID
                                                      AND dm.[CommodityKey] = kfb.CommodityId
                                                      AND dm.[MeterId] = kfb.ReplacedMeterId
                         INNER JOIN  [dbo].[FactServicePoint] fsp WITH ( NOLOCK ) ON fsp.[ServicePointKey] =  dm.[ServicePointKey]
                         INNER JOIN [dbo].[FactServicePointBilling] fspb WITH ( NOLOCK ) ON fsp.[ServiceContractKey] =  fspb.[ServiceContractKey]
                                                AND fspb.[ClientId] =   dm.ClientId
                                                AND fspb.[IsFault] = 0
                                                AND [fspb].[EndDate] = kfb.[EndDate]
                 WHERE [IsFault] != 1

                /* Insert data into the FactServicePointBilling_BillCostDetails table */
                INSERT INTO [dbo].[FactServicePointBilling_BillCostDetails]
                           ([BillingCostDetailsKey],
                            [BillingCostDetailName],
                            [BillingCostDetailValue])
                               SELECT [BillingCostDetailsKey],
                               [BillingCostDetailName],
                               [BillingCostDetailValue]
                               FROM [ETL].[INS_FactBilling_BillCostDetails] WITH ( NOLOCK )

                /* Insert data into the FactServicePointBilling_ServiceCostDetails table */
                INSERT INTO [dbo].[FactServicePointBilling_ServiceCostDetails]
                           ([ServiceCostDetailsKey],
                            [ServiceCostDetailName],
                            [ServiceCostDetailValue])
                               SELECT [ServiceCostDetailsKey],
                               [ServiceCostDetailName],
                               [ServiceCostDetailValue]
                               FROM [ETL].[INS_FactBilling_ServiceCostDetails] WITH ( NOLOCK )

    END;
GO


/****** Object:  StoredProcedure [ETL].[U_FactBilling]    Script Date: 5/13/2017 4:37:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[U_FactBilling]') AND type in (N'P', N'PC'))
BEGIN
        EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[U_FactBilling] AS'
END
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 7/2/2014
-- Description:
--
-- 10/27/2016 -- Phil Victor -- Added IsFault
-- 01/10/2017 -- Phil Victor -- Added CostDetailsKeys
-- =============================================
ALTER PROCEDURE [ETL].[U_FactBilling] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @TrackingDate DATETIME;
        SET @TrackingDate = GETUTCDATE();

        UPDATE [dbo].[FactServicePointBilling]
        SET
                [BillPeriodStartDateKey] = dsd.[DateKey],
                TotalUsage = b.TotalUnits ,
                CostOfUsage = b.TotalCost ,
                BillDays = b.BillDays ,
                [StartDate] = b.StartDate,
                RateClassKey1 = drc.RateClassKey ,
                ReadQuality = b.ReadQuality ,
                ReadDate = b.ReadDate ,
                DueDate = b.DueDate ,
                UpdateDate = @TrackingDate ,
                TrackingId = b.TrackingId ,
                SourceKey = ds.SourceKey ,
                SourceId = b.SourceId ,
                TrackingDate = b.TrackingDate,
                IsFault = b.IsFault
        FROM [ETL].[T1U_FactBilling] b WITH ( NOLOCK )
                INNER JOIN [dbo].[FactServicePointBilling] fb WITH ( NOLOCK ) ON fb.ServiceContractKey = b.ServiceContractKey
                                                              AND fb.BillPeriodEndDateKey = b.BillPeriodEndDateKey
                INNER JOIN [dbo].[DimSource] ds ON ds.SourceId = b.SourceId
                INNER JOIN [dbo].[DimDate] dsd WITH ( NOLOCK ) ON dsd.[FullDateAlternateKey] = b.[StartDate]
                LEFT JOIN [dbo].[DimRateClass] drc ON drc.ClientId = @ClientID
                                                  AND drc.RateClassName = b.RateClass
        WHERE   b.ClientID = @ClientID;

        /* Update BillCostDetails - can only do value */
        UPDATE [dbo].[FactServicePointBilling_BillCostDetails]
        SET [BillingCostDetailValue] = b.[BillingCostDetailValue]
        FROM [ETL].[T1U_FactBilling_BillCostDetails] b WITH ( NOLOCK )
            INNER JOIN [dbo].[FactServicePointBilling_BillCostDetails] fb WITH ( NOLOCK ) ON fb.[BillingCostDetailsKey] = b.[BillingCostDetailsKey]
                    AND fb.[BillingCostDetailName] = b.[BillingCostDetailName]

        /* Update ServiceCostDetails - can only do value */
        UPDATE [dbo].[FactServicePointBilling_ServiceCostDetails]
        SET    [ServiceCostDetailValue] = b.[ServiceCostDetailValue]
        FROM [ETL].[T1U_FactBilling_ServiceCostDetails] b WITH ( NOLOCK )
            INNER JOIN [dbo].[FactServicePointBilling_ServiceCostDetails] fb WITH ( NOLOCK ) ON fb.[ServiceCostDetailsKey] = b.[ServiceCostDetailsKey]
                AND fb.[ServiceCostDetailName] = b.[ServiceCostDetailName]

    END;






GO
