﻿


/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  StoredProcedure [ETL].[S_KEY_Billing]    Script Date: 5/12/2017 2:35:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_KEY_Billing]') AND type in (N'P', N'PC'))
        DROP PROCEDURE [ETL].[S_KEY_Billing]
GO

/****** Object:  Index [IX_Key_FactBilling_UtilityRecordId]    Script Date: 5/12/2017 2:35:17 PM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND name = N'IX_Key_FactBilling_UtilityRecordId')
        DROP INDEX [IX_Key_FactBilling_UtilityRecordId] ON [ETL].[KEY_FactBilling]
GO

/****** Object:  Index [IX_Key_FactBilling_PremiseId]    Script Date: 5/12/2017 2:35:17 PM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND name = N'IX_Key_FactBilling_PremiseId')
        DROP INDEX [IX_Key_FactBilling_PremiseId] ON [ETL].[KEY_FactBilling]
GO

/****** Object:  Index [IX_KEY_FactBilling_ClientId]    Script Date: 5/12/2017 2:35:17 PM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND name = N'IX_KEY_FactBilling_ClientId')
        DROP INDEX [IX_KEY_FactBilling_ClientId] ON [ETL].[KEY_FactBilling]
GO

/****** Object:  Index [IX_KEY_FactBilling_Client_RateClass]    Script Date: 5/12/2017 2:35:17 PM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND name = N'IX_KEY_FactBilling_Client_RateClass')
        DROP INDEX [IX_KEY_FactBilling_Client_RateClass] ON [ETL].[KEY_FactBilling]
GO

/****** Object:  Table [ETL].[KEY_FactBilling]    Script Date: 5/12/2017 2:35:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND type in (N'U'))
        DROP TABLE [ETL].[KEY_FactBilling]
GO

/****** Object:  Table [ETL].[KEY_FactBilling]    Script Date: 5/12/2017 2:35:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND type in (N'U'))
BEGIN
            CREATE TABLE [ETL].[KEY_FactBilling](
                [ClientId] [int] NOT NULL,
                [PremiseId] [varchar](50) NOT NULL,
                [AccountId] [varchar](50) NOT NULL,
                [CustomerId] [varchar](50) NOT NULL,
                [ServiceContractId] [varchar](64) NOT NULL,
                [ServicePointId] [varchar](64) NULL,
                [MeterId] [varchar](64) NULL,
                [MeterType] [varchar](64) NULL,
                [ReplacedMeterId] [varchar](64) NULL,
                [RateClass] [varchar](256) NULL,
                [CommodityId] [int] NOT NULL,
                [AMIStartDate] [datetime] NULL,
                [AMIEndDate] [datetime] NULL,
                [StartDate] [datetime] NOT NULL,
                [EndDate] [datetime] NOT NULL,
                [SourceId] [int] NOT NULL,
                [TrackingId] [varchar](50) NOT NULL,
                [TrackingDate] [datetime] NOT NULL,
                [BillingCostDetailsKey] [uniqueidentifier] NULL,
                [ServiceCostDetailsKey] [uniqueidentifier] NULL,
                [UtilityBillRecordId] [varchar](64) NULL,
                [CanceledUtilityBillRecordId] [varchar](64) NULL
            )
            END
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IX_KEY_FactBilling_Client_RateClass]    Script Date: 5/12/2017 2:35:17 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND name = N'IX_KEY_FactBilling_Client_RateClass')
        CREATE NONCLUSTERED INDEX [IX_KEY_FactBilling_Client_RateClass] ON [ETL].[KEY_FactBilling]
        (
            [ClientId] ASC,
            [RateClass] ASC
        )WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_KEY_FactBilling_ClientId]    Script Date: 5/12/2017 2:35:17 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND name = N'IX_KEY_FactBilling_ClientId')
        CREATE NONCLUSTERED INDEX [IX_KEY_FactBilling_ClientId] ON [ETL].[KEY_FactBilling]
        (
            [ClientId] ASC
        )
        INCLUDE (   [AccountId],
            [AMIEndDate],
            [AMIStartDate],
            [CommodityId],
            [PremiseId],
            [ServiceContractId],
            [TrackingDate],
            [TrackingId],
            [EndDate]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Key_FactBilling_PremiseId]    Script Date: 5/12/2017 2:35:17 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND name = N'IX_Key_FactBilling_PremiseId')
        CREATE NONCLUSTERED INDEX [IX_Key_FactBilling_PremiseId] ON [ETL].[KEY_FactBilling]
        (
            [ClientId] ASC,
            [PremiseId] ASC,
            [AccountId] ASC
        )
        INCLUDE (   [AMIEndDate],
            [AMIStartDate],
            [CommodityId],
            [ServiceContractId],
            [TrackingDate],
            [TrackingId]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Key_FactBilling_UtilityRecordId]    Script Date: 5/12/2017 2:35:17 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND name = N'IX_Key_FactBilling_UtilityRecordId')
        CREATE NONCLUSTERED INDEX [IX_Key_FactBilling_UtilityRecordId] ON [ETL].[KEY_FactBilling]
        (
            [UtilityBillRecordId] ASC,
            [CanceledUtilityBillRecordId] ASC
        )WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  StoredProcedure [ETL].[S_KEY_Billing]    Script Date: 5/12/2017 2:35:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_KEY_Billing]') AND type in (N'P', N'PC'))
BEGIN
        EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[S_KEY_Billing] AS'
END
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 9/19/2014
-- Description:

-- 01/10/2017 -- Phil Victor -- Added CostDetailsKey
-- =============================================
ALTER PROCEDURE [ETL].[S_KEY_Billing] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT  ClientId ,
                AccountId ,
                PremiseId ,
                CustomerId ,
                ServiceContractId ,
                ServicePointId ,
                MeterId ,
                MeterType ,
                ReplacedMeterId ,
                RateClass ,
                StartDate ,
                EndDate ,
                CommodityId ,
                b.AMIStartDate ,
                b.AMIEndDate ,
                SourceId ,
                TrackingId ,
                TrackingDate,
                [BillingCostDetailsKey],
                [ServiceCostDetailsKey],
                [UtilityBillRecordId],
                [CanceledUtilityBillRecordId]
        FROM    ( SELECT    ClientId ,
                            AccountId ,
                            PremiseId ,
                            CustomerId ,
                            ServiceContractId ,
                            ServicePointId ,
                            MeterId ,
                            MeterType ,
                            ReplacedMeterId ,
                            RateClass ,
                            BillStartDate AS StartDate ,
                            BillEndDate EndDate ,
                            BillDays ,
                            TotalUnits ,
                            TotalCost ,
                            CommodityId ,
                            AMIStartDate ,
                            AMIEndDate ,
                            SourceId ,
                            TrackingId ,
                            TrackingDate ,
                            [BillingCostDetailsKey],
                            [ServiceCostDetailsKey],
                            [UtilityBillRecordId],
                            [CanceledUtilityBillRecordId] ,
                            ROW_NUMBER() OVER ( PARTITION BY ClientId,
                                                AccountId,
                                                PremiseId,
                                                CustomerId,
                                                ServiceContractId,
                                                BillEndDate,
                                                CommodityId ORDER BY TrackingDate DESC ) AS SortId
                  FROM      Holding.Billing
                  WHERE     ClientId = @ClientID
                ) b
        WHERE   SortId = 1
        ORDER BY ClientId ,
                PremiseId ,
                AccountId ,
                CustomerId ,
                ServiceContractId ,
                EndDate ,
                CommodityId;


    END;
GO

