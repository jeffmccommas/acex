﻿

/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  Index [IX_DisAggReCalcQueue]    Script Date: 5/15/2017 8:49:26 AM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Holding].[DisAggReCalcQueue]') AND name = N'IX_DisAggReCalcQueue')
    DROP INDEX [IX_DisAggReCalcQueue] ON [Holding].[DisAggReCalcQueue]
GO

/****** Object:  Table [Holding].[DisAggReCalcQueue]    Script Date: 5/15/2017 8:49:26 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Holding].[DisAggReCalcQueue]') AND type in (N'U'))
    DROP TABLE [Holding].[DisAggReCalcQueue]
GO

/****** Object:  Table [Holding].[DisAggReCalcQueue]    Script Date: 5/15/2017 8:49:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Holding].[DisAggReCalcQueue]') AND type in (N'U'))
BEGIN
        CREATE TABLE [Holding].[DisAggReCalcQueue](
            [ClientId] [int] NOT NULL,
            [PremiseId] [varchar](50) NOT NULL,
            [AccountId] [varchar](50) NOT NULL,
            [CustomerId] [varchar](50) NOT NULL,
            [SourceId] [int] NULL,
            [TrackingId] [varchar](50) NULL,
            [TrackingDate] [datetime] NULL,
            [UtilityBillRecordId] [VARCHAR](64) NULL,
            [CanceledUtilityBillRecordId] [VARCHAR](64) NULL
        )
END
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_DisAggReCalcQueue]    Script Date: 5/15/2017 8:49:26 AM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Holding].[DisAggReCalcQueue]') AND name = N'IX_DisAggReCalcQueue')
        CREATE CLUSTERED INDEX [IX_DisAggReCalcQueue] ON [Holding].[DisAggReCalcQueue]
        (
            [ClientId] ASC,
            [CustomerId] ASC,
            [AccountId] ASC,
            [PremiseId] ASC
        )WITH (STATISTICS_NORECOMPUTE = ON, DROP_EXISTING = OFF, ONLINE = OFF)
GO
