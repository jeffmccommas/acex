﻿
/**************************************/
/***** RUN ON THE INSIGHTS DATABASE ***/
/**************************************/

/****** Object:  StoredProcedure [ETL].[U_API_BillsForBillDisAgg]    Script Date: 5/15/2017 9:49:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[U_API_BillsForBillDisAgg]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [ETL].[U_API_BillsForBillDisAgg]
GO

/****** Object:  StoredProcedure [ETL].[U_API_BillsForBillDisAgg]    Script Date: 5/15/2017 9:49:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[U_API_BillsForBillDisAgg]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[U_API_BillsForBillDisAgg] AS'
END
GO

-- =============================================
-- Author:      Philip Victor
-- Create date: 03/20/2017
-- Description:
-- =============================================
ALTER PROCEDURE [ETL].[U_API_BillsForBillDisAgg] @ClientID INT
AS
    BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
        SET NOCOUNT ON;

        /* Handled canceled Bills */
        IF EXISTS ( SELECT  1
                    FROM   [ETL].[U_DisAggBills_API]
                    WHERE [CanceledUtilityBillRecordId] IS NOT NULL AND Len([CanceledUtilityBillRecordId]) > 0 )
        BEGIN

            DELETE  [wb]
            FROM   [wh].[Bill] [wb]
                    INNER JOIN  [ETL].[U_DisAggBills_API] [bd] ON [wb].[UtilityBillRecordId] = [bd].[CanceledUtilityBillRecordId]
                WHERE  ([wb].[UtilityBillRecordId] IS NOT NULL AND Len([wb].[UtilityBillRecordId]) > 0)
                    AND ( [bd].[CanceledUtilityBillRecordId] IS NOT NULL AND Len([bd].[CanceledUtilityBillRecordId]) > 0);
        END

   -- Insert statements for procedure here
        MERGE [wh].[Bill] AS [wb]
        USING
            (SELECT [bd].[ClientId]
                   ,[bd].[CustomerId]
                   ,[bd].[AccountId]
                   ,[bd].[PremiseId]
                   ,[bd].[StartDate]
                   ,[bd].[EndDate]
                   ,[bd].[TotalUnits]
                   ,[bd].[TotalCost]
                   ,[bd].[CommodityKey]
                   ,[bd].[UomKey]
                   ,[bd].[BillDays]
                   ,[bd].[UtilityBillRecordId]
                   ,[bd].[CanceledUtilityBillRecordId]
             FROM   [ETL].[U_DisAggBills_API] [bd]
                    INNER JOIN [wh].[ProfileCustomerAccount] [pca] ON [pca].[AccountID] = [bd].[AccountId]
                                                                    AND [pca].[ClientID] = [bd].[ClientId]
                                                                    AND [pca].[CustomerID] = [bd].[CustomerId]
             WHERE  [bd].[ClientId] = @ClientID
            ) AS [source]
        ON [wb].[ClientId] = [source].[ClientId]
            AND [wb].[CustomerId] = [source].[CustomerId]
            AND [wb].[AccountId] = [source].[AccountId]
            AND [wb].[PremiseId] = [source].[PremiseId]
            AND [wb].[EndDate] = [source].[EndDate]
            AND [wb].[CommodityKey] = [source].[CommodityKey]
        WHEN MATCHED THEN
            UPDATE SET
                    [TotalUnits] = [source].[TotalUnits]
                   ,[TotalCost] = [source].[TotalCost]
                   ,[UomKey] = [source].[UomKey]
                   ,[BillDays] = [source].[BillDays]
                   ,[CommodityKey] = [source].[CommodityKey]
        WHEN NOT MATCHED THEN
            INSERT (
                    [ClientId]
                   ,[CustomerId]
                   ,[AccountId]
                   ,[PremiseId]
                   ,[StartDate]
                   ,[EndDate]
                   ,[CommodityKey]
                   ,[TotalUnits]
                   ,[TotalCost]
                   ,[UomKey]
                   ,[BillDays]
                   ,[UtilityBillRecordId]
                   ,[CanceledUtilityBillRecordId]
                   )
            VALUES (
                    [source].[ClientId]
                   ,[source].[CustomerId]
                   ,[source].[AccountId]
                   ,[source].[PremiseId]
                   ,[source].[StartDate]
                   ,[source].[EndDate]
                   ,[source].[CommodityKey]
                   ,[source].[TotalUnits]
                   ,[source].[TotalCost]
                   ,[source].[UomKey]
                   ,[source].[BillDays]
                   ,[source].[UtilityBillRecordId]
                   ,[source].[CanceledUtilityBillRecordId]
                   );
    END;





GO
