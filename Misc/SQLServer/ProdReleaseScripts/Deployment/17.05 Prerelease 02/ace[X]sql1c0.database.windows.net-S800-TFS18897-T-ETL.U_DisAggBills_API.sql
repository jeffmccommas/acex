﻿
/**************************************/
/***** RUN ON THE INSIGHTS DATABASE ***/
/**************************************/



/****** Object:  Table [ETL].[U_DisAggBills_API]    Script Date: 5/15/2017 9:44:00 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[U_DisAggBills_API]') AND type in (N'U'))
    DROP TABLE [ETL].[U_DisAggBills_API]
GO

/****** Object:  Table [ETL].[U_DisAggBills_API]    Script Date: 5/15/2017 9:44:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS ( SELECT  *
                FROM    [sys].[objects]
                WHERE   [object_id] = OBJECT_ID(N'[ETL].[U_DisAggBills_API]')
                        AND [type] IN (N'U') )
BEGIN
    CREATE TABLE [ETL].[U_DisAggBills_API]
        (
         [ClientId] [INT] NOT NULL
        ,[CustomerId] [VARCHAR](50) NOT NULL
        ,[AccountId] [VARCHAR](50) NOT NULL
        ,[PremiseId] [VARCHAR](50) NOT NULL
        ,[StartDate] [DATETIME] NOT NULL
        ,[EndDate] [DATETIME] NOT NULL
        ,[TotalUnits] [DECIMAL](18, 2) NOT NULL
        ,[TotalCost] [DECIMAL](18, 2) NOT NULL
        ,[CommodityKey] [VARCHAR](20) NULL
        ,[UomKey] [VARCHAR](50) NOT NULL
        ,[BillDays] [INT] NOT NULL
        ,[UtilityBillRecordId] [VARCHAR](64) NULL
        ,[CanceledUtilityBillRecordId] [VARCHAR](64) NULL
        );
END;
GO
