﻿

/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  StoredProcedure [dbo].[uspBDGetBills]    Script Date: 5/15/2017 9:16:30 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspBDGetBills]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[uspBDGetBills]
GO

/****** Object:  StoredProcedure [dbo].[uspBDGetBills]    Script Date: 5/15/2017 9:16:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspBDGetBills]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[uspBDGetBills] AS'
END
GO

-- =============================================
-- Author:      MGanley
-- Create date: 3/10/2015
-- Description: Get billing data for BillDisagg
--      CustomerId = 'GUL47191'
--      AccountId = 'ZUW26449'
--      PremiseId = 'RIV9449'
-- =============================================
ALTER PROCEDURE [dbo].[uspBDGetBills]
    @ClientID INT
   ,@CustomerID VARCHAR(50)
   ,@AccountID VARCHAR(50)
   ,@PremiseID VARCHAR(50)
AS
    BEGIN

        DECLARE @premiseKey VARCHAR(50);

        SELECT  @premiseKey = [p].[PremiseKey]
        FROM    [dbo].[DimPremise] [p] WITH (NOLOCK)
                INNER JOIN [dbo].[FactCustomerPremise] [cp] WITH (NOLOCK) ON [cp].[PremiseKey] = [p].[PremiseKey]
                INNER JOIN [dbo].[DimCustomer] [c] WITH (NOLOCK) ON [c].[CustomerKey] = [cp].[CustomerKey]
                INNER JOIN [dbo].[DimClient] [r] WITH (NOLOCK) ON [r].[ClientKey] = [c].[ClientKey]
        WHERE   [r].[ClientId] = @ClientID
                AND [c].[CustomerId] = @CustomerID
                AND [p].[AccountId] = @AccountID
                AND [p].[PremiseId] = @PremiseID;

        SET NOCOUNT ON;

        WITH    [b]
                  AS (SELECT    [sc].[PremiseKey]
                               ,[f].[PremiseId]
                               ,[f].[StartDate]
                               ,[f].[EndDate]
                               ,[f].[TotalUsage] AS [totalunits]
                               ,[f].[CostOfUsage] AS [totalcost]
                               ,[sc].[CommodityKey]
                               ,[f].[UOMKey]
                               ,[f].[BillDays]
                               ,[f].[UtilityBillRecordId]
                               ,[f].[CanceledUtilityBillRecordId]
                      FROM      [dbo].[FactServicePointBilling] [f] WITH (NOLOCK)
                                INNER JOIN [dbo].[DimServiceContract] [sc] WITH (NOLOCK) ON [sc].[ServiceContractKey] = [f].[ServiceContractKey]
                      WHERE     [sc].[PremiseKey] = @premiseKey
                     )
            SELECT TOP 100
                    [b].[PremiseKey]
                   ,MAX([b].[PremiseId]) AS [PremiseId]
                   ,[b].[StartDate]
                   ,[b].[EndDate]
                   ,SUM([b].[totalunits]) AS [totalunits]
                   ,SUM([b].[totalcost]) AS [totalcost]
                   ,[co].[CommodityDesc] AS [CommodityKey]
                   ,[u].[UOMDesc] AS [UomKey]
                   ,[b].[BillDays]
                   ,[b].[UtilityBillRecordId]
                   ,[b].[CanceledUtilityBillRecordId]
            FROM    [b]
                    INNER JOIN [dbo].[DimCommodity] [co] WITH (NOLOCK) ON [co].[CommodityKey] = [b].[CommodityKey]
                    INNER JOIN [dbo].[DimUOM] [u] WITH (NOLOCK) ON [u].[UOMKey] = [b].[UOMKey]
            GROUP BY [b].[PremiseKey]
                    ,[b].[StartDate]
                   ,[b].[EndDate]
                   ,[co].[CommodityDesc]
                   ,[u].[UOMDesc]
                   ,[b].[BillDays]
                   ,[b].[UtilityBillRecordId]
                   ,[b].[CanceledUtilityBillRecordId]
            ORDER BY [b].[EndDate] DESC;


    END;






GO
