﻿

/**************************************/
/***** RUN ON THE INSIGHTS DATABASE ***/
/**************************************/

/****** Object:  Table [wh].[Bill]    Script Date: 5/15/2017 9:54:33 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[wh].[Bill]') AND type in (N'U'))
    DROP TABLE [wh].[Bill]
GO

/****** Object:  Table [wh].[Bill]    Script Date: 5/15/2017 9:54:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS ( SELECT  *
                FROM    [sys].[objects]
                WHERE   [object_id] = OBJECT_ID(N'[wh].[Bill]')
                        AND [type] IN (N'U') )
BEGIN
    CREATE TABLE [wh].[Bill]
        (
         [ClientId] [INT] NOT NULL
        ,[CustomerID] [VARCHAR](50) NOT NULL
        ,[AccountID] [VARCHAR](50) NOT NULL
        ,[PremiseID] [VARCHAR](50) NOT NULL
        ,[StartDate] [DATETIME] NOT NULL
        ,[EndDate] [DATETIME] NOT NULL
        ,[CommodityKey] [VARCHAR](50) NOT NULL
        ,[TotalUnits] [DECIMAL](18, 6) NULL
        ,[TotalCost] [DECIMAL](18, 6) NULL
        ,[UomKey] [VARCHAR](16) NULL
        ,[BillDays] [INT] NULL
        ,[UtilityBillRecordId] [VARCHAR](64) NULL
        ,[CanceledUtilityBillRecordId] [VARCHAR](64) NULL
        ,CONSTRAINT [PK_Bill] PRIMARY KEY CLUSTERED
            ([ClientId] ASC, [CustomerID] ASC, [AccountID] ASC, [PremiseID] ASC, [EndDate] ASC, [CommodityKey] ASC)
            WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
        );
END;
GO
