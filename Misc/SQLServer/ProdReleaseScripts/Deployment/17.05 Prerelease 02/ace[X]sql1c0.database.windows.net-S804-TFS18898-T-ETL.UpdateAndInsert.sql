﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[DF__T1U_FactB__IsFau__4DAA618B]') AND type = 'D')
BEGIN
        ALTER TABLE [ETL].[T1U_FactBilling] DROP CONSTRAINT [DF__T1U_FactB__IsFau__4DAA618B]
END

GO

/****** Object:  Table [ETL].[T1U_FactBilling]    Script Date: 5/13/2017 4:28:29 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[T1U_FactBilling]') AND type in (N'U'))
        DROP TABLE [ETL].[T1U_FactBilling]
GO

/****** Object:  Table [ETL].[INS_FactBilling]    Script Date: 5/13/2017 4:28:29 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[INS_FactBilling]') AND type in (N'U'))
        DROP TABLE [ETL].[INS_FactBilling]
GO

/****** Object:  Table [ETL].[INS_FactBilling]    Script Date: 5/13/2017 4:28:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[INS_FactBilling]') AND type in (N'U'))
BEGIN
CREATE TABLE [ETL].[INS_FactBilling](
    [ClientId] [int] NOT NULL,
    [PremiseId] [varchar](50) NOT NULL,
    [AccountId] [varchar](50) NOT NULL,
    [ServiceContractId] [varchar](64) NOT NULL,
    [StartDate] [datetime] NOT NULL,
    [EndDate] [datetime] NOT NULL,
    [CommodityId] [int] NOT NULL,
    [BillPeriodTypeId] [int] NOT NULL,
    [BillCycleScheduleId] [varchar](50) NULL,
    [UOMId] [int] NOT NULL,
    [SourceId] [int] NOT NULL,
    [TotalUnits] [decimal](18, 2) NOT NULL,
    [TotalCost] [decimal](18, 2) NOT NULL,
    [BillDays] [int] NOT NULL,
    [RateClass] [varchar](256) NULL,
    [ReadQuality] [varchar](50) NULL,
    [ReadDate] [datetime] NULL,
    [DueDate] [date] NULL,
    [ETL_LogId] [int] NOT NULL,
    [TrackingId] [varchar](50) NOT NULL,
    [TrackingDate] [datetime] NOT NULL,
    [BillingCostDetailsKey] [uniqueidentifier] NULL,
    [ServiceCostDetailsKey] [uniqueidentifier] NULL,
    [UtilityBillRecordId] [VARCHAR](64) NULL,
    [CanceledUtilityBillRecordId] [VARCHAR](64) NULL,
 CONSTRAINT [PK_INS_FactBilling] PRIMARY KEY CLUSTERED
(
    [ClientId] ASC,
    [PremiseId] ASC,
    [AccountId] ASC,
    [ServiceContractId] ASC,
    [EndDate] ASC,
    [CommodityId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO


/****** Object:  Table [ETL].[T1U_FactBilling]    Script Date: 5/13/2017 4:28:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[T1U_FactBilling]') AND type in (N'U'))
BEGIN
        CREATE TABLE [ETL].[T1U_FactBilling](
            [ServiceContractKey] [int] NOT NULL,
            [BillPeriodStartDateKey] [int] NOT NULL,
            [BillPeriodEndDateKey] [int] NOT NULL,
            [CommodityKey] [int] NOT NULL,
            [SourceId] [int] NOT NULL,
            [ClientID] [int] NOT NULL,
            [TotalUnits] [decimal](18, 2) NOT NULL,
            [TotalCost] [decimal](18, 2) NOT NULL,
            [StartDate] [datetime] NOT NULL,
            [BillDays] [int] NOT NULL,
            [RateClass] [varchar](256) NULL,
            [ReadQuality] [varchar](50) NULL,
            [ReadDate] [datetime] NULL,
            [DueDate] [date] NULL,
            [ETL_LogId] [int] NOT NULL,
            [TrackingId] [varchar](50) NOT NULL,
            [TrackingDate] [datetime] NOT NULL,
            [IsFault] [bit] NOT NULL,
            [BillingCostDetailsKey] [uniqueidentifier] NULL,
            [ServiceCostDetailsKey] [uniqueidentifier] NULL,
            [UtilityBillRecordId] [VARCHAR](64) NULL,
            [CanceledUtilityBillRecordId] [VARCHAR](64) NULL,
         CONSTRAINT [PK_T1U_FactBilling] PRIMARY KEY CLUSTERED
        (
            [ServiceContractKey] ASC,
            [BillPeriodStartDateKey] ASC,
            [BillPeriodEndDateKey] ASC
        )WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
        )
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[DF__T1U_FactB__IsFau__4DAA618B]') AND type = 'D')
BEGIN
ALTER TABLE [ETL].[T1U_FactBilling] ADD  DEFAULT ((0)) FOR [IsFault]
END

GO
