﻿

/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[I_BillsToDisAggQueue]    Script Date: 5/15/2017 8:40:18 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[I_BillsToDisAggQueue]') AND type in (N'P', N'PC'))
        DROP PROCEDURE [ETL].[I_BillsToDisAggQueue]
GO

/****** Object:  StoredProcedure [ETL].[I_BillsToDisAggQueue]    Script Date: 5/15/2017 8:40:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[I_BillsToDisAggQueue]') AND type in (N'P', N'PC'))
BEGIN
        EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[I_BillsToDisAggQueue] AS'
END
GO


/*
-- =============================================
-- Author:      Wayne
-- Create date: 3/30/2015
--
-- =============================================
*/

ALTER PROCEDURE [ETL].[I_BillsToDisAggQueue] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        MERGE INTO [Holding].[DisAggReCalcQueue] AS [T]
        USING
            (SELECT [a].[ClientId]
                   ,[a].[PremiseId]
                   ,[a].[AccountId]
                   ,[a].[CustomerId]
                   ,[a].[SourceId]
                   ,[a].[TrackingId]
                   ,[a].[TrackingDate]
                   ,[a].[UtilityBillRecordId]
                   ,[a].[CanceledUtilityBillRecordId]
                   ,[a].[latest]
             FROM   (SELECT DISTINCT
                            [dp].[PremiseKey]
                           ,[dp].[ClientId]
                           ,[dp].[PremiseId]
                           ,[dp].[AccountId]
                           ,[c].[CustomerId]
                           ,[kfb].[SourceId]
                           ,[kfb].[TrackingId]
                           ,[kfb].[TrackingDate]
                           ,[kfb].[UtilityBillRecordId]
                           ,[kfb].[CanceledUtilityBillRecordId]
                           ,ROW_NUMBER() OVER (PARTITION BY [dp].[PremiseId], [dp].[AccountId], [c].[CustomerId] ORDER BY [kfb].[TrackingDate] DESC) AS [latest]
                     FROM   [ETL].[KEY_FactBilling] [kfb]
                            INNER JOIN [dbo].[DimPremise] [dp] ON [dp].[ClientId] = [kfb].[ClientId]
                                                                  AND [dp].[PremiseId] = [kfb].[PremiseId]
                                                                  AND [dp].[AccountId] = [kfb].[AccountId]
                            INNER JOIN (SELECT  [fcp].[PremiseKey]
                                               ,[fcp].[CustomerKey]
                                               ,ROW_NUMBER() OVER (PARTITION BY [fcp].[PremiseKey] ORDER BY [fcp].[DateCreated] DESC) AS [rnk]
                                        FROM    [dbo].[FactCustomerPremise] [fcp]
                                       ) AS [fcp] ON [dp].[PremiseKey] = [fcp].[PremiseKey]
                                                     AND [fcp].[rnk] = 1
                            INNER JOIN [dbo].[DimCustomer] [c] ON [c].[CustomerKey] = [fcp].[CustomerKey]
                                                                  AND [fcp].[rnk] = 1
                     WHERE  [kfb].[ClientId] = @ClientID
                    ) AS [a]
             WHERE  [a].[latest] = 1
            ) AS [s]
        ON [s].[ClientId] = [T].[ClientId]
            AND [s].[PremiseId] = [T].[PremiseId]
            AND [s].[AccountId] = [T].[AccountId]
        WHEN MATCHED THEN
            UPDATE SET
                    [T].[SourceId] = [s].[SourceId]
                   ,[T].[TrackingId] = [s].[TrackingId]
                   ,[T].[TrackingDate] = [s].[TrackingDate]
        WHEN NOT MATCHED THEN
            INSERT (
                    [ClientId]
                   ,[PremiseId]
                   ,[AccountId]
                   ,[CustomerId]
                   ,[SourceId]
                   ,[TrackingId]
                   ,[TrackingDate]
                   ,[UtilityBillRecordId]
                   ,[CanceledUtilityBillRecordId]
                   )
            VALUES (
                    [s].[ClientId]
                   ,[s].[PremiseId]
                   ,[s].[AccountId]
                   ,[s].[CustomerId]
                   ,[s].[SourceId]
                   ,[s].[TrackingId]
                   ,[s].[TrackingDate]
                   ,[s].[UtilityBillRecordId]
                   ,[s].[CanceledUtilityBillRecordId]
                   );

    END;




GO
