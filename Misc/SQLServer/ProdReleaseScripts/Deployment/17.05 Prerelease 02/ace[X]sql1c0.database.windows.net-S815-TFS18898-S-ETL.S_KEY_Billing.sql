﻿

/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[S_KEY_Billing]    Script Date: 5/16/2017 10:34:04 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_KEY_Billing]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [ETL].[S_KEY_Billing]
GO

/****** Object:  StoredProcedure [ETL].[S_KEY_Billing]    Script Date: 5/16/2017 10:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_KEY_Billing]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[S_KEY_Billing] AS'
END
GO

/****** Object:  StoredProcedure [ETL].[S_KEY_Billing]    Script Date: 5/17/2017 12:06:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 9/19/2014
-- Description:

-- 01/10/2017 -- Phil Victor -- Added CostDetailsKey
-- =============================================
ALTER PROCEDURE [ETL].[S_KEY_Billing] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        DELETE [hb1]
        FROM    [Holding].[Billing] [hb1]
                INNER JOIN [Holding].[Billing] [hb2] ON [hb1].[UtilityBillRecordId] = [hb2].[CanceledUtilityBillRecordId]
        WHERE  ([hb1].[UtilityBillRecordId] IS NOT NULL AND Len([hb1].[UtilityBillRecordId]) > 0)
                    AND ( [hb2].[CanceledUtilityBillRecordId] IS NOT NULL AND Len([hb2].[CanceledUtilityBillRecordId]) > 0);

        SELECT  [b].[ClientId]
               ,[b].[AccountId]
               ,[b].[PremiseId]
               ,[b].[CustomerId]
               ,[b].[ServiceContractId]
               ,[b].[ServicePointId]
               ,[b].[MeterId]
               ,[b].[MeterType]
               ,[b].[ReplacedMeterId]
               ,[b].[RateClass]
               ,[b].[StartDate]
               ,[b].[EndDate]
               ,[b].[CommodityId]
               ,[b].[AMIStartDate]
               ,[b].[AMIEndDate]
               ,[b].[SourceId]
               ,[b].[TrackingId]
               ,[b].[TrackingDate]
               ,[b].[BillingCostDetailsKey]
               ,[b].[ServiceCostDetailsKey]
               ,[b].[UtilityBillRecordId]
               ,[b].[CanceledUtilityBillRecordId]
        FROM    (SELECT [ClientId]
                       ,[AccountId]
                       ,[PremiseId]
                       ,[CustomerId]
                       ,[ServiceContractId]
                       ,[ServicePointId]
                       ,[MeterId]
                       ,[MeterType]
                       ,[ReplacedMeterId]
                       ,[RateClass]
                       ,[BillStartDate] AS [StartDate]
                       ,[BillEndDate] [EndDate]
                       ,[BillDays]
                       ,[TotalUnits]
                       ,[TotalCost]
                       ,[CommodityId]
                       ,[AMIStartDate]
                       ,[AMIEndDate]
                       ,[SourceId]
                       ,[TrackingId]
                       ,[TrackingDate]
                       ,[BillingCostDetailsKey]
                       ,[ServiceCostDetailsKey]
                       ,[UtilityBillRecordId]
                       ,[CanceledUtilityBillRecordId]
                       ,ROW_NUMBER() OVER (PARTITION BY [ClientId], [AccountId], [PremiseId], [CustomerId], [ServiceContractId],
                                           [BillEndDate], [CommodityId] ORDER BY [TrackingDate] DESC) AS [SortId]
                 FROM   [Holding].[Billing]
                 WHERE  [ClientId] = @ClientID
                ) [b]
        WHERE   [b].[SortId] = 1
        ORDER BY [b].[ClientId]
               ,[b].[PremiseId]
               ,[b].[AccountId]
               ,[b].[CustomerId]
               ,[b].[ServiceContractId]
               ,[b].[EndDate]
               ,[b].[CommodityId];


    END;


