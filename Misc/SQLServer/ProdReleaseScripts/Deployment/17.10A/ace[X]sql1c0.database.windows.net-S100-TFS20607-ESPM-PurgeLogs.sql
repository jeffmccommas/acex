USE [Insights]
GO

INSERT into portal.portalconfig (PortalConfigName,IsEnabled,PortalConfigTypeId) 
values ('RETENTIONPERIOD',1,1)
Go

insert into portal.clientportalconfig (ClientId,PortalConfigId,PortalConfigValue,IsEnabled)
values('224',3,'60',1)
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ( '[dbo].[PurgeESPMLogs]', 'P' ) IS NOT NULL   
    DROP PROCEDURE [dbo].[PurgeESPMLogs];  
GO 
GO

CREATE PROCEDURE [dbo].[PurgeESPMLogs]
AS
Declare @EndPointRetention int
;With Clients(ClientId, RetentionPeriod)--Column names for CTE, which are optional
AS
(
SELECT 
    CPC.ClientId,
	CAST(CPC.PortalConfigValue AS INT)   
FROM 
     [portal].[ClientPortalConfig] CPC 
INNER JOIN [portal].[PortalConfig] PC on PC.PortalConfigId = CPC.PortalConfigId 
WHERE 
    PC.PortalConfigName  = 'RETENTIONPERIOD'
	AND PC.IsEnabled = 1 
	AND CPC.IsEnabled = 1
)

DELETE ET FROM [portal].[EndpointTracking] ET 
INNER JOIN Clients C on C.ClientId = ET.CLientId
where ET.LogDate < DateAdd(d,C.RetentionPeriod*-1,GETDATE())

SELECT @EndPointRetention = CAST(Min(CPC.PortalConfigValue) AS INT)
FROM 
[portal].[ClientPortalConfig] CPC 
INNER JOIN [portal].[PortalConfig] PC on PC.PortalConfigId = CPC.PortalConfigId 
WHERE 
    PC.PortalConfigName  = 'RETENTIONPERIOD'
	AND PC.IsEnabled = 1 
	AND CPC.IsEnabled = 1

DELETE EL FROM [portal].[EventLog] EL 
where EL.LogDate < DateAdd(d,@EndPointRetention*-1,GETDATE())

GO
