/********************************************************/
/***** RUN ONLY ON THE INSIGHTSMETADATA DATABASE ***/
/********************************************************/

/****** Object:  StoredProcedure [cm].[uspEMSelectDailyWeatherDetailForBills]    Script Date: 9/13/2017 11:51:14 AM ******/
DROP PROCEDURE [cm].[uspEMSelectDailyWeatherDetailForBills]
GO

/****** Object:  UserDefinedTableType [cm].[AccountBillAmountsTable]    Script Date: 9/13/2017 11:50:24 AM ******/
DROP TYPE [cm].[AccountBillAmountsTable]
GO

/****** Object:  UserDefinedTableType [cm].[AccountBillAmountsTable]    Script Date: 9/13/2017 11:50:24 AM ******/
CREATE TYPE [cm].[AccountBillAmountsTable] AS TABLE(
	[AccountId] [VARCHAR](50) NOT NULL,
	[PremiseId] [VARCHAR](50) NOT NULL,
	[ServiceId] [VARCHAR](50) NOT NULL,
	[BillDate] [VARCHAR](50) NOT NULL,
	[BillStartDate] [VARCHAR](50) NOT NULL,
	[BillEndDate] [VARCHAR](50) NOT NULL,
	[CommodityKey] [VARCHAR](50) NOT NULL,
	[UOMKey] [VARCHAR](50) NOT NULL,
	[BillDays] [INT] NOT NULL,
	[TotalServiceUse] [DECIMAL](18, 2) NOT NULL,
	[CostofUsage] [DECIMAL](18, 2) NOT NULL,
	[AdditionalServiceCost] [DECIMAL](18, 2) NOT NULL,
	[ZipCode] [VARCHAR](16) NOT NULL,
	[AvgTemp] [INT] NOT NULL,
	[RateClass] [VARCHAR](100) NULL,
	[ServiceCostDetailsKey] [UNIQUEIDENTIFIER] NULL,
	PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC,
	[PremiseId] ASC,
	[ServiceId] ASC,
	[BillEndDate] ASC,
	[CommodityKey] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO


