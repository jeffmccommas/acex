﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [dbo].[GetBillSummary]    Script Date: 9/6/2017 5:08:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Updated By:      Muazzam Ali
-- Update date: 9/6/2017
-- Description: Adding new field - [dbo].[FactServicePointBilling].BillDate
-- =============================================
ALTER PROCEDURE [dbo].[GetBillSummary]
    @ClientID AS INT ,
    @CustomerID AS VARCHAR(50) ,
    @AccountID AS VARCHAR(50) = NULL ,
    @StartDate AS DATE ,
    @EndDate AS DATE ,
    @Count AS INT = 1
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @defaultZipCode AS VARCHAR(9);

        EXEC [cm].[uspSelectContentClientConfiguration] @ClientID = @ClientID,
            @configurationKey = N'defaultzipcode',
            @configurationValue = @defaultZipCode OUTPUT;

		-- Bill Customer
        SELECT  c.CustomerId AS Id ,
                c.FirstName ,
                c.LastName
        FROM    dbo.DimCustomer c WITH ( NOLOCK )
                INNER JOIN dbo.DimClient r WITH ( NOLOCK ) ON r.ClientKey = c.ClientKey
        WHERE   r.ClientId = @ClientID
                AND c.CustomerId = @CustomerID;

        SELECT	DISTINCT
                p.PremiseKey ,
                p.AccountId ,
                p.PremiseId ,
                SUBSTRING(ISNULL(NULLIF(p.PostalCode,''), @defaultZipCode), 1, 5) AS ZipCode
        INTO    #CustomerInfo
        FROM    dbo.DimPremise p WITH ( NOLOCK )
                INNER JOIN dbo.FactCustomerPremise cp WITH ( NOLOCK ) ON cp.PremiseKey = p.PremiseKey
                INNER JOIN dbo.DimCustomer c WITH ( NOLOCK ) ON c.CustomerKey = cp.CustomerKey
                INNER JOIN dbo.DimClient r WITH ( NOLOCK ) ON r.ClientKey = c.ClientKey
        WHERE   r.ClientId = @ClientID
                AND c.CustomerId = @CustomerID
                AND ( @AccountID IS NULL
                      OR p.AccountId = @AccountID
                    );

		CREATE CLUSTERED INDEX IX_CustomerInfo_PremiseKey ON [#CustomerInfo]([PremiseKey]);
		CREATE INDEX IX_UC_CustomerInfo_AccountId ON [#CustomerInfo]([AccountId]);
		CREATE INDEX IX_UC_CustomerInfo_PremiseKeyAccountId ON [#CustomerInfo]([PremiseKey],[AccountId]);

		-- Bill Account
        SELECT	DISTINCT
                AccountId AS Id
        FROM    #CustomerInfo;

		--insert calculation of total bill amount into the temp table
		--9/6/2017 Added ISNULL check for BillDate. If fb.BillDate is null then use fb.EndDate
	
        SELECT  *
        INTO    #AccountBillAmounts
        FROM    ( SELECT    c.AccountId ,
                            fb.PremiseId ,
                            sc.ServiceContractId AS ServiceId ,
                            CONVERT(DATE, ISNULL(fb.BillDate, fb.EndDate)) AS BillDate,
                            CONVERT(DATE, DATEADD(DAY, -1 * fb.BillDays,
                                                  ISNULL(fb.ReadDate,
                                                         fb.EndDate))) AS BillStartDate ,
                            CONVERT(DATE, ISNULL(fb.ReadDate, fb.EndDate)) AS BillEndDate ,
                            fb.DueDate AS BillDueDate ,
                            f.CommodityDesc AS CommodityKey ,
                            uom.UOMDesc AS UOMKey ,
							r.RateClassDescription AS RateClass,
                            fb.BillDays ,
                            fb.TotalUsage AS TotalServiceUse ,
                            fb.CostOfUsage ,
                            ISNULL(fb.OtherCost, 0.00) AS AdditionalServiceCost ,
                            c.ZipCode ,
                            bpt.BillPeriodTypeDesc AS BillFrequency ,
							fb.BillingCostDetailsKey,
							fb.ServiceCostDetailsKey,
                            ROW_NUMBER() OVER ( PARTITION BY fb.ServiceContractKey ORDER BY d.FullDateAlternateKey DESC ) AS RowIndex
                  FROM      #CustomerInfo c WITH ( NOLOCK )
                            INNER JOIN dbo.DimServiceContract sc WITH ( NOLOCK ) ON sc.PremiseKey = c.PremiseKey
                            INNER JOIN dbo.FactServicePointBilling fb WITH ( NOLOCK ) ON fb.ServiceContractKey = sc.ServiceContractKey
                            INNER JOIN dbo.DimDate d WITH ( NOLOCK ) ON d.DateKey = fb.BillPeriodEndDateKey
                            INNER JOIN dbo.DimBillPeriodType bpt WITH ( NOLOCK ) ON bpt.BillPeriodTypeKey = fb.BillPeriodTypeKey
                            INNER JOIN dbo.DimUOM uom WITH ( NOLOCK ) ON fb.UOMKey = uom.UOMKey
                            INNER JOIN dbo.DimCommodity f WITH ( NOLOCK ) ON f.CommodityKey = sc.CommodityKey
							INNER JOIN dbo.DimRateClass r WITH (NOLOCK) ON fb.RateClassKey1 = r.RateClassKey 
                  WHERE     d.FullDateAlternateKey BETWEEN @StartDate
                                                   AND     @EndDate
                ) AS Bill
        WHERE   Bill.RowIndex <= @Count
        ORDER BY Bill.AccountId ,
                Bill.PremiseId ,
                Bill.ServiceId ,
                Bill.BillEndDate;

		CREATE INDEX IX1_UC_AccountBillAmounts ON [#AccountBillAmounts]([AccountId],[PremiseId],[ServiceId],[BillDueDate]);
		CREATE INDEX IX2_UC_AccountBillAmounts ON	[#AccountBillAmounts]([AccountId],[PremiseId],[BillDate],[BillFrequency]);
		CREATE INDEX IX3_UC_AccountBillAmounts ON	[#AccountBillAmounts]([AccountId],[PremiseId],[BillDate],[CommodityKey]);
    	 
		-- Bill - doing a sum of totalcost for account and year/month of the bill date. Max bill date is the latest bill date for this account on the year/month
        SELECT  AccountId ,
                PremiseId ,
                FORMAT(BillDate, 'yyyy-MM-dd') AS BillDate ,
                FORMAT(MIN(BillDueDate), 'yyyy-MM-dd') AS BillDueDate ,
                SUM(CostOfUsage) AS TotalAmount ,
                BillFrequency ,
                SUM(AdditionalServiceCost) AS AdditionalBillCost
        FROM    #AccountBillAmounts
        GROUP BY AccountId ,
                PremiseId ,
                BillDate ,
                BillFrequency
        ORDER BY AccountId ,
                PremiseId ,
                BillDate DESC ,
                BillFrequency;

		---- Bill Premise 
		----NOTE that even if startdate and end date don't have billing entries for this premise - 
		----it will still bring back the information for a premise and account for this customer
        SELECT  p.AccountId ,
                p.PremiseId ,
                p.Street1 AS Addr1 , --Addr1
                p.Street2 AS Addr2 , --Addr2
                p.City ,
                p.StateProvince AS State ,
                p.PostalCode AS Zip
        FROM    dbo.DimPremise p WITH ( NOLOCK )
                INNER JOIN #CustomerInfo c ON c.PremiseKey = p.PremiseKey;

        WITH    Temps
                  AS ( SELECT   ba.AccountId ,
                                ba.PremiseId ,
                                ba.ServiceId ,
                                ba.BillDate
                       FROM     #AccountBillAmounts ba
                                LEFT JOIN cm.EMZipcode zc ON zc.ZipCode = ba.ZipCode
                       GROUP BY ba.AccountId ,
                                ba.PremiseId ,
                                ba.ServiceId ,
                                ba.BillDate
                     )
            ---- Bill Service
    SELECT  ba.AccountId ,
            ba.PremiseId ,
            ba.ServiceId ,
            FORMAT(ba.BillDate, 'yyyy-MM-dd') AS BillDate ,
            FORMAT(ba.BillStartDate, 'yyyy-MM-dd') AS BillStartDate ,
            FORMAT(ba.BillEndDate, 'yyyy-MM-dd') AS BillEndDate ,
            CommodityKey ,
            UOMKey ,
			RateClass ,
            BillDays ,
            TotalServiceUse ,
            CostOfUsage ,
            AdditionalServiceCost ,
			ba.ZipCode,
			0 As AvgTemp,
			ba.ServiceCostDetailsKey,
			ba.BillingCostDetailsKey,
            ROW_NUMBER() OVER ( ORDER BY ba.AccountId, ba.PremiseId ) AS RowIdentifier ,
            RowIndex
    FROM    #AccountBillAmounts ba
            INNER JOIN Temps t ON t.AccountId = ba.AccountId
                                  AND t.PremiseId = ba.PremiseId
                                  AND t.ServiceId = ba.ServiceId
                                  AND t.BillDate = ba.BillDate
    ORDER BY ba.AccountId ,
            ba.PremiseId ,
            ba.BillDate DESC ,
            ba.CommodityKey;
		
		--- Service Cost Detail
	SELECT    ba.AccountId, ba.PremiseId, FORMAT(ba.BillDate, 'yyyy-MM-dd') AS BillDate, ba.ServiceId, ba.CommodityKey, fscd.*
	FROM     #AccountBillAmounts ba
	INNER JOIN dbo.FactServicePointBilling_ServiceCostDetails fscd WITH (NOLOCK) ON ba.ServiceCostDetailsKey = fscd.ServiceCostDetailsKey
	ORDER BY ba.AccountId, ba.PremiseId, ba.BillDate DESC, ba.CommodityKey

		--- Bill  Cost Detail
	SELECT  DISTINCT ba.AccountId, FORMAT(ba.BillDate, 'yyyy-MM-dd') AS BillDate, fbcd.BillingCostDetailName, fbcd.BillingCostDetailValue
	FROM #AccountBillAmounts ba 
	INNER JOIN dbo.FactServicePointBilling_BillCostDetails fbcd WITH (NOLOCK) ON ba.BillingCostDetailsKey = fbcd.BillingCostDetailsKey
	ORDER BY ba.AccountId, FORMAT(ba.BillDate, 'yyyy-MM-dd') DESC

        DROP TABLE #AccountBillAmounts;
        DROP TABLE #CustomerInfo;
    END;
