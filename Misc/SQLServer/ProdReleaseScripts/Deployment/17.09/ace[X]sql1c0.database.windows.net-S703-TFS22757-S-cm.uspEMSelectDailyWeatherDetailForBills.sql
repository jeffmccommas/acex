﻿/*    ==Scripting Parameters==

    Source Database Engine Edition : Microsoft Azure SQL Database Edition
    Source Database Engine Type : Microsoft Azure SQL Database

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [InsightsMetaData]
GO
/****** Object:  StoredProcedure [cm].[uspEMSelectDailyWeatherDetailForBills]    Script Date: 9/20/2017 1:51:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





ALTER PROCEDURE [cm].[uspEMSelectDailyWeatherDetailForBills]
	@AccountBills AccountBillAmountsTable READONLY
AS
    BEGIN

		WITH    Temps
			AS ( SELECT ba.AccountId,
						ba.PremiseId,
						ba.ServiceId,
						ba.BillEndDate,
						AVG(wd.AvgTemp) AS AverageTemp
				FROM @AccountBills ba
						LEFT JOIN cm.EMZipcode zc ON zc.ZipCode = LEFT(ba.ZipCode,5)
						LEFT JOIN cm.EMDailyWeatherDetail wd ON wd.StationID = zc.StationIdDaily
									               AND wd.WeatherReadingDate >= ba.BillStartDate
									               AND wd.WeatherReadingDate < ba.BillEndDate
				GROUP BY ba.AccountId,
						ba.PremiseId,
						ba.ServiceId,
						ba.BillEndDate
				)
		SELECT  ba.AccountId,
				ba.PremiseId,
				ba.ServiceId,
				ba.BillDate,
				ba.BillStartDate,
				ba.BillEndDate,
				CommodityKey,
				UOMKey,
				BillDays,
				TotalServiceUse,
				CostOfUsage,
				AdditionalServiceCost ,
				ba.ZipCode,
				ISNULL(AverageTemp,0.00) AS AvgTemp,
				ba.RateClass,
				ba.ServiceCostDetailsKey,
				ROW_NUMBER() OVER ( ORDER BY ba.AccountId, ba.PremiseId ) AS RowIdentifier
		FROM    @AccountBills ba
				INNER JOIN Temps t ON t.AccountId = ba.AccountId
									  AND t.PremiseId = ba.PremiseId
									  AND t.ServiceId = ba.ServiceId
									  AND t.BillEndDate = ba.BillEndDate
		ORDER BY ba.AccountId,
				ba.PremiseId,
				ba.BillDate DESC,
				ba.CommodityKey;

    END





