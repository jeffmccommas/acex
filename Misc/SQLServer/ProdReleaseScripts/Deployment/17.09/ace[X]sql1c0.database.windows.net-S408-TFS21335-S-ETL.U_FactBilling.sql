﻿


/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[S_KEY_Billing]    Script Date: 8/28/2017 3:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 9/19/2014
-- Description:

-- 01/10/2017 -- Phil Victor -- Added CostDetailsKey
-- 08/29/2017 -- Phil Victor -- Added BillDate
-- =============================================
ALTER PROCEDURE [ETL].[S_KEY_Billing] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        -- Remove duplicates in Holding table based on Bill Record Ids.
        DELETE [hb1]
        FROM    [Holding].[Billing] [hb1]
                INNER JOIN [Holding].[Billing] [hb2] ON [hb1].[UtilityBillRecordId] = [hb2].[CanceledUtilityBillRecordId]
        WHERE  ([hb1].[UtilityBillRecordId] IS NOT NULL AND Len([hb1].[UtilityBillRecordId]) > 0)
                    AND ( [hb2].[CanceledUtilityBillRecordId] IS NOT NULL AND Len([hb2].[CanceledUtilityBillRecordId]) > 0);

        -- Get the data.
        SELECT  [b].[ClientId]
               ,[b].[AccountId]
               ,[b].[PremiseId]
               ,[b].[CustomerId]
               ,[b].[ServiceContractId]
               ,[b].[ServicePointId]
               ,[b].[MeterId]
               ,[b].[MeterType]
               ,[b].[ReplacedMeterId]
               ,[b].[RateClass]
               ,[b].[StartDate]
               ,[b].[EndDate]
               ,[b].[CommodityId]
               ,[b].[AMIStartDate]
               ,[b].[AMIEndDate]
               ,[b].[SourceId]
               ,[b].[TrackingId]
               ,[b].[TrackingDate]
               ,[b].[BillingCostDetailsKey]
               ,[b].[ServiceCostDetailsKey]
               ,[b].[UtilityBillRecordId]
               ,[b].[CanceledUtilityBillRecordId]
               ,[b].[BillDate]
        FROM    (SELECT [ClientId]
                       ,[AccountId]
                       ,[PremiseId]
                       ,[CustomerId]
                       ,[ServiceContractId]
                       ,[ServicePointId]
                       ,[MeterId]
                       ,[MeterType]
                       ,[ReplacedMeterId]
                       ,[RateClass]
                       ,[BillStartDate] AS [StartDate]
                       ,[BillEndDate] AS [EndDate]
                       ,[BillDays]
                       ,[TotalUnits]
                       ,[TotalCost]
                       ,[CommodityId]
                       ,[AMIStartDate]
                       ,[AMIEndDate]
                       ,[SourceId]
                       ,[TrackingId]
                       ,[TrackingDate]
                       ,[BillingCostDetailsKey]
                       ,[ServiceCostDetailsKey]
                       ,[UtilityBillRecordId]
                       ,[CanceledUtilityBillRecordId]
                       ,[BillDate]
                       ,ROW_NUMBER() OVER (PARTITION BY [ClientId], [PremiseId], [AccountId], [ServiceContractId],
                                           [BillEndDate], [CommodityId] ORDER BY [TrackingDate] DESC) AS [SortId]
                 FROM   [Holding].[Billing]
                 WHERE  [ClientId] = @ClientID
                ) [b]
        WHERE   [b].[SortId] = 1
        ORDER BY [b].[ClientId]
               ,[b].[PremiseId]
               ,[b].[AccountId]
               ,[b].[ServiceContractId]
               ,[b].[EndDate]
               ,[b].[CommodityId];


    END;