IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClientDataPurgeAccountsToKeep]') AND TYPE IN (N'U'))
	-- drop table ClientDataPurgeAccountsToKeep
	create table dbo.ClientDataPurgeAccountsToKeep
	(clientID int,
	PremiseKey int,
	CustomerID varchar(50),
	AccountID varchar(50),
	PremiseID varchar(50))

-- ************************************************
-- table to hold the rows to be archived and purged
-- ************************************************
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[archivePurgeFactPremiseAttribute]') AND TYPE IN (N'U'))
	select * into dbo.archivePurgeFactPremiseAttribute from dbo.FactPremiseAttribute where 1 = 2

-- ************************************************
-- table to hold any exact duplicates 
-- add distinct value back to factpremiseattribute after purge
-- ************************************************
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[archivePurgeFactPremiseAttributeReAdd]') AND TYPE IN (N'U'))
	select * into dbo.archivePurgeFactPremiseAttributeReAdd from dbo.FactPremiseAttribute where 1 = 2

-- ************************************************
-- stored procedures for archiving and cleanup
-- ************************************************
-- sp to pull rows to be archived
if exists (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[archiveFactPremiseAttributeData]') AND TYPE IN (N'P'))
	drop procedure dbo.archiveFactPremiseAttributeData
go

create procedure dbo.archiveFactPremiseAttributeData @clientID int, @environmentkey varchar(20)
as

set nocount on

-- create temp table to hold all duplicates, not all will be deleted
create table #AllDuplicates
(PremiseKey int, 
ContentAttributeKey varchar(150), 
Value varchar(150), 
SourceKey int, 
EffectiveDate Datetime,
RowNum int,
DateOrder int,
bDelete int)
CREATE CLUSTERED INDEX [CIDX_tmpAllDuplicates] ON #AllDuplicates (PremiseKey, ContentAttributeKey, SourceKey, Value, DateOrder, EffectiveDate)

-- get all the rows that meet criteria of being a duplicate
;with dups
as
(
	select premisekey, contentattributekey, sourcekey
	from dbo.FactPremiseAttribute
	group by premisekey, contentattributekey, sourcekey
	having count(premisekey) > 1
)
insert into #AllDuplicates
select t1.PremiseKey, t1.ContentAttributeKey, t1.Value, t1.SourceKey, t1.EffectiveDate, 
	row_number() over (partition by t1.premisekey, t1.sourcekey, t1.contentattributekey, value order by t1.premisekey, t1.contentattributekey, t1.effectivedate asc) as rownum,
	row_number() over (partition by t1.premisekey, t1.contentattributekey order by t1.premisekey, t1.contentattributekey, t1.effectivedate asc) as dateorder,
	0 as bDelete
from [dbo].[FactPremiseAttribute] t1 join dups t2
	on t1.premisekey = t2.premisekey
	and t1.contentattributekey = t2.contentattributekey
	and t1.sourcekey = t2.sourcekey
where not exists (select premisekey from dbo.ClientDataPurgeAccountsToKeep t3
	where t1.premisekey = t3.premisekey)

-- set the delete flag on the temp table if the row is a duplicate premise, contentattributekey, value, sourcekey AND it must be a duplicate of the previous effectivedate
update #AllDuplicates
set bDelete = 1
from #AllDuplicates t1
where rownum > 1
and exists (select * from #AllDuplicates t2
	where t1.premisekey = t2.premisekey
	and t1.contentattributekey = t2.contentattributekey
	and t1.sourcekey = t2.sourcekey
	and t1.value = t2.value
	and t1.dateorder = t2.dateorder + 1)

-- insert into archive table the rows that were flagged bDelete
insert into dbo.archivePurgeFactPremiseAttribute
select t1.*
from dbo.factpremiseattribute t1 join #AllDuplicates t2
	on t1.PremiseKey = t2.PremiseKey
	and t1.ContentAttributeKey = t2.contentattributekey
	and t1.Value = t2.value 
	and t1.SourceKey = t2.sourcekey
	and t1.EffectiveDate = t2.effectivedate
where t2.bDelete = 1

-- if there were any exact duplicates, the distinct row has to be added back to factpremiseattribute after the purge, save to this table
;with ReAdd
as
(
	select t1.premisekey, t1.premiseid, t1.accountid, t1.sourcekey, t1.contentattributekey, t1.value, t1.effectivedate, count(t1.effectivedate) as rcount
	from factpremiseattribute t1 join dbo.archivePurgeFactPremiseAttribute t2
		on t1.PremiseKey = t2.PremiseKey
		and t1.ContentAttributeKey = t2.contentattributekey
		and t1.Value = t2.value 
		and t1.SourceKey = t2.sourcekey
		and t1.EffectiveDate = t2.effectivedate
	group by t1.premisekey, t1.premiseid, t1.accountid, t1.sourcekey, t1.contentattributekey, t1.value, t1.effectivedate
	having count(t1.effectivedate) > 1
)
insert into dbo.archivePurgeFactPremiseAttributeReAdd
select distinct t1.* 
from dbo.archivePurgeFactPremiseAttribute t1 join ReAdd t2
	on t1.PremiseKey = t2.PremiseKey
		and t1.ContentAttributeKey = t2.contentattributekey
		and t1.Value = t2.value 
		and t1.SourceKey = t2.sourcekey
		and t1.EffectiveDate = t2.effectivedate

set nocount off
go

-- sp purged data
if exists (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[purgeFactPremiseAttributeData]') AND TYPE IN (N'P'))
	drop procedure dbo.purgeFactPremiseAttributeData
go

create procedure dbo.purgeFactPremiseAttributeData
as

set nocount on
-- purge data that was just saved
	delete dbo.FactPremiseAttribute
	from dbo.FactPremiseAttribute t1 join dbo.archivePurgeFactPremiseAttribute t2
		on t1.PremiseKey = t2.PremiseKey
		and t1.ContentAttributeKey = t2.contentattributekey
		and t1.Value = t2.value 
		and t1.SourceKey = t2.sourcekey
		and t1.EffectiveDate = t2.effectivedate

	-- add back just 1 distinct row of any of the purged rows that had been exact duplicate of eachother
	insert into dbo.FactPremiseAttribute
	select distinct * from dbo.archivePurgeFactPremiseAttributeReAdd t1
		where not exists (select * from factpremiseattribute t2
			where t1.PremiseKey = t2.PremiseKey
			and t1.ContentAttributeKey = t2.contentattributekey
			and t1.Value = t2.value 
			and t1.SourceKey = t2.sourcekey
			and t1.EffectiveDate = t2.effectivedate)

set nocount off
go
--************************************************************
if exists (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CleanupArchiveFactPremiseAttributeTable]') AND TYPE IN (N'P'))
	drop procedure dbo.CleanupArchiveFactPremiseAttributeTable
go

create procedure dbo.CleanupArchiveFactPremiseAttributeTable
as

set nocount on
	truncate table dbo.archivePurgeFactPremiseAttribute

	truncate table dbo.archivePurgeFactPremiseAttributeReAdd

set nocount off
go
