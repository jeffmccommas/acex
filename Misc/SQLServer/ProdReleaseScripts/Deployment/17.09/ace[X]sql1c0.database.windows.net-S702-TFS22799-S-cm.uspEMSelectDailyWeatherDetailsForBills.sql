/********************************************************/
/***** RUN ONLY ON THE INSIGHTSMETADATA DATABASE ***/
/********************************************************/


/****** Object:  StoredProcedure [cm].[uspEMSelectDailyWeatherDetailForBills]    Script Date: 9/13/2017 11:51:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [cm].[uspEMSelectDailyWeatherDetailForBills]
	@AccountBills AccountBillAmountsTable READONLY
AS
    BEGIN

		WITH    Temps
			AS ( SELECT ba.AccountId,
						ba.PremiseId,
						ba.ServiceId,
						ba.BillDate,
						AVG(wd.AvgTemp) AS AverageTemp
				FROM @AccountBills ba
						LEFT JOIN cm.EMZipcode zc ON zc.ZipCode = LEFT(ba.ZipCode,5)
						LEFT JOIN cm.EMDailyWeatherDetail wd ON wd.StationID = zc.StationIdDaily
									               AND wd.WeatherReadingDate >= ba.BillStartDate
									               AND wd.WeatherReadingDate < ba.BillEndDate
				GROUP BY ba.AccountId,
						ba.PremiseId,
						ba.ServiceId,
						ba.BillDate
				)
		SELECT  ba.AccountId,
				ba.PremiseId,
				ba.ServiceId,
				ba.BillDate,
				ba.BillStartDate,
				ba.BillEndDate,
				CommodityKey,
				UOMKey,
				BillDays,
				TotalServiceUse,
				CostOfUsage,
				AdditionalServiceCost ,
				ba.ZipCode,
				ISNULL(AverageTemp,0.00) AS AvgTemp,
				ba.RateClass,
				ba.ServiceCostDetailsKey,
				ROW_NUMBER() OVER ( ORDER BY ba.AccountId, ba.PremiseId ) AS RowIdentifier
		FROM    @AccountBills ba
				INNER JOIN Temps t ON t.AccountId = ba.AccountId
									  AND t.PremiseId = ba.PremiseId
									  AND t.ServiceId = ba.ServiceId
									  AND t.BillDate = ba.BillDate
		ORDER BY ba.AccountId,
				ba.PremiseId,
				ba.BillDate DESC,
				ba.CommodityKey;

    END





GO


