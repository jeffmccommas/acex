﻿/**************************************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_210 AND INSIGHTS_TEST DATABASE SHARDS ***/
/*************************************************************************/

IF NOT EXISTS (SELECT * FROM [sys].[columns] WHERE [object_id]=OBJECT_ID(N'[Holding].[Billing_Duplicates]')AND [name]='BillDate')
BEGIN
    ALTER TABLE [Holding].[Billing_Duplicates] ADD [BillDate] DATETIME NULL;
END;