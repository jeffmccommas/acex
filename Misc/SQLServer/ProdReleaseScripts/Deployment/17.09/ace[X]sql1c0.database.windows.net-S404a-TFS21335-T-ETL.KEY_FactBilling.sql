﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  Index [IX_Key_FactBilling_UtilityRecordId]    Script Date: 8/29/2017 1:37:51 PM ******/
DROP INDEX IF EXISTS [IX_Key_FactBilling_UtilityRecordId] ON [ETL].[KEY_FactBilling]
GO
/****** Object:  Index [IX_Key_FactBilling_PremiseId]    Script Date: 8/29/2017 1:37:51 PM ******/
DROP INDEX IF EXISTS [IX_Key_FactBilling_PremiseId] ON [ETL].[KEY_FactBilling]
GO
/****** Object:  Index [IX_KEY_FactBilling_ClientId]    Script Date: 8/29/2017 1:37:51 PM ******/
DROP INDEX IF EXISTS [IX_KEY_FactBilling_ClientId] ON [ETL].[KEY_FactBilling]
GO
/****** Object:  Index [IX_KEY_FactBilling_Client_RateClass]    Script Date: 8/29/2017 1:37:51 PM ******/
DROP INDEX IF EXISTS [IX_KEY_FactBilling_Client_RateClass] ON [ETL].[KEY_FactBilling]
GO
/****** Object:  Table [ETL].[KEY_FactBilling]    Script Date: 8/29/2017 1:37:51 PM ******/
DROP TABLE IF EXISTS [ETL].[KEY_FactBilling]
GO
/****** Object:  Table [ETL].[KEY_FactBilling]    Script Date: 8/29/2017 1:37:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND type in (N'U'))
BEGIN
CREATE TABLE [ETL].[KEY_FactBilling](
    [ClientId] [int] NOT NULL,
    [PremiseId] [varchar](50) NOT NULL,
    [AccountId] [varchar](50) NOT NULL,
    [CustomerId] [varchar](50) NOT NULL,
    [ServiceContractId] [varchar](64) NOT NULL,
    [ServicePointId] [varchar](64) NULL,
    [MeterId] [varchar](64) NULL,
    [MeterType] [varchar](64) NULL,
    [ReplacedMeterId] [varchar](64) NULL,
    [RateClass] [varchar](256) NULL,
    [CommodityId] [int] NOT NULL,
    [AMIStartDate] [datetime] NULL,
    [AMIEndDate] [datetime] NULL,
    [StartDate] [datetime] NOT NULL,
    [EndDate] [datetime] NOT NULL,
    [SourceId] [int] NOT NULL,
    [TrackingId] [varchar](50) NOT NULL,
    [TrackingDate] [datetime] NOT NULL,
    [BillingCostDetailsKey] [uniqueidentifier] NULL,
    [ServiceCostDetailsKey] [uniqueidentifier] NULL,
    [UtilityBillRecordId] [varchar](64) NULL,
    [CanceledUtilityBillRecordId] [varchar](64) NULL,
    [BillDate] [datetime] NULL
)
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_KEY_FactBilling_Client_RateClass]    Script Date: 8/29/2017 1:37:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND name = N'IX_KEY_FactBilling_Client_RateClass')
CREATE NONCLUSTERED INDEX [IX_KEY_FactBilling_Client_RateClass] ON [ETL].[KEY_FactBilling]
(
    [ClientId] ASC,
    [RateClass] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_KEY_FactBilling_ClientId]    Script Date: 8/29/2017 1:37:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND name = N'IX_KEY_FactBilling_ClientId')
CREATE NONCLUSTERED INDEX [IX_KEY_FactBilling_ClientId] ON [ETL].[KEY_FactBilling]
(
    [ClientId] ASC
)
INCLUDE (   [AccountId],
    [AMIEndDate],
    [AMIStartDate],
    [CommodityId],
    [PremiseId],
    [ServiceContractId],
    [TrackingDate],
    [TrackingId],
    [EndDate]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Key_FactBilling_PremiseId]    Script Date: 8/29/2017 1:37:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND name = N'IX_Key_FactBilling_PremiseId')
CREATE NONCLUSTERED INDEX [IX_Key_FactBilling_PremiseId] ON [ETL].[KEY_FactBilling]
(
    [ClientId] ASC,
    [PremiseId] ASC,
    [AccountId] ASC
)
INCLUDE (   [AMIEndDate],
    [AMIStartDate],
    [CommodityId],
    [ServiceContractId],
    [TrackingDate],
    [TrackingId]) WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Key_FactBilling_UtilityRecordId]    Script Date: 8/29/2017 1:37:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[ETL].[KEY_FactBilling]') AND name = N'IX_Key_FactBilling_UtilityRecordId')
CREATE NONCLUSTERED INDEX [IX_Key_FactBilling_UtilityRecordId] ON [ETL].[KEY_FactBilling]
(
    [UtilityBillRecordId] ASC,
    [CanceledUtilityBillRecordId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
