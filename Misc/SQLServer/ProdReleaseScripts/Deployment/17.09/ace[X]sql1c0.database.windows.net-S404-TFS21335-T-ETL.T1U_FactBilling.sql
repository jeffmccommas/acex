﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[T1U_FactBilling]') AND type in (N'U'))
    ALTER TABLE [ETL].[T1U_FactBilling] DROP CONSTRAINT IF EXISTS [DF__T1U_FactB__IsFau__039C5DE8]
GO

/****** Object:  Table [ETL].[T1U_FactBilling]    Script Date: 8/28/2017 2:49:33 PM ******/
DROP TABLE IF EXISTS [ETL].[T1U_FactBilling]
GO

/****** Object:  Table [ETL].[T1U_FactBilling]    Script Date: 8/28/2017 2:49:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[T1U_FactBilling]') AND type in (N'U'))
BEGIN
CREATE TABLE [ETL].[T1U_FactBilling](
    [ServiceContractKey] [int] NOT NULL,
    [BillPeriodStartDateKey] [int] NOT NULL,
    [BillPeriodEndDateKey] [int] NOT NULL,
    [CommodityKey] [int] NOT NULL,
    [SourceId] [int] NOT NULL,
    [ClientID] [int] NOT NULL,
    [TotalUnits] [decimal](18, 2) NOT NULL,
    [TotalCost] [decimal](18, 2) NOT NULL,
    [StartDate] [datetime] NOT NULL,
    [BillDays] [int] NOT NULL,
    [RateClass] [varchar](256) NULL,
    [ReadQuality] [varchar](50) NULL,
    [ReadDate] [datetime] NULL,
    [DueDate] [date] NULL,
    [ETL_LogId] [int] NOT NULL,
    [TrackingId] [varchar](50) NOT NULL,
    [TrackingDate] [datetime] NOT NULL,
    [IsFault] [bit] NOT NULL,
    [BillingCostDetailsKey] [uniqueidentifier] NULL,
    [ServiceCostDetailsKey] [uniqueidentifier] NULL,
    [UtilityBillRecordId] [varchar](64) NULL,
    [CanceledUtilityBillRecordId] [varchar](64) NULL,
    [BillDate] [datetime] NULL
 CONSTRAINT [PK_T1U_FactBilling] PRIMARY KEY CLUSTERED
(
    [ServiceContractKey] ASC,
    [BillPeriodStartDateKey] ASC,
    [BillPeriodEndDateKey] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[DF__T1U_FactB__IsFau__039C5DE8]') AND type = 'D')
BEGIN
    ALTER TABLE [ETL].[T1U_FactBilling] ADD  DEFAULT ((0)) FOR [IsFault]
END
GO
