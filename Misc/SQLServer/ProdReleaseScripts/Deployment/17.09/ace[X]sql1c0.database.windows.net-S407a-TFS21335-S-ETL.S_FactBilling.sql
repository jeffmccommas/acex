﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/***** Object:  StoredProcedure [ETL].[S_FactBilling]    Script Date: 8/29/2017 1:59:25 PM ******/
DROP PROCEDURE IF EXISTS [ETL].[S_FactBilling]
GO
/****** Object:  StoredProcedure [ETL].[S_FactBilling]    Script Date: 8/29/2017 1:59:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[S_FactBilling]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[S_FactBilling] AS'
END
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 7/21/2014
-- Description:
--
-- 10/27/2016 -- Phil Victor -- Added RateClassKey
-- 01/12/2017 -- Phil Victor -- Added CostDetailKeys
-- 05/13/2017 -- Phil Victor -- Added UtilityRecordIds
-- 08/29/2017 -- Phil Victor -- Added BillDate
-- =============================================
ALTER PROCEDURE [ETL].[S_FactBilling] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT  [fspb].[ServiceContractKey]
               ,[BillPeriodStartDateKey]
               ,[BillPeriodEndDateKey]
               ,[fspb].[ClientId]
               ,[fspb].[PremiseId]
               ,[fspb].[AccountId]
               ,IIF([dsc].[IsInferred] = 1, '', [dsc].[ServiceContractId]) AS [ServiceContractId]
               ,[fspb].[StartDate]
               ,[fspb].[EndDate]
               ,[fspb].[CommodityId]
               ,[fspb].[TotalUsage]
               ,[fspb].[CostOfUsage]
               ,[fspb].[SourceKey]
               ,[fspb].[BillDays]
               ,[fspb].[RateClassKey1] AS [RateClassKey]
               ,IIF([fspb].[BillingCostDetailsKey] IS NULL, [kfb].[BillingCostDetailsKey], [fspb].[BillingCostDetailsKey]) AS [BillingCostDetailKey]
               ,IIF([fspb].[ServiceCostDetailsKey] IS NULL, [kfb].[ServiceCostDetailsKey], [fspb].[ServiceCostDetailsKey]) AS [ServiceCostDetailsKey]
               ,[fspb].[UtilityBillRecordId]
               ,[fspb].[CanceledUtilityBillRecordId]
               ,[fspb].[BillDate]
        FROM    [ETL].[KEY_FactBilling] [kfb] WITH (NOLOCK)
                INNER JOIN [dbo].[DimPremise] [p] WITH (NOLOCK) ON [p].[ClientId] = [kfb].[ClientId]
                                                             AND [p].[AccountId] = [kfb].[AccountId]
                                                             AND [p].[PremiseId] = [kfb].[PremiseId]
                INNER JOIN [dbo].[DimServiceContract] [dsc] WITH (NOLOCK) ON [dsc].[ClientId] = [kfb].[ClientId]
                                                                       AND [dsc].[PremiseKey] = [p].[PremiseKey]
                                                                       AND [kfb].[CommodityId] = [dsc].[CommodityKey]
                                                                       AND [kfb].[ServiceContractId] = [dsc].[ServiceContractId]
                INNER JOIN [dbo].[FactServicePointBilling] [fspb] WITH (NOLOCK) ON [fspb].[ClientId] = [kfb].[ClientId]
                                                                             AND [fspb].[ServiceContractKey] = [dsc].[ServiceContractKey]
                                                                             AND [kfb].[EndDate] = [fspb].[EndDate]
        WHERE   [kfb].[ClientId] = @ClientID
        ORDER BY [fspb].[ClientId]
               ,[fspb].[PremiseId]
               ,[fspb].[AccountId]
               ,[dsc].[ServiceContractId]
               ,[fspb].[EndDate]
               ,[fspb].[CommodityId];

    END;



GO
