﻿

/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  Table [Holding].[Billing]    Script Date: 8/24/2017 8:47:14 AM ******/
DROP TABLE IF EXISTS [Holding].[Billing]
GO
/****** Object:  Table [Holding].[Billing]    Script Date: 8/24/2017 8:47:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Holding].[Billing]') AND type in (N'U'))
BEGIN
CREATE TABLE [Holding].[Billing](
    [ClientId] [int] NOT NULL,
    [CustomerId] [varchar](50) NOT NULL,
    [AccountId] [varchar](50) NOT NULL,
    [PremiseId] [varchar](50) NOT NULL,
    [ServiceContractId] [varchar](64) NOT NULL,
    [ServicePointId] [varchar](64) NULL,
    [BillStartDate] [datetime] NOT NULL,
    [BillEndDate] [datetime] NOT NULL,
    [BillDays] [int] NOT NULL,
    [TotalUnits] [decimal](18, 2) NOT NULL,
    [TotalCost] [decimal](18, 2) NOT NULL,
    [CommodityId] [int] NOT NULL,
    [BillPeriodTypeId] [int] NOT NULL,
    [UOMId] [int] NOT NULL,
    [AMIStartDate] [datetime] NULL,
    [AMIEndDate] [datetime] NULL,
    [MeterId] [varchar](64) NULL,
    [MeterType] [varchar](64) NULL,
    [ReplacedMeterId] [varchar](64) NULL,
    [RateClass] [varchar](256) NULL,
    [DueDate] [date] NULL,
    [ReadDate] [datetime] NULL,
    [ReadQuality] [varchar](50) NULL,
    [BillCycleScheduleId] [varchar](50) NULL,
    [SourceId] [int] NOT NULL,
    [TrackingId] [varchar](50) NOT NULL,
    [TrackingDate] [datetime] NOT NULL,
    [BillingRowId] [int] IDENTITY(1,1) NOT NULL,
    [ServiceCostDetailsKey] [uniqueidentifier] NULL,
    [BillingCostDetailsKey] [uniqueidentifier] NULL,
    [UtilityBillRecordId] [varchar](64) NULL,
    [CanceledUtilityBillRecordId] [varchar](64) NULL,
    [BillDate] [datetime] NULL
)
END
GO