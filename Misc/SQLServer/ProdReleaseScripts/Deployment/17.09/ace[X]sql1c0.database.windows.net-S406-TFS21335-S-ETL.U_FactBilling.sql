﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  StoredProcedure [ETL].[U_FactBilling]    Script Date: 8/28/2017 3:13:51 PM ******/
DROP PROCEDURE IF EXISTS [ETL].[U_FactBilling]
GO

/****** Object:  StoredProcedure [ETL].[U_FactBilling]    Script Date: 8/28/2017 3:13:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[U_FactBilling]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[U_FactBilling] AS'
END
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 7/2/2014
-- Description:
--
-- 10/27/2016 -- Phil Victor -- Added IsFault
-- 01/10/2017 -- Phil Victor -- Added CostDetailsKeys
-- 08/29/2017 -- Phil Victor -- Added BillDate
-- =============================================
ALTER PROCEDURE [ETL].[U_FactBilling]
    @ClientID INT
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @component [VARCHAR](400) = '[ETL].[U_FactBilling]';

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Starting sproc run';


    DECLARE @TrackingDate DATETIME;
    SET @TrackingDate = GETUTCDATE();

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Start UPDATE [dbo].[FactServicePointBilling] ';

    UPDATE [dbo].[FactServicePointBilling]
    SET --   [BillPeriodStartDateKey] = [dsd].[DateKey]
          [TotalUsage] = [b].[TotalUnits]
         , [CostOfUsage] = [b].[TotalCost]
         , [BillDays] = [b].[BillDays]
         , [StartDate] = [b].[StartDate]
         , [RateClassKey1] = [drc].[RateClassKey]
         , [ReadQuality] = [b].[ReadQuality]
         , [ReadDate] = [b].[ReadDate]
         , [DueDate] = [b].[DueDate]
         , [UpdateDate] = @TrackingDate
         , [TrackingId] = [b].[TrackingId]
         , [SourceKey] = [ds].[SourceKey]
         , [SourceId] = [b].[SourceId]
         , [TrackingDate] = [b].[TrackingDate]
         , [IsFault] = [b].[IsFault]
         , [BillDate] = [b].[BillDate]
    FROM   [ETL].[T1U_FactBilling] [b] WITH (NOLOCK)
           INNER JOIN [dbo].[FactServicePointBilling] [fb] WITH (NOLOCK) ON [fb].[ServiceContractKey] = [b].[ServiceContractKey]
                                                                            AND [fb].[BillPeriodEndDateKey] = [b].[BillPeriodEndDateKey]
                                                                            AND [fb].[BillPeriodStartDateKey] = [b].[BillPeriodStartDateKey]
           INNER JOIN [dbo].[DimSource] [ds] ON [ds].[SourceId] = [b].[SourceId]
           INNER JOIN [dbo].[DimDate] [dsd] WITH (NOLOCK) ON [dsd].[FullDateAlternateKey] = [b].[StartDate]
           LEFT JOIN [dbo].[DimRateClass] [drc] ON [drc].[ClientId] = @ClientID
                                                   AND [drc].[RateClassName] = [b].[RateClass]
    WHERE  [b].[ClientID] = @ClientID;

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Finished UPDATE [dbo].[FactServicePointBilling] ';

    /* Update BillCostDetails - can only do value */
    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Start UPDATE [dbo].[FactServicePointBilling_BillCostDetails] ';
    UPDATE [dbo].[FactServicePointBilling_BillCostDetails]
    SET    [BillingCostDetailValue] = [b].[BillingCostDetailValue]
    FROM   [ETL].[T1U_FactBilling_BillCostDetails] [b] WITH (NOLOCK)
           INNER JOIN [dbo].[FactServicePointBilling_BillCostDetails] [fb] WITH (NOLOCK) ON [fb].[BillingCostDetailsKey] = [b].[BillingCostDetailsKey]
                                                                                            AND [fb].[BillingCostDetailName] = [b].[BillingCostDetailName];
    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Finished UPDATE [dbo].[FactServicePointBilling_BillCostDetails] ';

    /* Update ServiceCostDetails - can only do value */
    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Start UPDATE [dbo].[FactServicePointBilling_ServiceCostDetails] ';
    UPDATE [dbo].[FactServicePointBilling_ServiceCostDetails]
    SET    [ServiceCostDetailValue] = [b].[ServiceCostDetailValue]
    FROM   [ETL].[T1U_FactBilling_ServiceCostDetails] [b] WITH (NOLOCK)
           INNER JOIN [dbo].[FactServicePointBilling_ServiceCostDetails] [fb] WITH (NOLOCK) ON [fb].[ServiceCostDetailsKey] = [b].[ServiceCostDetailsKey]
                                                                                               AND [fb].[ServiceCostDetailName] = [b].[ServiceCostDetailName];

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Finished UPDATE [dbo].[FactServicePointBilling_ServiceCostDetails] ';

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Finished sproc run';
END;











GO
