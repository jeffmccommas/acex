﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[S_Billing]    Script Date: 8/28/2017 3:09:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Jason Khourie
-- Create date: 7/21/2014
-- Description:
--
-- 10/28/2016 -- Phil Victor -- Added RateClassKey
-- 01/10/2017 -- Phil Victor -- Added CostDetailsKey
-- 05/13/2017 -- Phil Victor -- Added UtilityRecordIds
-- 08/29/2017 -- Phil Victor -- Added BillDate
-- =============================================
ALTER PROCEDURE [ETL].[S_Billing] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT  [ClientId]
               ,[AccountId]
               ,[PremiseId]
               ,[ServiceContractId]
               ,[ServicePointId]
               ,[MeterId]
               ,[StartDate]
               ,[EndDate]
               ,[BillDays]
               ,[TotalUnits]
               ,[TotalCost]
               ,[CommodityId]
               ,[BillPeriodTypeId]
               ,[BillCycleScheduleId]
               ,[UOMId]
               ,[RateClass]
               ,[RateClassKey]
               ,[DueDate]
               ,[ReadDate]
               ,[ReadQuality]
               ,[SourceId]
               ,[TrackingId]
               ,[TrackingDate]
               ,[BillingCostDetailsKey]
               ,[ServiceCostDetailsKey]
               ,[UtilityBillRecordId]
               ,[CanceledUtilityBillRecordId]
               ,[BillDate]
        FROM    (SELECT [hb].[ClientId]
                       ,[AccountId]
                       ,[PremiseId]
                       ,[ServiceContractId]
                       ,[ServicePointId]
                       ,[MeterId]
                       ,[BillStartDate] AS StartDate
                       ,[BillEndDate] AS EndDate
                       ,[BillDays]
                       ,[TotalUnits]
                       ,[TotalCost]
                       ,[CommodityId]
                       ,[BillPeriodTypeId]
                       ,[BillCycleScheduleId]
                       ,[UOMId]
                       ,[RateClass]
                       ,[drc].[RateClassKey]
                       ,[DueDate]
                       ,[ReadDate]
                       ,[ReadQuality]
                       ,[SourceId]
                       ,[TrackingId]
                       ,[TrackingDate]
                       ,[BillingCostDetailsKey]
                       ,[ServiceCostDetailsKey]
                       ,[UtilityBillRecordId]
                       ,[CanceledUtilityBillRecordId]
                       ,[BillDate]
                       ,ROW_NUMBER() OVER (PARTITION BY [hb].[ClientId], [AccountId], [PremiseId], [ServiceContractId],
                                           [BillEndDate], [CommodityId] ORDER BY [TrackingDate] DESC) AS [SortId]
                 FROM   [Holding].[Billing] [hb]
                        LEFT JOIN [dbo].[DimRateClass] [drc] ON [drc].[ClientId] = [hb].[ClientId]
                                                          AND [drc].[RateClassName] = [hb].[RateClass]
                 WHERE  [hb].[ClientId] = @ClientID
                ) [b]
        WHERE   [b].[SortId] = 1
        ORDER BY [ClientId]
               ,[PremiseId]
               ,[AccountId]
               ,[ServiceContractId]
               ,[EndDate]
               ,[CommodityId];


    END;




