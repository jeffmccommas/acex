﻿


/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  View [Holding].[v_Billing]    Script Date: 8/28/2017 2:16:47 PM ******/
DROP VIEW IF EXISTS [Holding].[v_Billing]
GO

/****** Object:  View [Holding].[v_Billing]    Script Date: 8/28/2017 2:16:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Holding].[v_Billing]'))
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:      Jason Khourie
-- Create date: 613/2014
--
-- 08/29/2017 -- Phil Victor -- Added BillDate
-- =============================================

CREATE VIEW [Holding].[v_Billing]
AS
    SELECT  [theSelected].[ClientId] ,
            [theSelected].[CustomerId] ,
            [theSelected].[AccountId] ,
            [theSelected].[PremiseId] ,
            [theSelected].[ServiceContractId] ,
            [theSelected].[ServicePointId] ,
            [theSelected].[MeterId] ,
            [theSelected].[RateClass] ,
            [theSelected].[StartDate] ,
            [theSelected].[EndDate] ,
            [theSelected].[BillDays] ,
            [theSelected].[TotalUnits] ,
            [theSelected].[TotalCost] ,
            [theSelected].[CommodityId] ,
            [theSelected].[BillPeriodTypeId] ,
            [theSelected].[BillCycleScheduleId] ,
            [theSelected].[UOMId] ,
            [theSelected].[DueDate] ,
            [theSelected].[ReadDate] ,
            [theSelected].[ReadQuality] ,
            [theSelected].[SourceId] ,
            [theSelected].TrackingId ,
            [theSelected].TrackingDate ,
            [theSelected].[BillingCostDetailsKey] ,
            [theSelected].[ServiceCostDetailsKey],
            [theSelected].[UtilityBillRecordId],
            [theSelected].[CanceledUtilityBillRecordId],
            [theSelected].[BillDate]
    FROM    ( SELECT    [b].[ClientId] ,
                        [b].[CustomerId] ,
                        [b].[AccountId] ,
                        [b].[PremiseId] ,
                        [b].[ServiceContractId] ,
                        [b].[ServicePointId] ,
                        [b].[MeterId] ,
                        [b].[RateClass] ,
                        [b].[BillStartDate] AS StartDate ,
                        [b].[BillEndDate] AS EndDate ,
                        [b].[BillDays] ,
                        [b].[TotalUnits] ,
                        [b].[TotalCost] ,
                        [b].[CommodityId] ,
                        [b].[BillPeriodTypeId] ,
                        [b].[BillCycleScheduleId] ,
                        [b].[UOMId] ,
                        [b].[DueDate] ,
                        [b].[ReadDate] ,
                        [b].[ReadQuality] ,
                        [b].[SourceId] ,
                        [b].TrackingId ,
                        [b].TrackingDate ,
                        [b].[BillingCostDetailsKey] ,
                        [b].[ServiceCostDetailsKey] ,
                        [b].[UtilityBillRecordId],
                        [b].[CanceledUtilityBillRecordId],
                        [b].[BillDate],
                        ROW_NUMBER() OVER ( PARTITION BY [b].[ClientId],
                                            [b].[PremiseId], [b].[AccountId],
                                            [b].[ServiceContractId],
                                            [b].[BillStartDate], [b].[BillEndDate],
                                            [b].[CommodityId] ORDER BY [b].[TrackingDate] DESC ) AS theRank
              FROM      [Holding].[Billing] b WITH ( NOLOCK )
                        INNER JOIN [Holding].[Client] [cl] ON [cl].[ClientId] = [b].[ClientId]
            ) [theSelected]
    WHERE   [theSelected].[theRank] = 1;'
GO
