﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

IF NOT EXISTS (SELECT * FROM [sys].[columns] WHERE [object_id]=OBJECT_ID(N'[dbo].[FactServicePointBilling]')AND [name]='BillDate')
BEGIN
    ALTER TABLE [dbo].[FactServicePointBilling] ADD [BillDate] DATETIME NULL;
END;

IF NOT EXISTS (SELECT * FROM [sys].[columns] WHERE [object_id]=OBJECT_ID(N'[audit].[FactBilling_PK_Violators]')AND [name]='BillDate')
BEGIN
    ALTER TABLE [audit].[FactBilling_PK_Violators] ADD [BillDate] DATETIME NULL;
END;

IF NOT EXISTS (SELECT * FROM [sys].[columns] WHERE [object_id]=OBJECT_ID(N'[audit].[MissingCanceled_FactServicePointBilling]')AND [name]='BillDate')
BEGIN
    ALTER TABLE [audit].[MissingCanceled_FactServicePointBilling] ADD [BillDate] DATETIME NULL;
END;

IF NOT EXISTS (SELECT * FROM [sys].[columns] WHERE [object_id]=OBJECT_ID(N'[audit].[RemovedFrom_FactServicePointBilling]')AND [name]='BillDate')
BEGIN
    ALTER TABLE [audit].[RemovedFrom_FactServicePointBilling] ADD [BillDate] DATETIME NULL;
END;
