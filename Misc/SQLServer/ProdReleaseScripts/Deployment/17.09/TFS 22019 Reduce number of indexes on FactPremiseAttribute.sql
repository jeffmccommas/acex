-- TFS 22019 Reduce number of indexes on FactPremiseAttribute 
-- *******************************************
-- Proposed Scripts on PROD - InsightsDW_Test
-- *******************************************
	if exists (SELECT Name FROM sysindexes WHERE Name = 'FactPremiseAttribute_ContentAttributeKey')
		DROP INDEX [FactPremiseAttribute_ContentAttributeKey] ON [dbo].[FactPremiseAttribute]
		GO

	if exists (SELECT Name FROM sysindexes WHERE Name = 'IX_FactPremiseAttribute')
		DROP INDEX [IX_FactPremiseAttribute] ON [dbo].[FactPremiseAttribute]
		GO

	if exists (SELECT Name FROM sysindexes WHERE Name = 'IX_FactPremiseAttribute_1')
		DROP INDEX [IX_FactPremiseAttribute_1] ON [dbo].[FactPremiseAttribute]
		GO

	if exists (SELECT Name FROM sysindexes WHERE Name = 'IX_FactPremiseAttribute_2')
		DROP INDEX [IX_FactPremiseAttribute_2] ON [dbo].[FactPremiseAttribute]
		GO

	if exists (SELECT Name FROM sysindexes WHERE Name = 'NC_FactPremiseAttribute')
		DROP INDEX [NC_FactPremiseAttribute] ON [dbo].[FactPremiseAttribute]
		GO

	if exists (SELECT Name FROM sysindexes WHERE Name = 'NC_FactPremiseAttribute_ContentAttributeKey')
		DROP INDEX [NC_FactPremiseAttribute_ContentAttributeKey] ON [dbo].[FactPremiseAttribute]
		GO

	if exists (SELECT Name FROM sysindexes WHERE Name = 'NC_FactPremiseAttribute_ContentAttributeKey_PremiseID')
		DROP INDEX [NC_FactPremiseAttribute_ContentAttributeKey_PremiseID] ON [dbo].[FactPremiseAttribute]
		GO

	if exists (SELECT Name FROM sysindexes WHERE Name = 'NC_FactPremiseAttribute_ContentAttributeKey_with_Premisekey')
		DROP INDEX [NC_FactPremiseAttribute_ContentAttributeKey_with_Premisekey] ON [dbo].[FactPremiseAttribute]
		GO

	if exists (SELECT Name FROM sysindexes WHERE Name = 'NC_FactPremiseAttribute_EffectiveDate')
		DROP INDEX [NC_FactPremiseAttribute_EffectiveDate] ON [dbo].[FactPremiseAttribute]
		GO

	if exists (SELECT Name FROM sysindexes WHERE Name = 'NC_FactPremiseAttribute_Value')
		DROP INDEX [NC_FactPremiseAttribute_Value] ON [dbo].[FactPremiseAttribute]
		GO

-- clustered index
-- ***************
if not exists (SELECT Name FROM sysindexes WHERE Name = 'CIDX_FactPremiseAttribute')
CREATE CLUSTERED INDEX [CIDX_FactPremiseAttribute] ON [dbo].[FactPremiseAttribute]
	(
		[PremiseKey] ASC,
		[ContentAttributeKey] ASC,
		[EffectiveDate] DESC
	)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

-- 1 nonclustered index with included columns
-- *******************************************
if not exists (SELECT Name FROM sysindexes WHERE Name = 'NCIDX_FactPremiseAttribute')
CREATE INDEX [NCIDX_FactPremiseAttribute] ON [dbo].[FactPremiseAttribute]
	(
		[ClientID] ASC,
		[PremiseID] ASC,
		[AccountID] ASC,
		[ContentAttributeKey] ASC,
		[EffectiveDate] DESC,
		[Value] ASC
	)
	INCLUDE
	(	[AttributeId],
		[ContentOptionKey],
		[OptionKey],
		[SourceID],
		[TrackingID],
		[TrackingDate]
		
	)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
	GO
