﻿/********************************************************/
/***** RUN ONLY ON THE InsightsMetaData DATABASE ***/
/********************************************************/
/* Initial Check in to help compare the change. Actual change will be in the next checkin */

USE [InsightsMetaData]
GO

/* STEP 1 - Drop Stored procedure that is using AccountBillAmountsTable */
/****** Object:  StoredProcedure [cm].[uspEMSelectDailyWeatherDetailForBills]    Script Date: 9/8/2017 2:01:48 PM ******/
DROP PROCEDURE [cm].[uspEMSelectDailyWeatherDetailForBills]
GO


/* STEP 2 - Drop cm.AccountBillAmountsTable*/
/****** Object:  UserDefinedTableType [cm].[AccountBillAmountsTable]    Script Date: 9/8/2017 2:01:16 PM ******/
DROP TYPE [cm].[AccountBillAmountsTable]
GO


/* STEP 3 - Create AccountBillAmountsTable*/
/****** Object:  UserDefinedTableType [cm].[AccountBillAmountsTable]    Script Date: 9/8/2017 2:01:16 PM ******/
CREATE TYPE [cm].[AccountBillAmountsTable] AS TABLE(
	[AccountId] [varchar](50) NOT NULL,
	[PremiseId] [varchar](50) NOT NULL,
	[ServiceId] [varchar](50) NOT NULL,
	[BillDate] [varchar](50) NOT NULL,
	[BillStartDate] [varchar](50) NOT NULL,
	[BillEndDate] [varchar](50) NOT NULL,
	[CommodityKey] [varchar](50) NOT NULL,
	[UOMKey] [varchar](50) NOT NULL,
	[BillDays] [int] NOT NULL,
	[TotalServiceUse] [decimal](18, 2) NOT NULL,
	[CostofUsage] [decimal](18, 2) NOT NULL,
	[AdditionalServiceCost] [decimal](18, 2) NOT NULL,
	[ZipCode] [varchar](16) NOT NULL,
	[AvgTemp] [int] NOT NULL,
	[RateClass] [varchar](100) NULL,
	PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC,
	[PremiseId] ASC,
	[ServiceId] ASC,
	[BillEndDate] ASC,
	[CommodityKey] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO


/* STEP 4 - Create Stored Procedure again*/
/****** Object:  StoredProcedure [cm].[uspEMSelectDailyWeatherDetailForBills]    Script Date: 9/8/2017 1:37:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [cm].[uspEMSelectDailyWeatherDetailForBills]
	@AccountBills AccountBillAmountsTable READONLY
AS
    BEGIN

		WITH    Temps
			AS ( SELECT ba.AccountId,
						ba.PremiseId,
						ba.ServiceId,
						ba.BillDate,
						AVG(wd.AvgTemp) AS AverageTemp
				FROM @AccountBills ba
						LEFT JOIN cm.EMZipcode zc ON zc.ZipCode = LEFT(ba.ZipCode,5)
						LEFT JOIN cm.EMDailyWeatherDetail wd ON wd.StationID = zc.StationIdDaily
									               AND wd.WeatherReadingDate >= ba.BillStartDate
									               AND wd.WeatherReadingDate < ba.BillEndDate
				GROUP BY ba.AccountId,
						ba.PremiseId,
						ba.ServiceId,
						ba.BillDate
				)
		SELECT  ba.AccountId,
				ba.PremiseId,
				ba.ServiceId,
				ba.BillDate,
				ba.BillStartDate,
				ba.BillEndDate,
				CommodityKey,
				UOMKey,
				BillDays,
				TotalServiceUse,
				CostOfUsage,
				AdditionalServiceCost ,
				ba.ZipCode,
				ISNULL(AverageTemp,0.00) AS AvgTemp,
				ba.RateClass,
				ROW_NUMBER() OVER ( ORDER BY ba.AccountId, ba.PremiseId ) AS RowIdentifier
		FROM    @AccountBills ba
				INNER JOIN Temps t ON t.AccountId = ba.AccountId
									  AND t.PremiseId = ba.PremiseId
									  AND t.ServiceId = ba.ServiceId
									  AND t.BillDate = ba.BillDate
		ORDER BY ba.AccountId,
				ba.PremiseId,
				ba.BillDate DESC,
				ba.CommodityKey;

    END


GO



