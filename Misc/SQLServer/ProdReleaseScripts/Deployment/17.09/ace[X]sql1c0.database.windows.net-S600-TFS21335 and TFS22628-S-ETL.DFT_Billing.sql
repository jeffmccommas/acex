﻿

/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [ETL].[DFT_Billing]    Script Date: 8/28/2017 3:08:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      <Author,,Name>
-- Create date: <Create Date,,>
-- Description: <Description,,>
--
--- 10/27/2016 - Phil Victor -- Added IsFault Check and RateClassKey check
--- 01/11/2017 - Phil Victor -- Added CostDetailsKey
--- 08/29/2017 - Phil Victor -- Added BillDate
--- 09/08/2017 - Phil Victor -- Removed Missing Canceled functionality to support ReBill.
-- =============================================
ALTER PROCEDURE [ETL].[DFT_Billing]
    @ClientId AS INT
  , @EtlLogId AS INT
AS
BEGIN

    SET NOCOUNT ON;
    DECLARE @component [VARCHAR](400) = '[ETL].[DFT_Billing]';

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Starting sproc run';

    CREATE TABLE [#tblSrcBilling]
        (
            [ClientId] INT
          , [AccountId] VARCHAR(50)
          , [PremiseId] VARCHAR(50)
          , [ServiceContractId] VARCHAR(50)
          , [ServicePointId] VARCHAR(50)
          , [MeterId] VARCHAR(50)
          , [StartDate] DATETIME
          , [EndDate] DATETIME
          , [BillDays] INT
          , [TotalUnits] DECIMAL(18, 2)
          , [TotalCost] DECIMAL(18, 2)
          , [CommodityId] INT
          , [BillPeriodTypeId] INT
          , [BillCycleScheduleId] VARCHAR(50)
          , [UOMId] INT
          , [RateClass] VARCHAR(50)
          , [RateClassKey] INT
          , [DueDate] DATETIME
          , [ReadDate] DATETIME
          , [ReadQuality] VARCHAR(50)
          , [SourceId] INT
          , [TrackingId] VARCHAR(50)
          , [TrackingDate] DATETIME
          , [BillingCostDetailsKey] UNIQUEIDENTIFIER
          , [ServiceCostDetailsKey] UNIQUEIDENTIFIER
          , [UtilityBillRecordId] [VARCHAR](64)
          , [CanceledUtilityBillRecordId] [VARCHAR](64)
          , [BillDate] DATETIME
        );

    CREATE TABLE [#tblSrcFactBilling]
        (
            [ServiceContractKey] INT
          , [BillPeriodStartDateKey] INT
          , [BillPeriodEndDateKey] INT
          , [ClientId] INT
          , [PremiseId] VARCHAR(50)
          , [AccountId] VARCHAR(50)
          , [ServiceContractId] VARCHAR(50)
          , [StartDate] DATETIME
          , [EndDate] DATETIME
          , [CommodityId] INT
          , [TotalUsage] DECIMAL(18, 2)
          , [CostOfUsage] DECIMAL(18, 2)
          , [SourceKey] INT
          , [BillDays] INT
          , [RateClassKey] INT
          , [BillingCostDetailsKey] UNIQUEIDENTIFIER
          , [ServiceCostDetailsKey] UNIQUEIDENTIFIER
          , [UtilityBillRecordId] [VARCHAR](64)
          , [CanceledUtilityBillRecordId] [VARCHAR](64)
          , [BillDate] DATETIME
        );

    /* Insert data into Temp tables */
    INSERT INTO [#tblSrcBilling]
    EXEC [ETL].[S_Billing] @ClientID = @ClientId; /** Holding Table contents **/

    INSERT INTO [#tblSrcFactBilling]
    EXEC [ETL].[S_FactBilling] @ClientID = @ClientId; /** Current database contents **/


    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = '#tblSrcBilling and #tblSrcFactBilling loaded';


    /* Handled canceled Bills */
    IF EXISTS (
                  SELECT 1
                  FROM   [ETL].[KEY_FactBilling]
                  WHERE  [CanceledUtilityBillRecordId] IS NOT NULL
                         AND LEN([CanceledUtilityBillRecordId]) > 0
              )
    BEGIN

        EXEC [audit].[ETL_RecordProgress] @component = @component
                                        , @text = 'Handling CanceledUtilityBillRecord';

        SELECT DISTINCT [fsb].*
        INTO   [#RemovedFrom]
        FROM   [dbo].[FactServicePointBilling] [fsb]
               INNER JOIN [ETL].[KEY_FactBilling] [kfb] ON [fsb].[UtilityBillRecordId] = [kfb].[CanceledUtilityBillRecordId];

        INSERT INTO [audit].[RemovedFrom_FactServicePointBilling] (
                                                                      [ServiceContractKey]
                                                                    , [BillPeriodStartDateKey]
                                                                    , [BillPeriodEndDateKey]
                                                                    , [UOMKey]
                                                                    , [BillPeriodTypeKey]
                                                                    , [ClientId]
                                                                    , [PremiseId]
                                                                    , [AccountId]
                                                                    , [CommodityId]
                                                                    , [BillPeriodTypeId]
                                                                    , [UOMId]
                                                                    , [StartDate]
                                                                    , [EndDate]
                                                                    , [TotalUsage]
                                                                    , [CostOfUsage]
                                                                    , [OtherCost]
                                                                    , [RateClassKey1]
                                                                    , [RateClassKey2]
                                                                    , [NextReadDate]
                                                                    , [ReadDate]
                                                                    , [CreateDate]
                                                                    , [UpdateDate]
                                                                    , [BillCycleScheduleId]
                                                                    , [BillDays]
                                                                    , [ETL_LogId]
                                                                    , [TrackingId]
                                                                    , [TrackingDate]
                                                                    , [SourceKey]
                                                                    , [SourceId]
                                                                    , [ReadQuality]
                                                                    , [DueDate]
                                                                    , [IsFault]
                                                                    , [BillingCostDetailsKey]
                                                                    , [ServiceCostDetailsKey]
                                                                    , [UtilityBillRecordId]
                                                                    , [CanceledUtilityBillRecordId]
                                                                    , [BillDate]
                                                                  )
                    SELECT [rfsb].[ServiceContractKey]
                         , [rfsb].[BillPeriodStartDateKey]
                         , [rfsb].[BillPeriodEndDateKey]
                         , [rfsb].[UOMKey]
                         , [rfsb].[BillPeriodTypeKey]
                         , [rfsb].[ClientId]
                         , [rfsb].[PremiseId]
                         , [rfsb].[AccountId]
                         , [rfsb].[CommodityId]
                         , [rfsb].[BillPeriodTypeId]
                         , [rfsb].[UOMId]
                         , [rfsb].[StartDate]
                         , [rfsb].[EndDate]
                         , [rfsb].[TotalUsage]
                         , [rfsb].[CostOfUsage]
                         , [rfsb].[OtherCost]
                         , [rfsb].[RateClassKey1]
                         , [rfsb].[RateClassKey2]
                         , [rfsb].[NextReadDate]
                         , [rfsb].[ReadDate]
                         , [rfsb].[CreateDate]
                         , [rfsb].[UpdateDate]
                         , [rfsb].[BillCycleScheduleId]
                         , [rfsb].[BillDays]
                         , [rfsb].[ETL_LogId]
                         , [rfsb].[TrackingId]
                         , [rfsb].[TrackingDate]
                         , [rfsb].[SourceKey]
                         , [rfsb].[SourceId]
                         , [rfsb].[ReadQuality]
                         , [rfsb].[DueDate]
                         , [rfsb].[IsFault]
                         , [rfsb].[BillingCostDetailsKey]
                         , [rfsb].[ServiceCostDetailsKey]
                         , [rfsb].[UtilityBillRecordId]
                         , [rfsb].[CanceledUtilityBillRecordId]
                         , [rfsb].[BillDate]
                    FROM   [#RemovedFrom] [rfsb];

        DELETE [fsb]
        FROM [dbo].[FactServicePointBilling] [fsb]
             INNER JOIN [ETL].[KEY_FactBilling] [kfb] ON [fsb].[UtilityBillRecordId] = [kfb].[CanceledUtilityBillRecordId];

        TRUNCATE TABLE [#tblSrcFactBilling];
        INSERT INTO [#tblSrcFactBilling]
        EXEC [ETL].[S_FactBilling] @ClientID = @ClientId; /** Current database contents **/

        DELETE [fspbd]
        FROM [dbo].[FactServicePointBilling_BillCostDetails] [fspbd]
             INNER JOIN [#RemovedFrom] [rfspb] ON [rfspb].[BillingCostDetailsKey] = [fspbd].[BillingCostDetailsKey];

        DELETE [fspbs]
        FROM [dbo].[FactServicePointBilling_ServiceCostDetails] [fspbs]
             INNER JOIN [#RemovedFrom] [rfspb] ON [rfspb].[ServiceCostDetailsKey] = [fspbs].[ServiceCostDetailsKey];

        DROP TABLE [#RemovedFrom];

        EXEC [audit].[ETL_RecordProgress] @component = @component
                                        , @text = 'Finished CanceledUtilityBillRecord';

    END;

    /*** Handle new bills with a UtilityBillRecordId ***/
    IF EXISTS (
                  SELECT 1
                  FROM   [ETL].[KEY_FactBilling]
                  WHERE  [UtilityBillRecordId] IS NOT NULL
                         AND LEN([UtilityBillRecordId]) > 0
              )
    BEGIN

        EXEC [audit].[ETL_RecordProgress] @component = @component
                                        , @text = 'Handle new bills with a UtilityBillRecordId';
        SELECT DISTINCT @ClientId AS [ClientId]
             , [b].[PremiseId]
             , [b].[AccountId]
             , [b].[ServiceContractId]
             , [b].[StartDate]
             , [b].[EndDate]
             , [b].[CommodityId]
             , [b].[BillPeriodTypeId]
             , [b].[BillCycleScheduleId]
             , [b].[UOMId]
             , [b].[SourceId]
             , [b].[TotalUnits]
             , [b].[TotalCost]
             , [b].[BillDays]
             , [b].[RateClass]
             , [b].[ReadQuality]
             , [b].[ReadDate]
             , [b].[DueDate]
             , [b].[TrackingId]
             , [b].[TrackingDate]
             , [b].[BillingCostDetailsKey]
             , [b].[ServiceCostDetailsKey]
             , [b].[UtilityBillRecordId]
             , [b].[CanceledUtilityBillRecordId]
             , [b].[RateClassKey]
             , [b].[BillDate]
        INTO   [#AddedOrUpdated]
        FROM   [#tblSrcBilling] [b]
               INNER JOIN [ETL].[KEY_FactBilling] [kfb] ON [kfb].[UtilityBillRecordId] = [b].[UtilityBillRecordId]
        WHERE  (
                   [kfb].[UtilityBillRecordId] IS NOT NULL
                   AND LEN([kfb].[UtilityBillRecordId]) > 0
               )
               AND (
                       [b].[UtilityBillRecordId] IS NOT NULL
                       AND LEN([b].[UtilityBillRecordId]) > 0
                   );

        INSERT INTO [ETL].[INS_FactBilling]
                    SELECT DISTINCT @ClientId
                         , [b].[PremiseId]
                         , [b].[AccountId]
                         , [b].[ServiceContractId]
                         , [b].[StartDate]
                         , [b].[EndDate]
                         , [b].[CommodityId]
                         , [b].[BillPeriodTypeId]
                         , [b].[BillCycleScheduleId]
                         , [b].[UOMId]
                         , [b].[SourceId]
                         , [b].[TotalUnits]
                         , [b].[TotalCost]
                         , [b].[BillDays]
                         , [b].[RateClass]
                         , [b].[ReadQuality]
                         , [b].[ReadDate]
                         , [b].[DueDate]
                         , @EtlLogId
                         , [b].[TrackingId]
                         , [b].[TrackingDate]
                         , [b].[BillingCostDetailsKey]
                         , [b].[ServiceCostDetailsKey]
                         , [b].[UtilityBillRecordId]
                         , [b].[CanceledUtilityBillRecordId]
                         , [b].[BillDate]
                    FROM   [#AddedOrUpdated] [b]
                           LEFT JOIN [#tblSrcFactBilling] [fb] ON [fb].[ClientId] = [b].[ClientId]
                                                                  AND [fb].[PremiseId] = [b].[PremiseId]
                                                                  AND [fb].[AccountId] = [b].[AccountId]
                                                                  AND [fb].[ServiceContractId] = [b].[ServiceContractId]
                                                                  AND [fb].[EndDate] = [b].[EndDate]
                                                                  AND [fb].[CommodityId] = [b].[CommodityId]
                                                                  AND [fb].[UtilityBillRecordId] = [b].[UtilityBillRecordId]
                    WHERE  [fb].[UtilityBillRecordId] IS NULL
                           AND [b].[UtilityBillRecordId] IS NOT NULL;

        /* Handle Updates */
        INSERT INTO [ETL].[T1U_FactBilling]
                    SELECT DISTINCT [fb].[ServiceContractKey]
                         , [fb].[BillPeriodStartDateKey]
                         , [fb].[BillPeriodEndDateKey]
                         , [b].[CommodityId]
                         , [b].[SourceId]
                         , @ClientId
                         , [b].[TotalUnits]
                         , [b].[TotalCost]
                         , [b].[StartDate]
                         , [b].[BillDays]
                         , [b].[RateClass]
                         , [b].[ReadQuality]
                         , [b].[ReadDate]
                         , [b].[DueDate]
                         , @EtlLogId
                         , [b].[TrackingId]
                         , [b].[TrackingDate]
                         , IIF([b].[RateClassKey] != [fb].[RateClassKey], 1, 0) AS [IsFault]
                         , [fb].[BillingCostDetailsKey]
                         , [fb].[ServiceCostDetailsKey]
                         , [b].[UtilityBillRecordId]
                         , [b].[CanceledUtilityBillRecordId]
                         , [b].[BillDate]
                    FROM   [#AddedOrUpdated] [b]
                           INNER JOIN [#tblSrcFactBilling] [fb] ON [fb].[ClientId] = [b].[ClientId]
                                                                   AND [fb].[PremiseId] = [b].[PremiseId]
                                                                   AND [fb].[AccountId] = [b].[AccountId]
                                                                   AND [fb].[ServiceContractId] = [b].[ServiceContractId]
                                                                   AND [fb].[UtilityBillRecordId] = [b].[UtilityBillRecordId]
                                                                   AND [fb].[EndDate] = [b].[EndDate]
                                                                   AND [fb].[CommodityId] = [b].[CommodityId]
                           LEFT JOIN [dbo].[DimRateClass] [drc] WITH (NOLOCK) ON [drc].[ClientId] = [b].[ClientId]
                                                                                 AND [drc].[RateClassName] = [b].[RateClass]
                    WHERE  [fb].[UtilityBillRecordId] IS NOT NULL
                           AND [b].[UtilityBillRecordId] IS NOT NULL
                           AND (
                                   ISNULL([b].[TotalUnits], 0) != ISNULL([fb].[TotalUsage], 0)
                                   OR ISNULL([b].[TotalCost], 0) != ISNULL([fb].[CostOfUsage], 0)
                                   OR ISNULL([b].[BillDays], 0) != ISNULL([fb].[BillDays], 0)
                                   OR ISNULL([b].[RateClassKey], 0) != ISNULL([fb].[RateClassKey], 0)
                               );

        DROP TABLE [#AddedOrUpdated];

        EXEC [audit].[ETL_RecordProgress] @component = @component
                                        , @text = 'Finished new bills with a UtilityBillRecordId';
    END;

    /**** Old Path ****/
    IF EXISTS (
                  SELECT 1
                  FROM   [#tblSrcBilling]
                  WHERE  (
                             [UtilityBillRecordId] IS NULL
                             OR LEN([UtilityBillRecordId]) = 0
                         )
                         AND (
                                 [CanceledUtilityBillRecordId] IS NULL
                                 OR LEN([CanceledUtilityBillRecordId]) = 0
                             )
              )
    BEGIN

        EXEC [audit].[ETL_RecordProgress] @component = @component
                                        , @text = 'Start inserts to [ETL].[INS_FactBilling] ';

        /** Handle inserts **/
        INSERT INTO [ETL].[INS_FactBilling]
                    SELECT DISTINCT @ClientId
                         , [b].[PremiseId]
                         , [b].[AccountId]
                         , [b].[ServiceContractId]
                         , [b].[StartDate]
                         , [b].[EndDate]
                         , [b].[CommodityId]
                         , [b].[BillPeriodTypeId]
                         , [b].[BillCycleScheduleId]
                         , [b].[UOMId]
                         , [b].[SourceId]
                         , [b].[TotalUnits]
                         , [b].[TotalCost]
                         , [b].[BillDays]
                         , [b].[RateClass]
                         , [b].[ReadQuality]
                         , [b].[ReadDate]
                         , [b].[DueDate]
                         , @EtlLogId
                         , [b].[TrackingId]
                         , [b].[TrackingDate]
                         , [b].[BillingCostDetailsKey]
                         , [b].[ServiceCostDetailsKey]
                         , [b].[UtilityBillRecordId]
                         , [b].[CanceledUtilityBillRecordId]
                         , [b].[BillDate]
                    FROM   [#tblSrcBilling] [b]
                           LEFT JOIN [#tblSrcFactBilling] [fb] ON [fb].[ClientId] = [b].[ClientId]
                                                                  AND [fb].[PremiseId] = [b].[PremiseId]
                                                                  AND [fb].[AccountId] = [b].[AccountId]
                                                                  AND [fb].[ServiceContractId] = [b].[ServiceContractId]
                                                                  AND [fb].[EndDate] = [b].[EndDate]
                                                                  AND [fb].[CommodityId] = [b].[CommodityId]
                    WHERE  (
                               [fb].[ServiceContractKey] IS NULL
                               OR LEN([fb].[ServiceContractKey]) = 0
                           )
                           AND (
                                   [fb].[UtilityBillRecordId] IS NULL
                                   AND [b].[UtilityBillRecordId] IS NULL
                               );

        EXEC [audit].[ETL_RecordProgress] @component = @component
                                        , @text = 'Finished inserts to [ETL].[INS_FactBilling] ';

        EXEC [audit].[ETL_RecordProgress] @component = @component
                                        , @text = 'Start inserts to [ETL].[T1U_FactBilling] ';

        /* Handle Updates */
        INSERT INTO [ETL].[T1U_FactBilling]
                    SELECT DISTINCT [fb].[ServiceContractKey]
                         , [fb].[BillPeriodStartDateKey]
                         , [fb].[BillPeriodEndDateKey]
                         , [b].[CommodityId]
                         , [b].[SourceId]
                         , @ClientId
                         , [b].[TotalUnits]
                         , [b].[TotalCost]
                         , [b].[StartDate]
                         , [b].[BillDays]
                         , [b].[RateClass]
                         , [b].[ReadQuality]
                         , [b].[ReadDate]
                         , [b].[DueDate]
                         , @EtlLogId
                         , [b].[TrackingId]
                         , [b].[TrackingDate]
                         , IIF([b].[RateClassKey] != [fb].[RateClassKey], 1, 0) AS [IsFault]
                         , [fb].[BillingCostDetailsKey]
                         , [fb].[ServiceCostDetailsKey]
                         , [b].[UtilityBillRecordId]
                         , [b].[CanceledUtilityBillRecordId]
                         , [b].[BillDate]
                    FROM   [#tblSrcBilling] [b]
                           INNER JOIN [#tblSrcFactBilling] [fb] ON [fb].[ClientId] = [b].[ClientId]
                                                                   AND [fb].[PremiseId] = [b].[PremiseId]
                                                                   AND [fb].[AccountId] = [b].[AccountId]
                                                                   AND [fb].[ServiceContractId] = [b].[ServiceContractId]
                                                                   AND [fb].[EndDate] = [b].[EndDate]
                                                                   AND [fb].[CommodityId] = [b].[CommodityId]
                           LEFT JOIN [dbo].[DimRateClass] [drc] WITH (NOLOCK) ON [drc].[ClientId] = [b].[ClientId]
                                                                                 AND [drc].[RateClassName] = [b].[RateClass]
                    WHERE  [fb].[UtilityBillRecordId] IS NULL
                           AND [b].[UtilityBillRecordId] IS NULL
                           AND (
                                   ISNULL([b].[TotalUnits], 0) != ISNULL([fb].[TotalUsage], 0)
                                   OR ISNULL([b].[TotalCost], 0) != ISNULL([fb].[CostOfUsage], 0)
                                   OR ISNULL([b].[BillDays], 0) != ISNULL([fb].[BillDays], 0)
                                   OR ISNULL([b].[RateClassKey], 0) != ISNULL([fb].[RateClassKey], 0)
                               );

        EXEC [audit].[ETL_RecordProgress] @component = @component
                                        , @text = 'Finished inserts to [ETL].[T1U_FactBilling] ';
    END;

    /******************************************************************************************************************************/

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Starting to Handling PK Errors ';

    -- Handle PK errors
    SELECT [ifb].*
    INTO   [#HandlePKErrors]
    FROM   [ETL].[INS_FactBilling] [ifb]
           INNER JOIN [dbo].[FactServicePointBilling] [fsb] ON [fsb].[AccountId] = [ifb].[AccountId]
                                                               AND [fsb].[ClientId] = [ifb].[ClientId]
                                                               AND [fsb].[CommodityId] = [ifb].[CommodityId]
                                                               AND [fsb].[EndDate] = [ifb].[EndDate]
                                                               AND [fsb].[PremiseId] = [ifb].[PremiseId]
                                                               AND [fsb].[UOMId] = [ifb].[UOMId]
           INNER JOIN [dbo].[DimPremise] [dp] WITH (NOLOCK) ON [dp].[PremiseId] = [ifb].[PremiseId]
                                                               AND [dp].[AccountId] = [ifb].[AccountId]
                                                               AND [dp].[ClientId] = [ifb].[ClientId]
           INNER JOIN [dbo].[DimServiceContract] [dsc] WITH (NOLOCK) ON [dsc].[PremiseKey] = [dp].[PremiseKey]
                                                                        AND [dsc].[CommodityKey] = [ifb].[CommodityId]
                                                                        AND [dsc].[ServiceContractId] = [ifb].[ServiceContractId]
                                                                        AND [dsc].[ServiceContractKey] = [fsb].[ServiceContractKey];

    SELECT *
    FROM   [#HandlePKErrors];

    IF EXISTS (
                  SELECT 1
                  FROM   [#HandlePKErrors]
                  WHERE  [AccountId] IS NOT NULL
                         AND [PremiseId] IS NOT NULL
              )
    BEGIN

        DELETE [ifb]
        FROM [ETL].[INS_FactBilling] [ifb]
             INNER JOIN [#HandlePKErrors] [hpk] ON [hpk].[AccountId] = [ifb].[AccountId]
                                                   AND [hpk].[ClientId] = [ifb].[ClientId]
                                                   AND [hpk].[CommodityId] = [ifb].[CommodityId]
                                                   AND [hpk].[EndDate] = [ifb].[EndDate]
                                                   AND [hpk].[PremiseId] = [ifb].[PremiseId]
                                                   AND [hpk].[UOMId] = [ifb].[UOMId]
                                                   AND [hpk].[ServiceContractId] = [ifb].[ServiceContractId];

        DELETE [hb]
        FROM [Holding].[Billing] [hb]
             INNER JOIN [#HandlePKErrors] [hpk] ON [hpk].[AccountId] = [hb].[AccountId]
                                                   AND [hpk].[ClientId] = [hb].[ClientId]
                                                   AND [hpk].[CommodityId] = [hb].[CommodityId]
                                                   AND [hpk].[EndDate] = [hb].[BillEndDate]
                                                   AND [hpk].[PremiseId] = [hb].[PremiseId]
                                                   AND [hpk].[ServiceContractId] = [hb].[ServiceContractId];

        DELETE [bcd]
        FROM [Holding].[Billing_BillCostDetails] [bcd]
             INNER JOIN [#HandlePKErrors] [hpk] ON [hpk].[BillingCostDetailsKey] = [bcd].[BillingCostDetailsKey];

        DELETE [scd]
        FROM [Holding].[Billing_ServiceCostDetails] [scd]
             INNER JOIN [#HandlePKErrors] [hpk] ON [hpk].[ServiceCostDetailsKey] = [scd].[ServiceCostDetailsKey];

        INSERT INTO [audit].[FactBilling_PK_Violators]
                    SELECT *
                    FROM   [#HandlePKErrors];

    END;

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Finished Handling PK Errors ';

    /******************************************************************************************************************************/


    ----   /* Handle New BillCostDetails Insert */
    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Start Handle New BillCostDetails Insert ';

    INSERT INTO [ETL].[INS_FactBilling_BillCostDetails]
                SELECT [fb].[BillingCostDetailsKey]
                     , [bcd].[BillingCostDetailName]
                     , [bcd].[BillingCostDetailValue]
                FROM   [ETL].[INS_FactBilling] [fb] WITH (NOLOCK)
                       INNER JOIN [Holding].[Billing_BillCostDetails] [bcd] WITH (NOLOCK) ON [fb].[BillingCostDetailsKey] = [bcd].[BillingCostDetailsKey];

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Finish Handle New BillCostDetails Insert ';

    ----   /* Handle New  ServiceCostDetails Insert */
    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Start Handle New  ServiceCostDetails Insert  ';

    INSERT INTO [ETL].[INS_FactBilling_ServiceCostDetails]
                SELECT [fb].[ServiceCostDetailsKey]
                     , [scd].[ServiceCostDetailName]
                     , [scd].[ServiceCostDetailValue]
                FROM   [ETL].[INS_FactBilling] [fb] WITH (NOLOCK)
                       INNER JOIN [Holding].[Billing_ServiceCostDetails] [scd] WITH (NOLOCK) ON [fb].[ServiceCostDetailsKey] = [scd].[ServiceCostDetailsKey];

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Finish Handle New  ServiceCostDetails Insert  ';

    ----    /* Update the CostDetailKeys to ones already present in the database */
    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Start Update the BillCostDetailKeys to ones already present in the database   ';

    UPDATE [bcd]
    SET    [bcd].[BillingCostDetailsKey] = [fspb].[BillingCostDetailsKey]
    FROM   [Holding].[Billing_BillCostDetails] [bcd]
           INNER JOIN [ETL].[KEY_FactBilling] [kfb] ON [kfb].[BillingCostDetailsKey] = [bcd].[BillingCostDetailsKey]
           INNER JOIN [dbo].[DimPremise] [p] WITH (NOLOCK) ON [p].[ClientId] = [kfb].[ClientId]
                                                              AND [p].[AccountId] = [kfb].[AccountId]
                                                              AND [p].[PremiseId] = [kfb].[PremiseId]
           INNER JOIN [dbo].[DimServiceContract] [dsc] WITH (NOLOCK) ON [dsc].[ClientId] = [kfb].[ClientId]
                                                                        AND [dsc].[PremiseKey] = [p].[PremiseKey]
                                                                        AND [kfb].[CommodityId] = [dsc].[CommodityKey]
                                                                        AND [kfb].[ServiceContractId] = [dsc].[ServiceContractId]
           INNER JOIN [dbo].[FactServicePointBilling] [fspb] WITH (NOLOCK) ON [fspb].[ClientId] = [kfb].[ClientId]
                                                                              AND [fspb].[ServiceContractKey] = [dsc].[ServiceContractKey]
                                                                              AND [kfb].[StartDate] = [fspb].[StartDate]
                                                                              AND [kfb].[EndDate] = [fspb].[EndDate]
    WHERE  [fspb].[BillingCostDetailsKey] IS NOT NULL;

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Finish Update the BillCostDetailKeys to ones already present in the database   ';

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Start Update the ServiceCostDetailKeys to ones already present in the database   ';

    UPDATE [scd]
    SET    [scd].[ServiceCostDetailsKey] = [fspb].[ServiceCostDetailsKey]
    FROM   [Holding].[Billing_ServiceCostDetails] [scd]
           INNER JOIN [ETL].[KEY_FactBilling] [kfb] ON [kfb].[ServiceCostDetailsKey] = [scd].[ServiceCostDetailsKey]
           INNER JOIN [dbo].[DimPremise] [p] WITH (NOLOCK) ON [p].[ClientId] = [kfb].[ClientId]
                                                              AND [p].[AccountId] = [kfb].[AccountId]
                                                              AND [p].[PremiseId] = [kfb].[PremiseId]
           INNER JOIN [dbo].[DimServiceContract] [dsc] WITH (NOLOCK) ON [dsc].[ClientId] = [kfb].[ClientId]
                                                                        AND [dsc].[PremiseKey] = [p].[PremiseKey]
                                                                        AND [kfb].[CommodityId] = [dsc].[CommodityKey]
                                                                        AND [kfb].[ServiceContractId] = [dsc].[ServiceContractId]
           INNER JOIN [dbo].[FactServicePointBilling] [fspb] WITH (NOLOCK) ON [fspb].[ClientId] = [kfb].[ClientId]
                                                                              AND [fspb].[ServiceContractKey] = [dsc].[ServiceContractKey]
                                                                              AND [kfb].[StartDate] = [fspb].[StartDate]
                                                                              AND [kfb].[EndDate] = [fspb].[EndDate]
    WHERE  [fspb].[ServiceCostDetailsKey] IS NOT NULL;

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Finish Update the ServiceCostDetailKeys to ones already present in the database   ';

    ----    /* If a new BillCostDetail has been added */
    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Start INSERT INTO [ETL].[INS_FactBilling_BillCostDetails]   ';

    INSERT INTO [ETL].[INS_FactBilling_BillCostDetails]
                SELECT [bcd].[BillingCostDetailsKey]
                     , [bcd].[BillingCostDetailName]
                     , [bcd].[BillingCostDetailValue]
                FROM   [Holding].[Billing_BillCostDetails] [bcd] WITH (NOLOCK)
                WHERE  [bcd].[BillingCostDetailName] NOT IN (
                                                                SELECT [fbcd].[BillingCostDetailName]
                                                                FROM   [Holding].[Billing_BillCostDetails] [fb] WITH (NOLOCK)
                                                                       INNER JOIN [dbo].[FactServicePointBilling_BillCostDetails] [fbcd] WITH (NOLOCK) ON [fb].[BillingCostDetailsKey] = [fbcd].[BillingCostDetailsKey]
                                                            )
                       AND NOT EXISTS (
                                          SELECT 1
                                          FROM   [ETL].[INS_FactBilling_BillCostDetails] [ifb]
                                                 INNER JOIN [Holding].[Billing_BillCostDetails] [hbcd] ON [ifb].[BillingCostDetailsKey] = [hbcd].[BillingCostDetailsKey]
                                                                                                          AND [ifb].[BillingCostDetailName] = [hbcd].[BillingCostDetailName]
                                      );
    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Finish INSERT INTO [ETL].[INS_FactBilling_BillCostDetails]   ';

    /* If a new ServiceCostDetail has been added */
    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Start INSERT INTO [ETL].[INS_FactBilling_ServiceCostDetails]   ';
    INSERT INTO [ETL].[INS_FactBilling_ServiceCostDetails]
                SELECT [scd].[ServiceCostDetailsKey]
                     , [scd].[ServiceCostDetailName]
                     , [scd].[ServiceCostDetailValue]
                FROM   [Holding].[Billing_ServiceCostDetails] [scd] WITH (NOLOCK)
                WHERE  [scd].[ServiceCostDetailName] NOT IN (
                                                                SELECT [fscd].[ServiceCostDetailName]
                                                                FROM   [Holding].[Billing_ServiceCostDetails] [fb] WITH (NOLOCK)
                                                                       INNER JOIN [dbo].[FactServicePointBilling_ServiceCostDetails] [fscd] WITH (NOLOCK) ON [fb].[ServiceCostDetailsKey] = [fscd].[ServiceCostDetailsKey]
                                                            )
                       AND NOT EXISTS (
                                          SELECT 1
                                          FROM   [ETL].[INS_FactBilling_ServiceCostDetails] [ifsb]
                                                 INNER JOIN [Holding].[Billing_ServiceCostDetails] [sbcd] ON [ifsb].[ServiceCostDetailsKey] = [sbcd].[ServiceCostDetailsKey]
                                                                                                             AND [ifsb].[ServiceCostDetailName] = [sbcd].[ServiceCostDetailName]
                                      );
    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Finish INSERT INTO [ETL].[INS_FactBilling_ServiceCostDetails]   ';

    -- /* If the value changed in the BillCostDetails */
    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Start INSERT INTO [ETL].[T1U_FactBilling_BillCostDetails]   ';
    INSERT INTO [ETL].[T1U_FactBilling_BillCostDetails]
                SELECT [hbcd].[BillingCostDetailsKey]
                     , [hbcd].[BillingCostDetailName]
                     , [hbcd].[BillingCostDetailValue]
                FROM   [dbo].[FactServicePointBilling_BillCostDetails] [fbcd] WITH (NOLOCK)
                       INNER JOIN [Holding].[Billing_BillCostDetails] [hbcd] WITH (NOLOCK) ON [fbcd].[BillingCostDetailsKey] = [hbcd].[BillingCostDetailsKey]
                WHERE  [fbcd].[BillingCostDetailName] = [hbcd].[BillingCostDetailName]
                       AND [fbcd].[BillingCostDetailValue] != [hbcd].[BillingCostDetailValue];
    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Finish INSERT INTO [ETL].[T1U_FactBilling_BillCostDetails]   ';

    -- /* If the value changed in the ServiceCostDetails */
    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Start INSERT INTO [ETL].[T1U_FactBilling_ServiceCostDetails]   ';
    INSERT INTO [ETL].[T1U_FactBilling_ServiceCostDetails]
                SELECT [hscd].[ServiceCostDetailsKey]
                     , [hscd].[ServiceCostDetailName]
                     , [hscd].[ServiceCostDetailValue]
                FROM   [dbo].[FactServicePointBilling_ServiceCostDetails] [fscd] WITH (NOLOCK)
                       INNER JOIN [Holding].[Billing_ServiceCostDetails] [hscd] WITH (NOLOCK) ON [fscd].[ServiceCostDetailsKey] = [hscd].[ServiceCostDetailsKey]
                WHERE  [fscd].[ServiceCostDetailName] = [hscd].[ServiceCostDetailName]
                       AND [fscd].[ServiceCostDetailValue] != [hscd].[ServiceCostDetailValue];
    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Finish INSERT INTO [ETL].[T1U_FactBilling_ServiceCostDetails]   ';

    DROP TABLE [#tblSrcBilling];
    DROP TABLE [#tblSrcFactBilling];

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Finished sproc run';

END;







