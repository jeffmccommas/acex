﻿


/*************************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_210 AND INSIGHTSDW_TEST  ***/
/***** DATABASE SHARD ONLY                                 ***/
/************************************************************/

/****** Object:  StoredProcedure [ETL].[HandleBillingDuplicates]    Script Date: 8/29/2017 11:06:52 AM ******/
DROP PROCEDURE IF EXISTS [ETL].[HandleBillingDuplicates]
GO
/****** Object:  StoredProcedure [ETL].[HandleBillingDuplicates]    Script Date: 8/29/2017 11:06:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[HandleBillingDuplicates]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [ETL].[HandleBillingDuplicates] AS'
END
GO
-- =============================================
-- Author:      Philip Victor
-- Create date: 6/5/2017
-- Description: Removed duplicates from Holding.Billing
--
-- 8/29/2017 -- Phil Victor -- Added BillDate
-- =============================================
ALTER PROCEDURE [ETL].[HandleBillingDuplicates]
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    DECLARE @component [VARCHAR](400) = '[ETL].[HandleBillingDuplicates]'
          , @ProgressMsg AS VARCHAR(4000)
          , @Count AS INT = 0
          , @PreCount AS INT = 0
          , @EndCount AS INT = 0;

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Starting sproc run. Details in [audit].[HoldingBillingDuplicateLog]';

    EXEC [audit].[HoldingBillingDuplicates_RecordProgress] @component = @component
                                                         , @text = 'Starting Sproc run';

    -- Make sure something in Holding
    SELECT @Count = COUNT(*)
    FROM   [Holding].[Billing]; -- 696377

    SET @ProgressMsg = 'Count of Bills in [Holding].[Billing] :- ' + CAST(@Count AS VARCHAR(50));
    EXEC [audit].[HoldingBillingDuplicates_RecordProgress] @component = @component
                                                         , @text = @ProgressMsg;

    -- Get the duplicate bills
    IF OBJECT_ID('tempdb..#DuplicateBills') IS NOT NULL
        DROP TABLE [#DuplicateBills];

    SELECT   [b].*
    INTO     [#DuplicateBills]
    FROM     (
                 SELECT *
                      , ROW_NUMBER() OVER (PARTITION BY [hb].[ClientId]
                                                      , [hb].[AccountId]
                                                      , [hb].[PremiseId]
                                                      , [hb].[ServiceContractId]
                                                      , [hb].[BillEndDate]
                                                      , [hb].[CommodityId]
                                           ORDER BY [hb].[TrackingDate] DESC
                                          ) AS [SortId]
                 FROM   [Holding].[Billing] [hb]
             ) [b]
    WHERE    [b].[SortId] = 2
    ORDER BY [b].[ClientId]
           , [b].[PremiseId]
           , [b].[AccountId]
           , [b].[ServiceContractId]
           , [b].[BillEndDate]
           , [b].[CommodityId];

    SELECT @Count = COUNT(*)
    FROM   [#DuplicateBills];

    SET @ProgressMsg = 'Count of DUPLICATED Bills in [Holding].[Billing] :- ' + CAST(@Count AS VARCHAR(50));
    EXEC [audit].[HoldingBillingDuplicates_RecordProgress] @component = @component
                                                         , @text = @ProgressMsg;

    -- Get contents to be inserted in Holding.B illing_Duplicates
    IF OBJECT_ID('tempdb..#AllDupRecords') IS NOT NULL
        DROP TABLE [#AllDupRecords];
    SELECT ROW_NUMBER() OVER (PARTITION BY [hb].[ClientId]
                                         , [hb].[AccountId]
                                         , [hb].[PremiseId]
                                         , [hb].[ServiceContractId]
                                         , [hb].[BillEndDate]
                                         , [hb].[CommodityId]
                              ORDER BY [hb].[TrackingDate] DESC
                             ) AS [DuplicateNumber]
         , [hb].*
    INTO   [#AllDupRecords]
    FROM   [#DuplicateBills] [db]
           INNER JOIN [Holding].[Billing] [hb] ON [hb].[PremiseId] = [db].[PremiseId]
                                                  AND [hb].[AccountId] = [db].[AccountId]
                                                  AND [hb].[ServiceContractId] = [db].[ServiceContractId]
                                                  AND [hb].[BillEndDate] = [db].[BillEndDate]
                                                  AND [hb].[CommodityId] = [db].[CommodityId]; --(1742 row(s) affected)

    SELECT @Count = COUNT(*)
    FROM   [#AllDupRecords];

    SET @ProgressMsg = 'Total number of duplicates in [Holding].[Billing] :- ' + CAST(@Count AS VARCHAR(50));
    EXEC [audit].[HoldingBillingDuplicates_RecordProgress] @component = @component
                                                         , @text = @ProgressMsg;


    ----Display #AllDupRecords
    --SELECT   *
    --FROM     [#AllDupRecords]
    --ORDER BY [ClientId]
    --       , [PremiseId]
    --       , [AccountId]
    --       , [ServiceContractId]
    --       , [BillEndDate]
    --       , [CommodityId];

    -- Move the Bill and Service Cost Details of the duplicate bills

    SELECT @PreCount = COUNT(*)
    FROM   [Holding].[Billing_BillCostDetails_Duplicates];

    INSERT INTO [Holding].[Billing_BillCostDetails_Duplicates]
                SELECT [bcd].*
                FROM   [Holding].[Billing_BillCostDetails] [bcd]
                       INNER JOIN [#AllDupRecords] [ad] ON [ad].[BillingCostDetailsKey] = [bcd].[BillingCostDetailsKey];

    SELECT @EndCount = COUNT(*)
    FROM   [Holding].[Billing_BillCostDetails_Duplicates];

    SET @Count = @EndCount - @PreCount;
    SET @ProgressMsg = 'Rows inserted into [Holding].[Billing_BillCostDetails_Duplicates] :- '
                       + CAST(@Count AS VARCHAR(50));

    EXEC [audit].[HoldingBillingDuplicates_RecordProgress] @component = @component
                                                         , @text = @ProgressMsg;

    ---****
    SELECT @PreCount = COUNT(*)
    FROM   [Holding].[Billing_ServiceCostDetails_Duplicates];

    INSERT INTO [Holding].[Billing_ServiceCostDetails_Duplicates]
                SELECT [bcd].*
                FROM   [Holding].[Billing_ServiceCostDetails] [bcd]
                       INNER JOIN [#AllDupRecords] [ad] ON [ad].[ServiceCostDetailsKey] = [bcd].[ServiceCostDetailsKey]; --(7174 row(s) affected)

    SELECT @EndCount = COUNT(*)
    FROM   [Holding].[Billing_ServiceCostDetails_Duplicates];

    SET @Count = @EndCount - @PreCount;
    SET @ProgressMsg = 'Rows inserted into [Holding].[Billing_ServiceCostDetails_Duplicates] :- '
                       + CAST(@Count AS VARCHAR(50));

    EXEC [audit].[HoldingBillingDuplicates_RecordProgress] @component = @component
                                                         , @text = @ProgressMsg;

    -- Move the Duplicate bills
    SELECT @PreCount = COUNT(*)
    FROM   [Holding].[Billing_Duplicates];

    INSERT INTO [Holding].[Billing_Duplicates] (
                                                   [DuplicateNumber]
                                                 , [ClientId]
                                                 , [CustomerId]
                                                 , [AccountId]
                                                 , [PremiseId]
                                                 , [ServiceContractId]
                                                 , [ServicePointId]
                                                 , [BillStartDate]
                                                 , [BillEndDate]
                                                 , [BillDays]
                                                 , [TotalUnits]
                                                 , [TotalCost]
                                                 , [CommodityId]
                                                 , [BillPeriodTypeId]
                                                 , [UOMId]
                                                 , [AMIStartDate]
                                                 , [AMIEndDate]
                                                 , [MeterId]
                                                 , [MeterType]
                                                 , [ReplacedMeterId]
                                                 , [RateClass]
                                                 , [DueDate]
                                                 , [ReadDate]
                                                 , [ReadQuality]
                                                 , [BillCycleScheduleId]
                                                 , [SourceId]
                                                 , [TrackingId]
                                                 , [TrackingDate]
                                                 , [ServiceCostDetailsKey]
                                                 , [BillingCostDetailsKey]
                                                 , [UtilityBillRecordId]
                                                 , [CanceledUtilityBillRecordId]
                                                 , [BillDate]
                                               )
                SELECT [DuplicateNumber]
                     , [ClientId]
                     , [CustomerId]
                     , [AccountId]
                     , [PremiseId]
                     , [ServiceContractId]
                     , [ServicePointId]
                     , [BillStartDate]
                     , [BillEndDate]
                     , [BillDays]
                     , [TotalUnits]
                     , [TotalCost]
                     , [CommodityId]
                     , [BillPeriodTypeId]
                     , [UOMId]
                     , [AMIStartDate]
                     , [AMIEndDate]
                     , [MeterId]
                     , [MeterType]
                     , [ReplacedMeterId]
                     , [RateClass]
                     , [DueDate]
                     , [ReadDate]
                     , [ReadQuality]
                     , [BillCycleScheduleId]
                     , [SourceId]
                     , [TrackingId]
                     , [TrackingDate]
                     , [ServiceCostDetailsKey]
                     , [BillingCostDetailsKey]
                     , [UtilityBillRecordId]
                     , [CanceledUtilityBillRecordId]
                     , [BillDate]
                FROM   [#AllDupRecords]; --(1742 row(s) affected)

    SELECT @EndCount = COUNT(*)
    FROM   [Holding].[Billing_Duplicates];

    SET @Count = @EndCount - @PreCount;
    SET @ProgressMsg = 'Rows inserted into [Holding].[Billing_Duplicates] :- ' + CAST(@Count AS VARCHAR(50));

    EXEC [audit].[HoldingBillingDuplicates_RecordProgress] @component = @component
                                                         , @text = @ProgressMsg;



    ------Make sure the records are found and delete them from the Holding table
    SELECT @PreCount = COUNT(*)
    FROM   [Holding].[Billing];

    DELETE [hb]
    FROM [Holding].[Billing] [hb]
         INNER JOIN [Holding].[Billing_Duplicates] [db] ON [hb].[PremiseId] = [db].[PremiseId]
                                                           AND [hb].[AccountId] = [db].[AccountId]
                                                           AND [hb].[ServiceContractId] = [db].[ServiceContractId]
                                                           AND [hb].[BillEndDate] = [db].[BillEndDate]
                                                           AND [hb].[CommodityId] = [db].[CommodityId]
                                                           AND [hb].[UtilityBillRecordId] = [db].[UtilityBillRecordId]
                                                           AND [hb].[CustomerId] = [db].[CustomerId]
                                                           AND [hb].[ServiceCostDetailsKey] = [db].[ServiceCostDetailsKey];

    SELECT @EndCount = COUNT(*)
    FROM   [Holding].[Billing];

    SET @Count = @EndCount - @PreCount;
    SET @ProgressMsg = 'Rows DELETED FROM [Holding].[Billing] :- ' + CAST(@Count AS VARCHAR(50));

    EXEC [audit].[HoldingBillingDuplicates_RecordProgress] @component = @component
                                                         , @text = @ProgressMsg;

    -- Make sure the records are found and delete them from the Holding [Billing_BillCostDetails] table
    SELECT @PreCount = COUNT(*)
    FROM   [Holding].[Billing_BillCostDetails];

    DELETE [bc]
    FROM [Holding].[Billing_BillCostDetails] [bc]
         INNER JOIN [Holding].[Billing_BillCostDetails_Duplicates] [bcd] ON [bc].[BillingCostDetailsKey] = [bcd].[BillingCostDetailsKey]
                                                                            AND [bc].[BillingCostDetailName] = [bcd].[BillingCostDetailName]
                                                                            AND [bc].[BillingCostDetailValue] = [bcd].[BillingCostDetailValue];

    SELECT @EndCount = COUNT(*)
    FROM   [Holding].[Billing_BillCostDetails];

    SET @Count = @EndCount - @PreCount;
    SET @ProgressMsg = 'Rows DELETED FROM [Holding].[Billing_BillCostDetails] :- ' + CAST(@Count AS VARCHAR(50));

    EXEC [audit].[HoldingBillingDuplicates_RecordProgress] @component = @component
                                                         , @text = @ProgressMsg;

    -- Make sure the records are found and delete them from the Holding [Billing_ServiceCostDetails] table
    SELECT @PreCount = COUNT(*)
    FROM   [Holding].[Billing_ServiceCostDetails];


    DELETE [bc]
    FROM [Holding].[Billing_ServiceCostDetails] [bc]
         INNER JOIN [Holding].[Billing_ServiceCostDetails_Duplicates] [bcd] ON [bc].[ServiceCostDetailsKey] = [bcd].[ServiceCostDetailsKey]
                                                                               AND [bc].[ServiceCostDetailName] = [bcd].[ServiceCostDetailName]
                                                                               AND [bc].[ServiceCostDetailValue] = [bcd].[ServiceCostDetailValue];

    SELECT @EndCount = COUNT(*)
    FROM   [Holding].[Billing_ServiceCostDetails];

    SET @Count = @EndCount - @PreCount;
    SET @ProgressMsg = 'Rows DELETED FROM [Holding].[Billing_ServiceCostDetails] :- ' + CAST(@Count AS VARCHAR(50));

    EXEC [audit].[HoldingBillingDuplicates_RecordProgress] @component = @component
                                                         , @text = @ProgressMsg;

    EXEC [audit].[HoldingBillingDuplicates_RecordProgress] @component = @component
                                                         , @text = 'Finished Sproc run';

    EXEC [audit].[ETL_RecordProgress] @component = @component
                                    , @text = 'Finished sproc run. Details in [audit].[HoldingBillingDuplicateLog]';

END;
GO
