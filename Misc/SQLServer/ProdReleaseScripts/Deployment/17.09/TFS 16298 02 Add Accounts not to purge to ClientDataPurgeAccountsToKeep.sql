-- keep these accounts for QA testing
-- select * from dbo.ClientDataPurgeAccountsToKeep

-- from Lydia
insert into dbo.ClientDataPurgeAccountsToKeep
select distinct t1.clientid, t1.premisekey, t3.customerid,t1.accountid, t1.premiseid
from dimpremise t1 join factcustomerpremise t2
	on t1.premisekey = t2.premisekey
join dimcustomer t3
	on t2.customerkey = t3.customerkey
where t1.premiseid in ('1218337000','3CommPrem1','9000E3P','9000G1P','9000G2P','9000G3P','9000G4P','9000BDP','busbakery2','busbakery1',
'9000busegw1','some-premiseid1cleartst','9000buse1','1080543139','Prem20170217_1111','TC87_PremMultiprem','TC87_PremMultiprem2',
'TC87_PremRep1Meter','TC87_PremDelMeter','9000TOU123TEST','resdemogb15','GBelec1','1000001','some-premiseid','QAW67463PREM')
order by t1.premiseid

-- from Irina -- run on Test and 210
insert into dbo.ClientDataPurgeAccountsToKeep
select distinct t1.clientid, t1.premisekey, t3.customerid,t1.accountid, t1.premiseid
from dimpremise t1 join factcustomerpremise t2
	on t1.premisekey = t2.premisekey
join dimcustomer t3
	on t2.customerkey = t3.customerkey
where t1.premiseid in ('PREMID_TFS22619_001','PREMID_TFS22619_002','9350869811','350869811','1330678451','7960018520')
order by t1.premiseid


-- ClientID 4
insert into dbo.ClientDataPurgeAccountsToKeep
select t1.clientid, t1.premisekey, t3.customerid,t1.accountid, t1.premiseid
from dimpremise t1 join factcustomerpremise t2
	on t1.premisekey = t2.premisekey
join dimcustomer t3
	on t2.customerkey = t3.customerkey
where t1.clientid = 4
and not exists (select * from dbo.ClientDataPurgeAccountsToKeep t5
	where t1.clientid = t5.clientid
	and t1.premisekey = t5.premisekey
	and t3.customerid = t5.customerid
	and t1.accountid = t5.accountid
	and t1.premiseid = t5.premiseid)
order by t1.premiseid
