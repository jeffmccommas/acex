﻿
/********************************************************/
/***** RUN ONLY ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  Table [ETL].[INS_FactBilling]    Script Date: 8/28/2017 2:39:21 PM ******/
DROP TABLE IF EXISTS [ETL].[INS_FactBilling]
GO

/****** Object:  Table [ETL].[INS_FactBilling]    Script Date: 8/28/2017 2:39:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ETL].[INS_FactBilling]') AND type in (N'U'))
BEGIN
CREATE TABLE [ETL].[INS_FactBilling](
    [ClientId] [int] NOT NULL,
    [PremiseId] [varchar](50) NOT NULL,
    [AccountId] [varchar](50) NOT NULL,
    [ServiceContractId] [varchar](64) NOT NULL,
    [StartDate] [datetime] NOT NULL,
    [EndDate] [datetime] NOT NULL,
    [CommodityId] [int] NOT NULL,
    [BillPeriodTypeId] [int] NOT NULL,
    [BillCycleScheduleId] [varchar](50) NULL,
    [UOMId] [int] NOT NULL,
    [SourceId] [int] NOT NULL,
    [TotalUnits] [decimal](18, 2) NOT NULL,
    [TotalCost] [decimal](18, 2) NOT NULL,
    [BillDays] [int] NOT NULL,
    [RateClass] [varchar](256) NULL,
    [ReadQuality] [varchar](50) NULL,
    [ReadDate] [datetime] NULL,
    [DueDate] [date] NULL,
    [ETL_LogId] [int] NOT NULL,
    [TrackingId] [varchar](50) NOT NULL,
    [TrackingDate] [datetime] NOT NULL,
    [BillingCostDetailsKey] [uniqueidentifier] NULL,
    [ServiceCostDetailsKey] [uniqueidentifier] NULL,
    [UtilityBillRecordId] [varchar](64) NULL,
    [CanceledUtilityBillRecordId] [varchar](64) NULL,
    [BillDate] [datetime] NULL
 CONSTRAINT [PK_INS_FactBilling] PRIMARY KEY CLUSTERED
(
    [ClientId] ASC,
    [PremiseId] ASC,
    [AccountId] ASC,
    [ServiceContractId] ASC,
    [EndDate] ASC,
    [CommodityId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
END
GO
