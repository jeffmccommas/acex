/*
Run this script on:

aceUsql1c0.database.windows.net.Insights    -  This database will be modified

to synchronize it with:

aceQsql1c0.database.windows.net.Insights

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 11.1.3 from Red Gate Software Ltd at 1/12/2016 12:00:45 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

PRINT(N'Drop constraints from [cp].[ControlPanelLoginClient]')
ALTER TABLE [cp].[ControlPanelLoginClient] DROP CONSTRAINT [FK_ControlPanelLoginClient_ControlPanelLogin]

PRINT(N'Drop constraints from [dbo].[UserEndpoint]')
ALTER TABLE [dbo].[UserEndpoint] DROP CONSTRAINT [FK_UserEndpoint_Endpoint]
ALTER TABLE [dbo].[UserEndpoint] DROP CONSTRAINT [FK_UserEndpoint_User]

PRINT(N'Drop constraints from [dbo].[User]')
ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_User_Client]

PRINT(N'Drop constraint FK_UserEnvironment_User from [dbo].[UserEnvironment]')
ALTER TABLE [dbo].[UserEnvironment] DROP CONSTRAINT [FK_UserEnvironment_User]

PRINT(N'Drop constraint FK_UserRole_User from [dbo].[UserRole]')
ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK_UserRole_User]

PRINT(N'Update row in [dbo].[UserEndpoint]')
UPDATE [dbo].[UserEndpoint] SET [EnableInd]=0 WHERE [UserID]=31 AND [EndpointID]=0

PRINT(N'Update row in [cp].[ControlPanelLogin]')
UPDATE [cp].[ControlPanelLogin] SET [UpdDate]='2016-01-07 16:09:13.537' WHERE [CpID]=15

PRINT(N'Update row in [dbo].[User]')
UPDATE [dbo].[User] SET [ActorName]='Web Users', [UpdDate]='2016-01-07 17:14:27.750' WHERE [UserID]=31

PRINT(N'Add row to [cp].[ControlPanelLogin]')
SET IDENTITY_INSERT [cp].[ControlPanelLogin] ON
INSERT INTO [cp].[ControlPanelLogin] ([CpID], [FirstName], [LastName], [UserName], [PasswordHash], [PasswordSalt], [SuperUserInd], [EnableInd], [NewDate], [UpdDate]) VALUES (45, 'Paul', 'Cole', 'Pcole', '8MzQg9eazewBnBapRznzpbEFUjDZ6G0/UnutS2eEJv0=', 'B22804394E581472E507001A288812D4', 0, 1, '2016-01-07 16:09:58.110', '2016-01-07 16:10:13.727')
SET IDENTITY_INSERT [cp].[ControlPanelLogin] OFF

PRINT(N'Add rows to [cp].[ControlPanelLoginClient]')
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (15, 283)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (15, 766)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (15, 6539)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (15, 54654)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (15, 122469)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (45, 4)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (45, 61)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (45, 87)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (45, 283)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (45, 6539)
PRINT(N'Operation applied to 10 rows out of 10')

PRINT(N'Add constraints to [cp].[ControlPanelLoginClient]')
ALTER TABLE [cp].[ControlPanelLoginClient] ADD CONSTRAINT [FK_ControlPanelLoginClient_ControlPanelLogin] FOREIGN KEY ([CpID]) REFERENCES [cp].[ControlPanelLogin] ([CpID])

PRINT(N'Add constraints to [dbo].[UserEndpoint]')
ALTER TABLE [dbo].[UserEndpoint] ADD CONSTRAINT [FK_UserEndpoint_Endpoint] FOREIGN KEY ([EndpointID]) REFERENCES [dbo].[Endpoint] ([EndpointID])
ALTER TABLE [dbo].[UserEndpoint] ADD CONSTRAINT [FK_UserEndpoint_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])

PRINT(N'Add constraints to [dbo].[User]')
ALTER TABLE [dbo].[User] ADD CONSTRAINT [FK_User_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])

PRINT(N'Add constraint FK_UserEnvironment_User to [dbo].[UserEnvironment]')
ALTER TABLE [dbo].[UserEnvironment] WITH NOCHECK ADD CONSTRAINT [FK_UserEnvironment_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])

PRINT(N'Add constraint FK_UserRole_User to [dbo].[UserRole]')
ALTER TABLE [dbo].[UserRole] WITH NOCHECK ADD CONSTRAINT [FK_UserRole_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])
COMMIT TRANSACTION
GO
