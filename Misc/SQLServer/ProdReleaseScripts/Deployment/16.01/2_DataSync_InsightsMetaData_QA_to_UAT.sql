/*
Run this script on:

aceUsql1c0.database.windows.net.InsightsMetadata    -  This database will be modified

to synchronize it with:

aceQsql1c0.database.windows.net.InsightsMetadata

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 11.1.3 from Red Gate Software Ltd at 1/12/2016 3:27:57 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

PRINT(N'Drop constraints from [cm].[ClientTextContentLocaleContent]')
ALTER TABLE [cm].[ClientTextContentLocaleContent] DROP CONSTRAINT [FK_ClientTextContentLocaleContent_ClientTextContent]
ALTER TABLE [cm].[ClientTextContentLocaleContent] DROP CONSTRAINT [FK_ClientTextContentLocaleContent_TypeLocale]

PRINT(N'Drop constraints from [cm].[ClientActionCondition]')
ALTER TABLE [cm].[ClientActionCondition] DROP CONSTRAINT [FK_ClientActionCondition_ClientAction]
ALTER TABLE [cm].[ClientActionCondition] DROP CONSTRAINT [FK_ClientActionCondition_TypeCondition]

PRINT(N'Drop constraints from [cm].[ClientWidget]')
ALTER TABLE [cm].[ClientWidget] DROP CONSTRAINT [FK_ClientWidget_TypeTextContent]
ALTER TABLE [cm].[ClientWidget] DROP CONSTRAINT [FK_ClientWidget_TypeTab]
ALTER TABLE [cm].[ClientWidget] DROP CONSTRAINT [FK_ClientWidget_TypeWidget]
ALTER TABLE [cm].[ClientWidget] DROP CONSTRAINT [FK_ClientWidget_TypeWidgetType]

PRINT(N'Drop constraint FK_ClientWidgetConditionality_ClientWidget from [cm].[ClientWidgetCondition]')
ALTER TABLE [cm].[ClientWidgetCondition] DROP CONSTRAINT [FK_ClientWidgetConditionality_ClientWidget]

PRINT(N'Drop constraint FK_ClientWidgetConfigurationality_ClientWidget from [cm].[ClientWidgetConfiguration]')
ALTER TABLE [cm].[ClientWidgetConfiguration] DROP CONSTRAINT [FK_ClientWidgetConfigurationality_ClientWidget]

PRINT(N'Drop constraint FK_ClientWidgetTextContentality_ClientWidget from [cm].[ClientWidgetTextContent]')
ALTER TABLE [cm].[ClientWidgetTextContent] DROP CONSTRAINT [FK_ClientWidgetTextContentality_ClientWidget]

PRINT(N'Drop constraints from [cm].[ClientTextContent]')
ALTER TABLE [cm].[ClientTextContent] DROP CONSTRAINT [FK_ClientTextContent_TypeCategory]
ALTER TABLE [cm].[ClientTextContent] DROP CONSTRAINT [FK_ClientTextContent_Client]
ALTER TABLE [cm].[ClientTextContent] DROP CONSTRAINT [FK_ClientTextContent_TypeTextContent]

PRINT(N'Drop constraints from [cm].[ClientTab]')
ALTER TABLE [cm].[ClientTab] DROP CONSTRAINT [FK_ClientTab_TypeLayout]
ALTER TABLE [cm].[ClientTab] DROP CONSTRAINT [FK_ClientTab_TypeTextContent]
ALTER TABLE [cm].[ClientTab] DROP CONSTRAINT [FK_ClientTab_TypeTab]
ALTER TABLE [cm].[ClientTab] DROP CONSTRAINT [FK_ClientTab_TypeTabType]

PRINT(N'Drop constraint FK_ClientTabAction_ClientTab from [cm].[ClientTabAction]')
ALTER TABLE [cm].[ClientTabAction] DROP CONSTRAINT [FK_ClientTabAction_ClientTab]

PRINT(N'Drop constraint FK_ClientTabChildTab_ClientTab from [cm].[ClientTabChildTab]')
ALTER TABLE [cm].[ClientTabChildTab] DROP CONSTRAINT [FK_ClientTabChildTab_ClientTab]

PRINT(N'Drop constraint FK_ClientTabConditionality_ClientTab from [cm].[ClientTabCondition]')
ALTER TABLE [cm].[ClientTabCondition] DROP CONSTRAINT [FK_ClientTabConditionality_ClientTab]

PRINT(N'Drop constraint FK_ClientTabConfigurationality_ClientTab from [cm].[ClientTabConfiguration]')
ALTER TABLE [cm].[ClientTabConfiguration] DROP CONSTRAINT [FK_ClientTabConfigurationality_ClientTab]

PRINT(N'Drop constraint FK_ClientTabProfileSection_ClientTab from [cm].[ClientTabProfileSection]')
ALTER TABLE [cm].[ClientTabProfileSection] DROP CONSTRAINT [FK_ClientTabProfileSection_ClientTab]

PRINT(N'Drop constraint FK_ClientTabTextContentality_ClientTab from [cm].[ClientTabTextContent]')
ALTER TABLE [cm].[ClientTabTextContent] DROP CONSTRAINT [FK_ClientTabTextContentality_ClientTab]

PRINT(N'Drop constraints from [cm].[ClientConfiguration]')
ALTER TABLE [cm].[ClientConfiguration] DROP CONSTRAINT [FK_ClientConfiguration_TypeCategory]
ALTER TABLE [cm].[ClientConfiguration] DROP CONSTRAINT [FK_ClientConfiguration_Client]
ALTER TABLE [cm].[ClientConfiguration] DROP CONSTRAINT [FK_ClientConfiguration_TypeConfiguration]
ALTER TABLE [cm].[ClientConfiguration] DROP CONSTRAINT [FK_ClientConfiguration_TypeEnvironment]

PRINT(N'Drop constraints from [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeAction]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeActionType]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeUom]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeUom1]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_Client]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeCommodity]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeExpression]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeDifficulty]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeHabitInterval]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_ClientFileContent2]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeTextContent]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeTextContent1]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeNextStepLinkType]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeExpression1]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_ClientFileContent]
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_ClientFileContent3]

PRINT(N'Drop constraint FK_ClientActionAppliance_ClientAction from [cm].[ClientActionAppliance]')
ALTER TABLE [cm].[ClientActionAppliance] DROP CONSTRAINT [FK_ClientActionAppliance_ClientAction]

PRINT(N'Drop constraint FK_ClientActionSeasonality_ClientAction from [cm].[ClientActionSeason]')
ALTER TABLE [cm].[ClientActionSeason] DROP CONSTRAINT [FK_ClientActionSeasonality_ClientAction]

PRINT(N'Drop constraint FK_ClientActionWhatIfData_ClientAction from [cm].[ClientActionWhatIfData]')
ALTER TABLE [cm].[ClientActionWhatIfData] DROP CONSTRAINT [FK_ClientActionWhatIfData_ClientAction]

PRINT(N'Drop constraint FK_ClientAppliance_TypeTextContent from [cm].[ClientAppliance]')
ALTER TABLE [cm].[ClientAppliance] DROP CONSTRAINT [FK_ClientAppliance_TypeTextContent]

PRINT(N'Drop constraint FK_ClientCommodity_TypeTextContent from [cm].[ClientCommodity]')
ALTER TABLE [cm].[ClientCommodity] DROP CONSTRAINT [FK_ClientCommodity_TypeTextContent]

PRINT(N'Drop constraint FK_ClientCurrency_TypeTextContent from [cm].[ClientCurrency]')
ALTER TABLE [cm].[ClientCurrency] DROP CONSTRAINT [FK_ClientCurrency_TypeTextContent]

PRINT(N'Drop constraint FK_ClientCurrency_TypeTextContent1 from [cm].[ClientCurrency]')
ALTER TABLE [cm].[ClientCurrency] DROP CONSTRAINT [FK_ClientCurrency_TypeTextContent1]

PRINT(N'Drop constraint FK_ClientEnduse_TypeTextContent from [cm].[ClientEnduse]')
ALTER TABLE [cm].[ClientEnduse] DROP CONSTRAINT [FK_ClientEnduse_TypeTextContent]

PRINT(N'Drop constraint FK_ClientMeasurement_TypeTextContent from [cm].[ClientMeasurement]')
ALTER TABLE [cm].[ClientMeasurement] DROP CONSTRAINT [FK_ClientMeasurement_TypeTextContent]

PRINT(N'Drop constraint FK_ClientProfileAttribute_TypeTextContent from [cm].[ClientProfileAttribute]')
ALTER TABLE [cm].[ClientProfileAttribute] DROP CONSTRAINT [FK_ClientProfileAttribute_TypeTextContent]

PRINT(N'Drop constraint FK_ClientProfileOption_TypeTextContent from [cm].[ClientProfileOption]')
ALTER TABLE [cm].[ClientProfileOption] DROP CONSTRAINT [FK_ClientProfileOption_TypeTextContent]

PRINT(N'Drop constraint FK_ClientProfileSection_TypeTextContent from [cm].[ClientProfileSection]')
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent]

PRINT(N'Drop constraint FK_ClientProfileSection_TypeTextContent3 from [cm].[ClientProfileSection]')
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent3]

PRINT(N'Drop constraint FK_ClientProfileSection_TypeTextContent2 from [cm].[ClientProfileSection]')
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent2]

PRINT(N'Drop constraint FK_ClientProfileSection_TypeTextContent1 from [cm].[ClientProfileSection]')
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent1]

PRINT(N'Drop constraint FK_ClientSeason_TypeTextContent from [cm].[ClientSeason]')
ALTER TABLE [cm].[ClientSeason] DROP CONSTRAINT [FK_ClientSeason_TypeTextContent]

PRINT(N'Drop constraint FK_ClientTabTextContentality_TypeTextContentality from [cm].[ClientTabTextContent]')
ALTER TABLE [cm].[ClientTabTextContent] DROP CONSTRAINT [FK_ClientTabTextContentality_TypeTextContentality]

PRINT(N'Drop constraint FK_ClientUOM_ClientUOM1 from [cm].[ClientUOM]')
ALTER TABLE [cm].[ClientUOM] DROP CONSTRAINT [FK_ClientUOM_ClientUOM1]

PRINT(N'Drop constraint FK_ClientWidgetTextContentality_TypeTextContentality from [cm].[ClientWidgetTextContent]')
ALTER TABLE [cm].[ClientWidgetTextContent] DROP CONSTRAINT [FK_ClientWidgetTextContentality_TypeTextContentality]

PRINT(N'Drop constraint FK_ClientTabConfigurationality_TypeConfigurationality from [cm].[ClientTabConfiguration]')
ALTER TABLE [cm].[ClientTabConfiguration] DROP CONSTRAINT [FK_ClientTabConfigurationality_TypeConfigurationality]

PRINT(N'Drop constraint FK_ClientWidgetConfigurationality_TypeConfigurationality from [cm].[ClientWidgetConfiguration]')
ALTER TABLE [cm].[ClientWidgetConfiguration] DROP CONSTRAINT [FK_ClientWidgetConfigurationality_TypeConfigurationality]

PRINT(N'Drop constraint FK_ClientActionSavings_TypeAction from [cm].[ClientActionSavings]')
ALTER TABLE [cm].[ClientActionSavings] DROP CONSTRAINT [FK_ClientActionSavings_TypeAction]

PRINT(N'Drop constraint FK_ClientTabAction_TypeAction from [cm].[ClientTabAction]')
ALTER TABLE [cm].[ClientTabAction] DROP CONSTRAINT [FK_ClientTabAction_TypeAction]

PRINT(N'Drop constraints from [cm].[ClientCondition]')
ALTER TABLE [cm].[ClientCondition] DROP CONSTRAINT [FK_ClientCondition_TypeCategory]
ALTER TABLE [cm].[ClientCondition] DROP CONSTRAINT [FK_ClientCondition_Client]
ALTER TABLE [cm].[ClientCondition] DROP CONSTRAINT [FK_ClientCondition_TypeCondition]
ALTER TABLE [cm].[ClientCondition] DROP CONSTRAINT [FK_ClientCondition_TypeProfileAttribute]
ALTER TABLE [cm].[ClientCondition] DROP CONSTRAINT [FK_ClientCondition_TypeProfileOption]

PRINT(N'Drop constraints from [cm].[ClientAssetLocaleContent]')
ALTER TABLE [cm].[ClientAssetLocaleContent] DROP CONSTRAINT [FK_ClientAssetLocaleContent_ClientAsset]
ALTER TABLE [cm].[ClientAssetLocaleContent] DROP CONSTRAINT [FK_ClientAssetLocaleContent_TypeLocale]

PRINT(N'Update rows in [cm].[ClientTextContentLocaleContent]')
UPDATE [cm].[ClientTextContentLocaleContent] SET [ShortText]=N'Overview', [MediumText]=N'Overview', [LongText]=N'Overview' WHERE [ClientTextContentID]=5464 AND [LocaleKey]='en-US'
UPDATE [cm].[ClientTextContentLocaleContent] SET [ShortText]=N'Descripción general', [MediumText]=N'Descripción general', [LongText]=N'Descripción general' WHERE [ClientTextContentID]=5464 AND [LocaleKey]='es-ES'
UPDATE [cm].[ClientTextContentLocaleContent] SET [ShortText]=N'Обзор', [MediumText]=N'Обзор', [LongText]=N'Обзор' WHERE [ClientTextContentID]=5464 AND [LocaleKey]='ru-RU'
PRINT(N'Operation applied to 3 rows out of 3')

PRINT(N'Update rows in [cm].[ClientTab]')
UPDATE [cm].[ClientTab] SET [MenuOrder]=1, [TabIconType]=NULL, [TabIconClass]=NULL, [Images]=NULL WHERE [ClientTabID]=1181
UPDATE [cm].[ClientTab] SET [TabIconType]=NULL, [TabIconClass]=NULL, [Images]=NULL WHERE [ClientTabID]=1182
UPDATE [cm].[ClientTab] SET [MenuOrder]=3, [TabIconType]=NULL, [TabIconClass]=NULL, [Images]=NULL WHERE [ClientTabID]=1183
UPDATE [cm].[ClientTab] SET [MenuOrder]=5, [URL]='C5', [TabIconType]=NULL, [TabIconClass]=NULL, [Images]=NULL WHERE [ClientTabID]=1186
PRINT(N'Operation applied to 4 rows out of 4')

PRINT(N'Update rows in [cm].[ClientConfiguration]')
UPDATE [cm].[ClientConfiguration] SET [Value]='true' WHERE [ClientConfigurationID]=1662
UPDATE [cm].[ClientConfiguration] SET [Value]='tmpImportEvent_SentReport' WHERE [ClientConfigurationID]=2084
UPDATE [cm].[ClientConfiguration] SET [Value]='id=4' WHERE [ClientConfigurationID]=2094
UPDATE [cm].[ClientConfiguration] SET [Value]='1' WHERE [ClientConfigurationID]=2158
PRINT(N'Operation applied to 4 rows out of 4')

PRINT(N'Update rows in [cm].[ClientAction]')
UPDATE [cm].[ClientAction] SET [ActionPriority]=2 WHERE [ClientActionID]=1515
UPDATE [cm].[ClientAction] SET [ActionPriority]=1 WHERE [ClientActionID]=1516
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Update row in [cm].[ClientCondition]')
UPDATE [cm].[ClientCondition] SET [ProfileOptionKey]='refrigerator.year.2000' WHERE [ClientConditionID]=127

PRINT(N'Update rows in [cm].[ClientAssetLocaleContent]')
UPDATE [cm].[ClientAssetLocaleContent] SET [FileName]='csr image for page one.png', [FileSize]=221494, [FileUrl]='//images.contentful.com/wp32f8j5jqi2/2OydzrCKdOC6w0GcaiQgOI/3e4ed5b5d5a920c8f670d3a50f85edfd/csr_image_for_page_one.png' WHERE [ClientAssetID]=561 AND [LocaleKey]='en-US'
UPDATE [cm].[ClientAssetLocaleContent] SET [FileName]='csr image for page one.png', [FileSize]=221494, [FileUrl]='//images.contentful.com/wp32f8j5jqi2/2OydzrCKdOC6w0GcaiQgOI/6b4a2f199f874e9098aca6c1f178d613/csr_image_for_page_one.png' WHERE [ClientAssetID]=561 AND [LocaleKey]='es-ES'
UPDATE [cm].[ClientAssetLocaleContent] SET [FileName]='csr image for page one.png', [FileSize]=221494, [FileUrl]='//images.contentful.com/wp32f8j5jqi2/2OydzrCKdOC6w0GcaiQgOI/aebfd2f4727a6618859e61e51670893f/csr_image_for_page_one.png' WHERE [ClientAssetID]=561 AND [LocaleKey]='ru-RU'
PRINT(N'Operation applied to 3 rows out of 3')

PRINT(N'Add rows to [cm].[TypeAction]')
INSERT INTO [cm].[TypeAction] ([ActionKey], [ActionID]) VALUES ('insight.demosmecofrig', 256)
INSERT INTO [cm].[TypeAction] ([ActionKey], [ActionID]) VALUES ('insight.demosmecostore', 257)
INSERT INTO [cm].[TypeAction] ([ActionKey], [ActionID]) VALUES ('insight.demosmecostrip', 258)
INSERT INTO [cm].[TypeAction] ([ActionKey], [ActionID]) VALUES ('insight.demosmecowater', 259)
PRINT(N'Operation applied to 4 rows out of 4')

PRINT(N'Add row to [cm].[TypeConfiguration]')
INSERT INTO [cm].[TypeConfiguration] ([ConfigurationKey], [ConfigurationID]) VALUES ('package.sentreport_event.eventtypeid', 1744)

PRINT(N'Add rows to [cm].[TypeTextContent]')
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('insight.demosmecofrig.linktext', 2713)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('insight.demosmecofrig.name', 2714)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('insight.demosmecostore.linktext', 2715)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('insight.demosmecostore.name', 2716)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('insight.demosmecostrip.linktext', 2717)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('insight.demosmecostrip.name', 2718)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('insight.demosmecowater.linktext', 2719)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('insight.demosmecowater.name', 2720)
PRINT(N'Operation applied to 8 rows out of 8')

PRINT(N'Add rows to [cm].[ClientAction]')
SET IDENTITY_INSERT [cm].[ClientAction] ON
INSERT INTO [cm].[ClientAction] ([ClientActionID], [ClientID], [ActionKey], [ActionTypeKey], [Hide], [Disable], [CommodityKey], [DifficultyKey], [HabitIntervalKey], [NameKey], [DescriptionKey], [AnnualCost], [UpfrontCost], [AnnualSavingsEstimate], [AnnualUsageSavingsEstimate], [AnnualUsageSavingsUomKey], [CostVariancePercent], [PaybackTime], [ROI], [RebateAvailable], [RebateAmount], [RebateUrl], [RebateImageKey], [NextStepLinkText], [ImageKey], [VideoKey], [Comments], [ActionPriority], [SavingsCalcMethod], [SavingsAmount], [CostExpression], [RebateExpression], [NextStepLink], [NextStepLinkType], [Tags], [IconClass]) VALUES (1520, 87, 'insight.demosmecofrig', 'insights', 0, 0, 'gas', 'medium', 'onetime', 'insight.demosmecofrig.name', 'common.undefined', 0.00, 0.00, 0.00, 0.00, NULL, 0.00, 0, 0.00, 0, 0.00, 'http://www.aclara.com', 'image.democsr1', 'insight.demosmecofrig.linktext', 'laptopman.image', NULL, 'For SMECO demo', 4, 'None', 0.00, NULL, 'expression.improveinsulation.cost', 'https://www.smecoenergyshop.com/products/list/category/refrigerators', 'external', NULL, 'icon-bulb')
INSERT INTO [cm].[ClientAction] ([ClientActionID], [ClientID], [ActionKey], [ActionTypeKey], [Hide], [Disable], [CommodityKey], [DifficultyKey], [HabitIntervalKey], [NameKey], [DescriptionKey], [AnnualCost], [UpfrontCost], [AnnualSavingsEstimate], [AnnualUsageSavingsEstimate], [AnnualUsageSavingsUomKey], [CostVariancePercent], [PaybackTime], [ROI], [RebateAvailable], [RebateAmount], [RebateUrl], [RebateImageKey], [NextStepLinkText], [ImageKey], [VideoKey], [Comments], [ActionPriority], [SavingsCalcMethod], [SavingsAmount], [CostExpression], [RebateExpression], [NextStepLink], [NextStepLinkType], [Tags], [IconClass]) VALUES (1521, 87, 'insight.demosmecostore', 'insights', 0, 0, 'gas', 'medium', 'onetime', 'insight.demosmecostore.name', 'common.undefined', 0.00, 0.00, 0.00, 0.00, NULL, 0.00, 0, 0.00, 0, 0.00, 'http://www.aclara.com', 'image.democsr1', 'insight.demosmecostore.linktext', 'image.161681534ledlightindesklamp', NULL, 'For SMECO demo', 1, 'None', 0.00, NULL, 'expression.improveinsulation.cost', 'https://smeco.amcgmarketplace.com/store', 'external', NULL, 'icon-dollar')
INSERT INTO [cm].[ClientAction] ([ClientActionID], [ClientID], [ActionKey], [ActionTypeKey], [Hide], [Disable], [CommodityKey], [DifficultyKey], [HabitIntervalKey], [NameKey], [DescriptionKey], [AnnualCost], [UpfrontCost], [AnnualSavingsEstimate], [AnnualUsageSavingsEstimate], [AnnualUsageSavingsUomKey], [CostVariancePercent], [PaybackTime], [ROI], [RebateAvailable], [RebateAmount], [RebateUrl], [RebateImageKey], [NextStepLinkText], [ImageKey], [VideoKey], [Comments], [ActionPriority], [SavingsCalcMethod], [SavingsAmount], [CostExpression], [RebateExpression], [NextStepLink], [NextStepLinkType], [Tags], [IconClass]) VALUES (1522, 87, 'insight.demosmecostrip', 'insights', 0, 0, 'gas', 'medium', 'onetime', 'insight.demosmecostrip.name', 'common.undefined', 0.00, 0.00, 0.00, 0.00, NULL, 0.00, 0, 0.00, 0, 0.00, 'http://www.aclara.com', 'image.democsr1', 'insight.demosmecostrip.linktext', 'image.243504595outlets', NULL, 'For SMECO demo', 3, 'None', 0.00, NULL, 'expression.improveinsulation.cost', 'https://smeco.amcgmarketplace.com/store/product/tricklestar-smart-strip-power-strip/', 'external', NULL, 'icon-bulb')
INSERT INTO [cm].[ClientAction] ([ClientActionID], [ClientID], [ActionKey], [ActionTypeKey], [Hide], [Disable], [CommodityKey], [DifficultyKey], [HabitIntervalKey], [NameKey], [DescriptionKey], [AnnualCost], [UpfrontCost], [AnnualSavingsEstimate], [AnnualUsageSavingsEstimate], [AnnualUsageSavingsUomKey], [CostVariancePercent], [PaybackTime], [ROI], [RebateAvailable], [RebateAmount], [RebateUrl], [RebateImageKey], [NextStepLinkText], [ImageKey], [VideoKey], [Comments], [ActionPriority], [SavingsCalcMethod], [SavingsAmount], [CostExpression], [RebateExpression], [NextStepLink], [NextStepLinkType], [Tags], [IconClass]) VALUES (1523, 87, 'insight.demosmecowater', 'insights', 0, 0, 'gas', 'medium', 'onetime', 'insight.demosmecowater.name', 'common.undefined', 0.00, 0.00, 0.00, 0.00, NULL, 0.00, 0, 0.00, 0, 0.00, 'http://www.aclara.com', 'image.democsr1', 'insight.demosmecowater.linktext', 'image.246858241faucetmodern', NULL, 'For SMECO demo', 2, 'None', 0.00, NULL, 'expression.improveinsulation.cost', 'https://smeco.amcgmarketplace.com/store/water-conservation-products', 'external', NULL, 'icon-drop')
SET IDENTITY_INSERT [cm].[ClientAction] OFF
PRINT(N'Operation applied to 4 rows out of 4')

PRINT(N'Add rows to [cm].[ClientConfiguration]')
SET IDENTITY_INSERT [cm].[ClientConfiguration] ON
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (2201, 0, 'prod', 'package.sentreport_event.eventtypeid', 'system', 'eventtypeid', '7')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (2202, 101, 'dev', 'package.sentreport_event.outputfolder', 'system', 'destination folder', '\\discovery\reporting\BI_Events_DEV\101\')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (2203, 101, 'prod', 'package.sentreport_event.outputfolder', 'system', 'destination folder', '\\discovery\reporting\BI_Events\101\')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (2204, 101, 'qa', 'package.sentreport_event.outputfolder', 'system', 'destination folder', '\\discovery\reporting\BI_Events_QA\101\')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (2205, 101, 'uat', 'package.sentreport_event.outputfolder', 'system', 'destination folder', '\\discovery\reporting\BI_Events_UAT\101\')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (2206, 224, 'dev', 'package.sentreport_event.outputfolder', 'system', 'destination folder', '\\discovery\reporting\BI_Events_DEV\224\')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (2207, 224, 'prod', 'package.sentreport_event.outputfolder', 'system', 'destination folder', '\\discovery\reporting\BI_Events\224\')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (2208, 224, 'qa', 'package.sentreport_event.outputfolder', 'system', 'destination folder', '\\discovery\reporting\BI_Events_QA\224\')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (2209, 224, 'uat', 'package.sentreport_event.outputfolder', 'system', 'destination folder', '\\discovery\reporting\BI_Events_UAT\224\')
INSERT INTO [cm].[ClientConfiguration] ([ClientConfigurationID], [ClientID], [EnvironmentKey], [ConfigurationKey], [CategoryKey], [Description], [Value]) VALUES (2210, 276, 'prod', 'tab.dashboard.insights.showviewalllink', 'insights', 'If true then show the View All link. Else do not show this link.', 'false')
SET IDENTITY_INSERT [cm].[ClientConfiguration] OFF
PRINT(N'Operation applied to 10 rows out of 10')

PRINT(N'Add row to [cm].[ClientTab]')
SET IDENTITY_INSERT [cm].[ClientTab] ON
INSERT INTO [cm].[ClientTab] ([ClientTabID], [ClientID], [TabKey], [Name], [MenuOrder], [LayoutKey], [TabTypeKey], [NameKey], [URL], [WidgetKey], [IsUrlInternal], [TabIconType], [TabIconClass], [Images], [Disable]) VALUES (1187, 283, 'tab.waystosave', 'Ways to Save', 4, 'layout.1row2columnslarge', 'main', 'tab.waystosave.name', 'C4', NULL, 1, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [cm].[ClientTab] OFF

PRINT(N'Add rows to [cm].[ClientTextContent]')
SET IDENTITY_INSERT [cm].[ClientTextContent] ON
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (5488, 0, 'insight.demosmecofrig.linktext', 'action')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (5489, 0, 'insight.demosmecofrig.name', 'action')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (5490, 0, 'insight.demosmecostore.linktext', 'action')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (5491, 0, 'insight.demosmecostore.name', 'action')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (5492, 0, 'insight.demosmecostrip.linktext', 'action')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (5493, 0, 'insight.demosmecostrip.name', 'action')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (5494, 0, 'insight.demosmecowater.linktext', 'action')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (5495, 0, 'insight.demosmecowater.name', 'action')
SET IDENTITY_INSERT [cm].[ClientTextContent] OFF
PRINT(N'Operation applied to 8 rows out of 8')

PRINT(N'Add row to [cm].[ClientWidget]')
SET IDENTITY_INSERT [cm].[ClientWidget] ON
INSERT INTO [cm].[ClientWidget] ([ClientWidgetID], [ClientID], [WidgetKey], [Name], [TabKey], [WidgetTypeKey], [WidgetColumn], [WidgetRow], [WidgetOrder], [Disable], [IntroTextKey]) VALUES (177, 283, 'waystosave.setgoal', 'Ways to Save: Set Goal', 'tab.waystosave', 'setgoal', 2, 1, 1, 1, 'common.undefined')
SET IDENTITY_INSERT [cm].[ClientWidget] OFF

PRINT(N'Add row to [cm].[ClientActionCondition]')
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (1520, 'refrigerator.old', 0)

PRINT(N'Add rows to [cm].[ClientTextContentLocaleContent]')
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5488, 'en-US', N'Learn more', N'Learn more', N'Learn more', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5488, 'es-ES', N'Más información', N'Más información', N'Más información', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5488, 'ru-RU', N'Подробнее', N'Подробнее', N'Подробнее', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5489, 'en-US', N'__Know before you go.__ research which refrigerators will qualify for a rebate.', N'__Know before you go.__ research which refrigerators will qualify for a rebate.', N'__Know before you go.__ research which refrigerators will qualify for a rebate.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5489, 'es-ES', N'__Know antes de irte. investigación __ que refrigeradores tendrá derecho a un reembolso.', N'__Know antes de irte. investigación __ que refrigeradores tendrá derecho a un reembolso.', N'__Know antes de irte. investigación __ que refrigeradores tendrá derecho a un reembolso.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5489, 'ru-RU', N'__Know прежде чем вы идете. ___ исследование, которое холодильники будет претендовать на скидку.', N'__Know прежде чем вы идете. ___ исследование, которое холодильники будет претендовать на скидку.', N'__Know прежде чем вы идете. ___ исследование, которое холодильники будет претендовать на скидку.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5490, 'en-US', N'Shop Now', N'Shop Now', N'Shop Now', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5490, 'es-ES', N'Comprar ahora', N'Comprar ahora', N'Comprar ahora', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5490, 'ru-RU', N'Купить сейчас', N'Купить сейчас', N'Купить сейчас', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5491, 'en-US', N'__Upgrade your lighting now and save.__ Check out these special offers on the best bulbs for your home.', N'__Upgrade your lighting now and save.__ Check out these special offers on the best bulbs for your home.', N'__Upgrade your lighting now and save.__ Check out these special offers on the best bulbs for your home.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5491, 'es-ES', N'__Upgrade ahora su iluminación y ahorra. __ revisa estas ofertas especiales en los mejores bulbos para su hogar.', N'__Upgrade ahora su iluminación y ahorra. __ revisa estas ofertas especiales en los mejores bulbos para su hogar.', N'__Upgrade ahora su iluminación y ahorra. __ revisa estas ofertas especiales en los mejores bulbos para su hogar.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5491, 'ru-RU', N'__Upgrade вашей освещения сейчас и сохранить. __ проверить эти специальные предложения на лучшие лампы для вашего дома.', N'__Upgrade вашей освещения сейчас и сохранить. __ проверить эти специальные предложения на лучшие лампы для вашего дома.', N'__Upgrade вашей освещения сейчас и сохранить. __ проверить эти специальные предложения на лучшие лампы для вашего дома.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5492, 'en-US', N'Shop Now', N'Shop Now', N'Shop Now', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5492, 'es-ES', N'Comprar ahora', N'Comprar ahora', N'Comprar ahora', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5492, 'ru-RU', N'Купить сейчас', N'Купить сейчас', N'Купить сейчас', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5493, 'en-US', N'Be smart. Get a Smart Strip for less.', N'Be smart. Get a Smart Strip for less.', N'Be smart. Get a Smart Strip for less.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5493, 'es-ES', N'Ser inteligente. Haz una tira inteligente para menos.', N'Ser inteligente. Haz una tira inteligente para menos.', N'Ser inteligente. Haz una tira inteligente para menos.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5493, 'ru-RU', N'Быть умным. Получите смарт газа меньше.', N'Быть умным. Получите смарт газа меньше.', N'Быть умным. Получите смарт газа меньше.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5494, 'en-US', N'Shop Now', N'Shop Now', N'Shop Now', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5494, 'es-ES', N'Comprar ahora', N'Comprar ahora', N'Comprar ahora', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5494, 'ru-RU', N'Купить сейчас', N'Купить сейчас', N'Купить сейчас', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5495, 'en-US', N'Slow drips could be costing you. Speed up the path to savings and order new water saving products now.', N'Slow drips could be costing you. Speed up the path to savings and order new water saving products now.', N'Slow drips could be costing you. Speed up the path to savings and order new water saving products now.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5495, 'es-ES', N'Lento goteo podría ser que te cueste. Acelerar el camino a ahorro y agua nueva orden productos de ahorro ahora.', N'Lento goteo podría ser que te cueste. Acelerar el camino a ahorro y agua nueva orden productos de ahorro ahora.', N'Lento goteo podría ser que te cueste. Acelerar el camino a ahorro y agua nueva orden productos de ahorro ahora.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (5495, 'ru-RU', N'Медленно капает может стоить вам. Ускорить путь к экономии и порядка новых водосберегающих продуктов теперь.', N'Медленно капает может стоить вам. Ускорить путь к экономии и порядка новых водосберегающих продуктов теперь.', N'Медленно капает может стоить вам. Ускорить путь к экономии и порядка новых водосберегающих продуктов теперь.', 0)
PRINT(N'Operation applied to 24 rows out of 24')

PRINT(N'Add constraints to [cm].[ClientTextContentLocaleContent]')
ALTER TABLE [cm].[ClientTextContentLocaleContent] ADD CONSTRAINT [FK_ClientTextContentLocaleContent_ClientTextContent] FOREIGN KEY ([ClientTextContentID]) REFERENCES [cm].[ClientTextContent] ([ClientTextContentID])
ALTER TABLE [cm].[ClientTextContentLocaleContent] ADD CONSTRAINT [FK_ClientTextContentLocaleContent_TypeLocale] FOREIGN KEY ([LocaleKey]) REFERENCES [cm].[TypeLocale] ([LocaleKey])

PRINT(N'Add constraints to [cm].[ClientActionCondition]')
ALTER TABLE [cm].[ClientActionCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientActionCondition_ClientAction] FOREIGN KEY ([ClientActionID]) REFERENCES [cm].[ClientAction] ([ClientActionID])
ALTER TABLE [cm].[ClientActionCondition] ADD CONSTRAINT [FK_ClientActionCondition_TypeCondition] FOREIGN KEY ([ConditionKey]) REFERENCES [cm].[TypeCondition] ([ConditionKey])

PRINT(N'Add constraints to [cm].[ClientWidget]')
ALTER TABLE [cm].[ClientWidget] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidget_TypeTextContent] FOREIGN KEY ([IntroTextKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientWidget] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidget_TypeTab] FOREIGN KEY ([TabKey]) REFERENCES [cm].[TypeTab] ([TabKey])
ALTER TABLE [cm].[ClientWidget] ADD CONSTRAINT [FK_ClientWidget_TypeWidget] FOREIGN KEY ([WidgetKey]) REFERENCES [cm].[TypeWidget] ([WidgetKey])
ALTER TABLE [cm].[ClientWidget] ADD CONSTRAINT [FK_ClientWidget_TypeWidgetType] FOREIGN KEY ([WidgetTypeKey]) REFERENCES [cm].[TypeWidgetType] ([WidgetTypeKey])

PRINT(N'Add constraint FK_ClientWidgetConditionality_ClientWidget to [cm].[ClientWidgetCondition]')
ALTER TABLE [cm].[ClientWidgetCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidgetConditionality_ClientWidget] FOREIGN KEY ([ClientWidgetID]) REFERENCES [cm].[ClientWidget] ([ClientWidgetID])

PRINT(N'Add constraint FK_ClientWidgetConfigurationality_ClientWidget to [cm].[ClientWidgetConfiguration]')
ALTER TABLE [cm].[ClientWidgetConfiguration] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidgetConfigurationality_ClientWidget] FOREIGN KEY ([ClientWidgetID]) REFERENCES [cm].[ClientWidget] ([ClientWidgetID])

PRINT(N'Add constraint FK_ClientWidgetTextContentality_ClientWidget to [cm].[ClientWidgetTextContent]')
ALTER TABLE [cm].[ClientWidgetTextContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidgetTextContentality_ClientWidget] FOREIGN KEY ([ClientWidgetID]) REFERENCES [cm].[ClientWidget] ([ClientWidgetID])

PRINT(N'Add constraints to [cm].[ClientTextContent]')
ALTER TABLE [cm].[ClientTextContent] ADD CONSTRAINT [FK_ClientTextContent_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
ALTER TABLE [cm].[ClientTextContent] ADD CONSTRAINT [FK_ClientTextContent_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientTextContent] ADD CONSTRAINT [FK_ClientTextContent_TypeTextContent] FOREIGN KEY ([TextContentKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraints to [cm].[ClientTab]')
ALTER TABLE [cm].[ClientTab] ADD CONSTRAINT [FK_ClientTab_TypeLayout] FOREIGN KEY ([LayoutKey]) REFERENCES [cm].[TypeLayout] ([LayoutKey])
ALTER TABLE [cm].[ClientTab] WITH NOCHECK ADD CONSTRAINT [FK_ClientTab_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientTab] ADD CONSTRAINT [FK_ClientTab_TypeTab] FOREIGN KEY ([TabKey]) REFERENCES [cm].[TypeTab] ([TabKey])
ALTER TABLE [cm].[ClientTab] ADD CONSTRAINT [FK_ClientTab_TypeTabType] FOREIGN KEY ([TabTypeKey]) REFERENCES [cm].[TypeTabType] ([TabTypeKey])

PRINT(N'Add constraint FK_ClientTabAction_ClientTab to [cm].[ClientTabAction]')
ALTER TABLE [cm].[ClientTabAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabAction_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])

PRINT(N'Add constraint FK_ClientTabChildTab_ClientTab to [cm].[ClientTabChildTab]')
ALTER TABLE [cm].[ClientTabChildTab] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabChildTab_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])

PRINT(N'Add constraint FK_ClientTabConditionality_ClientTab to [cm].[ClientTabCondition]')
ALTER TABLE [cm].[ClientTabCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabConditionality_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])

PRINT(N'Add constraint FK_ClientTabConfigurationality_ClientTab to [cm].[ClientTabConfiguration]')
ALTER TABLE [cm].[ClientTabConfiguration] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabConfigurationality_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])

PRINT(N'Add constraint FK_ClientTabProfileSection_ClientTab to [cm].[ClientTabProfileSection]')
ALTER TABLE [cm].[ClientTabProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabProfileSection_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])

PRINT(N'Add constraint FK_ClientTabTextContentality_ClientTab to [cm].[ClientTabTextContent]')
ALTER TABLE [cm].[ClientTabTextContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabTextContentality_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])

PRINT(N'Add constraints to [cm].[ClientConfiguration]')
ALTER TABLE [cm].[ClientConfiguration] WITH NOCHECK ADD CONSTRAINT [FK_ClientConfiguration_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
ALTER TABLE [cm].[ClientConfiguration] ADD CONSTRAINT [FK_ClientConfiguration_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientConfiguration] ADD CONSTRAINT [FK_ClientConfiguration_TypeConfiguration] FOREIGN KEY ([ConfigurationKey]) REFERENCES [cm].[TypeConfiguration] ([ConfigurationKey])
ALTER TABLE [cm].[ClientConfiguration] ADD CONSTRAINT [FK_ClientConfiguration_TypeEnvironment] FOREIGN KEY ([EnvironmentKey]) REFERENCES [cm].[TypeEnvironment] ([EnvironmentKey])

PRINT(N'Add constraints to [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeAction] FOREIGN KEY ([ActionKey]) REFERENCES [cm].[TypeAction] ([ActionKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeActionType] FOREIGN KEY ([ActionTypeKey]) REFERENCES [cm].[TypeActionType] ([ActionTypeKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeUom] FOREIGN KEY ([AnnualUsageSavingsUomKey]) REFERENCES [cm].[TypeUom] ([UomKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeUom1] FOREIGN KEY ([AnnualUsageSavingsUomKey]) REFERENCES [cm].[TypeUom] ([UomKey])
ALTER TABLE [cm].[ClientAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientAction_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeCommodity] FOREIGN KEY ([CommodityKey]) REFERENCES [cm].[TypeCommodity] ([CommodityKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeExpression] FOREIGN KEY ([CostExpression]) REFERENCES [cm].[TypeExpression] ([ExpressionKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeDifficulty] FOREIGN KEY ([DifficultyKey]) REFERENCES [cm].[TypeDifficulty] ([DifficultyKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeHabitInterval] FOREIGN KEY ([HabitIntervalKey]) REFERENCES [cm].[TypeHabitInterval] ([HabitIntervalKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_ClientFileContent2] FOREIGN KEY ([ImageKey]) REFERENCES [cm].[TypeFileContent] ([FileContentKey])
ALTER TABLE [cm].[ClientAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientAction_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientAction_TypeTextContent1] FOREIGN KEY ([NextStepLinkText]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeNextStepLinkType] FOREIGN KEY ([NextStepLinkType]) REFERENCES [cm].[TypeNextStepLinkType] ([NextStepLinkTypeKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_TypeExpression1] FOREIGN KEY ([RebateExpression]) REFERENCES [cm].[TypeExpression] ([ExpressionKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_ClientFileContent] FOREIGN KEY ([RebateImageKey]) REFERENCES [cm].[TypeFileContent] ([FileContentKey])
ALTER TABLE [cm].[ClientAction] ADD CONSTRAINT [FK_ClientAction_ClientFileContent3] FOREIGN KEY ([VideoKey]) REFERENCES [cm].[TypeFileContent] ([FileContentKey])

PRINT(N'Add constraint FK_ClientActionAppliance_ClientAction to [cm].[ClientActionAppliance]')
ALTER TABLE [cm].[ClientActionAppliance] WITH NOCHECK ADD CONSTRAINT [FK_ClientActionAppliance_ClientAction] FOREIGN KEY ([ClientActionID]) REFERENCES [cm].[ClientAction] ([ClientActionID])

PRINT(N'Add constraint FK_ClientActionSeasonality_ClientAction to [cm].[ClientActionSeason]')
ALTER TABLE [cm].[ClientActionSeason] WITH NOCHECK ADD CONSTRAINT [FK_ClientActionSeasonality_ClientAction] FOREIGN KEY ([ClientActionID]) REFERENCES [cm].[ClientAction] ([ClientActionID])

PRINT(N'Add constraint FK_ClientActionWhatIfData_ClientAction to [cm].[ClientActionWhatIfData]')
ALTER TABLE [cm].[ClientActionWhatIfData] WITH NOCHECK ADD CONSTRAINT [FK_ClientActionWhatIfData_ClientAction] FOREIGN KEY ([ClientActionID]) REFERENCES [cm].[ClientAction] ([ClientActionID])

PRINT(N'Add constraint FK_ClientAppliance_TypeTextContent to [cm].[ClientAppliance]')
ALTER TABLE [cm].[ClientAppliance] WITH NOCHECK ADD CONSTRAINT [FK_ClientAppliance_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientCommodity_TypeTextContent to [cm].[ClientCommodity]')
ALTER TABLE [cm].[ClientCommodity] WITH NOCHECK ADD CONSTRAINT [FK_ClientCommodity_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientCurrency_TypeTextContent to [cm].[ClientCurrency]')
ALTER TABLE [cm].[ClientCurrency] WITH NOCHECK ADD CONSTRAINT [FK_ClientCurrency_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientCurrency_TypeTextContent1 to [cm].[ClientCurrency]')
ALTER TABLE [cm].[ClientCurrency] WITH NOCHECK ADD CONSTRAINT [FK_ClientCurrency_TypeTextContent1] FOREIGN KEY ([SymbolKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientEnduse_TypeTextContent to [cm].[ClientEnduse]')
ALTER TABLE [cm].[ClientEnduse] WITH NOCHECK ADD CONSTRAINT [FK_ClientEnduse_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientMeasurement_TypeTextContent to [cm].[ClientMeasurement]')
ALTER TABLE [cm].[ClientMeasurement] WITH NOCHECK ADD CONSTRAINT [FK_ClientMeasurement_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientProfileAttribute_TypeTextContent to [cm].[ClientProfileAttribute]')
ALTER TABLE [cm].[ClientProfileAttribute] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileAttribute_TypeTextContent] FOREIGN KEY ([QuestionTextKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientProfileOption_TypeTextContent to [cm].[ClientProfileOption]')
ALTER TABLE [cm].[ClientProfileOption] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileOption_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientProfileSection_TypeTextContent to [cm].[ClientProfileSection]')
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent] FOREIGN KEY ([DescriptionKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientProfileSection_TypeTextContent3 to [cm].[ClientProfileSection]')
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent3] FOREIGN KEY ([IntroTextKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientProfileSection_TypeTextContent2 to [cm].[ClientProfileSection]')
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent2] FOREIGN KEY ([SubTitleKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientProfileSection_TypeTextContent1 to [cm].[ClientProfileSection]')
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent1] FOREIGN KEY ([TitleKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientSeason_TypeTextContent to [cm].[ClientSeason]')
ALTER TABLE [cm].[ClientSeason] WITH NOCHECK ADD CONSTRAINT [FK_ClientSeason_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientTabTextContentality_TypeTextContentality to [cm].[ClientTabTextContent]')
ALTER TABLE [cm].[ClientTabTextContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabTextContentality_TypeTextContentality] FOREIGN KEY ([TextContentKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientUOM_ClientUOM1 to [cm].[ClientUOM]')
ALTER TABLE [cm].[ClientUOM] WITH NOCHECK ADD CONSTRAINT [FK_ClientUOM_ClientUOM1] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientWidgetTextContentality_TypeTextContentality to [cm].[ClientWidgetTextContent]')
ALTER TABLE [cm].[ClientWidgetTextContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidgetTextContentality_TypeTextContentality] FOREIGN KEY ([TextContentKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientTabConfigurationality_TypeConfigurationality to [cm].[ClientTabConfiguration]')
ALTER TABLE [cm].[ClientTabConfiguration] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabConfigurationality_TypeConfigurationality] FOREIGN KEY ([ConfigurationKey]) REFERENCES [cm].[TypeConfiguration] ([ConfigurationKey])

PRINT(N'Add constraint FK_ClientWidgetConfigurationality_TypeConfigurationality to [cm].[ClientWidgetConfiguration]')
ALTER TABLE [cm].[ClientWidgetConfiguration] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidgetConfigurationality_TypeConfigurationality] FOREIGN KEY ([ConfigurationKey]) REFERENCES [cm].[TypeConfiguration] ([ConfigurationKey])

PRINT(N'Add constraint FK_ClientActionSavings_TypeAction to [cm].[ClientActionSavings]')
ALTER TABLE [cm].[ClientActionSavings] WITH NOCHECK ADD CONSTRAINT [FK_ClientActionSavings_TypeAction] FOREIGN KEY ([ActionKey]) REFERENCES [cm].[TypeAction] ([ActionKey])

PRINT(N'Add constraint FK_ClientTabAction_TypeAction to [cm].[ClientTabAction]')
ALTER TABLE [cm].[ClientTabAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabAction_TypeAction] FOREIGN KEY ([ActionKey]) REFERENCES [cm].[TypeAction] ([ActionKey])

PRINT(N'Add constraints to [cm].[ClientCondition]')
ALTER TABLE [cm].[ClientCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientCondition_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
ALTER TABLE [cm].[ClientCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientCondition_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientCondition] ADD CONSTRAINT [FK_ClientCondition_TypeCondition] FOREIGN KEY ([ConditionKey]) REFERENCES [cm].[TypeCondition] ([ConditionKey])
ALTER TABLE [cm].[ClientCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientCondition_TypeProfileAttribute] FOREIGN KEY ([ProfileAttributeKey]) REFERENCES [cm].[TypeProfileAttribute] ([ProfileAttributeKey])
ALTER TABLE [cm].[ClientCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientCondition_TypeProfileOption] FOREIGN KEY ([ProfileOptionKey]) REFERENCES [cm].[TypeProfileOption] ([ProfileOptionKey])

PRINT(N'Add constraints to [cm].[ClientAssetLocaleContent]')
ALTER TABLE [cm].[ClientAssetLocaleContent] ADD CONSTRAINT [FK_ClientAssetLocaleContent_ClientAsset] FOREIGN KEY ([ClientAssetID]) REFERENCES [cm].[ClientAsset] ([ClientAssetID])
ALTER TABLE [cm].[ClientAssetLocaleContent] ADD CONSTRAINT [FK_ClientAssetLocaleContent_TypeLocale] FOREIGN KEY ([LocaleKey]) REFERENCES [cm].[TypeLocale] ([LocaleKey])
COMMIT TRANSACTION
GO
