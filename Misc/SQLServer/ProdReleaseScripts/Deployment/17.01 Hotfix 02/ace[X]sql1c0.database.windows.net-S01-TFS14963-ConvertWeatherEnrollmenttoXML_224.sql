
/**************************************************/
/***** RUN ONLY ON THE INSIGHTSSW_224 DATABASE ***/
/*************************************************/



/****** Object:  StoredProcedure [dbo].[ConvertWeatherEnrollmentToXML_224]    Script Date: 2/6/2017 8:30:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[ConvertWeatherEnrollmentToXML_224] @ClientId AS INT
AS
BEGIN
    SET NOCOUNT ON;
    declare @client_id int=224;

    WITH XMLNAMESPACES (DEFAULT 'Aclara:Insights') SELECT
        ISNULL
        (
            (
                SELECT
                      [csv].[Accountid] AS '@CustomerId'    -- Customer attributes
                    , [csv].[AccountId] AS 'Account/@AccountId' -- Account attributes
                    , [csv].[PremiseId] AS 'Account/Premise/@PremiseId' -- Premise attributes


                    --***************************************************
                    --          treatment group enrollment
                    --***************************************************

                    , CASE
                        WHEN [csv].[Segment] LIKE 'T%' THEN
                            -- ProfileItem attributes for treatment enroll
                            (
                                SELECT
                                      'ws2017elec.treatmentgroup.enrollmentstatus'          AS 'ProfileItem/@AttributeKey'
                                    , 'ws2017elec.treatmentgroup.enrollmentstatus.enrolled' AS 'ProfileItem/@AttributeValue'
                                    , GETDATE()         AS 'ProfileItem/@CreateDate'
                                    , GETDATE()         AS 'ProfileItem/@ModifiedDate'
                                    , 'utility' AS 'ProfileItem/@Source'

                                FOR XML PATH(''), TYPE, ELEMENTS
                            )
                    ELSE NULL
                    END AS 'Account/Premise'


                    --***************************************************
                    --          treatment group number assignment
                    --***************************************************

                    ,CASE
                        WHEN [csv].[Segment] LIKE 'T%' THEN
                        -- ProfileItem attributes for treatment group 1 enroll
                        (
                            SELECT
                                  'ws2017elec.treatmentgroup.groupnumber'               AS 'ProfileItem/@AttributeKey'
                                , [csv].[Segment]                                               AS 'ProfileItem/@AttributeValue'
                                , GETDATE()         AS 'ProfileItem/@CreateDate'
                                , GETDATE()         AS 'ProfileItem/@ModifiedDate'
                                , 'utility' AS 'ProfileItem/@Source'
                                FOR XML PATH(''), TYPE, ELEMENTS
                        )
                        ELSE NULL
                        END AS 'Account/Premise'

                    --***************************************************
                    --              control group enrollment
                    --***************************************************
                    ,CASE
                        WHEN [csv].[Segment] LIKE 'C%' THEN --one of the control groups
                        -- ProfileItem attributes for analysis group enroll
                        (
                            SELECT
                                  'ws2017elec.controlgroup.enrollmentstatus'            AS 'ProfileItem/@AttributeKey'
                                , 'ws2017elec.controlgroup.enrollmentstatus.enrolled'   AS 'ProfileItem/@AttributeValue'
                                , GETDATE()         AS 'ProfileItem/@CreateDate'
                                , GETDATE()         AS 'ProfileItem/@ModifiedDate'
                                , 'utility' AS 'ProfileItem/@Source'
                                FOR XML PATH(''), TYPE, ELEMENTS
                        )
                        ELSE NULL
                        END AS 'Account/Premise'

                    --***************************************************
                    --          control group number assignment
                    --***************************************************
                    ,CASE
                        WHEN [csv].[Segment] LIKE 'C%' THEN  -- ProfileItem attributes for treatment group
                        (
                            SELECT
                                  'ws2017elec.controlgroup.groupnumber'             AS 'ProfileItem/@AttributeKey'
                                , [csv].[Segment]                                           AS 'ProfileItem/@AttributeValue'
                                , GETDATE()         AS 'ProfileItem/@CreateDate'
                                , GETDATE()         AS 'ProfileItem/@ModifiedDate'
                                , 'utility' AS 'ProfileItem/@Source'
                                FOR XML PATH(''), TYPE, ELEMENTS
                        )
                        ELSE NULL
                    END AS 'Account/Premise'

                    --***************************************************
                    --              analysis group enrollment
                    --***************************************************
                    ,CASE WHEN [csv].[Segment] LIKE 'T%' OR [csv].[Segment] LIKE'C%' THEN --one of the treatment groups or one of the control groups

                        -- ProfileItem attributes for analysis group enroll
                        (
                            SELECT
                                  'ws2017elec.analysisgroup.enrollmentstatus'           AS 'ProfileItem/@AttributeKey'
                                , 'ws2017elec.analysisgroup.enrollmentstatus.enrolled'  AS 'ProfileItem/@AttributeValue'
                                , GETDATE()         AS 'ProfileItem/@CreateDate'
                                , GETDATE()         AS 'ProfileItem/@ModifiedDate'
                                , 'utility' AS 'ProfileItem/@Source'
                                FOR XML PATH(''), TYPE, ELEMENTS
                        )
                        ELSE NULL
                        END AS 'Account/Premise'

                    --***************************************************
                    --              channel
                    --***************************************************

                    -- paper

                    ,CASE
                        WHEN [csv].[Segment] LIKE 'T%' THEN --
                        -- ProfileItem attributes for channel -all are printandemail
                        (
                            SELECT
                                  'ws2017elec.channel'                  AS 'ProfileItem/@AttributeKey'
                                , 'ws2017elec.channel.printandemail'        AS 'ProfileItem/@AttributeValue'
                                , GETDATE()         AS 'ProfileItem/@CreateDate'
                                , GETDATE()         AS 'ProfileItem/@ModifiedDate'
                                , 'utility' AS 'ProfileItem/@Source'
                                FOR XML PATH(''), TYPE, ELEMENTS
                            )
                        ELSE NULL
                    END
                    AS 'Account/Premise'


            FROM
                [dbo].[RawWeatherEnrollmentTemp_224] AS [csv]
                --Removed this part because none of these are return users for 224.  This code was in for 101 and may be useful at a later date.
                --LEFT OUTER JOIN (SELECT DISTINCT [PremiseId], [AccountId] FROM [dbo].[RawUilExclusionCsv]) AS [exclusion] ON
                    --[exclusion].[PremiseId] = [csv].[PremiseId] AND
                    --[exclusion].[AccountId] = [csv].[AccountId]
                --INNER JOIN [dbo].[DimPremise] AS [dp] ON
                    --[dp].[AccountId] = [csv].[AccountId] AND
                    --[dp].[PremiseId] = [csv].[PremiseId]

                --INNER JOIN
                    --(
                        -- retrieve the most recent customer for a premise based on DateCreated
                        --SELECT
                            ---[PremiseKey],
                            --[CustomerKey],
                            --ROW_NUMBER() OVER(PARTITION BY [PremiseKey] ORDER BY [DateCreated] DESC) AS [Rank]
                            --FROM
                                --[dbo].[FactCustomerPremise]
                    --) AS [fcp] ON
                    --[fcp].[PremiseKey] = [dp].[PremiseKey]

                --INNER JOIN [dbo].[DimCustomer] AS [dc] ON
                    --[dc].[CustomerKey] = [fcp].[CustomerKey]
            --WHERE
                --[fcp].[Rank] = 1 AND -- only look at most recent customer/premise relation
                --ISNULL([csv].[Segment], '') != '' AND [csv].[Segment] != 'Group Assignment' AND
                --[exclusion].[AccountId] IS NULL AND [exclusion].[PremiseId] IS NULL   -- filter out excluded accounts
            --ORDER BY
                --[dc].[CustomerId],
                --[csv].[AccountId],
                --[csv].[PremiseId]

        --
            FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
        ),
        '<Customers xmlns="Aclara:Insights"></Customers>'
    ) AS [XmlData];

END




GO


