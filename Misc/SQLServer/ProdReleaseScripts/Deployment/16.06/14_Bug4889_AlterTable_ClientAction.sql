/*
Run this script on:
        aceusql1c0.database.windows.net.InsightsMetaData   
*/
USE [InsightsMetaData]
GO

ALTER TABLE [cm].[ClientAction]
ADD [MinimumSavingsAmt] [decimal](18, 2) NULL
GO	
ALTER TABLE [cm].[ClientAction]
ADD [MaximumSavingsAmt] [decimal](18, 2) NULL
GO
