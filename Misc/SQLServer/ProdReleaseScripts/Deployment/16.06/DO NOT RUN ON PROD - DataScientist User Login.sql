-- Execute this script in the master database
IF NOT EXISTS 
    (SELECT name  
     FROM master.sys.sql_logins
     WHERE name = 'DataScientist')
BEGIN
    CREATE LOGIN DataScientist WITH PASSWORD = 'Acl@r@168'
END


IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'DataScientistUser')
BEGIN
    CREATE USER DataScientistUser FOR LOGIN DataScientist
END


-- Execute this script in all the shards except the master database
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'DataScientistUser')
BEGIN
    CREATE USER DataScientistUser FOR LOGIN DataScientist
END


EXEC sp_addrolemember 'db_datareader', 'DataScientistUser'
GO
