﻿/*
Run this script on:

aceUsql1c0.database.windows.net.InsightsMetadata    -  This database will be modified

to synchronize it with:

aceQsql1c0.database.windows.net.InsightsMetadata

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 11.1.3 from Red Gate Software Ltd at 6/16/2016 8:15:26 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

PRINT(N'Drop constraints from [cm].[ClientTextContentLocaleContent]')
ALTER TABLE [cm].[ClientTextContentLocaleContent] DROP CONSTRAINT [FK_ClientTextContentLocaleContent_ClientTextContent]
ALTER TABLE [cm].[ClientTextContentLocaleContent] DROP CONSTRAINT [FK_ClientTextContentLocaleContent_TypeLocale]

PRINT(N'Drop constraints from [cm].[ClientProfileSectionCondition]')
ALTER TABLE [cm].[ClientProfileSectionCondition] DROP CONSTRAINT [FK_ClientProfileSectionCondition_ClientProfileSection]
ALTER TABLE [cm].[ClientProfileSectionCondition] DROP CONSTRAINT [FK_ClientProfileSectionCondition_TypeCondition]

PRINT(N'Drop constraints from [cm].[ClientProfileSectionAttribute]')
ALTER TABLE [cm].[ClientProfileSectionAttribute] DROP CONSTRAINT [FK_ClientProfileSectionAttribute_ClientProfileSection]
ALTER TABLE [cm].[ClientProfileSectionAttribute] DROP CONSTRAINT [FK_ClientProfileSectionAttribute_TypeProfileAttribute]

PRINT(N'Drop constraints from [cm].[ClientAssetLocaleContent]')
ALTER TABLE [cm].[ClientAssetLocaleContent] DROP CONSTRAINT [FK_ClientAssetLocaleContent_ClientAsset]
ALTER TABLE [cm].[ClientAssetLocaleContent] DROP CONSTRAINT [FK_ClientAssetLocaleContent_TypeLocale]

PRINT(N'Drop constraints from [cm].[ClientUOM]')
ALTER TABLE [cm].[ClientUOM] DROP CONSTRAINT [FK_ClientUOM_Client]
ALTER TABLE [cm].[ClientUOM] DROP CONSTRAINT [FK_ClientUOM_ClientUOM1]
ALTER TABLE [cm].[ClientUOM] DROP CONSTRAINT [FK_ClientUOM_ClientUOM]
ALTER TABLE [cm].[ClientUOM] DROP CONSTRAINT [FK_ClientUOM_TypeUom]

PRINT(N'Drop constraints from [cm].[ClientTextContent]')
ALTER TABLE [cm].[ClientTextContent] DROP CONSTRAINT [FK_ClientTextContent_TypeCategory]
ALTER TABLE [cm].[ClientTextContent] DROP CONSTRAINT [FK_ClientTextContent_Client]
ALTER TABLE [cm].[ClientTextContent] DROP CONSTRAINT [FK_ClientTextContent_TypeTextContent]

PRINT(N'Drop constraints from [cm].[ClientTabProfileSection]')
ALTER TABLE [cm].[ClientTabProfileSection] DROP CONSTRAINT [FK_ClientTabProfileSection_ClientTab]
ALTER TABLE [cm].[ClientTabProfileSection] DROP CONSTRAINT [FK_ClientTabProfileSection_TypeProfileSection]

PRINT(N'Drop constraints from [cm].[ClientProfileSection]')
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_Client]
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent]
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent3]
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeProfileSection]
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent2]
ALTER TABLE [cm].[ClientProfileSection] DROP CONSTRAINT [FK_ClientProfileSection_TypeTextContent1]

PRINT(N'Drop constraints from [cm].[ClientProfileOption]')
ALTER TABLE [cm].[ClientProfileOption] DROP CONSTRAINT [FK_ClientProfileOption_Client]
ALTER TABLE [cm].[ClientProfileOption] DROP CONSTRAINT [FK_ClientProfileOption_TypeTextContent]
ALTER TABLE [cm].[ClientProfileOption] DROP CONSTRAINT [FK_ClientProfileOption_TypeProfileOption]

PRINT(N'Drop constraints from [cm].[ClientProfileAttributeProfileOption]')
ALTER TABLE [cm].[ClientProfileAttributeProfileOption] DROP CONSTRAINT [FK_ClientProfileAttributeProfileOption_ClientProfileAttribute]
ALTER TABLE [cm].[ClientProfileAttributeProfileOption] DROP CONSTRAINT [FK_ClientProfileAttributeProfileOption_TypeProfileOption]

PRINT(N'Drop constraints from [cm].[ClientFileContent]')
ALTER TABLE [cm].[ClientFileContent] DROP CONSTRAINT [FK_ClientFileContent_TypeCategory]
ALTER TABLE [cm].[ClientFileContent] DROP CONSTRAINT [FK_ClientFileContent_Client]
ALTER TABLE [cm].[ClientFileContent] DROP CONSTRAINT [FK_ClientFileContent_TypeFileContent]
ALTER TABLE [cm].[ClientFileContent] DROP CONSTRAINT [FK_ClientFileContentLargeFile_TypeAsset]
ALTER TABLE [cm].[ClientFileContent] DROP CONSTRAINT [FK_ClientFileContentMediumFile_TypeAsset]
ALTER TABLE [cm].[ClientFileContent] DROP CONSTRAINT [FK_ClientFileContentSmallFile_TypeAsset]

PRINT(N'Drop constraints from [cm].[ClientEnumerationItem]')
ALTER TABLE [cm].[ClientEnumerationItem] DROP CONSTRAINT [FK_ClientEnumerationItem_Client]
ALTER TABLE [cm].[ClientEnumerationItem] DROP CONSTRAINT [FK_ClientEnumerationItem_TypeEnumerationItem]

PRINT(N'Drop constraints from [cm].[ClientEnumerationEnumerationItem]')
ALTER TABLE [cm].[ClientEnumerationEnumerationItem] DROP CONSTRAINT [FK_ClientEnumerationEnumerationItem_ClientEnumeration]
ALTER TABLE [cm].[ClientEnumerationEnumerationItem] DROP CONSTRAINT [FK_ClientEnumerationEnumerationItem_TypeEnumerationItem]

PRINT(N'Drop constraints from [cm].[ClientAsset]')
ALTER TABLE [cm].[ClientAsset] DROP CONSTRAINT [FK_ClientAsset_TypeAsset]
ALTER TABLE [cm].[ClientAsset] DROP CONSTRAINT [FK_ClientAsset_Client]

PRINT(N'Drop constraints from [cm].[ClientActionCondition]')
ALTER TABLE [cm].[ClientActionCondition] DROP CONSTRAINT [FK_ClientActionCondition_ClientAction]
ALTER TABLE [cm].[ClientActionCondition] DROP CONSTRAINT [FK_ClientActionCondition_TypeCondition]

PRINT(N'Drop constraint FK_ClientAction_TypeUom from [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeUom]

PRINT(N'Drop constraint FK_ClientAction_TypeUom1 from [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeUom1]

PRINT(N'Drop constraint FK_ClientAction_TypeTextContent from [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeTextContent]

PRINT(N'Drop constraint FK_ClientAction_TypeTextContent1 from [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_TypeTextContent1]

PRINT(N'Drop constraint FK_ClientAppliance_TypeTextContent from [cm].[ClientAppliance]')
ALTER TABLE [cm].[ClientAppliance] DROP CONSTRAINT [FK_ClientAppliance_TypeTextContent]

PRINT(N'Drop constraint FK_ClientCommodity_TypeTextContent from [cm].[ClientCommodity]')
ALTER TABLE [cm].[ClientCommodity] DROP CONSTRAINT [FK_ClientCommodity_TypeTextContent]

PRINT(N'Drop constraint FK_ClientCurrency_TypeTextContent from [cm].[ClientCurrency]')
ALTER TABLE [cm].[ClientCurrency] DROP CONSTRAINT [FK_ClientCurrency_TypeTextContent]

PRINT(N'Drop constraint FK_ClientCurrency_TypeTextContent1 from [cm].[ClientCurrency]')
ALTER TABLE [cm].[ClientCurrency] DROP CONSTRAINT [FK_ClientCurrency_TypeTextContent1]

PRINT(N'Drop constraint FK_ClientEnduse_TypeTextContent from [cm].[ClientEnduse]')
ALTER TABLE [cm].[ClientEnduse] DROP CONSTRAINT [FK_ClientEnduse_TypeTextContent]

PRINT(N'Drop constraint FK_ClientMeasurement_TypeTextContent from [cm].[ClientMeasurement]')
ALTER TABLE [cm].[ClientMeasurement] DROP CONSTRAINT [FK_ClientMeasurement_TypeTextContent]

PRINT(N'Drop constraint FK_ClientProfileAttribute_TypeTextContent from [cm].[ClientProfileAttribute]')
ALTER TABLE [cm].[ClientProfileAttribute] DROP CONSTRAINT [FK_ClientProfileAttribute_TypeTextContent]

PRINT(N'Drop constraint FK_ClientSeason_TypeTextContent from [cm].[ClientSeason]')
ALTER TABLE [cm].[ClientSeason] DROP CONSTRAINT [FK_ClientSeason_TypeTextContent]

PRINT(N'Drop constraint FK_ClientTab_TypeTextContent from [cm].[ClientTab]')
ALTER TABLE [cm].[ClientTab] DROP CONSTRAINT [FK_ClientTab_TypeTextContent]

PRINT(N'Drop constraint FK_ClientTabTextContentality_TypeTextContentality from [cm].[ClientTabTextContent]')
ALTER TABLE [cm].[ClientTabTextContent] DROP CONSTRAINT [FK_ClientTabTextContentality_TypeTextContentality]

PRINT(N'Drop constraint FK_ClientWidget_TypeTextContent from [cm].[ClientWidget]')
ALTER TABLE [cm].[ClientWidget] DROP CONSTRAINT [FK_ClientWidget_TypeTextContent]

PRINT(N'Drop constraint FK_ClientWidgetTextContentality_TypeTextContentality from [cm].[ClientWidgetTextContent]')
ALTER TABLE [cm].[ClientWidgetTextContent] DROP CONSTRAINT [FK_ClientWidgetTextContentality_TypeTextContentality]

PRINT(N'Drop constraint FK_ClientAction_ClientFileContent2 from [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_ClientFileContent2]

PRINT(N'Drop constraint FK_ClientAction_ClientFileContent from [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_ClientFileContent]

PRINT(N'Drop constraint FK_ClientAction_ClientFileContent3 from [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] DROP CONSTRAINT [FK_ClientAction_ClientFileContent3]

PRINT(N'Drop constraint FK_ClientCondition_TypeCategory from [cm].[ClientCondition]')
ALTER TABLE [cm].[ClientCondition] DROP CONSTRAINT [FK_ClientCondition_TypeCategory]

PRINT(N'Drop constraint FK_ClientConfiguration_TypeCategory from [cm].[ClientConfiguration]')
ALTER TABLE [cm].[ClientConfiguration] DROP CONSTRAINT [FK_ClientConfiguration_TypeCategory]

PRINT(N'Drop constraint FK_ClientConfigurationBulk_TypeCategory from [cm].[ClientConfigurationBulk]')
ALTER TABLE [cm].[ClientConfigurationBulk] DROP CONSTRAINT [FK_ClientConfigurationBulk_TypeCategory]

PRINT(N'Drop constraint FK_TypeEnumeration_TypeCategory from [cm].[TypeEnumeration]')
ALTER TABLE [cm].[TypeEnumeration] DROP CONSTRAINT [FK_TypeEnumeration_TypeCategory]

PRINT(N'Delete rows from [cm].[ClientProfileSectionAttribute]')
DELETE FROM [cm].[ClientProfileSectionAttribute] WHERE [ClientProfileSectionID]=125 AND [ProfileAttributeKey]='heatsystem.servicefrequency'
DELETE FROM [cm].[ClientProfileSectionAttribute] WHERE [ClientProfileSectionID]=125 AND [ProfileAttributeKey]='house.drafty'
DELETE FROM [cm].[ClientProfileSectionAttribute] WHERE [ClientProfileSectionID]=125 AND [ProfileAttributeKey]='house.zones'
DELETE FROM [cm].[ClientProfileSectionAttribute] WHERE [ClientProfileSectionID]=127 AND [ProfileAttributeKey]='hottub.count'
DELETE FROM [cm].[ClientProfileSectionAttribute] WHERE [ClientProfileSectionID]=127 AND [ProfileAttributeKey]='pool.count'
DELETE FROM [cm].[ClientProfileSectionAttribute] WHERE [ClientProfileSectionID]=130 AND [ProfileAttributeKey]='business.cooltemp'
PRINT(N'Operation applied to 6 rows out of 6')

PRINT(N'Delete rows from [cm].[ClientTabProfileSection]')
DELETE FROM [cm].[ClientTabProfileSection] WHERE [ClientTabID]=99 AND [ProfileSectionKey]='profilesection.home1'
DELETE FROM [cm].[ClientTabProfileSection] WHERE [ClientTabID]=99 AND [ProfileSectionKey]='profilesection.shortcentralac'
DELETE FROM [cm].[ClientTabProfileSection] WHERE [ClientTabID]=99 AND [ProfileSectionKey]='profilesection.shorthome2'
PRINT(N'Operation applied to 3 rows out of 3')

PRINT(N'Delete row from [admin].[ETLBulkConfiguration]')
DELETE FROM [admin].[ETLBulkConfiguration] WHERE [ClientID]=4 AND [Environment]='UAT' AND [ClientComponent]='BulkLoad'

PRINT(N'Update rows in [cm].[ClientTextContentLocaleContent]')
UPDATE [cm].[ClientTextContentLocaleContent] SET [MediumText]=N'Please enter a value between {%minvalue%} - {%maxvalue%}.', [LongText]=N'Please enter a value between {%minvalue%} - {%maxvalue%}.' WHERE [ClientTextContentID]=5987 AND [LocaleKey]='en-US'
UPDATE [cm].[ClientTextContentLocaleContent] SET [ShortText]=N'Don''t know', [MediumText]=N'Don''t know', [LongText]=N'Don''t know' WHERE [ClientTextContentID]=6100 AND [LocaleKey]='en-US'
UPDATE [cm].[ClientTextContentLocaleContent] SET [ShortText]=N'Waste is the enemy of an efficient business. Energy waste can often be reduced significantly by simply maintaining and optimizing the use of your existing equipment. Sealing and insulating heating ducts, for example, is a simple and relatively inexpensive way to make sure the money you spend on heating is actually used to heat your building, not being wasted through leaks and poor insulation.', [MediumText]=N'Waste is the enemy of an efficient business. Energy waste can often be reduced significantly by simply maintaining and optimizing the use of your existing equipment. Sealing and insulating heating ducts, for example, is a simple and relatively inexpensive way to make sure the money you spend on heating is actually used to heat your building, not being wasted through leaks and poor insulation.', [LongText]=N'Waste is the enemy of an efficient business. Energy waste can often be reduced significantly by simply maintaining and optimizing the use of your existing equipment. Sealing and insulating heating ducts, for example, is a simple and relatively inexpensive way to make sure the money you spend on heating is actually used to heat your building, not being wasted through leaks and poor insulation.' WHERE [ClientTextContentID]=6360 AND [LocaleKey]='en-US'
UPDATE [cm].[ClientTextContentLocaleContent] SET [ShortText]=N'Efficient commercial refrigeration equipment -- specifically refrigerators certified by ENERGY STAR  -- can reduce refrigeration energy use by 40% or more. If you aren''t ready to invest in new equipment, regular maintenance like cleaning coils and replacing gaskets, and retrofits like adding variable speed fan motors can acheive some of the savings with a smaller up-front investment.', [MediumText]=N'Efficient commercial refrigeration equipment -- specifically refrigerators certified by ENERGY STAR  -- can reduce refrigeration energy use by 40% or more. If you aren''t ready to invest in new equipment, regular maintenance like cleaning coils and replacing gaskets, and retrofits like adding variable speed fan motors can acheive some of the savings with a smaller up-front investment.', [LongText]=N'Efficient commercial refrigeration equipment -- specifically refrigerators certified by ENERGY STAR  -- can reduce refrigeration energy use by 40% or more. If you aren''t ready to invest in new equipment, regular maintenance like cleaning coils and replacing gaskets, and retrofits like adding variable speed fan motors can acheive some of the savings with a smaller up-front investment.' WHERE [ClientTextContentID]=6372 AND [LocaleKey]='en-US'
UPDATE [cm].[ClientTextContentLocaleContent] SET [ShortText]=N'Please enter a value between {%minvalue%} - {%maxvalue%}.', [MediumText]=N'Please enter a value between {%minvalue%} - {%maxvalue%}.', [LongText]=N'Please enter a value between {%minvalue%} - {%maxvalue%}.' WHERE [ClientTextContentID]=6592 AND [LocaleKey]='en-US'
PRINT(N'Operation applied to 5 rows out of 5')

PRINT(N'Update rows in [cm].[ClientProfileSectionAttribute]')
UPDATE [cm].[ClientProfileSectionAttribute] SET [DisplayOrder]=3 WHERE [ClientProfileSectionID]=125 AND [ProfileAttributeKey]='heatsystem.yearinstalledrange'
UPDATE [cm].[ClientProfileSectionAttribute] SET [DisplayOrder]=4 WHERE [ClientProfileSectionID]=127 AND [ProfileAttributeKey]='clotheswasher.count'
UPDATE [cm].[ClientProfileSectionAttribute] SET [DisplayOrder]=5 WHERE [ClientProfileSectionID]=127 AND [ProfileAttributeKey]='dishwasher.count'
UPDATE [cm].[ClientProfileSectionAttribute] SET [DisplayOrder]=2 WHERE [ClientProfileSectionID]=127 AND [ProfileAttributeKey]='waterheater.yearrange'
UPDATE [cm].[ClientProfileSectionAttribute] SET [DisplayOrder]=4 WHERE [ClientProfileSectionID]=130 AND [ProfileAttributeKey]='business.coolyear'
PRINT(N'Operation applied to 5 rows out of 5')

PRINT(N'Update rows in [cm].[ClientTabProfileSection]')
UPDATE [cm].[ClientTabProfileSection] SET [Rank]=2 WHERE [ClientTabID]=99 AND [ProfileSectionKey]='profilesection.shortheating'
UPDATE [cm].[ClientTabProfileSection] SET [Rank]=3 WHERE [ClientTabID]=99 AND [ProfileSectionKey]='profilesection.shortwaterheater'
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Update rows in [cm].[ClientProfileSection]')
UPDATE [cm].[ClientProfileSection] SET [Images]='image.145678766lowertvbrightness' WHERE [ClientProfileSectionID]=39
UPDATE [cm].[ClientProfileSection] SET [Rank]=60 WHERE [ClientProfileSectionID]=127
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Update row in [cm].[ClientProfileOption]')
UPDATE [cm].[ClientProfileOption] SET [Description]='Don''t know' WHERE [ClientProfileOptionID]=1584

PRINT(N'Update row in [cm].[ClientProfileAttributeProfileOption]')
UPDATE [cm].[ClientProfileAttributeProfileOption] SET [DisplayOrder]=7 WHERE [ClientProfileAttributeID]=1926 AND [ProfileOptionKey]='business.oven.count.gt5'

PRINT(N'Update rows in [cm].[TypeUom]')
UPDATE [cm].[TypeUom] SET [UomID]=2 WHERE [UomKey]='ccf'
UPDATE [cm].[TypeUom] SET [UomID]=3 WHERE [UomKey]='cf'
UPDATE [cm].[TypeUom] SET [UomID]=6 WHERE [UomKey]='gal'
UPDATE [cm].[TypeUom] SET [UomID]=0 WHERE [UomKey]='kwh'
UPDATE [cm].[TypeUom] SET [UomID]=9 WHERE [UomKey]='lbs'
UPDATE [cm].[TypeUom] SET [UomID]=1 WHERE [UomKey]='therms'
UPDATE [cm].[TypeUom] SET [UomID]=10 WHERE [UomKey]='trees'
PRINT(N'Operation applied to 7 rows out of 7')

PRINT(N'Add rows to [cm].[TypeAsset]')
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('1A0n2oY8GIMIyQkOMqKgMQ', 709)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('1NUq7WKxDCmOymCGQeGOE8', 728)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('1SjjeVaXTiw0aucQWIQQki', 703)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('1zxUKf3qKYYSsKYuM0iIGK', 718)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('2Omjr8S1k4ws08eI0Ec4Ie', 712)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('2c9zSTNoq4MUUeKSMi2ESU', 719)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('337AqYx7eg8CAoeucYWmEQ', 720)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('3OXuuEiea4IO0oWsGYm8WI', 721)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('3dJdeCRXEASQE0eYmCAska', 704)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('3eTv5Ptx8kC4ICu8EYOEuq', 729)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('3kj0e2tNa0iMOeyGmUCeCI', 732)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('4JwXRvQfA4meaq4Omg0EoI', 722)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('4ZBNIc0uc8meekYaumSQyk', 734)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('4j4lMNE5a8EqcI2CmMuKgq', 713)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('4kSWUGTDvyakGuUM28yoaW', 738)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('4vtDyPmXjyi8IEyGmaogoy', 733)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('4x8njqGiooYk82aEAYI4w2', 710)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('54OMiRRDlC4eIuEEquMQoO', 705)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('57RmIL7LCo4qqOKsM4mYWw', 735)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('58QaaHrTA4qGo8U0kYoKIa', 714)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('5HP4UMl3q0y0E6oA0ai4kQ', 723)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('5xpNkuA4RGwSUSGc628qY6', 706)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('6rGEk2mzOoGqWeu6MeeScU', 736)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('7Gd4nXQkk8emEOEEIQw0Uc', 716)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('HfaIU8twg8Mic6SQkG8gU', 730)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('NH1IYFWTSekmSigE0KIw', 725)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('XY3v3RtfOuGiI2MekuMqM', 726)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('juD1w9rlgOaGwg6M8iiiO', 724)
INSERT INTO [cm].[TypeAsset] ([AssetKey], [AssetID]) VALUES ('mtL3DZ7BjqAEqcm4yc22S', 707)
PRINT(N'Operation applied to 29 rows out of 29')

PRINT(N'Add row to [cm].[TypeCategory]')
INSERT INTO [cm].[TypeCategory] ([CategoryKey], [CategoryID]) VALUES ('greenbuttonengine', 21)

PRINT(N'Add rows to [cm].[TypeEnumerationItem]')
INSERT INTO [cm].[TypeEnumerationItem] ([EnumerationItemKey], [EnumerationItemID]) VALUES ('hcf', 44)
INSERT INTO [cm].[TypeEnumerationItem] ([EnumerationItemKey], [EnumerationItemID]) VALUES ('mcf', 45)
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Add rows to [cm].[TypeFileContent]')
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busaceecon', 266)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busadjustflameburner', 267)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busairinfiltrationbarriers', 308)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busavoidoverfiring', 248)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busboosterheater', 268)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.buscirculationtempcontrols', 288)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.buscleanheatcoilsfrig', 297)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.buscloseairintake', 289)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busconsolidateimaging', 269)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.buscoverdisplaycases', 298)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busdesiccant', 270)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busdevicepowermanage', 271)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busdisplaycasemotors', 299)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busdownsizeventilation', 254)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.buseffcooking', 249)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busefficientexitsigns', 290)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busefftoaster', 256)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busenergystarequipment', 272)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busenthalpycontrols', 273)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busexternalwindowshading', 309)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.bushigheffchiller', 259)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.businstallheatpumpwh', 274)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.businstallheboiler', 275)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.businstallheheatpump', 276)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.businstallhorizonalwashers', 291)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.businstallwhtimer', 277)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.businsulateceilingtiles', 310)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.businsulateflatroof', 304)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.businsulatewh', 278)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busintegratedheatrecovery', 300)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.buslowerwhtemp', 279)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busmaintainheatsystem', 280)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busmaintcookappliances', 250)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busmanagepreheating', 251)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busoccupancysensors', 292)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busoptimizeheatcycles', 286)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busprogramthermostatheat', 281)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busprogthermcool', 257)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busraisetempac', 260)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busreduceunoccac', 261)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busreflectivewindowfilm', 311)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busreplacecentralac', 264)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busreplaceroomac', 262)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busreplacewaterheater', 282)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.bussealducts', 283)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.bussealwindowsdoors', 305)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busturnoffequipment', 284)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busupgradecommercialfrig', 301)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busupgradedisplaylighting', 293)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busupgradeoverheadlights', 294)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busupgradestandardfrig', 306)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.bususedaylighting', 295)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.busvariablespeedcompressors', 302)
INSERT INTO [cm].[TypeFileContent] ([FileContentKey], [FileContentID]) VALUES ('image.shutoffcookingequip', 252)
PRINT(N'Operation applied to 54 rows out of 54')

PRINT(N'Add rows to [cm].[TypeProfileSection]')
INSERT INTO [cm].[TypeProfileSection] ([ProfileSectionKey], [ProfileSectionID]) VALUES ('profilesection.shortappliances', 95)
INSERT INTO [cm].[TypeProfileSection] ([ProfileSectionKey], [ProfileSectionID]) VALUES ('profilesection.shortcooktop', 99)
INSERT INTO [cm].[TypeProfileSection] ([ProfileSectionKey], [ProfileSectionID]) VALUES ('profilesection.shortdryer', 96)
INSERT INTO [cm].[TypeProfileSection] ([ProfileSectionKey], [ProfileSectionID]) VALUES ('profilesection.shorthome1', 93)
INSERT INTO [cm].[TypeProfileSection] ([ProfileSectionKey], [ProfileSectionID]) VALUES ('profilesection.shorthottub', 100)
INSERT INTO [cm].[TypeProfileSection] ([ProfileSectionKey], [ProfileSectionID]) VALUES ('profilesection.shortoven', 101)
INSERT INTO [cm].[TypeProfileSection] ([ProfileSectionKey], [ProfileSectionID]) VALUES ('profilesection.shortpoolheater', 97)
PRINT(N'Operation applied to 7 rows out of 7')

PRINT(N'Add rows to [cm].[TypeTextContent]')
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('profilesection.shortcooktop.desc', 3806)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('profilesection.shortcooktop.subtitle', 3807)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('profilesection.shortcooktop.title', 3808)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('uom.hcf.name', 3803)
INSERT INTO [cm].[TypeTextContent] ([TextContentKey], [TextContentID]) VALUES ('uom.mcf.name', 3804)
PRINT(N'Operation applied to 5 rows out of 5')

PRINT(N'Add rows to [cm].[TypeUom]')
INSERT INTO [cm].[TypeUom] ([UomKey], [UomID]) VALUES ('hcf', 8)
INSERT INTO [cm].[TypeUom] ([UomKey], [UomID]) VALUES ('mcf', 12)
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Add rows to [cm].[ClientActionCondition]')
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (113, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (115, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (117, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (130, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (133, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (136, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (140, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (143, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (146, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (194, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (201, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (211, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (216, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (237, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (238, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (820, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (821, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (822, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (823, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (824, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (825, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (826, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (827, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (828, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (829, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (830, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (831, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (832, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (834, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (835, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (836, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (837, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (838, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (839, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (840, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (841, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (848, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (852, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (857, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (875, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (876, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (877, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (878, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (879, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (880, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (881, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (882, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (883, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (885, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (887, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (890, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (894, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (898, 'premisetype.residential', 0)
INSERT INTO [cm].[ClientActionCondition] ([ClientActionID], [ConditionKey], [DisplayOrder]) VALUES (905, 'premisetype.residential', 0)
PRINT(N'Operation applied to 54 rows out of 54')

PRINT(N'Add rows to [cm].[ClientAsset]')
SET IDENTITY_INSERT [cm].[ClientAsset] ON
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (700, 0, '1SjjeVaXTiw0aucQWIQQki')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (701, 0, '3dJdeCRXEASQE0eYmCAska')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (702, 0, '54OMiRRDlC4eIuEEquMQoO')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (703, 0, '5xpNkuA4RGwSUSGc628qY6')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (704, 0, 'mtL3DZ7BjqAEqcm4yc22S')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (705, 0, '1A0n2oY8GIMIyQkOMqKgMQ')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (706, 0, '4x8njqGiooYk82aEAYI4w2')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (707, 0, '2Omjr8S1k4ws08eI0Ec4Ie')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (708, 0, '4j4lMNE5a8EqcI2CmMuKgq')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (709, 0, '58QaaHrTA4qGo8U0kYoKIa')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (710, 0, '7Gd4nXQkk8emEOEEIQw0Uc')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (711, 0, '1zxUKf3qKYYSsKYuM0iIGK')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (712, 0, '2c9zSTNoq4MUUeKSMi2ESU')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (713, 0, '337AqYx7eg8CAoeucYWmEQ')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (714, 0, '3OXuuEiea4IO0oWsGYm8WI')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (715, 0, '4JwXRvQfA4meaq4Omg0EoI')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (716, 0, '5HP4UMl3q0y0E6oA0ai4kQ')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (717, 0, 'juD1w9rlgOaGwg6M8iiiO')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (718, 0, 'NH1IYFWTSekmSigE0KIw')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (719, 0, 'XY3v3RtfOuGiI2MekuMqM')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (720, 0, '1NUq7WKxDCmOymCGQeGOE8')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (721, 0, '3eTv5Ptx8kC4ICu8EYOEuq')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (722, 0, 'HfaIU8twg8Mic6SQkG8gU')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (723, 0, '3kj0e2tNa0iMOeyGmUCeCI')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (724, 0, '4vtDyPmXjyi8IEyGmaogoy')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (725, 0, '4ZBNIc0uc8meekYaumSQyk')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (726, 0, '57RmIL7LCo4qqOKsM4mYWw')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (727, 0, '6rGEk2mzOoGqWeu6MeeScU')
INSERT INTO [cm].[ClientAsset] ([ClientAssetID], [ClientID], [AssetKey]) VALUES (728, 0, '4kSWUGTDvyakGuUM28yoaW')
SET IDENTITY_INSERT [cm].[ClientAsset] OFF
PRINT(N'Operation applied to 29 rows out of 29')

PRINT(N'Add rows to [cm].[ClientEnumerationEnumerationItem]')
INSERT INTO [cm].[ClientEnumerationEnumerationItem] ([ClientEnumerationID], [EnumerationItemKey], [DisplayOrder], [IsDefault]) VALUES (18, 'hcf', 8, 0)
INSERT INTO [cm].[ClientEnumerationEnumerationItem] ([ClientEnumerationID], [EnumerationItemKey], [DisplayOrder], [IsDefault]) VALUES (18, 'mcf', 9, 0)
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Add rows to [cm].[ClientEnumerationItem]')
SET IDENTITY_INSERT [cm].[ClientEnumerationItem] ON
INSERT INTO [cm].[ClientEnumerationItem] ([ClientEnumerationItemID], [ClientID], [EnumerationItemKey], [NameKey]) VALUES (149, 0, 'hcf', 'uom.hcf.name')
INSERT INTO [cm].[ClientEnumerationItem] ([ClientEnumerationItemID], [ClientID], [EnumerationItemKey], [NameKey]) VALUES (150, 0, 'mcf', 'uom.mcf.name')
SET IDENTITY_INSERT [cm].[ClientEnumerationItem] OFF
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Add rows to [cm].[ClientFileContent]')
SET IDENTITY_INSERT [cm].[ClientFileContent] ON
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (283, 0, 'image.busavoidoverfiring', 'action', '3dJdeCRXEASQE0eYmCAska', '3dJdeCRXEASQE0eYmCAska', '3dJdeCRXEASQE0eYmCAska', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (284, 0, 'image.buseffcooking', 'action', 'mtL3DZ7BjqAEqcm4yc22S', 'mtL3DZ7BjqAEqcm4yc22S', 'mtL3DZ7BjqAEqcm4yc22S', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (285, 0, 'image.busmaintcookappliances', 'action', '5xpNkuA4RGwSUSGc628qY6', '5xpNkuA4RGwSUSGc628qY6', '5xpNkuA4RGwSUSGc628qY6', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (286, 0, 'image.busmanagepreheating', 'action', '5x4pwsXxDOkoSykIAaWyCE', '5x4pwsXxDOkoSykIAaWyCE', '5x4pwsXxDOkoSykIAaWyCE', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (287, 0, 'image.shutoffcookingequip', 'action', '54OMiRRDlC4eIuEEquMQoO', '54OMiRRDlC4eIuEEquMQoO', '54OMiRRDlC4eIuEEquMQoO', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (288, 0, 'image.busdownsizeventilation', 'action', '1SjjeVaXTiw0aucQWIQQki', '1SjjeVaXTiw0aucQWIQQki', '1SjjeVaXTiw0aucQWIQQki', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (289, 0, 'image.busefftoaster', 'action', '1A0n2oY8GIMIyQkOMqKgMQ', '1A0n2oY8GIMIyQkOMqKgMQ', '1A0n2oY8GIMIyQkOMqKgMQ', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (290, 0, 'image.busprogthermcool', 'action', '4x8njqGiooYk82aEAYI4w2', '4x8njqGiooYk82aEAYI4w2', '4x8njqGiooYk82aEAYI4w2', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (291, 0, 'image.bushigheffchiller', 'action', '2Omjr8S1k4ws08eI0Ec4Ie', '2Omjr8S1k4ws08eI0Ec4Ie', '2Omjr8S1k4ws08eI0Ec4Ie', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (292, 0, 'image.busraisetempac', 'action', '4j4lMNE5a8EqcI2CmMuKgq', '4j4lMNE5a8EqcI2CmMuKgq', '4j4lMNE5a8EqcI2CmMuKgq', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (293, 0, 'image.busreduceunoccac', 'action', '58QaaHrTA4qGo8U0kYoKIa', '58QaaHrTA4qGo8U0kYoKIa', '58QaaHrTA4qGo8U0kYoKIa', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (294, 0, 'image.busreplaceroomac', 'action', '436361RoomACExteriorsmall', '436361RoomACExteriorsmall', '436361RoomACExteriorsmall', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (295, 0, 'image.busreplacecentralac', 'action', '7Gd4nXQkk8emEOEEIQw0Uc', '7Gd4nXQkk8emEOEEIQw0Uc', '7Gd4nXQkk8emEOEEIQw0Uc', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (296, 0, 'image.busaceecon', 'action', 'sAZcPRV6GkKwUgAKIC6UG', 'sAZcPRV6GkKwUgAKIC6UG', 'sAZcPRV6GkKwUgAKIC6UG', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (297, 0, 'image.busadjustflameburner', 'action', '2c9zSTNoq4MUUeKSMi2ESU', '2c9zSTNoq4MUUeKSMi2ESU', '2c9zSTNoq4MUUeKSMi2ESU', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (298, 0, 'image.busboosterheater', 'action', 'juD1w9rlgOaGwg6M8iiiO', 'juD1w9rlgOaGwg6M8iiiO', 'juD1w9rlgOaGwg6M8iiiO', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (299, 0, 'image.busconsolidateimaging', 'action', '5HP4UMl3q0y0E6oA0ai4kQ', '5HP4UMl3q0y0E6oA0ai4kQ', '5HP4UMl3q0y0E6oA0ai4kQ', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (300, 0, 'image.busdesiccant', 'action', '4JwXRvQfA4meaq4Omg0EoI', '4JwXRvQfA4meaq4Omg0EoI', '4JwXRvQfA4meaq4Omg0EoI', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (301, 0, 'image.busdevicepowermanage', 'action', 'NH1IYFWTSekmSigE0KIw', 'NH1IYFWTSekmSigE0KIw', 'NH1IYFWTSekmSigE0KIw', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (302, 0, 'image.busenergystarequipment', 'action', 'XY3v3RtfOuGiI2MekuMqM', 'XY3v3RtfOuGiI2MekuMqM', 'XY3v3RtfOuGiI2MekuMqM', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (303, 0, 'image.busenthalpycontrols', 'action', '4ohljiULHiKky8SG028GkW', '4ohljiULHiKky8SG028GkW', '4ohljiULHiKky8SG028GkW', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (304, 0, 'image.businstallheatpumpwh', 'action', '43741081faucetwithdripsmall', '43741081faucetwithdripsmall', '43741081faucetwithdripsmall', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (305, 0, 'image.businstallheboiler', 'action', '38f13GlqXYgA2UAmaqqoIs', '38f13GlqXYgA2UAmaqqoIs', '38f13GlqXYgA2UAmaqqoIs', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (306, 0, 'image.businstallheheatpump', 'action', '4XpL8reHy8CWyyIAsAIACm', '4XpL8reHy8CWyyIAsAIACm', '4XpL8reHy8CWyyIAsAIACm', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (307, 0, 'image.businstallwhtimer', 'action', '223356295faucetwithhotwatersmall', '223356295faucetwithhotwatersmall', '223356295faucetwithhotwatersmall', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (308, 0, 'image.businsulatewh', 'action', '337AqYx7eg8CAoeucYWmEQ', '337AqYx7eg8CAoeucYWmEQ', '337AqYx7eg8CAoeucYWmEQ', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (309, 0, 'image.buslowerwhtemp', 'action', '142322101technicianadjustingwaterheatertempsmall', '142322101technicianadjustingwaterheatertempsmall', '142322101technicianadjustingwaterheatertempsmall', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (310, 0, 'image.busmaintainheatsystem', 'action', '2wlVoFcpOogiYYAmUCisqe', '15KtG4G0Xgk0agKKqIWK4o', '15KtG4G0Xgk0agKKqIWK4o', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (311, 0, 'image.busprogramthermostatheat', 'action', '92965054programmablethermostatcloseupsmall', '92965054programmablethermostatcloseupsmall', '92965054programmablethermostatcloseupsmall', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (312, 0, 'image.busreplacewaterheater', 'action', '3OXuuEiea4IO0oWsGYm8WI', '3OXuuEiea4IO0oWsGYm8WI', '3OXuuEiea4IO0oWsGYm8WI', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (313, 0, 'image.bussealducts', 'action', '1zxUKf3qKYYSsKYuM0iIGK', '1zxUKf3qKYYSsKYuM0iIGK', '1zxUKf3qKYYSsKYuM0iIGK', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (314, 0, 'image.busturnoffequipment', 'action', '4i1ysUwXnqE6aoQS8o8iMS', '4i1ysUwXnqE6aoQS8o8iMS', '4i1ysUwXnqE6aoQS8o8iMS', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (315, 0, 'image.busoptimizeheatcycles', 'action', '6GnWnPjZSgIsgKUqKOqUa2', '6GnWnPjZSgIsgKUqKOqUa2', '6GnWnPjZSgIsgKUqKOqUa2', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (316, 0, 'image.buscirculationtempcontrols', 'action', 'HfaIU8twg8Mic6SQkG8gU', 'HfaIU8twg8Mic6SQkG8gU', 'HfaIU8twg8Mic6SQkG8gU', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (317, 0, 'image.buscloseairintake', 'action', '3eTv5Ptx8kC4ICu8EYOEuq', '3eTv5Ptx8kC4ICu8EYOEuq', '3eTv5Ptx8kC4ICu8EYOEuq', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (318, 0, 'image.busefficientexitsigns', 'action', '5sE892qxUWOU060ciUwGOK', '5sE892qxUWOU060ciUwGOK', '5sE892qxUWOU060ciUwGOK', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (319, 0, 'image.businstallhorizonalwashers', 'action', '1NUq7WKxDCmOymCGQeGOE8', '1NUq7WKxDCmOymCGQeGOE8', '1NUq7WKxDCmOymCGQeGOE8', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (320, 0, 'image.busoccupancysensors', 'action', '6SmJHuPjaw0A2ukSukMIoo', '6SmJHuPjaw0A2ukSukMIoo', '6SmJHuPjaw0A2ukSukMIoo', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (321, 0, 'image.busupgradedisplaylighting', 'action', 'rhFpxIENfUom4wwiIUaoo', 'rhFpxIENfUom4wwiIUaoo', 'rhFpxIENfUom4wwiIUaoo', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (322, 0, 'image.busupgradeoverheadlights', 'action', '6W4KAhZZvOIe4QS4UcuUog', '6W4KAhZZvOIe4QS4UcuUog', '6W4KAhZZvOIe4QS4UcuUog', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (323, 0, 'image.bususedaylighting', 'action', '56tBL8ZrsscaO84O4am0sU', '56tBL8ZrsscaO84O4am0sU', '56tBL8ZrsscaO84O4am0sU', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (324, 0, 'image.buscleanheatcoilsfrig', 'action', 'iyCakF7nr2yWsw02ueKuS', 'iyCakF7nr2yWsw02ueKuS', 'iyCakF7nr2yWsw02ueKuS', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (325, 0, 'image.buscoverdisplaycases', 'action', '57RmIL7LCo4qqOKsM4mYWw', '57RmIL7LCo4qqOKsM4mYWw', '57RmIL7LCo4qqOKsM4mYWw', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (326, 0, 'image.busdisplaycasemotors', 'action', '4ZBNIc0uc8meekYaumSQyk', '4ZBNIc0uc8meekYaumSQyk', '4ZBNIc0uc8meekYaumSQyk', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (327, 0, 'image.busintegratedheatrecovery', 'action', '2TLw9CObZmO6eK02806WKo', '2TLw9CObZmO6eK02806WKo', '2TLw9CObZmO6eK02806WKo', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (328, 0, 'image.busupgradecommercialfrig', 'action', '6rGEk2mzOoGqWeu6MeeScU', '6rGEk2mzOoGqWeu6MeeScU', '6rGEk2mzOoGqWeu6MeeScU', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (329, 0, 'image.busvariablespeedcompressors', 'action', '3kj0e2tNa0iMOeyGmUCeCI', '3kj0e2tNa0iMOeyGmUCeCI', '3kj0e2tNa0iMOeyGmUCeCI', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (330, 0, 'image.businsulateflatroof', 'action', '6DdKbC0On6QqoqSIUiuYSk', '6DdKbC0On6QqoqSIUiuYSk', '6DdKbC0On6QqoqSIUiuYSk', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (331, 0, 'image.bussealwindowsdoors', 'action', '2BM3IJqyb2CwSwiI8OW6aW', '2BM3IJqyb2CwSwiI8OW6aW', '2BM3IJqyb2CwSwiI8OW6aW', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (332, 0, 'image.busupgradestandardfrig', 'action', '4vtDyPmXjyi8IEyGmaogoy', '6zbqoie8WA0Uc4S80aEIGk', '6zbqoie8WA0Uc4S80aEIGk', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (333, 0, 'image.busairinfiltrationbarriers', 'action', '4kSWUGTDvyakGuUM28yoaW', '4kSWUGTDvyakGuUM28yoaW', '4kSWUGTDvyakGuUM28yoaW', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (334, 0, 'image.busexternalwindowshading', 'action', 'zwwwaQExYOcCYwQUs8og6', 'zwwwaQExYOcCYwQUs8og6', 'zwwwaQExYOcCYwQUs8og6', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (335, 0, 'image.businsulateceilingtiles', 'action', '39TR8Y3nxYWQ6eU6yCWqQS', '39TR8Y3nxYWQ6eU6yCWqQS', '39TR8Y3nxYWQ6eU6yCWqQS', '')
INSERT INTO [cm].[ClientFileContent] ([ClientFileID], [ClientID], [FileContentKey], [CategoryKey], [SmallFile], [MediumFile], [LargeFile], [Type]) VALUES (336, 0, 'image.busreflectivewindowfilm', 'action', '6069i3mDKwQyMOiOmEOC4g', '6069i3mDKwQyMOiOmEOC4g', '6069i3mDKwQyMOiOmEOC4g', '')
SET IDENTITY_INSERT [cm].[ClientFileContent] OFF
PRINT(N'Operation applied to 54 rows out of 54')

PRINT(N'Add rows to [cm].[ClientProfileAttributeProfileOption]')
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (1926, 'business.oven.count.eq0', 1)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (1926, 'business.oven.count.eq1', 2)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (1926, 'business.oven.count.eq2', 3)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (1926, 'business.oven.count.eq3', 4)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (1926, 'business.oven.count.eq4', 5)
INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID], [ProfileOptionKey], [DisplayOrder]) VALUES (1926, 'business.oven.count.eq5', 6)
PRINT(N'Operation applied to 6 rows out of 6')

PRINT(N'Add rows to [cm].[ClientProfileSection]')
SET IDENTITY_INSERT [cm].[ClientProfileSection] ON
INSERT INTO [cm].[ClientProfileSection] ([ClientProfileSectionID], [ClientID], [ProfileSectionKey], [Name], [DescriptionKey], [TitleKey], [SubTitleKey], [Rank], [Images], [Disable], [IntroTextKey]) VALUES (142, 0, 'profilesection.shorthome1', 'Most important home information', 'profilesection.home1.desc', 'profilesection.home1.title', 'profilesection.home1.subtitle', 10, 'laptopman.image', 0, 'profilesection.home1.intro')
INSERT INTO [cm].[ClientProfileSection] ([ClientProfileSectionID], [ClientID], [ProfileSectionKey], [Name], [DescriptionKey], [TitleKey], [SubTitleKey], [Rank], [Images], [Disable], [IntroTextKey]) VALUES (143, 0, 'profilesection.shortappliances', 'Appliance profile', 'profilesection.extraappliances.desc', 'profilesection.extraappliances.title', 'profilesection.extraappliances.subtitle', 70, 'image.188730713smallfreezer', 0, 'profilesection.gasappliances.intro')
INSERT INTO [cm].[ClientProfileSection] ([ClientProfileSectionID], [ClientID], [ProfileSectionKey], [Name], [DescriptionKey], [TitleKey], [SubTitleKey], [Rank], [Images], [Disable], [IntroTextKey]) VALUES (144, 0, 'profilesection.shortdryer', 'Dryer', 'profilesection.dryer.desc', 'profilesection.dryer.title', 'profilesection.dryer.subtitle', 180, 'image.77883730laundry', 0, NULL)
INSERT INTO [cm].[ClientProfileSection] ([ClientProfileSectionID], [ClientID], [ProfileSectionKey], [Name], [DescriptionKey], [TitleKey], [SubTitleKey], [Rank], [Images], [Disable], [IntroTextKey]) VALUES (145, 0, 'profilesection.shortpoolheater', 'Pool heater details', 'profilesection.poolheater.desc', 'profilesection.poolheater.title', 'profilesection.poolheater.subtitle', 160, 'image.254911351swimmingpoolwithfloat', 0, NULL)
INSERT INTO [cm].[ClientProfileSection] ([ClientProfileSectionID], [ClientID], [ProfileSectionKey], [Name], [DescriptionKey], [TitleKey], [SubTitleKey], [Rank], [Images], [Disable], [IntroTextKey]) VALUES (149, 0, 'profilesection.shortcooktop', 'Cooktop', 'profilesection.shortcooktop.desc', 'profilesection.shortcooktop.title', 'profilesection.shortcooktop.subtitle', 190, 'image.116613715womancooking', 0, NULL)
INSERT INTO [cm].[ClientProfileSection] ([ClientProfileSectionID], [ClientID], [ProfileSectionKey], [Name], [DescriptionKey], [TitleKey], [SubTitleKey], [Rank], [Images], [Disable], [IntroTextKey]) VALUES (150, 0, 'profilesection.shorthottub', 'Hottub', 'profilesection.hottub.desc', 'profilesection.hottub.title', 'profilesection.hottub.subtitle', 170, 'image.14167462hottub', 0, NULL)
INSERT INTO [cm].[ClientProfileSection] ([ClientProfileSectionID], [ClientID], [ProfileSectionKey], [Name], [DescriptionKey], [TitleKey], [SubTitleKey], [Rank], [Images], [Disable], [IntroTextKey]) VALUES (151, 0, 'profilesection.shortoven', 'Oven', 'profilesection.cooking.desc', 'profilesection.cooking.title', 'profilesection.cooking.subtitle', 190, 'image.143613391kitchen', 0, NULL)
SET IDENTITY_INSERT [cm].[ClientProfileSection] OFF
PRINT(N'Operation applied to 7 rows out of 7')

PRINT(N'Add rows to [cm].[ClientTabProfileSection]')
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (99, 'profilesection.shortappliances', 4)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (99, 'profilesection.shortcooktop', 9)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (99, 'profilesection.shortdryer', 5)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (99, 'profilesection.shorthome1', 1)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (99, 'profilesection.shorthottub', 7)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (99, 'profilesection.shortoven', 8)
INSERT INTO [cm].[ClientTabProfileSection] ([ClientTabID], [ProfileSectionKey], [Rank]) VALUES (99, 'profilesection.shortpoolheater', 6)
PRINT(N'Operation applied to 7 rows out of 7')

PRINT(N'Add rows to [cm].[ClientTextContent]')
SET IDENTITY_INSERT [cm].[ClientTextContent] ON
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (6597, 0, 'uom.hcf.name', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (6598, 0, 'uom.mcf.name', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (6599, 0, 'profilesection.shortcooktop.desc', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (6600, 0, 'profilesection.shortcooktop.subtitle', 'common')
INSERT INTO [cm].[ClientTextContent] ([ClientTextContentID], [ClientID], [TextContentKey], [CategoryKey]) VALUES (6601, 0, 'profilesection.shortcooktop.title', 'common')
SET IDENTITY_INSERT [cm].[ClientTextContent] OFF
PRINT(N'Operation applied to 5 rows out of 5')

PRINT(N'Add rows to [cm].[ClientUOM]')
SET IDENTITY_INSERT [cm].[ClientUOM] ON
INSERT INTO [cm].[ClientUOM] ([ClientUOMID], [ClientID], [UomKey], [NameKey]) VALUES (8, 0, 'hcf', 'uom.hcf.name')
INSERT INTO [cm].[ClientUOM] ([ClientUOMID], [ClientID], [UomKey], [NameKey]) VALUES (9, 0, 'mcf', 'uom.mcf.name')
SET IDENTITY_INSERT [cm].[ClientUOM] OFF
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Add rows to [cm].[ClientAssetLocaleContent]')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (700, 'en-US', N'image business 177566807 downsize ventilation', N'image 177566807 for business downsize ventilation', '177566807sm.jpg', 'image/jpeg', 12436, '//images.contentful.com/wp32f8j5jqi2/1SjjeVaXTiw0aucQWIQQki/3e1d05e1b4dd53992890a1ce0060d91b/177566807sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (700, 'es-ES', N'image business 177566807 downsize ventilation', N'image 177566807 for business downsize ventilation', '177566807sm.jpg', 'image/jpeg', 12436, '//images.contentful.com/wp32f8j5jqi2/1SjjeVaXTiw0aucQWIQQki/3e1d05e1b4dd53992890a1ce0060d91b/177566807sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (700, 'ru-RU', N'image business 177566807 downsize ventilation', N'image 177566807 for business downsize ventilation', '177566807sm.jpg', 'image/jpeg', 12436, '//images.contentful.com/wp32f8j5jqi2/1SjjeVaXTiw0aucQWIQQki/3e1d05e1b4dd53992890a1ce0060d91b/177566807sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (701, 'en-US', N'image.busavoidoverfiring113264989sm', N'image for business action avoid overfiring', '113264989sm.jpg', 'image/jpeg', 13317, '//images.contentful.com/wp32f8j5jqi2/3dJdeCRXEASQE0eYmCAska/728941f8f6d2460fea7967c89fc563cc/113264989sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (701, 'es-ES', N'image.busavoidoverfiring113264989sm', N'image for business action avoid overfiring', '113264989sm.jpg', 'image/jpeg', 13317, '//images.contentful.com/wp32f8j5jqi2/3dJdeCRXEASQE0eYmCAska/728941f8f6d2460fea7967c89fc563cc/113264989sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (701, 'ru-RU', N'image.busavoidoverfiring113264989sm', N'image for business action avoid overfiring', '113264989sm.jpg', 'image/jpeg', 13317, '//images.contentful.com/wp32f8j5jqi2/3dJdeCRXEASQE0eYmCAska/728941f8f6d2460fea7967c89fc563cc/113264989sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (702, 'en-US', N'image.busshutoffcookequip118458406sm', N' 118458406  image for shutoffbookequip business measure', 'shutterstock_118458406.jpg', 'image/jpeg', 15942, '//images.contentful.com/wp32f8j5jqi2/54OMiRRDlC4eIuEEquMQoO/ddf6b45dc7e6d8cdc70ac55735a6dff5/shutterstock_118458406.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (702, 'es-ES', N'image.busshutoffcookequip118458406sm', N' 118458406  image for shutoffbookequip business measure', 'shutterstock_118458406.jpg', 'image/jpeg', 15942, '//images.contentful.com/wp32f8j5jqi2/54OMiRRDlC4eIuEEquMQoO/ddf6b45dc7e6d8cdc70ac55735a6dff5/shutterstock_118458406.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (702, 'ru-RU', N'image.busshutoffcookequip118458406sm', N' 118458406  image for shutoffbookequip business measure', 'shutterstock_118458406.jpg', 'image/jpeg', 15942, '//images.contentful.com/wp32f8j5jqi2/54OMiRRDlC4eIuEEquMQoO/ddf6b45dc7e6d8cdc70ac55735a6dff5/shutterstock_118458406.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (703, 'en-US', N'busimage178419896sm', N'image for business.maintaincookingappliances action', '178419896sm.jpg', 'image/jpeg', 11469, '//images.contentful.com/wp32f8j5jqi2/5xpNkuA4RGwSUSGc628qY6/19b51924a2e86d29b73cfd49e1d64c85/178419896sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (703, 'es-ES', N'busimage178419896sm', N'image for business.maintaincookingappliances action', '178419896sm.jpg', 'image/jpeg', 11469, '//images.contentful.com/wp32f8j5jqi2/5xpNkuA4RGwSUSGc628qY6/19b51924a2e86d29b73cfd49e1d64c85/178419896sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (703, 'ru-RU', N'busimage178419896sm', N'image for business.maintaincookingappliances action', '178419896sm.jpg', 'image/jpeg', 11469, '//images.contentful.com/wp32f8j5jqi2/5xpNkuA4RGwSUSGc628qY6/19b51924a2e86d29b73cfd49e1d64c85/178419896sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (704, 'en-US', N'image.buseffcooksm98045243sm', N'image 9045243 for action business efficient cooking', 'shutterstock_98045243.jpg', 'image/jpeg', 8404, '//images.contentful.com/wp32f8j5jqi2/mtL3DZ7BjqAEqcm4yc22S/74889746c074e8052845a9fae1da944d/shutterstock_98045243.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (704, 'es-ES', N'image.buseffcooksm98045243sm', N'image 9045243 for action business efficient cooking', 'shutterstock_98045243.jpg', 'image/jpeg', 8404, '//images.contentful.com/wp32f8j5jqi2/mtL3DZ7BjqAEqcm4yc22S/74889746c074e8052845a9fae1da944d/shutterstock_98045243.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (704, 'ru-RU', N'image.buseffcooksm98045243sm', N'image 9045243 for action business efficient cooking', 'shutterstock_98045243.jpg', 'image/jpeg', 8404, '//images.contentful.com/wp32f8j5jqi2/mtL3DZ7BjqAEqcm4yc22S/74889746c074e8052845a9fae1da944d/shutterstock_98045243.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (705, 'en-US', N'image business efficient toaster', N'image for action business efficient toaster 309009758', '309009758sm.jpg', 'image/jpeg', 14447, '//images.contentful.com/wp32f8j5jqi2/1A0n2oY8GIMIyQkOMqKgMQ/da6657030dc88568df093a581e7749ed/309009758sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (705, 'es-ES', N'image business efficient toaster', N'image for action business efficient toaster 309009758', '309009758sm.jpg', 'image/jpeg', 14447, '//images.contentful.com/wp32f8j5jqi2/1A0n2oY8GIMIyQkOMqKgMQ/da6657030dc88568df093a581e7749ed/309009758sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (705, 'ru-RU', N'image business efficient toaster', N'image for action business efficient toaster 309009758', '309009758sm.jpg', 'image/jpeg', 14447, '//images.contentful.com/wp32f8j5jqi2/1A0n2oY8GIMIyQkOMqKgMQ/da6657030dc88568df093a581e7749ed/309009758sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (706, 'en-US', N'busimg378414529sm', N'image for 378414529 business.programthermostatcool', '378414259sm.jpg', 'image/jpeg', 8786, '//images.contentful.com/wp32f8j5jqi2/4x8njqGiooYk82aEAYI4w2/108b0f5835dee7a9af87e253293b1152/378414259sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (706, 'es-ES', N'busimg378414529sm', N'image for 378414529 business.programthermostatcool', '378414259sm.jpg', 'image/jpeg', 8786, '//images.contentful.com/wp32f8j5jqi2/4x8njqGiooYk82aEAYI4w2/108b0f5835dee7a9af87e253293b1152/378414259sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (706, 'ru-RU', N'busimg378414529sm', N'image for 378414529 business.programthermostatcool', '378414259sm.jpg', 'image/jpeg', 8786, '//images.contentful.com/wp32f8j5jqi2/4x8njqGiooYk82aEAYI4w2/108b0f5835dee7a9af87e253293b1152/378414259sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (707, 'en-US', N'busimghigheffchill248162068sm', N'image business action high efficiency chiller 248162068', '248162068sm.jpg', 'image/jpeg', 15643, '//images.contentful.com/wp32f8j5jqi2/2Omjr8S1k4ws08eI0Ec4Ie/1050b0c3de8d82d13ffeeabf6e141e1f/248162068sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (707, 'es-ES', N'busimghigheffchill248162068sm', N'image business action high efficiency chiller 248162068', '248162068sm.jpg', 'image/jpeg', 15643, '//images.contentful.com/wp32f8j5jqi2/2Omjr8S1k4ws08eI0Ec4Ie/1050b0c3de8d82d13ffeeabf6e141e1f/248162068sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (707, 'ru-RU', N'busimghigheffchill248162068sm', N'image business action high efficiency chiller 248162068', '248162068sm.jpg', 'image/jpeg', 15643, '//images.contentful.com/wp32f8j5jqi2/2Omjr8S1k4ws08eI0Ec4Ie/1050b0c3de8d82d13ffeeabf6e141e1f/248162068sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (708, 'en-US', N'imagebusraisetempac344282417sm', N'image for action business raise temp ac 344282417', '344282417sm.jpg', 'image/jpeg', 7945, '//images.contentful.com/wp32f8j5jqi2/4j4lMNE5a8EqcI2CmMuKgq/432f3b145f1ad8b420c20f736eccd357/344282417sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (708, 'es-ES', N'imagebusraisetempac344282417sm', N'image for action business raise temp ac 344282417', '344282417sm.jpg', 'image/jpeg', 7945, '//images.contentful.com/wp32f8j5jqi2/4j4lMNE5a8EqcI2CmMuKgq/432f3b145f1ad8b420c20f736eccd357/344282417sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (708, 'ru-RU', N'imagebusraisetempac344282417sm', N'image for action business raise temp ac 344282417', '344282417sm.jpg', 'image/jpeg', 7945, '//images.contentful.com/wp32f8j5jqi2/4j4lMNE5a8EqcI2CmMuKgq/432f3b145f1ad8b420c20f736eccd357/344282417sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (709, 'en-US', N'busimageredunocac165181559sm', N'business image reduce unoccupied ac  165181559', '165181559sm.jpg', 'image/jpeg', 13273, '//images.contentful.com/wp32f8j5jqi2/58QaaHrTA4qGo8U0kYoKIa/4197a54472accd143955eb9b62f47765/165181559sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (709, 'es-ES', N'busimageredunocac165181559sm', N'business image reduce unoccupied ac  165181559', '165181559sm.jpg', 'image/jpeg', 13273, '//images.contentful.com/wp32f8j5jqi2/58QaaHrTA4qGo8U0kYoKIa/4197a54472accd143955eb9b62f47765/165181559sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (709, 'ru-RU', N'busimageredunocac165181559sm', N'business image reduce unoccupied ac  165181559', '165181559sm.jpg', 'image/jpeg', 13273, '//images.contentful.com/wp32f8j5jqi2/58QaaHrTA4qGo8U0kYoKIa/4197a54472accd143955eb9b62f47765/165181559sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (710, 'en-US', N'busimage62403391sm', N'image for business action replace central ac', '62403391sm.jpg', 'image/jpeg', 14045, '//images.contentful.com/wp32f8j5jqi2/7Gd4nXQkk8emEOEEIQw0Uc/ca68a52489164e0231e3fbd025dde061/62403391sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (710, 'es-ES', N'busimage62403391sm', N'image for business action replace central ac', '62403391sm.jpg', 'image/jpeg', 14045, '//images.contentful.com/wp32f8j5jqi2/7Gd4nXQkk8emEOEEIQw0Uc/ca68a52489164e0231e3fbd025dde061/62403391sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (710, 'ru-RU', N'busimage62403391sm', N'image for business action replace central ac', '62403391sm.jpg', 'image/jpeg', 14045, '//images.contentful.com/wp32f8j5jqi2/7Gd4nXQkk8emEOEEIQw0Uc/ca68a52489164e0231e3fbd025dde061/62403391sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (711, 'en-US', N'busimage384938695sm', N'image for business measure seal ducts', '384938695sm.jpg', 'image/jpeg', 8275, '//images.contentful.com/wp32f8j5jqi2/1zxUKf3qKYYSsKYuM0iIGK/93eca6f141c944239e0d2f4f0070858c/384938695sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (711, 'es-ES', N'busimage384938695sm', N'image for business measure seal ducts', '384938695sm.jpg', 'image/jpeg', 8275, '//images.contentful.com/wp32f8j5jqi2/1zxUKf3qKYYSsKYuM0iIGK/93eca6f141c944239e0d2f4f0070858c/384938695sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (711, 'ru-RU', N'busimage384938695sm', N'image for business measure seal ducts', '384938695sm.jpg', 'image/jpeg', 8275, '//images.contentful.com/wp32f8j5jqi2/1zxUKf3qKYYSsKYuM0iIGK/93eca6f141c944239e0d2f4f0070858c/384938695sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (712, 'en-US', N'busimage1135537sm', N'image for business measure adjust flame burner 1135537', '1135537sm.jpg', 'image/jpeg', 3325, '//images.contentful.com/wp32f8j5jqi2/2c9zSTNoq4MUUeKSMi2ESU/1dc1bb4df0b96c6570d61258a969f427/1135537sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (712, 'es-ES', N'busimage1135537sm', N'image for business measure adjust flame burner 1135537', '1135537sm.jpg', 'image/jpeg', 3325, '//images.contentful.com/wp32f8j5jqi2/2c9zSTNoq4MUUeKSMi2ESU/1dc1bb4df0b96c6570d61258a969f427/1135537sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (712, 'ru-RU', N'busimage1135537sm', N'image for business measure adjust flame burner 1135537', '1135537sm.jpg', 'image/jpeg', 3325, '//images.contentful.com/wp32f8j5jqi2/2c9zSTNoq4MUUeKSMi2ESU/1dc1bb4df0b96c6570d61258a969f427/1135537sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (713, 'en-US', N'busimage370706066sm', N'image for action business insulatewh', '370706066sm.jpg', 'image/jpeg', 12287, '//images.contentful.com/wp32f8j5jqi2/337AqYx7eg8CAoeucYWmEQ/173b3b68024b0f0dc81be991140f261e/370706066sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (713, 'es-ES', N'busimage370706066sm', N'image for action business insulatewh', '370706066sm.jpg', 'image/jpeg', 12287, '//images.contentful.com/wp32f8j5jqi2/337AqYx7eg8CAoeucYWmEQ/173b3b68024b0f0dc81be991140f261e/370706066sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (713, 'ru-RU', N'busimage370706066sm', N'image for action business insulatewh', '370706066sm.jpg', 'image/jpeg', 12287, '//images.contentful.com/wp32f8j5jqi2/337AqYx7eg8CAoeucYWmEQ/173b3b68024b0f0dc81be991140f261e/370706066sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (714, 'en-US', N'busimagereplacewaterheater21590251sm', N'image for business action replace water heater  21590251 small', '21590251sm.jpg', 'image/jpeg', 11381, '//images.contentful.com/wp32f8j5jqi2/3OXuuEiea4IO0oWsGYm8WI/91b25220de58553649dff5a02ae10c87/21590251sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (714, 'es-ES', N'busimagereplacewaterheater21590251sm', N'image for business action replace water heater  21590251 small', '21590251sm.jpg', 'image/jpeg', 11381, '//images.contentful.com/wp32f8j5jqi2/3OXuuEiea4IO0oWsGYm8WI/91b25220de58553649dff5a02ae10c87/21590251sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (714, 'ru-RU', N'busimagereplacewaterheater21590251sm', N'image for business action replace water heater  21590251 small', '21590251sm.jpg', 'image/jpeg', 11381, '//images.contentful.com/wp32f8j5jqi2/3OXuuEiea4IO0oWsGYm8WI/91b25220de58553649dff5a02ae10c87/21590251sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (715, 'en-US', N'busimage285339023', N'image for business action desiccant', '285339023sm.jpg', 'image/jpeg', 16292, '//images.contentful.com/wp32f8j5jqi2/4JwXRvQfA4meaq4Omg0EoI/7347b7166fbc27de2507175792a11659/285339023sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (715, 'es-ES', N'busimage285339023', N'image for business action desiccant', '285339023sm.jpg', 'image/jpeg', 16292, '//images.contentful.com/wp32f8j5jqi2/4JwXRvQfA4meaq4Omg0EoI/7347b7166fbc27de2507175792a11659/285339023sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (715, 'ru-RU', N'busimage285339023', N'image for business action desiccant', '285339023sm.jpg', 'image/jpeg', 16292, '//images.contentful.com/wp32f8j5jqi2/4JwXRvQfA4meaq4Omg0EoI/7347b7166fbc27de2507175792a11659/285339023sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (716, 'en-US', N'bus109653467sm', N'image for business measure consolidate imaging', '109653467sm.jpg', 'image/jpeg', 8552, '//images.contentful.com/wp32f8j5jqi2/5HP4UMl3q0y0E6oA0ai4kQ/e706d2f6b6a2258727c2d41ed09da73f/109653467sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (716, 'es-ES', N'bus109653467sm', N'image for business measure consolidate imaging', '109653467sm.jpg', 'image/jpeg', 8552, '//images.contentful.com/wp32f8j5jqi2/5HP4UMl3q0y0E6oA0ai4kQ/e706d2f6b6a2258727c2d41ed09da73f/109653467sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (716, 'ru-RU', N'bus109653467sm', N'image for business measure consolidate imaging', '109653467sm.jpg', 'image/jpeg', 8552, '//images.contentful.com/wp32f8j5jqi2/5HP4UMl3q0y0E6oA0ai4kQ/e706d2f6b6a2258727c2d41ed09da73f/109653467sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (717, 'en-US', N'busimageboostheater384192526sm', N'image for business action boosterheater', '384192526sm.jpg', 'image/jpeg', 7734, '//images.contentful.com/wp32f8j5jqi2/juD1w9rlgOaGwg6M8iiiO/8557c236ec723195e3b5fef2ef877dcf/384192526sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (717, 'es-ES', N'busimageboostheater384192526sm', N'image for business action boosterheater', '384192526sm.jpg', 'image/jpeg', 7734, '//images.contentful.com/wp32f8j5jqi2/juD1w9rlgOaGwg6M8iiiO/8557c236ec723195e3b5fef2ef877dcf/384192526sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (717, 'ru-RU', N'busimageboostheater384192526sm', N'image for business action boosterheater', '384192526sm.jpg', 'image/jpeg', 7734, '//images.contentful.com/wp32f8j5jqi2/juD1w9rlgOaGwg6M8iiiO/8557c236ec723195e3b5fef2ef877dcf/384192526sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (718, 'en-US', N'busimage404062066sm', N'image for business action device power manage', '404062066sm.jpg', 'image/jpeg', 11792, '//images.contentful.com/wp32f8j5jqi2/NH1IYFWTSekmSigE0KIw/1663b147648f0b24560c8cae2a31dac1/404062066sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (718, 'es-ES', N'busimage404062066sm', N'image for business action device power manage', '404062066sm.jpg', 'image/jpeg', 11792, '//images.contentful.com/wp32f8j5jqi2/NH1IYFWTSekmSigE0KIw/1663b147648f0b24560c8cae2a31dac1/404062066sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (718, 'ru-RU', N'busimage404062066sm', N'image for business action device power manage', '404062066sm.jpg', 'image/jpeg', 11792, '//images.contentful.com/wp32f8j5jqi2/NH1IYFWTSekmSigE0KIw/1663b147648f0b24560c8cae2a31dac1/404062066sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (719, 'en-US', N'busimage291555194sm', N'business image for action energy star equipment', '291555194sm.jpg', 'image/jpeg', 8704, '//images.contentful.com/wp32f8j5jqi2/XY3v3RtfOuGiI2MekuMqM/c955d5213d5b16fb626a507f17581d53/291555194sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (719, 'es-ES', N'busimage291555194sm', N'business image for action energy star equipment', '291555194sm.jpg', 'image/jpeg', 8704, '//images.contentful.com/wp32f8j5jqi2/XY3v3RtfOuGiI2MekuMqM/c955d5213d5b16fb626a507f17581d53/291555194sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (719, 'ru-RU', N'busimage291555194sm', N'business image for action energy star equipment', '291555194sm.jpg', 'image/jpeg', 8704, '//images.contentful.com/wp32f8j5jqi2/XY3v3RtfOuGiI2MekuMqM/c955d5213d5b16fb626a507f17581d53/291555194sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (720, 'en-US', N'busimage285478865sm', N'business image install horizontal washers', '285478865sm.jpg', 'image/jpeg', 7772, '//images.contentful.com/wp32f8j5jqi2/1NUq7WKxDCmOymCGQeGOE8/2dc1a09921f4e034d88c6560fc781a7b/285478865sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (720, 'es-ES', N'busimage285478865sm', N'business image install horizontal washers', '285478865sm.jpg', 'image/jpeg', 7772, '//images.contentful.com/wp32f8j5jqi2/1NUq7WKxDCmOymCGQeGOE8/2dc1a09921f4e034d88c6560fc781a7b/285478865sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (720, 'ru-RU', N'busimage285478865sm', N'business image install horizontal washers', '285478865sm.jpg', 'image/jpeg', 7772, '//images.contentful.com/wp32f8j5jqi2/1NUq7WKxDCmOymCGQeGOE8/2dc1a09921f4e034d88c6560fc781a7b/285478865sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (721, 'en-US', N'busimage195625907sm', N'image for business action close air intake', '195625907sm.jpg', 'image/jpeg', 15702, '//images.contentful.com/wp32f8j5jqi2/3eTv5Ptx8kC4ICu8EYOEuq/cdfee294fe9ad8e87ded7d00a31c2d4a/195625907sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (721, 'es-ES', N'busimage195625907sm', N'image for business action close air intake', '195625907sm.jpg', 'image/jpeg', 15702, '//images.contentful.com/wp32f8j5jqi2/3eTv5Ptx8kC4ICu8EYOEuq/cdfee294fe9ad8e87ded7d00a31c2d4a/195625907sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (721, 'ru-RU', N'busimage195625907sm', N'image for business action close air intake', '195625907sm.jpg', 'image/jpeg', 15702, '//images.contentful.com/wp32f8j5jqi2/3eTv5Ptx8kC4ICu8EYOEuq/cdfee294fe9ad8e87ded7d00a31c2d4a/195625907sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (722, 'en-US', N'busimage311806709sm', N'image for business action circulationtempcontrols', '311806709sm.jpg', 'image/jpeg', 5726, '//images.contentful.com/wp32f8j5jqi2/HfaIU8twg8Mic6SQkG8gU/456ec78da671bb808eca9080bc322b68/311806709sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (722, 'es-ES', N'busimage311806709sm', N'image for business action circulationtempcontrols', '311806709sm.jpg', 'image/jpeg', 5726, '//images.contentful.com/wp32f8j5jqi2/HfaIU8twg8Mic6SQkG8gU/456ec78da671bb808eca9080bc322b68/311806709sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (722, 'ru-RU', N'busimage311806709sm', N'image for business action circulationtempcontrols', '311806709sm.jpg', 'image/jpeg', 5726, '//images.contentful.com/wp32f8j5jqi2/HfaIU8twg8Mic6SQkG8gU/456ec78da671bb808eca9080bc322b68/311806709sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (723, 'en-US', N'busimagevariablespd311322197sm', N'business image for action variable speed compressors', '311322197sm.jpg', 'image/jpeg', 44071, '//images.contentful.com/wp32f8j5jqi2/3kj0e2tNa0iMOeyGmUCeCI/4829788c1feab04862479ca8606b225f/311322197sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (723, 'es-ES', N'busimagevariablespd311322197sm', N'business image for action variable speed compressors', '311322197sm.jpg', 'image/jpeg', 44071, '//images.contentful.com/wp32f8j5jqi2/3kj0e2tNa0iMOeyGmUCeCI/4829788c1feab04862479ca8606b225f/311322197sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (723, 'ru-RU', N'busimagevariablespd311322197sm', N'business image for action variable speed compressors', '311322197sm.jpg', 'image/jpeg', 44071, '//images.contentful.com/wp32f8j5jqi2/3kj0e2tNa0iMOeyGmUCeCI/4829788c1feab04862479ca8606b225f/311322197sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (724, 'en-US', N'busimage396321337sm', N'business image for action upgrade standard fridge', '396321337sm.jpg', 'image/jpeg', 7697, '//images.contentful.com/wp32f8j5jqi2/4vtDyPmXjyi8IEyGmaogoy/3d29db2640ef930373436de0fe41bf1b/396321337sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (724, 'es-ES', N'busimage396321337sm', N'business image for action upgrade standard fridge', '396321337sm.jpg', 'image/jpeg', 7697, '//images.contentful.com/wp32f8j5jqi2/4vtDyPmXjyi8IEyGmaogoy/3d29db2640ef930373436de0fe41bf1b/396321337sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (724, 'ru-RU', N'busimage396321337sm', N'business image for action upgrade standard fridge', '396321337sm.jpg', 'image/jpeg', 7697, '//images.contentful.com/wp32f8j5jqi2/4vtDyPmXjyi8IEyGmaogoy/3d29db2640ef930373436de0fe41bf1b/396321337sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (725, 'en-US', N'busimage204535609sm', N'business image for action display case motors', '204535609sm.jpg', 'image/jpeg', 74453, '//images.contentful.com/wp32f8j5jqi2/4ZBNIc0uc8meekYaumSQyk/56990f12a13ecf7e62199c016406e36e/204535609sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (725, 'es-ES', N'busimage204535609sm', N'business image for action display case motors', '204535609sm.jpg', 'image/jpeg', 74453, '//images.contentful.com/wp32f8j5jqi2/4ZBNIc0uc8meekYaumSQyk/56990f12a13ecf7e62199c016406e36e/204535609sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (725, 'ru-RU', N'busimage204535609sm', N'business image for action display case motors', '204535609sm.jpg', 'image/jpeg', 74453, '//images.contentful.com/wp32f8j5jqi2/4ZBNIc0uc8meekYaumSQyk/56990f12a13ecf7e62199c016406e36e/204535609sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (726, 'en-US', N'busimage159202811sm', N'business image for action cover display cases', '159202811sm.jpg', 'image/jpeg', 20975, '//images.contentful.com/wp32f8j5jqi2/57RmIL7LCo4qqOKsM4mYWw/0e9e76f957b1700baa25b9978ad82721/159202811sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (726, 'es-ES', N'busimage159202811sm', N'business image for action cover display cases', '159202811sm.jpg', 'image/jpeg', 20975, '//images.contentful.com/wp32f8j5jqi2/57RmIL7LCo4qqOKsM4mYWw/0e9e76f957b1700baa25b9978ad82721/159202811sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (726, 'ru-RU', N'busimage159202811sm', N'business image for action cover display cases', '159202811sm.jpg', 'image/jpeg', 20975, '//images.contentful.com/wp32f8j5jqi2/57RmIL7LCo4qqOKsM4mYWw/0e9e76f957b1700baa25b9978ad82721/159202811sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (727, 'en-US', N'busimage134708201sm', N'business image for updatecommercialfridge', '134708201sm.jpg', 'image/jpeg', 16759, '//images.contentful.com/wp32f8j5jqi2/6rGEk2mzOoGqWeu6MeeScU/74b8b4317c6c096cfc3050a20fc41fb6/134708201sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (727, 'es-ES', N'busimage134708201sm', N'business image for updatecommercialfridge', '134708201sm.jpg', 'image/jpeg', 16759, '//images.contentful.com/wp32f8j5jqi2/6rGEk2mzOoGqWeu6MeeScU/74b8b4317c6c096cfc3050a20fc41fb6/134708201sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (727, 'ru-RU', N'busimage134708201sm', N'business image for updatecommercialfridge', '134708201sm.jpg', 'image/jpeg', 16759, '//images.contentful.com/wp32f8j5jqi2/6rGEk2mzOoGqWeu6MeeScU/74b8b4317c6c096cfc3050a20fc41fb6/134708201sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (728, 'en-US', N'busimage173248397sm', N'business image for airinfiltrationbarriers action', '173248397sm.jpg', 'image/jpeg', 9857, '//images.contentful.com/wp32f8j5jqi2/4kSWUGTDvyakGuUM28yoaW/9d6505e16daf996f15d1ae6f32be65e4/173248397sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (728, 'es-ES', N'busimage173248397sm', N'business image for airinfiltrationbarriers action', '173248397sm.jpg', 'image/jpeg', 9857, '//images.contentful.com/wp32f8j5jqi2/4kSWUGTDvyakGuUM28yoaW/9d6505e16daf996f15d1ae6f32be65e4/173248397sm.jpg')
INSERT INTO [cm].[ClientAssetLocaleContent] ([ClientAssetID], [LocaleKey], [Title], [Description], [FileName], [FileContentType], [FileSize], [FileUrl]) VALUES (728, 'ru-RU', N'busimage173248397sm', N'business image for airinfiltrationbarriers action', '173248397sm.jpg', 'image/jpeg', 9857, '//images.contentful.com/wp32f8j5jqi2/4kSWUGTDvyakGuUM28yoaW/9d6505e16daf996f15d1ae6f32be65e4/173248397sm.jpg')
PRINT(N'Operation applied to 87 rows out of 87')

PRINT(N'Add rows to [cm].[ClientProfileSectionAttribute]')
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (125, 'heatsystem.fuel', 1)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (125, 'heatsystem.style', 2)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (125, 'secondaryheating.count', 4)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (127, 'waterheater.fuel', 3)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (142, 'centralac.count', 5)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (142, 'house.people', 4)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (142, 'house.style', 1)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (142, 'house.totalarearange', 3)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (142, 'house.yearbuiltrange', 2)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (143, 'cooktop.count', 2)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (143, 'dryer.count', 3)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (143, 'freezer.count', 9)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (143, 'hottub.count', 6)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (143, 'oven.count', 1)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (143, 'pool.count', 4)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (143, 'pool.poolheater', 5)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (143, 'refrigerator.count', 7)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (143, 'refrigerator2.count', 8)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (144, 'dryer.fuel', 1)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (144, 'dryer.loadswkly', 3)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (144, 'dryer.year', 2)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (145, 'pool.fuel', 1)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (145, 'pool.heatingfrqcy', 2)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (145, 'pool.poolcoverusage', 3)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (149, 'cooktop.fuel', 1)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (150, 'hottub.coverusage', 3)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (150, 'hottub.fuel', 1)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (150, 'hottub.monthsheated', 4)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (150, 'hottub.size', 2)
INSERT INTO [cm].[ClientProfileSectionAttribute] ([ClientProfileSectionID], [ProfileAttributeKey], [DisplayOrder]) VALUES (151, 'oven.fuel', 1)
PRINT(N'Operation applied to 30 rows out of 30')

PRINT(N'Add rows to [cm].[ClientProfileSectionCondition]')
INSERT INTO [cm].[ClientProfileSectionCondition] ([ClientProfileSectionID], [ConditionKey], [DisplayOrder]) VALUES (144, 'dryer.yes', 1)
INSERT INTO [cm].[ClientProfileSectionCondition] ([ClientProfileSectionID], [ConditionKey], [DisplayOrder]) VALUES (145, 'pool.heated', 2)
INSERT INTO [cm].[ClientProfileSectionCondition] ([ClientProfileSectionID], [ConditionKey], [DisplayOrder]) VALUES (145, 'pool.yes', 1)
INSERT INTO [cm].[ClientProfileSectionCondition] ([ClientProfileSectionID], [ConditionKey], [DisplayOrder]) VALUES (149, 'cooktop.yes', 1)
INSERT INTO [cm].[ClientProfileSectionCondition] ([ClientProfileSectionID], [ConditionKey], [DisplayOrder]) VALUES (150, 'hottub.yes', 1)
INSERT INTO [cm].[ClientProfileSectionCondition] ([ClientProfileSectionID], [ConditionKey], [DisplayOrder]) VALUES (151, 'oven.yes', 1)
PRINT(N'Operation applied to 6 rows out of 6')

PRINT(N'Add rows to [cm].[ClientTextContentLocaleContent]')
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (6597, 'en-US', N'HCF', N'HCF', N'HCF', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (6597, 'es-ES', N'HCF', N'HCF', N'HCF', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (6597, 'ru-RU', N'ЛПУ', N'ЛПУ', N'ЛПУ', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (6598, 'en-US', N'MCF', N'MCF', N'MCF', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (6598, 'es-ES', N'MCF', N'MCF', N'MCF', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (6598, 'ru-RU', N'MCF', N'MCF', N'MCF', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (6599, 'en-US', N'Cover pots and pans whenever possible. This will reduce cooking energy use and can save on cooling costs by reducing the amount of heat added to your kitchen.', N'Cover pots and pans whenever possible. This will reduce cooking energy use and can save on cooling costs by reducing the amount of heat added to your kitchen.', N'Cover pots and pans whenever possible. This will reduce cooking energy use and can save on cooling costs by reducing the amount of heat added to your kitchen.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (6599, 'es-ES', N'Tapa de ollas y sartenes siempre que sea posible. Esto reducirá la cocina uso de energía y puede ahorrar en costos de refrigeración al reducir la cantidad de calor añadido a su cocina.', N'Tapa de ollas y sartenes siempre que sea posible. Esto reducirá la cocina uso de energía y puede ahorrar en costos de refrigeración al reducir la cantidad de calor añadido a su cocina.', N'Tapa de ollas y sartenes siempre que sea posible. Esto reducirá la cocina uso de energía y puede ahorrar en costos de refrigeración al reducir la cantidad de calor añadido a su cocina.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (6599, 'ru-RU', N'Обложка, кастрюли и сковородки, когда это возможно. Это позволит уменьшить приготовления использования энергии и может сэкономить на охлаждение затрат путем уменьшения количества тепла, добавлены к вашей кухне.', N'Обложка, кастрюли и сковородки, когда это возможно. Это позволит уменьшить приготовления использования энергии и может сэкономить на охлаждение затрат путем уменьшения количества тепла, добавлены к вашей кухне.', N'Обложка, кастрюли и сковородки, когда это возможно. Это позволит уменьшить приготовления использования энергии и может сэкономить на охлаждение затрат путем уменьшения количества тепла, добавлены к вашей кухне.', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (6600, 'en-US', N'Cover It Up', N'Cover It Up', N'Cover It Up', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (6600, 'es-ES', N'Cubierta para arriba', N'Cubierta para arriba', N'Cubierta para arriba', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (6600, 'ru-RU', N'Накройте его', N'Накройте его', N'Накройте его', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (6601, 'en-US', N'Cooktop', N'Cooktop', N'Cooktop', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (6601, 'es-ES', N'Superficie de cocción', N'Superficie de cocción', N'Superficie de cocción', 0)
INSERT INTO [cm].[ClientTextContentLocaleContent] ([ClientTextContentID], [LocaleKey], [ShortText], [MediumText], [LongText], [RequireVariableSubstitution]) VALUES (6601, 'ru-RU', N'Кухонная плита', N'Кухонная плита', N'Кухонная плита', 0)
PRINT(N'Operation applied to 15 rows out of 15')

PRINT(N'Add constraints to [cm].[ClientTextContentLocaleContent]')
ALTER TABLE [cm].[ClientTextContentLocaleContent] ADD CONSTRAINT [FK_ClientTextContentLocaleContent_ClientTextContent] FOREIGN KEY ([ClientTextContentID]) REFERENCES [cm].[ClientTextContent] ([ClientTextContentID])
ALTER TABLE [cm].[ClientTextContentLocaleContent] ADD CONSTRAINT [FK_ClientTextContentLocaleContent_TypeLocale] FOREIGN KEY ([LocaleKey]) REFERENCES [cm].[TypeLocale] ([LocaleKey])

PRINT(N'Add constraints to [cm].[ClientProfileSectionCondition]')
ALTER TABLE [cm].[ClientProfileSectionCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSectionCondition_ClientProfileSection] FOREIGN KEY ([ClientProfileSectionID]) REFERENCES [cm].[ClientProfileSection] ([ClientProfileSectionID])
ALTER TABLE [cm].[ClientProfileSectionCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSectionCondition_TypeCondition] FOREIGN KEY ([ConditionKey]) REFERENCES [cm].[TypeCondition] ([ConditionKey])

PRINT(N'Add constraints to [cm].[ClientProfileSectionAttribute]')
ALTER TABLE [cm].[ClientProfileSectionAttribute] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSectionAttribute_ClientProfileSection] FOREIGN KEY ([ClientProfileSectionID]) REFERENCES [cm].[ClientProfileSection] ([ClientProfileSectionID])
ALTER TABLE [cm].[ClientProfileSectionAttribute] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSectionAttribute_TypeProfileAttribute] FOREIGN KEY ([ProfileAttributeKey]) REFERENCES [cm].[TypeProfileAttribute] ([ProfileAttributeKey])

PRINT(N'Add constraints to [cm].[ClientAssetLocaleContent]')
ALTER TABLE [cm].[ClientAssetLocaleContent] ADD CONSTRAINT [FK_ClientAssetLocaleContent_ClientAsset] FOREIGN KEY ([ClientAssetID]) REFERENCES [cm].[ClientAsset] ([ClientAssetID])
ALTER TABLE [cm].[ClientAssetLocaleContent] ADD CONSTRAINT [FK_ClientAssetLocaleContent_TypeLocale] FOREIGN KEY ([LocaleKey]) REFERENCES [cm].[TypeLocale] ([LocaleKey])

PRINT(N'Add constraints to [cm].[ClientUOM]')
ALTER TABLE [cm].[ClientUOM] WITH NOCHECK ADD CONSTRAINT [FK_ClientUOM_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientUOM] WITH NOCHECK ADD CONSTRAINT [FK_ClientUOM_ClientUOM1] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientUOM] ADD CONSTRAINT [FK_ClientUOM_ClientUOM] FOREIGN KEY ([UomKey]) REFERENCES [cm].[TypeUom] ([UomKey])
ALTER TABLE [cm].[ClientUOM] ADD CONSTRAINT [FK_ClientUOM_TypeUom] FOREIGN KEY ([UomKey]) REFERENCES [cm].[TypeUom] ([UomKey])

PRINT(N'Add constraints to [cm].[ClientTextContent]')
ALTER TABLE [cm].[ClientTextContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientTextContent_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
ALTER TABLE [cm].[ClientTextContent] ADD CONSTRAINT [FK_ClientTextContent_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientTextContent] ADD CONSTRAINT [FK_ClientTextContent_TypeTextContent] FOREIGN KEY ([TextContentKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraints to [cm].[ClientTabProfileSection]')
ALTER TABLE [cm].[ClientTabProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabProfileSection_ClientTab] FOREIGN KEY ([ClientTabID]) REFERENCES [cm].[ClientTab] ([ClientTabID])
ALTER TABLE [cm].[ClientTabProfileSection] ADD CONSTRAINT [FK_ClientTabProfileSection_TypeProfileSection] FOREIGN KEY ([ProfileSectionKey]) REFERENCES [cm].[TypeProfileSection] ([ProfileSectionKey])

PRINT(N'Add constraints to [cm].[ClientProfileSection]')
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSection_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent] FOREIGN KEY ([DescriptionKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent3] FOREIGN KEY ([IntroTextKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientProfileSection] ADD CONSTRAINT [FK_ClientProfileSection_TypeProfileSection] FOREIGN KEY ([ProfileSectionKey]) REFERENCES [cm].[TypeProfileSection] ([ProfileSectionKey])
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent2] FOREIGN KEY ([SubTitleKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientProfileSection] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileSection_TypeTextContent1] FOREIGN KEY ([TitleKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraints to [cm].[ClientProfileOption]')
ALTER TABLE [cm].[ClientProfileOption] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileOption_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientProfileOption] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileOption_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])
ALTER TABLE [cm].[ClientProfileOption] ADD CONSTRAINT [FK_ClientProfileOption_TypeProfileOption] FOREIGN KEY ([ProfileOptionKey]) REFERENCES [cm].[TypeProfileOption] ([ProfileOptionKey])

PRINT(N'Add constraints to [cm].[ClientProfileAttributeProfileOption]')
ALTER TABLE [cm].[ClientProfileAttributeProfileOption] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileAttributeProfileOption_ClientProfileAttribute] FOREIGN KEY ([ClientProfileAttributeID]) REFERENCES [cm].[ClientProfileAttribute] ([ClientProfileAttributeID])
ALTER TABLE [cm].[ClientProfileAttributeProfileOption] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileAttributeProfileOption_TypeProfileOption] FOREIGN KEY ([ProfileOptionKey]) REFERENCES [cm].[TypeProfileOption] ([ProfileOptionKey])

PRINT(N'Add constraints to [cm].[ClientFileContent]')
ALTER TABLE [cm].[ClientFileContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientFileContent_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
ALTER TABLE [cm].[ClientFileContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientFileContent_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientFileContent] ADD CONSTRAINT [FK_ClientFileContent_TypeFileContent] FOREIGN KEY ([FileContentKey]) REFERENCES [cm].[TypeFileContent] ([FileContentKey])
ALTER TABLE [cm].[ClientFileContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientFileContentLargeFile_TypeAsset] FOREIGN KEY ([LargeFile]) REFERENCES [cm].[TypeAsset] ([AssetKey])
ALTER TABLE [cm].[ClientFileContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientFileContentMediumFile_TypeAsset] FOREIGN KEY ([MediumFile]) REFERENCES [cm].[TypeAsset] ([AssetKey])
ALTER TABLE [cm].[ClientFileContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientFileContentSmallFile_TypeAsset] FOREIGN KEY ([SmallFile]) REFERENCES [cm].[TypeAsset] ([AssetKey])

PRINT(N'Add constraints to [cm].[ClientEnumerationItem]')
ALTER TABLE [cm].[ClientEnumerationItem] WITH NOCHECK ADD CONSTRAINT [FK_ClientEnumerationItem_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
ALTER TABLE [cm].[ClientEnumerationItem] ADD CONSTRAINT [FK_ClientEnumerationItem_TypeEnumerationItem] FOREIGN KEY ([EnumerationItemKey]) REFERENCES [cm].[TypeEnumerationItem] ([EnumerationItemKey])

PRINT(N'Add constraints to [cm].[ClientEnumerationEnumerationItem]')
ALTER TABLE [cm].[ClientEnumerationEnumerationItem] ADD CONSTRAINT [FK_ClientEnumerationEnumerationItem_ClientEnumeration] FOREIGN KEY ([ClientEnumerationID]) REFERENCES [cm].[ClientEnumeration] ([ClientEnumerationID])
ALTER TABLE [cm].[ClientEnumerationEnumerationItem] ADD CONSTRAINT [FK_ClientEnumerationEnumerationItem_TypeEnumerationItem] FOREIGN KEY ([EnumerationItemKey]) REFERENCES [cm].[TypeEnumerationItem] ([EnumerationItemKey])

PRINT(N'Add constraints to [cm].[ClientAsset]')
ALTER TABLE [cm].[ClientAsset] ADD CONSTRAINT [FK_ClientAsset_TypeAsset] FOREIGN KEY ([AssetKey]) REFERENCES [cm].[TypeAsset] ([AssetKey])
ALTER TABLE [cm].[ClientAsset] WITH NOCHECK ADD CONSTRAINT [FK_ClientAsset_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])

PRINT(N'Add constraints to [cm].[ClientActionCondition]')
ALTER TABLE [cm].[ClientActionCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientActionCondition_ClientAction] FOREIGN KEY ([ClientActionID]) REFERENCES [cm].[ClientAction] ([ClientActionID])
ALTER TABLE [cm].[ClientActionCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientActionCondition_TypeCondition] FOREIGN KEY ([ConditionKey]) REFERENCES [cm].[TypeCondition] ([ConditionKey])

PRINT(N'Add constraint FK_ClientAction_TypeUom to [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientAction_TypeUom] FOREIGN KEY ([AnnualUsageSavingsUomKey]) REFERENCES [cm].[TypeUom] ([UomKey])

PRINT(N'Add constraint FK_ClientAction_TypeUom1 to [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientAction_TypeUom1] FOREIGN KEY ([AnnualUsageSavingsUomKey]) REFERENCES [cm].[TypeUom] ([UomKey])

PRINT(N'Add constraint FK_ClientAction_TypeTextContent to [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientAction_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientAction_TypeTextContent1 to [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientAction_TypeTextContent1] FOREIGN KEY ([NextStepLinkText]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientAppliance_TypeTextContent to [cm].[ClientAppliance]')
ALTER TABLE [cm].[ClientAppliance] WITH NOCHECK ADD CONSTRAINT [FK_ClientAppliance_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientCommodity_TypeTextContent to [cm].[ClientCommodity]')
ALTER TABLE [cm].[ClientCommodity] WITH NOCHECK ADD CONSTRAINT [FK_ClientCommodity_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientCurrency_TypeTextContent to [cm].[ClientCurrency]')
ALTER TABLE [cm].[ClientCurrency] WITH NOCHECK ADD CONSTRAINT [FK_ClientCurrency_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientCurrency_TypeTextContent1 to [cm].[ClientCurrency]')
ALTER TABLE [cm].[ClientCurrency] WITH NOCHECK ADD CONSTRAINT [FK_ClientCurrency_TypeTextContent1] FOREIGN KEY ([SymbolKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientEnduse_TypeTextContent to [cm].[ClientEnduse]')
ALTER TABLE [cm].[ClientEnduse] WITH NOCHECK ADD CONSTRAINT [FK_ClientEnduse_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientMeasurement_TypeTextContent to [cm].[ClientMeasurement]')
ALTER TABLE [cm].[ClientMeasurement] WITH NOCHECK ADD CONSTRAINT [FK_ClientMeasurement_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientProfileAttribute_TypeTextContent to [cm].[ClientProfileAttribute]')
ALTER TABLE [cm].[ClientProfileAttribute] WITH NOCHECK ADD CONSTRAINT [FK_ClientProfileAttribute_TypeTextContent] FOREIGN KEY ([QuestionTextKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientSeason_TypeTextContent to [cm].[ClientSeason]')
ALTER TABLE [cm].[ClientSeason] WITH NOCHECK ADD CONSTRAINT [FK_ClientSeason_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientTab_TypeTextContent to [cm].[ClientTab]')
ALTER TABLE [cm].[ClientTab] WITH NOCHECK ADD CONSTRAINT [FK_ClientTab_TypeTextContent] FOREIGN KEY ([NameKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientTabTextContentality_TypeTextContentality to [cm].[ClientTabTextContent]')
ALTER TABLE [cm].[ClientTabTextContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientTabTextContentality_TypeTextContentality] FOREIGN KEY ([TextContentKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientWidget_TypeTextContent to [cm].[ClientWidget]')
ALTER TABLE [cm].[ClientWidget] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidget_TypeTextContent] FOREIGN KEY ([IntroTextKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientWidgetTextContentality_TypeTextContentality to [cm].[ClientWidgetTextContent]')
ALTER TABLE [cm].[ClientWidgetTextContent] WITH NOCHECK ADD CONSTRAINT [FK_ClientWidgetTextContentality_TypeTextContentality] FOREIGN KEY ([TextContentKey]) REFERENCES [cm].[TypeTextContent] ([TextContentKey])

PRINT(N'Add constraint FK_ClientAction_ClientFileContent2 to [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientAction_ClientFileContent2] FOREIGN KEY ([ImageKey]) REFERENCES [cm].[TypeFileContent] ([FileContentKey])

PRINT(N'Add constraint FK_ClientAction_ClientFileContent to [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientAction_ClientFileContent] FOREIGN KEY ([RebateImageKey]) REFERENCES [cm].[TypeFileContent] ([FileContentKey])

PRINT(N'Add constraint FK_ClientAction_ClientFileContent3 to [cm].[ClientAction]')
ALTER TABLE [cm].[ClientAction] WITH NOCHECK ADD CONSTRAINT [FK_ClientAction_ClientFileContent3] FOREIGN KEY ([VideoKey]) REFERENCES [cm].[TypeFileContent] ([FileContentKey])

PRINT(N'Add constraint FK_ClientCondition_TypeCategory to [cm].[ClientCondition]')
ALTER TABLE [cm].[ClientCondition] WITH NOCHECK ADD CONSTRAINT [FK_ClientCondition_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])

PRINT(N'Add constraint FK_ClientConfiguration_TypeCategory to [cm].[ClientConfiguration]')
ALTER TABLE [cm].[ClientConfiguration] WITH NOCHECK ADD CONSTRAINT [FK_ClientConfiguration_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])

PRINT(N'Add constraint FK_ClientConfigurationBulk_TypeCategory to [cm].[ClientConfigurationBulk]')
ALTER TABLE [cm].[ClientConfigurationBulk] WITH NOCHECK ADD CONSTRAINT [FK_ClientConfigurationBulk_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])

PRINT(N'Add constraint FK_TypeEnumeration_TypeCategory to [cm].[TypeEnumeration]')
ALTER TABLE [cm].[TypeEnumeration] WITH NOCHECK ADD CONSTRAINT [FK_TypeEnumeration_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
COMMIT TRANSACTION
GO
