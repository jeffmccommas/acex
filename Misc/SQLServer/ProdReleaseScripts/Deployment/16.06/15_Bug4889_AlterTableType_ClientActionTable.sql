/*
Run this script on:
        aceusql1c0.database.windows.net.InsightsMetaData   
*/
USE [InsightsMetaData]
GO

/****** Object:  StoredProcedure [cm].[uspUpdateClientAction]    Script Date: 6/22/2016 9:29:04 AM ******/
DROP PROCEDURE [cm].[uspUpdateClientAction]
GO

/****** Object:  UserDefinedTableType [cm].[ClientActionTable]    Script Date: 6/22/2016 9:27:42 AM ******/
DROP TYPE [cm].[ClientActionTable]
GO

/****** Object:  UserDefinedTableType [cm].[ClientActionTable]    Script Date: 6/22/2016 9:27:42 AM ******/
CREATE TYPE [cm].[ClientActionTable] AS TABLE(
	[ClientID] [INT] NOT NULL,
	[ActionKey] [VARCHAR](256) NOT NULL,
	[ActionTypeKey] [VARCHAR](256) NOT NULL,
	[Hide] [BIT] NOT NULL,
	[Disable] [BIT] NOT NULL,
	[CommodityKey] [VARCHAR](256) NOT NULL,
	[DifficultyKey] [VARCHAR](256) NOT NULL,
	[HabitIntervalKey] [VARCHAR](256) NOT NULL,
	[NameKey] [VARCHAR](256) NOT NULL,
	[DescriptionKey] [VARCHAR](256) NOT NULL,
	[AnnualCost] [DECIMAL](18, 2) NOT NULL,
	[UpfrontCost] [DECIMAL](18, 2) NOT NULL,
	[AnnualSavingsEstimate] [DECIMAL](18, 2) NOT NULL,
	[AnnualUsageSavingsEstimate] [DECIMAL](18, 2) NULL,
	[AnnualUsageSavingsUomKey] [VARCHAR](50) NULL,
	[CostVariancePercent] [DECIMAL](18, 2) NOT NULL,
	[PaybackTime] [INT] NOT NULL,
	[ROI] [DECIMAL](18, 2) NOT NULL,
	[RebateAvailable] [BIT] NOT NULL,
	[RebateAmount] [DECIMAL](18, 2) NULL,
	[RebateUrl] [VARCHAR](1000) NULL,
	[RebateImageKey] [VARCHAR](256) NULL,
	[IconClass] [VARCHAR](256) NULL,
	[ImageKey] [VARCHAR](256) NULL,
	[VideoKey] [VARCHAR](256) NULL,
	[Comments] [VARCHAR](MAX) NULL,
	[ConditionKeys] [VARCHAR](4000) NULL,
	[ApplianceKeys] [VARCHAR](4000) NULL,
	[SeasonKeys] [VARCHAR](4000) NULL,
	[ActionPriority] [INT] NULL,
	[SavingsCalcMethod] [VARCHAR](50) NOT NULL,
	[SavingsAmount] [DECIMAL](18, 2) NULL,
	[WhatIfDataKeys] [VARCHAR](4000) NULL,
	[CostExpression] [VARCHAR](256) NULL,
	[RebateExpression] [VARCHAR](256) NULL,
	[NextStepLink] [VARCHAR](1000) NULL,
	[NextStepLinkText] [VARCHAR](256) NULL,
	[NextStepLinkType] [VARCHAR](256) NULL,
	[Tags] [VARCHAR](256) NULL,
	[MinimumSavingsAmt] [DECIMAL](18, 2) NULL,
	[MaximumSavingsAmt] [DECIMAL](18, 2) NULL,
	PRIMARY KEY CLUSTERED 
(
	[ClientID] ASC,
	[ActionKey] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO


