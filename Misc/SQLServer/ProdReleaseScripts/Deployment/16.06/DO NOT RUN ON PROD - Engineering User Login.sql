-- Execute this script in the master database
IF NOT EXISTS 
    (SELECT name  
     FROM master.sys.sql_logins
     WHERE name = 'Engineering')
BEGIN
    CREATE LOGIN Engineering WITH PASSWORD = 'Acl@r@371'
END

IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'EngineeringUser')
BEGIN
    CREATE USER EngineeringUser FOR LOGIN Engineering
END


-- Execute this script in all the shards except the master database
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'EngineeringUser')
BEGIN
    CREATE USER EngineeringUser FOR LOGIN Engineering
END


EXEC sp_addrolemember 'db_owner', 'EngineeringUser'
GO
