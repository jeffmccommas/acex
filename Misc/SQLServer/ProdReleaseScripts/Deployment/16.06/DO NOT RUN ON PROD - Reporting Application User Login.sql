-- Execute this script in the master database
IF NOT EXISTS 
    (SELECT name  
     FROM master.sys.sql_logins
     WHERE name = 'ReportingApplication')
BEGIN
    CREATE LOGIN ReportingApplication WITH PASSWORD = 'Acl@r@889'
IF

END NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'ReportingApplicationUser')
BEGIN
    CREATE USER ReportingApplicationUser FOR LOGIN ReportingApplication
END


-- Execute this script in all the shards except the master database
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'ReportingApplicationUser')
BEGIN
    CREATE USER ReportingApplicationUser FOR LOGIN ReportingApplication
END


EXEC sp_addrolemember 'db_datareader', 'ReportingApplicationUser'
GO


GRANT EXECUTE ON [rpt].[getImpactAnalysisMetrics] TO ReportingApplicationUser

GRANT EXECUTE ON [rpt].[GetProgramNetReportDeliveredDetail] TO ReportingApplicationUser

GRANT EXECUTE ON [rpt].[getUtilityPortalUsers] TO ReportingApplicationUser

GRANT EXECUTE ON [rpt].[ClientActionDetails] TO ReportingApplicationUser