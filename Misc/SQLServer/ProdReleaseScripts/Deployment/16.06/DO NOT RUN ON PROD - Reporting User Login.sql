-- Execute this script in the master database
IF NOT EXISTS 
    (SELECT name  
     FROM master.sys.sql_logins
     WHERE name = 'Reporting')
BEGIN
    CREATE LOGIN Reporting WITH PASSWORD = 'Acl@r@223'
END

IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'ReportingUser')
BEGIN
    CREATE USER ReportingUser FOR LOGIN Reporting
END


-- Execute this script in all the shards except the master database
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'ReportingUser')
BEGIN
    CREATE USER ReportingUser FOR LOGIN Reporting
END


EXEC sp_addrolemember 'db_owner', 'ReportingUser'
GO
