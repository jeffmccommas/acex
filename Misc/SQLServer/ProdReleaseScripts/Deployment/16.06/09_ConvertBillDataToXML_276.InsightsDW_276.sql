/*
Run this script on:
        aceusql1c0.database.windows.net.InsightsDW_276
        acepsql1c0.database.windows.net.InsightsDW_276
*/
/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_276]    Script Date: 6/21/2016 8:54:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:      Ubaid
-- Create date: 06/04/2015
-- Description: This will create customer, billing and profile xml
--              for input  into bulk import
-- =============================================
ALTER PROCEDURE [dbo].[ConvertBillDataToXML_276] @ClientId AS INT
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @XmlConfiguration XML;

        EXEC [dbo].[GetComponentXmlConfiguration] @ClientId = @ClientId,
            @ComponentName = N'preprocess.programs.zipcodemapping',
            @XmlConfiguration = @XmlConfiguration OUTPUT;

        SELECT  x.r.value('@key[1]', 'varchar(100)') AS Zipcode ,
                x.r.value('text()[1]', 'varchar(100)') AS Company
        INTO    #ZipcodeMapping
        FROM    @XmlConfiguration.nodes('/ZipcodeMapping/MapInfo') AS x ( r );

        WITH XMLNAMESPACES (DEFAULT 'Aclara:Insights')


        SELECT  ( SELECT    customer_id AS '@CustomerId' ,
                    first_name AS '@FirstName' ,
                    last_name AS '@LastName' ,
                    RTRIM(LTRIM([service_house_number] + ' ' + [service_street_name])) AS '@Street1' ,
                    service_city AS '@City' ,
                    CAST(LTRIM(RTRIM(service_state)) AS VARCHAR(2)) AS '@State' ,
                    'US' AS '@Country' ,
                    CASE WHEN LEN(LTRIM(RTRIM(service_zip_code))) = 5
                         THEN CAST(SUBSTRING(service_zip_code, 1, 5) AS VARCHAR(5))
                         WHEN LEN(LTRIM(RTRIM(service_zip_code))) = 10
                         THEN CAST(( SUBSTRING(service_zip_code, 1, 5)
                                     + SUBSTRING(service_zip_code, 7, 4) ) AS VARCHAR(9))
                         ELSE NULL
                    END AS '@PostalCode' ,
                    'utility' AS '@Source' ,
                    CASE WHEN   customer_type = 'Commercial' THEN 'true'
                        ELSE 'false'
                    END AS '@IsBusiness' ,
                    'true' AS '@IsAuthenticated' ,
                    account_id AS 'Account/@AccountId' ,
                    premise_id AS 'Account/Premise/@PremiseId' ,
                    RTRIM(LTRIM([service_house_number] + ' ' + [service_street_name])) AS 'Account/Premise/@PremiseStreet1' ,
                    service_city AS 'Account/Premise/@PremiseCity' ,
                    CAST(LTRIM(RTRIM(service_state)) AS VARCHAR(2)) AS 'Account/Premise/@PremiseState' ,
                    CASE WHEN LEN(LTRIM(RTRIM(service_zip_code))) = 5
                         THEN CAST(SUBSTRING(service_zip_code, 1, 5) AS VARCHAR(5))
                         WHEN LEN(LTRIM(RTRIM(service_zip_code))) = 10
                         THEN CAST(( SUBSTRING(service_zip_code, 1, 5)
                                     + SUBSTRING(service_zip_code, 7, 4) ) AS VARCHAR(9))
                         ELSE NULL
                    END AS 'Account/Premise/@PremisePostalCode' ,
                    'US' AS 'Account/Premise/@PremiseCountry' ,
                    'false' AS 'Account/Premise/@HasElectricService' ,
                    'true' AS 'Account/Premise/@HasGasService' ,
                    'false' AS 'Account/Premise/@HasWaterService'
          FROM      dbo.RawBillTemp_276
          WHERE ClientId = @ClientId
        FOR
          XML PATH('Customer') ,
              ELEMENTS ,
              ROOT('Customers')
        ) AS XmlData ,
        'Customer' AS XmlType
UNION
SELECT  ( SELECT    customer_id AS '@CustomerId' ,
                    'utility' AS '@Source' ,
                    account_id AS 'Account/@AccountId' ,
                    billperiod_type AS 'Account/Bill/@BillPeriodType' ,
                    read_cycle AS 'Account/Bill/@BillCycleScheduleId' ,
                    CONVERT(VARCHAR, DATEADD(d,
                                             ( CAST(bill_days AS INT) * -1 ),
                                             CAST(bill_enddate AS DATETIME)), 126) AS 'Account/Bill/@StartDate' ,
                    CONVERT(VARCHAR, CAST(bill_enddate AS DATETIME), 126) AS 'Account/Bill/@EndDate' ,
                    premise_id AS 'Account/Bill/Premise/@PremiseId' ,
                    CASE WHEN ISNUMERIC(usage_value) = 1
                         THEN CAST(usage_value AS DECIMAL(18, 2))
                         ELSE 0
                    END AS 'Account/Bill/Premise/Service/@TotalUsage' ,
                    CASE WHEN ISNUMERIC(usage_charge) = 1
                         THEN CONVERT(DECIMAL(18,2), CAST(usage_charge AS MONEY))
                         ELSE 0
                    END AS 'Account/Bill/Premise/Service/@TotalCost' ,
                    service_commodity AS 'Account/Bill/Premise/Service/@Commodity' ,
                    CASE WHEN meter_units = '1' THEN '3'
                         WHEN meter_units = '2' THEN '2'
                         WHEN meter_units = '3' THEN '1'
                         WHEN meter_units = '5' THEN '6'
                         WHEN meter_units = '7' THEN '7'
                         WHEN meter_units = '8' THEN '8'
                         ELSE '-1'
                    END AS 'Account/Bill/Premise/Service/@UOM' ,
                    service_point_id AS 'Account/Bill/Premise/Service/@ServicePointId' ,
                    'SC_'+service_point_id+'_'+service_commodity AS 'Account/Bill/Premise/Service/@ServiceContractId' ,
                    IIF(LEN(service_read_date)=0,NULL,CONVERT(VARCHAR, CONVERT(DATETIME, SUBSTRING(service_read_date, 1, 8) + ' ' + SUBSTRING(service_read_date, 10, 5)), 126)) AS 'Account/Bill/Premise/Service/@ReadDate' ,
                    rate_code AS 'Account/Bill/Premise/Service/@RateClass'
          FROM      dbo.RawBillTemp_276
          WHERE ClientId = @ClientId AND bill_enddate != ''
        FOR
          XML PATH('Customer') ,
              ELEMENTS ,
              ROOT('Customers')
        ) AS XmlData ,
        'Bill' AS XmlType
        UNION
        SELECT  ( SELECT    customer_id AS '@CustomerId' ,
                    account_id AS 'Account/@AccountId' ,
                    premise_id AS 'Account/Premise/@PremiseId' ,
                    ( SELECT    'customer.segment' AS 'ProfileItem/@AttributeKey' ,
                                ISNULL(m.Company, 'LG') AS 'ProfileItem/@AttributeValue' ,
                                'utility' AS 'ProfileItem/@Source' ,
                                GETDATE() AS 'ProfileItem/@ModifiedDate'
                    FOR
                      XML PATH('') ,
                          TYPE ,
                          ELEMENTS
                    ) AS 'Account/Premise'
          FROM      [dbo].[RawBillTemp_276] t
                    LEFT JOIN #ZipcodeMapping m ON m.Zipcode = SUBSTRING(LTRIM(RTRIM(service_zip_code)),
                                                              1, 5)
          WHERE     ClientId = @ClientId
        FOR
          XML PATH('Customer') ,
              ELEMENTS ,
              ROOT('Customers')
        ) AS XmlData ,
        'Profile' AS XmlType;

    END;




