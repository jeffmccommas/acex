/*
Run this script on:
        aceusql1c0.database.windows.net.InsightsDW_210
        acepsql1c0.database.windows.net.InsightsDW_210
*/
/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_224]    Script Date: 6/21/2016 8:58:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:      Ubaid
-- Create date: 06/04/2015
-- Description: This will create customer, billing and profile xml
--              for input  into bulk import
-- =============================================
ALTER PROCEDURE [dbo].[ConvertBillDataToXML_224] @ClientId AS INT
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @ProgramMapping AS TABLE
            (
              ProgramName VARCHAR(255) ,
              PremiseAttributeKey VARCHAR(255)
            );

        INSERT  INTO @ProgramMapping
        VALUES  ( 'EpaymentSubscriber', 'paperlessbilling.enrollmentstatus' );
        INSERT  INTO @ProgramMapping
        VALUES  ( 'GreenpowerPurchaser', 'greenprogram.enrollmentstatus' );
        INSERT  INTO @ProgramMapping
        VALUES  ( 'BudgetBilling', 'budgetbilling.enrollmentstatus' );
        INSERT  INTO @ProgramMapping
        VALUES  ( 'LowIncome', 'lowincomeprogram.enrollmentstatus' );
        INSERT  INTO @ProgramMapping
        VALUES  ( 'MyAccount', 'utilityportal.enrollmentstatus' );
        INSERT  INTO @ProgramMapping
        VALUES  ( 'Photovoltaic', 'solarprogram.enrollmentstatus' );

        BEGIN TRY
            WITH XMLNAMESPACES (DEFAULT 'Aclara:Insights')


        SELECT  ( SELECT    customer_id AS '@CustomerId' ,
                    first_name AS '@FirstName' ,
                    last_name AS '@LastName' ,
                    mail_address_line_1 AS '@Street1' ,
                    mail_address_line_2 AS '@Street2' ,
                    mail_city AS '@City' ,
                    mail_state AS '@State' ,
                    'US' AS '@Country' ,
                    CASE WHEN LEN(LTRIM(RTRIM(mail_zip_code))) = 5
                         THEN CAST(SUBSTRING(mail_zip_code, 1, 5) AS VARCHAR(5))
                         ELSE CAST(( SUBSTRING(mail_zip_code, 1, 5)
                                     + SUBSTRING(mail_zip_code, 7, 4) ) AS VARCHAR(9))
                    END AS '@PostalCode' ,
                    NULLIF(REPLACE(REPLACE(REPLACE(phone_1, '(', ''), ')', ''),
                                   ' ', ''), '') AS '@PhoneNumber' ,
                    NULLIF(REPLACE(REPLACE(REPLACE(phone_2, '(', ''), ')', ''),
                                   ' ', ''), '') AS '@MobilePhoneNumber' ,
                    CASE WHEN LEN(email) < 6 THEN NULL
                         ELSE COALESCE(NULLIF(LTRIM(RTRIM(email)), ''), email)
                    END AS '@EmailAddress' ,
                    NULL AS '@AlternateEmailAddress' ,
                    'utility' AS '@Source' ,
                    CASE WHEN   customer_type = 'Commercial' THEN 'true'
                        ELSE 'false'
                    END AS '@IsBusiness' ,
                    'true' AS '@IsAuthenticated' ,
                    account_id AS 'Account/@AccountId' ,
                    premise_id AS 'Account/Premise/@PremiseId' ,
                    LTRIM(RTRIM(service_house_number)) + RTRIM(ISNULL(' '
                                                              + service_street_name,
                                                              ''))
                    + RTRIM(ISNULL(' ' + service_unit, '')) AS 'Account/Premise/@PremiseStreet1' ,
                    service_city AS 'Account/Premise/@PremiseCity' ,
                    CASE WHEN LEN(LTRIM(RTRIM(service_zip_code))) < 5
                         THEN NULL
                         WHEN LEN(LTRIM(RTRIM(service_zip_code))) = 5
                         THEN CAST(SUBSTRING(service_zip_code, 1, 5) AS VARCHAR(5))
                         WHEN LEN(LTRIM(RTRIM(service_zip_code))) = 10
                         THEN CAST(( SUBSTRING(service_zip_code, 1, 5)
                                     + SUBSTRING(service_zip_code, 7, 4) ) AS VARCHAR(9))
                         ELSE NULL
                    END AS 'Account/Premise/@PremisePostalCode' ,
                    CAST(LTRIM(RTRIM(service_state)) AS VARCHAR(2)) AS 'Account/Premise/@PremiseState' ,
                    'US' AS 'Account/Premise/@PremiseCountry' ,
                    'true' AS 'Account/Premise/@HasElectricService' ,
                    'true' AS 'Account/Premise/@HasGasService' ,
                    'true' AS 'Account/Premise/@HasWaterService'
          FROM      dbo.RawBillTemp_224
          WHERE ClientId = @ClientId
        FOR
          XML PATH('Customer') ,
              ELEMENTS ,
              ROOT('Customers')
        ) AS XmlData ,
        'Customer' AS XmlType
UNION
SELECT  ( SELECT    customer_id AS '@CustomerId' ,
                    'customer' AS '@Source' ,
                    account_id AS 'Account/@AccountId' ,
                    billperiod_type AS 'Account/Bill/@BillPeriodType' ,
                    read_cycle AS 'Account/Bill/@BillCycleScheduleId' ,
                    CONVERT(VARCHAR, CAST(service_read_startdate AS DATETIME), 126) AS 'Account/Bill/@StartDate' ,
                    CONVERT(VARCHAR, CAST(service_read_date AS DATETIME), 126) AS 'Account/Bill/@EndDate' ,
                    premise_id AS 'Account/Bill/Premise/@PremiseId' ,
                    CASE WHEN ISNUMERIC(usage_value) = 1
                         THEN CAST(usage_value AS DECIMAL(18, 2))
                         ELSE 0
                    END AS 'Account/Bill/Premise/Service/@TotalUsage' ,
                    CASE WHEN ISNUMERIC(usage_charge) = 1
                         THEN CAST(usage_charge AS DECIMAL(18, 2))
                         ELSE 0
                    END AS 'Account/Bill/Premise/Service/@TotalCost' ,
                    service_commodity AS 'Account/Bill/Premise/Service/@Commodity' ,
                    CASE WHEN meter_units = '1' THEN '3'
                         WHEN meter_units = '2' THEN '2'
                         WHEN meter_units = '3' THEN '1'
                         WHEN meter_units = '6' THEN '4'
                         WHEN meter_units = '4' THEN '5'
                         WHEN meter_units = '5' THEN '6'
                         WHEN meter_units = '7' THEN '7'
                         WHEN meter_units = '8' THEN '8'
                         ELSE '-1'
                    END AS 'Account/Bill/Premise/Service/@UOM' ,
                    IIF(LEN(active_date)=0,NULL,CONVERT(VARCHAR, CAST(active_date AS DATETIME), 126)) AS 'Account/Bill/Premise/Service/@AMIStartDate' ,
                    IIF(LEN(inactive_date)=0,NULL,CONVERT(VARCHAR, CAST(inactive_date AS DATETIME), 126)) AS 'Account/Bill/Premise/Service/@AMIEndDate' ,
                    IIF(LEN(service_read_date)=0,NULL,CONVERT(VARCHAR, CAST(service_read_date AS DATETIME), 126)) AS 'Account/Bill/Premise/Service/@ReadDate' ,
                    rate_code AS 'Account/Bill/Premise/Service/@RateClass' ,
                    meter_type AS 'Account/Bill/Premise/Service/@MeterType' ,
                    meter_id AS 'Account/Bill/Premise/Service/@MeterId' ,
                    meter_replaces_meterid AS 'Account/Bill/Premise/Service/@ReplacedMeterId' ,
                    CASE WHEN is_estimate = '0' THEN 'Actual'
                         WHEN is_estimate = '1' THEN 'Estimated'
                    END AS 'Account/Bill/Premise/Service/@ReadQuality'
          FROM      dbo.RawBillTemp_224
          WHERE ClientId = @ClientId
        FOR
          XML PATH('Customer') ,
              ELEMENTS ,
              ROOT('Customers')
        ) AS XmlData ,
        'Bill' AS XmlType
        UNION
        SELECT
        ( SELECT customer_id AS '@CustomerId'
                , customer_id AS 'Account/@AccountId'
                , premise_id AS 'Account/Premise/@PremiseId'
                ,IIF(Attribute1 IS NULL,NULL, ( SELECT Attribute1 AS 'ProfileItem/@AttributeKey', Attribute1 + '.enrolled' AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise'
                ,IIF(Attribute2 IS NULL,NULL, ( SELECT Attribute2 AS 'ProfileItem/@AttributeKey', Attribute2 + '.enrolled' AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise'
                ,IIF(Attribute3 IS NULL,NULL, ( SELECT Attribute3 AS 'ProfileItem/@AttributeKey', Attribute3 + '.enrolled' AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise'
                ,IIF(Attribute4 IS NULL,NULL, ( SELECT Attribute4 AS 'ProfileItem/@AttributeKey', Attribute4 + '.enrolled' AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise'
                ,IIF(Attribute5 IS NULL,NULL, ( SELECT Attribute5 AS 'ProfileItem/@AttributeKey', Attribute5 + '.enrolled' AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise'
                ,IIF(Attribute6 IS NULL,NULL, ( SELECT Attribute6 AS 'ProfileItem/@AttributeKey', Attribute6 + '.enrolled' AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise'
                FROM (SELECT  customer_id ,
                premise_id ,
                pm1.PremiseAttributeKey AS Attribute1 ,
                pm2.PremiseAttributeKey AS Attribute2 ,
                pm3.PremiseAttributeKey AS Attribute3 ,
                pm4.PremiseAttributeKey AS Attribute4 ,
                pm5.PremiseAttributeKey AS Attribute5 ,
                pm6.PremiseAttributeKey AS Attribute6
        FROM    [dbo].[RawBillTemp_224]
                CROSS APPLY ( SELECT    str = Programs + ',,,,,,'
                            ) f1
                CROSS APPLY ( SELECT    p1 = CHARINDEX(',', str)
                            ) ap1
                CROSS APPLY ( SELECT    p2 = CHARINDEX(',', str, p1 + 1)
                            ) ap2
                CROSS APPLY ( SELECT    p3 = CHARINDEX(',', str, p2 + 1)
                            ) ap3
                CROSS APPLY ( SELECT    p4 = CHARINDEX(',', str, p3 + 1)
                            ) ap4
                CROSS APPLY ( SELECT    p5 = CHARINDEX(',', str, p4 + 1)
                            ) ap5
                CROSS APPLY ( SELECT    p6 = CHARINDEX(',', str, p5 + 1)
                            ) ap6
                CROSS APPLY ( SELECT    Program1 = SUBSTRING(str, 1, p1 - 1) ,
                                        Program2 = SUBSTRING(str, p1 + 1, p2 - p1 - 1) ,
                                        Program3 = SUBSTRING(str, p2 + 1, p3 - p2 - 1) ,
                                        Program4 = SUBSTRING(str, p3 + 1, p4 - p3 - 1) ,
                                        Program5 = SUBSTRING(str, p4 + 1, p5 - p4 - 1) ,
                                        Program6 = SUBSTRING(str, p5 + 1, p6 - p5 - 1)
                            ) Programs
                LEFT JOIN @ProgramMapping pm1 ON pm1.ProgramName = Programs.Program1
                LEFT JOIN @ProgramMapping pm2 ON pm2.ProgramName = Programs.Program2
                LEFT JOIN @ProgramMapping pm3 ON pm3.ProgramName = Programs.Program3
                LEFT JOIN @ProgramMapping pm4 ON pm4.ProgramName = Programs.Program4
                LEFT JOIN @ProgramMapping pm5 ON pm5.ProgramName = Programs.Program5
                LEFT JOIN @ProgramMapping pm6 ON pm6.ProgramName = Programs.Program6
        WHERE   ClientId = @ClientId AND Programs != '') s
        FOR
            XML PATH('Customer'),
            ELEMENTS,
            ROOT('Customers')) AS XmlData
        ,'Profile' AS XmlType;

        END TRY
        BEGIN CATCH
            THROW;
        END CATCH;

    END;




