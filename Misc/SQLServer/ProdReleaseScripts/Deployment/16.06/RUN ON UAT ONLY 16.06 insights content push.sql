/*
Run this script on:

aceUsql1c0.database.windows.net.Insights    -  This database will be modified

to synchronize it with:

aceQsql1c0.database.windows.net.Insights

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 11.1.3 from Red Gate Software Ltd at 6/24/2016 11:37:44 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

PRINT(N'Drop constraints from [dbo].[UserRole]')
ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK_UserRole_Role]
ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK_UserRole_User]

PRINT(N'Drop constraints from [dbo].[UserEnvironment]')
ALTER TABLE [dbo].[UserEnvironment] DROP CONSTRAINT [FK_UserEnvironment_Environment]
ALTER TABLE [dbo].[UserEnvironment] DROP CONSTRAINT [FK_UserEnvironment_User]

PRINT(N'Drop constraints from [dbo].[UserEndpoint]')
ALTER TABLE [dbo].[UserEndpoint] DROP CONSTRAINT [FK_UserEndpoint_Endpoint]
ALTER TABLE [dbo].[UserEndpoint] DROP CONSTRAINT [FK_UserEndpoint_User]

PRINT(N'Drop constraints from [cp].[ControlPanelLoginClient]')
ALTER TABLE [cp].[ControlPanelLoginClient] DROP CONSTRAINT [FK_ControlPanelLoginClient_ControlPanelLogin]

PRINT(N'Drop constraints from [dbo].[User]')
ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_User_Client]

PRINT(N'Drop constraint FK_ConfigAreaEndpoint_Endpoint from [dbo].[ConfigAreaEndpoint]')
ALTER TABLE [dbo].[ConfigAreaEndpoint] DROP CONSTRAINT [FK_ConfigAreaEndpoint_Endpoint]

PRINT(N'Drop constraint FK_ClientProperty_Client from [dbo].[ClientProperty]')
ALTER TABLE [dbo].[ClientProperty] DROP CONSTRAINT [FK_ClientProperty_Client]

PRINT(N'Update rows in [dbo].[UserEnvironment]')
UPDATE [dbo].[UserEnvironment] SET [UpdDate]='2016-04-21 12:39:10.540' WHERE [UserID]=33 AND [EnvID]=3
UPDATE [dbo].[UserEnvironment] SET [UpdDate]='2016-04-21 12:39:10.540' WHERE [UserID]=33 AND [EnvID]=4
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Update rows in [dbo].[User]')
UPDATE [dbo].[User] SET [UpdDate]='2016-06-06 16:44:04.287' WHERE [UserID]=29
UPDATE [dbo].[User] SET [UpdDate]='2016-04-21 12:37:49.007' WHERE [UserID]=33
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Update rows in [cp].[ControlPanelLogin]')
UPDATE [cp].[ControlPanelLogin] SET [EnableInd]=0, [UpdDate]='2016-06-22 18:04:06.523' WHERE [CpID]=4
UPDATE [cp].[ControlPanelLogin] SET [UpdDate]='2016-06-13 15:22:21.277' WHERE [CpID]=16
UPDATE [cp].[ControlPanelLogin] SET [SuperUserInd]=1 WHERE [CpID]=36
UPDATE [cp].[ControlPanelLogin] SET [UpdDate]='2016-06-01 10:56:10.310' WHERE [CpID]=37
UPDATE [cp].[ControlPanelLogin] SET [SuperUserInd]=1, [UpdDate]='2016-06-22 17:59:52.297' WHERE [CpID]=46
UPDATE [cp].[ControlPanelLogin] SET [EnableInd]=0, [UpdDate]='2016-06-22 18:04:13.497' WHERE [CpID]=55
PRINT(N'Operation applied to 6 rows out of 6')

PRINT(N'Update row in [dbo].[Endpoint]')
UPDATE [dbo].[Endpoint] SET [SortOrder]=95 WHERE [EndpointID]=22

PRINT(N'Update row in [dbo].[Client]')
UPDATE [dbo].[Client] SET [Name]='AceDemo', [Description]='ACE Demo User', [UpdDate]='2016-06-06 19:38:58.870' WHERE [ClientID]=4

PRINT(N'Add row to [dbo].[Client]')
INSERT INTO [dbo].[Client] ([ClientID], [Name], [Description], [RateCompanyID], [ReferrerID], [NewDate], [UpdDate], [AuthType], [EnableInd]) VALUES (80, 'Consumers', 'Consumers Energy', 1, NULL, '2016-06-20 12:48:31.000', '2016-06-22 11:10:14.160', 1, 1)

PRINT(N'Add row to [dbo].[Endpoint]')
INSERT INTO [dbo].[Endpoint] ([EndpointID], [Name], [ShortName], [SortOrder]) VALUES (23, '/api/v1/greenbuttonbill', 'greenbuttonbill', 200)

PRINT(N'Add row to [dbo].[User]')
SET IDENTITY_INSERT [dbo].[User] ON
INSERT INTO [dbo].[User] ([UserID], [ClientID], [ActorName], [CEAccessKeyID], [NewDate], [UpdDate], [EnableInd]) VALUES (34, 80, 'Web Users', '735e819cc0a94642b8589ee2e6e2ebeef905', '2016-06-20 15:17:53.350', '2016-06-21 14:24:00.230', 1)
SET IDENTITY_INSERT [dbo].[User] OFF

PRINT(N'Add rows to [cp].[ControlPanelLoginClient]')
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (3, 80)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (14, 80)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (16, 4)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (16, 61)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (16, 101)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (16, 210)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (16, 224)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (16, 233)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (16, 276)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (16, 283)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (16, 290)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (16, 766)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (16, 1234)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (16, 6539)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (16, 54654)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (16, 122469)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (36, 80)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (37, 61)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (37, 283)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (37, 290)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (37, 6539)
INSERT INTO [cp].[ControlPanelLoginClient] ([CpID], [ClientID]) VALUES (46, 80)
PRINT(N'Operation applied to 22 rows out of 22')

PRINT(N'Add rows to [dbo].[UserEndpoint]')
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (3, 23, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (29, 10, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (29, 11, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (29, 14, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (29, 15, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (29, 16, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (29, 17, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (29, 18, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (29, 19, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (29, 20, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (29, 21, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (29, 22, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (34, 0, 0)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (34, 1, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (34, 2, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (34, 4, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (34, 5, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (34, 6, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (34, 7, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (34, 8, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (34, 9, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (34, 11, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (34, 12, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (34, 13, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (34, 14, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (34, 16, 1)
INSERT INTO [dbo].[UserEndpoint] ([UserID], [EndpointID], [EnableInd]) VALUES (34, 18, 0)
PRINT(N'Operation applied to 27 rows out of 27')

PRINT(N'Add rows to [dbo].[UserEnvironment]')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (33, 0, 'DemoBasicKey290Prod', 'DemoBasicKey290Prod', '2016-04-21 12:39:10.523')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (33, 1, 'DemoBasicKey290', 'DemoBasicKey290', '2016-04-21 12:39:10.540')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (33, 2, 'DemoBasicKey290', 'DemoBasicKey290', '2016-04-21 12:39:10.540')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (34, 0, 'ConsumersProdBasicKey80', 'ConsumersProdBasicKey80', '2016-06-20 15:20:23.217')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (34, 1, 'ConsumersBasicKey80', 'ConsumersBasicKey80', '2016-06-20 15:20:23.230')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (34, 2, 'ConsumersBasicKey80', 'ConsumersBasicKey80', '2016-06-20 15:20:23.230')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (34, 3, 'ConsumersBasicKey80', 'ConsumersBasicKey80', '2016-06-20 15:20:23.230')
INSERT INTO [dbo].[UserEnvironment] ([UserID], [EnvID], [CESecretAccessKey], [BasicKey], [UpdDate]) VALUES (34, 4, 'ConsumersBasicKey80', 'ConsumersBasicKey80', '2016-06-20 15:20:23.230')
PRINT(N'Operation applied to 8 rows out of 8')

PRINT(N'Add rows to [dbo].[UserRole]')
INSERT INTO [dbo].[UserRole] ([UserID], [RoleID], [EnableInd]) VALUES (34, 0, 1)
INSERT INTO [dbo].[UserRole] ([UserID], [RoleID], [EnableInd]) VALUES (34, 1, 1)
INSERT INTO [dbo].[UserRole] ([UserID], [RoleID], [EnableInd]) VALUES (34, 2, 1)
PRINT(N'Operation applied to 3 rows out of 3')

PRINT(N'Add constraints to [dbo].[UserRole]')
ALTER TABLE [dbo].[UserRole] ADD CONSTRAINT [FK_UserRole_Role] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role] ([RoleID])
ALTER TABLE [dbo].[UserRole] WITH NOCHECK ADD CONSTRAINT [FK_UserRole_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])

PRINT(N'Add constraints to [dbo].[UserEnvironment]')
ALTER TABLE [dbo].[UserEnvironment] ADD CONSTRAINT [FK_UserEnvironment_Environment] FOREIGN KEY ([EnvID]) REFERENCES [dbo].[Environment] ([EnvID])
ALTER TABLE [dbo].[UserEnvironment] WITH NOCHECK ADD CONSTRAINT [FK_UserEnvironment_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])

PRINT(N'Add constraints to [dbo].[UserEndpoint]')
ALTER TABLE [dbo].[UserEndpoint] ADD CONSTRAINT [FK_UserEndpoint_Endpoint] FOREIGN KEY ([EndpointID]) REFERENCES [dbo].[Endpoint] ([EndpointID])
ALTER TABLE [dbo].[UserEndpoint] ADD CONSTRAINT [FK_UserEndpoint_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])

PRINT(N'Add constraints to [cp].[ControlPanelLoginClient]')
ALTER TABLE [cp].[ControlPanelLoginClient] ADD CONSTRAINT [FK_ControlPanelLoginClient_ControlPanelLogin] FOREIGN KEY ([CpID]) REFERENCES [cp].[ControlPanelLogin] ([CpID])

PRINT(N'Add constraints to [dbo].[User]')
ALTER TABLE [dbo].[User] ADD CONSTRAINT [FK_User_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])

PRINT(N'Add constraint FK_ConfigAreaEndpoint_Endpoint to [dbo].[ConfigAreaEndpoint]')
ALTER TABLE [dbo].[ConfigAreaEndpoint] WITH NOCHECK ADD CONSTRAINT [FK_ConfigAreaEndpoint_Endpoint] FOREIGN KEY ([EndpointID]) REFERENCES [dbo].[Endpoint] ([EndpointID])

PRINT(N'Add constraint FK_ClientProperty_Client to [dbo].[ClientProperty]')
ALTER TABLE [dbo].[ClientProperty] WITH NOCHECK ADD CONSTRAINT [FK_ClientProperty_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
COMMIT TRANSACTION
GO
