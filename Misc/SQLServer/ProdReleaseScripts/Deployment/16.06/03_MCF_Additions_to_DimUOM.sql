﻿/*
Run this script on:
        aceusql1c0.database.windows.net.InsightsMetaData
        aceusql1c0.database.windows.net.InsightsDW_Test
        aceusql1c0.database.windows.net.InsightsDW_80
        aceusql1c0.database.windows.net.InsightsDW_276
        aceusql1c0.database.windows.net.InsightsDW_224
        aceusql1c0.database.windows.net.InsightsDW_210
        aceusql1c0.database.windows.net.InsightsDW_101

        acepsql1c0.database.windows.net.InsightsMetaData
        acepsql1c0.database.windows.net.InsightsDW_Test
        acepsql1c0.database.windows.net.InsightsDW_80
        acepsql1c0.database.windows.net.InsightsDW_276
        acepsql1c0.database.windows.net.InsightsDW_224
        acepsql1c0.database.windows.net.InsightsDW_210
        acepsql1c0.database.windows.net.InsightsDW_101
*/

INSERT INTO [dbo].[DimUOM]
           ([UOMKey],[UOMId],[UOMDesc])
     VALUES
           (7,7,'hcf')
GO

INSERT INTO [dbo].[DimUOM]
           ([UOMKey],[UOMId],[UOMDesc])
     VALUES
           (8,8,'mcf')
GO