/*
Run this script on:
        aceusql1c0.database.windows.net.InsightsMetaData   
*/
USE [InsightsMetaData]
GO
/****** Object:  StoredProcedure [cm].[uspSelectContentClientAction]    Script Date: 6/22/2016 9:28:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Srini Ambati
-- Create date: 07/09/2014
-- Description:	This proc returns Action Content specific to client
-- Usage Example:
/*
BEGIN TRANSACTION

EXEC [cm].[uspSelectContentClientAction] 87

select * from cm.ClientAction

ROLLBACK TRANSACTION
*/
-- =============================================

ALTER PROCEDURE [cm].[uspSelectContentClientAction]
    (
      @clientID INT
    )
AS
    BEGIN
        SELECT  ca.ActionKey AS [Key],
				ca.NameKey,
                ISNULL([cm].[fnGetActionFieldValues](ca.ClientActionID, 'ClientActionCondition'), '') AS ConditionKeys ,
				ca.DescriptionKey ,
                ca.Hide ,
                ISNULL(ca.VideoKey, '') AS VideoKey ,
                ISNULL(ca.ImageKey, '') AS ImageKey ,
                ISNULL(ca.IconClass, '') AS IconClass ,
                ca.CommodityKey ,
                ca.ActionTypeKey AS [Type] ,
                [cm].[fnGetActionFieldValues](ca.ClientActionID, 'ClientActionAppliance') AS ApplianceKeys ,
                ca.UpfrontCost AS DefaultUpfrontCost,
                ca.AnnualCost AS DefaultAnnualCost,
                ca.CostVariancePercent AS DefaultCostVariancePercent,
                ca.DifficultyKey AS Difficulty ,
                ca.HabitIntervalKey AS HabitInterval ,
                ca.AnnualSavingsEstimate AS DefaultAnnualSavingsEstimate,
                ca.AnnualUsageSavingsEstimate AS DefaultAnnualUsageSavingsEstimate ,
                ca.AnnualUsageSavingsUomKey AS DefaultAnnualUsageSavingsUomKey,
                ca.PaybackTime AS DefaultPaybackTime,
                ca.ROI AS [DefaultRoi],
                ca.RebateAvailable ,
                ISNULL(ca.RebateUrl, '') AS RebateUrl , 
                ISNULL(ca.RebateImageKey, '') AS RebateImageKey ,
                ca.RebateAmount ,
                ISNULL([cm].[fnGetActionFieldValues](ca.ClientActionID, 'ClientActionSeason'), '') AS SeasonKeys ,
				ca.ActionPriority , 
				ca.SavingsCalcMethod , 
				SavingsAmount ,
				ISNULL([cm].[fnGetActionFieldValues](ca.ClientActionID, 'ClientActionWhatIfData'), '') AS WhatIfDataKeys , 
				ca.CostExpression ,
				ca.RebateExpression ,
				ISNULL(ca.[NextStepLink], '') AS NextStepLink ,
				ISNULL(ca.[NextStepLinkText], '') AS NextStepLinkText ,
				ISNULL(ca.[NextStepLinkType], '') AS NextStepLinkType ,
				ISNULL(ca.[Tags], '') AS Tags ,
				MinimumSavingsAmt , 
				MaximumSavingsAmt
        FROM    cm.ClientAction AS ca WITH ( NOLOCK )
                INNER JOIN ( SELECT MAX(clientid) AS clientid ,
                                    ActionKey
                             FROM   cm.ClientAction WITH ( NOLOCK )
                             WHERE  clientid IN ( 0, @clientID )
                             GROUP BY ActionKey
                           ) AS bw ON bw.clientid = ca.ClientID
                                      AND bw.ActionKey = ca.ActionKey
        WHERE   [Disable] = 0
        ORDER BY ca.[ActionKey]

    END
