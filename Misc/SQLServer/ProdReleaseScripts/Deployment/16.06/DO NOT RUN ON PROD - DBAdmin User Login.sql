-- Execute this script in the master database
IF NOT EXISTS 
    (SELECT name  
     FROM master.sys.sql_logins
     WHERE name = 'DBAdmin')
BEGIN
    CREATE LOGIN DBAdmin WITH PASSWORD = 'Acl@r@592'
END

IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'DBAdminUser')
BEGIN
    CREATE USER DBAdminUser FOR LOGIN DBAdmin
END


-- Execute this script in all the shards except the master database
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'DBAdminUser')
BEGIN
    CREATE USER DBAdminUser FOR LOGIN DBAdmin
END


EXEC sp_addrolemember 'db_owner', 'DBAdminUser'
GO
