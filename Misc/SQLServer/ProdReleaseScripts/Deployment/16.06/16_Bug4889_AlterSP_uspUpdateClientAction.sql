/*
Run this script on:
        aceusql1c0.database.windows.net.InsightsMetaData   
*/
USE [InsightsMetaData]
GO

/****** Object:  StoredProcedure [cm].[uspUpdateClientAction]    Script Date: 6/22/2016 9:29:04 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		Srini Ambati
-- Create date: 01/01/2015
-- Description:	This table merges the data from contentful into SQL tables for use by BI
-- Usage Example:
/*
BEGIN TRANSACTION

	declare @p1 cm.ClientActionTable
	insert into @p1 values(0,N'getpoolcover',N'efficiency',0,0,N'gas',N'low',N'onetime', N'action.getpoolcover.name', N'action.getpoolcover.desc', 0.00, 0.00, 20.00, 16.00, N'therms', 0.00,0, 0.00, 0, 0.00, N'', N'', N'', N'', N'', NULL, N'pool.yes', N'pool', N'', 0, N'WhatIf', 0.55, N'test1')
	EXEC [cm].[uspUpdateClientAction] @clientActionTable=@p1

	SELECT * FROM cm.TypeAction
	SELECT * FROM [cm].[ClientAction]
	SELECT * FROM cm.ClientActionWhatIfData

ROLLBACK TRANSACTION
*/
-- =============================================
CREATE PROCEDURE [cm].[uspUpdateClientAction]
    (
      @clientActionTable [cm].[ClientActionTable] READONLY 
    )
AS
    BEGIN

	-- Step 1: Declara local variables and assign values
        DECLARE @maxTextContentID INT;
        DECLARE @maxActionID INT;
        DECLARE @currentDateTime DATETIME;

        SELECT  @maxTextContentID = ISNULL(MAX(TextContentID), 0) + 1
        FROM    [cm].[TypeTextContent] WITH ( NOLOCK );

        SELECT  @maxActionID = ISNULL(MAX(ActionID), 0) + 1
        FROM    [cm].[TypeAction] WITH ( NOLOCK );
		
        SET @currentDateTime = GETDATE();

        BEGIN TRANSACTION

        BEGIN TRY

			-- Step 2: Insert new TextContent keys 
            WITH    NewTextContent ( TextContentKey )
                      AS ( SELECT DISTINCT
                                    NameKey
                           FROM     @clientActionTable
                         )
                INSERT  INTO [cm].[TypeTextContent]
                        ( TextContentKey ,
                          TextContentID ,
                          DateUpdated
						)
                        SELECT  na.TextContentKey ,
                                ROW_NUMBER() OVER ( ORDER BY na.TextContentKey )
                                + @maxTextContentID ,
                                @currentDateTime
                        FROM    NewTextContent AS na WITH ( NOLOCK )
                                LEFT OUTER JOIN [cm].[TypeTextContent] AS ta
                                WITH ( NOLOCK ) ON na.TextContentKey = ta.TextContentKey
                        WHERE   ta.TextContentKey IS NULL

			-- Step 3: Insert new Action keys 
			;
            WITH    NewAction ( ActionKey )
                      AS ( SELECT DISTINCT
                                    ActionKey
                           FROM     @clientActionTable
                         )
                INSERT  INTO [cm].[TypeAction]
                        ( ActionKey ,
                          ActionID ,
                          DateUpdated
						)
                        SELECT  na.ActionKey ,
                                ROW_NUMBER() OVER ( ORDER BY na.ActionKey )
                                + @maxActionID ,
                                @currentDateTime
                        FROM    NewAction AS na WITH ( NOLOCK )
                                LEFT OUTER JOIN [cm].[TypeAction] AS ta WITH ( NOLOCK ) ON na.ActionKey = ta.ActionKey
                        WHERE   ta.ActionKey IS NULL

		-- Step 4: Merge Client Actions
            MERGE ClientAction AS TARGET
            USING @clientActionTable AS SOURCE
            ON ( TARGET.ClientID = SOURCE.ClientID
                 AND TARGET.ActionKey = SOURCE.ActionKey
               )
            WHEN MATCHED THEN
                UPDATE SET
					TARGET.[ActionTypeKey] = SOURCE.[ActionTypeKey] ,
					TARGET.[Hide] = SOURCE.[Hide] ,
					TARGET.[Disable] = SOURCE.[Disable] ,
					TARGET.[CommodityKey] = SOURCE.[CommodityKey] ,
					TARGET.[DifficultyKey] = SOURCE.[DifficultyKey] ,
					TARGET.[HabitIntervalKey] = SOURCE.[HabitIntervalKey] ,
					TARGET.[NameKey] = SOURCE.[NameKey] ,
					TARGET.[DescriptionKey] = SOURCE.[DescriptionKey] ,
					TARGET.[AnnualCost] = SOURCE.[AnnualCost] ,
					TARGET.[UpfrontCost] = SOURCE.[UpfrontCost] ,
					TARGET.[AnnualSavingsEstimate] = SOURCE.[AnnualSavingsEstimate] ,
					TARGET.[AnnualUsageSavingsEstimate] = SOURCE.[AnnualUsageSavingsEstimate] ,
					TARGET.[AnnualUsageSavingsUomKey] = SOURCE.[AnnualUsageSavingsUomKey] ,
					TARGET.[CostVariancePercent] = SOURCE.[CostVariancePercent] ,
					TARGET.[PaybackTime] = SOURCE.[PaybackTime] ,
					TARGET.[ROI] = SOURCE.[ROI] ,
					TARGET.[RebateAvailable] = SOURCE.[RebateAvailable] ,
					TARGET.[RebateAmount] = SOURCE.[RebateAmount] ,
					TARGET.[RebateUrl] = SOURCE.[RebateUrl] ,
					TARGET.[RebateImageKey] = SOURCE.[RebateImageKey] ,
					TARGET.[IconClass] = SOURCE.[IconClass] ,
					TARGET.[ImageKey] = SOURCE.[ImageKey] ,
					TARGET.[VideoKey] = SOURCE.[VideoKey] ,
					TARGET.[Comments] = SOURCE.[Comments] ,
					TARGET.[ActionPriority] = SOURCE.[ActionPriority] ,
					TARGET.[SavingsCalcMethod] = SOURCE.[SavingsCalcMethod] ,
					TARGET.[SavingsAmount] = SOURCE.[SavingsAmount] ,
					TARGET.[CostExpression] = SOURCE.[CostExpression] ,
					TARGET.[RebateExpression] = SOURCE.[RebateExpression] ,
					TARGET.[NextStepLink] = SOURCE.[NextStepLink] ,
					TARGET.[NextStepLinkText] = SOURCE.[NextStepLinkText] , 
					TARGET.[NextStepLinkType] = SOURCE.[NextStepLinkType] ,
					TARGET.[Tags] = SOURCE.[Tags] ,
					TARGET.[MinimumSavingsAmt] = SOURCE.[MinimumSavingsAmt] ,
					TARGET.[MaximumSavingsAmt] = SOURCE.[MaximumSavingsAmt]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( [ClientID] ,
                         [ActionKey] ,
                         [ActionTypeKey]	,
						[Hide]	,
						[Disable]	,
						[CommodityKey]	,
						[DifficultyKey]	,
						[HabitIntervalKey]	,
						[NameKey]	,
						[DescriptionKey]	,
						[AnnualCost]	,
						[UpfrontCost]	,
						[AnnualSavingsEstimate]	,
						[AnnualUsageSavingsEstimate]	,
						[AnnualUsageSavingsUomKey]	,
						[CostVariancePercent]	,
						[PaybackTime]	,
						[ROI]	,
						[RebateAvailable]	,
						[RebateAmount]	,
						[RebateUrl]	,
						[RebateImageKey]	,
						[IconClass]	,
						[ImageKey]	,
						[VideoKey]	,
						[Comments] ,
						[DateUpdated] , 
						[ActionPriority] ,
						[SavingsCalcMethod] ,
						[SavingsAmount] ,
						[CostExpression] ,
						[RebateExpression] ,
						[NextStepLink] ,
						[NextStepLinkText] , 
						[NextStepLinkType] ,
						[Tags] ,
						[MinimumSavingsAmt] ,
						[MaximumSavingsAmt] 
					   )
                VALUES ( SOURCE.ClientID ,
                         SOURCE.ActionKey ,
                         SOURCE.[ActionTypeKey],
						SOURCE.[Hide],
						SOURCE.[Disable],
						SOURCE.[CommodityKey],
						SOURCE.[DifficultyKey],
						SOURCE.[HabitIntervalKey],
						SOURCE.[NameKey],
						SOURCE.[DescriptionKey],
						SOURCE.[AnnualCost],
						SOURCE.[UpfrontCost],
						SOURCE.[AnnualSavingsEstimate],
						SOURCE.[AnnualUsageSavingsEstimate],
						SOURCE.[AnnualUsageSavingsUomKey],
						SOURCE.[CostVariancePercent],
						SOURCE.[PaybackTime],
						SOURCE.[ROI],
						SOURCE.[RebateAvailable],
						SOURCE.[RebateAmount],
						SOURCE.[RebateUrl],
						SOURCE.[RebateImageKey],
						SOURCE.[IconClass],
						SOURCE.[ImageKey],
						SOURCE.[VideoKey],
						SOURCE.[Comments],
                         @currentDateTime,
						SOURCE.[ActionPriority] ,
						SOURCE.[SavingsCalcMethod] ,
						SOURCE.[SavingsAmount] ,
						SOURCE.[CostExpression] ,
						SOURCE.[RebateExpression] ,
						SOURCE.[NextStepLink] ,
						SOURCE.[NextStepLinkText] , 
						SOURCE.[NextStepLinkType] ,
						SOURCE.[Tags],
						SOURCE.[MinimumSavingsAmt] ,
						SOURCE.[MaximumSavingsAmt] 
					   );

            INSERT  INTO cm.ContentOrphanLog
                    ( ClientID ,
                      Typename ,
                      [Key] ,
                      KeyValue
                    )
                    ( SELECT    ClientID ,
                                'Action' ,
                                'ActionKey' ,
                                ActionKey
                      FROM      cm.ClientAction
                      WHERE     ClientID = 0
                                AND ActionKey NOT IN (
                                SELECT  ActionKey
                                FROM    @clientActionTable )
                    );


		-- Step 5: Create temp tables

            CREATE TABLE #ActionConditions
                (
                  [ClientActionID] INT NOT NULL ,
                  [ConditionKey] VARCHAR(256) NOT NULL ,
                  [DisplayOrder] INT NOT NULL
                )
            ON  [PRIMARY]

            CREATE TABLE #ActionAppliances
                (
                  [ClientActionID] INT NOT NULL ,
                  [ApplianceKey] VARCHAR(256) NOT NULL ,
                  [DisplayOrder] INT NOT NULL
                )
            ON  [PRIMARY]

            CREATE TABLE #ActionSeason
                (
                  [ClientActionID] INT NOT NULL ,
                  [SeasonKey] VARCHAR(256) NOT NULL ,
                  [DisplayOrder] INT NOT NULL
                )
            ON  [PRIMARY]

            CREATE TABLE #ActionWhatIfData
                (
                  [ClientActionID] INT NOT NULL ,
                  [WhatIfDataKey] VARCHAR(256) NOT NULL ,
                  [DisplayOrder] INT NOT NULL
                )
            ON  [PRIMARY]

            DECLARE @Action_Cursor CURSOR
            DECLARE @clientID INT
            DECLARE @ActionKey VARCHAR(256)
            DECLARE @ConditionKeys VARCHAR(4000)
            DECLARE @ApplianceKeys VARCHAR(4000)
            DECLARE @SeasonKeys VARCHAR(4000)
			DECLARE @WhatIfDataKeys VARCHAR(4000)

            DECLARE @ConditionKey VARCHAR(256)
            DECLARE @Condition_Cursor CURSOR

            DECLARE @ApplianceKey VARCHAR(256)
            DECLARE @Appliance_Cursor CURSOR

            DECLARE @SeasonKey VARCHAR(256)
            DECLARE @Season_Cursor CURSOR

            DECLARE @WhatIfDataKey VARCHAR(256)
            DECLARE @WhatIfData_Cursor CURSOR

            DECLARE @DisplayOrder INT 
			--DECLARE @ClientActionId INT

            SET @Action_Cursor = CURSOR FOR
			SELECT ClientID, ActionKey, ConditionKeys, ApplianceKeys, SeasonKeys, WhatIfDataKeys FROM @clientActionTable
            OPEN @Action_Cursor
            FETCH NEXT
			FROM @Action_Cursor INTO @clientID, @ActionKey, @ConditionKeys,@ApplianceKeys, @SeasonKeys, @WhatIfDataKeys
            WHILE @@FETCH_STATUS = 0
                BEGIN
					--SET @ClientActionId = [cm].[fnGetClientActionID](@clientID,@ActionKey)
					
                    IF ( @ConditionKeys <> '' )
                        BEGIN 
						--SELECT @ConditionKeys
						-- insert into Condition temp table
                            SET @Condition_Cursor = CURSOR FOR
							SELECT [Data], [Id] FROM [cm].[fnSplitStringAsTable](@ConditionKeys, ';')
                            OPEN @Condition_Cursor 
                            FETCH NEXT
							FROM @Condition_Cursor INTO @ConditionKey, @DisplayOrder
                            WHILE @@FETCH_STATUS = 0
                                BEGIN
                                    IF ( LTRIM(RTRIM(@ConditionKey)) <> '' )
                                        BEGIN 
										    DECLARE @fnClientActionID INT
											SELECT @fnClientActionID = [cm].[fnGetClientActionID](@clientID,@ActionKey)
											
											DECLARE @retVal int
											SELECT @retVal = COUNT(*) 
											FROM  #ActionConditions
											WHERE [ClientActionID] = @fnClientActionID AND [ConditionKey] = @ConditionKey

											IF (@retVal = 0)
											BEGIN
												  INSERT  INTO #ActionConditions
                                                    ( [ClientActionID] ,
                                                      [ConditionKey] ,
                                                      [DisplayOrder]
													)
													VALUES  ( @fnClientActionID ,
															  LTRIM(RTRIM(@ConditionKey)) ,
															  @DisplayOrder
															)
											END			
                                        END 
                                    FETCH NEXT
								FROM @Condition_Cursor INTO @ConditionKey, @DisplayOrder
                                END
                            CLOSE @Condition_Cursor
                            DEALLOCATE @Condition_Cursor
                        END 
					ELSE
						BEGIN
							DELETE FROM [cm].ClientActionCondition WHERE ClientActionID = [cm].[fnGetClientActionID](@clientID,@ActionKey)
						END

                    IF ( @ApplianceKeys <> '' )
                        BEGIN 
						--SELECT @ApplianceKeys
						-- insert into Appliance temp table
                            SET @Appliance_Cursor = CURSOR FOR
							SELECT [Data], [Id] FROM [cm].[fnSplitStringAsTable](@ApplianceKeys, ';')
                            OPEN @Appliance_Cursor
                            FETCH NEXT
							FROM @Appliance_Cursor INTO @ApplianceKey,@DisplayOrder
                            WHILE @@FETCH_STATUS = 0
                                BEGIN
                                    IF ( LTRIM(RTRIM(@ApplianceKey)) <> '' )
                                        BEGIN 
                                            INSERT  INTO #ActionAppliances
                                                    ( [ClientActionID] ,
                                                      [ApplianceKey] ,
                                                      [DisplayOrder]
													)
                                            VALUES  ( [cm].[fnGetClientActionID](@clientID,@ActionKey) ,
                                                      LTRIM(RTRIM(@ApplianceKey)) ,
                                                      @DisplayOrder
													)
                                        END 
                                    FETCH NEXT
								FROM @Appliance_Cursor INTO @ApplianceKey, @DisplayOrder
                                END
                            CLOSE @Appliance_Cursor
                            DEALLOCATE @Appliance_Cursor
                        END 
					ELSE
						BEGIN
							DELETE FROM [cm].ClientActionAppliance WHERE ClientActionID = [cm].[fnGetClientActionID](@clientID,@ActionKey)
						END

                    IF ( @SeasonKeys <> '' )
                        BEGIN 
						--SELECT @SeasonKeys
						-- insert into Season temp table
                            SET @Season_Cursor = CURSOR FOR
							SELECT [Data], [Id] FROM [cm].[fnSplitStringAsTable](@SeasonKeys, ';')
                            OPEN @Season_Cursor
                            FETCH NEXT
							FROM @Season_Cursor INTO @SeasonKey, @DisplayOrder
                            WHILE @@FETCH_STATUS = 0
                                BEGIN
                                    IF ( LTRIM(RTRIM(@SeasonKey)) <> '' )
                                        BEGIN 
                                            INSERT  INTO #ActionSeason
                                                    ( [ClientActionID] ,
                                                      [SeasonKey] ,
                                                      [DisplayOrder]
													)
                                            VALUES  ( [cm].[fnGetClientActionID](@clientID, @ActionKey) ,
                                                      LTRIM(RTRIM(@SeasonKey)) ,
                                                      @DisplayOrder
													)
                                        END 
                                    FETCH NEXT
								FROM @Season_Cursor INTO @SeasonKey, @DisplayOrder
                                END
                            CLOSE @Season_Cursor
                            DEALLOCATE @Season_Cursor
                        END 
					ELSE
						BEGIN
							DELETE FROM [cm].ClientActionSeason WHERE ClientActionID = [cm].[fnGetClientActionID](@clientID,@ActionKey)
						END

					 IF ( @WhatIfDataKeys <> '' )
                        BEGIN 
						--SELECT @WhatIfDataKeys
						-- insert into WhatIfData temp table
                            SET @WhatIfData_Cursor = CURSOR FOR
							SELECT [Data], [Id] FROM [cm].[fnSplitStringAsTable](@WhatIfDataKeys, ';')
                            OPEN @WhatIfData_Cursor
                            FETCH NEXT
							FROM @WhatIfData_Cursor INTO @WhatIfDataKey, @DisplayOrder
                            WHILE @@FETCH_STATUS = 0
                                BEGIN
                                    IF ( LTRIM(RTRIM(@WhatIfDataKey)) <> '' )
                                        BEGIN 
                                            INSERT  INTO #ActionWhatIfData
                                                    ( [ClientActionID] ,
                                                      [WhatIfDataKey] ,
                                                      [DisplayOrder]
													)
                                            VALUES  ( [cm].[fnGetClientActionID](@clientID, @ActionKey) ,
                                                      LTRIM(RTRIM(@WhatIfDataKey)) ,
                                                      @DisplayOrder
													)
                                        END 
                                    FETCH NEXT
								FROM @WhatIfData_Cursor INTO @WhatIfDataKey, @DisplayOrder
                                END
                            CLOSE @WhatIfData_Cursor
                            DEALLOCATE @WhatIfData_Cursor
                        END 
					ELSE
						BEGIN
							DELETE FROM [cm].ClientActionWhatIfData WHERE ClientActionID = [cm].[fnGetClientActionID](@clientID,@ActionKey)
						END


                    FETCH NEXT
					FROM @Action_Cursor INTO @clientID, @ActionKey, @ConditionKeys, @ApplianceKeys, @SeasonKeys, @WhatIfDataKeys
                END
            CLOSE @Action_Cursor
            DEALLOCATE @Action_Cursor
		
            SELECT  [ClientActionID] ,
                    [ConditionKey] AS MissingConditionKey
            FROM    #ActionConditions
            WHERE   ConditionKey NOT IN ( SELECT    ConditionKey
                                          FROM      cm.TypeCondition )

			-- Step 6: Merge Client Action Profiles Conditions
            MERGE [cm].ClientActionCondition AS t
            USING
                ( SELECT DISTINCT
                            ClientActionID ,
                            ConditionKey,
							[DisplayOrder]                            
                  FROM      #ActionConditions
                ) AS s
            ON ( t.ClientActionID = s.ClientActionID
                 AND t.ConditionKey = s.ConditionKey
               )
            WHEN MATCHED THEN
                UPDATE SET
                         t.[DisplayOrder] = s.[DisplayOrder]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( ClientActionID ,
                         ConditionKey
					   )
                VALUES ( s.ClientActionID ,
                         s.ConditionKey
					   )
            WHEN NOT MATCHED BY SOURCE AND t.ClientActionID IN (SELECT ClientActionID FROM #ActionConditions) THEN
                DELETE;

            SELECT  [ClientActionID] ,
                    [ApplianceKey] AS MissingApplianceKey
            FROM    #ActionAppliances
            WHERE   ApplianceKey NOT IN ( SELECT    ApplianceKey
                                              FROM      cm.TypeAppliance )

			-- Step 7: Merge Client Action Profiles Appliances
            MERGE [cm].ClientActionAppliance AS t
            USING
                ( SELECT DISTINCT
                            ClientActionID ,
                            ApplianceKey ,
                            [DisplayOrder]
                  FROM      #ActionAppliances
                ) AS s
            ON ( t.ClientActionID = s.ClientActionID
                 AND t.ApplianceKey = s.ApplianceKey
               )
            WHEN MATCHED THEN
                UPDATE SET
                         t.[DisplayOrder] = s.[DisplayOrder]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( ClientActionID ,
                         ApplianceKey 
					   )
                VALUES ( s.ClientActionID ,
                         s.ApplianceKey 
					   )
            WHEN NOT MATCHED BY SOURCE AND t.ClientActionID IN (SELECT ClientActionID FROM #ActionAppliances) THEN
                DELETE;

            SELECT  [ClientActionID] ,
                    [SeasonKey] AS MissingSeasonKey
            FROM    #ActionSeason
            WHERE   SeasonKey NOT IN ( SELECT  SeasonKey
                                            FROM    cm.TypeSeason )

			-- Step 8: Merge Client Action Profiles Seasons
            MERGE [cm].ClientActionSeason AS t
            USING
                ( SELECT DISTINCT
                            ClientActionID ,
                            SeasonKey ,
                            [DisplayOrder]
                  FROM      #ActionSeason
                ) AS s
            ON ( t.ClientActionID = s.ClientActionID
                 AND t.SeasonKey = s.SeasonKey
               )
            WHEN MATCHED THEN
                UPDATE SET
                         t.[DisplayOrder] = s.[DisplayOrder]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( ClientActionID ,
                         SeasonKey 
					   )
                VALUES ( s.ClientActionID ,
                         s.SeasonKey 
					   )
            WHEN NOT MATCHED BY SOURCE AND t.ClientActionID IN (SELECT ClientActionID FROM #ActionSeason) THEN
                DELETE;

			SELECT  [ClientActionID] ,
                    [WhatIfDataKey] AS MissingWhatIfDataKey
            FROM    #ActionWhatIfData
            WHERE   WhatIfDataKey NOT IN ( SELECT  WhatIfDataKey
                                            FROM    cm.TypeWhatIfData )

			-- Step 8: Merge ClientActionWhatIfData
            MERGE [cm].ClientActionWhatIfData AS t
            USING
                ( SELECT DISTINCT
                            ClientActionID ,
                            WhatIfDataKey ,
                            [DisplayOrder]
                  FROM      #ActionWhatIfData
                ) AS s
            ON ( t.ClientActionID = s.ClientActionID
                 AND t.WhatIfDataKey = s.WhatIfDataKey
               )
            WHEN MATCHED THEN
                UPDATE SET
                         t.[DisplayOrder] = s.[DisplayOrder]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( ClientActionID ,
                         WhatIfDataKey 
					   )
                VALUES ( s.ClientActionID ,
                         s.WhatIfDataKey 
					   )
            WHEN NOT MATCHED BY SOURCE AND t.ClientActionID IN (SELECT ClientActionID FROM #ActionWhatIfData) THEN
                DELETE;

        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                ROLLBACK TRANSACTION;

            THROW;

        END CATCH;

        IF @@TRANCOUNT > 0
            COMMIT TRANSACTION;

    END







GO


