/*
Run this script on:
        aceusql1c0.database.windows.net.InsightsMetaData    -  This database will be modified
to synchronize it with:
        aceqsql1c0.database.windows.net.InsightsMetaData
*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [cm].[EMZipcode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [cm].[EMZipcode] ADD
[ClimateId] [int] NULL CONSTRAINT [DF_EMZipcode_ClimateId] DEFAULT ((0))
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [cm].[uspEMSelectRegionParamsByClientZipcode]'
GO

ALTER PROCEDURE [cm].[uspEMSelectRegionParamsByClientZipcode]
	@ClientID As INT,
    @Zipcode AS VARCHAR(12) = NULL
AS
    BEGIN

		SELECT TOP 1 [ZipCode]
			  ,[TemperatureRegionId]
			  ,[ApplianceRegionId]
			  ,[SolarRegionId]
			  ,[State]
			  ,[StationIdDaily]
			  ,[ClimateId]
		  FROM [cm].[EMZipcode] WITH (NOLOCK)
		  WHERE ([ClientID] = 0 OR [ClientID] = @ClientID) AND [ZipCode] = @Zipcode
		  ORDER BY [ClientID] DESC

    END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [cm].[BEMClientBuildingCategorySizeEUI]'
GO
CREATE TABLE [cm].[BEMClientBuildingCategorySizeEUI]
(
[ClientId] [int] NOT NULL,
[BuildingCategoryId] [tinyint] NOT NULL,
[BuildingSizeId] [tinyint] NOT NULL,
[CommodityId] [tinyint] NOT NULL,
[EUIHeating] [decimal] (18, 4) NOT NULL,
[EUICooling] [decimal] (18, 4) NOT NULL,
[EUIVentilation] [decimal] (18, 4) NOT NULL,
[EUIWaterHeating] [decimal] (18, 4) NOT NULL,
[EUILighting] [decimal] (18, 4) NOT NULL,
[EUICooking] [decimal] (18, 4) NOT NULL,
[EUIRefrigeration] [decimal] (18, 4) NOT NULL,
[EUIOfficeEquipment] [decimal] (18, 4) NOT NULL,
[EUIComputers] [decimal] (18, 4) NOT NULL,
[EUIMiscellaneous] [decimal] (18, 4) NOT NULL,
[EUILaundry] [decimal] (18, 4) NOT NULL,
[ClimateId] [tinyint] NOT NULL CONSTRAINT [DF__BEMClient__Clima__078DB5B4] DEFAULT ((0))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BEMClientBuildingCategorySizeEUI] on [cm].[BEMClientBuildingCategorySizeEUI]'
GO
ALTER TABLE [cm].[BEMClientBuildingCategorySizeEUI] ADD CONSTRAINT [PK_BEMClientBuildingCategorySizeEUI] PRIMARY KEY CLUSTERED  ([ClientId], [BuildingCategoryId], [BuildingSizeId], [CommodityId], [ClimateId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [cm].[BEMBuildingCategory]'
GO
CREATE TABLE [cm].[BEMBuildingCategory]
(
[BuildingCategoryId] [tinyint] NOT NULL,
[Name] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BEMBuildingCategory] on [cm].[BEMBuildingCategory]'
GO
ALTER TABLE [cm].[BEMBuildingCategory] ADD CONSTRAINT [PK_BEMBuildingCategory] PRIMARY KEY CLUSTERED  ([BuildingCategoryId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [cm].[uspBEMSelectBuildingEUIs]'
GO


CREATE PROCEDURE [cm].[uspBEMSelectBuildingEUIs]
    (
      @clientID INT 
    )
AS
    BEGIN
       
        WITH    BEMEnergyUseIntensityConfig
                  AS ( SELECT   bem.ClientId AS [ClientId],
								bem.[BuildingCategoryId] AS [BuildingCategoryId],
                                bem.[BuildingSizeId] AS [BuildingSizeId],
                                bem.[CommodityId] AS [CommodityId],
                                bem.[ClimateId] AS [ClimateId],
								bem.[EUIHeating] AS [EUIHeating],
								bem.[EUICooling] AS [EUICooling],
								bem.[EUIComputers] AS [EUIComputers],
								bem.[EUICooking] AS [EUICooking],
								bem.[EUILaundry] AS [EUILaundry],
								bem.[EUILighting] AS [EUILighting],
								bem.[EUIMiscellaneous] AS [EUIMiscellaneous],
								bem.[EUIOfficeEquipment] AS [EUIOfficeEquipment],
								bem.[EUIRefrigeration] AS [EUIRefrigeration],
								bem.[EUIVentilation] AS [EUIVentilation],
								bem.[EUIWaterHeating] AS [EUIWaterHeating],
								bc.[Name] AS [BuildingCategoryName],
                                ROW_NUMBER() OVER ( PARTITION BY bem.BuildingCategoryId, bem.BuildingSizeId, bem.CommodityId, bem.[ClimateId] ORDER BY IIF(ClientId = @clientID, 1, 0)DESC, ClientId DESC ) AS SortId
                       FROM     [cm].[BEMClientBuildingCategorySizeEUI] AS bem WITH ( NOLOCK )
                                INNER JOIN [cm].BEMBuildingCategory AS bc WITH ( NOLOCK ) ON bem.BuildingCategoryId = bc.BuildingCategoryId
                       WHERE    bem.ClientId IN ( 0, @clientID )
                     )
            SELECT  [ClientId],
					[BuildingCategoryId] ,
                    [BuildingSizeId] ,
                    [CommodityId],
				    [ClimateId],
					[EUIHeating],
					[EUICooling],
					[EUIComputers],
					[EUICooking],
					[EUILaundry],
					[EUILighting],
					[EUIMiscellaneous],
					[EUIOfficeEquipment],
					[EUIRefrigeration],
					[EUIVentilation],
					[EUIWaterHeating],
					[BuildingCategoryName]
            FROM    BEMEnergyUseIntensityConfig
            WHERE   SortId = 1
    END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [cm].[BEMBuildingSize]'
GO
CREATE TABLE [cm].[BEMBuildingSize]
(
[BuildingSizeId] [tinyint] NOT NULL,
[Description] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MinSquareFeet] [int] NOT NULL,
[MaxSquareFeet] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BEMBuildingSize] on [cm].[BEMBuildingSize]'
GO
ALTER TABLE [cm].[BEMBuildingSize] ADD CONSTRAINT [PK_BEMBuildingSize] PRIMARY KEY CLUSTERED  ([BuildingSizeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [cm].[BEMClientBuildingCategorySizeEUI]'
GO
ALTER TABLE [cm].[BEMClientBuildingCategorySizeEUI] ADD CONSTRAINT [FK_BEMClientBuildingCategorySizeEUI_BEMBuildingCategory] FOREIGN KEY ([BuildingCategoryId]) REFERENCES [cm].[BEMBuildingCategory] ([BuildingCategoryId])
ALTER TABLE [cm].[BEMClientBuildingCategorySizeEUI] ADD CONSTRAINT [FK_BEMClientBuildingCategorySizeEUI_BEMBuildingSize] FOREIGN KEY ([BuildingSizeId]) REFERENCES [cm].[BEMBuildingSize] ([BuildingSizeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO

