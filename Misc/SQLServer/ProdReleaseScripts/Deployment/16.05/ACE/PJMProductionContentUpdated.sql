USE InsightsMetaData


--- update content
UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'NA',
LongText = 'The following message is for test purposes only.{%newline%}{%newline%}An Emergency Curtailment Event is being initiated today, {%notificationdate%} at {%personalizedstarttime%} Eastern Daylight Time by the FirstEnergy Ohio Electric Utilities based upon the determination by PJM Interconnection.  Per Rider ELR, you must curtail all load above your Firm Load by {%personalizedstarttime%} Eastern Daylight Time.  The FirstEnergy Ohio Electric Utilities will send out a notice when it is determined when this Emergency Curtailment Event will end or when it has ended.  If you have any questions, please contact your FirstEnergy Customer Support Representative.{%newline%}{%newline%}Sincerely,{%newline%}FirstEnergy Ohio Electric Utilities',
MediumText = 'The following message is for test purposes only.{%newline%}{%newline%}An Emergency Curtailment Event is being initiated today, {%notificationdate%} at {%personalizedstarttime%} Eastern Daylight Time by the FirstEnergy Ohio Electric Utilities based upon the determination by PJM Interconnection.  Per Rider ELR, you must curtail all load above your Firm Load by {%personalizedstarttime%} Eastern Daylight Time.  The FirstEnergy Ohio Electric Utilities will send out a notice when it is determined when this Emergency Curtailment Event will end or when it has ended.  If you have any questions, please contact your FirstEnergy Customer Support Representative.{%newline%}{%newline%}Sincerely,{%newline%}FirstEnergy Ohio Electric Utilities'
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.external1.body')
AND LocaleKey = 'en-US'

UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment - Event Start TEST ',
LongText = 'TEST Rider ELR - Emergency Curtailment Event TEST ',
MediumText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment - Event Start TEST '
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.external1.subject')
AND LocaleKey = 'en-US'

UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'NA',
LongText = 'The following message is for test purposes only.{%newline%}{%newline%}The Emergency Curtailment Event from today, {%enddate%} has ended.  Please contact your FirstEnergy Customer Support Representative if you have any questions. {%newline%}{%newline%}Sincerely,{%newline%}FirstEnergy Ohio Electric Utilities',
MediumText = 'The following message is for test purposes only.{%newline%}{%newline%}The Emergency Curtailment Event from today, {%enddate%} has ended.  Please contact your FirstEnergy Customer Support Representative if you have any questions. {%newline%}{%newline%}Sincerely,{%newline%}FirstEnergy Ohio Electric Utilities'
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.external2.body')
AND LocaleKey = 'en-US'

UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment - Event has Ended TEST ',
LongText = 'TEST Rider ELR - Emergency Curtailment Event - End of Event TEST ',
MediumText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment - Event has Ended TEST '
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.external2.subject')
AND LocaleKey = 'en-US'


UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'NA',
LongText = 'The following message is for test purposes only.{%newline%}{%newline%}The Emergency Curtailment Event from today, {%enddate%}, will end at {%endtime%} Eastern Daylight Time.  Please contact your FirstEnergy Customer Support Representative if you have any questions. {%newline%}{%newline%}Sincerely,{%newline%}FirstEnergy Ohio Electric Utilities',
MediumText = 'The following message is for test purposes only.{%newline%}{%newline%}The Emergency Curtailment Event from today, {%enddate%}, will end at {%endtime%} Eastern Daylight Time.  Please contact your FirstEnergy Customer Support Representative if you have any questions. {%newline%}{%newline%}Sincerely,{%newline%}FirstEnergy Ohio Electric Utilities'
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.external3.body')
AND LocaleKey = 'en-US'


UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment Event - Future End Time Notice TEST ',
LongText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment Event - Future End Time Notice TEST ',
MediumText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment Event - Future End Time Notice TEST '
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.external3.subject')
AND LocaleKey = 'en-US'


UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'NA',
LongText = 'The following message is for test purposes only.{%newline%}{%newline%}An Emergency Curtailment Event for the Ohio Direct Load Control Program is being initiated today, {%notificationdate%}, at {%personalizedstarttime%} Eastern Daylight Time, by the FirstEnergy Ohio Electric Utilities based upon the determination by the PJM Interconnection. The FirstEnergy Ohio Electric Utilities will send out a notice when it is determined when this Emergency Curtailment Event will end. {%newline%}{%newline%}Sincerely,{%newline%}FirstEnergy Ohio Electric Utilities',
MediumText = 'The following message is for test purposes only.{%newline%}{%newline%}An Emergency Curtailment Event for the Ohio Direct Load Control Program is being initiated today, {%notificationdate%}, at {%personalizedstarttime%} Eastern Daylight Time, by the FirstEnergy Ohio Electric Utilities based upon the determination by the PJM Interconnection. The FirstEnergy Ohio Electric Utilities will send out a notice when it is determined when this Emergency Curtailment Event will end. {%newline%}{%newline%}Sincerely,{%newline%}FirstEnergy Ohio Electric Utilities'
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.external4.body')
AND LocaleKey = 'en-US'


UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment - Event Start Notice TEST ',
LongText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment - Event Start Notice TEST ',
MediumText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment - Event Start Notice TEST '
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.external4.subject')
AND LocaleKey = 'en-US'


UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'NA',
LongText = 'The following message is for test purposes only.{%newline%}{%newline%}The Ohio DLC Emergency Curtailment Event from today, {%enddate%}, has ended / will end at {%endtime%} Eastern Daylight Time. {%newline%}{%newline%}Sincerely,{%newline%}FirstEnergy Ohio Electric Utilities',
MediumText = 'The following message is for test purposes only.{%newline%}{%newline%}The Ohio DLC Emergency Curtailment Event from today, {%enddate%}, has ended / will end at {%endtime%} Eastern Daylight Time. {%newline%}{%newline%}Sincerely,{%newline%}FirstEnergy Ohio Electric Utilities'
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.external5.body')
AND LocaleKey = 'en-US'


UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'TEST Rider ELR - Emergency Curtailment Event - Event End Notice TEST ',
LongText = 'TEST Rider ELR - Emergency Curtailment Event - Event End Notice TEST ',
MediumText = 'TEST Rider ELR - Emergency Curtailment Event - Event End Notice TEST '
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.external5.subject')
AND LocaleKey = 'en-US'

UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'NA',
LongText = 'The following message is for test purposes only.{%newline%}{%newline%}Rider ELR - PJM Initiated Emergency Curtailment Event Start - Internal Notice with List of Customers. {%newline%} {%newline%}
Please note the following is an internal correspondence from FirstEnergy Ohio Electric Utilities that contains confidential customer information.  You are prohibited from forwarding or otherwise providing this information to anyone that is not a current employee of the FirstEnergy Ohio Electric Utilities or FirstEnergy Service Company in a shared service role.  Do not forward or otherwise provide this information to any FirstEnergy Solutions employees or anyone outside of the FirstEnergy Organization. {%newline%} {%newline%}If a Rider ELR customer is not included on this list, then they are not required to curtail to firm load levels at the emergency curtailment event start time noted herein.  If a Rider ELR customer received emergency curtailment event notification earlier today they need to curtail to firm load levels at the event start time included in the earlier notification. Rider ELR customers who have not yet been notified of an emergency curtailment event may be notified later in the day. {%newline%}{%newline%}The following Rider ELR Customers were just notified of an Emergency Curtailment Event initiated by PJM today, {%notificationdate%} starting at {%personalizedstarttime%} Eastern Daylight Time:{%newline%}',
MediumText = 'The following message is for test purposes only.{%newline%}{%newline%}Rider ELR - PJM Initiated Emergency Curtailment Event Start - Internal Notice with List of Customers.{%newline%} {%newline%}
Please note the following is an internal correspondence from FirstEnergy Ohio Electric Utilities that contains confidential customer information.  You are prohibited from forwarding or otherwise providing this information to anyone that is not a current employee of the FirstEnergy Ohio Electric Utilities or FirstEnergy Service Company in a shared service role.  Do not forward or otherwise provide this information to any FirstEnergy Solutions employees or anyone outside of the FirstEnergy Organization. {%newline%} {%newline%}If a Rider ELR customer is not included on this list, then they are not required to curtail to firm load levels at the emergency curtailment event start time noted herein.  If a Rider ELR customer received emergency curtailment event notification earlier today they need to curtail to firm load levels at the event start time included in the earlier notification. Rider ELR customers who have not yet been notified of an emergency curtailment event may be notified later in the day. {%newline%}{%newline%}The following Rider ELR Customers were just notified of an Emergency Curtailment Event initiated by PJM today, {%notificationdate%} starting at {%personalizedstarttime%} Eastern Daylight Time:{%newline%}'
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.internal1.body')
AND LocaleKey = 'en-US'

UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment - Event Start - Internal Notice with List of Customers TEST ',
LongText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment - Event Start - Internal Notice with List of Customers TEST ',
MediumText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment - Event Start - Internal Notice with List of Customers TEST '
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.internal1.subject')
AND LocaleKey = 'en-US'

UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'NA',
LongText = 'The following message is for test purposes only.{%newline%}{%newline%}Rider ELR - PJM Initiated Emergency Curtailment Event has Ended - Internal Notice with List of Customers. {%newline%}{%newline%}
Please note the following is an internal correspondence from FirstEnergy Ohio Electric Utilities that contains confidential customer information.  You are prohibited from forwarding or otherwise providing this information to anyone that is not a current employee of the FirstEnergy Ohio Electric Utilities or FirstEnergy Service Company in a shared service role.  Do not forward or otherwise provide this information to any FirstEnergy Solutions employees or anyone outside of the FirstEnergy organization. {%newline%}{%newline%}This emergency curtailment event notification only applies to customers listed herein.  Other Rider ELR customers (not listed herein) that are currently in an emergency curtailment event will have a different event termination time.  {%newline%} {%newline%}The following Rider ELR Customers were notified that the Emergency Curtailment Event for today, {%enddate%} has ended: {%newline%}',
MediumText = 'The following message is for test purposes only.{%newline%}{%newline%}Rider ELR - PJM Initiated Emergency Curtailment Event has Ended - Internal Notice with List of Customers. {%newline%}{%newline%}
Please note the following is an internal correspondence from FirstEnergy Ohio Electric Utilities that contains confidential customer information.  You are prohibited from forwarding or otherwise providing this information to anyone that is not a current employee of the FirstEnergy Ohio Electric Utilities or FirstEnergy Service Company in a shared service role.  Do not forward or otherwise provide this information to any FirstEnergy Solutions employees or anyone outside of the FirstEnergy organization. {%newline%}{%newline%}This emergency curtailment event notification only applies to customers listed herein.  Other Rider ELR customers (not listed herein) that are currently in an emergency curtailment event will have a different event termination time.  {%newline%} {%newline%}The following Rider ELR Customers were notified that the Emergency Curtailment Event for today, {%enddate%} has ended: {%newline%}'
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.internal2.body')
AND LocaleKey = 'en-US'

UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment - Event has Ended - Internal Notice with List of Customers TEST ',
LongText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment - Event has Ended - Internal Notice with List of Customers TEST ',
MediumText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment - Event has Ended - Internal Notice with List of Customers TEST '
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.internal2.subject')
AND LocaleKey = 'en-US'

UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'NA',
LongText = 'The following message is for test purposes only.{%newline%}{%newline%}Rider ELR - PJM Initiated Emergency Curtailment Event - Future End Time - Internal Notice with List of Customers. {%newline%}{%newline%}
Please note the following is an internal correspondence from FirstEnergy Ohio Electric Utilities that contains confidential customer information.  You are prohibited from forwarding or otherwise providing this information to anyone that is not a current employee of the FirstEnergy Ohio Electric Utilities or FirstEnergy Service Company in a shared service role.  Do not forward or otherwise provide this information to any FirstEnergy Solutions employees or anyone outside of the FirstEnergy organization.  {%newline%}{%newline%}This emergency curtailment event notification only applies to customers listed herein.  Other Rider ELR customers (not listed herein) that are currently in an emergency curtailment event will have a different event termination time.  The following Rider ELR Customers were notified that the Emergency Curtailment Event for today, {%enddate%} will end at {%endtime%} Eastern Daylight Time:{%newline%}',
MediumText = 'The following message is for test purposes only.{%newline%}{%newline%}Rider ELR - PJM Initiated Emergency Curtailment Event - Future End Time - Internal Notice with List of Customers.{%newline%}{%newline%}
Please note the following is an internal correspondence from FirstEnergy Ohio Electric Utilities that contains confidential customer information.  You are prohibited from forwarding or otherwise providing this information to anyone that is not a current employee of the FirstEnergy Ohio Electric Utilities or FirstEnergy Service Company in a shared service role.  Do not forward or otherwise provide this information to any FirstEnergy Solutions employees or anyone outside of the FirstEnergy organization.  {%newline%}{%newline%}This emergency curtailment event notification only applies to customers listed herein.  Other Rider ELR customers (not listed herein) that are currently in an emergency curtailment event will have a different event termination time.  The following Rider ELR Customers were notified that the Emergency Curtailment Event for today, {%enddate%} will end at {%endtime%} Eastern Daylight Time:{%newline%}'
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.internal3.body')
AND LocaleKey = 'en-US'

UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment Event - Future End Time - Internal Notice with List of Customers TEST ',
LongText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment Event - Future End Time - Internal Notice with List of Customers TEST ',
MediumText = 'TEST Rider ELR - PJM Initiated Emergency Curtailment Event - Future End Time - Internal Notice with List of Customers TEST '
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.internal3.subject')
AND LocaleKey = 'en-US'