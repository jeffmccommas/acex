USE InsightsMetaData

--- update teamname/registration mapping
UPDATE admin.ETLBulkConfiguration
SET XMLConfiguration = '<NotificationXmlConfiguration>
<TeamNameMapping>
<MapInfo key="1614211">2016-AK Steel</MapInfo>
<MapInfo key="1614210">2016-Alliance Castings Company</MapInfo>
<MapInfo key="1614201">2016-American Sand and Gravel</MapInfo>
<MapInfo key="1614213">2016-Ashta Chemicals</MapInfo>
<MapInfo key="1614207">2016-Bunting Bearings_Simcote</MapInfo>
<MapInfo key="1614198">2016-Charter Steel_Nasa Lewis Research</MapInfo>
<MapInfo key="1614208">2016-Concast</MapInfo>
<MapInfo key="1614205">2016-Dietrich Industries_Quaker City Castings_Vallourec Star</MapInfo>
<MapInfo key="1614204">2016-Falcon Foundry</MapInfo>
<MapInfo key="1614197">2016-Material Science Corporation Walbridge</MapInfo>
<MapInfo key="1614200">2016-Milliron Industries</MapInfo>
<MapInfo key="1614194">2016-North Star Bluescope Steel_Linde_Worthington Industries</MapInfo>
<MapInfo key="1614203">2016-Nucor Steel Marion</MapInfo>
<MapInfo key="1614202">2016-Ohio Star Forge_Knapp Foundry</MapInfo>
<MapInfo key="1614214">2016-Praxair</MapInfo>
<MapInfo key="1614206">2016-Reserve Alloys</MapInfo>
<MapInfo key="1614209">2016-US Gypsum Co</MapInfo>
<MapInfo key="1614199">2016-Viking Forge Corp</MapInfo>
<MapInfo key="1511031">ELR-120</MapInfo>
<MapInfo key="1511030">ELR-60</MapInfo>
<MapInfo key="1511028">ELR-30</MapInfo>
</TeamNameMapping>
</NotificationXmlConfiguration>'
WHERE ClientID = 233 AND Environment IN ('prod')
