USE InsightsMetaData

--- update teamname/registration mapping
UPDATE admin.ETLBulkConfiguration
SET XMLConfiguration = '<NotificationXmlConfiguration>
	<TeamNameMapping>
		<MapInfo key="1614211">2016-AK Steel</MapInfo>
		<MapInfo key="1614210">2016-Alliance Castings Company</MapInfo>
		<MapInfo key="1614201">2016-American Sand and Gravel</MapInfo>
		<MapInfo key="1614213">2016-Ashta Chemicals</MapInfo>
		<MapInfo key="1614207">2016-Bunting Bearings_Simcote</MapInfo>
		<MapInfo key="1614198">2016-Charter Steel_Nasa Lewis Research</MapInfo>
		<MapInfo key="1614208">2016-Concast</MapInfo>
		<MapInfo key="1614205">2016-Dietrich Industries_Quaker City Castings_Vallourec Star</MapInfo>
		<MapInfo key="1614204">2016-Falcon Foundry</MapInfo>
		<MapInfo key="1614197">2016-Material Science Corporation Walbridge</MapInfo>
		<MapInfo key="1614200">2016-Milliron Industries</MapInfo>
		<MapInfo key="1614194">2016-North Star Bluescope Steel_Linde_Worthington Industries</MapInfo>
		<MapInfo key="1614203">2016-Nucor Steel Marion</MapInfo>
		<MapInfo key="1614202">2016-Ohio Star Forge_Knapp Foundry</MapInfo>
		<MapInfo key="1614214">2016-Praxair</MapInfo>
		<MapInfo key="1614206">2016-Reserve Alloys</MapInfo>
		<MapInfo key="1614209">2016-US Gypsum Co</MapInfo>
		<MapInfo key="1614199">2016-Viking Forge Corp</MapInfo>
		<MapInfo key="1511032">2016-AK Steel</MapInfo>
		<MapInfo key="1511031">2016-Bunting Bearings_Simcote</MapInfo>
		<MapInfo key="1511030">2016-Concast</MapInfo>
		<MapInfo key="1511029">2016-North Star Bluescope Steel_Linde_Worthington Industries</MapInfo>
		<MapInfo key="1511028">2016-Material Science Corporation Walbridge</MapInfo>
		<MapInfo key="1511026">2016-Viking Forge Corp</MapInfo>
    <MapInfo key="1511032">2016-AK Steel</MapInfo>
    <MapInfo key="1511031">2016-Bunting Bearings_Simcote</MapInfo>
    <MapInfo key="1511030">2016-American Sand and Gravel</MapInfo>
    <MapInfo key="1511029">2016-Ohio Star Forge_Knapp Foundry</MapInfo>
    <MapInfo key="1511028">2016-Praxair</MapInfo>
    <MapInfo key="1511026">2016-Viking Forge Corp</MapInfo>
	</TeamNameMapping>
</NotificationXmlConfiguration>'
WHERE ClientID = 233 AND Environment IN ('prod')

--- update internal list teamname
update cm.ClientConfiguration
set value = '2016-RiderELRInternalList'
WHERE CategoryKey = 'notification'
AND ConfigurationKey = 'webserviceclient.nuance.internalteamnames'
and clientid = 233

--- update PJM password
UPDATE cm.ClientConfiguration 
SET Value = 'Ac!ara2016'
WHERE CategoryKey = 'notification' AND ClientID = 233 AND ConfigurationKey = 'webserviceclient.elrs.password'

--- update sandbox username
UPDATE cm.ClientConfiguration 
SET Value = 'aclara_ohio'  ---aclara_ohio2
WHERE CategoryKey = 'notification' AND ClientID = 233 AND ConfigurationKey = 'webserviceclient.elrs.username' AND EnvironmentKey = 'prod'

--- update sandbox endpoint
UPDATE cm.ClientConfiguration 
SET Value = 'https://lrstrain.pjm.com/ews'  ---https://lrs.pjm.com/ews
WHERE CategoryKey = 'notification' AND ClientID = 233 AND ConfigurationKey = 'webserviceclient.elrs.endpoint' AND EnvironmentKey = 'prod'

--- update content
UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'TEST: Rider ELR - PJM Initiated Emergency Curtailment - Event Start',
LongText = 'TEST TEST Rider ELR - Emergency Curtailment Event',
MediumText = 'TEST: Rider ELR - PJM Initiated Emergency Curtailment - Event Start'
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.external1.subject')
AND LocaleKey = 'en-US'

UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'TEST: Rider ELR - PJM Initiated Emergency Curtailment - Event has Ended',
LongText = 'TEST: Rider ELR - Emergency Curtailment Event - End of Event',
MediumText = 'TEST: Rider ELR - PJM Initiated Emergency Curtailment - Event has Ended'
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.external2.subject')
AND LocaleKey = 'en-US'

UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'TEST: Rider ELR - PJM Initiated Emergency Curtailment Event - Future End Time Notice',
LongText = 'TEST: Rider ELR - PJM Initiated Emergency Curtailment Event - Future End Time Notice',
MediumText = 'TEST: Rider ELR - PJM Initiated Emergency Curtailment Event - Future End Time Notice'
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.external3.subject')
AND LocaleKey = 'en-US'


UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'TEST: Ohio DLC - PJM Initiated Emergency Curtailment - Event Start Notice',
LongText = 'TEST: Ohio DLC - PJM Initiated Emergency Curtailment - Event Start Notice',
MediumText = 'TEST: Ohio DLC - PJM Initiated Emergency Curtailment - Event Start Notice'
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.external4.subject')
AND LocaleKey = 'en-US'


UPDATE cm.ClientTextContentLocaleContent 
SET ShortText = 'TEST: Ohio DLC - Emergency Curtailment Event - Event End Notice',
LongText = 'TEST: Ohio DLC - Emergency Curtailment Event - Event End Notice',
MediumText = 'TEST: Ohio DLC - Emergency Curtailment Event - Event End Notice'
WHERE ClientTextContentID IN 
(SELECT ClientTextContentID 
FROM cm.ClientTextContent WHERE TextContentKey = 'message.loadmanagement.external2.subject')
AND LocaleKey = 'en-US'
