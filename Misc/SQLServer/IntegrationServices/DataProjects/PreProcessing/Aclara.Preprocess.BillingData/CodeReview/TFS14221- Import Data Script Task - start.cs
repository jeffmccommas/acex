﻿#region Help:  Introduction to the script task
/* The Script Task allows you to perform virtually any operation that can be accomplished in
 * a .Net application within the context of an Integration Services control flow.
 *
 * Expand the other regions which have "Help" prefixes for examples of specific ways to use
 * Integration Services features within this script task. */
#endregion


#region Namespaces
using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.SqlServer.Dts.Runtime;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
#endregion

namespace ST_f4a455204e0c4b99b266b9cff44ed6e0
{
    /// <summary>
    /// ScriptMain is the entry point class of the script.  Do not change the name, attributes,
    /// or parent of this class.
    /// </summary>
    [Microsoft.SqlServer.Dts.Tasks.ScriptTask.SSISScriptTaskEntryPointAttribute]
    public class ScriptMain : Microsoft.SqlServer.Dts.Tasks.ScriptTask.VSTARTScriptObjectModelBase
    {
        #region Help:  Using Integration Services variables and parameters in a script
        /* To use a variable in this script, first ensure that the variable has been added to
         * either the list contained in the ReadOnlyVariables property or the list contained in
         * the ReadWriteVariables property of this script task, according to whether or not your
         * code needs to write to the variable.  To add the variable, save this script, close this instance of
         * Visual Studio, and update the ReadOnlyVariables and
         * ReadWriteVariables properties in the Script Transformation Editor window.
         * To use a parameter in this script, follow the same steps. Parameters are always read-only.
         *
         * Example of reading from a variable:
         *  DateTime startTime = (DateTime) Dts.Variables["System::StartTime"].Value;
         *
         * Example of writing to a variable:
         *  Dts.Variables["User::myStringVariable"].Value = "new value";
         *
         * Example of reading from a package parameter:
         *  int batchId = (int) Dts.Variables["$Package::batchId"].Value;
         *
         * Example of reading from a project parameter:
         *  int batchId = (int) Dts.Variables["$Project::batchId"].Value;
         *
         * Example of reading from a sensitive project parameter:
         *  int batchId = (int) Dts.Variables["$Project::batchId"].GetSensitiveValue();
         * */

        #endregion

        #region Help:  Firing Integration Services events from a script
        /* This script task can fire events for logging purposes.
         *
         * Example of firing an error event:
         *  Dts.Events.FireError(18, "Process Values", "Bad value", "", 0);
         *
         * Example of firing an information event:
         *  Dts.Events.FireInformation(3, "Process Values", "Processing has started", "", 0, ref fireAgain)
         *
         * Example of firing a warning event:
         *  Dts.Events.FireWarning(14, "Process Values", "No values received for input", "", 0);
         * */
        #endregion

        #region Help:  Using Integration Services connection managers in a script
        /* Some types of connection managers can be used in this script task.  See the topic
         * "Working with Connection Managers Programatically" for details.
         *
         * Example of using an ADO.Net connection manager:
         *  object rawConnection = Dts.Connections["Sales DB"].AcquireConnection(Dts.Transaction);
         *  SqlConnection myADONETConnection = (SqlConnection)rawConnection;
         *  //Use the connection in some code here, then release the connection
         *  Dts.Connections["Sales DB"].ReleaseConnection(rawConnection);
         *
         * Example of using a File connection manager
         *  object rawConnection = Dts.Connections["Prices.zip"].AcquireConnection(Dts.Transaction);
         *  string filePath = (string)rawConnection;
         *  //Use the connection in some code here, then release the connection
         *  Dts.Connections["Prices.zip"].ReleaseConnection(rawConnection);
         * */
        #endregion


        /// <summary>
        /// This method is called when this script task executes in the control flow.
        /// Before returning from this method, set the value of Dts.TaskResult to indicate success or failure.
        /// To open Help, press F1.
        /// </summary>
        public void Main()
        {
            var sourceFile = Dts.Variables["FullPath"].Value + Dts.Variables["Filename"].Value.ToString();
            var logFile = sourceFile.Replace(Dts.Variables["FileExtension"].Value.ToString(), ".log");

            try
            {
                var path = Dts.Variables["FullPath"].Value.ToString();
                var filename = Dts.Variables["Filename"].Value.ToString();
                var separator = Dts.Variables["FieldSeparator"].Value.ToString().Replace("\\t", "\t");
                var dbStructureId = Dts.Variables["DbStructureId"].Value.ToString();

                bool noHeaderPresent;
                bool.TryParse(Dts.Variables["NoHeader"].Value.ToString(), out noHeaderPresent);

                var trackingId = GetTrackingId(filename);

                using (var conn = Dts.Connections["InsightsBulk"].AcquireConnection(Dts.Transaction) as SqlConnection)
                {
                    if (conn != null)
                    {
                        using (var file = new MyFileDataReader(path + filename, separator.ToCharArray(), noHeaderPresent))
                        {
                            if (file.NoData)
                            {
                                using (var writer = new StreamWriter(logFile, true))
                                {
                                    writer.WriteLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) + " : **ERROR** - No Data Found to Process");
                                }

                                Dts.TaskResult = (int)ScriptResults.Failure;
                                return;
                            }

                            using (var writer = new StreamWriter(logFile, true))
                            {
                                writer.WriteLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) + " : FieldCount = " + file.FieldCount + " : RawTable = " + "RawBillTemp_" + dbStructureId + " : No Header = " + noHeaderPresent);
                            }

                            var copy = new SqlBulkCopy(conn)
                            {
                                DestinationTableName = "RawBillTemp_" + dbStructureId,
                                BulkCopyTimeout = 6000
                            };
                            copy.WriteToServer(file);
                        }
                    }
                }


                using (var writer = new StreamWriter(logFile, true))
                {
                    writer.WriteLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Bulk copy to RawBillTemp_" + dbStructureId + " succeeded!");
                }

                Dts.Variables["TrackingId"].Value = trackingId;
                Dts.TaskResult = (int)ScriptResults.Success;
            }
            catch (Exception ex)
            {
                using (var writer = new StreamWriter(logFile, true))
                {
                    writer.WriteLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) + " : **ERROR** Bulk copy to RawBillTemp!" + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace);
                }

                Dts.TaskResult = (int)ScriptResults.Failure;
            }

        }

        /// <summary>
        /// Get the tracking Id
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private static string GetTrackingId(string filename)
        {
            var match = false;
            var trackingId = DateTime.Now.ToString("yyyyMMddHHmmss");

            filename = filename.Substring(0, filename.LastIndexOf(".", StringComparison.Ordinal));

            var arr = filename.Split('_');

            foreach (var item in arr)
            {
                if (match)
                {
                    trackingId += "_" + item;
                }
                else if (item.Length >= 12 && item.Length <= 14)
                {
                    DateTime d;
                    if (DateTime.TryParseExact(item, new[] { "yyyyMMddHHmmss", "yyyyMMddHHmm" }, null, DateTimeStyles.None, out d))
                    {
                        match = true;
                        trackingId = item;
                    }
                }
            }

            return trackingId;
        }

        #region ScriptResults declaration
        /// <summary>
        /// This enum provides a convenient shorthand within the scope of this class for setting the
        /// result of the script.
        ///
        /// This code was generated automatically.
        /// </summary>
        private enum ScriptResults
        {
            Success = DTSExecResult.Success,
            Failure = DTSExecResult.Failure
        };
        #endregion

    }

    /// <summary>
    /// MyFileDataReader class
    /// </summary>
    public class MyFileDataReader : IDataReader
    {
        private StreamReader Stream { get; }

        private string[] _values;

        private readonly Queue<string> _bufferedLines;

        public int RowIndex { get; set; }

        public int FieldCount { get; set; }

        public char[] FieldSeparator { get; protected set; }

        public bool NoData { get; set; }

        /// <summary>
        ///  Constructor
        /// </summary>
        /// <param name="fileName">The name of the file being processed.</param>
        /// <param name="fieldSeparator">The seperator of the fields in the file.</param>
        /// <param name="noHeaderPresent">True, if there is no header in the file.</param>
        public MyFileDataReader(string fileName, char[] fieldSeparator, bool noHeaderPresent)
        {
            // Initialize variables
            FieldSeparator = fieldSeparator;
            _bufferedLines = new Queue<string>();
            NoData = false;

            // Create the file reader
            Stream = new StreamReader(fileName);

            // Read in the first line.
            var readLine = Stream.ReadLine();

            // If there is a first line determine how many columns (field count) are present.
            if (readLine != null)
            {
                // If no header is present store it because it is data.
                if (noHeaderPresent)
                {
                    FieldCount = readLine.Split(FieldSeparator, StringSplitOptions.None).Length;
                    _bufferedLines.Enqueue(readLine);
                }
                else
                {
                    FieldCount = readLine.Split(FieldSeparator, StringSplitOptions.RemoveEmptyEntries).Length;
                }
            }

            // If no columns found and the file is empty report no data found.
            if(FieldCount <= 0 || Stream.Peek() < 0 || Stream.EndOfStream)
            {
                NoData = true;
            }
        }

        /// <summary>
        /// Read from the file.
        /// </summary>
        /// <returns>True if there are more rows; otherwise, False</returns>
        public bool Read()
        {
            // Get the first record.
            string currentRecord;

            // If file has no header use the record read to determine the number of columns.
            if (_bufferedLines.Count > 0)
            {
                currentRecord = _bufferedLines.Dequeue();
                _bufferedLines.Clear();
            }
            else
            {
                currentRecord = Stream.ReadLine();
            }

            // If the current record is null the end of the file has been reached.
            var eof = currentRecord == null;

            if (eof)
            {
                return false;
            }

            // Get the values from the first record read.
            _values = null;
            _values = currentRecord.Split(FieldSeparator, StringSplitOptions.None);

            // Bump up the row index.
            RowIndex++;

            return true;
        }

        /// <summary>
        /// Read a field value from the file.
        /// </summary>
        /// <param name="columnIndex">The column index</param>
        /// <returns>The value read</returns>
        public object GetValue(int columnIndex)
        {
            try
            {
                return _values[columnIndex].Trim();
            }
            catch (Exception eX)
            {
                throw new Exception("Unable to read data at record " + (RowIndex + 1) + " column " + (columnIndex + 1) + Environment.NewLine
                    + "The values found are = " + string.Join(FieldSeparator[0].ToString(), _values) + Environment.NewLine +
                    eX.Message);
            }
        }

        /// <summary>
        /// Dispose of the file stream.
        /// </summary>
        public void Dispose()
        {
            if (Stream == null)
            {
                return;
            }

            Stream.Close();
            Stream.Dispose();
        }

        #region Not Implemented Methods

        public int Depth { get { throw new NotImplementedException(); } }
        public int RecordsAffected { get { throw new NotImplementedException(); } }
        public bool IsClosed { get { throw new NotImplementedException(); } }
        public object this[int i] { get { throw new NotImplementedException(); } }
        public object this[string name] { get { throw new NotImplementedException(); } }

        public int GetInt32(int i) { throw new NotImplementedException(); }
        public int GetOrdinal(string name) { throw new NotImplementedException(); }
        public int GetValues(object[] values) { throw new NotImplementedException(); }

        public DataTable GetSchemaTable() { throw new NotImplementedException(); }
        public IDataReader GetData(int i) { throw new NotImplementedException(); }
        public DateTime GetDateTime(int i) { throw new NotImplementedException(); }
        public Type GetFieldType(int i) { throw new NotImplementedException(); }
        public Guid GetGuid(int i) { throw new NotImplementedException(); }

        public byte GetByte(int i) { throw new NotImplementedException(); }
        public char GetChar(int i) { throw new NotImplementedException(); }
        public long GetInt64(int i) { throw new NotImplementedException(); }
        public float GetFloat(int i) { throw new NotImplementedException(); }
        public short GetInt16(int i) { throw new NotImplementedException(); }
        public double GetDouble(int i) { throw new NotImplementedException(); }
        public decimal GetDecimal(int i) { throw new NotImplementedException(); }

        public string GetName(int i) { throw new NotImplementedException(); }
        public string GetString(int i) { throw new NotImplementedException(); }
        public string GetDataTypeName(int i) { throw new NotImplementedException(); }

        public bool IsDBNull(int i) { throw new NotImplementedException(); }
        public bool GetBoolean(int i) { throw new NotImplementedException(); }
        public bool NextResult() { throw new NotImplementedException(); }

        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length) { throw new NotImplementedException(); }
        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length) { throw new NotImplementedException(); }

        public void Close() { throw new NotImplementedException(); }

        #endregion Not Implemented Methods
    }
}