﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace ScgCsv
{
    class Program
    {
        static bool quiet = false;
        static int client = 87;
        static void Main(string[] args)
        {
            try
            {
                // Job should always run
                // If params are bad it defaults to client 87 and writes to console
                if (args != null)
                {
                    if (args.Length > 0)
                    {
                        if (args.Length ==1)
                        {
                            if (args[0] == "Q")
                            {
                                quiet = true;
                                //default to 87
                                client = 87;
                            }
                        }
                        if (args.Length == 2)
                        {
                            if (args[0] == "Q" || args[1] == "Q")
                            {
                                quiet = true;
                            }
                            else
                            {
                                Console.WriteLine("messages will be written to console app window");
                            }
                            if (int.TryParse(args[0], out client) != true )
                            {
                                if (int.TryParse(args[1], out client) != true)
                                {
                                    //default to 87
                                    client = 87;
                                    if (!quiet)
                                    {
                                        Console.WriteLine("Defaulting to client 87");
                                    }
                                }
                            }
                        }
                    }

                }
                SqlConnection jobConnection;
                SqlCommand jobCommand;
                SqlParameter jobReturnValue;
                SqlParameter jobParameter;
                int jobResult;
                string servername = ConfigurationSettings.AppSettings["ServerName"];
                jobConnection = new SqlConnection(@"Data Source=" + servername + ";Initial Catalog=msdb;Integrated Security=SSPI");
                jobCommand = new SqlCommand("sp_start_job", jobConnection);
                jobCommand.CommandType = CommandType.StoredProcedure;

                jobReturnValue = new SqlParameter("@RETURN_VALUE", SqlDbType.Int);
                jobReturnValue.Direction = ParameterDirection.ReturnValue;
                jobCommand.Parameters.Add(jobReturnValue);

                jobParameter = new SqlParameter("@job_name", SqlDbType.VarChar);
                jobParameter.Direction = ParameterDirection.Input;
                jobCommand.Parameters.Add(jobParameter);
                
                jobParameter.Value = "RunConvertSoCalCsvToXml_" + client.ToString();

                jobConnection.Open();
                jobCommand.ExecuteNonQuery();
                jobResult = (Int32)jobCommand.Parameters["@RETURN_VALUE"].Value;
                jobConnection.Close();
                if (!quiet)
                {
                    switch (jobResult)
                    {
                        case 0:
                            Console.WriteLine("SQL Server Agent job, RunConvertSoCalCsvToXml, started successfully.");
                            break;
                        default:
                            Console.WriteLine("SQL Server Agent job, RunConvertSoCalCsvToXml, failed to start.");
                            break;
                    }
                }
            }
             
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (!quiet)
            {
                Console.WriteLine("Done");
               
            }
        }
    }
}
