﻿USE [msdb]
GO

/****** Object:  Job [PaaS_ImportEventsToBulk_224]    Script Date: 7/20/2017 11:18:37 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'e83ca034-f642-46dc-b35d-f309c966b0eb', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ImportEventsToBulk_224]    Script Date: 7/20/2017 11:18:37 AM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [CE_UserApp]    Script Date: 7/20/2017 11:18:37 AM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'CE_UserApp' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'CE_UserApp'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'PaaS_ImportEventsToBulk_224',
        @enabled=0,
        @notify_level_eventlog=0,
        @notify_level_email=2,
        @notify_level_netsend=0,
        @notify_level_page=0,
        @delete_level=0,
        @description=N'Imports Events files to holding',
        @category_name=N'CE_UserApp',
        @owner_login_name=N'sa',
        @notify_email_operator_name=N'ACE BI PROD', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [PaaS_ImportEventsToBulk_224]    Script Date: 7/20/2017 11:18:37 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'PaaS_ImportEventsToBulk_224',
        @step_id=1,
        @cmdexec_success_code=0,
        @on_success_action=1,
        @on_success_step_id=0,
        @on_fail_action=2,
        @on_fail_step_id=0,
        @retry_attempts=0,
        @retry_interval=0,
        @os_run_priority=0, @subsystem=N'SSIS',
        @command=N'/ISSERVER "\"\SSISDB\BI\ImportEvents\Main Events Controller.dtsx\"" /SERVER "\"AclAceSql91PERF\bi1\"" /Par "\"$Project::DefaultClient(Int32)\"";224 /Par "\"$Project::ConnectionStringDW\"";"\"Data Source=AclAceInsSQLPerf.database.windows.net;User ID=AclaraCEAdmin@AclAceInsSQLPerf;Password=Acl@r@282;Initial Catalog=InsightsDW_224;Provider=SQLNCLI11.1;Persist Security Info=True;Encrypt=true;\"" /Par "\"$Project::ConnectionStringMetaData\"";"\"Data Source=AclAceInsSQLPerf.database.windows.net;User ID=AclaraCEAdmin@AclAceInsSQLPerf;Password=Acl@r@282;Initial Catalog=InsightsMetaData;Provider=SQLNCLI11.1;Persist Security Info=True;Encrypt=true;\""  /Par "\"$Project::LogFilePath\"";"\"\\AclAceFil91PERF\PaaS_CloudFileDrop\224\Preprocess\BI\Events\\"" /Par LogFile;"\"logfile.txt\"" /Par "\"$Project::CM.InsightsDW.InitialCatalog\"";"\"InsightsDW_224\"" /Par "\"$Project::CM.InsightsDW.Password\"";"\"Acl@r@282\"" /Par "\"$Project::CM.InsightsDW.ServerName\"";"\"AclAceInsSQLPerf.database.windows.net\"" /Par "\"$Project::CM.InsightsDW.UserName\"";"\"AclaraCEAdmin@AclAceInsSQLPerf\"" /Par "\"$Project::CM.InsightsMetaData.InitialCatalog\"";InsightsMetaData /Par "\"$Project::CM.InsightsMetaData.Password\"";"\"Acl@r@282\"" /Par "\"$Project::CM.InsightsMetaData.ServerName\"";"\"AclAceInsSQLPerf.database.windows.net\"" /Par "\"$Project::CM.InsightsMetaData.UserName\"";"\"AclaraCEAdmin@AclAceInsSQLPerf\"" /Par "\"$ServerOption::LOGGING_LEVEL(Int16)\"";2 /Par "\"$ServerOption::SYNCHRONIZED(Boolean)\"";True /CALLERINFO SQLAGENT /REPORTING E',
        @database_name=N'master',
        @flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Hourly',
        @enabled=1,
        @freq_type=4,
        @freq_interval=1,
        @freq_subday_type=8,
        @freq_subday_interval=1,
        @freq_relative_interval=0,
        @freq_recurrence_factor=0,
        @active_start_date=20150929,
        @active_end_date=99991231,
        @active_start_time=0,
        @active_end_time=235959,
        @schedule_uid=N'8c235599-00ea-41e5-8f65-294a2b0ecb95'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

