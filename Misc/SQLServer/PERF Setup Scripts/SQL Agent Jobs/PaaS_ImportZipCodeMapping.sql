﻿USE [msdb]
GO

/****** Object:  Job [PaaS_ImportZipCodeMapping]    Script Date: 7/20/2017 11:30:31 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'686e51cc-1775-4f93-a8f5-9d707acea27d', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ImportZipCodeMapping]    Script Date: 7/20/2017 11:30:31 AM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [CE_UserApp]    Script Date: 7/20/2017 11:30:31 AM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'CE_UserApp' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'CE_UserApp'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'PaaS_ImportZipCodeMapping',
        @enabled=0,
        @notify_level_eventlog=0,
        @notify_level_email=2,
        @notify_level_netsend=0,
        @notify_level_page=0,
        @delete_level=0,
        @description=N'Imports set of zipcode data from CE hubstatic db on DEV, outputs to file and then imports to ACE',
        @category_name=N'CE_UserApp',
        @owner_login_name=N'sa',
        @notify_email_operator_name=N'ACE BI PROD', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Export ZipCodeMapping File]    Script Date: 7/20/2017 11:30:31 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Export ZipCodeMapping File',
        @step_id=1,
        @cmdexec_success_code=0,
        @on_success_action=3,
        @on_success_step_id=0,
        @on_fail_action=2,
        @on_fail_step_id=0,
        @retry_attempts=0,
        @retry_interval=0,
        @os_run_priority=0, @subsystem=N'SSIS',
        @command=N'/ISSERVER "\"\SSISDB\BI\ImportWeather\ExportZipCodeMappingFiletoACE.dtsx\"" /SERVER "\"AclAceSql91PERF\bi1\"" /Par "\"$Project::InsightsMetaDataConn\"";"\"Data Source=AclAceInsSQLPerf.database.windows.net;User ID=AclaraCEAdmin@AclAceInsSQLPerf;Password=Acl@r@282;Initial Catalog=InsightsMetaData;Provider=SQLNCLI11.1;Persist Security Info=True;Encrypt=true;\"" /Par HubstaticConnString;"\"Data Source=SWEPSQLdev1\EP1;User ID=CMSUser;Password=nex61nex;Initial Catalog=Hubstatic;Provider=SQLNCLI11.1;Persist Security Info=False;Auto Translate=False;\"" /Par "\"$ServerOption::LOGGING_LEVEL(Int16)\"";2 /Par "\"$ServerOption::SYNCHRONIZED(Boolean)\"";True /CALLERINFO SQLAGENT /REPORTING E',
        @database_name=N'master',
        @flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Import ZipCodeMapping File]    Script Date: 7/20/2017 11:30:31 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Import ZipCodeMapping File',
        @step_id=2,
        @cmdexec_success_code=0,
        @on_success_action=1,
        @on_success_step_id=0,
        @on_fail_action=2,
        @on_fail_step_id=0,
        @retry_attempts=0,
        @retry_interval=0,
        @os_run_priority=0, @subsystem=N'SSIS',
        @command=N'/ISSERVER "\"\SSISDB\BI\ImportWeather\ImportZipCodeMapping.dtsx\"" /SERVER "\"AclAceSql91PERF\bi1\"" /Par "\"$Project::InsightsMetaDataConn\"";"\"Data Source=AclAceInsSQLPerf.database.windows.net;User ID=AclaraCEAdmin@AclAceInsSQLPerf;Password=Acl@r@282;Initial Catalog=InsightsMetaData;Provider=SQLNCLI11.1;Persist Security Info=True;Encrypt=true;\"" /Par "\"$ServerOption::LOGGING_LEVEL(Int16)\"";2 /Par "\"$ServerOption::SYNCHRONIZED(Boolean)\"";True /CALLERINFO SQLAGENT /REPORTING E',
        @database_name=N'master',
        @flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Weekly',
        @enabled=1,
        @freq_type=8,
        @freq_interval=1,
        @freq_subday_type=1,
        @freq_subday_interval=0,
        @freq_relative_interval=0,
        @freq_recurrence_factor=1,
        @active_start_date=20150918,
        @active_end_date=99991231,
        @active_start_time=1000,
        @active_end_time=235959,
        @schedule_uid=N'1fcfe823-4073-4e1f-964d-6edb508fdfc1'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

