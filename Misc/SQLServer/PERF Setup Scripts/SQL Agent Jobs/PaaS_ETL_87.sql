﻿USE [msdb]
GO

/****** Object:  Job [PaaS_ETL_87]    Script Date: 7/20/2017 11:06:02 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'c0761b3d-9045-40d7-8d38-b92be63948ae', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ETL_87]    Script Date: 7/20/2017 11:06:02 AM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [CE_UserApp]    Script Date: 7/20/2017 11:06:02 AM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'CE_UserApp' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'CE_UserApp'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'PaaS_ETL_87',
        @enabled=0,
        @notify_level_eventlog=0,
        @notify_level_email=2,
        @notify_level_netsend=0,
        @notify_level_page=0,
        @delete_level=0,
        @description=N'No description available.',
        @category_name=N'CE_UserApp',
        @owner_login_name=N'sa',
        @notify_email_operator_name=N'ACE BI PROD', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [ExecutePackage]    Script Date: 7/20/2017 11:06:02 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'ExecutePackage',
        @step_id=1,
        @cmdexec_success_code=0,
        @on_success_action=1,
        @on_success_step_id=0,
        @on_fail_action=2,
        @on_fail_step_id=0,
        @retry_attempts=0,
        @retry_interval=0,
        @os_run_priority=0, @subsystem=N'SSIS',
        @command=N'/ISSERVER "\"\SSISDB\BI\InsightsDW\Load_Warehouse.dtsx\"" /SERVER "\"AclAceSql91PERF\bi1\"" /Par "\"$Project::ClientID(Int16)\"";87 /Par "\"$Project::DatabaseServerNameAPI\"";"\"AclAceInsSQLPerf.database.windows.Net\"" /Par "\"$Project::CM.API.ConnectionString\"";"\"Data Source=AclAceInsSQLPerf.database.windows.Net;Initial Catalog=Insights;User ID=AclaraCEAdmin@AclAceInsSQLPerf;Password=Acl@r@282;Persist Security Info=True;Encrypt=true;\"" /Par "\"$Project::CM.API.InitialCatalog\"";Insights /Par "\"$Project::CM.API.Password\"";"\"Acl@r@282\"" /Par "\"$Project::CM.API.UserName\"";"\"AclaraCEAdmin@AclAceInsSQLPerf\"" /Par "\"$Project::DatabaseServerNameBI\"";"\"AclAceInsSQLPerf.database.windows.Net\"" /Par "\"$Project::CM.DisAggDW.ConnectionString\"";"\"Data Source=AclAceInsSQLPerf.database.windows.Net;Initial Catalog=InsightsDW_Test;User ID=AclaraCEAdmin@AclAceInsSQLPerf;Password=Acl@r@282;Persist Security Info=True;Encrypt=true;\"" /Par "\"$Project::CM.DisAggDW.InitialCatalog\"";"\"InsightsDW_Test\"" /Par "\"$Project::CM.DisAggDW.Password\"";"\"Acl@r@282\"" /Par "\"$Project::CM.DisAggDW.UserName\"";"\"AclaraCEAdmin@AclAceInsSQLPerf\"" /Par "\"$Project::CM.InsightsDW.ConnectionString\"";"\"Data Source=AclAceInsSQLPerf.database.windows.Net;Initial Catalog=InsightsDW_Test;User ID=AclaraCEAdmin@AclAceInsSQLPerf;Password=Acl@r@282;Persist Security Info=True;Encrypt=true;\"" /Par "\"$Project::CM.InsightsDW.InitialCatalog\"";"\"InsightsDW_Test\"" /Par "\"$Project::CM.InsightsDW.Password\"";"\"Acl@r@282\"" /Par "\"$Project::CM.Metadata.ConnectionString\"";"\"Data Source=AclAceInsSQLPerf.database.windows.Net;Initial Catalog=InsightsMetaData;User ID=AclaraCEAdmin@AclAceInsSQLPerf;Password=Acl@r@282;Persist Security Info=True;Encrypt=true;Provider=SQLOLEDB.1;\"" /Par "\"$Project::CM.Metadata.Password\"";"\"Acl@r@282\"" /Par "\"$Project::CM.Metadata.UserName\"";"\"AclaraCEAdmin@AclAceInsSQLPerf\"" /Par "\"$Project::CM.SourceData.ConnectionString\"";"\"Data Source=AclAceInsSQLPerf.database.windows.Net;Initial Catalog=InsightsDW_Test;User ID=AclaraCEAdmin@AclAceInsSQLPerf;Password=Acl@r@282;Persist Security Info=True;Encrypt=true;Provider=SQLOLEDB.1;\"" /Par "\"$Project::CM.SourceData.InitialCatalog\"";"\"InsightsDW_Test\"" /Par "\"$Project::CM.SourceData.Password\"";"\"Acl@r@282\"" /Par "\"$Project::CM.SourceData.UserName\"";"\"AclaraCEAdmin@AclAceInsSQLPerf\"" /Par "\"$Project::CM.Warehouse.ConnectionString\"";"\"Data Source=AclAceInsSQLPerf.database.windows.Net;Initial Catalog=InsightsDW_Test;User ID=AclaraCEAdmin@AclAceInsSQLPerf;Password=Acl@r@282;Persist Security Info=True;Encrypt=true;Provider=SQLOLEDB.1;\"" /Par "\"$Project::CM.Warehouse.InitialCatalog\"";"\"InsightsDW_Test\"" /Par "\"$Project::CM.Warehouse.Password\"";"\"Acl@r@282\"" /Par "\"$Project::CM.Warehouse.UserName\"";"\"AclaraCEAdmin@AclAceInsSQLPerf\"" /Par "\"$ServerOption::LOGGING_LEVEL(Int16)\"";2 /Par "\"$ServerOption::SYNCHRONIZED(Boolean)\"";True /CALLERINFO SQLAGENT /REPORTING E',
        @database_name=N'master',
        @flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily 9 pm',
        @enabled=1,
        @freq_type=4,
        @freq_interval=1,
        @freq_subday_type=1,
        @freq_subday_interval=0,
        @freq_relative_interval=0,
        @freq_recurrence_factor=0,
        @active_start_date=20141107,
        @active_end_date=99991231,
        @active_start_time=210000,
        @active_end_time=235959,
        @schedule_uid=N'2a6abff1-29fd-4bd1-8600-4cd1cf242568'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

