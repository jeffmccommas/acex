﻿USE [msdb]
GO

/****** Object:  Job [PaaS_ConvertActionsToXml_174]    Script Date: 7/20/2017 9:53:14 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'dfca7f90-3375-4fc8-bd20-5c6655387a66', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ConvertBillDataToXml_101]    Script Date: 7/20/2017 9:53:29 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'8e2121cf-f694-4dbb-9a57-bd56c255c4c9', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ConvertBillDataToXml_118]    Script Date: 7/20/2017 9:54:06 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'a5d710a5-5657-4150-a461-497747593a23', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ConvertBillDataToXml_174]    Script Date: 7/20/2017 9:54:18 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'4a2fbc16-d92b-4a31-8c68-2d5e1d088d6f', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ConvertBillDataToXml_276]    Script Date: 7/20/2017 10:13:42 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'b7eec768-4289-42ce-91f0-e5bb78b40982', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ConvertBillDataToXml_80]    Script Date: 7/20/2017 10:13:58 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'69f2f9c3-097a-4261-a049-31e5918b7309', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ConvertExactTargetOptOutsToXml_101]    Script Date: 7/20/2017 10:23:39 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'96a72f2c-b95a-49d3-8281-6505acacd801', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ConvertNCOAOptOutsToXml_101]    Script Date: 7/20/2017 10:27:02 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'2ca99142-5073-47a0-860f-b3e7d07bb829', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ConvertPremiseAttributesToXml_174]    Script Date: 7/20/2017 10:29:39 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'5afe54fa-46bc-4b3c-a363-f2fe40d85cad', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ConvertPremiseAttributesToXml_276]    Script Date: 7/20/2017 10:37:17 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'94c18be1-cfa0-4484-a91a-7bf961b5bba8', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ConvertProgramDataToXml_276]    Script Date: 7/20/2017 10:39:34 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'215b0147-9191-40d6-a84c-9964dc55b80f', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ConvertWeatherEmailOptOutsToXml_101]    Script Date: 7/20/2017 10:39:48 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'38c31ea4-2cb8-4709-8dce-a8a486b911b3', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ConvertWeatherEnrollToXml_101]    Script Date: 7/20/2017 10:40:00 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'9803efcc-9742-4ebe-8c64-cdcea19b45b6', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ConvertWeatherOptOutsToXml_101]    Script Date: 7/20/2017 10:40:19 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'34bb4449-bc5c-4a94-9427-2dcfaa274039', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ETL_101]    Script Date: 7/20/2017 10:44:53 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'71288e07-b9cc-469e-b511-e9b453ae87e6', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ETL_118]    Script Date: 7/20/2017 10:45:02 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'ae45aeb8-0d2c-4cd2-8f75-b9ad66fe3f78', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ETL_174]    Script Date: 7/20/2017 10:45:13 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'4190ed24-9e74-4a15-a553-c2c753426769', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ETL_276]    Script Date: 7/20/2017 11:05:34 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'dfae87f0-0525-4343-b4e6-57f334775a35', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ETL_80]    Script Date: 7/20/2017 11:05:46 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'1512dc76-e53b-41f0-8e3a-6da05977b442', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ExportWeatherReport_101]    Script Date: 7/20/2017 11:14:29 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'8f6a4ffc-506b-4863-bf2a-23128deba3fc', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ImportCallCenterOptsOuts_101]    Script Date: 7/20/2017 11:14:43 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'dc8c4e9b-ce38-4ac1-a2b8-6fc503dbdd10', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ImportConsumptionData_101]    Script Date: 7/20/2017 11:14:52 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'0e86ac20-d33f-4eda-b4ec-031751fbbee0', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ImportEmailOptsOuts_101]    Script Date: 7/20/2017 11:15:01 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'234b52d0-e50d-4cb6-b20a-4bd0cdbef66e', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ImportEventsGeneric_101]    Script Date: 7/20/2017 11:15:10 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'7b842c66-35e8-4fac-89bb-cfcd58311125', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ImportEventsToBulk_101]    Script Date: 7/20/2017 11:18:26 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'0939e09d-3f27-4acc-a412-0730df272bd3', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ImportExactTargetEvents_101]    Script Date: 7/20/2017 11:23:21 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'5bb4bb24-3cc9-46e1-8f99-c113897d9ff2', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ImportOptOuts_101]    Script Date: 7/20/2017 11:28:47 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'084dff04-3384-4982-883d-2f444681e0ca', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_RunAmiToDW_101]    Script Date: 7/20/2017 11:32:58 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'c9701a2f-9813-4509-884e-e54ddb746f51', @delete_unused_schedule=1
GO






