﻿USE [msdb]
GO

/****** Object:  Job [PaaS_ConvertPremiseAttributesToXml_224]    Script Date: 7/20/2017 10:29:52 AM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'ef4e89a3-2a10-473e-b67f-bf62f9902419', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ConvertPremiseAttributesToXml_224]    Script Date: 7/20/2017 10:29:52 AM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [CE_UserApp]    Script Date: 7/20/2017 10:29:52 AM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'CE_UserApp' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'CE_UserApp'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'PaaS_ConvertPremiseAttributesToXml_224',
        @enabled=0,
        @notify_level_eventlog=0,
        @notify_level_email=2,
        @notify_level_netsend=0,
        @notify_level_page=0,
        @delete_level=0,
        @description=N'No description available.',
        @category_name=N'CE_UserApp',
        @owner_login_name=N'sa',
        @notify_email_operator_name=N'ACE BI PROD', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Run package]    Script Date: 7/20/2017 10:29:52 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Run package',
        @step_id=1,
        @cmdexec_success_code=0,
        @on_success_action=1,
        @on_success_step_id=0,
        @on_fail_action=2,
        @on_fail_step_id=0,
        @retry_attempts=0,
        @retry_interval=0,
        @os_run_priority=0, @subsystem=N'SSIS',
        @command=N'/ISSERVER "\"\SSISDB\BI\Aclara.Preprocess.PremiseAttributes\PremiseAttributes.dtsx\"" /SERVER "\"AclAceSql91PERF\BI1\"" /Par "\"$Project::ClientID(Int32)\"";224 /Par "\"CM.InsightsBulk.ConnectionString\"";"\"Data Source=AclAceInsSQLPerf.database.windows.net;User ID=AclaraCEAdmin@AclAceInsSQLPerf;Password=Acl@r@282;Initial Catalog=InsightsDW_224;Provider=SQLNCLI11.1;Persist Security Info=True;Encrypt=true;\"" /Par "\"CM.InsightsBulk.InitialCatalog\"";"\"InsightsDW_224\"" /Par "\"CM.InsightsBulk.Password\"";"\"Acl@r@282\"" /Par "\"CM.InsightsBulk.ServerName\"";"\"AclAceInsSQLPerf.database.windows.net\"" /Par "\"CM.InsightsBulk.UserName\"";"\"AclaraCEAdmin@AclAceInsSQLPerf\"" /Par "\"CM.InsightsMetadata.ConnectionString\"";"\"Data Source=AclAceInsSQLPerf.database.windows.net;User ID=AclaraCEAdmin@AclAceInsSQLPerf;Password=Acl@r@282;Initial Catalog=InsightsMetadata;Provider=SQLNCLI11.1;Persist Security Info=True;Encrypt=true;\"" /Par "\"CM.InsightsMetadata.Password\"";"\"Acl@r@282\"" /Par "\"CM.InsightsMetadata.ServerName\"";"\"AclAceInsSQLPerf.database.windows.net\"" /Par "\"CM.InsightsMetadata.UserName\"";"\"AclaraCEAdmin@AclAceInsSQLPerf\"" /Par "\"$ServerOption::LOGGING_LEVEL(Int16)\"";2 /Par "\"$ServerOption::SYNCHRONIZED(Boolean)\"";True /CALLERINFO SQLAGENT /REPORTING E',
        @database_name=N'master',
        @flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Runs every 5 minute',
        @enabled=1,
        @freq_type=4,
        @freq_interval=1,
        @freq_subday_type=4,
        @freq_subday_interval=5,
        @freq_relative_interval=0,
        @freq_recurrence_factor=0,
        @active_start_date=20150206,
        @active_end_date=99991231,
        @active_start_time=0,
        @active_end_time=235959,
        @schedule_uid=N'1e1486ba-8b05-4457-b53c-bef6811384b1'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

