CREATE TABLE [dbo].[DimTimePeriod]
(
[TimePeriodKey] [int] NOT NULL IDENTITY(1, 1),
[TimePeriodDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimTimePeriod] ADD CONSTRAINT [PK_DimTimePeriod] PRIMARY KEY CLUSTERED  ([TimePeriodKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimTimePeriod] ON [dbo].[DimTimePeriod] ([TimePeriodDesc]) ON [PRIMARY]
GO
