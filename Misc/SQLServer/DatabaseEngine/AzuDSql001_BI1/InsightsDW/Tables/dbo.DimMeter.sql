CREATE TABLE [dbo].[DimMeter]
(
[MeterKey] [int] NOT NULL IDENTITY(1, 1),
[ServicePointKey] [int] NOT NULL,
[ClientId] [int] NOT NULL,
[CommodityKey] [int] NOT NULL,
[MeterId] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MeterTypeKey] [int] NULL,
[ReplacedMeterId] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsDeleted] [bit] NULL,
[CreateDate] [datetime] NOT NULL,
[UpdateDate] [datetime] NULL,
[SourceId] [int] NOT NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrackingDate] [datetime] NOT NULL,
[IsInferred] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimMeter] ADD CONSTRAINT [PK_DimMeter_1] PRIMARY KEY CLUSTERED  ([MeterKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimMeter_Client] ON [dbo].[DimMeter] ([ClientId]) INCLUDE ([CommodityKey], [MeterId], [MeterKey], [ServicePointKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimMeter_Commodity_Meter] ON [dbo].[DimMeter] ([CommodityKey], [MeterId]) INCLUDE ([ServicePointKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimMeter] ADD CONSTRAINT [FK_DimMeter_MeterTypes] FOREIGN KEY ([MeterTypeKey]) REFERENCES [dbo].[DimMeterType] ([MeterTypeKey])
GO
ALTER TABLE [dbo].[DimMeter] ADD CONSTRAINT [FK_DimMeter_DimServicePoint] FOREIGN KEY ([ServicePointKey]) REFERENCES [dbo].[DimServicePoint] ([ServicePointKey])
GO
