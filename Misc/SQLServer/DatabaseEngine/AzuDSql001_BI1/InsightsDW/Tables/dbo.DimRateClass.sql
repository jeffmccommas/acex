CREATE TABLE [dbo].[DimRateClass]
(
[RateClassKey] [int] NOT NULL IDENTITY(1, 1),
[RateClassDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientId] [int] NOT NULL,
[RateClassName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimRateClass] ADD CONSTRAINT [PK_ClientRateClass] PRIMARY KEY CLUSTERED  ([RateClassKey]) ON [PRIMARY]
GO
