CREATE TABLE [dbo].[DimCommodity]
(
[CommodityKey] [int] NOT NULL IDENTITY(1, 1),
[CommodityDesc] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CommodityId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimCommodity] ADD CONSTRAINT [PK_DimCommodity] PRIMARY KEY CLUSTERED  ([CommodityKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimCommodity] ON [dbo].[DimCommodity] ([CommodityId]) ON [PRIMARY]
GO
