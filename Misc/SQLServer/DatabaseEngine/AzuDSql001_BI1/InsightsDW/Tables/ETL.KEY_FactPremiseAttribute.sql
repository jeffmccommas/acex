CREATE TABLE [ETL].[KEY_FactPremiseAttribute]
(
[ClientID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AttributeId] [int] NOT NULL,
[SourceID] [int] NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_KEY_FactPremiseAttribute_1] ON [ETL].[KEY_FactPremiseAttribute] ([ClientID]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_KEY_FactPremiseAttribute] ON [ETL].[KEY_FactPremiseAttribute] ([ClientID], [PremiseID], [AccountID], [AttributeId]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
