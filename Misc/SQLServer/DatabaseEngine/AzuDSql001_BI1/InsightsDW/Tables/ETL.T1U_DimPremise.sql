CREATE TABLE [ETL].[T1U_DimPremise]
(
[PremiseKey] [int] NOT NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Street1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StateProvince] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Country] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PostalCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GasService] [int] NULL,
[ElectricService] [int] NULL,
[WaterService] [int] NULL,
[ClientId] [int] NOT NULL,
[SourceId] [int] NOT NULL,
[ETL_LogId] [int] NOT NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [ETL].[T1U_DimPremise] ADD CONSTRAINT [PK_ETL_T1U_DimPremise] PRIMARY KEY CLUSTERED  ([PremiseId], [AccountId], [ClientId]) ON [PRIMARY]
GO
