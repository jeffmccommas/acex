CREATE TABLE [dbo].[factClientAttribute]
(
[ClientKey] [int] NOT NULL,
[DateKey] [int] NOT NULL,
[AttributeName] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [int] NOT NULL,
[AttributeValue] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EffectiveDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[factClientAttribute] ADD CONSTRAINT [PK_factClientAttribute] PRIMARY KEY CLUSTERED  ([ClientKey], [DateKey], [AttributeName]) ON [PRIMARY]
GO
