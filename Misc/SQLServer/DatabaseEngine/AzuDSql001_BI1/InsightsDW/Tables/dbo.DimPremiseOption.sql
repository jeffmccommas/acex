CREATE TABLE [dbo].[DimPremiseOption]
(
[OptionKey] [int] NOT NULL IDENTITY(1, 1),
[AttributeKey] [int] NULL,
[OptionId] [int] NULL,
[AttributeId] [int] NULL,
[CMSOptionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CMSAttributeKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ETLLogId] [int] NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrackingDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimPremiseOption] ADD CONSTRAINT [PK_DimPremiseOption] PRIMARY KEY CLUSTERED  ([OptionKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimPremiseOption] ON [dbo].[DimPremiseOption] ([OptionId]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
