CREATE TABLE [ETL].[KEY_FactBilling]
(
[ClientId] [int] NOT NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomerId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ServiceContractId] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ServicePointId] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeterId] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeterType] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReplacedMeterId] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateClass] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CommodityId] [int] NOT NULL,
[AMIStartDate] [datetime] NULL,
[AMIEndDate] [datetime] NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[SourceId] [int] NOT NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_KEY_FactBilling_ClientId] ON [ETL].[KEY_FactBilling] ([ClientId]) INCLUDE ([AccountId], [AMIEndDate], [AMIStartDate], [CommodityId], [PremiseId], [ServiceContractId], [TrackingDate], [TrackingId]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Key_FactBilling_PremiseId] ON [ETL].[KEY_FactBilling] ([ClientId], [PremiseId], [AccountId]) INCLUDE ([AMIEndDate], [AMIStartDate], [CommodityId], [ServiceContractId], [TrackingDate], [TrackingId]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_KEY_FactBilling_Client_RateClass] ON [ETL].[KEY_FactBilling] ([ClientId], [RateClass]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
