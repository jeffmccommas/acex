CREATE TABLE [Export].[HE_Report_CSV_Measure_history]
(
[profile_name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MeasureID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[history_date] [datetime] NOT NULL,
[bill_year] [int] NULL,
[bill_month] [int] NULL
) ON [PRIMARY]
GO
