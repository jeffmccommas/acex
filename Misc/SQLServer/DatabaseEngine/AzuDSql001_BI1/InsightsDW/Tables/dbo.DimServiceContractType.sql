CREATE TABLE [dbo].[DimServiceContractType]
(
[SecviceContractTypeKey] [int] NOT NULL IDENTITY(1, 1),
[SecviceContractTypeDesciption] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SecviceContractTypeName] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimServiceContractType] ADD CONSTRAINT [PK_DimServiceContractType] PRIMARY KEY CLUSTERED  ([SecviceContractTypeKey]) ON [PRIMARY]
GO
