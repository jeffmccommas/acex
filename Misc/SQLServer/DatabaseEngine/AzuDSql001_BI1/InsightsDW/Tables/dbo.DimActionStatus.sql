CREATE TABLE [dbo].[DimActionStatus]
(
[ActionStatusKey] [int] NOT NULL,
[ActionStatusDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionStatusId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimActionStatus] ADD CONSTRAINT [PK_DimActionStatus] PRIMARY KEY CLUSTERED  ([ActionStatusKey]) ON [PRIMARY]
GO
