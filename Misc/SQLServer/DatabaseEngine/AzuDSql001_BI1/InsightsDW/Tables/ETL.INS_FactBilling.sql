CREATE TABLE [ETL].[INS_FactBilling]
(
[ClientId] [int] NOT NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ServiceContractId] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[CommodityId] [int] NOT NULL,
[BillPeriodTypeId] [int] NOT NULL,
[BillCycleScheduleId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UOMId] [int] NOT NULL,
[SourceId] [int] NOT NULL,
[TotalUnits] [decimal] (18, 2) NOT NULL,
[TotalCost] [decimal] (18, 2) NOT NULL,
[BillDays] [int] NOT NULL,
[RateClass] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReadQuality] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReadDate] [datetime] NULL,
[DueDate] [date] NULL,
[ETL_LogId] [int] NOT NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [ETL].[INS_FactBilling] ADD CONSTRAINT [PK_INS_FactBilling] PRIMARY KEY CLUSTERED  ([ClientId], [PremiseId], [AccountId], [ServiceContractId], [StartDate], [EndDate], [CommodityId]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
