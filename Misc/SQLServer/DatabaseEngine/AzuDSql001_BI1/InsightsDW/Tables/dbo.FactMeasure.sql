CREATE TABLE [dbo].[FactMeasure]
(
[ClientId] [int] NOT NULL,
[CustomerId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionTypeKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AnnualCost] [decimal] (18, 6) NOT NULL,
[AnnualCostVariancePercent] [decimal] (18, 6) NOT NULL,
[AnnualSavingsEstimate] [decimal] (18, 6) NOT NULL,
[AnnualSavingsEstimateCurrencyKey] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Roi] [decimal] (18, 6) NULL,
[Payback] [decimal] (18, 6) NULL,
[UpfrontCost] [decimal] (18, 6) NULL,
[CommodityKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ElecSavEst] [decimal] (18, 6) NULL,
[ElecSavEstCurrencyKey] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ElecUsgSavEst] [decimal] (18, 6) NULL,
[ElecUsgSavEstUomKey] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GasSavEst] [decimal] (18, 6) NULL,
[GasSavEstCurrencyKey] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GasUsgSavEst] [decimal] (18, 6) NULL,
[GasUsgSavEstUomKey] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WaterSavEst] [decimal] (18, 6) NULL,
[WaterSavEstCurrencyKey] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WaterUsgSavEst] [decimal] (18, 6) NULL,
[WaterUsgSavEstUomKey] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Priority] [int] NOT NULL CONSTRAINT [DF_Measure_Priority] DEFAULT ((0)),
[NewDate] [datetime] NOT NULL CONSTRAINT [DF_Measure_NewDate] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactMeasure] ADD CONSTRAINT [PK_FactMeasure] PRIMARY KEY CLUSTERED  ([ClientId], [CustomerId], [AccountId], [PremiseId], [ActionKey]) WITH (FILLFACTOR=50, STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
