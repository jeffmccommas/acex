CREATE TABLE [Export].[home_energy_report_client]
(
[Field Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sample_Data] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sample_Data2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DW Source] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Annual] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Monthly] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Gas] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Water] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Electric] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[email_channel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[print_channel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[clientID] [int] NULL,
[export_column_name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[export_column_order] [int] NULL,
[export_display_flag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[export_key] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX_U_export_home_energy_report_client___fieldname__clientID] ON [Export].[home_energy_report_client] ([Field Name], [clientID]) ON [PRIMARY]
GO
