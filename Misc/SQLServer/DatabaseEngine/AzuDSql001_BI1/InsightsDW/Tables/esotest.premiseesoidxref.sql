CREATE TABLE [esotest].[premiseesoidxref]
(
[clientid] [int] NOT NULL,
[customerid] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[account] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Premise] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[esoid] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [esotest].[premiseesoidxref] ADD CONSTRAINT [PK_premiseesoidxref] PRIMARY KEY CLUSTERED  ([clientid], [customerid], [account], [Premise]) ON [PRIMARY]
GO
