CREATE TABLE [dbo].[BenchmarkGroups]
(
[BenchmarkGroupKey] [int] NOT NULL IDENTITY(1, 1),
[ClientId] [int] NOT NULL,
[BillMonth] [date] NOT NULL,
[BenchmarkGroupTypeKey] [int] NOT NULL,
[GroupId] [int] NOT NULL,
[CommodityKey] [int] NOT NULL,
[Criteria] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseCount] [int] NOT NULL,
[AverageUsage] [decimal] (18, 2) NULL,
[EfficientUsage] [decimal] (18, 2) NULL,
[AverageCost] [decimal] (18, 2) NULL,
[EfficientCost] [decimal] (18, 2) NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_BenchmarkGroups_DateCreated] DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[BenchmarkGroups] ADD CONSTRAINT [PK_BenchmarkGroups] PRIMARY KEY CLUSTERED  ([BenchmarkGroupKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BenchmarkGroups] ON [dbo].[BenchmarkGroups] ([ClientId], [BillMonth], [BenchmarkGroupTypeKey], [GroupId], [CommodityKey]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
