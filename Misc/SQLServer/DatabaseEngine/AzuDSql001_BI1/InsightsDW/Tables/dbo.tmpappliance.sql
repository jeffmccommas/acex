CREATE TABLE [dbo].[tmpappliance]
(
[appliancedesc] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[appliancecategorykey] [int] NULL,
[enduse] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientKey] [int] NULL
) ON [PRIMARY]
GO
