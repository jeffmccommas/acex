CREATE TABLE [dbo].[DimCustomerType]
(
[CustomerTypeKey] [int] NOT NULL,
[CustomerDescription] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
