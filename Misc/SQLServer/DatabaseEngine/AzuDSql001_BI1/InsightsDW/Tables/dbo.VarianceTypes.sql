CREATE TABLE [dbo].[VarianceTypes]
(
[VarianceTypeId] [int] NOT NULL IDENTITY(1, 1),
[VarianceDesc] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VarianceTypes] ADD CONSTRAINT [PK_VarianceTypes] PRIMARY KEY CLUSTERED  ([VarianceTypeId]) ON [PRIMARY]
GO
