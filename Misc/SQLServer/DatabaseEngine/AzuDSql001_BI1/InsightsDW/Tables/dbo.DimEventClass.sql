CREATE TABLE [dbo].[DimEventClass]
(
[EventClassKey] [int] NOT NULL,
[EventClassValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimEventClass] ADD CONSTRAINT [PK_DimEventClass] PRIMARY KEY CLUSTERED  ([EventClassKey]) ON [PRIMARY]
GO
