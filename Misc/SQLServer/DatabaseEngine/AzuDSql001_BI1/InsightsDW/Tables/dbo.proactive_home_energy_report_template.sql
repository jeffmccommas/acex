CREATE TABLE [dbo].[proactive_home_energy_report_template]
(
[Field Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sample_Data] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sample_Data2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DW Source] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Annual] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Monthly] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Gas] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Water] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Electric] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
