CREATE TABLE [Export].[home_energy_report_promo_prereqs]
(
[PromoPreReqKey] [int] NOT NULL,
[PromoKey] [int] NOT NULL,
[ConditionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Export].[home_energy_report_promo_prereqs] ADD CONSTRAINT [PK_ClientPromoPrereq] PRIMARY KEY CLUSTERED  ([PromoPreReqKey]) ON [PRIMARY]
GO
