CREATE TABLE [ETL].[T1U_FactBilling]
(
[ServiceContractKey] [int] NOT NULL,
[BillPeriodStartDateKey] [int] NOT NULL,
[BillPeriodEndDateKey] [int] NOT NULL,
[CommodityKey] [int] NOT NULL,
[SourceId] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[TotalUnits] [decimal] (18, 2) NOT NULL,
[TotalCost] [decimal] (18, 2) NOT NULL,
[BillDays] [int] NOT NULL,
[RateClass] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReadQuality] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReadDate] [datetime] NULL,
[DueDate] [date] NULL,
[ETL_LogId] [int] NOT NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [ETL].[T1U_FactBilling] ADD CONSTRAINT [PK_T1U_FactBilling] PRIMARY KEY CLUSTERED  ([ServiceContractKey], [BillPeriodStartDateKey], [BillPeriodEndDateKey]) ON [PRIMARY]
GO
