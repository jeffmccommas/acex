CREATE TABLE [dbo].[EmailExclusions]
(
[ClientId] [int] NULL,
[Email] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
