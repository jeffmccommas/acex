CREATE TABLE [dbo].[FactBilling]
(
[PremiseKey] [int] NOT NULL,
[BillPeriodStartDateKey] [int] NOT NULL,
[BillPeriodEndDateKey] [int] NOT NULL,
[CommodityKey] [int] NOT NULL,
[UOMKey] [int] NOT NULL,
[BillPeriodTypeKey] [int] NOT NULL CONSTRAINT [DF_FactBilling_BillPeriodTypeKey] DEFAULT ((1)),
[ServicePointKey] [int] NOT NULL,
[ClientId] [int] NOT NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ServicePointId] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommodityId] [int] NOT NULL,
[BillPeriodTypeId] [int] NOT NULL,
[UOMId] [int] NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[TotalUnits] [decimal] (18, 2) NOT NULL,
[TotalCost] [decimal] (18, 2) NOT NULL,
[CreateDate] [datetime] NULL,
[UpdateDate] [datetime] NULL,
[BillDays] [int] NOT NULL,
[ETL_LogId] [int] NOT NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [datetime] NOT NULL,
[SourceKey] [int] NOT NULL,
[SourceId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactBilling] ADD CONSTRAINT [PK_FactBilling] PRIMARY KEY CLUSTERED  ([PremiseKey], [BillPeriodStartDateKey], [BillPeriodEndDateKey], [CommodityKey], [UOMKey], [BillPeriodTypeKey], [ServicePointKey]) WITH (FILLFACTOR=50) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_FactBilling_ClientId] ON [dbo].[FactBilling] ([ClientId]) INCLUDE ([BillDays], [EndDate], [PremiseKey]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20140930-172226] ON [dbo].[FactBilling] ([ClientId], [PremiseId], [AccountId], [ServicePointId], [StartDate], [EndDate], [CommodityId], [BillPeriodTypeId], [UOMId]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_FactBilling_PremiseId] ON [dbo].[FactBilling] ([PremiseId]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
