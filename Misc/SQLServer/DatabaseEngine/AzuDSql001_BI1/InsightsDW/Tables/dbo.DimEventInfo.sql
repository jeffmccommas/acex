CREATE TABLE [dbo].[DimEventInfo]
(
[EventInfoKey] [int] NOT NULL,
[EventInfoName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimEventInfo] ADD CONSTRAINT [PK_DimEventInfo] PRIMARY KEY CLUSTERED  ([EventInfoKey]) ON [PRIMARY]
GO
