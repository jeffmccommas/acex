CREATE TABLE [dbo].[DimClient]
(
[ClientKey] [int] NOT NULL IDENTITY(1, 1),
[ClientId] [int] NOT NULL,
[ClientName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimClient] ADD CONSTRAINT [PK_DimClient] PRIMARY KEY CLUSTERED  ([ClientKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimClient] ON [dbo].[DimClient] ([ClientId]) ON [PRIMARY]
GO
