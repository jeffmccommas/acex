CREATE TABLE [dbo].[FactPremiseAttribute]
(
[PremiseKey] [int] NOT NULL,
[OptionKey] [int] NOT NULL,
[SourceKey] [int] NOT NULL,
[ContentAttributeKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContentOptionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AttributeId] [int] NOT NULL,
[OptionId] [int] NOT NULL,
[Value] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceID] [int] NOT NULL,
[EffectiveDateKey] [int] NOT NULL,
[EffectiveDate] [datetime] NOT NULL,
[ETLLogID] [int] NOT NULL,
[TrackingID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FactPremiseAttribute_1] ON [dbo].[FactPremiseAttribute] ([ClientID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FactPremiseAttribute] ON [dbo].[FactPremiseAttribute] ([ClientID], [PremiseID], [AccountID], [AttributeId]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_FactPremiseAttribute_ContentAttributeKey_with_Premisekey] ON [dbo].[FactPremiseAttribute] ([ContentAttributeKey]) INCLUDE ([ClientID], [ContentOptionKey], [EffectiveDate], [PremiseKey]) WITH (FILLFACTOR=90, PAD_INDEX=ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_FactPremiseAttribute_ContentAttributeKey] ON [dbo].[FactPremiseAttribute] ([ContentAttributeKey]) INCLUDE ([ClientID], [EffectiveDate], [PremiseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FactPremiseAttribute_ContentAttributeKey] ON [dbo].[FactPremiseAttribute] ([ContentAttributeKey]) INCLUDE ([AttributeId], [ClientID], [EffectiveDate], [PremiseKey], [Value]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_FactPremiseAttribute_ContentAttributeKey_PremiseID] ON [dbo].[FactPremiseAttribute] ([ContentAttributeKey], [PremiseID]) INCLUDE ([ClientID], [EffectiveDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_FactPremiseAttribute_EffectiveDate] ON [dbo].[FactPremiseAttribute] ([EffectiveDate]) INCLUDE ([ClientID], [PremiseKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FactPremiseAttribute_2] ON [dbo].[FactPremiseAttribute] ([PremiseKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_FactPremiseAttribute] ON [dbo].[FactPremiseAttribute] ([PremiseKey], [ContentAttributeKey]) INCLUDE ([EffectiveDate], [OptionKey], [Value]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_FactPremiseAttribute_Value] ON [dbo].[FactPremiseAttribute] ([Value], [ContentAttributeKey]) INCLUDE ([ClientID], [EffectiveDate], [PremiseID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
