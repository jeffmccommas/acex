CREATE TABLE [dbo].[PremiseSavings]
(
[Premisekey] [int] NOT NULL,
[ClientActionSavingsID] [int] NOT NULL,
[ActionKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Rank] [int] NOT NULL,
[Cost] [decimal] (18, 2) NOT NULL,
[AnnualSavingsEstimate] [decimal] (18, 2) NOT NULL,
[AnnualUsageSavingsEstimate] [decimal] (18, 2) NULL,
[CreateDate] [datetime] NOT NULL CONSTRAINT [DF__PremiseSa__Creat__369C13AA] DEFAULT (getdate())
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_Premise_Savings_ActionKey] ON [dbo].[PremiseSavings] ([ActionKey]) INCLUDE ([AnnualSavingsEstimate], [Premisekey]) WITH (FILLFACTOR=50) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_PremiseSavings_PremiseKey] ON [dbo].[PremiseSavings] ([Premisekey], [ActionKey]) INCLUDE ([AnnualSavingsEstimate]) WITH (FILLFACTOR=50) ON [PRIMARY]
GO
