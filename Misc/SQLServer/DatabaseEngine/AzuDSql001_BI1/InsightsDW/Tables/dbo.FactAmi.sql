CREATE TABLE [dbo].[FactAmi]
(
[AmiKey] [int] NOT NULL IDENTITY(1, 1),
[ClientId] [int] NOT NULL,
[PremiseKey] [int] NOT NULL,
[ServicePointKey] [int] NOT NULL,
[MeterId] [int] NOT NULL,
[ETL_LogId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactAmi] ADD CONSTRAINT [PK_FactAmi] PRIMARY KEY CLUSTERED  ([AmiKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [NC_FactAmi] ON [dbo].[FactAmi] ([ClientId], [PremiseKey], [ServicePointKey], [MeterId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_FactAmi_PremiseKey] ON [dbo].[FactAmi] ([PremiseKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactAmi] ADD CONSTRAINT [FK_FactAmi_DimPremise] FOREIGN KEY ([PremiseKey]) REFERENCES [dbo].[DimPremise] ([PremiseKey])
GO
ALTER TABLE [dbo].[FactAmi] ADD CONSTRAINT [FK_FactAmi_DimServicePoint] FOREIGN KEY ([ServicePointKey]) REFERENCES [dbo].[DimServicePoint] ([ServicePointKey])
GO
