CREATE TABLE [dbo].[DimSource]
(
[SourceKey] [int] NOT NULL,
[SourceId] [int] NULL,
[SourceDesc] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimSource] ADD CONSTRAINT [PK_DimSource] PRIMARY KEY CLUSTERED  ([SourceKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimSource] ON [dbo].[DimSource] ([SourceId]) ON [PRIMARY]
GO
