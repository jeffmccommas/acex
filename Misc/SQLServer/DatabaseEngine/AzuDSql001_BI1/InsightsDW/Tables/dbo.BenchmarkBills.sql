CREATE TABLE [dbo].[BenchmarkBills]
(
[ClientId] [int] NOT NULL,
[PremiseKey] [int] NOT NULL,
[EndDate] [datetime] NOT NULL,
[BillDays] [int] NOT NULL,
[BillDate] [date] NOT NULL,
[DateModified] [datetime] NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_BenchmarkBills_Client] ON [dbo].[BenchmarkBills] ([ClientId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BenchmarkBills_Premise] ON [dbo].[BenchmarkBills] ([PremiseKey], [EndDate]) ON [PRIMARY]
GO
