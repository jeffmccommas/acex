CREATE TABLE [dbo].[DimCity]
(
[CityKey] [int] NOT NULL IDENTITY(1, 1),
[StateProvinceKey] [int] NOT NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimCity] ADD CONSTRAINT [PK_DimCity] PRIMARY KEY CLUSTERED  ([CityKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimCity] ON [dbo].[DimCity] ([City], [StateProvinceKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimCity] ADD CONSTRAINT [FK_DimCity_DimStateProvince] FOREIGN KEY ([StateProvinceKey]) REFERENCES [dbo].[DimStateProvince] ([StateProvinceKey])
GO
