CREATE TABLE [dbo].[factEventDetails]
(
[EventDetailKey] [bigint] NOT NULL IDENTITY(1, 1),
[PremiseKey] [int] NOT NULL,
[EventActionKey] [int] NOT NULL,
[EventDateKey] [int] NOT NULL,
[EventDateTime] [datetime] NOT NULL,
[DateImported] [datetime] NOT NULL,
[ChannelKey] [int] NOT NULL,
[OriginKey] [int] NOT NULL,
[SessionID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EventOrder] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[factEventDetails] ADD CONSTRAINT [PK_factEventDetails] PRIMARY KEY CLUSTERED  ([EventDetailKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[factEventDetails] ADD CONSTRAINT [FK_factEventDetails_DimChannel] FOREIGN KEY ([ChannelKey]) REFERENCES [dbo].[DimChannel] ([ChannelKey])
GO
ALTER TABLE [dbo].[factEventDetails] ADD CONSTRAINT [FK_factEventDetails_DimEventAction] FOREIGN KEY ([EventActionKey]) REFERENCES [dbo].[DimEventAction] ([EventActionKey])
GO
ALTER TABLE [dbo].[factEventDetails] WITH NOCHECK ADD CONSTRAINT [FK_factEventDetails_DimDate] FOREIGN KEY ([EventDateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
ALTER TABLE [dbo].[factEventDetails] ADD CONSTRAINT [FK_factEventDetails_DimOrigin] FOREIGN KEY ([OriginKey]) REFERENCES [dbo].[DimOrigin] ([OriginKey])
GO
