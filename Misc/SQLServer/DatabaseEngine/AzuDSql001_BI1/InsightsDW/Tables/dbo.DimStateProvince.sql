CREATE TABLE [dbo].[DimStateProvince]
(
[StateProvinceKey] [int] NOT NULL IDENTITY(1, 1),
[CountryRegionKey] [int] NOT NULL,
[StateProvinceName] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StateProvinceFullName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimStateProvince] ADD CONSTRAINT [PK_DimStateProvince] PRIMARY KEY NONCLUSTERED  ([StateProvinceKey]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IX_DimStateProvince] ON [dbo].[DimStateProvince] ([CountryRegionKey], [StateProvinceName]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimStateProvince_1] ON [dbo].[DimStateProvince] ([StateProvinceName]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimStateProvince] ADD CONSTRAINT [FK_DimStateProvince_DimCountryRegion] FOREIGN KEY ([CountryRegionKey]) REFERENCES [dbo].[DimCountryRegion] ([CountryRegionKey])
GO
