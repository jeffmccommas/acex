CREATE TABLE [dbo].[DimCustomer]
(
[CustomerKey] [int] NOT NULL IDENTITY(1, 1),
[ClientKey] [int] NOT NULL,
[ClientId] [int] NOT NULL,
[PostalCodeKey] [int] NOT NULL,
[CustomerAuthenticationTypeKey] [int] NOT NULL,
[SourceKey] [int] NULL,
[CityKey] [int] NOT NULL,
[CustomerId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateProvince] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MobilePhoneNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailAddress] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AlternateEmailAddress] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[UpdateDate] [datetime] NULL,
[SourceId] [int] NULL,
[AuthenticationTypeId] [int] NULL,
[Language] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ETL_LogId] [int] NULL,
[IsInferred] [tinyint] NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrackingDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimCustomer] ADD CONSTRAINT [PK_DimCustomer] PRIMARY KEY CLUSTERED  ([ClientId], [CustomerId]) WITH (FILLFACTOR=50) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimCustomer_ClientKey] ON [dbo].[DimCustomer] ([ClientKey]) INCLUDE ([CustomerKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimCustomer_CustomerId] ON [dbo].[DimCustomer] ([CustomerId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DimCustomer_CustomerKey] ON [dbo].[DimCustomer] ([CustomerKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimCustomer] WITH NOCHECK ADD CONSTRAINT [FK_DimCustomer_DimClient] FOREIGN KEY ([ClientKey]) REFERENCES [dbo].[DimClient] ([ClientKey])
GO
ALTER TABLE [dbo].[DimCustomer] WITH NOCHECK ADD CONSTRAINT [FK_DimCustomer_DimCustomerAuthenticationType] FOREIGN KEY ([CustomerAuthenticationTypeKey]) REFERENCES [dbo].[DimCustomerAuthenticationType] ([CustomerAuthenticationTypeKey])
GO
ALTER TABLE [dbo].[DimCustomer] WITH NOCHECK ADD CONSTRAINT [FK_DimCustomer_DimPostalCode] FOREIGN KEY ([PostalCodeKey]) REFERENCES [dbo].[DimPostalCode] ([PostalCodeKey])
GO
ALTER TABLE [dbo].[DimCustomer] WITH NOCHECK ADD CONSTRAINT [FK_DimCustomer_DimSource] FOREIGN KEY ([SourceKey]) REFERENCES [dbo].[DimSource] ([SourceKey])
GO
