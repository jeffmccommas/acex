CREATE TABLE [dbo].[holdoldBenchmarks]
(
[BenchmarkKey] [int] NOT NULL IDENTITY(1, 1),
[ClientId] [int] NOT NULL,
[MyCost] [float] NULL,
[AverageCost] [float] NULL,
[EfficientCost] [float] NULL,
[NewMyCost] [decimal] (18, 2) NULL,
[NewAverageCost] [decimal] (18, 2) NULL,
[NewEfficientCost] [decimal] (18, 2) NULL
) ON [PRIMARY]
GO
