CREATE TABLE [dbo].[TmpBenchmarkProfiles]
(
[GroupId] [int] NOT NULL,
[CommodityKey] [int] NOT NULL,
[House.People] [int] NULL,
[House.Style] [int] NULL,
[House.TotalArea] [int] NULL,
[BillMonth] [datetime] NOT NULL,
[BillPeriodTypeKey] [int] NOT NULL,
[BillCycle] [int] NOT NULL,
[PeersCount] [int] NOT NULL,
[AvgCost] [decimal] (18, 2) NULL,
[AvgUnits] [decimal] (18, 2) NULL,
[EfficientUsage] [decimal] (18, 2) NULL,
[EfficientCost] [decimal] (18, 2) NULL
) ON [PRIMARY]
GO
