CREATE TABLE [dbo].[DimOrigin]
(
[OriginKey] [int] NOT NULL,
[OriginValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimOrigin] ADD CONSTRAINT [PK_DimOrigin] PRIMARY KEY CLUSTERED  ([OriginKey]) ON [PRIMARY]
GO
