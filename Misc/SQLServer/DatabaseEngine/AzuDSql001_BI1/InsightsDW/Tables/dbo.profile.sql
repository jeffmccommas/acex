CREATE TABLE [dbo].[profile]
(
[he_criteria_profile_key] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[profile_name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[clientID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[fuel_type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[energyobjectcategory_List] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[period] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[season_List] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[enduse_List] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ordered_enduse_List] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_id1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_detail1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_id2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_detail2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_id3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_detail3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_id4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_detail4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_id5] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_detail5] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_id6] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_detail6] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_sort1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_sort2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_sort3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_sort4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_sort5] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[criteria_sort6] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[process_id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[process_desc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[process_time] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[process_num_rows] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[process_start_time] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[process_end_time] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ins_time] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[upd_time] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[total_exec_time] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
