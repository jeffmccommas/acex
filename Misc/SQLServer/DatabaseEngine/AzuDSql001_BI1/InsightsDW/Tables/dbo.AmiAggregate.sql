CREATE TABLE [dbo].[AmiAggregate]
(
[AmiAggregateKey] [int] NOT NULL IDENTITY(1, 1),
[AmiKey] [int] NOT NULL,
[AmiAggregateKeyType] [int] NOT NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DayOfWeek] [int] NULL,
[Month] [int] NULL,
[Year] [int] NULL,
[DailyMinInPeriod] [decimal] (18, 4) NOT NULL,
[DailyMaxInPeriod] [decimal] (18, 4) NOT NULL,
[TotalUsageInPeriod] [decimal] (38, 4) NOT NULL,
[AvgerageInPeriod] [decimal] (18, 4) NOT NULL,
[DateOfMinInPeriod] [date] NULL,
[DateOfMaxInPeriod] [date] NULL
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [NonClusteredIndex-20141104-142818] ON [dbo].[AmiAggregate] ([AmiKey], [AmiAggregateKeyType], [PremiseId], [DayOfWeek], [Month], [Year]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_AmiAggregate_PremiseId_Month_Year_AmiAggregateKeyType] ON [dbo].[AmiAggregate] ([PremiseId], [Month], [Year], [AmiAggregateKeyType]) INCLUDE ([AvgerageInPeriod], [DailyMaxInPeriod], [DailyMinInPeriod], [DateOfMaxInPeriod], [DateOfMinInPeriod], [DayOfWeek]) ON [PRIMARY]
GO
