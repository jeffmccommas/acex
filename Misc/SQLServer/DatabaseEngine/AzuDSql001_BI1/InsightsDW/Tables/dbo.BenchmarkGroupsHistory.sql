CREATE TABLE [dbo].[BenchmarkGroupsHistory]
(
[BenchmarkGroupHistoryKey] [int] NOT NULL IDENTITY(1, 1),
[ClientId] [int] NOT NULL,
[BillMonth] [date] NOT NULL,
[BenchmarkGroupTypeKey] [int] NOT NULL,
[GroupId] [int] NOT NULL,
[CommodityKey] [int] NOT NULL,
[Criteria] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseCount] [int] NOT NULL,
[AverageUsage] [decimal] (18, 2) NULL,
[EfficientUsage] [decimal] (18, 2) NULL,
[AverageCost] [decimal] (18, 2) NULL,
[EfficientCost] [decimal] (18, 2) NULL,
[DateCreated] [datetime] NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_BenchmarkGroupsHistory_DateUpdated] DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[BenchmarkGroupsHistory] ADD CONSTRAINT [PK_BenchmarkGroupsHistory] PRIMARY KEY CLUSTERED  ([BenchmarkGroupHistoryKey]) ON [PRIMARY]
GO
