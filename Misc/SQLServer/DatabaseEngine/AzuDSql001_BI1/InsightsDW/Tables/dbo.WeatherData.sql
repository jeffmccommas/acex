CREATE TABLE [dbo].[WeatherData]
(
[StationID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WeatherReadingDate] [datetime] NULL,
[MinTemp] [int] NULL,
[MaxTemp] [int] NULL,
[AvgTemp] [int] NULL,
[AvgWetBulb] [int] NULL,
[HeatingDegreeDays] [int] NULL,
[CoolingDegreeDays] [int] NULL,
[ZipCode] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_WeatherData_ZipCode] ON [dbo].[WeatherData] ([ZipCode], [WeatherReadingDate]) INCLUDE ([HeatingDegreeDays]) ON [PRIMARY]
GO
