CREATE TABLE [dbo].[FactServicePointBilling]
(
[ServiceContractKey] [int] NOT NULL,
[BillPeriodStartDateKey] [int] NOT NULL,
[BillPeriodEndDateKey] [int] NOT NULL,
[UOMKey] [int] NOT NULL,
[BillPeriodTypeKey] [int] NOT NULL CONSTRAINT [DF_FactServicePointBilling_BillPeriodTypeKey] DEFAULT ((1)),
[ClientId] [int] NOT NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommodityId] [int] NOT NULL,
[BillPeriodTypeId] [int] NOT NULL,
[UOMId] [int] NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[TotalUsage] [decimal] (18, 2) NOT NULL,
[CostOfUsage] [decimal] (18, 2) NOT NULL,
[OtherCost] [decimal] (18, 2) NULL,
[RateClassKey1] [int] NULL,
[RateClassKey2] [int] NULL,
[NextReadDate] [date] NULL,
[ReadDate] [datetime] NULL,
[CreateDate] [datetime] NULL,
[UpdateDate] [datetime] NULL,
[BillCycleScheduleId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillDays] [int] NOT NULL,
[ETL_LogId] [int] NOT NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [datetime] NOT NULL,
[SourceKey] [int] NOT NULL,
[SourceId] [int] NOT NULL,
[ReadQuality] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DueDate] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactServicePointBilling] ADD CONSTRAINT [PK_FactServicePointBilling] PRIMARY KEY CLUSTERED  ([ServiceContractKey], [BillPeriodStartDateKey], [BillPeriodEndDateKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FactServicePointBilling_ClientId] ON [dbo].[FactServicePointBilling] ([ClientId]) INCLUDE ([BillDays], [EndDate], [ServiceContractKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FactServicePointBilling] ON [dbo].[FactServicePointBilling] ([ClientId], [PremiseId], [AccountId], [StartDate], [EndDate], [CommodityId], [BillPeriodTypeId], [UOMId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactServicePointBilling] ADD CONSTRAINT [FK_FactServicePointBilling_ClientRateClass] FOREIGN KEY ([RateClassKey1]) REFERENCES [dbo].[DimRateClass] ([RateClassKey])
GO
ALTER TABLE [dbo].[FactServicePointBilling] ADD CONSTRAINT [FK_FactServicePointBilling_ClientRateClass1] FOREIGN KEY ([RateClassKey2]) REFERENCES [dbo].[DimRateClass] ([RateClassKey])
GO
ALTER TABLE [dbo].[FactServicePointBilling] ADD CONSTRAINT [FK_FactServicePointBilling_FactServiceContract] FOREIGN KEY ([ServiceContractKey]) REFERENCES [dbo].[DimServiceContract] ([ServiceContractKey])
GO
