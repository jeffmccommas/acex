CREATE TABLE [dbo].[factActionData]
(
[ActionDetailKey] [int] NOT NULL,
[ActionDataKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionDataValue] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[factActionData] ADD CONSTRAINT [PK_factActionInfo] PRIMARY KEY CLUSTERED  ([ActionDetailKey], [ActionDataKey]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
