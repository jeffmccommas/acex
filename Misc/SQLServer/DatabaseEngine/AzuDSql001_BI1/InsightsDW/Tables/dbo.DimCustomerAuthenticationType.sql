CREATE TABLE [dbo].[DimCustomerAuthenticationType]
(
[CustomerAuthenticationTypeKey] [int] NOT NULL,
[CustomerAuthenticationTypeId] [int] NULL,
[CustomerDescription] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimCustomerAuthenticationType] ADD CONSTRAINT [PK_DimCustomerAuthenticationType] PRIMARY KEY CLUSTERED  ([CustomerAuthenticationTypeKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimCustomerAuthenticationType] ON [dbo].[DimCustomerAuthenticationType] ([CustomerAuthenticationTypeId]) ON [PRIMARY]
GO
