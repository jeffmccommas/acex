CREATE TABLE [dbo].[factAction]
(
[ActionDetailKey] [int] NOT NULL IDENTITY(1, 1),
[PremiseKey] [int] NOT NULL,
[DateKey] [int] NOT NULL,
[ActionId] [int] NOT NULL,
[ActionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SubActionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusKey] [int] NOT NULL,
[StatusDate] [datetime] NOT NULL,
[ClientID] [int] NOT NULL,
[PremiseID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceKey] [int] NOT NULL,
[CreateDate] [datetime] NOT NULL,
[ETLLogID] [int] NOT NULL,
[TrackingID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[factAction] ADD CONSTRAINT [PK_factAction] PRIMARY KEY CLUSTERED  ([ActionDetailKey]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_factAction] ON [dbo].[factAction] ([ClientID], [PremiseID], [AccountID], [ActionKey], [SubActionKey]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_factAction_PremiseKey] ON [dbo].[factAction] ([PremiseKey]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
