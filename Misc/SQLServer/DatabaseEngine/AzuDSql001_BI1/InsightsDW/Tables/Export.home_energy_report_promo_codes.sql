CREATE TABLE [Export].[home_energy_report_promo_codes]
(
[PromoID] [int] NOT NULL,
[PromoLetter] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TextContentKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [int] NULL,
[Effective] [datetime] NULL CONSTRAINT [DF_ClientPromo_Effective] DEFAULT (getdate()),
[Expires] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Export].[home_energy_report_promo_codes] ADD CONSTRAINT [PK_ClientPromo] PRIMARY KEY CLUSTERED  ([PromoID]) ON [PRIMARY]
GO
