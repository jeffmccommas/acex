CREATE TABLE [dbo].[BenchmarkGroupType]
(
[BenchmarkGroupTypeKey] [int] NOT NULL IDENTITY(1, 1),
[BenchmarkGroupTypeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BenchmarkGroupType] ADD CONSTRAINT [PK_PeerGroupType] PRIMARY KEY CLUSTERED  ([BenchmarkGroupTypeKey]) ON [PRIMARY]
GO
