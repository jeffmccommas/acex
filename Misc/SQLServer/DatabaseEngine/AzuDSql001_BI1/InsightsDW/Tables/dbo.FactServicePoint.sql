CREATE TABLE [dbo].[FactServicePoint]
(
[FactServicePointKey] [int] NOT NULL IDENTITY(1, 1),
[ServiceContractKey] [int] NOT NULL,
[ServicePointKey] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactServicePoint] ADD CONSTRAINT [PK_FactServicePoint] PRIMARY KEY CLUSTERED  ([FactServicePointKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FactServicePoint_ServiceContract_ServicePoint] ON [dbo].[FactServicePoint] ([ServiceContractKey], [ServicePointKey]) INCLUDE ([FactServicePointKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FactServicePoint_ServicePointKey] ON [dbo].[FactServicePoint] ([ServicePointKey]) INCLUDE ([ServiceContractKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactServicePoint] ADD CONSTRAINT [FK_FactServicePoint_FactServiceContract] FOREIGN KEY ([ServiceContractKey]) REFERENCES [dbo].[DimServiceContract] ([ServiceContractKey])
GO
ALTER TABLE [dbo].[FactServicePoint] ADD CONSTRAINT [FK_FactServicePoint_DimServicePoint] FOREIGN KEY ([ServicePointKey]) REFERENCES [dbo].[DimServicePoint] ([ServicePointKey])
GO
