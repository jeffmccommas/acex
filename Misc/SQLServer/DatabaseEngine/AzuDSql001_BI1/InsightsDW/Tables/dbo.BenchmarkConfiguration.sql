CREATE TABLE [dbo].[BenchmarkConfiguration]
(
[ConfigurationKey] [int] NOT NULL IDENTITY(1, 1),
[ConfigurationId] [int] NOT NULL,
[DateKey] [int] NOT NULL,
[AttributeKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsNumeric] [bit] NOT NULL,
[Enabled] [bit] NOT NULL CONSTRAINT [DF_BenchmarkConfiguration_Enabled] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BenchmarkConfiguration] ADD CONSTRAINT [PK_BenchmarkConfiguration] PRIMARY KEY CLUSTERED  ([ConfigurationKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BenchmarkConfiguration] ON [dbo].[BenchmarkConfiguration] ([ConfigurationId], [DateKey], [AttributeKey]) ON [PRIMARY]
GO
