CREATE TABLE [ETL].[KEY_DimCustomer]
(
[ClientId] [int] NOT NULL,
[CustomerId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [ETL].[KEY_DimCustomer] ADD CONSTRAINT [PK_KEY_DimCustomer] PRIMARY KEY CLUSTERED  ([ClientId], [CustomerId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_KEY_DimCustomer] ON [ETL].[KEY_DimCustomer] ([ClientId]) ON [PRIMARY]
GO
