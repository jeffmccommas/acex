CREATE TABLE [dbo].[FactEvent]
(
[ClientID] [int] NOT NULL,
[CustomerKey] [int] NOT NULL,
[PremiseKey] [int] NOT NULL,
[DateKey] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[FlagDateCreated] [datetime] NULL,
[FlagDateUpdated] [datetime] NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactEvent] ADD CONSTRAINT [PK_FactEvent] PRIMARY KEY CLUSTERED  ([ClientID], [CustomerKey], [PremiseKey], [DateKey], [EventTypeID]) ON [PRIMARY]
GO
