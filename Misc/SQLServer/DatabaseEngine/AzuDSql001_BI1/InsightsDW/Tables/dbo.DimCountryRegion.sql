CREATE TABLE [dbo].[DimCountryRegion]
(
[CountryRegionKey] [int] NOT NULL IDENTITY(1, 1),
[CountryRegionCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimCountryRegion] ADD CONSTRAINT [PK_DimCountryRegion] PRIMARY KEY CLUSTERED  ([CountryRegionKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimCountryRegion] ON [dbo].[DimCountryRegion] ([CountryRegionCode]) ON [PRIMARY]
GO
