CREATE TABLE [dbo].[DimServiceContract]
(
[ServiceContractKey] [int] NOT NULL IDENTITY(1, 1),
[PremiseKey] [int] NOT NULL,
[CommodityKey] [int] NOT NULL,
[ClientId] [int] NOT NULL,
[ServiceContractId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ServiceContractTypeKey] [int] NULL,
[ServiceContractDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveDate] [datetime] NULL,
[InActiveDate] [datetime] NULL,
[IsInferred] [tinyint] NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrackingDate] [datetime] NULL,
[BudgetBillingIndicator] [int] NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_DimServiceContract_Premise_Commodity_ServiceContract] ON [dbo].[DimServiceContract] ([PremiseKey], [CommodityKey], [ClientId], [ServiceContractId]) ON [PRIMARY]

GO
ALTER TABLE [dbo].[DimServiceContract] ADD CONSTRAINT [PK_FactServiceContract] PRIMARY KEY CLUSTERED  ([ServiceContractKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimServiceContract_ClientId] ON [dbo].[DimServiceContract] ([ClientId]) INCLUDE ([CommodityKey], [PremiseKey], [ServiceContractId], [ServiceContractKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimServiceContract_Client_Ext] ON [dbo].[DimServiceContract] ([ClientId]) INCLUDE ([CommodityKey], [PremiseKey], [ServiceContractId], [ServiceContractKey], [TrackingDate]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimServiceContract] ADD CONSTRAINT [FK_FactServiceContract_DimServiceContractType] FOREIGN KEY ([ServiceContractTypeKey]) REFERENCES [dbo].[DimServiceContractType] ([SecviceContractTypeKey])
GO
