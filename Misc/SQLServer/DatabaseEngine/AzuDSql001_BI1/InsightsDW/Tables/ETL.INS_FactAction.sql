CREATE TABLE [ETL].[INS_FactAction]
(
[ClientId] [int] NOT NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionId] [int] NOT NULL,
[ActionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SubActionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [int] NOT NULL,
[StatusDate] [datetime] NOT NULL,
[CreateDate] [datetime] NOT NULL,
[SourceId] [int] NOT NULL,
[ETL_LogId] [int] NOT NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_INS_FactAction_ClientID] ON [ETL].[INS_FactAction] ([ClientId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_INS_FactAction] ON [ETL].[INS_FactAction] ([ClientId], [PremiseId], [AccountId], [ActionKey], [SubActionKey], [StatusId], [StatusDate], [CreateDate]) ON [PRIMARY]
GO
