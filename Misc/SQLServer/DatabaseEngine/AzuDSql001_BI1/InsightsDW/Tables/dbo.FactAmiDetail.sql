CREATE TABLE [dbo].[FactAmiDetail]
(
[AmiKey] [int] NOT NULL,
[DateKey] [int] NOT NULL,
[ClientId] [int] NOT NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ServicePointId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomerId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommodityId] [int] NOT NULL,
[MeterId] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UOMId] [int] NOT NULL,
[TotalUnits] [decimal] (18, 4) NOT NULL,
[SourceId] [int] NOT NULL,
[ETL_LogId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactAmiDetail] ADD CONSTRAINT [PK_FactAmiDetail] PRIMARY KEY CLUSTERED  ([AmiKey], [DateKey]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_FactAmiDetail_AccountId] ON [dbo].[FactAmiDetail] ([AccountId]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_FactAmiDetail_PremiseId] ON [dbo].[FactAmiDetail] ([PremiseId]) INCLUDE ([DateKey]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_FactAmiDetail_ServicePointId] ON [dbo].[FactAmiDetail] ([ServicePointId]) INCLUDE ([ClientId]) WITH (FILLFACTOR=80, STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactAmiDetail] ADD CONSTRAINT [FK_FactAmiDetail_FactAmi] FOREIGN KEY ([AmiKey]) REFERENCES [dbo].[FactAmi] ([AmiKey])
GO
ALTER TABLE [dbo].[FactAmiDetail] ADD CONSTRAINT [FK_FactAmiDetail_DimDate] FOREIGN KEY ([DateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO
