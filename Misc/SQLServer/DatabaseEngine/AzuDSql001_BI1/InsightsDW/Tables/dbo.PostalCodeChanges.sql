CREATE TABLE [dbo].[PostalCodeChanges]
(
[ClientId] [int] NOT NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NewPostalCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PostalCodeChanges] ADD CONSTRAINT [PK_PostalCodeChanges] PRIMARY KEY CLUSTERED  ([ClientId], [PremiseId]) ON [PRIMARY]
GO
