CREATE TABLE [dbo].[PromoCodeGasSavings]
(
[id] [int] NOT NULL,
[low_endpoint] [int] NOT NULL,
[high_endpoint] [int] NOT NULL,
[savings] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PromoCodeGasSavings] ADD CONSTRAINT [PK_PromoCodeGasSavings] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
