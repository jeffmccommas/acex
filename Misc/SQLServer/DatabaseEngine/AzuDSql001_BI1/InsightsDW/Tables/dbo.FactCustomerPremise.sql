CREATE TABLE [dbo].[FactCustomerPremise]
(
[CustomerKey] [int] NOT NULL,
[PremiseKey] [int] NOT NULL,
[ETL_LogId] [int] NULL,
[DateCreated] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactCustomerPremise] ADD CONSTRAINT [PK_FactCustomerPremise] PRIMARY KEY CLUSTERED  ([CustomerKey], [PremiseKey]) WITH (FILLFACTOR=50) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20141030-091713] ON [dbo].[FactCustomerPremise] ([CustomerKey]) INCLUDE ([DateCreated], [PremiseKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20140916-164849] ON [dbo].[FactCustomerPremise] ([PremiseKey]) INCLUDE ([CustomerKey], [DateCreated]) ON [PRIMARY]
GO
