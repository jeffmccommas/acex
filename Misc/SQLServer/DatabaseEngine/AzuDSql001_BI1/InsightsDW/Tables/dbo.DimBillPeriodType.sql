CREATE TABLE [dbo].[DimBillPeriodType]
(
[BillPeriodTypeKey] [int] NOT NULL IDENTITY(1, 1),
[BillPeriodTypeDesc] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BillPeriodTypeId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimBillPeriodType] ADD CONSTRAINT [PK_DimBillPeriodType] PRIMARY KEY CLUSTERED  ([BillPeriodTypeKey]) ON [PRIMARY]
GO
