CREATE TABLE [ETL].[KEY_FactAction]
(
[ClientID] [int] NOT NULL,
[PremiseID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SubActionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceID] [int] NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_KEY_FactAction_ClientID] ON [ETL].[KEY_FactAction] ([ClientID]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_KEY_FactAction] ON [ETL].[KEY_FactAction] ([ClientID], [PremiseID], [AccountID], [ActionKey], [SubActionKey]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
