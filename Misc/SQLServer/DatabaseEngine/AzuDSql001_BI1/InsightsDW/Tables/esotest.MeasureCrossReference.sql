CREATE TABLE [esotest].[MeasureCrossReference]
(
[MeasureId] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DWMeasureName] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [esotest].[MeasureCrossReference] ADD CONSTRAINT [PK_MeasureCrossReference] PRIMARY KEY CLUSTERED  ([MeasureId], [DWMeasureName]) ON [PRIMARY]
GO
