CREATE TABLE [dbo].[factDailyWeather]
(
[StationID] [int] NOT NULL,
[DimDate] [int] NOT NULL,
[MinTemp] [smallint] NULL,
[MaxTemp] [smallint] NULL,
[AvgTemp] [smallint] NULL,
[AvgWetBulb] [smallint] NULL,
[HeatingDegreeDays] [smallint] NULL,
[CoolingDegreeDays] [smallint] NULL,
[DataIsDerived] [tinyint] NULL CONSTRAINT [DF__factDaily__DataI__477199F1] DEFAULT ((0)),
[Comment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[factDailyWeather] ADD CONSTRAINT [PK_factDailyWeather] PRIMARY KEY CLUSTERED  ([StationID], [DimDate]) ON [PRIMARY]
GO
