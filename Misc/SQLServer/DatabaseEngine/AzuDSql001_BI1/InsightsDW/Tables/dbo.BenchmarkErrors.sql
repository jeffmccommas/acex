CREATE TABLE [dbo].[BenchmarkErrors]
(
[LogId] [bigint] NOT NULL IDENTITY(1, 1),
[ClientId] [int] NOT NULL,
[PremiseKey] [int] NOT NULL,
[BillMonth] [date] NOT NULL,
[ErrorTypeKey] [tinyint] NOT NULL,
[ErrorInfo] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_BenchmarkErrors_DateCreated] DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[BenchmarkErrors] ADD CONSTRAINT [PK_BenchmarkErrors] PRIMARY KEY CLUSTERED  ([LogId]) ON [PRIMARY]
GO
