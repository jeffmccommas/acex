CREATE TABLE [dbo].[DimMeterIntervalLength]
(
[IntervalLengthKey] [tinyint] NOT NULL,
[IntervalLengthId] [tinyint] NULL,
[IntervalLengthSeconds] [int] NOT NULL,
[IntervalLengthDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimMeterIntervalLength] ADD CONSTRAINT [PK_DimMeterIntervalLength] PRIMARY KEY CLUSTERED  ([IntervalLengthKey]) ON [PRIMARY]
GO
