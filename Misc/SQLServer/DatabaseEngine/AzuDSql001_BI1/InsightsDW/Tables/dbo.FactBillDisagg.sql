CREATE TABLE [dbo].[FactBillDisagg]
(
[ClientId] [int] NOT NULL,
[CustomerId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BillDate] [datetime] NOT NULL,
[EndUseKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ApplianceKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommodityKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PeriodId] [tinyint] NOT NULL,
[Usage] [decimal] (18, 6) NOT NULL,
[Cost] [decimal] (18, 6) NOT NULL,
[UomKey] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyKey] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Confidence] [int] NULL,
[ReconciliationRatio] [decimal] (18, 6) NULL,
[StatusId] [tinyint] NOT NULL CONSTRAINT [DF_BillDisagg_StatusId] DEFAULT ((0)),
[NewDate] [datetime] NOT NULL CONSTRAINT [DF_BillDisagg_NewDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FactBillDisagg] ADD CONSTRAINT [PK_FactBillDisagg] PRIMARY KEY CLUSTERED  ([ClientId], [CustomerId], [AccountId], [PremiseId], [BillDate], [EndUseKey], [ApplianceKey], [CommodityKey], [PeriodId]) WITH (FILLFACTOR=50, STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
