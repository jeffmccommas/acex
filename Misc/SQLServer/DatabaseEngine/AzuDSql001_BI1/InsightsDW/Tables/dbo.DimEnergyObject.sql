CREATE TABLE [dbo].[DimEnergyObject]
(
[EnergyObjectKey] [int] NOT NULL IDENTITY(1, 1),
[EnergyObjectDesc] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnergyObjectCategory] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResidentialApplianceID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimEnergyObject] ADD CONSTRAINT [PK_DimAppliance] PRIMARY KEY CLUSTERED  ([EnergyObjectKey]) ON [PRIMARY]
GO
