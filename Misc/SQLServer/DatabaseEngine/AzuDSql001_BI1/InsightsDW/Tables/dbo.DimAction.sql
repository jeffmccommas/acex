CREATE TABLE [dbo].[DimAction]
(
[ActionKey] [int] NOT NULL,
[ActionId] [int] NOT NULL,
[CMSActionKey] [varbinary] (50) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimAction] ADD CONSTRAINT [PK_DimAction] PRIMARY KEY CLUSTERED  ([ActionKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimAction] ON [dbo].[DimAction] ([ActionId]) ON [PRIMARY]
GO
