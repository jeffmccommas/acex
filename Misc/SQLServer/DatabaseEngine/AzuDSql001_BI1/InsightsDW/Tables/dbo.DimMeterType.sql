CREATE TABLE [dbo].[DimMeterType]
(
[MeterTypeKey] [int] NOT NULL IDENTITY(1, 1),
[MeterTypeDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MeterTypeName] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimMeterType] ADD CONSTRAINT [PK_MeterTypes] PRIMARY KEY CLUSTERED  ([MeterTypeKey]) ON [PRIMARY]
GO
