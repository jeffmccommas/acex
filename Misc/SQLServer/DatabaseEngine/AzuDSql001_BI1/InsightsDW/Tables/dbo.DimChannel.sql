CREATE TABLE [dbo].[DimChannel]
(
[ChannelKey] [int] NOT NULL,
[ChannelValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimChannel] ADD CONSTRAINT [PK_DimChannel] PRIMARY KEY CLUSTERED  ([ChannelKey]) ON [PRIMARY]
GO
