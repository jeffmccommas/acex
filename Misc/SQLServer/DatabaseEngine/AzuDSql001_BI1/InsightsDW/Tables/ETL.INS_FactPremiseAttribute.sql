CREATE TABLE [ETL].[INS_FactPremiseAttribute]
(
[AttributeId] [int] NOT NULL,
[OptionId] [int] NOT NULL,
[Value] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceID] [int] NOT NULL,
[CreateDate] [datetime] NULL,
[UpdateDate] [datetime] NULL,
[ETLLogID] [int] NOT NULL,
[TrackingID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
