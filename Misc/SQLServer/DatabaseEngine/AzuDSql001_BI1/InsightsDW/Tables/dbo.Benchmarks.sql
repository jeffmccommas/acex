CREATE TABLE [dbo].[Benchmarks]
(
[BenchmarkKey] [int] NOT NULL IDENTITY(1, 1),
[ClientId] [int] NOT NULL,
[PremiseKey] [int] NOT NULL,
[BillMonth] [date] NOT NULL,
[StartDateKey] [int] NOT NULL,
[EndDateKey] [int] NOT NULL,
[CommodityKey] [int] NOT NULL,
[BenchmarkGroupTypeKey] [int] NOT NULL,
[GroupId] [int] NOT NULL,
[GroupCount] [int] NOT NULL,
[MyUsage] [decimal] (18, 2) NULL,
[AverageUsage] [decimal] (18, 2) NULL,
[EfficientUsage] [decimal] (18, 2) NULL,
[UOMKey] [int] NOT NULL,
[MyCost] [decimal] (18, 2) NULL,
[AverageCost] [decimal] (18, 2) NULL,
[EfficientCost] [decimal] (18, 2) NULL,
[CostCurrencyCost] [int] NOT NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_Benchmarks_DateCreated] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Benchmarks] ADD CONSTRAINT [PK_Benchmarks] PRIMARY KEY CLUSTERED  ([BenchmarkKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Benchmarks] ON [dbo].[Benchmarks] ([ClientId], [PremiseKey], [BillMonth], [CommodityKey], [BenchmarkGroupTypeKey]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_Benchmarks_Premisekey] ON [dbo].[Benchmarks] ([PremiseKey]) INCLUDE ([AverageCost], [AverageUsage], [BenchmarkGroupTypeKey], [CommodityKey], [EfficientCost], [EfficientUsage], [EndDateKey], [GroupCount], [MyCost], [MyUsage], [StartDateKey]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
