CREATE TABLE [dbo].[BenchmarkErrorType]
(
[ErrorTypeKey] [int] NOT NULL IDENTITY(1, 1),
[ErrorTypeValue] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BenchmarkErrorType] ADD CONSTRAINT [PK_BenchmarkErrorType] PRIMARY KEY CLUSTERED  ([ErrorTypeKey]) ON [PRIMARY]
GO
