CREATE TABLE [dbo].[DimPostalCode]
(
[PostalCodeKey] [int] NOT NULL IDENTITY(1, 1),
[CityKey] [int] NOT NULL,
[PostalCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimPostalCode] ADD CONSTRAINT [PK_DimPostalCode] PRIMARY KEY CLUSTERED  ([PostalCodeKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimPostalCode] ON [dbo].[DimPostalCode] ([PostalCode], [CityKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimPostalCode] ADD CONSTRAINT [FK_DimPostalCode_DimCity] FOREIGN KEY ([CityKey]) REFERENCES [dbo].[DimCity] ([CityKey])
GO
