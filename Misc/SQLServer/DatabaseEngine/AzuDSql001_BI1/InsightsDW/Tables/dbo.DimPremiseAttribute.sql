CREATE TABLE [dbo].[DimPremiseAttribute]
(
[AttributeKey] [int] NOT NULL IDENTITY(1, 1),
[AttributeId] [int] NULL,
[CMSAttributeKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ETLLogId] [int] NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrackingDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimPremiseAttribute] ADD CONSTRAINT [PK_DimPremiseAttribute] PRIMARY KEY CLUSTERED  ([AttributeKey]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimPremiseAttribute] ON [dbo].[DimPremiseAttribute] ([AttributeId]) ON [PRIMARY]
GO
