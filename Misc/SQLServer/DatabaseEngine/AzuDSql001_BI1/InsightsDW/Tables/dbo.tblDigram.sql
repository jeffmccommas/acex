CREATE TABLE [dbo].[tblDigram]
(
[digramID] [int] NOT NULL IDENTITY(1, 1),
[digram] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
