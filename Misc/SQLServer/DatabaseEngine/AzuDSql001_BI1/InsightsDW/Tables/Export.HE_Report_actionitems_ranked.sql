CREATE TABLE [Export].[HE_Report_actionitems_ranked]
(
[ActionItemKey] [int] NULL,
[ActionItemDesc] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasureId] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnergyObjectDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PremiseKey] [int] NOT NULL,
[RefCost] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefSavings] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rank_id] [int] NULL,
[level_num] [int] NULL,
[valid_level1] [int] NULL
) ON [PRIMARY]
GO
