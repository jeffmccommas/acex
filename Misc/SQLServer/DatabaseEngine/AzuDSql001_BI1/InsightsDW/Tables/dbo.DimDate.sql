CREATE TABLE [dbo].[DimDate]
(
[DateKey] [int] NOT NULL,
[FullDateAlternateKey] [date] NULL,
[DateName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Year] [datetime] NULL,
[YearName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Quarter] [datetime] NULL,
[QuarterName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Month] [datetime] NULL,
[MonthName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Week] [datetime] NULL,
[WeekName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DayOfYear] [int] NULL,
[DayOfYearName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DayOfQuarter] [int] NULL,
[DayOfQuarterName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DayOfMonth] [int] NULL,
[DayOfMonthName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DayOfWeek] [int] NULL,
[DayOfWeekName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WeekOfYear] [int] NULL,
[WeekOfYearName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MonthOfYear] [int] NULL,
[MonthOfYearName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MonthOfQuarter] [int] NULL,
[MonthOfQuarterName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuarterOfYear] [int] NULL,
[QuarterOfYearName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimDate] ADD CONSTRAINT [PK_DimDate] PRIMARY KEY CLUSTERED  ([DateKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimDate] ON [dbo].[DimDate] ([FullDateAlternateKey]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', NULL, NULL
GO
EXEC sp_addextendedproperty N'DSVTable', N'DimDate', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', NULL, NULL
GO
EXEC sp_addextendedproperty N'Project', N'4b9dc531-2679-4fe9-a0e6-a8c7cf91c978', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', NULL, NULL
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DateName'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'DateName', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DateName'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfMonth'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'DayOfMonth', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfMonth'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfMonthName'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'DayOfMonthName', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfMonthName'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfQuarter'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'DayOfQuarter', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfQuarter'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfQuarterName'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'DayOfQuarterName', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfQuarterName'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfWeek'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'DayOfWeek', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfWeek'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfWeekName'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'DayOfWeekName', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfWeekName'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfYear'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'DayOfYear', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfYear'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfYearName'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'DayOfYearName', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'DayOfYearName'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'Month'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Month', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'Month'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'MonthName'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'MonthName', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'MonthName'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'MonthOfQuarter'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'MonthOfQuarter', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'MonthOfQuarter'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'MonthOfQuarterName'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'MonthOfQuarterName', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'MonthOfQuarterName'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'MonthOfYear'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'MonthOfYear', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'MonthOfYear'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'MonthOfYearName'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'MonthOfYearName', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'MonthOfYearName'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'Quarter'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Quarter', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'Quarter'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'QuarterName'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'QuarterName', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'QuarterName'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'QuarterOfYear'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'QuarterOfYear', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'QuarterOfYear'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'QuarterOfYearName'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'QuarterOfYearName', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'QuarterOfYearName'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'Week'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Week', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'Week'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'WeekName'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'WeekName', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'WeekName'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'WeekOfYear'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'WeekOfYear', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'WeekOfYear'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'WeekOfYearName'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'WeekOfYearName', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'WeekOfYearName'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'Year'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'Year', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'Year'
GO
EXEC sp_addextendedproperty N'AllowGen', N'True', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'YearName'
GO
EXEC sp_addextendedproperty N'DSVColumn', N'YearName', 'SCHEMA', N'dbo', 'TABLE', N'DimDate', 'COLUMN', N'YearName'
GO
