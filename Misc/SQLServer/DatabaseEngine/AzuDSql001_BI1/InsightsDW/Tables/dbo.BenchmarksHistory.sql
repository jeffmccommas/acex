CREATE TABLE [dbo].[BenchmarksHistory]
(
[BenchmarkHistoryKey] [int] NOT NULL IDENTITY(1, 1),
[ClientId] [int] NOT NULL,
[PremiseKey] [int] NOT NULL,
[BillMonth] [date] NOT NULL,
[StartDateKey] [int] NOT NULL,
[EndDateKey] [int] NOT NULL,
[CommodityKey] [int] NOT NULL,
[BenchmarkGroupTypeKey] [int] NOT NULL,
[GroupId] [int] NOT NULL,
[GroupCount] [int] NOT NULL,
[MyUsage] [decimal] (18, 2) NULL,
[AverageUsage] [decimal] (18, 2) NULL,
[EfficientUsage] [decimal] (18, 2) NULL,
[UOMKey] [int] NOT NULL,
[MyCost] [decimal] (18, 2) NULL,
[AverageCost] [decimal] (18, 2) NULL,
[EfficientCost] [decimal] (18, 2) NULL,
[CostCurrencyCost] [int] NOT NULL,
[DateCreated] [datetime] NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_BenchmarksHistory_DateUpdated] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BenchmarksHistory] ADD CONSTRAINT [PK_BenchmarksHistory] PRIMARY KEY CLUSTERED  ([BenchmarkHistoryKey]) ON [PRIMARY]
GO
