CREATE TABLE [dbo].[factEventAdditionalInfo]
(
[EventDetailKey] [bigint] NOT NULL,
[EventInfoKey] [int] NOT NULL,
[EventInfoValue] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPrimaryInfo] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[factEventAdditionalInfo] ADD CONSTRAINT [PK_factEventAdditionalInfo] PRIMARY KEY CLUSTERED  ([EventDetailKey], [EventInfoKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[factEventAdditionalInfo] ADD CONSTRAINT [FK_factEventAdditionalInfo_factEventDetails] FOREIGN KEY ([EventDetailKey]) REFERENCES [dbo].[factEventDetails] ([EventDetailKey])
GO
ALTER TABLE [dbo].[factEventAdditionalInfo] ADD CONSTRAINT [FK_factEventAdditionalInfo_DimEventInfo] FOREIGN KEY ([EventInfoKey]) REFERENCES [dbo].[DimEventInfo] ([EventInfoKey])
GO
