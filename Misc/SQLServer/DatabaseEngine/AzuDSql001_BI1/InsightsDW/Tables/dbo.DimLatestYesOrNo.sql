CREATE TABLE [dbo].[DimLatestYesOrNo]
(
[YesOrNoKey] [tinyint] NOT NULL,
[YesOrNoValue] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimLatestYesOrNo] ADD CONSTRAINT [PK_DimLatestYesOrNo] PRIMARY KEY CLUSTERED  ([YesOrNoKey]) ON [PRIMARY]
GO
