CREATE TABLE [dbo].[DimEventType]
(
[EventTypeId] [int] NOT NULL,
[EventShortName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EventDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EventKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimEventType] ADD CONSTRAINT [PK_DimEventType] PRIMARY KEY CLUSTERED  ([EventTypeId]) ON [PRIMARY]
GO
