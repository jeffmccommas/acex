CREATE TABLE [dbo].[BenchmarkConfigurationDetail]
(
[ConfigurationDetailKey] [int] NOT NULL IDENTITY(1, 1),
[ConfigurationKey] [int] NOT NULL,
[StartOperator] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_BenchmarkConfigurationDetail_Operator1] DEFAULT ('='),
[StartValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EndOperator] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_BenchmarkConfigurationDetail_Operator2] DEFAULT ('='),
[EndValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MapValue] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BenchmarkConfigurationDetail] ADD CONSTRAINT [PK_BenchmarkConfigurationDetail] PRIMARY KEY CLUSTERED  ([ConfigurationDetailKey]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BenchmarkConfigurationDetail] ADD CONSTRAINT [FK_BenchmarkConfigurationDetail_BenchmarkConfiguration] FOREIGN KEY ([ConfigurationKey]) REFERENCES [dbo].[BenchmarkConfiguration] ([ConfigurationKey])
GO
