CREATE TABLE [dbo].[DimServicePoint]
(
[ServicePointKey] [int] NOT NULL IDENTITY(1, 1),
[PremiseKey] [int] NOT NULL,
[ServicePointId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientId] [int] NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceId] [int] NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrackingDate] [datetime] NULL,
[IsInferred] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimServicePoint] ADD CONSTRAINT [PK_DimServicePoint] PRIMARY KEY CLUSTERED  ([ServicePointKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimServicePoint_Client] ON [dbo].[DimServicePoint] ([ClientId]) INCLUDE ([PremiseKey], [ServicePointId], [ServicePointKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimServicePoint_Premise_Client] ON [dbo].[DimServicePoint] ([PremiseKey], [ClientId]) INCLUDE ([ServicePointId], [ServicePointKey]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimServicePoint2] ON [dbo].[DimServicePoint] ([PremiseKey], [ServicePointId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimServicePoint] ON [dbo].[DimServicePoint] ([ServicePointId], [PremiseKey]) ON [PRIMARY]
GO
