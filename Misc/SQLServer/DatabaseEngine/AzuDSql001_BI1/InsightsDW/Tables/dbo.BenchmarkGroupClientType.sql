CREATE TABLE [dbo].[BenchmarkGroupClientType]
(
[BenchmarkGroupClientTypeKey] [int] NOT NULL IDENTITY(1, 1),
[ClientId] [int] NOT NULL,
[BenchmarkGroupTypeKey] [int] NOT NULL,
[ConfigurationId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BenchmarkGroupClientType] ADD CONSTRAINT [PK_BenchmarkGroupClientType] PRIMARY KEY CLUSTERED  ([BenchmarkGroupClientTypeKey]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BenchmarkGroupClientType] ON [dbo].[BenchmarkGroupClientType] ([ClientId], [BenchmarkGroupTypeKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BenchmarkGroupClientType] ADD CONSTRAINT [FK_BenchmarkGroupClientType_BenchmarkGroupType] FOREIGN KEY ([BenchmarkGroupTypeKey]) REFERENCES [dbo].[BenchmarkGroupType] ([BenchmarkGroupTypeKey])
GO
