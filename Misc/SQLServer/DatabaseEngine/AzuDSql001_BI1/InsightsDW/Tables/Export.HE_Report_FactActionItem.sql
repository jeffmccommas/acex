CREATE TABLE [Export].[HE_Report_FactActionItem]
(
[ActionItemKey] [int] NOT NULL,
[PremiseKey] [int] NOT NULL,
[EnergyObjectKey] [int] NOT NULL,
[Payback] [decimal] (18, 4) NOT NULL,
[CO2Savings] [decimal] (18, 4) NOT NULL,
[ROI] [decimal] (18, 4) NOT NULL,
[AnnualUsageElectric] [decimal] (18, 4) NOT NULL,
[AnnualUsageGas] [decimal] (18, 4) NOT NULL,
[AnnualUsageWater] [decimal] (18, 4) NOT NULL,
[AnnualUsagePropane] [decimal] (18, 4) NOT NULL,
[AnnualUsageOil] [decimal] (18, 4) NOT NULL,
[AnnualUsageWood] [decimal] (18, 4) NOT NULL,
[UsageSavingsElectric] [decimal] (18, 4) NOT NULL,
[UsageSavingsGas] [decimal] (18, 4) NOT NULL,
[UsageSavingsWater] [decimal] (18, 4) NOT NULL,
[UsageSavingsPropane] [decimal] (18, 4) NULL,
[UsageSavingsOil] [decimal] (18, 4) NOT NULL,
[UsageSavingsWood] [decimal] (18, 4) NOT NULL,
[DollarSavingsElectric] [decimal] (18, 4) NOT NULL,
[DollarSavingsGas] [decimal] (18, 4) NOT NULL,
[DollarSavingsWater] [decimal] (18, 4) NOT NULL,
[DollarSavingsPropane] [decimal] (18, 4) NOT NULL,
[DollarSavingsOil] [decimal] (18, 4) NOT NULL,
[DollarSavingsWood] [decimal] (18, 4) NOT NULL,
[Cost] [decimal] (18, 4) NOT NULL,
[DateUpdatedKey] [int] NOT NULL,
[StatusKey] [int] NOT NULL,
[Winter] [bit] NULL,
[Spring] [bit] NULL,
[Summer] [bit] NULL,
[Fall] [bit] NULL
) ON [PRIMARY]
GO
