CREATE TABLE [dbo].[DimPremise]
(
[PremiseKey] [int] NOT NULL IDENTITY(1, 1),
[PostalCodeKey] [int] NOT NULL,
[CityKey] [int] NOT NULL,
[SourceKey] [int] NOT NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Street1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateProvince] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GasService] [int] NULL,
[ElectricService] [int] NULL,
[WaterService] [int] NULL,
[CreateDate] [datetime] NULL,
[UpdateDate] [datetime] NULL,
[ClientId] [int] NOT NULL,
[GeoLocation] [sys].[geography] NULL,
[SourceId] [int] NULL,
[ETL_LogId] [int] NULL,
[IsInferred] [tinyint] NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrackingDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimPremise] ADD CONSTRAINT [PK_DimPremise] PRIMARY KEY CLUSTERED  ([PremiseKey]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DimPremise] ON [dbo].[DimPremise] ([ClientId], [PremiseId], [AccountId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimPremise] WITH NOCHECK ADD CONSTRAINT [FK_DimPremise_DimCity] FOREIGN KEY ([CityKey]) REFERENCES [dbo].[DimCity] ([CityKey])
GO
ALTER TABLE [dbo].[DimPremise] WITH NOCHECK ADD CONSTRAINT [FK_DimPremise_DimPostalCode] FOREIGN KEY ([PostalCodeKey]) REFERENCES [dbo].[DimPostalCode] ([PostalCodeKey])
GO
ALTER TABLE [dbo].[DimPremise] WITH NOCHECK ADD CONSTRAINT [FK_DimPremise_DimSource] FOREIGN KEY ([SourceKey]) REFERENCES [dbo].[DimSource] ([SourceKey])
GO
