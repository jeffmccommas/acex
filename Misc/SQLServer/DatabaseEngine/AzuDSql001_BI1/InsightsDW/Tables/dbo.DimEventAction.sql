CREATE TABLE [dbo].[DimEventAction]
(
[EventActionKey] [int] NOT NULL,
[EventClassKey] [int] NOT NULL,
[EventActionValue] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimEventAction] ADD CONSTRAINT [PK_DimEventAction] PRIMARY KEY CLUSTERED  ([EventActionKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DimEventAction] ADD CONSTRAINT [FK_DimEventAction_DimEventClass] FOREIGN KEY ([EventClassKey]) REFERENCES [dbo].[DimEventClass] ([EventClassKey])
GO
