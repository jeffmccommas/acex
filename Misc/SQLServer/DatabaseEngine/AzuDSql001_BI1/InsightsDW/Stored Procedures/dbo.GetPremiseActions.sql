SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ubaid Tariq
-- Create date: 1/5/2015
-- Description:	Returns all actions for customer
-- =============================================
CREATE PROCEDURE [dbo].[GetPremiseActions]
    @ClientID INT ,
    @CustomerID VARCHAR(50)
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT  AccountID
        FROM    dbo.DimPremise dp WITH ( NOLOCK )
                INNER JOIN dbo.FactCustomerPremise fcp WITH ( NOLOCK ) ON dp.PremiseKey = fcp.PremiseKey
                INNER JOIN dbo.DimCustomer dc WITH ( NOLOCK ) ON dc.CustomerKey = fcp.CustomerKey
        WHERE   dc.ClientID = @ClientID
                AND dc.CustomerId = @CustomerID
        GROUP BY AccountID

        SELECT  * ,
                CONVERT(VARCHAR(MAX), ISNULL(STUFF(( SELECT TOP 100 PERCENT
                                                            ';'
                                                            + t2.ActionDataKey
                                                            + ':'
                                                            + t2.ActionDataValue
                                                     FROM   insightsdw.dbo.factActionData
                                                            AS t2
                                                     WHERE  t2.ActionDetailKey = p.ActionDetailKey
                                                     ORDER BY t2.ActionDataKey ,
                                                            t2.ActionDataValue
                                                   FOR
                                                     XML PATH('')
                                                   ), 1, 1, ''), '')) AS AdditionalInfo
        FROM    ( SELECT    fpa.ActionDetailKey ,
                            CONVERT(VARCHAR(10), fpa.ClientID) AS ClientID ,
                            dc.CustomerID ,
                            fpa.PremiseID ,
                            fpa.AccountID ,
                            ActionKey ,
                            SubActionKey ,
                            tas.ActionStatusKey AS ActionStatus ,
                            StatusDate AS ActionStatusDate ,
                            ds.SourceDesc AS SourceKey ,
                            fpa.TrackingDate ,
                            ROW_NUMBER() OVER ( PARTITION BY fpa.PremiseKey,
                                                ActionKey, SubActionKey ORDER BY fpa.TrackingDate DESC ) AS SortId
                  FROM      dbo.factAction fpa WITH ( NOLOCK )
                            INNER JOIN dbo.DimPremise dp ON dp.PremiseKey = fpa.PremiseKey
                            INNER JOIN dbo.FactCustomerPremise fcp ON fcp.PremiseKey = dp.PremiseKey
                            INNER JOIN dbo.DimCustomer dc ON dc.CustomerKey = fcp.CustomerKey
                            INNER JOIN dbo.DimSource ds ON ds.SourceKey = fpa.SourceKey
                            INNER JOIN InsightsMetadata.cm.TypeActionStatus tas ON tas.ActionStatusID = StatusKey
                  WHERE     dc.ClientId = @ClientID
                            AND dc.CustomerId = @CustomerID
                ) p
        WHERE   SortId = 1
        ORDER BY p.ClientID ,
                p.PremiseID ,
                p.AccountID ,
                p.ActionKey ,
                p.SubActionKey
		 


    END







GO
