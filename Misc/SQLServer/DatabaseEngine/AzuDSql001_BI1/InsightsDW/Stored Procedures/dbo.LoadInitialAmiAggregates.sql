SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Wayne
-- Create date: 10.13.14
-- Description:	Produces a table of ami aggrgates used for ami reporting
--
-- This proc is for inital loading. A permanent proc will be
-- run at end of weekly ami import routine to recalculate on 
-- newly added ami data for premises
-- for a month/year
-- =============================================
-- Changed by:	Wayne
-- Change date: 10.13.2014
-- Description:	v 1.1.0
--	
-- changed to find last occurance in period for a high or low value
-- originally was finding first		
-- =============================================
CREATE PROCEDURE [dbo].[LoadInitialAmiAggregates]
as
BEGIN
	insert AmiAggregate
	select s.*
	,CAST(CAST(mindate AS CHAR(8)) AS DATETIME) as [DateOfMinInPeriod]
	,CAST(CAST(maxdate AS CHAR(8)) AS DATETIME) as [DateOfMaxInPeriod]
	 from
	(
	select
	amikey
	,GROUPING_ID(amikey,premiseid
					, DATEPART(weekday,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME))
					, DATEPART(Week,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME))
					, DATEPART(month,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME))
					, DATEPART(Year,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME)) 
				) as AmiAggregateKeyType
	,premiseid
	,DATEPART(weekday,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME)) as DayOfWeek
	,Datepart(Week,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME)) as Week
	,Datepart(month,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME)) as Month
	,DATEpart(Year,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME)) as Year
	,min(TotalUnits) as DailyMinInPeriod
	,max(TotalUnits) as DailyMaxInPeriod
	,sum(TotalUnits) as TotalUsageInPeriod
	,cast(avg(TotalUnits) as decimal(18,4)) as AvgerageInPeriod
	,count(TotalUnits) as OccurrencesInPeriod
	from  factamidetail 
	group by amikey, premiseid,
	grouping sets

		(	cube(
				DATEPART(weekday,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME)),
				DATEPART(week,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME)) ,
				Datepart(month,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME)) ,
				Datepart(Year,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME))
			
				)
		)
	HAVING GROUPING_ID(amikey,premiseid
					, DATEPART(weekday,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME))
					, DATEPART(week,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME))
					, DATEPART(month,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME))
					, DATEPART(Year,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME)) 
				) IN (	14,  -- THE WHOLE YEAR
						12,  -- FOR THE MONTH IN THE YEAR
						10, -- FOR THE week IN THE YEAR
						6,  -- FOR THE DAY IN THE YEAR
						4   -- FOR THE DAY IN THE MONTH IN THE YEAR
						)
	/*
	ones we dont need 
	15, 7,11,3,5,9,1, 13  have null years
	2,8,0 all have week dim but we only want week within year
	*/
	) as s
	outer apply  (select top 1 f2.[DateKey] as mindate from factamidetail f2
	where f2.AmiKey=s.AmiKey and s.DailyMinInPeriod=f2.TotalUnits
	order by  datekey desc
	)
	as a
	outer apply  (select top 1 f2.[DateKey] as maxdate from factamidetail f2
	where f2.AmiKey=s.AmiKey and s.DailyMaxInPeriod=f2.TotalUnits
	order by  datekey desc
	)
	as b
END
GO
