SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Sunil
-- Create date: unknown
-- Description:	Produces a csv extract referred to as Home Energy Report
-- =============================================
-- Changed by:	Wayne
-- Change date: 10.13.2014
-- Description:	v 1.7.0	
-- Eliminate negative values in any PCT column		
-- =============================================
-- Changed by:	Wayne
-- Change date: 11.24.2014
-- Description:	v 1.33.0	
-- new code added in selection of column
-- from client table where clause
-- which now evaluate the channel parameter
-- and the "email" or "print" column (new columns) in client table
-- to determine if column is exported
-- =============================================
-- Changed by:	 Wayne
-- Change date: 12.18.2014
-- Description: v 1.29.0	
-- 
-- tweaked column select code
-- if channel ='all' treat as if it was "print" 
-- for selecting column from client table
-- to determine if column is exported
--
-- also added an ORDER BY clause to final output
-- to insure it is in premiseid order
-- =============================================
-- Changed by:	Wayne
-- Change date: 12.31.2014
-- Description:	v 1.33.0 (same version)
--	
--  Removed the WITH (NOLOCK)'s because
--  they were causing race conditions
-- =============================================
CREATE procedure [Export].[usp_HE_Report_Create_Staging]
@profile_name varchar (50) = 'SCG_Profile101',
@channel VARCHAR(5)	=	'email'
as
begin

	declare
		@clientID int, 
		@fuel_type varchar (100), 
		@energyobjectcategory_List varchar (1024) , 
		@period varchar (10) , 
		@season_List varchar (100) , 
		@enduse_List varchar (1024) , 
		@ordered_enduse_List varchar (1024), 
		@criteria_id1 varchar (50) , 
		@criteria_detail1 varchar (255)  , 
		@criteria_id2 varchar (50) , 
		@criteria_detail2 varchar (255) , 
		@criteria_id3 varchar (50) , 
		@criteria_detail3 varchar (255) , 
		@criteria_id4 varchar (50) , 
		@criteria_detail4 varchar (255) , 
		@criteria_id5 varchar (50) , 
		@criteria_detail5 varchar (255) , 
		@criteria_id6 varchar (50) , 
		@criteria_detail6 varchar (255), 
		@saveProfile char (1), 
		@verifySave char (1),
		@process_id char (1)

	SELECT
		@clientID = clientID
		, @fuel_type = fuel_type
		, @energyobjectcategory_List = energyobjectcategory_List
		, @period = period
		, @season_List = season_List
		, @enduse_List = enduse_List
		, @ordered_enduse_List = ordered_enduse_List
		, @criteria_id1 = criteria_id1
		, @criteria_detail1 = criteria_detail1
		, @criteria_id2 = criteria_id2
		, @criteria_detail2 = criteria_detail2
		, @criteria_id3 = criteria_id3
		, @criteria_detail3 = criteria_detail3
		, @criteria_id4 = criteria_id4
		, @criteria_detail4 = criteria_detail4
		, @criteria_id5 = criteria_id5
		, @criteria_detail5 = criteria_detail5
		, @criteria_id6 = criteria_id6
		, @criteria_detail6 = criteria_detail6
	  FROM [export].[home_energy_report_criteria_profile] 
	where [profile_name] = @profile_name

	if not exists (select * from [export].[home_energy_report_client] where clientID = @clientID and lower (export_display_flag) = 'y')
	begin
		select	@clientID = 0
	end

	declare @num_energyObjectCategory int, @count_cols int

	set nocount on

	if object_id ('tempdb..#energyObjectCategory_list') is not null drop table #energyObjectCategory_list
	create table #energyObjectCategory_list ([energyObjectCategory] varchar (50), TotalDollars decimal (18, 2))
	insert #energyObjectCategory_list ([energyObjectCategory])
	select cast (param as varchar (50))
	from [dbo].[ufn_split_values] (@energyObjectCategory_list, ',')
	select	@num_energyObjectCategory = count (*) from #energyObjectCategory_list

	declare @sqlcmd varchar (max), @sqlcmd_cursor varchar (8000), @column_name varchar (128), @only_column_name varchar (128), @column_count int = 1
		, @tablename varchar (128) = '[export].[home_energy_report_staging_allcolumns]' 
		-- + '_' + convert (char (8), getdate (), 112) + '_' + replace (convert (char (11), getdate (), 114), ':', '')


/*
--	++++++++++++++++++++++++++++++++++++++++++++++
--	create staging table
--	++++++++++++++++++++++++++++++++++++++++++++++
 */

	select	@tablename = '[export].[home_energy_report_staging]' 
		-- + '_' + convert (char (8), getdate (), 112) + '_' + replace (convert (char (11), getdate (), 114), ':', '')

	select	@sqlcmd = 'if object_id (''' + @tablename + ''') is not null drop table ' + @tablename
	exec (@sqlcmd)



	select	@sqlcmd_cursor = '
	declare @channel VARCHAR(5);
	set @channel=''' + @channel + ''';  
	declare	columnlist_cursor cursor for
	select	''['' + t1.[Field Name] + ''] varchar (50) NULL'', t1.[Field Name]
	from	[export].[home_energy_report_client] t1  
	where	lower (' + @fuel_type + ') = ''x''
	and		lower (' + @period + ') = ''x''
	and		lower (export_display_flag) = ''y''
	and		clientID = ' + convert (varchar, @clientID) + '
	and		1=case when lower(@channel) =''email'' AND  lower(t1.Email_Channel) = ''x'' then 1
				when lower(@channel) =''print'' AND  lower(t1.Print_Channel) = ''x'' then 1 
				when lower(@channel) =''all'' AND  lower(t1.Print_Channel) = ''x'' then 1 
				else 0 end
	order by export_column_order
	'
	select	@sqlcmd = 'create table ' + @tablename + ' ('

	exec (@sqlcmd_cursor)

	open columnlist_cursor

	select @column_count = 1
	fetch next from columnlist_cursor into @column_name, @only_column_name

	while @@FETCH_STATUS = 0
	begin

		if @column_count = 1
		begin
			select	@sqlcmd = @sqlcmd + char (13) + char (10) + @column_name
		end
		else
		begin
			if @only_column_name like '%EnergyObjectCategory%'
			begin
					if isnumeric (right (@only_column_name, 2)) = 1
					begin
						if convert (int, right (@only_column_name, 2)) <= @num_energyObjectCategory
							select	@sqlcmd = @sqlcmd + char (13) + char (10) + ', ' + @column_name
					end
					else
					begin
						if convert (int, right (@only_column_name, 1)) <= @num_energyObjectCategory
							select	@sqlcmd = @sqlcmd + char (13) + char (10) + ', ' + @column_name
					end
			end
			else
			begin
				select	@sqlcmd = @sqlcmd + char (13) + char (10) + ', ' + @column_name
			end
		end

		fetch next from columnlist_cursor into @column_name, @only_column_name
		select	@column_count += 1

	end

	close columnlist_cursor
	deallocate columnlist_cursor

	select	@sqlcmd = @sqlcmd + char (13) + char (10) + ')'


	exec (@sqlcmd)

/*
--	++++++++++++++++++++++++++++++++++++++++++++++
--	insert header row
--	++++++++++++++++++++++++++++++++++++++++++++++
 */


	select	@sqlcmd = 'insert ' + @tablename + ' 
		( '


	select	@sqlcmd_cursor = '
	declare @channel VARCHAR(5);
	set @channel=''' + @channel + ''';
	declare	columnlist_cursor cursor for
	select	t1.[Field Name], t1.[Field Name]
	from	[export].[home_energy_report_client] t1   
	where	lower (' + @fuel_type + ') = ''x''
	and		lower (' + @period + ') = ''x''
	and		lower (export_display_flag) = ''y''
	and		clientID = ' + convert (varchar, @clientID) + '
     and		1=case when lower(@channel) =''email'' AND  lower(t1.Email_Channel) = ''x'' then 1
			 when lower(@channel) =''print'' AND  lower(t1.Print_Channel) = ''x'' then 1 
			 when lower(@channel) =''all'' AND  lower(t1.Print_Channel) = ''x'' then 1
			 else 0 end
	order by export_column_order
	'

	exec (@sqlcmd_cursor)

	open columnlist_cursor
	select @column_count = 1

	fetch next from columnlist_cursor into @column_name, @only_column_name

	while @@FETCH_STATUS = 0
	begin

		if @column_count = 1
		begin
			select	@sqlcmd = @sqlcmd + char (13) + char (10) + '[' + @column_name + ']'
		end
		else
		begin
			if @only_column_name like '%EnergyObjectCategory%'
			begin
					if isnumeric (right (@only_column_name, 2)) = 1
					begin
						if convert (int, right (@only_column_name, 2)) <= @num_energyObjectCategory
							select	@sqlcmd = @sqlcmd + char (13) + char (10) + ', [' + @column_name + ']'
					end
					else
					begin
						if convert (int, right (@only_column_name, 1)) <= @num_energyObjectCategory
							SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + ', [' + @column_name + ']'
					END
			END
			ELSE
			BEGIN
				SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + ', [' + @column_name + ']'
			END
		END

		FETCH NEXT FROM columnlist_cursor INTO @column_name, @only_column_name
		SELECT	@column_count += 1

	END

	CLOSE columnlist_cursor
	DEALLOCATE columnlist_cursor

	SELECT @sqlcmd = @sqlcmd + ')'



	/*
	--	++++++++++++++++++++++++++++++++++++++++++++++
	--	declare a cursor to go get columns headers for report
	--	This is just line 1 of report
	--	++++++++++++++++++++++++++++++++++++++++++++++
	*/
	SELECT	@sqlcmd = @sqlcmd + '
	select '


	SELECT	@sqlcmd_cursor = '
	declare @channel VARCHAR(5);
	set @channel=''' + @channel + ''';
	declare	columnlist_cursor cursor for
	select	/*t1.[Field Name]*/ t1.export_column_name, t1.[Field Name]
	from	[export].[home_energy_report_client] t1  
	where	lower (' + @fuel_type + ') = ''x''
	and		lower (' + @period + ') = ''x''
	and		lower (export_display_flag) = ''y''
	and		clientID = ' + CONVERT (VARCHAR, @clientID) + '
     and		1=case when lower(@channel) =''email'' AND  lower(t1.Email_Channel) = ''x'' then 1
			 when lower(@channel) =''print'' AND  lower(t1.Print_Channel) = ''x'' then 1 
			 when lower(@channel) =''all'' AND  lower(t1.Print_Channel) = ''x'' then 1
			 else 0 end
	order by export_column_order
	'

	EXEC (@sqlcmd_cursor)

	/*
	--	++++++++++++++++++++++++++++++++++++++++++++++
	--	loop thru cursor
	--	This is just line 1 of report
	--	formatting is done in loop too
	--	++++++++++++++++++++++++++++++++++++++++++++++
	*/

	OPEN columnlist_cursor
	SELECT @column_count = 1

	FETCH NEXT FROM columnlist_cursor INTO @column_name, @only_column_name

	WHILE @@FETCH_STATUS = 0
	BEGIN

		IF @column_count = 1
		BEGIN
			SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + '''' + @column_name + ''''
		END
		ELSE
		BEGIN
			IF @only_column_name LIKE '%EnergyObjectCategory%'
			BEGIN
					IF ISNUMERIC (RIGHT (@only_column_name, 2)) = 1
					BEGIN
						IF CONVERT (INT, RIGHT (@only_column_name, 2)) <= @num_energyObjectCategory
							SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + ', ''' + @column_name + ''''
					END
					ELSE
					BEGIN
						IF CONVERT (INT, RIGHT (@only_column_name, 1)) <= @num_energyObjectCategory
							SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + ', ''' + @column_name + ''''
					END
			END
			ELSE
			BEGIN
				SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + ', ''' + @column_name + ''''
			END
		END

		FETCH NEXT FROM columnlist_cursor INTO @column_name, @only_column_name
		SELECT	@column_count += 1

	END

	CLOSE columnlist_cursor
	DEALLOCATE columnlist_cursor

	EXEC (@sqlcmd)


	/*
	--	++++++++++++++++++++++++++++++++++++++++++++++
	--	Done with header row
	--	now
	--	populate data rows
	--	++++++++++++++++++++++++++++++++++++++++++++++
	 */

	SELECT	@tablename = '[export].[home_energy_report_staging]' 

	/*
	--	++++++++++++++++++++++++++++++++++++++++++++++
	--	declare a cursor to go get columns headers for report
	--	This is for all data rows of the report
	--	++++++++++++++++++++++++++++++++++++++++++++++
	*/

	SELECT	@sqlcmd_cursor = '
	declare @channel VARCHAR(5);
	set @channel=''' + @channel + ''';
	declare	columnlist_cursor cursor for
	select	''['' + t1.[Field Name] + '']'', t1.[Field Name]
	from	[export].[home_energy_report_client] t1  
	where	lower (' + @fuel_type + ') = ''x''
	and		lower (' + @period + ') = ''x''
	and		lower (export_display_flag) = ''y''
	and		clientID = ' + CONVERT (VARCHAR, @clientID) + '
	and		1=case when lower(@channel) =''email'' AND  lower(t1.Email_Channel) = ''x'' then 1
			 when lower(@channel) =''print'' AND  lower(t1.Print_Channel) = ''x'' then 1 
			 when lower(@channel) =''all'' AND  lower(t1.Print_Channel) = ''x'' then 1
			 else 0 end
	order by export_column_order
	'

	EXEC (@sqlcmd_cursor)

	
	/*
	--	++++++++++++++++++++++++++++++++++++++++++++++
	--	loop thru cursor
	--	This is all data rows of report
	--	formatting is done in loop too
	--	++++++++++++++++++++++++++++++++++++++++++++++
	*/

	OPEN columnlist_cursor
	SELECT	@sqlcmd = 'insert [export].[home_energy_report_staging]
		select '

	--	++++++++++++++++++++++++++++
	--	building  the insert/select starts here
	--	+++++++++++++++++++++++++++++

	SELECT @column_count = 1
	FETCH NEXT FROM columnlist_cursor INTO @column_name, @only_column_name

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @column_count = 1
		BEGIN
			SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + 's2.' + @column_name
		END
		ELSE
		BEGIN
			IF @only_column_name LIKE '%EnergyObjectCategory%'
				BEGIN
						IF ISNUMERIC (RIGHT (@only_column_name, 2)) = 1
						BEGIN
							IF CONVERT (INT, RIGHT (@only_column_name, 2)) <= @num_energyObjectCategory
							BEGIN
								IF @column_name LIKE '%cost%' OR @column_name LIKE '%dollar%' OR @column_name LIKE '%Percent%' OR @column_name LIKE '%pct%' OR @column_name LIKE '%savings%' OR @column_name LIKE '%usage%' 
									BEGIN
										if @only_column_name LIKE '%Percent%' OR @only_column_name LIKE '%pct%'
											Begin 
												--per kristen no negative values for percentages
												SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + ', abs(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(s2.' + @column_name + ')))) '
											end
										else
											Begin 
												SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + ', dbo.BankersRoundingStringtoWhole(ltrim(rtrim(s2.' + @column_name + '))) '
											end
									END
								ELSE
									BEGIN
										SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + ', s2.' + @column_name
									END
							END
						END
						ELSE
						BEGIN
							IF CONVERT (INT, RIGHT (@only_column_name, 1)) <= @num_energyObjectCategory
							BEGIN
								IF @column_name LIKE '%cost%' OR @column_name LIKE '%dollar%' OR @column_name LIKE '%Percent%' OR @column_name LIKE '%pct%' OR @column_name LIKE '%savings%' OR @column_name LIKE '%usage%' 
									BEGIN
											if  @column_name LIKE '%Percent%' OR @column_name LIKE '%pct%'
												--per kristen no negative values for percentages
												Begin
													SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + ',abs( dbo.BankersRoundingStringtoWhole(ltrim(rtrim(s2.' + @column_name +  ')))) '
												end
											else
												Begin
													SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + ', dbo.BankersRoundingStringtoWhole(ltrim(rtrim(s2.' + @column_name +  '))) '
												end
											END
										ELSE
											BEGIN
												SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + ', s2.' + @column_name
											END
									END
							END
			END
			ELSE
				BEGIN

					IF (@column_name LIKE '%cost%' OR @column_name LIKE '%dollar%'  OR @column_name LIKE '%savings%' OR @column_name LIKE '%usage%') and (@column_name not  like '%Percent%' and @column_name not LIKE '%pct%')
					BEGIN

						SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + ', dbo.BankersRoundingStringtoWhole(ltrim(rtrim(s2.' + @column_name +  '))) '

					END
						--per kristen no negative values for percentages
					ELSE IF @column_name  LIKE '%Percent%' OR @column_name LIKE '%pct%'
					BEGIN

						SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + ', abs(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(s2.' + @column_name +  ')))) '

					END
				ELSE
					BEGIN
						SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + ', s2.' + @column_name
					END
				END
		END

		FETCH NEXT FROM columnlist_cursor INTO @column_name, @only_column_name
		SELECT	@column_count += 1

	END

	CLOSE columnlist_cursor
	DEALLOCATE columnlist_cursor
	SELECT	@sqlcmd = @sqlcmd + CHAR (13) + CHAR (10) + '
	from [export].[home_energy_report_staging_allcolumns] s2  
	order by s2.premiseid' 

--	next line for testing only
--	SELECT @sqlcmd
-------------------

	EXEC (@sqlcmd)

SET NOCOUNT OFF
END



GO
