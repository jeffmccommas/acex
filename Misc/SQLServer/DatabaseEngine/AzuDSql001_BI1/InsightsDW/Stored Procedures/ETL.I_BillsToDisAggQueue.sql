SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
-- =============================================
-- Author:		Wayne
-- Create date:	3/30/2015
-- Description:	Add the bills that are being processed in this etl run
--				to the DisAggRecalcQueue for reprocessing in a later step
--				This allow for bills and disagg to be done in same etl run    
-- ==================================================================
-- Modified By:	Wayne
-- Date:			06/08/15
-- Description:	joins to a View that shows those who have the disagg attribute set to true
==================================================================
-- Author:		Wayne
-- Create date:	6/20/2015
-- Description:	changed to use a differnt attribute in 15.06 - calculateanalytics
--				also require a new view vcalculateanalyticsFlagTrue
-- =============================================
*/

CREATE PROCEDURE [ETL].[I_BillsToDisAggQueue]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;

		MERGE into InsightsBulk.[Holding].[DisAggReCalcQueue] AS T
		USING (SELECT a.ClientId,a.PremiseId, a.AccountId, a.CustomerId, a.SourceId, a.TrackingId, a.TrackingDate FROM 
			  (
				  SELECT DISTINCT dp.PremiseKey, dp.ClientId,dp.PremiseId, dp.AccountId, c.CustomerId, b.SourceId, b.TrackingId, b.TrackingDate,
				  ROW_NUMBER() OVER(PARTITION BY dp.PremiseId, dp.AccountId, c.CustomerId ORDER BY b.TrackingDate desc) AS latest 

				  FROM InsightsBulk.Holding.v_Billing b
				  INNER JOIN InsightsDW.dbo.DimPremise dp
				  ON dp.ClientId = b.ClientId AND dp.PremiseId = b.PremiseId AND  dp.AccountId = b.AccountId 
				   INNER JOIN	(
								  SELECT PremiseKey, CustomerKey, 
								  ROW_NUMBER() OVER(PARTITION BY premisekey ORDER BY datecreated desc) AS rnk 
								  from
								  [InsightsDW].[dbo].[FactCustomerPremise] fcp
							  ) AS fcp 	
					on dp.PremiseKey=fcp.premisekey and rnk=1
					INNER JOIN	[InsightsDW].[dbo].[DimCustomer] c	 			
					on c.customerkey=fcp.customerkey and rnk=1
				  WHERE b.ClientId = @ClientID  
		) AS a
		
		
		INNER JOIN insightsdw.[dbo].[vcalculateanalyticsFlagTrue] d
		ON d.premisekey=a.premisekey
		WHERE latest=1
		) AS s
	   ON s.ClientId  = t.clientid AND s.PremiseId = t.PremiseId AND  s.AccountId = t.AccountId 
	   WHEN MATCHED THEN UPDATE SET SourceId=  s.SourceId, TrackingId= s.TrackingId, TrackingDate=s.TrackingDate
	   WHEN NOT MATCHED THEN INSERT (
							  [ClientId]
							   ,[PremiseId]
							   ,[AccountId]
							   ,[CustomerId]
							   ,[SourceId]
							   ,[TrackingId]
							   ,[TrackingDate]
					   )
					   VALUES
                            (
						  s.ClientId,s.PremiseId, s.AccountId, s.CustomerId, s.SourceId, s.TrackingId, s.TrackingDate
					   );
					   
END;

GO
