
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/10/2014
-- Description:	
-- update 3/9/2016 - remove with (nolock) table hints - causing error in ETL "Table hints are not supported on queries that reference external tables"
-- =============================================
CREATE PROCEDURE [ETL].[I_FactPremiseAttributes] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @EffectiveDate DATETIME;
        SET @EffectiveDate = GETUTCDATE();

        INSERT  INTO [dbo].[FactPremiseAttribute]
                ( PremiseKey ,
                  OptionKey ,
                  SourceKey ,
                  ContentAttributeKey ,
                  ContentOptionKey ,
                  AttributeId ,
                  OptionId ,
                  Value ,
                  ClientID ,
                  PremiseID ,
                  AccountID ,
                  SourceID ,
                  EffectiveDateKey ,
                  EffectiveDate ,
                  ETLLogID ,
                  TrackingID ,
                  TrackingDate
                )
                SELECT  dp.PremiseKey ,
                        -1 ,
                        ds.SourceKey ,
                        tpa.ProfileAttributeKey ,
                        tpo.ProfileOptionKey ,
                        fpa.AttributeId ,
                        fpa.OptionId ,
                        fpa.Value ,
                        fpa.ClientID ,
                        fpa.PremiseID ,
                        fpa.AccountID ,
                        fpa.SourceID ,
                        CONVERT(VARCHAR(35), @EffectiveDate, 112) ,
                        @EffectiveDate ,
                        fpa.ETLLogID ,
                        fpa.TrackingID ,
                        fpa.TrackingDate
                FROM    ETL.INS_FactPremiseAttribute fpa 
                        INNER JOIN dbo.DimPremise dp ON dp.PremiseId = fpa.PremiseID
                                                              AND dp.AccountId = fpa.AccountID
                                                              AND dp.ClientId = fpa.ClientID
                        INNER JOIN dbo.DimClient dc ON dc.ClientId = fpa.ClientID
                        INNER JOIN dbo.DimSource ds ON ds.SourceId = fpa.SourceID
                        INNER JOIN cm.TypeProfileAttribute tpa
							ON tpa.ProfileAttributeID = fpa.AttributeId
                        INNER JOIN cm.TypeProfileOption tpo
							ON tpo.ProfileOptionID = fpa.OptionId
                WHERE   fpa.ClientID = @ClientID;

    END;


														



GO


