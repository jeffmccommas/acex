SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<LCF>
-- Create date: <11/18/2014>
-- Description:	<This is for BugFixDescription,,>
-- =============================================
CREATE PROCEDURE [Export].[GetUpdatedCustomersCount]
    (
      @clientid INT ,
      @processdate DATETIME
    )
AS
    BEGIN
        SELECT  COUNT(c.customerid)
        FROM    [dbo].[dimcustomer] c WITH ( NOLOCK )
                INNER JOIN [dbo].[factcustomerpremise] fcp WITH ( NOLOCK ) ON c.customerkey = fcp.customerkey
                INNER JOIN [dbo].[dimpremise] p WITH ( NOLOCK ) ON p.premisekey = fcp.premisekey
        WHERE   c.clientid = @clientid
                AND (c.updatedate > @processdate
                OR c.createdate > @processdate
                OR p.createdate > @processdate)
    END



GO
