SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/29/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[S_DimServicePoint]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;

		SELECT * 
		FROM [dbo].[DimServicePoint] 
		WHERE ClientId = @ClientID
		ORDER BY ClientId, ServicePointId


END





GO
