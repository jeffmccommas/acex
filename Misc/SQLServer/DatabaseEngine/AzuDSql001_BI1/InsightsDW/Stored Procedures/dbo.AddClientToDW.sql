SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
-- =======================================
-- Written by:	 Wayne
-- Date:		 04.29.15
-- Description: Adds a new client id to 
-- 			 dimClient table
-- =======================================
-- changed by:	 Wayne
-- Date:		 06.10.15
-- Description: checks if exists befor adding
-- =======================================
*/
create PROC [dbo].[AddClientToDW]
(
    @ClientId INT,
    @ClientName VARCHAR(50)
)
AS
BEGIN
    SET NOCOUNT ON;
    IF NOT EXISTS(SELECT * FROM [InsightsDW].[dbo].[DimClient] 
	   WHERE Clientid=@ClientId)
    BEGIN
	   INSERT [InsightsDW].[dbo].[DimClient]
			 ( 
			   [ClientId], 
			   [ClientName] 
			 )
	   VALUES  ( 
			   @ClientId, 
			   @ClientName
			 )
	   PRINT 'added'
    END
    ELSE
    BEGIN
	   PRINT 'already exists'
    END
END
GO
