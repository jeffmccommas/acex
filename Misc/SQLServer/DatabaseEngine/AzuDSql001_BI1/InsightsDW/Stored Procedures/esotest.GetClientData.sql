SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [esotest].[GetClientData] (@Clientid INT )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT TOP 100000 c.customerid,p.accountid,p.premiseid,p.postalcode ,cp.premisekey FROM dimcustomer c WITH (NOLOCK)
INNER JOIN factcustomerpremise cp WITH (NOLOCK)
ON c.customerkey = cp.CustomerKey
INNER JOIN dimpremise p WITH (NOLOCK)
ON p.premisekey = cp.premisekey
INNER JOIN factpremiseattribute fpa WITH (NOLOCK)
ON fpa.premisekey = p.premisekey
WHERE c.clientid = @clientid
AND fpa.contentattributekey = 'paperreport.analysisgroup.enrollmentstatus'
AND fpa.contentoptionkey = 'paperreport.analysisgroup.enrollmentstatus.enrolled'
--AND c.customerid = '110103550' -- pool user
--AND c.customerid = '1000009400'

ORDER BY c.customerid
END















GO
