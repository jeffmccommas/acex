SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 4/21/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_DimPremise]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;


		INSERT INTO [InsightsDW].[dbo].[DimPremise] (PostalCodeKey, 
													CityKey, 
													SourceKey, 
													PremiseId, 
													AccountId,
													Street1, 
													Street2, 
													City, 
													StateProvince, 
													Country, 
													PostalCode, 
													GasService, 
													ElectricService, 
													WaterService, 
													CreateDate,
													--UpdateDate,
													ClientId, 
													GeoLocation,
													SourceId,
													ETL_LogId,
													TrackingId,
													TrackingDate)
		SELECT  ISNULL(dpc.PostalCodeKey, -1),
				ISNULL(dc.CityKey, -1),
				ds.SourceKey,
				p.PremiseId,
				p.AccountId,
				p.Street1,  
				p.Street2, 
				p.City, 
				p.StateProvince,
				p.Country,
				p.PostalCode,
				p.GasService,
				p.ElectricService,
				p.WaterService,
				GETUTCDATE(),
				--ISNULL(p.UpdateDate, GETUTCDATE()),
				p.ClientId,
				NULL,
				p.SourceId,
				p.ETL_LogId,
				TrackingId,
				TrackingDate
		FROM [InsightsDW].[ETL].[INS_DimPremise] p WITH (NOLOCK) 
		INNER JOIN [InsightsDW].[dbo].[DimClient] cl WITH (NOLOCK) ON cl.ClientId = p.ClientId 
		INNER JOIN [InsightsDW].[dbo].[DimSource] ds WITH (NOLOCK) ON ds.SourceId = p.SourceId 
		LEFT JOIN [InsightsDW].[dbo].[DimCountryRegion] dcr WITH (NOLOCK) ON dcr.CountryRegionCode = p.Country
		LEFT JOIN [InsightsDW].[dbo].[DimStateProvince] dsp WITH (NOLOCK) ON dsp.CountryRegionKey = dcr.CountryRegionKey
														AND dsp.StateProvinceName = p.StateProvince
		LEFT JOIN [InsightsDW].[dbo].[DimCity] dc WITH (NOLOCK) ON dc.City = p.City
														AND  dc.StateProvinceKey = dsp.StateProvinceKey
		LEFT JOIN [InsightsDW].[dbo].[DimPostalCode] dpc WITH (NOLOCK) ON dpc.PostalCode = p.PostalCode
														AND dpc.CityKey = dc.CityKey
		WHERE p.ClientId = @ClientID 
				AND p.PostalCode IS NOT NULL


END





GO
