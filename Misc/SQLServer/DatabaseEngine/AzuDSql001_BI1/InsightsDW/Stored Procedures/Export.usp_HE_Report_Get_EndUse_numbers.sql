SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Sunil
-- Create date: unknown
-- Description:	part of home energy report stream
-- =============================================
-- Changed by:	Wayne
-- Change date: 9.24.2014
-- Description:	added a group by when inserting into #eo_cost table
-- =============================================
-- Changed by:	Wayne
-- Change date: 12.31.2014
-- Description:	v 1.33.0 (same version)
--	
--  Removed the WITH (NOLOCK)'s because
--  they were causing race conditions
-- =============================================
CREATE procedure [Export].[usp_HE_Report_Get_EndUse_numbers]
--declare
@profile_name varchar (50)  = 'SCG_Profile101'
as
begin

set nocount on

declare 
		@count int = 1
		, @fuel_type varchar (50), @sqlcmd varchar (8000)
		, @energyObjectCategory varchar (50)

	declare
		@clientID int, 
		@fuel_type_list varchar (100), 
		@energyobjectcategory_List varchar (1024) , 
		@period varchar (10) , 
		@season_List varchar (100) , 
		@enduse_List varchar (1024) , 
		@ordered_enduse_List varchar (1024), 
		@criteria_id1 varchar (50) , 
		@criteria_detail1 varchar (255)  , 
		@criteria_id2 varchar (50) , 
		@criteria_detail2 varchar (255) , 
		@criteria_id3 varchar (50) , 
		@criteria_detail3 varchar (255) , 
		@criteria_id4 varchar (50) , 
		@criteria_detail4 varchar (255) , 
		@criteria_id5 varchar (50) , 
		@criteria_detail5 varchar (255) , 
		@criteria_id6 varchar (50) , 
		@criteria_detail6 varchar (255), 
		@saveProfile char (1), 
		@verifySave char (1),
		@process_id char (1)

	SELECT
		@clientID = clientID
		, @fuel_type_list = fuel_type
		, @energyobjectcategory_List = energyobjectcategory_List
		, @period = period
		, @season_List = season_List
		, @enduse_List = enduse_List
		, @ordered_enduse_List = ordered_enduse_List
		, @criteria_id1 = criteria_id1
		, @criteria_detail1 = criteria_detail1
		, @criteria_id2 = criteria_id2
		, @criteria_detail2 = criteria_detail2
		, @criteria_id3 = criteria_id3
		, @criteria_detail3 = criteria_detail3
		, @criteria_id4 = criteria_id4
		, @criteria_detail4 = criteria_detail4
		, @criteria_id5 = criteria_id5
		, @criteria_detail5 = criteria_detail5
		, @criteria_id6 = criteria_id6
		, @criteria_detail6 = criteria_detail6
	  FROM [export].[home_energy_report_criteria_profile]
	where [profile_name] = @profile_name


if object_id ('tempdb..#energyObjectCategory_list') is not null drop table #energyObjectCategory_list
if object_id ('tempdb..#eo_cost') is not null drop table #eo_cost
if object_id ('tempdb..#eo_totalcost') is not null drop table #eo_totalcost
if object_id ('tempdb..#fuel_type_list') is not null drop table #fuel_type_list

create table #energyObjectCategory_list ([energyObjectCategory] varchar (50), TotalDollars decimal (18, 2))
insert #energyObjectCategory_list ([energyObjectCategory])
select cast (param as varchar (50))
from [dbo].[ufn_split_values] (@energyObjectCategory_list, ',')

create table #fuel_type_list ([fuel_type] varchar (50))
INSERT #fuel_type_list ([fuel_type])
SELECT CAST (param AS VARCHAR (50))
FROM [dbo].[ufn_split_values] (@fuel_type_list, ',')

CREATE TABLE #eo_cost (Dollars DECIMAL (18, 2), PremiseKey INT, Percentage DECIMAL (18, 2), energyObjectCategory VARCHAR (50))
CREATE TABLE #eo_totalcost (TotalDollars DECIMAL (18, 2), PremiseKey INT)



IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DimEnergyObject]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[DimEnergyObject](
		[EnergyObjectKey] [int] IDENTITY(1,1) NOT NULL,
		[EnergyObjectDesc] [nvarchar](50) NULL,
		[EnergyObjectCategory] [nvarchar](50) NULL,
		[ResidentialApplianceID] [int] NULL,
	 CONSTRAINT [PK_DimAppliance] PRIMARY KEY CLUSTERED 
	(
		[EnergyObjectKey] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
ELSE
BEGIN
	DELETE [dbo].[DimEnergyObject]
END

INSERT INTO [dbo].[DimEnergyObject]  
           ([EnergyObjectDesc]		--this IS an appliance
           ,[EnergyObjectCategory]  --this is an end use
           ,[ResidentialApplianceID])
SELECT ApplianceKey, EnduseKey, ClientApplianceID  FROM venergyobject  

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FactEnergyObject]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[FactEnergyObject](
		[EnergyObjectKey] [int] NOT NULL,
		[PremiseKey] [int] NOT NULL,
		[EnergyObjectGroupKey] [int] NOT NULL,  -- this is now [cm].[TypeEnduseCategory].[EnduseCategoryID]
		[TimePeriodKey] [int] NOT NULL,
		[CostElectric] [decimal](18, 4) NULL,
		[CostGas] [decimal](18, 4) NULL,
		[CostWater] [decimal](18, 4) NULL,
		[CostPropane] [decimal](18, 4) NULL,
		[CostOil] [decimal](18, 4) NULL,
		[CostWood] [decimal](18, 4) NULL,
		[Cost] [decimal](18, 4) NOT NULL,
		[UsageElectric] [decimal](18, 4) NOT NULL,
		[UsageGas] [decimal](18, 4) NOT NULL,
		[UsageWater] [decimal](18, 4) NOT NULL,
		[UsagePropane] [decimal](18, 4) NOT NULL,
		[UsageOil] [decimal](18, 4) NOT NULL,
		[UsageWood] [decimal](18, 4) NOT NULL,
		[Count] [int] NULL,
		[UnitCount] [int] NULL,
		[BaseUsageElectric] [decimal](18, 4) NULL,
		[BaseUsageGas] [decimal](18, 4) NULL,
		[BaseUsageWater] [decimal](18, 4) NULL,
		[BaseUsagePropane] [decimal](18, 4) NULL,
		[BaseUsageOil] [decimal](18, 4) NULL,
		[BaseUsageWood] [decimal](18, 4) NULL,
		[BaseCost] [decimal](18, 4) NULL,
		[BaseType] [varchar](20) NULL,
		[Points] [int] NULL,
		[CO2] [nchar](10) NULL
	) ON [PRIMARY]
END
ELSE
BEGIN
	DELETE  [dbo].[FactEnergyObject]
END

--INSERT monthly
INSERT  [dbo].[FactEnergyObject]
(
	[EnergyObjectKey],
	[PremiseKey],
	[EnergyObjectGroupKey],
	[TimePeriodKey],
	[CostGas],
	[Cost],
		[UsageElectric],
		[UsageGas],
		[UsageWater],
		[UsagePropane],
		[UsageOil],
		[UsageWood]
	)
SELECT DISTINCT energyobjectkey, Premisekey,EnergyObjectGroupKey, 1, cost/12, cost,0,0,0,0,0,0  FROM(
SELECT (SELECT energyobjectkey FROM dimenergyobject deo WHERE deo.[EnergyObjectDesc]=(SELECT TOP 1 apl.ApplianceKey FROM InsightsMetadata.cm.ClientAppliance apl 
						INNER JOIN [InsightsMetadata].[cm].ClientEnduseAppliance cae
						ON cae.ApplianceKey = apl.ApplianceKey
						INNER JOIN [InsightsMetadata].[cm].ClientEnduse eu
						ON eu.ClientEnduseID = cae.ClientEnduseID
						where apl.ClientApplianceID=app.ClientApplianceID 
						) 
						AND deo.[EnergyObjectCategory]=(SELECT TOP 1 eu.endusekey FROM InsightsMetadata.cm.ClientAppliance apl 
						INNER JOIN [InsightsMetadata].[cm].ClientEnduseAppliance cae
						ON cae.ApplianceKey = apl.ApplianceKey
						INNER JOIN [InsightsMetadata].[cm].ClientEnduse eu
						ON eu.ClientEnduseID = cae.ClientEnduseID
						where apl.ClientApplianceID=app.ClientApplianceID 
						)
						) AS energyobjectkey 

, ps.*, (SELECT TOP 1 eu.endusekey FROM InsightsMetadata.cm.ClientAppliance apl 
						INNER JOIN [InsightsMetadata].[cm].ClientEnduseAppliance cae
						ON cae.ApplianceKey = apl.ApplianceKey
						INNER JOIN [InsightsMetadata].[cm].ClientEnduse eu
						ON eu.ClientEnduseID = cae.ClientEnduseID
						where apl.ClientApplianceID=app.ClientApplianceID 
						) AS Enduse, 
			  (SELECT TOP 1 apl.ApplianceKey FROM InsightsMetadata.cm.ClientAppliance apl 
						INNER JOIN [InsightsMetadata].[cm].ClientEnduseAppliance cae
						ON cae.ApplianceKey = apl.ApplianceKey
						INNER JOIN [InsightsMetadata].[cm].ClientEnduse eu
						ON eu.ClientEnduseID = cae.ClientEnduseID
						where apl.ClientApplianceID=app.ClientApplianceID 
						) AS ApplianceKey,
			(SELECT TOP 1 teu.endusecategoryid FROM InsightsMetadata.cm.ClientAppliance apl 
						INNER JOIN [InsightsMetadata].[cm].ClientEnduseAppliance cae
						ON cae.ApplianceKey = apl.ApplianceKey
						INNER JOIN [InsightsMetadata].[cm].ClientEnduse eu
						ON eu.ClientEnduseID = cae.ClientEnduseID
						INNER JOIN [InsightsMetadata].[cm].[TypeEnduseCategory] teu
						ON eu.EnduseCategoryKey=teu.EnduseCategoryKey
						where apl.ClientApplianceID=app.ClientApplianceID 
						) AS EnergyObjectGroupKey
FROM InsightsDW.dbo.PremiseSavings ps  
INNER join    InsightsMetadata.[cm].[ClientAction] ca  
ON ca.ActionKey=ps.actionkey 
INNER JOIN [InsightsMetadata].[cm].[ClientActionCondition] cac  
ON cac.ClientActionID=ca.ClientActionID
INNER JOIN  [InsightsMetadata].[cm].[ClientCondition] cc  
ON cc.ConditionKey=cac.conditionkey
INNER JOIN [InsightsMetadata].[cm].[ClientProfileAttributeProfileOption] capo  
ON capo.ProfileOptionKey = cc.ProfileOptionKey
INNER JOIN   InsightsMetadata.[cm].[ClientActionSavingsCondition] casc  
ON casc.ClientActionSavingsID = ps.ClientActionSavingsID
INNER JOIN [InsightsMetadata].[cm].[ClientProfileAttribute] cpa  
ON cpa.ClientProfileAttributeID = capo.ClientProfileAttributeID
INNER JOIN [InsightsMetadata].[cm].[ClientApplianceProfileAttribute] app  
ON cpa.ProfileAttributeKey=app.ProfileAttributeKey


) AS aa


--INSERT annual
INSERT  [dbo].[FactEnergyObject]
(
energyobjectkey,
	[PremiseKey],
	[EnergyObjectGroupKey],
	[TimePeriodKey],
	[CostGas],
	[Cost],
			[UsageElectric],
		[UsageGas],
		[UsageWater],
		[UsagePropane],
		[UsageOil],
		[UsageWood]
	)
SELECT DISTINCT energyobjectkey, Premisekey,EnergyObjectGroupKey, 2, cost, cost ,0,0,0,0,0,0 FROM(
SELECT (SELECT energyobjectkey FROM dimenergyobject deo WHERE deo.[EnergyObjectDesc]=(SELECT TOP 1 apl.ApplianceKey FROM InsightsMetadata.cm.ClientAppliance apl 
						INNER JOIN [InsightsMetadata].[cm].ClientEnduseAppliance cae
						ON cae.ApplianceKey = apl.ApplianceKey
						INNER JOIN [InsightsMetadata].[cm].ClientEnduse eu
						ON eu.ClientEnduseID = cae.ClientEnduseID
						where apl.ClientApplianceID=app.ClientApplianceID 
						) 
						AND deo.[EnergyObjectCategory]=(SELECT TOP 1 eu.endusekey FROM InsightsMetadata.cm.ClientAppliance apl 
						INNER JOIN [InsightsMetadata].[cm].ClientEnduseAppliance cae
						ON cae.ApplianceKey = apl.ApplianceKey
						INNER JOIN [InsightsMetadata].[cm].ClientEnduse eu
						ON eu.ClientEnduseID = cae.ClientEnduseID
						where apl.ClientApplianceID=app.ClientApplianceID 
						)
						) AS energyobjectkey

, ps.*, (SELECT TOP 1 eu.endusekey FROM InsightsMetadata.cm.ClientAppliance apl 
						INNER JOIN [InsightsMetadata].[cm].ClientEnduseAppliance cae
						ON cae.ApplianceKey = apl.ApplianceKey
						INNER JOIN [InsightsMetadata].[cm].ClientEnduse eu
						ON eu.ClientEnduseID = cae.ClientEnduseID
						where apl.ClientApplianceID=app.ClientApplianceID 
						) AS EnergyObjectCategory,
		 (SELECT TOP 1 apl.ApplianceKey FROM InsightsMetadata.cm.ClientAppliance apl 
						INNER JOIN [InsightsMetadata].[cm].ClientEnduseAppliance cae
						ON cae.ApplianceKey = apl.ApplianceKey
						INNER JOIN [InsightsMetadata].[cm].ClientEnduse eu
						ON eu.ClientEnduseID = cae.ClientEnduseID
						where apl.ClientApplianceID=app.ClientApplianceID 
						) AS ApplianceKey,
			(SELECT TOP 1 teu.endusecategoryid FROM InsightsMetadata.cm.ClientAppliance apl 
						INNER JOIN [InsightsMetadata].[cm].ClientEnduseAppliance cae
						ON cae.ApplianceKey = apl.ApplianceKey
						INNER JOIN [InsightsMetadata].[cm].ClientEnduse eu
						ON eu.ClientEnduseID = cae.ClientEnduseID
						INNER JOIN [InsightsMetadata].[cm].[TypeEnduseCategory] teu
						ON eu.EnduseCategoryKey=teu.EnduseCategoryKey
						where apl.ClientApplianceID=app.ClientApplianceID 
						) AS EnergyObjectGroupKey
FROM InsightsDW.dbo.PremiseSavings ps   
INNER join    InsightsMetadata.[cm].[ClientAction] ca  
ON ca.ActionKey=ps.actionkey 
INNER JOIN [InsightsMetadata].[cm].[ClientActionCondition] cac  
ON cac.ClientActionID=ca.ClientActionID
INNER JOIN  [InsightsMetadata].[cm].[ClientCondition] cc  
ON cc.ConditionKey=cac.conditionkey
INNER JOIN [InsightsMetadata].[cm].[ClientProfileAttributeProfileOption] capo  
ON capo.ProfileOptionKey = cc.ProfileOptionKey
INNER JOIN   InsightsMetadata.[cm].[ClientActionSavingsCondition] casc  
ON casc.ClientActionSavingsID = ps.ClientActionSavingsID
INNER JOIN [InsightsMetadata].[cm].[ClientProfileAttribute] cpa  
ON cpa.ClientProfileAttributeID = capo.ClientProfileAttributeID
INNER JOIN [InsightsMetadata].[cm].[ClientApplianceProfileAttribute] app  
ON cpa.ProfileAttributeKey=app.ProfileAttributeKey

) AS aa


DECLARE fuel_type_cursor CURSOR FOR
SELECT	fuel_type
FROM	#fuel_type_list

OPEN fuel_type_cursor
FETCH NEXT FROM fuel_type_cursor INTO @fuel_type
WHILE @@FETCH_STATUS = 0
BEGIN

	select	@sqlcmd = '
	insert #eo_cost (Dollars, PremiseKey, energyObjectCategory)  
	select	sum(feo.Cost' + @fuel_type + ') as Dollars, feo.PremiseKey, eol.energyObjectCategory
	from	dbo.DimEnergyObject deo  
			inner join dbo.FactEnergyObject feo on feo.EnergyObjectKey = deo.EnergyObjectKey
			inner join dbo.DimTimePeriod tp on feo.TimePeriodKey = tp.TimePeriodKey
			inner join #energyObjectCategory_list eol on eol.energyObjectCategory = deo.EnergyObjectCategory
	where	lower (tp.TimePeriodDesc) = ''' + @period + '''  	
	group by feo.PremiseKey, eol.energyObjectCategory'--change 9.24.14 by wayne


	EXEC (@sqlcmd)
	

	FETCH NEXT FROM fuel_type_cursor INTO @fuel_type

END
CLOSE fuel_type_cursor
DEALLOCATE fuel_type_cursor

----select	SUM (feo.CostElectric) TotalDollars, feo.PremiseKey, eol.energyObjectCategory
----from	dbo.DimEnergyObject deo
----		inner join dbo.FactEnergyObject feo on feo.EnergyObjectKey = deo.EnergyObjectKey
----		inner join #energyObjectCategory_list eol on eol.energyObjectCategory = deo.EnergyObjectCategory
----group by feo.PremiseKey, eol.energyObjectCategory


INSERT	#eo_totalcost (TotalDollars, PremiseKey)  
SELECT	SUM (Dollars) TotalDollars, PremiseKey
FROM	 #eo_cost  
GROUP BY PremiseKey

UPDATE	eoc
SET		percentage = 
			CASE WHEN eoct.TotalDollars = 0 THEN 0 ELSE ROUND (100 * eoc.Dollars/eoct.TotalDollars, 0) END
FROM	#eo_cost eoc
		INNER JOIN #eo_totalcost eoct ON eoc.PremiseKey = eoct.PremiseKey



DECLARE energyObjectCategory_cursor CURSOR FOR
SELECT	energyObjectCategory
FROM	#energyObjectCategory_list

SELECT @count = 1
OPEN energyObjectCategory_cursor
FETCH NEXT FROM energyObjectCategory_cursor INTO @energyObjectCategory
WHILE @@FETCH_STATUS = 0
BEGIN

	IF @period = 'annual'
	BEGIN
		SELECT @sqlcmd = '
		update	he
		set	EnergyObjectCategory' + CONVERT (VARCHAR, @count) + ' = ''' + @energyObjectCategory + ''' ' + 
		'from [export].[home_energy_report_staging_allcolumns] he'
		EXEC (@sqlcmd)

		SELECT @sqlcmd = '
		update	he
		set	EnergyObjectCategoryDollar' + CONVERT (VARCHAR, @count) + ' =  convert (varchar, convert (int, round (coalesce (ec.Dollars, 0), 0))) ' +
		' from [export].[home_energy_report_staging_allcolumns] he  
				inner join dbo.DimPremise p1  on p1.PremiseId = he.PremiseID  
				left join #eo_cost ec  
				on ec.PremiseKey = p1.PremiseKey and ec.energyObjectCategory = ''' + @energyObjectCategory + ''''
 		EXEC (@sqlcmd)

		SELECT @sqlcmd = '
		update	he
		set	EnergyObjectCategoryPercent' + CONVERT (VARCHAR, @count) + ' = convert (varchar, convert (int, coalesce (ec.Percentage, 0))) ' +
		' from [export].[home_energy_report_staging_allcolumns] he  
				inner join dbo.DimPremise p1  
				on p1.PremiseId = he.PremiseID
				left join #eo_cost ec  
				on ec.PremiseKey = p1.PremiseKey and ec.energyObjectCategory = ''' + @energyObjectCategory + ''''
 		EXEC (@sqlcmd)
	END
	ELSE
	BEGIN
		SELECT @sqlcmd = '
		update	he
		set	EnergyObjectCategoryMonthly' + CONVERT (VARCHAR, @count) + ' = ''' + @energyObjectCategory + ''' ' + 
		'from [export].[home_energy_report_staging_allcolumns] he'
		EXEC (@sqlcmd)

		SELECT @sqlcmd = '
		update	he
		set	EnergyObjectCategoryDollarMonthly' + CONVERT (VARCHAR, @count) + ' =  convert (varchar, convert (int, round (coalesce (ec.Dollars, 0), 0))) ' +
		' from [export].[home_energy_report_staging_allcolumns] he
				inner join dbo.DimPremise p1 on p1.PremiseId = he.PremiseID
				left join #eo_cost ec on ec.PremiseKey = p1.PremiseKey and ec.energyObjectCategory = ''' + @energyObjectCategory + ''''
 		EXEC (@sqlcmd)

		SELECT @sqlcmd = '
		update	he
		set	EnergyObjectCategoryPercentMonthly' + CONVERT (VARCHAR, @count) + ' = convert (varchar, convert (int, coalesce (ec.Percentage, 0))) ' +
		' from [export].[home_energy_report_staging_allcolumns] he  
				inner join dbo.DimPremise p1  
				on p1.PremiseId = he.PremiseID
				left join #eo_cost ec  
				on ec.PremiseKey = p1.PremiseKey and ec.energyObjectCategory = ''' + @energyObjectCategory + ''''
 		EXEC (@sqlcmd)
	END



	SELECT @count += 1

	FETCH NEXT FROM energyObjectCategory_cursor INTO @energyObjectCategory

END
CLOSE energyObjectCategory_cursor
DEALLOCATE energyObjectCategory_cursor

SET NOCOUNT OFF

END

GO
