SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Comment all changes !!
--
-- =============================================
-- Author:	 Sunil
-- Create date: unknown
-- Description: Produces a csv extract referred to Home Energy Report
--
-- I will put all change modifications for all HE Report procs/job modofcations into this source
-- as it is the parent
-- =============================================
--#region Changes October 2014	 
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.9.2014
-- Description: v 1.1.0
--	
-- Added a log file to run in SSIS package	
-- =============================================	
-- Changed by:	 Wayne
-- Change date: 10.9.2014
-- Description: v 1.2.0
-- In Proc
-- 1.	Only display the last 5 digits of the account number 
--		and all others marked with an  asterisk “*”  (not mask the first 5)
-- 2.	On masked account number use **** (not #### ).		
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.9.2014
-- Description: v 1.3.0
-- In Proc
-- Removed the ami fields from annual report.		
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.9.2014
-- Description: v 1.4.0
--	
-- Removed the ami from the final "annual" csv by changing the client table annual column - no code change			
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.9.2014
-- Description: v 1.5.0
--	
-- Added a report number parameter that will appear in final csv
-- to indicate the iteration of the report		
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.10.2014
-- Description: v 1.6.0
--	
-- Added a parameter to bypass writing history		
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.13.2014
-- Description: v 1.7.0
--	
-- Eliminate (make positive) negative values in any PCT column		
-- also
-- Added a config param for BillYear and BillMonth
-- will need in future change		
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.13.2014
-- Description: v 1.8.0
--	
-- fix bug in min and max usage and dates		
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.14.2014
-- Description: v 1.9.0
--	
-- Changed the referrerid to be clientid per barsana		
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.14.2014
-- Description: v 1.10.0
--	
-- try to push to right		
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.15.2014
-- Description: v 1.11.0
--	
-- fixed a problem in profile table
-- that was causing entire energy object not to be reported on
-- extra space after a comma		
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.16.2014
-- Description: v 1.12.0
--	
-- added a rolling year for ami aggregates
-- this proc needed to use rolling year aggregatetypekeys		
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.16.2014
-- Description: v 1.13.0
--	
-- not a code change just a SSIS package change 
-- to remove commas in data
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.17.2014
-- Description: v 1.14.0
--	
-- Waste goal should be based on monthly totals for Monthy report
-- and annual totals for Annual report	
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.17.2014
-- Description: v 1.15.0
-- first run with new benchmark calcs (no code change)
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.17.2014
-- Description: v 1.17.0
--	
-- ComparetoEfficientGMontlyCostPercent & ComparetoAverageGMontlyCostPercent values 
-- should be calculated of values that have already been rounded
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.17.2014
-- Description: v 1.18.0
--	
-- remove any premise that has opted out from csv   !!!  This has been removed by v.19
--
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.24.2014
-- Description: v 1.19.0
--	
-- change reports to be based on channel
-- 2 options
-- 'email' OR '--PRINT'
--
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.24.2014
-- Description: v 1.20.0
--	
--  Below are the number of reports each treatment group gets. 
--	added some code to not process above these max numbers
--	T-9		paperreport.channel.emailonly		12
--	T-11	paperreport.channel.--PRINTandemail	4
--	T-14	paperreport.channel.--PRINTonly		4
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.24.2014
-- Description: v 1.21.0
--	
--  added a distinct to initial load of allcolumns table
--  needed because sometimes the customer on the account changes
--  like a wife replacing a husband but the premisee, acct are the same
--  it was causing the same premise more than 1 time
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.29.2014
-- Description: v 1.22.0
--	
-- changed the distinct to a ranking window to get 1 row
-- in the join to improve performace
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.16.2014
-- Description: v 1.23.0
--	
-- removed rolling year and week from ami aggregates
-- using aggregates for 1 month only in aggregates table		
-- =============================================
--#endregion

--#region Changes November 2014	 
-- =============================================
-- Changed by:	LCF
-- Change date: 11.13.2014
-- Description: v 1.24.0
--	
-- removed @write_history parameter
-- =============================================
-- Changed by:	 Wayne
-- Change date: 11.14.2014
-- Description: v 1.25.0
--	
-- passed in channel when calling 
-- the usp_HE_Report_Get_peer_comparison_numbers  proc
-- =============================================
-- Changed by:	 Wayne
-- Change date: 11.14.2014
-- Description: v 1.26.0
--	
-- Eliminate premises with null or zero in month column 1
-- in export.usp_HE_Report_Create_Staging_Allcols
-- month column 1 is  = @bill_year + @bill_month
-- =============================================
-- Changed by:	 Wayne
-- Change date: 11.18.2014
-- Description: v 1.27.0
--	
-- EWhen ther are multiple customers for premise
-- in Dimcustomer the code should get the most recent customer
-- added a desc sort on the ROW_NUMBER() 
-- =============================================
-- Changed by:	 Wayne
-- Change date: 11.19.2014
-- Description: v 1.28.0
--	
-- added logic for report number to
-- [Export].[usp_HE_Report_Get_peer_comparison_numbers]
-- if month 13 is null or 0
-- then use a 1 otherwise use the parameter value
-- and removed a rdundant update being done 
-- to report number in 
-- [Export].[usp_HE_Report_Get_AMI]
-- =============================================
-- Changed by:	 Wayne
-- Change date: 11.24.2014
-- Description: v 1.29.0	
-- new code added in selection of column
-- from client table where clause
-- which now evaluate the channel parameter
-- and the "email" or "--PRINT" column (new columns) in client table
-- to determine if column is export
-- =============================================
-- Changed by:	 Wayne
-- Change date: 11.24.2014
-- Description: v 1.30.0
--	
-- fixed some bad [field name]s in the client table that had 
-- special (non --PRINTable) characters at end
-- and had to change cod ethat referred to
-- bad [field name] to the now corrected [field name]
-- =============================================
--#endregion

--#region Changes December 2014	 
-- =============================================
-- Changed by:	 Wayne
-- Change date: 12.17.2014
-- Description: v 1.31.0
--	
-- Added a new store procedure to add promo info
-- to the allcolumns table
-- Replaces Sams manual work
-- Uses Sam's "R" logic
-- parameterized promo codes
-- =============================================
-- Changed by:	 Wayne
-- Change date: 12.18.2014
-- Description: v 1.32.0
--	
-- renamed the [dbo].[FactActionItem] table to [export].[HE_Report_FactActionItem]
-- used in proc [Export].[usp_HE_Report_Get_Measures]
-- and [Export].[usp_HE_Report_Get_Measures_AddColumns]
-- =============================================
-- Changed by:	 Wayne
-- Change date: 12.18.2014
-- Description: v 1.33.0	
-- 
-- tweaked column select code
-- if channel ='all' treat as if it was "--PRINT" 
-- for selecting column from client table
-- to determine if column is exported
--
-- also added an ORDER BY clause to final output
-- to insure it is in premiseid order
--
-- also added ability to retsart at specific proc
-- =============================================
-- Changed by:	Wayne
-- Change date: 12.31.2014
-- Description:	v 1.33.0 (same version)
--	
-- changed SUM to MAX in Q1 vs Q3 comparison	for promos
-- =============================================
-- Changed by:	Wayne
-- Change date: 12.31.2014
-- Description:	v 1.33.0 (same version)
--	
--  Removed the WITH (NOLOCK)'s because
--  they were causing race conditions
-- =============================================
-- Changed by:	Wayne
-- Change date: 1.8.2015
-- Description:	v 1.34.0 (same version)
--	
--  if amy of the ami aggregates are 0 then
-- fall the report program number 
-- back to 2 unless it has already been set to 1
-- because month 13 is 0
-- =============================================
-- Changed by:	Wayne
-- Change date: 1.14.2015
-- Description:	v 1.35.0 
--	
--  if a new bill has been added since the last time this report has run
--  run three things
--  1. zip code fix
--  2. fix bill dates
--  3. missing benchmarks 
-- =============================================
-- Changed by:		Wayne
-- Change date:	3.12.2015
-- Version:		v 1.35.0 
--	
-- Description:	Added treatment group to output
-- =============================================
-- Changed by:	 Wayne
-- Change date: 1.8.2015
-- Description: v 1.35.0 
--	
-- tweak fallback rules for when no month 13
-- only report 3 requires ami data
-- all others must fallback to 1 if they dont have month 13
-- =============================================
-- Changed by:		Wayne
-- Change date:	3.12.2015
-- Version:		v 1.34.0 
--	
-- Description:	If treatment group param is not null
--				Then use it to filter in addition to channel
-- =============================================
-- Changed by:		Wayne
-- Change date:	3.14.2015
-- Version:		v 1.34.0 
--	
-- Description:	changed to calculate only 1 month's benchmarks if missing
--				rather than all missing, too much overhead
-- =============================================
--#endregion

--#region Possible Return Codes	 
-- =============================================
-- Return Code -14 is a validation error, severity will be 11
--
-- '@Report_number parameter must be numeric'								   Return Code=  -14	   State Code= 1
-- '@bill_year parameter must be numeric'									   Return Code=  -14	   State Code= 2
-- '@bill_year parameter must be > 2013'									   Return Code=  -14	   State Code= 3
-- '@bill_month parameter must be numeric'									   Return Code=  -14	   State Code= 4
-- '@bill_month parameter is invalid 1-12 are acceptable'						   Return Code=  -14	   State Code= 5
-- 'Invalid @channel parameter ony "--PRINT", "email" and "all" are valid'			   Return Code=  -14	   State Code= 6
-- 'Only 12 reports can go out for emailState Code= check the @report_number parameter' Return Code=  -14	   State Code= 7
-- 'Only 4 reports can go out for --PRINTState Code= check the @report_number parameter'  Return Code=  -14	   State Code= 8

-- 'Invalid Proifile Name'												   Return Code=  -15	   
--
-- Step 1 Failure														   Return Code=  -1	   
-- Step 2 Failure														   Return Code=  -2	   
-- Step 3 Failure														   Return Code=  -3	   
-- Step 4 Failure														   Return Code=  -4	   
-- Step 5 Failure														   Return Code=  -5	   
-- Step 6 Failure														   Return Code=  -6	   
-- Step 7 Failure														   Return Code=  -7	   
-- Step 8 Failure														   Return Code=  -8	   
-- Step 9 Failure														   Return Code=  -9	   
-- Step 10 Failure														   Return Code=  -10	   
-- Step 11 Failure														   Return Code=  -11   
-- Step 12 Failure														   Return Code=  -12
-- Step 13 Failure														   Return Code=  -13
-- =============================================
--#endregion

CREATE PROCEDURE [Export].[usp_HE_Report_Prepare_CSV_Data]
	 @profile_name	    VARCHAR(50)	= 'SCG_Profile101'
    , @channel		    VARCHAR(5)		= 'email'		 -- options are 'email'  or '--PRINT'
    , @report_number    VARCHAR(1)		= '1'		 -- options are 1-12
    , @bill_year	    VARCHAR(4)		= '2014'		 -- >= 2014
    , @bill_month	    VARCHAR(2)		= '10'		 -- options are 1-12
    , @restart_step	    int		 	= 0	   		 -- options are 0-13
    , @promo_code_1	    VARCHAR(1)		= 'C'		  --use variable promo_code 1 which is for non heating customers
    , @promo_code_2	    VARCHAR(1)		= 'E'		  --use variable promo_code 2 which is for non heating customers
    , @promo_code_3	    VARCHAR(1)		= 'C'		  --use variable promo_code 3 which is for heating customers
    , @promo_code_4	    VARCHAR(1)		= 'F'		  --use variable promo_code 4 which is for heating customers
    , @promo_code_5	    VARCHAR(1)		= 'C'		  --use variable promo_code 5 which is for customers on email channel
    , @treatment_groups  VARCHAR(20)	='T-9'
    , @run_benchmarks    INT			=0			  --1 will force a rerun of benchmarks for the month
    
AS
BEGIN
    SET NOCOUNT ON;

    
    -- Declare error Vars
    DECLARE @ErrorNumber	  INT		   = ERROR_NUMBER();
    DECLARE @ErrorLine		  INT		   = ERROR_LINE();
    DECLARE @ErrorMessage	  NVARCHAR(4000)  = ERROR_MESSAGE();
    DECLARE @ErrorSeverity	  INT		   = ERROR_SEVERITY();
    DECLARE @ErrorState	  INT		   = ERROR_STATE();

    BEGIN TRY
	   --	validate the incomming parameter values
	   IF ISNUMERIC(@report_number) = 0
		  BEGIN
			 RAISERROR(N'@Report_number parameter must be numeric' , 11 , 1);
		  END;
	   IF ISNUMERIC(@bill_year) = 0
		  BEGIN
			 RAISERROR(N'@bill_year parameter must be numeric' , 11 , 2);
		  END;
	   IF @bill_year < 2014
		  BEGIN
			 RAISERROR(N'@bill_year parameter must be > 2013' , 11 , 3);
		  END;
	   IF ISNUMERIC(@bill_month) = 0
		  BEGIN
			 RAISERROR(N'@bill_month parameter must be numeric' , 11 , 4);
		  END;
	   IF ISNUMERIC(@bill_month) > 12
		  BEGIN
			 RAISERROR(N'@bill_month parameter is invalid 1-12 are acceptable' , 11 , 5);
		  END;
	   IF LOWER(@channel) != '--PRINT' AND LOWER(@channel) != 'email' AND LOWER(@channel) != 'all'
		  BEGIN
			 RAISERROR(N'Invalid @channel parameter ony "--PRINT", "email" and "all" are valid' , 11 , 6);
		  END;
	   IF LOWER(@channel) = 'email' AND @report_number > 12
		  BEGIN
			 RAISERROR(N'Only 12 reports can go out for email, check the @report_number parameter' , 11 , 7);
		  END;
	   IF LOWER(@channel) = '--PRINT' AND @report_number > 4
		  BEGIN
			 RAISERROR(N'Only 4 reports can go out for --PRINT, check the @report_number parameter' , 11 , 8);
		  END;
    	END TRY
	BEGIN CATCH
	   SET @ErrorNumber	   = ERROR_NUMBER();
	   SET @ErrorLine	   = ERROR_LINE();
	   SET @ErrorMessage   = 'Validation Error - ' + ERROR_MESSAGE();
	   SET @ErrorSeverity  = ERROR_SEVERITY();
	   SET @ErrorState	   = ERROR_STATE();
	   RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
	   RETURN -14 --validation error
	 END CATCH
	 

    --	Declare additional needed to populate Profile Table values
    DECLARE
       @clientID				 INT , 
       @fuel_type				 VARCHAR(100) , 
       @energyobjectcategory_List	 VARCHAR(1024) , 
       @period					 VARCHAR(10) , 
       @season_List				 VARCHAR(100) , 
       @enduse_List				 VARCHAR(1024) , 
       @ordered_enduse_List		 VARCHAR(1024) , 
       @criteria_id1			 VARCHAR(50) , 
       @criteria_detail1			 VARCHAR(255) , 
       @criteria_id2			 VARCHAR(50) , 
       @criteria_detail2			 VARCHAR(255) , 
       @criteria_id3			 VARCHAR(50) , 
       @criteria_detail3			 VARCHAR(255) , 
       @criteria_id4			 VARCHAR(50) , 
       @criteria_detail4			 VARCHAR(255) , 
       @criteria_id5			 VARCHAR(50) , 
       @criteria_detail5			 VARCHAR(255) , 
       @criteria_id6			 VARCHAR(50) , 
       @criteria_detail6			 VARCHAR(255) , 
       @saveProfile				 CHAR(1) , 
       @verifySave				 CHAR(1) , 
       @process_id				 CHAR(1);


    --	Declare additional needed parameters
    DECLARE
       @sqlcmd		    NVARCHAR(max) , 
       @sqlcmd_cursor   VARCHAR(8000) , 
       @column_name	    VARCHAR(128) , 
       @column_count    INT = 1 , 
       @tablename	    NVARCHAR(128) = '[export].[home_energy_report_staging_allcolumns]';


    --	Populate parameters
	   BEGIN TRY
		  SELECT @clientID				= clientID
			 , @fuel_type				= fuel_type
			 , @energyobjectcategory_List	= energyobjectcategory_List
			 , @period					= period
			 , @season_List				= season_List
			 , @enduse_List				= enduse_List
			 , @ordered_enduse_List		= ordered_enduse_List
			 , @criteria_id1				= criteria_id1
			 , @criteria_detail1			= criteria_detail1
			 , @criteria_id2				= criteria_id2
			 , @criteria_detail2			= criteria_detail2
			 , @criteria_id3				= criteria_id3
			 , @criteria_detail3			= criteria_detail3
			 , @criteria_id4				= criteria_id4
			 , @criteria_detail4			= criteria_detail4
			 , @criteria_id5				= criteria_id5
			 , @criteria_detail5			= criteria_detail5
			 , @criteria_id6				= criteria_id6
			 , @criteria_detail6			= criteria_detail6
		  FROM export.home_energy_report_criteria_profile
		  WHERE profile_name = @profile_name;
		  IF @@rowcount=0
		  BEGIN
			 RAISERROR(N'Invalid Proifile Name' , 11 , 1);		    
		  END;
	   END TRY
	   BEGIN CATCH
		  SET @ErrorNumber	   = ERROR_NUMBER();
		  SET @ErrorLine	   = ERROR_LINE();
		  SET @ErrorMessage   = 'Error executing usp_HE_Report_Prepare_CSV_Data,  ' + ERROR_MESSAGE();
		  SET @ErrorSeverity  = ERROR_SEVERITY();
		  SET @ErrorState	   = ERROR_STATE();
		  RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		  RETURN -15
	   END CATCH
	 


	 -- Pre PROCESS Steps

	 -- Declare error Vars
	 DECLARE @benchmarks_need_calculating	   int		   = 0;
	 DECLARE @bills_received_flag			   int		   = 0;
	 DECLARE @countOfBills				   INT		   = 0;
	 DECLARE @countOfBenchmarks			   INT		   = 0;
	 DECLARE @billyearmonth varchar(10) = CAST(CAST(CAST(@bill_year AS varchar(4)) 
							 + '-' 
							 + CAST(@bill_month AS varchar(2)) 
							 + '-' 
							 + '01' AS date)AS varchar(10));

      DECLARE  @endrangedate date= DATEADD(DAY,-1,DATEADD(MONTH,1,(CAST(@bill_year+@bill_month+'01' AS DATE)))) 
      DECLARE  @startrangedate date= CAST(@bill_year+@bill_month+'01' AS DATE)
	 DECLARE @startrangekey INT =cast(convert(char(8), @startrangedate, 112) as int) 
      DECLARE @endrangekey INT =cast(convert(char(8), @endrangedate, 112) as int) 

	 
	 -- STEP 1:	Check that the bills are in DW
	 --PRINT 'STEP 1:	Check that the bills are in DW'
	 IF @restart_step=0 OR @restart_step<=1
	 BEGIN
		BEGIN TRY
		    SELECT @countOfBills=COUNT(*) FROM InsightsDW.dbo.factbilling 
			 WHERE ClientId=101 AND  BillPeriodEndDateKey BETWEEN @startrangekey AND @endrangekey
		    IF @countOfBills < 400000 -- arbitrary threashold scg is usually 700k
			 BEGIN
				SET @ErrorMessage   = 'All bills have not been loaded into DW for the reporting month'
				RAISERROR(@ErrorMessage, 11,1);
			 END
		END TRY
		BEGIN CATCH
		  SET @ErrorNumber	   = ERROR_NUMBER();
		  SET @ErrorLine	   = ERROR_LINE();
		  SET @ErrorMessage   = 'Error executing usp_HE_Report_Prepare_CSV_Data, bill count is low ' + ERROR_MESSAGE();
		  SET @ErrorSeverity  = ERROR_SEVERITY();
		  SET @ErrorState	   = ERROR_STATE();
		  RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		  RETURN -1
		END CATCH
	 END

	 -- STEP 2:	check if we need benchmarks
	 --PRINT 'STEP 2:	check if we need benchmarks'
	 IF @restart_step=0 OR @restart_step<=2
	 BEGIN
		BEGIN TRY
		SELECT @countOfBenchmarks=COUNT(*) FROM InsightsDW.dbo.benchmarks b 
			 WHERE ClientId=@clientID AND  b.BillMonth=@billyearmonth
		    IF @countOfBenchmarks < 25000 -- arbitrary threashold scg is usually 80k
			 BEGIN
				    SET @benchmarks_need_calculating=1	--will also force a rerun of benchmarks for the month
			 END
		    ELSE	
			 BEGIN
				SET @benchmarks_need_calculating=0
			 END
		END TRY
		BEGIN CATCH
		  SET @ErrorNumber	   = ERROR_NUMBER();
		  SET @ErrorLine	   = ERROR_LINE();
		  SET @ErrorMessage    = 'Error executing usp_HE_Report_Prepare_CSV_Data ' + ERROR_MESSAGE();
		  SET @ErrorSeverity   = ERROR_SEVERITY();
		  SET @ErrorState	   = ERROR_STATE();
		  RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		  RETURN -2
		END CATCH
	 END

	 -- STEP 3:	Run fix bill dates 
	 --PRINT 'STEP 3:	Run fix bill dates if needed'
	 IF (@restart_step=0 OR @restart_step<=3) and (@run_benchmarks=1 OR @benchmarks_need_calculating=1)
	 BEGIN
	   BEGIN TRY
		  EXEC [dbo].[FixBillDatesForBecnhmarks]
		  @ClientId = @clientID
	   END TRY
	   BEGIN CATCH
		  SET @ErrorNumber	   = ERROR_NUMBER();
		  SET @ErrorLine	   = ERROR_LINE();
		  SET @ErrorMessage    = 'Error executing dbo.FixBillDatesForBecnhmarks ' + ERROR_MESSAGE();
		  SET @ErrorSeverity   = ERROR_SEVERITY();
		  SET @ErrorState	   = ERROR_STATE();
		  RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		  RETURN -3
	   END CATCH	   
    END

    	 -- STEP 4:	Run fix zip codes dates if needed
	 --PRINT 'STEP 4:	Run fix zip codes dates if needed'
	 IF (@restart_step=0 OR @restart_step<=4) and (@run_benchmarks=1 OR @benchmarks_need_calculating=1)
	 BEGIN
	   BEGIN TRY
		  EXEC [dbo].[FixZipCodesForBenchmarks]
		  @ClientId = @clientID
	   END TRY
	   BEGIN CATCH
		  SET @ErrorNumber	   = ERROR_NUMBER();
		  SET @ErrorLine	   = ERROR_LINE();
		  SET @ErrorMessage    = 'Error executing dbo.FixZipCodesForBenchmarks ' + ERROR_MESSAGE();
		  SET @ErrorSeverity   = ERROR_SEVERITY();
		  SET @ErrorState	   = ERROR_STATE();
		  RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		  RETURN -4
	   END CATCH
    END

	 -- STEP 5:	Run benchmarks if needed
	 IF (@restart_step=0 OR @restart_step<=5) and (@run_benchmarks=1 OR @benchmarks_need_calculating=1)
	 BEGIN
	 --PRINT 'STEP 5:	Run benchmarks if needed'
	   BEGIN TRY
		  EXEC	 [dbo].[CreateBenchmarks]
				@ClientID = @clientID,
				@BillMonth = @billyearmonth,
				@BenchmarkGroupTypeKey = 1,
				@MinGroupSize = 100		
		  
	   END TRY
	   BEGIN CATCH
		  SET @ErrorNumber	   = ERROR_NUMBER();
		  SET @ErrorLine	   = ERROR_LINE();
		  SET @ErrorMessage    = 'Error executing dbo.RunBenchmarksMissingMonths ' + ERROR_MESSAGE();
		  SET @ErrorSeverity   = ERROR_SEVERITY();
		  SET @ErrorState	   = ERROR_STATE();
		  RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		  RETURN -5
	   END CATCH
    END


	 -- STEP 6:	Create table with ALL columns
	 --PRINT 'STEP 6:	Create table with ALL columns'
	 IF @restart_step=0 OR @restart_step<=6
	 BEGIN
		BEGIN TRY
		EXEC InsightsDW.export.usp_HE_Report_Create_Staging_Allcols 
			   @profile_name = @profile_name 
			 , @channel	   = @channel 
			 , @bill_year	   = @bill_year 
			 , @bill_month   = @bill_month
			 , @treatment_groups=@treatment_groups;
		END TRY
		BEGIN CATCH
		  SET @ErrorNumber	   = ERROR_NUMBER();
		  SET @ErrorLine	   = ERROR_LINE();
		  SET @ErrorMessage   = 'Error executing export.usp_HE_Report_Create_Staging_Allcols ' + ERROR_MESSAGE();
		  SET @ErrorSeverity  = ERROR_SEVERITY();
		  SET @ErrorState	   = ERROR_STATE();
		  RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		  RETURN -6
		END CATCH
	 END

    -- STEP 7:	Get Peer Comparison information
    --PRINT 'STEP 7:	Get Peer Comparison information'
    IF @restart_step=0 OR @restart_step<=7
    BEGIN
	   BEGIN TRY
	   EXEC InsightsDW.export.usp_HE_Report_Get_peer_comparison_numbers 
		    @profile_name	   = @profile_name 
		  , @bill_year	   = @bill_year 
		  , @bill_month	   = @bill_month 
		  , @channel		   = @channel 
		  , @report_number	   = @report_number;
		    END TRY
		BEGIN CATCH
		  SET @ErrorNumber	   = ERROR_NUMBER();
		  SET @ErrorLine	   = ERROR_LINE();
		  SET @ErrorMessage   = 'Error executing export.usp_HE_Report_Get_peer_comparison_numbers  ' + ERROR_MESSAGE();
		  SET @ErrorSeverity  = ERROR_SEVERITY();
		  SET @ErrorState	   = ERROR_STATE();
		  RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		  RETURN -7
		END CATCH
	END	  

	 -- STEP 8:	Get EndUse information
	 --PRINT 'STEP 8:	Get EndUse information'
    IF @restart_step=0 OR @restart_step<=8
    BEGIN
		BEGIN TRY														
		EXEC InsightsDW.export.usp_HE_Report_Get_EndUse_numbers 
			@profile_name = @profile_name;
		END TRY
		BEGIN CATCH
		  SET @ErrorNumber	   = ERROR_NUMBER();
		  SET @ErrorLine	   = ERROR_LINE();
		  SET @ErrorMessage   = 'Error executing export.usp_HE_Report_Get_EndUse_numbers  ' + ERROR_MESSAGE();
		  SET @ErrorSeverity  = ERROR_SEVERITY();
		  SET @ErrorState	   = ERROR_STATE();
		  RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		  RETURN -8
		END CATCH
    END 

    	 -- STEP 9:	reload latest measures
	 --PRINT 'STEP 9:	reload latest measures'
	 IF @restart_step=0 OR @restart_step<=9
	 BEGIN
		BEGIN TRY
		EXEC InsightsDW.dbo.EvaluateMeasureSavings 
			@clientID = @clientID;
		END TRY
		BEGIN CATCH
		  SET @ErrorNumber	   = ERROR_NUMBER();
		  SET @ErrorLine	   = ERROR_LINE();
		  SET @ErrorMessage   = 'Error executing InsightsDW.dbo.EvaluateMeasureSavings  ' + ERROR_MESSAGE();
		  SET @ErrorSeverity  = ERROR_SEVERITY();
		  SET @ErrorState	   = ERROR_STATE();
		  RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		  RETURN -9
		END CATCH
	 END	  

	 -- STEP 10:	Get Measures information
	 --PRINT 'STEP 10:	Get Measures information'
	 IF @restart_step=0 OR @restart_step<=10
	 BEGIN
		BEGIN TRY
		EXEC InsightsDW.export.usp_HE_Report_Get_Measures 
			@profile_name = @profile_name 
		   , @bill_year    = @bill_year 
		   , @bill_month   = @bill_month;
		END TRY
		BEGIN CATCH
		  SET @ErrorNumber	   = ERROR_NUMBER();
		  SET @ErrorLine	   = ERROR_LINE();
		  SET @ErrorMessage   = 'Error executing export.usp_HE_Report_Get_Measures ' + ERROR_MESSAGE();
		  SET @ErrorSeverity  = ERROR_SEVERITY();
		  SET @ErrorState	   = ERROR_STATE();
		  RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		  RETURN -10
		END CATCH
	 END	  

 	 -- STEP 11:	Get AMI Data 
	 --PRINT 'STEP 11:	Get AMI Data '
	 IF @restart_step=0 OR @restart_step<=11
	 BEGIN  
		BEGIN TRY
		EXEC InsightsDW.Export.usp_HE_Report_Get_AMI 
	    		@profile_name	   = @profile_name 
		   , @channel		   = @channel 
		   , @report_number	   = @report_number 
		   , @bill_year	   = @bill_year 
		   , @bill_month	   =	@bill_month;
		END TRY
		BEGIN CATCH
		  SET @ErrorNumber	   = ERROR_NUMBER();
		  SET @ErrorLine	   = ERROR_LINE();
		  SET @ErrorMessage   = 'Error executing Export.usp_HE_Report_Get_AMI ' + ERROR_MESSAGE();
		  SET @ErrorSeverity  = ERROR_SEVERITY();
		  SET @ErrorState	   = ERROR_STATE();
		  RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		  RETURN -11
		END CATCH
	 END	  

	 -- STEP 12:	Get the Promo Data   
	 --PRINT 'STEP 12:	Get the Promo Data   '
	 IF @restart_step=0 OR @restart_step<=12
	 BEGIN
		BEGIN TRY
		EXEC InsightsDW.Export.usp_HE_Report_Get_Promos 
		  @channel		   = @channel 
		, @promo_code_1	   = @promo_code_1
		, @promo_code_2	   = @promo_code_2
		, @promo_code_3	   = @promo_code_3 
		, @promo_code_4	   = @promo_code_4 
		, @promo_code_5	   = @promo_code_5 
		END TRY
		BEGIN CATCH
		  SET @ErrorNumber	   = ERROR_NUMBER();
		  SET @ErrorLine	   = ERROR_LINE();
		  SET @ErrorMessage   = 'Error executing Export.usp_HE_Report_Get_Promos ' + ERROR_MESSAGE();
		  SET @ErrorSeverity  = ERROR_SEVERITY();
		  SET @ErrorState	   = ERROR_STATE();
		  RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		  RETURN -12
		END CATCH
	 END	  

	  -- STEP 13:	create table that is appropriate for the profile specified.
	  --PRINT 'STEP 13:	create table that is appropriate for the profile specified.'
	 IF @restart_step=0 OR @restart_step<=13
	 BEGIN
		BEGIN TRY
		  EXEC InsightsDW.export.usp_HE_Report_Create_Staging 
			    @profile_name = @profile_name
			  , @channel		   = @channel;
		END TRY
		BEGIN CATCH
		  SET @ErrorNumber	   = ERROR_NUMBER();
		  SET @ErrorLine	   = ERROR_LINE();
		  SET @ErrorMessage   = 'Error executing export.usp_HE_Report_Create_Staging  ' + ERROR_MESSAGE();
		  SET @ErrorSeverity  = ERROR_SEVERITY();
		  SET @ErrorState	   = ERROR_STATE();
		  RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		  RETURN -13
		END CATCH
	 END	  

    SET NOCOUNT OFF;
    --success
    RETURN 0;
    
END;





GO
