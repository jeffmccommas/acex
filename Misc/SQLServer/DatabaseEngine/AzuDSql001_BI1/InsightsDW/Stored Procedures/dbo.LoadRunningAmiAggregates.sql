SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Wayne
-- Create date:	10.13.14
-- Description:	Produces a table of ami aggrgates used for ami reporting
--
--				This proc is for inital loading. A permanent proc will be
--				run at end of weekly ami import routine to recalculate on 
--				newly added ami data for premises
--				for a month/year
-- =============================================
-- Changed by:	Wayne
-- Change date:	10.13.2014
-- Description:	v 1.1.0
--	
--				changed to find last occurance in period for a high or low value
--				originally was finding first		
-- =============================================
-- Changed by:	Wayne
-- Change date:	10.13.2014
-- Description:	v 1.1.0
--	
--				changed to make it a rolling 12 month period	
-- =============================================
-- Changed by:	Wayne
-- Change date:	10.13.2014
-- Description:	v 1.1.0
--	
--	IMPORTANT PLEASE READ !!
--
--				 Requirements have changed again
--				 We no longer need ami on annual reports
--				 changing to only calculate current month aggreates
--				 This will improve performace
--				 I'll leave in the other calculations for now
--				 but it will just be 1 months data being summarized
-- =============================================
-- Changed by:		Wayne
-- Change date:	10.13.2014
-- Description:	v 1.1.0
--	
--				 changed to make sure date lookup in outer apply was with the range	
-- =============================================
-- Changed by:		Wayne
-- Change date:	11.4.2014
-- Description:	v 1.1.0
--	
--				 changed to allow for appending of new month by using a MERGE	
-- =============================================
-- Changed by:		Wayne
-- Change date:	11.4.2014
-- Description:	v 1.1.0
--	
--				 Added a check to insure month is 2 characters	
-- =============================================
-- Changed by:		Wayne
-- Change date:	1.16.2015
-- Description:	To insure we always are processing the most current
--				AMI data that will be used for the next HE report,
--				changed to use the minimum year and month in incomming
--				ami file instead of param. This avoids re-aggregating a prior month
--				when a incomming file only contains a new month
--                  part of bug 56274 
-- =============================================
-- Changed by:		Wayne
-- Change date:	4.16.2015
-- Description:	fixed error in test for existence on drop table statment
-- =============================================
--Notes
	/*
				 0 rows - are 8 per premise per month
				 a zero has all the totals for that day of week in the month
				 [DailyMinInPeriod] 	The lowest value for that day of week in the month
				 [DailyMaxInPeriod]  The highest value for that day of week in the month
				 [TotalUsageInPeriod] the total for that day of week in the month
				 [AvgerageInPeriod]  the average for that day of week during the month
				 [DateOfMinInPeriod] the date the lowest value for that day of week occurreced
				 [DateOfMaxInPeriod] the date the highest value for that day of week occurreced


				 2 rows 1 per premise per month
				 a two is a summary for whole month
				 [DailyMinInPeriod] 	   The lowest value in the month
				 [DailyMaxInPeriod]		   The highest value in the month
				 [TotalUsageInPeriod]	   the total for the month
				 [AvgerageInPeriod]		   the average during the month
				 [DateOfMinInPeriod]	   the date the lowest value for that month occurreced
				 [DateOfMaxInPeriod]	   the date the highest value for that month occurreced
	*/
--=========================================================
CREATE PROCEDURE [dbo].[LoadRunningAmiAggregates]
@year VARCHAR(04)='2014',
@month VARCHAR(02)='09',
@clientid int =101
as
BEGIN
-- Month must be 2 characters
IF LEN(@month)=1
    BEGIN
	    SET @month='0'+@month
    END

-- the code below overrides the paramter for date---------
-- if the ScgDailyTotals is not empty
DECLARE @datekey INT,@datechar VARCHAR(8)
SELECT @datekey=MIN(AmiDate) FROM InsightsBulk.[dbo].[ScgDailyTotals]

if @datekey IS NOT NULL
BEGIN
    SET @datechar= CONVERT(varchar(8), @datekey)
    SET @year=SUBSTRING(@datechar,1,4)
    SET @month=SUBSTRING(@datechar,5,2)
END
---------------------------------------------------------

if object_id ('tempdb..#AmiAggregate') is not null drop table #AmiAggregate
CREATE TABLE #AmiAggregate(
	[AmiAggregateKey] [int] IDENTITY(1,1) NOT NULL,
	[AmiKey] [int] NOT NULL,
	[AmiAggregateKeyType] [int] NOT NULL,
	[PremiseId] [varchar](50) NOT NULL,
	[DayOfWeek] [int] NULL,
	[Month] [int] NULL,
	[Year] [int] NULL,
	[DailyMinInPeriod] [decimal](18, 4) NOT NULL,
	[DailyMaxInPeriod] [decimal](18, 4) NOT NULL,
	[TotalUsageInPeriod] [decimal](38, 4) NOT NULL,
	[AvgerageInPeriod] [decimal](18, 4) NOT NULL,
	[DateOfMinInPeriod] [date] NULL,
	[DateOfMaxInPeriod] [date] NULL
)

	DECLARE  @endrangedate date= DATEADD(DAY,-1,DATEADD(MONTH,1,(CAST(@year+@month+'01' AS DATE))))  --this is end of month
	DECLARE  @startrangedate date= CAST(@year+@month+'01' AS DATE) --this is start of month

	DECLARE @startrangekey INT =cast(convert(char(8), @startrangedate, 112) as int) 
	DECLARE @endrangekey INT =cast(convert(char(8), @endrangedate, 112) as int) 

	insert #AmiAggregate
	select s.*
	,CAST(CAST(mindate AS CHAR(8)) AS DATETIME) as [DateOfMinInPeriod]
	,CAST(CAST(maxdate AS CHAR(8)) AS DATETIME) as [DateOfMaxInPeriod]
	 from
	(
	select
	amikey
	,GROUPING_ID(amikey,d.premiseid
					, DATEPART(weekday,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME))
					, DATEPART(month,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME))
				) as AmiAggregateKeyType
	,d.premiseid
	,DATEPART(weekday,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME)) as DayOfWeek
	,Datepart(month,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME)) as Month
	,max(@year) as Year
	,min(TotalUnits) as DailyMinInPeriod
	,max(TotalUnits) as DailyMaxInPeriod
	,sum(TotalUnits) as TotalUsageInPeriod
	,cast(avg(TotalUnits) as decimal(18,4)) as AvgerageInPeriod
	from  factamidetail d
	INNER join
	( SELECT    PremiseID ,
                    ContentOptionKey ,
                    ROW_NUMBER() OVER ( PARTITION BY PremiseKey,
                                        ContentAttributeKey ORDER BY TrackingDate DESC ) AS SortID
          FROM      dbo.FactPremiseAttribute
          WHERE     ClientID = @clientid
			 AND ContentAttributeKey = 'paperreport.treatmentgroup.enrollmentstatus'
        ) p
		ON p.PremiseID=d.PremiseId
		WHERE   p.SortID = 1
        AND p.ContentOptionKey = 'paperreport.treatmentgroup.enrollmentstatus.enrolled'
	--
	and DateKey BETWEEN @startrangekey AND @endrangekey
	group by amikey, d.premiseid,
	grouping sets

		(	cube(
				DATEPART(weekday,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME)),
				Datepart(month,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME)) 

			
				)
		)
	HAVING GROUPING_ID(amikey,d.premiseid
					, DATEPART(weekday,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME))
					, DATEPART(month,CAST(CAST(DateKey AS CHAR(8)) AS DATETIME))

				) 
				IN (	0,  -- FOR the DAY OF WEEK IN month
						2  -- FOR THE MONTH
					)
	/*
	ones we dont need 
	11,3,5,9,1, 13  have null years
	14, for THE a calendar YEAR
	2,8,0 all have week dim but we only want week within year
	4 is by calendar year we need over rolling year
	10 week in cal year for future use
	*/
	) as s
	outer apply  (select top 1 f2.[DateKey] as mindate from factamidetail f2
	where f2.AmiKey=s.AmiKey and s.DailyMinInPeriod=f2.TotalUnits
	and DateKey BETWEEN @startrangekey AND @endrangekey
	order by  datekey desc
	)
	as a
	outer apply  (select top 1 f2.[DateKey] as maxdate from factamidetail f2
	where f2.AmiKey=s.AmiKey and s.DailyMaxInPeriod=f2.TotalUnits
	and DateKey BETWEEN @startrangekey AND @endrangekey
	order by  datekey desc
	)
	as b

	MERGE InsightsDW.[dbo].[AmiAggregate] AS Target
		USING ( 
			SELECT 
				  [AmiKey]
				  ,[AmiAggregateKeyType]
				  ,[PremiseId]
				  ,[DayOfWeek]
				  ,[Month]
				  ,[Year]
				  ,[DailyMinInPeriod]
				  ,[DailyMaxInPeriod]
				  ,[TotalUsageInPeriod]
				  ,[AvgerageInPeriod]
				  ,[DateOfMinInPeriod]
				  ,[DateOfMaxInPeriod]
			  FROM #AmiAggregate
			) AS Source
		 ON (	
					Source.Amikey = Target.Amikey					
				and Source.[AmiAggregateKeyType] = Target.[AmiAggregateKeyType]	
				and Source.[PremiseId] = Target.[PremiseId]	
				and ((Source.[DayOfWeek] = Target.[DayOfWeek]) OR (Source.[DayOfWeek] IS NULL AND Target.[DayOfWeek] IS null))
				and ((Source.[Month] = Target.[Month]) OR (Source.[Month] IS NULL AND  Target.[Month] IS null))
				and ((Source.[Year] = Target.[Year]) OR (Source.[Year] IS NULL AND  Target.[Year] IS null))
			)
		WHEN MATCHED  THEN
			UPDATE SET	Target.[DailyMinInPeriod] = Source.[DailyMinInPeriod],
						Target.[DailyMaxInPeriod] = Source.[DailyMaxInPeriod],
						Target.[TotalUsageInPeriod] = Source.[TotalUsageInPeriod],
						Target.[AvgerageInPeriod] = Source.[AvgerageInPeriod],
						Target.[DateOfMinInPeriod] = Source.[DateOfMinInPeriod],
						Target.[DateOfMaxInPeriod] = Source.[DateOfMaxInPeriod]

		WHEN NOT MATCHED BY TARGET THEN
			INSERT (					
				  [AmiKey]
				  ,[AmiAggregateKeyType]
				  ,[PremiseId]
				  ,[DayOfWeek]
				  ,[Month]
				  ,[Year]
				  ,[DailyMinInPeriod]
				  ,[DailyMaxInPeriod]
				  ,[TotalUsageInPeriod]
				  ,[AvgerageInPeriod]
				  ,[DateOfMinInPeriod]
				  ,[DateOfMaxInPeriod]
				  )
				  VALUES
                  (				   
				  Source.[AmiKey]
				  ,Source.[AmiAggregateKeyType]
				  ,Source.[PremiseId]
				  ,Source.[DayOfWeek]
				  ,Source.[Month]
				  ,Source.[Year]
				  ,Source.[DailyMinInPeriod]
				  ,Source.[DailyMaxInPeriod]
				  ,Source.[TotalUsageInPeriod]
				  ,Source.[AvgerageInPeriod]
				  ,Source.[DateOfMinInPeriod]
				  ,Source.[DateOfMaxInPeriod]
				  );
END
GO
