SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Sunil
-- Create date: unknown
-- Description:	Produces a csv extract reffred to Home Energy Report
--
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.9.2014
-- Description: v 1.2.0
-- In Proc
-- 1.	Only display the last 5 digits of the account number 
--		and all others marked with an  asterisk “*”  (not mask the first 5)
-- 2.	On masked account number use **** (not #### ).		
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.9.2014
-- Description: v 1.5.0
--	
-- Added a report number parameter that will appear in final csv
-- to indicate the iteration of the report		
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.14.2014
-- Description: v 1.9.0
--	
-- Changed the referrerid to be clientid per barsana		
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.17.2014
-- Description: v 1.18.0
--	
-- remove any premise that has opted out from csv  !!!  This has been removed by v.19
--
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.24.2014
-- Description: v 1.19.0
--	
-- change reports to be based on channel
-- 2 options
-- 'email' OR 'print'
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.24.2014
-- Description: v 1.21.0
--	
-- added a distinct to initial load of allcolumns table
-- needed because sometimes the customer on the account changes
-- like a wife replacing a husband but the premisee, acct are the same
-- it was causing the same premise more than 1 time
-- had to move email to update statement after initial insert
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.29.2014
-- Description: v 1.22.0
--	
-- changed the distinct to a ranking window to get 1 row
-- in the join to improve performace
-- =============================================
-- Changed by:	 Wayne
-- Change date: 11.17.2014
-- Description: v 1.22.0
--	
-- changed the distinct to a ranking window to get 1 row
-- in the join to improve performace
-- =============================================
-- Changed by:	 Wayne
-- Change date: 11.14.2014
-- Description: v 1.26.0
--	
-- Eliminate premises with null or zero in month column 1
-- in export.usp_HE_Report_Create_Staging_Allcols
-- month column 1 should be  @bill_year + @bill_month
-- =============================================
-- Changed by:	 Wayne
-- Change date: 11.18.2014
-- Description: v 1.27.0
--	
-- When there are multiple customers for premise
-- in Dimcustomer the code should get the most recent customer
-- added a desc sort on the ROW_NUMBER() 
-- =============================================
-- Changed by:	 Wayne
-- Change date: 11.24.2014
-- Description: v 1.28.0
--	
-- fixed some bad [field name]s in the client table that had 
-- special (non printable) characters at end
-- and had to change cod ethat referred to
-- bad [field name] to the now corrected [field name]
-- =============================================
-- Changed by:	 Wayne
-- Change date: 11.24.2014
-- Description: v 1.29.0	
-- 
-- now evaluates the channel parameter
-- to determine if column "Subscriber_Key" is updated
-- =============================================
-- Changed by:	 Wayne
-- Change date: 12.08.2014
-- Description: v 1.30.0	
-- 
-- hotfix to correct logic for selection (in PROD as of 12.8
-- =============================================
-- Changed by:	Wayne
-- Change date: 12.31.2014
-- Description:	v 1.33.0 (same version)
--	
--  Removed the WITH (NOLOCK)'s because
--  they were causing race conditions
-- =============================================
-- Changed by:		Wayne
-- Change date:	3.12.2015
-- Version:		v 1.34.0 
--	
-- Description:	Added treatment group to output
-- =============================================
-- Changed by:		Wayne
-- Change date:	3.12.2015
-- Version:		v 1.34.0 
--	
-- Description:	If treatment group param is not null
--				Then use it to filter in addition to channel
-- =============================================
-- Possible Return Codes
-- =============================================
-- '@bill_year parameter must be numeric'									   Return Code=  -2	   State Code= 1
-- '@bill_year parameter must be > 2013'									   Return Code=  -3	   State Code= 1
-- '@bill_month parameter must be numeric'									   Return Code=  -4	   State Code= 1
-- '@bill_month parameter is invalid 1-12 are acceptable'						   Return Code=  -5	   State Code= 1
-- 'Invalid @channel parameter ony "print" and "email" are valid'				   Return Code=  -6	   State Code= 1
-- 'Invalid Proifile Name'												   Return Code=  -9	   State Code= 1
-- =============================================
CREATE PROCEDURE [Export].[usp_HE_Report_Create_Staging_Allcols]
	 @profile_name	    VARCHAR(50)	= 'SCG_Profile101'
    , @channel		    VARCHAR(5)		= 'email'		 -- options are 'email'  or 'print'
    , @bill_year	    VARCHAR(4)		= '2014'		 -- >= 2014
    , @bill_month	    VARCHAR(2)		= '10'		 -- options are 1-12
    , @treatment_groups varchar(20)	='T-9'			--separate bt commas 'T-9,T-14'
AS
BEGIN
    SET NOCOUNT ON;

    --	validate the incomming parameter values
    IF ISNUMERIC(@bill_year) = 0
        BEGIN
            RAISERROR(N'@bill_year parameter must be numeric' , -2 , 1);
        END;
    IF @bill_year < 2014
        BEGIN
            RAISERROR(N'@bill_year parameter must be > 2013' , -3 , 1);
        END;
    IF ISNUMERIC(@bill_month) = 0
        BEGIN
            RAISERROR(N'@bill_month parameter must be numeric' , -4 , 1);
        END;
    IF ISNUMERIC(@bill_month) > 12
        BEGIN
            RAISERROR(N'@bill_month parameter is invalid 1-12 are acceptable' , -5 , 1);
        END;
    IF LOWER(@channel) != 'print' AND LOWER(@channel) != 'email'
        BEGIN
            RAISERROR(N'Invalid @channel parameter ony "print" and "email" are valid' , -6 , 1);
        END;


        --	Declare additional needed to populate Profile Table values
    DECLARE
         @clientID					INT 
       , @fuel_type					VARCHAR(100) 
       , @energyobjectcategory_List	VARCHAR(1024) 
       , @period					VARCHAR(10) 
       , @season_List				VARCHAR(100) 
       , @enduse_List				VARCHAR(1024) 
       , @ordered_enduse_List			VARCHAR(1024) 
       , @criteria_id1				VARCHAR(50) 
       , @criteria_detail1			VARCHAR(255) 
       , @criteria_id2				VARCHAR(50) 
       , @criteria_detail2			VARCHAR(255) 
       , @criteria_id3				VARCHAR(50) 
       , @criteria_detail3			VARCHAR(255) 
       , @criteria_id4				VARCHAR(50) 
       , @criteria_detail4			VARCHAR(255) 
       , @criteria_id5				VARCHAR(50) 
       , @criteria_detail5			VARCHAR(255) 
       , @criteria_id6				VARCHAR(50) 
       , @criteria_detail6			VARCHAR(255) 
       , @saveProfile				CHAR(1) 
       , @verifySave				CHAR(1) 
       , @process_id				CHAR(1);

        --	Populate parameters
    SELECT @clientID				= clientID
         , @fuel_type				= fuel_type
         , @energyobjectcategory_List	= energyobjectcategory_List
         , @period					= period
         , @season_List				= season_List
         , @enduse_List				= enduse_List
         , @ordered_enduse_List		= ordered_enduse_List
         , @criteria_id1				= criteria_id1
         , @criteria_detail1			= criteria_detail1
         , @criteria_id2				= criteria_id2
         , @criteria_detail2			= criteria_detail2
         , @criteria_id3				= criteria_id3
         , @criteria_detail3			= criteria_detail3
         , @criteria_id4				= criteria_id4
         , @criteria_detail4			= criteria_detail4
         , @criteria_id5				= criteria_id5
         , @criteria_detail5			= criteria_detail5
         , @criteria_id6				= criteria_id6
         , @criteria_detail6			= criteria_detail6
      FROM export.home_energy_report_criteria_profile
      WHERE profile_name = @profile_name;
	 IF @@rowcount=0
      BEGIN
	     RAISERROR(N'Invalid Proifile Name' , -9 , 1);
      END;

    
    --	Declare additional needed parameters
    DECLARE
         @sqlcmd					VARCHAR(max) 
       , @sqlcmd_cursor				VARCHAR(8000) 
       , @column_name				VARCHAR(128)
       , @column_count				INT			 = 1
       , @tablename					VARCHAR(128)	 = '[export].[home_energy_report_staging_allcolumns]'
       , @reportchannel				VARCHAR(128) 
       , @reportchannelprintandemail	VARCHAR(128)	 = 'paperreport.channel.printandemail';

    -- new variable to use for filtering out null
    -- or zero month 1
    DECLARE
       @billyearmonth varchar(10) = CAST(CAST(CAST(@bill_year AS varchar(4)) 
							 + '-' 
							 + CAST(@bill_month AS varchar(2)) 
							 + '-' 
							 + '01' AS date)AS varchar(10));

    declare @num_energyObjectCategory int, @count_cols int

	set nocount on

	if object_id ('tempdb..#treatment_groups_list') is not null drop table #treatment_groups_list
	create table #treatment_groups_list (groupid varchar (20))
	--
	insert #treatment_groups_list (groupid)
	   SELECT cast (param as varchar (20))
	   FROM [dbo].[ufn_split_values] (@treatment_groups, ',')

    -- Convert param into an attribute for later comparisons
    IF @channel = 'email'
        BEGIN
            SET @reportchannel = 'paperreport.channel.emailonly';
        END;
    ELSE
        BEGIN
            IF @channel = 'print'
                BEGIN
                    SET @reportchannel = 'paperreport.channel.printonly';
                END;
            ELSE
                BEGIN
                    IF @channel = 'all'
                        BEGIN
                            SET @reportchannel = 'All.Participants';
                        END;
                    ELSE
                        BEGIN
                            RAISERROR('invalid channel parameter' , 1 , 1);
                        END;
                END;
        END;

    -- drop the home_energy_report_staging_allcolumns table
    SELECT @sqlcmd = 'if object_id (''' + @tablename + ''') is not null drop table ' + @tablename;
    EXEC (@sqlcmd);

    -- get the columns from thehome_energy_report_client table
    SELECT @sqlcmd_cursor =
    '
	    declare	columnlist_cursor cursor for
	    SELECT	''['' + t1.[Field Name] + ''] varchar (50) NULL''
	    FROM	[export].[home_energy_report_client] t1
	    WHERE	clientID = 0
	    order by export_key
	';
    EXEC (@sqlcmd_cursor);

    -- open the cursor containing all the columns from client tablee
    OPEN columnlist_cursor;

    --create the home_energy_report_staging_allcolumns table using the client table columns 
    SELECT @sqlcmd = 'create table ' + @tablename + ' (';
    SELECT @column_count = 1;
    FETCH NEXT FROM columnlist_cursor INTO @column_name;

    WHILE @@FETCH_STATUS = 0
        BEGIN

            IF @column_count = 1
                BEGIN
                    SELECT @sqlcmd = @sqlcmd + CHAR(13) + CHAR(10) + @column_name;
                END;
            ELSE
                BEGIN
                    SELECT @sqlcmd = @sqlcmd + CHAR(13) + CHAR(10) + ', ' + @column_name;
                END;

            FETCH NEXT FROM columnlist_cursor INTO @column_name;
            SELECT @column_count+=1;

        END;

    CLOSE columnlist_cursor;
    DEALLOCATE columnlist_cursor;

    SELECT @sqlcmd = @sqlcmd + CHAR(13) + CHAR(10) + ')';
    EXEC (@sqlcmd);
    -- table has been create



    --	Insert the rows into [InsightsDW].[export].[home_energy_report_staging_allcolumns]
    --	After this it only updates that are done to the table.
    --	--
    --	--note: the Dimcustomer  should get the most recent customer below
    --	--that wht the sort on the ROW_NUMBER() is desc
    IF @channel = 'all'  -- they must just be part of ** any ** treatment group
        BEGIN
             SELECT @sqlcmd = '
		   INSERT	' + @tablename + '
		   (	
				PremiseID
			   , Premise_Address
			   , [Premise_Address 2]
			   , Premise_City
			   , Premise_State
			   , Premise_Zip
			   , haselectric
			   , HasGas
			   , HasWater
			   , groupid
		   )
		   SELECT	distinct p1.PremiseID,
			   coalesce (p1.[Street1],'''')		as	Premise_Address,
			   coalesce (p1.[Street2],'''')		as	[Premise_Address 2],
			   coalesce (p1.[City],'''')			as	Premise_City,
			   coalesce (p1.[StateProvince],'''')	as	Premise_State,
			   coalesce (p1.[PostalCode],'''')		as	Premise_Zip,
			   p1.[ElectricService]				as	Haselectric,
			   p1.[GasService]					as	HasGas,
			   p1.[WaterService]				as	HasWater,
			   ag.Groupid       				as	groupid
		
		   FROM		dimPremise p1  
		   INNER JOIN	(
						SELECT PremiseKey, CustomerKey, 
						ROW_NUMBER() OVER(PARTITION BY premisekey ORDER BY datecreated desc) AS rnk 
						from
						[InsightsDW].[dbo].[FactCustomerPremise] fcp
					) AS fcp 	
		   on p1.premisekey=fcp.premisekey and rnk=1
		   INNER JOIN	[InsightsDW].[dbo].[DimCustomer] c	 			
		   on c.customerkey=fcp.customerkey and rnk=1
		   INNER JOIN InsightsDW.[dbo].Benchmarks fb  
		   ON fb.PremiseKey=fcp.PremiseKey
		   inner join [dbo].[vScgAnalysisGroup] ag
		   on ag.premisekey=p1.premisekey
		   WHERE	p1.clientid = ' + CONVERT(varchar(4) , @clientID) + '
				AND fb.BillMonth =''' + @billyearmonth + '''
			     AND fb.MyUsage IS NOT NULL AND fb.MyUsage  !=0 AND fb.MyUsage  >0 
				and	EXISTS
				(
				select * from 
				    (
					   SELECT  
							 fcp.PremiseKey ,
							 ContentOptionKey ,
							 ROW_NUMBER() OVER ( PARTITION BY PremiseKey,
													   ContentAttributeKey ORDER BY TrackingDate DESC ) AS SortID
						FROM      dbo.FactPremiseAttribute fcp  
						WHERE     ClientID = ' + CONVERT(varchar(10) , @ClientID) + ' and fcp.PremiseKey=p1.PremiseKey 
								   AND ContentAttributeKey = ''paperreport.treatmentgroup.enrollmentstatus''
					 ) p
					   WHERE   p.SortID = 1
					   AND p.ContentOptionKey = ''paperreport.treatmentgroup.enrollmentstatus.enrolled'' 

		  ) 
		   ';

        END;
    ELSE  -- or they must be assigned to a channel that matches the channel ** specified by a param either print or email ** 
        BEGIN
            SELECT @sqlcmd = '
		  INSERT	' + @tablename + '
		  (	
			   PremiseID
			 , Premise_Address
			 , [Premise_Address 2]
			 , Premise_City
			 , Premise_State
			 , Premise_Zip
			 , haselectric
			 , HasGas
			 , HasWater
			 , groupid
		  )
		  SELECT	distinct p1.PremiseID,
			 coalesce (p1.[Street1],'''')			as	Premise_Address,
			 coalesce (p1.[Street2],'''')			as	[Premise_Address 2],
			 coalesce (p1.[City],'''')			as	Premise_City,
			 coalesce (p1.[StateProvince],'''')	as	Premise_State,
			 coalesce (p1.[PostalCode],'''')		as	Premise_Zip,
			  p1.[ElectricService]				as	Haselectric,
			   p1.[GasService]					as	HasGas,
			   p1.[WaterService]				as	HasWater,
			   ag.Groupid       				as	groupid
		  FROM		dimPremise p1  
		  INNER JOIN	(
					   SELECT 
					     PremiseKey
					   , CustomerKey
					   , ROW_NUMBER() OVER(PARTITION BY premisekey ORDER BY datecreated  desc) AS rnk 
					   from
					   [InsightsDW].[dbo].[FactCustomerPremise] fcp 
				    ) AS fcp 	 
		  on p1.premisekey=fcp.premisekey and rnk=1
		  INNER JOIN	[InsightsDW].[dbo].[DimCustomer] c	 			
		  on c.customerkey=fcp.customerkey and rnk=1
		  INNER JOIN InsightsDW.[dbo].Benchmarks fb  
		  ON fb.PremiseKey=fcp.PremiseKey
		   inner join [dbo].[vScgAnalysisGroup] ag
		   on ag.premisekey=p1.premisekey
		  WHERE	p1.clientid = ' + CONVERT(varchar(4) , @clientID) + '
			 AND fb.BillMonth =''' + @billyearmonth + '''
			 AND fb.MyUsage IS NOT NULL AND fb.MyUsage  !=0 AND fb.MyUsage  >0 

	   AND (SELECT fpa7.ContentOptionKey FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa7
					    WHERE fpa7.ContentAttributeKey = ''paperreport.channel''
					    AND fpa7.PremiseKey=p1.PremiseKey
					    AND fpa7.EffectiveDate =   (
												    SELECT MAX(fpa8.EffectiveDate) FROM
												    [InsightsDW].[dbo].[FactPremiseAttribute] fpa8
												    WHERE fpa7.Premiseid=fpa8.Premiseid AND fpa7.ClientID=fpa8.ClientID
												    AND fpa8.ContentAttributeKey = ''paperreport.channel''
											 )
					)  in ( '''+ @reportchannel + ''', '''+ @reportchannelprintandemail + ''')
    	   AND (SELECT fpa5.ContentOptionKey FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa5
					    WHERE fpa5.ContentAttributeKey = ''paperreport.treatmentgroup.enrollmentstatus'' 
					    AND fpa5.PremiseKey=p1.PremiseKey
					    AND fpa5.EffectiveDate =   (
												    SELECT MAX(fpa6.EffectiveDate) FROM
												    [InsightsDW].[dbo].[FactPremiseAttribute] fpa6
												    WHERE fpa5.Premiseid=fpa6.Premiseid AND fpa5.ClientID=fpa6.ClientID
												    AND fpa6.ContentAttributeKey =  ''paperreport.treatmentgroup.enrollmentstatus'' 
											 )
					)= ''paperreport.treatmentgroup.enrollmentstatus.enrolled'''
	    IF (SELECT COUNT(*) FROM #treatment_groups_list) >0
	    BEGIN
		    SELECT @sqlcmd = @sqlcmd + ' AND ag.groupid in (SELECT groupid FROM #treatment_groups_list)'
	    END
		  
    END;
    EXEC (@sqlcmd);
	

    --	From wayne:
    --	This update below could probably be merged with above

    -- changed below  for Masking
    -- 1.	Only display the last 5 digits of the account number 
    --		and all others marked with an  asterisk “*”  (not mask the first 5)
    -- 2.	On masked account number use **** (not #### ).
    SELECT @sqlcmd =
    '
	    ;
	    WITH cust_key AS 
	    (
		    SELECT [CustomerKey], PremiseId, AccountId
		    FROM
		    (		
			    SELECT 
					 [CustomerKey]
				    , p1.PremiseId
				    , p1.AccountId
				    , ROW_NUMBER () OVER (PARTITION by p1.PremiseKey ORDER BY datecreated  desc) row_id
			    FROM [dbo].[FactCustomerPremise] fcp  
			    INNER JOIN [dbo].[DimPremise] p1  
			    on p1.PremiseKey = fcp.PremiseKey  
			    WHERE	p1.clientid = ' + CONVERT(varchar(4) , @clientID) + '
				   AND	EXISTS
				   (    
					   SELECT *
					   FROM	Benchmarks pm
					   WHERE	pm.PremiseKey = p1.PremiseKey  
				   )		
		    ) f2
			 WHERE f2.row_id = 1
	    )
	    '
    IF @channel = 'email'  -- only email will have Subscriber_Key
		  BEGIN
			  SELECT @sqlcmd = @sqlcmd + '
			  UPDATE	t1
			  SET	
				    ReferrerID		    = ''' + CONVERT(varchar(4) , @clientID) + ''',
				    Mailing_Name	    = c1.[FirstName] + '' '' + c1.[LastName],
				    Premise_Name	    = c1.[FirstName] + '' '' + c1.[LastName],
				    Mailing_Address	    = c1.[Street1] ,
				    [Mailing_Address 2] = c1.[Street2] ,
				    Mailing_City	    = c1.[City] ,
				    Mailing_State	    = c1.[StateProvince] ,
				    Mailing_Zip	    = c1.[PostalCode] ,
				    Account_Number	    = ck.[AccountId] ,
				    Subscriber_Key	    = ck.[AccountId] ,
				    CustomerID		    = c1.CustomerID,
				    Email			    = substring(c1.emailaddress,1,50),
				    MaskedAccountNumber = substring(''********************'',1,LEN(ck.[AccountId]) - 5)  
				    + substring(ck.[AccountId],LEN(ck.[AccountId]) - 5 + 1, LEN(ck.[AccountId]) - 1) 
			  FROM	 DimCustomer c1  
				    INNER JOIN cust_key ck  			ON ck.CustomerKey = c1.CustomerKey
				    INNER JOIN ' + @tablename + ' t1  	ON t1.PremiseID = ck.PremiseID
			  '
		   END	 
	 ELSE
	        BEGIN
             SELECT @sqlcmd = @sqlcmd + '
		   UPDATE	t1
		   SET	
				ReferrerID		 = ''' + CONVERT(varchar(4) , @clientID) + ''',
				Mailing_Name		 = c1.[FirstName] + '' '' + c1.[LastName],
				Premise_Name		 = c1.[FirstName] + '' '' + c1.[LastName],
				Mailing_Address	 = c1.[Street1] ,
				[Mailing_Address 2]  = c1.[Street2] ,
				Mailing_City		 = c1.[City] ,
				Mailing_State		 = c1.[StateProvince] ,
				Mailing_Zip		 = c1.[PostalCode] ,
				Account_Number		 = ck.[AccountId] ,
				CustomerID		 = c1.CustomerID,
				Email			 = substring(c1.emailaddress,1,50),
				MaskedAccountNumber = substring(''********************'',1,LEN(ck.[AccountId]) - 5)  
				+ substring(ck.[AccountId],LEN(ck.[AccountId]) - 5 + 1, LEN(ck.[AccountId]) - 1) 
		   FROM	 DimCustomer c1  
				INNER JOIN cust_key ck  			ON ck.CustomerKey = c1.CustomerKey
				INNER JOIN ' + @tablename + ' t1  	ON t1.PremiseID = ck.PremiseID
		   '
	    END
      
	    ;
    EXEC (@sqlcmd);

    -- UPDATE square footage
    UPDATE InsightsDW.export.home_energy_report_staging_allcolumns
      SET HomeSquareFootage = fpa.value
      FROM InsightsDW.dbo.factPremiseAttribute fpa 
	 INNER JOIN InsightsDW.export.home_energy_report_staging_allcolumns ac
      ON ac.PremiseID = fpa.PremiseId
      WHERE fpa.ContentOptionKey	 = 'house.totalarea.value'
        AND fpa.effectivedate		 = (SELECT MAX(fpa2.effectivedate)
								   FROM InsightsDW.dbo.factPremiseAttribute fpa2
								   WHERE fpa2.ContentOptionKey = 'house.totalarea.value'
									AND fpa2.PremiseId = fpa.PremiseId
								);

    -- UPDATE blank columns that will be filled in with promo code stuff by sam
    -- note the column names below were goofed with extra characters at end
    -- I fixed them in the client table 11.24 - wayne
    UPDATE InsightsDW.export.home_energy_report_staging_allcolumns
      SET ImproveinsulationSavings = ''
        , NewwaterheaterSavings = ''
        , InsulationRebateSavings = ''
        , PromoCode1 = ''
        , PromoCode2 = '';

    SET NOCOUNT OFF;
END;


GO
