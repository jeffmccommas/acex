SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[GetRecommendedActions]
    @ClientID AS INT ,
    @CustomerID AS VARCHAR(50) = NULL ,
    @AccountID AS VARCHAR(50) = NULL ,
    @PremiseID AS VARCHAR(50) = NULL ,
    @CommodityKeys AS VARCHAR(500) = 'default' ,
    @Types AS VARCHAR(500) = 'default' ,
    @EndUseKeys AS VARCHAR(500) = 'default' ,
    @ApplianceKeys AS VARCHAR(500) = 'default' ,
    @MeasurementKeys AS VARCHAR(500) = NULL ,
    @WhatIfData AS VARCHAR(MAX) = NULL ,
    @ProfileDefaultCollectionKey AS VARCHAR(50) = NULL
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @CommodityTable TABLE ( Value VARCHAR(50) );
        DECLARE @TypeTable TABLE ( Value VARCHAR(50) );
        DECLARE @EndUseTable TABLE ( Value VARCHAR(50) );
        DECLARE @ApplianceTable TABLE ( Value VARCHAR(50) );
        DECLARE @MeasurementsTable TABLE ( Value VARCHAR(50) );
        DECLARE @WhatifDataTable TABLE ( Value VARCHAR(100) );

        INSERT  INTO @CommodityTable
                EXEC [dbo].[SplitIntoTable] @ValueList = @CommodityKeys;

        INSERT  INTO @TypeTable
                EXEC [dbo].[SplitIntoTable] @ValueList = @Types;

        INSERT  INTO @EndUseTable
                EXEC [dbo].[SplitIntoTable] @ValueList = @EndUseKeys;

        INSERT  INTO @ApplianceTable
                EXEC [dbo].[SplitIntoTable] @ValueList = @ApplianceKeys;

        INSERT  INTO @WhatifDataTable
                EXEC [dbo].[SplitIntoTable] @ValueList = @WhatIfData;

        CREATE TABLE #RegionalDefaults
            (
              AttributeKey VARCHAR(256) ,
              DefaultKey VARCHAR(256) ,
              DefaultValue VARCHAR(256)
            );

        CREATE TABLE #Premises
            (
              CustomerId VARCHAR(50) ,
              AccountId VARCHAR(50) ,
              PremiseId VARCHAR(50) ,
              PremiseKey VARCHAR(50)
            );

        CREATE TABLE #PremiseAttributeValues
            (
              PremiseKey INT ,
              AttributeKey VARCHAR(256) ,
              AttributeValue VARCHAR(256) ,
              OptionKey INT ,
              EffectiveDate DATETIME
            );

        INSERT  INTO #RegionalDefaults
                EXEC InsightsMetadata.[dbo].[GetBenchmarkRegionalDefaults] @ClientId = @ClientId,
                    @ProfileDefaultCollectionKey = @ProfileDefaultCollectionKey;

        WITH    WhatIfData
                  AS ( SELECT   LTRIM(RTRIM(SUBSTRING(Value, 0,
                                                      CHARINDEX(':', Value, 1)))) AS AttributeKey ,
                                LTRIM(RTRIM(SUBSTRING(Value,
                                                      CHARINDEX(':', Value, 1)
                                                      + 1, 50))) AS DefaultValue
                       FROM     @WhatifDataTable
                     )
            SELECT  wd.* ,
                    po.OptionKey AS DefaultOptionKey
            INTO    #WhatIfData
            FROM    WhatIfData wd
                    INNER JOIN dbo.DimPremiseAttribute pa ON pa.CMSAttributeKey = wd.AttributeKey
                    LEFT JOIN dbo.DimPremiseOption po ON po.CMSOptionKey = wd.DefaultValue;

        IF @CustomerID IS NOT NULL
            BEGIN
                INSERT  INTO #Premises
                        SELECT DISTINCT
                                c.CustomerId ,
                                p.AccountId ,
                                p.PremiseId ,
                                p.PremiseKey
                        FROM    dbo.DimPremise p
                                INNER JOIN dbo.FactCustomerPremise cp ON cp.PremiseKey = p.PremiseKey
                                INNER JOIN dbo.DimCustomer c ON c.CustomerKey = cp.CustomerKey
                                INNER JOIN dbo.DimClient r ON r.ClientKey = c.ClientKey
                        WHERE   r.ClientId = @ClientID
                                AND c.CustomerId = @CustomerID
                                AND ( @AccountID IS NULL
                                      OR p.AccountId = @AccountID
                                    )
                                AND ( @PremiseID IS NULL
                                      OR p.PremiseId = @PremiseID
                                    );

                INSERT  INTO #PremiseAttributeValues
                        SELECT  PremiseKey ,
                                ppa.AttributeKey ,
                                ppa.AttributeValue AS AttributeValue ,
                                po.OptionKey ,
                                ppa.EffectiveDate
                        FROM    #Premises p
                                INNER JOIN [DBLINK_API].[Insights].[wh].[ProfilePremiseAttribute] ppa ON ppa.ClientId = @ClientID
                                                              AND ppa.AccountId = p.AccountId
                                                              AND ppa.PremiseId = p.PremiseId
                                INNER JOIN dbo.DimPremiseAttribute pa ON pa.CMSAttributeKey = ppa.AttributeKey
                                LEFT JOIN dbo.DimPremiseOption po ON po.AttributeKey = pa.AttributeKey
                                                              AND po.CMSOptionKey = ppa.AttributeValue; 
            END;
        ELSE
            BEGIN

                INSERT  INTO #Premises
                        SELECT  'Customer_XXX' AS CustomerId ,
                                'Account_XXX' AS AccountId ,
                                'Premise_XXX' AS PremiseId ,
                                '-1' AS PremiseKey;
            END;

        WITH    EndUses
                  AS ( SELECT   ClientEnduseID ,
                                EnduseKey ,
                                ROW_NUMBER() OVER ( PARTITION BY EnduseKey ORDER BY ClientID DESC ) AS SortId
                       FROM     InsightsMetadata.cm.ClientEnduse
                       WHERE    ClientID = 0
                                OR ClientID = @ClientID
                     )
            SELECT  EnduseKey ,
                    ApplianceKey
            INTO    #EndUses
            FROM    EndUses eu
                    INNER JOIN InsightsMetadata.cm.ClientEnduseAppliance eua ON eua.ClientEnduseID = eu.ClientEnduseID
            WHERE   SortId = 1;
				
        SELECT  rd.AttributeKey ,
                IIF(DefaultValue IS NULL, DefaultKey, DefaultValue) AS DefaultValue ,
                po.OptionKey AS DefaultOptionKey
        INTO    #DefaultAttributes
        FROM    #RegionalDefaults rd
                INNER JOIN dbo.DimPremiseAttribute pa ON pa.CMSAttributeKey = rd.AttributeKey
                INNER JOIN dbo.DimPremiseOption po ON po.CMSOptionKey = rd.DefaultKey;

        SELECT  [ClientConditionID] ,
                [ConditionKey] ,
                cc.[ProfileAttributeKey] ,
                ProfileAttributeTypeKey ,
                [Operator] ,
                IIF(ProfileAttributeTypeKey = 'decimal'
                OR ProfileAttributeTypeKey = 'integer', Value, ProfileOptionKey) AS ActualValue ,
                IIF(ProfileAttributeTypeKey = 'decimal'
                OR ProfileAttributeTypeKey = 'integer', Value, OptionKey) AS AttributeValue
        INTO    #Conditions
        FROM    InsightsMetadata.[cm].[ClientCondition] cc
                INNER JOIN InsightsMetadata.[cm].[TypeProfileAttribute] tpa ON cc.ProfileAttributeKey = tpa.ProfileAttributeKey
                INNER JOIN dbo.DimPremiseAttribute pa ON pa.CMSAttributeKey = cc.ProfileAttributeKey
                INNER JOIN dbo.DimPremiseOption po ON po.AttributeKey = pa.AttributeKey
                                                      AND po.CMSOptionKey = cc.ProfileOptionKey;
													  											  
        
        SELECT  PremiseKey ,
                c.ProfileAttributeKey
        INTO    #PremiseAttributes
        FROM    #Premises p
                CROSS JOIN ( SELECT DISTINCT
                                    ProfileAttributeKey
                             FROM   #Conditions
                           ) c;
       			        
										                             
        SELECT  PremiseKey ,
                ClientConditionID ,
                ConditionKey
        INTO    #PresmieConditions
        FROM    ( SELECT    p.PremiseKey ,
                            p.ProfileAttributeKey ,
                            ROW_NUMBER() OVER ( PARTITION BY p.PremiseKey,
                                                c.ClientConditionID ORDER BY fpa.EffectiveDate DESC ) AS SortId ,
                            IIF(ProfileAttributeTypeKey = 'decimal'
                            OR ProfileAttributeTypeKey = 'integer', ISNULL(ISNULL(wd.DefaultValue,
                                                              fpa.AttributeValue),
                                                              rd.DefaultValue), CONVERT(FLOAT, ISNULL(ISNULL(wd.DefaultOptionKey,
                                                              fpa.OptionKey),
                                                              rd.DefaultOptionKey))) AS PremiseAttributeValue ,
                            c.ClientConditionID ,
                            c.ConditionKey ,
                            c.Operator ,
                            c.ActualValue ,
                            c.AttributeValue
                  FROM      #PremiseAttributes p
                            LEFT JOIN #WhatIfData wd ON wd.AttributeKey = p.ProfileAttributeKey
                            LEFT JOIN #PremiseAttributeValues fpa ON p.PremiseKey = fpa.PremiseKey
                                                              AND p.ProfileAttributeKey = fpa.AttributeKey
                            LEFT JOIN #DefaultAttributes rd ON rd.AttributeKey = p.ProfileAttributeKey
                            INNER JOIN #Conditions c ON p.ProfileAttributeKey = c.ProfileAttributeKey
                ) a
        WHERE   SortId = 1
                AND ( ( Operator = '='
                        AND PremiseAttributeValue = AttributeValue
                      )
                      OR ( Operator = '>'
                           AND PremiseAttributeValue > AttributeValue
                         )
                      OR ( Operator = '>='
                           AND PremiseAttributeValue >= AttributeValue
                         )
                      OR ( Operator = '<'
                           AND PremiseAttributeValue < AttributeValue
                         )
                      OR ( Operator = '<='
                           AND PremiseAttributeValue <= AttributeValue
                         )
                    );
       
        --SELECT  ca.ClientActionID ,
        --        ActionKey ,
        --        cac.ConditionKey ,
        --        COUNT(ConditionKey) OVER ( PARTITION BY ca.ClientActionID ) AS ConditionsCount
        --INTO    #Actions
        --FROM    InsightsMetadata.[cm].[ClientAction] ca
        --        LEFT JOIN InsightsMetadata.[cm].[ClientActionCondition] cac ON ca.ClientActionID = cac.ClientActionID
        --WHERE   ca.Disable = 0;

        WITH    ClientActions
                  AS ( SELECT   ClientActionID ,
                                ActionKey ,
                                Disable ,
                                ROW_NUMBER() OVER ( PARTITION BY ActionKey ORDER BY ClientID DESC ) AS SortID
                       FROM     InsightsMetadata.[cm].[ClientAction]
                       WHERE    ClientID = @ClientID
                                OR ClientID = 0
                     )
            SELECT  ca.ClientActionID ,
                    ActionKey ,
                    cac.ConditionKey ,
                    COUNT(ConditionKey) OVER ( PARTITION BY ca.ClientActionID ) AS ConditionsCount
            INTO    #Actions
            FROM    ClientActions ca
                    LEFT JOIN InsightsMetadata.[cm].[ClientActionCondition] cac ON ca.ClientActionID = cac.ClientActionID
            WHERE   ca.SortID = 1
                    AND ca.Disable = 0;

        SELECT  PremiseKey ,
                ClientActionID ,
                ActionKey
        INTO    #PremiseActions
        FROM    #PresmieConditions pc
                INNER JOIN #Actions a ON pc.ConditionKey = a.ConditionKey
                                         OR a.ConditionKey IS NULL
        GROUP BY PremiseKey ,
                ClientActionID ,
                ActionKey
        HAVING  COUNT(a.ConditionKey) = MIN(ConditionsCount)
        ORDER BY PremiseKey ,
                ClientActionID;

        SELECT  cas.ClientActionSavingsID ,
                ActionKey ,
                ConditionKey ,
                COUNT(*) OVER ( PARTITION BY cas.ClientActionSavingsID ) AS TotalSavingConditions
        INTO    #ActionSavings
        FROM    InsightsMetadata.[cm].[ClientActionSavings] cas
                INNER JOIN InsightsMetadata.[cm].[ClientActionSavingsCondition] casc ON cas.ClientActionSavingsID = casc.ClientActionSavingsID;

        SELECT  pa.PremiseKey ,
                ClientActionSavingsID
        INTO    #PremiseSavings
        FROM    #PremiseActions pa
                INNER JOIN #ActionSavings a ON pa.ActionKey = a.ActionKey
                INNER JOIN #PresmieConditions pc ON pc.PremiseKey = pa.PremiseKey
                                                    AND pc.ConditionKey = a.ConditionKey
        GROUP BY pa.PremiseKey ,
                ClientActionSavingsID
        HAVING  COUNT(a.ConditionKey) = MIN(TotalSavingConditions);
				
        SELECT  DISTINCT
                PremiseKey ,
                ps.ClientActionSavingsID ,
                cas.ActionKey AS [Key] ,
                ca.CommodityKey ,
                Rank ,
                Cost AS UpfrontCost ,
                0.0 AS AnnualCost ,
                ca.CostVariancePercent ,
                cas.AnnualSavingsEstimate ,
                cas.AnnualUsageSavingsEstimate ,
                ca.AnnualUsageSavingsUomKey ,
                'good' AS SavingsEstimateQuality ,
                ( Cost / cas.AnnualSavingsEstimate ) * 12 AS PayBackTime ,
                cas.AnnualSavingsEstimate * 10 AS ROI ,
                '' AS Popularity
        INTO    #Result
        FROM    #PremiseSavings ps
                INNER JOIN InsightsMetadata.[cm].[ClientActionSavings] cas ON ps.ClientActionSavingsID = cas.ClientActionSavingsID
                INNER JOIN InsightsMetadata.cm.ClientAction ca ON ca.ActionKey = cas.ActionKey
                INNER JOIN InsightsMetadata.cm.ClientActionAppliance caa ON caa.ClientActionID = ca.ClientActionID
                INNER JOIN #EndUses eu ON eu.ApplianceKey = caa.ApplianceKey
                INNER JOIN @CommodityTable ct ON ct.Value = ca.CommodityKey
                                                 OR ct.Value = 'default'
                INNER JOIN @TypeTable tt ON tt.Value = ca.ActionTypeKey
                                            OR tt.Value = 'default'
                INNER JOIN @ApplianceTable at ON at.Value = caa.ApplianceKey
                                                 OR at.Value = 'default'
                INNER JOIN @EndUseTable eut ON eut.Value = eu.EnduseKey
                                               OR eut.Value = 'default';

        SELECT  p.CustomerId ,
                p.AccountId ,
                p.PremiseId ,
                r.*
        FROM    #Result r
                INNER JOIN #Premises p ON p.PremiseKey = r.PremiseKey;

        IF @MeasurementKeys IS NOT NULL
            BEGIN

                INSERT  INTO @MeasurementsTable
                        EXEC [dbo].[SplitIntoTable] @ValueList = @MeasurementKeys;

                WITH    CalcMeasures
                          AS ( SELECT   PremiseKey ,
                                        ClientActionSavingsID ,
                                        [Key] ,
                                        CASE WHEN CommodityKey = 'electric'
                                             THEN AnnualUsageSavingsEstimate
                                                  / 1.52
                                             WHEN CommodityKey = 'gas'
                                             THEN AnnualUsageSavingsEstimate
                                                  / 11.7
                                             WHEN CommodityKey = 'water'
                                             THEN 0
                                        END AS Co2 ,
                                        CASE WHEN CommodityKey = 'electric'
                                             THEN ( AnnualUsageSavingsEstimate
                                                    / 1.52 ) / 85.98
                                             WHEN CommodityKey = 'gas'
                                             THEN ( AnnualUsageSavingsEstimate
                                                    / 11.7 ) / 85.98
                                             WHEN CommodityKey = 'water'
                                             THEN 0
                                        END AS Trees ,
                                        100 AS Points
                               FROM     #Result
                             )
                    SELECT  PremiseKey ,
                            ClientActionSavingsID ,
                            [Key] ,
                            m.Value AS MeasurementType ,
                            CASE WHEN m.Value = 'co2' THEN cm.Co2
                                 WHEN m.Value = 'trees' THEN cm.Trees
                                 WHEN m.Value = 'points' THEN cm.Points
                            END AS MeasurementQuantity ,
                            'lbs' AS MeasurementKey
                    FROM    CalcMeasures cm
                            CROSS JOIN @MeasurementsTable m
                    WHERE   m.Value IN ( 'co2', 'trees', 'points' )
                    ORDER BY PremiseKey ,
                            [Key] ,
                            MeasurementType;

            END;

        DROP TABLE #PremiseSavings;
        DROP TABLE #ActionSavings;
        DROP TABLE #PremiseActions;
        DROP TABLE #Actions;
        DROP TABLE #PresmieConditions;
        DROP TABLE #Conditions;
        DROP TABLE #PremiseAttributeValues;
        DROP TABLE #PremiseAttributes;
        DROP TABLE #DefaultAttributes;
        DROP TABLE #RegionalDefaults;
        DROP TABLE #WhatIfData;
        DROP TABLE #Premises;
        DROP TABLE #EndUses;
        DROP TABLE #Result;

    END;

GO
