SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---

/*
-- =======================================
-- Written by:	 Wayne
-- Date:		 4/9/15
-- Decription:	 Imports the disag from the dll
--			 into a dynamically named temp table
--
-- =======================================
-- change by:	 Wayne
-- Date:		 7/24/15
-- Decription:	 change global temp table to a real table	 
-- =======================================
*/
CREATE PROCEDURE [dbo].[ImportDisAggToTemp]
(
    @detailRow [dbo].[DisAggDetail] READONLY,
    @clientid INT
)
AS
BEGIN
 
    SET NOCOUNT ON;

DECLARE @sqlstring NVARCHAR(2000)
SET @sqlstring=' 
    INSERT INTO  dbo.BillDisaggTemp' + CAST(@clientid AS VARCHAR(5)) 
+ '(
       [ClientID]
      ,[CustomerID]
      ,[AccountID]
      ,[PremiseID]
      ,[BillDate]
      ,[EndUseKey]
      ,[ApplianceKey]
      ,[CommodityKey]
      ,[PeriodID]
      ,[Usage]
      ,[Cost]
      ,[UomKey]
      ,[CurrencyKey]
      ,[Confidence]
      ,[ReconciliationRatio]
      ,[StatusID]
      ,[NewDate]

)

SELECT [ClientID]
      ,[CustomerID]
      ,[AccountID]
      ,[PremiseID]
      ,[BillDate]
      ,[EndUseKey]
      ,[ApplianceKey]
      ,[CommodityKey]
      ,[PeriodID]
      ,[Usage]
      ,[Cost]
      ,[UomKey]
      ,[CurrencyKey]
      ,[Confidence]
      ,[ReconciliationRatio]
      ,[StatusID]
      ,[NewDate]
            FROM  @mydetailRow d 
		  WHERE d.clientid='+ CAST(@clientid AS VARCHAR(5)) 
 

EXEC sp_executesql @sqlstring, N'@mydetailRow [dbo].[DisAggDetail] READONLY', @mydetailRow=@detailRow;

--EXEC (@sqlstring)
END

GO
