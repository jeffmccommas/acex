SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/16/2014
-- Description:	
-- =============================================
create PROCEDURE [ETL].[D_STG_FactBilling]
				 @ClientID INT
AS

BEGIN

	SET NOCOUNT ON;

	DELETE FROM ETL.INS_FactBilling WHERE ClientID = @ClientID
	DELETE FROM ETL.T1U_FactBilling WHERE ClientID = @ClientID

END





GO
