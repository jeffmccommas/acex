SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
CREATE PROCEDURE [dbo].[GetPremiseAttributesByDate]
    @ClientID INT ,
    @LastRunDate AS DATETIME
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT  c.ProfileAttributeKey ,
                c.ProfileAttributeTypeKey
        INTO    #ClientProfileAttributes
        FROM    ( SELECT    tpa.ProfileAttributeKey ,
                            ClientProfileAttributeID ,
                            ProfileAttributeTypeKey ,
                            ROW_NUMBER() OVER ( PARTITION BY tpa.ProfileAttributeKey ORDER BY ClientID DESC ) AS SortId
                  FROM      [InsightsMetadata].[cm].TypeProfileAttribute tpa
                            INNER JOIN [InsightsMetadata].[cm].ClientProfileAttribute cpa ON tpa.ProfileAttributeKey = cpa.ProfileAttributeKey
                  WHERE     ClientID = 0
                            OR ClientID = @ClientID
                ) c
        WHERE   SortId = 1;

        WITH    ModifiedAttributes
                  AS ( SELECT   PremiseKey ,
                                ContentAttributeKey ,
                                ContentOptionKey ,
                                Value ,
                                EffectiveDate ,
                                ROW_NUMBER() OVER ( PARTITION BY PremiseKey,
                                                    ContentAttributeKey ORDER BY EffectiveDate DESC ) AS SortId
                       FROM     dbo.FactPremiseAttribute
                       WHERE    ClientID = @ClientID
                                AND EffectiveDate > @LastRunDate
                     )
            SELECT  ma.PremiseKey ,
                    ma.ContentAttributeKey ,
                    IIF(cpa.ProfileAttributeTypeKey = 'list'
                    OR cpa.ProfileAttributeTypeKey = 'boolean', ma.ContentOptionKey, ma.Value) AS Value ,
                    ma.EffectiveDate
            INTO    #Result
            FROM    ModifiedAttributes ma
                    INNER JOIN #ClientProfileAttributes cpa ON ma.ContentAttributeKey = cpa.ProfileAttributeKey
            WHERE   ma.SortId = 1
            ORDER BY ma.PremiseKey ,
                    ma.ContentAttributeKey;	 

        SELECT  *
        FROM    ( SELECT    c.CustomerId ,
                            p.AccountId ,
                            p.PremiseId ,
                            ContentAttributeKey ,
                            Value
                  FROM      #Result r
                            INNER JOIN dbo.DimPremise p ON p.PremiseKey = r.PremiseKey
                            INNER JOIN dbo.FactCustomerPremise cp ON cp.PremiseKey = p.PremiseKey
                            INNER JOIN dbo.DimCustomer c ON c.CustomerKey = cp.CustomerKey
                ) r PIVOT
( MAX(Value) FOR ContentAttributeKey IN ( "waterheater.fuel",
                                          "heatsystem.fuel", "centralac.count",
                                          "house.totalarea", "house.style",
                                          "house.basementstyle",
                                          "house.people", "house.yearbuilt",
                                          "heatsystem.style",
                                          "heatsystem.yearinstalled",
                                          "heatsystem.efficiencyvalue",
                                          "waterheater.year",
                                          "waterheater.efficiencyvalue",
                                          "house.windowqualitypoor",
                                          "house.insulationqualitypoor",
                                          "roomac.count", "centralac.style",
                                          "pool.count", "pool.fuel",
                                          "house.bedrooms",
                                          "house.assessedvalue",
                                          "shower.showerheadflow",
                                          "lighting.floortype",
                                          "lighting.outsidetype",
                                          "lighting.spottype",
                                          "lighting.walltype",
                                          "refrigerator.year",
                                          "house.wallinsulationinches",
                                          "house.atticinsulationinches" ) ) AS PivotTable;


        DROP TABLE #Result;
        DROP TABLE #ClientProfileAttributes;
		 


    END;









GO
