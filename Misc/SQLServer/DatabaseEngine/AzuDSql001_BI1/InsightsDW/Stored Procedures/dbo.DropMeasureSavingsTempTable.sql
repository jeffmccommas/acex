SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
-- =======================================
-- Written by:	Wayne
-- Date:		07.20.15
-- Description: Drop the temp table used for Measure Savings
-- 				caclulations
-- =======================================
*/
CREATE PROCEDURE [dbo].[DropMeasureSavingsTempTable]
(
    @ClientID       INT = 101
)
as
BEGIN
DECLARE @sqlstring VARCHAR(2000)=
'
IF OBJECT_ID(''dbo.MeasureSavingsTemp' + CAST(@ClientID AS varchar(5)) + ''') IS NOT NULL
    DROP TABLE dbo.MeasureSavingsTemp' + CAST(@ClientID AS varchar(5)) 
exec (@sqlstring)
END

GO
