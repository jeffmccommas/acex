SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
====================================================================================
Written by:	 Wayne
Date:		 5.4.15
Description:	 Obfuscates the first and last names, email address, and phone numbers
			 in both the customers and Premise tables
			 
===================================================================================
IMPORTANT:  The way this proc detectss if the row has already been obfuscated or not 
		  is by the email address.
		  if the email address = is euqal to firsname.lastname@somefakeemail.com
		  then the row has been obfuscated
======================================================================================
checking:
=========
run script below to check in advance:
=====================================
SELECT TOP 10 ' before: ' + ISNULL(FirstName,'') + ' ' + ISNULL(LastName,'')  + '  Street: ' + ISNULL(dp.street1,'')  + '  Email: ' + ISNULL(c.EmailAddress,'')  + '  Phone: ' +  ISNULL(c.PhoneNumber,'') ,
			 ' after: ' + UPPER(left (dbo.fnObfuscate(ISNULL(firstname,'')),1)) + lower(SUBSTRING(dbo.fnObfuscate(ISNULL(firstname,'')),2, LEN(dbo.fnObfuscate(ISNULL(firstname,''))))) 
					   + ' ' 
					  + UPPER(left (dbo.fnObfuscate(ISNULL(lastname,'')),1)) + lower(SUBSTRING(dbo.fnObfuscate(ISNULL(lastname,'')),2, LEN(dbo.fnObfuscate(ISNULL(lastname,''))))) 
					  + '  Street: ' + dbo.fnObfuscatestreet(dp.Street1)	
					  + '  Email: ' + + UPPER(left (dbo.fnObfuscate(ISNULL(firstname,'')),1)) + lower(SUBSTRING(dbo.fnObfuscate(ISNULL(firstname,'')),2, LEN(dbo.fnObfuscate(ISNULL(firstname,''))))) 
					   + '.' 
					  + UPPER(left (dbo.fnObfuscate(ISNULL(lastname,'')),1)) + lower(SUBSTRING(dbo.fnObfuscate(ISNULL(lastname,'')),2, LEN(dbo.fnObfuscate(ISNULL(lastname,''))))) 
					  + '@somefakeemail.com'	  
					  + '  Phone: ' + REPLACE(CASE WHEN ISNUMERIC(c.PhoneNumber )=1 THEN SUBSTRING(c.PhoneNumber,1,3)+ SUBSTRING(c.PhoneNumber,10,1) + SUBSTRING(c.PhoneNumber,4,1) + SUBSTRING(c.PhoneNumber,9,1) + SUBSTRING(c.PhoneNumber,5,1) +
					   SUBSTRING(c.PhoneNumber,8,1) + SUBSTRING(c.PhoneNumber,6,1) + SUBSTRING(c.PhoneNumber,7,1) 
					   ELSE REVERSE(c.PhoneNumber) END ,'-','')
					  FROM dbo.DimCustomer c
INNER JOIN dbo.FactCustomerPremise fcp
ON fcp.CustomerKey = c.CustomerKey
INNER JOIN dbo.DimPremise dp
ON dp.PremiseKey = fcp.PremiseKey
WHERE c.ClientId=101
===================================================================================
*/
CREATE PROC [dbo].[ObfuscateDwData]
@client_id INT = 101
AS
BEGIN

    UPDATE  dbo.DimCustomer
    SET FirstName= UPPER(LEFT (dbo.fnObfuscate(firstname),1)) + LOWER(SUBSTRING(dbo.fnObfuscate(firstname),2, LEN(dbo.fnObfuscate(firstname)))) 
	  ,LastName= UPPER(LEFT (dbo.fnObfuscate(lastname),1)) + LOWER(SUBSTRING(dbo.fnObfuscate(lastname),2, LEN(dbo.fnObfuscate(lastname)))) 
	  ,Street1=dbo.fnObfuscatestreet(c.Street1)
	  ,EmailAddress=+ UPPER(LEFT (dbo.fnObfuscate(firstname),1)) + LOWER(SUBSTRING(dbo.fnObfuscate(firstname),2, LEN(dbo.fnObfuscate(firstname)))) 
						  + '.' 
						 + UPPER(LEFT (dbo.fnObfuscate(lastname),1)) + LOWER(SUBSTRING(dbo.fnObfuscate(lastname),2, LEN(dbo.fnObfuscate(lastname)))) 
						 + '@somefakeemail.com'	
	  ,alternateEmailAddress=+ UPPER(LEFT (dbo.fnObfuscate(firstname),1)) + LOWER(SUBSTRING(dbo.fnObfuscate(firstname),2, LEN(dbo.fnObfuscate(firstname)))) 
						  + '.' 
						 + UPPER(LEFT (dbo.fnObfuscate(lastname),1)) + LOWER(SUBSTRING(dbo.fnObfuscate(lastname),2, LEN(dbo.fnObfuscate(lastname)))) 
						 + '@OTHERfake.com'	
	   ,PhoneNumber = REPLACE(CASE WHEN ISNUMERIC(c.PhoneNumber )=1 THEN SUBSTRING(c.PhoneNumber,1,3)+ SUBSTRING(c.PhoneNumber,10,1) + SUBSTRING(c.PhoneNumber,4,1) + SUBSTRING(c.PhoneNumber,9,1) + SUBSTRING(c.PhoneNumber,5,1) +
					   SUBSTRING(c.PhoneNumber,8,1) + SUBSTRING(c.PhoneNumber,6,1) + SUBSTRING(c.PhoneNumber,7,1)
					   ELSE REVERSE(c.PhoneNumber) END ,'-','')
	  ,MobilePhoneNumber = REPLACE(CASE WHEN ISNUMERIC(c.MobilePhoneNumber )=1 THEN SUBSTRING(c.MobilePhoneNumber,1,3)+ SUBSTRING(c.MobilePhoneNumber,10,1) + SUBSTRING(c.MobilePhoneNumber,4,1) + SUBSTRING(c.MobilePhoneNumber,9,1) + SUBSTRING(c.MobilePhoneNumber,5,1) +
					   SUBSTRING(c.MobilePhoneNumber,8,1) + SUBSTRING(c.MobilePhoneNumber,6,1) + SUBSTRING(c.MobilePhoneNumber,7,1) 
					   ELSE REVERSE(c.MobilePhoneNumber) END,'-','')
    FROM  dbo.DimCustomer c
    INNER JOIN dbo.FactCustomerPremise fcp
    ON fcp.CustomerKey = c.CustomerKey
    INNER JOIN dbo.DimPremise do
    ON do.PremiseKey = fcp.PremiseKey
    WHERE c.ClientId=@client_id AND c.EmailAddress != (c.FirstName+'.'+c.LastName+'@somefakeemail.com'	)


    UPDATE dbo.DimPremise
    SET Street1=dbo.fnObfuscatestreet(dp.Street1)
	,Street2=dbo.fnObfuscatestreet(dp.Street2)
	 FROM dbo.DimPremise dp
	  INNER JOIN	(
						SELECT PremiseKey, CustomerKey, 
						ROW_NUMBER() OVER(PARTITION BY premisekey ORDER BY datecreated DESC) AS rnk 
						FROM
						[InsightsDW].[dbo].[FactCustomerPremise] fcp
					) AS fcp 	
		   ON dp.premisekey=fcp.premisekey AND rnk=1
    INNER JOIN 	  dbo.DimCustomer c
    ON fcp.CustomerKey = c.CustomerKey
    WHERE c.ClientId=@client_id AND c.EmailAddress != (c.FirstName+'.'+c.LastName+'@somefakeemail.com'	)
 END
GO
