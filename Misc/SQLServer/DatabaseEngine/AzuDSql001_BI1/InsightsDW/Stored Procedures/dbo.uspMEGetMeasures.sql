SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- GetMeasures
CREATE procedure [dbo].[uspMEGetMeasures] ( 
	@ClientId Int, 
	@CustomerId varchar(50), 
	@AccountId varchar(50) = NULL, 
	@PremiseId varchar(50) = NULL
	) AS
BEGIN		
	SET NOCOUNT ON;
			
	SELECT [ClientId]
      ,[CustomerId]
      ,[AccountId]
      ,[PremiseId]
      ,[ActionKey]
      ,[ActionTypeKey]
      ,[AnnualCost]
      ,[AnnualCostVariancePercent]
      ,[AnnualSavingsEstimate]
      ,[AnnualSavingsEstimateCurrencyKey]
      ,[Roi]
      ,[Payback]
      ,[UpfrontCost]
      ,[CommodityKey]
      ,[ElecSavEst]
      ,[ElecSavEstCurrencyKey]
      ,[ElecUsgSavEst]
      ,[ElecUsgSavEstUomKey]
      ,[GasSavEst]
      ,[GasSavEstCurrencyKey]
      ,[GasUsgSavEst]
      ,[GasUsgSavEstUomKey]
      ,[WaterSavEst]
      ,[WaterSavEstCurrencyKey]
      ,[WaterUsgSavEst]
      ,[WaterUsgSavEstUomKey]
      ,[Priority]
      ,[NewDate]
  FROM [InsightsDW].dbo.[FactMeasure]
  WHERE ClientId = @ClientID  
		  AND CustomerId = @CustomerID 
		  AND (AccountId = @AccountID OR @AccountID IS NULL) 
  		  AND (PremiseId = @PremiseID OR @PremiseID IS NULL)         
	ORDER BY CustomerId, AccountId, PremiseId, ActionKey, CommodityKey

END

GO
