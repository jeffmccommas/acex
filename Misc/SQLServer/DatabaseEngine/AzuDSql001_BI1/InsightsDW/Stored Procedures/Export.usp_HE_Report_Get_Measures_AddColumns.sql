SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--
-- =============================================
-- Author:	 Sunil
-- Create date: unknown
-- Description: Produces a csv extract referred to Home Energy Report
--
-- =============================================
-- Changed by:	 Wayne
-- Change date: 12.18.2014
-- Description: v 1.32.0
--	
-- renamed the [dbo].[FactActionItem] table to [export].[HE_Report_FactActionItem]
-- used in proc [Export].[usp_HE_Report_Get_Measures]
-- and [Export].[usp_HE_Report_Get_Measures_AddColumns]
-- =============================================
-- Changed by:	Wayne
-- Change date: 07.14.2015
-- Description:	replaced the HE_Report_FactActionItem table
--				with a new view vHE_Report_FactActionItem
--	
--  The view (its underlying table) will be populated by new dll
--  being created by the api team (Mike G)
-- =============================================
CREATE procedure [Export].[usp_HE_Report_Get_Measures_AddColumns]
@profile_name varchar (50) = 'Sunil_Profile256',
@criteria_id varchar (20),
@criteria_detail varchar (20),
@orderbyClause varchar (2000) OUTPUT,
@validClause varchar (8000) OUTPUT,
@columnList varchar (8000) OUTPUT,
@columnListh2 varchar (8000) OUTPUT
as
begin

/*
[001]	sunil kadimdiwan, 2014-06-21
Add columns and compute based on criteria selected for recommending Measures.

 */

set nocount on

	declare @sqlcmd varchar (8000)

	declare @enduse varchar (50)

	declare
		@clientID int, 
		@fuel_type varchar (100), 
		@energyobjectcategory_List varchar (1024) , 
		@period varchar (10) , 
		@season_List varchar (100) , 
		@enduse_List varchar (1024) , 
		@ordered_enduse_List varchar (1024), 
		@criteria_id1 varchar (50) , 
		@criteria_detail1 varchar (255)  , 
		@criteria_id2 varchar (50) , 
		@criteria_detail2 varchar (255) , 
		@criteria_id3 varchar (50) , 
		@criteria_detail3 varchar (255) , 
		@criteria_id4 varchar (50) , 
		@criteria_detail4 varchar (255) , 
		@criteria_id5 varchar (50) , 
		@criteria_detail5 varchar (255) , 
		@criteria_id6 varchar (50) , 
		@criteria_detail6 varchar (255), 
		@saveProfile char (1), 
		@verifySave char (1),
		@process_id char (1)

	SELECT
		@clientID = clientID
		, @fuel_type = fuel_type
		, @energyobjectcategory_List = energyobjectcategory_List
		, @period = period
		, @season_List = season_List
		, @enduse_List = enduse_List
		, @ordered_enduse_List = ordered_enduse_List
		, @criteria_id1 = criteria_id1
		, @criteria_detail1 = criteria_detail1
		, @criteria_id2 = criteria_id2
		, @criteria_detail2 = criteria_detail2
		, @criteria_id3 = criteria_id3
		, @criteria_detail3 = criteria_detail3
		, @criteria_id4 = criteria_id4
		, @criteria_detail4 = criteria_detail4
		, @criteria_id5 = criteria_id5
		, @criteria_detail5 = criteria_detail5
		, @criteria_id6 = criteria_id6
		, @criteria_detail6 = criteria_detail6
	  FROM [export].[home_energy_report_criteria_profile]
	where [profile_name] = @profile_name

	declare @num_energyObjectCategory int, @num_energyObjectCategory2 int

	if object_id ('tempdb..#energyObjectCategory_list') is not null drop table #energyObjectCategory_list
	create table #energyObjectCategory_list ([energyObjectCategory] varchar (50), TotalDollars decimal (18, 2))
	insert #energyObjectCategory_list ([energyObjectCategory])
	select cast (param as varchar (50))
	from [dbo].[ufn_split_values] (@energyObjectCategory_list, ',')
	select	@num_energyObjectCategory = count (*) from #energyObjectCategory_list

	if @criteria_id = 'SAVINGS'
	begin
		select @sqlcmd = '
		alter table export.HE_Report_actionitems add DollarSavings' + @fuel_type + ' varchar (50) NULL, DollarSavings' + @fuel_type + '_flag int NULL'
		exec (@sqlcmd)

		select @sqlcmd = '
		alter table export.HE_Report_actionitems_ranked add DollarSavings' + @fuel_type + ' varchar (50) NULL, DollarSavings' + @fuel_type + '_flag int NULL'
		exec (@sqlcmd)

		select @columnListh2 = @columnListh2 + ', h2.DollarSavings' + @fuel_type + ', h2.DollarSavings' + @fuel_type + '_flag'
		select @columnList = @columnList + ', DollarSavings' + @fuel_type + ', DollarSavings' + @fuel_type + '_flag'

		if len (@orderbyClause) = 0 
			select	@orderbyClause = 'DollarSavings' + @fuel_type + '  DESC '
		else
			select	@orderbyClause = @orderbyClause + ', ' + 'DollarSavings' + @fuel_type + '  DESC '

		if len (@validClause) = 0 
			select	@validClause = 'case when DollarSavings' + @fuel_type + '_flag = 1'
		else
			select	@validClause = @validClause + ' AND ' + 'DollarSavings' + @fuel_type + '_flag = 1'

	end
	else
	if @criteria_id = 'INVESTMENT'
	begin
		select @sqlcmd = '
		alter table export.HE_Report_actionitems add Cost' + ' varchar (50) NULL, Cost_flag int NULL'
		exec (@sqlcmd)
		select @sqlcmd = '
		alter table export.HE_Report_actionitems_ranked add Cost varchar (50) NULL, Cost_flag int NULL'
		exec (@sqlcmd)

		select @columnListh2 = @columnListh2 + ', h2.Cost, h2.Cost_flag'
		select @columnList = @columnList + ', Cost, Cost_flag'

		if len (@orderbyClause) = 0 
			select	@orderbyClause = 'Cost DESC '
		else
			select	@orderbyClause = @orderbyClause + ', ' + 'Cost DESC '

		if len (@validClause) = 0 
			select	@validClause = 'case when Cost_flag = 1'
		else
			select	@validClause = @validClause + ' AND ' + 'Cost_flag = 1'

	end
	else
	if @criteria_id = 'ROI'
	begin
		select @sqlcmd = '
		alter table export.HE_Report_actionitems add ROI' + ' varchar (50) NULL, ROI_flag int NULL'
		exec (@sqlcmd)
		select @sqlcmd = '
		alter table export.HE_Report_actionitems_ranked add ROI varchar (50) NULL, ROI_flag int NULL'
		exec (@sqlcmd)

		select @columnListh2 = @columnListh2 + ', h2.ROI, h2.ROI_flag'
		select @columnList = @columnList + ', ROI, ROI_flag'

		if len (@orderbyClause) = 0 
			select	@orderbyClause = 'ROI DESC '
		else
			select	@orderbyClause = @orderbyClause + ', ' + 'ROI DESC '

		if len (@validClause) = 0 
			select	@validClause = 'case when ROI_flag = 1'
		else
			select	@validClause = @validClause + ' AND ' + 'ROI_flag = 1'

	end
	else
	if @criteria_id = 'CO2Savings'
	begin
		select @sqlcmd = '
		alter table export.HE_Report_actionitems add CO2Savings' + ' varchar (50) NULL, CO2Savings_flag int NULL'
		exec (@sqlcmd)
		select @sqlcmd = '
		alter table export.HE_Report_actionitems_ranked add CO2Savings varchar (50) NULL, CO2Savings_flag int NULL'
		exec (@sqlcmd)

		select @columnListh2 = @columnListh2 + ', CO2Savings, CO2Savings_flag'
		select @columnList = @columnList + ', h2.CO2Savings, h2.CO2Savings_flag'

		if len (@orderbyClause) = 0 
			select	@orderbyClause = 'CO2Savings DESC '
		else
			select	@orderbyClause = @orderbyClause + ', ' + 'CO2Savings DESC '

		if len (@validClause) = 0 
			select	@validClause = 'case when CO2Savings_flag = 1'
		else
			select	@validClause = @validClause + ' AND ' + 'CO2Savings_flag = 1'

	end
	
	if object_id ('tempdb..#ordered_enduse_list') is not null drop table #ordered_enduse_list
	create table #ordered_enduse_list ([ordered_enduse] varchar (50))
	insert #ordered_enduse_list ([ordered_enduse])
	select cast (param as varchar (50))
	from [dbo].[ufn_split_values] (@ordered_enduse_list, ',')



	declare	enduse_cursor cursor for
	select	el.ordered_enduse
	from	#ordered_enduse_list el
	open	enduse_cursor

	fetch next from enduse_cursor into @enduse

	while @@FETCH_STATUS = 0
	begin


		select	@sqlcmd = '
		update	hera
		set		RefSavings = coalesce (f1.' + 'DollarSavings' + @fuel_type + ', 0)
		from	[export].[vHE_Report_FactActionItem] f1
				inner join DimEnergyObject eo on eo.EnergyObjectKey = f1.EnergyObjectKey
				inner join [InsightsMetadata].dbo.vDimActionItem ai on ai.ActionItemKey = f1.ActionItemKey
				inner join export.HE_Report_actionitems hera on hera.PremiseKey = f1.PremiseKey and hera.ActionItemKey = f1.ActionItemKey
		where	eo.EnergyObjectDesc = ''' + @enduse + ''''

		--select @sqlcmd
		exec (@sqlcmd)

		select	@sqlcmd = '
		update	hera
		set		RefSavings = 0
		from	export.HE_Report_actionitems hera
				inner join DimEnergyObject eo on hera.EnergyObjectDesc = eo.EnergyObjectDesc
				inner join [InsightsMetadata].dbo.vDimActionItem ai on ai.ActionItemKey = hera.ActionItemKey
		where	hera.RefSavings is NULL and eo.EnergyObjectDesc = ''' + @enduse + ''''

		--select @sqlcmd
		exec (@sqlcmd)


		select	@sqlcmd = '
		update	hera
		set		RefCost = coalesce (f1.Cost, 0)
		from	[export].[vHE_Report_FactActionItem] f1
				inner join DimEnergyObject eo on eo.EnergyObjectKey = f1.EnergyObjectKey
				inner join [InsightsMetadata].dbo.vDimActionItem ai on ai.ActionItemKey = f1.ActionItemKey
				inner join export.HE_Report_actionitems hera on hera.PremiseKey = f1.PremiseKey and hera.ActionItemKey = f1.ActionItemKey
		where	eo.EnergyObjectDesc = ''' + @enduse + ''''

		--select @sqlcmd
		exec (@sqlcmd)

		select	@sqlcmd = '
		update	hera
		set		RefCost = 0
		from	export.HE_Report_actionitems hera
				inner join DimEnergyObject eo on hera.EnergyObjectDesc = eo.EnergyObjectDesc
				inner join [InsightsMetadata].dbo.vDimActionItem ai on ai.ActionItemKey = hera.ActionItemKey
		where	hera.RefCost is NULL and eo.EnergyObjectDesc = ''' + @enduse + ''''

		--select @sqlcmd
		exec (@sqlcmd)

		if @criteria_id = 'SAVINGS'
		begin
			select	@sqlcmd = '
			update	hera
			set		DollarSavings' + @fuel_type + ' = coalesce (f1.' + 'DollarSavings' + @fuel_type + ', 0)
			from	[export].[vHE_Report_FactActionItem] f1
					inner join DimEnergyObject eo on eo.EnergyObjectKey = f1.EnergyObjectKey
					inner join [InsightsMetadata].dbo.vDimActionItem ai on ai.ActionItemKey = f1.ActionItemKey
					inner join export.HE_Report_actionitems hera on hera.PremiseKey = f1.PremiseKey and hera.ActionItemKey = f1.ActionItemKey
			where	eo.EnergyObjectDesc = ''' + @enduse + ''''

			--select @sqlcmd
			exec (@sqlcmd)

			select	@sqlcmd = '
			update	hera
			set		DollarSavings' + @fuel_type + ' = 0
			from	export.HE_Report_actionitems hera
					inner join DimEnergyObject eo on hera.EnergyObjectDesc = eo.EnergyObjectDesc
					inner join [InsightsMetadata].dbo.vDimActionItem ai on ai.ActionItemKey = hera.ActionItemKey
			where	hera.DollarSavings' + @fuel_type + ' is NULL and eo.EnergyObjectDesc = ''' + @enduse + ''''

			--select @sqlcmd
			exec (@sqlcmd)

			select	@sqlcmd = '
			update	hera
			set		DollarSavings' + @fuel_type + '_flag = case when convert (numeric, coalesce (f1.' + 'DollarSavings' + @fuel_type + ', 0)) ' + @criteria_detail + ' then 1 else 0 end ' + ' 
			from	[export].[vHE_Report_FactActionItem] f1
					inner join DimEnergyObject eo on eo.EnergyObjectKey = f1.EnergyObjectKey
					inner join [InsightsMetadata].dbo.vDimActionItem ai on ai.ActionItemKey = f1.ActionItemKey
					inner join export.HE_Report_actionitems hera on hera.PremiseKey = f1.PremiseKey and hera.ActionItemKey = f1.ActionItemKey
			where	eo.EnergyObjectDesc = ''' + @enduse + ''''

--			select @sqlcmd
			exec (@sqlcmd)

		end
		else
		if @criteria_id = 'INVESTMENT'
		begin
			select	@sqlcmd = '
			update	hera
			set		Cost' + ' = f1.' + 'Cost
			from	[export].[vHE_Report_FactActionItem] f1
					inner join DimEnergyObject eo on eo.EnergyObjectKey = f1.EnergyObjectKey
					inner join [InsightsMetadata].dbo.vDimActionItem ai on ai.ActionItemKey = f1.ActionItemKey
					inner join export.HE_Report_actionitems hera on hera.PremiseKey = f1.PremiseKey and hera.ActionItemKey = f1.ActionItemKey
			where	eo.EnergyObjectDesc = ''' + @enduse + ''''

			--select @sqlcmd
			exec (@sqlcmd)

			select	@sqlcmd = '
			update	hera
			set		Cost_flag = case when convert (numeric, coalesce (f1.' + 'Cost, 0)) ' + @criteria_detail + ' then 1 else 0 end ' + ' 
			from	[export].[vHE_Report_FactActionItem] f1
					inner join DimEnergyObject eo on eo.EnergyObjectKey = f1.EnergyObjectKey
					inner join [InsightsMetadata].dbo.vDimActionItem ai on ai.ActionItemKey = f1.ActionItemKey
					inner join export.HE_Report_actionitems hera on hera.PremiseKey = f1.PremiseKey and hera.ActionItemKey = f1.ActionItemKey
			where	eo.EnergyObjectDesc = ''' + @enduse + ''''

--			select @sqlcmd
			exec (@sqlcmd)

		end
		else
		if @criteria_id = 'ROI'
		begin
			select	@sqlcmd = '
			update	hera
			set		ROI' + ' = f1.' + 'ROI
			from	[export].[vHE_Report_FactActionItem] f1
					inner join DimEnergyObject eo on eo.EnergyObjectKey = f1.EnergyObjectKey
					inner join [InsightsMetadata].dbo.vDimActionItem ai on ai.ActionItemKey = f1.ActionItemKey
					inner join export.HE_Report_actionitems hera on hera.PremiseKey = f1.PremiseKey and hera.ActionItemKey = f1.ActionItemKey
			where	eo.EnergyObjectDesc = ''' + @enduse + ''''

			select	@sqlcmd = '
			update	hera
			set		ROI_flag = case when convert (numeric, coalesce (f1.' + 'ROI, 0)) ' + @criteria_detail + ' then 1 else 0 end ' + ' 
			from	[export].[vHE_Report_FactActionItem] f1
					inner join DimEnergyObject eo on eo.EnergyObjectKey = f1.EnergyObjectKey
					inner join [InsightsMetadata].dbo.vDimActionItem ai on ai.ActionItemKey = f1.ActionItemKey
					inner join export.HE_Report_actionitems hera on hera.PremiseKey = f1.PremiseKey and hera.ActionItemKey = f1.ActionItemKey
			where	eo.EnergyObjectDesc = ''' + @enduse + ''''

--			select @sqlcmd
			exec (@sqlcmd)
		end
		else
		if @criteria_id = 'CO2Savings'
		begin
			select	@sqlcmd = '
			update	hera
			set		CO2Savings' + ' = f1.' + 'CO2Savings
			from	[export].[vHE_Report_FactActionItem] f1
					inner join DimEnergyObject eo on eo.EnergyObjectKey = f1.EnergyObjectKey
					inner join [InsightsMetadata].dbo.vDimActionItem ai on ai.ActionItemKey = f1.ActionItemKey
					inner join export.HE_Report_actionitems hera on hera.PremiseKey = f1.PremiseKey and hera.ActionItemKey = f1.ActionItemKey
			where	eo.EnergyObjectDesc = ''' + @enduse + ''''

			select	@sqlcmd = '
			update	hera
			set		CO2Savings_flag = case when convert (numeric, coalesce (f1.' + 'CO2Savings, 0)) ' + @criteria_detail + ' then 1 else 0 end ' + ' 
			from	[export].[vHE_Report_FactActionItem] f1
					inner join DimEnergyObject eo on eo.EnergyObjectKey = f1.EnergyObjectKey
					inner join [InsightsMetadata].dbo.vDimActionItem ai on ai.ActionItemKey = f1.ActionItemKey
					inner join export.HE_Report_actionitems hera on hera.PremiseKey = f1.PremiseKey and hera.ActionItemKey = f1.ActionItemKey
			where	eo.EnergyObjectDesc = ''' + @enduse + ''''

--			select @sqlcmd
			exec (@sqlcmd)
		end

		fetch next from enduse_cursor into @enduse
	end
	close enduse_cursor
	deallocate enduse_cursor

set nocount off
end
GO
