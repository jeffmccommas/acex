SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 9/19/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_INF_FactPremiseAttribute_DimPremise]
				 @ETL_LogId INT,
				 @ClientID INT

AS

BEGIN

		SET NOCOUNT ON;

		INSERT INTO [InsightsDW].[dbo].[DimPremise] (PostalCodeKey, 
													CityKey, 
													SourceKey, 
													PremiseId, 
													AccountId,
													Street1, 
													Street2, 
													City, 
													StateProvince, 
													Country, 
													PostalCode, 
													GasService, 
													ElectricService, 
													WaterService, 
													CreateDate,
													ClientId, 
													GeoLocation,
													SourceId,
													ETL_LogId,
													IsInferred,
													TrackingId,
													TrackingDate)
		SELECT  DISTINCT -1,
				-1,
				SourceKey,
				PremiseId,
				AccountId,
				NULL,  
				NULL, 
				NULL, 
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				GETUTCDATE(),
				ClientId,
				NULL,
				SourceId,
				@ETL_LogId,
				1,
				GETUTCDATE(),
				GETUTCDATE()
			FROM
			(SELECT  kfpa.ClientId,
						dcl.ClientKey,
						kfpa.PremiseID,
						kfpa.AccountID,
						ds.SourceKey,
						kfpa.SourceID,
						ROW_NUMBER() OVER ( PARTITION BY 
						 kfpa.ClientId,  kfpa.PremiseID, kfpa.AccountID
						ORDER BY  kfpa.SourceID) AS SortId 
			FROM ETL.KEY_FactPremiseAttribute kfpa WITH (NOLOCK)
			INNER JOIN dbo.DimClient dcl WITH (NOLOCK) ON dcl.ClientId = kfpa.ClientId	
			INNER JOIN dbo.DimSource ds WITH (NOLOCK) ON ds.SourceId = kfpa.SourceId
			LEFT JOIN dbo.DimPremise dp WITH (NOLOCK) ON dp.PremiseId = kfpa.PremiseId
											AND dp.ClientId = kfpa.ClientId
											AND dp.AccountId = kfpa.AccountId
			WHERE kfpa.ClientID = @ClientID AND dp.PremiseId IS NULL) p
			WHERE SortId = 1


END

														 



GO
