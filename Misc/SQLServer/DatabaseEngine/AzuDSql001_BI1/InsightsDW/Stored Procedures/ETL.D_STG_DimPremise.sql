SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/16/2014
-- Description:	
-- =============================================
create PROCEDURE [ETL].[D_STG_DimPremise]
				 @ClientID INT
AS

BEGIN

	SET NOCOUNT ON;

	DELETE FROM ETL.INS_DimPremise WHERE ClientID = @ClientID
	DELETE FROM ETL.T1U_DimPremise WHERE ClientID = @ClientID

END





GO
