SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
-- =======================================
-- Written by:	Wayne
-- Date:		07.20.15
-- Description: Create the index on the  temp table for Measure Savings
-- 				caclulations
-- =======================================
*/
CREATE PROCEDURE [dbo].[IndexMeasureSavingsTempTable]
(
    @ClientID       INT = 101
)
as
BEGIN
DECLARE @sqlstring VARCHAR(1000) ='
CREATE NONCLUSTERED INDEX [MeasureSavingsTemp_ClientID_Cust_Account_Prem] ON dbo.MeasureSavingsTemp' + CAST(@ClientID AS varchar(5)) 
+ '(
    [ClientID] ASC,
 [CustomerID] ASC,
 [AccountID] ASC,
 [PremiseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)'
EXECUTE (@sqlstring)
END
GO
