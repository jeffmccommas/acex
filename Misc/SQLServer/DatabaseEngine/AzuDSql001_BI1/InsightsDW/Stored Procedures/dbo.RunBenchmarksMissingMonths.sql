SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Wayne
-- Create date:	9.23.2014
-- Description:	runs benchmarks for all missing months in InsightsDW.[dbo].[FactServicePointBilling]
--				not in InsightsDW.dbo.Benchmarks
-- =============================================
-- change by:		Wayne
-- change date:	1.15.15
-- Description:	set the min group size to 100
-- =============================================
-- change by:		Wayne
-- change date:	1.23.15
-- Description:	Made change in determining how many months to
--				Calculate benchmarks for (used new var @mycount)
-- =============================================
-- change by:		Wayne
-- change date:	3.4.15
-- Description:	Made change to always fix bill dates and zip codes
-- =============================================
CREATE PROC [dbo].[RunBenchmarksMissingMonths] @ClientIdParam INT
AS
    BEGIN
        BEGIN TRY
            IF @ClientIdParam IS NULL
                BEGIN
                    RAISERROR('client id must be provided', 11, 1);
                END;
            IF @ClientIdParam < 1
                BEGIN
                    RAISERROR('client id must be  greater than 0', 11, 2);
                END;
            IF @ClientIdParam > 256
                BEGIN
                    RAISERROR('client id must be less than 257', 11, 3);
                END;
            IF NOT EXISTS ( SELECT  *
                            FROM    InsightsDW.[dbo].DimClient
                            WHERE   ClientId = @ClientIdParam )
                BEGIN
                    RAISERROR('Invalid Client ID', 11, 4);
                END;
            SET NOCOUNT ON;
	-- create a list of distinct months that are not already in 
	-- InsightsDW.dbo.Benchmarks to run
	-- the proc for
            SELECT DISTINCT
                    d.Month AS DistinctMonth
            INTO    #temp
            FROM    InsightsDW.[dbo].[FactServicePointBilling] fb
                    INNER JOIN InsightsDW.[dbo].DimDate d ON fb.BillPeriodEndDateKey = d.DateKey
            WHERE   ClientId = @ClientIdParam
                    AND SUBSTRING(PremiseId, 1, 4) != 'seed' --exclude seeds, they are not final yet
            EXCEPT
            SELECT DISTINCT
                    BillMonth
            FROM    InsightsDW.dbo.Benchmarks
            WHERE   ClientId = @ClientIdParam
            ORDER BY d.Month ASC;

            DECLARE @mycount INT;
            SELECT  @mycount = COUNT(*)
            FROM    #temp;
            IF @mycount = 0
                BEGIN
                    PRINT 'no months to process';
                    RETURN;
                END;
            ELSE
                BEGIN
                    PRINT CAST(@mycount AS VARCHAR(3)) + '  months to process';
                END;

	-- always fix bill dates
            EXEC [dbo].[FixBillDatesForBecnhmarks] @ClientId = @ClientIdParam;
     
	-- Always fix zip codes
            EXEC [dbo].[FixZipCodesForBenchmarks] @clientId = @ClientIdParam;

	-- var to hold a month as input to proc execution
            DECLARE @month DATETIME;

	-- var to hold a month as input to proc execution
            DECLARE @lastmonth DATETIME = '2012-01-01';	

	-- var to know when i just processed last month in list
            DECLARE @maxmonth DATETIME;

	-- populate the max
            SELECT TOP 1
                    @maxmonth = DistinctMonth
            FROM    #temp
            ORDER BY DistinctMonth DESC;
	--PRINT 'max month is ' + CAST(CAST(@maxmonth AS date) AS VARCHAR(10))

	-- var to hold return
            DECLARE @return_value INT;

	--loop
            WHILE ( 1 = 1 )
                BEGIN
                    SELECT TOP 1
                            @month = DistinctMonth
                    FROM    dbo.#temp
                    WHERE   DistinctMonth > @lastmonth -- getting next higher month
                    ORDER BY DistinctMonth ASC;

                    IF @month IS NOT NULL
                        BEGIN     
			-- calling the proc
                            EXEC @return_value = [dbo].[CreateBenchmarks] @ClientID = @ClientIdParam,
                                @BillMonth = @month,
                                @BenchmarkGroupTypeKey = 1,
                                @MinGroupSize = 100;		
                            PRINT 'Month '
                                + CAST(CAST(@month AS DATE) AS VARCHAR(10))
                                + ' has been processed';        
                        END;

		--keep track of where we are in list
                    SET @lastmonth = @month;  
		  
		-- BREAK out of the loop once the max id has been reached
                    IF @month = @maxmonth
                        BREAK; 
                END;
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(4000);
            DECLARE @ErrorSeverity INT;
            DECLARE @ErrorState INT;
		  --populate vars
            SELECT  @ErrorMessage = ERROR_MESSAGE() ,
                    @ErrorSeverity = ERROR_SEVERITY() ,
                    @ErrorState = ERROR_STATE();
		  -- rethrow goes to front end c# or another proc
            RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
            RETURN -1;
        END CATCH;
    END;


GO
