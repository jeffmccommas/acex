SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:	 Wayne
-- Create date: 12.2.14
-- Description: find premises with most ami data coverage in month
-- =============================================
CREATE PROCEDURE [dbo].[FindTopAmiPremises]
      @client_id INT
    , @bill_year	    VARCHAR(4)		= '2014'		 -- >= 2014
    , @bill_month	    VARCHAR(2)		= '10'		 -- options are 1-12    
    , @howmany		    INT			=  10
    , @BottomTop	    VARCHAR(6)		= 'Top'	      -- 'Bottom' or 'Top'
    , @hasbenchmarks    INT			= 0			 -- 1=must, 0=dont care

AS
    IF ISNUMERIC(@bill_year) = 0
        BEGIN
            RAISERROR(N'@bill_year parameter must be numeric' , -1 , 1);
        END;
    IF @bill_year < 2014
        BEGIN
            RAISERROR(N'@bill_year parameter must be > 2013' , -2 , 1);
        END;
    IF ISNUMERIC(@bill_month) = 0
        BEGIN
            RAISERROR(N'@bill_month parameter must be numeric' , -3 , 1);
        END;
    IF ISNUMERIC(@bill_month) > 12
        BEGIN
            RAISERROR(N'@bill_month parameter is invalid 1-12 are acceptable' , -4 , 1);
        END;
    --    
    DECLARE  @endrangedate date= DATEADD(DAY,-1,DATEADD(MONTH,1,(CAST(@bill_year+@bill_month+'01' AS DATE))))  
    DECLARE  @startrangedate date= CAST(@bill_year+@bill_month+'01' AS DATE)

    DECLARE @startrangekey INT =cast(convert(char(8), @startrangedate, 112) as int) 
    DECLARE @endrangekey INT =cast(convert(char(8), @endrangedate, 112) as int) 

    --
	DECLARE @sql VARCHAR(500);
	SELECT @sql = CASE WHEN @hasbenchmarks=1
	THEN   'SELECT TOP (' + CAST(@howmany AS VARCHAR(6)) +') fad.premiseid, count(*) AS days_of_AMI_data FROM [Insightsdw].[dbo].FactAmiDetail fad
		  INNER JOIN [Insightsdw].[dbo].DimPremise dp
		  ON dp.PremiseId = fad.PremiseId AND dp.AccountId = fad.AccountId 
		  INNER JOIN [Insightsdw].[dbo].Benchmarks b
		  ON b.PremiseKey=dp.PremiseKey
		  WHERE fad.DateKey BETWEEN ' + CAST(@startrangekey AS VARCHAR(8)) + ' AND ' + CAST(@endrangekey AS VARCHAR(8)) + '
		  AND fad.ClientId=' + CAST(@client_id AS VARCHAR(6)) + '
		  group by fad.premiseid, fad.AccountId
		  ORDER BY COUNT(*) ' 
    ELSE
		  'SELECT TOP (' + CAST(@howmany AS VARCHAR(6)) +') premiseid, count(*) AS days_of_AMI_data FROM [Insightsdw].[dbo].FactAmiDetail fad
		  WHERE fad.DateKey BETWEEN ' + CAST(@startrangekey AS VARCHAR(8)) + ' AND ' + CAST(@endrangekey AS VARCHAR(8)) + '
		  AND fad.ClientId=' + CAST(@client_id AS VARCHAR(6)) + '
		  group by premiseid, fad.AccountId
		  ORDER BY COUNT(*) '
    END 
    SELECT @sql =@sql + CASE WHEN @BottomTop='Top' THEN 'desc' ELSE 'asc' end
    EXEC (@sql);
GO
