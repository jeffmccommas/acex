SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ubaid Tariq
-- Create date: 12/16/2014
-- Description:	Delete Keys before processing new actions
-- =============================================
CREATE PROCEDURE [ETL].[D_KEY_FactAction] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        DELETE  FROM ETL.KEY_FactAction
        WHERE   ClientID = @ClientID

    END

GO
