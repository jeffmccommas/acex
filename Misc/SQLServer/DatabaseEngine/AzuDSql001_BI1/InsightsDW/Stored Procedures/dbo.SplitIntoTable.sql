SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[SplitIntoTable]
    @ValueList AS VARCHAR(MAX),
	@Separator AS CHAR(1) = ','
AS 
    BEGIN
        SET NOCOUNT ON;

        DECLARE @table TABLE ( Value VARCHAR(50) );
        DECLARE @pos INT
        DECLARE @len INT
        DECLARE @value VARCHAR(8000)

        SET @pos = 0
        SET @len = 0

        IF LEN(@ValueList) > 0 
            BEGIN

                WHILE CHARINDEX(@Separator, @valueList, @pos + 1) > 0 
                    BEGIN
                        SET @len = CHARINDEX(@Separator, @valueList, @pos + 1) - @pos
                        SET @value = SUBSTRING(@valueList, @pos, @len)
        
                        INSERT  INTO @table
                        VALUES  ( RTRIM(LTRIM(@value)) ) 

                        SET @pos = CHARINDEX(@Separator, @valueList, @pos + @len) + 1
                    END
            
                SET @value = SUBSTRING(@ValueList, @pos, 50)          
  
                INSERT  INTO @table
                VALUES  ( RTRIM(LTRIM(@value)) ) 

            END	
        SELECT  *
        FROM    @table

    END

GO
