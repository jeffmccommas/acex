-- exec rpt.GetProgramNetReportDeliveredDetail 224, '11/1/2015', '11/1/2015', null, null, 'FullUserSet', 'her2015program','T'

-- drop procedure rpt.GetProgramNetReportDeliveredDetail

create procedure rpt.GetProgramNetReportDeliveredDetail  @clientID int, @startDateBaseline date, @endDateBaseline date, @startDateCurrent date, @endDateCurrent date, @reportView varchar(50),@program varchar(200), @treatmentGroupPrefix varchar(5)
as

set nocount on

create table #Events
(clientID int not null,
customerKey int not null,
premisekey int not null,
customerID varchar(50),
accountID varchar(50),
premiseID varchar(50) not null,
eventTypeID int not null,
flagDateCreated datetime,
flagDateUpdated datetime,
FullDateAlternateKey date not null,
monthName varchar(50),
eventMonth varchar(15),
groupName varchar(50),
groupSort int,
bInclude int,
segment varchar(50),
program varchar(200),
EventShortName varchar(50),
emailID varchar(25),
CustomerFirstName varchar(50),
CustomerLastName varchar(50),
CustomerStreet1 varchar(50),
CustomerStreet2 varchar(50),
CustomerCity varchar(50),
CustomerStateProvince varchar(5),
CustomerPostalCode varchar(10),
PremiseStreet1 varchar(50),
PremiseStreet2 varchar(50),
PremiseCity varchar(50),
PremiseStateProvince varchar(5),
PremisePostalCode varchar(10),
CustomerEmailAddress varchar(150),
CustomerPhone varchar(20),
--EventGroup varchar(20),
ReportGroup varchar(20))

-- get set of events including segments - call sp used to pull data for HER Metrics reporting
insert into #Events (clientID,customerKey,premisekey,customerID,accountID,premiseID,eventTypeID,flagDateCreated,flagDateUpdated,
	FullDateAlternateKey,monthName,eventMonth,groupName,groupSort,bInclude,segment,program,EventShortName,
	emailID,CustomerFirstName,CustomerLastName,CustomerStreet1,CustomerStreet2,CustomerCity,CustomerStateProvince,CustomerPostalCode,
	PremiseStreet1,PremiseStreet2,PremiseCity,PremiseStateProvince,PremisePostalCode,CustomerEmailAddress,CustomerPhone)
exec rpt.getImpactAnalysisMetrics @clientID, @startDateBaseline,@endDateBaseline, @startDateCurrent, @endDateCurrent, @reportView, @program

-- get rid of events that are not sent, sendfail or bounce eventtypes
delete 
from #Events
where eventtypeid not in (7,11,23,24,26,22,25,35,36)

-- set reportGroup so that we can identify the mailing for output and to get the netsent instead of reports generated
update #Events
set ReportGroup = case when len(emailID) = 0 then 'Print ' + eventmonth else 'Email ' + eventmonth end

-- get the net sends
;with SentReport
as
(
	select ClientID, CustomerID, AccountId, PremiseID,Segment, ReportGroup, emailID, eventMonth
	from #Events
	where eventtypeid = 7
),
sendFail
as
(
	select ClientID, CustomerID, AccountId, PremiseID,Segment, ReportGroup, emailID, eventMonth
	from #Events
	where eventtypeid in (35,36)
),
bounced
as
(
	select ClientID, CustomerID, AccountId, PremiseID,Segment, ReportGroup, emailID, eventMonth
	from #Events
	where eventtypeid in (11,23,24,26,22,25)
)
select distinct t1.ClientID, t1.CustomerID, t1.AccountId, t1.PremiseID, t1.Segment, t1.ReportGroup, emailID, eventMonth
into #netsent
from SentReport t1 where not exists (select * from sendFail t2
	where t1.ClientID = t2.ClientID
	and t1.CustomerID = t2.CustomerID
	and t1.AccountID = t2.AccountID
	and t1.PremiseID = t2.PremiseID
	and t1.Segment = t2.Segment
	and t1.ReportGroup = t2.reportGroup)
and not exists (select * from bounced t3
	where t1.ClientID = t3.ClientID
	and t1.CustomerID = t3.CustomerID
	and t1.AccountID = t3.AccountID
	and t1.PremiseID = t3.PremiseID
	and t1.Segment = t3.Segment
	and t1.ReportGroup = t3.reportGroup)

-- add in users who never had a report sent
;with userset
as
(
	select [PremiseKey] as [PremiseKey], 
		clientID, accountID, premiseID,
		[Value] as [GroupId],
		row_number() over (partition by PremiseKey order by EffectiveDate desc) as [rank]
	from [dbo].[FactPremiseAttribute]
	where ContentAttributeKey like @program + '%groupnumber'
	and Value like @treatmentGroupPrefix + '%'
)
insert into #netsent
select t1.ClientID, t3.CustomerID, t1.AccountID, t1.PremiseID, t1.groupID,'UserHasNoReport','','' 
from userset t1 join factcustomerpremise t2
	on t1.premisekey = t2.premisekey
join dimcustomer t3 
	on t2.customerkey = t3.customerkey
where t1.rank = 1
--and t1.groupID like 'T%'
and not exists (select premisekey
	from factevent t4
	where t1.premisekey = t4.premisekey
	and t4.eventtypeid = 7)


-- pivot netsent data 
DECLARE @Columns AS VARCHAR(MAX);
DECLARE @Script AS VARCHAR(MAX);

SELECT  @Columns = COALESCE(@Columns + ', ', '')
               + QUOTENAME(ReportGroup)
        FROM    ( 
					SELECT DISTINCT ReportGroup
					FROM #netsent				  
                ) 
		AS rptGrp 
		ORDER BY ReportGroup

SET @Script = 
	'SELECT * 
	FROM  
	( 
		SELECT ClientID, CustomerID, AccountID, PremiseID,Segment, EmailID,ReportGroup
		FROM #netsent
	) a 
	PIVOT
	( 
		count(emailID)
		FOR ReportGroup IN ( ' + @Columns + ' ) 
     ) 
PivotTable
order by Segment, CustomerID, AccountID, PremiseID;'
	
EXEC(@Script);

drop table #netSent
drop table #Events

set nocount on
go

/*
-- modification to sp to support new requirement 4/22/2016 - basically add ReportType column, update reporttype when updating reportgroup, include reporttype in remaining queries, comment out 
--		including users who didn't get a report sent and comment out flattening out the file.  Just query the required columns from #netSent
create table #Events
(clientID int not null,
customerKey int not null,
premisekey int not null,
customerID varchar(50),
accountID varchar(50),
premiseID varchar(50) not null,
eventTypeID int not null,
flagDateCreated datetime,
flagDateUpdated datetime,
FullDateAlternateKey date not null,
monthName varchar(50),
eventMonth varchar(15),
groupName varchar(50),
groupSort int,
bInclude int,
segment varchar(50),
program varchar(200),
EventShortName varchar(50),
emailID varchar(25),
CustomerFirstName varchar(50),
CustomerLastName varchar(50),
CustomerStreet1 varchar(50),
CustomerStreet2 varchar(50),
CustomerCity varchar(50),
CustomerStateProvince varchar(5),
CustomerPostalCode varchar(10),
PremiseStreet1 varchar(50),
PremiseStreet2 varchar(50),
PremiseCity varchar(50),
PremiseStateProvince varchar(5),
PremisePostalCode varchar(10),
CustomerEmailAddress varchar(150),
CustomerPhone varchar(20),
--EventGroup varchar(20),
ReportType varchar(20),
ReportGroup varchar(20))

-- get set of events including segments - call sp used to pull data for HER Metrics reporting
insert into #Events (clientID,customerKey,premisekey,customerID,accountID,premiseID,eventTypeID,flagDateCreated,flagDateUpdated,
	FullDateAlternateKey,monthName,eventMonth,groupName,groupSort,bInclude,segment,program,EventShortName,
	emailID,CustomerFirstName,CustomerLastName,CustomerStreet1,CustomerStreet2,CustomerCity,CustomerStateProvince,CustomerPostalCode,
	PremiseStreet1,PremiseStreet2,PremiseCity,PremiseStateProvince,PremisePostalCode,CustomerEmailAddress,CustomerPhone)
exec rpt.getImpactAnalysisMetrics 224, '11/1/2015', '11/1/2015', null, null, 'FullUserSet', 'her2015program'

-- get rid of events that are not sent, sendfail or bounce eventtypes
delete 
from #Events
where eventtypeid not in (7,11,23,24,26,22,25,35,36)

-- set reportGroup so that we can identify the mailing for output and to get the netsent instead of reports generated
update #Events
set Reporttype = case when len(emailID) = 0 then 'Print' else 'Email' end,
ReportGroup = case when len(emailID) = 0 then 'Print ' + eventmonth else 'Email ' + eventmonth end


-- get the net sends
;with SentReport
as
(
	select ClientID, CustomerID, AccountId, PremiseID,Segment,Reporttype, ReportGroup, emailID, eventMonth
	from #Events
	where eventtypeid = 7
),
sendFail
as
(
	select ClientID, CustomerID, AccountId, PremiseID,Segment,Reporttype, ReportGroup, emailID, eventMonth
	from #Events
	where eventtypeid in (35,36)
),
bounced
as
(
	select ClientID, CustomerID, AccountId, PremiseID,Segment,Reporttype, ReportGroup, emailID, eventMonth
	from #Events
	where eventtypeid in (11,23,24,26,22,25)
)
select distinct t1.ClientID, t1.CustomerID, t1.AccountId, t1.PremiseID, t1.Segment, t1.Reporttype, t1.ReportGroup, emailID, eventMonth
into #netsent
from SentReport t1 where not exists (select * from sendFail t2
	where t1.ClientID = t2.ClientID
	and t1.CustomerID = t2.CustomerID
	and t1.AccountID = t2.AccountID
	and t1.PremiseID = t2.PremiseID
	and t1.Segment = t2.Segment
	and t1.ReportGroup = t2.reportGroup)
and not exists (select * from bounced t3
	where t1.ClientID = t3.ClientID
	and t1.CustomerID = t3.CustomerID
	and t1.AccountID = t3.AccountID
	and t1.PremiseID = t3.PremiseID
	and t1.Segment = t3.Segment
	and t1.ReportGroup = t3.reportGroup)

select clientID, CustomerID, AccountID, PremiseID, Segment, ReportType, ReportGroup as ReportDate from #netsent order by segment, customerid, eventmonth
*/






