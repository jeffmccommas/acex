SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
-- =============================================
-- Author:		Wayne
-- Create date:	6/26/2015
-- Description:	Delete the  disagg values in the Holding Queue table
--				
-- ==================================================================
*/
Create PROC [dbo].[DeleteDisAggHoldingReCalcsAfterMeasures]
@clientid INT
AS
BEGIN
 DECLARE @sqlstring VARCHAR(1000) ='
DELETE InsightsBulk.Holding.DisAggReCalcQueue 
 FROM 
 InsightsBulk.Holding.DisAggReCalcQueue q
  INNER JOIN (SELECT DISTINCT clientid, PremiseID, CustomerID, AccountID FROM MeasureSavingsTemp' + CAST(@clientid AS varchar(5)) + ' ) a
    ON q.ClientID=a.clientid
    AND q.PremiseID=a.premiseid
    AND q.CustomerID=a.customerid
    AND q.AccountID=a.accountid'
    EXECUTE (@sqlstring)
 END
GO
