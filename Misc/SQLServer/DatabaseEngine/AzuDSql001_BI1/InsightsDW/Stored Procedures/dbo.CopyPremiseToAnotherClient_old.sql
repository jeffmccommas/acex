SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:	 Wayne
-- Create date: 12.2.14
-- Description: copys premise, billing and ami data from all tables for a source client
--			 to another(target) client id
-- =============================================
-- Changed By:	 Wayne
-- Change date: 12.2.14
-- Description: allow it to work even if no benchmarks
-- =============================================
-- Changed By:	 Wayne
-- Change date: 3.20.15
-- Description: allow it to work even if premise exist in target
--			 by generating a new id
-- =============================================
-- Changed By:	 Wayne
-- Change date: 3.22.15
-- Description: added all 3 actions tables
--			 to the copy
-- =============================================
-- Changed By:	 Wayne
-- Change date: 7.24.15
-- Description: added service contract
--			 to the copy
-- you must also pass in a source and destination commodity
	--CommodityKey	
	--1	electric
	--2	gas
	--3	water
	--4	sewer
	--5	garbage
	--this IN turn WITH be translated INTO a comodity type
--	1 will translate to 4	for electric 
--	2 will translate to 5	for gas 	
--	3 will translate to 6	for water
-- =============================================
create PROCEDURE [dbo].[CopyPremiseToAnotherClient_old]
     @premiseid varchar(50)
   , @source_client_id INT
   , @target_client_id INT
   , @generate_newid INT=0
   , @verbose_flag int =0
   , @source_commodity INT
   , @target_commodity int
AS

SET NOCOUNT ON;
-- Declare error Vars
DECLARE
       @ErrorNumber int
      , @ErrorLine int
      , @ErrorMessage nvarchar(4000)
      , @ErrorSeverity int
      , @ErrorState int;

-- dec lare working vars
DECLARE
       @new_premisekey INT
     ,  @alreadytherewithcommoidity INT =0
	 , @alreadytherekeys INT =0
     , @message nvarchar(200)
	, @hasbenchmarks INT=0
	, @new_premiseid VARCHAR(100)=null
	, @new_customerid VARCHAR(100)=null
	, @new_servicepointid VARCHAR(100)=NULL
    , @new_servicecontractid VARCHAR(100)=NULL
	,@new_DimServiceContractKey int


--Start a try block
BEGIN TRY
    --Do validation before starting transaction

    -- check if premise id exists in source client for that commodity
    IF NOT EXISTS(SELECT *
                    FROM InsightsDW.dbo.DimPremise dp
					INNER JOIN dbo.DimServiceContract s
					ON s.PremiseKey = dp.PremiseKey
                    WHERE dp.ClientId = @source_client_id
                      AND PremiseId = @premiseid)
        BEGIN
		  SET @message=N'The premise Id does not exist for client for that commodity ' + CAST(@source_client_id AS nvarchar(10))
            RAISERROR(@message , 12 , 1);
        END;
	   IF @verbose_flag=1 PRINT 'copying premise ' + @premiseid
    
	--check if it already exists in target client for any commodity
    IF EXISTS(SELECT *
                FROM InsightsDW.dbo.DimPremise dp
                WHERE dp.ClientId = @target_client_id
                  AND PremiseId = @premiseid)
	   BEGIN
		  SET @alreadytherekeys=1;
		  IF @verbose_flag=1 PRINT 'Premise id '  + @premiseid + ' is already in the target client for that same commodity'
	   END
	   ELSE 
	   BEGIN
		  SET @alreadytherekeys=0;
	   END

    --check if it already exists in target client for the target commodity
    IF EXISTS(SELECT *
                FROM InsightsDW.dbo.DimPremise dp
				INNER JOIN dbo.DimServiceContract s
					ON s.PremiseKey = dp.PremiseKey
                WHERE dp.ClientId = @target_client_id
                  AND PremiseId = @premiseid)
	   BEGIN
		  SET @alreadytherewithcommoidity=1;
		  IF @verbose_flag=1 PRINT 'Premise id '  + @premiseid + ' is already in the target client for that same commodity'
	   END
	   ELSE 
	   BEGIN
		  SET @alreadytherewithcommoidity=0;
	   END

	  --check service point
	      IF EXISTS(SELECT * FROM dbo.DimServicePoint sp
                WHERE sp.ClientId = @target_client_id
                  AND sp.PremiseId = @premiseid)
	   BEGIN
		  SET @alreadytherekeys=1;
		  IF @verbose_flag=1 PRINT 'associated service point id for premise id= '  + @premiseid + ' is already in the target client'
	   END

	   --check customer
	   IF EXISTS(SELECT *
                FROM InsightsDW.dbo.FactCustomerPremise fcp
			 INNER JOIN dbo.DimPremise dp
			 ON dp.PremiseKey = fcp.PremiseKey
                WHERE ClientId = @target_client_id
                  AND PremiseId = @premiseid)
	   BEGIN
		  SET @alreadytherekeys=1;
		  IF @verbose_flag=1 PRINT 'associated customer id for premise id='  + @premiseid + ' is already in the target client'
	   END

	   	   --check customer
	   IF EXISTS
	   (SELECT customerid FROM dimcustomer WHERE ClientId=@target_client_id AND CustomerId=
	   (SELECT c.CustomerId
                FROM InsightsDW.dbo.FactCustomerPremise fcp
			 INNER JOIN dbo.DimCustomer c
			 ON c.CustomerKey = fcp.CustomerKey
			 INNER JOIN dbo.DimPremise dp
			 ON dp.PremiseKey = fcp.PremiseKey
                WHERE dp.ClientId = @source_client_id
                  AND dp.PremiseId = @premiseid)
			   )
	   BEGIN
		  SET @alreadytherekeys=1;
		  IF @verbose_flag=1 PRINT 'associated customer id for premise id='  + @premiseid + ' is already in the target client'
	   END
	   
	   IF EXISTS(SELECT *
                FROM InsightsDW.dbo.FactCustomerPremise fcp
			 INNER JOIN dbo.DimPremise dp
			 ON dp.PremiseKey = fcp.PremiseKey
                WHERE ClientId = @target_client_id
                  AND PremiseId = @premiseid)
	   BEGIN
		  SET @alreadytherekeys=1;
		  IF @verbose_flag=1 PRINT 'associated customer id for premise id='  + @premiseid + ' is already in the target client'
	   END
	  
	   IF @alreadytherekeys=1 AND @generate_newid=0
        BEGIN
            SET @message = N'The premise Id already exist for client ' + CAST(@target_client_id AS nvarchar(5));
            RAISERROR(@message , 12 , 1);
        END 

	   IF @alreadytherekeys=0 
	   BEGIN
		  --good we can continue
		  IF @verbose_flag=1 PRINT 'Keeping the existing ids ' + @premiseid;
	   END
        ELSE IF @alreadytherekeys=1 AND @generate_newid=1 AND @alreadytherewithcommoidity=1
	   BEGIN 
		  IF @verbose_flag=1 PRINT '**  Generating new ids'
		  --
		  PRINT left('**   *************************************'+ SPACE(70),70) + '  **'
		  IF @alreadytherewithcommoidity=1
		  BEGIN
			  --no need to genertate a new one if it doesnt exits for that commodity
			  EXEC	dbo.GenerateNewId 'DimPremise', 'premiseid',@target_client_id,  @new_premiseid   OUTPUT
			  --IF LEN(@new_premiseid) < LEN(@premiseid) SET @new_premiseid='0' + @new_premiseid
			  PRINT  left('**  Your new premise id is ' + @new_premiseid + SPACE(70),70) + '  **'
		  end
		  --
		  EXEC	dbo.GenerateNewId 'DimServiceContract', 'servicecontractid',@target_client_id,  @new_servicecontractid   OUTPUT
		  
		  PRINT left('**  Your new service contract id is ' + @new_servicecontractid + SPACE(70),70) + '  **'
		  --
		  EXEC	dbo.GenerateNewId 'DimCustomer', 'customerid',@target_client_id,  @new_customerid   OUTPUT
		  
		  PRINT left('**  Your new customer id is ' + @new_customerid + SPACE(70),70) + '  **'
		  --
		  EXEC	dbo.GenerateNewId 'DimServicePoint', 'servicepointid',@target_client_id,  @new_servicepointid   OUTPUT
		  PRINT left('**  Your new service point id is ' + @new_servicepointid + SPACE(70),70) + '  **'

		  PRINT left('**  *************************************'+ SPACE(70),70) + '  **'
		  --
	
	   END
	   ELSE
        BEGIN
		  PRINT '@verbose_flag= ' + COALESCE(CAST(@verbose_flag AS VARCHAR(1)),'NULL') + ' Premisid= ' + COALESCE(@premiseid,'NULL') + ' @alreadytherekeys = ' + COALESCE(CAST(@alreadytherekeys AS varchar(1)),'NULL')
		   SET @message = N'Something went wrong ' 
             RAISERROR(@message , 12 , 1);
	   END

    --check if premise has benchmarks
    IF  EXISTS(SELECT *
                FROM InsightsDW.dbo.Benchmarks b
			 INNER JOIN dbo.DimPremise dp
			 ON dp.PremiseKey = b.PremiseKey
                WHERE dp.clientId = @source_client_id
                  AND PremiseId = @premiseid)  
        BEGIN
		  SET @hasbenchmarks=1
        END
	   ELSE
	   BEGIN
		  SET @hasbenchmarks=0
        END
	   
    --vars for counts
    DECLARE
           @DimPremise_count int = 0
		   ,@DimServiceContract_count int = 0
         , @FactPremiseAttribute_count int = 0
         , @DimServicePoint_count int = 0
         , @DimCustomer_count int = 0
	    , @FactCustomerPremise_count int = 0
         , @FactBilling_count int = 0
         , @FactAmi_count int = 0
         , @FactAmiDetail_count int = 0
	    , @FactAction_count int = 0
	    , @FactActionData_count int = 0
	    , @FactActionItem_count int = 0
	    , @Benchmarks_count int = 0
	    , @target_DimPremise_count int = 0
		, @target_DimServiceContract_count int = 0
         , @target_FactPremiseAttribute_count int = 0
         , @target_DimServicePoint_count int = 0
         , @target_DimCustomer_count int = 0
	    , @target_FactCustomerPremise_count int = 0
         , @target_FactBilling_count int = 0
         , @target_FactAmi_count int = 0
         , @target_FactAmiDetail_count int = 0
	    , @target_Benchmarks_count int = 0
	    , @target_FactAction_count int = 0
	    , @target_FactActionData_count int = 0
	    , @target_FactActionItem_count int = 0
	    , @counts_dont_agree INT =0
	    , @new_ActionDetailKey INT=0
		 , @source_Service_contract_type_key INT=0
		, @target_Service_contract_type_key INT=0

--	1 will translate to 4	for electric 
--	2 will translate to 5	for gas 	
--	3 will translate to 6	for water
if @source_commodity = 1 set  @source_Service_contract_type_key=4;
if @source_commodity = 2 set  @source_Service_contract_type_key=5;
if @source_commodity = 3 set  @source_Service_contract_type_key=6;

if @target_commodity = 1 set  @target_Service_contract_type_key=4;
if @target_commodity = 2 set  @target_Service_contract_type_key=5;
if @target_commodity = 3 set  @target_Service_contract_type_key=6;


--populate source counts

    SELECT @DimPremise_count = COUNT(*)
      FROM InsightsDW.dbo.DimPremise
      WHERE PremiseId = @premiseid and clientid=@source_client_id;

    SELECT @DimServiceContract_count = COUNT(*)
      FROM
           InsightsDW.dbo.DimServiceContract INNER JOIN dbo.DimPremise dp
           ON dp.PremiseKey = DimServiceContract.PremiseKey
      WHERE PremiseId = @premiseid and dp.clientid=@source_client_id
	  AND DimServiceContract.ServiceContractTypeKey=@source_Service_contract_type_key

    SELECT @FactPremiseAttribute_count = COUNT(*)
      FROM InsightsDW.dbo.FactPremiseAttribute fpa
      WHERE PremiseId = @premiseid and clientid=@source_client_id;

    SELECT @DimCustomer_count = COUNT(*)
      FROM
           InsightsDW.dbo.DimCustomer c INNER JOIN InsightsDW.dbo.FactCustomerPremise fcp
           ON fcp.CustomerKey = c.CustomerKey
           INNER JOIN dbo.DimPremise dp
           ON dp.PremiseKey = fcp.PremiseKey
      WHERE PremiseId = @premiseid and dp.clientid=@source_client_id;
    SELECT @DimServicePoint_count = COUNT(*)
      FROM InsightsDW.dbo.DimServicePoint fpa
      WHERE PremiseId = @premiseid and clientid=@source_client_id;

    SELECT @FactBilling_count = COUNT(*)
      FROM InsightsDW.dbo.FactBilling
      WHERE PremiseId = @premiseid and clientid=@source_client_id
	  AND CommodityId=@source_commodity;

    SELECT @FactAmi_count = COUNT(*)
      FROM
           InsightsDW.dbo.FactAmi INNER JOIN dbo.DimPremise dp
           ON dp.PremiseKey = FactAmi.PremiseKey
      WHERE PremiseId = @premiseid and dp.clientid=@source_client_id;

    SELECT @FactAmiDetail_count = COUNT(*)
      FROM InsightsDW.dbo.FactAmiDetail fad
      WHERE PremiseId = @premiseid and clientid=@source_client_id;

    SELECT @Benchmarks_count = COUNT(*)
      FROM
           InsightsDW.dbo.Benchmarks INNER JOIN dbo.DimPremise dp
           ON dp.PremiseKey = Benchmarks.PremiseKey
      WHERE PremiseId = @premiseid and dp.clientid=@source_client_id;
	 --
	 -- Actions
	 --
        SELECT @FactAction_count = COUNT(*)
      FROM
           InsightsDW.dbo.factAction fa INNER JOIN dbo.DimPremise dp
           ON dp.PremiseKey = fa.PremiseKey
      WHERE dp.PremiseId = @premiseid and dp.clientid=@source_client_id;
	 	 --
	 SELECT @FactActionItem_count = COUNT(*)
      FROM
           InsightsDW.dbo.FactActionItem fai INNER JOIN dbo.DimPremise dp
           ON dp.PremiseKey = fai.PremiseKey
      WHERE dp.PremiseId = @premiseid and dp.clientid=@source_client_id;
	 	 --
        SELECT @FactActionData_count = COUNT(*)
      FROM
           InsightsDW.dbo.factActionData fad INNER JOIN 
		 InsightsDW.dbo.FactAction fai
		 ON fai.ActionDetailKey = fad.ActionDetailKey
		 INNER JOIN  dbo.DimPremise dp
           ON dp.PremiseKey = fai.PremiseKey
      WHERE dp.PremiseId = @premiseid and dp.clientid=@source_client_id;

IF @verbose_flag=1
BEGIN
--Print source counts
    PRINT 'There are ' + CAST(@DimPremise_count AS varchar(10)) + ' row(s) in the DimPremise table for client ' + CAST(@source_client_id AS varchar(10))
		  + ' and premise id ' + @premiseid
    PRINT 'There are ' + CAST(@DimServiceContract_count AS varchar(10)) + ' row(s) in the DimServiceContract table for client ' + CAST(@source_client_id AS varchar(10))
		  + ' and premise id ' + @premiseid
    PRINT 'There are ' + CAST(@FactPremiseAttribute_count AS varchar(10)) + ' row(s) in the FactPremiseAttribute table for client ' + CAST(
		  @source_client_id AS varchar(10)) + ' and premise id ' + @premiseid
    PRINT 'There are ' + CAST(@DimServicePoint_count AS varchar(10)) + ' row(s) in the DimServicePoint table for client ' + CAST(@source_client_id AS
		  VARCHAR(10)) + ' and premise id ' + @premiseid
    PRINT 'There are ' + CAST(@DimCustomer_count AS varchar(10)) + ' row(s) in the DimCustomer table for client ' + 
		  CAST(@source_client_id AS varchar(10)) + ' and premise id ' + @premiseid
    PRINT 'There are ' + CAST(@FactBilling_count AS varchar(10)) + ' row(s) in the FactBilling table for client and that commodity' + 
		  CAST(@source_client_id AS varchar(10)) + ' and premise id ' + @premiseid
    PRINT 'There are ' + CAST(@FactAmi_count AS varchar(10)) + ' row(s) in the FactAmi table for client ' + CAST(@source_client_id AS varchar(10)) +
		  ' and premise id ' + @premiseid
    PRINT 'There are ' + CAST(@FactAmiDetail_count AS varchar(10)) + ' row(s) in the FactAmiDetail table for client ' + CAST(@source_client_id AS
		  VARCHAR(10)) + ' and premise id ' + @premiseid
    PRINT 'There are ' + CAST(@Benchmarks_count AS varchar(10)) + ' row(s) in the Benchmarks table for client ' + CAST(@source_client_id AS
		  VARCHAR(10)) + ' and premise id ' + @premiseid
    --
    -- actions
    --
    PRINT 'There are ' + CAST(@FactAction_count AS varchar(10)) + ' row(s) in the FactAction table for client ' + CAST(@source_client_id AS varchar(10)) +
		  ' and premise id ' + @premiseid
    PRINT 'There are ' + CAST(@FactActionData_count AS varchar(10)) + ' row(s) in the FactActionData table for client ' + CAST(@source_client_id AS
		  VARCHAR(10)) + ' and premise id ' + @premiseid
    PRINT 'There are ' + CAST(@FactActionItem_count AS varchar(10)) + ' row(s) in the FactActionItem table for client ' + CAST(@source_client_id AS
		  VARCHAR(10)) + ' and premise id ' + @premiseid		  
END

    BEGIN TRANSACTION;
    DECLARE @myrowcount INT;
    --add premise

	IF @alreadytherewithcommoidity=0 AND @alreadytherekeys=0
	BEGIN
	-- we only need to do this if the premise is not there at all
	-- if it there but just for another commodity we can just use the 
	-- premise row that is there
		INSERT INTO InsightsDW.dbo.DimPremise
		SELECT PostalCodeKey
			 , CityKey
			 , SourceKey
			 , AccountId
			 , COALESCE(@new_premiseid, PremiseId)
			 , Street1
			 , Street2
			 , City
			 , StateProvince
			 , Country
			 , PostalCode
			 , GasService
			 , ElectricService
			 , WaterService
			 , CreateDate
			 , UpdateDate
			 , @target_client_id
			 , GeoLocation
			 , SourceId
			 , ETL_LogId
			 , IsInferred
			 , TrackingId
			 , TrackingDate
		  FROM InsightsDW.dbo.DimPremise
		  WHERE ClientId = @source_client_id
			AND PremiseId = @premiseid;
		SET @myrowcount=@@ROWCOUNT;
		IF @verbose_flag=1 PRINT CAST( @myrowcount AS varchar(5)) + ' row added to DimPremise';
		SET @new_premisekey = SCOPE_IDENTITY();
		-- debugging code below
	END


	--add servicecontract
	INSERT INTO [dbo].[DimServiceContract]
	SELECT	TOP 1 
			COALESCE(@new_premisekey, dp.PremiseKey)
           ,[CommodityKey]
           , @target_client_id
           ,COALESCE(@new_servicecontractid, s.ServiceContractId)
           ,@target_Service_contract_type_key
           ,[ServiceContractDescription]
           ,[ActiveDate]
           ,[InActiveDate]
           ,s.[IsInferred]
           ,s.[TrackingId]
           ,s.[TrackingDate]
           ,[BudgetBillingIndicator]
		   FROM [dbo].[DimServiceContract] s
	INNER JOIN dbo.DimPremise dp
	ON dp.PremiseKey = s.PremiseKey      
	WHERE dp.ClientId = @source_client_id
    AND dp.PremiseId = @premiseid
	AND s.[ServiceContractTypeKey]=@source_Service_contract_type_key
	ORDER BY [ActiveDate] desc;
    SET @myrowcount=@@ROWCOUNT;
    IF @verbose_flag=1 PRINT CAST( @myrowcount AS varchar(5)) + ' row added to DimServiceContract';
    SET @new_DimServiceContractKey = SCOPE_IDENTITY();




    --add premise attributes
		
    INSERT INTO InsightsDW.dbo.FactPremiseAttribute
    SELECT COALESCE(@new_premisekey, dp.PremiseKey)
         , fpa.OptionKey
         , fpa.SourceKey
         , fpa.ContentAttributeKey
         , fpa.ContentOptionKey
         , fpa.AttributeId
         , fpa.OptionId
         , fpa.Value
         , @target_client_id
         ,  COALESCE(@new_premiseid,dp.premiseid)
         , fpa.AccountID
         , fpa.SourceID
         , fpa.EffectiveDateKey
         , fpa.EffectiveDate
         , fpa.ETLLogID
         , fpa.TrackingID
         , fpa.TrackingDate
      FROM
           InsightsDW.dbo.FactPremiseAttribute fpa INNER JOIN InsightsDW.dbo.DimPremise dp
           ON dp.PremiseKey = fpa.PremiseKey
      WHERE dp.ClientId = @source_client_id
        AND dp.PremiseId = @premiseid;
    SET @myrowcount=@@ROWCOUNT;
    IF @verbose_flag=1 PRINT CAST( @myrowcount AS varchar(5)) + ' row added to FactPremiseAttribute';   



    --insert servicepoint
    INSERT INTO InsightsDW.dbo.DimServicePoint
    SELECT COALESCE(@new_premisekey, PremiseKey)
         , COALESCE(@new_ServicePointId, sp.ServicePointId)
         , @target_client_id
         , COALESCE(@new_premiseid, sp.PremiseId)
         , SourceId
         , TrackingId
         , TrackingDate
         , IsInferred
      FROM InsightsDW.dbo.DimServicePoint sp
      WHERE sp.PremiseId = @premiseid
        AND sp.ClientId = @source_client_id;

    SET @myrowcount=@@ROWCOUNT;
    IF @verbose_flag=1 PRINT CAST( @myrowcount AS varchar(5)) + ' row added to DimServicePoint';   

    --customer Stuff
    DECLARE
           @customerkeytable TABLE(id int IDENTITY
                                 , old_customerkey int
                                 , new_customerkey int);

    DECLARE @new_Clientkey INT

    SELECT @new_Clientkey=ClientKey
             FROM InsightsDW.dbo.DimClient
             WHERE ClientId = @target_client_id


		  IF @verbose_flag=1 PRINT 'Adding to customer table'

		  IF @new_customerid IS  NULL
            BEGIN
		  INSERT INTO InsightsDW.dbo.DimCustomer(ClientKey
									    , ClientId
									    , PostalCodeKey
									    , CustomerAuthenticationTypeKey
									    , SourceKey
									    , CityKey
									    , CustomerId
									    , FirstName
									    , LastName
									    , Street1
									    , Street2
									    , City
									    , StateProvince
									    , Country
									    , PostalCode
									    , PhoneNumber
									    , MobilePhoneNumber
									    , EmailAddress
									    , AlternateEmailAddress
									    , CreateDate
									    , UpdateDate
									    , SourceId
									    , AuthenticationTypeId
									    , Language
									    , ETL_LogId
									    , IsInferred
									    , TrackingId
									    , TrackingDate)
		  OUTPUT NULL
			  , INSERTed.customerKey AS new_customerkey
			    INTO @customerkeytable

		  -- go get list of customer associated with old premise
		  SELECT
			 @new_Clientkey
			 , @target_client_id AS clientid
			 , c.PostalCodeKey
			 , c.CustomerAuthenticationTypeKey
			 , c.SourceKey
			 , c.CityKey
			 , COALESCE(@new_customerid,c.CustomerId)
			 , c.FirstName
			 , c.LastName
			 , c.Street1
			 , c.Street2
			 , c.City
			 , c.StateProvince
			 , c.Country
			 , c.PostalCode
			 , c.PhoneNumber
			 , c.MobilePhoneNumber
			 , c.EmailAddress
			 , c.AlternateEmailAddress
			 , c.CreateDate
			 , c.UpdateDate
			 , c.SourceId
			 , c.AuthenticationTypeId
			 , c.Language
			 , c.ETL_LogId
			 , c.IsInferred
			 , c.TrackingId
			 , c.TrackingDate
		    FROM
			    InsightsDW.dbo.DimCustomer c INNER JOIN dbo.FactCustomerPremise fcp
			    ON fcp.CustomerKey = c.CustomerKey
									   INNER JOIN InsightsDW.dbo.DimPremise dp
			    ON dp.PremiseKey = fcp.PremiseKey
		    WHERE dp.ClientId = @source_client_id
			 AND dp.PremiseId = @premiseid
		    ORDER BY c.CustomerKey ASC;
		    SET @myrowcount=@@ROWCOUNT;
		  END
		  ELSE
            BEGIN
		  INSERT INTO InsightsDW.dbo.DimCustomer(ClientKey
									    , ClientId
									    , PostalCodeKey
									    , CustomerAuthenticationTypeKey
									    , SourceKey
									    , CityKey
									    , CustomerId
									    , FirstName
									    , LastName
									    , Street1
									    , Street2
									    , City
									    , StateProvince
									    , Country
									    , PostalCode
									    , PhoneNumber
									    , MobilePhoneNumber
									    , EmailAddress
									    , AlternateEmailAddress
									    , CreateDate
									    , UpdateDate
									    , SourceId
									    , AuthenticationTypeId
									    , Language
									    , ETL_LogId
									    , IsInferred
									    , TrackingId
									    , TrackingDate)
		  OUTPUT NULL
			  , INSERTed.customerKey AS new_customerkey
			    INTO @customerkeytable

		  -- go get list of customer associated with old premise
		  SELECT TOP 1
			 @new_Clientkey
			 , @target_client_id AS clientid
			 , c.PostalCodeKey
			 , c.CustomerAuthenticationTypeKey
			 , c.SourceKey
			 , c.CityKey
			 , COALESCE(@new_customerid,c.CustomerId)
			 , c.FirstName
			 , c.LastName
			 , c.Street1
			 , c.Street2
			 , c.City
			 , c.StateProvince
			 , c.Country
			 , c.PostalCode
			 , c.PhoneNumber
			 , c.MobilePhoneNumber
			 , c.EmailAddress
			 , c.AlternateEmailAddress
			 , c.CreateDate
			 , c.UpdateDate
			 , c.SourceId
			 , c.AuthenticationTypeId
			 , c.Language
			 , c.ETL_LogId
			 , c.IsInferred
			 , c.TrackingId
			 , c.TrackingDate
		    FROM
			    InsightsDW.dbo.DimCustomer c INNER JOIN dbo.FactCustomerPremise fcp
			    ON fcp.CustomerKey = c.CustomerKey
									   INNER JOIN InsightsDW.dbo.DimPremise dp
			    ON dp.PremiseKey = fcp.PremiseKey
		    WHERE dp.ClientId = @source_client_id
			 AND dp.PremiseId = @premiseid
		    ORDER BY c.CustomerKey ASC;
		    SET @myrowcount=@@ROWCOUNT;
		  END

		  
		  IF @verbose_flag=1 PRINT CAST( @myrowcount AS varchar(5)) + ' row added to DimCustomer';   

		  --update the key table
		  WITH q
			 AS (SELECT TOP 100 PERCENT c.customerkey
								 , ROW_NUMBER()OVER(ORDER BY c.customerKey)AS rn
				  FROM
					  InsightsDW.dbo.DimCustomer c INNER JOIN dbo.FactCustomerPremise fcp
					  ON fcp.CustomerKey = c.CustomerKey
											 INNER JOIN InsightsDW.dbo.DimPremise dp
					  ON dp.PremiseKey = fcp.PremiseKey
				  WHERE dp.ClientId = @source_client_id
				    AND dp.PremiseId = @premiseid
				  ORDER BY c.CustomerKey ASC)
			 UPDATE @customerkeytable
			   SET old_customerkey = (SELECT q.customerkey
								   FROM q
								   WHERE id = q.rn)

--add customer premise association

INSERT INTO dbo.FactCustomerPremise
SELECT k.new_customerkey
     , COALESCE(@new_premisekey, dp.PremiseKey)
     , fcp.ETL_LogId
     , fcp.DateCreated
  FROM
       dbo.FactCustomerPremise fcp INNER JOIN InsightsDW.dbo.DimPremise dp
       ON dp.PremiseKey = fcp.PremiseKey
                                   INNER JOIN @customerkeytable k
       ON k.old_customerkey = fcp.CustomerKey
  WHERE dp.ClientId = @source_client_id
    AND dp.PremiseId = @premiseid;   --old key

    SET @myrowcount=@@ROWCOUNT;
    IF @verbose_flag=1 PRINT CAST( @myrowcount AS varchar(5)) + ' row added to FactCustomerPremise';       

--add fact billing

IF (@source_commodity = @target_commodity) AND 
(
NOT EXISTS (SELECT * FROM
  InsightsDW.dbo.factbilling
	  WHERE ClientId = @target_client_id
		AND PremiseKey = COALESCE(@new_premisekey, PremiseKey)
		AND CommodityId=@source_commodity
		)
)
begin
	INSERT INTO InsightsDW.dbo.FactBilling
	SELECT COALESCE(@new_premisekey, PremiseKey)
		 , BillPeriodStartDateKey
		 , BillPeriodEndDateKey
		 , CommodityKey
		 , UOMKey
		 , BillPeriodTypeKey
		 , ServicePointKey
		 , @target_client_id
		 , COALESCE(@new_premiseid, PremiseId)
		 , AccountId
		 , COALESCE(@new_servicepointid,ServicePointId)
		 , CommodityId
		 , BillPeriodTypeId
		 , UOMId
		 , StartDate
		 , EndDate
		 , TotalUnits
		 , TotalCost
		 , CreateDate
		 , UpdateDate
		 , BillDays
		 , ETL_LogId
		 , TrackingId
		 , TrackingDate
		 , SourceKey
		 , SourceID
	  FROM InsightsDW.dbo.factbilling
	  WHERE ClientId = @source_client_id
		AND PremiseId = @premiseid
		AND CommodityId=@source_commodity

	SET @myrowcount=@@ROWCOUNT;
		IF @verbose_flag=1 PRINT CAST( @myrowcount AS varchar(5)) + ' rows added to FactBilling';    
END
else
BEGIN
	PRINT 'source commodity= ' + CAST(@source_commodity AS VARCHAR(1))
	PRINT 'targer commodity= ' + CAST(@target_commodity AS VARCHAR(1))
	PRINT @new_premisekey
	PRINT 'cound not add bills'
END

--AMI Stuff
DECLARE
       @amikeytable TABLE(id int IDENTITY
                        , old_amikey int NULL
                        , new_amikey int NULL);

  ----------------------------------------


----add ami headers
INSERT INTO dbo.FactAmi
OUTPUT NULL, INSERTed.AmiKey AS new_amikey    
       INTO @amikeytable
SELECT TOP 100 PERCENT @target_client_id
                     , COALESCE(@new_premisekey, dp.PremiseKey)
                     , ServicePointKey
                     , MeterId
                     , fa.ETL_LogId
  FROM
       dbo.FactAmi fa INNER JOIN dbo.DimPremise dp
       ON dp.PremiseKey = fa.PremiseKey
  WHERE dp.Premiseid = @premiseid
    AND dp.ClientId = @source_client_id
  ORDER BY fa.AmiKey;
SET @myrowcount=@@ROWCOUNT;
    IF @verbose_flag=1 PRINT CAST( @myrowcount AS varchar(5)) + ' rows added to FactAmi'; 

--update the key table
WITH q
    AS (SELECT TOP 100 PERCENT fa.amikey
                             , ROW_NUMBER()OVER(ORDER BY fa.AmiKey)AS rn
          FROM
               dbo.FactAmi fa INNER JOIN dbo.DimPremise dp
               ON dp.PremiseKey = fa.PremiseKey
          WHERE dp.PremiseId = @premiseid
            AND dp.ClientId = @source_client_id
          ORDER BY fa.AmiKey)
    UPDATE @amikeytable
      SET old_amikey = (SELECT q.amikey
                          FROM q
                          WHERE id = q.rn);



--add ami details
INSERT INTO dbo.FactAmiDetail
SELECT k.new_amikey
     , fad.DateKey
     , @target_client_id
     , COALESCE(@new_premiseid, fad.PremiseId)
     , fad.AccountId
     , COALESCE(@new_servicepointid,fad.ServicePointId)
     , COALESCE(@new_customerid,fad.CustomerId)
     , fad.CommodityId
     , fad.MeterId
     , fad.UOMId
     , fad.TotalUnits
     , fad.SourceId
     , fad.ETL_LogId
  FROM
       dbo.FactAmiDetail fad INNER JOIN @amikeytable k
       ON fad.AmiKey = k.old_amikey
  WHERE fad.ClientId = @source_client_id
    AND fad.PremiseId = @premiseid;

SET @myrowcount=@@ROWCOUNT;
    IF @verbose_flag=1 PRINT CAST( @myrowcount AS varchar(5)) + ' rows added to FactAmiDetail';


-- add benchmarks if they have any
IF @hasbenchmarks =1
BEGIN
INSERT INTO dbo.Benchmarks
SELECT   @target_client_id, 
         COALESCE(@new_premisekey, dp.PremiseKey),
          BillMonth ,
          StartDateKey ,
          EndDateKey ,
          CommodityKey ,
          BenchmarkGroupTypeKey ,
          GroupId ,
          GroupCount ,
          MyUsage ,
          AverageUsage ,
          EfficientUsage ,
          UOMKey ,
          MyCost ,
          AverageCost ,
          EfficientCost ,
          CostCurrencyCost ,
          DateCreated
FROM dbo.Benchmarks  b
INNER JOIN dbo.DimPremise dp
ON dp.PremiseKey = b.PremiseKey      
  WHERE dp.ClientId = @source_client_id
    AND dp.PremiseId = @premiseid;
END

--
-- Actions
--

DECLARE
       @actionkeytable TABLE(id int IDENTITY
                        , old_actionkey int NULL
                        , new_actionkey int NULL);


----add ami headers
merge INTO dbo.Factaction USING 
(
SELECT    COALESCE(@new_premisekey, dp.PremiseKey) AS new_premiskey,
          DateKey ,
          ActionId , 
          ActionKey ,
          SubActionKey ,
          StatusKey ,
          StatusDate ,
          @target_client_id  AS target_client_id,
          COALESCE(@new_premiseid, fa.PremiseId) AS new_premisid,
          fa.AccountID ,
          fa.SourceKey ,
          fa.CreateDate ,
          ETLLogID ,
          fa.TrackingID ,
          fa.TrackingDate,
		fa.ActionDetailKey AS old_actionkey
FROM
    InsightsDW.dbo.factAction fa INNER JOIN dbo.DimPremise dp
    ON dp.PremiseKey = fa.PremiseKey
WHERE dp.PremiseId = @premiseid and dp.clientid=@source_client_id
) p ON 1=0
WHEN NOT MATCHED then
INSERT 
        ( PremiseKey ,
          DateKey ,
          ActionId ,
          ActionKey ,
          SubActionKey ,
          StatusKey ,
          StatusDate ,
          ClientID ,
          PremiseID ,
          AccountID ,
          SourceKey ,
          CreateDate ,
          ETLLogID ,
          TrackingID ,
          TrackingDate
        )
VALUES(
	     p.new_premiskey,
          p. DateKey ,
          p.ActionId , 
          p.ActionKey ,
          p.SubActionKey ,
          p.StatusKey ,
          p.StatusDate ,
          p.target_client_id ,
          p.new_premisid,
          p.AccountID ,
          p.SourceKey ,
          p.CreateDate ,
          p.ETLLogID ,
          p.TrackingID ,
          p.TrackingDate
		)
OUTPUT p.old_actionkey AS old_actionkey, INSERTed.ActionDetailKey AS new_actionkey    
       INTO @actionkeytable;

    --
INSERT   InsightsDW.dbo.FactActionItem	
   SELECT ActionItemKey ,
          COALESCE(@new_premisekey, dp.PremiseKey) ,
          EnergyObjectKey ,
          Payback ,
          CO2Savings ,
          ROI ,
          AnnualUsageElectric ,
          AnnualUsageGas ,
          AnnualUsageWater ,
          AnnualUsagePropane ,
          AnnualUsageOil ,
          AnnualUsageWood ,
          UsageSavingsElectric ,
          UsageSavingsGas ,
          UsageSavingsWater ,
          UsageSavingsPropane ,
          UsageSavingsOil ,
          UsageSavingsWood ,
          DollarSavingsElectric ,
          DollarSavingsGas ,
          DollarSavingsWater ,
          DollarSavingsPropane ,
          DollarSavingsOil ,
          DollarSavingsWood ,
          Cost ,
          DateUpdatedKey ,
          StatusKey ,
          Winter ,
          Spring ,
          Summer ,
          Fall        
FROM
    InsightsDW.dbo.FactActionItem fai INNER JOIN dbo.DimPremise dp
    ON dp.PremiseKey = fai.PremiseKey
WHERE dp.PremiseId = @premiseid and dp.clientid=@source_client_id;
    --
INSERT  InsightsDW.dbo.factActionData
SELECT    a.new_actionkey ,
          ActionDataKey ,
          ActionDataValue
FROM	    
    InsightsDW.dbo.factActionData fad INNER JOIN 
    InsightsDW.dbo.FactAction fa
    ON fa.ActionDetailKey = fad.ActionDetailKey
    INNER JOIN  dbo.DimPremise dp
    ON dp.PremiseKey = fa.PremiseKey
    INNER join @actionkeytable a
    ON a.old_actionkey=fa.ActionDetailKey
WHERE dp.PremiseId = @premiseid and dp.clientid=@source_client_id;


SET @myrowcount=@@ROWCOUNT;
    IF @verbose_flag=1 PRINT CAST( @myrowcount AS varchar(5)) +' rows added to Benchmarks';

--populate target counts
   SELECT @target_DimPremise_count = COUNT(*)
      FROM InsightsDW.dbo.DimPremise
      WHERE PremiseId = COALESCE(@new_premiseid,@PremiseId) and clientid=@target_client_id;
	  --
   SELECT @target_FactPremiseAttribute_count = COUNT(*)
      FROM InsightsDW.dbo.FactPremiseAttribute fpa
      WHERE PremiseId = COALESCE(@new_premiseid,@PremiseId) and clientid=@target_client_id;

--service contract
	  --
	     SELECT @target_DimServiceContract_count = COUNT(*)
      FROM
           InsightsDW.dbo.DimServiceContract INNER JOIN dbo.DimPremise dp
           ON dp.PremiseKey = DimServiceContract.PremiseKey
      WHERE PremiseId = COALESCE(@new_premiseid,@PremiseId) and dp.clientid=@target_client_id
	  AND DimServiceContract.ServiceContractTypeKey=@target_Service_contract_type_key

--customer
   SELECT @target_DimCustomer_count = COUNT(*)
      FROM
           InsightsDW.dbo.DimCustomer c INNER JOIN InsightsDW.dbo.FactCustomerPremise fcp
           ON fcp.CustomerKey = c.CustomerKey
                                        INNER JOIN dbo.DimPremise dp
           ON dp.PremiseKey = fcp.PremiseKey
      WHERE PremiseId = COALESCE(@new_premiseid,@PremiseId) and dp.clientid=@target_client_id;
--
   SELECT @target_DimServicePoint_count = COUNT(*)
      FROM InsightsDW.dbo.DimServicePoint fpa
      WHERE PremiseId = COALESCE(@new_premiseid,@PremiseId) and clientid=@target_client_id;

   SELECT @target_FactBilling_count = COUNT(*)
      FROM InsightsDW.dbo.FactBilling
      WHERE PremiseId = COALESCE(@new_premiseid,@PremiseId) and clientid=@target_client_id;

   SELECT @target_FactAmi_count = COUNT(*)
      FROM
           InsightsDW.dbo.FactAmi INNER JOIN dbo.DimPremise dp
           ON dp.PremiseKey = FactAmi.PremiseKey
      WHERE PremiseId = COALESCE(@new_premiseid,@PremiseId) and dp.clientid=@target_client_id;
	  --

   SELECT @target_FactAmiDetail_count = COUNT(*)
      FROM InsightsDW.dbo.FactAmiDetail fad
      WHERE PremiseId = COALESCE(@new_premiseid,@PremiseId) and clientid=@target_client_id;
	  --
	     SELECT @target_Benchmarks_count = COUNT(*)
      FROM
           InsightsDW.dbo.Benchmarks INNER JOIN dbo.DimPremise dp
           ON dp.PremiseKey = Benchmarks.PremiseKey
      WHERE PremiseId = COALESCE(@new_premiseid,@PremiseId) and dp.clientid=@target_client_id;
	 --
	 -- Actions
	 --
        SELECT @target_FactAction_count = COUNT(*)
      FROM
           InsightsDW.dbo.factAction fa INNER JOIN dbo.DimPremise dp
           ON dp.PremiseKey = fa.PremiseKey
      WHERE dp.PremiseId =  COALESCE(@new_premiseid,@PremiseId) and dp.clientid=@target_client_id;
	 	 --
	 SELECT @target_FactActionItem_count = COUNT(*)
      FROM
           InsightsDW.dbo.FactActionItem fai INNER JOIN dbo.DimPremise dp
           ON dp.PremiseKey = fai.PremiseKey
      WHERE dp.PremiseId =  COALESCE(@new_premiseid,@PremiseId) and dp.clientid=@target_client_id;
	 	 --
        SELECT @target_FactActionData_count = COUNT(*)
      FROM
           InsightsDW.dbo.factActionData fad INNER JOIN 
		 InsightsDW.dbo.FactAction fai
		 ON fai.ActionDetailKey = fad.ActionDetailKey
		 INNER JOIN  dbo.DimPremise dp
           ON dp.PremiseKey = fai.PremiseKey
      WHERE dp.PremiseId =  COALESCE(@new_premiseid,@PremiseId) and dp.clientid=@target_client_id;


IF @verbose_flag=1 
BEGIN
	   -- print target counts
	   PRINT '==================== Summary =============================='
	   PRINT 'There are ' + CAST(@target_DimPremise_count AS varchar(10)) + ' row(s) in the DimPremise table for client ' + CAST(@target_client_id AS varchar(10))
			 + ' and premise id ' + COALESCE(@new_premiseid,@PremiseId);
			   PRINT 'There are ' + CAST(@target_DimServiceContract_count AS varchar(10)) + ' row(s) in the DimServiceContract table for client ' + CAST(@target_client_id AS varchar(10))
			 + ' and premise id ' + COALESCE(@new_premiseid,@PremiseId);
	   PRINT 'There are ' + CAST(@target_FactPremiseAttribute_count AS varchar(10)) + ' row(s) in the FactPremiseAttribute table for client ' + CAST(
			 @target_client_id AS varchar(10)) + ' and premise id ' + @new_premiseid;
	   PRINT 'There are ' + CAST(@target_DimServicePoint_count AS varchar(10)) + ' row(s) in the DimServicePoint table for client ' + CAST(@target_client_id AS
			 VARCHAR(10)) + ' and premise id ' + COALESCE(@new_premiseid,@PremiseId);
	   PRINT 'There are ' + CAST(@target_DimCustomer_count AS varchar(10)) + ' row(s) in the DimCustomer table for client ' + 
			 CAST(@target_client_id AS varchar(10)) + ' and premise id ' + COALESCE(@new_premiseid,@PremiseId);
	   PRINT 'There are ' + CAST(@target_FactBilling_count AS varchar(10)) + ' row(s) in the FactBilling table for client ' + 
			 CAST(@target_client_id AS varchar(10)) + ' and premise id ' + COALESCE(@new_premiseid,@PremiseId);
	   PRINT 'There are ' + CAST(@target_FactAmi_count AS varchar(10)) + ' row(s) in the FactAmi table for client ' + CAST(@target_client_id AS varchar(10)) +
			 ' and premise id ' + COALESCE(@new_premiseid,@PremiseId);
	   PRINT 'There are ' + CAST(@target_FactAmiDetail_count AS varchar(10)) + ' row(s) in the FactAmiDetail table for client ' + CAST(@target_client_id AS
			 VARCHAR(10)) + ' and premise id ' + COALESCE(@new_premiseid,@PremiseId);
	   PRINT 'There are ' + CAST(@target_Benchmarks_count AS varchar(10)) + ' row(s) in the Benchmarks table for client ' + CAST(@target_client_id AS
			 VARCHAR(10)) + ' and premise id ' + COALESCE(@new_premiseid,@PremiseId);
	   --
	   -- actions
	   --
	   PRINT 'There are ' + CAST(@target_FactAction_count AS varchar(10)) + ' row(s) in the FactAction table for client ' + CAST(@source_client_id AS varchar(10)) +
			 ' and premise id '  + COALESCE(@new_premiseid,@PremiseId);
	   PRINT 'There are ' + CAST(@target_FactActionData_count AS varchar(10)) + ' row(s) in the FactActionData table for client ' + CAST(@source_client_id AS
			 VARCHAR(10)) + ' and premise id '  + COALESCE(@new_premiseid,@PremiseId);
	   PRINT 'There are ' + CAST(@target_FactActionItem_count AS varchar(10)) + ' row(s) in the FactActionItem table for client ' + CAST(@source_client_id AS
			 VARCHAR(10)) + ' and premise id '  + COALESCE(@new_premiseid,@PremiseId);
END

--print descrepancies
SET @counts_dont_agree=0
IF @DimPremise_count != @target_DimPremise_count
    BEGIN
	   PRINT 'DimPremise counts dont agree!!  --ERROR'
	   SET @counts_dont_agree=1
    END
IF @DimServiceContract_count != @target_DimServiceContract_count
    BEGIN
	   PRINT 'DimServiceContract counts dont agree!!  --ERROR'
	   SET @counts_dont_agree=1
    END
IF @FactPremiseAttribute_count != @target_FactPremiseAttribute_count
    BEGIN
	   PRINT 'FactPremiseAttribute counts dont agree!!  --ERROR'
	   SET @counts_dont_agree=1
    END
IF @DimServicePoint_count != @target_DimServicePoint_count
    BEGIN
	   PRINT 'DimServicePoint counts dont agree!!  --ERROR'
		  SET @counts_dont_agree=1
    END
IF @DimCustomer_count != @target_DimCustomer_count AND @new_customerid IS null
    BEGIN
	   PRINT 'DimCustomer counts dont agree!!  --ERROR'
		  SET @counts_dont_agree=1
    END
IF  @target_DimCustomer_count=1 AND @new_customerid IS not null
    BEGIN
	   PRINT 'Not checking Customer count because only 1 customer is added when its a generated customer id is used'		  
    END
IF @FactCustomerPremise_count != @target_FactCustomerPremise_count 
    BEGIN
	   PRINT 'FactCustomerPremise_count counts dont agree!!  --ERROR'
		  SET @counts_dont_agree=1
    END
IF @FactBilling_count != @target_FactBilling_count
    BEGIN
	   PRINT 'FactBilling counts dont agree!!  --ERROR'
		  SET @counts_dont_agree=1
    END
IF @FactAmi_count != @target_FactAmi_count
    BEGIN
	   PRINT 'FactAmi counts dont agree!!  --ERROR'
		  SET @counts_dont_agree=1
    END
IF @FactAmiDetail_count != @target_FactAmiDetail_count
    BEGIN
	   PRINT 'FactAmiDetail counts dont agree!!  --ERROR'
		  SET @counts_dont_agree=1
    END 
IF @Benchmarks_count != @target_Benchmarks_count AND @hasbenchmarks=1
    BEGIN
	   PRINT 'Benchmarks counts dont agree!!  --ERROR'
		  SET @counts_dont_agree=1
    END 
--
-- actions
--
IF @FactAction_count != @target_FactAction_count
    BEGIN
	   PRINT 'FactAction counts dont agree!!  --ERROR'
		  SET @counts_dont_agree=1
    END 
IF @FactActionData_count != @target_FactActionData_count
    BEGIN
	   PRINT 'FactActionData counts dont agree!!  --ERROR'
		  SET @counts_dont_agree=1
    END 
IF @FactActionItem_count != @target_FactActionItem_count
    BEGIN
	   PRINT 'FactActionItem counts dont agree!!  --ERROR'
		  SET @counts_dont_agree=1
    END 
IF @counts_dont_agree=1
BEGIN
    RAISERROR('Row count error' , 12 , 1);
END
ELSE
PRINT 'All counts OK '

--counts agree, ready to committ
COMMIT TRANSACTION;

END TRY
BEGIN CATCH

    IF @@TRANCOUNT > 0
        BEGIN
            PRINT 'Performing ROLLBACK';
            ROLLBACK TRANSACTION;
        END;
    SET @ErrorNumber = ERROR_NUMBER();
    SET @ErrorLine = ERROR_LINE();
    SET @ErrorMessage = 'Error executing  CopyPremiseToAnotherClient ' + ERROR_MESSAGE();
    SET @ErrorSeverity = ERROR_SEVERITY();
    SET @ErrorState = ERROR_STATE();
    RAISERROR(@ErrorMessage , @ErrorSeverity , @ErrorState);
    RETURN;
END CATCH;

SET NOCOUNT OFF;
--success
RETURN 0;
GO
