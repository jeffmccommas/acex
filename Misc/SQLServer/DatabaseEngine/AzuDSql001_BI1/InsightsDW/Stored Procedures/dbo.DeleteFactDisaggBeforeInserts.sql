SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
-- =======================================
-- Written by:	Wayne
-- Date:		07.20.15
-- Description: Deletes exiting fact rows in  FactBillDisagg
-- 				table before inserting new rows
-- =======================================
*/
Create PROCEDURE [dbo].[DeleteFactDisaggBeforeInserts]
(
    @ClientID       INT = 101
)
as
BEGIN
DECLARE @sqlstring VARCHAR(1000) ='
 DELETE insightsdw.dbo.FactBillDisagg
FROM   insightsdw.dbo.FactBillDisagg da
INNER JOIN 
(SELECT DISTINCT clientid, CustomerID, accountid, premiseid FROM BillDisaggTemp'+ CAST(@ClientID AS varchar(5))  + ' ) s
on da.ClientID=s.ClientID
AND da.CustomerID=s.CustomerID
AND da.AccountID=s.AccountID
AND da.PremiseID=s.PremiseID'
 EXECUTE (@sqlstring)
END


GO
