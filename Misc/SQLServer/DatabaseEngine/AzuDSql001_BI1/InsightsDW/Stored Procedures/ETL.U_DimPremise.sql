SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 4/21/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[U_DimPremise]
				@ClientID INT
AS

BEGIN

	SET NOCOUNT ON;

	UPDATE DimPremise
	SET PostalCodeKey = ISNULL(dpc.PostalCodeKey, -1), 
	    CityKey = ISNULL(dc.CityKey, -1), 
		SourceKey = ds.SourceKey, 
		Street1 = dpu.Street1, 
		Street2 = dpu.Street2, 
		City = dpu.City, 
		StateProvince = dpu.StateProvince, 
		Country = dpu.Country, 
		PostalCode = dpu.PostalCode, 
		--CreateDate = ISNULL(dp.CreateDate, GETUTCDATE()),
		GasService = dpu.GasService,
		ElectricService = dpu.ElectricService,
		WaterService = dpu.WaterService,
		UpdateDate = GETUTCDATE(),
		SourceId = dpu.SourceId, 
		ClientId = dpu.ClientId,
		ETL_LogId = dpu.ETL_LogId,
		IsInferred = 0
	FROM [InsightsDW].[dbo].[DimPremise] dp  WITH (NOLOCK)
	INNER JOIN [InsightsDW].[ETL].[T1U_DimPremise] dpu WITH (NOLOCK) ON dpu.ClientId = dp.ClientId 
														AND dpu.PremiseId = dp.PremiseId
														AND dpu.AccountId = dp.AccountId
	INNER JOIN [InsightsDW].[dbo].[DimClient] cl WITH (NOLOCK) ON cl.ClientId = dpu.ClientId 
	INNER JOIN [InsightsDW].[dbo].[DimSource] ds WITH (NOLOCK) ON ds.SourceId = dpu.SourceId 
	LEFT JOIN [InsightsDW].[dbo].[DimCountryRegion] dcr WITH (NOLOCK) ON dcr.CountryRegionCode = dpu.Country
	LEFT JOIN [InsightsDW].[dbo].[DimStateProvince] dsp WITH (NOLOCK) ON dsp.StateProvinceName = dpu.StateProvince
														AND dsp.CountryRegionKey = dcr.CountryRegionKey
	LEFT JOIN [InsightsDW].[dbo].[DimCity] dc WITH (NOLOCK) ON dc.City = dpu.City
													AND  dc.StateProvinceKey = dsp.StateProvinceKey
	LEFT JOIN [InsightsDW].[dbo].[DimPostalCode] dpc WITH (NOLOCK) ON dpc.PostalCode = dpu.PostalCode
													AND dpc.CityKey = dc.CityKey
	WHERE dp.ClientId = @ClientID AND dpu.PostalCode IS NOT NULL


END





GO
