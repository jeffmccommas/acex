SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
-- =======================================
-- Written by:	Wayne
-- Date:		07.20.15
-- Description: Drop the temp table used for Bill Disagg
-- 				caclulations
-- =======================================
*/
create PROCEDURE [dbo].[DropBillDisaggTempTable]
(
    @ClientID       INT = 101
)
as
BEGIN
DECLARE @sqlstring VARCHAR(2000)=
'
IF OBJECT_ID(''dbo.BillDisaggTemp' + CAST(@ClientID AS varchar(5)) + ''') IS NOT NULL
    DROP TABLE dbo.BillDisaggTemp' + CAST(@ClientID AS varchar(5)) 
exec (@sqlstring)
END

GO
