
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ubaid Tariq
-- Create date: 5/26/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_INF_FactBilling_DimServicePoint]
    @ETL_LogId INT ,
    @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #ServicePoints
            (
              PremiseKey INT ,
              CommodityId INT ,
              ServiceContractId VARCHAR(64) ,
              ServicePointId VARCHAR(64) NULL ,
              MeterId VARCHAR(64) NULL ,
              ReplacedMeterId VARCHAR(64) NULL ,
              SourceId INT ,
              PremiseId VARCHAR(50) ,
              TrackingId VARCHAR(50) ,
              TrackingDate DATETIME ,
              IsInferred BIT
            );

        CREATE NONCLUSTERED INDEX IX_SP ON #ServicePoints (PremiseKey, CommodityId, ServicePointId, MeterId );

        INSERT  INTO #ServicePoints
                SELECT  a.PremiseKey ,
                        a.CommodityId ,
                        a.ServiceContractId ,
                        a.ServicePointId ,
                        a.MeterId ,
                        a.ReplacedMeterId ,
                        a.SourceId ,
                        a.PremiseId ,
                        a.TrackingId ,
                        a.TrackingDate ,
                        a.IsInferred
                FROM    ( SELECT    p.PremiseKey ,
                                    CommodityId ,
                                    kfb.ServiceContractId ,
                                    kfb.ServicePointId ,
                                    kfb.MeterId ,
                                    kfb.ReplacedMeterId ,
                                    kfb.SourceId ,
                                    kfb.PremiseId ,
                                    kfb.TrackingId ,
                                    kfb.TrackingDate ,
                                    0 AS IsInferred ,
                                    ROW_NUMBER() OVER ( PARTITION BY p.PremiseKey,
                                                        kfb.CommodityId,
                                                        kfb.ServicePointId,
                                                        kfb.MeterId ORDER BY kfb.TrackingDate DESC ) AS SortId
                          FROM      ETL.KEY_FactBilling kfb
                                    INNER JOIN dbo.DimPremise p ON p.ClientId = kfb.ClientId
                                                              AND p.AccountId = kfb.AccountId
                                                              AND p.PremiseId = kfb.PremiseId
                          WHERE     kfb.ClientId = @ClientID
                        ) a
                WHERE   a.SortId = 1;

        UPDATE  sp
        SET     ServicePointId = dsp.ServicePointId
        FROM    #ServicePoints sp
                INNER JOIN dbo.DimServicePoint dsp ON dsp.ClientId = @ClientID
                                                      AND dsp.PremiseKey = sp.PremiseKey
                INNER JOIN dbo.DimMeter m ON m.ServicePointKey = dsp.ServicePointKey
                                             AND m.MeterId = ISNULL(sp.ReplacedMeterId,
                                                              sp.MeterId)
                                             AND m.CommodityKey = sp.CommodityId
        WHERE   sp.ServicePointId IS NULL;
       
        UPDATE  sp
        SET     ServicePointId = 'SP_' + MeterId ,
                IsInferred = 1
        FROM    #ServicePoints sp
        WHERE   ServicePointId IS NULL;

        INSERT  INTO [InsightsDW].[dbo].[DimServicePoint]
                ( PremiseKey ,
                  ServicePointId ,
                  ClientId ,
                  PremiseId ,
                  SourceId ,
                  TrackingId ,
                  TrackingDate ,
                  IsInferred
                )
                SELECT  sp.PremiseKey ,
                        sp.ServicePointId ,
                        @ClientID ,
                        sp.PremiseId ,
                        sp.SourceId ,
                        sp.TrackingId ,
                        sp.TrackingDate ,
                        sp.IsInferred
                FROM    ( SELECT    * ,
                                    ROW_NUMBER() OVER ( PARTITION BY PremiseKey,
                                                        ServicePointId ORDER BY TrackingDate DESC ) AS SortId
                          FROM      #ServicePoints
                        ) sp
                        LEFT	JOIN dbo.DimServicePoint dsp ON sp.SortId = 1
                                                              AND dsp.ClientId = @ClientID
                                                              AND dsp.PremiseKey = sp.PremiseKey
                                                              AND dsp.ServicePointId = sp.ServicePointId
                WHERE   dsp.ServicePointId IS NULL;
	
        WITH    ServiceContractServicePoint
                  AS ( SELECT   dsc.ServiceContractKey ,
                                dsp.ServicePointKey ,
                                ROW_NUMBER() OVER ( PARTITION BY dsc.PremiseKey,
                                                    dsc.ServiceContractId,
                                                    dsc.CommodityKey,
                                                    dsp.ServicePointId ORDER BY dsc.TrackingDate DESC ) AS SortId
                       FROM     #ServicePoints sp
                                INNER JOIN dbo.DimServicePoint dsp ON dsp.ClientId = @ClientID
                                                              AND sp.PremiseKey = dsp.PremiseKey
                                                              AND sp.ServicePointId = dsp.ServicePointId
                                INNER JOIN dbo.DimServiceContract dsc ON dsc.ClientId = @ClientID
                                                              AND dsc.PremiseKey = sp.PremiseKey
                                                              AND dsc.CommodityKey = sp.CommodityId
                                                              AND dsc.ServiceContractId = sp.ServiceContractId
                     )
            INSERT  INTO dbo.FactServicePoint
                    ( ServiceContractKey ,
                      ServicePointKey
                    )
                    SELECT  scsp.ServiceContractKey ,
                            scsp.ServicePointKey
                    FROM    ServiceContractServicePoint scsp
                            LEFT JOIN dbo.FactServicePoint fsp ON scsp.SortId = 1
                                                              AND fsp.ServiceContractKey = scsp.ServiceContractKey
                                                              AND fsp.ServicePointKey = scsp.ServicePointKey
                    WHERE   fsp.FactServicePointKey IS NULL;

        UPDATE  kfb
        SET     kfb.ServicePointId = sc.ServicePointId
        FROM    ETL.KEY_FactBilling kfb
                INNER JOIN dbo.DimPremise p ON p.ClientId = kfb.ClientId
                                               AND p.AccountId = kfb.AccountId
                                               AND p.PremiseId = kfb.PremiseId
                INNER JOIN #ServicePoints sc ON sc.PremiseKey = p.PremiseKey
                                                AND sc.CommodityId = kfb.CommodityId
                                                AND sc.MeterId = kfb.MeterId
        WHERE   kfb.ClientId = @ClientID
                AND kfb.ServicePointId IS NULL;

        DROP TABLE #ServicePoints;

    END;

														 
GO
