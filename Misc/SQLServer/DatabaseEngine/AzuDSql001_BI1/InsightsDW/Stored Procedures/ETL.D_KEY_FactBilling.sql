SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 9/10/2014
-- Description:	
-- =============================================
create PROCEDURE [ETL].[D_KEY_FactBilling]
				 @ClientID INT
AS

BEGIN

	SET NOCOUNT ON;

	DELETE FROM ETL.KEY_FactBilling WHERE ClientID = @ClientID

END





GO
