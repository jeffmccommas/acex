SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [esotest].[GetProfileAttributes] (@clientid INT,@premisekey INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
;WITH a AS(
SELECT p.premisekey,contentattributekey,value ,
ROW_NUMBER() OVER (PARTITION BY p.premisekey,contentattributekey ORDER BY contentattributekey) AS rownum
FROM factpremiseattribute f
INNER JOIN factcustomerpremise cp 
ON f.premisekey = cp.premisekey
INNER JOIN dimpremise p
ON p.premisekey = cp.premisekey
INNER JOIN dimcustomer c
ON c.customerkey = cp.customerkey
WHERE c.clientid = @clientid
AND cp.premisekey = @premisekey AND contentattributekey NOT LIKE '%paper%')
SELECT premisekey,contentattributekey,value  FROM a WHERE rownum = 1


END


GO
