SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

---

/*
-- =======================================
-- Written by:	Wayne
-- Date:		07/20/15
-- Decription:	Imports the Measure Savings rows from the dll
--				into a dynamically named temp table
--
-- =======================================
*/
CREATE PROCEDURE [dbo].[ImportMeasureSavingsToTemp]
(
    @detailRow [dbo].MeasureTableType READONLY,
    @clientid INT
)
AS
BEGIN
 
    SET NOCOUNT ON;

DECLARE @sqlstring NVARCHAR(2000)
SET @sqlstring=' 
    INSERT INTO  dbo.MeasureSavingsTemp' + CAST(@clientid AS VARCHAR(5)) 
+ ' (
      [ClientId]
     ,[CustomerId]
      ,[AccountId]
      ,[PremiseId]
	  --
      ,[ActionKey]
      ,[ActionTypeKey]
	  --
      ,[AnnualCost]
      ,[AnnualCostVariancePercent]
      ,[AnnualSavingsEstimate]
      ,[AnnualSavingsEstimateCurrencyKey]
	  --
      ,[Roi]
      ,[Payback]
      ,[UpfrontCost]
      ,[CommodityKey]
	  --
      ,[ElecSavEst]
      ,[ElecSavEstCurrencyKey]
      ,[ElecUsgSavEst]
      ,[ElecUsgSavEstUomKey]
	  --
      ,[GasSavEst]
      ,[GasSavEstCurrencyKey]
      ,[GasUsgSavEst]
      ,[GasUsgSavEstUomKey]
	  --
      ,[WaterSavEst]
      ,[WaterSavEstCurrencyKey]
      ,[WaterUsgSavEst]
      ,[WaterUsgSavEstUomKey]
	  --
      ,[Priority]
      ,[NewDate]

	  )
SELECT [ClientId]
      ,[CustomerId]
      ,[AccountId]
      ,[PremiseId]
	  --
      ,[ActionKey]
      ,[ActionTypeKey]
	  --
      ,[AnnualCost]
      ,[AnnualCostVariancePercent]
      ,[AnnualSavingsEstimate]
      ,[AnnualSavingsEstimateCurrencyKey]
	  --
      ,[Roi]
      ,[Payback]
      ,[UpfrontCost]
      ,[CommodityKey]
	  --
      ,[ElecSavEst]
      ,[ElecSavEstCurrencyKey]
      ,[ElecUsgSavEst]
      ,[ElecUsgSavEstUomKey]
	  --
      ,[GasSavEst]
      ,[GasSavEstCurrencyKey]
      ,[GasUsgSavEst]
      ,[GasUsgSavEstUomKey]
	  --
      ,[WaterSavEst]
      ,[WaterSavEstCurrencyKey]
      ,[WaterUsgSavEst]
      ,[WaterUsgSavEstUomKey]
	  --
      ,[Priority]
      ,[NewDate]
            FROM  @mydetailRow d 
		  WHERE d.clientid=' + CAST(@clientid AS VARCHAR(5)) 
		 
 

EXEC sp_executesql @sqlstring, N'@mydetailRow [dbo].[MeasureTableType] READONLY', @mydetailRow=@detailRow;

--EXEC (@sqlstring)
END


GO
