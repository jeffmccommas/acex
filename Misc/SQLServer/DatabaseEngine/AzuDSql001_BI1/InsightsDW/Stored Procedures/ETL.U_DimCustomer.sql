SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 4/17/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[U_DimCustomer]
				@ClientID INT
AS

BEGIN

	SET NOCOUNT ON;


	UPDATE DimCustomer 
	SET PostalCodeKey = ISNULL(dpc.PostalCodeKey, -1), 
		CustomerAuthenticationTypeKey = dcat.CustomerAuthenticationTypeKey, 
		SourceKey = ds.SourceKey, 
		CityKey = ISNULL(dc.CityKey, -1), 
		FirstName = c.FirstName, 
		LastName = c.LastName, 
		Street1 = c.Street1, 
		Street2 = c.Street2, 
		City = c.City, 
		StateProvince = c.StateProvince, 
		Country = c.Country, 
		PostalCode = c.PostalCode, 
		PhoneNumber = c.PhoneNumber, 
		MobilePhoneNumber = c.MobilePhoneNumber, 			
		EmailAddress = c.EmailAddress, 
		AlternateEmailAddress = c.AlternateEmailAddress,  
		--CreateDate = ISNULL(c.CreateDate, GETUTCDATE()),
		UpdateDate = GETUTCDATE(),
		SourceId = c.SourceId, 
		AuthenticationTypeId = c.AuthenticationTypeId,
		ETL_LogId = c.ETL_LogId,
		IsInferred = 0
	FROM [InsightsDW].[dbo].[DimCustomer] cd  WITH (NOLOCK)
	INNER JOIN [InsightsDW].[ETL].[T1U_DimCustomer] c WITH (NOLOCK) ON cd.ClientId = c.ClientId 
														AND cd.CustomerId = c.CustomerId
	INNER JOIN [InsightsDW].[dbo].[DimClient] cl WITH (NOLOCK) ON cl.ClientId = c.ClientId 
	INNER JOIN [InsightsDW].[dbo].[DimCustomerAuthenticationType] dcat WITH (NOLOCK) ON dcat.CustomerAuthenticationTypeId = c.AuthenticationTypeId 
	INNER JOIN [InsightsDW].[dbo].[DimSource] ds WITH (NOLOCK) ON ds.SourceId = c.SourceId 
	LEFT JOIN [InsightsDW].[dbo].[DimCountryRegion] dcr ON dcr.CountryRegionCode = c.Country
	LEFT JOIN [InsightsDW].[dbo].[DimStateProvince] dsp ON dsp.StateProvinceName = c.StateProvince
														AND dsp.CountryRegionKey = dcr.CountryRegionKey
	LEFT JOIN [InsightsDW].[dbo].[DimCity] dc ON dc.City = c.City
													AND dc.StateProvinceKey = dsp.StateProvinceKey
	LEFT JOIN [InsightsDW].[dbo].[DimPostalCode] dpc ON dpc.PostalCode = c.PostalCode
													AND dpc.CityKey = dc.CityKey
	WHERE cd.ClientID = @ClientID AND c.PostalCode IS NOT NULL

END





GO
