SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Ubaid Tariq
-- Create date: 7/20/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_FactActionData] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;


        INSERT  INTO dbo.factActionData
                ( ActionDetailKey ,
                  ActionDataKey ,
                  ActionDataValue
                )
                SELECT  fa.ActionDetailKey ,
                        ai.ActionData ,
                        ai.ActionDataValue
                FROM    [InsightsDW].[ETL].[INS_FactAction] efa
                        INNER JOIN [InsightsDW].[dbo].[factAction] fa ON fa.ClientID = efa.ClientID
                                                              AND fa.PremiseID = efa.PremiseID
                                                              AND fa.AccountID = efa.AccountID
                                                              AND fa.ActionKey = efa.ActionKey
                                                              AND fa.SubActionKey = efa.SubActionKey
                                                              AND fa.StatusKey = efa.StatusId
                                                              AND fa.StatusDate = efa.StatusDate
                                                              AND fa.CreateDate = efa.CreateDate
                        INNER JOIN InsightsBulk.Holding.ActionItem ai ON ai.ClientId = fa.ClientID
                                                              AND ai.PremiseId = fa.PremiseID
                                                              AND ai.AccountID = fa.AccountID
                                                              AND ai.ActionKey = fa.ActionKey
                                                              AND ai.SubActionKey = fa.SubActionKey
                                                              AND ai.StatusId = fa.StatusKey
                                                              AND ai.StatusDate = fa.StatusDate
                WHERE   efa.ClientID = @ClientID
                        AND ActionData IS NOT NULL;




    END;


														



GO
