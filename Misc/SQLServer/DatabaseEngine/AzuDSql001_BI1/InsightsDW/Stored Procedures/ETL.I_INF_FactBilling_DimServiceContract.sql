
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Ubaid Tariq
-- Create date: 5/26/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_INF_FactBilling_DimServiceContract]
    @ETL_LogId INT ,
    @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #ServiceContracts
            (
              PremiseKey INT ,
              PremiseId VARCHAR(50) ,
              CommodityId INT ,
              CommodityDesc VARCHAR(20) NULL ,
              ServiceContractId VARCHAR(64) ,
              AMIStartDate DATETIME NULL ,
              AMIEndDate DATETIME NULL ,
              TrackingId VARCHAR(50) ,
              TrackingDate DATETIME ,
              IsInferred BIT ,
              SortId BIGINT
            );

        CREATE NONCLUSTERED INDEX IX_SC ON #ServiceContracts (PremiseKey, CommodityId );

        INSERT  INTO #ServiceContracts
                SELECT  *
                FROM    ( SELECT    p.PremiseKey ,
                                    kfb.PremiseId ,
                                    kfb.CommodityId ,
                                    dc.CommodityDesc ,
                                    kfb.ServiceContractId ,
                                    kfb.AMIStartDate ,
                                    kfb.AMIEndDate ,
                                    kfb.TrackingId ,
                                    kfb.TrackingDate ,
                                    0 AS IsInferred ,
                                    ROW_NUMBER() OVER ( PARTITION BY p.PremiseKey,
                                                        kfb.CommodityId,
                                                        kfb.ServiceContractId ORDER BY kfb.TrackingDate DESC ) AS SortId
                          FROM      ETL.KEY_FactBilling kfb
                                    INNER JOIN dbo.DimCommodity dc ON dc.CommodityId = kfb.CommodityId
                                    INNER JOIN dbo.DimPremise p ON p.ClientId = kfb.ClientId
                                                              AND p.AccountId = kfb.AccountId
                                                              AND p.PremiseId = kfb.PremiseId
                          WHERE     kfb.ClientId = @ClientID
                        ) a
                WHERE   a.SortId = 1;
        
        UPDATE  kfb
        SET     ServiceContractId = 'SC_' + PremiseId + '_' + CommodityDesc ,
                IsInferred = 1
        FROM    #ServiceContracts kfb
        WHERE   ServiceContractId = '';

        UPDATE  sc
        SET     ActiveDate = c.AMIStartDate ,
                InActiveDate = c.AMIEndDate
        FROM    #ServiceContracts c
                INNER JOIN dbo.DimServiceContract sc ON sc.ClientId = @ClientID
                                                        AND sc.PremiseKey = c.PremiseKey
                                                        AND sc.[CommodityKey] = c.CommodityId
                                                        AND sc.ServiceContractId = c.ServiceContractId
        WHERE   c.AMIStartDate IS NOT NULL
                OR c.AMIEndDate IS NOT NULL;

        INSERT  INTO [dbo].[DimServiceContract]
                ( [PremiseKey] ,
                  [CommodityKey] ,
                  [ClientId] ,
                  [ServiceContractId] ,
                  [ServiceContractTypeKey] ,
                  [ActiveDate] ,
                  [InActiveDate] ,
                  [IsInferred] ,
                  [TrackingId] ,
                  [TrackingDate]
                )
                SELECT  c.PremiseKey ,
                        c.CommodityId ,
                        @ClientID ,
                        c.ServiceContractId ,
                        sct.SecviceContractTypeKey ,
                        c.AMIStartDate ,
                        c.AMIEndDate ,
                        c.IsInferred ,
                        c.TrackingId ,
                        c.TrackingDate
                FROM    #ServiceContracts c
                        INNER	 JOIN dbo.DimCommodity dc ON dc.CommodityKey = c.CommodityId
                        INNER	 JOIN [dbo].[DimServiceContractType] sct ON sct.[SecviceContractTypeName] = dc.CommodityDesc
                                                              + '_total'
                        LEFT JOIN dbo.DimServiceContract sc ON sc.ClientId = @ClientID
                                                              AND sc.PremiseKey = c.PremiseKey
                                                              AND sc.[CommodityKey] = c.CommodityId
                                                              AND sc.ServiceContractId = c.ServiceContractId
                WHERE   sc.ServiceContractId IS NULL;

        UPDATE  kfb
        SET     kfb.ServiceContractId = sc.ServiceContractId
        FROM    ETL.KEY_FactBilling kfb
                INNER JOIN dbo.DimPremise p ON p.ClientId = kfb.ClientId
                                               AND p.AccountId = kfb.AccountId
                                               AND p.PremiseId = kfb.PremiseId
                INNER JOIN #ServiceContracts sc ON sc.PremiseKey = p.PremiseKey
                                                   AND sc.CommodityId = kfb.CommodityId
        WHERE   kfb.ClientId = @ClientID
                AND kfb.ServiceContractId = '';

        DROP TABLE #ServiceContracts;

    END;

														 


GO
