SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE PROCEDURE [esotest].[UpdateESOMeasureSavings]
 AS
/**********************************************************************************************************
* SP Name:
*		holding.InsertBillingdata
* Parameters:
*		CustomerHoldingTable			BillingTableType
* Purpose:	This stored procedure insertsrows into the billing holding tables
*	
*	
*	
*	
*              
*
*              
*
**********************************************************************************************************/
BEGIN
	SET NOCOUNT ON

	UPDATE esotest.esomeasureSavings
	SET actionkey = xref.DWMeasureName
	FROM esotest.ESOMeasureSavings ms
	INNER JOIN esotest.MeasureCrossReference xref
	ON  ms.esomeasureid = xref.MeasureId
	
	UPDATE esotest.esomeasureSavings
	SET reportcostsavings = ps.annualsavingsestimate,
	reportusagesavings = ps.AnnualUsageSavingsestimate
	FROM esotest.ESOMeasureSavings ms
	INNER JOIN esotest.MeasureCrossReference xref
	ON  ms.esomeasureid = xref.MeasureId
	INNER JOIN dbo.premisesavings ps
	ON ps.premisekey = ms.premisekey
	AND ps.ActionKey = xref.DWMeasureName


END --proc






























GO
