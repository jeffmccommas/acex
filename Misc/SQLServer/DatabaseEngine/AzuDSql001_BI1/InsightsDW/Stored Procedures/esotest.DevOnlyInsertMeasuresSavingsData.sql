SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esotest].[DevOnlyInsertMeasuresSavingsData]
 (@ESOTbl AS [esotest].[ESOMeasureSavingsTableType] READONLY)
AS
/**********************************************************************************************************
* SP Name:
*		holding.InsertBillingdata
* Parameters:
*		CustomerHoldingTable			BillingTableType
* Purpose:	This stored procedure insertsrows into the billing holding tables
*	
*	
*	
*	
*              
*
*              
*
**********************************************************************************************************/
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO esotest.esomeasureSavings (clientid, customerid, account, premise, premisekey, esomeasureid, annualbill, houseTotalArea, housepeople, heatsystemStyle, waterheaterfuel, heatsystemfuel, poolcount, hottubcount, esosavings, esocost,esoid,actionkey)

	SELECT clientid, customerid, account, premise, premisekey,esomeasureid, annualbill, houseTotalArea, housepeople, heatsystemStyle, waterheaterfuel, heatsystemfuel, poolcount, hottubcount,  esosavings, esocost,esoid,actionkey
	FROM @ESOTbl 



END --proc



























GO
