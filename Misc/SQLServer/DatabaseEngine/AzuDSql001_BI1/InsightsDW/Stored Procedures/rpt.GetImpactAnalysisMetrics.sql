/****** Object:  StoredProcedure [rpt].[getImpactAnalysisMetrics]    Script Date: 5/13/2016 12:39:27 PM ******/
DROP PROCEDURE [rpt].[getImpactAnalysisMetrics]
GO

/****** Object:  StoredProcedure [rpt].[getImpactAnalysisMetrics]    Script Date: 5/13/2016 12:39:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create procedure [rpt].[getImpactAnalysisMetrics] @clientID int, @startDateBaseline date, @endDateBaseline date, @startDateCurrent date, @endDateCurrent date, @reportView varchar(50),@program varchar(200)
as

set nocount on

create table #AllEvents
(clientID int not null,
customerKey int not null,
premisekey int not null,
accountID varchar(50),
premiseID varchar(50) not null,
eventTypeID int not null,
flagDateCreated datetime,
flagDateUpdated datetime,
FullDateAlternateKey date not null,
monthName varchar(50),
eventMonth varchar(15),
groupName varchar(50),
groupSort int,
bInclude int,
segment varchar(50),
program varchar(200),
emailID varchar(25),
isNewOrReturn varchar(5)) -- 5/13/2016 - TFS 3319 Break out Web Visits by New/Unique and Return
create clustered index idx_AllEvents on #AllEvents (clientID, CustomerKey, PremiseKey, premiseID, FullDateAlternateKey)

create table #ClientEventFilter 
(ClientID int,
EventTypeID int,
groupname varchar(50),
groupSort int)

declare @monthcount int
select @monthCount = (select datediff(m,@startDateBaseline,@endDateBaseline))

if @startDateCurrent is null 
begin
	select @startDateCurrent = @endDateBaseline
end

if @endDateCurrent is null 
begin
	select @endDateCurrent = getdate()
end


insert into #ClientEventFilter (clientID, eventtypeid)
select distinct clientid, eventtypeid
from factevent
where clientID = @clientID

update #ClientEventFilter
set groupname = case 
		when t1.EventTypeID in (1,14,15,16,17,18,19,20,21,37,38,39,41,42,44,45,46,47,48,49,50) then 'Click'  -- add events 44 - 49 for UIL March Drop
		when t1.EventTypeID in (4) then 'Open'
		when t1.EventTypeID in (11,23,24,26) then 'Hard Bounce'  
		when t1.EventTypeID in (22,25) then 'Soft Bounce'  
		when t1.EventTypeID in (8,9,10,27,28,29,30,31,32,33) then 'Web Visits'
		when t1.EventTypeID in (2,6) then 'Impacts'
		when t1.EventTypeID in (7) then 'Sent Report'
		when t1.EventTypeID in (5,12,13,34,35,36,40,43) then 'Opt Out'
		when t1.EventTypeID = 0 then 'Unknown'
		else EventShortName
		end,
groupSort = case 
		when t1.EventTypeID in (1,14,15,16,17,18,19,20,21,37,38,39,41,42,44,45,46,47,48,49,50) then 6 
		when t1.EventTypeID in (4) then 7
		when t1.EventTypeID in (11,23,24,26) then 3  
		when t1.EventTypeID in (22,25) then 2  
		when t1.EventTypeID in (8,9,10,27,28,29,30,31,32,33) then 9
		when t1.EventTypeID in (2,6) then 10
		when t1.EventTypeID in (7) then 1
		when t1.EventTypeID in (5,12,13,34,35,36,40,43) then 4
		when t1.EventTypeID = 0 then 0
		else 0
		end
from #ClientEventFilter t1 join dimeventtype t2
	on t1.eventtypeid = t2.eventtypeid

;with AllParticipants
	as
		( select distinct [premise].[ClientID],[premise].[PremiseKey], [premise].[PremiseId], [group].[GroupId] as segment
				from
					[DimPremise] as [premise]
					inner join
						(
							select
								[PremiseKey] as [PremiseKey], 
								[Value] as [GroupId],
								row_number() over (partition by PremiseKey, ContentAttributeKey order by EffectiveDate desc) as [rank]
								from
									[dbo].[FactPremiseAttribute]
								where
									ContentAttributeKey like @program + '%groupnumber'
						) as [group] on
						[group].[PremiseKey] = [premise].[PremiseKey]
				where	
					[group].[rank] = 1
		)
insert into #AllEvents
	select distinct t1.clientID, t1.customerkey, t1.premisekey, t3.accountid,t3.premiseID,
			t1.eventtypeID, t1.flagdatecreated, t1.flagdateupdated, t2.FullDateAlternateKey,t2.monthName, 
				substring(convert(varchar(15),fullDateAlternateKey),1,7) as eventMonth,
			t4.groupname, t4.groupsort, case when t1.eventtypeid = 12 then 0 else 1 end,
			t5.segment, @program, t1.emailID, 'N'
	from dbo.factevent t1 join dbo.dimdate t2
				on t1.datekey = t2.datekey 
			join dbo.dimPremise t3 on t1.premisekey = t3.premisekey
				join #ClientEventFilter t4
					on t1.eventtypeid = t4.eventtypeid  
			join AllParticipants t5
				on t3.clientID = t5.clientID
				and t3.premiseKey = t5.premiseKey
			where t1.clientID = @clientID

-- 5/13/2016 - TFS 3319 Break out Web Visits by New/Unique and Return
-- if the count of premisekey for an event is > 1, then there are return user events.  Set the flag as return for all the events AFTER the first instance of the event (because the first one would count as new)
;with newreturn
as
(
	select premisekey, EventTypeID, segment, count(premisekey) as premisecount, 
	'R' as UserStatus,
	min(flagdateUpdated) as minflagdateupdated
	from #AllEvents
	group by premisekey,EventTypeID, segment
	having count(premisekey) > 1
)
update #AllEvents
set isNewOrReturn = UserStatus
from #AllEvents t1 join newreturn t2
	on t1.premisekey = t2.premisekey
	and t1.EventTypeID = t2.EventTypeID
	and t1.segment = t2.segment
	and t1.flagdateUpdated > minflagdateupdated


if @reportView = 'FullUserSet'
begin

	select t2.clientID,t2.customerKey, t2.premisekey,t5.customerID,t2.accountID,t2.premiseID,t2.eventTypeID,t2.flagDateCreated,
	t2.flagDateUpdated,t2.FullDateAlternateKey,t2.monthName,t2.eventMonth,t2.groupName,t2.groupSort,
	t2.bInclude,t2.segment,t2.program,t3.EventShortName,t2.EmailID, 
	t5.FirstName as CustomerFirstName, t5.LastName as CustomerLastName,  t5.Street1 as CustomerStreet1, t5.Street2 as CustomerStreet2, t5.City as CustomerCity, t5.StateProvince as CustomerStateProvince, t5.PostalCode as CustomerPostalCode,
	t6.Street1 as PremiseStreet1, t6.Street2 as PremiseStreet2, t6.City as PremiseCity, t6.StateProvince as PremiseStateProvince, t6.PostalCode as PremisePostalCode,
	t5.EmailAddress as CustomerEmailAddress, t5.PhoneNumber as CustomerPhone,isNewOrReturn
	from #AllEvents t2 join dbo.DimEventType t3
		on t2.EventtypeID = t3.eventTypeID
	join factcustomerpremise t4
		on t2.customerkey = t4.customerKey
		and t2.premisekey = t4.premiseKey
	join dimCustomer t5
		on t4.customerkey = t5.customerkey
	join dimPremise t6
		on t4.PremiseKey = t6.PremiseKey
	order by t2.segment, t5.customerid, t2.accountid, t2.premiseid, t3.EventShortName, t2.FullDateAlternateKey,t2.EmailID

end

if @reportView = 'MonthlyEvents'
begin	
	select segment, t3.EventtypeID,t3.EventShortName , count(distinct t2.premiseid) as count, 
	'pre ' + convert(varchar(5), year(@endDateBaseline)) + '-' + convert(varchar(5), month(@endDateBaseline)) as timeperiod, 
		convert(varchar(5), year(@startDateBaseline)) + '-' + convert(varchar(5), month(@startDateBaseline)) as SortOrder, 
			groupname
				from #AllEvents t2 join dbo.DimEventType t3
				on t2.EventtypeID = t3.eventTypeID
				where convert(DATETIME2,FlagDateUpdated)  > @startDateBaseline and convert(DATETIME2,FlagDateUpdated) < @endDateBaseline
				--and bInclude = 1
				group by segment,t3.EventShortName,t3.EventtypeID,groupname
							UNION
				select segment, t3.EventtypeID,t3.EventShortName , count(distinct t2.premiseid) as count, t2.eventMonth as timeperiod, t2.eventmonth as SortOrder, groupname
				from #AllEvents t2 join dbo.DimEventType t3
				on t2.EventtypeID = t3.eventTypeID
				where convert(DATETIME2,FlagDateUpdated)  > @startDateCurrent
				--and bInclude = 1
				group by segment,t3.EventShortName,t3.EventtypeID,t2.eventMonth, groupname
			order by sortorder, segment, groupname,t3.EventShortName,t3.EventtypeID
end

if @reportView = 'BaselineOnly'
begin	
	select segment, t3.EventtypeID,t3.EventShortName , count(distinct t2.premiseid) as count, 
	'pre ' + convert(varchar(5), year(@endDateBaseline)) + '-' + convert(varchar(5), month(@endDateBaseline)) as timeperiod, 
		convert(varchar(5), year(@startDateBaseline)) + '-' + convert(varchar(5), month(@startDateBaseline)) as SortOrder, 
			groupname
				from #AllEvents t2 join dbo.DimEventType t3
				on t2.EventtypeID = t3.eventTypeID
				where convert(DATETIME2,FlagDateUpdated)  > @startDateBaseline and convert(DATETIME2,FlagDateUpdated) < @endDateBaseline
				--and bInclude = 1
				group by segment,t3.EventShortName,t3.EventtypeID,groupname
		order by sortorder, segment, groupname,t3.EventShortName,t3.EventtypeID
end

if @reportView = 'BaselineMonthlyAvgOnly'
begin
			select segment, t3.EventtypeID,t3.EventShortName , count(distinct t2.premiseid)/@monthcount as count, 
			'pre ' + convert(varchar(5), year(@endDateBaseline)) + '-' + convert(varchar(5), month(@endDateBaseline)) + 'Avg' as timeperiod, 
			convert(varchar(5), year(@startDateBaseline)) + '-' + convert(varchar(5), month(@startDateBaseline)) as SortOrder, 
			groupname
				from #AllEvents t2 join dbo.DimEventType t3
				on t2.EventtypeID = t3.eventTypeID
				where convert(DATETIME2,FlagDateUpdated)  > @startDateBaseline and convert(DATETIME2,FlagDateUpdated) < @endDateBaseline
				--and bInclude = 1
				group by segment,t3.EventShortName,t3.EventtypeID,groupname
		order by sortorder, segment, groupname,t3.EventShortName,t3.EventtypeID
end			

if @reportView = 'ProgramOnly'
begin	
	select segment, t3.EventtypeID,t3.EventShortName , count(distinct t2.premiseid) as count, t2.eventMonth as timeperiod, t2.eventmonth as SortOrder, groupname
		from #AllEvents t2 join dbo.DimEventType t3
		on t2.EventtypeID = t3.eventTypeID
		where convert(DATETIME2,FlagDateUpdated)  > @startDateCurrent
		--and bInclude = 1
		group by segment,t3.EventShortName,t3.EventtypeID,t2.eventMonth, groupname
		order by sortorder, segment, groupname,t3.EventShortName,t3.EventtypeID
end
 
if @reportView = 'Cumulative'
begin	
	select segment, count(distinct t2.premiseid) as count, groupname
		from #AllEvents t2 join dbo.DimEventType t3
		on t2.EventtypeID = t3.eventTypeID
		where convert(DATETIME2,FlagDateUpdated)  > @startDateCurrent
		--and bInclude = 1
		group by segment, groupname
		order by segment, groupname
end

set nocount off

GO


