
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Lydia Lefkowitz
-- Create date: 8/27/2014
-- Description:	Used in webapi Bill Controller Get V1
-- Revisions
-- 8/28/2014  LRL   Changed to separate resultsets for customer, accounts, premises, bills
-- 8/29/2014  LRL	Separated out the BillService Model to its own resultset and changed blank values that are decimal to 0 for Entity Framework
-- 9/2/2014	  LRL	Units to UOMKey on rename similar to CommodityKey where relying on content, 
					--totalamount etc is now hardcoded to 0.0 until we know where this is stored, totalcost is costofusage and totalunits is totalserviceuse
-- 9/5/2014   LRL   Added totalamount which is grouped by account and year/month for a billfrequency for that customer. Have no place being stored currently so this is temporary solution to sum all together
-- 9/8/2014   LRL   Took out service id at top bill level
-- 9/10/2014  LRL   Format bill date , starttime and endtime to not include time. Bill date at top level is max of all bills for that month
-- 9/10/2014  LRL   Standardize date format to mm-dd-yyyy
-- 9/12/2014  LRL   MaxDate on the bill service added, correction on join on enddate
-- =============================================
-- Author:		Unknown
-- Checkkin by:	Wayne
-- Create date: 8/4/2015
-- Description:	checking in/deploy someones change
--				that addedd a check for null to AVgTemp -    ISNULL(AvgTemp,0.00) AS AvgTemp 
-- =============================================
CREATE PROCEDURE [dbo].[GetBillSummary]
    @ClientID AS INT ,
    @CustomerID AS VARCHAR(50) ,
    @AccountID AS VARCHAR(50) = NULL ,
    @StartDate AS DATE ,
    @EndDate AS DATE ,
    @Count AS INT = 1
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @defaultZipCode AS VARCHAR(9);

        EXEC InsightsMetadata.[cm].[uspSelectContentClientConfiguration] @ClientID = @ClientID,
            @configurationKey = N'defaultzipcode',
            @configurationValue = @defaultZipCode OUTPUT;

		-- Bill Customer
        SELECT  c.CustomerId AS Id ,
                c.FirstName ,
                c.LastName
        FROM    dbo.DimCustomer c WITH ( NOLOCK )
                INNER JOIN dbo.DimClient r WITH ( NOLOCK ) ON r.ClientKey = c.ClientKey
        WHERE   r.ClientId = @ClientID
                AND c.CustomerId = @CustomerID;

        SELECT	DISTINCT
                p.PremiseKey ,
                p.AccountId ,
                p.PremiseId ,
                SUBSTRING(ISNULL(p.PostalCode, @defaultZipCode), 1, 5) AS ZipCode
        INTO    #CustomerInfo
        FROM    dbo.DimPremise p WITH ( NOLOCK )
                INNER JOIN dbo.FactCustomerPremise cp WITH ( NOLOCK ) ON cp.PremiseKey = p.PremiseKey
                INNER JOIN dbo.DimCustomer c WITH ( NOLOCK ) ON c.CustomerKey = cp.CustomerKey
                INNER JOIN dbo.DimClient r WITH ( NOLOCK ) ON r.ClientKey = c.ClientKey
        WHERE   r.ClientId = @ClientID
                AND c.CustomerId = @CustomerID
                AND ( @AccountID IS NULL
                      OR p.AccountId = @AccountID
                    );

		-- Bill Account
        SELECT	DISTINCT
                AccountId AS Id
        FROM    #CustomerInfo;

		--insert calculation of total bill amount into the temp table
	
        SELECT  *
        INTO    #AccountBillAmounts
        FROM    ( SELECT    c.AccountId ,
                            fb.PremiseId ,
                            sc.ServiceContractId AS ServiceId ,
                            CONVERT(DATE, fb.EndDate) AS BillDate ,
                            CONVERT(DATE, DATEADD(DAY, -1 * fb.BillDays,
                                                  ISNULL(fb.ReadDate,
                                                         fb.EndDate))) AS BillStartDate ,
                            CONVERT(DATE, ISNULL(fb.ReadDate, fb.EndDate)) AS BillEndDate ,
                            fb.DueDate AS BillDueDate ,
                            f.CommodityDesc AS CommodityKey ,
                            uom.UOMDesc AS UOMKey ,
                            fb.BillDays ,
                            fb.TotalUsage AS TotalServiceUse ,
                            fb.CostOfUsage ,
                            ISNULL(fb.OtherCost, 0.00) AS AdditionalServiceCost ,
                            c.ZipCode ,
                            bpt.BillPeriodTypeDesc AS BillFrequency ,
                            ROW_NUMBER() OVER ( PARTITION BY fb.ServiceContractKey ORDER BY d.FullDateAlternateKey DESC ) AS RowIndex
                  FROM      #CustomerInfo c WITH ( NOLOCK )
                            INNER JOIN dbo.DimServiceContract sc WITH ( NOLOCK ) ON sc.PremiseKey = c.PremiseKey
                            INNER JOIN dbo.FactServicePointBilling fb WITH ( NOLOCK ) ON fb.ServiceContractKey = sc.ServiceContractKey
                            INNER JOIN dbo.DimDate d WITH ( NOLOCK ) ON d.DateKey = fb.BillPeriodEndDateKey
                            INNER JOIN dbo.DimBillPeriodType bpt WITH ( NOLOCK ) ON bpt.BillPeriodTypeKey = fb.BillPeriodTypeKey
                            INNER JOIN dbo.DimUOM uom WITH ( NOLOCK ) ON fb.UOMKey = uom.UOMKey
                            INNER JOIN dbo.DimCommodity f WITH ( NOLOCK ) ON f.CommodityKey = sc.CommodityKey
                  WHERE     d.FullDateAlternateKey BETWEEN @StartDate
                                                   AND     @EndDate
                ) AS Bill
        WHERE   Bill.RowIndex <= @Count
        ORDER BY Bill.AccountId ,
                Bill.PremiseId ,
                Bill.ServiceId ,
                Bill.BillEndDate;
    	 
		-- Bill - doing a sum of totalcost for account and year/month of the bill date. Max bill date is the latest bill date for this account on the year/month
        SELECT  AccountId ,
                PremiseId ,
                FORMAT(BillDate, 'yyyy-MM-dd') AS BillDate ,
                FORMAT(MIN(BillDueDate), 'yyyy-MM-dd') AS BillDueDate ,
                SUM(CostOfUsage) AS TotalAmount ,
                BillFrequency ,
                SUM(AdditionalServiceCost) AS AdditionalBillCost
        FROM    #AccountBillAmounts
        GROUP BY AccountId ,
                PremiseId ,
                BillDate ,
                BillFrequency
        ORDER BY AccountId ,
                PremiseId ,
                BillDate DESC ,
                BillFrequency;

		---- Bill Premise 
		----NOTE that even if startdate and end date don't have billing entries for this premise - 
		----it will still bring back the information for a premise and account for this customer
        SELECT  p.AccountId ,
                p.PremiseId ,
                p.Street1 AS Addr1 , --Addr1
                p.Street2 AS Addr2 , --Addr2
                p.City ,
                p.StateProvince AS State ,
                p.PostalCode AS Zip
        FROM    dbo.DimPremise p WITH ( NOLOCK )
                INNER JOIN #CustomerInfo c ON c.PremiseKey = p.PremiseKey;

        WITH    Temps
                  AS ( SELECT   ba.AccountId ,
                                ba.PremiseId ,
                                ba.ServiceId ,
                                ba.BillDate ,
                                AVG(wd.AvgTemp) AS AvgTemp
                       FROM     #AccountBillAmounts ba
                                LEFT JOIN InsightsMetadata.cm.EMZipcode zc ON zc.ZipCode = ba.ZipCode
                                LEFT	JOIN InsightsMetadata.cm.EMDailyWeatherDetail wd ON wd.StationID = zc.StationIdDaily
                                                              AND wd.WeatherReadingDate >= ba.BillStartDate
                                                              AND wd.WeatherReadingDate < ba.BillEndDate
                       GROUP BY ba.AccountId ,
                                ba.PremiseId ,
                                ba.ServiceId ,
                                ba.BillDate
                     )
            ---- Bill Service
    SELECT  ba.AccountId ,
            ba.PremiseId ,
            ba.ServiceId ,
            FORMAT(ba.BillDate, 'yyyy-MM-dd') AS BillDate ,
            FORMAT(ba.BillStartDate, 'yyyy-MM-dd') AS BillStartDate ,
            FORMAT(ba.BillEndDate, 'yyyy-MM-dd') AS BillEndDate ,
            CommodityKey ,
            UOMKey ,
            BillDays ,
            TotalServiceUse ,
            CostOfUsage ,
            AdditionalServiceCost ,
             ISNULL(AvgTemp,0.00) AS AvgTemp ,
            ROW_NUMBER() OVER ( ORDER BY ba.AccountId, ba.PremiseId ) AS RowIdentifier ,
            RowIndex
    FROM    #AccountBillAmounts ba
            INNER JOIN Temps t ON t.AccountId = ba.AccountId
                                  AND t.PremiseId = ba.PremiseId
                                  AND t.ServiceId = ba.ServiceId
                                  AND t.BillDate = ba.BillDate
    ORDER BY ba.AccountId ,
            ba.PremiseId ,
            ba.BillDate DESC ,
            ba.CommodityKey;

        DROP TABLE #AccountBillAmounts;
        DROP TABLE #CustomerInfo;
    END;







GO
