SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 9/19/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_INF_FactPremiseAttribute_DimCustomer]
				 @ETL_LogId INT,
				 @ClientID INT

AS

BEGIN

		SET NOCOUNT ON;

		INSERT INTO [InsightsDW].[dbo].[DimCustomer] (
							ClientKey, 
							ClientId, 
							PostalCodeKey, 
							CustomerAuthenticationTypeKey, 
							SourceKey, 
							CityKey, 
							CustomerId, 
							FirstName, 
							LastName, 
							Street1, 
							Street2, 
							City, 
							StateProvince, 
							Country, 
							PostalCode, 
							PhoneNumber, 
							MobilePhoneNumber, 
							EmailAddress, 
							AlternateEmailAddress, 
							CreateDate, 
							SourceId, 
							AuthenticationTypeId,
							ETL_LogId,
							IsInferred,
							TrackingId,
							TrackingDate)
			SELECT  ClientKey,
			ClientId,
			-1,
			1,
			SourceKey,
			-1,
			CustomerId,
			NULL, 
			NULL,
			NULL,  
			NULL, 
			NULL, 
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			GETUTCDATE(),
			SourceId,
			1,
			@ETL_LogId,
			1,
			GETUTCDATE(),
			GETUTCDATE()
		FROM
		(SELECT  kfpa.ClientId,
				dcl.ClientKey,
				kfpa.CustomerId,
				ds.SourceKey,
				kfpa.SourceID,
				ROW_NUMBER() OVER ( PARTITION BY 
				 kfpa.ClientId,  kfpa.CustomerId
				ORDER BY  kfpa.SourceID) AS SortId 
		FROM ETL.KEY_FactPremiseAttribute kfpa WITH (NOLOCK)
		INNER JOIN dbo.DimClient dcl WITH (NOLOCK) ON dcl.ClientId = kfpa.ClientId	
		INNER JOIN dbo.DimSource ds WITH (NOLOCK) ON ds.SourceId = kfpa.SourceId
		LEFT JOIN dbo.DimCustomer dc WITH (NOLOCK) ON dc.CustomerId = kfpa.CustomerId
											AND dc.ClientId = kfpa.ClientId
		WHERE kfpa.ClientId = @ClientID AND dc.CustomerId IS NULL) c
		WHERE SortId = 1
END

														 



GO
