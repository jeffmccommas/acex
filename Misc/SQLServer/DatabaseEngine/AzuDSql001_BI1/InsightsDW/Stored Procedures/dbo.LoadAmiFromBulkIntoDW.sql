SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Wayne
-- Create date: 8.4.2014
-- Description:	Inserts AMI data from BulkInsights into the DW
-- =============================================
-- Changed by:	Wayne
-- Change date: 8.4.2014
-- Description:	modifications made for table structure changes
-- =============================================
-- Changed by:	Wayne
-- Change date: 9.22.2014
-- Description:	modifications made for more table structure changes
--				adding a parent / child design
-- =============================================
-- Changed by:	Wayne
-- Change date:	10.23.2014
-- Description:	changed the significant digits on the sum
--				for reporting and also fix a logic problem
-- =============================================
-- Changed by:	Wayne
-- Change date:	11.1.2014
-- Description:	Added a PK to the temp table for performance
-- ============================================
CREATE PROCEDURE [dbo].[LoadAmiFromBulkIntoDW]
AS
BEGIN
SET NOCOUNT ON;

CREATE TABLE #FactAmiDetail(
	[AmiKey] [int] NOT NULL,
	[DateKey] [int] NOT NULL,
	[ClientId] [int] NOT NULL,
	[PremiseId] [varchar](50) NOT NULL,
	[AccountId] [varchar](50) NOT NULL,
	[ServicePointId] [varchar](50) NOT NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[CommodityId] [int] NOT NULL,
	[MeterId] [varchar](64) NOT NULL,
	[UOMId] [int] NOT NULL,
	[TotalUnits] [decimal](18, 4) NULL,
	[SourceId] [int] NOT NULL,
	[ETL_LogId] [int] NOT NULL
	,PRIMARY KEY ([AmiKey], [DateKey])
)


		MERGE InsightsDW.[dbo].[FactAmi] AS Target
		USING (
				SELECT 
					 dp.ClientId
					,dp.[PremiseKey]
					,dsp.[ServicePointKey]
					,-1 AS [MeterId]
					,dsd.[DateKey]
					,dp.[PremiseId]
					,dp.[AccountId]
					,d.[ServicePointId]
					,c.[CustomerId]
					,d.[CommodityId]
					,d.[UOMId]
					,d.[TotalUnits]			
					,8 AS [SourceId]
					,(SELECT  cast(REPLACE(REPLACE(REPLACE(CONVERT(varchar(14),GETDATE(),121),'-',''),':',''),' ','') as INT))  AS [ETL_LogId]
					, ROW_NUMBER() OVER (PARTITION BY dp.ClientId, dp.PremiseKey,dsp.ServicePointkey,MeterId  ORDER BY datekey asc) AS Rn        
					FROM InsightsBulk.[dbo].[ScgDailyTotals] d WITH (NOLOCK)		
					INNER JOIN dbo.DimDate dsd WITH (NOLOCK) ON d.amidate = dsd.datekey
					INNER JOIN [dbo].[DimServicePoint] dsp WITH (NOLOCK) ON d.ServicePointId = dsp.ServicePointId
					--no meter is being sent by scg
					--INNER JOIN [dbo].DimMeter dm WITH (NOLOCK) ON dm.ServicePointKey = dsp.ServicePointKey
			
					INNER JOIN [dbo].DimPremise dp WITH (NOLOCK) ON dp.Premisekey=dsp.PremiseKey
					INNER JOIN dbo.FactCustomerPremise fct WITH (NOLOCK) ON dp.Premisekey=fct.PremiseKey
					INNER JOIN dbo.DimCustomer c WITH (NOLOCK) ON c.CustomerKey=fct.CustomerKey
			

				) AS Source
		 ON (	
				Source.ClientId = Target.ClientId					
				and Source.PremiseKey = Target.PremiseKey	
				and Source.ServicePointkey = Target.ServicePointkey	
				and Source.MeterId = Target.MeterId
			)
		WHEN MATCHED and  Source.rn=1 THEN
			UPDATE SET Target.[MeterId] = Source.[MeterId]
		WHEN NOT MATCHED BY TARGET  and Source.rn=1 THEN
			INSERT (
					   [ClientId]
					  ,[PremiseKey]
					  ,[ServicePointKey]
					  ,[MeterID]
					  ,[ETL_LogId]
					)
			VALUES (
			   source.[ClientId]
			  ,source.[PremiseKey]
			  ,source.[ServicePointKey]
			  ,source.[MeterId]
			  ,Source.[ETL_LogId] 
					)
		OUTPUT		inserted.AmiKey
					,Source.[DateKey]
					,Source.[ClientId]
					,Source.[PremiseId]
					,Source.[AccountId]
					,Source.[ServicePointId]
					,Source.[CustomerId]
					,Source.[CommodityId]
					,Source.[MeterId]
					,Source.[UOMId]
					,Source.[TotalUnits]			
					,Source.[SourceId]
					,Source.[ETL_LogId] 
					INTO #FactAmiDetail;

 

		MERGE InsightsDW.[dbo].[FactAmiDetail] AS Target
		USING (
				SELECT 
					 AmiKey 
					,d.[AmiDate]
					,d.[ClientId]
					,[PremiseId]
					,[AccountId]
					,d.[ServicePointId]
					,[CustomerId]
					,d.[CommodityId]
					,d.[MeterId]
					,d.[UOMId]
					,d.[TotalUnits]			
					,[SourceId]
					,[ETL_LogId]  				
				FROM 
				InsightsBulk.[dbo].[ScgDailyTotals] d 
				inner join 
				#FactAmiDetail t WITH (NOLOCK) 
				on  d.clientid=t.clientid AND d.[ServicePointId]=t.[ServicePointId]
 ) AS Source
 ON (	
		Source.AmiKey = Target.AmiKey					
		and Source.[AmiDate] = Target.[DateKey]	
	)
WHEN MATCHED THEN
    UPDATE SET Target.TotalUnits = Source.TotalUnits
WHEN NOT MATCHED BY TARGET THEN
    INSERT (
			AmiKey 
			,[DateKey]
			,[ClientId]
			,[PremiseId]
			,[AccountId]
			,[ServicePointId]
			,[CustomerId]
			,[CommodityId]
			,[MeterId]
			,[UOMId]
			,[TotalUnits]			
			,[SourceId]
			,[ETL_LogId] 
			)
	VALUES (
				Source.AmiKey 
			,Source.[AmiDate]
			,Source.[ClientId]
			,Source.[PremiseId]
			,Source.[AccountId]
			,Source.[ServicePointId]
			,Source.[CustomerId]
			,Source.[CommodityId]
			,Source.[MeterId]
			,Source.[UOMId]
			,Source.[TotalUnits]			
			,Source.[SourceId]
			,Source.[ETL_LogId] 
			);
END

GO
