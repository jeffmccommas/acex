SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[getBillingData]
    (
      @ClientID AS INT ,
      @premiseKey AS INT
    )
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        WITH    b AS ( SELECT   sc.PremiseKey ,
                                StartDate ,
                                EndDate ,
                                f.TotalUsage AS TotalUnits ,
                                f.CostOfUsage AS TotalCost ,
                                CommodityKey ,
                                UOMKey ,
                                BillDays ,
                                ROW_NUMBER() OVER ( PARTITION BY sc.PremiseKey,
                                                    CommodityKey, UOMKey ORDER BY EndDate ) AS dupreccount
                       FROM     dbo.FactServicePointBilling f WITH ( NOLOCK )
                                INNER JOIN dbo.DimServiceContract sc WITH ( NOLOCK ) ON sc.ServiceContractKey = f.ServiceContractKey
                       WHERE    PremiseKey = @premiseKey
                     )
            SELECT TOP 12
                    p.PremiseKey ,
                    StartDate ,
                    EndDate ,
                    TotalUnits ,
                    TotalCost ,
                    CommodityDesc ,
                    UOMDesc ,
                    BillDays
            FROM    b
                    INNER JOIN DimPremise p WITH ( NOLOCK ) ON b.PremiseKey = p.PremiseKey
                    INNER JOIN FactCustomerPremise cp WITH ( NOLOCK ) ON p.PremiseKey = cp.PremiseKey
                    INNER JOIN DimCustomer cust WITH ( NOLOCK ) ON cust.CustomerKey = cp.CustomerKey
                    INNER JOIN DimCommodity co WITH ( NOLOCK ) ON co.CommodityKey = b.CommodityKey
                    INNER JOIN DimUOM u WITH ( NOLOCK ) ON u.UOMKey = b.UOMKey
            WHERE   cust.ClientId = @ClientID
                    AND b.CommodityKey = 2 --gas
                    AND b.UOMKey = 6
                    AND dupreccount = 1
                    AND p.PremiseKey = @premiseKey
            ORDER BY b.StartDate DESC;


    END;




GO
