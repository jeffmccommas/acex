SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [esotest].[getBillingData] (@ClientID AS INT, @premiseKey AS INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
WITH b AS
(
SELECT f.premisekey, startdate,enddate,totalunits,totalcost,commoditykey,uomkey,BillDays,ROW_NUMBER() OVER (PARTITION BY f.premisekey,startdate,enddate,commoditykey,uomkey ORDER BY f.premisekey,startdate,enddate,commoditykey,uomkey) AS dupreccount 
FROM factbilling f WITH (NOLOCK)
WHERE premisekey = @premisekey
)
SELECT TOP 12 p.premisekey,startdate,enddate,totalunits,totalcost,commoditydesc,uomdesc,BillDays
FROM b
INNER JOIN dimpremise p WITH (NOLOCK)
ON b.premisekey = p.premisekey
INNER JOIN factcustomerpremise cp WITH (NOLOCK)
ON p.premisekey = cp.premisekey
INNER JOIN dimcustomer cust WITH (NOLOCK)
ON cust.customerkey = cp.customerkey
INNER JOIN dimcommodity co WITH (NOLOCK)
ON co.commoditykey = b.commoditykey
INNER JOIN dimuom u WITH (NOLOCK)
ON u.uomkey = b.uomkey
WHERE cust.clientid = @clientid
AND b.commoditykey = 2 --gas
AND b.uomkey = 6
AND dupreccount = 1
AND p.premisekey = @premiseKey
ORDER BY b.startdate DESC


END




GO
