SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ubaid Tariq
-- Create date: 5/29/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_INF_FactBilling_DimRateClass]
    @ETL_LogId INT ,
    @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        WITH    RateClassInfo
                  AS ( SELECT DISTINCT
                                RateClass
                       FROM     ETL.KEY_FactBilling
                       WHERE    ClientId = @ClientID
                                AND RateClass IS NOT NULL
                     )
            INSERT  INTO dbo.DimRateClass
                    ( RateClassDescription ,
                      ClientId ,
                      RateClassName
                    )
                    SELECT  rc.RateClass ,
                            @ClientID ,
                            rc.RateClass
                    FROM    RateClassInfo rc
                            LEFT JOIN dbo.DimRateClass drc ON drc.ClientId = @ClientID
                                                              AND drc.RateClassName = rc.RateClass
                    WHERE   drc.RateClassKey IS NULL;
	

    END;

														 




GO
