SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
-- =============================================
-- Description:	This proc fixes bad zip codes that will be used 
--				when calculating peer groups    
-- Written by:		Wayne
-- Date:			01.14.15
-- =============================================
*/
CREATE procedure [dbo].[FixZipCodesForBenchmarks]
@clientId		 INT	=101

as
begin

set nocount ON

    UPDATE dbo.DimPremise
    SET PostalCode = pc.NewPostalCode
    FROM [InsightsDW].dbo.DimPremise dp
    INNER JOIN [InsightsDW].dbo.PostalCodeChanges pc
    ON pc.ClientId = dp.ClientId AND pc.PremiseId = dp.PremiseId


SET NOCOUNT OFF

END
GO
