SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:           <Author,,Name>
-- Create date: <Create Date,,>
-- Description:      <Description,,>
-- =============================================
CREATE PROCEDURE [esotest].[GenerateXMLBill]
       -- Add the parameters for the stored procedure here
      
@inClientID INT,
@inCustomerId varchar(50),
@inAccountId VARCHAR(50),
@inPremiseId VARCHAR(50)   

-- SET @incustomerid = '1000009400'
--SET @inAccountId = '1210255501'
--SET @inPremiseid = '1210255500'
--SET @inclientid = 101
AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;
	   ;WITH XMLNAMESPACES
(
    DEFAULT 'Nexus:BillAnalyzer'
)
SELECT '3.0' AS '@Version', c.CustomerId AS '@CustomerId',c.FirstName AS '@FirstName', c.LastName AS '@LastName', c.clientid AS '@ReferrerId' ,
(SELECT 
       p.accountid AS '@Account',p.Street1 AS '@Addr1', p.postalCode AS '@Zip',p.StateProvince AS '@State',
       (
       SELECT Convert(VARCHAR(10),b.startdate,126) AS '@BillDate', totalcost AS '@TotalAmount',
       (
       SELECT premiseId AS '@PremiseId', p.Street1 AS '@Addr1',p.city AS '@City', p.StateProvince AS '@State',p.postalCode AS '@Zip',
       (

       SELECT 'G' AS '@Fuel',TotalCost AS '@TotalServiceAmount',totalunits AS '@TotalServiceUse',servicepointid AS '@ServiceId','R' AS '@AccountType',
       (
       --meter bill stuff
       SELECT meterlevel.servicePointId AS '@Meter','Regular' AS '@MeterType',
       (
       SELECT 'RateClass' AS '@RateClass',readlevel.BillDays AS '@BillDays',Convert(VARCHAR(10),readlevel.EndDate,126) AS '@EndDate'
       FROM factbilling readlevel 
       WHERE readlevel.premisekey = b.premisekey AND
       readlevel.BillPeriodStartDateKey = b.BillPeriodStartDateKey AND
       readlevel.BillPeriodEndDateKey = b.BillPeriodEndDateKey AND
       readlevel.CommodityKey = b.CommodityKey AND 
       readlevel.uomkey = b.UOMKey AND
       readlevel.BillPeriodTypeKey = b.BillPeriodTypeKey AND
       readlevel.servicepointkey = b.ServicePointKey
       FOR XML PATH ('ReadInterval'),TYPE
       )
       FROM factbilling meterlevel 
       WHERE meterlevel.premisekey = b.premisekey AND
       meterlevel.BillPeriodStartDateKey = b.BillPeriodStartDateKey AND
       meterlevel.BillPeriodEndDateKey = b.BillPeriodEndDateKey AND
       meterlevel.CommodityKey = b.CommodityKey AND 
       meterlevel.uomkey = b.UOMKey AND
       meterlevel.BillPeriodTypeKey = b.BillPeriodTypeKey AND
       meterlevel.servicepointkey = b.ServicePointKey
       FOR XML PATH('Meter'),TYPE
       )
       FROM factbilling servicelevel 
       WHERE servicelevel.premisekey = b.premisekey AND
       servicelevel.BillPeriodStartDateKey = b.BillPeriodStartDateKey AND
       servicelevel.BillPeriodEndDateKey = b.BillPeriodEndDateKey AND
       servicelevel.CommodityKey = b.CommodityKey AND 
       servicelevel.uomkey = b.UOMKey AND
       servicelevel.BillPeriodTypeKey = b.BillPeriodTypeKey AND
       servicelevel.servicepointkey = b.ServicePointKey
       FOR XML PATH ('Service'),TYPE
       )
       FROM dimpremise plevel
       WHERE plevel.premisekey = p.premisekey
       FOR XML PATH('Premise'),TYPE
       )
              FROM factbilling b
       WHERE b.premisekey = p.premiseKey
       ORDER BY startdate desc
       
       FOR XML PATH ('Bill'),TYPE
       )
from dimcustomer c 
INNER JOIN factcustomerpremise fcp
ON fcp.customerkey = c.customerkey
INNER JOIN dimpremise p
ON p.premisekey = fcp.premisekey
WHERE c.customerid = @incustomerid AND
p.accountid = @inaccountid AND
p.premiseid = @inPremiseId AND 
c.clientid = @inclientid
              FOR XML PATH ('Account'), TYPE
              )

              --User level
FROM dimcustomer c 
INNER JOIN factcustomerpremise fcp
ON fcp.customerkey = c.customerkey
INNER JOIN dimpremise p
ON p.premisekey = fcp.premisekey
WHERE c.customerid = @incustomerid AND
p.accountid = @inaccountid AND
p.premiseid = @inPremiseId AND 
c.clientid = @inclientid
FOR XML PATH ('User'),TYPE

--,
--ROOT ('Customers')
end
--get profilekey
--declare @profilekey as INT
--DECLARE @rc AS int
--SELECT  @profilekey = premisekey FROM dimpremise WHERE premiseid = '1210255500'
--EXECUTE @RC = [dbo].[GetProfileAttributes] 
--   101
--  ,@profilekey
--GO
--END
--SELECT COUNT(DISTINCT customerid) FROM esotest.ESOMeasureSavings





GO
