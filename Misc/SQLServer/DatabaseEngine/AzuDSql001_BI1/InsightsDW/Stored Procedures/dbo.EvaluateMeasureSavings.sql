SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
========================================================
Written by:	Ubaid
Date:		8/21/2014
Description:	returns savings and actions
========================================================
Changed by	Wayne
Date:
Description:	Output result to a permanent table "PremiseSavings", 
			currently trucating table first
			Note must change to selective delete 
========================================================
Changed by	Wayne
Date:		06/03/2015
Description:	skip actions that are disabled
			also change truncate of table to a 
			delete by client id
========================================================
*/
CREATE PROCEDURE [dbo].[EvaluateMeasureSavings] @ClientId AS INT
AS
    BEGIN
        SET NOCOUNT ON;

        CREATE TABLE #RegionalDefaults
            (
              AttributeKey VARCHAR(256) ,
              DefaultKey VARCHAR(256) ,
              DefaultValue VARCHAR(256)
            );
						
        INSERT  INTO #RegionalDefaults
                EXEC InsightsMetadata.[dbo].[GetBenchmarkRegionalDefaults] @ClientId = @ClientId;

        SELECT  rd.AttributeKey ,
                IIF(DefaultValue IS NULL, DefaultKey, DefaultValue) AS DefaultValue ,
                PO.OptionKey AS DefaultOptionKey
        INTO    #DefaultAttributes
        FROM    #RegionalDefaults rd
                INNER JOIN dbo.DimPremiseAttribute pa ON pa.CMSAttributeKey = rd.AttributeKey
                INNER JOIN dbo.DimPremiseOption po ON po.CMSOptionKey = rd.DefaultKey

        SELECT  [ClientConditionID] ,
                [ConditionKey] ,
                cc.[ProfileAttributeKey] ,
                ProfileAttributeTypeKey ,
                [Operator] ,
                IIF(ProfileAttributeTypeKey = 'decimal'
                OR ProfileAttributeTypeKey = 'integer', Value, ProfileOptionKey) AS ActualValue ,
                IIF(ProfileAttributeTypeKey = 'decimal'
                OR ProfileAttributeTypeKey = 'integer', Value, OptionKey) AS AttributeValue
        INTO    #Conditions
        FROM    InsightsMetadata.[cm].[ClientCondition] cc
                INNER JOIN InsightsMetadata.[cm].[TypeProfileAttribute] tpa ON cc.ProfileAttributeKey = tpa.ProfileAttributeKey
                INNER JOIN dbo.dimpremiseattribute pa ON pa.cmsattributekey = cc.ProfileAttributeKey
                INNER JOIN dbo.dimpremiseoption po ON po.attributekey = pa.attributekey
                                                      AND po.CMSOptionKey = cc.profileOptionKey
        
        SELECT  PremiseKey ,
                c.ProfileAttributeKey
        INTO    #AllPremises
        FROM    ( SELECT    PremiseKey ,
                            ContentOptionKey ,
                            ROW_NUMBER() OVER ( PARTITION BY PremiseKey ORDER BY TrackingDate DESC ) AS SortID
                  FROM      dbo.FactPremiseAttribute
                  WHERE     ClientID = @ClientId
                            AND ContentAttributeKey = 'paperreport.treatmentgroup.enrollmentstatus'
                ) p
                CROSS JOIN ( SELECT DISTINCT
                                    ProfileAttributeKey
                             FROM   #Conditions
                           ) c
        WHERE   p.SortID = 1
                AND p.ContentOptionKey = 'paperreport.treatmentgroup.enrollmentstatus.enrolled'
			                                     
        SELECT  PremiseKey ,
                ClientConditionID ,
                ConditionKey
        INTO    #PresmieConditions
        FROM    ( SELECT    p.PremiseKey ,
                            p.ProfileAttributeKey ,
                            ROW_NUMBER() OVER ( PARTITION BY p.PremiseKey,
                                                c.ClientConditionID ORDER BY fpa.TrackingDate DESC ) AS SortId ,
                            IIF(ProfileAttributeTypeKey = 'decimal'
                            OR ProfileAttributeTypeKey = 'integer', IIF(fpa.value IS NULL, rd.DefaultValue, fpa.Value), CONVERT(FLOAT, IIF(fpa.value IS NULL, rd.DefaultOptionKey, fpa.OptionKey))) AS PremiseAttributeValue ,
                            c.ClientConditionID ,
                            c.ConditionKey ,
                            c.Operator ,
                            c.ActualValue ,
                            c.AttributeValue
                  FROM      #AllPremises p
                            LEFT JOIN dbo.FactPremiseAttribute fpa ON p.PremiseKey = fpa.PremiseKey
                                                              AND p.ProfileAttributeKey = fpa.ContentAttributeKey
                            LEFT JOIN #DefaultAttributes rd ON rd.AttributeKey = p.ProfileAttributeKey
                            INNER JOIN #Conditions c ON p.ProfileAttributeKey = c.ProfileAttributeKey
                ) a
        WHERE   SortId = 1
                AND ( ( Operator = '='
                        AND PremiseAttributeValue = AttributeValue
                      )
                      OR ( Operator = '>'
                           AND PremiseAttributeValue > AttributeValue
                         )
                      OR ( Operator = '>='
                           AND PremiseAttributeValue >= AttributeValue
                         )
                      OR ( Operator = '<'
                           AND PremiseAttributeValue < AttributeValue
                         )
                      OR ( Operator = '<='
                           AND PremiseAttributeValue <= AttributeValue
                         )
                    );
       
        SELECT  ca.ClientActionID ,
                ActionKey ,
                cac.ConditionKey ,
                COUNT(ConditionKey) OVER ( PARTITION BY ca.ClientActionID ) AS ConditionsCount
			 INTO    #Actions
        FROM    InsightsMetadata.[cm].[ClientAction] ca
                LEFT JOIN InsightsMetadata.[cm].[ClientActionCondition] cac ON ca.ClientActionID = cac.ClientActionID
			 WHERE ca.ActionKey NOT in
				(SELECT actionkey FROM InsightsMetadata.[cm].[ClientAction] ac where ac.Disable=1 AND ac.ClientID=@ClientId)   --- added by Wayne 6.3.15 to bypass those that are specifically disabled for the client


        SELECT  PremiseKey ,
                ClientActionID ,
                ActionKey
        INTO    #PremiseActions
        FROM    #PresmieConditions pc
                INNER JOIN #Actions a ON pc.ConditionKey = a.ConditionKey OR a.ConditionKey IS NULL
        GROUP BY PremiseKey ,
                ClientActionID ,
                ActionKey
        HAVING  COUNT(a.ConditionKey) = MIN(ConditionsCount)
        ORDER BY PremiseKey ,
                ClientActionID

        SELECT  cas.ClientActionSavingsID ,
                ActionKey ,
                ConditionKey ,
                COUNT(*) OVER ( PARTITION BY cas.ClientActionSavingsID ) AS TotalSavingConditions
        INTO    #ActionSavings
        FROM    InsightsMetadata.[cm].[ClientActionSavings] cas
                INNER JOIN InsightsMetadata.[cm].[ClientActionSavingsCondition] casc ON cas.ClientActionSavingsID = casc.ClientActionSavingsID

        SELECT  pa.PremiseKey ,
                ClientActionSavingsID
        INTO    #PremiseSavings
        FROM    #PremiseActions pa
                INNER JOIN #ActionSavings a ON pa.ActionKey = a.ActionKey
                INNER JOIN #PresmieConditions pc ON pc.PremiseKey = pa.PremiseKey
                                                    AND pc.ConditionKey = a.ConditionKey
        GROUP BY pa.PremiseKey ,
                ClientActionSavingsID
        HAVING  COUNT(a.ConditionKey) = MIN(TotalSavingConditions)
/*
=============================================================

This needs to be a selective delete not a truncate - 06/03/15 by wayne

=============================================================
*/	   
	   DELETE FROM  PremiseSavings 
	   FROM PremiseSavings ps
	   INNER JOIN dbo.DimPremise dp
	   ON dp.PremiseKey=ps.Premisekey
	   WHERE dp.ClientId=@ClientId 

--=============================================================

	   INSERT PremiseSavings
        SELECT  PremiseKey ,
                ps.ClientActionSavingsID ,
                ActionKey ,
                Rank ,
                Cost ,
                AnnualSavingsEstimate,
				cas.AnnualUsageSavingsestimate,
				GETDATE()
        FROM    #PremiseSavings ps
                INNER JOIN InsightsMetadata.[cm].[ClientActionSavings] cas ON ps.ClientActionSavingsID = cas.ClientActionSavingsID
--

        DROP TABLE #PremiseSavings
        DROP TABLE #ActionSavings
        DROP TABLE #PremiseActions
        DROP TABLE #Actions
        DROP TABLE #PresmieConditions
        DROP TABLE #Conditions
        DROP TABLE #AllPremises
        DROP TABLE #DefaultAttributes
        DROP TABLE #RegionalDefaults
  
/*
=============================================================

Because this is now a delete indexes will become fragmented - 06/03/15 by wayne

=============================================================
*/

    ALTER INDEX [NC_Premise_Savings_ActionKey] ON [dbo].[PremiseSavings] REBUILD PARTITION = ALL WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 50)

    ALTER INDEX [NC_PremiseSavings_PremiseKey] ON [dbo].[PremiseSavings] REBUILD PARTITION = ALL WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON , FILLFACTOR = 50)

    END

GO
