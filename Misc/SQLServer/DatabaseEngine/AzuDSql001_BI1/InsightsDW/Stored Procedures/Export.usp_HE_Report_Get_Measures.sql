SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Sunil
-- Create date: unknown
-- Description:	Produces a csv extract reffred to Home Energy Report
--
-- I will put all change modifications for all HE Report procs/job modofcations into this source
-- as it is the parent
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.10.2014
-- Description: v 1.6.0
--	
-- Added a parameter to bypass writing history		
-- =============================================
-- Changed by:	 Wayne
-- Change date: 10.15.2014
-- Description: v 1.11.0
--	
-- fixed a problem in profile table
-- that was causing entire energy object not to be reported on
-- extra space after a comma		
-- =============================================
-- Changed by:	 LCF
-- Change date: 11.13.2014
-- Description: v 1.12.0
--	
-- removed the writetohistory parameter and added billmonth and billyear parameters
-- =============================================
-- Changed by:	 LCF
-- Change date: 11.20.2014
-- Description: v 1.12.0
--	
-- fixed the delete statement so it only deletes users on the report
-- =============================================
-- Changed by:	 Wayne
-- Change date: 12.18.2014
-- Description: v 1.32.0
--	
-- renamed the [dbo].[FactActionItem] table to [export].[HE_Report_FactActionItem]
-- used in proc [Export].[usp_HE_Report_Get_Measures]
-- and [Export].[usp_HE_Report_Get_Measures_AddColumns]
-- =============================================
-- Changed by:	Wayne
-- Change date: 12.31.2014
-- Description:	v 1.33.0 (same version)
--	
--  Removed the WITH (NOLOCK)'s because
--  they were causing race conditions
-- =============================================
-- Changed by:	Wayne
-- Change date: 07.14.2015
-- Description:	replaced the HE_Report_FactActionItem table
--				with a new view vHE_Report_FactActionItem
--	
--  The view (its underlying table) will be populated by new dll
--  being created by the api team (Mike G)
-- =============================================
CREATE procedure [Export].[usp_HE_Report_Get_Measures]
--declare
@profile_name varchar (50)  = 'SCG_Profile101',
@bill_year varchar(4),
@bill_month varchar(2)

as
begin

set nocount on

	declare @enduse varchar (50)

	declare
		@clientID int, 
		@fuel_type varchar (100), 
		@energyobjectcategory_List varchar (1024) , 
		@period varchar (10) , 
		@season_List varchar (100) , 
		@enduse_List varchar (1024) , 
		@ordered_enduse_List varchar (1024), 
		@criteria_id1 varchar (50) , 
		@criteria_detail1 varchar (255)  , 
		@criteria_id2 varchar (50) , 
		@criteria_detail2 varchar (255) , 
		@criteria_id3 varchar (50) , 
		@criteria_detail3 varchar (255) , 
		@criteria_id4 varchar (50) , 
		@criteria_detail4 varchar (255) , 
		@criteria_id5 varchar (50) , 
		@criteria_detail5 varchar (255) , 
		@criteria_id6 varchar (50) , 
		@criteria_detail6 varchar (255), 
		@saveProfile char (1), 
		@verifySave char (1),
		@process_id char (1)

	SELECT
		@clientID = clientID
		, @fuel_type = fuel_type
		, @energyobjectcategory_List = energyobjectcategory_List
		, @period = period
		, @season_List = season_List
		, @enduse_List = enduse_List
		, @ordered_enduse_List = ordered_enduse_List
		, @criteria_id1 = criteria_id1
		, @criteria_detail1 = criteria_detail1
		, @criteria_id2 = criteria_id2
		, @criteria_detail2 = criteria_detail2
		, @criteria_id3 = criteria_id3
		, @criteria_detail3 = criteria_detail3
		, @criteria_id4 = criteria_id4
		, @criteria_detail4 = criteria_detail4
		, @criteria_id5 = criteria_id5
		, @criteria_detail5 = criteria_detail5
		, @criteria_id6 = criteria_id6
		, @criteria_detail6 = criteria_detail6
	  FROM [export].[home_energy_report_criteria_profile]  
	where [profile_name] = @profile_name
	--DELETE FROM [export].[HE_Report_CSV_Measure_history] WHERE profile_name = @profile_name AND bill_month = @bill_month and bill_year = @bill_year

	if object_id ('export.HE_Report_actionitems') is not null drop table export.HE_Report_actionitems
	if object_id ('export.HE_Report_actionitems_ranked') is not null drop table export.HE_Report_actionitems_ranked
	if object_id ('tempdb..#HE_Report_actionitems2') is not null drop table #HE_Report_actionitems2

	--if object_id ('export.HE_Report_actionitems4') is not null drop table export.HE_Report_actionitems4

	CREATE TABLE [export].[HE_Report_actionitems] (
		[ActionItemKey] int NULL,
		[ActionItemDesc] [varchar](100) NULL,
		[MeasureId] [varchar](256) NULL,  -- this is the [cm].[ClientAction].actionkey i think
		[EnergyObjectDesc] varchar (50) NULL,  --appliance
		[PremiseKey] [int] NOT NULL,
		[RefCost] varchar (50) NULL,
		[RefSavings] varchar (50) NULL
	)

	CREATE TABLE [export].[HE_Report_actionitems_ranked] (
		[ActionItemKey] int NULL,
		[ActionItemDesc] [varchar](100) NULL,
		[MeasureId] [varchar](256) NULL,   --this is the [cm].[ClientAction].actionkey i think
		[EnergyObjectDesc] varchar (50) NULL,    ----appliance
		[PremiseKey] [int] NOT NULL,
		[RefCost] varchar (50) NULL,
		[RefSavings] varchar (50) NULL,
		[rank_id] [int] NULL, 
		[level_num] int NULL,
		[valid_level1] [int] NULL
	)



	declare @sqlcmd varchar (8000), @orderbyClause varchar (2000) = '', @validClause varchar (8000) = ''
	, @columnList varchar (8000) = 
			'[ActionItemKey], [ActionItemDesc], [MeasureId], [EnergyObjectDesc], [PremiseKey], [RefCost], [RefSavings]'
	, @columnListh2 varchar (8000) = 
			'h2.[ActionItemKey], h2.[ActionItemDesc], h2.[MeasureId], h2.[EnergyObjectDesc], h2.[PremiseKey], h2.[RefCost], h2.[RefSavings]'
	, @continueValidating int = 1, @validationcount int = 1

	select	@sqlcmd = '
	insert	export.HE_Report_actionitems ([ActionItemKey], [ActionItemDesc], [MeasureId], [EnergyObjectDesc], [PremiseKey])
	select	distinct fai.ActionItemKey, ai.ActionItemDesc, ai.MeasureId, eo.EnergyObjectDesc, fai.PremiseKey
	from	[InsightsMetadata].dbo.vDimActionItem ai  
			inner join [export].[vHE_Report_FactActionItem] fai  
			on fai.ActionItemKey = ai.ActionItemKey
			inner join DimEnergyObject eo  
			on eo.EnergyObjectKey = fai.EnergyObjectKey
	'


	exec (@sqlcmd)

   
--	work with Criteria 1
--select @criteria_id1
	if @criteria_id1 <> 'NOVALUE'
	begin
		EXEC export.usp_HE_Report_Get_Measures_AddColumns @profile_name = @profile_name, @criteria_id = @criteria_id1, @criteria_detail = @criteria_detail1
		, @orderbyClause = @orderbyClause OUTPUT, @validClause = @validClause OUTPUT, @columnList = @columnList OUTPUT, @columnListh2 = @columnListh2 OUTPUT
	end

--	work with Criteria 2
	if @criteria_id2 <> 'NOVALUE'
	begin
		EXEC export.usp_HE_Report_Get_Measures_AddColumns @profile_name = @profile_name, @criteria_id = @criteria_id2, @criteria_detail = @criteria_detail2
		, @orderbyClause = @orderbyClause OUTPUT, @validClause = @validClause OUTPUT, @columnList = @columnList OUTPUT, @columnListh2 = @columnListh2 OUTPUT
	end

--	work with Criteria 3
	if @criteria_id3 <> 'NOVALUE'
	begin
		EXEC export.usp_HE_Report_Get_Measures_AddColumns @profile_name = @profile_name, @criteria_id = @criteria_id3, @criteria_detail = @criteria_detail3
		, @orderbyClause = @orderbyClause OUTPUT, @validClause = @validClause OUTPUT, @columnList = @columnList OUTPUT, @columnListh2 = @columnListh2 OUTPUT
	end

--	work with Criteria 4
	if @criteria_id4 <> 'NOVALUE'
	begin
		EXEC export.usp_HE_Report_Get_Measures_AddColumns @profile_name = @profile_name, @criteria_id = @criteria_id4, @criteria_detail = @criteria_detail4
		, @orderbyClause = @orderbyClause OUTPUT, @validClause = @validClause OUTPUT, @columnList = @columnList OUTPUT, @columnListh2 = @columnListh2 OUTPUT
	end

--	work with Criteria 5
	if @criteria_id5 <> 'NOVALUE'
	begin
		EXEC export.usp_HE_Report_Get_Measures_AddColumns @profile_name = @profile_name, @criteria_id = @criteria_id5, @criteria_detail = @criteria_detail5
		, @orderbyClause = @orderbyClause OUTPUT, @validClause = @validClause OUTPUT, @columnList = @columnList OUTPUT, @columnListh2 = @columnListh2 OUTPUT
	end

--	work with Criteria 6
	if @criteria_id6 <> 'NOVALUE'
	begin
		EXEC export.usp_HE_Report_Get_Measures_AddColumns @profile_name = @profile_name, @criteria_id = @criteria_id6, @criteria_detail = @criteria_detail6
		, @orderbyClause = @orderbyClause OUTPUT, @validClause = @validClause OUTPUT, @columnList = @columnList OUTPUT, @columnListh2 = @columnListh2 OUTPUT
	end

	--select	@orderbyClause
	--select	@validClause
	--select @columnList
	--select @columnListh2

	select @sqlcmd = '
	alter table export.HE_Report_actionitems add valid_level1 int NULL'
	exec (@sqlcmd)

	select	@sqlcmd = '
	update	hera
	set		valid_level1 = ' + @validClause + ' then 1 else 0 end
	from	export.HE_Report_actionitems hera'

	exec (@sqlcmd)


	if object_id ('tempdb..#ordered_enduse_list') is not null drop table #ordered_enduse_list
	create table #ordered_enduse_list ([ordered_enduse] varchar (50), enduse_order int identity (1, 1))
	insert #ordered_enduse_list ([ordered_enduse])
	select ltrim(rtrim(cast (param as varchar (50))))
	from [dbo].[ufn_split_values] (@ordered_enduse_list, ',')

-- comment below out
--SELECT 1, @sqlcmd
--SELECT	* from #ordered_enduse_list
---
	select	*
	into	#HE_Report_actionitems2
	FROM	export.HE_Report_actionitems

	select @continueValidating = 1, @validationcount = 1

	while @continueValidating = 1
	begin

		TRUNCATE TABLE #HE_Report_actionitems2
--		select @continueValidating '@continueValidating', @validationcount '@validationcount'

		select	@sqlcmd = '
			Insert #HE_Report_actionitems2 (
			' + @columnList + ', valid_level' + convert (varchar, @validationcount) + ')
			select	*
			from
			(
				select /*p1.clientID,*/ ' + @columnListh2 + ', valid_level' + convert (varchar, @validationcount) + '
				from export.HE_Report_actionitems h2  
					inner join dimPremise p1  
					on p1.premiseKey = h2.premiseKey
					inner join #ordered_enduse_list oel  
					on ltrim(rtrim(oel.[ordered_enduse])) = h2.EnergyObjectDesc
				where	p1.clientID = ' + convert (varchar, @clientID) + '

				and not exists
				(
					select	hist2.profile_name, hist2.PremiseID, hist2.history_date, hist1.MeasureID,hist2.bill_month,hist2.bill_year
					from
					(
						select	profile_name, PremiseID, history_date,bill_year,bill_month,
								row_number () over (partition by PremiseID order by bill_year,bill_month desc ) as row_id
						from	[export].[HE_Report_CSV_Measure_history]
				) hist2  
					inner join [export].[HE_Report_CSV_Measure_history] hist1  
							on hist1.PremiseID = hist2.PremiseID and hist1.bill_year = hist2.bill_year and hist1.bill_month = hist2.bill_month
					inner join dbo.DimPremise histp1  
					on histp1.PremiseID = hist1.PremiseID
					where	hist2.row_id = 1
					and		h2.MeasureID = hist1.MeasureID 
					and		h2.PremiseKey = histp1.PremiseKey
				)


			) a2
		'
-------
--select 2, @sqlcmd
------
		exec (@sqlcmd)

		select	@sqlcmd = '
			;
			with lessthan3 as
			(
				select	premiseKey
				from	export.HE_Report_actionitems hera
				where	valid_level' + convert (varchar, @validationcount) + ' = 1
				group by premiseKey
				having count (*) < 3
			)
			delete	h2
			from	#HE_Report_actionitems2 h2
					inner join lessthan3 l3 on l3.PremiseKey = h2.premiseKey 
		'
-------
--SELECT 2, @sqlcmd
------
		exec (@sqlcmd)

		select	@sqlcmd = '
			insert [export].[HE_Report_actionitems_ranked]
			(' + @columnList + ', [rank_id], [level_num], valid_level' +  + convert (varchar, @validationCount) + ')
			select ' + @columnListh2 + ', [rank_id], ' + convert (varchar, @validationCount) + ', valid_level' +  + convert (varchar, @validationCount) + '
			from
			(
				select	' + @columnListh2 + ', valid_level' + convert (varchar, @validationCount) + '
				, row_number () over (partition by h2.premiseKey order by valid_level' + convert (varchar, @validationCount) + ' DESC, ' + @orderbyClause + ') as rank_id
				from	#HE_Report_actionitems2 h2  
				inner join #ordered_enduse_list oel  
				on ltrim(rtrim(oel.[ordered_enduse])) = h2.EnergyObjectDesc
			) h2
		'
-------
--SELECT 4, @sqlcmd
------
		exec (@sqlcmd)

		delete hera
		from	export.HE_Report_actionitems hera	
				inner join #HE_Report_actionitems2 hera2 on hera.PremiseKey = hera2.premiseKey 

		if charindex ('AND', @validClause) > 1
		begin
			select	@validationcount += 1
			select @validClause = substring (@validClause, 1, charindex ('AND', @validClause) - 1)

			select	@sqlcmd = '
			alter table export.HE_Report_actionitems add [valid_level' + convert (varchar, @validationcount) + '] [int] NULL'
			exec (@sqlcmd)

			select	@sqlcmd = '
			alter table export.HE_Report_actionitems_ranked add [valid_level' + convert (varchar, @validationcount) + '] [int] NULL'
			exec (@sqlcmd)

			select	@sqlcmd = '
			alter table #HE_Report_actionitems2 add [valid_level' + convert (varchar, @validationcount) + '] [int] NULL'
			exec (@sqlcmd)

			select	@sqlcmd = '
			update	hera
			set		valid_level'  + convert (varchar, @validationcount) + ' = ' + @validClause + ' then 1 else 0 end
			from	export.HE_Report_actionitems hera'
			--select @sqlcmd
			exec (@sqlcmd)

		end
		else
		begin
			select	@continueValidating = 0
		end
--bottom of loop		
	end
--PRINT 'Outside of the numeric column checking loop'
--	Outside of the numeric column checking loop

	select	@validationcount += 1
	select	@sqlcmd = '
	alter table export.HE_Report_actionitems add [valid_level' + convert (varchar, @validationcount) + '] [int] NULL'
	exec (@sqlcmd)

	select	@sqlcmd = '
	alter table export.HE_Report_actionitems_ranked add [valid_level' + convert (varchar, @validationcount) + '] [int] NULL'
	exec (@sqlcmd)

	select	@sqlcmd = '
	alter table #HE_Report_actionitems2 add [valid_level' + convert (varchar, @validationcount) + '] [int] NULL'
	exec (@sqlcmd)

	TRUNCATE TABLE #HE_Report_actionitems2
--		select @continueValidating '@continueValidating', @validationcount '@validationcount'
--PRINT 'before Insert #HE_Report_actionitems2'
	select	@sqlcmd = '
		Insert #HE_Report_actionitems2 (
		' + @columnList + ', valid_level' + convert (varchar, @validationcount) + ')
		select	*
		from
		(
			select distinct /*p1.clientID,*/ ' + @columnListh2 + ', valid_level' + convert (varchar, @validationcount) + '
			from export.HE_Report_actionitems h2  
				inner join dimPremise p1  
				on p1.premiseKey = h2.premiseKey
				inner join #ordered_enduse_list oel  
				on ltrim(rtrim(oel.[ordered_enduse])) = h2.EnergyObjectDesc
			where	p1.clientID = ' + convert (varchar, @clientID) + '
		) a2
	'

	exec (@sqlcmd)
--
--	SELECT 'count before delete=', COUNT(*) FROM 	#HE_Report_actionitems2
--
	select	@sqlcmd = '
		;
		with lessthan3 as
		(
			select	premiseKey
			from	export.HE_Report_actionitems hera
			where	valid_level' + convert (varchar, @validationcount) + ' = 1
			group by premiseKey
			having count (*) < 3
		)
		delete	h2
		from	#HE_Report_actionitems2 h2
				inner join lessthan3 l3 on l3.PremiseKey = h2.premiseKey 
	'
	exec (@sqlcmd)

--
--	SELECT 'COUNT AFTER delete=', COUNT(*) FROM 	#HE_Report_actionitems2
--

	select	@sqlcmd = '
		insert [export].[HE_Report_actionitems_ranked]
		(' + @columnList + ', [rank_id], [level_num], valid_level' +  + convert (varchar, @validationCount) + ')
		select ' + @columnListh2 + ', [rank_id], ' + convert (varchar, @validationCount) + ', valid_level' +  + convert (varchar, @validationCount) + '
		from
		(
			select	DISTINCT ' + @columnListh2 + ', valid_level' + convert (varchar, @validationCount) + '
			, row_number () over (partition by h2.premiseKey order by ' + @orderbyClause + ') as rank_id
			from	#HE_Report_actionitems2 h2  
			inner join #ordered_enduse_list oel   
			on ltrim(rtrim(oel.[ordered_enduse])) = h2.EnergyObjectDesc
		) h2

		WHERE NOT EXISTS
		(
			SELECT	hist2.profile_name, hist2.PremiseID, hist2.history_date, hist1.MeasureID
			FROM
			(
				SELECT	profile_name, PremiseID, history_date,bill_year,bill_month,
						ROW_NUMBER () OVER (PARTITION BY PremiseID ORDER BY bill_year,bill_month DESC) AS row_id
				FROM	[export].[HE_Report_CSV_Measure_history]
			) hist2  
			INNER JOIN [export].[HE_Report_CSV_Measure_history] hist1  
					ON hist1.PremiseID = hist2.PremiseID AND hist1.bill_year = hist2.bill_year AND hist1.bill_month = hist2.bill_month
			INNER JOIN dbo.DimPremise histp1  
			ON histp1.PremiseID = hist1.PremiseID
			WHERE	hist2.row_id = 1
			AND		h2.MeasureID = hist1.MeasureID 
			AND		h2.PremiseKey = histp1.PremiseKey
		)

	'
--select @sqlcmd
	exec (@sqlcmd)

	delete hera
	from	export.HE_Report_actionitems hera	
			inner join #HE_Report_actionitems2 hera2 on hera.PremiseKey = hera2.premiseKey 

--	Update the main table - [export].[home_energy_report_staging_allcolumns]

	select @sqlcmd = '
	UPDATE  h2
	SET		h2.MID1 = CASE WHEN h1.PremiseKey IS NOT NULL THEN COALESCE (h1.MeasureId, '''') ELSE '''' END, 
			h2.MTitle1 = CASE WHEN h1.PremiseKey IS NOT NULL THEN COALESCE (LEFT (h1.ActionItemDesc, 50), '''') ELSE '''' END, 
			h2.MText1 = '''',
			h2.MCost1 = CASE WHEN h1.PremiseKey IS NOT NULL THEN COALESCE (CONVERT (VARCHAR, h1.RefCost), '''') ELSE '''' END, 
			h2.MSavings1 = CASE WHEN h1.PremiseKey IS NOT NULL THEN ' + 
				' COALESCE (CONVERT (VARCHAR, h1.RefSavings), '''') ELSE '''' END 
	FROM	[export].[home_energy_report_staging_allcolumns] h2
			INNER JOIN dbo.DimPremise p1 ON p1.PremiseID = h2.PremiseID
			INNER JOIN export.HE_Report_actionitems_ranked h1 ON h1.PremiseKey = p1.PremiseKey
	WHERE	h1.rank_id = 1
	'
--SELECT @sqlcmd
	exec (@sqlcmd)


	select @sqlcmd = '
	UPDATE  h2
	SET		h2.MID2 = CASE WHEN h1.PremiseKey IS NOT NULL THEN COALESCE (h1.MeasureId, '''') ELSE '''' END, 
			h2.MTitle2 = CASE WHEN h1.PremiseKey IS NOT NULL THEN COALESCE (LEFT (h1.ActionItemDesc, 50), '''') ELSE '''' END, 
			h2.MText2 = '''',
			h2.MCost2 = CASE WHEN h1.PremiseKey IS NOT NULL THEN COALESCE (CONVERT (VARCHAR, h1.RefCost), '''') ELSE '''' END, 
			h2.MSavings2 = CASE WHEN h1.PremiseKey IS NOT NULL THEN ' + 
				' COALESCE (CONVERT (VARCHAR, h1.RefSavings), '''') ELSE '''' END 
	FROM	[export].[home_energy_report_staging_allcolumns] h2
			INNER JOIN dbo.DimPremise p1 ON p1.PremiseID = h2.PremiseID
			INNER JOIN export.HE_Report_actionitems_ranked h1 ON h1.PremiseKey = p1.PremiseKey
	WHERE	h1.rank_id = 2
	'
	exec (@sqlcmd)

	select @sqlcmd = '
	UPDATE  h2
	SET		h2.MID3 = CASE WHEN h1.PremiseKey IS NOT NULL THEN COALESCE (h1.MeasureId, '''') ELSE '''' END, 
			h2.MTitle3 = CASE WHEN h1.PremiseKey IS NOT NULL THEN COALESCE (LEFT (h1.ActionItemDesc, 50), '''') ELSE '''' END, 
			h2.MText3 = '''',
			h2.MCost3 = CASE WHEN h1.PremiseKey IS NOT NULL THEN COALESCE (CONVERT (VARCHAR, h1.RefCost), '''') ELSE '''' END, 
			h2.MSavings3 = CASE WHEN h1.PremiseKey IS NOT NULL THEN ' + 
				' COALESCE (CONVERT (VARCHAR, h1.RefSavings), '''') ELSE '''' END 
	FROM	[export].[home_energy_report_staging_allcolumns] h2
			INNER JOIN dbo.DimPremise p1 ON p1.PremiseID = h2.PremiseID
			INNER JOIN export.HE_Report_actionitems_ranked h1 ON h1.PremiseKey = p1.PremiseKey
	WHERE	h1.rank_id = 3
	'
	exec (@sqlcmd)


	update  h2
	set		h2.MID1 = '', 
			h2.MTitle1 = '', 
			h2.MText1 = '',
			h2.MCost1 = '', 
			h2.MSavings1 = ''
	from	[export].[home_energy_report_staging_allcolumns] h2
	where	coalesce (h2.MID1, '') = ''

	update  h2
	set		h2.MID2 = '', 
			h2.MTitle2 = '', 
			h2.MText2 = '',
			h2.MCost2 = '', 
			h2.MSavings2 = ''
	from	[export].[home_energy_report_staging_allcolumns] h2
	where	coalesce (h2.MID2, '') = ''

	update  h2
	set		h2.MID3 = '', 
			h2.MTitle3 = '', 
			h2.MText3 = '',
			h2.MCost3 = '', 
			h2.MSavings3 = '' -- This should be MSavings3
	from	[export].[home_energy_report_staging_allcolumns] h2
	where	coalesce (h2.MID3, '') = ''


	declare	@history_date datetime = getdate ()

/*
--	WRITE TO HISTORY
*/

--delete any for this period that are in our table
	DELETE FROM [export].[HE_Report_CSV_Measure_history]  WHERE profile_name = @profile_name AND bill_month = @bill_month and bill_year = @bill_year
		and exists
		(SELECT	premiseID
		FROM	[export].[home_energy_report_staging_allcolumns] a1
		WHERE a1.premiseid = [export].[HE_Report_CSV_Measure_history].premiseid)


		insert [export].[HE_Report_CSV_Measure_history]
		(profile_name, PremiseID, MeasureID, history_date,bill_year,bill_month)
		SELECT	@profile_name, premiseID, MID1, @history_date,@bill_year,@bill_Month
		FROM	[export].[home_energy_report_staging_allcolumns] a1
		WHERE	coalesce (MID1, '') <> ''
		
		insert [export].[HE_Report_CSV_Measure_history]
		(profile_name, PremiseID, MeasureID, history_date,bill_year,bill_month)
		SELECT	@profile_name, premiseID, MID2, @history_date,@bill_year,@bill_Month
		FROM	[export].[home_energy_report_staging_allcolumns] a1
		WHERE	coalesce (MID2, '') <> ''

		insert [export].[HE_Report_CSV_Measure_history]
		(profile_name, PremiseID, MeasureID, history_date,bill_year,bill_month)
		SELECT	@profile_name, premiseID, MID3, @history_date,@bill_year,@bill_Month
		FROM	[export].[home_energy_report_staging_allcolumns] a1
		WHERE	coalesce (MID3, '') <> ''
end



GO
