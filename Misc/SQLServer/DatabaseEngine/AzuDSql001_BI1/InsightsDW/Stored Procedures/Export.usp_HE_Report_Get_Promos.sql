SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Wayne
-- Create date:	12.17.14
-- Description:	Determines promo codes
-- Version added:	v 1.31.0
--	
-- =============================================
-- Changed by:	Wayne
-- Change date: 12.31.2014
-- Description:	v 1.33.0
--	
-- changed SUM to MAX in Q1 vs Q3 comparison	for promos	
-- =============================================
-- Changed by:	Wayne
-- Change date: 12.31.2014
-- Description:	v 1.33.0 (same version)
--	
--  Removed the WITH (NOLOCK)'s because
--  they were causing race conditions
-- =============================================
-- Changed by:	Wayne
-- Change date: 12.31.2014
-- Description:	v 1.33.0 (same version)
--	
--  hard coded some values where premises did
-- not have action asigned to them
-- causing a null savings for the promo
-- =============================================
CREATE PROCEDURE [Export].[usp_HE_Report_Get_Promos]
    @profile_name VARCHAR(50) = 'SCG_Profile101' ,
    @bill_year VARCHAR(4) = '2014' ,
    @bill_month VARCHAR(2) = '11' ,
    @channel VARCHAR(5) = 'print' ,		  -- options are 'email'  or 'print'
    @promo_code_1 VARCHAR(1) = 'C' ,			  --use variable promo_code 1 which is for non heating customers
    @promo_code_2 VARCHAR(1) = 'E' ,			  --use variable promo_code 2 which is for non heating customers
    @promo_code_3 VARCHAR(1) = 'C' ,			  --use variable promo_code 3 which is for heating customers
    @promo_code_4 VARCHAR(1) = 'F' ,			  --use variable promo_code 4 which is for heating customers
    @promo_code_5 VARCHAR(1) = 'C'			  --use variable promo_code 5 which is for customers on email channel
AS
    BEGIN
        SET NOCOUNT ON;
        IF OBJECT_ID('tempdb..#mydata') IS NOT NULL
            DROP TABLE #mydata;


        IF OBJECT_ID('tempdb..#mydata') IS  NULL
            BEGIN
                CREATE TABLE [dbo].[#mydata]
                    (
                      [PremiseKey] [INT] NOT NULL ,
                      [PremiseId] [VARCHAR](50) NOT NULL ,
                      [PostalCode] [VARCHAR](15) NULL ,
                      [house_size] [VARCHAR](256) NULL ,
                      [Q1] [DECIMAL](38, 2) NULL ,
                      [Q3] [DECIMAL](38, 2) NULL ,
                      [60_pct_greater_than_Q1] [DECIMAL](38, 2) NULL ,
                      likely_gas_heat BIT NULL ,
                      ImproveInsulationSavings INT NULL ,
                      NewwaterheaterSavings INT NULL ,
                      mean_temp [DECIMAL](38, 2) NULL ,
                      sum_HDD INT NULL ,
                      BillPeriodStartDate DATE NULL ,
                      BillPeriodEndDate DATE NULL
                    )
                ON  [PRIMARY];
            END

    -- this is just gettingthe premises we are concerned with
	   ;
        WITH    cte
                  AS ( SELECT   sc.PremiseKey ,
                                fb.PremiseId ,
                                fb.TotalUsage AS TotalUnits ,
                                NTILE(4) OVER ( PARTITION BY sc.PremiseKey ORDER BY fb.TotalUsage ASC ) AS quartile
                       FROM     InsightsDW.dbo.FactServicePointBilling fb
                                INNER JOIN dbo.DimServiceContract sc ON sc.ServiceContractKey = fb.ServiceContractKey
                                INNER JOIN InsightsDW.dbo.DimDate d ON d.FullDateAlternateKey = CAST(( SUBSTRING(CAST(fb.BillPeriodEndDateKey AS CHAR(8)),
                                                              1, 4) + '-'
                                                              + SUBSTRING(CAST(fb.BillPeriodEndDateKey AS CHAR(8)),
                                                              5, 2) + '-'
                                                              + '01' ) AS DATE)
                                INNER JOIN InsightsDW.[Export].[home_energy_report_staging_allcolumns] b ON fb.PremiseId = b.PremiseID
                     ),
                PivotData
                  AS ( SELECT   PremiseKey ,
                                PremiseId ,
                                cte.TotalUnits ,
                                quartile
                       FROM     cte
                       WHERE    cte.quartile IN ( 1, 3 )
                     )
            INSERT  #mydata
                    ( [PremiseKey] ,
                      [PremiseId] ,
                      [Q1] ,
                      [Q3] 
	                )
                    SELECT  PremiseKey ,
                            PremiseId ,
                            [1] AS Q1 ,
                            [3] AS Q3
                    FROM    PivotData PIVOT ( MAX(TotalUnits) FOR quartile IN ( [1],
                                                              [3] ) ) AS p;
            WITH    prermisedata
                      AS ( SELECT   Q1 + ( ( Q1 * 60 ) / 100 ) AS [60_pct_greater_than_Q1] ,
                                    CASE WHEN Q3 > Q1 + ( ( Q1 * 60 ) / 100 )
                                         THEN CAST(1 AS BIT)
                                         ELSE CAST(0 AS BIT)
                                    END AS likely_gas_heat ,
                                    dp.PostalCode ,
                                    pd.PremiseId ,
                                    pd.PremiseKey ,
                                    pd.Q1 ,
                                    pd.Q3
                           FROM     #mydata pd
                                    INNER JOIN InsightsDW.dbo.DimPremise dp ON pd.PremiseKey = dp.PremiseKey
                         ),
                    melissa
                      AS ( SELECT   pd.* ,
                                    CAST(m.[house.totalarea] AS INT) AS house_size_m
                           FROM     [InsightsMetadata].[dbo].[ClientPropertyData] m
                                    RIGHT OUTER JOIN prermisedata pd ON m.PremiseID = pd.PremiseId
                         ),
                    attributdata
                      AS ( SELECT   pd.* ,
                                    CAST(s.Value AS INT) AS house_size_dw
                           FROM     ( SELECT    *
                                      FROM      InsightsDW.dbo.FactPremiseAttribute fpa
                                      WHERE     fpa.ContentAttributeKey = 'house.totalarea'
                                                AND fpa.EffectiveDate = ( SELECT
                                                              MAX(EffectiveDate)
                                                              FROM
                                                              [InsightsDW].[dbo].[FactPremiseAttribute] fpa2
                                                              WHERE
                                                              fpa.PremiseKey = fpa2.PremiseKey
                                                              AND ContentAttributeKey = 'house.totalarea'
                                                              )
                                                AND Value > 0
                                    ) s
                                    RIGHT JOIN melissa pd ON pd.PremiseKey = s.PremiseKey
                         ),
                    identified_attributdata
                      AS ( SELECT   * ,
                                    CASE WHEN house_size_m IS NOT NULL
                                              AND house_size_m >= 500
                                         THEN house_size_m
                                         WHEN house_size_dw IS NOT NULL
                                              AND house_size_dw >= 500
                                         THEN house_size_dw
                                         WHEN house_size_m IS NOT NULL
                                              OR house_size_dw IS NOT NULL
                                         THEN COALESCE(house_size_m,
                                                       house_size_dw)
                                         ELSE 500
                                    END AS house_size
                           FROM     attributdata a
                         ),
                    insulationsavingsdata
                      AS ( SELECT   a.* ,
                                    ISNULL(r.AnnualSavingsEstimate,
                                           (
								    -- this should not ne hard coded
                                             SELECT CASE WHEN a.house_size >= 500
                                                              AND a.house_size < 1000
                                                         THEN 30
                                                         WHEN a.house_size >= 1000
                                                              AND a.house_size < 1500
                                                         THEN 45
                                                         WHEN a.house_size >= 1500
                                                              AND a.house_size < 2000
                                                         THEN 55
                                                         WHEN a.house_size >= 2000
                                                              AND a.house_size < 2500
                                                         THEN 70
                                                         WHEN a.house_size >= 2500
                                                              AND a.house_size < 3000
                                                         THEN 80
                                                         WHEN a.house_size >= 3000
                                                              AND a.house_size < 3500
                                                         THEN 85
                                                         WHEN a.house_size >= 3500
                                                              AND a.house_size < 4000
                                                         THEN 100
                                                         WHEN a.house_size >= 4000
                                                              AND a.house_size < 4500
                                                         THEN 120
                                                         WHEN a.house_size >= 4500
                                                              AND a.house_size < 5000
                                                         THEN 130
                                                         WHEN a.house_size >= 5000
                                                              AND a.house_size < 999999
                                                         THEN 180
                                                         ELSE 0
                                                    END
                                           )) AS ImproveInsulationSavings
                           FROM     identified_attributdata a
                                    LEFT OUTER JOIN ( SELECT  *
                                                      FROM    InsightsDW.dbo.PremiseSavings p
                                                      WHERE   p.ActionKey = 'improveinsulation'
                                                    ) r ON r.Premisekey = a.PremiseKey
                         ),
                    waterheatersavingsdata
                      AS ( SELECT   a.* ,
                                    ISNULL(r.AnnualSavingsEstimate, 0) AS NewwaterheaterSavings
                           FROM     insulationsavingsdata a
                                    LEFT OUTER JOIN ( SELECT  *
                                                      FROM    InsightsDW.dbo.PremiseSavings p
                                                      WHERE   p.ActionKey = 'replacewaterheater'
                                                    ) r ON r.Premisekey = a.PremiseKey
                         ),
                    sumcte
                      AS ( SELECT   m.PremiseId ,
                                    m.ImproveInsulationSavings ,
                                    m.NewwaterheaterSavings ,
                                    m.house_size ,
                                    m.likely_gas_heat
		
				--detail
                                    ,
                                    AVG(b.TotalUsage) OVER ( PARTITION BY sc.PremiseKey ) AS [Mean_Units] ,
                                    AVG(wd.Monthly_HDD) OVER ( PARTITION BY sc.PremiseKey ) AS [Mean_HDD]

				--monthly total less  mean calculations
                                    ,
                                    b.TotalUsage
                                    - ( AVG(b.TotalUsage) OVER ( PARTITION BY sc.PremiseKey ) ) AS [Monthly_unit_less_Mean_Units ("a")] ,
                                    wd.Monthly_HDD
                                    - ( AVG(wd.Monthly_HDD) OVER ( PARTITION BY sc.PremiseKey ) ) AS [Monthly_HDD_less_Mean_HDD ("b")]

				--monthly product
                                    ,
                                    ( b.TotalUsage
                                      - ( AVG(b.TotalUsage) OVER ( PARTITION BY sc.PremiseKey ) ) )
                                    * ( wd.Monthly_HDD
                                        - ( AVG(wd.Monthly_HDD) OVER ( PARTITION BY sc.PremiseKey ) ) ) AS [a X b]

				-- monthly squares
                                    ,
                                    ( b.TotalUsage
                                      - ( AVG(b.TotalUsage) OVER ( PARTITION BY sc.PremiseKey ) ) )
                                    * ( b.TotalUsage
                                        - ( AVG(b.TotalUsage) OVER ( PARTITION BY sc.PremiseKey ) ) ) AS [a Squared] ,
                                    ( wd.Monthly_HDD
                                      - ( AVG(wd.Monthly_HDD) OVER ( PARTITION BY sc.PremiseKey ) ) )
                                    * ( wd.Monthly_HDD
                                        - ( AVG(wd.Monthly_HDD) OVER ( PARTITION BY sc.PremiseKey ) ) ) AS [b Squared]
                           FROM     waterheatersavingsdata m
                                    INNER JOIN dbo.DimServiceContract sc ON sc.PremiseKey = m.PremiseKey
                                    INNER JOIN InsightsDW.dbo.FactServicePointBilling b ON b.ServiceContractKey = sc.ServiceContractKey
                                    OUTER APPLY ( SELECT    SUM(wd.HeatingDegreeDays) AS Monthly_HDD
                                                  FROM      InsightsDW.dbo.WeatherData wd
                                                  WHERE     CAST(wd.WeatherReadingDate AS DATE) BETWEEN CAST(SUBSTRING(CAST(b.BillPeriodStartDateKey AS CHAR(8)),
                                                              1, 4) + '-'
                                                              + SUBSTRING(CAST(b.BillPeriodStartDateKey AS CHAR(8)),
                                                              5, 2) + '-'
                                                              + SUBSTRING(CAST(b.BillPeriodStartDateKey AS CHAR(8)),
                                                              7, 2) AS DATE)
                                                              AND
                                                              CAST(SUBSTRING(CAST(b.BillPeriodEndDateKey AS CHAR(8)),
                                                              1, 4) + '-'
                                                              + SUBSTRING(CAST(b.BillPeriodEndDateKey AS CHAR(8)),
                                                              5, 2) + '-'
                                                              + SUBSTRING(CAST(b.BillPeriodEndDateKey AS CHAR(8)),
                                                              7, 2) AS DATE)
                                                            AND SUBSTRING(m.PostalCode,
                                                              1, 5) = wd.ZipCode
                                                ) wd
                         ),
                    sumcte2
                      AS ( SELECT   * 
	 -- suma of all month products
                                    ,
                                    SUM([a X b]) OVER ( PARTITION BY b.PremiseId ) AS [SUM a X b]

	 --sum of all months squares
                                    ,
                                    SUM([a Squared]) OVER ( PARTITION BY b.PremiseId ) AS [SUM a squared] ,
                                    SUM([b Squared]) OVER ( PARTITION BY b.PremiseId ) AS [SUM b squared]
                           FROM     sumcte b
                         ),
                    onerow
                      AS ( SELECT   *
                           FROM     ( SELECT    * ,
                                                CASE WHEN [SUM a X b]
                                                          / SQRT(CAST([SUM a squared]
                                                              * [SUM b squared] AS FLOAT)) > .60
                                                          AND likely_gas_heat = 'True'
                                                     THEN 1
                                                     ELSE 0
                                                END AS has_gas_heat ,

					--not email
                                                CASE WHEN @channel <> 'email'
                                                          AND ( SQRT(CAST([SUM a squared]
                                                              * [SUM b squared] AS FLOAT)) = 0 )
                                                     THEN @promo_code_1 -- use variable promo_code 1 which is for non heating customers
                                                     WHEN @channel <> 'email'
                                                          AND ( [SUM a X b]
                                                              / SQRT(CAST([SUM a squared]
                                                              * [SUM b squared] AS FLOAT)) > .60
                                                              AND likely_gas_heat = 'True'
                                                              )
                                                     THEN @promo_code_3 -- use variable promo_code 3 which is for heating customers
                                                     WHEN @channel <> 'email'
                                                          AND ( [SUM a X b]
                                                              / SQRT(CAST([SUM a squared]
                                                              * [SUM b squared] AS FLOAT)) <= .60
                                                              OR likely_gas_heat != 'True'
                                                              )
                                                     THEN @promo_code_1 -- use variable promo_code 1 which is for non heating customers

					--email
                                                     WHEN @channel = 'email'
                                                     THEN @promo_code_5  -- use variable promo_code 5 which is for email channel customers
					-- default
                                                     ELSE @promo_code_5  -- use variable promo_code 5
                                                END AS PromoCode1 ,
						  
			 -- promo code column 2
			 						  
				    -- not email
                                                CASE WHEN @channel <> 'email'
                                                          AND ( SQRT(CAST([SUM a squared]
                                                              * [SUM b squared] AS FLOAT)) = 0 )
                                                     THEN @promo_code_2 -- use variable promo_code 2 which is for non heating customers
                                                     WHEN @channel <> 'email'
                                                          AND ( [SUM a X b]
                                                              / SQRT(CAST([SUM a squared]
                                                              * [SUM b squared] AS FLOAT)) > .60
                                                              AND likely_gas_heat = 'True'
                                                              )
                                                     THEN @promo_code_4 -- use variable promo_code 4 which is for heating customers
                                                     WHEN @channel <> 'email'
                                                          AND ( [SUM a X b]
                                                              / SQRT(CAST([SUM a squared]
                                                              * [SUM b squared] AS FLOAT)) <= .60
                                                              OR likely_gas_heat != 'True'
                                                              )
                                                     THEN @promo_code_2 -- use variable promo_code 2 which is for non heating customers
				    --email
                                                     WHEN @channel = 'email'
                                                     THEN NULL -- use variable promo_code 5 which is for email channel customers
				    --default
                                                     ELSE NULL -- email only uses 1 promo code
                                                END AS PromoCode2 ,
                                                ROW_NUMBER() OVER ( PARTITION BY PremiseId ORDER BY sumcte2.PremiseId ) AS rnk
                                      FROM      sumcte2
                                    ) o
                           WHERE    rnk = 1
                         )
            --update the working table
		  /*
		  TDDO  remove hard coding and replace with join or something
		  */
	UPDATE  InsightsDW.[Export].[home_energy_report_staging_allcolumns]
    SET     ImproveinsulationSavings = CASE WHEN c.PromoCode1 = 'F'
                                                 OR c.PromoCode2 = 'F'
                                            THEN c.ImproveInsulationSavings
                                            ELSE NULL
                                       END ,
            NewwaterheaterSavings = CASE WHEN c.PromoCode1 = 'C'
                                              OR c.PromoCode2 = 'C'
                                              OR c.PromoCode1 = 'D'
                                              OR c.PromoCode2 = 'D'
                                         THEN c.NewwaterheaterSavings
                                         ELSE NULL
                                    END ,
            InsulationRebateSavings = CASE WHEN c.PromoCode1 = 'F'
                                                OR c.PromoCode2 = 'F'
                                           THEN CAST(( 5 * ROUND(( 0.15
                                                              * CAST(c.house_size AS DECIMAL(18,
                                                              2)) ) / 5, 0) ) AS INT)
                                           ELSE NULL
                                      END ,
            PromoCode1 = c.PromoCode1 ,
            PromoCode2 = c.PromoCode2
    FROM    InsightsDW.[Export].[home_energy_report_staging_allcolumns] a
            INNER JOIN onerow c ON c.PremiseId = a.PremiseID;

 

/*
--	WRITE TO Promo HISTORY
*/
        DECLARE @history_date DATETIME = GETDATE();

--delete any for this period that are in our table
        DELETE  FROM [Export].[HE_Report_CSV_Promo_History]
        WHERE   profile_name = @profile_name
                AND bill_month = @bill_month
                AND bill_year = @bill_year
                AND EXISTS ( SELECT PremiseID
                             FROM   [Export].[home_energy_report_staging_allcolumns] a1
                             WHERE  a1.PremiseID = [Export].[HE_Report_CSV_Promo_History].PremiseID );

        INSERT  [Export].[HE_Report_CSV_Promo_History]
                ( profile_name ,
                  PremiseID ,
                  Promo_Letter ,
                  history_date ,
                  bill_year ,
                  bill_month
                )
                SELECT  @profile_name ,
                        PremiseID ,
                        PromoCode1 ,
                        @history_date ,
                        @bill_year ,
                        @bill_month
                FROM    [Export].[home_energy_report_staging_allcolumns] a1
                WHERE   COALESCE(a1.PromoCode1, '') <> '';
		
        INSERT  [Export].[HE_Report_CSV_Promo_History]
                ( profile_name ,
                  PremiseID ,
                  Promo_Letter ,
                  history_date ,
                  bill_year ,
                  bill_month
                )
                SELECT  @profile_name ,
                        PremiseID ,
                        PromoCode2 ,
                        @history_date ,
                        @bill_year ,
                        @bill_month
                FROM    [Export].[home_energy_report_staging_allcolumns] a1
                WHERE   COALESCE(a1.PromoCode2, '') <> '';
	
    END;
GO
