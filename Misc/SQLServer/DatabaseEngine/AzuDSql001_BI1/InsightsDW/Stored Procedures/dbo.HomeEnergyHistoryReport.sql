SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/****** 

special he report - history report for jill

 ******/
 CREATE PROC [dbo].[HomeEnergyHistoryReport]
 AS
 begin
IF OBJECT_ID('dbo.HomeEnergyHistoryReport', 'U') IS NOT NULL
begin
    PRINT 'dropped'
    DROP TABLE InsightsBulk.dbo.HomeEnergyHistoryReport
end

SELECT  t.premiseid AS PREMISE_ID, t.accountid AS ACCOUNT_ID, t.customerid AS CUSTOMER_ID,t.treatmentgroup AS TREATMENT_GROUP, 
--PRINT
CASE WHEN NULLIF(p11.[MyHomeGUsage1],'') IS NOT NULL THEN 'YES' ELSE 'NO' END AS '2014_11_PRINT',									-- for print say NO if the usage column is blank
CASE WHEN NULLIF(p11.[AverageHomeGUsage1],'') IS NOT NULL THEN 'YES' ELSE 'NO' END AS '2014_11_PRINT_Neighborhood Comparison Included',	-- for print say NO if the average column is blank
'NO' AS '2014_11_PRINT_Annual Usage Comparison Included',
'NO' AS '2014_11_PRINT_Daily AMI Usage Included',
p11.[MyHomeGUsage1]		   AS '2014_11_PRINT_customer_usage',
p11.[AverageHomeGUsage1]	   AS '2014_11_PRINT_average_usage',
p11.[EfficientHomeGUsage1]  AS '2014_11_PRINT_efficient_usage',
p11.[MyHomeGUsage13]	   AS '2014_11_PRINT_customer_last year__usage',
--
-- EMAIL
CASE WHEN NULLIF(e11.[MyHomeGUsage1],'') IS NOT NULL THEN 'YES' ELSE 'NO' END AS  '2014_11_EMAIL',
CASE WHEN NULLIF(e11.[AverageHomeGUsage1],'') IS NOT NULL THEN 'YES' ELSE 'NO' END AS '2014_11_EMAIL_Neighborhood Comparison Included',  
'NO' AS '2014_11_EMAIL_Annual Usage Comparison Included',
'NO' AS '2014_11_EMAIL_Daily AMI Usage Included',
e11.[MyHomeGUsage1]		   AS '2014_11_EMAIL_customer_usage',
e11.[AverageHomeGUsage1]	   AS '2014_11_EMAIL_average_usage',
e11.[EfficientHomeGUsage1]  AS '2014_11_EMAIL_efficient_usage',
e11.[MyHomeGUsage13]	   AS '2014_11_EMAIL_customer_last year__usage',
--
--==================================================================================================================================
--PRINT
CASE WHEN NULLIF(p12.[MyHomeGUsage1],'')  IS NOT NULL THEN 'YES' ELSE 'NO' END AS '2014_12_PRINT',									-- for print say NO if the usage column is blank
'NO'	   AS '2014_12_PRINT_Neighborhood Comparison Included',     -- for print say NO if the average column is blank
'YES'   AS '2014_12_PRINT_Annual Usage Comparison Included',
'NO'	   AS '2014_12_PRINT_Daily AMI Usage Included',
p12.[MyHomeGUsage1]		   AS '2014_12_PRINT_customer_usage',
p12.[AverageHomeGUsage1]	   AS '2014_12_PRINT_average_usage',
p12.[EfficientHomeGUsage1]  AS '2014_12_PRINT_efficient_usage',
p12.[MyHomeGUsage13]	   AS '2014_12_PRINT_customer_last year__usage',
--
-- EMAIL
CASE WHEN NULLIF(e12.[MyHomeGUsage1],'')  IS NOT NULL THEN 'YES' ELSE 'NO' END AS  '2014_12_EMAIL',
'NO'	   AS '2014_12_EMAIL_Neighborhood Comparison Included',
'YES'   AS '2014_12_EMAIL_Annual Usage Comparison Included',
'NO'	   AS '2014_12_EMAIL_Daily AMI Usage Included',
e12.[MyHomeGUsage1]		   AS '2014_12_EMAIL_customer_usage',
e12.[AverageHomeGUsage1]	   AS '2014_12_EMAIL_average_usage',
e12.[EfficientHomeGUsage1]  AS '2014_12_EMAIL_efficient_usage',
e12.[MyHomeGUsage13]	   AS '2014_12_EMAIL_customer_last year__usage',
--
--==================================================================================================================================
-- PRINT
CASE WHEN NULLIF(p01.[MyHomeGUsage1],'')  IS NOT NULL THEN 'YES' ELSE 'NO' END AS '2015_01_PRINT',									-- for print say NO if the usage column is blank
CASE WHEN NULLIF(p01.[AverageHomeGUsage1],'') IS NOT NULL THEN 'YES' ELSE 'NO' END AS '2015_01_PRINT_Neighborhood Comparison Included',     -- for print say NO if the average column is blank
'YES' AS '2015_01_PRINT_Annual Usage Comparison Included',
'YES' AS '2015_01_PRINT_Daily AMI Usage Included',
p01.[MyHomeGUsage1]		   AS '2015_01_PRINT_customer_usage',
p01.[AverageHomeGUsage1]	   AS '2015_01_PRINT_average_usage',
p01.[EfficientHomeGUsage1]  AS '2015_01_PRINT_efficient_usage',
p01.[MyHomeGUsage13]	   AS '2015_01_PRINT_customer_last year__usage',
--
-- EMAIL
CASE WHEN NULLIF(e01.[MyHomeGUsage1],'')  IS NOT NULL THEN 'YES' ELSE 'NO' END AS '2015_01_EMAIL',
CASE WHEN NULLIF(e01.[AverageHomeGUsage1],'') IS NOT NULL THEN 'YES' ELSE 'NO' END AS '2015_01_EMAIL_Neighborhood Comparison Included',
'NO'	   AS '2015_01_EMAIL_Annual Usage Comparison Included',
'YES'   AS '2015_01_EMAIL_Daily AMI Usage Included',
e01.[MyHomeGUsage1]		   AS '2015_01_EMAIL_customer_usage',
e01.[AverageHomeGUsage1]	   AS '2015_01_EMAIL_average_usage',
e01.[EfficientHomeGUsage1]  AS '2015_01_EMAIL_efficient_usage',
e01.[MyHomeGUsage13]	   AS '2015_01_EMAIL_customer_last year__usage',
--
--==================================================================================================================================
-- PRINT
CASE WHEN NULLIF(p02.[MyHomeGUsage1],'')  IS NOT NULL THEN 'YES' ELSE 'NO' END AS '2015_02_PRINT',									-- for print say NO if the usage column is blank
CASE WHEN NULLIF(p02.[AverageHomeGUsage1],'') IS NOT NULL THEN 'YES' ELSE 'NO' END AS '2015_02_PRINT_Neighborhood Comparison Included',	-- for print say NO if the average column is blank
'YES' AS '2015_02_PRNTL_Annual Usage Comparison Included',
'YES' AS '2015_02_PRINT_Daily AMI Usage Included',
p02.[MyHomeGUsage1]		   AS '2015_02_PRINT_customer_usage',
p02.[AverageHomeGUsage1]	   AS '2015_02_PRINT_average_usage',
p02.[EfficientHomeGUsage1]  AS '2015_02_PRINT_efficient_usage',
p02.[MyHomeGUsage13]	   AS '2015_02_PRINT_customer_last year__usage',
--
-- EMAIL
CASE WHEN NULLIF(e02.[MyHomeGUsage1],'')  IS NOT NULL THEN 'YES' ELSE 'NO' END '2015_02_EMAIL',
CASE WHEN NULLIF(e02.[AverageHomeGUsage1],'') IS NOT NULL THEN 'YES' ELSE 'NO' END AS  '2015_02_EMAIL_Neighborhood Comparison Included',
'NO'	   AS '2015_02_EMAIL_Annual Usage Comparison Included',
'YES'   AS '2015_02_EMAIL_Daily AMI Usage Included',
e02.[MyHomeGUsage1]		   AS '2015_02_EMAIL_customer_usage',
e02.[AverageHomeGUsage1]	   AS '2015_02_EMAIL_average_usage',
e02.[EfficientHomeGUsage1]  AS '2015_02_EMAIL_efficient_usage',
e02.[MyHomeGUsage13]	   AS '2015_02_EMAIL_customer_last year__usage',
--
--==================================================================================================================================
--
--EMAIL
CASE WHEN e03.premiseid IS NOT NULL THEN 'YES' ELSE 'NO' END AS '2015_03_EMAIL',
'NO'	   AS '2015_03_EMAIL_Neighborhood Comparison Included',  --always no
'YES'   AS '2015_03_EMAIL_Annual Usage Comparison Included',
'NO'	   AS '2015_03_EMAIL_Daily AMI Usage Included',
e03.[MyHomeGUsage1]		   AS '2015_03_EMAIL_customer_usage',
e03.[AverageHomeGUsage1]	   AS '2015_03_EMAIL_average_usage',
e03.[EfficientHomeGUsage1]  AS '2015_03_EMAIL_efficient_usage',
e03.[MyHomeGUsage13]	   AS '2015_03_EMAIL_customer_last year__usage',
--
--==================================================================================================================================
--
-- EMAIL
CASE WHEN e04.premiseid IS NOT NULL THEN 'YES' ELSE 'NO' END AS '2015_04_EMAIL',
CASE WHEN NULLIF(e04.[AverageHomeGUsage1],'') IS NOT NULL THEN 'YES' ELSE 'NO' END AS '2015_04_EMAIL_Neighborhood Comparison Included',
'NO'  AS '2015_04_EMAIL_Annual Usage Comparison Included',
'YES' AS '2015_04_EMAIL_Daily AMI Usage Included',
e04.[MyHomeGUsage1]		   AS '2015_04_EMAIL_customer_usage',
e04.[AverageHomeGUsage1]	   AS '2015_04_EMAIL_average_usage',
e04.[EfficientHomeGUsage1]  AS '2015_04_EMAIL_efficient_usage',
e04.[MyHomeGUsage13]	   AS '2015_04_EMAIL_customer_last year__usage',
--
--==================================================================================================================================
--
-- EMAIL
CASE WHEN e05.premiseid IS NOT NULL THEN 'YES' ELSE 'NO' END AS '2015_05_EMAIL',
'NO'  AS '2015_05_EMAIL_Neighborhood Comparison Included', --always no
'YES' AS '2015_05_EMAIL_Annual Usage Comparison Included',
'NO'  AS '2015_05_EMAIL_Daily AMI Usage Included',
e05.[MyHomeGUsage1]		   AS '2015_05_EMAIL_customer_usage',
e05.[AverageHomeGUsage1]	   AS '2015_05_EMAIL_average_usage',
e05.[EfficientHomeGUsage1]  AS '2015_05_EMAIL_efficient_usage',
e05.[MyHomeGUsage13]	   AS '2015_05_EMAIL_customer_last year__usage',
--
--==================================================================================================================================
--
-- EMAIL
CASE WHEN e06.premiseid IS NOT NULL THEN 'YES' ELSE 'NO' END AS '2015_06_EMAIL',
'NO'	   AS '2015_06_EMAIL_Neighborhood Comparison Included',  --always no
'YES'   AS '2015_06_EMAIL_Annual Usage Comparison Included',
'YES'   AS '2015_06_EMAIL_Daily AMI Usage Included',
e06.[MyHomeGUsage1]		   AS '2015_06_EMAIL_customer_usage',
e06.[AverageHomeGUsage1]	   AS '2015_06_EMAIL_average_usage',
e06.[EfficientHomeGUsage1]  AS '2015_06_EMAIL_efficient_usage',
e06.[MyHomeGUsage13]	   AS '2015_06_EMAIL_customer_last year__usage'
--
--==================================================================================================================================
--
INTO InsightsBulk.dbo.HomeEnergyHistoryReport
  FROM [InsightsDW].[dbo].[vScgTreatmentGroupsWithCust] t
LEFT OUTER JOIN InsightsBulk.[dbo].[2014_11_PRINT] p11 ON p11.premiseid=t.premiseid AND p11.account_number=t.AccountId AND p11.customerid=t.customerid
LEFT OUTER JOIN InsightsBulk.[dbo].[2014_11_EMAIL] e11 ON e11.premiseid=t.premiseid AND e11.account_number=t.AccountId AND e11.customerid=t.customerid
LEFT OUTER JOIN InsightsBulk.[dbo].[2014_12_EMAIL] e12 ON e12.premiseid=t.premiseid AND e12.account_number=t.AccountId AND e12.customerid=t.customerid
LEFT OUTER JOIN InsightsBulk.[dbo].[2014_12_PRINT] p12 ON p12.premiseid=t.premiseid AND p12.account_number=t.AccountId AND p12.customerid=t.customerid
LEFT OUTER JOIN InsightsBulk.[dbo].[2015_01_EMAIL] e01 ON e01.premiseid=t.premiseid AND e01.account_number=t.AccountId AND e01.customerid=t.customerid
LEFT OUTER JOIN InsightsBulk.[dbo].[2015_01_PRINT] p01 ON p01.premiseid=t.premiseid AND p01.account_number=t.AccountId AND p01.customerid=t.customerid
LEFT OUTER JOIN InsightsBulk.[dbo].[2015_02_EMAIL] e02 ON e02.premiseid=t.premiseid AND e02.account_number=t.AccountId AND e02.customerid=t.customerid
LEFT OUTER JOIN InsightsBulk.[dbo].[2015_02_PRINT] p02 ON p02.premiseid=t.premiseid AND p02.account_number=t.AccountId AND p02.customerid=t.customerid
LEFT OUTER JOIN InsightsBulk.[dbo].[2015_03_EMAIL] e03 ON e03.premiseid=t.premiseid AND e03.account_number=t.AccountId AND e03.customerid=t.customerid
LEFT OUTER JOIN InsightsBulk.[dbo].[2015_04_EMAIL] e04 ON e04.premiseid=t.premiseid AND e04.account_number=t.AccountId AND e04.customerid=t.customerid
LEFT OUTER JOIN InsightsBulk.[dbo].[2015_05_EMAIL] e05 ON e05.premiseid=t.premiseid AND e05.account_number=t.AccountId AND e05.customerid=t.customerid
LEFT OUTER JOIN InsightsBulk.[dbo].[2015_06_EMAIL] e06 ON e06.premiseid=t.premiseid AND e06.account_number=t.AccountId AND e06.customerid=t.customerid
         
END
GO
