SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 9/10/2014
-- Description:	
-- =============================================
create PROCEDURE [ETL].[D_KEY_FactPremiseAttribute]
				 @ClientID INT
AS

BEGIN

	SET NOCOUNT ON;

	DELETE FROM ETL.KEY_FactPremiseAttribute WHERE ClientID = @ClientID

END





GO
