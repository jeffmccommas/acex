SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[LogBenchmarkError]
    @PremiseKey AS INT ,
    @PeerGroupTypeKey AS INT ,
    @StartDateKey AS INT = NULL ,
    @EndDateKey AS INT = NULL ,
    @FuelKey AS INT = NULL ,
    @ErrorCode AS TINYINT ,
    @ErrorInfo AS VARCHAR(200) = NULL
AS 
    BEGIN
        SET NOCOUNT ON;

        INSERT  INTO [dbo].[BenchmarkErrorLog]
                ( [PremiseKey] ,
                  [PeerGroupTypeKey] ,
                  [StartDateKey] ,
                  [EndDateKey] ,
                  [FuelKey] ,
                  [ErrorCode] ,
                  [ErrorInfo]
                )
        VALUES  ( @PremiseKey ,
                  @PeerGroupTypeKey ,
                  @StartDateKey ,
                  @EndDateKey ,
                  @FuelKey ,
                  @ErrorCode ,
                  @ErrorInfo
                )

    END
GO
