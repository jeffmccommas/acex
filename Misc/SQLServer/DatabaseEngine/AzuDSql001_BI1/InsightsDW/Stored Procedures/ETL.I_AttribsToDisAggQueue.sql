SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Wayne
-- Create date:	4/6/2015
-- Description:	insure a disagg recalc for customers who
--				have changed profile attributes in Insights
-- 
-- =============================================
-- Author:		Wayne
-- Create date:	6/20/2015
-- Description:	changed to use a differnt attribute in 15.06 - calculateanalytics
--				also require a new view vcalculateanalyticsFlagTrue
-- =============================================
CREATE PROCEDURE [ETL].[I_AttribsToDisAggQueue]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;

		MERGE INTO InsightsBulk.[Holding].[DisAggReCalcQueue] AS T
		USING (SELECT a.ClientId,a.PremiseId, a.AccountId, a.CustomerId, a.SourceId, a.TrackingId, a.TrackingDate FROM 
			  (
				  SELECT DISTINCT  dp.PremiseKey, dp.ClientId,dp.PremiseId, dp.AccountId, c.CustomerId, p.SourceId, p.TrackingId, p.TrackingDate,
				  ROW_NUMBER() OVER(PARTITION BY dp.PremiseId, dp.AccountId, c.CustomerId ORDER BY p.TrackingDate DESC) AS latest 

				  FROM [InsightsBulk].[Holding].[v_PremiseAttributes] p
				  INNER JOIN InsightsDW.dbo.DimPremise dp
				  ON dp.ClientId = p.ClientId AND dp.PremiseId = p.PremiseId AND  dp.AccountId = p.AccountId 
				   INNER JOIN	(
								  SELECT PremiseKey, CustomerKey, 
								  ROW_NUMBER() OVER(PARTITION BY premisekey ORDER BY datecreated DESC) AS rnk 
								  FROM
								  [InsightsDW].[dbo].[FactCustomerPremise] fcp
							  ) AS fcp 	
					ON dp.PremiseKey=fcp.premisekey AND rnk=1
					INNER JOIN	[InsightsDW].[dbo].[DimCustomer] c	 			
					ON c.customerkey=fcp.customerkey AND rnk=1
				  WHERE p.ClientId = @ClientID  --AND p.SourceId IN (4,5,6)
		) AS a				
		INNER JOIN insightsdw.[dbo].[vcalculateanalyticsFlagTrue] d
		ON d.premisekey=a.premisekey
		WHERE latest=1
		) AS s
	   ON s.ClientId  = t.clientid AND s.PremiseId = t.PremiseId AND  s.AccountId = t.AccountId 
	   WHEN MATCHED THEN UPDATE SET SourceId=  s.SourceId, TrackingId= s.TrackingId, TrackingDate=s.TrackingDate
	   WHEN NOT MATCHED THEN INSERT (
							  [ClientId]
							   ,[PremiseId]
							   ,[AccountId]
							   ,[CustomerId]
							   ,[SourceId]
							   ,[TrackingId]
							   ,[TrackingDate]
					   )
					   VALUES
                            (
						  s.ClientId,s.PremiseId, s.AccountId, s.CustomerId, s.SourceId, s.TrackingId, s.TrackingDate
					   );
					   
END;






GO
