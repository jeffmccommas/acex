SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 6/13/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_FactBilling] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        UPDATE  ifb
        SET     ServiceContractId = kfb.ServiceContractId
        FROM    ETL.INS_FactBilling ifb WITH ( NOLOCK )
                INNER JOIN ETL.KEY_FactBilling kfb WITH ( NOLOCK ) ON kfb.ClientId = ifb.ClientId
                                                              AND kfb.AccountId = ifb.AccountId
                                                              AND kfb.PremiseId = ifb.PremiseId
                                                              AND kfb.CommodityId = ifb.CommodityId
        WHERE   ifb.ServiceContractId = '';

        INSERT  INTO dbo.FactServicePointBilling
                ( ServiceContractKey ,
                  BillPeriodStartDateKey ,
                  BillPeriodEndDateKey ,
                  UOMKey ,
                  BillPeriodTypeKey ,
                  ClientId ,
                  PremiseId ,
                  AccountId ,
                  CommodityId ,
                  BillPeriodTypeId ,
                  UOMId ,
                  StartDate ,
                  EndDate ,
                  TotalUsage ,
                  CostOfUsage ,
                  OtherCost ,
                  RateClassKey1 ,
                  RateClassKey2 ,
                  DueDate ,
                  ReadDate ,
                  CreateDate ,
                  UpdateDate ,
                  BillCycleScheduleId ,
                  BillDays ,
                  ReadQuality ,
                  ETL_LogId ,
                  TrackingId ,
                  TrackingDate ,
                  SourceKey ,
                  SourceId
                )
                SELECT  dsc.ServiceContractKey ,
                        dsd.DateKey ,
                        ded.DateKey ,
                        duom.UOMKey ,
                        dbpt.BillPeriodTypeKey ,
                        fb.ClientId ,
                        fb.PremiseId ,
                        fb.AccountId ,
                        fb.CommodityId ,
                        fb.BillPeriodTypeId ,
                        fb.UOMId ,
                        StartDate ,
                        EndDate ,
                        fb.TotalUnits ,
                        fb.TotalCost ,
                        NULL ,
                        drc.RateClassKey ,
                        NULL ,
                        fb.DueDate ,
                        fb.ReadDate ,
                        GETUTCDATE() ,
                        GETUTCDATE() ,
                        fb.BillCycleScheduleId ,
                        fb.BillDays ,
                        fb.ReadQuality ,
                        fb.ETL_LogId ,
                        fb.TrackingId ,
                        fb.TrackingDate ,
                        ds.SourceKey ,
                        fb.SourceId
                FROM    ETL.INS_FactBilling fb WITH ( NOLOCK )
                        INNER JOIN dbo.DimPremise dp WITH ( NOLOCK ) ON dp.PremiseId = fb.PremiseId
                                                              AND dp.AccountId = fb.AccountId
                                                              AND dp.ClientId = fb.ClientId
                        INNER JOIN dbo.DimServiceContract dsc ON dsc.PremiseKey = dp.PremiseKey
                                                              AND dsc.CommodityKey = fb.CommodityId
                                                              AND dsc.ServiceContractId = fb.ServiceContractId
                        INNER JOIN dbo.DimUOM duom WITH ( NOLOCK ) ON duom.UOMId = fb.UOMId
                        INNER JOIN dbo.DimBillPeriodType dbpt WITH ( NOLOCK ) ON dbpt.BillPeriodTypeId = fb.BillPeriodTypeId
                        INNER JOIN dbo.DimDate dsd WITH ( NOLOCK ) ON dsd.FullDateAlternateKey = fb.StartDate
                        INNER JOIN dbo.DimDate ded WITH ( NOLOCK ) ON ded.FullDateAlternateKey = fb.EndDate
                        INNER JOIN dbo.DimSource ds ON ds.SourceId = fb.SourceId
                        LEFT JOIN dbo.DimRateClass drc ON drc.ClientId = @ClientID
                                                          AND drc.RateClassName = fb.RateClass
                WHERE   fb.ClientId = @ClientID;

    END;

														


GO
