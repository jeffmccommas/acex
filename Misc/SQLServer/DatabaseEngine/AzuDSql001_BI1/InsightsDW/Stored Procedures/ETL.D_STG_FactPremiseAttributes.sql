SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/16/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[D_STG_FactPremiseAttributes]
				 @ClientID INT
AS

BEGIN

	SET NOCOUNT ON;

	DELETE ETL.INS_FactPremiseAttribute WHERE ClientID = @ClientID

END





GO
