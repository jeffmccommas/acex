
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 6/13/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_INF_FactBilling_DimMeter]
    @ETL_LogId INT ,
    @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #Meters
            (
              PremiseKey INT ,
              CommodityId INT ,
              CommodityDesc VARCHAR(20) NULL ,
              MeterId VARCHAR(64) NULL ,
              MeterType VARCHAR(64) NULL ,
              ReplacedMeterId VARCHAR(64) NULL ,
              ServicePointId VARCHAR(64) NULL ,
              SourceId INT ,
              PremiseId VARCHAR(50) ,
              TrackingId VARCHAR(50) ,
              TrackingDate DATETIME ,
              IsInferred BIT
            );

        CREATE NONCLUSTERED INDEX IX_M ON #Meters (PremiseKey, CommodityId, ServicePointId, MeterId );

        CREATE TABLE #ServicePointMeters
            (
              PremiseKey INT ,
              CommodityId INT ,
              MeterId VARCHAR(64) NULL ,
              MeterType VARCHAR(64) NULL ,
              ReplacedMeterId VARCHAR(64) NULL ,
              SourceId INT ,
              TrackingId VARCHAR(50) ,
              TrackingDate DATETIME ,
              IsInferred BIT ,
              ServicePointKey INT
            );

        CREATE NONCLUSTERED INDEX IX_SPM ON #ServicePointMeters (ServicePointKey, CommodityId, MeterId, MeterType );

        INSERT  INTO #Meters
                SELECT  a.PremiseKey ,
                        a.CommodityId ,
                        a.CommodityDesc ,
                        a.MeterId ,
                        a.MeterType ,
                        a.ReplacedMeterId ,
                        a.ServicePointId ,
                        a.SourceId ,
                        a.PremiseId ,
                        a.TrackingId ,
                        a.TrackingDate ,
                        a.IsInferred
                FROM    ( SELECT    p.PremiseKey ,
                                    dc.CommodityId ,
                                    dc.CommodityDesc ,
                                    kfb.MeterId ,
                                    kfb.MeterType ,
                                    kfb.ReplacedMeterId ,
                                    kfb.ServicePointId ,
                                    kfb.SourceId ,
                                    kfb.PremiseId ,
                                    kfb.TrackingId ,
                                    kfb.TrackingDate ,
                                    0 AS IsInferred ,
                                    ROW_NUMBER() OVER ( PARTITION BY p.PremiseKey,
                                                        kfb.CommodityId,
                                                        kfb.ServicePointId,
                                                        kfb.MeterId ORDER BY kfb.TrackingDate DESC ) AS SortId
                          FROM      ETL.KEY_FactBilling kfb
                                    INNER JOIN dbo.DimCommodity dc ON dc.CommodityId = kfb.CommodityId
                                    INNER JOIN dbo.DimPremise p ON p.ClientId = kfb.ClientId
                                                              AND p.AccountId = kfb.AccountId
                                                              AND p.PremiseId = kfb.PremiseId
                          WHERE     kfb.ClientId = @ClientID
                        ) a
                WHERE   a.SortId = 1;

        UPDATE  m
        SET     MeterId = 'M_' + ServicePointId + '_' + CommodityDesc ,
                IsInferred = 1
        FROM    #Meters m
        WHERE   MeterId IS NULL;
		
        INSERT  INTO dbo.DimMeterType
                ( MeterTypeDescription ,
                  MeterTypeName
		        )
                SELECT  MeterType ,
                        MeterType
                FROM    ( SELECT DISTINCT
                                    MeterType
                          FROM      #Meters
                          WHERE     MeterType IS NOT NULL
                        ) mt
                        LEFT	JOIN dbo.DimMeterType dmt ON dmt.MeterTypeName = mt.MeterType
                WHERE   dmt.MeterTypeKey IS NULL;

        INSERT  INTO #ServicePointMeters
                SELECT  m.PremiseKey ,
                        m.CommodityId ,
                        m.MeterId ,
                        m.MeterType ,
                        m.ReplacedMeterId ,
                        m.SourceId ,
                        m.TrackingId ,
                        m.TrackingDate ,
                        m.IsInferred ,
                        dsp.ServicePointKey
                FROM    #Meters m
                        INNER JOIN dbo.DimServicePoint dsp ON dsp.ClientId = @ClientID
                                                              AND dsp.PremiseKey = m.PremiseKey
                                                              AND dsp.ServicePointId = m.ServicePointId;
                     
        INSERT  INTO dbo.DimMeter
                ( ServicePointKey ,
                  ClientId ,
                  CommodityKey ,
                  MeterId ,
                  MeterTypeKey ,
                  ReplacedMeterId ,
                  CreateDate ,
                  UpdateDate ,
                  SourceId ,
                  TrackingId ,
                  TrackingDate ,
                  IsInferred
					
                )
                SELECT  spm.ServicePointKey ,
                        @ClientID ,
                        spm.CommodityId ,
                        spm.MeterId ,
                        dmt.MeterTypeKey ,
                        spm.ReplacedMeterId ,
                        GETUTCDATE() ,
                        GETUTCDATE() ,
                        spm.SourceId ,
                        spm.TrackingId ,
                        spm.TrackingDate ,
                        spm.IsInferred
                FROM    #ServicePointMeters spm
                        LEFT JOIN dbo.DimMeter dm ON dm.ClientId = @ClientID
                                                     AND dm.ServicePointKey = spm.ServicePointKey
                                                     AND dm.CommodityKey = spm.CommodityId
                                                     AND dm.MeterId = spm.MeterId
                        LEFT JOIN dbo.DimMeterType dmt ON dmt.MeterTypeName = spm.MeterType
                WHERE   dm.MeterKey IS NULL;

        UPDATE  dm
        SET     IsDeleted = 1
        FROM    #ServicePointMeters m
                INNER JOIN dbo.DimMeter dm ON dm.ClientId = @ClientID
                                              AND dm.ServicePointKey = m.ServicePointKey
                                              AND dm.CommodityKey = m.CommodityId
                                              AND dm.MeterId = m.ReplacedMeterId
        WHERE   m.ReplacedMeterId IS NOT NULL;

        DROP TABLE #Meters;
        DROP TABLE #ServicePointMeters;

    END;

														 



GO
