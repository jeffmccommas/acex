
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jason Khourie
-- Create date: 8/28/2014
-- Description:	
-- Change Date  Description
-- 9/24/2014   Change join per suggestion from fpa.ClientId to dc.ClientId since already index on clientid, customerid on DimCustomer
-- =============================================
CREATE PROCEDURE [dbo].[GetProfilePremiseAttributes]
    @ClientID INT ,
    @CustomerID VARCHAR(50)
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT  AccountId
        FROM    dbo.DimPremise dp WITH ( NOLOCK )
                INNER JOIN dbo.FactCustomerPremise fcp WITH ( NOLOCK ) ON dp.PremiseKey = fcp.PremiseKey
                INNER JOIN dbo.DimCustomer dc WITH ( NOLOCK ) ON dc.CustomerKey = fcp.CustomerKey
        WHERE   dc.ClientId = @ClientID
                AND dc.CustomerId = @CustomerID
        GROUP BY AccountId;

        SELECT  *
        FROM    ( SELECT    fpa.ClientID ,
                            dc.CustomerId ,
                            fpa.PremiseID ,
                            fpa.AccountID ,
                            ContentAttributeKey AS AttributeKey ,
                            Value ,
                            ds.SourceDesc AS Source ,
                            fpa.EffectiveDate ,
                            ROW_NUMBER() OVER ( PARTITION BY fpa.PremiseKey,
                                                ContentAttributeKey ORDER BY EffectiveDate DESC ) AS SortId
                  FROM      InsightsDW.dbo.FactPremiseAttribute fpa WITH ( NOLOCK )
                            INNER JOIN dbo.DimPremise dp ON dp.PremiseKey = fpa.PremiseKey
                            INNER JOIN dbo.FactCustomerPremise fcp ON fcp.PremiseKey = dp.PremiseKey
                            INNER JOIN dbo.DimCustomer dc ON dc.CustomerKey = fcp.CustomerKey
                            INNER JOIN dbo.DimSource ds ON ds.SourceKey = fpa.SourceKey
                  WHERE     dc.ClientId = @ClientID
                            AND dc.CustomerId = @CustomerID
                ) p
        WHERE   SortId = 1
        ORDER BY p.ClientID ,
                p.PremiseID ,
                p.AccountID ,
                p.AttributeKey;
		 


    END;






GO
