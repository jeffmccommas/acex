SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ubaid
-- Create date: 6/18/2015
-- Description:	Script to read value of client attribute
-- =============================================
CREATE PROCEDURE [dbo].[GetClientAttributeValue]
    @ClientId AS INT ,
    @AttributeName AS VARCHAR(256) ,
    @DefaultValue AS VARCHAR(256) = NULL
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @AttributeValue AS VARCHAR(256);
    
        WITH    Result
                  AS ( SELECT   AttributeValue ,
                                ROW_NUMBER() OVER ( PARTITION BY AttributeName ORDER BY EffectiveDate DESC ) AS SortId
                       FROM     [factClientAttribute]
                       WHERE    clientid = @ClientId
                                AND AttributeName = @AttributeName
                     )
            SELECT  @AttributeValue = AttributeValue
            FROM    Result
            WHERE   Result.SortId = 1;

        SELECT  ISNULL(@AttributeValue, @DefaultValue) AS AttributeValue;

    END;

GO
