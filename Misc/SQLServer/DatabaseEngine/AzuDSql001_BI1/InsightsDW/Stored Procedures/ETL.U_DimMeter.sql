SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 6/13/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[U_DimMeter]
				 @ETL_LogId INT,
				 @ClientID INT,
				 @MeterID INT,
				 @RateClass VARCHAR(256)

AS

BEGIN

SET NOCOUNT ON;

	UPDATE [dbo].[DimMeter] 
	SET RateClass = @RateClass,
		IsInferred = 0
	WHERE ClientId = @ClientID 
			AND ServicePointId = @MeterID
	

END

														 



GO
