SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[LoadHoldingTables] 
    @ObfuscateData BIT,  
	@DateToLoad DATETIME = NULL
AS
BEGIN


IF OBJECT_ID('tempdb..#tempbilldata') IS NOT NULL
    DROP TABLE #tempbilldata

IF OBJECT_ID('tempdb..#CustomerBills') IS NOT NULL
    DROP TABLE #CustomerBills

IF OBJECT_ID('tempdb..#tmpUsers') IS NOT NULL
    DROP TABLE #tmpusers

CREATE TABLE #tmpusers
(
	clientid int,
	customerid varchar(50),
	isauthenticated INT,
	isbusiness INT,
	facilityid INT,
	esoid INT,
	accountid VARCHAR(50)
    
)


--residential authenticated
INSERT INTO #tmpusers (clientid,customerid,isauthenticated,isbusiness,esoid,accountid)
       select u.clientid,u.customerid, 1,0,u.esoid,u.accountid
       from hubdynamic.dbo.users u WITH (NOLOCK)
	   inner join hubdynamic.dbo.flags f
              on u.id = f.userid
       where f.flagtypeid = 10
	   and f.AppId = 0
	   AND u.clientid IN (SELECT clientid FROM insightsWH..hld_referrer)
       and u.esoid > 0
	     AND u.FacilityID IS NULL
	   AND LEFT(u.CustomerID, 2) <> 'X_'
--residential unauthenticated
INSERT INTO #tmpusers (clientid,customerid,isauthenticated,isbusiness,esoid,accountid)
       select u.clientid,u.customerid, 0,0,u.esoid,u.accountid
       from hubdynamic.dbo.users u WITH (NOLOCK)
	   inner join hubdynamic.dbo.flags f
              on u.id = f.userid
       where f.flagtypeid = 10
	   and f.AppId = 0
	   AND u.clientid IN (SELECT clientid FROM insightsWH..hld_referrer)
       and u.esoid > 0
	     AND u.FacilityID IS NULL
	   AND LEFT(u.CustomerID, 2) = 'X_'

CREATE TABLE #CustomerBills
    (
      ReferrerId INT NOT NULL,
      CustomerId VARCHAR(50) NOT NULL,
	  BillXml XML
    );

        SELECT  ReferrerId AS 'ReferrerId',
                b.CustomerId AS 'CustomerId' ,
                CAST(REPLACE(CONVERT(VARCHAR(MAX), [xmldata]),
                             '<?xml version="1.0" encoding="utf-16"?>', '') AS XML) AS 'billxml'
	INTO #tempbilldata
        FROM    tmpWhsLoad.dbo.xml_bill b WITH ( NOLOCK )
			
		INNER JOIN #tmpusers u
        on   b.ReferrerID = u.clientid
		AND b.customerid = u.CustomerId
		AND xmldata <> ''
		ORDER BY downloadtime DESC
  
        
 INSERT  INTO #CustomerBills   
 SELECT referrerid,customerid,billxml
 from  
  (
SELECT * , ROW_NUMBER() OVER (PARTITION BY referrerid,customerid ORDER BY customerid DESC) AS row_id FROM #tempbilldata)
AS s1
WHERE row_id = 1
      
IF @ObfuscateData = 1
begin
;WITH xmlnamespaces(N'Nexus:BillAnalyzer' AS b) 
INSERT INTO insightsWH..hld_Customer (clientid, customerid ,firstname,lastname,mobilephonenumber,emailaddress,accountid,street1,street2,city,[state],postalcode,CustomerType,AuthenticationType)
select u.c.value('(@ReferrerId)[1]','int') AS 'ClientId',
tmpwhsload.dbo.obfuscatedata(u.c.value('(@CustomerId)[1]','varchar(50)')) AS 'CustomerId',
u.c.value('(@FirstName)[1]','varchar(50)') AS 'FirstName',
u.c.value('(@LastName)[1]','varchar(50)') AS 'LastName',
tmpwhsload.dbo.obfuscatedata(u.c.value('(@SMSNumber)[1]','varchar(15)')) AS 'SMSNumber',
tmpwhsload.dbo.obfuscatedata(u.c.value('(@Email)[1]','varchar(50)')) AS 'EmailAddress',
tmpwhsload.dbo.obfuscatedata(a.c.value('(@Account)[1]','varchar(50)')) AS 'AccountId',
a.c.value('(@Addr1)[1]','varchar(200)') AS 'street1',
a.c.value('(@Addr2)[1]','varchar(200)') AS 'street2',
a.c.value('(@City)[1]','varchar(50)') AS 'City',
a.c.value('(@State)[1]','varchar(50)') AS 'State',
a.c.value('(@Zip)[1]','varchar(50)') AS 'postalcode',
1,
1
FROM
#customerbills
CROSS APPLY (SELECT billxml) AS x(b)
OUTER APPLY x.b.nodes('//b:User') AS u(c)
OUTER APPLY u.c.nodes('b:Account') AS a(c)


;WITH xmlnamespaces(N'Nexus:BillAnalyzer' AS b) 
INSERT INTO insightsWH..hld_Premise (clientid, customerid ,accountid,premiseid,firstname,lastname,street1,street2,city,[state],postalcode)
select u.c.value('(@ReferrerId)[1]','int') AS 'ClientId',
tmpwhsload.dbo.obfuscatedata(u.c.value('(@CustomerId)[1]','varchar(50)')) AS 'CustomerId',
tmpwhsload.dbo.obfuscatedata(a.c.value('(@Account)[1]','varchar(50)')) AS 'AccountId',
tmpwhsload.dbo.obfuscatedata(p.c.value('(@PremiseId)[1]','varchar(50)')) AS 'PremiseId',
p.c.value('(@FirstName)[1]','varchar(50)') AS 'FirstName',
p.c.value('(@LastName)[1]','varchar(50)') AS 'LastName',
p.c.value('(@Addr1)[1]','varchar(200)') AS 'street1',
p.c.value('(@Addr2)[1]','varchar(200)') AS 'street2',
p.c.value('(@City)[1]','varchar(50)') AS 'City',
p.c.value('(@State)[1]','varchar(50)') AS 'State',
p.c.value('(@Zip)[1]','varchar(50)') AS 'postalcode'
FROM
#customerbills
CROSS APPLY (SELECT billxml) AS x(b)
OUTER APPLY x.b.nodes('//b:User') AS u(c)
OUTER APPLY u.c.nodes('b:Account') AS a(c)
OUTER APPLY a.c.nodes('b:Bill[1]') AS l(c)
OUTER APPLY l.c.nodes('b:Premise') AS p(c)
end
ELSE
begin
;WITH xmlnamespaces(N'Nexus:BillAnalyzer' AS b) 
INSERT INTO insightsWH..hld_Customer (clientid, customerid ,firstname,lastname,mobilephonenumber,emailaddress,accountid,street1,street2,city,[state],postalcode,CustomerType,AuthenticationType)
select u.c.value('(@ReferrerId)[1]','int') AS 'ClientId',
u.c.value('(@CustomerId)[1]','varchar(50)') AS 'CustomerId',
u.c.value('(@FirstName)[1]','varchar(50)') AS 'FirstName',
u.c.value('(@LastName)[1]','varchar(50)') AS 'LastName',
u.c.value('(@SMSNumber)[1]','varchar(15)') AS 'SMSNumber',
u.c.value('(@Email)[1]','varchar(50)') AS 'EmailAddress',
a.c.value('(@Account)[1]','varchar(50)') AS 'AccountId',
a.c.value('(@Addr1)[1]','varchar(200)') AS 'street1',
a.c.value('(@Addr2)[1]','varchar(200)') AS 'street2',
a.c.value('(@City)[1]','varchar(50)') AS 'City',
a.c.value('(@State)[1]','varchar(50)') AS 'State',
a.c.value('(@Zip)[1]','varchar(50)') AS 'postalcode',
1,
1
FROM
#customerbills
CROSS APPLY (SELECT billxml) AS x(b)
OUTER APPLY x.b.nodes('//b:User') AS u(c)
OUTER APPLY u.c.nodes('b:Account') AS a(c)


;WITH xmlnamespaces(N'Nexus:BillAnalyzer' AS b) 
INSERT INTO insightsWH..hld_Premise (clientid, customerid ,accountid,premiseid,firstname,lastname,street1,street2,city,[state],postalcode)
select u.c.value('(@ReferrerId)[1]','int') AS 'ClientId',
u.c.value('(@CustomerId)[1]','varchar(50)') AS 'CustomerId',
a.c.value('(@Account)[1]','varchar(50)') AS 'AccountId',
p.c.value('(@PremiseId)[1]','varchar(50)') AS 'PremiseId',
p.c.value('(@FirstName)[1]','varchar(50)') AS 'FirstName',
p.c.value('(@LastName)[1]','varchar(50)') AS 'LastName',
p.c.value('(@Addr1)[1]','varchar(200)') AS 'street1',
p.c.value('(@Addr2)[1]','varchar(200)') AS 'street2',
p.c.value('(@City)[1]','varchar(50)') AS 'City',
p.c.value('(@State)[1]','varchar(50)') AS 'State',
p.c.value('(@Zip)[1]','varchar(50)') AS 'postalcode'
FROM
#customerbills
CROSS APPLY (SELECT billxml) AS x(b)
OUTER APPLY x.b.nodes('//b:User') AS u(c)
OUTER APPLY u.c.nodes('b:Account') AS a(c)
OUTER APPLY a.c.nodes('b:Bill[1]') AS l(c)
OUTER APPLY l.c.nodes('b:Premise') AS p(c)
END 

--Load Customers who are not authenticated
INSERT INTO insightsWH..hld_Customer (clientid, customerid ,accountid,firstname,lastname,emailaddress,street1,street2,city,[state],postalcode,CustomerType,AuthenticationType)
SELECT  DISTINCT u.clientid,
		u.[CustomerId],
		u.[AccountId], 
		users.email,
		CONVERT(VARCHAR(50),um.[FirstName]), 
		CONVERT(VARCHAR(50),um.[LastName]),
		CONVERT(VARCHAR(200),um.[Street1]),  
		CONVERT(VARCHAR(200),um.[Street2]), 
		CONVERT(VARCHAR(50),um.[City]), 
		CONVERT(VARCHAR(50),um.[State]),
		CONVERT(VARCHAR(15),um.[zipcode]),
		1,2
FROM #tmpusers u
INNER JOIN [esov3dynamic].[dbo].UserMaster um ON um.UserId = u.ESOID 
INNER JOIN [hubdynamic].[dbo].Users users ON users.esoid = u.esoid
WHERE u.customerid LIKE 'X_%'
End
--ToDo - Load Business Users
--ToDo - use the date to bring in only changed users
--ToDo - remove obfuscation code that is being used for xml data
--ToDo - find out how to handle authenticated users that no longer have billing data in system (should we pull names from ESO or should we not bring them in until we have billing data)
GO
