SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- GetBillDisagg
create procedure [dbo].[uspBDGetBillDisagg] ( 
	@ClientId Int, 
	@CustomerId varchar(50), 
	@BillDate datetime,
	@AccountId varchar(50) = NULL, 
	@PremiseId varchar(50) = NULL,
	@PeriodId int = 0
	) AS
BEGIN		
	SET NOCOUNT ON;
			
	SELECT ClientId, 
		   CustomerId, 
		   AccountId,
		   PremiseId, 
		   BillDate, 
		   EndUseKey, 
		   ApplianceKey, 
		   CommodityKey, 
		   PeriodId,
		   Usage, 
		   Cost, 
		   UomKey,
		   CurrencyKey,
		   Confidence,
		   ReconciliationRatio,
		   StatusId,
		   NewDate
	FROM insightsdw.dbo.factbilldisagg
	WHERE ClientId = @ClientID  
		  AND CustomerId = @CustomerID 
		  AND (AccountId = @AccountID OR @AccountID IS NULL) 
  		  AND (PremiseId = @PremiseID OR @PremiseID IS NULL)         
		  AND (BillDate <= @BillDate)
		  AND PeriodId = @PeriodId 
	--GROUP BY ClientId, CustomerId, AccountId, PremiseId, EndUseKey, ApplianceKey, CommodityKey, UomKey, CurrencyKey, PeriodId
	ORDER BY CustomerId, AccountId, PremiseId, EndUseKey, ApplianceKey, CommodityKey

END


GO
