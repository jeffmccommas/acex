SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ubaid
-- Create date: 6/18/2015
-- Description:	Script to read value of client attribute
-- =============================================
CREATE PROCEDURE [dbo].[SetClientAttributeValue]
    @ClientId AS INT ,
    @AttributeName AS VARCHAR(256) ,
    @AttributeValue AS VARCHAR(256)
AS
    BEGIN
        SET NOCOUNT ON;

        MERGE dbo.FactClientAttribute dest
        USING
            ( SELECT    dc.ClientKey ,
                        dd.DateKey ,
                        s.AttributeName ,
                        s.ClientId ,
                        s.AttributeValue
              FROM      ( SELECT    @ClientId AS ClientId ,
                                    @AttributeName AS AttributeName ,
                                    @AttributeValue AS AttributeValue
                        ) s
                        LEFT JOIN dbo.DimClient dc ON dc.ClientId = s.ClientId
                        LEFT JOIN dbo.DimDate dd ON dd.FullDateAlternateKey = CONVERT(DATE, GETDATE())
            ) src
        ON dest.ClientKey = src.ClientKey
            AND dest.DateKey = src.DateKey
            AND dest.AttributeName = src.AttributeName
        WHEN MATCHED THEN
            UPDATE SET
                    dest.AttributeValue = src.AttributeValue ,
                    dest.EffectiveDate = GETDATE()
        WHEN NOT MATCHED BY TARGET THEN
            INSERT ( ClientKey ,
                     DateKey ,
                     AttributeName ,
                     ClientId ,
                     AttributeValue ,
                     EffectiveDate
                   )
            VALUES ( src.ClientKey ,
                     src.DateKey ,
                     src.AttributeName ,
                     src.ClientId ,
                     src.AttributeValue ,
                     GETDATE()
                   );


    END;


GO
