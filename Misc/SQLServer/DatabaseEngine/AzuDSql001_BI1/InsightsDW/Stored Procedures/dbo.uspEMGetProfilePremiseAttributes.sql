SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		MGanley
-- Create date: 2/10/2015
-- Description:	For Energy Model retrieval
-- =============================================
CREATE PROCEDURE [dbo].[uspEMGetProfilePremiseAttributes]
				@ClientID INT,
				@CustomerID VARCHAR(50),
				@AccountID VARCHAR(50),
				@PremiseID VARCHAR(50)
AS

BEGIN

		SET NOCOUNT ON;

		SELECT *
		FROM    ( SELECT   fpa.ClientId ,
							dc.CustomerId,
							fpa.PremiseID,
							fpa.AccountID,
							ContentAttributeKey AS AttributeKey,
							Value,
							ds.SourceDesc AS Source,
							fpa.EffectiveDate,
							ROW_NUMBER() OVER ( PARTITION BY fpa.PremiseKey, OptionKey ORDER BY EffectiveDate DESC ) AS SortId
					FROM    InsightsDW.dbo.FactPremiseAttribute fpa WITH(NOLOCK)
					INNER JOIN dbo.DimPremise dp ON dp.PremiseKey = fpa.PremiseKey
					INNER JOIN dbo.FactCustomerPremise fcp ON fcp.PremiseKey = dp.PremiseKey
					INNER JOIN dbo.DimCustomer dc ON dc.CustomerKey = fcp.CustomerKey
					INNER JOIN dbo.DimSource ds ON ds.SourceKey = dc.SourceKey
					WHERE     dc.ClientId  = @ClientID
								AND dc.CustomerId = @CustomerID
								AND fpa.PremiseID = @PremiseID
								AND fpa.AccountID = @AccountID
					
				) p
		WHERE   SortId = 1 
		ORDER BY p.ClientID,
				 p.PremiseID,
				 p.AccountID,
				 p.AttributeKey
		 


END







GO
