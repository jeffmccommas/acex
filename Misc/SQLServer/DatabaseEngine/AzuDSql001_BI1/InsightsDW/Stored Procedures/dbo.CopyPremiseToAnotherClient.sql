SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Changed By:	 Wayne
-- Change date: 7.24.15
-- Description: Quick proc to copy customer to client 4
--			 
--    will add 3 commodities
	--CommodityKey	
	--1	electric
	--2	gas
	--3	water
	--4	sewer
	--5	garbage
	--this IN turn WITH be translated INTO a comodity type
--	1 will translate to 4	for electric 
--	2 will translate to 5	for gas 	
--	3 will translate to 6	for water
-- =============================================
CREATE PROCEDURE [dbo].[CopyPremiseToAnotherClient]
     @premiseid varchar(50)
	,@accountid varchar(50)
    ,@customerid varchar(50)
   , @source_client_id INT
   , @target_client_id INT
AS

SET NOCOUNT ON;
-- Declare error Vars
DECLARE
       @ErrorNumber int
      , @ErrorLine int
      , @ErrorMessage nvarchar(4000)
      , @ErrorSeverity int
      , @ErrorState int;

-- dec lare working vars
DECLARE
       @new_premisekey INT

     , @message nvarchar(200)



--Start a try block
BEGIN TRY
    --Do validation before starting transaction

    -- check if premise id exists in source client for that commodity
    IF NOT EXISTS(SELECT *
                    FROM InsightsDW.dbo.DimPremise dp
					INNER JOIN dbo.DimServiceContract s
					ON s.PremiseKey = dp.PremiseKey
                    WHERE dp.ClientId = @source_client_id
                      AND PremiseId = @premiseid
					  AND dp.AccountId=@accountid
					  AND s.CommodityKey=2)
        BEGIN
		  SET @message=N'The premise Id does not exist for client for gas' + CAST(@source_client_id AS nvarchar(10))
            RAISERROR(@message , 12 , 1);
        END;
	   PRINT 'copying premise ' + @premiseid
    
	--check if it already exists in target client for any commodity
    IF EXISTS(SELECT *
                FROM InsightsDW.dbo.DimPremise dp
                WHERE dp.ClientId = @target_client_id
                  AND PremiseId = @premiseid)
	   BEGIN
		  PRINT 'Premise id '  + @premiseid + ' is already in the target client for that same commodity'
		  RAISERROR(@message , 12 , 1);
	   END



	   
    --vars for counts

	DECLARE @new_ActionDetailKey INT=0
	DECLARE @target_Service_contract_type_key int =0
	DECLARE @source_Service_contract_type_key int =5
	DECLARE @source_commodity int =2
	DECLARE @myrowcount INT=0

--	1 will translate to 4	for electric 
--	2 will translate to 5	for gas 	
--	3 will translate to 6	for water




    BEGIN TRANSACTION;
    --add premise

		INSERT INTO InsightsDW.dbo.DimPremise
		SELECT PostalCodeKey
			 , CityKey
			 , SourceKey
			 , AccountId
			 , PremiseId
			 , Street1
			 , Street2
			 , City
			 , StateProvince
			 , Country
			 , PostalCode
			 , GasService
			 , ElectricService
			 , WaterService
			 , CreateDate
			 , UpdateDate
			 , @target_client_id
			 , GeoLocation
			 , SourceId
			 , ETL_LogId
			 , IsInferred
			 , TrackingId
			 , TrackingDate
		  FROM InsightsDW.dbo.DimPremise
		  WHERE ClientId = @source_client_id
			AND PremiseId = @premiseid
			AND AccountId=@accountid;	

		SET @myrowcount=@@ROWCOUNT
		PRINT CAST( @myrowcount AS varchar(5)) + ' row added to DimPremise';
		SET @new_premisekey = SCOPE_IDENTITY();

DECLARE @new_DimServiceContractKey_gas int
set  @target_Service_contract_type_key=5;
	--add servicecontract
	INSERT INTO [dbo].[DimServiceContract]
	SELECT	TOP 1 
			@new_premisekey
           ,[CommodityKey]
           , @target_client_id
           , s.ServiceContractId
           ,@target_Service_contract_type_key
           ,[ServiceContractDescription]
           ,[ActiveDate]
           ,[InActiveDate]
           ,s.[IsInferred]
           ,s.[TrackingId]
           ,s.[TrackingDate]
           ,[BudgetBillingIndicator]
		   FROM [dbo].[DimServiceContract] s
	INNER JOIN dbo.DimPremise dp
	ON dp.PremiseKey = s.PremiseKey      
	WHERE dp.ClientId = @source_client_id
    AND dp.PremiseId = @premiseid
	AND dp.AccountId=@accountid
	AND s.CommodityKey=2
	ORDER BY [ActiveDate] desc;

    SET @myrowcount=@@ROWCOUNT;
    PRINT CAST( @myrowcount AS varchar(5)) + ' row added to DimServiceContract for gas';
    SET @new_DimServiceContractKey_gas = SCOPE_IDENTITY();


DECLARE @new_DimServiceContractKey_electric int
set  @target_Service_contract_type_key=4;
	--add servicecontract
	INSERT INTO [dbo].[DimServiceContract]
	SELECT	TOP 1 
			@new_premisekey
           ,[CommodityKey]
           , @target_client_id
           , s.ServiceContractId
           ,@target_Service_contract_type_key
           ,[ServiceContractDescription]
           ,[ActiveDate]
           ,[InActiveDate]
           ,s.[IsInferred]
           ,s.[TrackingId]
           ,s.[TrackingDate]
           ,[BudgetBillingIndicator]
		   FROM [dbo].[DimServiceContract] s
	INNER JOIN dbo.DimPremise dp
	ON dp.PremiseKey = s.PremiseKey      
	WHERE dp.ClientId = @source_client_id
    AND dp.PremiseId = @premiseid
	AND dp.AccountId=@accountid
	AND s.CommodityKey=1
	ORDER BY [ActiveDate] desc;

    SET @myrowcount=@@ROWCOUNT;
    PRINT CAST( @myrowcount AS varchar(5)) + ' row added to DimServiceContract for electric';
    SET @new_DimServiceContractKey_electric = SCOPE_IDENTITY();



DECLARE @new_DimServiceContractKey_water int
set  @target_Service_contract_type_key=6;
	--add servicecontract
	INSERT INTO [dbo].[DimServiceContract]
	SELECT	TOP 1 
			@new_premisekey
           ,[CommodityKey]
           , @target_client_id
           , s.ServiceContractId
           ,@target_Service_contract_type_key
           ,[ServiceContractDescription]
           ,[ActiveDate]
           ,[InActiveDate]
           ,s.[IsInferred]
           ,s.[TrackingId]
           ,s.[TrackingDate]
           ,[BudgetBillingIndicator]
		   FROM [dbo].[DimServiceContract] s
	INNER JOIN dbo.DimPremise dp
	ON dp.PremiseKey = s.PremiseKey      
	WHERE dp.ClientId = @source_client_id
    AND dp.PremiseId = @premiseid
	AND dp.AccountId=@accountid
	AND s.CommodityKey=3
	ORDER BY [ActiveDate] desc;

    SET @myrowcount=@@ROWCOUNT;
    PRINT CAST( @myrowcount AS varchar(5)) + ' row added to DimServiceContract for water';
    SET @new_DimServiceContractKey_water = SCOPE_IDENTITY();


    --add premise attributes
		
    INSERT INTO InsightsDW.dbo.FactPremiseAttribute
    SELECT COALESCE(@new_premisekey, dp.PremiseKey)
         , fpa.OptionKey
         , fpa.SourceKey
         , fpa.ContentAttributeKey
         , fpa.ContentOptionKey
         , fpa.AttributeId
         , fpa.OptionId
         , fpa.Value
         , @target_client_id
         , dp.premiseid
         , fpa.AccountID
         , fpa.SourceID
         , fpa.EffectiveDateKey
         , fpa.EffectiveDate
         , fpa.ETLLogID
         , fpa.TrackingID
         , fpa.TrackingDate
      FROM
           InsightsDW.dbo.FactPremiseAttribute fpa INNER JOIN InsightsDW.dbo.DimPremise dp
           ON dp.PremiseKey = fpa.PremiseKey
      WHERE dp.ClientId = @source_client_id
        AND dp.PremiseId = @premiseid
		AND dp.AccountId=@accountid;
    SET @myrowcount=@@ROWCOUNT;
    PRINT CAST( @myrowcount AS varchar(5)) + ' row added to FactPremiseAttribute';   


	DECLARE @new_servicepointkey_gas INT
    DECLARE @new_servicepointkey_water INT
    DECLARE @new_servicepointkey_electric int
    --insert servicepoint
    INSERT INTO InsightsDW.dbo.DimServicePoint
    SELECT @new_premisekey
         , sp.ServicePointId
         , @target_client_id
         , sp.PremiseId
         , sp.SourceId
         , sp.TrackingId
         , sp.TrackingDate
         , sp.IsInferred
      FROM InsightsDW.dbo.DimServicePoint sp
	  INNER JOIN dbo.DimPremise dp
	  ON dp.PremiseKey = sp.PremiseKey
	  INNER JOIN dbo.FactServicePoint fsp
	  ON fsp.ServicePointKey = sp.ServicePointKey
	  INNER JOIN dbo.DimServiceContract dsc
	  ON dsc.ServiceContractKey = fsp.ServiceContractKey
      WHERE sp.PremiseId = @premiseid
	  AND dp.AccountId=@accountid
        AND sp.ClientId = @source_client_id
		AND dsc.CommodityKey=1  --electric
		
	SET @new_servicepointkey_electric= @@IDENTITY
    SET @myrowcount=@@ROWCOUNT;
    PRINT CAST( @myrowcount AS varchar(5)) + ' row added to DimServicePoint for electric';   

	    --insert servicepoint
    INSERT INTO InsightsDW.dbo.DimServicePoint
    SELECT @new_premisekey
         , sp.ServicePointId
         , @target_client_id
         , sp.PremiseId
         , sp.SourceId
         , sp.TrackingId
         , sp.TrackingDate
         , sp.IsInferred
      FROM InsightsDW.dbo.DimServicePoint sp
	  INNER JOIN dbo.DimPremise dp
	  ON dp.PremiseKey = sp.PremiseKey
	  INNER JOIN dbo.FactServicePoint fsp
	  ON fsp.ServicePointKey = sp.ServicePointKey
	  INNER JOIN dbo.DimServiceContract dsc
	  ON dsc.ServiceContractKey = fsp.ServiceContractKey
      WHERE sp.PremiseId = @premiseid
	  AND dp.AccountId=@accountid
        AND sp.ClientId = @source_client_id
		AND dsc.CommodityKey=2  --gas
		
	SET @new_servicepointkey_gas= @@IDENTITY
    SET @myrowcount=@@ROWCOUNT;
    PRINT CAST( @myrowcount AS varchar(5)) + ' row added to DimServicePoint for gas';  

	    --insert servicepoint
    INSERT INTO InsightsDW.dbo.DimServicePoint
    SELECT @new_premisekey
         , sp.ServicePointId
         , @target_client_id
         , sp.PremiseId
         , sp.SourceId
         , sp.TrackingId
         , sp.TrackingDate
         , sp.IsInferred
      FROM InsightsDW.dbo.DimServicePoint sp
	  INNER JOIN dbo.DimPremise dp
	  ON dp.PremiseKey = sp.PremiseKey
	  INNER JOIN dbo.FactServicePoint fsp
	  ON fsp.ServicePointKey = sp.ServicePointKey
	  INNER JOIN dbo.DimServiceContract dsc
	  ON dsc.ServiceContractKey = fsp.ServiceContractKey
      WHERE sp.PremiseId = @premiseid
	  AND dp.AccountId=@accountid
        AND sp.ClientId = @source_client_id
		AND dsc.CommodityKey=3  --water
		
	SET @new_servicepointkey_water= @@IDENTITY
    SET @myrowcount=@@ROWCOUNT;
    PRINT CAST( @myrowcount AS varchar(5)) + ' row added to DimServicePoint for water';  

	-- [dbo].[FactServicePoint]

	--electric
	INSERT [dbo].[FactServicePoint]
	VALUES
    (
	   @new_DimServiceContractKey_electric
	  , @new_servicepointkey_electric
	)
		--gas
	INSERT [dbo].[FactServicePoint]
	VALUES
    (
	   @new_DimServiceContractKey_gas
	  , @new_servicepointkey_gas
	)
		--water
	INSERT [dbo].[FactServicePoint]
	VALUES
    (
	   @new_DimServiceContractKey_water
	  , @new_servicepointkey_water
	)

    --customer Stuff

    DECLARE @new_Clientkey INT

    SELECT @new_Clientkey=ClientKey
             FROM InsightsDW.dbo.DimClient
             WHERE ClientId = @target_client_id


		  INSERT INTO InsightsDW.dbo.DimCustomer(ClientKey
									    , ClientId
									    , PostalCodeKey
									    , CustomerAuthenticationTypeKey
									    , SourceKey
									    , CityKey
									    , CustomerId
									    , FirstName
									    , LastName
									    , Street1
									    , Street2
									    , City
									    , StateProvince
									    , Country
									    , PostalCode
									    , PhoneNumber
									    , MobilePhoneNumber
									    , EmailAddress
									    , AlternateEmailAddress
									    , CreateDate
									    , UpdateDate
									    , SourceId
									    , AuthenticationTypeId
									    , Language
									    , ETL_LogId
									    , IsInferred
									    , TrackingId
									    , TrackingDate)

		  SELECT
			 @new_Clientkey
			 , @target_client_id AS clientid
			 , c.PostalCodeKey
			 , c.CustomerAuthenticationTypeKey
			 , c.SourceKey
			 , c.CityKey
			 , @customerid
			 , c.FirstName
			 , c.LastName
			 , c.Street1
			 , c.Street2
			 , c.City
			 , c.StateProvince
			 , c.Country
			 , c.PostalCode
			 , c.PhoneNumber
			 , c.MobilePhoneNumber
			 , c.EmailAddress
			 , c.AlternateEmailAddress
			 , c.CreateDate
			 , c.UpdateDate
			 , c.SourceId
			 , c.AuthenticationTypeId
			 , c.Language
			 , c.ETL_LogId
			 , c.IsInferred
			 , c.TrackingId
			 , c.TrackingDate
		    FROM
			    InsightsDW.dbo.DimCustomer c INNER JOIN dbo.FactCustomerPremise fcp
			    ON fcp.CustomerKey = c.CustomerKey
									   INNER JOIN InsightsDW.dbo.DimPremise dp
			    ON dp.PremiseKey = fcp.PremiseKey
		    WHERE dp.ClientId = @source_client_id
			 AND dp.PremiseId = @premiseid
			 AND dp.AccountId=@accountid
			 AND c.CustomerId=@customerid
		    ORDER BY c.CustomerKey ASC;

			DECLARE @new_customer_key int
			SET @new_customer_key=@@identity
		    SET @myrowcount=@@ROWCOUNT;
		 

		  
		  PRINT CAST( @myrowcount AS varchar(5)) + ' row added to DimCustomer';   



--add customer premise association

INSERT INTO dbo.FactCustomerPremise
VALUES(
	 @new_customer_key
     , @new_premisekey
     , '1234'
     , GETDATE()
	 )

    SET @myrowcount=@@ROWCOUNT;
    PRINT CAST( @myrowcount AS varchar(5)) + ' row added to FactCustomerPremise';       






--add fact billing for gas

--	1  for electric 
--	2 	for gas 	
--	3 	for water
 INSERT [dbo].[FactServicePointBilling]
SELECT @new_DimServiceContractKey_gas
      ,[BillPeriodStartDateKey]
      ,[BillPeriodEndDateKey]
      ,[UOMKey]
      ,[BillPeriodTypeKey]
      ,@target_client_id
      ,[PremiseId]
      ,[AccountId]
      ,[CommodityId]
      ,[BillPeriodTypeId]
      ,[UOMId]
      ,[StartDate]
      ,[EndDate]
      ,[TotalUsage]
      ,[CostOfUsage]
      ,[OtherCost]
      ,[RateClassKey1]
      ,[RateClassKey2]
      ,[NextReadDate]
      ,[ReadDate]
      ,[CreateDate]
      ,[UpdateDate]
      ,[BillCycleScheduleId]
      ,[BillDays]
      ,[ETL_LogId]
      ,[TrackingId]
      ,[TrackingDate]
      ,[SourceKey]
      ,[SourceId]
      ,[ReadQuality]
      ,[DueDate]
  FROM [dbo].[FactServicePointBilling]
  	  WHERE ClientId = @source_client_id
		AND PremiseId = @premiseid
		AND  AccountId=@accountid
		AND CommodityId=2

	SET @myrowcount=@@ROWCOUNT;
	PRINT CAST( @myrowcount AS varchar(5)) + ' rows added to FactServicePointBilling for gas';    


--add fact billing for electric

--1	kwh

 INSERT [dbo].[FactServicePointBilling]
SELECT @new_DimServiceContractKey_electric
      ,[BillPeriodStartDateKey]
      ,[BillPeriodEndDateKey]
      ,[UOMKey]
      ,[BillPeriodTypeKey]
      ,@target_client_id
      ,[PremiseId]
      ,[AccountId]
      ,[CommodityId]
      ,[BillPeriodTypeId]
      ,[UOMId]
      ,[StartDate]
      ,[EndDate]
      ,[TotalUsage]
      ,[CostOfUsage]
      ,[OtherCost]
      ,[RateClassKey1]
      ,[RateClassKey2]
      ,[NextReadDate]
      ,[ReadDate]
      ,[CreateDate]
      ,[UpdateDate]
      ,[BillCycleScheduleId]
      ,[BillDays]
      ,[ETL_LogId]
      ,[TrackingId]
      ,[TrackingDate]
      ,[SourceKey]
      ,[SourceId]
      ,[ReadQuality]
      ,[DueDate]
  FROM [dbo].[FactServicePointBilling]
  	  WHERE ClientId = @source_client_id
		AND PremiseId = @premiseid
		AND  AccountId=@accountid
		AND CommodityId=1

	SET @myrowcount=@@ROWCOUNT;
	PRINT CAST( @myrowcount AS varchar(5)) + ' rows added to FactServicePointBilling for electric';    




--add fact billing for water


 INSERT [dbo].[FactServicePointBilling]
SELECT @new_DimServiceContractKey_water
      ,[BillPeriodStartDateKey]
      ,[BillPeriodEndDateKey]
      ,[UOMKey]
      ,[BillPeriodTypeKey]
      ,@target_client_id
      ,[PremiseId]
      ,[AccountId]
      ,[CommodityId]
      ,[BillPeriodTypeId]
      ,[UOMId]
      ,[StartDate]
      ,[EndDate]
      ,[TotalUsage]
      ,[CostOfUsage]
      ,[OtherCost]
      ,[RateClassKey1]
      ,[RateClassKey2]
      ,[NextReadDate]
      ,[ReadDate]
      ,[CreateDate]
      ,[UpdateDate]
      ,[BillCycleScheduleId]
      ,[BillDays]
      ,[ETL_LogId]
      ,[TrackingId]
      ,[TrackingDate]
      ,[SourceKey]
      ,[SourceId]
      ,[ReadQuality]
      ,[DueDate]
  FROM [dbo].[FactServicePointBilling]
  	  WHERE ClientId = @source_client_id
		AND PremiseId = @premiseid
		AND  AccountId=@accountid
		AND CommodityId=3

	SET @myrowcount=@@ROWCOUNT;
	PRINT CAST( @myrowcount AS varchar(5)) + ' rows added to FactServicePointBilling for water';    

----==================================================================================================
----AMI Stuff
--DECLARE
--       @amikeytable TABLE(id int IDENTITY
--                        , old_amikey int NULL
--                        , new_amikey int NULL);

--  ----------------------------------------


------add ami headers
--INSERT INTO dbo.FactAmi
--OUTPUT NULL, INSERTed.AmiKey AS new_amikey    
--       INTO @amikeytable
--SELECT TOP 100 PERCENT @target_client_id
--                     , @new_premisekey
--                     , ServicePointKey
--                     , MeterId
--                     , fa.ETL_LogId
--  FROM
--       dbo.FactAmi fa INNER JOIN dbo.DimPremise dp
--       ON dp.PremiseKey = fa.PremiseKey
--  WHERE dp.Premiseid = @premiseid
--    AND dp.ClientId = @source_client_id
--  ORDER BY fa.AmiKey;
--SET @myrowcount=@@ROWCOUNT;
--PRINT CAST( @myrowcount AS varchar(5)) + ' rows added to FactAmi'; 

----update the key table
--WITH q
--    AS (SELECT TOP 100 PERCENT fa.amikey
--                             , ROW_NUMBER()OVER(ORDER BY fa.AmiKey)AS rn
--          FROM
--               dbo.FactAmi fa INNER JOIN dbo.DimPremise dp
--               ON dp.PremiseKey = fa.PremiseKey
--          WHERE dp.PremiseId = @premiseid
--            AND dp.ClientId = @source_client_id
--          ORDER BY fa.AmiKey)
--    UPDATE @amikeytable
--      SET old_amikey = (SELECT q.amikey
--                          FROM q
--                          WHERE id = q.rn);



----add ami details
--INSERT INTO dbo.FactAmiDetail
--SELECT k.new_amikey
--     , fad.DateKey
--     , @target_client_id
--     , fad.PremiseId)
--     , fad.AccountId
--     , fad.ServicePointId
--     , fad.CustomerId
--     , fad.CommodityId
--     , fad.MeterId
--     , fad.UOMId
--     , fad.TotalUnits
--     , fad.SourceId
--     , fad.ETL_LogId
--  FROM
--       dbo.FactAmiDetail fad INNER JOIN @amikeytable k
--       ON fad.AmiKey = k.old_amikey
--  WHERE fad.ClientId = @source_client_id
--    AND fad.PremiseId = @premiseid;

--SET @myrowcount=@@ROWCOUNT;
--    IF @verbose_flag=1 PRINT CAST( @myrowcount AS varchar(5)) + ' rows added to FactAmiDetail';
----=============================================================================================================

-- add benchmarks if they have any

INSERT INTO dbo.Benchmarks
SELECT   @target_client_id, 
          @new_premisekey, 
          BillMonth ,
          StartDateKey ,
          EndDateKey ,
          CommodityKey ,
          BenchmarkGroupTypeKey ,
          GroupId ,
          GroupCount ,
          MyUsage ,
          AverageUsage ,
          EfficientUsage ,
          UOMKey ,
          MyCost ,
          AverageCost ,
          EfficientCost ,
          CostCurrencyCost ,
          DateCreated
FROM dbo.Benchmarks  b
INNER JOIN dbo.DimPremise dp
ON dp.PremiseKey = b.PremiseKey      
  WHERE dp.ClientId = @source_client_id
    AND dp.PremiseId = @premiseid
	AND dp.AccountId=@accountid;

SET @myrowcount=@@ROWCOUNT;
PRINT CAST( @myrowcount AS varchar(5)) +' rows added to Benchmarks';

DECLARE
       @actionkeytable TABLE(id int IDENTITY
                        , old_actionkey int NULL
                        , new_actionkey int NULL);

--===========================================================================================
----add ami headers
merge INTO dbo.Factaction USING 
(
SELECT    @new_premisekey AS new_premiskey,
          DateKey ,
          ActionId , 
          ActionKey ,
          SubActionKey ,
          StatusKey ,
          StatusDate ,
          @target_client_id  AS target_client_id,
          fa.PremiseId AS new_premisid,
          fa.AccountID ,
          fa.SourceKey ,
          fa.CreateDate ,
          ETLLogID ,
          fa.TrackingID ,
          fa.TrackingDate,
		fa.ActionDetailKey AS old_actionkey
FROM
    InsightsDW.dbo.factAction fa INNER JOIN dbo.DimPremise dp
    ON dp.PremiseKey = fa.PremiseKey
WHERE dp.PremiseId = @premiseid and dp.clientid=@source_client_id
AND dp.AccountId=@accountid
) p ON 1=0
WHEN NOT MATCHED then
INSERT 
        ( PremiseKey ,
          DateKey ,
          ActionId ,
          ActionKey ,
          SubActionKey ,
          StatusKey ,
          StatusDate ,
          ClientID ,
          PremiseID ,
          AccountID ,
          SourceKey ,
          CreateDate ,
          ETLLogID ,
          TrackingID ,
          TrackingDate
        )
VALUES(
	     p.new_premiskey,
          p. DateKey ,
          p.ActionId , 
          p.ActionKey ,
          p.SubActionKey ,
          p.StatusKey ,
          p.StatusDate ,
          p.target_client_id ,
          p.new_premisid,
          p.AccountID ,
          p.SourceKey ,
          p.CreateDate ,
          p.ETLLogID ,
          p.TrackingID ,
          p.TrackingDate
		)
OUTPUT p.old_actionkey AS old_actionkey, INSERTed.ActionDetailKey AS new_actionkey    
       INTO @actionkeytable;

    --
INSERT   InsightsDW.dbo.FactActionItem	
   SELECT ActionItemKey ,
          @new_premisekey,
          EnergyObjectKey ,
          Payback ,
          CO2Savings ,
          ROI ,
          AnnualUsageElectric ,
          AnnualUsageGas ,
          AnnualUsageWater ,
          AnnualUsagePropane ,
          AnnualUsageOil ,
          AnnualUsageWood ,
          UsageSavingsElectric ,
          UsageSavingsGas ,
          UsageSavingsWater ,
          UsageSavingsPropane ,
          UsageSavingsOil ,
          UsageSavingsWood ,
          DollarSavingsElectric ,
          DollarSavingsGas ,
          DollarSavingsWater ,
          DollarSavingsPropane ,
          DollarSavingsOil ,
          DollarSavingsWood ,
          Cost ,
          DateUpdatedKey ,
          StatusKey ,
          Winter ,
          Spring ,
          Summer ,
          Fall        
FROM
    InsightsDW.dbo.FactActionItem fai INNER JOIN dbo.DimPremise dp
    ON dp.PremiseKey = fai.PremiseKey
WHERE dp.PremiseId = @premiseid and dp.clientid=@source_client_id
AND dp.AccountId=@accountid;
    --
INSERT  InsightsDW.dbo.factActionData
SELECT    a.new_actionkey ,
          ActionDataKey ,
          ActionDataValue
FROM	    
    InsightsDW.dbo.factActionData fad INNER JOIN 
    InsightsDW.dbo.FactAction fa
    ON fa.ActionDetailKey = fad.ActionDetailKey
    INNER JOIN  dbo.DimPremise dp
    ON dp.PremiseKey = fa.PremiseKey
    INNER join @actionkeytable a
    ON a.old_actionkey=fa.ActionDetailKey
WHERE dp.PremiseId = @premiseid and dp.clientid=@source_client_id
AND dp.AccountId=@accountid;


SET @myrowcount=@@ROWCOUNT;
PRINT CAST( @myrowcount AS varchar(5)) +' rows added to actiondata';
--=====================================================================================================


COMMIT TRANSACTION;

END TRY
BEGIN CATCH

    IF @@TRANCOUNT > 0
        BEGIN
            PRINT 'Performing ROLLBACK';
            ROLLBACK TRANSACTION;
        END;
    SET @ErrorNumber = ERROR_NUMBER();
    SET @ErrorLine = ERROR_LINE();
    SET @ErrorMessage = 'Error executing  CopyPremiseToAnotherClient ' + ERROR_MESSAGE();
    SET @ErrorSeverity = ERROR_SEVERITY();
    SET @ErrorState = ERROR_STATE();
    RAISERROR(@ErrorMessage , @ErrorSeverity , @ErrorState);
    RETURN;
END CATCH;

SET NOCOUNT OFF;
--success
RETURN 0;
GO
