SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/21/2014
-- Description:	
-- =============================================
create PROCEDURE [ETL].[S_Billing]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;


		SELECT * 
		FROM Holding.v_Billing 
		WHERE ClientId = @ClientID
		ORDER By ClientId, CustomerId, PremiseId, AccountId, ServicePointId, StartDate, EndDate, CommodityId, BillPeriodTypeId, UOMId


END





GO
