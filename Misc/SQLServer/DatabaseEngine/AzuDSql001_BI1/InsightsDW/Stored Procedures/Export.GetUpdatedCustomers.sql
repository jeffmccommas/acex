SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Export].[GetUpdatedCustomers]
    (
      @clientid INT ,
      @processdate DATETIME
    )
AS
    BEGIN
        SELECT  c.clientid ,
                c.customerid ,
                p.accountid ,
                p.premiseid
        FROM    [dbo].[dimcustomer] c WITH ( NOLOCK )
                INNER JOIN [dbo].[factcustomerpremise] fcp WITH ( NOLOCK ) ON c.customerkey = fcp.customerkey
                INNER JOIN [dbo].[dimpremise] p WITH ( NOLOCK ) ON p.premisekey = fcp.premisekey
        WHERE   c.clientid = @clientid
                AND (c.updatedate > @processdate
                OR c.createdate > @processdate
                OR p.createdate > @processdate)
    END


GO
