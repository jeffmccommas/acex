SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[CreateBenchmarks]
    @ClientID AS INT ,
    @BillMonth AS DATETIME ,
    @BenchmarkGroupTypeKey AS INT = 1 ,
    @MinGroupSize AS INT = 20
AS
    BEGIN

        DECLARE @BenchmarkDate AS DATE = DATEADD(MONTH,
                                                 DATEDIFF(MONTH, 0, @BillMonth),
                                                 0);

        DECLARE @ConfigurationId AS INT;


        SELECT  @ConfigurationId = ConfigurationId
        FROM    dbo.BenchmarkGroupClientType
        WHERE   clientId = @ClientID
                AND BenchmarkGroupTypeKey = @BenchmarkGroupTypeKey;

        DECLARE @cols NVARCHAR(1000)
        SELECT  @cols = STUFF(( SELECT DISTINCT TOP 100 PERCENT
                                        +'],[' + t2.AttributeKey
                                FROM    dbo.BenchmarkConfiguration AS t2
                                WHERE   ConfigurationId = @ConfigurationId
                                        AND t2.enabled = 1
                                        AND t2.AttributeKey NOT IN (
                                        'BillCycle', 'ZipCode' )
                              FOR
                                XML PATH('')
                              ), 1, 2, '') + ']';

        DECLARE @colsGrp NVARCHAR(1000)
        SELECT  @colsGrp = STUFF(( SELECT DISTINCT TOP 100 PERCENT
                                            +'] IS NOT NULL AND ['
                                            + t2.AttributeKey
                                   FROM     dbo.BenchmarkConfiguration AS t2
                                   WHERE    ConfigurationId = @ConfigurationId
                                            AND t2.enabled = 1
                                            AND t2.AttributeKey NOT IN (
                                            'BillCycle', 'ZipCode' )
                                 FOR
                                   XML PATH('')
                                 ), 1, 18, '') + '] IS NOT NULL';

        DECLARE @colsDef NVARCHAR(1000)
        SELECT  @colsDef = STUFF(( SELECT DISTINCT TOP 100 PERCENT
                                            +'] int NULL,[' + t2.AttributeKey
                                   FROM     dbo.BenchmarkConfiguration AS t2
                                   WHERE    ConfigurationId = @ConfigurationId
                                            AND t2.enabled = 1
                                            AND t2.AttributeKey NOT IN (
                                            'BillCycle', 'ZipCode' )
                                 FOR
                                   XML PATH('')
                                 ), 1, 11, '') + '] int NULL,';

        DECLARE @colsCriteria NVARCHAR(1000)
        SELECT  @colsCriteria = '('
                + STUFF(( SELECT DISTINCT TOP 100 PERCENT
                                    +'])+'';''+(SELECT Criteria FROM #Criteria WHERE AttributeKey = '''
                                    + t2.AttributeKey + ''' AND MapValue = ['
                                    + t2.AttributeKey
                          FROM      dbo.BenchmarkConfiguration AS t2
                          WHERE     ConfigurationId = @ConfigurationId
                                    AND t2.enabled = 1
                        FOR
                          XML PATH('')
                        ), 1, 7, '') + ']))';

        DECLARE @ZipCodeEnabled INT = 0;
        SELECT  @ZipCodeEnabled = COUNT(*)
        FROM    [InsightsDW].[dbo].[BenchmarkConfiguration]
        WHERE   ConfigurationId = @ConfigurationId
                AND AttributeKey = 'ZipCode'
                AND [Enabled] = 1

        DECLARE @tblScript NVARCHAR(MAX)

		SET @tblScript = 'DECLARE @EfficientPeerPercentage AS INT = 15;'

		        SET @tblScript = @tblScript
            +  'CREATE TABLE #ContentConfiguration
			(
				ConfigurationKey VARCHAR(256) ,
				CategoryKey VARCHAR(256) ,
				Value VARCHAR(256)
			);
						
		INSERT  INTO #ContentConfiguration
        EXEC InsightsMetadata.[cm].[uspSelectContentClientConfigurations] @clientID = '+ CONVERT(VARCHAR(10), @ClientID) + ',
            @categoryKey = ''benchmark'' ;'

			SET @tblScript = @tblScript
            +  'SELECT  @EfficientPeerPercentage = Value
            FROM    #ContentConfiguration
			WHERE   [ConfigurationKey] = ''efficientpeerpercentage'';'

        SET @tblScript = @tblScript
            +  'CREATE TABLE #RegionalDefaults
            (
              AttributeKey VARCHAR(256) ,
              DefaultKey VARCHAR(256) ,
              DefaultValue VARCHAR(256)
            );
						
		INSERT  INTO #RegionalDefaults
        EXEC InsightsMetadata.[dbo].[GetBenchmarkRegionalDefaults] @clientid = '
            + CONVERT(VARCHAR(10), @ClientID) + ';'

        SET @tblScript = @tblScript
            + 'WITH    cmsConfig
          AS ( SELECT   SUBSTRING([ConfigurationKey], 0,
                                  CHARINDEX(''.'', [ConfigurationKey], 1)) AS ConfigurationKey ,
                        SUBSTRING([ConfigurationKey],
                                  CHARINDEX(''.'', [ConfigurationKey], 1) + 1,
                                  10) AS BillPeriod ,
                        [Value] 
               FROM     #ContentConfiguration
               WHERE    [ConfigurationKey] LIKE ''benchmark%''
             )
    SELECT  * INTO #cmsConfig
    FROM    ( SELECT    ConfigurationKey ,
                        Value ,
                        CASE WHEN BillPeriod = ''month'' THEN 1
                             WHEN BillPeriod = ''bimonth'' THEN 2
                             WHEN BillPeriod = ''quarter'' THEN 3
                             ELSE -1
                        END AS BillPeriodType
              FROM      cmsConfig
            ) cc PIVOT ( MAX(Value) FOR ConfigurationKey IN ( "benchmarkmaxbillamount",
                                                              "benchmarkmaxbilldays",
                                                              "benchmarkminbillamount",
                                                              "benchmarkminbilldays" ) ) AS pcc;'

		
        SET @tblScript = @tblScript + 'WITH    config
          AS ( SELECT   ConfigurationId ,
                        MAX(Datekey) AS MaxDate
               FROM     dbo.[BenchmarkConfiguration]
               WHERE    ConfigurationId = '
            + CONVERT(VARCHAR(5), @ConfigurationId)
            + '
               GROUP BY ConfigurationId
             )
    SELECT  bc.ConfigurationKey ,
            bc.AttributeKey ,
            bc.IsNumeric ,
            IIF(IsNumeric = 1, DefaultValue, OptionKey) AS Value ,
            IIF(IsNumeric = 1, DefaultValue, CMSOptionKey) AS ActualValue
    INTO    #DefaultConfig
    FROM    dbo.BenchmarkConfiguration bc
            INNER JOIN config c ON c.MaxDate = bc.DateKey
                                   AND bc.ConfigurationId = c.ConfigurationId
            LEFT JOIN #RegionalDefaults rd ON rd.AttributeKey = bc.AttributeKey
            LEFT JOIN dimpremiseattribute pa ON pa.cmsattributekey = bc.attributekey
            LEFT JOIN dimpremiseoption po ON po.attributekey = pa.attributekey
                                             AND ( bc.IsNumeric = 1
                                                   OR po.CMSOptionKey = rd.DefaultKey
                                                 )
WHERE   bc.Enabled = 1
        AND bc.AttributeKey != ''BillCycle''
        AND bc.AttributeKey != ''ZipCode'';'


        SET @tblScript = @tblScript + 'WITH    config
          AS ( SELECT   ConfigurationId ,
                        MAX(Datekey) AS MaxDate
               FROM     dbo.[BenchmarkConfiguration]
               WHERE    ConfigurationId = '
            + CONVERT(VARCHAR(5), @ConfigurationId)
            + '
               GROUP BY ConfigurationId
             )
    SELECT  c.AttributeKey,c.IsNumeric ,
            cd.* ,
            IIF(c.isnumeric = 1, cd.startvalue, po.optionkey) AS OptionValue,
			c.AttributeKey + '' '' + StartOperator + '' '' + StartValue + IIF(EndValue IS NULL,'''','' AND '' + EndOperator + '' '' + EndValue) AS Criteria
    INTO    #Config
    FROM    dbo.BenchmarkConfiguration c
            INNER JOIN config cf ON cf.MaxDate = c.DateKey
                                    AND cf.ConfigurationId = c.ConfigurationId
            INNER JOIN [dbo].[BenchmarkConfigurationDetail] cd ON c.ConfigurationKey = cd.ConfigurationKey
            LEFT JOIN dimpremiseattribute pa ON pa.cmsattributekey = c.attributekey
            LEFT JOIN dimpremiseoption po ON po.attributekey = pa.attributekey
WHERE   ( po.cmsoptionkey = cd.startvalue
          OR c.isnumeric = 1
        )
        AND c.enabled = 1;'

        SET @tblScript = @tblScript + 'SELECT  AttributeKey ,
                MapValue ,
                REPLACE(REPLACE(STUFF(( SELECT  '','' + T2.Criteria
                        FROM    #Config T2
                        WHERE   c.AttributeKey = T2.AttributeKey
                                AND c.MapValue = T2.MapValue
                      FOR
                        XML PATH('''')
                      ), 1, 1, ''''),''&gt;'',''>''),''&lt;'',''<'') AS Criteria
        INTO    #Criteria
        FROM    #Config c
        GROUP BY AttributeKey ,
                MapValue;'

        SET @tblScript = @tblScript
            + 'SELECT DISTINCT
        PremiseKey
INTO    #AllPremises
FROM    ( SELECT    PremiseKey ,
                    ContentOptionKey ,
                    ROW_NUMBER() OVER ( PARTITION BY PremiseKey,
                                        ContentAttributeKey ORDER BY TrackingDate DESC ) AS SortID
          FROM      dbo.FactPremiseAttribute
          WHERE     ClientID = ' + CONVERT(VARCHAR(10), @ClientID)
            + '
                    AND ContentAttributeKey = ''paperreport.analysisgroup.enrollmentstatus''
        ) p
WHERE   p.SortID = 1
        AND p.ContentOptionKey = ''paperreport.analysisgroup.enrollmentstatus.enrolled'';' 

        SET @tblScript = @tblScript
            + 'SELECT  a.PremiseKey ,
        a.AttributeKey ,
        a.ActualValue ,
        cd.MapValue ,
        IIF(cd.MapValue IS NULL
        AND ErrorCode = 0, 2, ErrorCode) AS ErrorCode
INTO    #PremiseAttributes
FROM    ( SELECT  ap.PremiseKey ,
				bc.AttributeKey ,
				bc.ConfigurationKey ,
				IIF(ContentAttributeKey IS NULL, bc.Value, IIF(ISNUMERIC = 1, fpa.Value, OptionKey)) AS Value ,
				IIF(ContentAttributeKey IS NULL, bc.ActualValue, IIF(ISNUMERIC = 1, fpa.Value, ContentOptionKey)) AS ActualValue ,
				IIF(ContentAttributeKey IS NULL
				AND bc.Value IS NULL, 1, 0) AS ErrorCode ,
				ROW_NUMBER() OVER ( PARTITION BY ap.PremiseKey, AttributeKey ORDER BY TrackingDate DESC ) AS SortID
FROM    #AllPremises ap
			CROSS JOIN #DefaultConfig bc
			LEFT JOIN dbo.FactPremiseAttribute fpa ON fpa.ContentAttributeKey = bc.AttributeKey
                                                  AND fpa.PremiseKey = ap.PremiseKey
        ) a
        LEFT JOIN #Config cd ON a.ConfigurationKey = cd.ConfigurationKey
                                AND ( ( ( cd.StartOperator = ''=''
                                          AND a.Value = cd.OptionValue
                                        )
                                        OR ( cd.StartOperator = ''>''
                                             AND a.Value > cd.OptionValue
                                           )
                                        OR ( cd.StartOperator = ''>=''
                                             AND a.Value >= cd.OptionValue
                                           )
                                        OR ( cd.StartOperator = ''<''
                                             AND a.Value < cd.OptionValue
                                           )
                                        OR ( cd.StartOperator = ''<=''
                                             AND a.Value <= cd.OptionValue
                                           )
                                      )
                                      AND ( cd.endvalue IS NULL
                                            OR ( cd.EndOperator = ''>''
                                                 AND a.Value > cd.endvalue
                                               )
                                            OR ( cd.EndOperator = ''>=''
                                                 AND a.Value >= cd.endvalue
                                               )
                                            OR ( cd.EndOperator = ''<''
                                                 AND a.Value < cd.endvalue
                                               )
                                            OR ( cd.EndOperator = ''<=''
                                                 AND a.Value <= cd.endvalue
                                               )
                                          )
                                    )
WHERE   a.SortID = 1;'

        SET @tblScript = @tblScript + 'INSERT INTO [dbo].[BenchmarkErrors]
           ([PremiseKey]
		   ,[ClientId]
           ,[BillMonth]
           ,[ErrorTypeKey]
           ,[ErrorInfo]
           ,[DateCreated])
SELECT PremiseKey,'+ CONVERT(VARCHAR(10), @ClientID)+',''' + CONVERT(VARCHAR(20), @BenchmarkDate)
            + ''',ErrorCode, AttributeKey,GETDATE() FROM #PremiseAttributes
WHERE ErrorCode != 0;'

IF @ZipCodeEnabled = 1
    BEGIN
        SET @tblScript = @tblScript + 'INSERT INTO [dbo].[BenchmarkErrors]
           ([PremiseKey]
		   ,[ClientId]
           ,[BillMonth]
           ,[ErrorTypeKey]
           ,[ErrorInfo]
           ,[DateCreated])
		   SELECT  
        ap.PremiseKey ,
		'+ CONVERT(VARCHAR(10), @ClientID)+' ,
        ''' + CONVERT(VARCHAR(20), @BenchmarkDate) + ''' ,
        IIF(c1.MapValue IS NOT NULL, 0, IIF(p.PostalCode IS NULL, 1, 2)) AS ErrorCode ,
        ''ZipCode'' AS AttributeKey ,
        GETDATE()
FROM    #AllPremises ap
        INNER JOIN dbo.DimPremise p ON p.PremiseKey = ap.PremiseKey
        LEFT JOIN #Config c1 ON c1.AttributeKey = ''ZipCode''
                                AND CONVERT(INT, SUBSTRING(p.PostalCode, 1, 5)) BETWEEN c1.OptionValue
                                                              AND
                                                              ISNULL(c1.EndValue,
                                                              c1.OptionValue)
		WHERE c1.MapValue IS NULL;'
    END


        SET @tblScript = @tblScript + ' SELECT  *
        INTO    #RankedPremises
        FROM    ( SELECT    PremiseKey ,
                            AttributeKey ,
                            MapValue
                  FROM      #PremiseAttributes
                  WHERE     ErrorCode = 0
                ) p PIVOT ( MAX(MapValue) FOR AttributeKey IN ( ' + @cols
            + ' ) ) AS pvt
        WHERE   ' + @colsGrp + '; '

        SET @tblScript = @tblScript + 'SELECT  rp.PremiseKey ,
        MIN(BillPeriodStartDateKey) AS BillPeriodStartDateKey ,
        MIN(BillPeriodEndDateKey) AS BillPeriodEndDateKey ,
        CommodityKey ,
        fb.BillPeriodTypeKey ,
        SUM(TotalUnits) AS TotalUnits ,
        SUM(TotalCost) AS TotalCost ,
        Month ,
		MIN(fb.BillDays) AS BillDays ,
        MIN(DayOfMonth) AS DayOfMonth ,
        CASE WHEN MIN(fb.PremiseKey) IS NULL THEN 3
             WHEN MIN(fb.BillDays) NOT BETWEEN MIN([BenchmarkMinBillDays])
                                   AND         MIN([BenchmarkMaxBillDays])
             THEN 4
             WHEN SUM(TotalCost) NOT BETWEEN MIN([BenchmarkMinBillAmount])
                                 AND         MIN([BenchmarkMaxBillAmount])
             THEN 5
             ELSE 0
        END AS ErrorCode
INTO    #BillCustomers
FROM    [dbo].[BenchmarkBilling] fb
        INNER JOIN dbo.DimDate d ON d.DateKey = fb.BillPeriodEndDateKey
                                    AND d.Month = '''
            + CONVERT(VARCHAR(20), @BenchmarkDate) + '''
        INNER JOIN #cmsConfig bbc ON bbc.BillPeriodType = fb.BillPeriodTypeKey
        RIGHT JOIN #RankedPremises rp ON rp.PremiseKey = fb.PremiseKey
GROUP BY rp.PremiseKey ,
        CommodityKey ,
        fb.BillPeriodTypeKey ,
        MONTH;'

        SET @tblScript = @tblScript + 'INSERT  INTO [dbo].[BenchmarkErrors]
        ( [PremiseKey] ,
		  [ClientId] ,
          [BillMonth] ,
          [ErrorTypeKey] ,
          [ErrorInfo] ,
          [DateCreated]
        )
SELECT  PremiseKey ,'+ CONVERT(VARCHAR(10), @ClientID)+',
        ''' + CONVERT(VARCHAR(20), @BenchmarkDate) + ''' ,
        ErrorCode ,
        ''Bill Days:'' + CONVERT(VARCHAR(5), BillDays) + '', Bill Amount:''
        + CONVERT(VARCHAR(10), TotalCost) ,
        GETDATE()
FROM    #BillCustomers
WHERE   ErrorCode != 0;'


        SET @tblScript = @tblScript
            + 'SELECT  rp.* ,fb.Month,fb.BillPeriodStartDateKey,
		fb.BillPeriodEndDateKey,fb.BillPeriodTypeKey,
        c.MapValue AS BillCycle ,
		' + IIF(@ZipCodeEnabled = 1, 'c1.MapValue AS ZipCode,', '') + '
        CommodityKey ,
        TotalCost ,
        TotalUnits ,
        ROW_NUMBER() OVER ( PARTITION BY commodityKey, ' + @cols
            + ',fb.Month,fb.BillPeriodTypeKey, c.MapValue'
            + IIF(@ZipCodeEnabled = 1, ',c1.MapValue', '')
            + ' ORDER BY TotalUnits ) AS UsageRank ,
        COUNT(*) OVER ( PARTITION BY commodityKey, ' + @cols
            + ',fb.Month,fb.BillPeriodTypeKey, c.MapValue'
            + IIF(@ZipCodeEnabled = 1, ',c1.MapValue', '') + ' ) AS GroupSize,
			DENSE_RANK() OVER ( ORDER BY commodityKey, ' + @cols
            + ', fb.Month,fb.BillPeriodTypeKey, c.MapValue'
            + IIF(@ZipCodeEnabled = 1, ',c1.MapValue', '') + ' ) AS GroupId
INTO    #sCustomers
FROM    #RankedPremises rp
        INNER JOIN #BillCustomers fb ON fb.PremiseKey = rp.PremiseKey
        INNER JOIN #Config c ON c.AttributeKey = ''BillCycle''
		'
            + IIF(@ZipCodeEnabled = 1, ' INNER JOIN dbo.DimPremise p ON p.PremiseKey = rp.PremiseKey
		INNER JOIN #Config c1 ON c1.AttributeKey = ''ZipCode''', '')
            + '
WHERE   fb.ErrorCode = 0 AND fb.DayOfMonth BETWEEN c.OptionValue AND c.endValue'
            + IIF(@ZipCodeEnabled = 1, ' AND CONVERT(INT,SUBSTRING(p.PostalCode,1,5)) BETWEEN c1.OptionValue AND ISNULL(c1.EndValue,c1.OptionValue)', '')
            + ';'

        SET @tblScript = @tblScript
            + 'SELECT MIN(GroupId) As GroupId , commodityKey ,
        ' + @cols + ',
        Month As BillMonth ,BillCycle ,'
            + IIF(@ZipCodeEnabled = 1, 'ZipCode,', '') + '
        COUNT(*) AS PeersCount ,
        AVG(TotalCost) AS AverageCost ,
        AVG(TotalUnits) AS AverageUsage ,
        AVG(IIF(UsageRank * @EfficientPeerPercentage <= GroupSize, TotalUnits, NULL)) AS EfficientUsage ,
        AVG(IIF(UsageRank *  @EfficientPeerPercentage <= GroupSize, TotalCost, NULL)) AS EfficientCost
Into #Groups
FROM    #sCustomers
GROUP BY commodityKey ,
        ' + @cols + ' ,Month,BillPeriodTypeKey,
        BillCycle' + IIF(@ZipCodeEnabled = 1, ',ZipCode', '') + ';'

        SET @tblScript = @tblScript + 'DELETE bg  
OUTPUT Deleted.[ClientId]
      ,Deleted.[BillMonth]
      ,Deleted.[GroupId]
	  ,Deleted.[BenchmarkGroupTypeKey]
      ,Deleted.[CommodityKey]
      ,Deleted.[Criteria]
      ,Deleted.[PremiseCount]
      ,Deleted.[AverageUsage]
      ,Deleted.[EfficientUsage]
      ,Deleted.[AverageCost]
      ,Deleted.[EfficientCost]
      ,Deleted.[DateCreated]
	  ,GETDATE()
	  INTO [dbo].[BenchmarkGroupsHistory]
FROM [dbo].[BenchmarkGroups] bg
WHERE ClientId = ' + CONVERT(VARCHAR(10), @ClientID)
            + ' AND BenchmarkGroupTypeKey = '
            + CONVERT(VARCHAR(5), @BenchmarkGroupTypeKey)
            + ' AND BillMonth = ''' + CONVERT(VARCHAR(20), @BenchmarkDate)
            + ''''

        SET @tblScript = @tblScript + 'INSERT INTO [dbo].[BenchmarkGroups]
SELECT ' + CONVERT(VARCHAR(10), @ClientID) + ' AS ClientId,BillMonth,'
            + CONVERT(VARCHAR(5), @BenchmarkGroupTypeKey)
            + ',GroupId,CommodityKey,' + @colsCriteria
            + ' AS Criteria,PeersCount,AverageUsage,EfficientUsage,AverageCost,EfficientCost,GETDATE() AS DateCreated FROM #Groups;'

        SET @tblScript = @tblScript + 'DELETE b 
OUTPUT Deleted.[ClientId]
      ,Deleted.[PremiseKey]
      ,Deleted.[BillMonth]
      ,Deleted.[StartDateKey]
      ,Deleted.[EndDateKey]
      ,Deleted.[CommodityKey]
      ,Deleted.[BenchmarkGroupTypeKey]
      ,Deleted.[GroupId]
      ,Deleted.[GroupCount]
      ,Deleted.[MyUsage]
      ,Deleted.[AverageUsage]
      ,Deleted.[EfficientUsage]
      ,Deleted.[UOMKey]
      ,Deleted.[MyCost]
      ,Deleted.[AverageCost]
      ,Deleted.[EfficientCost]
      ,Deleted.[CostCurrencyCost]
      ,Deleted.[DateCreated]
	  ,GETDATE()
	  INTO [dbo].[BenchmarksHistory]
FROM [dbo].[Benchmarks] b
WHERE ClientId = ' + CONVERT(VARCHAR(10), @ClientID)
            + ' AND BenchmarkGroupTypeKey = '
            + CONVERT(VARCHAR(5), @BenchmarkGroupTypeKey)
            + ' AND BillMonth = ''' + CONVERT(VARCHAR(20), @BenchmarkDate)
            + ''';'

        SET @tblScript = @tblScript + 'INSERT INTO [dbo].[Benchmarks]
SELECT  ' + CONVERT(VARCHAR(10), @ClientID) + ' AS ClientId,
		dp.PremiseKey ,
		bp.BillMonth,
		sc.BillPeriodStartDateKey ,
		sc.BillPeriodEndDateKey ,
        sc.CommodityKey ,
		' + CONVERT(VARCHAR(5), @BenchmarkGroupTypeKey)
            + ' AS BenchmarkGroupTypeKey,
		sc.GroupId,
        bp.PeersCount ,
        sc.TotalUnits AS MyUsage ,
        bp.AverageUsage ,
        bp.EfficientUsage ,
		1,
        cast(sc.TotalCost as decimal(18,2)) AS MyCost  ,
        cast(bp.AverageCost as decimal(18,2)) ,
        cast(bp.EfficientCost  as decimal(18,2)) ,
        1,
		GETDATE()
		FROM    ( SELECT DISTINCT
        PremiseKey
FROM    ( SELECT    PremiseKey ,
                    ContentOptionKey ,
                    ROW_NUMBER() OVER ( PARTITION BY PremiseKey ORDER BY TrackingDate DESC ) AS SortID
          FROM      dbo.FactPremiseAttribute
          WHERE     ClientID = ' + CONVERT(VARCHAR(10), @ClientID)
            + '
                    AND ContentAttributeKey = ''paperreport.treatmentgroup.enrollmentstatus''
        ) p
WHERE   p.SortID = 1
        AND p.ContentOptionKey = ''paperreport.treatmentgroup.enrollmentstatus.enrolled'' ) dp
        INNER JOIN #sCustomers sc ON dp.PremiseKey = sc.PremiseKey
        INNER JOIN #Groups bp ON bp.GroupId = sc.GroupId
		WHERE bp.PeersCount >= ' + CONVERT(VARCHAR(5), @MinGroupSize) + '
		;'

        SELECT  @tblScript
        EXECUTE sp_executesql @tblScript

    END


GO
