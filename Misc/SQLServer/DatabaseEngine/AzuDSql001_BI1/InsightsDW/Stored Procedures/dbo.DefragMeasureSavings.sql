SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
-- =======================================
-- Written by:	Wayne
-- Date:		07.20.15
-- Description: Re indexes the [FactMeasure] table 
-- 				if it needs it.
-- =======================================
*/
CREATE PROCEDURE [dbo].[DefragMeasureSavings]
AS
BEGIN
SET NOCOUNT ON
DECLARE @frag DECIMAL(5,2);

SELECT @frag= avg_fragmentation_in_percent FROM sys.dm_db_index_physical_stats(DB_ID(), OBJECT_ID('dbo.FactMeasure'), 1, NULL, NULL)

IF @frag BETWEEN 10.00 AND 29.99
BEGIN
    PRINT 'reorgizing index, fragmentation was ' + CAST(@frag AS VARCHAR(10));
    ALTER INDEX [PK_FactMeasure] ON InsightsDW.[dbo].[FactMeasure] REORGANIZE WITH ( LOB_COMPACTION = ON )
    RETURN;
END

IF @frag > =30.00
BEGIN
    PRINT 'rebuilding index,  fragmentation was ' + CAST(@frag AS VARCHAR(10));
    ALTER INDEX [PK_FactMeasure] ON InsightsDW.[dbo].[FactMeasure] REBUILD PARTITION = ALL WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 50)
    RETURN;
END
PRINT 'index require no maitenance. fragmentation was ' + CAST(@frag AS VARCHAR(10));
SET NOCOUNT OFF

END

GO
