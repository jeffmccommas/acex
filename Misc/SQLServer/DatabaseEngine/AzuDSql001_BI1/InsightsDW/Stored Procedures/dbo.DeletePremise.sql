SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Written by:	 Wayne
date:		 3.23.15
Description:	 Deletes all rows in DW for a premise
*/
CREATE PROC [dbo].[DeletePremise]
    @premiseid VARCHAR(50) ,
    @client_id INT ,
    @verbose_flag INT = 0
AS
    SET NOCOUNT ON;
-- Declare error Vars
    DECLARE @ErrorNumber INT ,
        @ErrorLine INT ,
        @ErrorMessage NVARCHAR(4000) ,
        @ErrorSeverity INT ,
        @ErrorState INT;

    DECLARE @premise_key INT ,
        @DimPremise_count INT = 0 ,
        @FactPremiseAttribute_count INT = 0 ,
        @DimServicePoint_count INT = 0 ,
        @DimCustomer_count INT = 0 ,
        @FactCustomerPremise_count INT = 0 ,
        @FactSPBilling_count INT = 0 ,
        @FactAmi_count INT = 0 ,
        @FactAmiDetail_count INT = 0 ,
        @FactAction_count INT = 0 ,
        @FactActionData_count INT = 0 ,
        @FactActionItem_count INT = 0 ,
        @Benchmarks_count INT = 0 ,
        @counts_dont_agree INT = 0 ,
        @new_ActionDetailKey INT= 0 ,
        @message NVARCHAR(200);

--Start a try block
    BEGIN TRY
        SELECT  @premise_key = PremiseKey
        FROM    InsightsDW.dbo.DimPremise
        WHERE   ClientId = @client_id
                AND PremiseId = @premiseid;
        IF @premise_key IS NULL
            OR @premise_key = 0
            BEGIN
                SET @message = N'The premise Id does not exist for client '
                    + CAST(@client_id AS NVARCHAR(10));
                RAISERROR(@message , 12 , 1);
            END;
        IF @verbose_flag = 1
            PRINT 'deleting premise ' + @premiseid;
--populate source counts

        SELECT  @DimPremise_count = COUNT(*)
        FROM    InsightsDW.dbo.DimPremise
        WHERE   PremiseId = @premiseid
                AND ClientId = @client_id;
        SELECT  @FactPremiseAttribute_count = COUNT(*)
        FROM    InsightsDW.dbo.FactPremiseAttribute fpa
        WHERE   PremiseID = @premiseid
                AND ClientID = @client_id;

        SELECT  @DimCustomer_count = COUNT(*)
        FROM    InsightsDW.dbo.DimCustomer c
                INNER JOIN InsightsDW.dbo.FactCustomerPremise fcp ON fcp.CustomerKey = c.CustomerKey
                INNER JOIN dbo.DimPremise dp ON dp.PremiseKey = fcp.PremiseKey
        WHERE   PremiseId = @premiseid
                AND dp.ClientId = @client_id;
        SELECT  @DimServicePoint_count = COUNT(*)
        FROM    InsightsDW.dbo.DimServicePoint fpa
        WHERE   PremiseId = @premiseid
                AND ClientId = @client_id;

        SELECT  @FactSPBilling_count = COUNT(*)
        FROM    InsightsDW.dbo.FactServicePointBilling
        WHERE   PremiseId = @premiseid
                AND ClientId = @client_id;
	 --
        SELECT  @FactAmi_count = COUNT(*)
        FROM    InsightsDW.dbo.FactAmi
                INNER JOIN dbo.DimPremise dp ON dp.PremiseKey = FactAmi.PremiseKey
        WHERE   PremiseId = @premiseid
                AND dp.ClientId = @client_id;

        SELECT  @FactAmiDetail_count = COUNT(*)
        FROM    InsightsDW.dbo.FactAmiDetail fad
        WHERE   PremiseId = @premiseid
                AND ClientId = @client_id;
	 --
        SELECT  @Benchmarks_count = COUNT(*)
        FROM    InsightsDW.dbo.Benchmarks
                INNER JOIN dbo.DimPremise dp ON dp.PremiseKey = Benchmarks.PremiseKey
        WHERE   PremiseId = @premiseid
                AND dp.ClientId = @client_id;
	 --
	 -- Actions
	 --
        SELECT  @FactAction_count = COUNT(*)
        FROM    InsightsDW.dbo.factAction fa
                INNER JOIN dbo.DimPremise dp ON dp.PremiseKey = fa.PremiseKey
        WHERE   dp.PremiseId = @premiseid
                AND dp.ClientId = @client_id;
	 	 --
        SELECT  @FactActionItem_count = COUNT(*)
        FROM    InsightsDW.dbo.FactActionItem fai
                INNER JOIN dbo.DimPremise dp ON dp.PremiseKey = fai.PremiseKey
        WHERE   dp.PremiseId = @premiseid
                AND dp.ClientId = @client_id;
	 	 --
        SELECT  @FactActionData_count = COUNT(*)
        FROM    InsightsDW.dbo.factActionData fad
                INNER JOIN InsightsDW.dbo.factAction fai ON fai.ActionDetailKey = fad.ActionDetailKey
                INNER JOIN dbo.DimPremise dp ON dp.PremiseKey = fai.PremiseKey
        WHERE   dp.PremiseId = @premiseid
                AND dp.ClientId = @client_id;

        IF @verbose_flag = 1
            BEGIN
--Print source counts
                PRINT 'There are ' + CAST(@DimPremise_count AS VARCHAR(10))
                    + ' row(s) in the DimPremise table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are '
                    + CAST(@FactPremiseAttribute_count AS VARCHAR(10))
                    + ' row(s) in the FactPremiseAttribute table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are '
                    + CAST(@DimServicePoint_count AS VARCHAR(10))
                    + ' row(s) in the DimServicePoint table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are ' + CAST(@DimCustomer_count AS VARCHAR(10))
                    + ' row(s) in the DimCustomer table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are ' + CAST(@FactSPBilling_count AS VARCHAR(10))
                    + ' row(s) in the FactServicePointBilling table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are ' + CAST(@FactAmi_count AS VARCHAR(10))
                    + ' row(s) in the FactAmi table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are ' + CAST(@FactAmiDetail_count AS VARCHAR(10))
                    + ' row(s) in the FactAmiDetail table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are ' + CAST(@Benchmarks_count AS VARCHAR(10))
                    + ' row(s) in the Benchmarks table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
    --
    -- actions
    --
                PRINT 'There are ' + CAST(@FactAction_count AS VARCHAR(10))
                    + ' row(s) in the FactAction table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are '
                    + CAST(@FactActionData_count AS VARCHAR(10))
                    + ' row(s) in the FactActionData table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are '
                    + CAST(@FactActionItem_count AS VARCHAR(10))
                    + ' row(s) in the FactActionItem table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;		  
            END;

        BEGIN TRANSACTION;

--need to get some id to keep counts ok
        DECLARE @hold_customerid VARCHAR(50)= ( SELECT  c.CustomerId
                                                FROM    InsightsDW.dbo.FactCustomerPremise fcp
                                                        INNER JOIN dbo.DimCustomer c ON c.CustomerKey = fcp.CustomerKey
                                                        INNER JOIN dbo.DimPremise dp ON dp.PremiseKey = fcp.PremiseKey
                                                WHERE   dp.PremiseId = @premiseid
                                                        AND dp.ClientId = @client_id
                                              );

        DECLARE @hold_premisekey INT= ( SELECT  PremiseKey
                                        FROM    InsightsDW.dbo.DimPremise
                                        WHERE   PremiseId = @premiseid
                                                AND ClientId = @client_id
                                      );

        DECLARE @hold_amikey INT= ( SELECT  AmiKey
                                    FROM    InsightsDW.dbo.FactAmi
                                    WHERE   PremiseKey = @hold_premisekey
                                            AND ClientId = @client_id
                                  );

        DECLARE @hold_ActionDetailKey INT= ( SELECT ActionDetailKey
                                             FROM   InsightsDW.dbo.factAction
                                             WHERE  PremiseKey = @hold_premisekey
                                                    AND ClientID = @client_id
                                           );

        DECLARE @hold_ActionItemKey INT= ( SELECT   ActionDetailKey
                                           FROM     InsightsDW.dbo.factAction fa
                                           WHERE    fa.ActionDetailKey = @hold_ActionDetailKey
                                                    AND ClientID = @client_id
                                         );

-- if no other premise is using customer
-- then delete customer
        DECLARE @otherpremsesusingcust INT= 0;

        SELECT  @otherpremsesusingcust = COUNT(*)
        FROM    ( SELECT    c.CustomerId ,
                            dp.PremiseId
                  FROM      InsightsDW.dbo.FactCustomerPremise fcp
                            INNER JOIN dbo.DimCustomer c ON c.CustomerKey = fcp.CustomerKey
                            INNER JOIN dbo.DimPremise dp ON dp.PremiseKey = fcp.PremiseKey
                  WHERE     dp.ClientId = @client_id
                            AND c.CustomerId = ( SELECT c.CustomerId
                                                 FROM   InsightsDW.dbo.FactCustomerPremise fcp
                                                        INNER JOIN dbo.DimCustomer c ON c.CustomerKey = fcp.CustomerKey
                                                        INNER JOIN dbo.DimPremise dp ON dp.PremiseKey = fcp.PremiseKey
                                                 WHERE  dp.ClientId = @client_id
                                                        AND dp.PremiseId = @premiseid
                                               )
                ) AS s
        WHERE   s.PremiseId <> @premiseid;

        IF COALESCE(@otherpremsesusingcust, 0) = 0
            BEGIN
                PRINT 'There are '
                    + CAST(COALESCE(@otherpremsesusingcust, 0) AS VARCHAR(3))
                    + ' premises using the same customer - it will be deleted';
                DELETE  InsightsDW.dbo.DimCustomer
                FROM    InsightsDW.dbo.FactCustomerPremise fcp
                        INNER JOIN dbo.DimCustomer c ON c.CustomerKey = fcp.CustomerKey
                        INNER JOIN dbo.DimPremise dp ON dp.PremiseKey = fcp.PremiseKey
                WHERE   dp.PremiseId = @premiseid
                        AND dp.ClientId = @client_id;
            END;
        ELSE
            BEGIN
                PRINT 'There are '
                    + CAST(COALESCE(@otherpremsesusingcust, 0) AS VARCHAR(3))
                    + ' premises using the same customer - it will be not be deleted';
            END;
--

--

        DELETE  InsightsDW.dbo.FactCustomerPremise
        FROM    InsightsDW.dbo.FactCustomerPremise fcp
        WHERE   fcp.PremiseKey = @hold_premisekey;

--
        DELETE  InsightsDW.dbo.Benchmarks
        WHERE   PremiseKey = @hold_premisekey
                AND ClientId = @client_id;
--
        DELETE  dbo.FactPremiseAttribute
        WHERE   PremiseKey = @hold_premisekey
                AND ClientID = @client_id;
--
        DELETE  InsightsDW.dbo.FactServicePointBilling
        WHERE   PremiseId = @premiseid
                AND ClientId = @client_id;

-- ami
        DELETE  InsightsDW.dbo.FactAmiDetail
        FROM    InsightsDW.dbo.FactAmiDetail d
        WHERE   d.PremiseId = @premiseid
                AND d.ClientId = @client_id;

        DELETE  InsightsDW.dbo.FactAmi
        WHERE   PremiseKey = @hold_premisekey
                AND ClientId = @client_id;
--
        DELETE  FROM dbo.DimServicePoint
        WHERE   PremiseKey = @hold_premisekey
                AND ClientId = @client_id;

--action
        IF @hold_ActionDetailKey IS NOT NULL
            BEGIN
                DELETE  InsightsDW.dbo.factActionData
                WHERE   ActionDetailKey = @hold_ActionDetailKey;
            END;
--
        DELETE  InsightsDW.dbo.FactActionItem
        WHERE   PremiseKey = @hold_premisekey;

--
        DELETE  InsightsDW.dbo.factAction
        WHERE   PremiseKey = @hold_premisekey
                AND ClientID = @client_id;
 --

        DELETE  dbo.DimPremise
        WHERE   PremiseId = @premiseid
                AND ClientId = @client_id;


--populate after counts

        SELECT  @DimCustomer_count = COUNT(*)
        FROM    InsightsDW.dbo.DimCustomer c
        WHERE   c.CustomerId = @hold_customerid
                AND c.ClientId = @client_id;

        SELECT  @DimServicePoint_count = COUNT(*)
        FROM    InsightsDW.dbo.DimServicePoint fpa
        WHERE   PremiseId = @premiseid
                AND ClientId = @client_id;

        SELECT  @FactSPBilling_count = COUNT(*)
        FROM    InsightsDW.dbo.FactServicePointBilling
        WHERE   PremiseId = @premiseid
                AND ClientId = @client_id;	 
	   --
	   -- Ami
	   --
        SELECT  @FactAmiDetail_count = COUNT(*)
        FROM    InsightsDW.dbo.FactAmiDetail fad
        WHERE   PremiseId = @premiseid
                AND ClientId = @client_id;
	   --
        SELECT  @FactAmi_count = COUNT(*)
        FROM    InsightsDW.dbo.FactAmi
        WHERE   FactAmi.PremiseKey = @hold_premisekey
                AND FactAmi.ClientId = @client_id;
	   --
        SELECT  @Benchmarks_count = COUNT(*)
        FROM    InsightsDW.dbo.Benchmarks b
        WHERE   @hold_premisekey = b.PremiseKey
                AND b.ClientId = @client_id;
	   --
	   -- Actions
	   --
        SELECT  @FactAction_count = COALESCE(( SELECT   COUNT(*)
                                               FROM     InsightsDW.dbo.factAction fa
                                               WHERE    fa.ActionDetailKey = @hold_ActionDetailKey
                                             ), 0);
	   --
        SELECT  @FactActionItem_count = COALESCE(( SELECT   COUNT(*)
                                                   FROM     InsightsDW.dbo.FactActionItem fai
                                                   WHERE    fai.PremiseKey = @hold_premisekey
                                                 ), 0);
	   --
        SELECT  @FactActionData_count = COUNT(*)
        FROM    InsightsDW.dbo.factActionData fad
        WHERE   fad.ActionDetailKey = @hold_ActionDetailKey;
		  
--

	 
        SELECT  @DimPremise_count = COUNT(*)
        FROM    InsightsDW.dbo.DimPremise
        WHERE   PremiseId = @premiseid
                AND ClientId = @client_id;

        SELECT  @FactPremiseAttribute_count = COUNT(*)
        FROM    InsightsDW.dbo.FactPremiseAttribute fpa
        WHERE   PremiseID = @premiseid
                AND ClientID = @client_id;


        IF @verbose_flag = 1
            BEGIN
--Print after delete counts
                PRINT '***** After the DELETE ******';
                PRINT 'There are ' + CAST(@DimPremise_count AS VARCHAR(10))
                    + ' row(s) in the DimPremise table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are '
                    + CAST(@FactPremiseAttribute_count AS VARCHAR(10))
                    + ' row(s) in the FactPremiseAttribute table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are '
                    + CAST(@DimServicePoint_count AS VARCHAR(10))
                    + ' row(s) in the DimServicePoint table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are ' + CAST(@DimCustomer_count AS VARCHAR(10))
                    + ' row(s) in the DimCustomer table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are ' + CAST(@FactSPBilling_count AS VARCHAR(10))
                    + ' row(s) in the FactServicePointBilling table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are ' + CAST(@FactAmi_count AS VARCHAR(10))
                    + ' row(s) in the FactAmi table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are ' + CAST(@FactAmiDetail_count AS VARCHAR(10))
                    + ' row(s) in the FactAmiDetail table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are ' + CAST(@Benchmarks_count AS VARCHAR(10))
                    + ' row(s) in the Benchmarks table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
    --
    -- actions
    --
                PRINT 'There are ' + CAST(@FactAction_count AS VARCHAR(10))
                    + ' row(s) in the FactAction table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are '
                    + CAST(@FactActionData_count AS VARCHAR(10))
                    + ' row(s) in the FactActionData table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;
                PRINT 'There are '
                    + CAST(@FactActionItem_count AS VARCHAR(10))
                    + ' row(s) in the FactActionItem table for client '
                    + CAST(@client_id AS VARCHAR(10)) + ' and premise id '
                    + @premiseid;		  
            END;

--print descrepancies
        SET @counts_dont_agree = 0;
        IF @DimPremise_count != 0
            BEGIN
                PRINT 'DimPremise counts is not 0 !!  --ERROR';
                SET @counts_dont_agree = 1;
            END;
        IF @FactPremiseAttribute_count != 0
            BEGIN
                PRINT 'FactPremiseAttribute counts is not 0 !!  --ERROR';
                SET @counts_dont_agree = 1;
            END;
        IF @DimServicePoint_count != 0
            BEGIN
                PRINT 'DimServicePoint counts is not 0 !!  --ERROR';
                SET @counts_dont_agree = 1;
            END;
        IF @DimCustomer_count != 0
            AND @otherpremsesusingcust = 0
            BEGIN
                PRINT 'DimCustomer counts is not 0 !!  --ERROR';
                SET @counts_dont_agree = 1;
            END;
        IF @DimCustomer_count != 0
            AND @otherpremsesusingcust != 0
            BEGIN
                PRINT 'its ok that we didnt delete customer id='
                    + @hold_customerid + ' , it is in use by other premises';
		 
            END;
        IF @FactCustomerPremise_count != 0
            BEGIN
                PRINT 'FactCustomerPremise_count  is not 0 !!  --ERROR';
                SET @counts_dont_agree = 1;
            END;
        IF @FactSPBilling_count != 0
            BEGIN
                PRINT 'FactServicePointBilling counts is not 0 !!  --ERROR';
                SET @counts_dont_agree = 1;
            END;
        IF @FactAmi_count != 0
            BEGIN
                PRINT 'FactAmi counts is not 0 !!  --ERROR';
                SET @counts_dont_agree = 1;
            END;
        IF @FactAmiDetail_count != 0
            BEGIN
                PRINT 'FactAmiDetail counts is not 0 !!  --ERROR';
                SET @counts_dont_agree = 1;
            END; 
        IF @Benchmarks_count != 0
            BEGIN
                PRINT 'Benchmarks counts is not 0 !!  --ERROR';
                SET @counts_dont_agree = 1;
            END; 
--
-- actions
--
        IF @FactAction_count != 0
            BEGIN
                PRINT 'FactAction counts is not 0 !!  --ERROR';
                SET @counts_dont_agree = 1;
            END; 
        IF @FactActionData_count != 0
            BEGIN
                PRINT 'FactActionData counts is not 0 !!  --ERROR';
                SET @counts_dont_agree = 1;
            END; 
        IF @FactActionItem_count != 0
            BEGIN
                PRINT 'FactActionItem counts is not 0 !!  --ERROR';
                SET @counts_dont_agree = 1;
            END; 
        IF @counts_dont_agree = 1
            BEGIN
                RAISERROR('Row count error' , 12 , 1);
            END;
        ELSE
            PRINT 'All counts OK ';

--counts agree, ready to committ
        COMMIT TRANSACTION;

    END TRY
    BEGIN CATCH

        IF @@TRANCOUNT > 0
            BEGIN
                PRINT 'Performing ROLLBACK';
                ROLLBACK TRANSACTION;
            END;
        SET @ErrorNumber = ERROR_NUMBER();
        SET @ErrorLine = ERROR_LINE();
        SET @ErrorMessage = 'Error executing  CopyPremiseToAnotherClient '
            + ERROR_MESSAGE();
        SET @ErrorSeverity = ERROR_SEVERITY();
        SET @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage , @ErrorSeverity , @ErrorState);
        RETURN;
    END CATCH;

    SET NOCOUNT OFF;
--success
    RETURN 0;
GO
