SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Merge into Fact Events
*/
CREATE PROC [dbo].[MergeEventsIntoDW]
@clientId INT
AS
BEGIN
    SET NOCOUNT ON;
    IF OBJECT_ID( 'tempdb..#Changes') IS NOT NULL 
    DROP TABLE #Changes;
 
    CREATE TABLE #Changes(
	  ChangeType         NVARCHAR(10)
	 ,PremiseKey		  INT
	);
 

    MERGE InsightsDW.[dbo].[FactEvent] AS Target
    USING (
    SELECT * FROM
    (
    SELECT  
			   e.ClientID
			 , c.CustomerKey
			 , dp.PremiseKey
			 , d.datekey
			 , e.EventTypeID
			 , e.FlagDateCreated
			 , e.FlagDateUpdated
			 , e.TrackingId
			 , e.TrackingDate 
			 , ROW_NUMBER() OVER(PARTITION BY 
			 e.ClientID
			 , c.CustomerKey
			 , dp.PremiseKey
			 , d.datekey
			 , e.EventTypeID ORDER BY datekey  DESC) AS rnk 
    FROM InsightsBulk.Holding.[Events] e
    INNER JOIN InsightsDW.dbo.DimCustomer c
    ON c.CustomerId = e.CustomerId AND c.ClientId=e.ClientID
    INNER JOIN InsightsDW.dbo.DimPremise dp
    ON dp.PremiseId = e.PremiseId AND dp.AccountId=e.AccountId AND dp.ClientId=e.ClientID
    INNER JOIN insightsdw.dbo.DimDate d
    ON d.FullDateAlternateKey=e.EventDate
    WHERE e.ClientID=@clientId
    ) s
    WHERE rnk=1
    ) AS Source
    ON  Source.ClientID=Target.ClientID
    AND Source.CustomerKey =Target.CustomerKey
    AND Source.PremiseKey=Target.PremiseKey
    AND Source.DateKey=Target.DateKey
    AND Source.EventTypeID=Target.EventTypeID
    WHEN MATCHED 
	   THEN UPDATE  SET
	   Target.DateKey=Source.DateKey,
	   TARGET.[TrackingId]=SOURCE.[TrackingId],
	   TARGET.[TrackingDate]=Source.[TrackingDate]
    WHEN NOT MATCHED
	   THEN INSERT 
	   (
			[ClientID]
		    ,[CustomerKey]
		    ,[PremiseKey]
		    ,[DateKey]
		    ,[EventTypeID]
		    ,[FlagDateCreated]
		    ,[FlagDateUpdated]
		    ,[TrackingId]
		    ,[TrackingDate]
	   )
	   VALUES 
	   (
			   Source.ClientID
			 , Source.CustomerKey
			 , Source.PremiseKey
			 , Source.datekey
			 , Source.EventTypeID
			 , source.FlagDateCreated
			 , source.FlagDateUpdated
			 , Source.TrackingId
			 , Source.TrackingDate
	   )
	   OUTPUT
	   $ACTION ChangeType,
	   Source.PremiseKey
	   INTO #Changes;
	   SELECT '********************* SUMMARY *************************'
	   UNION ALL
	   SELECT '*      Total ' + CAST(ChangeType AS VARCHAR(15)) + 'S to FactEvents was ' + CAST(COUNT(*) AS VARCHAR(10)) + '      *' AS Message FROM #Changes GROUP BY ChangeType
	   UNION ALL
	   SELECT '*************************************************************'

DELETE InsightsBulk.Holding.[Events]
WHERE ClientID=@clientId

    SET NOCOUNT OFF;
END 

GO
