SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ubaid Tariq	
-- Create date: 12/16/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_INF_FactAction_DimPremise]
    @ETL_LogId INT ,
    @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        INSERT  INTO [InsightsDW].[dbo].[DimPremise]
                ( PostalCodeKey ,
                  CityKey ,
                  SourceKey ,
                  PremiseId ,
                  AccountId ,
                  Street1 ,
                  Street2 ,
                  City ,
                  StateProvince ,
                  Country ,
                  PostalCode ,
                  GasService ,
                  ElectricService ,
                  WaterService ,
                  CreateDate ,
                  ClientId ,
                  GeoLocation ,
                  SourceId ,
                  ETL_LogId ,
                  IsInferred ,
                  TrackingId ,
                  TrackingDate
                )
                SELECT  DISTINCT
                        -1 ,
                        -1 ,
                        SourceKey ,
                        PremiseId ,
                        AccountId ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        GETUTCDATE() ,
                        ClientId ,
                        NULL ,
                        SourceId ,
                        @ETL_LogId ,
                        1 ,
                        GETUTCDATE() ,
                        GETUTCDATE()
                FROM    ( SELECT    kfa.ClientId ,
                                    dcl.ClientKey ,
                                    kfa.PremiseID ,
                                    kfa.AccountID ,
                                    ds.SourceKey ,
                                    kfa.SourceID ,
                                    ROW_NUMBER() OVER ( PARTITION BY kfa.ClientId,
                                                        kfa.PremiseID,
                                                        kfa.AccountID ORDER BY kfa.SourceID ) AS SortId
                          FROM      ETL.KEY_FactAction kfa WITH ( NOLOCK )
                                    INNER JOIN dbo.DimClient dcl WITH ( NOLOCK ) ON dcl.ClientId = kfa.ClientId
                                    INNER JOIN dbo.DimSource ds WITH ( NOLOCK ) ON ds.SourceId = kfa.SourceId
                                    LEFT JOIN dbo.DimPremise dp WITH ( NOLOCK ) ON dp.PremiseId = kfa.PremiseId
                                                              AND dp.ClientId = kfa.ClientId
                                                              AND dp.AccountId = kfa.AccountId
                          WHERE     kfa.ClientID = @ClientID
                                    AND dp.PremiseId IS NULL
                        ) p
                WHERE   SortId = 1


    END

														 




GO
