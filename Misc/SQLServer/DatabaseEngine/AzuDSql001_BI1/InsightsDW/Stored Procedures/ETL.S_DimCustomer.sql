SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/21/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[S_DimCustomer]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;

		SELECT CustomerKey, dc.ClientId, dc.CustomerId, FirstName, LastName, Street1, Street2, City, StateProvince, Country, PostalCode, PhoneNumber, MobilePhoneNumber, EmailAddress, AlternateEmailAddress, SourceId, AuthenticationTypeId
		FROM [dbo].[DimCustomer] dc
		INNER JOIN [ETL].[KEY_DimCustomer] kdc WITH (NOLOCK) ON kdc.ClientId = dc.ClientId 
																AND kdc.CustomerId = dc.CustomerId
		WHERE kdc.ClientId = @ClientID
		ORDER BY kdc.ClientId, kdc.CustomerId


END





GO
