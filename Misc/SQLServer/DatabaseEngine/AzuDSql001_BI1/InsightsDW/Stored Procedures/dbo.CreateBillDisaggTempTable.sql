SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
-- =======================================
-- Written by:	Wayne
-- Date:		07.24.15
-- Description: Create the BillDisaggTemp temp table
-- 				
-- =======================================
*/
CREATE PROCEDURE [dbo].[CreateBillDisaggTempTable]
(
    @ClientID       INT = 101
)
AS
BEGIN
DECLARE @sqlstring VARCHAR(2000)=
'
IF OBJECT_ID(''dbo.BillDisaggTemp' + CAST(@ClientID AS VARCHAR(5)) + ''') IS NOT NULL
    DROP TABLE dbo.BillDisaggTemp' + CAST(@ClientID AS VARCHAR(5)) + ' 
CREATE TABLE  dbo.BillDisaggTemp' + CAST(@ClientID AS VARCHAR(5)) + ' (
    [ClientID] [INT] NOT NULL,
	[CustomerId] [VARCHAR](50) NOT NULL,
	[AccountId] [VARCHAR](50) NOT NULL,
	[PremiseId] [VARCHAR](50) NOT NULL,
	[BillDate] [DATETIME] NOT NULL,
	[EndUseKey] [VARCHAR](50) NOT NULL,
	[ApplianceKey] [VARCHAR](50) NOT NULL,
	[CommodityKey] [VARCHAR](50) NOT NULL,
	[PeriodId] [TINYINT] NOT NULL,
	[Usage] [DECIMAL](18, 6) NOT NULL,
	[Cost] [DECIMAL](18, 6) NOT NULL,
	[UomKey] [VARCHAR](16) NOT NULL,
	[CurrencyKey] [VARCHAR](16) NOT NULL,
	[Confidence] [INT] NULL,
	[ReconciliationRatio] [DECIMAL](18, 6) NULL,
	 [StatusID] [INT] NULL,
	 [NewDate] [DATETIME] NOT NULL
) ON [PRIMARY]'
EXECUTE (@sqlstring)
END

GO
