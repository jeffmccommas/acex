SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 4/2/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_FactCustomerPremise]
    @ClientID INT ,
    @ETLLogID INT
AS
    BEGIN

        SET NOCOUNT ON;

        INSERT  INTO FactCustomerPremise
                ( CustomerKey ,
                  PremiseKey ,
                  ETL_LogId ,
                  DateCreated
                )
                SELECT  c.CustomerKey ,
                        p.PremiseKey ,
                        @ETLLogID ,
                        GETUTCDATE()
                FROM    ETL.KEY_FactAction kfa
                        INNER JOIN DimCustomer c WITH ( NOLOCK ) ON c.ClientId = kfa.ClientId
                                                              AND c.CustomerId = kfa.CustomerId
                        INNER JOIN DimPremise p WITH ( NOLOCK ) ON p.ClientId = kfa.ClientId
                                                              AND p.PremiseID = kfa.PremiseId
                                                              AND p.AccountId = kfa.AccountId
                        LEFT JOIN FactCustomerPremise fcp WITH ( NOLOCK ) ON fcp.CustomerKey = c.CustomerKey
                                                              AND fcp.PremiseKey = p.PremiseKey
                WHERE   kfa.ClientID = @ClientID
                        AND fcp.PremiseKey IS NULL
                GROUP BY c.clientid ,
                        c.customerid ,
                        p.premiseid ,
                        c.CustomerKey ,
                        p.premisekey

        INSERT  INTO FactCustomerPremise
                ( CustomerKey ,
                  PremiseKey ,
                  ETL_LogId ,
                  DateCreated
                )
                SELECT  c.CustomerKey ,
                        p.PremiseKey ,
                        @ETLLogID ,
                        GETUTCDATE()
                FROM    ETL.KEY_FactPremiseAttribute kfpa
                        INNER JOIN DimCustomer c WITH ( NOLOCK ) ON c.ClientId = kfpa.ClientId
                                                              AND c.CustomerId = kfpa.CustomerId
                        INNER JOIN DimPremise p WITH ( NOLOCK ) ON p.ClientId = kfpa.ClientId
                                                              AND p.PremiseID = kfpa.PremiseId
                                                              AND p.AccountId = kfpa.AccountId
                        LEFT JOIN FactCustomerPremise fcp WITH ( NOLOCK ) ON fcp.CustomerKey = c.CustomerKey
                                                              AND fcp.PremiseKey = p.PremiseKey
                WHERE   kfpa.ClientID = @ClientID
                        AND fcp.PremiseKey IS NULL
                GROUP BY c.clientid ,
                        c.customerid ,
                        p.premiseid ,
                        c.CustomerKey ,
                        p.premisekey

        INSERT  INTO FactCustomerPremise
                ( CustomerKey ,
                  PremiseKey ,
                  ETL_LogId ,
                  DateCreated
                )
                SELECT  c.CustomerKey ,
                        p.PremiseKey ,
                        @ETLLogID ,
                        GETUTCDATE()
                FROM    ETL.KEY_FactBilling kfb
                        INNER JOIN DimCustomer c WITH ( NOLOCK ) ON c.ClientId = kfb.ClientId
                                                              AND c.CustomerId = kfb.CustomerId
                        INNER JOIN DimPremise p WITH ( NOLOCK ) ON p.ClientId = kfb.ClientId
                                                              AND p.PremiseID = kfb.PremiseId
                                                              AND p.AccountId = kfb.AccountId
                        LEFT JOIN FactCustomerPremise fcp WITH ( NOLOCK ) ON fcp.CustomerKey = c.CustomerKey
                                                              AND fcp.PremiseKey = p.PremiseKey
                WHERE   kfb.ClientID = @ClientID
                        AND fcp.PremiseKey IS NULL
                GROUP BY c.clientid ,
                        c.customerid ,
                        p.premiseid ,
                        c.CustomerKey ,
                        p.premisekey

    END

														 



GO
