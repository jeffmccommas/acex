SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Mike Ganley
-- Create date: 10/27/2014
-- Description:	InsightsDW Get Consumption V1 (used by Insights API callers)
-- Revisions
-- 10/27/2014  MKG   Initial version to get daily results only, no matter resultion provided, not enough fidelty nor detail in tables to 
--                   properly aggregate or select data for aggregation.  The ResolutionKey is hard-coded on the response to DAILY (7) right now, 
--					 since it really does not exist in the daily ami data.
-- 10/28/2014  MKG   Added paging and pagesize for proper paging support.
-- 10/30/2014  MKG   Modified return values of integers to string where appropraite and used Key in names.
--                   Modified Resolution, changed type to varchar(20), added PageIndex and PageSize to response, changed order
-- =============================================
CREATE PROCEDURE [dbo].[GetConsumption]
    @ClientID AS INT ,
    @CustomerID AS VARCHAR(50) ,
    @AccountID AS VARCHAR(50),
	@PremiseID AS VARCHAR(50),
	@ServicePointID AS VARCHAR(50),
    @StartDate AS DATE,
    @EndDate AS DATE,
	@ResolutionKey AS VARCHAR(20) = NULL,
	@Page As Int = 1,
	@PageSize As Int = 10000
AS 
BEGIN
	SET NOCOUNT ON;

	DECLARE @FirstRec int
	DECLARE @LastRec int

	SELECT @FirstRec = (@Page - 1) * @PageSize
	SELECT @LastRec = (@Page * @PageSize) + 1;

	SELECT TOP (1)	FactAmiDetail.ClientId,
					FactAmiDetail.TotalUnits, 
					FactAmiDetail.CustomerId,
					FactAmiDetail.AccountId,
					FactAmiDetail.PremiseId,
					FactAmiDetail.ServicePointId,
					DimCommodity.CommodityDesc As CommodityKey,
					DimUOM.UOMDesc As UOMKey,
					'day' As ResolutionKey,			--this can only ever be 'day' right now
					@Page As PageIndex,
					@PageSize As PageSize
	FROM FactAmiDetail WITH (nolock) INNER JOIN
            DimDate AS d WITH (nolock) ON FactAmiDetail.DateKey = d.DateKey INNER JOIN
            DimUOM WITH (nolock) ON FactAmiDetail.UOMId = DimUOM.UOMId INNER JOIN
            DimCommodity WITH (nolock) ON FactAmiDetail.CommodityId = DimCommodity.CommodityId
	WHERE (d.FullDateAlternateKey BETWEEN @StartDate AND @EndDate) 
		AND (FactAmiDetail.ClientId = @ClientID ) 
		AND (FactAmiDetail.CustomerId = @CustomerID) 
		AND (FactAmiDetail.AccountId = @AccountID) 
		AND (FactAmiDetail.PremiseId = @PremiseID) 
		AND (FactAmiDetail.ServicePointId = @ServicePointID)

	-- paging of result by using the Page and PageSize parameters
	-- will be helpful someday when we have smaller intervals; total selection size here equates to a year of 15min; a little under 106k values
	;WITH TempResult as
	(
	SELECT TOP 106000 ROW_NUMBER() OVER(ORDER BY FullDateAlternateKey DESC) as RowNum,
		  d.FullDateAlternateKey As DateStamp, 
		  FactAmiDetail.TotalUnits AS Quantity
		FROM FactAmiDetail WITH (nolock) INNER JOIN
            DimDate AS d WITH (nolock) ON FactAmiDetail.DateKey = d.DateKey
		WHERE (d.FullDateAlternateKey BETWEEN @StartDate AND @EndDate) 
		 AND (FactAmiDetail.ClientId = @ClientID ) 
		 AND (FactAmiDetail.CustomerId = @CustomerID) 
		 AND (FactAmiDetail.AccountId = @AccountID) 
		 AND (FactAmiDetail.PremiseId = @PremiseID) 
		 AND (FactAmiDetail.ServicePointId = @ServicePointID)
	)
	SELECT TOP (@LastRec-1) *
		FROM TempResult
		WHERE RowNum > @FirstRec 
		 AND RowNum < @LastRec


END



GO
