SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ubaid Tariq
-- Create date: 12/16/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[D_STG_FactAction]
				 @ClientID INT
AS

BEGIN

	SET NOCOUNT ON;

	DELETE ETL.INS_FactAction WHERE ClientID = @ClientID

END





GO
