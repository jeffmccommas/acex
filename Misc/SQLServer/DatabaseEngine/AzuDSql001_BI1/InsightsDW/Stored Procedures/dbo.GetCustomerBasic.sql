SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GetCustomerBasic]
    @ClientID AS INT,
    @CustomerID AS VARCHAR(50),
    @AccountID AS VARCHAR(50),
    @PremiseID AS VARCHAR(50)  
AS 
BEGIN
	SET NOCOUNT ON;

		SELECT dbo.DimCustomer.ClientId, dbo.DimCustomer.CustomerId, dbo.DimPremise.AccountId, dbo.DimPremise.PremiseId, 
			   dbo.DimCustomer.PostalCode AS CustomerPostalCode, dbo.DimPremise.PostalCode AS PremisePostalCode, dbo.FactCustomerPremise.DateCreated, 
			   dbo.DimCustomer.City As CustomerCity, dbo.DimCustomer.StateProvince As CustomerState, 
			   dbo.DimPremise.City AS PremiseCity, dbo.DimPremise.StateProvince As PremiseState
		FROM dbo.FactCustomerPremise INNER JOIN
			dbo.DimCustomer ON dbo.FactCustomerPremise.CustomerKey = dbo.DimCustomer.CustomerKey INNER JOIN
			dbo.DimPremise ON dbo.FactCustomerPremise.PremiseKey = dbo.DimPremise.PremiseKey
		WHERE (dbo.DimCustomer.ClientId = @ClientID) 
			  AND (dbo.DimCustomer.CustomerId = @CustomerID) 
			  AND (dbo.DimPremise.PremiseId = @PremiseID) 
			  AND (dbo.DimPremise.AccountId = @AccountID) 		
		ORDER BY dbo.FactCustomerPremise.DateCreated DESC


END




GO
