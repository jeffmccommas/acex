SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



Create PROCEDURE [Export].[usp_HE_Report_export_only_CSV_Data]
--declare
@profile_name VARCHAR (50) = 'Sunil_Profile87'
AS
BEGIN

/*

clientid	(No column name)
81	6239
87	29
256	87
210	1218

 */

	SET NOCOUNT ON

	DECLARE
		@clientID INT, 
		@fuel_type VARCHAR (100), 
		@energyobjectcategory_List VARCHAR (1024) , 
		@period VARCHAR (10) , 
		@season_List VARCHAR (100) , 
		@enduse_List VARCHAR (1024) , 
		@ordered_enduse_List VARCHAR (1024), 
		@criteria_id1 VARCHAR (50) , 
		@criteria_detail1 VARCHAR (255)  , 
		@criteria_id2 VARCHAR (50) , 
		@criteria_detail2 VARCHAR (255) , 
		@criteria_id3 VARCHAR (50) , 
		@criteria_detail3 VARCHAR (255) , 
		@criteria_id4 VARCHAR (50) , 
		@criteria_detail4 VARCHAR (255) , 
		@criteria_id5 VARCHAR (50) , 
		@criteria_detail5 VARCHAR (255) , 
		@criteria_id6 VARCHAR (50) , 
		@criteria_detail6 VARCHAR (255), 
		@saveProfile CHAR (1), 
		@verifySave CHAR (1),
		@process_id CHAR (1)

	DECLARE @sqlcmd VARCHAR (MAX), @sqlcmd_cursor VARCHAR (8000), @column_name VARCHAR (128), @column_count INT = 1
		, @tablename VARCHAR (128) = '[export].[home_energy_report_staging_allcolumns]' 

	SELECT
		@clientID = clientID
		, @fuel_type = fuel_type
		, @energyobjectcategory_List = energyobjectcategory_List
		, @period = period
		, @season_List = season_List
		, @enduse_List = enduse_List
		, @ordered_enduse_List = ordered_enduse_List
		, @criteria_id1 = criteria_id1
		, @criteria_detail1 = criteria_detail1
		, @criteria_id2 = criteria_id2
		, @criteria_detail2 = criteria_detail2
		, @criteria_id3 = criteria_id3
		, @criteria_detail3 = criteria_detail3
		, @criteria_id4 = criteria_id4
		, @criteria_detail4 = criteria_detail4
		, @criteria_id5 = criteria_id5
		, @criteria_detail5 = criteria_detail5
		, @criteria_id6 = criteria_id6
		, @criteria_detail6 = criteria_detail6
	  FROM [export].[home_energy_report_criteria_profile]
	WHERE [profile_name] = @profile_name


	SELECT	@tablename = '[export].[home_energy_report_staging]' 

	SELECT @sqlcmd = 'select * from	' + @tablename -- + ' order by PremiseID'
	EXEC (@sqlcmd)

SET NOCOUNT OFF
END



GO
