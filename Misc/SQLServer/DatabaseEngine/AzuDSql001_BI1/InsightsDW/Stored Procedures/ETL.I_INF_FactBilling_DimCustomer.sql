SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 6/13/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_INF_FactBilling_DimCustomer]
				 @ETL_LogId INT,
				 @ClientID INT

AS

BEGIN

		SET NOCOUNT ON;

		INSERT INTO [InsightsDW].[dbo].[DimCustomer] (
							ClientKey, 
							ClientId, 
							PostalCodeKey, 
							CustomerAuthenticationTypeKey, 
							SourceKey, 
							CityKey, 
							CustomerId, 
							FirstName, 
							LastName, 
							Street1, 
							Street2, 
							City, 
							StateProvince, 
							Country, 
							PostalCode, 
							PhoneNumber, 
							MobilePhoneNumber, 
							EmailAddress, 
							AlternateEmailAddress, 
							CreateDate, 
							SourceId, 
							AuthenticationTypeId,
							ETL_LogId,
							IsInferred,
							TrackingId,
							TrackingDate)
	SELECT  dcl.ClientKey,
			kfb.ClientId,
			-1,
			1,
			ds.SourceKey,
			-1,
			kfb.CustomerId,
			NULL, 
			NULL,
			NULL,  
			NULL, 
			NULL, 
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			GETUTCDATE(),
			kfb.SourceId,
			1,
			@ETL_LogId,
			1,
			GETUTCDATE(),
			GETUTCDATE()
	FROM ETL.KEY_FactBilling kfb WITH (NOLOCK)
	INNER JOIN dbo.DimClient dcl WITH (NOLOCK) ON dcl.ClientId = kfb.ClientId	
	INNER JOIN dbo.DimSource ds WITH (NOLOCK) ON ds.SourceId = kfb.SourceId
	LEFT JOIN dbo.DimCustomer dc WITH (NOLOCK) ON dc.CustomerId = kfb.CustomerId
									AND dc.ClientId = kfb.ClientId
	WHERE kfb.ClientId = @ClientID AND dc.CustomerId IS NULL
	GROUP BY dcl.ClientKey,
		kfb.ClientId,
		ds.SourceKey,
		kfb.SourceId,
		kfb.CustomerId

END

														 



GO
