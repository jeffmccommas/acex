SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
-- =======================================
-- Written by:	Wayne
-- Date:		07.20.15
-- Description: Writes error occurring in the 
-- 				Measure Savings processing to a log table
-- =======================================
*/
CREATE PROCEDURE [dbo].[LogMeasureSavingsError]
(
    @ClientID       INT,
    @PremiseID      VARCHAR(50),
    @CustomerID     VARCHAR(50),
    @AccountID      VARCHAR(50),
    @Error			VARCHAR(MAX)  
)
AS
BEGIN
 
    SET NOCOUNT ON;
    
    INSERT INTO  InsightsMetadata.[audit].[MeasureSavingsErrorLog]
    
    (  	  
      [ClientID]
      ,[PremiseID]
      ,[CustomerID]
      ,[AccountID]
      ,[Error]
      ,[TrackingDate]
)
     VALUES
       (
	   @ClientID
	  ,@PremiseID
	  ,@CustomerID
	  ,@AccountID
	  ,@Error
	  ,GETDATE()
	  )

END

GO
