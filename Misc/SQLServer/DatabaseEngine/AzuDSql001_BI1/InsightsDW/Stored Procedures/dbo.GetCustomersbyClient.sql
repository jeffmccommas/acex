SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCustomersbyClient] ( @clientId INT )
	-- Add the parameters for the stored procedure here
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        SELECT  customerid ,
                accountid ,
                premiseid
        FROM    [insightsdw].[dbo].[dimcustomer] c
                INNER JOIN [insightsdw].[dbo].[factcustomerpremise] cp ON c.customerkey = cp.CustomerKey
                INNER JOIN [insightsdw].[dbo].[dimpremise] p ON p.premisekey = cp.premisekey
        WHERE   c.clientid = @clientId
    -- Insert statements for procedure here
    END
GO
