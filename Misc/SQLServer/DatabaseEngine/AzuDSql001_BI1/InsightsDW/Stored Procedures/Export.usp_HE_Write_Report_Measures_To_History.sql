SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Wayne
-- Create date: 11.1.2014
-- Description:	Writes history of measures in last HE Report
--				for each premise that was on the report
--
-- Proc writes the last run made in HE Reports
-- to history for Monthly and/or Annual
-- by passing in a different profile	
-- example passing in 'SCG_Profile101,SCG_Profile101_M'
-- will write last run of he report measures to both
-- the monthly and annual profileshistory for the prmises on last report
--
-- be careful	 if you ran 2 different reports 
-- maybe a "print" and then an "email"
-- and then run proc, it only will update premises on the 
-- premises and measure for the last report (the email one)
-- may not really be of too much use except in special situations
-- =============================================
CREATE procedure [Export].[usp_HE_Write_Report_Measures_To_History]
--declare
@profile_name varchar (50)  = 'SCG_Profile101,SCG_Profile101_M'
as
begin

	SET nocount on

	DECLARE @table1 TABLE ( Aprofile VARCHAR(500) )  
	declare	@history_date datetime = getdate ()

	INSERT @table1
	SELECT * FROM dbo.splitstring(@profile_name)
	DECLARE @profile_name_split VARCHAR(500)
/*
--	WRITE TO HISTORY
*/

while exists (SELECT Aprofile from @table1)
	BEGIN
		-- get a row from table of profiles
		select top 1 @profile_name_split = Aprofile 
		from @table1 
		order by Aprofile Asc
		
		--process that profile to history

		--insert 1 row in history for each measure

		insert [export].[HE_Report_CSV_Measure_history]
		(profile_name, PremiseID, MeasureID, history_date)
		SELECT	@profile_name_split, premiseID, MID1, @history_date
		FROM	[export].[home_energy_report_staging_allcolumns] a1
		WHERE	coalesce (MID1, '') <> ''

		insert [export].[HE_Report_CSV_Measure_history]
		(profile_name, PremiseID, MeasureID, history_date)
		SELECT	@profile_name_split, premiseID, MID2, @history_date
		FROM	[export].[home_energy_report_staging_allcolumns] a1
		WHERE	coalesce (MID2, '') <> ''

		insert [export].[HE_Report_CSV_Measure_history]
		(profile_name, PremiseID, MeasureID, history_date)
		SELECT	@profile_name_split, premiseID, MID3, @history_date
		FROM	[export].[home_energy_report_staging_allcolumns] a1
		WHERE	coalesce (MID3, '') <> ''

		--remove the profile you just processed from table of profiles
		delete from @table1 where Aprofile = @profile_name_split

		--loop to top
	END
end
GO
