SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
-- =======================================
-- Written by:	Wayne
-- Date:		07.20.15
-- Description: Create the temp table for Measure Savings
-- 				caclulations
-- =======================================
*/
CREATE PROCEDURE [dbo].[CreateMeasureSavingsTempTable]
(
    @ClientID       INT = 101
)
as
BEGIN
DECLARE @sqlstring VARCHAR(2000)=
'
IF OBJECT_ID(''dbo.MeasureSavingsTemp' + CAST(@ClientID AS varchar(5)) + ''') IS NOT NULL
    DROP TABLE dbo.MeasureSavingsTemp' + CAST(@ClientID AS varchar(5)) + ' 
CREATE TABLE  dbo.MeasureSavingsTemp' + CAST(@ClientID AS varchar(5)) + ' (
	[ClientId] [INT] NOT NULL,
	[CustomerId] [VARCHAR](50) NOT NULL,
	[AccountId] [VARCHAR](50) NOT NULL,
	[PremiseId] [VARCHAR](50) NOT NULL,
	[ActionKey] [VARCHAR](50) NOT NULL,
	[ActionTypeKey] [VARCHAR](50) NOT NULL,
	[AnnualCost] [DECIMAL](18, 6) NOT NULL,
	[AnnualCostVariancePercent] [DECIMAL](18, 6) NOT NULL,
	[AnnualSavingsEstimate] [DECIMAL](18, 6) NOT NULL,
	[AnnualSavingsEstimateCurrencyKey] [VARCHAR](16) NOT NULL,
	[Roi] [DECIMAL](18, 6) NULL,
	[Payback] [DECIMAL](18, 6) NULL,
	[UpfrontCost] [DECIMAL](18, 6) NULL,
	[CommodityKey] [VARCHAR](50) NOT NULL,
	[ElecSavEst] [DECIMAL](18, 6) NULL,
	[ElecSavEstCurrencyKey] [VARCHAR](16) NULL,
	[ElecUsgSavEst] [DECIMAL](18, 6) NULL,
	[ElecUsgSavEstUomKey] [VARCHAR](16) NULL,
	[GasSavEst] [DECIMAL](18, 6) NULL,
	[GasSavEstCurrencyKey] [VARCHAR](16) NULL,
	[GasUsgSavEst] [DECIMAL](18, 6) NULL,
	[GasUsgSavEstUomKey] [VARCHAR](16) NULL,
	[WaterSavEst] [DECIMAL](18, 6) NULL,
	[WaterSavEstCurrencyKey] [VARCHAR](16) NULL,
	[WaterUsgSavEst] [DECIMAL](18, 6) NULL,
	[WaterUsgSavEstUomKey] [VARCHAR](16) NULL,
	[Priority] [INT] NOT NULL,
	[NewDate] [DATETIME] NOT NULL
) ON [PRIMARY]'
exec (@sqlstring)
END
GO
