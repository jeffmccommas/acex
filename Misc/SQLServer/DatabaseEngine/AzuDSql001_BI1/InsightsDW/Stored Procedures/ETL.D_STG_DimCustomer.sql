SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/16/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[D_STG_DimCustomer]
				 @ClientID INT
AS

BEGIN

	SET NOCOUNT ON;

	DELETE FROM ETL.INS_DimCustomer WHERE ClientID = @ClientID
	DELETE FROM ETL.T1U_DimCustomer WHERE ClientID = @ClientID

END





GO
