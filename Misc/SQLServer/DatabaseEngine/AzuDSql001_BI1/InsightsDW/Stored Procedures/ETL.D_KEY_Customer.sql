SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 9/08/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[D_KEY_Customer]
				 @ClientID INT
AS

BEGIN

	SET NOCOUNT ON;

	DELETE FROM ETL.KEY_DimCustomer WHERE ClientID = @ClientID

END





GO
