SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/21/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[S_DimPremise]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;


		SELECT  PremiseKey, dp.AccountId, dp.PremiseId, Street1, Street2, City, StateProvince, Country, PostalCode, GasService, ElectricService, WaterService, dp.ClientId, SourceId
		FROM [DimPremise] dp
		INNER JOIN ETL.KEY_DimPremise kdp ON kdp.ClientId = dp.ClientId
											AND kdp.PremiseId = dp.PremiseId
											AND kdp.AccountId = dp.AccountId
		WHERE dp.ClientId = @ClientID
		ORDER BY kdp.ClientId, kdp.PremiseId, kdp.AccountId


END





GO
