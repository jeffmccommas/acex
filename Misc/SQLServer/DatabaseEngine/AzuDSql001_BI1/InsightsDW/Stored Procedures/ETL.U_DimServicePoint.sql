SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 6/13/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[U_DimServicePoint]
				 @ETLLogID INT,
				 @ClientID INT,
				 @ServicePointID INT

AS

BEGIN

SET NOCOUNT ON;

	UPDATE [dbo].[DimServicePoint] 
	SET IsInferred = 0
	WHERE ClientId = @ClientID 
			AND ServicePointId = @ServicePointID
	

END

														 



GO
