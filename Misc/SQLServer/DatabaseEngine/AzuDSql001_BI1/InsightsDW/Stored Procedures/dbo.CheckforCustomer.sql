SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Wayne
-- Create date:	1.9.2015
-- Description:	Checks for a customer and returns a 1 (true) or 0 (false)
-- =============================================
CREATE PROC [dbo].[CheckforCustomer]
    @ClientId INT = NULL ,
    @CustomerId VARCHAR(255) = NULL
AS
    BEGIN

        SET NOCOUNT OFF

        BEGIN TRY
            IF @ClientId IS NULL
                BEGIN
                    RAISERROR('Client Id must be provided', 11, 1)
                END
            IF @ClientId < 1
                BEGIN
                    RAISERROR('Client Id must be  greater than 0', 11, 2)
                END
            IF NOT EXISTS ( SELECT  *
                            FROM    InsightsDW.[dbo].DimClient
                            WHERE   ClientId = @ClientId )
                BEGIN
                    RAISERROR('Invalid Client Id', 11, 4)
                END
            IF @CustomerId IS NULL
                BEGIN
                    RAISERROR('Customer Id must be provided', 11, 1)
                END
            IF EXISTS ( SELECT  CustomerKey
                        FROM    InsightsDW.[dbo].DimCustomer
                        WHERE   ClientId = @ClientId
                                AND CustomerId = @CustomerId )
                BEGIN
                    SELECT  1;
                END
            ELSE
                BEGIN
                    SELECT  0;
                END
            SET NOCOUNT ON;
        END TRY
        BEGIN CATCH
            THROW; --http://msdn.microsoft.com/en-us/library/ee677615.aspx
        END CATCH
    END

GO
