SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ubaid Tariq
-- Create date: 12/16/2014
-- Description:	get actions for etl
-- =============================================
CREATE PROCEDURE [ETL].[S_FactActions] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

		
        SELECT  * ,
                CONVERT(VARCHAR(MAX), ISNULL(STUFF(( SELECT TOP 100 PERCENT
                                                            ';'
                                                            + t2.ActionDataKey
                                                            + ':'
                                                            + t2.ActionDataValue
                                                     FROM   insightsdw.dbo.factActionData
                                                            AS t2
                                                     WHERE  t2.ActionDetailKey = p.ActionDetailKey
                                                     ORDER BY t2.ActionDataKey ,
                                                            t2.ActionDataValue
                                                   FOR
                                                     XML PATH('')
                                                   ), 1, 1, ''), '')) AS AdditionalInfo
        FROM    ( SELECT    fa.ActionDetailKey ,
                            fa.ClientID ,
                            fa.AccountID ,
                            fa.PremiseID ,
                            fa.StatusKey AS StatusId ,
                            fa.StatusDate ,
                            fa.ActionKey ,
							fa.SubActionKey ,
                            ROW_NUMBER() OVER ( PARTITION BY PremiseKey,
                                                fa.ActionKey, fa.SubActionKey ORDER BY TrackingDate DESC ) AS SortId
                  FROM      InsightsDW.dbo.factAction fa WITH ( NOLOCK )
                            INNER JOIN ETL.KEY_FactAction kfa ON kfa.ClientId = fa.ClientID
                                                              AND kfa.PremiseId = fa.PremiseID
                                                              AND kfa.AccountId = fa.AccountID
                                                              AND kfa.ActionKey = fa.ActionKey
															  AND kfa.SubActionKey = fa.SubActionKey
                  WHERE     fa.ClientID = @ClientID
                ) p
        WHERE   SortId = 1
        ORDER BY p.ClientID ,
                p.PremiseID ,
                p.AccountID ,
                p.ActionKey ,
				p.SubActionKey ,
                p.StatusId ,
                p.StatusDate
		 


    END






GO
