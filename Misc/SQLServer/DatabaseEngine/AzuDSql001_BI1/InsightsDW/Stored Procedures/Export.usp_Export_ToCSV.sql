SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [Export].[usp_Export_ToCSV]
@profile_name VARCHAR (50) = 'Sunil_Profile87',
@exportDir VARCHAR (128) = 'C:\mssql\export_CSV'
AS
BEGIN

	SET NOCOUNT ON

	DECLARE
		@clientID INT, 
		@fuel_type VARCHAR (100), 
		@energyobjectcategory_List VARCHAR (1024) , 
		@period VARCHAR (10) , 
		@season_List VARCHAR (100) , 
		@enduse_List VARCHAR (1024) , 
		@ordered_enduse_List VARCHAR (1024), 
		@criteria_id1 VARCHAR (50) , 
		@criteria_detail1 VARCHAR (255)  , 
		@criteria_id2 VARCHAR (50) , 
		@criteria_detail2 VARCHAR (255) , 
		@criteria_id3 VARCHAR (50) , 
		@criteria_detail3 VARCHAR (255) , 
		@criteria_id4 VARCHAR (50) , 
		@criteria_detail4 VARCHAR (255) , 
		@criteria_id5 VARCHAR (50) , 
		@criteria_detail5 VARCHAR (255) , 
		@criteria_id6 VARCHAR (50) , 
		@criteria_detail6 VARCHAR (255), 
		@saveProfile CHAR (1), 
		@verifySave CHAR (1),
		@process_id CHAR (1)

	SELECT
		@clientID = clientID
		, @fuel_type = fuel_type
		, @energyobjectcategory_List = energyobjectcategory_List
		, @period = period
		, @season_List = season_List
		, @enduse_List = enduse_List
		, @ordered_enduse_List = ordered_enduse_List
		, @criteria_id1 = criteria_id1
		, @criteria_detail1 = criteria_detail1
		, @criteria_id2 = criteria_id2
		, @criteria_detail2 = criteria_detail2
		, @criteria_id3 = criteria_id3
		, @criteria_detail3 = criteria_detail3
		, @criteria_id4 = criteria_id4
		, @criteria_detail4 = criteria_detail4
		, @criteria_id5 = criteria_id5
		, @criteria_detail5 = criteria_detail5
		, @criteria_id6 = criteria_id6
		, @criteria_detail6 = criteria_detail6
	  FROM [export].[home_energy_report_criteria_profile]
	WHERE [profile_name] = @profile_name

	DECLARE @bcpcmd VARCHAR (8000)

	SELECT	@bcpcmd = 'bcp "exec export.usp_HE_Report_Prepare_CSV_Data @profile_name = ''' + @profile_name + '''" queryout ' 
				+ @exportDir + '\export_'  + CONVERT (VARCHAR, @clientID) + '_' 
				+ @period + '_' + @fuel_type + '.csv '
				+ ' -UCopytemp -PCopytemp$1 -SAXDEVSQL2\BI1 -dInsightsDW -c -t , > NUL:' 

	SELECT	@bcpcmd

	EXEC xp_cmdshell @bcpcmd

	SET NOCOUNT OFF

END

GO
