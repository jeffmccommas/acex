SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ubaid Tariq
-- Create date: 12/16/2014
-- Description:	 
-- =============================================
CREATE PROCEDURE [ETL].[I_INF_FactAction_DimCustomer]
    @ETL_LogId INT ,
    @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        INSERT  INTO [InsightsDW].[dbo].[DimCustomer]
                ( ClientKey ,
                  ClientId ,
                  PostalCodeKey ,
                  CustomerAuthenticationTypeKey ,
                  SourceKey ,
                  CityKey ,
                  CustomerId ,
                  FirstName ,
                  LastName ,
                  Street1 ,
                  Street2 ,
                  City ,
                  StateProvince ,
                  Country ,
                  PostalCode ,
                  PhoneNumber ,
                  MobilePhoneNumber ,
                  EmailAddress ,
                  AlternateEmailAddress ,
                  CreateDate ,
                  SourceId ,
                  AuthenticationTypeId ,
                  ETL_LogId ,
                  IsInferred ,
                  TrackingId ,
                  TrackingDate
                )
                SELECT  ClientKey ,
                        ClientId ,
                        -1 ,
                        1 ,
                        SourceKey ,
                        -1 ,
                        CustomerId ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        GETUTCDATE() ,
                        SourceId ,
                        1 ,
                        @ETL_LogId ,
                        1 ,
                        GETUTCDATE() ,
                        GETUTCDATE()
                FROM    ( SELECT    kfa.ClientId ,
                                    dcl.ClientKey ,
                                    kfa.CustomerId ,
                                    ds.SourceKey ,
                                    kfa.SourceID ,
                                    ROW_NUMBER() OVER ( PARTITION BY kfa.ClientId,
                                                        kfa.CustomerId ORDER BY kfa.SourceID ) AS SortId
                          FROM      ETL.KEY_FactAction kfa WITH ( NOLOCK )
                                    INNER JOIN dbo.DimClient dcl WITH ( NOLOCK ) ON dcl.ClientId = kfa.ClientId
                                    INNER JOIN dbo.DimSource ds WITH ( NOLOCK ) ON ds.SourceId = kfa.SourceId
                                    LEFT JOIN dbo.DimCustomer dc WITH ( NOLOCK ) ON dc.CustomerId = kfa.CustomerId
                                                              AND dc.ClientId = kfa.ClientId
                          WHERE     kfa.ClientId = @ClientID
                                    AND dc.CustomerId IS NULL
                        ) c
                WHERE   SortId = 1
    END

														 




GO
