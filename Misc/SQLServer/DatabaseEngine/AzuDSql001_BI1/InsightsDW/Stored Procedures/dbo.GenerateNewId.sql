SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*	
-- Written by Wayne
-- Date 3.20.15
-- Description: Description:this creates a somewhat random premise id
--			 that is higher than the highest in the target
--			 but  by only a bit as to not run out of numbers
--			 but enough to not look sequential
--
-- Usage:		 DECLARE @newid  VARCHAR(100) 
--			 exec dbo.GenerateNewId 'DimPremise', 'premiseid',101,  @newid   OUTPUT
--			 SELECT @newid
*/
CREATE PROCEDURE [dbo].[GenerateNewId] (@thetable varchar(30), @theid varchar(100), @client_id INT, @newid  VARCHAR(100)  OUTPUT)
    AS
    BEGIN
    declare @maxcharid		  VARCHAR(100)
    	, @low_target_id		  DECIMAL(38,0)=0
	, @highint_target_adder	  DECIMAL(38,0)=0
	, @highchar_target_adder	  VARCHAR(100)
	, @high_target_id		  DECIMAL(38,0)=    0 
     , @sqlstr				  NVARCHAR(1000)
	, @thelength			  int=0
	, @new_int_id			  DECIMAL(38,0)=0
     SET @sqlstr=N'set @myvar=
	(SELECT top 1 '+ @theid +' FROM InsightsDW.dbo.' + @thetable +
		   ' WHERE clientid=' + CAST(@client_id AS VARCHAR(5)) + ' and  isnumeric(' + @theid + ') =1 and  cast(' + @theid +' as DECIMAL(38,0)) = ' +
	   '(SELECT MAX(cast(' + @theid +' as DECIMAL(38,0))) FROM InsightsDW.dbo.' + @thetable +
		   ' WHERE clientid=' + CAST(@client_id AS VARCHAR(5)) +
		   ' and  isnumeric(' + @theid + ') =1))' 
    EXECUTE sys.sp_executesql @sqlstr, N'@myvar varchar(100) OUTPUT', @maxcharid OUTPUT    
    SET @low_target_id= CAST(@maxcharid AS DECIMAL(38,0))+1;
    SET @thelength = LEN(cast(@low_target_id AS varchar(100)))
    IF @thelength > 5
    BEGIN 
    SET @highchar_target_adder = '1' + REPLICATE('0', @thelength-4)
    END
    ELSE IF @thelength > 3
    BEGIN 
    SET @highchar_target_adder = '1' + REPLICATE('0', @thelength-2)
    END
    ELSE
    BEGIN 
    SET @highchar_target_adder = '1' + REPLICATE('0', @thelength-1)
    END
    SET @highint_target_adder=CAST(@highchar_target_adder AS  DECIMAL(38,0))
    SET @high_target_id=@low_target_id+@highint_target_adder;
    SELECT @new_int_id = CAST(ROUND(((@high_target_id - @low_target_id -1) * + RAND()  + @low_target_id), 0) AS DECIMAL(38,0))
    SET @newid=CAST(@new_int_id AS varchar(100))
    IF LEN(@maxcharid) > LEN(@newid)     
    BEGIN
	   DECLARE @thediff int =LEN(@maxcharid) - LEN(@newid)
	   DECLARE @thepad VARCHAR(50)=REPLICATE('0', @thediff)
	   SET @newid= @thepad+@newid
    END
    RETURN
    END
GO
