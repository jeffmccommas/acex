SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Ubaid Tariq
-- Create date: 7/20/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_FactAction] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;


        INSERT  INTO dbo.factAction
                ( PremiseKey ,
                  DateKey ,
                  ActionId ,
                  ActionKey ,
                  SubActionKey ,
                  StatusKey ,
                  StatusDate ,
                  ClientID ,
                  PremiseID ,
                  AccountID ,
                  SourceKey ,
                  CreateDate ,
                  ETLLogID ,
                  TrackingID ,
                  TrackingDate
		        )
                SELECT  dp.PremiseKey ,
                        CONVERT(VARCHAR(35), ifa.CreateDate, 112) ,
                        [ActionId] ,
                        [ActionKey] ,
                        [SubActionKey] ,
                        [StatusId] ,
                        [StatusDate] ,
                        ifa.ClientID ,
                        ifa.PremiseId ,
                        ifa.AccountId ,
                        ifa.SourceId ,
                        ifa.CreateDate ,
                        ifa.ETL_LogId ,
                        ifa.TrackingId ,
                        ifa.TrackingDate
                FROM    [ETL].[INS_FactAction] ifa
                        INNER JOIN dbo.DimPremise dp ON dp.ClientId = ifa.ClientID
                                                        AND dp.PremiseId = ifa.PremiseId
                                                        AND dp.AccountId = ifa.AccountId
                WHERE   ifa.ClientID = @ClientID;




    END;


														


GO
