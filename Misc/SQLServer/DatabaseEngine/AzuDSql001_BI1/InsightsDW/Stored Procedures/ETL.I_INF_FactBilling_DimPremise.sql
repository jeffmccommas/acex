SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 6/13/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_INF_FactBilling_DimPremise]
				 @ETL_LogId INT,
				 @ClientID INT

AS

BEGIN

		SET NOCOUNT ON;

INSERT INTO [InsightsDW].[dbo].[DimPremise] (PostalCodeKey, 
													CityKey, 
													SourceKey, 
													PremiseId, 
													AccountId,
													Street1, 
													Street2, 
													City, 
													StateProvince, 
													Country, 
													PostalCode, 
													GasService, 
													ElectricService, 
													WaterService, 
													CreateDate,
													ClientId, 
													GeoLocation,
													SourceId,
													ETL_LogId,
													IsInferred,
													TrackingId,
													TrackingDate)
		SELECT  DISTINCT -1,
				-1,
				ds.SourceKey,
				kfb.PremiseId,
				kfb.AccountId,
				NULL,  
				NULL, 
				NULL, 
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				GETUTCDATE(),
				kfb.ClientId,
				NULL,
				kfb.SourceId,
				@ETL_LogId,
				1,
				GETUTCDATE(),
				GETUTCDATE()
	FROM ETL.KEY_FactBilling kfb WITH (NOLOCK)
	INNER JOIN dbo.DimClient dcl WITH (NOLOCK) ON dcl.ClientId = kfb.ClientId	
	INNER JOIN dbo.DimSource ds WITH (NOLOCK) ON ds.SourceId = kfb.SourceId
	LEFT JOIN dbo.DimPremise dp WITH (NOLOCK) ON dp.PremiseId = kfb.PremiseId
									AND dp.ClientId = kfb.ClientId
									AND dp.AccountId = kfb.AccountId
	WHERE kfb.ClientID = @ClientID AND dp.PremiseId IS NULL
	

END

														 



GO
