SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Wayne
-- Create date: 10.5.2014
-- Description:	Updates the ami columns on the report
-- =============================================
-- Changed by:	Wayne
-- Change date: 10.9.2014
-- Description:	v 4.0
--	
-- Removed the ami from the final "annual" csv by changing the client table annual column - no code change			
-- =============================================
-- Changed by:	Wayne
-- Change date: 10.13.2014
-- Description:	v 1.7.0
--	
-- Eliminate (make positive) negative values in any PCT column		
-- also
-- Added a config param for BillYear and BillMonth
-- will need in future change		
-- =============================================
-- Changed by:	Wayne
-- Change date: 10.13.2014
-- Description:	v 1.8.0
--	
-- fix bug in min and max usage and dates		
-- =============================================
-- Changed by:	Wayne
-- Change date: 10.16.2014
-- Description:	v 1.12.0
--	
-- added a rolling year for ami aggregates
-- this proc needed to use rolling year aggregatetypekeys		
-- =============================================
-- Changed by:	Wayne
-- Change date: 10.16.2014
-- Description:	v 1.23.0
--	
-- removed rolling year and week from ami aggregates
-- using aggregates for 1 month only in aggregates table		
-- =============================================
-- Changed by:	Wayne
-- Change date: 12.31.2014
-- Description:	v 1.33.0 (same version)
--	
--  Removed the WITH (NOLOCK)'s because
--  they were causing race conditions
-- =============================================
-- Changed by:	Wayne
-- Change date: 1.8.2015
-- Description:	v 1.34.0 (same version)
--	
--  if amy of the ami aggregates are 0 then
-- fall the report program number 
-- back to 2 unless it has already been set to 1
-- because month 13 is 0
-- =============================================
-- Changed by:	 Wayne
-- Change date: 1.8.2015
-- Description: v 1.35.0 
--	
-- tweak fallback rules for when no month 13
-- only report 3 requires ami data
-- all others must fallback to 1 if they dont have month 13
-- =============================================
CREATE procedure [Export].[usp_HE_Report_Get_AMI]
--declare
@profile_name		VARCHAR (50)=	'SCG_Profile101',
@channel			VARCHAR(5)	=	'email',				--options are 'email'  or 'print'
@report_number		VARCHAR(1)	=	'1',
@bill_year			VARCHAR(4)	=	'2014',
@bill_month			VARCHAR(2)	=	'08'

as
begin

	set nocount on
	declare @enduse varchar (50)

	declare
		@clientID int, 
		@fuel_type varchar (100), 
		@energyobjectcategory_List varchar (1024) , 
		@period varchar (10) , 
		@season_List varchar (100) , 
		@enduse_List varchar (1024) , 
		@ordered_enduse_List varchar (1024), 
		@criteria_id1 varchar (50) , 
		@criteria_detail1 varchar (255)  , 
		@criteria_id2 varchar (50) , 
		@criteria_detail2 varchar (255) , 
		@criteria_id3 varchar (50) , 
		@criteria_detail3 varchar (255) , 
		@criteria_id4 varchar (50) , 
		@criteria_detail4 varchar (255) , 
		@criteria_id5 varchar (50) , 
		@criteria_detail5 varchar (255) , 
		@criteria_id6 varchar (50) , 
		@criteria_detail6 varchar (255), 
		@saveProfile char (1), 
		@verifySave char (1),
		@process_id char (1)


	SELECT
		@clientID = clientID
		, @fuel_type = fuel_type
		, @energyobjectcategory_List = energyobjectcategory_List
		, @period = period
		, @season_List = season_List
		, @enduse_List = enduse_List
		, @ordered_enduse_List = ordered_enduse_List
		, @criteria_id1 = criteria_id1
		, @criteria_detail1 = criteria_detail1
		, @criteria_id2 = criteria_id2
		, @criteria_detail2 = criteria_detail2
		, @criteria_id3 = criteria_id3
		, @criteria_detail3 = criteria_detail3
		, @criteria_id4 = criteria_id4
		, @criteria_detail4 = criteria_detail4
		, @criteria_id5 = criteria_id5
		, @criteria_detail5 = criteria_detail5
		, @criteria_id6 = criteria_id6
		, @criteria_detail6 = criteria_detail6
	  FROM [export].[home_energy_report_criteria_profile]
	where [profile_name] = @profile_name
	
	declare @billmonth int =cast(@bill_month as int),
			@billyear int =cast(@bill_year as int)
	declare @sqlcmd varchar (8000), @orderbyClause varchar (2000) = '', @validClause varchar (8000) = ''

declare @startdate int
declare @enddate int
declare @dayofweek varchar(20)
/*
						15,   -- OVER the rolling year
						12,   -- FOR THE MONTH IN THE calendar YEAR
						 7,   -- FOR THE DAY IN THE rolling YEAR
						 4    -- FOR THE DAY IN THE MONTH IN THE rolling YEAR
*/
if @period='monthly'
begin  --'Monthly processing'
 
update [Export].[home_energy_report_staging_allcolumns]

set  [AverageSundayUse]=t.[AvgerageInPeriod4Sunday]
  ,[AverageMondayUse]=t.[AvgerageInPeriod4Monday]
  ,[AverageTuesdayUse]=t.[AvgerageInPeriod4tuesday]
  ,[AverageWednesdayUse]=t.[AvgerageInPeriod4Wednesday]
  ,[AverageThursdayUse]=t.[AvgerageInPeriod4Thursday]
  ,[AverageFridayUse]=t.[AvgerageInPeriod4Friday]
  ,[AverageSaturdayUse]=t.[AvgerageInPeriod4Saturday]
  ,[DailyAverageforPeriod]=[AvgerageInPeriod]
  ,[HighestDayinPeriod]=[DateOfMaxInPeriod]
  ,[LowestDayinPeriod]=[DateOfMinInPeriod]
  ,[HighestDayUse]=[DailyMaxInPeriod]
  ,[LowestDayUse]=[DailyMinInPeriod]
  ,ProgramReportNumber= 
    CASE WHEN @report_number=3  -- 3 is the ami report
    then
		  CASE WHEN ((MyHomegUsage13 IS NULL 	OR CAST(MyHomegUsage13 AS decimal(18,2)) <=0.00))  -- column 13 has no bill for this month so cant do 2 -- year-to-year
			 AND  --they dont have ami in one of the aggregate columns we cant do a 3 
			 (   
			   [AvgerageInPeriod] is null or [AvgerageInPeriod] =0.0000  or
			   [DailyMaxInPeriod] is null or [DailyMaxInPeriod] =0.0000  or
			   [DailyMinInPeriod] is null or [DailyMinInPeriod] =0.0000  or
			   [AvgerageInPeriod4Sunday] is null or [AvgerageInPeriod4Sunday]=0.0000  or
			   [AvgerageInPeriod4Monday] is null or [AvgerageInPeriod4Monday]=0.0000  or
			   [AvgerageInPeriod4tuesday] is null or [AvgerageInPeriod4tuesday]= 0.0000  or
			   [AvgerageInPeriod4Wednesday] is null or [AvgerageInPeriod4Wednesday]=0.0000  or
			   [AvgerageInPeriod4Thursday] is null or [AvgerageInPeriod4Thursday]=0.0000  or
			   [AvgerageInPeriod4Friday] is null or [AvgerageInPeriod4Friday]=0.0000  or
			   [AvgerageInPeriod4Saturday] is null or [AvgerageInPeriod4Saturday]=0.0000 
		    )
		    THEN 1  --which had no year-to-year, it was comaprison to efficent and average homes


		   WHEN	 --they dont have ami in one of the aggregate columns we cant do a 3 (they must have had a month 13)
		   (   
			   [AvgerageInPeriod] is null or [AvgerageInPeriod] =0.0000  or
			   [DailyMaxInPeriod] is null or [DailyMaxInPeriod] =0.0000  or
			   [DailyMinInPeriod] is null or [DailyMinInPeriod] =0.0000  or
			   [AvgerageInPeriod4Sunday] is null or [AvgerageInPeriod4Sunday]=0.0000  or
			   [AvgerageInPeriod4Monday] is null or [AvgerageInPeriod4Monday]=0.0000  or
			   [AvgerageInPeriod4tuesday] is null or [AvgerageInPeriod4tuesday]= 0.0000  or
			   [AvgerageInPeriod4Wednesday] is null or [AvgerageInPeriod4Wednesday]=0.0000  or
			   [AvgerageInPeriod4Thursday] is null or [AvgerageInPeriod4Thursday]=0.0000  or
			   [AvgerageInPeriod4Friday] is null or [AvgerageInPeriod4Friday]=0.0000  or
			   [AvgerageInPeriod4Saturday] is null or [AvgerageInPeriod4Saturday]=0.0000 
		    ) 
		    THEN 2 --so do a 2 which was year-to-year only needs month columns 1 and 13
		    ELSE 
			 @report_number -- we can do a 3 or more they must have ami data
		    END
   WHEN  ((MyHomegUsage13 IS NULL 	OR CAST(MyHomegUsage13 AS decimal(18,2)) <=0.00))  THEN 1  --which had no year-to-year, it was comaprison to efficent and average homes        

ELSE 
	 @report_number -- we can do a 1,2,4,5,6 or above or more they  do not need ami for these,  just a MyHomegUsage13
end	
from  [Export].[home_energy_report_staging_allcolumns] a  
LEFT outer join (
select * from (
SELECT  PremiseId ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 1 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN	[AmiAggregateKey]
            END) AS [AmiAggregateKey4Sunday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 1 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AmiKey]
            END) AS [AmiKey4Sunday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 1 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AmiAggregateKeyType]
            END) AS [AmiAggregateKeyType4Sunday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 1 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DayOfWeek]
            END) AS [DayOfWeek4Sunday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 1 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [Month]
            END) AS [Month4Sunday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 1 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [Year]
            END) AS [Year4Sunday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 1 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMinInPeriod]
            END) AS [DailyMinInPeriod4Sunday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 1 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMaxInPeriod]
            END) AS [DailyMaxInPeriod4Sunday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 1 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [TotalUsageInPeriod]
            END) AS [TotalUsageInPeriod4Sunday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 1 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AvgerageInPeriod]
            END) AS [AvgerageInPeriod4Sunday] ,

		
			-------------------------------------------------
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 2 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN	[AmiAggregateKey]
            END) AS [AmiAggregateKey4Monday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 2 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AmiKey]
            END) AS [AmiKey4Monday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 2 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AmiAggregateKeyType]
            END) AS [AmiAggregateKeyType4Monday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 2 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DayOfWeek]
            END) AS [DayOfWeek4Monday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 2 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [Month]
            END) AS [Month4Monday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 2 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [Year]
            END) AS [Year4Monday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 2 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMinInPeriod]
            END) AS [DailyMinInPeriod4Monday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 2 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMaxInPeriod]
            END) AS [DailyMaxInPeriod4Monday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 2 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [TotalUsageInPeriod]
            END) AS [TotalUsageInPeriod4Monday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 2 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AvgerageInPeriod]
            END) AS [AvgerageInPeriod4Monday] ,
  
			-------------------------------------------------
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 3 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN	[AmiAggregateKey]
            END) AS [AmiAggregateKey4Tuesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 3 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AmiKey]
            END) AS [AmiKey4Tuesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 3 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AmiAggregateKeyType]
            END) AS [AmiAggregateKeyType4Tuesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 3 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DayOfWeek]
            END) AS [DayOfWeek4Tuesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 3 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [Month]
            END) AS [Month4Tuesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 3 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [Year]
            END) AS [Year4Tuesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 3 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMinInPeriod]
            END) AS [DailyMinInPeriod4Tuesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 3 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMaxInPeriod]
            END) AS [DailyMaxInPeriod4Tuesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 3 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [TotalUsageInPeriod]
            END) AS [TotalUsageInPeriod4Tuesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 3 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AvgerageInPeriod]
            END) AS [AvgerageInPeriod4Tuesday] ,
        
			-------------------------------------------------
		MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 4 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN	[AmiAggregateKey]
            END) AS [AmiAggregateKey4Wednesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 4 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AmiKey]
            END) AS [AmiKey4Wednesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 4 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AmiAggregateKeyType]
            END) AS [AmiAggregateKeyType4Wednesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 4 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DayOfWeek]
            END) AS [DayOfWeek4Wednesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 4 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [Month]
            END) AS [Month4Wednesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 4 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [Year]
            END) AS [Year4Wednesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 4 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMinInPeriod]
            END) AS [DailyMinInPeriod4Wednesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 4 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMaxInPeriod]
            END) AS [DailyMaxInPeriod4Wednesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 4 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [TotalUsageInPeriod]
            END) AS [TotalUsageInPeriod4Wednesday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 4 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AvgerageInPeriod]
            END) AS [AvgerageInPeriod4Wednesday] ,
        
			-------------------------------------------------
	    MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 5 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN	[AmiAggregateKey]
            END) AS [AmiAggregateKey4Thursday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 5 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AmiKey]
            END) AS [AmiKey4Thursday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 5 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AmiAggregateKeyType]
            END) AS [AmiAggregateKeyType4Thursday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 5 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DayOfWeek]
            END) AS [DayOfWeek4Thursday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 5 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [Month]
            END) AS [Month4Thursday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 5 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [Year]
            END) AS [Year4Thursday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 5 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMinInPeriod]
            END) AS [DailyMinInPeriod4Thursday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 5 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMaxInPeriod]
            END) AS [DailyMaxInPeriod4Thursday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 5 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [TotalUsageInPeriod]
            END) AS [TotalUsageInPeriod4Thursday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 5 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AvgerageInPeriod]
            END) AS [AvgerageInPeriod4Thursday] ,
     
			-------------------------------------------------
		MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 6 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN	[AmiAggregateKey]
            END) AS [AmiAggregateKey4Friday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 6 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AmiKey]
            END) AS [AmiKey4Friday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 6 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AmiAggregateKeyType]
            END) AS [AmiAggregateKeyType4Friday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 6 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DayOfWeek]
            END) AS [DayOfWeek4Friday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 6 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [Month]
            END) AS [Month4Friday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 6 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [Year]
            END) AS [Year4Friday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 6 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMinInPeriod]
            END) AS [DailyMinInPeriod4Friday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 6 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMaxInPeriod]
            END) AS [DailyMaxInPeriod4Friday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 6 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [TotalUsageInPeriod]
            END) AS [TotalUsageInPeriod4Friday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 6 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AvgerageInPeriod]
            END) AS [AvgerageInPeriod4Friday] ,
      
			-------------------------------------------------
		MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 7 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN	[AmiAggregateKey]
            END) AS [AmiAggregateKey4Saturday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 7 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AmiKey]
            END) AS [AmiKey4Saturday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 7 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AmiAggregateKeyType]
            END) AS [AmiAggregateKeyType4Saturday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 7 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DayOfWeek]
            END) AS [DayOfWeek4Saturday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 7 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [Month]
            END) AS [Month4Saturday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 7 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [Year]
            END) AS [Year4Saturday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 7 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMinInPeriod]
            END) AS [DailyMinInPeriod4Saturday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 7 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMaxInPeriod]
            END) AS [DailyMaxInPeriod4Saturday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 7 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [TotalUsageInPeriod]
            END) AS [TotalUsageInPeriod4Saturday] ,
        MIN(CASE WHEN AmiAggregateKeyType = 0  and [dayofweek] = 7 AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AvgerageInPeriod]
            END) AS [AvgerageInPeriod4Saturday] ,
      
		-------------------------------------------------
					-- MIN and MAX days in Month
		-------------------------------------------------
		MIN(CASE WHEN AmiAggregateKeyType = 2  AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMinInPeriod]
            END) AS [DailyMinInPeriod] ,
		MIN(CASE WHEN AmiAggregateKeyType = 2  AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DailyMaxInPeriod]
            END) AS [DailyMaxInPeriod] ,
		MIN(CASE WHEN AmiAggregateKeyType = 2  AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DateOfMinInPeriod]
            END) AS [DateOfMinInPeriod] ,
		MIN(CASE WHEN AmiAggregateKeyType = 2  AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [DateOfMaxInPeriod]
            END) AS [DateOfMaxInPeriod] ,
		MIN(CASE WHEN AmiAggregateKeyType = 2  AND [MONTH]=@billmonth  AND [YEAR]=@billyear  THEN [AvgerageInPeriod]
			END) AS [AvgerageInPeriod] 
		-------------------------------------------------
FROM    ( SELECT	  [AmiAggregateKey]
					  ,[AmiKey]
					  ,[AmiAggregateKeyType]
					  ,[PremiseId]
					  ,[DayOfWeek]
					  ,[Month]
					  ,[Year]
					  ,[DailyMinInPeriod]
					  ,[DailyMaxInPeriod]
					  ,[TotalUsageInPeriod]
					  ,[AvgerageInPeriod]
					  ,[DateOfMinInPeriod]
					  ,[DateOfMaxInPeriod]
 ,
                    ROW_NUMBER() OVER ( PARTITION BY PremiseId, AmiAggregateKeyType ORDER BY PremiseId, AmiAggregateKeyType ) AS Row
          FROM      AmiAggregate  
          WHERE     AmiAggregateKeyType in(0,2)  
					AND [MONTH]=@billmonth  AND [YEAR]=@billyear
        ) X 
GROUP BY PremiseId 
)as s ) as t 
on t.premiseid=a.premiseid

end

end
GO
