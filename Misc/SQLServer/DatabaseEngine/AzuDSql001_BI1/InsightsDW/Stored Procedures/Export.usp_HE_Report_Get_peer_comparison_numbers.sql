SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
-- =============================================
-- Description:	This proc gets the benchmarks customers with matching attributes 
--				for particular profile which is passed in as a param
--				and uses the information to UPDATE the the home_energy_report_staging_allcolumns table
-- Written by:	Sunil
-- =============================================
-- Changed by:	Wayne
-- Change date:	8.5.14		
-- Description:	v 1.0.0
--				Changed to work with new DW tables
--				Added some comments
--				Renamed vars FROM 'fuel' to 'commodity'
-- =============================================
-- Changed by:	Wayne	
-- Change date: 8.27.14
-- Description:	v 1.0.0
--			added counts for bills
-- =============================================
-- Changed by:	Wayne
-- Change date: 10.14.2014
-- Description:	v 1.10.0
--	
-- try to push to right		
-- =============================================
-- Changed by:	Wayne
-- Change date: 10.17.2014
-- Description:	v 1.14.0
--	
-- Waste goal should be based on monthly totals for Monthy report
-- and annual totals for Annual report	
-- =============================================
-- Changed by:	Wayne
-- Change date: 10.17.2014
-- Description:	v 1.16.0
--	
-- ComparetoEfficientGMontlyCostPercent & ComparetoAverageGMontlyCostPercent values 
-- should be calculated of values that have already been rounded
-- =============================================
-- Changed by:	Wayne
-- Change date: 11.14.2014
-- Description:	v 1.24.0
--	
-- Change the format of certain date columns based on channel param
-- The following columns were affected:
--		BillStartDate, 
--		BilEndDate, 
--		and all the month labels
-- these column need to be in format like Jan 01 2014 
-- when the channels is Email, but do not change existing format for Print
--
-- =============================================
-- Changed by:	Wayne
-- Change date: 11.17.2014
-- Description:	v 1.26.0    (part of multiple items)
--	
-- put in some checks for bad test data
-- =============================================
-- Changed by:	Wayne
-- Change date: 11.19.2014
-- Description:	v 1.28.0
--	
-- added logic for report number to
-- [Export].[usp_HE_Report_Get_peer_comparison_numbers]
-- if month 13 is null or <=0
-- then use a 1 otherwise use the parameter value
-- and removed a rdundant update being done 
-- to report number in 
-- [Export].[usp_HE_Report_Get_AMI]
-- =============================================
-- Changed by:	Wayne
-- Change date: 12.31.2014
-- Description:	v 1.33.0    
--	
-- put in some checks for bad test data dividing by zero
-- =============================================
-- Changed by:	Wayne
-- Change date: 12.31.2014
-- Description:	v 1.33.0 (same version)
--	
--  Removed the WITH (NOLOCK)'s because
--  they were causing race conditions
-- =============================================
*/
CREATE procedure [Export].[usp_HE_Report_Get_peer_comparison_numbers]
@profile_name varchar (50)  = 'SCG_Profile101',
@bill_year VARCHAR(4) = '2014',
@bill_month VARCHAR(2) = '06',
@channel VARCHAR(5)	=	'email',
@report_number		VARCHAR(1)	=	'1'
as
begin

set nocount on

declare 
		@energyObjectCategory varchar (50)

	declare
		@clientID int, 
		@fuel_type_list varchar (100), 
		@energyobjectcategory_List varchar (1024) , 
		@period varchar (10) , 
		@season_List varchar (100) , 
		@enduse_List varchar (1024) , 
		@ordered_enduse_List varchar (1024), 
		@criteria_id1 varchar (50) , 
		@criteria_detail1 varchar (255)  , 
		@criteria_id2 varchar (50) , 
		@criteria_detail2 varchar (255) , 
		@criteria_id3 varchar (50) , 
		@criteria_detail3 varchar (255) , 
		@criteria_id4 varchar (50) , 
		@criteria_detail4 varchar (255) , 
		@criteria_id5 varchar (50) , 
		@criteria_detail5 varchar (255) , 
		@criteria_id6 varchar (50) , 
		@criteria_detail6 varchar (255), 
		@saveProfile char (1), 
		@verifySave char (1),
		@process_id char (1)

/*
Go get the profile and 
populate the vars
*/
	SELECT
		@clientID = clientID
		, @fuel_type_list = fuel_type
		, @energyobjectcategory_List = energyobjectcategory_List
		, @period = period
		, @season_List = season_List
		, @enduse_List = enduse_List
		, @ordered_enduse_List = ordered_enduse_List
		, @criteria_id1 = criteria_id1
		, @criteria_detail1 = criteria_detail1
		, @criteria_id2 = criteria_id2
		, @criteria_detail2 = criteria_detail2
		, @criteria_id3 = criteria_id3
		, @criteria_detail3 = criteria_detail3
		, @criteria_id4 = criteria_id4
		, @criteria_detail4 = criteria_detail4
		, @criteria_id5 = criteria_id5
		, @criteria_detail5 = criteria_detail5
		, @criteria_id6 = criteria_id6
		, @criteria_detail6 = criteria_detail6
	  FROM [export].[home_energy_report_criteria_profile]  
	where [profile_name] = @profile_name

/*
Define some additional vars
*/
		declare
			@PremiseKey int,
			@BillDateKey int,
			@CommodityKey int,
			@MyUsage decimal(18, 2) ,
			@MyCost decimal(18, 2) ,
			@AverageUsage decimal(18, 2) ,
			@AverageCost decimal(18, 2) ,
			@EfficientUsage decimal(18, 2) ,
			@EfficientCost decimal(18, 2) ,
			@MonthLabel char (6) ,
			@row_id varchar (2) , 
			@sqlcmd varchar (2048),
			@CommodityDesc varchar (50),
			@count int = 1

--================================================================= BENCHMARKS ====================================
/*
create a temp table
to hold monthly benchmarks
*/
		if object_id ('tempdb..#PeerBenchmarks') is not null drop table #PeerBenchmarks
		CREATE TABLE #PeerBenchmarks(
			[PremiseKey] [int] NOT NULL,
			EndDateKey [int] NOT NULL,
			StartDateKey [int] NOT NULL,
			CommodityKey [int] NOT NULL,
			[MyUsage] [decimal](18, 2) NULL,
			[MyCost] [decimal](18, 2) NULL,
			[AverageUsage] [decimal](18, 2) NULL,
			[AverageCost] [decimal](18, 2) NULL,
			[EfficientUsage] [decimal](18, 2) NULL,
			[EfficientCost] [decimal](18, 2) NULL,
			[MonthLabel] char (11) NULL,
			[row_id] varchar (2) NULL,
			CommodityDesc varchar (20) NULL
		)
/*
create a temp table
to hold annual benchmarks
*/
		if object_id ('tempdb..#PeerBenchmarks_annual') is not null drop table #PeerBenchmarks_annual
		CREATE TABLE #PeerBenchmarks_annual(
			[PremiseKey] [int] NOT NULL,
			CommodityKey [int] NOT NULL,
			[MyUsage] [decimal](18, 2) NULL,
			[MyCost] [decimal](18, 2) NULL,
			[AverageUsage] [decimal](18, 2) NULL,
			[AverageCost] [decimal](18, 2) NULL,
			[EfficientUsage] [decimal](18, 2) NULL,
			[EfficientCost] [decimal](18, 2) NULL,
			[MonthLabel] char (11) NULL,
			[row_id] varchar (2) NULL,
			CommodityDesc varchar (20) NULL
		)


/*
the period @startrangedate to @endrangedate will be 13 months, !! dont !! do annual calculations on this range, just show these bills
the period @calcstartrange to @endrangedate will be 12 months, alway do annual calculations on this range,
*/

DECLARE  @endrangedate date= DATEADD(DAY,-1,DATEADD(MONTH,1,(CAST(@bill_year+@bill_month+'01' AS DATE))))  --this is most recent bill
DECLARE  @startrangedate date= DATEADD(MONTH,-12,CAST(@bill_year+@bill_month+'01' AS DATE)) --this is same bill from last year 13months ago
DECLARE @calcstartrange date=DATEADD(MONTH, 1, @startrangedate)

DECLARE @calcstartrangekey int=cast(convert(char(8), @calcstartrange, 112) as int)
DECLARE @startrangekey INT =cast(convert(char(8), @startrangedate, 112) as int) 
DECLARE @endrangekey INT =cast(convert(char(8), @endrangedate, 112) as int) 

/*
load the possible monthly billing columns into a table
*/
if object_id ('tempdb..#ColumnDates') is not null drop table #ColumnDates
CREATE TABLE #ColumnDates
(
rowid [INT] IDENTITY(1,1) NOT NULL,
columnname int
)
INSERT #ColumnDates
SELECT DISTINCT  cast(convert(char(8), d.month, 112) as int) AS columnname  FROM InsightsDW.dbo.DimDate d
WHERE DateKey BETWEEN @startrangekey AND @endrangekey
ORDER BY cast(convert(char(8), d.month, 112) as int) desc	

--================================================================= MONTHLY LOOP ====================================
/*
load the monthly benchmarks into the temp table
*/

		insert #PeerBenchmarks
		(
		  MonthLabel
		, row_id
		, [PremiseKey]
		, EndDateKey
		, StartDateKey
		, [CommodityKey]
		, [MyUsage]
		, [MyCost]
		, [AverageUsage]
		, [AverageCost]
		, [EfficientUsage]
		, [EfficientCost]
		, CommodityDesc
		)
		select CASE WHEN @channel='email' THEN 
			format(CAST(CAST(columnname as char(8)) AS datetime),'MMM dd yyyy', 'en-US' )  
		ELSE        
			LEFT(DATENAME(MONTH,CAST(CAST(columnname AS VARCHAR(8)) AS DATEtime)), 3) + '-' 	
			+ right (convert (varchar, datepart (year,CAST(CAST(columnname AS VARCHAR(8)) AS datetime))), 2)  
		END AS MonthLabel
		, colid
		, [PremiseKey]
		, EndDateKey
		, StartDateKey
		, [CommodityKey]
		, [MyUsage]
		, [MyCost]
		, [AverageUsage]
		, [AverageCost]
		, [EfficientUsage]
		, [EfficientCost]
		, CommodityDesc
		FROM
		(		SELECT * from

					(SELECT  c.columnname, c.rowid AS colid  FROM 	 #columndates c
					 ) f
						LEFT JOIN (
									SELECT 
									d.month AS monthdate
								  , ROW_NUMBER () OVER (PARTITION BY fb.PremiseKey, fb.CommodityKey ORDER BY fb.EndDateKey DESC) AS row_id
								  , fb.[PremiseKey]
								  , fb.EndDateKey
								  , fb.StartDateKey
								  , fb.CommodityKey
								  , [MyUsage]
								  , [MyCost]
								  , [AverageUsage]
								  , [AverageCost]
								  , [EfficientUsage]
								  , [EfficientCost]
								  , f.CommodityDesc
								  ,cast(convert(char(8), d.month, 112) as int) monthdatekey
								  FROM InsightsDW.[dbo].Benchmarks as  fb  
									INNER JOIN	[dbo].[DimDate] d	    		ON d.DateKey = fb.EndDateKey
									INNER JOIN	[dbo].[DimCommodity] f  		ON f.CommodityKey = fb.CommodityKey
									INNER JOIN	[dbo].[DimPremise] p1   		ON p1.PremiseKey = fb.PremiseKey
									WHERE p1.clientID = 101 
									AND fb.EndDateKey BETWEEN @startrangekey AND  @endrangekey
									) AS a 
									ON a.monthdatekey=f.columnname 
		) fb2
		--WHERE fb2.row_id <= 13
		--AND fb2.EndDateKey BETWEEN @startrangekey AND  @endrangekey  --showing 13 months


/*
create a cursor
*/
		DECLARE peerinfo_cursor CURSOR FOR
		SELECT	df.CommodityDesc, row_id
		FROM	#PeerBenchmarks pbm  
				INNER JOIN [dbo].[DimCommodity] df  
				ON df.CommodityKey = pbm.CommodityKey
		GROUP BY df.CommodityDesc, row_id

		OPEN peerinfo_cursor
		FETCH NEXT FROM peerinfo_cursor INTO @CommodityDesc, @row_id
		SELECT	@count = 1

/*
loop thru and UPDATE the home_energy_report_staging_allcolumns table
with benchmark values
*/

		WHILE @@fetch_status = 0
		BEGIN
		
			SELECT	@sqlcmd = '
			UPDATE	he
			set	MonthLabel'		+ @row_id										  + ' = b2.MonthLabel,
				MyHome'			+ LEFT (@CommodityDesc, 1) + 'Usage'	+ @row_id + ' = b2.[MyUsage],
				MyHome'			+ LEFT (@CommodityDesc, 1) + 'Cost'		+ @row_id + ' = b2.[MyCost],
				AverageHome'	+ LEFT (@CommodityDesc, 1) + 'Usage'	+ @row_id + ' = b2.[AverageUsage],
				AverageHome'	+ LEFT (@CommodityDesc, 1) + 'Cost'		+ @row_id + ' = b2.[AverageCost],
				EfficientHome'	+ LEFT (@CommodityDesc, 1) + 'Usage'	+ @row_id + ' = b2.[EfficientUsage],
				EfficientHome'	+ LEFT (@CommodityDesc, 1) + 'Cost'		+ @row_id + ' = b2.[EfficientCost]
			FROM #PeerBenchmarks b2  
				INNER JOIN DimPremise p2	  									
				on p2.PremiseKey = b2.PremiseKey
				INNER JOIN [export].[home_energy_report_staging_allcolumns] he  	
				on he.PremiseID = p2.PremiseId
				where row_id = ' + @row_id + ' and b2.CommodityDesc = ''' + @CommodityDesc + '''
				and b2.premisekey is not null'

			EXEC (@sqlcmd)  --13 months
	

			IF @row_id = 1	
			BEGIN

				SELECT	@sqlcmd = '
				UPDATE	he
				set	
						CTEgasMonthlyCostPercent = 
						    case when CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.EfficientHomeGCost1)))  AS money) !=0 then
								((CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.MyHomeGCost1))) AS money) -CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.EfficientHomeGCost1))) AS money))
								/CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.EfficientHomeGCost1)))  AS money)
									   )  *100 else 0 end,
					CTAgasMonthlyCostPercent =  
						   case when CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.AverageHomeGCost1))) AS money) !=0 then
								 ((CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.MyHomeGCost1))) AS money) -CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.AverageHomeGCost1)))  AS money))
								 /CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.AverageHomeGCost1))) AS money)
                                           )   * 100 else 0 end,'
SELECT @sqlcmd=@sqlcmd +  
				CASE WHEN @channel='email' THEN 
					' BillStartDate'+ @CommodityDesc + '= format(CAST(CAST(b2.StartDateKey as char(8)) AS datetime),''MMM dd yyyy'', ''en-US'' ) ,
					  BillEndDate'+ @CommodityDesc + '=   format(CAST(CAST(b2.EndDateKey as char(8)) AS datetime),''MMM dd yyyy'', ''en-US'' ) '
				ELSE
					' BillStartDate'+ @CommodityDesc + '=  CAST(b2.StartDateKey AS VARCHAR(8)) ,
					  BillEndDate'+ @CommodityDesc + '=   CAST(b2.EndDateKey AS VARCHAR(8)) '
				END
SELECT @sqlcmd=@sqlcmd +  
				' FROM #PeerBenchmarks b2  
					INNER JOIN DimPremise p2	 									
					on p2.PremiseKey = b2.PremiseKey
					INNER JOIN [export].[home_energy_report_staging_allcolumns] he	 
					on he.PremiseID = p2.PremiseId
					where row_id = ' + @row_id + ' and b2.CommodityDesc = ''' + @CommodityDesc + '''					
					'
	
				EXEC (@sqlcmd) --this is a monthly calc
			

			END

			--UPDATE the bill count
					SELECT	@sqlcmd = '
			UPDATE [export].[home_energy_report_staging_allcolumns]
			SET MyHomeBillCount' + LEFT (@CommodityDesc, 1) +  '= fb3.billedmonths
				FROM
			( SELECT PremiseId, COUNT(*) AS billedmonths FROM  (		
				SELECT 
					   ROW_NUMBER () OVER (PARTITION BY fb.PremiseKey, fb.CommodityKey ORDER BY fb.EndDateKey DESC) AS row_id
					  , p1.PremiseId				
				  FROM InsightsDW.[dbo].Benchmarks as  fb  
					INNER JOIN [dbo].[DimDate] d	 		
					ON d.DateKey = fb.EndDateKey
					INNER JOIN [dbo].[DimCommodity] f	 
					ON f.CommodityKey = fb.CommodityKey
					INNER JOIN [dbo].[DimPremise] p1	 	
					ON p1.PremiseKey = fb.PremiseKey
				WHERE p1.clientID = '  + CAST(@clientID AS VARCHAR(10))  + 'AND f.CommodityDesc= ''' + @CommodityDesc + '''
				AND fb.EndDateKey BETWEEN ''' + CAST(@calcstartrangekey AS VARCHAR(8)) + ''' AND ''' + CAST(@endrangekey AS VARCHAR(8))  +''' 
			) fb2
			WHERE fb2.row_id <= 13 
			GROUP BY fb2.Premiseid) fb3
			INNER JOIN [export].[home_energy_report_staging_allcolumns] s
			ON s.PremiseID=fb3.PremiseId'

			
			EXEC (@sqlcmd)	 --annual caculation
		
			--
			SELECT	@count += 1

			FETCH NEXT FROM peerinfo_cursor INTO @CommodityDesc, @row_id

		END

		CLOSE peerinfo_cursor
		DEALLOCATE peerinfo_cursor


--================================================================= ANNUAL LOOP ====================================
/*
load the annual benchmarks into the annual temp table
*/
		INSERT #PeerBenchmarks_annual
		(	
			[PremiseKey]
			, CommodityKey
			, [MyUsage]
			, [MyCost]
			, [AverageUsage]
			, [AverageCost]
			, [EfficientUsage]
			, [EfficientCost]
			, CommodityDesc
		)
		SELECT	
			  [PremiseKey]
			, CommodityKey
			, SUM ([MyUsage])
			, SUM ([MyCost])
			, SUM ([AverageUsage])
			, SUM ([AverageCost])
			, SUM ([EfficientUsage])
			, SUM ([EfficientCost])
			, CommodityDesc
		FROM	#PeerBenchmarks    --this has 13 months
		where EndDateKey BETWEEN @calcstartrangekey AND  @endrangekey  --need 12 months
		GROUP BY [PremiseKey], CommodityKey, CommodityDesc
		

/*
create a cursor for annual
*/

		DECLARE peerinfo_annual_cursor CURSOR FOR
		SELECT	CommodityDesc
		FROM	#PeerBenchmarks_annual pbm
		GROUP BY CommodityDesc
/*
loop thru and UPDATE the home_energy_report_staging_allcolumns table
with annual benchmark values
*/
		OPEN peerinfo_annual_cursor
		FETCH NEXT FROM peerinfo_annual_cursor INTO @CommodityDesc

		WHILE @@fetch_status = 0
		BEGIN

			SELECT	@sqlcmd = '
			UPDATE	he
			set	MyHomeAnnual'		+ LEFT (@CommodityDesc, 1) + 'Cost = b2.[MyCost],
				AverageAnnual'		+ LEFT (@CommodityDesc, 1) + 'Cost = b2.[AverageCost],
				EfficientAnnual'	+ LEFT (@CommodityDesc, 1) + 'Cost = b2.[EfficientCost]
			FROM #PeerBenchmarks_annual b2  
				INNER JOIN DimPremise p2	  									
				on p2.PremiseKey = b2.PremiseKey
				INNER JOIN [export].[home_energy_report_staging_allcolumns] he	 
				ON he.PremiseID = p2.PremiseId
				where b2.CommodityDesc = ''' + @CommodityDesc + ''''

			
			EXEC (@sqlcmd) --annual calc
			



			SELECT	@sqlcmd = '
			UPDATE	he
			set	
				
					CTE' + @CommodityDesc + 'YTDCostPercent  = case when CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.EfficientHomeGCost1)))  AS money) !=0 then					
						  ((CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.MyHomeGCost1))) AS money) -CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.EfficientHomeGCost1))) AS money))/
						  CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.EfficientHomeGCost1)))  AS money)
                                           )  *100 else 0 end ,
					CTA' + @CommodityDesc + 'YTDCostPercent  =  case when CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.AverageHomeGCost1))) AS money) !=0 then
						  ((CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.MyHomeGCost1))) AS money) -CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.AverageHomeGCost1)))  AS money))/
						  CAST(dbo.BankersRoundingStringtoWhole(ltrim(rtrim(he.AverageHomeGCost1))) AS money)
                                           )   * 100 else 0 end,
				CompareToMyselfLessPct' + @CommodityDesc + '=CASE when cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage13 as decimal(18,2)) <> 0
				then((cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage13 as decimal(18,2)) - cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage1  as decimal(18,2)) 
															)/(cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage13 as decimal(18,2)))
															)*100 else 0 end,
				CompareToMyselfMorePct' + @CommodityDesc + '=CASE when cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage13 as decimal(18,2)) <> 0
				then((cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage1  as decimal(18,2)) - cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage13 as decimal(18,2))
															)/(	cast(he.MyHome'+ substring(@CommodityDesc,1,1) + 'Usage13 as decimal(18,2))) 
															)*100 else 0 end
			FROM #PeerBenchmarks_annual b2  
				INNER JOIN DimPremise p2	 									
				on p2.PremiseKey = b2.PremiseKey
				INNER JOIN [export].[home_energy_report_staging_allcolumns] he  	
				on he.PremiseID = p2.PremiseId
				where b2.CommodityDesc = ''' + @CommodityDesc + ''''

				

				
			EXEC (@sqlcmd) --annual calc
			

			--UPDATE the bill count
			SELECT	@sqlcmd = '
			UPDATE [export].[home_energy_report_staging_allcolumns]
			SET MyHomeBillCount' + LEFT (@CommodityDesc, 1) +  '= fb3.billedmonths
				FROM
			( SELECT PremiseId, COUNT(*) AS billedmonths FROM  (		
				SELECT 
					   ROW_NUMBER () OVER (PARTITION BY fb.PremiseKey, fb.CommodityKey ORDER BY fb.EndDateKey DESC) AS row_id
					  , p1.PremiseId				
				  FROM InsightsDW.[dbo].Benchmarks as  fb 
					INNER JOIN [dbo].[DimDate] d			ON d.DateKey = fb.EndDateKey
					INNER JOIN [dbo].[DimCommodity] f	ON f.CommodityKey = fb.CommodityKey
					INNER JOIN [dbo].[DimPremise] p1		ON p1.PremiseKey = fb.PremiseKey
				WHERE p1.clientID = '  + CAST(@clientID AS VARCHAR(10))  + 'AND f.CommodityDesc= ''' + @CommodityDesc + '''
				AND fb.EndDateKey BETWEEN ''' + CAST(@calcstartrangekey AS VARCHAR(8)) + ''' AND ''' + CAST(@endrangekey AS VARCHAR(8))  +'''
			) fb2
			WHERE fb2.row_id <= 13 
			GROUP BY fb2.Premiseid) fb3
			INNER JOIN [export].[home_energy_report_staging_allcolumns] s
			ON s.PremiseID=fb3.PremiseId'
			EXEC (@sqlcmd)	--annual calc
		--

			FETCH NEXT FROM peerinfo_annual_cursor INTO @CommodityDesc

		END

		CLOSE peerinfo_annual_cursor
		DEALLOCATE peerinfo_annual_cursor


		

		--at this point all values to be used in further calcs have already been pulled from tables
		UPDATE	he
		SET ProgramReportNumber =
				CASE WHEN MyHomegUsage13 IS NULL
					OR CAST(MyHomegUsage13 AS decimal(18,2)) <=0.00 then 1
					ELSE @report_number
				end	,	
				MyHomeAnnualTotalCost			= CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0)),
				AverageAnnualTotalCost			= CONVERT (MONEY, ISNULL(AverageAnnualECost,0))		+ CONVERT (MONEY, ISNULL(AverageAnnualGCost,0))		+ CONVERT (MONEY, ISNULL(AverageAnnualWCost,0)),
				EfficientAnnualTotalCost		= CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)),
				AnnualSavDollarPotential10Pct	= CONVERT (MONEY, .10 * (CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0)) + CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0)) + CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0)))),
				AnnualSavDollarPotential15Pct	= CONVERT (MONEY, .15 * (CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0)) + CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0)) + CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0)))), 
				MonthlySavDollarPotential15Pct	= CONVERT (MONEY, .15 * ((
												  CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0)) 
												+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0)) 
												+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0)))
				/(CAST(ISNULL(MyHomeBillCountE,0) AS int) + CAST(ISNULL(MyHomeBillCountG,0) AS INT) + CAST(ISNULL(MyHomeBillCountW,0) AS INT))))
				,
WasteGoalResult= 
case When @period='annual' THEN
				
				CASE	
				When  -- customer uses less than efficient homes 
						(CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0)))<
						(CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)))
						THEN 'A'

				When --or uses less than 8% more than efficient homes.
						 ( CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))			+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0))) <
						 (
							(CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)))
							 +
							(((CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)))
							) * .08) 
						) THEN 'A'
---------------------------
				When  -- customer uses more than 8% of efficient homes 
						 ( CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))			+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0))) >
						 (
							(CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)))
							 +
							(((CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)))
							) * .08) 
						) THEN 'B'

				When --or is within 8% of average homes.
						 ( CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0))) <
						 (
							(CONVERT (MONEY, ISNULL(AverageAnnualECost,0))	+ CONVERT (MONEY, ISNULL(AverageAnnualGCost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualWCost,0)))
							 +
							(((CONVERT (MONEY, ISNULL(AverageAnnualECost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualGCost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualWCost,0)))
							) * .08) 
						) THEN 'B'
-----------------------------
				When --or uses more than 8%  than Average homes.
						 ( CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))			+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0))) >
						 (
							(CONVERT (MONEY, ISNULL(AverageAnnualECost,0))		+ CONVERT (MONEY, ISNULL(AverageAnnualGCost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualWCost,0)))
							 +
							(((CONVERT (MONEY, ISNULL(AverageAnnualECost,0))	+ CONVERT (MONEY, ISNULL(AverageAnnualGCost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualWCost,0)))
							) * .08) 
						) THEN 'C'


						ELSE 'D'
				END

ELSE
				CASE	
				When  -- customer uses less than efficient homes 
						(CONVERT (MONEY, ISNULL(MyHomeECost1,0))		+ CONVERT (MONEY, ISNULL(MyHomeGCost1,0))		+ CONVERT (MONEY, ISNULL(MyHomeWCost1,0)))<
						(CONVERT (MONEY, ISNULL(EfficientHomeECost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeGCost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeWCost1,0)))
						THEN 'A'

				When --or uses less than 8% more than efficient homes.
						 ( CONVERT (MONEY, ISNULL(MyHomeECost1,0))			+ CONVERT (MONEY, ISNULL(MyHomeGCost1,0))		+ CONVERT (MONEY, ISNULL(MyHomeWCost1,0))) <
						 (
							(CONVERT (MONEY, ISNULL(EfficientHomeECost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeGCost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeWCost1,0)))
							 +
							(((CONVERT (MONEY, ISNULL(EfficientHomeECost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeGCost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeWCost1,0)))
							) * .08) 
						) THEN 'A'
---------------------------
				When  -- customer uses more than 8% of efficient homes 
						 ( CONVERT (MONEY, ISNULL(MyHomeECost1,0))			+ CONVERT (MONEY, ISNULL(MyHomeGCost1,0))		+ CONVERT (MONEY, ISNULL(MyHomeWCost1,0))) >
						 (
							(CONVERT (MONEY, ISNULL(EfficientHomeECost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeGCost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeWCost1,0)))
							 +
							(((CONVERT (MONEY, ISNULL(EfficientHomeECost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeGCost1,0))	+ CONVERT (MONEY, ISNULL(EfficientHomeWCost1,0)))
							) * .08) 
						) THEN 'B'

				When --or is within 8% of average homes.
						 ( CONVERT (MONEY, ISNULL(MyHomeECost1,0))		+ CONVERT (MONEY, ISNULL(MyHomeGCost1,0))	+ CONVERT (MONEY, ISNULL(MyHomeWCost1,0))) <
						 (
							(CONVERT (MONEY, ISNULL(AverageHomeECost1,0))	+ CONVERT (MONEY, ISNULL(AverageHomeGCost1,0)) + CONVERT (MONEY, ISNULL(AverageHomeWCost1,0)))
							 +
							(((CONVERT (MONEY, ISNULL(AverageHomeECost1,0)) + CONVERT (MONEY, ISNULL(AverageHomeGCost1,0)) + CONVERT (MONEY, ISNULL(AverageHomeWCost1,0)))
							) * .08) 
						) THEN 'B'
-----------------------------
				When --or uses more than 8%  than Average homes.
						 ( CONVERT (MONEY, ISNULL(MyHomeECost1,0))		+ CONVERT (MONEY, ISNULL(MyHomeGCost1,0))	+ CONVERT (MONEY, ISNULL(MyHomeWCost1,0))) >
						 (
							(CONVERT (MONEY, ISNULL(AverageHomeECost1,0))		+ CONVERT (MONEY, ISNULL(AverageHomeGCost1,0)) + CONVERT (MONEY, ISNULL(AverageHomeWCost1,0)))
							 +
							(((CONVERT (MONEY, ISNULL(AverageHomeECost1,0))	+ CONVERT (MONEY, ISNULL(AverageHomeGCost1,0)) + CONVERT (MONEY, ISNULL(AverageHomeWCost1,0)))
							) * .08) 
						) THEN 'C'


						ELSE 'D'
				END
END
,

			CTATotalYTDCostPercent = 
			(( 
					(CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))	+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0)))
				- (CONVERT (MONEY, ISNULL(AverageAnnualECost,0))	+ CONVERT (MONEY, ISNULL(AverageAnnualGCost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualWCost,0)))
				) / (CONVERT (MONEY, ISNULL(AverageAnnualECost,0))	+ CONVERT (MONEY, ISNULL(AverageAnnualGCost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualWCost,0)))) *100,
			CTETotalYTDCostPercent = 
			(( 
					(CONVERT (MONEY, ISNULL(MyHomeAnnualECost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualGCost,0))		+ CONVERT (MONEY, ISNULL(MyHomeAnnualWCost,0)))
				- (CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))		+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)))
				) / (CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0)))) *100,

			CTATotalMonthlyCostPercent = CTAGasMonthlyCostPercent,
			CTETotalMonthlyCostPercent = CTEGasMonthlyCostPercent

		FROM	[export].[home_energy_report_staging_allcolumns] he
		--next line to bypass bad or missing data
		WHERE 
			(CAST(ISNULL(MyHomeBillCountE,0) AS int) + CAST(ISNULL(MyHomeBillCountG,0) AS INT) + CAST(ISNULL(MyHomeBillCountW,0) AS INT)) !=0
			AND (CONVERT (MONEY, ISNULL(AverageAnnualECost,0))	+ CONVERT (MONEY, ISNULL(AverageAnnualGCost,0)) + CONVERT (MONEY, ISNULL(AverageAnnualWCost,0))) !=0
			AND (CONVERT (MONEY, ISNULL(EfficientAnnualECost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualGCost,0))	+ CONVERT (MONEY, ISNULL(EfficientAnnualWCost,0))) !=0
			
SET NOCOUNT OFF

END

GO
