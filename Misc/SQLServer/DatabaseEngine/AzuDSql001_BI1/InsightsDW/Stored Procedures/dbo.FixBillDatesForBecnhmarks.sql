
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ubaid
-- Create date: 18 Oct, 2014
-- Description:	Fix Duplicate bill dates for benchmarks
-- =============================================
CREATE PROCEDURE [dbo].[FixBillDatesForBecnhmarks] @ClientId AS INT
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @timestamp AS DATETIME = GETDATE();

        IF @ClientId = 101
            BEGIN

                SELECT DISTINCT
                        PremiseKey
                INTO    #AllPremises
                FROM    ( SELECT    PremiseKey ,
                                    ContentOptionKey ,
                                    ROW_NUMBER() OVER ( PARTITION BY PremiseKey,
                                                        ContentAttributeKey ORDER BY TrackingDate DESC ) AS SortID
                          FROM      dbo.FactPremiseAttribute
                          WHERE     ClientID = @ClientId
                                    AND ContentAttributeKey = 'paperreport.analysisgroup.enrollmentstatus'
                        ) p
                WHERE   p.SortID = 1
                        AND p.ContentOptionKey = 'paperreport.analysisgroup.enrollmentstatus.enrolled';

                SELECT  PremiseKey ,
                        DATEDIFF(DAY, StartDate, EndDate) + BillDays AS BDays ,
                        DATEFROMPARTS(DATEPART(YEAR, EndDate),
                                      DATEPART(MONTH, EndDate), 15) AS BillDate
                INTO    #PremisesWithBills
                FROM    ( SELECT    sc.PremiseKey ,
                                    BillDays ,
                                    MIN(EndDate) OVER ( PARTITION BY sc.PremiseKey ) AS StartDate ,
                                    MAX(EndDate) OVER ( PARTITION BY sc.PremiseKey ) AS EndDate ,
                                    SUM(BillDays) OVER ( PARTITION BY sc.PremiseKey ) AS TotalBillDays ,
                                    ROW_NUMBER() OVER ( PARTITION BY sc.PremiseKey ORDER BY EndDate ) AS RNumber
                          FROM      dbo.FactServicePointBilling fb
                                    INNER JOIN dbo.DimServiceContract sc ON sc.ServiceContractKey = fb.ServiceContractKey
                                    INNER JOIN #AllPremises ap ON sc.PremiseKey = ap.PremiseKey
                          WHERE     EndDate >= DATEADD(MONTH, -15, GETDATE())
                        ) t
                WHERE   RNumber = 1
                        AND ( DATEDIFF(DAY, StartDate, EndDate) + BillDays )
                        - TotalBillDays <= 1;

                PRINT 'Filter Premises with Missing Bills:'
                    + CONVERT(VARCHAR(20), DATEDIFF(SECOND, @timestamp,
                                                    GETDATE()));
                SET @timestamp = GETDATE();


                SELECT  @ClientId AS ClientId ,
                        PremiseKey ,
                        EndDate ,
                        BillDays ,
                        BillDate
                INTO    #Results
                FROM    ( SELECT    sc.PremiseKey ,
                                    fb.EndDate ,
                                    fb.BillDays ,
                                    DATEADD(MONTH,
                                            -1
                                            * ( ( ROW_NUMBER() OVER ( PARTITION BY ap.PremiseKey ORDER BY EndDate DESC ) )
                                                - 1 ), ap.BillDate) AS BillDate ,
                                    ( ROW_NUMBER() OVER ( PARTITION BY ap.PremiseKey ORDER BY EndDate DESC ) ) AS RIndex
                          FROM      #PremisesWithBills ap
                                    INNER JOIN dbo.DimServiceContract sc ON sc.PremiseKey = ap.PremiseKey
                                    INNER JOIN dbo.FactServicePointBilling fb ON sc.ServiceContractKey = fb.ServiceContractKey
                        ) A
                WHERE   RIndex <= 13
                ORDER BY PremiseKey ,
                        A.BillDate DESC;

                PRINT 'Process Bill Date:'
                    + CONVERT(VARCHAR(20), DATEDIFF(SECOND, @timestamp,
                                                    GETDATE()));
                SET @timestamp = GETDATE();

                DELETE  bb
                FROM    [dbo].[BenchmarkBills] bb
                        LEFT JOIN #Results r ON r.PremiseKey = bb.PremiseKey
                                                AND r.EndDate = bb.EndDate
                WHERE   bb.ClientId = @ClientId
                        AND r.PremiseKey IS NULL;

                PRINT 'Delete Old Records:'
                    + CONVERT(VARCHAR(20), DATEDIFF(SECOND, @timestamp,
                                                    GETDATE()));
                SET @timestamp = GETDATE();

                INSERT  INTO [dbo].[BenchmarkBills]
                        SELECT  r.* ,
                                GETDATE()
                        FROM    #Results r
                                LEFT JOIN [dbo].[BenchmarkBills] bb ON r.PremiseKey = bb.PremiseKey
                                                              AND r.EndDate = bb.EndDate
                        WHERE   bb.PremiseKey IS NULL;

                PRINT 'Insert New Records:'
                    + CONVERT(VARCHAR(20), DATEDIFF(SECOND, @timestamp,
                                                    GETDATE()));
                SET @timestamp = GETDATE();

                UPDATE  bb
                SET     BillDate = r.BillDate ,
                        DateModified = GETDATE()
                FROM    [dbo].[BenchmarkBills] bb
                        INNER JOIN #Results r ON r.PremiseKey = bb.PremiseKey
                                                 AND bb.EndDate = r.EndDate
                WHERE   r.BillDate != bb.BillDate;

                PRINT 'Update Bill Dates:'
                    + CONVERT(VARCHAR(20), DATEDIFF(SECOND, @timestamp,
                                                    GETDATE()));


                DROP TABLE #Results;
                DROP TABLE #PremisesWithBills;
                DROP TABLE #AllPremises;

            END;
    END;
GO
