SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 4/2/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_DimCustomer]
				@ClientID INT
AS

BEGIN

	SET NOCOUNT ON;

	INSERT INTO [InsightsDW].[dbo].[DimCustomer] (
							ClientKey, 
							ClientId, 
							PostalCodeKey, 
							CustomerAuthenticationTypeKey, 
							SourceKey, 
							CityKey, 
							CustomerId, 
							FirstName, 
							LastName, 
							Street1, 
							Street2, 
							City, 
							StateProvince, 
							Country, 
							PostalCode, 
							PhoneNumber, 
							MobilePhoneNumber, 
							EmailAddress, 
							AlternateEmailAddress, 
							CreateDate, 
							--UpdateDate,
							SourceId, 
							AuthenticationTypeId,
							ETL_LogId,
							TrackingId,
							TrackingDate)
	SELECT  cl.ClientKey,
			cl.ClientId,
			ISNULL(dpc.PostalCodeKey, -1),
			dcat.CustomerAuthenticationTypeKey,
			ds.SourceKey,
			ISNULL(dc.CityKey, -1) AS CityKey,
			c.CustomerId,
			c.FirstName, 
			c.LastName,
			c.Street1,  
			c.Street2, 
			c.City, 
			c.StateProvince,
			c.Country,
			c.PostalCode,
			c.PhoneNumber,
			c.MobilePhoneNumber,
			c.EmailAddress,
			c.AlternateEmailAddress,
			GETUTCDATE(),
			--ISNULL(c.UpdateDate, GETUTCDATE()),
			c.SourceId,
			c.AuthenticationTypeId,
			ETL_LogId,
			TrackingId,
			TrackingDate
	FROM [InsightsDW].[ETL].[INS_DimCustomer] c WITH (NOLOCK)
	INNER JOIN [InsightsDW].[dbo].[DimClient] cl WITH (NOLOCK) ON cl.ClientId = c.ClientId 
	INNER JOIN [InsightsDW].[dbo].[DimCustomerAuthenticationType] dcat WITH (NOLOCK) ON dcat.CustomerAuthenticationTypeId = c.AuthenticationTypeId 
	INNER JOIN [InsightsDW].[dbo].[DimSource] ds WITH (NOLOCK) ON ds.SourceId = c.SourceId 
	LEFT JOIN [InsightsDW].[dbo].[DimCountryRegion] dcr WITH (NOLOCK) ON dcr.CountryRegionCode = c.Country
	LEFT JOIN [InsightsDW].[dbo].[DimStateProvince] dsp WITH (NOLOCK) ON dsp.StateProvinceName = c.StateProvince
													AND dsp.CountryRegionKey = dcr.CountryRegionKey
	LEFT JOIN [InsightsDW].[dbo].[DimCity] dc WITH (NOLOCK) ON dc.City = c.City
													AND dc.StateProvinceKey = dsp.StateProvinceKey
	LEFT JOIN [InsightsDW].[dbo].[DimPostalCode] dpc WITH (NOLOCK) ON dpc.PostalCode = c.PostalCode
													AND dpc.CityKey = dc.CityKey
	WHERE c.ClientId = @ClientID AND c.PostalCode IS NOT NULL


END

														 



GO
