SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		MGanley
-- Create date: 3/10/2015
-- Description:	Get billing data for BillDisagg
--		CustomerId = 'GUL47191'
--		AccountId = 'ZUW26449'
--		PremiseId = 'RIV9449'
-- =============================================
CREATE PROCEDURE [dbo].[uspBDGetBills]
    @ClientID INT ,
    @CustomerID VARCHAR(50) ,
    @AccountID VARCHAR(50) ,
    @PremiseID VARCHAR(50)
AS
    BEGIN

        DECLARE @premiseKey VARCHAR(50);

        SELECT  @premiseKey = p.PremiseKey
        FROM    dbo.DimPremise p WITH ( NOLOCK )
                INNER JOIN dbo.FactCustomerPremise cp WITH ( NOLOCK ) ON cp.PremiseKey = p.PremiseKey
                INNER JOIN dbo.DimCustomer c WITH ( NOLOCK ) ON c.CustomerKey = cp.CustomerKey
                INNER JOIN dbo.DimClient r WITH ( NOLOCK ) ON r.ClientKey = c.ClientKey
        WHERE   r.ClientId = @ClientID
                AND c.CustomerId = @CustomerID
                AND p.AccountId = @AccountID
                AND p.PremiseId = @PremiseID;

        SET NOCOUNT ON;

        WITH    b AS ( SELECT   sc.PremiseKey ,
                                StartDate ,
                                EndDate ,
                                f.TotalUsage AS totalunits ,
                                f.CostOfUsage AS totalcost ,
                                CommodityKey ,
                                UOMKey ,
                                BillDays
                       FROM     dbo.FactServicePointBilling f WITH ( NOLOCK )
                                INNER JOIN dbo.DimServiceContract sc WITH ( NOLOCK ) ON sc.ServiceContractKey = f.ServiceContractKey
                       WHERE    PremiseKey = @premiseKey
                     )
            SELECT TOP 100
                    p.PremiseKey ,
                    p.PremiseId ,
                    StartDate ,
                    EndDate ,
                    totalunits ,
                    totalcost ,
                    CommodityDesc AS CommodityKey ,
                    UOMDesc AS UomKey ,
                    BillDays
            FROM    b
                    INNER JOIN DimPremise p WITH ( NOLOCK ) ON b.PremiseKey = p.PremiseKey
                    INNER JOIN FactCustomerPremise cp WITH ( NOLOCK ) ON p.PremiseKey = cp.PremiseKey
                    INNER JOIN DimCustomer cust WITH ( NOLOCK ) ON cust.CustomerKey = cp.CustomerKey
                    INNER JOIN DimCommodity co WITH ( NOLOCK ) ON co.CommodityKey = b.CommodityKey
                    INNER JOIN DimUOM u WITH ( NOLOCK ) ON u.UOMKey = b.UOMKey
            WHERE   cust.ClientId = @ClientID
                    AND p.PremiseKey = @premiseKey
            ORDER BY b.StartDate DESC;


    END;


GO
