-- exec rpt.getUtilityPortalUsers 224, '10/1/2013','11/1/2015',null, null, 'her2015program'
if exists (select * from sysobjects where type = 'p' and name = 'rpt.getUtilityPortalUsers')
drop procedure rpt.getUtilityPortalUsers
go

create procedure rpt.getUtilityPortalUsers @clientID int, @startDateBaseline date, @endDateBaseline date, @startDateCurrent date, @endDateCurrent date, @program varchar(200)
as

set nocount on

if @startDateCurrent is null 
begin
	select @startDateCurrent = @endDateBaseline
end

if @endDateCurrent is null 
begin
	select @endDateCurrent = getdate()
end

	;with treatmentgroups
	as
	(
		SELECT    clientID, premisekey,	ContentAttributeKey,value,EffectiveDate,
		ROW_NUMBER() OVER ( PARTITION BY PremiseKey, ContentAttributeKey ORDER BY fpa.EffectiveDate DESC,fpa.TrackingDate DESC ) AS SortId
		FROM  dbo.FactPremiseAttribute fpa 
		where fpa.clientID = @clientID
		and fpa.ContentAttributeKey like @program + '%groupnumber'
	),
	myacctenrollment
	as
	(
		SELECT    premisekey,	ContentAttributeKey,value,EffectiveDate,EffectiveDateKey,
		ROW_NUMBER() OVER ( PARTITION BY PremiseKey, ContentAttributeKey ORDER BY fpa.EffectiveDate DESC,fpa.TrackingDate DESC ) AS SortId
		FROM  dbo.FactPremiseAttribute fpa 
		where fpa.ContentAttributeKey = 'utilityportal.enrollmentstatus' 
	)
	--select t1.value, substring(convert(varchar(30),t3.fulldateAlternateKey),1,4) + '_' + substring(convert(varchar(30),t3.fulldateAlternateKey),6,2) as PortalEnrollMonth,count(distinct t1.premisekey) as PremiseCount 
	select t1.clientID, t6.customerKey, t1.premiseKey, t6.customerID, t4.accountID, t4.premiseID, 0 as eventTypeID, t2.EffectiveDate as flagDateCreated, t2.EffectiveDate as flagDateIpdated,
		t3.FullDateAlternateKey,t3.monthName,substring(convert(varchar(15),t3.fullDateAlternateKey),1,7) as eventMonth,'Utility Portal' as groupName,0 as groupSort, 1 as bInclude, t1.value as segment, @program as program,
		'Enrolled' as EventShortName, '' as EmailID
	from treatmentgroups t1 join myacctenrollment t2
		on t1.premisekey = t2.premisekey
	join dimdate t3
		on t2.effectivedatekey = t3.datekey
	join dimpremise t4
		on t1.premisekey = t4.premisekey
	join factcustomerpremise t5
		on t4.premisekey = t5.premisekey
	join dimcustomer t6
		on t5.customerkey = t6.customerkey
	where t2.sortid = 1
	and t1.sortid = 1
	and t2.value = 'utilityportal.enrollmentstatus.enrolled'
	and t2.EffectiveDate > @startDateCurrent
--	group by t1.value, substring(convert(varchar(30),t3.fulldateAlternateKey),1,4) + '_' + substring(convert(varchar(30),t3.fulldateAlternateKey),6,2)
--	order by t1.value, substring(convert(varchar(30),t3.fulldateAlternateKey),1,4) + '_' + substring(convert(varchar(30),t3.fulldateAlternateKey),6,2)
	order by t1.value, substring(convert(varchar(15),t3.fullDateAlternateKey),1,7)

set nocount off
go
