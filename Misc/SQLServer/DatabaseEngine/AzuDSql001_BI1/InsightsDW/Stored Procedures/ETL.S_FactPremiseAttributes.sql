SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/21/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[S_FactPremiseAttributes]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;

		--SELECT * 
		--FROM wh.ProfilePremiseAttribute WITH (NOLOCK)
		--WHERE ClientID = @ClientID
		--ORDER By ClientId,  AccountId, PremiseId, AttributeKey

		SELECT *
		FROM    ( SELECT    fpa.ClientID,
							fpa.PremiseID,
							fpa.AccountID,
							ContentAttributeKey AS AttributeKey,
							Value,
							EffectiveDate,
							ROW_NUMBER() OVER ( PARTITION BY PremiseKey, OptionKey ORDER BY EffectiveDate DESC ) AS SortId
					FROM    InsightsDW.dbo.FactPremiseAttribute fpa WITH(NOLOCK)
					INNER JOIN ETL.KEY_FactPremiseAttribute kfpa ON	kfpa.ClientId = fpa.ClientID
																	AND kfpa.PremiseId = fpa.PremiseID
																	AND kfpa.AccountId = fpa.AccountID
																	AND kfpa.Attributeid = fpa.AttributeId
					WHERE      fpa.ClientID = @ClientID
				) p
		WHERE   SortId = 1 
		ORDER BY p.ClientID,
				 p.PremiseID,
				 p.AccountID,
				 p.AttributeKey
		 


END





GO
