SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/29/2014
-- Description:	
-- =============================================
create PROCEDURE [ETL].[S_DimMeter]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;

		SELECT * 
		FROM [dbo].[DimMeter] 
		WHERE ClientId = @ClientID
		ORDER BY ClientId, ServicePointId, MeterId


END





GO
