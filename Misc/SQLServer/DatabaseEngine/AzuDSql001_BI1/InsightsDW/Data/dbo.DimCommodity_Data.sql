SET IDENTITY_INSERT [dbo].[DimCommodity] ON
INSERT INTO [dbo].[DimCommodity] ([CommodityKey], [CommodityDesc], [CommodityId]) VALUES (0, 'all', 0)
INSERT INTO [dbo].[DimCommodity] ([CommodityKey], [CommodityDesc], [CommodityId]) VALUES (1, 'electric', 1)
INSERT INTO [dbo].[DimCommodity] ([CommodityKey], [CommodityDesc], [CommodityId]) VALUES (2, 'gas', 2)
INSERT INTO [dbo].[DimCommodity] ([CommodityKey], [CommodityDesc], [CommodityId]) VALUES (3, 'water', 3)
INSERT INTO [dbo].[DimCommodity] ([CommodityKey], [CommodityDesc], [CommodityId]) VALUES (4, 'sewer', 4)
INSERT INTO [dbo].[DimCommodity] ([CommodityKey], [CommodityDesc], [CommodityId]) VALUES (5, 'garbage', 5)
SET IDENTITY_INSERT [dbo].[DimCommodity] OFF
