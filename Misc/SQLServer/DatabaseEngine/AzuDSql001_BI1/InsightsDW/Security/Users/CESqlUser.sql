IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'CESqlUser')
CREATE LOGIN [CESqlUser] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [CESqlUser] FOR LOGIN [CESqlUser]
GO
