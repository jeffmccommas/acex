CREATE ROLE [db_execproc]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'db_execproc', N'ACLARACE\svcSchedTasks'
GO
