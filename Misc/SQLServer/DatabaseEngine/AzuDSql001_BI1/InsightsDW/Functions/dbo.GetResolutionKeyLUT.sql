SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[GetResolutionKeyLUT] ()
RETURNS @ReturnTable Table                                        
   (   
     ResolutionId  int,                                     
     ResolutionKey varchar(20)
   )
AS
BEGIN
		BEGIN
           INSERT INTO @ReturnTable VALUES 
			(1,'fivemin'),
			(2,'tenmin'),
			(3,'fifteenmin'),
			(4,'halfhour'),
			(5,'hour'),
			(6,'fourhour'),
			(7,'sixhour'),
			(8,'twelvehour'),
			(9,'day'),
			(10,'week'),
			(11,'month'),
			(12,'year'),
			(13,'specifiedperiod'),
			(14,'billperiod'),
			(15,'twomonth'),
			(16,'quarter'),
			(17,'sixmonth')			                               
		END
RETURN
END





GO
