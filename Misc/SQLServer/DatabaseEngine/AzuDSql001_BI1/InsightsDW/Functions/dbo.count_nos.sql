SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[count_nos]
 (@string VARCHAR(100)) RETURNS VARCHAR(10) AS BEGIN DECLARE @return VARCHAR(100) SELECT @return = (LEN(@string)*10) - (LEN(REPLACE(@string,'0','')) + LEN(REPLACE(@string,'1','')) + LEN(REPLACE(@string,'2','')) + LEN(REPLACE(@string,'3','')) + LEN(REPLACE(@string,'4','')) + LEN(REPLACE(@string,'5','')) + LEN(REPLACE(@string,'6','')) + LEN(REPLACE(@string,'7','')) + LEN(REPLACE(@string,'8','')) + LEN(REPLACE(@string,'9',''))) 
 RETURN @return END 
GO
