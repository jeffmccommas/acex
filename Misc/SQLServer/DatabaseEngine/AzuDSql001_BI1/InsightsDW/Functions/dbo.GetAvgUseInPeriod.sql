SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
-- =============================================
-- Author:		Wayne
-- Create date: 9.30.2014
-- Description:	gets average use between two dates
-- =============================================
*/
CREATE FUNCTION [dbo].[GetAvgUseInPeriod] (@startdate int, @enddate int)

RETURNS TABLE



AS

RETURN

(


select top 1 Premiseid, SUM(TotalUnits) as TotalUnits, (select count(*) from Insightsdw.dbo.dimdate d 
                                                            where DateKey Between @startdate and @enddate 
                                                            ) as Numberofdays,
                             SUM(TotalUnits) / (select count(*) from Insightsdw.dbo.dimdate d 
                                                            where DateKey Between @startdate and @enddate 
                                                            )  as Average 
  from 
       (
             select
             premiseid, d.datekey, 
                    isnull(summarized.totalunits,0)  as totalunits
             from 
             Insightsdw.dbo.dimdate d 
             left outer join
               (
               select f.PremiseId, f.datekey, sum(totalunits) totalunits from
                           [InsightsDW].[dbo].[FactAmiDetail] f
                           inner join
                           Insightsdw.dbo.dimdate d 
                           on f.datekey = d.datekey 
                           Group by f.PremiseId,f.datekey
                    ) as summarized
             on d.DateKey=summarized.DateKey
             where d.DateKey Between  @startdate and @enddate
       ) as a
       group by a.Premiseid 
     


);



GO
