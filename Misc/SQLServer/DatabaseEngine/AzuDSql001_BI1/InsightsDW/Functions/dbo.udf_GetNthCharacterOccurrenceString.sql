SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[udf_GetNthCharacterOccurrenceString]
(
@p_StringToSearch       VARCHAR(MAX),
@p_CharToSearchFor      VARCHAR(50),
@p_NthOccurrence        INT
)
RETURNS VARCHAR(250)
AS
/*=======================================================
Description: Get's the position of the Nth occurrence of
--	Execution Example --(find the 2nd space in an address field)
	select Description,    dbo.udf_GetNthCharacterOccurrence(Description, ' ', 2) from       BlogExample
=======================================================*/
BEGIN--udf
--declare local variables
DECLARE
@v_MinLoop					INT,
@v_CharPosition				INT,
@space_after_CharPosition   INT,
@p_FoundString				VARCHAR(250),
@p_StringToSearch2       VARCHAR(MAX)
DECLARE @pat varchar(10) 
SET @pat = '%[^ ' + char(09) + char(10) +  char(13) + ']%'
--default local variables
SET @v_MinLoop = 2
SET @v_CharPosition =CHARINDEX(@p_CharToSearchFor, @p_StringToSearch)
SET @p_StringToSearch2=SUBSTRING(@p_StringToSearch, PATINDEX(@pat, @p_StringToSearch),LEN(@p_StringToSearch) - PATINDEX(@pat, @p_StringToSearch) - PATINDEX(@pat, REVERSE(@p_StringToSearch)) + 2)
--only enter loop if we're looking for position >1
WHILE @v_MinLoop <= @p_NthOccurrence
BEGIN--loop
     
      --Figure out what position the character is in
      SET @v_CharPosition =(SELECT CHARINDEX(@p_CharToSearchFor, @p_StringToSearch2, @v_CharPosition+1))
      --increment the loop
      SET @v_MinLoop = @v_MinLoop + 1
END--loop
SET @space_after_CharPosition =(SELECT CHARINDEX(' ', @p_StringToSearch2, @v_CharPosition+1))
IF @space_after_CharPosition >0
SET @p_FoundString	= SUBSTRING(@p_StringToSearch2,@v_CharPosition, @space_after_CharPosition-@v_CharPosition)
--return the value
RETURN(@p_FoundString)
END--udf

GO
