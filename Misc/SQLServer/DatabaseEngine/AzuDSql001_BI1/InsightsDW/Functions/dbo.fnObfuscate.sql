SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnObfuscate](
	@inputWord VARCHAR(128)
)
RETURNS VARCHAR(128)
AS
BEGIN

	DECLARE @charIndex SMALLINT
	DECLARE @trigramID SMALLINT, @digramID SMALLINT, @letterID SMALLINT
	DECLARE @outputWord VARCHAR(128)

	SET @charIndex = 1
	SET @outputWord = ''
	-- Trigram Replace
	WHILE @charIndex <= LEN (@inputWord)
	BEGIN
		SELECT @trigramID = ISNULL(trigramID, 0) FROM tblTrigram WHERE trigram = SUBSTRING(@inputWord, @charIndex, 3)
		IF @trigramID != 0
		BEGIN
			IF @trigramID = 98 SET @trigramID = 1 ELSE SET @trigramID = @trigramID + 1

			SELECT @outputWord = @outputWord + trigram FROM tblTrigram WHERE trigramID = @trigramID

			-- Remove Trigram From input word
			SELECT @inputWord = REPLACE(@inputWord, SUBSTRING(@inputWord, @charIndex, 3), '???')
			-- Skip Rest of Trigram
			SELECT @charIndex = @charIndex + 2 


		END
		ELSE
			SET @outputWord = @outputWord + '?' -- PlaceHolder

		SET @trigramID = 0
		SET @charIndex = @charIndex + 1
	END
	
	SET @charIndex = 1

	-- Digram Replace
	WHILE @charIndex <= LEN (@inputWord)
	BEGIN
		SELECT @digramID = ISNULL(digramID, 0) FROM tblDigram WHERE digram = SUBSTRING(@inputWord, @charIndex, 2)
		IF @digramID != 0
		BEGIN
			IF @digramID = 100 SET @digramID = 1 ELSE SET @digramID = @digramID + 1

			SELECT @outputWord = STUFF(@outputWord, @charIndex, 2, digram) FROM tblDigram WHERE digramID = @digramID
			-- Remove digram From input word
			SELECT @inputWord = REPLACE(@inputWord, SUBSTRING(@inputWord, @charIndex, 2), '??')
			-- Skip Rest of Digram
			SELECT @charIndex = @charIndex + 1
		END

		SET @digramID = 0
		SET @charIndex = @charIndex + 1
	END

	SET @charIndex = 1


	-- Letter Replace
	WHILE @charIndex <= LEN (@inputWord)
	BEGIN
		IF SUBSTRING(@inputWord, @charIndex, 1) != '?' AND SUBSTRING(@inputWord, @charIndex, 1) != ' '
			SELECT @outputWord = CASE 
					WHEN ASCII(SUBSTRING(@inputWord, @charIndex, 1)) + 1 <90 
						THEN STUFF(@outputWord, @charIndex, 1, CHAR(ASCII(SUBSTRING(@inputWord, @charIndex, 1)) + 1))
						ELSE STUFF(@outputWord, @charIndex, 1, 'A') END

		SET @charIndex = @charIndex + 1
	END

	-- Replace spaces back in to word/phrase
	SET @outputWord = REPLACE(@outputWord, '?', ' ')

	RETURN @outputWord

END
GO
