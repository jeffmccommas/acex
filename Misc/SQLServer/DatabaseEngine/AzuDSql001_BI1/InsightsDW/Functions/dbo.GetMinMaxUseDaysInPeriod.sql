SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
-- =============================================
-- Author:		Wayne
-- Create date: 9.30.2014
-- Description:	gets average use between two dates
-- =============================================
*/
CREATE FUNCTION [dbo].[GetMinMaxUseDaysInPeriod] (@startdate int, @enddate int)

RETURNS TABLE



AS

RETURN

( 

SELECT top 1 a1.PremiseId, a1.Low, a1.LowDailyUse, a1.LowDate, b1.High, b1.HighDailyUse, b1.HighDate
FROM (
	select a.PremiseId, a.Low, a.LowDailyUse, (select top 1 datekey from  [InsightsDW].[dbo].[FactAmiDetail] f
				where f.DateKey Between @startdate and @enddate
				and f.PremiseId=a.premiseid and f.TotalUnits=a.LowDailyUse) as LowDate
			from
           (  select 'MIN' as Low, premiseid,  Min(totalunits)  as LowDailyUse
             from   [InsightsDW].[dbo].[FactAmiDetail] f
				where f.DateKey Between @startdate and @enddate
				Group by f.PremiseId
				) as a
				)
				as a1
	inner join
	(select b.PremiseId, b.High, b.HighDailyUse, (select top 1 datekey from  [InsightsDW].[dbo].[FactAmiDetail] f
				where f.DateKey Between @startdate and @enddate
				and f.PremiseId=b.premiseid and f.TotalUnits=b.HighDailyUse) as HighDate
			from
			(
             select 'MAX' as High, premiseid,  Max(totalunits)  as HighDailyUse
             from   [InsightsDW].[dbo].[FactAmiDetail] f
				where f.DateKey Between @startdate and @enddate
				Group by f.PremiseId
				) as b
				)
				as b1
	on a1.PremiseId=b1.PremiseId

);



GO
