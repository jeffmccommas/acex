SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[udf_GetNthCharacterOccurrence]
(
@p_StringToSearch       VARCHAR(MAX),
@p_CharToSearchFor      VARCHAR(50),
@p_NthOccurrence        INT
)
RETURNS INT
AS
/*=======================================================
Description: Get's the position of the Nth occurrence of
--	Execution Example --(find the 2nd space in an address field)
	select Description,    dbo.udf_GetNthCharacterOccurrence(Description, ' ', 2) from       BlogExample
=======================================================*/
BEGIN--udf
--declare local variables
DECLARE
@v_MinLoop  INT,
@v_CharPosition   INT
--default local variables
SET @v_MinLoop = 2
SET @v_CharPosition =CHARINDEX(@p_CharToSearchFor, @p_StringToSearch)
--only enter loop if we're looking for position >1
WHILE @v_MinLoop <= @p_NthOccurrence
BEGIN--loop
     
      --Figure out what position the character is in
      SET @v_CharPosition =(SELECT CHARINDEX(@p_CharToSearchFor, @p_StringToSearch, @v_CharPosition+1))
      --increment the loop
      SET @v_MinLoop = @v_MinLoop + 1
END--loop
--return the value
RETURN(@v_CharPosition)
END--udf
GO
