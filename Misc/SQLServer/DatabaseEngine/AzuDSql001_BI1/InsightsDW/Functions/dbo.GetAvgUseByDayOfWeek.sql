SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
-- =============================================
-- Author:		Wayne
-- Create date: 9.30.2014
-- Description:	gets the number of occurences a specific day of week between two dates
--				and then averages the usage on those days
-- =============================================
*/
CREATE FUNCTION [dbo].[GetAvgUseByDayOfWeek] (@startdate int, @enddate int, @dayofweek varchar(20))

RETURNS TABLE



AS

RETURN

(


select top 1 Premiseid, SUM(TotalUnits) as TotalUnits, (select count(*) from Insightsdw.dbo.dimdate d 
                                                            where DateKey Between @startdate and @enddate 
                                                            and d.datename like concat(@dayofweek,'%')) as Numberofdays,
                             SUM(TotalUnits) / (select count(*) from Insightsdw.dbo.dimdate d 
                                                            where DateKey Between @startdate and @enddate 
                                                            and d.datename like concat(@dayofweek,'%'))  as Average 
  from 
       (
             select --isnull(premiseid,'65236500') as 
             premiseid, d.datekey, 
                    isnull(summarized.totalunits,0)  as totalunits
             from 
             Insightsdw.dbo.dimdate d 
             left outer join
               (
               select f.PremiseId, f.datekey, sum(totalunits) totalunits from
                           [InsightsDW].[dbo].[FactAmiDetail] f
                           inner join
                           Insightsdw.dbo.dimdate d 
                           on f.datekey = d.datekey 
                           --where Premiseid = '65236500'
                           Group by f.PremiseId,f.datekey
                    ) as summarized
             on d.DateKey=summarized.DateKey
             where d.DateKey Between  @startdate and @enddate
             and d.datename like  concat(@dayofweek,'%')
       ) as a
       --where a.Premiseid = '65236500'
       group by a.Premiseid 
     


);



GO
