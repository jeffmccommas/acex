SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================

CREATE FUNCTION [dbo].[GetApplianceName]
(
	-- Add the parameters for the function here
	@ApplianceName NVARCHAR(100)
)
RETURNS NVARCHAR(100)
AS
BEGIN
DECLARE @RetApplianceName NVARCHAR(100)
	-- Declare the return variable here
	DECLARE @underscoreloc  int
	
	select @underscoreloc = charindex('_',@ApplianceName)
	IF @underscoreloc  = 0
	begin
	SELECT @RetApplianceName =  @ApplianceName
	end 
ELSE
begin
SELECT @RetApplianceName = LEFT(@ApplianceName,@underscoreloc - 1)
end
RETURN @RetApplianceName

END





GO
