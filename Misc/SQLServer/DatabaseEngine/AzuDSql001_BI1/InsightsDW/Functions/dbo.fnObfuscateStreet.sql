SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
====================================================================================
Written by:	 Wayne
Date:		 5.4.15
Description:	 Obfuscates the first and last names
			 
===================================================================================
*/
CREATE FUNCTION [dbo].[fnObfuscateStreet](
	@inputWord VARCHAR(128)
)
RETURNS VARCHAR(128)
AS
BEGIN

	DECLARE @charIndex SMALLINT
	DECLARE @outputWord VARCHAR(128)

	SET @charIndex = 1
	SET @outputWord = ''
	declare @word_list  table (word varchar (50));
	insert @word_list (word)
	   SELECT cast (param as varchar (50))
	   FROM [dbo].[ufn_split_values] (@inputWord, ' ')

	DECLARE @somevar nvarchar(100) -- match your column definition
	DECLARE @counter int
	
	DECLARE StreetCursor CURSOR FOR SELECT word FROM @word_list
	OPEN StreetCursor
	FETCH NEXT FROM StreetCursor INTO @somevar
	--use a cursor to parse
	/*
	   dont want to change numbers to letters or special characters
	*/
	WHILE @@FETCH_STATUS = 0
	BEGIN
	    IF ISNUMERIC(@somevar) =1
		   BEGIN
			   SET @outputWord=@outputWord + ' ' + REVERSE(@somevar)
		   END
	   ELSE IF @somevar in ('AVE', 'DR', 'Drive', 'ST','CT', 'COURT')
		   BEGIN
			   SET @outputWord=@outputWord + ' ' + @somevar
		   END
	    ELSE IF PATINDEX('%[0-9]%', @somevar) > 0
		   BEGIN
			   SET @outputWord=@outputWord + ' ' + @somevar
		   END
	    ELSE IF LEN(@somevar) =1 OR LEN(@somevar) =2 
		   BEGIN
			   SET @outputWord=@outputWord + ' ' + @somevar
		   END
	    ELSE
		  BEGIN
			   SET @outputWord=@outputWord + ' ' + UPPER(left (dbo.fnObfuscate(@somevar),1)) + lower(SUBSTRING(dbo.fnObfuscate(@somevar),2, LEN(dbo.fnObfuscate(@somevar)))) 
		   END
	    FETCH NEXT FROM StreetCursor INTO @somevar
	END
	CLOSE StreetCursor
	DEALLOCATE StreetCursor
	-- done with parsing, return new string
	RETURN REPLACE(@outputWord,'  ',' ')

END
GO
