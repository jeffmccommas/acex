SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		sunil kadimdiwan
-- Create date: 2014-03-11
-- Description:	Return a table with values from the multi_value_string separated by delimiter
-- =============================================
CREATE FUNCTION [dbo].[ufn_split_values] 
(
	-- Add the parameters for the function here
	@multi_value_string nvarchar (4000),
	@delimiter char (1)
)
RETURNS 
@mv_table TABLE 
(
	param nvarchar (4000)
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
	declare @charidx int = 1
	declare @subvalue nvarchar (100)
	while @charidx > 0
	begin
		select @charidx = charindex (@delimiter, @multi_value_string)
		if @charidx > 0
		begin
			select @subvalue = left (@multi_value_string, @charidx - 1)
		end
		else
		begin
			select @subvalue = @multi_value_string
		end
		insert @mv_table (param) values (ltrim (@subvalue))
		select @multi_value_string = RIGHT (@multi_value_string, LEN (@multi_value_string) - @charidx)
		if LEN (@multi_value_string) = 0 BREAK

	end

	RETURN 
END
GO
