SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW  [dbo].[vPossible_faster_T9]
AS

    SELECT s.PremiseKey, s.PremiseId, s.AccountId, t.TreatmentGroup FROM
    (
		  SELECT dp.PremiseKey, dp.PremiseId,dp.AccountId, 
		  ROW_NUMBER() OVER (PARTITION BY fpa.PremiseKey,  fpa.ContentAttributeKey 
		  ORDER BY fpa.EffectiveDate DESC) AS rn
		    FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa
		    INNER JOIN [InsightsDW].[dbo].[DimPremise] dp
		    ON dp.Premiseid = fpa.Premiseid  AND dp.ClientID=fpa.ClientID		
		    WHERE fpa.ContentAttributeKey =  'paperreport.treatmentgroup.enrollmentstatus' 
			AND fpa.Value = 'paperreport.treatmentgroup.enrollmentstatus.enrolled' 
		    --AND fpa.EffectiveDate = 'paperreport.treatmentgroup.enrollmentstatus' 							
			 AND fpa.clientid=101
    ) AS s
    INNER JOIN 
    (
		  SELECT dp.PremiseKey,  fpa.Value AS TreatmentGroup,
		  ROW_NUMBER() OVER (PARTITION BY fpa.PremiseKey,  fpa.ContentAttributeKey 
		  ORDER BY fpa.EffectiveDate DESC) AS rn
		    FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa
		    INNER JOIN [InsightsDW].[dbo].[DimPremise] dp
		    ON dp.Premiseid = fpa.Premiseid  AND dp.ClientID=fpa.ClientID		
		    WHERE fpa.ContentAttributeKey = 'paperreport.treatmentgroup.groupnumber' 
			AND fpa.Value = 'T-9' 
		    --AND fpa.EffectiveDate = 'paperreport.treatmentgroup.enrollmentstatus' 							
			 AND fpa.clientid=101
    ) AS t
    ON s.PremiseKey=t.PremiseKey
    where t.rn=1 AND s.rn=1






GO
