SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =====================================================
-- Written by:	 Wayne
-- Date:		 4/1/2015
-- Description: Used in provcessing of Utility Programs
-- =====================================================
-- Changed by:	 Wayne
-- Date:		 4/6/2015
-- Description: Added Hash joins to speed up
--			 Made a significant difference
-- =====================================================
CREATE VIEW [dbo].[vScgCurrentUtilityProgramEnrollmentWide2]
AS

SELECT dp.PremiseId,t.PremiseKey, dp.AccountId,  CAST(t.EffectiveDate AS DATE) AS EffectiveDate,
      MIN(CASE u.UtilityProgramName WHEN 'My Account' THEN u.UtilityProgramName END) My_Account,
     MIN(CASE u.UtilityProgramName WHEN 'CARE' THEN u.UtilityProgramName END) CARE,
     MIN(CASE u.UtilityProgramName WHEN 'Level Pay Plan' THEN u.UtilityProgramName END) Level_Pay_Plan,
     MIN(CASE u.UtilityProgramName WHEN 'Paperless' THEN u.UtilityProgramName END) Paperless,
	MIN(CASE u.UtilityProgramName WHEN 'Medical' THEN u.UtilityProgramName END)  Medical
 FROM InsightsDW.dbo.FactPremiseAttribute t
INNER HASH JOIN 
(SELECT PremiseKey, AttributeId, MAX(EffectiveDate) AS max_date
FROM InsightsDW.dbo.FactPremiseAttribute
WHERE ClientID = 101
GROUP BY PremiseKey, AttributeId) a
ON a.PremiseKey = t.PremiseKey
AND a.AttributeId = t.AttributeId
AND a.max_date = t.EffectiveDate
INNER HASH JOIN [InsightsDW].[dbo].[DimPremise] dp
  ON dp.PremiseKey = t.PremiseKey  AND dp.ClientID=t.ClientID
  INNER HASH JOIN insightsdw.dbo.DimPremiseAttribute dpa
  ON dpa.AttributeId = t.AttributeId
  INNER HASH JOIN InsightsMetadata.dbo.UtilityPrograms u
  ON dpa.AttributeId=u.ProfileAttributeID
WHERE
							 t.ClientID=101
							 AND t.ContentAttributeKey IN (
							   'budgetbilling.enrollmentstatus',
							 'lowincomeprogram.enrollmentstatus',
							 'medicalprogram.enrollmentstatus',
							 'paperlessbilling.enrollmentstatus',
							 'utilityportal.enrollmentstatus'
							   ) 							 
								AND t.Value LIKE '%.enrolled' 
										 
					 
GROUP BY dp.PremiseId, t.PremiseKey, dp.AccountId,  CAST(t.EffectiveDate AS DATE)


GO
