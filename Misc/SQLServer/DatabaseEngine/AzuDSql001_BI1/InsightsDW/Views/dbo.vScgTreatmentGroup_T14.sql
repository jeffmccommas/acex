SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:	 Wayne
-- Create date: unknown
-- Description: View of those premises that belong to scg T-14
--
-- =============================================
-- Changed by:	 Wayne
-- Change date: 12.9.2014
--	
-- fixed select for channel column subquery		
-- =============================================
CREATE VIEW [dbo].[vScgTreatmentGroup_T14]
AS
-- there are 3 treatment groups for SCG
-- 'T-9' or 'T-11' or 'T-14'
--need to see if in particular group
SELECT dp.*, fpa.Value AS TreatmentGroup,
(
	SELECT fpa7.ContentOptionKey FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa7
	WHERE fpa7.ContentAttributeKey = 'paperreport.channel'
	AND fpa7.PremiseKey=fpa.PremiseKey
	AND fpa7.EffectiveDate = (
								SELECT MAX(fpa8.EffectiveDate) FROM
								[InsightsDW].[dbo].[FactPremiseAttribute] fpa8
								WHERE fpa7.Premiseid=fpa8.Premiseid AND fpa7.ClientID=fpa8.ClientID
								AND fpa8.ContentAttributeKey = 'paperreport.channel'
							 )
) AS Channel
  FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa
  INNER JOIN [InsightsDW].[dbo].[DimPremise] dp
  ON dp.PremiseKey = fpa.PremiseKey
  INNER JOIN [InsightsDW].[dbo].[FactPremiseAttribute] fpa3
  ON fpa.PremiseKey=fpa3.PremiseKey
  WHERE fpa.ContentAttributeKey = 'paperreport.treatmentgroup.groupnumber'
  AND fpa.EffectiveDate = (SELECT MAX(EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa2
							WHERE fpa.PremiseKey=fpa2.PremiseKey
							AND ContentAttributeKey = 'paperreport.treatmentgroup.groupnumber' )
							AND fpa.Value='T-14'
  AND fpa3.Value = 'paperreport.treatmentgroup.enrollmentstatus.enrolled'
  AND fpa3.EffectiveDate = (SELECT MAX(fpa4.EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa4
							WHERE fpa3.PremiseKey=fpa4.PremiseKey
							AND fpa3.ClientID=fpa4.Clientid
							AND fpa4.ContentAttributeKey = 'paperreport.treatmentgroup.enrollmentstatus' )
						
AND fpa.clientid=101




GO
