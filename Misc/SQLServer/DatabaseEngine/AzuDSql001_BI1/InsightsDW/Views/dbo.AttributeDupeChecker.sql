SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
========================================
written  by:	Wayne
description:	check for dupes in [FactPremiseAttribute]
date:			10.3.2014
==========================================
*/
CREATE VIEW [dbo].[AttributeDupeChecker]
AS
 WITH cte AS (
SELECT [PremiseKey]
      ,[OptionKey]
      ,[SourceKey]
      ,[ContentAttributeKey]
      ,[ContentOptionKey]
      ,[AttributeId]
      ,[OptionId]
      ,[Value]
      ,[ClientID]
      ,[PremiseID]
      ,[AccountID]
      ,[SourceID]
      ,[EffectiveDateKey]
      ,[EffectiveDate]
      ,[ETLLogID]
      ,[TrackingID]
      ,[TrackingDate]
     ,ROW_NUMBER() OVER(PARTITION BY [PremiseKey]
      ,[OptionKey]
      ,[SourceKey]
      ,[ContentAttributeKey]
      ,[ContentOptionKey]
      ,[AttributeId]
      ,[OptionId]
      ,[Value]
      ,[ClientID]
      ,[PremiseID]
      ,[AccountID]
      ,[SourceID]
	 ORDER BY [EffectiveDateKey] DESC) AS [rn]
  FROM InsightsDW.[dbo].[FactPremiseAttribute]
  WHERE EffectiveDate > '10-10-2014'
)
SELECT TOP 10 * FROM cte WHERE [rn] > 1



GO
