SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create VIEW [dbo].[InEffectAttributes]
AS

SELECT fpa.PremiseKey, fpa.ContentAttributeKey, fpa.ContentOptionKey, fpa.Value, fpa.EffectiveDate 
 FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa
	where fpa.EffectiveDate = (
								SELECT MAX(fpa8.EffectiveDate) FROM
								[InsightsDW].[dbo].[FactPremiseAttribute] fpa8
								WHERE fpa.PremiseKey=fpa8.PremiseKey 
								AND fpa.ContentAttributeKey=fpa8.ContentAttributeKey
							 )
GO
