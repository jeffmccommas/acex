SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:	 Wayne
-- Create date: unknown
-- Description: View of those premises that belong to scg T-9
-- =============================================
-- Changed by:	 Wayne
-- Change date: 12.9.2014
--	
-- fixed select for channel column subquery		
-- =============================================
CREATE VIEW [dbo].[vScgTreatmentGroup_T9]
AS
--need to see if in particular group
SELECT dp.*, fpa.Value AS TreatmentGroup,
(
	SELECT fpa7.ContentOptionKey FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa7
	WHERE fpa7.ContentAttributeKey = 'paperreport.channel'
	AND fpa7.PremiseKey=fpa.PremiseKey
	AND fpa7.EffectiveDate = (
								SELECT MAX(fpa8.EffectiveDate) FROM
								[InsightsDW].[dbo].[FactPremiseAttribute] fpa8
								WHERE fpa7.Premiseid=fpa8.Premiseid 
								AND fpa7.ClientID=fpa8.ClientID
								AND fpa8.ContentAttributeKey = 'paperreport.channel'
							 )
) AS Channel

  FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa
  INNER JOIN [InsightsDW].[dbo].[DimPremise] dp
  ON dp.Premiseid = fpa.Premiseid  AND dp.ClientID=fpa.ClientID
  INNER JOIN [InsightsDW].[dbo].[FactPremiseAttribute] fpa3
  ON fpa.Premiseid=fpa3.Premiseid AND fpa.ClientID=fpa3.ClientID
  WHERE fpa.ContentAttributeKey = 'paperreport.treatmentgroup.groupnumber'
   AND fpa3.Value = 'paperreport.treatmentgroup.enrollmentstatus.enrolled' 
  AND fpa3.EffectiveDate = (SELECT MAX(fpa4.EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa4
							WHERE fpa3.Premiseid=fpa4.Premiseid 
							AND fpa3.ClientID=fpa4.ClientID
							AND fpa4.ContentAttributeKey = 'paperreport.treatmentgroup.enrollmentstatus' )
  
 
  AND fpa.EffectiveDate = (SELECT MAX(EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa2
							WHERE fpa.Premiseid=fpa2.Premiseid 
							AND fpa.ClientID=fpa2.ClientID
							AND fpa2.ContentAttributeKey = 'paperreport.treatmentgroup.groupnumber' )
AND fpa.Value='T-9'
						
AND fpa.clientid=101







GO
