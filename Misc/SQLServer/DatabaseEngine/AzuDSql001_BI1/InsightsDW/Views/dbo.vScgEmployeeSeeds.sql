SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vScgEmployeeSeeds]
as
SELECT * FROM InsightsDW.dbo.DimPremise dp
WHERE dp.AccountId in (SELECT DISTINCT s.account_id FROM [InsightsBulk].[dbo].[ScgEmpSeeds] s)
GO
