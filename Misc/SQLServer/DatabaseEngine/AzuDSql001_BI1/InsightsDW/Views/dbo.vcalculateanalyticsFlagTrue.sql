SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*

==================================================================

-- Created By:		Wayne
-- Date:			06/08/15
-- Description:	View that shows those who have the disagge attribute set

==================================================================
*/
CREATE VIEW [dbo].[vcalculateanalyticsFlagTrue]
AS
SELECT dp.PremiseKey, fpa.Value AS Value
  FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa
  INNER JOIN [InsightsDW].[dbo].[DimPremise] dp
  ON dp.PremiseKey = fpa.PremiseKey
  WHERE fpa.ContentAttributeKey = 'calculateanalytics' 
  AND fpa.EffectiveDate = (SELECT MAX(EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa2
							WHERE fpa.PremiseKey=fpa2.PremiseKey
							AND ContentAttributeKey = 'calculateanalytics' )
							
AND fpa.Value='calculateanalytics.yes'

UNION 

SELECT dp.PremiseKey, 'calculateanalytics.yes'
FROM
[InsightsDW].[dbo].[DimPremise] dp
LEFT JOIN [InsightsDW].[dbo].[FactPremiseAttribute] fpa
    ON dp.PremiseKey = fpa.PremiseKey AND fpa.ContentAttributeKey = 'calculateanalytics' 
     WHERE fpa.Value IS NULL
    

GO
