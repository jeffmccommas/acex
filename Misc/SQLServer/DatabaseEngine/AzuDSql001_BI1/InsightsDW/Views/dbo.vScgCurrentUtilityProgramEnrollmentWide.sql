SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =====================================================
-- Written by:	 Wayne
-- Date:		 4/1/2015
-- Description: Used in provcessing of Utility Programs
-- =====================================================
-- Changed by:	 Wayne
-- Date:		 4/6/2015
-- Description: Added Hash joins to speed up
--			 Made a significant difference
-- =====================================================
CREATE VIEW [dbo].[vScgCurrentUtilityProgramEnrollmentWide]
AS

SELECT dp.PremiseId, fpa.PremiseKey,c.CustomerId, dp.AccountId,  CAST(fpa.EffectiveDate AS DATE) AS EffectiveDate,
          MIN(CASE u.UtilityProgramName WHEN 'My Account' THEN u.UtilityProgramName END) My_Account,
     MIN(CASE u.UtilityProgramName WHEN 'CARE' THEN u.UtilityProgramName END) CARE,
     MIN(CASE u.UtilityProgramName WHEN 'Level Pay Plan' THEN u.UtilityProgramName END) Level_Pay_Plan,
     MIN(CASE u.UtilityProgramName WHEN 'Paperless' THEN u.UtilityProgramName END) Paperless,
	MIN(CASE u.UtilityProgramName WHEN 'Medical' THEN u.UtilityProgramName END)  Medical
 FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa
 INNER HASH JOIN [InsightsDW].[dbo].[DimPremise] dp
  ON dp.PremiseKey = fpa.PremiseKey  AND dp.ClientID=fpa.ClientID
  INNER HASH JOIN insightsdw.dbo.DimPremiseAttribute dpa
  ON dpa.AttributeId = fpa.AttributeId
  INNER HASH JOIN InsightsMetadata.dbo.UtilityPrograms u
  ON dpa.AttributeId=u.ProfileAttributeID
  INNER HASH JOIN (
						SELECT PremiseKey, CustomerKey, 
						ROW_NUMBER() OVER(PARTITION BY premisekey ORDER BY datecreated DESC) AS rnk 
						FROM
						[InsightsDW].[dbo].[FactCustomerPremise] fcp
					) AS fcp 	
		   ON dp.premisekey=fcp.premisekey AND rnk=1
		   INNER HASH JOIN [InsightsDW].[dbo].[DimCustomer] c	 			
		   ON c.customerkey=fcp.customerkey AND rnk=1
	WHERE fpa.EffectiveDate = (
								SELECT MAX(fpa8.EffectiveDate) FROM
								[InsightsDW].[dbo].[FactPremiseAttribute] fpa8
								WHERE fpa.PremiseKey=fpa8.PremiseKey 
								AND fpa.ContentAttributeKey=fpa8.ContentAttributeKey
							
							 )
							 AND fpa.ClientID=101
							 AND fpa.ContentAttributeKey IN (
							   'budgetbilling.enrollmentstatus',
							 'lowincomeprogram.enrollmentstatus',
							 'medicalprogram.enrollmentstatus',
							 'paperlessbilling.enrollmentstatus',
							 'utilityportal.enrollmentstatus'
							   ) 
							   	AND fpa.Value LIKE '%.enrolled' 							 
					 
GROUP BY dp.PremiseId, fpa.PremiseKey, c.CustomerId, dp.AccountId, CAST(fpa.EffectiveDate AS DATE)



GO
