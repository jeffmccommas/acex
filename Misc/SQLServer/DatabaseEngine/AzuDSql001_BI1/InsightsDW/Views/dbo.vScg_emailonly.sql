SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




create VIEW [dbo].[vScg_emailonly]
AS
--need to see if in particular group
SELECT dp.*, fpa.ContentOptionKey as channel
  FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa
  INNER JOIN [InsightsDW].[dbo].[DimPremise] dp
  ON dp.Premiseid = fpa.Premiseid  and dp.ClientID=fpa.ClientID
  INNER JOIN [InsightsDW].[dbo].[FactPremiseAttribute] fpa3
  ON fpa.Premiseid=fpa3.Premiseid and fpa.ClientID=fpa3.ClientID
  WHERE 
  fpa.ContentAttributeKey = 'paperreport.channel'
  AND fpa.EffectiveDate = (SELECT MAX(EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa2
							WHERE fpa.Premiseid=fpa2.Premiseid and fpa.ClientID=fpa2.ClientID
							AND ContentAttributeKey = 'paperreport.channel' )
  AND fpa.ContentOptionKey='paperreport.channel.emailonly'
---
  AND fpa3.Value = 'paperreport.analysisgroup.enrollmentstatus.enrolled' 
  AND fpa3.EffectiveDate = (SELECT MAX(fpa4.EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa4
							WHERE fpa3.Premiseid=fpa4.Premiseid and fpa3.ClientID=fpa4.ClientID
							AND fpa4.ContentAttributeKey = 'paperreport.analysisgroup.enrollmentstatus' )  						
  AND fpa.clientid=101

GO
