SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =====================================================
-- Written by:	 Wayne
-- Date:		 4/1/2015
-- Description: Used in provcessing of Utility Programs
--
-- Notes:		 4/6/2015
--			 experimented with HASH match on this view
--			 but nested lopps were not a problem
--			 so no changes required	   
-- =====================================================
CREATE VIEW [dbo].[vScgAnalysisGroupUtilityProgramEnrollmentWide]
AS

SELECT dp.PremiseId, fpa.PremiseKey,c.CustomerId, dp.AccountId, 
(SELECT fpa4.value FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa4
						    WHERE fpa.PremiseKey=fpa4.PremiseKey
						    AND fpa4.ContentAttributeKey IN ('paperreport.treatmentgroup.groupnumber',  'paperreport.controlgroup.groupnumber' ) 
						    AND fpa4.EffectiveDate = (SELECT MAX(EffectiveDate) FROM
														 [InsightsDW].[dbo].[FactPremiseAttribute] fpa5
														 WHERE fpa4.Premiseid=fpa5.Premiseid 
														 AND fpa4.ClientID=fpa5.ClientID
														 AND fpa5.ContentAttributeKey =fpa4.ContentAttributeKey
												  ) 
												  ) AS GroupNumber,
	 MIN(CASE u.UtilityProgramName WHEN 'My Account' THEN u.UtilityProgramName END) My_Account,
     MIN(CASE u.UtilityProgramName WHEN 'CARE' THEN u.UtilityProgramName END) CARE,
     MIN(CASE u.UtilityProgramName WHEN 'Level Pay Plan' THEN u.UtilityProgramName END) Level_Pay_Plan,
     MIN(CASE u.UtilityProgramName WHEN 'Paperless' THEN u.UtilityProgramName END) Paperless,
	MIN(CASE u.UtilityProgramName WHEN 'Medical' THEN u.UtilityProgramName END)  Medical
 FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa
 INNER JOIN  [InsightsDW].[dbo].[DimPremise] dp
  ON dp.PremiseKey = fpa.PremiseKey  AND dp.ClientID=fpa.ClientID
  INNER JOIN  insightsdw.dbo.DimPremiseAttribute dpa
  ON dpa.AttributeId = fpa.AttributeId
  INNER JOIN  InsightsMetadata.dbo.UtilityPrograms u
  ON dpa.AttributeId=u.ProfileAttributeID
  INNER JOIN  (
						SELECT PremiseKey, CustomerKey, 
						ROW_NUMBER() OVER(PARTITION BY premisekey ORDER BY datecreated DESC) AS rnk 
						FROM
						[InsightsDW].[dbo].[FactCustomerPremise] fcp
					) AS fcp 	
		   ON dp.premisekey=fcp.premisekey AND rnk=1
		   INNER JOIN  [InsightsDW].[dbo].[DimCustomer] c	 			
		   ON c.customerkey=fcp.customerkey AND rnk=1
     INNER JOIN   [InsightsDW].[dbo].[FactPremiseAttribute] fpa2
	ON fpa.PremiseKey=fpa2.PremiseKey		
    						 
	WHERE fpa.EffectiveDate = (
								SELECT MAX(fpa8.EffectiveDate) FROM
								[InsightsDW].[dbo].[FactPremiseAttribute] fpa8
								WHERE fpa.PremiseKey=fpa8.PremiseKey 
								AND fpa.ContentAttributeKey=fpa8.ContentAttributeKey
							
							 )
							 AND fpa.ClientID=101
							 AND fpa.ContentAttributeKey IN (
							   'budgetbilling.enrollmentstatus',
							 'lowincomeprogram.enrollmentstatus',
							 'medicalprogram.enrollmentstatus',
							 'paperlessbilling.enrollmentstatus',
							 'utilityportal.enrollmentstatus'
							   ) 
							   	AND fpa.Value LIKE '%.enrolled' 
	AND fpa2.Value = 'paperreport.analysisgroup.enrollmentstatus.enrolled' 
	AND fpa2.EffectiveDate = (SELECT MAX(fpa3.EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa3
							WHERE fpa2.PremiseKey=fpa3.PremiseKey
							AND fpa3.ContentAttributeKey = 'paperreport.analysisgroup.enrollmentstatus' )

							
										 
GROUP BY dp.PremiseId, fpa.PremiseKey, c.CustomerId, dp.AccountId




GO
