SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [dbo].[vw_RandNumber]
as
    -- return a random number using the built-in rand() function
    select
        rand() as RandNumber    -- returns a float value between 0 and 1
    ;
GO
