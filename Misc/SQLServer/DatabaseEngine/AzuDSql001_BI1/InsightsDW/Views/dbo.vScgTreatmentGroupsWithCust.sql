SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:	 Wayne
-- Create date: 12.12.2014
-- Description: View of those premises that belong to any scg treatment group with customer id
-- =============================================
CREATE VIEW [dbo].[vScgTreatmentGroupsWithCust]
AS
--need to see if in any group
SELECT dp.PremiseKey, dp.PremiseId, dp.AccountId, dc.CustomerId, fpa.Value AS TreatmentGroup,
(
	SELECT fpa7.ContentOptionKey FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa7
	WHERE fpa7.ContentAttributeKey = 'paperreport.channel'
	AND fpa7.PremiseKey=fpa.PremiseKey
	AND fpa7.EffectiveDate = (
								SELECT MAX(fpa8.EffectiveDate) FROM
								[InsightsDW].[dbo].[FactPremiseAttribute] fpa8
								WHERE fpa7.Premiseid=fpa8.Premiseid 
								AND fpa7.ClientID=fpa8.ClientID
								AND fpa8.ContentAttributeKey = 'paperreport.channel'
							 )
) AS Channel

  FROM    [dbo].[FactPremiseAttribute] fpa
        INNER JOIN [dbo].[DimPremise] dp ON dp.PremiseKey = fpa.PremiseKey
        INNER JOIN	(
		SELECT PremiseKey, CustomerKey, 
		ROW_NUMBER() OVER(PARTITION BY premisekey ORDER BY datecreated DESC) AS rnk 
		FROM
		[InsightsDW].[dbo].[FactCustomerPremise] fcp 
	) AS fcp 	ON dp.premisekey=fcp.premisekey AND rnk=1
	INNER JOIN	[InsightsDW].[dbo].[DimCustomer] dc				
	ON dc.customerkey=fcp.customerkey AND rnk=1
  INNER JOIN [InsightsDW].[dbo].[FactPremiseAttribute] fpa3
  ON fpa.Premiseid=fpa3.Premiseid AND fpa.ClientID=fpa3.ClientID
  WHERE fpa.ContentAttributeKey = 'paperreport.treatmentgroup.groupnumber'
   AND fpa3.Value = 'paperreport.treatmentgroup.enrollmentstatus.enrolled' 
  AND fpa3.EffectiveDate = (SELECT MAX(fpa4.EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa4
							WHERE fpa3.Premiseid=fpa4.Premiseid 
							AND fpa3.ClientID=fpa4.ClientID
							AND fpa4.ContentAttributeKey = 'paperreport.treatmentgroup.enrollmentstatus' )
  
 
  AND fpa.EffectiveDate = (SELECT MAX(EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa2
							WHERE fpa.Premiseid=fpa2.Premiseid 
							AND fpa.ClientID=fpa2.ClientID
							AND fpa2.ContentAttributeKey = 'paperreport.treatmentgroup.groupnumber' )
						
AND fpa.clientid=101









GO
