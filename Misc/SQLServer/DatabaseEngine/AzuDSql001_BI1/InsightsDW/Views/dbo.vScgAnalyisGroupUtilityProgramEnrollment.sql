SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vScgAnalyisGroupUtilityProgramEnrollment]
AS

SELECT dp.PremiseId, fpa.PremiseKey, u.UtilityProgramName, CAST(fpa.EffectiveDate AS DATE) AS EffectiveDate, 
						(SELECT fpa4.value FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa4
						    WHERE fpa.PremiseKey=fpa4.PremiseKey
						    AND fpa4.ContentAttributeKey IN ('paperreport.treatmentgroup.groupnumber',  'paperreport.controlgroup.groupnumber' ) 
						    AND fpa4.EffectiveDate = (SELECT MAX(EffectiveDate) FROM
														 [InsightsDW].[dbo].[FactPremiseAttribute] fpa5
														 WHERE fpa4.Premiseid=fpa5.Premiseid 
														 AND fpa4.ClientID=fpa5.ClientID
														 AND fpa5.ContentAttributeKey =fpa4.ContentAttributeKey
												  ) 
												  ) AS Groupid
 FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa
 INNER JOIN [InsightsDW].[dbo].[DimPremise] dp
  ON dp.PremiseKey = fpa.PremiseKey  AND dp.ClientID=fpa.ClientID
  INNER JOIN insightsdw.dbo.DimPremiseAttribute dpa
  ON dpa.AttributeId = fpa.AttributeId
  INNER JOIN InsightsMetadata.dbo.UtilityPrograms u
  ON dpa.AttributeId=u.ProfileAttributeID
   INNER JOIN  [InsightsDW].[dbo].[FactPremiseAttribute] fpa2
	ON fpa.PremiseKey=fpa2.PremiseKey		
	WHERE fpa.EffectiveDate = (
								SELECT MAX(fpa8.EffectiveDate) FROM
								[InsightsDW].[dbo].[FactPremiseAttribute] fpa8
								WHERE fpa.PremiseKey=fpa8.PremiseKey 
								AND fpa.ContentAttributeKey=fpa8.ContentAttributeKey
								
							 )
							 AND fpa.ClientID=101
							 AND fpa.ContentAttributeKey IN (
							   'budgetbilling.enrollmentstatus',
							 'lowincomeprogram.enrollmentstatus',
							 'medicalprogram.enrollmentstatus',
							 'paperlessbilling.enrollmentstatus',
							 'utilityportal.enrollmentstatus'
							   ) 							 
								AND fpa.Value LIKE '%.enrolled' 
					 
	AND fpa2.Value = 'paperreport.analysisgroup.enrollmentstatus.enrolled' 
	AND fpa2.EffectiveDate = (SELECT MAX(fpa3.EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa3
							WHERE fpa2.PremiseKey=fpa3.PremiseKey
							AND fpa3.ContentAttributeKey = 'paperreport.analysisgroup.enrollmentstatus' )

			





GO
