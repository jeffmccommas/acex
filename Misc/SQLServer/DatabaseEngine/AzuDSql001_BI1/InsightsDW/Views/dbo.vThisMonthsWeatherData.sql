SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
Written By:	 Wayne
Date:		 3.12.2015
Description:	 returns this reporting months weather data
			 using the reporting month and year in
			 configuration table
 ******/
CREATE VIEW [dbo].[vThisMonthsWeatherData]
as
SELECT  [StationID]
      ,[WeatherReadingDate]
      ,[MinTemp]
      ,[MaxTemp]
      ,[AvgTemp]
      ,[AvgWetBulb]
      ,[HeatingDegreeDays]
      ,[CoolingDegreeDays]
      ,[ZipCode]
  FROM [InsightsDW].[dbo].[WeatherData] w
  INNER JOIN dbo.DimDate d
ON d.FullDateAlternateKey=w.WeatherReadingDate
WHERE   d.Month= CAST( 
			 (
				( SELECT Value FROM Insightsmetadata.[cm].[ClientConfiguration]  WHERE ConfigurationKey = 'package.homeenergyreport.reportyear') 
				    + '-' +
				( SELECT Value FROM Insightsmetadata.[cm].[ClientConfiguration]  WHERE ConfigurationKey = 'package.homeenergyreport.reportmonth') 
				    + '-01'
			 )  AS DATE
		  )
GO
