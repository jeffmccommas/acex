SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vScgAnalysisGroupCustomerInfo]
AS
SELECT  dp.ClientId ,
        dc.CustomerId ,
        dp.AccountId ,
        dp.PremiseId 
FROM    [dbo].[FactPremiseAttribute] fpa
        INNER JOIN [dbo].[DimPremise] dp ON dp.PremiseKey = fpa.PremiseKey
        INNER JOIN	(
		SELECT PremiseKey, CustomerKey, 
		ROW_NUMBER() OVER(PARTITION BY premisekey ORDER BY datecreated desc) AS rnk 
		from
		[InsightsDW].[dbo].[FactCustomerPremise] fcp 
	) AS fcp 	on dp.premisekey=fcp.premisekey and rnk=1
	INNER JOIN	[InsightsDW].[dbo].[DimCustomer] dc				
	on dc.customerkey=fcp.customerkey and rnk=1
WHERE   fpa.Value = 'paperreport.analysisgroup.enrollmentstatus.enrolled'
        AND fpa.EffectiveDate = ( SELECT    MAX(fpa2.EffectiveDate)
                                  FROM      [dbo].[FactPremiseAttribute] fpa2
                                  WHERE     fpa.PremiseKey = fpa2.PremiseKey
                                            AND fpa2.ContentAttributeKey = 'paperreport.analysisgroup.enrollmentstatus'
                                )
        AND fpa.clientid = 101
GO
