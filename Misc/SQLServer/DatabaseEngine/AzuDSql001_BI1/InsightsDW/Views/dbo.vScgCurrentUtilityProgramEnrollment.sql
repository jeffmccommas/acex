SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =====================================================
-- Written by:	 Wayne
-- Date:		 4/1/2015
-- Description: Used in provcessing of Utility Programs
-- =====================================================
-- Changed by:	 Wayne
-- Date:		 4/6/2015
-- Description: Added Hash joins to speed up
--			 Made a significant difference
-- =====================================================
CREATE VIEW [dbo].[vScgCurrentUtilityProgramEnrollment]
AS

SELECT dp.PremiseId, fpa.PremiseKey, u.UtilityProgramName, CAST(fpa.EffectiveDate AS DATE) AS EffectiveDate
 FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa
 INNER HASH JOIN [InsightsDW].[dbo].[DimPremise] dp
  ON dp.PremiseKey = fpa.PremiseKey  AND dp.ClientID=fpa.ClientID
  INNER HASH JOIN insightsdw.dbo.DimPremiseAttribute dpa
  ON dpa.AttributeId = fpa.AttributeId
  INNER HASH JOIN InsightsMetadata.dbo.UtilityPrograms u
  ON dpa.AttributeId=u.ProfileAttributeID
	WHERE fpa.EffectiveDate = (
								SELECT MAX(fpa8.EffectiveDate) FROM
								[InsightsDW].[dbo].[FactPremiseAttribute] fpa8
								WHERE fpa.PremiseKey=fpa8.PremiseKey 
								AND fpa.ContentAttributeKey=fpa8.ContentAttributeKey
								
							 )
							 AND fpa.ClientID=101
							 AND fpa.ContentAttributeKey IN (
							   'budgetbilling.enrollmentstatus',
							 'lowincomeprogram.enrollmentstatus',
							 'medicalprogram.enrollmentstatus',
							 'paperlessbilling.enrollmentstatus',
							 'utilityportal.enrollmentstatus'
							   ) 							 
								AND fpa.Value LIKE '%.enrolled' 
					 



GO
