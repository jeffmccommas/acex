SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW  [Export].[vHE_Report_FactActionItem]
AS
SELECT  
		 ps.actionkey
		,dp.PremiseKey
		,eu.EnduseCategoryKey AS EnergyObjectKey
		,ps.Payback
		,NULL AS CO2Savings
		,ps.ROI
		,NULL AS AnnualUsageElectric
		,NULL AS AnnualUsageGas
		,NULL AS AnnualUsageWater
		,NULL AS AnnualUsagePropane
		,NULL AS AnnualUsageOil
		,NULL AS AnnualUsageWood
		,ps.[ElecUsgSavEst] AS UsageSavingsElectric
		,ps.[GasUsgSavEst]	AS UsageSavingsGas
		,ps.[WaterUsgSavEst] AS UsageSavingsWater
		,NULL AS UsageSavingsPropane
		,NULL AS UsageSavingsOil
		,NULL AS UsageSavingsWood
		,ps.[ElecSavEst] AS DollarSavingsElectric
		,ps.[GasSavEst] AS DollarSavingsGas
		,ps.[WaterSavEst] AS DollarSavingsWater
		,NULL AS DollarSavingsPropane
		,NULL AS DollarSavingsOil
		,NULL AS DollarSavingsWood
		,ps.[UpfrontCost] AS Cost
		,ps.newdate AS  DateUpdatedKey
		,1 AS StatusKey
		,seas.winter AS Winter
		,seas.spring AS Spring
		,seas.summer AS Summer
		,seas.fall AS Fall
FROM [InsightsDW].[dbo].[FactMeasure] ps
INNER JOIN InsightsMetadata.[cm].[ClientAction] ca
ON ps.ActionKey = ca.ActionKey
INNER JOIN InsightsMetadata.[cm].[ClientActionAppliance] a
ON  a.ClientActionID = ca.ClientActionID
INNER JOIN InsightsMetadata.cm.ClientEnduseAppliance cea
ON cea.ApplianceKey = a.ApplianceKey
INNER JOIN InsightsMetadata.cm.ClientEnduse eu
ON eu.ClientEnduseID = cea.ClientEnduseID
INNER JOIN InsightsMetadata.cm.TypeEnduseCategory euc
ON euc.EnduseCategoryKey = eu.EnduseCategoryKey
INNER JOIN [dbo].[vSeasonsForMeasures] AS seas
ON  seas.ClientActionID = ca.ClientActionID
INNER JOIN InsightsDW.dbo.DimPremise dp
ON dp.PremiseId=ps.premiseid AND dp.AccountId=ps.accountid

GO
