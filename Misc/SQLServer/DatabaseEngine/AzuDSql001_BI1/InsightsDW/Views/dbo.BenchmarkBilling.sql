SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [dbo].[BenchmarkBilling]
AS
    SELECT  sc.PremiseKey ,
            bb.BillDate ,
            CONVERT(INT, CONVERT(VARCHAR(10), DATEADD(DAY, -1 * bb.BillDays,
                                                      bb.BillDate), 112)) AS BillPeriodStartDateKey ,
            CONVERT(INT, CONVERT(VARCHAR(10), bb.BillDate, 112)) AS BillPeriodEndDateKey ,
            fb.CommodityId AS CommodityKey ,
            fb.BillPeriodTypeKey ,
            fb.TotalUsage AS TotalUnits ,
            fb.CostOfUsage AS TotalCost ,
            bb.BillDays
    FROM    [InsightsDW].[dbo].[BenchmarkBills] bb
            INNER JOIN InsightsDW.dbo.DimServiceContract sc ON sc.PremiseKey = bb.PremiseKey
            INNER JOIN InsightsDW.dbo.FactServicePointBilling fb ON fb.ServiceContractKey = sc.ServiceContractKey
                                                              AND fb.EndDate = bb.EndDate;




GO
