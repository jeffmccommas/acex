SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vScgWithPools]
AS
SELECT dp.*, fpa.Value AS PoolCount
  FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa
  INNER JOIN [InsightsDW].[dbo].[DimPremise] dp
  ON dp.PremiseKey = fpa.PremiseKey
  WHERE fpa.ContentAttributeKey = 'pool.count' 
  AND fpa.EffectiveDate = (SELECT MAX(EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa2
							WHERE fpa.PremiseKey=fpa2.PremiseKey
							AND ContentAttributeKey = 'pool.count' )
							AND value >0
AND dp.clientid=101
GO
