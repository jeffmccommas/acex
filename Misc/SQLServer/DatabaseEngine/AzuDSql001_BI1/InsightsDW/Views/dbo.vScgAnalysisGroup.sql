SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vScgAnalysisGroup]
AS
--shows everyone in analysis group, thier channel and group
SELECT dp.*, REPLACE((
	SELECT fpa7.ContentOptionKey 	
	FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa7
	WHERE fpa7.ContentAttributeKey = 'paperreport.channel'
	AND fpa7.PremiseKey=fpa.PremiseKey
	AND fpa7.EffectiveDate = (
								SELECT MAX(fpa8.EffectiveDate) FROM
								[InsightsDW].[dbo].[FactPremiseAttribute] fpa8
								WHERE fpa7.Premiseid=fpa8.Premiseid 
								AND fpa7.ClientID=fpa8.ClientID
								AND fpa8.ContentAttributeKey = 'paperreport.channel'
							 )
),'paperreport.channel.','') AS Channel,

COALESCE(
 (
		   SELECT fpa9.Value 
		FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa9  --use 9 to get group number
  
		INNER JOIN [InsightsDW].[dbo].[FactPremiseAttribute] fpa3 --use 3 to get status
		ON fpa9.PremiseKey=fpa3.PremiseKey AND fpa9.ClientID=fpa3.ClientID

		WHERE 
		fpa9.PremiseKey=fpa.premisekey AND fpa.ClientID=fpa9.ClientID --inside premis must be same AS outside
		 AND fpa9.ContentAttributeKey = 'paperreport.controlgroup.groupnumber'
		 AND fpa3.Value = 'paperreport.controlgroup.enrollmentstatus.enrolled' 
		AND fpa3.EffectiveDate = (SELECT MAX(fpa4.EffectiveDate) FROM
								   [InsightsDW].[dbo].[FactPremiseAttribute] fpa4
								   WHERE fpa3.PremiseKey=fpa4.PremiseKey 
								   AND fpa3.ClientID=fpa4.ClientID
								   AND fpa4.ContentAttributeKey = 'paperreport.controlgroup.enrollmentstatus' )
  
 
		AND fpa9.EffectiveDate = (SELECT MAX(EffectiveDate) FROM
								   [InsightsDW].[dbo].[FactPremiseAttribute] fpa2
								   WHERE fpa9.PremiseKey=fpa2.PremiseKey 
								   AND fpa9.ClientID=fpa2.ClientID
								   AND fpa2.ContentAttributeKey = 'paperreport.controlgroup.groupnumber' )
						

							 
	   ) ,
	   (
		   SELECT fpa9.Value 
		FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa9  --use 9 to get group number
  
		INNER JOIN [InsightsDW].[dbo].[FactPremiseAttribute] fpa3 --use 3 to get status
		ON fpa9.PremiseKey=fpa3.PremiseKey AND fpa9.ClientID=fpa3.ClientID

		WHERE 
		fpa9.PremiseKey=fpa.premisekey AND fpa.ClientID=fpa9.ClientID --inside premis must be same AS outside
		 AND fpa9.ContentAttributeKey = 'paperreport.treatmentgroup.groupnumber'
		 AND fpa3.Value = 'paperreport.treatmentgroup.enrollmentstatus.enrolled' 
		AND fpa3.EffectiveDate = (SELECT MAX(fpa4.EffectiveDate) FROM
								   [InsightsDW].[dbo].[FactPremiseAttribute] fpa4
								   WHERE fpa3.PremiseKey=fpa4.PremiseKey 
								   AND fpa3.ClientID=fpa4.ClientID
								   AND fpa4.ContentAttributeKey = 'paperreport.treatmentgroup.enrollmentstatus' )
  
 
		AND fpa9.EffectiveDate = (SELECT MAX(EffectiveDate) FROM
								   [InsightsDW].[dbo].[FactPremiseAttribute] fpa2
								   WHERE fpa9.PremiseKey=fpa2.PremiseKey 
								   AND fpa9.ClientID=fpa2.ClientID
								   AND fpa2.ContentAttributeKey = 'paperreport.treatmentgroup.groupnumber' )
						

							 
) 
)
AS GroupId
  FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa  --fpa IS main table
  INNER JOIN [InsightsDW].[dbo].[DimPremise] dp
  ON dp.PremiseKey = fpa.PremiseKey
  WHERE fpa.Value = 'paperreport.analysisgroup.enrollmentstatus.enrolled' 
  AND fpa.EffectiveDate = (SELECT MAX(fpa2.EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa2
							WHERE fpa.PremiseKey=fpa2.PremiseKey
							AND fpa2.ContentAttributeKey = 'paperreport.analysisgroup.enrollmentstatus' )
					
AND fpa.clientid=101



GO
