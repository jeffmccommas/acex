SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =====================================================
-- Written by:	 Wayne
-- Date:		 4/1/2015
-- Description: Used in provcessing of Utility Programs
-- =====================================================
-- Changed by:	 Wayne
-- Date:		 4/6/2015
-- Description: Added Hash joins to speed up
--			 Made a significant difference
-- =====================================================
CREATE VIEW [dbo].[vScgCurrentUtilityProgramEnrollmentAttribsWide]
AS

SELECT dp.PremiseId,t.PremiseKey, dp.AccountId,  CAST(t.EffectiveDate AS DATE) AS EffectiveDate,
      MIN(CASE dpa.CMSAttributeKey WHEN 'utilityportal.enrollmentstatus' THEN dpa.CMSAttributeKey END) My_Account,
     MIN(CASE dpa.CMSAttributeKey WHEN 'lowincomeprogram.enrollmentstatus' THEN dpa.CMSAttributeKey END) CARE,
     MIN(CASE dpa.CMSAttributeKey WHEN 'budgetbilling.enrollmentstatus' THEN dpa.CMSAttributeKey END) Level_Pay_Plan,
     MIN(CASE dpa.CMSAttributeKey WHEN 'paperlessbilling.enrollmentstatus' THEN dpa.CMSAttributeKey END) Paperless,
	MIN(CASE dpa.CMSAttributeKey WHEN 'medicalprogram.enrollmentstatus' THEN dpa.CMSAttributeKey END)  Medical
 FROM InsightsDW.dbo.FactPremiseAttribute t
INNER HASH JOIN 
(SELECT PremiseKey, AttributeId, MAX(EffectiveDate) AS max_date
FROM InsightsDW.dbo.FactPremiseAttribute
WHERE ClientID = 101
GROUP BY PremiseKey, AttributeId) a
ON a.PremiseKey = t.PremiseKey
AND a.AttributeId = t.AttributeId
AND a.max_date = t.EffectiveDate
INNER HASH JOIN [InsightsDW].[dbo].[DimPremise] dp
  ON dp.PremiseKey = t.PremiseKey  
  INNER HASH JOIN insightsdw.dbo.DimPremiseAttribute dpa
  ON dpa.AttributeId = t.AttributeId
WHERE
							 t.ClientID=101
							 AND t.ContentAttributeKey IN (
							   'budgetbilling.enrollmentstatus',
							 'lowincomeprogram.enrollmentstatus',
							 'medicalprogram.enrollmentstatus',
							 'paperlessbilling.enrollmentstatus',
							 'utilityportal.enrollmentstatus'
							   ) 							 
								AND t.Value IN (
								'budgetbilling.enrollmentstatus.enrolled',
							 'lowincomeprogram.enrollmentstatus.enrolled',
							 'medicalprogram.enrollmentstatus.enrolled',
							 'paperlessbilling.enrollmentstatus.enrolled',
							 'utilityportal.enrollmentstatus.enrolled')
										 
					 
GROUP BY dp.PremiseId, t.PremiseKey, dp.AccountId,  CAST(t.EffectiveDate AS DATE)





GO
