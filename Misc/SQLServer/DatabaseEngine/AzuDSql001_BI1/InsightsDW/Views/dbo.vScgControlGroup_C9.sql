SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vScgControlGroup_C9]
AS
-- there are 2 control groups for SCG
-- 'C-8' OR cell='C-9'
--need to see if in particular group
SELECT dp.*, fpa.Value AS TreatmentGroup
  FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa
  INNER JOIN [InsightsDW].[dbo].[DimPremise] dp
  ON dp.PremiseKey = fpa.PremiseKey
  INNER JOIN [InsightsDW].[dbo].[FactPremiseAttribute] fpa3
  ON fpa.PremiseKey=fpa3.PremiseKey
  WHERE fpa.ContentAttributeKey = 'paperreport.controlgroup.groupnumber'
  AND fpa3.EffectiveDate = (SELECT MAX(fpa4.EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa4
							WHERE fpa3.PremiseKey=fpa4.PremiseKey
							AND fpa4.ContentAttributeKey = 'paperreport.controlgroup.enrollmentstatus' )
  
  AND fpa3.Value = 'paperreport.controlgroup.enrollmentstatus.enrolled' 
  AND fpa.EffectiveDate = (SELECT MAX(EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa2
							WHERE fpa.PremiseKey=fpa2.PremiseKey
							AND ContentAttributeKey = 'paperreport.controlgroup.groupnumber' )
							AND fpa.Value='C-9'
						
AND fpa.clientid=101


GO
