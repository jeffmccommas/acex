SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =====================================================
-- Written by:	 Wayne
-- Date:		 4/1/2015
-- Description: Used in provcessing of Utility Programs
-- =====================================================
-- Changed by:	 Wayne
-- Date:		 4/6/2015
-- Description: Added Hash joins to speed up
--			 Made a significant difference
-- =====================================================
CREATE VIEW [dbo].[vScgCurrentUtilityProgramEnrollmentAttribs]
AS  
SELECT dp.PremiseId, t.PremiseKey, dpa.CMSAttributeKey, CAST(t.EffectiveDate AS DATE) AS EffectiveDate
FROM InsightsDW.dbo.FactPremiseAttribute t
INNER HASH JOIN 
(SELECT PremiseKey, AttributeId, MAX(EffectiveDate) AS max_date
FROM InsightsDW.dbo.FactPremiseAttribute
WHERE ClientID = 101
GROUP BY PremiseKey, AttributeId) a
ON a.PremiseKey = t.PremiseKey
AND a.AttributeId = t.AttributeId
AND a.max_date = t.EffectiveDate
INNER HASH JOIN [InsightsDW].[dbo].[DimPremise] dp
  ON dp.PremiseKey = t.PremiseKey  AND dp.ClientID=t.ClientID
  INNER HASH JOIN insightsdw.dbo.DimPremiseAttribute dpa
  ON dpa.AttributeId = t.AttributeId
  INNER HASH JOIN InsightsMetadata.dbo.UtilityPrograms u
  ON dpa.AttributeId=u.ProfileAttributeID
WHERE
							 t.ClientID=101
							 AND t.ContentAttributeKey IN (
							   'budgetbilling.enrollmentstatus',
							 'lowincomeprogram.enrollmentstatus',
							 'medicalprogram.enrollmentstatus',
							 'paperlessbilling.enrollmentstatus',
							 'utilityportal.enrollmentstatus'
							   ) 							 
								AND t.Value LIKE '%.enrolled' 
					 






GO
