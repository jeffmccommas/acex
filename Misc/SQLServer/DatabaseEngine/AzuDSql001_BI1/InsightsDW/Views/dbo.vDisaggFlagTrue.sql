SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*

==================================================================

-- Created By:		Wayne
-- Date:			06/08/15
-- Description:	View that shows those who have the disagge attribute set

==================================================================
*/
CREATE VIEW [dbo].[vDisaggFlagTrue]
AS
SELECT dp.*, fpa.Value AS PoolCount
  FROM [InsightsDW].[dbo].[FactPremiseAttribute] fpa
  INNER JOIN [InsightsDW].[dbo].[DimPremise] dp
  ON dp.PremiseKey = fpa.PremiseKey
  WHERE fpa.ContentAttributeKey = 'calculatebilldisagg' 
  AND fpa.EffectiveDate = (SELECT MAX(EffectiveDate) FROM
							[InsightsDW].[dbo].[FactPremiseAttribute] fpa2
							WHERE fpa.PremiseKey=fpa2.PremiseKey
							AND ContentAttributeKey = 'calculatebilldisagg' )
							
AND fpa.Value='calculatebilldisagg.yes'



GO
