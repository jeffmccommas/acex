SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[BenchmarksBilling]
AS
    SELECT  ClientId ,
            PremiseKey ,
            BillPeriodStartDateKey ,
            BillPeriodEndDateKey ,
            BillDays ,
            DATEADD(DAY, -1 * ( ( BillDays - 1 ) / 2 ), EndDate) AS BillDate
    FROM    dbo.FactBilling

GO
