SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vEnergyObjectCategorySavings]
as
	SELECT 
	distinct
	(SELECT deo.EnergyObjectDesc FROM dimenergyobject deo WHERE deo.[EnergyObjectDesc]=(SELECT TOP 1 apl.ApplianceKey FROM InsightsMetadata.cm.ClientAppliance apl 
						INNER JOIN [InsightsMetadata].[cm].ClientEnduseAppliance cae
						ON cae.ApplianceKey = apl.ApplianceKey
						INNER JOIN [InsightsMetadata].[cm].ClientEnduse eu
						ON eu.ClientEnduseID = cae.ClientEnduseID
						where apl.ClientApplianceID=app.ClientApplianceID 
						) 
						AND deo.[EnergyObjectCategory]=(SELECT TOP 1 eu.endusekey FROM InsightsMetadata.cm.ClientAppliance apl 
						INNER JOIN [InsightsMetadata].[cm].ClientEnduseAppliance cae
						ON cae.ApplianceKey = apl.ApplianceKey
						INNER JOIN [InsightsMetadata].[cm].ClientEnduse eu
						ON eu.ClientEnduseID = cae.ClientEnduseID
						where apl.ClientApplianceID=app.ClientApplianceID 
						)
						) AS energyobjectkey,
	(SELECT TOP 1 eu.endusekey FROM InsightsMetadata.cm.ClientAppliance apl 
						INNER JOIN [InsightsMetadata].[cm].ClientEnduseAppliance cae
						ON cae.ApplianceKey = apl.ApplianceKey
						INNER JOIN [InsightsMetadata].[cm].ClientEnduse eu
						ON eu.ClientEnduseID = cae.ClientEnduseID
						where apl.ClientApplianceID=app.ClientApplianceID 
						) AS EnergyObjectCategory

, ps.ActionKey,dp.ClientId,dp.PremiseKey, dp.PremiseId,ps.Cost, ps.AnnualSavingsEstimate, ps.AnnualUsageSavingsEstimate, rank
FROM InsightsDW.dbo.PremiseSavings ps 
INNER join    InsightsMetadata.[cm].[ClientAction] ca
ON ca.ActionKey=ps.actionkey 
INNER JOIN [InsightsMetadata].[cm].[ClientActionCondition] cac
ON cac.ClientActionID=ca.ClientActionID
INNER JOIN  [InsightsMetadata].[cm].[ClientCondition] cc
ON cc.ConditionKey=cac.conditionkey
INNER JOIN [InsightsMetadata].[cm].[ClientProfileAttributeProfileOption] capo
ON capo.ProfileOptionKey = cc.ProfileOptionKey
INNER JOIN   InsightsMetadata.[cm].[ClientActionSavingsCondition] casc
ON casc.ClientActionSavingsID = ps.ClientActionSavingsID
INNER JOIN [InsightsMetadata].[cm].[ClientProfileAttribute] cpa
ON cpa.ClientProfileAttributeID = capo.ClientProfileAttributeID
INNER JOIN [InsightsMetadata].[cm].[ClientApplianceProfileAttribute] app
ON cpa.ProfileAttributeKey=app.ProfileAttributeKey
INNER JOIN dbo.DimPremise dp ON ps.Premisekey=dp.PremiseKey

--
--ORDER BY Premisekey, Rank
GO
