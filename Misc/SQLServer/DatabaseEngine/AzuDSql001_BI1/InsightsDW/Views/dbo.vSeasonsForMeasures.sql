SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =====================================================
-- Written by:	Wayne
-- Date:		7/14/2015
-- Description: shows for which seasons a action is applicable
--
-- Notes:	assumes that if there are no entries in InsightsMetadata.cm.ClientSeason
--			the applicable to all seasons		
-- =====================================================
CREATE VIEW [dbo].[vSeasonsForMeasures]
AS
SELECT ca.ClientActionID, ca.ActionKey, 
	 COALESCE(MIN(CASE WHEN seas.NameKey = 'season.all.name' OR seas.NameKey  = 'season.spring.name'	OR seas.NameKey IS NULL THEN 1 END),0) Spring,
	 COALESCE(MIN(CASE WHEN seas.NameKey = 'season.all.name' OR seas.NameKey  = 'season.summer.name'	OR seas.NameKey IS NULL THEN 1 END),0) Summer,
	 COALESCE(MIN(CASE WHEN seas.NameKey = 'season.all.name' OR seas.NameKey  = 'season.fall.name'		OR seas.NameKey IS NULL THEN 1 END),0) Fall,
	 COALESCE(MIN(CASE WHEN seas.NameKey = 'season.all.name' OR seas.NameKey  = 'season.winter.name'	OR seas.NameKey IS NULL THEN 1 END),0) Winter	 
 FROM 	 InsightsMetadata.[cm].[ClientAction] ca
left JOIN  (SELECT cas.ClientActionID, cs.NameKey FROM InsightsMetadata.cm.ClientActionSeason cas
INNER JOIN InsightsMetadata.cm.ClientSeason cs
ON cs.SeasonKey = cas.SeasonKey) AS seas
ON  seas.ClientActionID = ca.ClientActionID													 
GROUP BY ca.ClientActionID, ca.ActionKey
GO
