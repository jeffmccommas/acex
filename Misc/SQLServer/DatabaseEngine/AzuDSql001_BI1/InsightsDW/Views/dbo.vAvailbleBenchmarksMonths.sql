SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Wayne
-- Create date: 9.23.2014
-- Description:	show for which months that
-- benchmarks that have been run in the DW
-- =============================================
CREATE view [dbo].[vAvailbleBenchmarksMonths]
as
SELECT DISTINCT fb.ClientId , d.month AS BenchmarkMonth FROM InsightsDW.[dbo].[FactBilling] fb
INNER JOIN InsightsDW.[dbo].DimDate d
ON fb.BillPeriodEndDateKey =d.DateKey		
Inner join
InsightsDW.dbo.Benchmarks b
ON d.month =b.billmonth
GO
