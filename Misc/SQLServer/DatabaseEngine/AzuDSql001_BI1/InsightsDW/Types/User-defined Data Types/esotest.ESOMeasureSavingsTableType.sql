CREATE TYPE [esotest].[ESOMeasureSavingsTableType] AS TABLE
(
[clientid] [int] NOT NULL,
[customerid] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[account] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[premise] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[premisekey] [int] NOT NULL,
[esomeasureid] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[annualbill] [decimal] (10, 2) NULL,
[houseTotalArea] [decimal] (10, 2) NULL,
[housepeople] [float] NULL,
[heatsystemStyle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[waterheaterfuel] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[heatsystemfuel] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[poolcount] [decimal] (10, 2) NULL,
[hottubcount] [decimal] (10, 2) NULL,
[ActionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[esosavings] [decimal] (10, 2) NOT NULL,
[esocost] [decimal] (10, 2) NOT NULL,
[esoid] [int] NULL,
PRIMARY KEY CLUSTERED  ([clientid], [customerid], [account], [premise], [esomeasureid])
)
GO
