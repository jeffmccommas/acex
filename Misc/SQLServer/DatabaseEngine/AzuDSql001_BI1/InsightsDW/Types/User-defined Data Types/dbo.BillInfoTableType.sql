CREATE TYPE [dbo].[BillInfoTableType] AS TABLE
(
[StartDateKey] [int] NOT NULL,
[EndDateKey] [int] NOT NULL,
[FuelKey] [tinyint] NOT NULL,
[BillPeriodTypeKey] [tinyint] NOT NULL
)
GO
