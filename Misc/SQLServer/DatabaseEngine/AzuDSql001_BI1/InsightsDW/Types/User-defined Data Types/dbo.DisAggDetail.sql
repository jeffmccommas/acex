CREATE TYPE [dbo].[DisAggDetail] AS TABLE
(
[ClientID] [int] NOT NULL,
[CustomerId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BillDate] [datetime] NOT NULL,
[EndUseKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ApplianceKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommodityKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PeriodId] [tinyint] NOT NULL,
[Usage] [decimal] (18, 6) NOT NULL,
[Cost] [decimal] (18, 6) NOT NULL,
[UomKey] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyKey] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Confidence] [int] NULL,
[ReconciliationRatio] [decimal] (18, 6) NULL,
[StatusID] [int] NULL,
[NewDate] [datetime] NOT NULL
)
GO
