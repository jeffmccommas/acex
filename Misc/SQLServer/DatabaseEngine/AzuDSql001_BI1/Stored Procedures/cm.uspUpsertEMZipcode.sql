

/****** Object:  StoredProcedure [cm].[uspUpsertEMZipcode]    Script Date: 9/17/2015 6:13:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if exists (select * from sysobjects where type = 'p' and name = 'uspUpsertEMZipcode')
drop procedure [cm].[uspUpsertEMZipcode]
go

 CREATE PROCEDURE [cm].[uspUpsertEMZipcode]

-- =================================================================================================================
--   Title:         uspUpsertEMZipcode
--   Author:		Rakesh Jamla
--   Create date:   06/22/2015
--   Description:	This table merges the Zipcode data from temp table into EMZipcode table for use by BI
--   Revision History: 07/23/2015 ClientID column added by RJamla
--						09/17/2015 ECormier - update for PaaS db's 
-- ======================================================================================================================
AS

/*  This query added in SSIS Package to create temp table  used in this procedure  
-- CTE to get UNIQUE ZipCodes
  WITH CTE1 AS (
  SELECT [ZipCode]
      ,[City]
      ,[State]
      ,[CountryCode]
      ,[SolarRegion]
      ,[TemperatureRegion]
      ,[ApplianceRegion]
      ,[StationIdDaily]
	  ,ClientID
	  ,ROW_NUMBER() OVER(PARTITION BY [ZipCode],ClientID, [StationIdDaily]   ORDER BY Zipcode) AS rank
    FROM [InsightsMetadata].dbo.tmpZipcodeMapping )
	SELECT [ZipCode]
		  ,[City]
		  ,[State]
		  ,[CountryCode]
		  ,[SolarRegion]
		  ,[TemperatureRegion]
		  ,[ApplianceRegion]
		  ,[StationIdDaily]
		  ,ClientID
		  INTO #tmp1
	  FROM CTE1 
	 WHERE rank = 1  */


	--Synchronize the target table with refreshed data from source table
	MERGE [cm].[EMZipcode] AS TARGET
	USING [dbo].[tmpZipcodeMapping] AS SOURCE 
	ON TARGET.ZipCode = SOURCE.ZipCode 
	--AND Target.[ClientID] = Source.[ClientID]
	--AND Target.[ApplianceRegionId] = Source.[ApplianceRegion]
	--AND Target.[SolarRegionId] != Source.[SolarRegion]
    AND TARGET.[StationIdDaily] = SOURCE.[StationIdDaily]
	
	
	
	--When records are matched, update the records if there is any change
	WHEN MATCHED  AND Target.[StationIdDaily] != Source.[StationIDDaily]  THEN 
	UPDATE SET Target.[ZipCode] = Source.[ZipCode]
			  ,Target.[TemperatureRegionID] = Source.[TemperatureRegion]
			  ,Target.[ApplianceRegionId] = Source.[ApplianceRegion]
			  ,Target.[SolarRegionId] = Source.[SolarRegion]
			  ,Target.[State] = Source.[State]
			  ,Target.[City] = Source.[City]
			  ,Target.[CountryCode] = Source.[CountryCode]
			  ,Target.[ClientID] = Source.ClientID
			  ,Target.[StationIdDaily] = Source.[StationIDDaily]
			
	--When no records are matched, insert the incoming records from source table to target table
	WHEN NOT MATCHED BY TARGET THEN 
	INSERT ([ZipCode]
		  ,[TemperatureRegionId]
		  ,[ApplianceRegionId]
		  ,[SolarRegionId]
		  ,[State]
		  ,[City]
		  ,[CountryCode]
		  ,[ClientID]
		  ,[StationIdDaily]) 
	VALUES (   Source.[ZipCode]
			  ,Source.[TemperatureRegion]
			  ,Source.[ApplianceRegion]
			  ,Source.[SolarRegion]
			  ,Source.[State]
			  ,Source.[City]
			  ,Source.[CountryCode]
			  ,Source.ClientID
			  ,Source.[StationIdDaily])
	
	
	--$action specifies a column of type nvarchar(10) in the OUTPUT clause that returns one of two 
	--values for each row: 'INSERT', 'UPDATE' according to the action that was performed on that row
	OUTPUT $action, 
	INSERTED.ZipCode AS ZipCode, 
	INSERTED.[TemperatureRegionId] AS [TemperatureRegionId], 
	INSERTED.[ApplianceRegionId] AS ApplianceRegionId; 
	SELECT @@ROWCOUNT, GETDATE();

	

GO


