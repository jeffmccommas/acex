

/****** Object:  StoredProcedure [cm].[uspUpsertEMDailyWeatherDetail]    Script Date: 9/17/2015 6:05:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if exists (select * from sysobjects where type = 'p' and name = 'uspUpsertEMDailyWeatherDetail')
drop procedure [cm].[uspUpsertEMDailyWeatherDetail]
go
CREATE PROCEDURE [cm].[uspUpsertEMDailyWeatherDetail]

-- =================================================================================================================
--   Title:         uspUpsertEMDailyWeatherDetail
--   Author:		Rakesh Jamla
--   Create date:   06/18/2015
--   Description:	This table merges the Weather data from temp table into EMDailyWeatherDetail table for use by BI
--   Revision History: 7/6/2015 [StationID] and [WeatherReadingDate] columns are not unique. USED CTE for Merge with temp table
--						09/17/2015 ECormier - update for PaaS db's 
--						09/23/2015 ECormier - modify merge to include updating if any column data has changed, not just mintemp and maxtemp 
--											- modify CTE to insert into #tmp1 to make DataIsDerived and Comment = null if value is empty
-- ======================================================================================================================
AS


	     WITH CTE1 AS (
  SELECT   [StationID]
		  ,[WeatherReadingDate]
		  ,[MinTemp]
		  ,[MaxTemp]
		  ,[AvgTemp]
		  ,[AvgWetBulb]
		  ,[HeatingDegreeDays]
		  ,[CoolingDegreeDays]
		  ,[DataIsDerived]
		  ,[Comment]
	  ,ROW_NUMBER() OVER(PARTITION BY [StationID], [WeatherReadingDate]   ORDER BY [StationID]) AS rank
    FROM [dbo].[tmpEMDailyWeatherDetail] )
	SELECT [StationID]
		  ,[WeatherReadingDate]
		  ,[MinTemp]
		  ,[MaxTemp]
		  ,[AvgTemp]
		  ,[AvgWetBulb]
		  ,[HeatingDegreeDays]
		  ,[CoolingDegreeDays]
		  ,case when len([DataIsDerived]) = 0 then null else DataIsDerived end as DataIsDerived
		  ,case when len([Comment]) = 0 then null else Comment end as Comment
		  INTO #tmp1
	  FROM CTE1 
	 WHERE rank = 1

	 --Synchronize the target table with refreshed data from source table
	MERGE [cm].[EMDailyWeatherDetail] AS TARGET
	USING #tmp1 AS SOURCE 
	ON TARGET.[StationID] = SOURCE.[StationID] 
    AND TARGET.[WeatherReadingDate] = SOURCE.[WeatherReadingDate]
	
	--When records are matched, update the records if there is any change
	WHEN MATCHED AND Target.[MinTemp] != Source.[MinTemp] OR Target.[MaxTemp] != Source.[MaxTemp] 
				or Target.[AvgTemp] != Source.[AvgTemp] 
				or Target.[AvgWetBulb] != Source.[AvgWetBulb]
				or Target.[HeatingDegreeDays] != Source.[HeatingDegreeDays]
				or Target.[CoolingDegreeDays] != Source.[CoolingDegreeDays]
				or Target.[DataIsDerived] != Source.[DataIsDerived]
				or Target.[Comment] != Source.[Comment]
	THEN 
	UPDATE SET Target.[StationID] = Source.[StationID]
			  ,Target.[WeatherReadingDate] = Source.[WeatherReadingDate]
			  ,Target.[MinTemp] = Source.[MinTemp]
			  ,Target.[MaxTemp] = Source.[MaxTemp]
			  ,Target.[AvgTemp] = Source.[AvgTemp]
			  ,Target.[AvgWetBulb] = Source.[AvgWetBulb]
			  ,Target.[HeatingDegreeDays] = Source.[HeatingDegreeDays]
			  ,Target.[CoolingDegreeDays] = Source.[CoolingDegreeDays]
			  ,Target.[DataIsDerived] = Source.[DataIsDerived]
			  ,Target.[Comment] = Source.[Comment]

	--When no records are matched, insert the incoming records from source table to target table
	WHEN NOT MATCHED BY TARGET THEN 
	INSERT ([StationID],
			[WeatherReadingDate],
			[MinTemp],
			[MaxTemp],
			[AvgTemp],
			[AvgWetBulb],
			[HeatingDegreeDays],
			[CoolingDegreeDays],
			[DataIsDerived],
			[Comment]) 
	VALUES (Source.[StationID]
			,Source.[WeatherReadingDate]
			,Source.[MinTemp]
			,Source.[MaxTemp]
			,Source.[AvgTemp]
			,Source.[AvgWetBulb]
			,Source.[HeatingDegreeDays]
			,Source.[CoolingDegreeDays]
			,Source.[DataIsDerived]
			,Source.[Comment])
	
	
	--$action specifies a column of type nvarchar(10) in the OUTPUT clause that returns one of two 
	--values for each row: 'INSERT', 'UPDATE' according to the action that was performed on that row
	OUTPUT $action, 
	INSERTED.StationID AS SourceStationID, 
	INSERTED.WeatherReadingDate AS SourceWeatherReadingDate, 
	INSERTED.MinTemp AS SourceMinTemp; 
	SELECT @@ROWCOUNT, GETDATE();

		DROP TABLE #tmp1
GO


