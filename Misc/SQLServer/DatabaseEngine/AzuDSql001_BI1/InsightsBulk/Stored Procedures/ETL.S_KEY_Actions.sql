SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Ubaid Tariq
-- Create date: 12/16/2014
-- Description:	Returns keys related to actions
-- =============================================
CREATE PROCEDURE [ETL].[S_KEY_Actions] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT  ClientID ,
                AccountID ,
                PremiseID ,
                CustomerID ,
                ActionKey ,
                SubActionKey ,
                SourceId
        FROM    ( SELECT    ClientID ,
                            AccountID ,
                            PremiseID ,
                            CustomerID ,
                            ActionKey ,
                            SubActionKey ,
                            SourceId ,
                            ROW_NUMBER() OVER ( PARTITION BY ClientId,
                                                PremiseId, AccountId,
                                                ActionKey, SubActionKey ORDER BY TrackingDate DESC ) AS SortId
                  FROM      Holding.ActionItem WITH ( NOLOCK )
                  WHERE     ClientId = @ClientID
                            AND PremiseID IS NOT NULL
                            AND AccountID IS NOT NULL
                            AND CustomerID IS NOT NULL
                            AND LEN(LTRIM(RTRIM(CustomerID))) > 0
                            AND ActionKey IS NOT NULL
                ) b
        WHERE   SortId = 1
        ORDER BY ClientId ,
                AccountId ,
                PremiseId ,
                CustomerID 

    END







GO
