SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/16/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[S_HLD_Client]
				 @ClientID INT,
				 @RunMelissaData INT = NULL OUTPUT,
				 @RunBenchmark INT  = NULL OUTPUT,
				 @RunActionModel INT = NULL OUTPUT
AS

BEGIN

	SET NOCOUNT ON;

	DECLARE @RunMelissaData1 INT
	DECLARE @RunBenchmark1 INT
	DECLARE @RunActionModel1 INT

	SELECT  @RunMelissaData1 = RunMelissaData,
		    @RunBenchmark1 = RunBenchmark,
			@RunActionModel1 = RunActionModel
	FROM Holding.Client 
	WHERE ClientId = @ClientID

	SET @RunMelissaData = @RunMelissaData1
	SET @RunBenchmark = @RunBenchmark1
	SET @RunActionModel = @RunActionModel1


END





GO
