SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [Holding].[InsertPremiseAttributeData]
 @PremiseAttributeTbl AS holding.PremiseAttributesType READONLY
AS
/**********************************************************************************************************
* SP Name:
*		holding.InsertPremiseAttributedata
* Parameters:
*		PremiseAttributesTable			
* Purpose:	This stored procedure insertsrows into the premiseattributes holding tables
*	
*	
*	
*	
*              
*
*              
*
**********************************************************************************************************/
BEGIN
	SET NOCOUNT ON
	DECLARE @thecount INT
	INSERT INTO holding.PremiseAttributes (
	clientid,
	customerid,
	accountid,
	premiseid,
	attributekey,
	optionvalue,
	sourceid,
	trackingid,
	UpdateDate,
	CreateDate,
	trackingdate
	 )

	SELECT 
	clientid,
	customerid,
	accountid,
	premiseid,
	attributekey,
	optionvalue,
	sourceid,
	trackingid,
	UpdateDate,
	CreateDate,
	trackingdate

	FROM @PremiseAttributeTbl tmp

	SELECT @thecount = @@ROWCOUNT

	SELECT @thecount
	

END --proc

























GO
