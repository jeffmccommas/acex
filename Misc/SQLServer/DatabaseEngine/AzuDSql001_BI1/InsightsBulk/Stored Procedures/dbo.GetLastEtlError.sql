SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
    --- Troubleshooting proc only not for PROD Deploy ---

description: Get LAST failure of ETL
Written by:  Wayne
Date:	   3.2.15
*/

CREATE PROC [dbo].[GetLastEtlError]
as
SELECT TOP 10 message, CAST(CAST(message_time as date)  AS varchar(20)) as Message_day, cast(CAST(message_time as TIME(0)) AS varchar(20)) AS Message_time
  FROM [SSISDB].[internal].[operation_messages] om
  inner join [SSISDB].[internal].[operations] io
  on om.operation_id=io.[operation_id]
  where object_name = 'InsightsDW'
  and  message like '%error%' 
  AND message NOT like '%Code DTS_%'
 and om.operation_id = (select top 1 om.operation_id 
 FROM [SSISDB].[internal].[operation_messages] om
  inner join [SSISDB].[internal].[operations] io
  on om.operation_id=io.[operation_id]
  where object_name = 'InsightsDW' 
  AND io.status=4  -- The possible values are created (1), running (2), canceled (3), failed (4), pending (5), ended unexpectedly (6), succeeded (7), stopping (8), and completed (9).
  order by operation_message_id desc)
  order by operation_message_id DESC
  
GO
