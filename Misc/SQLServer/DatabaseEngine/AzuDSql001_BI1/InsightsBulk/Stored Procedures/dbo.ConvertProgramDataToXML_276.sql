
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Ubaid
-- Create date: 06/04/2015
-- Description:	This will create customer, billing and profile xml
--				for input  into bulk import
-- =============================================
CREATE PROCEDURE [dbo].[ConvertProgramDataToXML_276] @ClientId AS INT
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @ProgramMapping AS TABLE
            (
              Category VARCHAR(255) ,
              AttributeKey VARCHAR(255)
            );

        INSERT  INTO @ProgramMapping
        VALUES  ( 'FRN', 'spaceheatingprogram.enrollmentstatus' );
        INSERT  INTO @ProgramMapping
        VALUES  ( 'WHTS', 'waterheaterprogram.enrollmentstatus' );
        INSERT  INTO @ProgramMapping
        VALUES  ( 'THM', 'thermostatprogram.enrollmentstatus' );
        INSERT  INTO @ProgramMapping
        VALUES  ( 'WHTD', 'waterheaterprogram.enrollmentstatus' );
        INSERT  INTO @ProgramMapping
        VALUES  ( 'BOIL', 'spaceheatingprogram.enrollmentstatus' );
		
        BEGIN TRY

            WITH XMLNAMESPACES (DEFAULT 'Aclara:Insights')

	

        SELECT  ( SELECT    Account AS '@CustomerId' ,
                    Account AS 'Account/@AccountId' ,
                    Premise AS 'Account/Premise/@PremiseId' ,
                    ( SELECT    pm.AttributeKey AS 'ProfileItem/@AttributeKey' ,
                                pm.AttributeKey + '.enrolled' AS 'ProfileItem/@AttributeValue' ,
                                'utility' AS 'ProfileItem/@Source' ,
                                GETDATE() AS 'ProfileItem/@ModifiedDate'
                    FOR
                      XML PATH('') ,
                          TYPE ,
                          ELEMENTS
                    ) AS 'Account/Premise'
          FROM     [dbo].[RawProgramTemp_276] t INNER JOIN @ProgramMapping pm ON t.Category = pm.Category
          WHERE     ClientId = @ClientId
        FOR
          XML PATH('Customer') ,
              ELEMENTS ,
              ROOT('Customers')
        ) AS XmlData ,
        'Profile' AS XmlType;

        END TRY
        BEGIN CATCH
            THROW;
        END CATCH; 

    END;






GO
