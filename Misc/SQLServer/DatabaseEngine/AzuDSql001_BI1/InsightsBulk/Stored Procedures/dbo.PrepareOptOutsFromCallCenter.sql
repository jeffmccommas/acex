SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Wayne
-- Create date:	11/19/2014
-- Description:	This will use the call center format 
--				of out file 
--				which has 8 columns:
--				    1. Date	
--				    2. First Name	
--				    3. Last Name	
--				    4. Service Address-Street 1	
--				    5. City	
--				    6. State	
--				    7. Zip	
--				    8. Account Number
--				to find which customers (Servicepoint/Account) to optout
-- =============================================
-- Author:		Wayne
-- Create date:	4/14/2015
-- Description:	changed to use view in join
--				also changed to a MERGE
--				also added a client id param
-- =============================================
CREATE PROC [dbo].[PrepareOptOutsFromCallCenter]
@clientid INT =101
AS

Merge [dbo].[ScgOptOuts]	 AS Target
USING (
SELECT	sp.ServicePointId,
		dp.AccountId,
		a.GroupId,	
		dp.clientid
FROM InsightsDW.dbo.DimCustomer c
INNER JOIN InsightsDW.dbo.FactCustomerPremise fcp
ON fcp.CustomerKey = c.CustomerKey
INNER JOIN InsightsDW.dbo.DimPremise dp
ON dp.PremiseKey=fcp.PremiseKey
INNER JOIN InsightsDW.dbo.DimServicePoint sp
ON sp.PremiseKey = dp.PremiseKey
INNER JOIN insightsbulk.[dbo].[ScgCallCenterOptOuts] e
ON LTRIM(rtrim(e.accountnumber))=LTRIM(RTRIM(dp.AccountId))
LEFT outer JOIN InsightsDW.[dbo].[vScgAnalysisGroup] a
						ON a.premisekey=dp.PremiseKey
WHERE dp.clientid=@clientid AND e.ClientId=@clientid
) AS Source
ON  Source.ServicePointId=Target.service_point_id and
    Source.AccountId= Target.account_id and
	Source.GroupId	 = Target.CELL and
	Source.clientid=Target.clientid
WHEN NOT MATCHED
THEN INSERT VALUES (
Source.ServicePointId,
    Source.AccountId,
	Source.GroupId,
	Source.clientid);
GO
