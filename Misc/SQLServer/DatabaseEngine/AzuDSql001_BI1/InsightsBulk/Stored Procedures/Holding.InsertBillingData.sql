SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Holding].[InsertBillingData]
    @BillTbl AS Holding.BillingTableType READONLY
AS /**********************************************************************************************************
* SP Name:
*		holding.InsertBillingdata
* Parameters:
*		CustomerHoldingTable			BillingTableType
* Purpose:	This stored procedure insertsrows into the billing holding tables
*	
*	
*	
*	
*              
*
*              
*
**********************************************************************************************************/
    BEGIN
        SET NOCOUNT ON;
        DECLARE @servicescount INT;

        INSERT  INTO Holding.Billing
                ( ClientId ,
                  CustomerId ,
                  AccountId ,
                  PremiseId ,
                  ServiceContractId ,
                  ServicePointId ,
                  MeterId ,
                  BillStartDate ,
                  BillDays ,
                  BillEndDate ,
                  TotalUnits ,
                  TotalCost ,
                  CommodityId ,
                  BillPeriodTypeId ,
                  BillCycleScheduleId ,
                  UOMId ,
                  AMIStartDate ,
                  AMIEndDate ,
                  RateClass ,
                  MeterType ,
                  ReplacedMeterId ,
                  ReadQuality ,
                  ReadDate ,
                  DueDate ,
                  SourceId ,
                  TrackingId ,
                  TrackingDate
                )
                SELECT  ClientID ,
                        CustomerID ,
                        AccountID ,
                        PremiseId ,
                        ServiceContractId ,
                        ServicePointId ,
                        MeterId ,
                        BillStartDate ,
                        DATEDIFF(dd, BillStartDate, BillEndDate) ,
                        BillEndDate ,
                        totalunits ,
                        totalcost ,
                        commodityid ,
                        BillPeriodTypeId ,
                        BillCycleScheduleId ,
                        UOM ,
                        AMIStartDate ,
                        AMIEndDate ,
                        RateClass ,
                        MeterType ,
                        ReplacedMeterId ,
                        ReadQuality ,
                        ReadDate ,
                        DueDate ,
                        SourceId ,
                        TrackingId ,
                        TrackingDate
                FROM    @BillTbl;


        SELECT  @@Rowcount;
    END; --proc






















GO
