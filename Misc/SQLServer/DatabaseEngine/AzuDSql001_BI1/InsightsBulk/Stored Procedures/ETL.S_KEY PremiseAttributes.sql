SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/21/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[S_KEY PremiseAttributes]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;

		SELECT ClientID, 
				AccountID, 
				PremiseID, 
				CustomerID,
				AttributeId, 
				AttributeKey,
				SourceId
		FROM
		(SELECT ClientID, 
				AccountID, 
				PremiseID, 
				CustomerID,
				AttributeId, 
				AttributeKey,
				SourceId,
				ROW_NUMBER() OVER ( PARTITION BY 
				ClientId, PremiseId, AccountId, AttributeKey
				ORDER BY TrackingDate DESC ) AS SortId
		FROM Holding.v_PremiseAttributes WITH(NOLOCK)
		WHERE ClientId = @ClientID
		AND PremiseID IS NOT NULL
		AND AccountID IS NOT NULL
		AND CustomerID IS NOT NULL
		AND LEN(LTRIM(RTRIM(CustomerID))) > 0
		AND AttributeId IS NOT NULL
		AND OptionId IS NOT NULL
		AND SourceId IS NOT NULL
		) b
		WHERE SortId = 1
		ORDER BY ClientId, AccountId, PremiseId, b.CustomerID, AttributeKey

END





GO
