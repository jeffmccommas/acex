SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [dbo].[ExportTempEventsToFileForImport] @clientID int, @ETFileType varchar(30)
as

set nocount on

if @ETFileType = 'ETClickOpen'
begin

	select distinct t1.ClientID as referrerid,t1.CustomerID,t3.AccountID, t3.PremiseID, t4.EventType, 
		t4.Time as FlagDateCreated, t4.Time as FlagDateUpdated
	from insightsDW.dbo.DimCustomer t1 join insightsDW.dbo.FactCustomerPremise t2
		on t1.customerkey = t2.customerkey
	join insightsDW.dbo.FactPremiseAttribute t3
		on t2.premisekey = t3.premiseKey
	join insightsbulk.dbo.ETClickOpen t4
		on t1.emailaddress = t4.Email_Address
	where len(t1.emailaddress) > 0 

end

if @ETFileType = 'ETBounceOptOut'
begin

	select distinct t1.ClientID as referrerid,t1.CustomerID,t3.AccountID, t3.PremiseID, t4.EventType, 
	t4.UpdateDate as FlagDateCreated, t4.UpdateDate as FlagDateUpdated
	from insightsDW.dbo.DimCustomer t1 join insightsDW.dbo.FactCustomerPremise t2
		on t1.customerkey = t2.customerkey
	join insightsDW.dbo.FactPremiseAttribute t3
		on t2.premisekey = t3.premiseKey
	join insightsbulk.dbo.ETBounceOptOut t4
		on t1.emailaddress = t4.Email
	where len(t1.emailaddress) > 0

end

set nocount off
GO
