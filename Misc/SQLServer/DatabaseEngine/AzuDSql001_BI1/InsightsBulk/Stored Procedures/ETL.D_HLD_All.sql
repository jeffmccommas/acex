SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 8/8/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[D_HLD_All]
				 @ClientID INT
AS

BEGIN

	SET NOCOUNT ON;

	DELETE FROM Holding.Billing WHERE ClientID = @ClientID
	DELETE FROM Holding.Customer WHERE ClientID = @ClientID
	DELETE FROM Holding.[Events] WHERE ClientID = @ClientID
	DELETE FROM Holding.Premise WHERE ClientID = @ClientID
	DELETE FROM Holding.PremiseAttributes WHERE ClientID = @ClientID
	DELETE FROM Holding.ActionItem WHERE ClientID = @ClientID

END





GO
