SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/21/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[S_PremiseAttributesOptions]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;

		SELECT ClientID, AccountID, CustomerID, PremiseID, AttributeId, AttributeKey, OptionId, OptionValue, SourceId, TrackingId, TrackingDate
		FROM
		(SELECT ClientID, AccountID, CustomerID, PremiseID, AttributeId, AttributeKey, OptionId, OptionValue, SourceId, TrackingId, TrackingDate
				,ROW_NUMBER() OVER ( PARTITION BY 
				ClientId, PremiseId, AccountId, AttributeKey
				ORDER BY TrackingDate DESC ) AS SortId
		FROM Holding.v_PremiseAttributes WITH(NOLOCK)
		WHERE ClientId = @ClientID) b
		WHERE SortId = 1
		ORDER BY ClientId, PremiseId, AccountId, AttributeKey

END





GO
