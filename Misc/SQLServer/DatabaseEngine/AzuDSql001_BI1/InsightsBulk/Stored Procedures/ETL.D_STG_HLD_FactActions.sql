SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ubaid Tariq
-- Create date: 7/16/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[D_STG_HLD_FactActions]
				 @ClientID INT
AS

BEGIN

	SET NOCOUNT ON;

	DELETE FROM [ETL].[ActionItem] WHERE ClientID = @ClientID

END






GO
