SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ubaid
-- Create date: 11/13/2014
-- Description:	Remove premise attributes with no values returned by melissa
-- =============================================
CREATE PROCEDURE [Holding].[DeletePremiseAttributesWithNoValues] 
	@ClientId AS INT
AS
BEGIN
	SET NOCOUNT ON;

    DELETE FROM Holding.PremiseAttributes
	WHERE ClientID = @ClientId
	AND OptionValue = ''
END
GO
