SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ubaid Tariq
-- Create date: 12/18/2014
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Holding].[FindUpdateActionItemKeys] @ClientID INT
AS
    BEGIN
	
        SET NOCOUNT ON;

        SELECT  *
        INTO    #Actions
        FROM    ( SELECT    ClientID ,
                            ca.ActionKey ,
                            ta.ActionID ,
                            ROW_NUMBER() OVER ( PARTITION BY ca.ActionKey ORDER BY ActionID DESC ) AS SortId
                  FROM      [insightsmetadata].[cm].[ClientAction] ca
                            INNER JOIN [insightsmetadata].cm.TypeAction ta ON ta.ActionKey = ca.ActionKey
                  WHERE     ClientID = 0
                            OR ClientID = @ClientID
                ) a
        WHERE   SortId = 1

        UPDATE  ai
        SET     ActionId = a.ActionID
        FROM    [Holding].[ActionItem] ai
                INNER JOIN #Actions a ON a.ActionKey = ai.ActionKey
        WHERE   ai.ClientID = @ClientID
                
        DROP TABLE #Actions
    END

GO
