SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[AddExistingMelissaData]
---------------------------------------------------
--written by:	Wayne
--date:			9.13.14
--description:	adds melissa data that we have already collected to the DW
---------------------------------------------------
AS
	WITH XMLNAMESPACES (DEFAULT 'Aclara:Insights')
	SELECT
	( 
SELECT 
		CustomerID	AS '@CustomerId',	-- Customer attributes
		AccountID	AS 'Account/@AccountId',	-- Account attributes
		PremiseID	AS 'Account/Premise/@PremiseId'	-- Premise attributes

----centralac.style

, CASE WHEN [centralac.style] IS NULL THEN NULL
		ELSE (SELECT     
        'centralac.style'  AS 'ProfileItem/@AttributeKey'
		,[centralac.style] AS 'ProfileItem/@AttributeValue'
		,GETDATE()			AS 'ProfileItem/@CreateDate'
		,GETDATE()			AS 'ProfileItem/@ModifiedDate'
		,'utility'				AS 'ProfileItem/@Source'
		FOR XML PATH(''), TYPE, ELEMENTS) 
end as 'Account/Premise'
			

--------[heatsystem.fuel

, CASE WHEN [heatsystem.fuel] IS NULL THEN NULL
		ELSE (SELECT     
        'heatsystem.fuel'  AS 'ProfileItem/@AttributeKey'
		,[heatsystem.fuel] AS 'ProfileItem/@AttributeValue'
		,GETDATE()			AS 'ProfileItem/@CreateDate'
		,GETDATE()			AS 'ProfileItem/@ModifiedDate'
		,'utility'				AS 'ProfileItem/@Source'
		FOR XML PATH(''), TYPE, ELEMENTS) 
end as 'Account/Premise'

-------------heatsystem.style

, CASE WHEN [heatsystem.style] IS NULL THEN NULL
		ELSE (SELECT     
        'heatsystem.style'  AS 'ProfileItem/@AttributeKey'
		,[heatsystem.style] AS 'ProfileItem/@AttributeValue'
		,GETDATE()			AS 'ProfileItem/@CreateDate'
		,GETDATE()			AS 'ProfileItem/@ModifiedDate'
		,'utility'				AS 'ProfileItem/@Source'
		FOR XML PATH(''), TYPE, ELEMENTS) 
end as 'Account/Premise'

-------------house.levels
      
, CASE WHEN [house.levels] IS NULL THEN NULL
		ELSE (SELECT     
        'house.levels'  AS 'ProfileItem/@AttributeKey'
		,[house.levels] AS 'ProfileItem/@AttributeValue'
		,GETDATE()			AS 'ProfileItem/@CreateDate'
		,GETDATE()			AS 'ProfileItem/@ModifiedDate'
		,'utility'				AS 'ProfileItem/@Source'
		FOR XML PATH(''), TYPE, ELEMENTS) 
end as 'Account/Premise'

-------------house.people
      
, CASE WHEN [house.people] IS NULL THEN NULL
		ELSE (SELECT     
        'house.people'  AS 'ProfileItem/@AttributeKey'
		,[house.people] AS 'ProfileItem/@AttributeValue'
		,GETDATE()			AS 'ProfileItem/@CreateDate'
		,GETDATE()			AS 'ProfileItem/@ModifiedDate'
		,'utility'				AS 'ProfileItem/@Source'
		FOR XML PATH(''), TYPE, ELEMENTS) 
end as 'Account/Premise'

------------house.rented
      
, CASE WHEN [house.rented] IS NULL THEN NULL
		ELSE (SELECT     
        'house.rented'  AS 'ProfileItem/@AttributeKey'
		,[house.rented] AS 'ProfileItem/@AttributeValue'
		,GETDATE()			AS 'ProfileItem/@CreateDate'
		,GETDATE()			AS 'ProfileItem/@ModifiedDate'
		,'utility'				AS 'ProfileItem/@Source'
		FOR XML PATH(''), TYPE, ELEMENTS) 
end as 'Account/Premise'

-------------house.roofarea
     
, CASE WHEN [house.roofarea] IS NULL THEN NULL
		ELSE (SELECT     
        'house.roofarea'  AS 'ProfileItem/@AttributeKey'
		,[house.roofarea] AS 'ProfileItem/@AttributeValue'
		,getdate()			AS 'ProfileItem/@CreateDate'
		,getdate()			AS 'ProfileItem/@ModifiedDate'
		,'utility'				AS 'ProfileItem/@Source'
		FOR XML PATH(''), TYPE, elements) 
end as 'Account/Premise'

-------------house.rooms
      
, case when [house.rooms] is null then null
		else (select     
        'house.rooms'  AS 'ProfileItem/@AttributeKey'
		,[house.rooms] AS 'ProfileItem/@AttributeValue'
		,getdate()			AS 'ProfileItem/@CreateDate'
		,getdate()			AS 'ProfileItem/@ModifiedDate'
		,'utility'				AS 'ProfileItem/@Source'
		FOR XML PATH(''), TYPE, elements) 
end as 'Account/Premise'

-------------house.style

, case when [house.style] is null then null
		else (select     
        'house.style'  AS 'ProfileItem/@AttributeKey'
		,[house.style] AS 'ProfileItem/@AttributeValue'
		,getdate()			AS 'ProfileItem/@CreateDate'
		,getdate()			AS 'ProfileItem/@ModifiedDate'
		,'utility'				AS 'ProfileItem/@Source'
		FOR XML PATH(''), TYPE, elements) 
end as 'Account/Premise'

-------------house.totalarea
    
, case when [house.totalarea] is null then null
		else (select     
        'house.totalarea'  AS 'ProfileItem/@AttributeKey'
		,[house.totalarea] AS 'ProfileItem/@AttributeValue'
		,getdate()			AS 'ProfileItem/@CreateDate'
		,getdate()			AS 'ProfileItem/@ModifiedDate'
		,'utility'				AS 'ProfileItem/@Source'
		FOR XML PATH(''), TYPE, elements) 
end as 'Account/Premise'

-------------house.yearbuilt

, case when [house.yearbuilt] is null then null
		else (select     
        'house.yearbuilt'  AS 'ProfileItem/@AttributeKey'
		,[house.yearbuilt] AS 'ProfileItem/@AttributeValue'
		,getdate()			AS 'ProfileItem/@CreateDate'
		,getdate()			AS 'ProfileItem/@ModifiedDate'
		,'utility'				AS 'ProfileItem/@Source'
		FOR XML PATH(''), TYPE, elements) 
end as 'Account/Premise'

-------------outsidewater.gardenarea

, case when [outsidewater.gardenarea] is null then null
		else (select     
        'outsidewater.gardenarea'  AS 'ProfileItem/@AttributeKey'
		,[outsidewater.gardenarea] AS 'ProfileItem/@AttributeValue'
		,getdate()			AS 'ProfileItem/@CreateDate'
		,getdate()			AS 'ProfileItem/@ModifiedDate'
		,'utility'				AS 'ProfileItem/@Source'
		FOR XML PATH(''), TYPE, elements) 
end as 'Account/Premise'

-------------outsidewater.lawnarea
      
, case when [outsidewater.lawnarea] is null then null
		else (select     
        'outsidewater.lawnarea'  AS 'ProfileItem/@AttributeKey'
		,[outsidewater.lawnarea] AS 'ProfileItem/@AttributeValue'
		,getdate()			AS 'ProfileItem/@CreateDate'
		,getdate()			AS 'ProfileItem/@ModifiedDate'
		,'utility'				AS 'ProfileItem/@Source'
		FOR XML PATH(''), TYPE, elements) 
end as 'Account/Premise'

-------------pool.count
     
, case when [pool.count] is null then null
		else (select     
        'pool.count'  AS 'ProfileItem/@AttributeKey'
		,[pool.count] AS 'ProfileItem/@AttributeValue'
		,getdate()			AS 'ProfileItem/@CreateDate'
		,getdate()			AS 'ProfileItem/@ModifiedDate'
		,'utility'				AS 'ProfileItem/@Source'
		FOR XML PATH(''), TYPE, elements) 
end as 'Account/Premise'

------------pool.poolheater

, case when [pool.poolheater] is null then null
		else (select     
        'pool.poolheater'  AS 'ProfileItem/@AttributeKey'
		,[pool.poolheater] AS 'ProfileItem/@AttributeValue'
		,getdate()			AS 'ProfileItem/@CreateDate'
		,getdate()			AS 'ProfileItem/@ModifiedDate'
		,'utility'				AS 'ProfileItem/@Source'
		FOR XML PATH(''), TYPE, elements) 
end as 'Account/Premise'

-------

 FROM [InsightsMetadata].[dbo].[ClientPropertyData] 
 where [centralac.style] is not null 
	  OR [heatsystem.fuel] is not null
      OR [heatsystem.style] is not null
      OR [house.levels] is not null
      OR [house.people] is not null
      OR [house.rented] is not null
      OR [house.roofarea] is not null
      OR [house.rooms] is not null
      OR [house.style] is not null
      OR [house.totalarea] is not null
      OR [house.yearbuilt] is not null
      OR [outsidewater.gardenarea] is not null
      OR [outsidewater.lawnarea] is not null
      OR [pool.count] is not null
      OR [pool.poolheater] is not null  
FOR XML PATH('Customer'),ELEMENTS, root('Customers'))
AS col_xml
GO
