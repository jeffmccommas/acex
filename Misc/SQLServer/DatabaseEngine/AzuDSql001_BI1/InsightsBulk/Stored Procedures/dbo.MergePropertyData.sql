SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[MergePropertyData]
    @PremiseInfo AS [dbo].[PremiseInfoTableType] READONLY ,
    @PremiseAttributes AS [dbo].[PremiseAttributeTableType] READONLY
AS
    BEGIN
        SET NOCOUNT ON;

        BEGIN TRY
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

            BEGIN TRANSACTION
      
            SELECT  *
            INTO    #PremAttributes
            FROM    ( SELECT    *
                      FROM      @PremiseAttributes
                    ) pa PIVOT ( MAX(OptionValue) FOR AttributeKey IN ( "centralac.style",
                                                              "heatsystem.fuel",
                                                              "heatsystem.style",
                                                              "house.levels",
                                                              "house.people",
                                                              "house.rented",
                                                              "house.roofarea",
                                                              "house.rooms",
                                                              "house.style",
                                                              "house.totalarea",
                                                              "house.yearbuilt",
                                                              "outsidewater.gardenarea",
                                                              "outsidewater.lawnarea",
                                                              "pool.count",
                                                              "pool.poolheater",
															  "waterheater.fuel" ) ) AS paa;


            INSERT  INTO [InsightsMetadata].[dbo].[ClientPropertyData]
                    SELECT  p.ClientID ,
                            p.CustomerID ,
                            p.AccountID,
							p.PremiseID ,
                            info.Address ,
                            info.City ,
                            info.State ,
                            info.Zip ,
                            info.Latitude ,
                            info.Longitude ,
                            [centralac.style] ,
                            pa.[heatsystem.fuel] ,
                            pa.[heatsystem.style] ,
                            pa.[house.levels] ,
                            pa.[house.people] ,
                            pa.[house.rented] ,
                            pa.[house.roofarea] ,
                            pa.[house.rooms] ,
                            pa.[house.style] ,
                            pa.[house.totalarea] ,
                            pa.[house.yearbuilt] ,
                            pa.[outsidewater.gardenarea] ,
                            pa.[outsidewater.lawnarea] ,
                            pa.[pool.count] ,
                            pa.[pool.poolheater] ,
                            info.ResultCode ,
							GETUTCDATE() ,
							info.ResultData,
							pa.[waterheater.fuel]
                    FROM    @PremiseInfo info
                            INNER JOIN Holding.Premise p ON p.PremiseRowId = info.RowId
                            LEFT JOIN #PremAttributes pa ON info.RowId = pa.RowId
			
            DROP TABLE #PremAttributes

            COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0 
                ROLLBACK TRANSACTION
			
            THROW;
        END CATCH  
    END

GO
