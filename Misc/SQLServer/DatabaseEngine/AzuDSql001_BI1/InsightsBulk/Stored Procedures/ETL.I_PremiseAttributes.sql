SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/27/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[I_PremiseAttributes]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;

		DECLARE @TrackingDate DATETIME = GETUTCDATE()

		INSERT INTO Holding.PremiseAttributes
		        ( ClientID ,
		          CustomerID ,
		          AccountID ,
		          PremiseID ,
		          AttributeId,
		          AttributeKey ,
		          OptionId ,
		          OptionValue ,
		          SourceId ,
		          TrackingID ,
		          TrackingDate
		        )
		SELECT  ClientID, 
				CustomerID,
				AccountID, 
				PremiseID, 
				AttributeId, 
				AttributeKey, 
				NULL, 
				AttributeValue, 
				SourceId, 
				CONVERT(VARCHAR(10),@TrackingDate,112) + REPLACE(CONVERT(VARCHAR(10),@TrackingDate,108),':',''), 
				@TrackingDate
		FROM ETL.PremiseAttributes_INS WITH(NOLOCK)
		WHERE ClientID = @ClientID


END





GO
