SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ubaid Tariq
-- Create date: 12/16/2014
-- Description:	get all actions for a client
-- =============================================
CREATE PROCEDURE [ETL].[S_Actions] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT  [ClientId] ,
                [CustomerId] ,
                [AccountID] ,
                [PremiseId] ,
                [StatusId] ,
                [StatusDate] ,
                [ActionKey] ,
                [SubActionKey] ,
                MAX(ActionId) AS ActionId ,
                MAX(SourceId) AS SourceId ,
                MAX(TrackingID) AS TrackingId ,
                MAX(TrackingDate) AS TrackingDate ,
                CONVERT(VARCHAR(MAX), ISNULL(STUFF(( SELECT TOP 100 PERCENT
                                                            ';'
                                                            + t2.ActionData
                                                            + ':'
                                                            + t2.ActionDataValue
                                                     FROM   [InsightsBulk].[Holding].[ActionItem]
                                                            AS t2
                                                     WHERE  t2.ClientId = a.ClientId
                                                            AND t2.CustomerId = a.CustomerId
                                                            AND t2.AccountID = a.AccountID
                                                            AND t2.PremiseId = a.PremiseId
                                                            AND t2.StatusId = a.StatusId
                                                            AND t2.StatusDate = a.StatusDate
                                                            AND t2.ActionKey = a.ActionKey
                                                            AND t2.SubActionKey = a.SubActionKey
                                                     ORDER BY t2.ActionData ,
                                                            t2.ActionDataValue
                                                   FOR
                                                     XML PATH('')
                                                   ), 1, 1, ''), '')) AS AdditionalInfo
        FROM    [InsightsBulk].[Holding].[ActionItem] a
        WHERE   a.ClientId = @ClientID
        GROUP BY ClientId ,
                CustomerId ,
                AccountID ,
                PremiseId ,
                ActionKey ,
                SubActionKey ,
                StatusId ,
                StatusDate
        ORDER BY a.ClientId ,
                a.PremiseId ,
                a.AccountID ,
                a.ActionKey ,
                a.SubActionKey ,
                a.StatusId ,
                a.StatusDate;

    END;





GO
