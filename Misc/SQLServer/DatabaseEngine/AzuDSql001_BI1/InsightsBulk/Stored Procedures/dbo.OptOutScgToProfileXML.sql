SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--==================================================================================================================================================================
-- Author:		Wayne
-- Create date:	09/25/2014
-- Description:	This will optout SCG particpants by creating profile xml
--				for input  into BULK jobstream
--==================================================================================================================================================================
-- Notes:
--
--  SCG Groups 
--
-- T-9 	  = Email PCR test group; (12,500 + 10% buffer to account for attrition)				= 13,750 rows		    AKA Treatment 1 - Email only PCR: 12,500
-- T-11 	  = 12 month paper/email PCR test group; (12,500 + 10% buffer to account for attrition)	= 13,750 rows		    AKA Treatment 2 - Paper/Email PCR: 12,500
-- C-8 	  = associated control group for this test;										= 25,000 rows		    AKA Control Group 1
-- T-14 	  = paper only PCR test group; (50,000 +7% buffer to account for attrition)			= 53,500 rows		    AKA Treatment 3 - Paper only PCR: 50,000
-- C-9 	  = associated control group for this test;										= 117,158 rows		    AKA Control group 2
--
--  SCG Opt out rules: 
--
-- IF the member is 		The channel will change from			to
-- T-9					any								paperreport.channel.none
-- T-11					paperreport.channel.printandemail		paperreport.channel.printonly
-- T-14					any								paperreport.channel.none
-- C-8					any								paperreport.channel.none
-- T-14					any								paperreport.channel.none
-- T-11					paperreport.channel.printonly			paperreport.channel.none
--==================================================================================================================================================================
-- Changed By:	 Wayne
-- Change Date: 04/14/2015
-- Description: Handle optouts for non treatment group participants 
--			 always change them based on rules in InsightsMetadata.[dbo].[OptOutChannel] table
--
--			 also changed it to be non-client specific
--			 by createing new table InsightsMetadata.[dbo].[OptOutChannel]
--
--			 this change was a major overhaul of proc
--			 much simpler now, removed some of previous comments as they no longer apply
--==================================================================================================================================================================
CREATE PROCEDURE [dbo].[OptOutScgToProfileXML]
@clientid int
AS
BEGIN
	SET NOCOUNT ON;
	WITH XMLNAMESPACES (DEFAULT 'Aclara:Insights')
	select 
	isnull(
	(	 
	   SELECT 
			cs.CustomerId	AS '@CustomerId',	-- Customer attributes
			dp.AccountId	AS 'Account/@AccountId',	-- Account attributes
			dp.PremiseId	AS 'Account/Premise/@PremiseId'	-- Premise attributes
			,
					(SELECT					
					 'paperreport.channel'	AS 'ProfileItem/@AttributeKey'
					, o1.[PostOptOutChannelKey]	AS 'ProfileItem/@AttributeValue'
					, GETDATE()			AS 'ProfileItem/@CreateDate'
					, GETDATE()			AS 'ProfileItem/@ModifiedDate'
					, 'utility'		     AS 'ProfileItem/@Source'
					FROM InsightsMetadata.[dbo].[OptOutChannel] o1
					WHERE o1.[OptOutChannelID]=o.[OptOutChannelID]
					    FOR XML PATH(''), TYPE, elements)									
					    AS 'Account/Premise'	

						FROM				
						InsightsBulk.[dbo].[ScgOptOuts] c
						inner join InsightsDW.dbo.DimServicePoint sp
						on c.service_point_id=sp.ServicePointId						
						inner join InsightsDW.dbo.DimPremise dp
						on sp.PremiseKey=dp.PremiseKey
						inner join InsightsDW.dbo.FactCustomerPremise fcp
						on fcp.PremiseKey=dp.Premisekey
						inner join InsightsDW.dbo.DimCustomer cs
						on cs.CustomerKey=fcp.CustomerKey						
						INNER JOIN InsightsDW.[dbo].[vScgAnalysisGroup] a
						ON a.premisekey=dp.PremiseKey
						INNER JOIN InsightsMetadata.[dbo].[OptOutChannel] o
					     ON o.clientid=101 
						  AND o.[GroupValue]=a.GroupId
						    AND ((  o.[PreOutOutChannelKey] LIKE '%'+ a.channel ) OR (o.[PreOutOutChannelKey]='any'))
						WHERE dp.ClientId=@clientid 
		 		
	

	FOR XML PATH('Customer'),ELEMENTS, root('Customers')
	)
	,'<Customers xmlns="Aclara:Insights"></Customers>')  AS COL_XML 
END
GO
