SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
-- =======================================
-- Written by:	 Wayne
-- Date:		 3.26.15
-- Description: Writes error occurring in the 
-- 			 disagg processing to a log table
-- =======================================
*/
CREATE PROCEDURE [dbo].[GetDisAggQueueBatch]
(
    @clientId     INT,
    @batchid		INT
)
AS
BEGIN
 
    SET NOCOUNT ON;
    
    SELECT customerid, accountid, premiseid FROM dbo.tempqueue
    WHERE taskid=@BatchID AND ClientID=@ClientID

END
GO
