SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ubaid
-- Create date: 1/15/2015
-- Description:	Convert actionitems to xml
-- =============================================
CREATE PROCEDURE [dbo].[ConvertActionItemToXML] @ClientId AS INT
AS
    BEGIN
        SET NOCOUNT ON;

        WITH XMLNAMESPACES (DEFAULT 'Aclara:Insights')

	SELECT ISNULL(	 
		( SELECT customerid AS '@CustomerId'
		, accountid AS 'Account/@AccountId'
		, premiseid AS 'Account/Premise/@PremiseId'
		,actionkey AS 'Account/Premise/ActionItem/@ActionKey'
		,IIF(projectid = '', NULL, projectid) AS 'Account/Premise/ActionItem/@SubActionKey'
		,LOWER([source]) AS 'Account/Premise/ActionItem/@Source'
		,LOWER([status]) AS 'Account/Premise/ActionItem/@Status'
		,CONVERT(DATETIME,[statusdate]) AS 'Account/Premise/ActionItem/@StatusDate'
		,IIF([programname] IS NULL,NULL, ( 
			SELECT 'ProgramName' AS 'ActionKeyData/@ActionData'
			,[programname] AS 'ActionKeyData/@ActionValue' FOR XML PATH(''),TYPE, ELEMENTS
		)) AS 'Account/Premise/ActionItem'
		,IIF([incentiveamount] IS NULL,NULL, (
			SELECT 'IncentiveAmount' AS 'ActionKeyData/@ActionData'
			,[incentiveamount] AS 'ActionKeyData/@ActionValue' FOR XML PATH(''),TYPE, ELEMENTS
		)) AS 'Account/Premise/ActionItem'
		,IIF([incentivetype] IS NULL,NULL, ( 
			SELECT 'IncentiveType' AS 'ActionKeyData/@ActionData'
			,[incentivetype] AS 'ActionKeyData/@ActionValue' FOR XML PATH(''),TYPE, ELEMENTS
		)) AS 'Account/Premise/ActionItem'
		,IIF([upfrontcost] IS NULL,NULL, (
			SELECT 'UpfrontCost' AS 'ActionKeyData/@ActionData'
			,[upfrontcost] AS 'ActionKeyData/@ActionValue' FOR XML PATH(''),TYPE, ELEMENTS
		)) AS 'Account/Premise/ActionItem'
		,IIF([annualsavingsestimate] IS NULL OR [annualsavingsestimate] = '',NULL, (
			SELECT 'AnnualSavingsEstimate' AS 'ActionKeyData/@ActionData'
			,[annualsavingsestimate] AS 'ActionKeyData/@ActionValue' FOR XML PATH(''),TYPE, ELEMENTS
		)) AS 'Account/Premise/ActionItem'
		,( 
			SELECT 'MeasureName' AS 'ActionKeyData/@ActionData'
			,[measurename] AS 'ActionKeyData/@ActionValue' FOR XML PATH(''),TYPE, ELEMENTS
		) AS 'Account/Premise/ActionItem'
		FROM ( SELECT    * ,
                    ROW_NUMBER() OVER ( PARTITION BY customerid, accountid,
                                        premiseid, measurename, projectid,
                                        status, statusdate ORDER BY statusdate ) AS sortid
          FROM      [dbo].[RawActionTemp]
          WHERE     clientid = @ClientId
                    AND [actionkey] IS NOT NULL
        ) a
		WHERE a.sortid = 1
		FOR XML PATH('Customer'),ELEMENTS, ROOT('Customers'))
	,'<Customers xmlns="Aclara:Insights"></Customers>') AS xml_actionItem;

    END;
GO
