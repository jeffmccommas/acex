SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/16/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[D_STG_HLD_FactPremiseAttributes]
				 @ClientID INT
AS

BEGIN

	SET NOCOUNT ON;

	DELETE FROM ETL.PremiseAttributes_INS WHERE ClientID = @ClientID
	DELETE FROM ETL.PremiseAttributes_UPD WHERE ClientID = @ClientID

END





GO
