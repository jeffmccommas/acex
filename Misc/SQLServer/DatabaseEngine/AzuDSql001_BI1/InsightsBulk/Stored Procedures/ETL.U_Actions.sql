
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Ubaid Tariq
-- Create date: 12/29/2014
-- Description:	sync actions between api and bulk tables
-- =============================================
CREATE PROCEDURE [ETL].[U_Actions] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

        DELETE  ai
        FROM    Holding.ActionItem ai
                INNER JOIN [ETL].[ActionItem] au ON ai.ClientId = au.ClientId
                                                        AND ai.CustomerId = au.CustomerId
                                                        AND ai.AccountID = au.AccountID
                                                        AND ai.PremiseId = au.PremiseId
                                                        AND ai.ActionKey = au.ActionKey
														AND ai.SubActionKey = au.SubActionKey
        WHERE   ai.ClientId = @ClientID

        INSERT  INTO Holding.ActionItem
                ( ClientId ,
                  CustomerId ,
                  AccountID ,
                  PremiseId ,
                  StatusId ,
                  StatusDate ,
                  SourceId ,
                  ActionKey ,
				  SubActionKey ,
                  ActionId ,
                  ActionData ,
                  ActionDataValue ,
                  TrackingID ,
                  TrackingDate
	            )
                SELECT  [ClientId] ,
                        [CustomerID] ,
                        [AccountID] ,
                        [PremiseId] ,
                        [StatusId] ,
                        [StatusDate] ,
                        [SourceId] ,
                        [ActionKey] ,
						[SubActionKey] ,
                        NULL AS ActionID ,
                        SUBSTRING(x.name, 0, CHARINDEX(':', x.name)) AS ActionData ,
                        SUBSTRING(x.name, CHARINDEX(':', x.name) + 1,
                                  LEN(x.name)) AS ActionDataValue ,
                        CONVERT(VARCHAR(10), TrackingDate, 112)
                        + REPLACE(CONVERT(VARCHAR(10), TrackingDate, 108), ':',
                                  '') AS TrackingID ,
                        [TrackingDate]
                FROM    [ETL].[ActionItem] ai
                        CROSS APPLY ( SELECT    name
                                      FROM      [dbo].[splitstring_bulk](ai.AdditionalInfo,
                                                              ';')
                                    ) x
                WHERE   ClientId = @ClientID

    END







GO
