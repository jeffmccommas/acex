SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Holding].[FindUpdatePremiseAttributeOptionKeys] @ClientID INT
AS
    BEGIN
	
        SET NOCOUNT ON;


        UPDATE  pa
        SET     AttributeId = tpa.ProfileAttributeID
        FROM    [Holding].[PremiseAttributes] pa
                INNER JOIN [InsightsMetadata].[cm].TypeProfileAttribute tpa ON tpa.profileattributekey = pa.attributekey
                INNER JOIN [InsightsMetadata].[cm].ClientProfileAttribute cpa ON tpa.ProfileAttributeKey = cpa.ProfileAttributeKey
        WHERE   pa.ClientID = @ClientID 

        SELECT  *
        INTO    #ClientProfileAttributes
        FROM    ( SELECT    tpa.ProfileAttributeKey ,
                            ClientProfileAttributeID ,
                            ProfileAttributeTypeKey ,
                            MinValue ,
                            MaxValue ,
                            ROW_NUMBER() OVER ( PARTITION BY tpa.ProfileAttributeKey ORDER BY ClientID DESC ) AS SortId
                  FROM      [InsightsMetadata].[cm].TypeProfileAttribute tpa
                            INNER JOIN [InsightsMetadata].[cm].ClientProfileAttribute cpa ON tpa.ProfileAttributeKey = cpa.ProfileAttributeKey
                  WHERE     ClientID = 0
                            OR ClientID = @ClientID
                ) c
        WHERE   SortId = 1 

        UPDATE  pa
        SET     OptionId = tpo.ProfileOptionID
        FROM    [Holding].[PremiseAttributes] pa
                INNER JOIN #ClientProfileAttributes tpa ON tpa.profileattributekey = pa.attributekey
                INNER JOIN [InsightsMetadata].[cm].[ClientProfileAttributeProfileOption] apo ON apo.ClientProfileAttributeID = tpa.ClientProfileAttributeID
                INNER JOIN InsightsMetadata.cm.TypeProfileOption tpo ON tpo.ProfileOptionKey = apo.ProfileOptionKey
        WHERE   pa.ClientID = @ClientID
                AND optionValue != ''
                AND ( 
						( tpa.profileattributetypekey = 'list'
							OR tpa.profileattributetypekey = 'boolean'
							OR TRY_CAST(optionValue AS FLOAT) BETWEEN tpa.minvalue 
                                                        AND   tpa.maxvalue
						)
						AND 
						( OptionValue = apo.ProfileOptionKey
							OR ( tpa.profileattributetypekey != 'list'
									AND tpa.profileattributetypekey != 'boolean'
								)
						)
					)
				OR tpa.ProfileAttributeTypeKey = 'text'

        DROP TABLE #ClientProfileAttributes
    END

GO
