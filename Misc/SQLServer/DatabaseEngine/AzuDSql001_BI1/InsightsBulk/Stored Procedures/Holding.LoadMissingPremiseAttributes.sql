SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [Holding].[LoadMissingPremiseAttributes]
    @ClientId AS INT ,
    @LastRunDate AS DATETIME
AS
    BEGIN	

        SET NOCOUNT ON;
		
        DECLARE @TrackingID AS VARCHAR(50);
        SELECT  @TrackingID = FORMAT(GETDATE(), 'yyyyMMddhhmmss') 

        DELETE  FROM [Holding].[Premise]
        WHERE   ClientID = @ClientId

        DELETE  FROM [Holding].[PremiseAttributes]
        WHERE   ClientID = @ClientId

        DECLARE @MissingAttributes AS TABLE
            (
              AttributeKey VARCHAR(50) NOT NULL
            );

        INSERT INTO @MissingAttributes VALUES  ( 'centralac.style')
		INSERT INTO @MissingAttributes VALUES  ( 'heatsystem.fuel' )
		INSERT INTO @MissingAttributes VALUES  ( 'heatsystem.style' )
		INSERT INTO @MissingAttributes VALUES  ( 'house.levels' )
		INSERT INTO @MissingAttributes VALUES  ( 'house.people' )
		INSERT INTO @MissingAttributes VALUES  ( 'house.rented' )
		INSERT INTO @MissingAttributes VALUES  ( 'house.roofarea' )
		INSERT INTO @MissingAttributes VALUES  ( 'house.rooms' )
		INSERT INTO @MissingAttributes VALUES  ( 'house.style' )
		INSERT INTO @MissingAttributes VALUES  ( 'house.totalarea' )
		INSERT INTO @MissingAttributes VALUES  ( 'house.yearbuilt' )
		INSERT INTO @MissingAttributes VALUES  ( 'outsidewater.gardenarea' )
		INSERT INTO @MissingAttributes VALUES  ( 'outsidewater.lawnarea' )
		INSERT INTO @MissingAttributes VALUES  ( 'pool.count' )
		INSERT INTO @MissingAttributes VALUES  ( 'pool.poolheater' )
		INSERT INTO @MissingAttributes VALUES  ( 'waterheater.fuel' )
                       

        SELECT  r.ClientId ,
                c.CustomerId ,
                p.AccountId ,
                p.PremiseId ,
                p.PremiseKey ,
                IIF(GeoLocation IS NULL, 1, 0) AS MissingPremiseInfo
        INTO    #NewPremises
        FROM    insightsDW.dbo.DimClient r
                INNER JOIN insightsDW.dbo.DimCustomer c ON c.ClientKey = r.ClientKey
                INNER JOIN insightsDW.dbo.FactCustomerPremise cp ON cp.CustomerKey = c.CustomerKey
                INNER JOIN insightsDW.dbo.DimPremise p ON p.PremiseKey = cp.PremiseKey
        WHERE   r.ClientId = @ClientId
                AND p.CreateDate > @LastRunDate
                AND p.AccountId IS NOT NULL
                AND p.PremiseId IS NOT NULL
                AND p.Street1 IS NOT NULL

        INSERT  INTO [Holding].[PremiseAttributes]
                ( [ClientID] ,
                  [CustomerID] ,
                  [AccountID] ,
                  [PremiseID] ,
                  [AttributeKey] ,
                  [OptionValue] ,
                  [SourceId] ,
                  [UpdateDate] ,
                  [CreateDate] ,
                  [TrackingID] ,
                  [TrackingDate]
                )
                SELECT np.ClientId ,
                        np.CustomerId ,
                        np.AccountId ,
                        np.PremiseId ,
                        np.AttributeKey ,
                        '' ,
                        7 ,
                        GETUTCDATE() ,
                        GETUTCDATE() ,
                        @TrackingID ,
                        GETUTCDATE()
                FROM    insightsDW.dbo.FactPremiseAttribute pav
                        INNER JOIN insightsDW.dbo.dimpremiseoption po ON po.OptionKey = pav.OptionKey
                        INNER JOIN insightsDW.dbo.DimPremiseAttribute pa ON pa.attributekey = po.attributekey
                        INNER JOIN @MissingAttributes m ON m.AttributeKey = pa.cmsAttributekey
                        RIGHT JOIN ( SELECT ClientId ,
                                            CustomerId ,
                                            AccountId ,
                                            PremiseId ,
                                            PremiseKey ,
                                            AttributeKey
                                     FROM   #NewPremises
                                            CROSS JOIN @MissingAttributes
                                   ) np ON np.PremiseKey = pav.PremiseKey
                                           AND np.AttributeKey = pa.cmsAttributekey
                WHERE   pa.cmsAttributekey IS NULL
               

        INSERT  INTO [Holding].[Premise]
                ( [ClientID] ,
                  [CustomerID] ,
                  [AccountID] ,
                  [PremiseID] ,
                  [Street1] ,
                  [Street2] ,
                  [City] ,
                  [State] ,
                  [Country] ,
                  [postalcode] ,
                  [GasService] ,
                  [ElectricService] ,
                  [WaterService] ,
                  [Latitude] ,
                  [Longitude] ,
                  [SourceId] ,
                  [TrackingID] ,
                  [TrackingDate]
                )
                SELECT  mpi.ClientId ,
                        mpi.CustomerId ,
                        mpi.AccountId ,
                        mpi.PremiseId ,
                        p.Street1 ,
                        p.Street2 ,
                        p.City ,
                        p.StateProvince ,
                        p.Country ,
                        p.PostalCode ,
                        0 ,
                        0 ,
                        0 ,
                        NULL ,
                        NULL ,
                        7 ,
                        @TrackingID ,
                        GETDATE()
                FROM    ( SELECT DISTINCT
                                    np.ClientId ,
                                    np.CustomerId ,
                                    np.AccountId ,
                                    np.PremiseId ,
                                    np.PremiseKey
                          FROM      #NewPremises np
                                    INNER JOIN [Holding].[PremiseAttributes] pa ON np.ClientId = pa.ClientId
                                                              AND np.CustomerId = pa.CustomerId
                                                              AND np.AccountId = pa.AccountId
                                                              AND np.PremiseId = pa.PremiseId
                        ) mpi
                        INNER JOIN insightsDW.dbo.DimPremise p ON p.PremiseKey = mpi.premiseKey


        SELECT  cpd.*
        INTO    #ExistingAttributes
        FROM    InsightsMetadata.dbo.clientPropertyData cpd
                INNER JOIN [Holding].[Premise] pa ON pa.ClientID = cpd.ClientID
                                                     AND pa.CustomerID = cpd.CustomerID
                                                     AND pa.AccountID = cpd.AccountID
                                                     AND pa.PremiseID = cpd.PremiseID
													 WHERE pa.ClientID = @ClientId



        UPDATE  pa
        SET     OptionValue = unpvt.OptionValue
        FROM    ( SELECT    *
                  FROM      #ExistingAttributes
                ) AS p UNPIVOT ( OptionValue FOR AttributeKey IN ( "centralac.style",
                                                              "heatsystem.fuel",
                                                              "heatsystem.style",
                                                              "house.levels",
                                                              "house.people",
                                                              "house.rented",
                                                              "house.roofarea",
                                                              "house.rooms",
                                                              "house.style",
                                                              "house.totalarea",
                                                              "house.yearbuilt",
                                                              "outsidewater.gardenarea",
                                                              "outsidewater.lawnarea",
                                                              "pool.count",
                                                              "pool.poolheater",
															  "waterheater.fuel" ) ) AS unpvt
                INNER JOIN InsightsBulk.Holding.PremiseAttributes pa ON pa.ClientID = unpvt.ClientID
                                                              AND pa.CustomerID = unpvt.CustomerID
                                                              AND pa.AccountID = unpvt.AccountID
                                                              AND pa.PremiseID = unpvt.PremiseID
                                                              AND pa.AttributeKey = unpvt.AttributeKey 

        UPDATE  p
        SET     Latitude = ISNULL(ea.Latitude,p.Latitude) ,
                Longitude = ISNULL( ea.Longitude,p.Longitude),
				postalcode = ISNULL(ea.Zip,p.postalcode)
        FROM    #ExistingAttributes ea
                INNER JOIN Holding.Premise p ON p.ClientID = ea.ClientID
                                                AND p.CustomerID = ea.CustomerID
                                                AND p.AccountID = ea.AccountID
                                                AND p.PremiseID = ea.PremiseID





        SELECT  PremiseRowId ,
                Street1 + ISNULL(' ' + Street2, '') AS Street ,
                p.City ,
                p.State ,
                p.PostalCode
        FROM    [Holding].[Premise] p
                LEFT JOIN #ExistingAttributes ea ON p.ClientID = ea.ClientID
                                                    AND p.CustomerID = ea.CustomerID
                                                    AND p.AccountID = ea.AccountID
                                                    AND p.PremiseID = ea.PremiseID
        WHERE   p.ClientID = @ClientId AND ea.CustomerID IS NULL
		ORDER BY PostalCode

        DROP TABLE #ExistingAttributes
        DROP TABLE #NewPremises

    END


GO
