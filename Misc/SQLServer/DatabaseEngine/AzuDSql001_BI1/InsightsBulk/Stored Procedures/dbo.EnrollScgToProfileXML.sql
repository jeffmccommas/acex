SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Wayne
-- Create date: 08/21/2014
-- Description:	This will enroll SCG particpants by creating profile xml
--				for input  into Linda's jobstream
--
--	optionvalues to be one of the following valid values:
--
--  0 means do nothing FOR THAT ATTRIBUTE
--
--	paperreport.analysisgroup.enrollmentstatus.enrolled		@analysisgroup =1
--	paperreport.analysisgroup.enrollmentstatus.notenrolled	@analysisgroup =2 (For un-enrolling only)
--
--	paperreport.treatmentgroup.enrollmentstatus.enrolled	@treatmentgroup =1
--	paperreport.treatmentgroup.enrollmentstatus.notenrolled	@treatmentgroup=2 (For un-enrolling only)
--
--	paperreport.treatmentgroup.groupnumber.value			@treatmentgroupnumber =1 or 2 or 3
--
--	paperreport.channel.printandemail						@channel=1
--	paperreport.channel.printonly							@channel=2
--	paperreport.channel.emailonly							@channel=3
-- =============================================
-- changed by:	Wayne
-- change date: 08/25/2014
-- Description:	Added code to assign a treatment group number
-- =============================================
-- changed by:	Wayne
-- change date: 08/25/2014
-- Description:	CELL can take on the following values:
--“T-9” = Email PCR test group; (12,500 + 10% buffer to account for attrition) = 13,750 rows						AKA Treatment 1 - Email only PCR: 12,500
--“T-11” = 12 month paper/email PCR test group; (12,500 + 10% buffer to account for attrition) = 13,750 rows		AKA Treatment 2 - Paper/Email PCR: 12,500
--“C-8” = associated control group for this test; 25,000 rows														Control Group 1
--“T-14” = paper only PCR test group; (50,000 +7% buffer to account for attrition) = 53,500 rows					AKA Treatment 3 - Paper only PCR: 50,000
--“C-9” = associated control group for this test; 117,158 rows														Control group 2
--The remaining rows will be blank (not part of the test)
-- =============================================
-- Changed by:	Wayne
-- Change date: 10/24/2014
-- Description:	filter for client id was missing
-- =============================================
-- Changed by:	Wayne
-- Change date: 10/24/2014
-- Description:	Added a sort by cs.CustomerId,	dp.AccountId, dp.PremiseId
-- =============================================
-- removed input parameters as they are no longer needed as per wayne
CREATE PROCEDURE [dbo].[EnrollScgToProfileXML]
AS
BEGIN
	SET NOCOUNT ON;
	declare @ClientId int=101;
	WITH XMLNAMESPACES (DEFAULT 'Aclara:Insights')
	select 
	isnull((	 
	SELECT 
			cs.CustomerId	AS '@CustomerId',	-- Customer attributes
			dp.AccountId	AS 'Account/@AccountId',	-- Account attributes
			dp.PremiseId	AS 'Account/Premise/@PremiseId'	-- Premise attributes
							

--***************************************************
--			treatment group enrollment
--***************************************************

								
					,case when cell like 'T%'
					 then 
					 -- ProfileItem attributes for treatment enroll
					(SELECT					
					 'paperreport.treatmentgroup.enrollmentstatus'			AS 'ProfileItem/@AttributeKey'
					,'paperreport.treatmentgroup.enrollmentstatus.enrolled'	AS 'ProfileItem/@AttributeValue'
					, GETDATE()			AS 'ProfileItem/@CreateDate'
					, GETDATE()			AS 'ProfileItem/@ModifiedDate'
					, 'utility' AS 'ProfileItem/@Source'
									FOR XML PATH(''), TYPE, elements) 
									else null
					end as 'Account/Premise'


--***************************************************
--			treatment group number assignment
--***************************************************


					,case when cell like 'T%'
					 then 
					 -- ProfileItem attributes for treatment group 1 enroll
					(SELECT					
					 'paperreport.treatmentgroup.groupnumber'			AS 'ProfileItem/@AttributeKey'
					,cell													AS 'ProfileItem/@AttributeValue'
					, GETDATE()			AS 'ProfileItem/@CreateDate'
					, GETDATE()			AS 'ProfileItem/@ModifiedDate'
					, 'utility' AS 'ProfileItem/@Source'
									FOR XML PATH(''), TYPE, elements) 
									else null
					end as 'Account/Premise'

---------------------------------------------------------------		
			


--***************************************************
--				channel
--***************************************************

					,case when cell='T-11' --paper/email 
					 then 
					-- ProfileItem attributes for channel -paper/email 
					(SELECT					
					 'paperreport.channel'					AS 'ProfileItem/@AttributeKey'
					,'paperreport.channel.printandemail'	AS 'ProfileItem/@AttributeValue'
					, GETDATE()			AS 'ProfileItem/@CreateDate'
					, GETDATE()			AS 'ProfileItem/@ModifiedDate'
					, 'utility' AS 'ProfileItem/@Source'
									FOR XML PATH(''), TYPE, elements)
									else null
					end as 'Account/Premise'

---------------------------------
					,case when cell='T-14'	--paper only
					 then 
					-- ProfileItem attributes for channel -emailonly
					(SELECT					
					 'paperreport.channel'			AS 'ProfileItem/@AttributeKey'
					,'paperreport.channel.printonly'	AS 'ProfileItem/@AttributeValue'
					, GETDATE()			AS 'ProfileItem/@CreateDate'
					, GETDATE()			AS 'ProfileItem/@ModifiedDate'
					, 'utility' AS 'ProfileItem/@Source'
									FOR XML PATH(''), TYPE, elements)
									else null
					end as 'Account/Premise'

---------------------------------

					,case when cell='T-9'
					 then 
					-- ProfileItem attributes for channel emailonly
					(SELECT					
					 'paperreport.channel'			AS 'ProfileItem/@AttributeKey'
					,'paperreport.channel.emailonly'	AS 'ProfileItem/@AttributeValue'
					, GETDATE()			AS 'ProfileItem/@CreateDate'
					, GETDATE()			AS 'ProfileItem/@ModifiedDate'
					, 'utility' AS 'ProfileItem/@Source'
									FOR XML PATH(''), TYPE, elements)
									else null
					end as 'Account/Premise'


--***************************************************
--				control group
--***************************************************

					,case when cell like 'C%' --one of the control groups
					 then 
						-- ProfileItem attributes for analysis group enroll
					(SELECT					
					 'paperreport.controlgroup.enrollmentstatus'			AS 'ProfileItem/@AttributeKey'
					,'paperreport.controlgroup.enrollmentstatus.enrolled'	AS 'ProfileItem/@AttributeValue'
					, GETDATE()			AS 'ProfileItem/@CreateDate'
					, GETDATE()			AS 'ProfileItem/@ModifiedDate'
					, 'utility' AS 'ProfileItem/@Source'
									FOR XML PATH(''), TYPE, elements)
					else null
					end as 'Account/Premise'


--***************************************************
--				analysis group
--***************************************************

					,case when cell like 'T%' or cell like'C%'--one of the treatment groups or  --one of the control groups
								          
					 then 
						-- ProfileItem attributes for analysis group enroll
					(SELECT					
					 'paperreport.analysisgroup.enrollmentstatus'			AS 'ProfileItem/@AttributeKey'
					,'paperreport.analysisgroup.enrollmentstatus.enrolled'	AS 'ProfileItem/@AttributeValue'
					, GETDATE()			AS 'ProfileItem/@CreateDate'
					, GETDATE()			AS 'ProfileItem/@ModifiedDate'
					, 'utility' AS 'ProfileItem/@Source'
									FOR XML PATH(''), TYPE, elements)
					else null
					end as 'Account/Premise'

--***************************************************
--			conbtrol group number assignment
--***************************************************


					,case when cell like 'C%'
					 then 
					 -- ProfileItem attributes for treatment group 1 enroll
					(SELECT					
					 'paperreport.controlgroup.groupnumber'			AS 'ProfileItem/@AttributeKey'
					,cell												AS 'ProfileItem/@AttributeValue'
					, GETDATE()			AS 'ProfileItem/@CreateDate'
					, GETDATE()			AS 'ProfileItem/@ModifiedDate'
					, 'utility' AS 'ProfileItem/@Source'
									FOR XML PATH(''), TYPE, elements) 
									else null
					end as 'Account/Premise'

---------------------------------------------------------------		
			


						from InsightsBulk.dbo.ScgEnrollment c
						inner join InsightsDW.dbo.DimServicePoint sp
						on c.service_point_id=sp.ServicePointId						
						inner join InsightsDW.dbo.DimPremise dp
						on sp.PremiseKey=dp.PremiseKey
						inner join InsightsDW.dbo.FactCustomerPremise fcp
						on fcp.PremiseKey=dp.Premisekey
						inner join InsightsDW.dbo.DimCustomer cs
						on cs.CustomerKey=fcp.CustomerKey
		where	[CELL] is not null and cell !='' and cell !='SU'  and  @clientid=dp.ClientId
		--one time thing --remove
		--and not EXISTS
		--(
		--	select * from InsightsDW.dbo.FactPremiseAttribute fcp
		--	where fcp.ContentAttributeKey like 'paperreport.%.enrollmentstatus'
		--	and dp.PremiseKey=fcp.PremiseKey
		--)
		ORDER BY cs.CustomerId,	dp.AccountId, dp.PremiseId
		--
	FOR XML PATH('Customer'),ELEMENTS, root('Customers')
	),'<Customers xmlns="Aclara:Insights"></Customers>')  AS COL_XML 
END
GO
