SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Wayne
-- Create date: 09/21/2014
-- Description:	This will create customer, billing and profile xml
--				for input  into Linda's jobstream
-- =============================================
-- Changed by:	Wayne
-- Change date: 10/22/2014
-- Description:	filter for client id was missing
-- =============================================
-- Changed by:	Wayne
-- Change date: 12/09/2014
-- Description:	added phone numbers into xml for customers
-- =============================================
-- Changed by:	 Wayne
-- Change date: 2/27/2015
-- Description: Added code to add xml to indicate utility comnpany program membership attributes
--			 Also propagated "My account" enrollments into holding events table
--
-- for bug 56443
-- =============================================
CREATE PROCEDURE [dbo].[ConvertCsvToXML]
@DefaultCommodity INT,
@BillPeriodType INT,
@ClientId int
AS
BEGIN
	SET NOCOUNT ON;

declare @num TABLE (n int not null)

declare @i int
set @i = 0
while @i < 300
begin
 insert  @num VALUES (@i);
 set @i = @i + 1;
end


update dbo.RawCsvTemp       
	  SET program_1= CASE WHEN [1] IS NOT NULL then (SELECT ProfileAttributeKey from [InsightsMetadata].[dbo].[UtilityPrograms] u inner JOIN [InsightsMetadata].[cm].[TypeProfileAttribute] a    ON a.ProfileAttributeid=u.ProfileAttributeID WHERE  u.[UtilityProgramName]=p.[1]) end 
	  ,program_2= CASE WHEN [2] IS NOT NULL then (SELECT ProfileAttributeKey from [InsightsMetadata].[dbo].[UtilityPrograms] u inner JOIN [InsightsMetadata].[cm].[TypeProfileAttribute] a     ON a.ProfileAttributeid=u.ProfileAttributeID WHERE u.[UtilityProgramName]=p.[2]) end
	  ,program_3= CASE WHEN [3] IS NOT NULL then (SELECT ProfileAttributeKey from [InsightsMetadata].[dbo].[UtilityPrograms] u inner JOIN [InsightsMetadata].[cm].[TypeProfileAttribute] a     ON a.ProfileAttributeid=u.ProfileAttributeID WHERE u.[UtilityProgramName]=p.[3])  end
	  ,program_4= CASE WHEN [4] IS NOT NULL then (SELECT ProfileAttributeKey from [InsightsMetadata].[dbo].[UtilityPrograms] u inner JOIN [InsightsMetadata].[cm].[TypeProfileAttribute] a     ON a.ProfileAttributeid=u.ProfileAttributeID WHERE u.[UtilityProgramName]=p.[4]) end
	  ,program_5= CASE WHEN [5] IS NOT NULL then (SELECT ProfileAttributeKey from [InsightsMetadata].[dbo].[UtilityPrograms] u inner JOIN [InsightsMetadata].[cm].[TypeProfileAttribute] a     ON a.ProfileAttributeid=u.ProfileAttributeID WHERE u.[UtilityProgramName]=p.[5]) end
 
 FROM dbo.RawCsvTemp  a INNER join ( SELECT *
FROM
 (
select [customer_id]
      ,[premise_id]
      ,[mail_address_line_1]
      ,[mail_address_line_2]
      ,[mail_address_line_3]
      ,[mail_city]
      ,[mail_state]
      ,[mail_zip_code]
      ,[first_name]
      ,[last_name]
      ,[phone_1]
      ,[phone_2]
      ,[email]
      ,[customer_type]
      ,[account_id]
      ,[active_date]
      ,[inactive_date]
      ,[read_cycle]
      ,[rate_code]
      ,[service_point_id]
      ,[service_house_number]
      ,[service_street_name]
      ,[service_unit]
      ,[service_city]
      ,[service_state]
      ,[service_zip_code]
      ,[meter_type]
      ,[meter_units]
      ,[bldg_sq_foot]
      ,[year_built]
      ,[bedrooms]
      ,[assess_value]
      ,[usage_value]
      ,[date_to]
      ,[duration]
      ,[is_estimate]
      ,[usage_charge]
      ,[ClientId]     
     , substring([Programs] , start+2, endPos-Start-2) token
     , row_number() over(partition by 
	[customer_id]
      ,[premise_id]
      ,[mail_address_line_1]
      ,[mail_address_line_2]
      ,[mail_address_line_3]
      ,[mail_city]
      ,[mail_state]
      ,[mail_zip_code]
      ,[first_name]
      ,[last_name]
      ,[phone_1]
      ,[phone_2]
      ,[email]
      ,[customer_type]
      ,[account_id]
                         order by start) n
      from (
SELECT
       [customer_id]
      ,[premise_id]
      ,[mail_address_line_1]
      ,[mail_address_line_2]
      ,[mail_address_line_3]
      ,[mail_city]
      ,[mail_state]
      ,[mail_zip_code]
      ,[first_name]
      ,[last_name]
      ,[phone_1]
      ,[phone_2]
      ,[email]
      ,[customer_type]
      ,[account_id]
      ,[active_date]
      ,[inactive_date]
      ,[read_cycle]
      ,[rate_code]
      ,[service_point_id]
      ,[service_house_number]
      ,[service_street_name]
      ,[service_unit]
      ,[service_city]
      ,[service_state]
      ,[service_zip_code]
      ,[meter_type]
      ,[meter_units]
      ,[bldg_sq_foot]
      ,[year_built]
      ,[bedrooms]
      ,[assess_value]
      ,[usage_value]
      ,[date_to]
      ,[duration]
      ,[is_estimate]
      ,[usage_charge]
      ,[ClientId]
      ,[Programs]  --- this is d
     , n AS  start
     , charindex(',',[Programs],n+2) endPos
               from @num
        cross join 
( SELECT [customer_id]
      ,[premise_id]
      ,[mail_address_line_1]
      ,[mail_address_line_2]
      ,[mail_address_line_3]
      ,[mail_city]
      ,[mail_state]
      ,[mail_zip_code]
      ,[first_name]
      ,[last_name]
      ,[phone_1]
      ,[phone_2]
      ,[email]
      ,[customer_type]
      ,[account_id]
      ,[active_date]
      ,[inactive_date]
      ,[read_cycle]
      ,[rate_code]
      ,[service_point_id]
      ,[service_house_number]
      ,[service_street_name]
      ,[service_unit]
      ,[service_city]
      ,[service_state]
      ,[service_zip_code]
      ,[meter_type]
      ,[meter_units]
      ,[bldg_sq_foot]
      ,[year_built]
      ,[bedrooms]
      ,[assess_value]
      ,[usage_value]
      ,[date_to]
      ,[duration]
      ,[is_estimate]
      ,[usage_charge]
      ,[ClientId],
                         ',' + Programs +',' Programs
                      from  dbo.RawCsvTemp
				  where clientid=@clientid) m
              where n < len(Programs)-1
                and substring(Programs,n+1,1) = ','
			 AND clientid=@clientid) Programs
      ) pvt
Pivot ( max(token)for n in ([1],[2],[3],[4],[5]))r
) p
on p.premise_id=a.premise_id AND p.account_id=a.account_id

--update table only if programs change
UPDATE dbo.RawCsvTemp 
SET Last_Program_1=w.My_Account
, last_program_2=w.CARE
, last_program_3=w.Level_Pay_Plan
, last_program_4=w.Paperless
, last_program_5=w.Medical
FROM dbo.RawCsvTemp   e 
INNER JOIN insightsdw.[dbo].[vScgCurrentUtilityProgramEnrollmentAttribsWide] w
ON e.premise_id=w.PremiseId
AND e.account_id=w.accountid


/*

add enrollments to My account
to event table

cuurently we are only concerned with 
enrollments into My Account
*/
    DECLARE @TrackingID AS VARCHAR(50) = (select  CONVERT(VARCHAR(10), GETDATE(), 112) 
								    + right('0'+cast(datepart(hh, GETDATE()) as varchar(2)),2)
								    + right('0'+cast(datepart(mi, GETDATE()) as varchar(2)),2)
								    + right('0'+cast(datepart(ss, GETDATE()) as varchar(2)),2))

-- in this insert we want to find program enrollment additions events since last bill								    
   INSERT INTO Holding.events
        SELECT CAST(e.clientid AS INT)
             , Customer_Id
             , Account_Id
             , Premise_Id
		   , CAST((SUBSTRING(CAST(CAST(e.date_to AS DATE) AS VARCHAR(10)),1,7) + '-15') AS DATE) --should always be the 15th of month that the date_to is in
             , 3  -- this is the enrolled in My account event
             , null
             , null
		   , @TrackingID
		   , GETDATE()		    
	     FROM dbo.RawCsvTemp e		
          INNER JOIN InsightsDW.[dbo].[vScgAnalysisGroup] a
		ON a.ClientId=e.ClientId and  a.PremiseId=e.premise_id
		--INNER JOIN InsightsDW.dbo.factbilling fb
		--ON fb.PremiseId=e.premise_id
	     WHERE  @clientid=e.ClientId	     
		and
          (
		-- this identifies enrollment "My Account" event
	        ( (  e.program_1 IS NOT NULL AND program_1 !=''  AND e.program_1='utilityportal.enrollmentstatus') AND program_1 NOT IN ( COALESCE(Last_Program_1,''), COALESCE(Last_Program_2,''), COALESCE(Last_Program_3,''), COALESCE(Last_Program_4,''), COALESCE( Last_Program_5,'' )))
	    OR  ( (  e.program_2 IS NOT NULL AND program_2 !=''  AND e.program_1='utilityportal.enrollmentstatus') AND program_2 NOT IN ( COALESCE(Last_Program_1,''), COALESCE(Last_Program_2,''), COALESCE(Last_Program_3,''), COALESCE(Last_Program_4,''), COALESCE( Last_Program_5,'' )))
	    OR  ( (  e.program_3 IS NOT NULL AND program_3 !=''  AND e.program_1='utilityportal.enrollmentstatus') AND program_3 NOT IN ( COALESCE(Last_Program_1,''), COALESCE(Last_Program_2,''), COALESCE(Last_Program_3,''), COALESCE(Last_Program_4,''), COALESCE( Last_Program_5,'' )))
	    OR  ( (  e.program_4 IS NOT NULL AND program_4 !=''  AND e.program_1='utilityportal.enrollmentstatus') AND program_4 NOT IN ( COALESCE(Last_Program_1,''), COALESCE(Last_Program_2,''), COALESCE(Last_Program_3,''), COALESCE(Last_Program_4,''), COALESCE( Last_Program_5,'' )))
	    OR  ( (  e.program_5 IS NOT NULL AND program_5 !=''  AND e.program_1='utilityportal.enrollmentstatus') AND program_5 NOT IN ( COALESCE(Last_Program_1,''), COALESCE(Last_Program_2,''), COALESCE(Last_Program_3,''), COALESCE(Last_Program_4,''), COALESCE( Last_Program_5,'' )))	    
	    ----										
		)

-- outer select
;WITH XMLNAMESPACES (DEFAULT 'Aclara:Insights')
select
( --first column
	
		select 
	isnull(	 
	
	( SELECT 
		Customer_ID										AS '@CustomerId',									-- Customer attributes
		first_name										AS '@FirstName',									-- Customer attributes
		last_name										AS '@LastName',										-- Customer attributes
		mail_address_line_1								AS '@Street1',										-- Customer attributes
		mail_city										AS '@City',											-- Customer attributes
		mail_state										AS '@State',										-- Customer attributes
		'US'											AS '@Country',										-- Customer attributes
		CASE WHEN LEN(LTRIM(RTRIM(mail_zip_code))) =5 
			THEN  CAST(SUBSTRING(mail_zip_code,1,5) 
			AS VARCHAR(5))
		ELSE
			CAST((SUBSTRING(mail_zip_code,1,5)+
			SUBSTRING(mail_zip_code,7,4)) 
			AS VARCHAR(9))
		end													 AS '@PostalCode',									-- Customer attributes
		NULLIF(REPLACE(REPLACE(REPLACE(phone_1, '(',''), ')',''),' ','')	,'')	 AS '@PhoneNumber',									-- Customer attributes,
		NULLIF(REPLACE(REPLACE(REPLACE(phone_2, '(',''), ')',''),' ','')	,'')	 AS '@MobilePhoneNumber',							-- Customer attributes
		CASE WHEN LEN(email) <6 
			THEN NULL
		else
			COALESCE(NULLIF(LTRIM(RTRIM(email)),''), email)	
		end												AS '@EmailAddress',									-- Customer attributes
		NULL											AS '@AlternateEmailAddress',						-- Customer attributes
		'utility'										AS '@Source',										-- Customer attributes
		'false'											AS '@IsBusiness',									-- Customer attributes
		'true'											AS '@IsAuthenticated',								-- Customer attributes
					
		Account_ID										AS 'Account/@AccountId',							-- Account attributes	

		premise_id										AS 'Account/Premise/@PremiseId',					-- Premise attributes
		LTRIM(RTRIM(service_house_number)) + 
		 RTRIM(ISNULL(' ' + service_street_name, '')) +
		 RTRIM(ISNULL(' ' + service_unit,''))			AS 'Account/Premise/@PremiseStreet1',				-- Premise attributes
		service_city									AS 'Account/Premise/@PremiseCity',					-- Premise attributes
		CASE 
		 WHEN LEN(LTRIM(RTRIM(service_zip_code))) <5 
		  THEN null
		 WHEN LEN(LTRIM(RTRIM(service_zip_code))) =5 
		  THEN  CAST(SUBSTRING(service_zip_code,1,5) 
		   AS VARCHAR(5))
		 WHEN LEN(LTRIM(RTRIM(service_zip_code))) =10 
		  THEN CAST((SUBSTRING(service_zip_code,1,5)
		   +SUBSTRING(service_zip_code,7,4)) 
		   AS VARCHAR(9))
		 ELSE null
		end												AS 'Account/Premise/@PremisePostalCode',			-- Premise attributes
		CAST(LTRIM(RTRIM(service_state)) AS varchar(2)) AS 'Account/Premise/@PremiseState',					-- Premise attributes
		'US'											AS 'Account/Premise/@PremiseCountry',				-- Premise attributes
		'false'											AS 'Account/Premise/@HasElectricService',			-- Premise attributes
		'true'											AS 'Account/Premise/@HasGasService',				-- Premise attributes
		'false'											AS 'Account/Premise/@HasWaterService'				-- Premise attributes
		from dbo.RawCsvTemp
		where @clientid=ClientId	
			FOR XML PATH('Customer'),ELEMENTS, root('Customers'))
		,'<Customers xmlns="Aclara:Insights"></Customers>')   ) AS COL_XML_CUSTOMER ,
(--second column

select 
	isnull(
	(	 
	SELECT 
					Customer_ID										AS '@CustomerId',									-- Customer attributes
					'customer'										AS '@Source',										-- Customer attributes
					Account_ID										AS 'Account/@AccountId',							-- Account attributes
					@BillPeriodType									AS 'Account/Bill/@BillPeriodType',					-- Bill attributes
					convert(varchar,DATEADD(d,(CAST(duration AS INT)*-1),CAST(date_to AS DATETIME)),126) 
																	as 'Account/Bill/@StartDate',						-- Bill attributes
					convert(varchar,CAST(date_to AS DATETIME),126)	as 'Account/Bill/@EndDate',							-- Bill attributes
					premise_id										AS 'Account/Bill/Premise/@PremiseId',				-- Premise attributes
					CASE WHEN ISNUMERIC(usage_value)=1 then CAST(usage_value AS DECIMAL(18,2)) 
															ELSE 0 
																END AS 'Account/Bill/Premise/Service/@TotalUsage',		-- Service attributes
					CASE WHEN ISNUMERIC(usage_charge)=1 then CAST(usage_charge AS DECIMAL(18,2)) 
															ELSE 0 
																END AS  'Account/Bill/Premise/Service/@TotalCost',		-- Service attributes
					@DefaultCommodity AS 'Account/Bill/Premise/Service/@Commodity',										-- Service attributes
					CASE WHEN @DefaultCommodity=1 THEN '2' 
							WHEN @DefaultCommodity=2 THEN '6' 
																END as 'Account/Bill/Premise/Service/@UOM',										-- Service attributes
					service_point_id								as 'Account/Bill/Premise/Service/@ServicePointId',	-- Service attributes
					convert(varchar,DATEADD(d,(CAST(duration AS INT)*-1),CAST(date_to AS DATETIME)),126) 
																	as 'Account/Bill/Premise/Service/@AMIStartDate',	-- Service attributes
					convert(varchar,CAST(date_to AS DATETIME),126)	as 'Account/Bill/Premise/Service/@AMIEndDate',		-- Service attributes
					rate_code										as 'Account/Bill/Premise/Service/@RateClass'		-- Service attributes										 	 
		    FROM dbo.RawCsvTemp
			where @clientid=ClientId	
			FOR XML PATH('Customer'),ELEMENTS, root('Customers'))
		,'<Customers xmlns="Aclara:Insights"></Customers>') ) AS COL_XML_BILLING,
(--third column	
	select 
	isnull(
	(	 
	SELECT 
			Customer_ID						AS '@CustomerId',				-- Customer attributes
			Account_ID						AS 'Account/@AccountId',		-- Account attributes
			Premise_ID						AS 'Account/Premise/@PremiseId'	-- Premise attributes
							
			,-- yearbuilt	
								
					case when year_built IS NOT NULL AND year_built !='' AND LEN(ltrim(RTRIM(year_built)))=4 
					AND ISNUMERIC(ltrim(RTRIM(year_built)))=1
					AND CAST(ltrim(RTRIM(year_built)) AS int) > 1800 then 
					(SELECT
					-- ProfileItem attributes
						'house.yearbuilt'	AS 'ProfileItem/@AttributeKey'
					, year_built			AS 'ProfileItem/@AttributeValue'
					, GETDATE()				AS 'ProfileItem/@CreateDate'
					, GETDATE()				AS 'ProfileItem/@ModifiedDate'
					, 'utility'				AS 'ProfileItem/@Source'
					FOR XML PATH(''), 
					TYPE, elements) 
					else null
					end						AS 'Account/Premise'
					--
							
									
			,--   bldg_sq_foot  == house.totalarea
							
					case when bldg_sq_foot IS NOT NULL AND bldg_sq_foot !='' AND ISNUMERIC(bldg_sq_foot) =1
					AND CAST(ltrim(RTRIM(bldg_sq_foot)) AS int) > 0 then 
					(SELECT
					-- ProfileItem attributes
						'house.totalarea'	AS 'ProfileItem/@AttributeKey'
					, bldg_sq_foot			AS 'ProfileItem/@AttributeValue'
					, GETDATE()				AS 'ProfileItem/@CreateDate'
					, GETDATE()				AS 'ProfileItem/@ModifiedDate'
					, 'utility'				AS 'ProfileItem/@Source'
					FOR XML PATH(''), 
					TYPE, elements) 
					else null
					end as 'Account/Premise'	

		  -- add new programs

				,--   program1
							
					case when  ( (  program_1 IS NOT NULL AND program_1 !='' ) AND program_1 NOT IN ( COALESCE(Last_Program_1,''), COALESCE(Last_Program_2,''), COALESCE(Last_Program_3,''), COALESCE(Last_Program_4,''), COALESCE( Last_Program_5,'' )))
					 then 
					(SELECT
					-- ProfileItem attributes					
						program_1	AS 'ProfileItem/@AttributeKey'
					, program_1 + '.enrolled'			AS 'ProfileItem/@AttributeValue'
					, GETDATE()				AS 'ProfileItem/@CreateDate'
					, GETDATE()				AS 'ProfileItem/@ModifiedDate'
					, 'utility'				AS 'ProfileItem/@Source'
					FOR XML PATH(''), 
					TYPE, elements) 
					else null
					end as 'Account/Premise'	
			,--   program 2
							
					case when  ( (  program_2 IS NOT NULL AND program_2 !='' ) AND program_2 NOT IN ( COALESCE(Last_Program_1,''), COALESCE(Last_Program_2,''), COALESCE(Last_Program_3,''), COALESCE(Last_Program_4,''), COALESCE( Last_Program_5,'' )))
					 then 
					(SELECT
					-- ProfileItem attributes					
						program_2	AS 'ProfileItem/@AttributeKey'
					, program_2 + '.enrolled'			AS 'ProfileItem/@AttributeValue'
					, GETDATE()				AS 'ProfileItem/@CreateDate'
					, GETDATE()				AS 'ProfileItem/@ModifiedDate'
					, 'utility'				AS 'ProfileItem/@Source'
					FOR XML PATH(''), 
					TYPE, elements) 
					else null
					end as 'Account/Premise'	

			

			,--   program 3
							
					case when  ( (  program_3 IS NOT NULL AND program_3 !='' ) AND program_3 NOT IN ( COALESCE(Last_Program_1,''), COALESCE(Last_Program_2,''), COALESCE(Last_Program_3,''), COALESCE(Last_Program_4,''), COALESCE( Last_Program_5,'' )))
					 then 
					(SELECT
					-- ProfileItem attributes					
						program_3	AS 'ProfileItem/@AttributeKey'
					, program_3 + '.enrolled'			AS 'ProfileItem/@AttributeValue'
					, GETDATE()				AS 'ProfileItem/@CreateDate'
					, GETDATE()				AS 'ProfileItem/@ModifiedDate'
					, 'utility'				AS 'ProfileItem/@Source'
					FOR XML PATH(''), 
					TYPE, elements) 
					else null
					end as 'Account/Premise'	

			,--   program 4
							
					case when  ( (  program_4 IS NOT NULL AND program_4 !='' ) AND program_4 NOT IN ( COALESCE(Last_Program_1,''), COALESCE(Last_Program_2,''), COALESCE(Last_Program_3,''), COALESCE(Last_Program_4,''), COALESCE( Last_Program_5,'' )))
					 then 
					(SELECT
					-- ProfileItem attributes					
						program_4	AS 'ProfileItem/@AttributeKey'
					, program_4 + '.enrolled'			AS 'ProfileItem/@AttributeValue'
					, GETDATE()				AS 'ProfileItem/@CreateDate'
					, GETDATE()				AS 'ProfileItem/@ModifiedDate'
					, 'utility'				AS 'ProfileItem/@Source'
					FOR XML PATH(''), 
					TYPE, elements) 
					else null
					end as 'Account/Premise'	

			,--   program 5
							
					case when  ( (  program_5 IS NOT NULL AND program_5 !='' ) AND program_5 NOT IN ( COALESCE(Last_Program_1,''), COALESCE(Last_Program_2,''), COALESCE(Last_Program_3,''), COALESCE(Last_Program_4,''), COALESCE( Last_Program_5,'' )))
					 then 
					(SELECT
					-- ProfileItem attributes					
						program_5	AS 'ProfileItem/@AttributeKey'
					, program_5 + '.enrolled'			AS 'ProfileItem/@AttributeValue'
					, GETDATE()				AS 'ProfileItem/@CreateDate'
					, GETDATE()				AS 'ProfileItem/@ModifiedDate'
					, 'utility'				AS 'ProfileItem/@Source'
					FOR XML PATH(''), 
					TYPE, elements) 
					else null
					end as 'Account/Premise'	

	   -- Add attributes to de-enroll from programs

			,--   program1
							
					case when ( (  Last_Program_1 IS NOT NULL AND Last_Program_1 !='' ) AND Last_Program_1 NOT IN ( COALESCE(Program_1,''), COALESCE(Program_2,''), COALESCE(Program_3,''), COALESCE(Program_4,''), COALESCE( Program_5,'' )))			
					 then 
					(SELECT
					-- ProfileItem attributes					
						last_program_1	AS 'ProfileItem/@AttributeKey'
					, last_program_1 + '.notenrolled'			AS 'ProfileItem/@AttributeValue'
					, GETDATE()				AS 'ProfileItem/@CreateDate'
					, GETDATE()				AS 'ProfileItem/@ModifiedDate'
					, 'utility'				AS 'ProfileItem/@Source'
					FOR XML PATH(''), 
					TYPE, elements) 
					else null
					end as 'Account/Premise'	

			,--   program 2
							
					case when  ( (  Last_Program_2 IS NOT NULL AND Last_Program_2 !='' ) AND Last_Program_2 NOT IN ( COALESCE(Program_1,''), COALESCE(Program_2,''), COALESCE(Program_3,''), COALESCE(Program_4,''), COALESCE( Program_5,'' )))			
					 then 
					(SELECT
					-- ProfileItem attributes					
						last_program_2	AS 'ProfileItem/@AttributeKey'
					, last_program_2 + '.notenrolled'			AS 'ProfileItem/@AttributeValue'
					, GETDATE()				AS 'ProfileItem/@CreateDate'
					, GETDATE()				AS 'ProfileItem/@ModifiedDate'
					, 'utility'				AS 'ProfileItem/@Source'
					FOR XML PATH(''), 
					TYPE, elements) 
					else null
					end as 'Account/Premise'	

			,--   program 3
							
					case when ( (  Last_Program_3 IS NOT NULL AND Last_Program_3 !='' ) AND Last_Program_3 NOT IN ( COALESCE(Program_1,''), COALESCE(Program_2,''), COALESCE(Program_3,''), COALESCE(Program_4,''), COALESCE( Program_5,'' )))			
					 then 
					(SELECT
					-- ProfileItem attributes					
						last_program_3	AS 'ProfileItem/@AttributeKey'
					, last_program_3 + '.notenrolled'			AS 'ProfileItem/@AttributeValue'
					, GETDATE()				AS 'ProfileItem/@CreateDate'
					, GETDATE()				AS 'ProfileItem/@ModifiedDate'
					, 'utility'				AS 'ProfileItem/@Source'
					FOR XML PATH(''), 
					TYPE, elements) 
					else null
					end as 'Account/Premise'	

			,--   program 4
							
					case when  ( (  Last_Program_4 IS NOT NULL AND Last_Program_4 !='' ) AND Last_Program_4 NOT IN ( COALESCE(Program_1,''), COALESCE(Program_2,''), COALESCE(Program_3,''), COALESCE(Program_4,''), COALESCE( Program_5,'' )))			
					 then 
					(SELECT
					-- ProfileItem attributes					
						last_program_4	AS 'ProfileItem/@AttributeKey'
					, last_program_4 + '.notenrolled'			AS 'ProfileItem/@AttributeValue'
					, GETDATE()				AS 'ProfileItem/@CreateDate'
					, GETDATE()				AS 'ProfileItem/@ModifiedDate'
					, 'utility'				AS 'ProfileItem/@Source'
					FOR XML PATH(''), 
					TYPE, elements) 
					else null
					end as 'Account/Premise'	

			,--   program 5
							
					case when  ( (  Last_Program_5 IS NOT NULL AND Last_Program_5 !='' ) AND Last_Program_5 NOT IN ( COALESCE(Program_1,''), COALESCE(Program_2,''), COALESCE(Program_3,''), COALESCE(Program_4,''), COALESCE( Program_5,'' )))			
					 then 
					(SELECT
					-- ProfileItem attributes					
						last_program_5	AS 'ProfileItem/@AttributeKey'
					, last_program_5 + '.notenrolled'			AS 'ProfileItem/@AttributeValue'
					, GETDATE()				AS 'ProfileItem/@CreateDate'
					, GETDATE()				AS 'ProfileItem/@ModifiedDate'
					, 'utility'				AS 'ProfileItem/@Source'
					FOR XML PATH(''), 
					TYPE, elements) 
					else null
					end as 'Account/Premise'	

		from dbo.RawCsvTemp 
		where  @clientid=ClientId	
		AND (
			
		--
		(bldg_sq_foot IS NOT NULL AND bldg_sq_foot !='' AND ISNUMERIC(bldg_sq_foot) =1
											AND CAST(ltrim(RTRIM(bldg_sq_foot)) AS int) > 0  )	
		--
		or 																				
				(year_built IS NOT NULL AND year_built !='' AND LEN(ltrim(RTRIM(year_built)))=4 
													AND ISNUMERIC(ltrim(RTRIM(year_built)))=1
											AND CAST(ltrim(RTRIM(year_built)) AS int) > 1800 )
											     
		-- this identifies enrollment 
	    OR  ( (  program_1 IS NOT NULL AND program_1 !='') AND program_1 NOT IN ( COALESCE(Last_Program_1,''), COALESCE(Last_Program_2,''), COALESCE(Last_Program_3,''), COALESCE(Last_Program_4,''), COALESCE( Last_Program_5,'' )))
	    OR  ( (  program_2 IS NOT NULL AND program_2 !='') AND program_2 NOT IN ( COALESCE(Last_Program_1,''), COALESCE(Last_Program_2,''), COALESCE(Last_Program_3,''), COALESCE(Last_Program_4,''), COALESCE( Last_Program_5,'' )))
	    OR  ( (  program_3 IS NOT NULL AND program_3 !='') AND program_3 NOT IN ( COALESCE(Last_Program_1,''), COALESCE(Last_Program_2,''), COALESCE(Last_Program_3,''), COALESCE(Last_Program_4,''), COALESCE( Last_Program_5,'' )))
	    OR  ( (  program_4 IS NOT NULL AND program_4 !='') AND program_4 NOT IN ( COALESCE(Last_Program_1,''), COALESCE(Last_Program_2,''), COALESCE(Last_Program_3,''), COALESCE(Last_Program_4,''), COALESCE( Last_Program_5,'' )))
	    OR  ( (  program_5 IS NOT NULL AND program_5 !='') AND program_5 NOT IN ( COALESCE(Last_Program_1,''), COALESCE(Last_Program_2,''), COALESCE(Last_Program_3,''), COALESCE(Last_Program_4,''), COALESCE( Last_Program_5,'' )))
	    												
	    ---- this identifies ones they had but no longer have
	    OR ( (  Last_Program_1 IS NOT NULL AND Last_Program_1 !='' ) AND Last_Program_1 NOT IN ( COALESCE(Program_1,''), COALESCE(Program_2,''), COALESCE(Program_3,''), COALESCE(Program_4,''), COALESCE( Program_5,'' )))	
	    OR ( (  Last_Program_2 IS NOT NULL AND Last_Program_2 !='' ) AND Last_Program_2 NOT IN ( COALESCE(Program_1,''), COALESCE(Program_2,''), COALESCE(Program_3,''), COALESCE(Program_4,''), COALESCE( Program_5,'' )))	
	    OR ( (  Last_Program_3 IS NOT NULL AND Last_Program_3 !='' ) AND Last_Program_3 NOT IN ( COALESCE(Program_1,''), COALESCE(Program_2,''), COALESCE(Program_3,''), COALESCE(Program_4,''), COALESCE( Program_5,'' )))	
	    OR ( (  Last_Program_4 IS NOT NULL AND Last_Program_4 !='' ) AND Last_Program_4 NOT IN ( COALESCE(Program_1,''), COALESCE(Program_2,''), COALESCE(Program_3,''), COALESCE(Program_4,''), COALESCE( Program_5,'' )))	
	    OR ( (  Last_Program_5 IS NOT NULL AND Last_Program_5 !='' ) AND Last_Program_5 NOT IN ( COALESCE(Program_1,''), COALESCE(Program_2,''), COALESCE(Program_3,''), COALESCE(Program_4,''), COALESCE( Program_5,'' )))		
											
		)
	FOR XML PATH('Customer'),ELEMENTS, root('Customers')
	),'<Customers xmlns="Aclara:Insights"></Customers>')  ) AS COL_XML_PROFILE 


END
GO
