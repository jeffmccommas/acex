SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ubaid
-- Create date: 1/15/2015
-- Description:	maps measurename to actionkey based on xml configuration
-- =============================================
CREATE PROCEDURE [dbo].[TransformActionItemData] @ClientId AS INT
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @XmlConfiguration XML

        EXEC [insightsmetadata].[dbo].[GetComponentXmlConfiguration] @ClientId = @ClientId,
            @ComponentName = N'preprocess.actionitem',
            @XmlConfiguration = @XmlConfiguration OUTPUT;
            
        WITH    Mapping
                  AS ( SELECT   x.r.value('@key[1]', 'varchar(100)') AS ProductDescription ,
                                x.r.value('text()[1]', 'varchar(100)') AS ActionKey
                       FROM     @XmlConfiguration.nodes('/ActionItemMapping/MapInfo')
                                AS x ( r )
                     )
            UPDATE  t
            SET     t.actionkey = m.ActionKey
            FROM    insightsbulk.dbo.RawActionTemp t
                    INNER JOIN Mapping m ON m.ProductDescription = t.measurename
            WHERE   t.clientid = @ClientId;
     
    END

GO
