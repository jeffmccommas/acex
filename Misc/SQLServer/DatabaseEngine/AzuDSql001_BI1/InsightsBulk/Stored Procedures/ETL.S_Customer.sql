SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/21/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[S_Customer]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;

		SELECT ClientId, 
				 CustomerId, 
				 FirstName, 
				 LastName, 
				 Street1, 
				 Street2, 
				 City, 
				 StateProvince,
				 Country, 
				 PostalCode, 
				 MobilePhoneNumber, 
				 PhoneNumber, 
				 EmailAddress, 
				 AlternateEmailAddress, 
				 SourceId, 
				 AuthenticationTypeId, 
				 TrackingId, 
				 TrackingDate
		FROM
		(SELECT  ClientId, 
				 CustomerId, 
				 FirstName, 
				 LastName, 
				 Street1, 
				 Street2, 
				 City, 
				 StateProvince,
				 Country, 
				 PostalCode, 
				 MobilePhoneNumber, 
				 PhoneNumber, 
				 EmailAddress, 
				 AlternateEmailAddress, 
				 SourceId, 
				 AuthenticationTypeId, 
				 TrackingId, 
				 TrackingDate
				,ROW_NUMBER() OVER ( PARTITION BY 
				ClientId, CustomerId
				ORDER BY TrackingDate DESC ) AS SortId
		FROM Holding.v_Customer
		WHERE ClientId = @ClientID) b
		WHERE SortId = 1
		ORDER BY ClientId, CustomerId


END





GO
