
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ===================================================================================================================================================================
-- Author:		ECormier
-- Description:	gets configurations for Exact Target import files for ImportExactTarget_Daily ssis package
-- Create date:	7/2015
-- Update date: 8/4/2015 ECormier - add paasPassword column to @tmpFilesToImport temp table because that has been added to GetComponentConfiguration sp
-- Update date: 4/14/2016 ECormier - add modify to add date based on replacing *strDate value stored in cm.clientconfiguration
-- Update date: 2/2/2017 ECormier - add dbstructureid to temp table because it it being returned by [dbo].[GetComponentConfiguration] for all packages/configurations
-- ===================================================================================================================================================================
if exists (select * from sysobjects where type = 'p' and name = 'getETFilesToImport')
drop procedure getETFilesToImport
go


create procedure [dbo].[getETFilesToImport] @clientID int
as

set nocount on

/*
-- for debug
declare @clientID int
select @clientID = 101
*/

-- 7/21/2015 - move setting date and appending date to file names out of ssis package in into sp
declare @dateToUse date
select @dateToUse = dateadd(dd,-1,getdate())

declare @addDateExt varchar(20)
declare @AddYear varchar(4)
declare @AddMonth varchar(3)
declare @AddDay varchar(3)

-- set year string
select @AddYear = (select convert(varchar(4),year(@dateToUse)))

-- set month string
select @AddMonth = (select convert(varchar(2),month(@dateToUse)))
if len(@AddMonth) = 1 select @AddMonth = '0' + @AddMonth

-- set day string
select @AddDay = (select convert(varchar(2),day(@dateToUse)))
if len(@AddDay) = 1 select @AddDay = '0' + @AddDay

-- put them together to get the date suffix to add to file names
select @addDateExt = @AddYear + @AddMonth + @AddDay

-- create a temp table to hold the configurations for files to be imported
	declare @tmpFilesToImport table
	(dbstructureid varchar(255),
	ETEventsArchiveFolder varchar(255),
	ETEventsDestExtension varchar(10),	
	ETEventsDestFileName varchar(50),	
	ETEventsDestFilePath varchar(255),	
	ETEventsDestTable varchar(25),	
	ETEventsSrcExtension varchar(10),	
	ETEventsSrcFileName varchar(50),	
	ETEventsSrcFilePath varchar(255),	
	ETEventsSrcPassword varchar(25),	
	ETEventsSrcServer varchar(50),	
	ETEventsSrcUserName varchar(25),
	ETEventsTransExtension varchar(10),
	ETEventsTransFileName varchar(50),
	ETEventsTransFilePath varchar(255),
	paasPassword varchar(50),		
	reportmonth varchar(5), 	
	reportyear varchar(5),	
	sharedlocation varchar(255))

	-- get the configurations for the ClickOpen Events file
	insert into @tmpFilesToImport 
	EXECUTE [dbo].[GetComponentConfiguration] @clientID, 'ImportETEventsClickOpen'

	-- get the configurations for the BouncOptout Events file
	insert into @tmpFilesToImport 
	EXECUTE [dbo].[GetComponentConfiguration] @clientID, 'ImportETEventsBounce'

	-- get the configurations for the ImportFailure Events file
	insert into @tmpFilesToImport 
	EXECUTE [dbo].[GetComponentConfiguration] @clientID, 'ImportETEventsImportFail'

	-- 12/21/2015 - additional optout file from ET, program only optouts
	-- get the configurations for the Program only optout Events file
	insert into @tmpFilesToImport 
	EXECUTE [dbo].[GetComponentConfiguration] @clientID, 'ImportETEventsProgOptout'

	-- 11/20/2015 - use this until data can get into contentful
	/*insert into @tmpFilesToImport
	select ETEventsArchiveFolder,ETEventsDestExtension,'ETImportFail',ETEventsDestFilePath,'ETImportFailFile','.txt','EmailImportFail','\PreProcess\BI\ETImportFail\',ETEventsSrcPassword,
		ETEventsSrcServer,ETEventsSrcUserName,ETEventsTransExtension,'ImportEvents_ETImportFail',ETEventsTransFilePath,paasPassword,reportmonth,reportyear,sharedlocation
	from @tmpFilesToImport
	where ETEventsDestTable = 'ETClickOpen'
	*/

	-- update the file names to add the date extentions where needed
	update @tmpFilesToImport
	set ETEventsSrcFileName = case when eteventsdestfilename <> 'ETBounceOptOut' then ETEventsSrcFileName + @addDateExt else ETEventsSrcFileName end,
		ETEventsDestFileName = ETEventsDestFileName + @addDateExt + '_' + convert(varchar(10),@clientID),
		ETEventsTransFileName = ETEventsTransFileName + @addDateExt + '_' + convert(varchar(10),@clientID)

	-- 4/14/2016 - need to update cm.clientconfiguration to add '*strDate' to files that need date appended to them. Change was made to UIL optout file in ET, but not SoCalGas to add date
	-- 4/15/2016 - possible future change, ET is not changing their file format now
	/*
	update @tmpFilesToImport
	set ETEventsSrcFileName = replace(ETEventsSrcFileName,'*strDate',@addDateExt), 
	ETEventsDestFileName = replace(ETEventsDestFileName,'*strDate',@addDateExt) + '_' + convert(varchar(10),@clientID), 
	ETEventsTransFileName = replace(ETEventsTransFileName,'*strDate',@addDateExt) + '_' + convert(varchar(10),@clientID)
	*/

	-- return the results of query to ssis package 
	select * from @tmpFilesToImport 

set nocount off
GO
