SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Wayne
-- Create date:	1.20.2015
-- Updated: 7/16/2015 - Elaine - insert FlagDateUpdated to holding.Events.EventDate instead of getdate() since FlagDateUpdated was the date the event actually happened
-- Updated: 9/14/2015 - Elaine - remove database name in order to use on paaS shards
-- Description:	Validates and transforms Web Events from Bulk to Holding
-- =============================================
if exists (select * from sysobjects where type = 'p' and name = 'CopyEventsToHolding')
drop procedure  [dbo].[CopyEventsToHolding]
go

CREATE PROCEDURE [dbo].[CopyEventsToHolding]
     @ClientId int
AS
BEGIN
    DECLARE @DailyRowTotalsCount int;
    DECLARE @TrackingID AS VARCHAR(50) = (select  CONVERT(VARCHAR(10), GETDATE(), 112) 
								    + right('0'+cast(datepart(hh, GETDATE()) as varchar(2)),2)
								    + right('0'+cast(datepart(mi, GETDATE()) as varchar(2)),2)
								    + right('0'+cast(datepart(ss, GETDATE()) as varchar(2)),2))
    BEGIN TRY
	   -- start a transaction
	   BEGIN TRAN
	   -- copy rows from bulk to holding
        INSERT INTO Holding.events
        SELECT CAST(referrerid AS INT)
             , CustomerId
             , AccountId
             , PremiseId
		   	 , convert(DATE,FlagDateUpdated) --, GETDATE()  
             , COALESCE((SELECT t.Eventtypeid FROM .dbo.dimeventtype t WHERE e.eventtype=  t.EventKey), 0)
             , convert(DATETIME2,FlagDateCreated)
             , convert(DATETIME2,FlagDateUpdated)
		   , @TrackingID
		   , GETDATE()
	     FROM dbo.Events e
          WHERE CAST(referrerid AS INT) = @ClientId;
	   
	   -- clean  up
	   DELETE dbo.Events 
	   WHERE CAST(referrerid AS INT) = @ClientId;
	   -- set the counters to send back to SSIS
        SET @DailyRowTotalsCount = ISNULL(@@ROWCOUNT , 0);
        SELECT @DailyRowTotalsCount AS DailyRowTotalsCount;
	   
	   -- commit the transaction all was good
	   COMMIT TRAN
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRANSACTION
            END;

        DECLARE @ErrorNumber int = ERROR_NUMBER()
               ,@ErrorLine int = ERROR_LINE()
               ,@ErrorMessage nvarchar(4000) = ERROR_MESSAGE()
               ,@ErrorSeverity int = ERROR_SEVERITY()
               ,@ErrorState int = ERROR_STATE();

        PRINT 'Actual error number: ' + CAST(@ErrorNumber AS varchar(10));
        PRINT 'Actual line number: ' + CAST(@ErrorLine AS varchar(10));
	   SET @DailyRowTotalsCount =0
        SELECT 0 AS DailyRowTotalsCount;

        RAISERROR(@ErrorMessage , @ErrorSeverity , @ErrorState);
        RETURN -1;
    END CATCH;
    RETURN 0;
END;
GO
