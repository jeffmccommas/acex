SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/27/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[U_PremiseAttributes]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;

		DECLARE @TrackingDate DATETIME = GETUTCDATE()

		UPDATE Holding.PremiseAttributes
		SET  OptionValue = epa.OptionValue,
		     SourceId = epa.SourceId ,
		     TrackingID = CONVERT(VARCHAR(10),@TrackingDate,112) + REPLACE(CONVERT(VARCHAR(10),@TrackingDate,108),':',''),
		     TrackingDate = @TrackingDate
		FROM Holding.PremiseAttributes hpa
		INNER JOIN ETL.PremiseAttributes_UPD epa ON epa.ClientID = hpa.ClientID
													AND epa.CustomerID = hpa.CustomerID
													AND epa.AccountID = hpa.AccountID
													AND epa.PremiseID = hpa.PremiseID
													AND epa.AttributeKey = hpa.AttributeKey
		WHERE epa.ClientID = @ClientID


END





GO
