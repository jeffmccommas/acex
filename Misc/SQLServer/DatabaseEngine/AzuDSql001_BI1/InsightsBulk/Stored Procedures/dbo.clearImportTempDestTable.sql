if exists (select * from sysobjects where type = 'p' and name = 'clearImportTempDestTable')
drop procedure clearImportTempDestTable
go


SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [dbo].[clearImportTempDestTable] @clientID int, @ETFileType varchar(30)
as

set nocount on

if @ETFileType = 'ETClickOpen'
begin

	--truncate table InsightsBulk.dbo.ETClickOpen 
	delete from dbo.ETClickOpen where referrerID = @clientID

end

if @ETFileType = 'ETBounceOptOut'
begin

	--truncate table InsightsBulk.dbo.ETBounceOptOut
	delete from dbo.ETBounceOptOut where referrerID = @clientID
	 
end

set nocount off
GO
