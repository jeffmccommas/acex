SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [Holding].[MergePremiseAttributes]
    @PremiseAttributes AS [dbo].[PremiseAttributeTableType] READONLY
AS 
    BEGIN
	
        SET NOCOUNT ON;

        BEGIN TRY
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
			   
			BEGIN TRANSACTION
      
            MERGE [Holding].[PremiseAttributes] AS target
                USING 
                    ( SELECT    p.ClientID ,
                                p.CustomerID ,
                                p.AccountID ,
                                p.PremiseID ,
                                pa.AttributeKey ,
                                pa.OptionValue
                      FROM      @PremiseAttributes pa
                                INNER JOIN [Holding].[Premise] p ON p.PremiseRowId = pa.RowId
                      WHERE     pa.OptionValue IS NOT NULL
                    ) AS source ( ClientID, CustomerID, AccountID, PremiseID,
                                  AttributeKey, OptionValue )
                ON ( target.ClientID = source.ClientID
                     AND target.CustomerID = source.CustomerID
                     AND target.AccountID = source.AccountID
                     AND target.PremiseID = source.PremiseID
                     AND target.AttributeKey = source.AttributeKey
                   )
                WHEN MATCHED 
                    THEN UPDATE
                         SET    TARGET.OptionValue = SOURCE.OptionValue;

            COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
			IF @@TRANCOUNT > 0 
                ROLLBACK TRANSACTION

			THROW;
        END CATCH      
    END



GO
