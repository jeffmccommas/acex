SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/21/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[S_Premise]
				 @ClientID INT
AS

BEGIN

	SET NOCOUNT ON;

	--SELECT * 
	--FROM Holding.v_Premise 
	--WHERE ClientId = @ClientID
	--ORDER By ClientId, PremiseId, AccountId


	SELECT ClientId, PremiseId, AccountId, Street1, Street2, City, StateProvince, Country, PostalCode, GasService, ElectricService, WaterService, SourceId, TrackingId, TrackingDate
	FROM
	(SELECT  ClientId, PremiseId, AccountId, Street1, Street2, City, StateProvince, Country, PostalCode, GasService, ElectricService, WaterService, SourceId, TrackingId, TrackingDate,
			ROW_NUMBER() OVER ( PARTITION BY 
			ClientId, 
			AccountId, 
			PremiseId 
			ORDER BY TrackingDate DESC ) AS SortId
		FROM Holding.v_Premise
		WHERE ClientId = @ClientID) b
		WHERE SortId = 1
		ORDER By ClientId, PremiseId, AccountId




END





GO
