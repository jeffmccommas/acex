SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [Holding].[InsertCustomerData]
 @Cust AS holding.CustomerTableType READONLY,
 @Premise AS holding.PremiseType READONLY
AS
/**********************************************************************************************************
* SP Name:
*		holding.InsertCustomerdata
* Parameters:
*		CustomerHoldingTable			CustomerTableType
*       PremiseHoldingTable				PremiseTableType
* Purpose:	This stored procedure insertsrows into the customer and premise holding tables
*	
*	
*	
*	
*              
*
*              
*
**********************************************************************************************************/
BEGIN
DECLARE @thecount int
	SET NOCOUNT ON
	BEGIN TRAN t1
	INSERT INTO holding.Customer ( ClientID, CustomerID, FirstName, LastName, Street1, Street2, City, State, Country, postalcode, PhoneNumber, MobilePhoneNumber, EmailAddress, AlternateEmailAddress, IsAuthenticated, IsBusiness, SourceId, TrackingId, TrackingDate)
	SELECT ClientID, CustomerID, FirstName, LastName, Street1, Street2, City, State, Country, postalcode, PhoneNumber, MobilePhoneNumber, EmailAddress, AlternateEmailAddress, IsAuthenticated, IsBusiness, SourceId, TrackingId, TrackingDate
	FROM @cust
	 	SELECT @thecount = @@ROWCOUNT
	INSERT INTO holding.premise (ClientID, CustomerID, AccountID, PremiseID, Street1, Street2, City, State, Country, postalcode, GasService, ElectricService, WaterService, Latitude, Longitude, SourceId, TrackingID, TrackingDate)
	SELECT ClientID, CustomerID, AccountID, PremiseID, Street1, Street2, City, State, Country, postalcode, GasService, ElectricService, WaterService, Latitude, Longitude, SourceId, TrackingID, TrackingDate
	FROM @premise
	SET NOCOUNT OFF
    COMMIT TRAN t1
	SELECT @thecount
END --proc













GO
