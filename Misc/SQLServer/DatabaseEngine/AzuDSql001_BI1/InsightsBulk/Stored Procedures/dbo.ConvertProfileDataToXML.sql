
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ubaid
-- Create date: 1/15/2015
-- Description:	Converts profiledata to xml
-- =============================================
CREATE PROCEDURE [dbo].[ConvertProfileDataToXML] @ClientId AS INT
AS
    BEGIN
        SET NOCOUNT ON;

        WITH XMLNAMESPACES (DEFAULT 'Aclara:Insights')
		SELECT ISNULL(	 
		( SELECT customerid AS '@CustomerId'
		, accountid AS 'Account/@AccountId'
		, premiseid AS 'Account/Premise/@PremiseId',
		IIF([waterheater.fuel] ='',NULL, ( SELECT 'waterheater.fuel' AS 'ProfileItem/@AttributeKey', [waterheater.fuel] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([heatsystem.fuel] ='',NULL, ( SELECT 'heatsystem.fuel' AS 'ProfileItem/@AttributeKey', [heatsystem.fuel] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([centralac.count] ='',NULL, ( SELECT 'centralac.count' AS 'ProfileItem/@AttributeKey', [centralac.count] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([house.totalarea] ='',NULL, ( SELECT 'house.totalarea' AS 'ProfileItem/@AttributeKey', [house.totalarea] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([house.style] ='',NULL, ( SELECT 'house.style' AS 'ProfileItem/@AttributeKey', [house.style] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([house.basementstyle] ='',NULL, ( SELECT 'house.basementstyle' AS 'ProfileItem/@AttributeKey', [house.basementstyle] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([house.people] ='',NULL, ( SELECT 'house.people' AS 'ProfileItem/@AttributeKey', [house.people] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([house.yearbuilt] ='',NULL, ( SELECT 'house.yearbuilt' AS 'ProfileItem/@AttributeKey', [house.yearbuilt] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([heatsystem.style] ='',NULL, ( SELECT 'heatsystem.style' AS 'ProfileItem/@AttributeKey', [heatsystem.style] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([heatsystem.yearinstalled] ='',NULL, ( SELECT 'heatsystem.yearinstalled' AS 'ProfileItem/@AttributeKey', [heatsystem.yearinstalled] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([heatsystem.efficiencyvalue] ='',NULL, ( SELECT 'heatsystem.efficiencyvalue' AS 'ProfileItem/@AttributeKey', [heatsystem.efficiencyvalue] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([heatsystem.efficiencyvalue] ='' OR ISNUMERIC([heatsystem.efficiencyvalue])=0 ,NULL, ( SELECT 'heatsystem.efficiency' AS 'ProfileItem/@AttributeKey', IIF(CONVERT(FLOAT,[heatsystem.efficiencyvalue])>=0.90,'heatsystem.efficiency.high','heatsystem.efficiency.standard') AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([waterheater.year] ='',NULL, ( SELECT 'waterheater.year' AS 'ProfileItem/@AttributeKey', [waterheater.year] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([waterheater.efficiencyvalue] ='',NULL, ( SELECT 'waterheater.efficiencyvalue' AS 'ProfileItem/@AttributeKey', [waterheater.efficiencyvalue] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([waterheater.efficiencyvalue] ='' OR ISNUMERIC([waterheater.efficiencyvalue])=0 ,NULL, ( SELECT 'waterheater.highefficiency' AS 'ProfileItem/@AttributeKey', IIF(CONVERT(FLOAT,[waterheater.efficiencyvalue])>=0.95,'waterheater.highefficiency.yes','waterheater.highefficiency.no') AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([house.windowqualitypoor] ='',NULL, ( SELECT 'house.windowqualitypoor' AS 'ProfileItem/@AttributeKey', [house.windowqualitypoor] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([house.insulationqualitypoor] ='',NULL, ( SELECT 'house.insulationqualitypoor' AS 'ProfileItem/@AttributeKey', [house.insulationqualitypoor] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise',
		IIF([roomac.count] ='',NULL, ( SELECT 'roomac.count' AS 'ProfileItem/@AttributeKey', [roomac.count] AS 'ProfileItem/@AttributeValue','utility' AS 'ProfileItem/@Source',GETDATE() AS 'ProfileItem/@ModifiedDate' FOR XML PATH(''),TYPE, ELEMENTS )) AS 'Account/Premise'FROM [InsightsBulk].[dbo].[RawProfileTemp]
		WHERE clientid = @ClientId
		FOR XML PATH('Customer'),ELEMENTS, ROOT('Customers'))
	,'<Customers xmlns="Aclara:Insights"></Customers>') AS xml_profileData;

    END;

GO
