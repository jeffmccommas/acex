SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [Holding].[InsertActionItemData]
    @actionItemtbl AS holding.ActionItemType READONLY
AS /**********************************************************************************************************
* SP Name:
*		holding.InsertPremiseAttributedata
* Parameters:
*		PremiseAttributesTable			
* Purpose:	This stored procedure insertsrows into the premiseattributes holding tables
**********************************************************************************************************/
    BEGIN
        SET NOCOUNT ON
        DECLARE @thecount INT
        INSERT  INTO holding.ActionItem
                ( clientid ,
                  customerid ,
                  accountid ,
                  premiseid ,
                  statusid ,
                  statusdate ,
                  sourceId ,
                  ActionKey ,
                  SubActionKey ,
                  ActionData ,
                  ActionDataValue ,
                  trackingid ,
                  trackingDate
	            )
                SELECT  clientid ,
                        customerid ,
                        accountid ,
                        premiseid ,
                        statusid ,
                        statusdate ,
                        sourceId ,
                        ActionKey ,
                        SubActionKey ,
                        ActionData ,
                        ActionDataValue ,
                        trackingid ,
                        trackingDate
                FROM    @actionItemtbl tmp

        SELECT  @thecount = @@ROWCOUNT

        SELECT  @thecount
	

    END --proc




























GO
