SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [Holding].[SelectUserDefinedTableTypeColumns] 
	@UserDefinedTableTypeName AS varchar(50) 
AS
BEGIN

	SET NOCOUNT ON;

	SELECT	Columns.name AS ColumnName, 
            Columns.column_id AS ColumnID,
		    Columns.system_type_id As SystemTypeID, 
		    System_Types.name As SystemTypeName, 
		    Columns.max_length As MaximumLength
	FROM	sys.all_columns AS Columns 
	JOIN	sys.table_types AS Table_Type
	ON		Columns.object_id = Table_Type.type_table_object_id
	JOIN	sys.types AS System_Types
	ON		Columns.system_type_id = System_Types.system_type_id
	WHERE	Table_Type.name LIKE '%' + @UserDefinedTableTypeName + '%'
	ORDER BY Columns.column_id ASC

END


GO
