SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Wayne
-- Create date: 8.4.2014
-- Description:	Summerizes hourly ami data to the day
-- =============================================
-- Changed by:		Wayne
-- Change date:		8.4.2014
-- Description:		per new spec from scg
-- =============================================
-- Changed by:		Wayne
-- Change date:		9.9.2014
-- Description:		per Srini/Ubaid added several new fields
--					an made proc section client specific
-- =============================================
-- Changed by:		Wayne
-- Change date:		9.22.2014
-- Description:		Added a @DailyRowTotalsCount var
--					for reporting
-- =============================================
-- Changed by:		Wayne
-- Change date:		10.23.2014
-- Description:		changed the significant digits on the sum
--					for reporting
-- =============================================
-- Changed by:		Wayne
-- Change date:		10.31.2014
-- Description:		Added additional error reporting
-- =============================================
CREATE PROCEDURE [dbo].[TotalAmiByDay] 
@ClientId int
AS
BEGIN
DECLARE @DailyRowTotalsCount int
/*
Proc sections are client specifi.
*/
--************************
--	So Cal Gas
--************************

BEGIN TRY

		INSERT [dbo].[ScgDailyTotals]
		SELECT 
				@ClientId AS ClientId
				,CAST(rt.SERVICE_POINT_ID  AS VARCHAR(50)) AS SERVICEPOINTID
				,CAST(rt.DATE AS int)    AS  amiDate
				,2 as CommodityId			
				,SUM(CAST(rt.USAGE_VALUE AS DECIMAL(18,4))) AS TotalUnits
				,6 AS UOMId-- SCG specific
				,-1 AS meterid
		  FROM InsightsBulk.dbo.RawAmiTemp rt
		  INNER JOIN InsightsDW.dbo.DimServicePoint	sp
		  ON rt.SERVICE_POINT_ID=sp.ServicePointId 
		  WHERE sp.ClientId=@ClientId
		  GROUP BY 		
				CAST(rt.SERVICE_POINT_ID  AS VARCHAR(50)) 
				,CAST(rt.DATE AS int) 

		set @DailyRowTotalsCount=isnull(@@ROWCOUNT,0)


select @DailyRowTotalsCount AS DailyRowTotalsCount

END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;

    DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10));
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10));

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH

--************************
--	Some other utility
--************************
--IF @ClientId=XXX
--	BEGIN
--
--		put translation here
--
--	END
END
GO
