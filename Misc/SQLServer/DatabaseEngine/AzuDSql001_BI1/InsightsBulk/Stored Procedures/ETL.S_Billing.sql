SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/21/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[S_Billing] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT  ClientId ,
                AccountId ,
                PremiseId ,
                ServiceContractId ,
                ServicePointId ,
                MeterId ,
                StartDate ,
                EndDate ,
                BillDays ,
                TotalUnits ,
                TotalCost ,
                CommodityId ,
                BillPeriodTypeId ,
                BillCycleScheduleId ,
                UOMId ,
                RateClass ,
                DueDate ,
                ReadDate ,
                ReadQuality ,
                SourceId ,
                TrackingId ,
                TrackingDate
        FROM    ( SELECT    ClientId ,
                            AccountId ,
                            PremiseId ,
                            ServiceContractId ,
                            ServicePointId ,
                            MeterId ,
                            StartDate ,
                            EndDate ,
                            BillDays ,
                            TotalUnits ,
                            TotalCost ,
                            CommodityId ,
                            BillPeriodTypeId ,
                            BillCycleScheduleId ,
                            UOMId ,
                            RateClass ,
                            DueDate ,
                            ReadDate ,
                            ReadQuality ,
                            SourceId ,
                            TrackingId ,
                            TrackingDate ,
                            ROW_NUMBER() OVER ( PARTITION BY ClientId,
                                                AccountId, PremiseId,
                                                ServiceContractId, StartDate,
                                                EndDate, CommodityId ORDER BY TrackingDate DESC ) AS SortId
                  FROM      Holding.v_Billing
                  WHERE     ClientId = @ClientID
                ) b
        WHERE   SortId = 1
        ORDER BY ClientId ,
                PremiseId ,
                AccountId ,
                ServiceContractId ,
                StartDate ,
                EndDate ,
                CommodityId;


    END;





GO
