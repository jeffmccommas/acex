SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/29/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[S_Meter]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;

		SELECT ClientId, ServicePointId, MeterId, RateClass, SourceId, TrackingId, TrackingDate
		FROM
		(SELECT  ClientId, ServicePointId, MeterId, RateClass, SourceId, TrackingId, TrackingDate
				,ROW_NUMBER() OVER ( PARTITION BY 
				ClientId, ServicePointId, MeterId 
				ORDER BY TrackingDate DESC ) AS SortId
		FROM [Holding].[v_Meter] 
		WHERE ClientId = @ClientID) m
		WHERE SortId = 1
		ORDER BY ClientId, ServicePointId, MeterId


END





GO
