SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/29/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[S_ServicePoint]
				@ClientID INT
AS

BEGIN

		SET NOCOUNT ON;

        SELECT ClientId, CustomerId, AccountId, PremiseId, ServicePointId, SourceId, TrackingId, TrackingDate
		FROM
		(SELECT ClientId, CustomerId, AccountId, PremiseId, ServicePointId, SourceId, TrackingId, TrackingDate
				,ROW_NUMBER() OVER ( PARTITION BY 
				ClientId, ServicePointId 
				ORDER BY TrackingDate DESC ) AS SortId
		FROM [Holding].[v_ServicePoint] 
		WHERE ClientId = @ClientID) sp
		WHERE SortId = 1
		ORDER BY ClientId, ServicePointId


END





GO
