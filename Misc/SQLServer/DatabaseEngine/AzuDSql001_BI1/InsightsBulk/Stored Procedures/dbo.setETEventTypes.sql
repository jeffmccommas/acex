if exists (select * from sysobjects where type = 'p' and name = 'setETEventTypes')
drop procedure setETEventTypes
go

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [dbo].[setETEventTypes] @clientID int, @ETFileType varchar(30)
as

set nocount on

if @ETFileType = 'ETClickOpen'
begin

	update dbo.ETClickOpen set Clicktype = 'FAQ', EventType = 14 where activity = 'click' and url like '%FAQ%' and referrerID = @clientID

	update dbo.ETClickOpen set Clicktype = 'myaccount', EventType = 15 where activity = 'click' and url like '%myaccount%' and referrerID = @clientID

	update dbo.ETClickOpen set Clicktype = 'optout', EventType = 16 where activity = 'click' and url like '%optout%' and referrerID = @clientID

	update dbo.ETClickOpen set Clicktype = 'privacy', EventType = 17 where activity = 'click' and url like '%privacy%' and referrerID = @clientID

	update dbo.ETClickOpen set Clicktype = 'rebates', EventType = 18 where activity = 'click' and url like '%rebates%' and referrerID = @clientID

	update dbo.ETClickOpen set Clicktype = 'socalgas.com', EventType = 19 where activity = 'click' and url in ('http://www.socalgas.com/','http://socalgas.com') and referrerID = @clientID

	update dbo.ETClickOpen set Clicktype = 'viewAsWebPage', EventType = 20 where activity = 'click' and url like '%view.socalgas.aclara.com/%' and referrerID = @clientID

	update dbo.ETClickOpen set Clicktype = 'viewAsWebPage', EventType = 1 where activity = 'click' and ClickType is null and referrerID = @clientID

	update dbo.ETClickOpen set Clicktype = 'Open', EventType = 4 where activity = 'Open' and referrerID = @clientID 

end

if @ETFileType = 'ETBounceOptOut'
begin

	update dbo.ETBounceOptOut
	set bouncetype = ltrim(rtrim(reason))
	where updatetype like '%bounce%'
	and referrerID = @clientID

	update dbo.ETBounceOptOut
	set bouncetype = updatetype
	--where updatetype in ('undeliverable', 'Unsubscribed')
	where updatetype not like '%bounce%'
	and referrerID = @clientID

	update dbo.ETBounceOptOut
	set EventType = case when bouncetype like 'Soft%' then 22 
		when bouncetype like 'technical%' then 25
		when bouncetype like 'block%' then 24 
		when bouncetype like 'hard%' then 23
		when bouncetype like 'unknown%' then 26
		else 11 end
	where  updatetype like '%bounce%'
	and referrerID = @clientID

	update dbo.ETBounceOptOut
	set EventType = case bouncetype when 'undeliverable' then 12 when 'Unsubscribed' then 13 else 0 end
	where  updatetype not like '%bounce%'
	and referrerID = @clientID
 
end

set nocount off
GO
