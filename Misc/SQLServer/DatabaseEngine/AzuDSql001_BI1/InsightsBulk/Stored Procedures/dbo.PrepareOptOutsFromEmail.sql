SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Wayne
-- Create date:	11/19/2014
-- Description:	This will use the emails 
--				in the scg email opt out file 
--				which only has email and CELL
--				to find which customers (Servicepoint/Account) to optout
-- =============================================
-- Changed by:		Wayne
-- Change date:	12/5/2014
-- Description:	added to co get the groupnumber (CELL)
--				to be able to handle the hardbounce list
--				which doesnt have the CELL
-- =============================================
-- Change date:	4/14/2015
-- Description:	Made changes to support control group members
--				prior it was only processing if it was treatment group
--
--				also made change to use group from factpremiseattribute table (the view)
--				also changed to a MERGE
--				also added a client id param
-- =============================================
CREATE PROC [dbo].[PrepareOptOutsFromEmail]
@clientid INT =101
AS

Merge [dbo].[ScgOptOuts]	 AS Target
USING (
SELECT	sp.ServicePointId,
		dp.AccountId,
		a.GroupId,
		e.clientid
FROM InsightsDW.dbo.DimCustomer c
INNER JOIN insightsbulk.[dbo].[ScgEmailOptOuts] e
ON LTRIM(rtrim(e.Email))=LTRIM(RTRIM(c.EmailAddress))
INNER JOIN InsightsDW.dbo.FactCustomerPremise fcp
ON fcp.CustomerKey = c.CustomerKey
INNER JOIN InsightsDW.dbo.DimPremise dp
ON dp.PremiseKey=fcp.PremiseKey
INNER JOIN InsightsDW.dbo.DimServicePoint sp
ON sp.PremiseKey = dp.PremiseKey
LEFT outer JOIN InsightsDW.[dbo].[vScgAnalysisGroup] a
						ON a.premisekey=dp.PremiseKey
						WHERE dp.clientid=@clientid AND e.ClientId=@clientid
) AS Source
ON  Source.ServicePointId=Target.service_point_id and
    Source.AccountId= Target.account_id and
	Source.GroupId	 = Target.CELL and
	Source.clientid=Target.clientid
WHEN NOT MATCHED
THEN INSERT VALUES (
Source.ServicePointId,
    Source.AccountId,
	Source.GroupId,
	Source.clientid);
GO
