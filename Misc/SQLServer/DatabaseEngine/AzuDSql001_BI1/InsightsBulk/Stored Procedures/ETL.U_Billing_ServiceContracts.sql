SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ubaid
-- Create date: 5/28/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [ETL].[U_Billing_ServiceContracts] @ClientID INT
AS
    BEGIN

        SET NOCOUNT ON;

 
        UPDATE  b
        SET     b.ServiceContractId = 'SC_' + b.PremiseId + '_'
                + tc.CommodityKey
        FROM    Holding.Billing b
                INNER JOIN InsightsMetadata.cm.TypeCommodity tc ON tc.CommodityID = b.CommodityId
        WHERE   ClientId = @ClientID AND b.ServiceContractId IS NULL;


    END;

GO
