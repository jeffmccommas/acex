SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [Holding].[MergePremiseInfo]
    @PremiseInfo AS [dbo].[PremiseInfoTableType] READONLY
AS
    BEGIN
	
        SET NOCOUNT ON;

        BEGIN TRY
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

            BEGIN TRANSACTION
      
            MERGE [Holding].[Premise] AS target
            USING
                ( SELECT    RowId ,
                            Latitude ,
                            Longitude ,
                            Zip
                  FROM      @PremiseInfo pa
                ) AS source ( RowId, Latitude, Longitude, Zip )
            ON ( target.PremiseRowId = source.RowId )
            WHEN MATCHED THEN
                UPDATE SET
                         TARGET.Latitude = ISNULL(SOURCE.Latitude,
                                                  TARGET.Latitude) ,
                         TARGET.Longitude = ISNULL(SOURCE.Longitude,
                                                   TARGET.Longitude) ,
                         TARGET.postalcode = ISNULL(SOURCE.Zip,
                                                    TARGET.postalcode);

            COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                ROLLBACK TRANSACTION
			
			THROW;
        END CATCH  
    END




GO
