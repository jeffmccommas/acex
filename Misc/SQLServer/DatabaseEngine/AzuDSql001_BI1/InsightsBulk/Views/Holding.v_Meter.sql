SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



















-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/1/2014
-- =============================================

CREATE VIEW [Holding].[v_Meter]

AS

		SELECT cl.[ClientId]
			  ,[ServicePointId]
			  ,[MeterId]
			  ,[RateClass]
			  ,[SourceId]
			  ,MAX(LEFT([TrackingId], 15)) AS TrackingId
			  ,MAX([TrackingDate]) AS TrackingDate
		  FROM Holding.Billing hb WITH(NOLOCK)
		  INNER JOIN Holding.Client cl ON cl.ClientId = hb.ClientId	
		  GROUP BY cl.[ClientId]
			  ,[ServicePointId]
			  ,[MeterId]
			  ,[RateClass]
			  ,[SourceId]
































GO
