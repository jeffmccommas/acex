SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


















-- =============================================
-- Author:		Jason Khourie
-- Create date: 613/2014
-- =============================================

CREATE VIEW [Holding].[v_Billing]
AS
    SELECT  b.[ClientId] ,
            b.[CustomerId] ,
            b.[AccountId] ,
            b.[PremiseId] ,
            b.[ServiceContractId] ,
            b.[ServicePointId] ,
            b.[MeterId] ,
            b.[RateClass] ,
            b.[BillStartDate] AS StartDate ,
            b.[BillEndDate] AS EndDate ,
            b.[BillDays] ,
            b.[TotalUnits] ,
            b.[TotalCost] ,
            b.[CommodityId] ,
            b.[BillPeriodTypeId] ,
            b.[BillCycleScheduleId] ,
            b.[UOMId] ,
            b.[DueDate] ,
            b.[ReadDate] ,
            b.[ReadQuality] ,
            b.[SourceId] ,
            b.TrackingId ,
            b.TrackingDate
    FROM    Holding.Billing b WITH ( NOLOCK )
            INNER JOIN Holding.Client cl ON cl.ClientId = b.ClientId;	































GO
