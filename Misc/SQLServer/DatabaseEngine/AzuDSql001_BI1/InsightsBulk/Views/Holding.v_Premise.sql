SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jason Khourie
-- Create date: 4/21/2014
-- Description:	View of hld_User with Business and Unauthenticated users filtered out
-- =============================================

CREATE VIEW [Holding].[v_Premise]

AS

		SELECT  p.ClientId,
				p.PremiseId,
				p.AccountID AS AccountId,
				p.Street1,  
				p.Street2, 
				p.City, 
				p.[State] AS StateProvince,
				p.Country,
				p.PostalCode,
				GasService,
				ElectricService,
				WaterService,
				p.SourceId,
			    p.[TrackingId],
			    p.[TrackingDate]
		FROM Holding.Premise p 























GO
