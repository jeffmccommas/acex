SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jason Khourie
-- Create date: 7/1/2014
-- =============================================

CREATE VIEW [Holding].[v_ServicePoint]

AS

		SELECT cl.[ClientId]
			  ,[CustomerId]
			  ,[AccountId]
			  ,[PremiseId]
			  ,[ServicePointId]
			  ,[SourceId]
			  ,MAX(LEFT([TrackingId], 15)) AS TrackingId
			  ,MAX([TrackingDate]) AS TrackingDate
		  FROM Holding.Billing hb WITH(NOLOCK)
		  INNER JOIN Holding.Client cl ON cl.ClientId = hb.ClientId	
		  GROUP BY cl.[ClientId]
			  ,[CustomerId]
			  ,[AccountId]
			  ,[PremiseId]
			  ,[ServicePointId]
			  ,[SourceId]































GO
