SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jason Khourie
-- Create date: 6/24/2014
-- =============================================

CREATE VIEW [Holding].[v_PremiseAttributes]

AS


SELECT  ClientID, 
		AccountID, 
		CustomerID, 
		PremiseID, 
		AttributeId, 
		AttributeKey, 
		OptionId,
	    OptionValue, 
		SourceId, 
		TrackingId, 
		TrackingDate
FROM Holding.PremiseAttributes 






















GO
