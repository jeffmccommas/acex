SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jason Khourie
-- Create date: 4/2/2014
-- Description:	View of hld_User with Business and Unauthenticated users filtered out
-- =============================================

CREATE VIEW [Holding].[v_Customer]
AS


		SELECT  c.ClientId,
				c.CustomerId,
				c.FirstName, 
				c.LastName,
				c.Street1,  
				c.Street2, 
				c.City, 
				c.[State] AS StateProvince,
				c.Country AS Country,
				c.PostalCode,
			    c.MobilePhoneNumber,
				c.PhoneNumber,
				c.EmailAddress,
				c.AlternateEmailAddress,
				c.SourceId,
				1 AS AuthenticationTypeId,
			    c.[TrackingId],
			   c.[TrackingDate]
		FROM Holding.Customer c



























GO
