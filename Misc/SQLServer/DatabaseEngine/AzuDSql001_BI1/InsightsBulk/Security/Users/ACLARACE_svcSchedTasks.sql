IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ACLARACE\svcSchedTasks')
CREATE LOGIN [ACLARACE\svcSchedTasks] FROM WINDOWS
GO
CREATE USER [ACLARACE\svcSchedTasks] FOR LOGIN [ACLARACE\svcSchedTasks]
GO
