EXEC sp_addrolemember N'db_datawriter', N'ACLARACE\grpACL_PDM'
GO
EXEC sp_addrolemember N'db_datawriter', N'ACLARACE\grpACL_PJM'
GO
EXEC sp_addrolemember N'db_datawriter', N'ACLARACE\grpACL_PJMSr'
GO
EXEC sp_addrolemember N'db_datawriter', N'ACLARACE\grpACL_QASr'
GO
EXEC sp_addrolemember N'db_datawriter', N'ACLARACE\svcSchedTasks'
GO
