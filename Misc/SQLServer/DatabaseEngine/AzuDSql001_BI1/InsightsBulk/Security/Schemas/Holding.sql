CREATE SCHEMA [Holding]
AUTHORIZATION [dbo]
GO
GRANT EXECUTE ON SCHEMA:: [Holding] TO [db_execproc]
GRANT EXECUTE ON SCHEMA:: [Holding] TO [db_viewproc]
GO
