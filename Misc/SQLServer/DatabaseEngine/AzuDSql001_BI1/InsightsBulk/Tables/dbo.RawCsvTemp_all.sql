CREATE TABLE [dbo].[RawCsvTemp_all]
(
[customer_id] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[premise_id] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mail_address_line_1] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mail_address_line_2] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mail_address_line_3] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mail_city] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mail_state] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mail_zip_code] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[first_name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[last_name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phone_1] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phone_2] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[email] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[customer_type] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[account_id] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[active_date] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[inactive_date] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[read_cycle] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rate_code] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[service_point_id] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[service_house_number] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[service_street_name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[service_unit] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[service_city] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[service_state] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[service_zip_code] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[meter_type] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[meter_units] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[bldg_sq_foot] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[year_built] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[bedrooms] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[assess_value] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[usage_value] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[date_to] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[duration] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[is_estimate] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[usage_charge] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [RawCsvTemp_all_clustered] ON [dbo].[RawCsvTemp_all] ([date_to]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_PremiseId_dateto] ON [dbo].[RawCsvTemp_all] ([premise_id], [date_to]) ON [PRIMARY]
GO
