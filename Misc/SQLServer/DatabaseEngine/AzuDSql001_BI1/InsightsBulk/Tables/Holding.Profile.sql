CREATE TABLE [Holding].[Profile]
(
[Account_Id] [numeric] (20, 0) NULL,
[XML Source.AccountId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XML Source.Customer_Id] [numeric] (20, 0) NULL,
[Lookup.AccountId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Lookup.Customer_Id] [numeric] (20, 0) NULL,
[CustomerId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Premise_Id] [numeric] (20, 0) NULL,
[PremiseId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModifiedDate] [datetime] NULL,
[CreateDate] [datetime] NULL,
[AttributeKey] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AttributeValue] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrackingID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Profile__Trackin__7908F585] DEFAULT ((0)),
[TrackingDate] [datetime] NOT NULL CONSTRAINT [DF__Profile__Trackin__79FD19BE] DEFAULT (getdate()),
[ClientID] [int] NOT NULL CONSTRAINT [DF__Profile__ClientI__7AF13DF7] DEFAULT ((0))
) ON [PRIMARY]
GO
