CREATE TABLE [dbo].[tempqueue]
(
[taskid] [bigint] NULL,
[ClientId] [int] NOT NULL,
[premiseid] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[accountid] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[customerid] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceId] [int] NOT NULL,
[TrackingId] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
