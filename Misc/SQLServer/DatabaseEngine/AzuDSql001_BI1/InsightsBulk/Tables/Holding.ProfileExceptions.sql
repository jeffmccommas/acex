CREATE TABLE [Holding].[ProfileExceptions]
(
[Account_Id] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XML Source.AccountId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XML Source.Customer_Id] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Lookup.AccountId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Lookup.Customer_Id] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Premise_Id] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PremiseId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModifiedDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AttributeKey] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AttributeValue] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Client_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrackingID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceFile] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorColumn] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
