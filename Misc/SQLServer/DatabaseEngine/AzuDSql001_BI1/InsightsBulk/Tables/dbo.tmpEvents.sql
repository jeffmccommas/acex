CREATE TABLE [dbo].[tmpEvents]
(
[referrerid] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EventType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FlagDateCreated] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FlagDateUpdated] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
