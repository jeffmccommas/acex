CREATE TABLE [Holding].[DisAggReCalcQueue]
(
[ClientId] [int] NOT NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomerId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceId] [int] NULL,
[TrackingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrackingDate] [datetime] NULL
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IX_DisAggReCalcQueue] ON [Holding].[DisAggReCalcQueue] ([ClientId], [CustomerId], [AccountId], [PremiseId]) WITH (FILLFACTOR=50, STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
