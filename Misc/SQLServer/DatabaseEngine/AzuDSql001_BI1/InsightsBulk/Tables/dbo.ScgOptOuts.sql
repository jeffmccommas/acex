CREATE TABLE [dbo].[ScgOptOuts]
(
[service_point_id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[account_id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CELL] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[clientid] [int] NULL
) ON [PRIMARY]
GO
