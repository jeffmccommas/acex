CREATE TABLE [Holding].[Client]
(
[ClientId] [int] NOT NULL,
[RunMelissaData] [int] NOT NULL,
[RunBenchmark] [int] NOT NULL,
[RunActionModel] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [Holding].[Client] ADD CONSTRAINT [PK_Client] PRIMARY KEY NONCLUSTERED  ([ClientId]) ON [PRIMARY]

GO
CREATE CLUSTERED INDEX [IX_Client] ON [Holding].[Client] ([ClientId]) ON [PRIMARY]
GO
