CREATE TABLE [ETL].[PremiseAttributes_INS]
(
[ClientID] [int] NOT NULL,
[AccountID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AttributeID] [int] NULL,
[AttributeValue] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AttributeKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceId] [int] NULL,
[CustomerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
