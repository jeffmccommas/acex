CREATE TABLE [dbo].[ETBounceOptOut]
(
[SubscriberKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reason] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Environment] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BounceType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EventType] [int] NULL,
[ReferrerID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
