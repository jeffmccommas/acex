CREATE TABLE [dbo].[RawActionTemp]
(
[clientid] [int] NOT NULL,
[customerid] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[accountid] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[premiseid] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[measurename] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[projectid] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[actionkey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[status] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[statusdate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[source] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[programname] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[incentiveamount] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[incentivetype] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[upfrontcost] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[annualsavingsestimate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
