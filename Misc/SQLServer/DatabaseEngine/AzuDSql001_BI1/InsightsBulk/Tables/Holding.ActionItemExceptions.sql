CREATE TABLE [Holding].[ActionItemExceptions]
(
[Customer_Id] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XML Source.CustomerId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Customer.CustomerId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account_Id] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PremiseId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Premise_Id] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionItem_Id] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionKey] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubActionKey] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionValue] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionData] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Client_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[servicecontarctID1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sourceid1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[trackingdate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrackingID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubActionKey_V] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceFile] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorColumn] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
