CREATE TABLE [dbo].[ScgDailyTotals]
(
[ClientId] [int] NOT NULL,
[ServicePointId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AmiDate] [int] NOT NULL,
[CommodityId] [int] NOT NULL,
[TotalUnits] [decimal] (18, 4) NOT NULL,
[UOMId] [int] NOT NULL,
[MeterId] [int] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_ScgDailyTotals] ON [dbo].[ScgDailyTotals] ([ClientId], [ServicePointId]) ON [PRIMARY]
GO
