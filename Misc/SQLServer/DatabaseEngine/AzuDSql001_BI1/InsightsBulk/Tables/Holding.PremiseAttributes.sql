CREATE TABLE [Holding].[PremiseAttributes]
(
[ClientID] [int] NOT NULL,
[CustomerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AttributeId] [int] NULL,
[AttributeKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OptionId] [int] NULL,
[OptionValue] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceId] [int] NULL,
[UpdateDate] [datetime] NULL,
[CreateDate] [datetime] NULL,
[TrackingID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IX_PremiseAttributes] ON [Holding].[PremiseAttributes] ([ClientID]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NC_PremiseAttributes_ClientID] ON [Holding].[PremiseAttributes] ([ClientID]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
