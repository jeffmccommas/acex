CREATE TABLE [dbo].[ETClickOpen]
(
[Email_Address] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Subscriber_Key] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Send_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activity] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Time] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URL] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClickType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EventType] [int] NULL,
[ReferrerID] [int] NULL
) ON [PRIMARY]
GO
