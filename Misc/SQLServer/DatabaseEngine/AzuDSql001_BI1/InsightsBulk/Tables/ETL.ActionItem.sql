CREATE TABLE [ETL].[ActionItem]
(
[ClientId] [int] NOT NULL,
[CustomerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [int] NOT NULL,
[StatusDate] [datetime] NOT NULL,
[SourceId] [int] NOT NULL,
[ActionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SubActionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AdditionalInfo] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrackingDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
