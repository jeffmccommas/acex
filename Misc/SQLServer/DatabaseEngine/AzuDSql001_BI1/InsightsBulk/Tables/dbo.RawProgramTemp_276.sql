CREATE TABLE [dbo].[RawProgramTemp_276]
(
[ClientId] [int] NOT NULL,
[Account] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Premise] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasureCode] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Measure] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Category] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
