CREATE TABLE [dbo].[RawProfileTemp]
(
[clientid] [int] NOT NULL,
[customerid] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[accountid] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[premiseid] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[waterheater.fuel] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[heatsystem.fuel] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[centralac.count] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[house.totalarea] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[house.style] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[house.basementstyle] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[house.people] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[house.yearbuilt] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[heatsystem.style] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[heatsystem.yearinstalled] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[heatsystem.efficiencyvalue] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[waterheater.year] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[waterheater.efficiencyvalue] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[house.windowqualitypoor] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[house.insulationqualitypoor] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[roomac.count] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
