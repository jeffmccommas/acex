CREATE TABLE [ETL].[PremiseAttributes_UPD]
(
[ClientID] [int] NULL,
[AccountID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PremiseID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AttributeId] [int] NULL,
[AttributeKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OptionId] [int] NULL,
[OptionValue] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceId] [int] NULL,
[TrackingID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrackingDate] [datetime] NULL,
[CustomerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
