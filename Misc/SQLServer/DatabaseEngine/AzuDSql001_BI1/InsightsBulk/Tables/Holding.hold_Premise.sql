CREATE TABLE [Holding].[hold_Premise]
(
[ClientID] [int] NOT NULL,
[CustomerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Street1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Street2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[State] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Country] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[postalcode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GasService] [int] NOT NULL,
[ElectricService] [int] NOT NULL,
[WaterService] [int] NOT NULL,
[Latitude] [float] NULL,
[Longitude] [float] NULL,
[SourceId] [int] NOT NULL,
[TrackingID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [datetime] NOT NULL,
[PremiseRowId] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
