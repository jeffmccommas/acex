CREATE TABLE [Holding].[PremiseAttributesTEST]
(
[ClientID] [int] NOT NULL,
[CustomerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AttributeId] [int] NULL,
[AttributeKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OptionId] [int] NULL,
[OptionValue] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceId] [int] NOT NULL,
[UpdateDate] [datetime] NOT NULL,
[CreateDate] [datetime] NOT NULL,
[TrackingID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
