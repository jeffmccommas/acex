CREATE TABLE [dbo].[RawAmiTemp]
(
[SERVICE_POINT_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[USAGE_VALUE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DATE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TIME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UNITS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IS_ESTIMATE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UTC_offset] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientId] [int] NULL
) ON [PRIMARY]
GO
