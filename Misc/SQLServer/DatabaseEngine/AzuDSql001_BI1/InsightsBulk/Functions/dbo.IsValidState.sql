SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[IsValidState]
(
@state varchar(50)
)
RETURNS int
AS
BEGIN
DECLARE @isvalid int
IF @state  IN 
   (
			 'AL'
			,'AK'
			,'AS'
			,'AZ'
			,'AR'
			,'CA'
			,'CO'
			,'CT'
			,'DE'
			,'DC'
			,'FL'
			,'GA'
			,'HI'
			,'ID'
			,'IL'
			,'IN'
			,'IA'
			,'KS'
			,'KY'
			,'LA'
			,'ME'
			,'MD'
			,'MA'
			,'MI'
			,'MN'
			,'MS'
			,'MO'
			,'MT'
			,'NE'
			,'NV'
			,'NH'
			,'NJ'
			,'NM'
			,'NY'
			,'NC'
			,'ND'
			,'OH'
			,'OK'
			,'OR'
			,'PA'
			,'PR'
			,'RI'
			,'SC'
			,'SD'
			,'TN'
			,'TX'
			,'UT'
			,'VT'
			,'VA'
			,'WA'
			,'WV'
			,'WI'
			,'WY'
			)
			SET @isvalid= 1
			ELSE 
			SET @isvalid= 0
RETURN @isvalid;
END
GO
