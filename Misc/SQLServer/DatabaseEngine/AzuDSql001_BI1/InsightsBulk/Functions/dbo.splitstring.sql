SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ubaid
-- Create date: 12.29.2014
-- Description:splits a string that has commas for delimiter
--
-- Used in th HE reports procs		
-- =============================================
CREATE FUNCTION [dbo].[splitstring]
    (
      @stringToSplit VARCHAR(MAX) ,
      @Separator AS CHAR(1) = ','
    )
RETURNS @returnList TABLE ( [Name] [NVARCHAR](500) )
AS
    BEGIN

        DECLARE @name NVARCHAR(255)
        DECLARE @pos INT

        WHILE CHARINDEX(@Separator, @stringToSplit) > 0
            BEGIN
                SELECT  @pos = CHARINDEX(@Separator, @stringToSplit)  
                SELECT  @name = SUBSTRING(@stringToSplit, 1, @pos - 1)

                INSERT  INTO @returnList
                        SELECT  RTRIM(LTRIM(@name))

                SELECT  @stringToSplit = SUBSTRING(@stringToSplit, @pos + 1,
                                                   LEN(@stringToSplit) - @pos)
            END

        INSERT  INTO @returnList
                SELECT  RTRIM(LTRIM(@stringToSplit))

        RETURN
    END
GO
