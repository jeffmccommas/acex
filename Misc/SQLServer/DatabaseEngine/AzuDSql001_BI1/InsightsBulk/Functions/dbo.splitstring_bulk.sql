SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ubaid
-- Create date: 12.29.2014
-- Description:splits a string that has commas for delimiter
--
-- Used in th HE reports procs		
-- =============================================
-- mod by:		Wayne
-- mod date:	12.29.2014
-- Description:	Created a new copy version because DW had the same function
--				but was not identical code
--				needed for merge of databases
-- =============================================
CREATE FUNCTION [dbo].[splitstring_bulk]
    (
      @stringToSplit VARCHAR(MAX) ,
      @Separator AS CHAR(1) = ','
    )
RETURNS @returnList TABLE ( [Name] [NVARCHAR](500) )
AS
    BEGIN

        DECLARE @name NVARCHAR(255)
        DECLARE @pos INT

        WHILE CHARINDEX(@Separator, @stringToSplit) > 0
            BEGIN
                SELECT  @pos = CHARINDEX(@Separator, @stringToSplit)  
                SELECT  @name = SUBSTRING(@stringToSplit, 1, @pos - 1)

                INSERT  INTO @returnList
                        SELECT  RTRIM(LTRIM(@name))

                SELECT  @stringToSplit = SUBSTRING(@stringToSplit, @pos + 1,
                                                   LEN(@stringToSplit) - @pos)
            END

        INSERT  INTO @returnList
                SELECT  RTRIM(LTRIM(@stringToSplit))

        RETURN
    END

GO
