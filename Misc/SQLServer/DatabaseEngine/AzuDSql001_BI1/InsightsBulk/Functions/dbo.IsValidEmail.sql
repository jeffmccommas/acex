SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[IsValidEmail]
(
@email varchar(255)
)
RETURNS int
AS
BEGIN
DECLARE @isvalid int
IF   REVERSE(@email) LIKE '.%'
			SET @isvalid= 0
			ELSE 
			SET @isvalid= 1
RETURN @isvalid;
END
GO
