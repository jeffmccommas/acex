CREATE TYPE [Holding].[ActionItemType] AS TABLE
(
[ClientID] [int] NOT NULL,
[CustomerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [int] NOT NULL,
[StatusDate] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceId] [int] NOT NULL,
[ActionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SubActionKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionData] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionDataValue] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrackingID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
