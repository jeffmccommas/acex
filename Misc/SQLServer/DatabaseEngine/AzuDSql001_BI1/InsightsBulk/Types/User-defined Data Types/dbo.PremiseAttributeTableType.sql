CREATE TYPE [dbo].[PremiseAttributeTableType] AS TABLE
(
[RowId] [int] NOT NULL,
[AttributeKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OptionValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
PRIMARY KEY CLUSTERED  ([RowId], [AttributeKey])
)
GO
