CREATE TYPE [dbo].[PremiseInfoTableType] AS TABLE
(
[RowId] [int] NOT NULL,
[Latitude] [float] NULL,
[Longitude] [float] NULL,
[Address] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResultCode] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResultData] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
PRIMARY KEY CLUSTERED  ([RowId])
)
GO
