CREATE TYPE [Holding].[PremiseType] AS TABLE
(
[ClientID] [int] NOT NULL,
[CustomerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PremiseID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Street1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[postalcode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GasService] [int] NOT NULL,
[ElectricService] [int] NOT NULL,
[WaterService] [int] NOT NULL,
[Latitude] [float] NULL,
[Longitude] [float] NULL,
[SourceId] [int] NOT NULL,
[TrackingID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrackingDate] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
