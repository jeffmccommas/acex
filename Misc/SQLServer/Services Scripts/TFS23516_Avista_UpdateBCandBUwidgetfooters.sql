--Bill Comparison Footer Script
SELECT * FROM cm.ClientTextContentLocaleContent WHERE ClientTextContentID IN (7812,7606,7819) AND LocaleKey='en-US'

UPDATE cm.ClientTextContentLocaleContent SET 
ShortText='Recent bill corrections may not be reflected.',
MediumText='Recent bill corrections may not be reflected.',
LongText='Recent bill corrections may not be reflected.'
WHERE ClientTextContentID IN (7812,7606,7819) AND LocaleKey='en-US'

--Billed Usage Chart Footer Script

SELECT * FROM cm.ClientTextContentLocaleContent WHERE ClientTextContentID IN (7659,7660,7622) AND LocaleKey='en-US'
UPDATE cm.ClientTextContentLocaleContent set
ShortText='Billing periods may vary, resulting in missing or duplicate bills in a single month. Recent bill corrections may not be reflected.',
MediumText='Billing periods may vary, resulting in missing or duplicate bills in a single month. Recent bill corrections may not be reflected.',
LongText='Billing periods may vary, resulting in missing or duplicate bills in a single month. Recent bill corrections may not be reflected.'
WHERE ClientTextContentID IN (7659,7660,7622) AND LocaleKey='en-US'
