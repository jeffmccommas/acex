USE [InsightsDW_224]
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToAclaraOneCSV_224]    Script Date: 9/8/2017 11:06:41 AM ******/
DROP PROCEDURE [dbo].[ConvertBillDataToAclaraOneCSV_224]
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToAclaraOneCSV_224]    Script Date: 9/8/2017 11:06:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Susan Steele>
-- Create date: <9-26-2016>
-- Description:	<Converts UI Billing Data to AclaraOne CSV format>
-- =============================================
CREATE PROCEDURE [dbo].[ConvertBillDataToAclaraOneCSV_224] 
    @ClientId AS INT
  , @TrackingId AS VARCHAR(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- create a temp table that looks like UIAclaraOneBillingData but all varchar and with an identity column
CREATE TABLE #UIAclaraOneBillingData
(rownum INT IDENTITY(1,1),
	[ClientId] [varchar](50) NULL,
	[customer_id] [varchar](50) NULL,
	[premise_id] [varchar](50) NULL,
	[mail_address_line_1] [varchar](200) NULL,
	[mail_address_line_2] [varchar](200) NULL,
	[mail_address_line_3] [varchar](200) NULL,
	[mail_city] [varchar](50) NULL,
	[mail_state] [varchar](50) NULL,
	[mail_zip_code] [varchar](50) NULL,
	[first_name] [varchar](50) NULL,
	[last_name] [varchar](50) NULL,
	[phone_1] [varchar](50) NULL,
	[phone_2] [varchar](50) NULL,
	[email] [varchar](225) NULL,
	[customer_type] [varchar](50) NULL,
	[account_id] [varchar](50) NULL,
	[active_date] [varchar](50) NULL,
	[inactive_date] [varchar](50) NULL,
	[read_cycle] [varchar](50) NULL,
	[rate_code] [varchar](50) NULL,
	[service_point_id] [varchar](50) NULL,
	[site_addressline1] [varchar](200) NULL,
	[site_addressline2] [varchar](200) NULL,
	[site_addressline3] [varchar](200) NULL,
	[site_city] [varchar](50) NULL,
	[site_state] [varchar](50) NULL,
	[site_zip_code] [varchar](50) NULL,
	[meter_type] [varchar](50) NULL,
	[meter_units] [varchar](50) NULL,
	[bldg_sq_foot] [varchar](50) NULL,
	[year_built] [varchar](50) NULL,
	[bedrooms] [varchar](50) NULL,
	[assess_value] [varchar](50) NULL,
	[usage_value] [varchar](50) NULL,
	[bill_enddate] [varchar](50) NULL,
	[bill_days] [varchar](50) NULL,
	[is_estimate] [varchar](50) NULL,
	[usage_charge] [varchar](50) NULL,
	[service_commodity] [varchar](50) NULL,
	[account_structure_type] [varchar](50) NULL,
	[meter_id] [varchar](50) NULL,
	[billperiod_type] [varchar](50) NULL,
	[meter_replaces_meterid] [varchar](50) NULL,
	[service_read_date] [varchar](50) NULL,
	[Service_contract] [varchar](50) NULL,
	[Programs] [varchar](255) NULL
)

-- add a row for header
insert into #UIAclaraOneBillingData (ClientId,customer_id,premise_id,mail_address_line_1,mail_address_line_2,mail_address_line_3,mail_city,mail_state,mail_zip_code,first_name,last_name,phone_1,phone_2,email,customer_type,account_id,active_date,inactive_date,read_cycle,rate_code,service_point_id,site_addressline1,site_addressline2,site_addressline3,site_city,site_state,site_zip_code,meter_type,meter_units,bldg_sq_foot,year_built,bedrooms,assess_value,usage_value,bill_enddate,bill_days,is_estimate,usage_charge,service_commodity,account_structure_type,meter_id,billperiod_type,meter_replaces_meterid,service_read_date,Service_contract,Programs)
select 'ClientId','customer_id','premise_id','mail_address_line_1','mail_address_line_2','mail_address_line_3','mail_city','mail_state','mail_zip_code','first_name','last_name','phone_1','phone_2','email','customer_type','account_id','active_date','inactive_date','read_cycle','rate_code','service_point_id','site_addressline1','site_addressline2','site_addressline3','site_city','site_state','site_zip_code','meter_type','meter_units','bldg_sq_foot','year_built','bedrooms','assess_value','usage_value','bill_enddate','bill_days','is_estimate','usage_charge','service_commodity','account_structure_type','meter_id','billperiod_type','meter_replaces_meterid','service_read_date','Service_contract','Programs'

-- insert into temp table 
INSERT INTO #UIAclaraOneBillingData (ClientId,customer_id,premise_id,mail_address_line_1,mail_address_line_2,mail_address_line_3,mail_city,mail_state,mail_zip_code,first_name,last_name,phone_1,phone_2,email,customer_type,account_id,active_date,inactive_date,read_cycle,rate_code,service_point_id,site_addressline1,site_addressline2,site_addressline3,site_city,site_state,site_zip_code,meter_type,meter_units,bldg_sq_foot,year_built,bedrooms,assess_value,usage_value,bill_enddate,bill_days,is_estimate,usage_charge,service_commodity,account_structure_type,meter_id,billperiod_type,meter_replaces_meterid,service_read_date,Service_contract,Programs)
SELECT
ClientId,
customer_id,
premise_id,
mail_address_line_1,
mail_address_line_2,
mail_address_line_3,
mail_city,
mail_state,
CASE 
	WHEN LEN(LTRIM(RTRIM(mail_zip_code))) = 5 THEN SUBSTRING(mail_zip_code, 1, 5)
	ELSE SUBSTRING(mail_zip_code, 1, 5) + SUBSTRING(mail_zip_code, 7, 4) 
END,
CASE WHEN CHARINDEX (',' , first_name ,0)=0 THEN first_name
ELSE REPLACE(first_name,',',' ') 
END, 
CASE WHEN CHARINDEX (',' , last_name ,0)=0 THEN last_name
ELSE REPLACE(last_name,',',' ') 
END,
NULLIF(REPLACE(REPLACE(REPLACE(phone_1, '(', ''), ')', ''), ' ', ''), '') ,
NULLIF(REPLACE(REPLACE(REPLACE(phone_2, '(', ''), ')', ''), ' ', ''), '') ,
CASE 
	WHEN LEN(email) < 6 THEN NULL
	ELSE COALESCE(NULLIF(LTRIM(RTRIM(email)), ''), email)
END,
customer_type, 
account_id,
CASE WHEN LEN(LTRIM(RTRIM(active_date))) = 0 THEN ''
ELSE CONVERT(CHAR(19),CONVERT(DATETIME, active_date, 120),121)
END,  --active_date  - need to look into formatting
'',  --inactive_date  - need to look into formatting
read_cycle,
rate_code,
'',						--This is blank for service_point_id.  UIL doesn't have this
service_street_name,	-- site_address_line1
service_unit,			-- site_address_line2
'',						-- site_address_line3
service_city,
service_state,
service_zip_code,
CASE WHEN meter_type = 'AMR' THEN 'ami' ELSE 'regular' END,
CASE WHEN LTRIM(RTRIM(meter_units))='3' THEN '0' ELSE '2' END , --have to change 1 to 0 for kwh, and 3 to 2 for CCF
bldg_sq_foot, 
year_built,
bedrooms,
assess_value,
CASE WHEN ISNUMERIC(usage_value) = 1
THEN
LEFT(usage_value,LEN(usage_value)-3)
ELSE 0
END,
CONVERT(CHAR(19),CONVERT(DATETIME, bill_enddate, 120),121),
bill_days,
is_estimate,
usage_charge,
service_commodity,
'',-- leave account_structure_type blank
CASE
	WHEN SUBSTRING(meter_id, 1, 1)='V' THEN meter_id
	ELSE meter_id
END,		--Need to add a 0 to the front of the meterid unless it's a Vmeter to match the meterids UI is sending in AMI data
billperiod_type,
meter_replaces_meterid,
CONVERT(CHAR(19),CONVERT(DATETIME, service_read_date, 120),121),
CONCAT('SC_',premise_id,'_electric'),					--We are making up the servicecontract id in the DW so, we don't want to carry this over.  It's not required in AclaraOne
''   -- leave programs blank
FROM dbo.RawBillTemp_224 rb
JOIN UIBTD2016Accounts btd
ON rb.account_id=btd.accountid


-- return this to ssis package and then write out to txt file from there
SELECT CONVERT(VARCHAR(10),ClientId) +  ',' + ISNULL(customer_id,'') + ',' + ISNULL(premise_id,'') + ',' + ISNULL(mail_address_line_1,'') + ',' + ISNULL(mail_address_line_2,'') + ','
 + ISNULL(mail_address_line_3,'')+ ',' + ISNULL(mail_city,'')+ ',' + ISNULL(mail_state,'')+ ',' + ISNULL(mail_zip_code,'')+ ',' + ISNULL(first_name,'')+ ',' + ISNULL(last_name,'')+ ',' 
 + ISNULL(phone_1,'')+ ',' + ISNULL(phone_2,'')+ ',' + ISNULL(email,'')+ ',' 
 + ISNULL(customer_type,'') + ',' + ISNULL(account_id,'') + ',' + ISNULL(CONVERT(VARCHAR(20),active_date),'') + ',' + ISNULL(CONVERT(VARCHAR(20),inactive_date),'') + ','
 + ISNULL(read_cycle,'') + ',' + ISNULL(rate_code,'') + ',' + ISNULL(service_point_id,'')  
 + ',' + ISNULL(site_addressline1,'') + ',' + ISNULL(site_addressline2,'') + ',' + ISNULL(site_addressline3,'') + ',' + ISNULL(site_city,'') + ',' + ISNULL(site_state,'') + ',' + ISNULL(site_zip_code,'') + ','
 + ISNULL(meter_type,'') + ',' + ISNULL(meter_units,'') + ',' + ISNULL(bldg_sq_foot,'') + ',' + ISNULL(year_built,'') + ',' + ISNULL(bedrooms,'') + ',' + ISNULL(assess_value,'') + ',' + ISNULL(usage_value,'') + ','
 + ISNULL(CONVERT(VARCHAR(20),bill_enddate),'') + ',' + ISNULL(CONVERT(VARCHAR(20),bill_days),'') + ',' + ISNULL(CONVERT(VARCHAR(20),is_estimate),'') + ','
 + ISNULL(CONVERT(VARCHAR(20),usage_charge),'') + ',' + ISNULL(CONVERT(VARCHAR(20),service_commodity),'') + ',' + ISNULL(CONVERT(VARCHAR(30),account_structure_type),'') + ',' 
 + ISNULL(meter_id,'') + ',' + ISNULL(CONVERT(VARCHAR(20),billperiod_type),'') + ',' + ISNULL(meter_replaces_meterid,'') + ',' + ISNULL(CONVERT(VARCHAR(20),service_read_date),'') + ',' 
 + ISNULL(Service_contract,'') + ',' + ISNULL(Programs,'') AS BillData, 'AclaraOneBill'
FROM #UIAclaraOneBillingData
ORDER BY rownum

END



GO


