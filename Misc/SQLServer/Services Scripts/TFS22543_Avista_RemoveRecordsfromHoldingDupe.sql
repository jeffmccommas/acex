DELETE holding.Billing_Duplicates 
 FROM holding.Billing_Duplicates bd 
 JOIN dbo.FactServicePointBilling fb WITH (NOLOCK)
 ON fb.AccountId=bd.AccountId
 AND fb.PremiseId=bd.PremiseId
 AND fb.EndDate=bd.BillEndDate
 AND fb.startDate=bd.BillStartDate
 AND fb.UtilityBillRecordId=bd.UtilityBillRecordIdT