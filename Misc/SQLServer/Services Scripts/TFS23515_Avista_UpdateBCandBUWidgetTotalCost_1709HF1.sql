SELECT * FROM cm.ClientConfiguration WHERE clientid=210 AND ConfigurationKey LIKE '%billcomparison.totalcost'
SELECT * FROM cm.ClientConfiguration WHERE clientid=210 AND ConfigurationKey LIKE '%billcomparison.showdetails'
SELECT * FROM cm.ClientConfiguration WHERE clientid=210 AND ConfigurationKey LIKE '%billedusagechart.addtototalcost' ORDER BY ConfigurationKey

UPDATE cm.ClientConfiguration SET value='GOVT_TAX_MUNI, STD_CUST, STD_MAX_DMND' 
WHERE clientid=210 AND ConfigurationKey LIKE '%billcomparison.totalcost' AND EnvironmentKey='prod'

UPDATE cm.ClientConfiguration SET value='true' 
WHERE clientid=210 AND ConfigurationKey LIKE '%billcomparison.showdetails' AND EnvironmentKey='prod'

UPDATE cm.ClientConfiguration SET value='GOVT_TAX_MUNI, STD_CUST, STD_MAX_DMND' 
WHERE clientid=210 AND ConfigurationKey LIKE '%billedusagechart.addtototalcost' AND EnvironmentKey='prod'