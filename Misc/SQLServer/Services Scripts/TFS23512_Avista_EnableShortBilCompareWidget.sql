SELECT * FROM cm.ClientConfigurationBulk WHERE clientid=210

SELECT * FROM cm.TypeConfigurationBulk ORDER BY ConfigurationID

INSERT INTO cm.TypeConfigurationBulk
        ( ConfigurationBulkKey ,
          ConfigurationID ,
          DateUpdated
        )
VALUES  ( 'tab.dashboard.billcomparison.billdetails' , -- ConfigurationBulkKey - varchar(256)
          42 , -- ConfigurationID - int
          GETDATE()  -- DateUpdated - datetime
        )

INSERT INTO cm.ClientConfigurationBulk
VALUES (210, 'prod', 'tab.dashboard.billcomparison.billdetails','tab.dashboard.billcomparison.billdetails','bill',
'JSON structure containing the bill detail ids, row order, type (cost, usage, other) and text content key.',
'{
  "BillDetails": [
    {
      "Type": "usage",
      "Order": 1,
      "Commodity": "electric",
      "DetailKey": "MAX_DMND",
      "TextContentKey": "max_dmnd"
    },
    {
      "Type": "cost",
      "Order": 2,
      "Commodity": "electric",
      "DetailKey": "STD_MAX_DMND",
      "TextContentKey": "std_max_dmnd"
    },
    {
      "Type": "cost",
      "Order": 5,
      "Commodity": "all",
      "DetailKey": "GOVT_TAX_MUNI",
      "TextContentKey": "govt_tax_muni"
    },
    {
      "Type": "cost",
      "Order": 4,
      "Commodity": "gas",
      "DetailKey": "STD_CUST",
      "TextContentKey": "std_cust_gas"
    },
    {
      "Type": "cost",
      "Order": 3,
      "Commodity": "electric",
      "DetailKey": "STD_CUST",
      "TextContentKey": "std_cust_elec"
    }
  ]
}',GETDATE())

