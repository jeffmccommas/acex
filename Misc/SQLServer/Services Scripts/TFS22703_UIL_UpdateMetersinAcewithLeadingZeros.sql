--UPDATE to add 0 in front of all meterids with length = 8, commodity key=1, and being entirely numeric.
UPDATE dimmeter 
SET meterid=CONCAT('0', meterid)
WHERE
LEN(RTRIM(LTRIM(meterid)))=8 AND CommodityKey=1 AND ISNUMERIC(meterid)=1
