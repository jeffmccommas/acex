--SELECT * FROM cm.ClientConfigurationBulk WHERE clientid=224 AND ConfigurationBulkKey = 'aclaraone.clientsettings' AND EnvironmentKey='qa' 
UPDATE cm.ClientConfigurationBulk SET JSONConfiguration=
'{
 "ClientId": 224,
 "TimeZone": "EST",
 "IsAmiIntervalStart": false,
 "IsDstHandled": true,
 "CalculationScheduleTime": "",
 "NotificationReportScheduleTime": "05:00",
 "InsightReportScheduleTime": "06:00",
 "ForgotPasswordEmailTemplateId": "af5c0251-486f-4100-a4cf-2d91a905f108",
 "RegistrationConfirmationEmailTemplateId": "7c0657cd-1020-42a4-a281-87db82a7f6c4",
 "OptInEmailConfirmationTemplateId": "777ca9f8-bb03-490b-946b-b1e6991b2b15",
 "OptInSmsConfirmationTemplateId": "224_12",
 "OptInEmailUserName": "uiprod",
 "OptInEmailPassword": "Xuiprod2311X",
 "OptInEmailFrom": "energyreportsCT@uinet.com",
 "TrumpiaShortCode": "99000",
 "TrumpiaApiKey": "b4c1971153b3509b6ec0d8a24a33454c",
 "TrumpiaUserName": "aclaraprod",
 "TrumpiaContactList": "AclaraDemoProd",
 "UnmaskedAccountIdEndingDigit": "4",
 "Programs": [
  {
   "ProgramName": "BTD2016elec",
   "UtilityProgramName": "BTD2016elec",
   "FileExportRequired": true,
   "DoubleOptInRequired": false,
   "TrumpiaKeyword": "abagdasarian2",
   "Insights": [
    {
     "InsightName": "BillToDate",
     "UtilityInsightName": "BillToDate",
     "AllowedCommunicationChannel": "Email",
     "DefaultCommunicationChannel": "Email",
     "DefaultFrequency": "Daily",
     "DefaultValue": "",
     "EmailTemplateId": "e95f1865-eed5-4421-83dc-3661611bc8e5",
     "SMSTemplateId": "224_1",
     "NotifyTime": "",
     "NotificationDay": "Thursday",
     "TrumpiaKeyword": "",
     "Level": "Account",
     "CommodityType": "",
     "ThresholdType": "",
     "ThresholdMin": "",
     "ThresholdMax": ""
    }
   ]
  }
 ],
  "ExtendedAttributes": "normalDecimalFormat=0.00,altNumericFormat=0"
}'
WHERE clientid=224 AND ConfigurationBulkKey = 'aclaraone.clientsettings' AND EnvironmentKey='prod' 

