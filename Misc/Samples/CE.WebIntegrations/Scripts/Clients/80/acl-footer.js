﻿//This is a simplified version of what Aclara would expect a client to implement
//in their environment in order to inject footer content into the Ace Website.
//There is a corresponding acl-footer.css file that contains the css classes
//used in the html below.

//The "footer" variable is appended to an html element with
//id="acl-remote-footer" which exists in the Ace Website.  This is done using the
//JQuery append() method.  
//Aclara will add a link to this javascript file and the acl-footer.css file so they 
//will be fired when each page loads in the Ace Website.

//If the footer were to contain links to the Ace Website the urls would have
//to contain the same querystring paramaters used to initially enter the
//Ace Website.  If there are only links that go to pages on the client's 
//website this is an unnecessary step.

$(document).ready(function () {
        //Building up the html content for the footer.  
        var footer = '';
        footer +=  "<div class='container'>";
        footer +=      "<div class='panel panel-default site-footer'>";
        footer +=          "<div class='panel-body xs-m-0'>";
        footer +=          "<div class='col-xs-12 col-ms-5 col-md-5 col-lg-5 xs-pl-0 footer__sitesocial text-xs-center'>";
        footer +=              "<ul class='pull-left-ms pull-left-sm pull-left-md pull-left-lg inline-block' style='margin-top: 10px; padding-left: 0; display: inline-block; list-style: none' role='menu'>";
        footer +=                  "<li style='display: inline-block; list-style: none' role='option'><span class='glyphicon glyphicon-home' aria-hidden='true'></span></li>";
        footer +=                  "<li style='display: inline-block; list-style: none; margin-left: 10px' role='option'><span class='glyphicon glyphicon-heart' aria-hidden='true'></span></li>";
        footer +=                  "<li style='display: inline-block; list-style: none; margin-left: 10px' role='option'><span class='glyphicon glyphicon-star' aria-hidden='true'></span></li>";
        footer +=              "</ul>";
        footer +=          "</div>";
        footer +=              "<div class='col-xs-12 col-ms-7 col-md-7 col-lg-7 text-right footer__sitelinks text-xs-center' role='navigation' aria-label='Account, Community, and working with us'>";
        footer +=                  "<ul class='top-links pull-right-ms pull-right-sm pull-right-md pull-right-lg inline-block' style='margin-top: 10px; padding-left: 0; display: inline-block; list-style: none' role='menu'>";
        footer +=                      "<li style='display: inline-block; list-style: none' role='option'><a href=''>Contact Us</a></li>";
        footer +=                      "<li style='display: inline-block; list-style: none; margin-left: 10px' role='option'><a href='#'>Privacy</a></li>";
        footer +=                      "<li style='display: inline-block; list-style: none; margin-left: 10px' role='option'><a href='#'>Security</a></li>";
        footer +=                  "</ul>";
        footer +=              "</div>";
        footer +=          "</div>";
        footer +=      "</div>";
        footer += "</div>";
        
        //This is where the client html is being injected into the Ace Website
        $('#acl-remote-footer').append(footer);
    });