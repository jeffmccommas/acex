﻿//This is a simplified version of what Aclara would expect a client to implement
//in their environment in order to inject header content into the Ace Website.
//There is a corresponding acl-header.css file that contains the css classes
//used in the html below.

//This is a static html example where there is a logo and three links.
//The "header" variable is appended to an html element with
//id="acl-remote-header" which exists in the Ace Website.  This is done using the
//JQuery append() method.  
//Aclara will add a link to this javascript file and the acl-header.css file so they 
//will be fired when each page loads in the Ace Website.

//If the header were to contain links to the Ace Website the urls would have
//to contain the same querystring paramaters used to initially enter the
//Ace Website.  If there are only links that go to pages on the client's 
//website this is an unnecessary step.

$(document).ready(function () {
        //Building up the html content for the header.  
        var header = "";
        header +=    "<div class='container'>";
        header +=        "<div class='panel panel-default site-header'>";
        header +=            "<div class='panel-body xs-m-0 xs-pl-0 xs-pr-0'>";
        header +=                "<div class='col-xs-12 col-ms-4 col-md-4 col-lg-4 xs-pl-0'>";
        header +=                   "<a class='logo xs-p-10 xs-pl-20 ms-pl-10' href='#'>";
        header += "<img class='logo desktop' alt='Consumer's Energy Website' src='https://AclAceWebIntegSampleWaDev.aclarax.com/Content/Clients/80/images/logo_ce.png' width='154' height='58'>";
        header +=                   "</a>";
        header +=              "</div>";
        header +=               "<div class='col-xs-12 col-ms-8 col-md-8 col-lg-8 text-right text-xs-center xs-pr-0 xs-pl-0' role='navigation' aria-label='Account, Community, and working with us'>";
        header +=                   "<ul class='top-links pull-right-ms pull-right-sm pull-right-md pull-right-lg inline-block xs-p-10 xs-pr-0 ms-pr-10' style='margin-top: 10px; display: inline-block; list-style: none' role='menu'>";
        header +=                      "<li style='display: inline-block; list-style: none' role='option'><a href='#'>EMERGENCY</a></li>";
        header +=                      "<li style='display: inline-block; list-style: none; margin-left: 10px' role='option'><a href='#'>CONTACT</a></li>";
        header +=                      "<li style='display: inline-block; list-style: none; margin-left: 10px; margin-right: 30px' role='option'><a href='#'>OUTAGES</a></li>";
        header +=                   "</ul>";
        header +=                   "<div class='remote-watermark'>Remote Header</div>";
        header +=               "</div>";
        header +=           "</div>";
        header +=        "</div>";
        header += "</div>";

        //This is where the client html is being injected into the Ace Website
        $('#acl-remote-header').append(header);
    });