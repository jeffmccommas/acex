﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CE.WebIntegrations.Startup))]
namespace CE.WebIntegrations
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
