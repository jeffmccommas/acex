﻿Place these files in a publically accessible location on your web server which is secured by SSL.  
Aclara will need to know the urls (https) to these files so we can add them to your Ace Website implementation.  
There are additional notes in the two javascript files provided. 