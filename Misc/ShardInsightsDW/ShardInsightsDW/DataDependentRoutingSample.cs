﻿/*
    Copyright 2014 Microsoft, Corp.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

using System;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement;

namespace InsightsDW
{
    internal static class DataDependentRoutingSample
    {
        private static string[] customerNames = new[]
        {
            "AdventureWorks Cycles", 
            "Contoso Ltd.", 
            "Microsoft Corp.", 
            "Northwind Traders", 
            "ProseWare, Inc.", 
            "Lucerne Publishing", 
            "Fabrikam, Inc.", 
            "Coho Winery", 
            "Alpine Ski House", 
            "Humongous Insurance"
        };

        private static Random r = new Random();

        public static void ExecuteDataDependentRoutingQuery(RangeShardMap<int> shardMap, string credentialsConnectionString)
        {
            // A real application handling a request would need to determine the request's customer ID before connecting to the database.
            // Since this is a demo app, we just choose a random key out of the range that is mapped. Here we assume that the ranges
            // start at 0, are contiguous, and are bounded (i.e. there is no range where HighIsMax == true)
            //int currentMaxHighKey = shardMap.GetMappings().Max(m => m.Value.High);
            //int ClientId = GetClientId(currentMaxHighKey);
            //string customerName = customerNames[r.Next(customerNames.Length)];
            //int regionId = 0;
            //int productId = 0;

            //AddCustomer(
            //    shardMap,
            //    credentialsConnectionString,
            //    ClientId,
            //    customerName,
            //    regionId);

            //AddOrder(
            //    shardMap,
            //    credentialsConnectionString,
            //    ClientId,
            //    productId);
        }

        /// <summary>
        /// Adds a customer to the customers table (or updates the customer if that id already exists).
        /// </summary>
//        private static void AddCustomer(
//            ShardMap shardMap,
//            string credentialsConnectionString,
//            int ClientId,
//            string name,
//            int regionId)
//        {
//            // Open and execute the command with retry for transient faults. Note that if the command fails, the connection is closed, so
//            // the entire block is wrapped in a retry. This means that only one command should be executed per block, since if we had multiple
//            // commands then the first command may be executed multiple times if later commands fail.
//            SqlDatabaseUtils.SqlRetryPolicy.ExecuteAction(() =>
//            {
//                // Looks up the key in the shard map and opens a connection to the shard
//                using (SqlConnection conn = shardMap.OpenConnectionForKey(ClientId, credentialsConnectionString))
//                {
//                    // Create a simple command that will insert or update the customer information
//                    SqlCommand cmd = conn.CreateCommand();
//                    cmd.CommandText = @"
//                    IF EXISTS (SELECT 1 FROM Customers WHERE ClientId = @ClientId)
//                        UPDATE Customers
//                            SET Name = @name, RegionId = @regionId
//                            WHERE ClientId = @ClientId
//                    ELSE
//                        INSERT INTO Customers (ClientId, Name, RegionId)
//                        VALUES (@ClientId, @name, @regionId)";
//                    cmd.Parameters.AddWithValue("@ClientId", ClientId);
//                    cmd.Parameters.AddWithValue("@name", name);
//                    cmd.Parameters.AddWithValue("@regionId", regionId);
//                    cmd.CommandTimeout = 60;

//                    // Execute the command
//                    cmd.ExecuteNonQuery();
//                }
//            });
//        }

        /// <summary>
        /// Adds an order to the orders table for the customer.
        /// </summary>
//        private static void AddOrder(
//            ShardMap shardMap,
//            string credentialsConnectionString,
//            int ClientId,
//            int productId)
//        {
//            SqlDatabaseUtils.SqlRetryPolicy.ExecuteAction(() =>
//            {
//                // Looks up the key in the shard map and opens a connection to the shard
//                using (SqlConnection conn = shardMap.OpenConnectionForKey(ClientId, credentialsConnectionString))
//                {
//                    // Create a simple command that will insert a new order
//                    SqlCommand cmd = conn.CreateCommand();

//                    // Create a simple command
//                    cmd.CommandText = @"INSERT INTO dbo.Orders (ClientId, OrderDate, ProductId)
//                                        VALUES (@ClientId, @orderDate, @productId)";
//                    cmd.Parameters.AddWithValue("@ClientId", ClientId);
//                    cmd.Parameters.AddWithValue("@orderDate", DateTime.Now.Date);
//                    cmd.Parameters.AddWithValue("@productId", productId);
//                    cmd.CommandTimeout = 60;

//                    // Execute the command
//                    cmd.ExecuteNonQuery();
//                }
//            });

//            ConsoleUtils.WriteInfo("Inserted order for customer ID: {0}", ClientId);
//        }

        /// <summary>
        /// Gets a customer ID to insert into the customers table.
        /// </summary>
        private static int GetClientId(int maxid)
        {
            // If this were a real app and we were inserting customer IDs, we would need a 
            // service that generates unique new customer IDs.

            // Since this is a demo, just create a random customer ID. To keep the numbers
            // manageable for demo purposes, only use a range of integers that lies within existing ranges.
            
            return r.Next(0, maxid);
        }
    }
}
