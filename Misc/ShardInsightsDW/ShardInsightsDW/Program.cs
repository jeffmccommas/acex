﻿/*
    Copyright 2014 Microsoft, Corp.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement;
using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement.Schema;

namespace InsightsDW
{
    internal class Program
    {
        public static void Main()
        {
            // Welcome screen
            Console.WriteLine("****************************************************************************");
            Console.WriteLine("*** Welcome to Aclara Elastic Database Managmenet tool                   ***");
            Console.WriteLine("*** Below is the design                                                  ***");
            Console.WriteLine("***                                                                      ***");
            Console.WriteLine("*** The shard map manager database for                                   ***");
            Console.WriteLine("*** both InsightsDW and InsightsMetaData                                 ***");
            Console.WriteLine("***                                is: InsightsDW                        ***");
            Console.WriteLine("*** The elastic query database for DW is: InsightsDW                     ***");
            Console.WriteLine("*** The shard for 101 is                : InsightsDW_101                 ***");
            Console.WriteLine("*** The shard for all others is         : InsightsDW_Test                ***");
            Console.WriteLine("***                                                                      ***");
            Console.WriteLine("*** The shard for InsightsMetaData is   : InsightsMetaData               ***");
            Console.WriteLine("****************************************************************************");
            Console.WriteLine();

            // Verify that we can connect to the Sql Database that is specified in App.config settings
            if (!SqlDatabaseUtils.TryConnectToSqlDatabase())
            {
                // Connecting to the server failed - please update the settings in App.Config

                // Give the user a chance to read the mesage, if this program is being run
                // in debug mode in Visual Studio
                if (Debugger.IsAttached)
                {
                    Console.WriteLine("Press ENTER to continue...");
                    Console.ReadLine();
                }

                // Exit
                return;
            }

            // Connection succeeded. Begin interactive loop
            MenuLoop();
        }

        /// <summary>
        /// The shard map manager, or null if it does not exist. 
        /// It is recommended that you keep only one shard map manager instance in
        /// memory per AppDomain so that the mapping cache is not duplicated.
        /// </summary>
        private static ShardMapManager InsightsDW_shardMapManager;
        static Shard Shard0 = null, Shard1 = null, Shard2 = null, Shard3 = null;

        #region Program control flow

        /// <summary>
        /// Main program loop.
        /// </summary>
        private static void MenuLoop()
        {
            // Get the shard map manager, if it already exists.

            // It is recommended that you keep only one shard map manager instance in
            // memory per AppDomain so that the mapping cache is not duplicated.

            // this manager will be used to hold the maps for both InsightsDW and InsightsMetadata

            InsightsDW_shardMapManager = ShardManagementUtils.TryGetShardMapManager(
                InsightsDWConfiguration.ShardMapManagerQAServerName,
                InsightsDWConfiguration.ShardMapManagerDatabaseName);

            // Loop until the user chose "Exit".
            bool continueLoop;
            do
            {
                PrintShardMapState();
                Console.WriteLine();

                PrintMenu();
                Console.WriteLine();

                continueLoop = GetMenuChoiceAndExecute();
                Console.WriteLine();
            }
            while (continueLoop);
        }

        /// <summary>
        /// Writes the shard map's state to the console.
        /// </summary>
        private static void PrintShardMapState()
        {
            Console.WriteLine("Current Shard Map state:");


            if (InsightsDW_shardMapManager != null)
            {
                Console.WriteLine("Shard Map Manager already exists");
            }
            else
            {
                Console.WriteLine("Shard Map Manager does not exists");
                
            }
            return;
            //================================================================
        //    ListShardMap<int> shardMap = TryGetShardMap();
        //    //RangeShardMap<int> shardMap = TryGetShardMap();
        //    if (shardMap == null)
        //    {
        //        return;
        //    }

        //    // Get all shards
        //    IEnumerable<Shard> allShards = shardMap.GetShards();

        //    // Get all mappings, grouped by the shard that they are on. We do this all in one go to minimise round trips.
        //    ILookup<Shard, RangeMapping<int>> mappingsGroupedByShard = shardMap.GetMappings().ToLookup(m => m.Shard);

        //    if (allShards.Any())
        //    {
        //        // The shard map contains some shards, so for each shard (sorted by database name)
        //        // write out the mappings for that shard
        //        foreach (Shard shard in shardMap.GetShards().OrderBy(s => s.Location.Database))
        //        {
        //           // IEnumerable<ListShardMap<int>> mappingsOnThisShard = mappingsGroupedByShard[shard];

        //            if (mappingsOnThisShard.Any())
        //            {
        //                string mappingsString = string.Join(", ", mappingsOnThisShard.Select(m => m.Value));
        //                Console.WriteLine("\t{0} contains key range {1}", shard.Location.Database, mappingsString);
        //            }
        //            else
        //            {
        //                Console.WriteLine("\t{0} contains no key ranges.", shard.Location.Database);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        Console.WriteLine("\tShard Map contains no shards");
        //    }
        }

        private const ConsoleColor EnabledColor = ConsoleColor.White; // color for items that are expected to succeed
        private const ConsoleColor DisabledColor = ConsoleColor.DarkGray; // color for items that are expected to fail

        /// <summary>
        /// Writes the program menu.
        /// </summary>
        private static void PrintMenu()
        {
            ConsoleColor createSmmColor; // color for create shard map manger menu item
            ConsoleColor otherMenuItemColor; // color for other menu items
            if (InsightsDW_shardMapManager == null)
            {
                createSmmColor = EnabledColor;
                otherMenuItemColor = DisabledColor;
            }
            else
            {
                createSmmColor = DisabledColor;
                otherMenuItemColor = EnabledColor;
            }

            ConsoleUtils.WriteColor(createSmmColor, "1. Create InsightsDW shard map manager, and add the 4 InsightsDW shards ");
            ConsoleUtils.WriteColor(otherMenuItemColor, "2. Add 4 shards to InsightsDW (map manager must already exist)");
            ConsoleUtils.WriteColor(otherMenuItemColor, "3. Remove and re-add InsightsDW Schema (map manager and map must already exist)");
            ConsoleUtils.WriteColor(otherMenuItemColor, "4. Create InsightsMetaData shardmap in existing manager (used for InsightsDW map),"); 
            ConsoleUtils.WriteColor(otherMenuItemColor, "   and add the 1 InsightsMetaData shard");
            //ConsoleUtils.WriteColor(otherMenuItemColor, "3. Insert sample rows using Data-Dependent Routing");
            //ConsoleUtils.WriteColor(otherMenuItemColor, "4. Execute sample Multi-Shard Query");
            ConsoleUtils.WriteColor(otherMenuItemColor, "5. Delete shard mappings for the 2 insightsDW shards (leaves maps and manager)");
            ConsoleUtils.WriteColor(otherMenuItemColor, "6. Delete shard mappings for the insightsMetadata shard (leaves maps and manager)");
            ConsoleUtils.WriteColor(otherMenuItemColor, "7. Delete shard map for the insightsDW  (leaves manager)");
            ConsoleUtils.WriteColor(otherMenuItemColor, "8. Delete shard map for the insightsmetadata  (leaves manager)");
            ConsoleUtils.WriteColor(otherMenuItemColor, "9. List Shards in DW map");
            ConsoleUtils.WriteColor(otherMenuItemColor, "10. Add just the 101 mappings");
            ConsoleUtils.WriteColor(otherMenuItemColor, "11. Add Shard");
            ConsoleUtils.WriteColor(otherMenuItemColor, "12. Delete Shard");
            ConsoleUtils.WriteColor(otherMenuItemColor, "13. Add Client to Test Shard (InsightsDW_Test)");
            ConsoleUtils.WriteColor(EnabledColor, "14. Exit");
        }

        /// <summary>
        /// Gets the user's chosen menu item and executes it.
        /// </summary>
        /// <returns>true if the program should continue executing.</returns>
        private static bool GetMenuChoiceAndExecute()
        {
            //AddClientToTestShard(283);

            while (true)
            {
                int clientId;
                int inputValue = ConsoleUtils.ReadIntegerInput("Enter an option [1-14] and press ENTER: ");

                switch (inputValue)
                {
                    case 1: // Create shard map manager
                        Console.WriteLine();
                        CreateInsightsDWShardMapManagerAndShard();
                        return true;
                    case 2: // Add shard
                        Console.WriteLine();
                        Create4InsightsDWShards();
                        return true;
                    case 3: // Create Schema Info for InsightsDW
                        Console.WriteLine();
                        CreateSchemaInfoForInsightsDW();
                        return true;
                    case 4: // Create the one InsightsMetaData Shard
                        Console.WriteLine();
                        Create1InsightsMetaDataShard();
                        return true;                        
                    //case 3: // Data Dependent Routing
                    //    Console.WriteLine();
                    //    DataDepdendentRouting();
                    //    return true;
                    //case 4: // Multi-Shard Query
                    //    Console.WriteLine();
                    //    MultiShardQuery();
                    //    return true;
                    case 5: // Delete the InsightsDW shards
                        Console.WriteLine();
                        DeleteInsightDWShards();
                        return true;
                    case 6: // Delete the InsightsMetaData shard
                        Console.WriteLine();
                        DeleteInsightMetaDataShard();
                        return true;
                    case 7: // Delete the InsightsDW shard map
                        Console.WriteLine();
                        DeleteInsightsDWShardMap();
                        return true;
                    case 8: // Delete the Insightsmetadata shard map
                        Console.WriteLine();
                        DeleteInsightsMetaDataShardMap();
                        return true;
                    case 9: 
                        Console.WriteLine();
                        ListDWShards();
                        return true;
                    case 10:
                        Console.WriteLine();
                        Add101Mappings();
                        return true;
                    case 11:
                        Console.WriteLine();
                        clientId = ConsoleUtils.ReadIntegerInput("Enter clientId and press ENTER: ");
                        AddShard(clientId);
                        return true;
                    case 12:
                        Console.WriteLine();
                        clientId = ConsoleUtils.ReadIntegerInput("Enter clientId and press ENTER: ");
                        DeleteShard(clientId);
                        return true;
                    case 13:
                        Console.WriteLine();
                        clientId = ConsoleUtils.ReadIntegerInput("Enter clientId and press ENTER: ");
                        AddClientToTestShard(clientId);
                        return true;
                    case 14://Exit
                        return false;
                }
            }  
        }

        #endregion      

        #region Menu item implementations

        /// <summary>
        /// Creates a shard map manager, creates a shard map, and creates a shard
        /// with a mapping for the full range of 32-bit integers.
        /// </summary>
        private static void CreateInsightsDWShardMapManagerAndShard()
        {
            ListShardMap<long> sm = null;
            string shardMapManagerConnectionString =
               InsightsDWConfiguration.GetConnectionString(
                   InsightsDWConfiguration.ShardMapManagerQAServerName,
                   InsightsDWConfiguration.ShardMapManagerDatabaseName, "QA");

            // check if shardmap exists and if not, create it 
            if (InsightsDW_shardMapManager != null)
            {
                ConsoleUtils.WriteWarning("Shard Map Manager already exists");
                return;
            }
            else
            {
                // Create shard map manager

                InsightsDW_shardMapManager = ShardManagementUtils.CreateOrGetShardMapManager(shardMapManagerConnectionString);
                
            }

            Create4InsightsDWShards();

            // Create shard map manager database
            //if (!SqlDatabaseUtils.DatabaseExists(InsightsDWConfiguration.ShardMapManagerServerName, InsightsDWConfiguration.ShardMapManagerDatabaseName))
            //{
            //    SqlDatabaseUtils.CreateDatabase(InsightsDWConfiguration.ShardMapManagerServerName, InsightsDWConfiguration.ShardMapManagerDatabaseName);
            //}

            //string mapName ="InsightsDW_map";
            //// Create shard map manager
            //if (!shardMapManager.TryGetListShardMap(mapName, out sm)) 
            //{ 
            //    sm = shardMapManager.CreateListShardMap<long>(mapName); 
            //}

            //// Create shard map
            //Shard shard0 = null, shard1 = null; 
            //// check if shard exists and if not, 
            //// create it (Idempotent / tolerant of re-execute) 
            //if (!sm.TryGetShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerServerName, "InsightsDW_101"), out shard0))
            //{
            //    Shard0 = sm.CreateShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerServerName, "InsightsDW_101"));
            //    Console.WriteLine("shard InsightsDW_101 created");
            //}

            //if (!sm.TryGetShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerServerName, "InsightsDW_Test"), out shard1))
            //{
            //    Shard1 = sm.CreateShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerServerName, "InsightsDW_Test"));
            //    Console.WriteLine("shard InsightsDW_Test created");
            //} 

            ////
            //// if the shards were just created create the mappings
            //if (Shard0 !=null)
            //{
            //    sm.CreatePointMapping(new PointMappingCreationInfo<long>(101, Shard0, MappingStatus.Online));
            //    Console.WriteLine("client 101 mapping created to InsightsDW_101");
            //}
            //if (Shard1 != null)
            //{
            //    sm.CreatePointMapping(new PointMappingCreationInfo<long>(87, Shard0, MappingStatus.Online));
            //    Console.WriteLine("client 87 mapping created to InsightsDW_Test");
            //    sm.CreatePointMapping(new PointMappingCreationInfo<long>(256, Shard0, MappingStatus.Online));
            //    Console.WriteLine("client 256 mapping created to InsightsDW_Test");
            //}


            //CreateSchemaInfo(sm.Name);
            //// List the shards and mappings 
            //foreach (Shard s in sm.GetShards()
            //                        .OrderBy(s => s.Location.DataSource)
            //                        .ThenBy(s => s.Location.Database))
            //{
            //    Console.WriteLine("shard: " + s.Location);
            //}

            //foreach (PointMapping<long> rm in sm.GetMappings())
            //{
            //    Console.WriteLine("key: [" + rm.Value.ToString() + "]  ==>" + rm.Shard.Location); 

            //} 

            
        }

        private static void AddShard(int clientId)
        {
            string databaseName = "InsightsDW_" + clientId.ToString();

            // Only create the database if it doesn't already exist. It might already exist if
            // we tried to create it previously but hit a transient fault.
            if (!SqlDatabaseUtils.DatabaseExists(InsightsDWConfiguration.ShardMapManagerQAServerName, databaseName))
            {
                SqlDatabaseUtils.CreateDatabase(InsightsDWConfiguration.ShardMapManagerQAServerName, databaseName);
            }

            ListShardMap<long> sm = null;
            string mapName = "InsightsDW_map";
            // Create shard map manager
            if (!InsightsDW_shardMapManager.TryGetListShardMap(mapName, out sm))
            {
                sm = InsightsDW_shardMapManager.CreateListShardMap<long>(mapName);
            }

            Shard shard0 = null;
            if (!sm.TryGetShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, databaseName), out shard0))
            {
               var Shard = sm.CreateShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, databaseName));
                Console.WriteLine("shard " + databaseName + " created");

                if (Shard != null)
                {
                    sm.CreatePointMapping(new PointMappingCreationInfo<long>(clientId, Shard, MappingStatus.Online));
                    Console.WriteLine("client " + clientId + " mapping created to " + databaseName);
                }
            }

            //CreateSchemaInfoForInsightsDW(sm.Name);
            // List the shards and mappings 
            foreach (Shard s in sm.GetShards()
                                    .OrderBy(s => s.Location.DataSource)
                                    .ThenBy(s => s.Location.Database))
            {
                Console.WriteLine("shard: " + s.Location);
            }

            foreach (PointMapping<long> rm in sm.GetMappings())
            {
                Console.WriteLine("key: [" + rm.Value.ToString() + "]  ==>" + rm.Shard.Location);

            } 
        }

        /// <summary>
        /// Creates a shard map manager, creates a shard map, and creates a shard
        /// with a mapping for the full range of 32-bit integers.
        /// </summary>
        private static void Create4InsightsDWShards()
        {
            ListShardMap<long> sm = null;
            string mapName = "InsightsDW_map";
            // Create shard map manager
            if (!InsightsDW_shardMapManager.TryGetListShardMap(mapName, out sm))
            {
                sm = InsightsDW_shardMapManager.CreateListShardMap<long>(mapName);
            }

            // Create shard map
            Shard shard0 = null, shard1 = null, shard2 = null, shard3 = null;
            // check if shard exists and if not, 
            // create it (Idempotent / tolerant of re-execute) 
            if (!sm.TryGetShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsDW_101"), out shard0))
            {
                Shard0 = sm.CreateShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsDW_101"));
                Console.WriteLine("shard InsightsDW_101 created");
            }
            if (!sm.TryGetShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsDW_224"), out shard1))
            {
                Shard1 = sm.CreateShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsDW_224"));
                Console.WriteLine("shard InsightsDW_224 created");
            }
            if (!sm.TryGetShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsDW_276"), out shard2))
            {
                Shard2 = sm.CreateShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsDW_276"));
                Console.WriteLine("shard InsightsDW_276 created");
            }
            if (!sm.TryGetShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsDW_Test"), out shard3))
            {
                Shard3 = sm.CreateShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsDW_Test"));
                Console.WriteLine("shard InsightsDW_Test created");
            }

            //
            // if the shards were just created create the mappings
            if (Shard0 != null)
            {
                sm.CreatePointMapping(new PointMappingCreationInfo<long>(101, Shard0, MappingStatus.Online));
                Console.WriteLine("client 101 mapping created to InsightsDW_101");
            }
            if (Shard1 != null)
            {
                sm.CreatePointMapping(new PointMappingCreationInfo<long>(224, Shard1, MappingStatus.Online));
                Console.WriteLine("client 224 mapping created to InsightsDW_224");
            }
            if (Shard2 != null)
            {
                sm.CreatePointMapping(new PointMappingCreationInfo<long>(276, Shard2, MappingStatus.Online));
                Console.WriteLine("client 276 mapping created to InsightsDW_276");
            }
            if (Shard3 != null)
            {
                sm.CreatePointMapping(new PointMappingCreationInfo<long>(87, Shard3, MappingStatus.Online));
                Console.WriteLine("client 87 mapping created to InsightsDW_Test");
                sm.CreatePointMapping(new PointMappingCreationInfo<long>(256, Shard3, MappingStatus.Online));
                Console.WriteLine("client 256 mapping created to InsightsDW_Test");
            }


            CreateSchemaInfoForInsightsDW(sm.Name);
            // List the shards and mappings 
            foreach (Shard s in sm.GetShards()
                                    .OrderBy(s => s.Location.DataSource)
                                    .ThenBy(s => s.Location.Database))
            {
                Console.WriteLine("shard: " + s.Location);
            }

            foreach (PointMapping<long> rm in sm.GetMappings())
            {
                Console.WriteLine("key: [" + rm.Value.ToString() + "]  ==>" + rm.Shard.Location);

            } 
        }

        private static void AddClientToTestShard(int clientId)
        {
            ListShardMap<long> sm = null;
            string mapName = "InsightsDW_map";
            // Create shard map manager
            if (InsightsDW_shardMapManager.TryGetListShardMap(mapName, out sm))
            {
                Shard shard3 = null;

                if (sm.TryGetShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsDW_Test"), out shard3))
                {
                    sm.CreatePointMapping(new PointMappingCreationInfo<long>(clientId, shard3, MappingStatus.Online));
                    Console.WriteLine("client " + clientId + " mapping created to InsightsDW_Test");
                }
            }
        }
        private static void Add101Mappings()
        {
            ListShardMap<long> sm = null;
            string mapName = "InsightsDW_map";
            Shard shard0;
            // Create shard map manager
            if (!InsightsDW_shardMapManager.TryGetListShardMap(mapName, out sm))
            {
                sm = InsightsDW_shardMapManager.CreateListShardMap<long>(mapName);
            }
            if (!sm.TryGetShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsDW_101"), out shard0))
            {
                Shard0 = sm.CreateShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsDW_101"));
                Console.WriteLine("shard InsightsDW_101 created");
            }
            if (Shard0 == null && shard0 != null)
            {
                Shard0 = shard0;
            }
            if (Shard0 != null)
            {
                sm.CreatePointMapping(new PointMappingCreationInfo<long>(101, Shard0, MappingStatus.Online));
                Console.WriteLine("client 101 mapping created to InsightsDW_101");
            }
        }
        private static void ListDWShards()
        {
            ListShardMap<long> sm = null;
            string mapName = "InsightsDW_map";
            // Create shard map manager
            if (!InsightsDW_shardMapManager.TryGetListShardMap(mapName, out sm))
            {
                sm = InsightsDW_shardMapManager.CreateListShardMap<long>(mapName);
            }
            // List the shards and mappings 
            foreach (Shard s in sm.GetShards()
                                    .OrderBy(s => s.Location.DataSource)
                                    .ThenBy(s => s.Location.Database))
            {
                Console.WriteLine("shard: " + s.Location);
            }

            foreach (PointMapping<long> rm in sm.GetMappings())
            {
                Console.WriteLine("key: [" + rm.Value.ToString() + "]  ==>" + rm.Shard.Location);

            }
        }
        /// <summary>
        /// Creates a shard map manager, creates a shard map, and creates a shard
        /// with a mapping for the full range of 32-bit integers.
        /// </summary>
        private static void Create1InsightsMetaDataShard()
        {
            ListShardMap<long> sm = null;
            string mapName = "InsightsMetaData_map";
            // Create shard map manager
            if (!InsightsDW_shardMapManager.TryGetListShardMap(mapName, out sm))
            {
                sm = InsightsDW_shardMapManager.CreateListShardMap<long>(mapName);
            }

            // Create shard map
            Shard shard0 = null;
            // check if shard exists and if not, 
            // create it (Idempotent / tolerant of re-execute) 
            if (!sm.TryGetShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsMetaData"), out shard0))
            {
                Shard0 = sm.CreateShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsMetaData"));
                Console.WriteLine("shard InsightsMetaData created");
                CreateSchemaInfoForInsightsMetaData(sm.Name);
            }


            //
            // we dont need any mapping


           
            // List the shards and mappings 
            foreach (Shard s in sm.GetShards()
                                    .OrderBy(s => s.Location.DataSource)
                                    .ThenBy(s => s.Location.Database))
            {
                Console.WriteLine("shard: " + s.Location);
            }


        }

        private static void CreateSchemaInfoForInsightsDW()
        {
            ListShardMap<long> sm = null;
            string mapName = "InsightsDW_map";
            if (InsightsDW_shardMapManager.TryGetListShardMap(mapName, out sm))
            {
                CreateSchemaInfoForInsightsDW(sm.Name);
            }
        }


        private static void CreateSchemaInfoForInsightsMetaData()
        {
            ListShardMap<long> sm = null;
            string mapName = "InsightsMetaData_map";
            if (InsightsDW_shardMapManager.TryGetListShardMap(mapName, out sm))
            {
                CreateSchemaInfoForInsightsDW(sm.Name);
            }
        }


        /// <summary>
        /// Creates schema info for the schema defined in InitializeShard.sql.
        /// </summary>
        private static void CreateSchemaInfoForInsightsMetaData(string shardMapName)
        {
            try
            {
                if (InsightsDW_shardMapManager.GetSchemaInfoCollection() != null)
                {
                    InsightsDW_shardMapManager.GetSchemaInfoCollection().Remove(shardMapName);
                }
            }
            catch(Exception ex)
            {
                //
            }
            Console.WriteLine("Starting Schema creation for InsightsMetaData map");
            // Create schema info
            SchemaInfo schemaInfo = new SchemaInfo();
            schemaInfo.Add(new ReferenceTableInfo("ActionItemImportErrors"));
            schemaInfo.Add(new ReferenceTableInfo("AppTaskHeartbeat"));
            schemaInfo.Add(new ReferenceTableInfo("AppTaskRunStatus"));
            schemaInfo.Add(new ReferenceTableInfo("AppTasks"));
            schemaInfo.Add(new ReferenceTableInfo("BillImportErrors"));
            schemaInfo.Add(new ReferenceTableInfo("Client"));
            schemaInfo.Add(new ReferenceTableInfo("ClientAction"));
            schemaInfo.Add(new ReferenceTableInfo("ClientActionAppliance"));
            schemaInfo.Add(new ReferenceTableInfo("ClientActionCondition"));
            schemaInfo.Add(new ReferenceTableInfo("ClientActionSavings"));
            schemaInfo.Add(new ReferenceTableInfo("ClientActionSavingsCondition"));
            schemaInfo.Add(new ReferenceTableInfo("ClientActionSeason"));
            schemaInfo.Add(new ReferenceTableInfo("ClientActionWhatIfData"));
            schemaInfo.Add(new ReferenceTableInfo("ClientAppliance"));
            schemaInfo.Add(new ReferenceTableInfo("ClientApplianceExpression"));
            schemaInfo.Add(new ReferenceTableInfo("ClientApplianceProfileAttribute"));
            schemaInfo.Add(new ReferenceTableInfo("ClientAsset"));
            schemaInfo.Add(new ReferenceTableInfo("ClientAssetLocaleContent"));
            schemaInfo.Add(new ReferenceTableInfo("ClientBenchmarkGroup"));
            schemaInfo.Add(new ReferenceTableInfo("ClientCommodity"));
            schemaInfo.Add(new ReferenceTableInfo("ClientCondition"));
            schemaInfo.Add(new ReferenceTableInfo("ClientConfiguration"));
            schemaInfo.Add(new ReferenceTableInfo("ClientCurrency"));
            schemaInfo.Add(new ReferenceTableInfo("ClientEnduse"));
            schemaInfo.Add(new ReferenceTableInfo("ClientEnduseAppliance"));
            schemaInfo.Add(new ReferenceTableInfo("ClientEnumeration"));
            schemaInfo.Add(new ReferenceTableInfo("ClientEnumerationEnumerationItem"));
            schemaInfo.Add(new ReferenceTableInfo("ClientEnumerationItem"));
            schemaInfo.Add(new ReferenceTableInfo("ClientExpression"));
            schemaInfo.Add(new ReferenceTableInfo("ClientFileContent"));
            schemaInfo.Add(new ReferenceTableInfo("ClientFilePurgePolicy"));
            schemaInfo.Add(new ReferenceTableInfo("ClientLayout"));
            schemaInfo.Add(new ReferenceTableInfo("ClientMeasurement"));
            schemaInfo.Add(new ReferenceTableInfo("ClientProfileAttribute"));
            schemaInfo.Add(new ReferenceTableInfo("ClientProfileAttributeCondition"));
            schemaInfo.Add(new ReferenceTableInfo("ClientProfileAttributeProfileOption"));
            schemaInfo.Add(new ReferenceTableInfo("ClientProfileDefault"));
            schemaInfo.Add(new ReferenceTableInfo("ClientProfileDefaultCollection"));
            schemaInfo.Add(new ReferenceTableInfo("ClientProfileOption"));
            schemaInfo.Add(new ReferenceTableInfo("ClientProfileSection"));
            schemaInfo.Add(new ReferenceTableInfo("ClientProfileSectionAttribute"));
            schemaInfo.Add(new ReferenceTableInfo("ClientProfileSectionCondition"));
            schemaInfo.Add(new ReferenceTableInfo("ClientPropertyData"));
            schemaInfo.Add(new ReferenceTableInfo("ClientSeason"));
            schemaInfo.Add(new ReferenceTableInfo("ClientTab"));
            schemaInfo.Add(new ReferenceTableInfo("ClientTabAction"));
            schemaInfo.Add(new ReferenceTableInfo("ClientTabChildTab"));
            schemaInfo.Add(new ReferenceTableInfo("ClientTabCondition"));
            schemaInfo.Add(new ReferenceTableInfo("ClientTabConfiguration"));
            schemaInfo.Add(new ReferenceTableInfo("ClientTabProfileSection"));
            schemaInfo.Add(new ReferenceTableInfo("ClientTabTextContent"));
            schemaInfo.Add(new ReferenceTableInfo("ClientTextContent"));
            schemaInfo.Add(new ReferenceTableInfo("ClientTextContentLocaleContent"));
            schemaInfo.Add(new ReferenceTableInfo("ClientUOM"));
            schemaInfo.Add(new ReferenceTableInfo("ClientWhatIfData"));
            schemaInfo.Add(new ReferenceTableInfo("ClientWidget"));
            schemaInfo.Add(new ReferenceTableInfo("ClientWidgetCondition"));
            schemaInfo.Add(new ReferenceTableInfo("ClientWidgetConfiguration"));
            schemaInfo.Add(new ReferenceTableInfo("ClientWidgetTextContent"));
            schemaInfo.Add(new ReferenceTableInfo("CommandLog"));
            schemaInfo.Add(new ReferenceTableInfo("Configuration"));
            schemaInfo.Add(new ReferenceTableInfo("ContentCache"));
            schemaInfo.Add(new ReferenceTableInfo("ContentOrphanLog"));
            schemaInfo.Add(new ReferenceTableInfo("CustomerExportConfiguration"));
            schemaInfo.Add(new ReferenceTableInfo("CustomerImportErrors"));
            schemaInfo.Add(new ReferenceTableInfo("DatabaseDiagrams"));
            schemaInfo.Add(new ReferenceTableInfo("DisAggErrorLog"));
            schemaInfo.Add(new ReferenceTableInfo("EMApplianceRegion"));
            schemaInfo.Add(new ReferenceTableInfo("EMDailyWeatherDetail"));
            schemaInfo.Add(new ReferenceTableInfo("EMDegreeDay"));
            schemaInfo.Add(new ReferenceTableInfo("EMEvaporation"));
            schemaInfo.Add(new ReferenceTableInfo("EMSolar"));
            schemaInfo.Add(new ReferenceTableInfo("EMTemperatureProfile"));
            schemaInfo.Add(new ReferenceTableInfo("EMTemperatureRegion"));
            schemaInfo.Add(new ReferenceTableInfo("EMZipcode"));
            schemaInfo.Add(new ReferenceTableInfo("EnergyEventImportLog"));
            schemaInfo.Add(new ReferenceTableInfo("EnvironmentConfiguration"));
            schemaInfo.Add(new ReferenceTableInfo("ETLBulkConfiguration"));
            schemaInfo.Add(new ReferenceTableInfo("ETLInfo"));
            schemaInfo.Add(new ReferenceTableInfo("ExecutionErrors"));
            schemaInfo.Add(new ReferenceTableInfo("ExecutionLog"));
            schemaInfo.Add(new ReferenceTableInfo("INV_Action"));
            schemaInfo.Add(new ReferenceTableInfo("INV_FactAction"));
            schemaInfo.Add(new ReferenceTableInfo("INV_FactBilling"));
            schemaInfo.Add(new ReferenceTableInfo("INV_FactPremiseAttribute"));
            schemaInfo.Add(new ReferenceTableInfo("NotificationEventLog"));
            schemaInfo.Add(new ReferenceTableInfo("OptOutChannel"));
            schemaInfo.Add(new ReferenceTableInfo("ProcessLog"));
            schemaInfo.Add(new ReferenceTableInfo("ProfileImportErrors"));
            schemaInfo.Add(new ReferenceTableInfo("StatisticLog"));
            schemaInfo.Add(new ReferenceTableInfo("TypeAction"));
            schemaInfo.Add(new ReferenceTableInfo("TypeActionStatus"));
            schemaInfo.Add(new ReferenceTableInfo("TypeActionType"));
            schemaInfo.Add(new ReferenceTableInfo("TypeAppliance"));
            schemaInfo.Add(new ReferenceTableInfo("TypeAsset"));
            schemaInfo.Add(new ReferenceTableInfo("TypeBenchmarkGroup"));
            schemaInfo.Add(new ReferenceTableInfo("TypeCategory"));
            schemaInfo.Add(new ReferenceTableInfo("TypeCommodity"));
            schemaInfo.Add(new ReferenceTableInfo("TypeCondition"));
            schemaInfo.Add(new ReferenceTableInfo("TypeConfiguration"));
            schemaInfo.Add(new ReferenceTableInfo("TypeCurrency"));
            schemaInfo.Add(new ReferenceTableInfo("TypeDifficulty"));
            schemaInfo.Add(new ReferenceTableInfo("TypeEnduse"));
            schemaInfo.Add(new ReferenceTableInfo("TypeEnduseCategory"));
            schemaInfo.Add(new ReferenceTableInfo("TypeEntityLevel"));
            schemaInfo.Add(new ReferenceTableInfo("TypeEnumeration"));
            schemaInfo.Add(new ReferenceTableInfo("TypeEnumerationItem"));
            schemaInfo.Add(new ReferenceTableInfo("TypeEnvironment"));
            schemaInfo.Add(new ReferenceTableInfo("TypeExpression"));
            schemaInfo.Add(new ReferenceTableInfo("TypeFileContent"));
            schemaInfo.Add(new ReferenceTableInfo("TypeHabitInterval"));
            schemaInfo.Add(new ReferenceTableInfo("TypeLayout"));
            schemaInfo.Add(new ReferenceTableInfo("TypeLocale"));
            schemaInfo.Add(new ReferenceTableInfo("TypeMeasurement"));
            schemaInfo.Add(new ReferenceTableInfo("TypeProfileAttribute"));
            schemaInfo.Add(new ReferenceTableInfo("TypeProfileAttributeType"));
            schemaInfo.Add(new ReferenceTableInfo("TypeProfileDefault"));
            schemaInfo.Add(new ReferenceTableInfo("TypeProfileDefaultCollection"));
            schemaInfo.Add(new ReferenceTableInfo("TypeProfileOption"));
            schemaInfo.Add(new ReferenceTableInfo("TypeProfileSection"));
            schemaInfo.Add(new ReferenceTableInfo("TypeProfileSource"));
            schemaInfo.Add(new ReferenceTableInfo("TypeQuality"));
            schemaInfo.Add(new ReferenceTableInfo("TypeSeason"));
            schemaInfo.Add(new ReferenceTableInfo("TypeTab"));
            schemaInfo.Add(new ReferenceTableInfo("TypeTabType"));
            schemaInfo.Add(new ReferenceTableInfo("TypeTextContent"));
            schemaInfo.Add(new ReferenceTableInfo("TypeUom"));
            schemaInfo.Add(new ReferenceTableInfo("TypeWhatIfData"));
            schemaInfo.Add(new ReferenceTableInfo("TypeWidget"));
            schemaInfo.Add(new ReferenceTableInfo("TypeWidgetType"));
            schemaInfo.Add(new ReferenceTableInfo("UtilityPrograms"));
            // Register it with the shard map manager for the given shard map name
            InsightsDW_shardMapManager.GetSchemaInfoCollection().Add(shardMapName, schemaInfo);
            Console.WriteLine("Schema creation for InsightsMetaData map complete");
        }
     
        /// <summary>
        /// Creates schema info for the schema defined in InitializeShard.sql.
        /// </summary>
        private static void CreateSchemaInfoForInsightsDW(string shardMapName)
        {
            try
            {
                if (InsightsDW_shardMapManager.GetSchemaInfoCollection() != null)
                {
                    InsightsDW_shardMapManager.GetSchemaInfoCollection().Remove(shardMapName);
                }
            }
            catch(Exception ex)
            {
                //
            }
           
            Console.WriteLine("Starting Schema creation for InsightsDW map");
            // Create schema info
            SchemaInfo schemaInfo = new SchemaInfo();
            schemaInfo.Add(new ReferenceTableInfo("BenchmarkConfiguration"));
            schemaInfo.Add(new ReferenceTableInfo("BenchmarkConfigurationDetail"));
            schemaInfo.Add(new ReferenceTableInfo("BenchmarkErrorType"));
            schemaInfo.Add(new ReferenceTableInfo("BenchmarkGroupType"));
            schemaInfo.Add(new ReferenceTableInfo("DimAction"));
            schemaInfo.Add(new ReferenceTableInfo("DimActionStatus"));
            schemaInfo.Add(new ReferenceTableInfo("DimBillPeriodType"));
            schemaInfo.Add(new ReferenceTableInfo("DimChannel"));
            schemaInfo.Add(new ReferenceTableInfo("DimCity"));
            schemaInfo.Add(new ReferenceTableInfo("DimCommodity"));
            schemaInfo.Add(new ReferenceTableInfo("DimCountryRegion"));
            schemaInfo.Add(new ReferenceTableInfo("DimCustomerAuthenticationType"));
            schemaInfo.Add(new ReferenceTableInfo("DimCustomerType"));
            schemaInfo.Add(new ReferenceTableInfo("DimDate"));
            schemaInfo.Add(new ReferenceTableInfo("DimEnergyObject"));
            schemaInfo.Add(new ReferenceTableInfo("DimEventAction"));
            schemaInfo.Add(new ReferenceTableInfo("DimEventClass"));
            schemaInfo.Add(new ReferenceTableInfo("DimEventInfo"));
            schemaInfo.Add(new ReferenceTableInfo("DimEventType"));
            schemaInfo.Add(new ReferenceTableInfo("DimLatestYesOrNo"));
            schemaInfo.Add(new ReferenceTableInfo("DimMeterIntervalLength"));
            schemaInfo.Add(new ReferenceTableInfo("DimMeterType"));
            schemaInfo.Add(new ReferenceTableInfo("DimOrigin"));
            schemaInfo.Add(new ReferenceTableInfo("DimPostalCode"));
            schemaInfo.Add(new ReferenceTableInfo("DimPremiseAttribute"));
            schemaInfo.Add(new ReferenceTableInfo("DimPremiseOption"));
            schemaInfo.Add(new ReferenceTableInfo("DimServiceContractType"));
            schemaInfo.Add(new ReferenceTableInfo("DimSource"));
            schemaInfo.Add(new ReferenceTableInfo("DimStateProvince"));
            schemaInfo.Add(new ReferenceTableInfo("DimTime"));
            schemaInfo.Add(new ReferenceTableInfo("DimTimePeriod"));
            schemaInfo.Add(new ReferenceTableInfo("DimUOM"));
            schemaInfo.Add(new ReferenceTableInfo("ETClickOpen"));
            schemaInfo.Add(new ReferenceTableInfo("factDailyWeather"));
            schemaInfo.Add(new ReferenceTableInfo("MeasureCrossReference"));
            schemaInfo.Add(new ReferenceTableInfo("proactive_home_energy_report_template"));
            schemaInfo.Add(new ReferenceTableInfo("PromoCodeGasSavings"));
            schemaInfo.Add(new ReferenceTableInfo("RawCsvTemp_all"));
            schemaInfo.Add(new ReferenceTableInfo("ScgEmpSeeds"));
            schemaInfo.Add(new ReferenceTableInfo("ScgEnrollment"));
            schemaInfo.Add(new ReferenceTableInfo("tmpappliance"));
            schemaInfo.Add(new ReferenceTableInfo("TmpBenchmarkProfiles"));
            schemaInfo.Add(new ReferenceTableInfo("VarianceTypes"));
            schemaInfo.Add(new ReferenceTableInfo("WeatherData"));
            schemaInfo.Add(new ReferenceTableInfo("weathertemp"));
            //schemaInfo.Add(new ShardedTableInfo("2014_11_EMAIL", "ReferrerID"));
            //schemaInfo.Add(new ShardedTableInfo("2014_11_PRINT", "ReferrerID"));
            //schemaInfo.Add(new ShardedTableInfo("2014_12_EMAIL", "ReferrerID"));
            //schemaInfo.Add(new ShardedTableInfo("2014_12_PRINT", "ReferrerID"));
            //schemaInfo.Add(new ShardedTableInfo("2015_01_EMAIL", "ReferrerID"));
            //schemaInfo.Add(new ShardedTableInfo("2015_01_PRINT", "ReferrerID"));
            //schemaInfo.Add(new ShardedTableInfo("2015_02_EMAIL", "ReferrerID"));
            //schemaInfo.Add(new ShardedTableInfo("2015_02_PRINT", "ReferrerID"));
            //schemaInfo.Add(new ShardedTableInfo("2015_03_EMAIL", "ReferrerID"));
            //schemaInfo.Add(new ShardedTableInfo("2015_04_EMAIL", "ReferrerID"));
            //schemaInfo.Add(new ShardedTableInfo("2015_05_EMAIL", "ReferrerID"));
            //schemaInfo.Add(new ShardedTableInfo("2015_06_EMAIL", "ReferrerID"));
            schemaInfo.Add(new ShardedTableInfo("ActionItem", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("AmiAggregate", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("BenchmarkBills", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("BenchmarkErrors", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("BenchmarkGroupClientType", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("BenchmarkGroups", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("BenchmarkGroupsHistory", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("Benchmarks", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("BenchmarksHistory", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("Billing", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("Client", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("Customer", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("DimClient", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("DimCustomer", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("DimMeter", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("DimPremise", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("DimRateClass", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("DimServiceContract", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("DimServicePoint", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("DisAggReCalcQueue", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("EmailExclusions", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("ESOMeasureSavings", "clientid"));
            schemaInfo.Add(new ShardedTableInfo("ETBounceOptOut", "ClientID"));
        //    schemaInfo.Add(new ShardedTableInfo("Events", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("Events", "referrerid"));
            schemaInfo.Add(new ShardedTableInfo("factAction", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("factActionData", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("FactActionItem", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("FactAmi", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("FactAmiDetail", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("FactBillDisagg", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("FactBilling", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("factClientAttribute", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("FactCustomerPremise", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("FactEnergyObject", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("FactEvent", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("factEventAdditionalInfo", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("factEventDetails", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("FactPremiseAttribute", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("FactServicePoint", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("FactServicePointBilling", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("HE_Report_actionitems", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("HE_Report_actionitems_ranked", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("HE_Report_CSV_Measure_history", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("HE_Report_CSV_Promo_History", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("HE_Report_FactActionItem", "ClientId"));
       //     schemaInfo.Add(new ShardedTableInfo("HE_Report_History", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("HE_Report_History", "ReferrerID"));
            schemaInfo.Add(new ShardedTableInfo("holdoldBenchmarks", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("home_energy_report_client", "clientID"));
            schemaInfo.Add(new ShardedTableInfo("home_energy_report_criteria_profile", "clientID"));
            schemaInfo.Add(new ShardedTableInfo("home_energy_report_criteria_profile_hold", "clientID"));
            schemaInfo.Add(new ShardedTableInfo("home_energy_report_promo_codes", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("home_energy_report_promo_prereqs", "ClientId"));
         //   schemaInfo.Add(new ShardedTableInfo("home_energy_report_staging", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("home_energy_report_staging", "ReferrerID"));
          //  schemaInfo.Add(new ShardedTableInfo("home_energy_report_staging_allcolumns", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("home_energy_report_staging_allcolumns", "ReferrerID"));
            schemaInfo.Add(new ShardedTableInfo("HomeEnergyHistoryReport", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("INS_DimCustomer", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("INS_DimPremise", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("INS_FactAction", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("INS_FactBilling", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("INS_FactPremiseAttribute", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("KEY_DimCustomer", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("KEY_DimPremise", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("KEY_FactAction", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("KEY_FactBilling", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("KEY_FactPremiseAttribute", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("PostalCodeChanges", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("Premise", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("PremiseAttributes", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("PremiseAttributes_INS", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("PremiseAttributes_UPD", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("PremiseAttributesTEST", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("premiseesoidxref", "clientid"));
            schemaInfo.Add(new ShardedTableInfo("PremiseSavings", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("profile", "clientID"));
            schemaInfo.Add(new ShardedTableInfo("RawActionTemp", "clientid"));
            schemaInfo.Add(new ShardedTableInfo("RawAmiTemp", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("RawBillTemp_224", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("RawCsvTemp", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("RawProfileTemp", "clientid"));
            schemaInfo.Add(new ShardedTableInfo("ScgCallCenterOptOuts", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("ScgDailyTotals", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("ScgEmailOptOuts", "clientid"));
            schemaInfo.Add(new ShardedTableInfo("ScgOptOuts", "clientid"));
            schemaInfo.Add(new ShardedTableInfo("T1U_DimCustomer", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("T1U_DimPremise", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("T1U_FactBilling", "ClientID"));
            schemaInfo.Add(new ShardedTableInfo("tempqueue", "ClientId"));
            schemaInfo.Add(new ShardedTableInfo("tmpEvents", "referrerid"));
            schemaInfo.Add(new ShardedTableInfo("tmpEventsBad", "referrerid"));
            schemaInfo.Add(new ShardedTableInfo("tmpHoldingEvents", "ClientID"));

            // Register it with the shard map manager for the given shard map name
            InsightsDW_shardMapManager.GetSchemaInfoCollection().Add(shardMapName, schemaInfo);
            Console.WriteLine("Schema creation complete");
        }

        /// <summary>
        /// Reads the user's choice of a split point, and creates a new shard with a mapping for the resulting range.
        /// </summary>
        private static void AddShard()
        {
            RangeShardMap<int> shardMap = TryGetShardMap();
            if (shardMap != null)
            {
                string shardMapManagerConnectionString =
               InsightsDWConfiguration.GetConnectionString(
                   InsightsDWConfiguration.ShardMapManagerQAServerName,
                   InsightsDWConfiguration.ShardMapManagerDatabaseName, "QA");

                ShardMapManagerFactory.CreateSqlShardMapManager(shardMapManagerConnectionString);
                Console.WriteLine("Created SqlShardMapManager - in the root InsightsDW database");

                InsightsDW_shardMapManager = ShardMapManagerFactory.GetSqlShardMapManager(
                    shardMapManagerConnectionString,
                    ShardMapManagerLoadPolicy.Lazy);

                //==========================================================================
                //
                // The connectionString contains server name, database name, and admin credentials 
                // for privileges on both the GSM and the shards themselves.

                //========================================
                //// Here we assume that the ranges start at 0, are contiguous, 
                //// and are bounded (i.e. there is no range where HighIsMax == true)
                //int currentMaxHighKey = shardMap.GetMappings().Max(m => m.Value.High);
                //int defaultNewHighKey = currentMaxHighKey + 100;

                //Console.WriteLine("A new range with low key {0} will be mapped to the new shard.", currentMaxHighKey);
                //int newHighKey = ConsoleUtils.ReadIntegerInput(
                //    string.Format("Enter the high key for the new range [default {0}]: ", defaultNewHighKey),
                //    defaultNewHighKey, 
                //    input => input > currentMaxHighKey);

                //Range<int> range = new Range<int>(currentMaxHighKey, newHighKey);

                //Console.WriteLine();
                //Console.WriteLine("Creating shard for range {0}", range);
                //CreateShardSample.CreateShard(shardMap, range);
            }
        }

        /// <summary>
        /// Executes the Data-Dependent Routing sample.
        /// </summary>
        private static void DataDepdendentRouting()
        {
            RangeShardMap<int> shardMap = TryGetShardMap();
            if (shardMap != null)
            {
                DataDependentRoutingSample.ExecuteDataDependentRoutingQuery(
                    shardMap,
                    InsightsDWConfiguration.GetCredentialsConnectionString("QA"));
            }
        }

        /// <summary>
        /// Executes the Multi-Shard Query sample.
        /// </summary>
        private static void MultiShardQuery()
        {
            RangeShardMap<int> shardMap = TryGetShardMap();
            if (shardMap != null)
            {
                MultiShardQuerySample.ExecuteMultiShardQuery(
                    shardMap,
                    InsightsDWConfiguration.GetCredentialsConnectionString("QA"));
            }
        }

        /// <summary>
        /// Drops all shards and the shard map manager database (if it exists).
        /// </summary>
        private static void DropAll()
        {
            RangeShardMap<int> shardMap = TryGetShardMap();
            if (shardMap != null)
            {
                // Drop shards
                foreach (Shard shard in shardMap.GetShards())
                {
                    SqlDatabaseUtils.DropDatabase(shard.Location.DataSource, shard.Location.Database);
                }
            }

            if (SqlDatabaseUtils.DatabaseExists(InsightsDWConfiguration.ShardMapManagerQAServerName, InsightsDWConfiguration.ShardMapManagerDatabaseName))
            {
                // Drop shard map manager database
                SqlDatabaseUtils.DropDatabase(InsightsDWConfiguration.ShardMapManagerQAServerName, InsightsDWConfiguration.ShardMapManagerDatabaseName);
            }

            // Since we just dropped the shard map manager database, this shardMapManager reference is now non-functional.
            // So set it to null so that the program knows that the shard map manager is gone.
            InsightsDW_shardMapManager = null;
        }
        /// <summary>
        /// Drops all shards and the shard map manager database (if it exists).
        /// </summary>
        private static void DeleteInsightDWShards()
        {
            ListShardMap<long> sm = null;
 
            string mapName = "InsightsDW_map";

            // Create shard map manager
            if (InsightsDW_shardMapManager.TryGetListShardMap(mapName, out sm))
            {
                // Create shard map
                Shard shard0 = null, shard1 = null;

                sm.DeleteMapping(sm.MarkMappingOffline(sm.GetMappingForKey(101)));
                sm.DeleteMapping(sm.MarkMappingOffline(sm.GetMappingForKey(87)));
                sm.DeleteMapping(sm.MarkMappingOffline(sm.GetMappingForKey(256)));

                if (sm.TryGetShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsDW_101"), out shard0))
                {

                    sm.DeleteShard(shard0);
                    Console.WriteLine("InsightsDW_101 shard deleted");
                }

                if (sm.TryGetShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsDW_Test"), out shard1))
                {
                    sm.DeleteShard(shard1);
                    Console.WriteLine("InsightsDW_Test shard deleted");
                }
            }

        }
        private static void DeleteInsightMetaDataShard()
        {
            ListShardMap<long> sm = null;

            string mapName = "InsightsMetaData_map";

            // Create shard map manager
            if (InsightsDW_shardMapManager.TryGetListShardMap(mapName, out sm))
            {
                // Create shard map
                Shard shard0 = null;


                if (sm.TryGetShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsMetaData"), out shard0))
                {

                    sm.DeleteShard(shard0);
                    Console.WriteLine("InsightsMetaData shard deleted");
                }

            }

        }
        private static void DeleteInsightsDWShardMap()
        {
            string mapName = "InsightsDW_map";
            ListShardMap<long> sm = null;
            if (InsightsDW_shardMapManager.TryGetListShardMap(mapName, out sm))
            {
                InsightsDW_shardMapManager.DeleteShardMap(sm);
            }

        }
        private static void DeleteInsightsMetaDataShardMap()
        {
            string mapName = "InsightsMetaData_map";
            
            ListShardMap<long> sm = null;
            if (InsightsDW_shardMapManager.TryGetListShardMap(mapName, out sm))
            {
                InsightsDW_shardMapManager.DeleteShardMap(sm);
            }

        }

        private static void DeleteShard(int clientId)
        {
            ListShardMap<long> sm = null;

            string mapName = "InsightsDW_map";

            // Create shard map manager
            if (InsightsDW_shardMapManager.TryGetListShardMap(mapName, out sm))
            {
                // Create shard map
                Shard shard0 = null;

                var mapp = sm.GetMappingForKey(clientId);
                var off = sm.MarkMappingOffline(mapp);
                sm.DeleteMapping(off);

                if (sm.TryGetShard(new ShardLocation(InsightsDWConfiguration.ShardMapManagerQAServerName, "InsightsDW_" + clientId), out shard0))
                {

                    sm.DeleteShard(shard0);
                    Console.WriteLine("InsightsDW_" + clientId + " shard deleted");
                }
            }
        }
        #endregion

        #region Shard map helper methods

        /// <summary>
        /// Gets the shard map, if it exists. If it doesn't exist, writes out the reason and returns null.
        /// </summary>
        private static RangeShardMap<int> TryGetShardMap()
        {
            if (InsightsDW_shardMapManager == null)
            {
                ConsoleUtils.WriteWarning("Shard Map Manager has not yet been created");
                return null;
            }

            RangeShardMap<int> shardMap;
            bool mapExists = InsightsDW_shardMapManager.TryGetRangeShardMap(InsightsDWConfiguration.InsightsDWShardMapName, out shardMap);

            if (!mapExists)
            {
                ConsoleUtils.WriteWarning("Shard Map Manager has been created, but the Shard Map has not been created");
                return null;
            }

            return shardMap;
        }

        #endregion
    }
}
