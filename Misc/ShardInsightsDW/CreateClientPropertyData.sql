ALTER TABLE [dbo].[ClientPropertyData] DROP CONSTRAINT [DF_ClientPropertyData_CreateDate]
GO

/****** Object:  Table [dbo].[ClientPropertyData]    Script Date: 12/15/2017 10:31:08 AM ******/
DROP TABLE [dbo].[ClientPropertyData]
GO

/****** Object:  Table [dbo].[ClientPropertyData]    Script Date: 12/15/2017 10:31:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ClientPropertyData](
	[ClientID] [int] NOT NULL,
	[CustomerID] [varchar](50) NOT NULL,
	[AccountID] [varchar](50) NOT NULL,
	[PremiseID] [varchar](50) NOT NULL,
	[Address] [varchar](256) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[Zip] [varchar](15) NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[centralac.style] [varchar](256) NULL,
	[heatsystem.fuel] [varchar](256) NULL,
	[heatsystem.style] [varchar](256) NULL,
	[house.levels] [varchar](256) NULL,
	[house.people] [varchar](256) NULL,
	[house.rented] [varchar](256) NULL,
	[house.roofarea] [varchar](256) NULL,
	[house.rooms] [varchar](256) NULL,
	[house.style] [varchar](256) NULL,
	[house.totalarea] [varchar](256) NULL,
	[house.yearbuilt] [varchar](256) NULL,
	[outsidewater.gardenarea] [varchar](256) NULL,
	[outsidewater.lawnarea] [varchar](256) NULL,
	[pool.count] [varchar](256) NULL,
	[pool.poolheater] [varchar](256) NULL,
	[ResultCode] [varchar](256) NULL,
	[CreateDate] [datetime] NOT NULL,
	[ResultData] [nvarchar](max) NULL,
	[waterheater.fuel] [varchar](256) NULL,
	[house.totalarearange] [varchar](256) NULL,
	[house.yearbuiltrange] [varchar](256) NULL,
 CONSTRAINT [PK_ClientPropertyData] PRIMARY KEY CLUSTERED 
(
	[ClientID] ASC,
	[CustomerID] ASC,
	[AccountID] ASC,
	[PremiseID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[ClientPropertyData] ADD  CONSTRAINT [DF_ClientPropertyData_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO


