WITH    ExternTables
          AS ( SELECT   e.object_id ,
                        '[' + s.name + '].[' + e.name + ']' AS TableName
               FROM     sys.external_tables e
                        INNER JOIN sys.schemas s ON s.schema_id = e.schema_id
             )
    SELECT  'IF OBJECT_ID(''' + e.TableName
            + ''', ''U'') IS NOT NULL DROP TABLE ' + e.TableName + ';'
            + ' CREATE EXTERNAL TABLE ' + e.TableName + ' ('
            + ( SELECT  STUFF(( SELECT  ',[' + c.name + '] ' + t.name
                                        + CASE WHEN t.name = 'decimal'
                                               THEN '('
                                                    + CONVERT(VARCHAR(5), c.precision)
                                                    + ','
                                                    + CONVERT(VARCHAR(5), c.scale)
                                                    + ')'
                                               WHEN t.name = 'nvarchar'
                                                    OR t.name = 'nchar'
                                               THEN '('
                                                    + IIF(c.max_length = -1, 'max', CONVERT(VARCHAR(5), c.max_length
                                                    / 2)) + ')'
                                               WHEN t.name = 'varchar'
                                                    OR t.name = 'char'
                                                    OR t.name = 'varbinary'
                                               THEN '('
                                                    + IIF(c.max_length = -1, 'max', CONVERT(VARCHAR(5), c.max_length))
                                                    + ')'
                                               ELSE ''
                                          END + ' '
                                        + IIF(c.is_nullable = 1, 'NULL', 'NOT NULL')
                                FROM    sys.columns c
                                        INNER JOIN sys.types t ON t.system_type_id = c.system_type_id
                                                              AND t.user_type_id = c.user_type_id
                                WHERE   object_id = e.object_id
                                ORDER BY c.column_id
                              FOR
                                XML PATH('')
                              ), 1, 1, '')
              )
            + ')WITH(DATA_SOURCE = InsightsMetaDataElasticDBQueryDataSrc,DISTRIBUTION=REPLICATED);'
    FROM    ExternTables e;