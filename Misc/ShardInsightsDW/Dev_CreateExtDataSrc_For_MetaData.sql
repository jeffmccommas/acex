CREATE MASTER KEY ENCRYPTION BY PASSWORD= 'Acl@r@282'; 
GO

CREATE DATABASE SCOPED CREDENTIAL ElasticDBQueryCred
WITH IDENTITY = 'AclaraCEAdmin',
SECRET = 'Acl@r@282';
GO


CREATE EXTERNAL DATA SOURCE InsightsMetaDataElasticDBQueryDataSrc WITH
  (TYPE = SHARD_MAP_MANAGER,
  LOCATION = 'aceDsql1c0.database.windows.net',
  DATABASE_NAME = 'InsightsDW',
  CREDENTIAL = ElasticDBQueryCred,
  SHARD_MAP_NAME = 'InsightsMetaData_map'
) ;
