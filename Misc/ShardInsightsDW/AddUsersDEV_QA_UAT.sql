﻿-- Execute this script in the required shard

-- AclaraCEDEV
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'AclaraCEDEV')
BEGIN
    CREATE USER AclaraCEDEV FOR LOGIN AclaraCEDEV

    EXEC sp_addrolemember 'db_datareader', 'AclaraCEDEV'
END
GO

-- Aclweb
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'Aclweb')
BEGIN
    CREATE USER Aclweb FOR LOGIN Aclweb

    EXEC sp_addrolemember 'db_owner', 'Aclweb'
END
GO

-- Aclwebjob
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'Aclwebjob')
BEGIN
    CREATE USER Aclwebjob FOR LOGIN Aclwebjob

    EXEC sp_addrolemember 'db_owner', 'Aclwebjob'
END
GO

-- AMLUser
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'AMLUser')
BEGIN
    CREATE USER AMLUser FOR LOGIN AML

    EXEC sp_addrolemember 'db_datareader', 'AMLUser'
    EXEC sp_addrolemember 'db_datawriter', 'AMLUser'
    EXEC sp_addrolemember 'db_owner', 'AMLUser'
END
GO

-- BatchProcessUser
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'BatchProcessUser')
BEGIN
    CREATE USER BatchProcessUser FOR LOGIN BatchProcess

    EXEC sp_addrolemember 'db_owner', 'BatchProcessUser'
END
GO

-- BusinessUser
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'BusinessUser')
BEGIN
    CREATE USER BusinessUser FOR LOGIN Business

    EXEC sp_addrolemember 'db_datareader', 'BusinessUser'
END
GO

-- DataScientistUser
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'DataScientistUser')
BEGIN
    CREATE USER DataScientistUser FOR LOGIN DataScientist

    EXEC sp_addrolemember 'db_datareader', 'DataScientistUser'
END
GO

-- DBAdminUser
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'DBAdminUser')
BEGIN
    CREATE USER DBAdminUser FOR LOGIN DBAdmin

    EXEC sp_addrolemember 'db_owner', 'DBAdminUser'
END
GO

-- DRFailoverUser
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'DRFailoverUser')
BEGIN
    CREATE USER DRFailoverUser FOR LOGIN DRFailover

    EXEC sp_addrolemember 'db_owner', 'DRFailoverUser'
END
GO

-- EngineeringUser
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'EngineeringUser')
BEGIN
    CREATE USER EngineeringUser FOR LOGIN Engineering

    EXEC sp_addrolemember 'db_owner', 'EngineeringUser'
    EXEC sp_addrolemember 'db_datareader', 'EngineeringUser'
    EXEC sp_addrolemember 'db_datawriter', 'EngineeringUser'
END
GO

-- ReportingApplicationUser
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'ReportingApplicationUser')
BEGIN
    CREATE USER ReportingApplicationUser FOR LOGIN ReportingApplication

    EXEC sp_addrolemember 'db_datareader', 'ReportingApplicationUser'
END
GO

-- ReportingUser
IF NOT EXISTS
    (SELECT name
     FROM sys.database_principals
     WHERE name = 'ReportingUser')
BEGIN
    CREATE USER ReportingUser FOR LOGIN Reporting

    EXEC sp_addrolemember 'db_owner', 'ReportingUser'
END
GO

