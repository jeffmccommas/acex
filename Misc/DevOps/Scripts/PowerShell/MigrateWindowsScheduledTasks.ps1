Param(
    [string]$sourceTaskNamePattern,
	[string]$sourceFolderName,
	[string]$targetFolderName
)

Function Migrate-WindowsScheduledTasks
{

    param($SourceTaskNamePattern, $SourceFolderName, $TargetFolderName)
	
	$MigrateWSTTempFolder = "$env:TEMP\Migrate.WST.Temp"

    try
    {

        Write-Host "Begin function: Migrate-WindowsScheduledTasks (Source task name pattern: $($SourceTaskNamePattern), Source folder name: $($SourceFolderName), Target folder name: $($TargetFolderName))"

		If(!(test-path $MigrateWSTTempFolder))
		{
            Write-Host "Create temp folder $($MigrateWSTTempFolder)."
			New-Item -ItemType Directory -Force -Path $MigrateWSTTempFolder
		}

		#Export tasks matching source task name patter and in source folder.
		Write-Host "Export Windows scheduled tasks into temp folder: $($MigrateWSTTempFolder)."
		Get-ScheduledTask -TaskPath $SourceFolderName | Where-Object {$_.TaskName -like ($SourceTaskNamePattern)} | foreach {
			Export-ScheduledTask -TaskName $_.TaskName -TaskPath $_.TaskPath |
 			Out-File (Join-Path "$MigrateWSTTempFolder" "$($_.TaskName).xml")
		}

		#Fix application folder name.
		Write-Host "Fix application folder name in exported (xml) Windows scheduled tasks into target folder: $($TargetFolderName)."
     	Get-ScheduledTask -TaskPath $SourceFolderName | Where-Object {$_.TaskName -like ($SourceTaskNamePattern)} | foreach {
			 (Get-Content (Join-Path "$MigrateWSTTempFolder" "$($_.TaskName).xml")).replace('InsightsDW.ImportData', 'InsightsDW.ImportDataConsoleApp') | Set-Content (Join-Path "$MigrateWSTTempFolder" "$($_.TaskName).xml")
		}

		$SchedTaskCred = Get-Credential -Message "Enter the Domain credentials of the Scheduled Task Service Account"
		$SchedTaskCredUser = $SchedTaskCred.UserName
		$SchedTaskCredPwd = $SchedTaskCred.GetNetworkCredential().Password

		#Import tasks matching source task name patter and in source folder.
		Write-Host "Import Windows scheduled tasks into target folder: $($TargetFolderName)."
     	Get-ScheduledTask -TaskPath $SourceFolderName | Where-Object {$_.TaskName -like ($SourceTaskNamePattern)} | foreach {
		     Register-ScheduledTask -Xml (Get-Content (Join-Path "$MigrateWSTTempFolder" "$($_.TaskName).xml") | Out-String) -TaskName $_.TaskName -TaskPath $TargetFolderName -User $SchedTaskCredUser -Password $SchedTaskCredPwd  �Force
		}

		#Disable imported tasks.
		Write-Host "Disable imported Windows scheduled"
		Get-ScheduledTask -TaskPath $SourceFolderName | Where-Object {$_.TaskName -like ($SourceTaskNamePattern)} | foreach {
		     Disable-ScheduledTask -TaskName $_.TaskName -TaskPath $TargetFolderName
		}
        Write-Host "Completed function: Migrate-WindowsScheduledTasks (Source task name pattern: $SourceTaskNamePattern, Source folder name: $($SourceFolderName), Target folder name: $($TargetFolderName))"

		exit 0
    }
        catch 
    {
		Write-Host �Caught an exception:� -ForegroundColor Red
		Write-Host �Exception Type: $($_.Exception.GetType().FullName)� -ForegroundColor Red
		Write-Host �Exception Message: $($_.Exception.Message)� -ForegroundColor Red
		Write-Host "Error: Migrate-WindowsScheduledTasks failed. (Source task name pattern: $SourceTaskNamePattern, Source folder name: $($SourceFolderName), Target folder name: $($TargetFolderName))"
		exit 1
    }
}

try
{
    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Begin script: Migrate-WindowsScheduledTasks" 
    Write-Host "-----------------------------------------------------------------------" 

    Write-Host "Function: Migrate-WindowsScheduledTasks (Source task name pattern: $sourceTaskNamePattern, Source folder name: $($sourceFolderName), Target folder name: $($targetFolderName))"

    Migrate-WindowsScheduledTasks $sourceTaskNamePattern $sourceFolderName $targetFolderName

    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Completed script: Migrate-WindowsScheduledTasks" 
    Write-Host "-----------------------------------------------------------------------" 

}
catch 
{
    Write-Host $_
    exit 1
}