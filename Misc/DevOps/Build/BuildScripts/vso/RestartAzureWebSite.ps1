Param(
    [string]$name,
    [string]$slot
)

Function Restart-ACLAzureWebSite
{

    param($Name, $Slot)

    try
    {

        Write-Host "Restart-AzureWebsite (Name: $($Name))"
        Restart-AzureWebsite -Name $Name

        Write-Host "Pause script."

        # Pause script for 5 minute(s).
        Start-Sleep -s 300
    }
        catch 
    {
        Write-Host "Error: Restart-AzureWebsite failed. (Name: $($Name))."
    }
}

try
{
    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Starting script: RestartAzureWebSite" 
    Write-Host "-----------------------------------------------------------------------" 

    Write-Host "Function: Restart-ACLAzureWebSite (Azure Website name: $($name), Slot: $($slot))" 

    Stop-ACLAzureWebSite $name $slot

    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Finishing script: RestartAzureWebSite" 
    Write-Host "-----------------------------------------------------------------------" 

}
catch 
{
    Write-Host $_
    exit 1
}
