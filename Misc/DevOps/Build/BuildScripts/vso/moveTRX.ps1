﻿#Note: This script will modify the name of the first file returned by Get-ChildItem

$filePath = "$Env:BUILD_SOURCESDIRECTORY" + "\TestResults"

#$trxFile = Get-ChildItem $filePath/*.trx | Select -Exp Name -First 1
#Rename-Item $filePath/$trxFile $filePath/TestResults.trx

cp -recurse -force $filePath\* $Env:AGENT_BUILDDIRECTORY\TestResults\