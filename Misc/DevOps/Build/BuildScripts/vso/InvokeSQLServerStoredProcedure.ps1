Param(
    [string]$connectionString,
	[string]$storedProcedureName
)

Function Invoke-SQLServerStoredProcedure
{

    param($ConnectionString, $StoredProcedureName)

    try
    {

        Write-Host "Begin function: Invoke-SQLServerStoredProcedure (Connection string: ********, Stored procedure name: $($StoredProcedureName))"

		$SqlConnection = New-Object System.Data.SqlClient.SqlConnection
		$SqlCmd = New-Object System.Data.SqlClient.SqlCommand

		$SqlConnection.ConnectionString = $ConnectionString

		$SqlCmd.CommandText = $StoredProcedureName
		$SqlCmd.Connection = $SqlConnection
		$SqlCmd.CommandType = [System.Data.CommandType]::StoredProcedure 

		$SqlConnection.Open()
		
		$result = $SqlCmd.ExecuteNonQuery()
 
		$SqlConnection.Close()		

        Write-Host "Completed function: Invoke-SQLServerStoredProcedure (Connection string: ********, Stored procedure name: $($StoredProcedureName))"

		exit 0
    }
        catch 
    {
		Write-Host �Caught an exception:� -ForegroundColor Red
		Write-Host �Exception Type: $($_.Exception.GetType().FullName)� -ForegroundColor Red
		Write-Host �Exception Message: $($_.Exception.Message)� -ForegroundColor Red
		Write-Host "Error: Invoke-SQLServerStoredProcedure failed. (Connection string: ********, Stored procedure name: $($StoredProcedureName)))"
		exit 1
    }
}

try
{
    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Begin script: Invoke-SQLServerStoredProcedure" 
    Write-Host "-----------------------------------------------------------------------" 

    Write-Host "Function: Invoke-SQLServerStoredProcedure (Connection string: ********, Stored procedure name: $($storedProcedureName))" 

    Invoke-SQLServerStoredProcedure $connectionString $storedProcedureName

    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Completed script: Invoke-SQLServerStoredProcedure" 
    Write-Host "-----------------------------------------------------------------------" 

}
catch 
{
    Write-Host $_
    exit 1
}