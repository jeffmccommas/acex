# Enable -Verbose option
[CmdletBinding()]
	
# A script to do run a batch file. The batch file will convert our raw scss files into CSS
#
# Original script that runs as pre-build event
#
#cd "$(ProjectDir)"
#set path=c:\\Ruby23-x64\bin
#set style=compressed
#if $(ConfigurationName)==Debug set style=expanded
#scss.bat --update ./content/scss:./content --style %style%


param([string]$scssFilePath)

#Comparison of expected build directories depending on the build
#D:\Builds\52\iiDEAS\TWACS_C3PO-Gated\src\      Dev\C3PO\Common\CommonUI 
#$scssLocation = "$fullSrcPath\                 Common\CommonUI"

$fullSrcPath = $Env:TF_BUILD_SOURCESDIRECTORY

Write-Verbose 'Build directory: $Env:TF_BUILD_SOURCESDIRECTORY'

#$fullBinariesPath = $Env:TF_BUILD_BINARIESDIRECTORY
#$scssLocation = "C:\workspaces\iiDEAS\Dev\C3PO\Common\CommonUI"
$scssLocation = "$fullSrcPath\$scssFilePath"
$scssBatLocation = "C:\Ruby23-x64\bin\scss.bat"
$style = 'compressed'


#For now, we will skip the optional parameter to produce expanded CSS for debug purposes

#if $(ConfigurationName)==Debug set style=expanded
#if ($PSBoundParameters.ContainsKey('Debug'))
#{
	#$style=expanded
#}


#Run the batch file
Start-Process -FilePath $scssBatLocation -ArgumentList '--update ./styles/SCSS:./styles --style $style' -WorkingDirectory $scssLocation