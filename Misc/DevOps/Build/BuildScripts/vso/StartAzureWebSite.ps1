Param(
    [string]$name,
    [string]$slot
)

Function Start-ACLAzureWebSite
{

    param($Name, $Slot)

    try
    {

        Write-Host "Start-AzureWebsite (Name: $($Name))"
        Start-AzureWebsite -Name $Name -Slot $Slot
    }
        catch 
    {
        Write-Host "Error: Start-AzureWebsite failed. (Name: $($Name))."
    }
}

try
{
    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Starting script: StartAzureWebSite" 
    Write-Host "-----------------------------------------------------------------------" 

    Write-Host "Function: Start-ACLAzureWebSite (Azure Website name: $($name), Slot: $($slot))" 

    Start-ACLAzureWebSite $name $slot

    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Finishing script: StartAzureWebSite" 
    Write-Host "-----------------------------------------------------------------------" 

}
catch 
{
    Write-Host $_
    exit 1
}
