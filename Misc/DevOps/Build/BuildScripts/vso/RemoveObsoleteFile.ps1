Param(
    [string]$folderPath,
	[double]$fileAgeLimitInDays
)

Function Remove-ObsoleteFile
{

    param($FolderPath, $FileAgeLimitInDays)

    try
    {

        Write-Host "Begin function: Remove-ObsoleteFile (Folder path: $($FolderPath), File age in days: $($FileAgeLimitInDays))"

		$creationDateTimeLimit = (Get-Date).AddDays($FileAgeLimitInDays)

	    Get-ChildItem -Path $FolderPath -Recurse -Force | Where-Object { !$_.PSIsContainer -and $_.CreationTime -lt $creationDateTimeLimit } | Remove-Item -Force -ErrorAction SilentlyContinue

        Write-Host "Completed function: Remove-ObsoleteFile (Folder path: $($FolderPath), File age in days: $($FileAgeLimitInDays))"

		exit 0
    }
        catch 
    {
		Write-Host �Caught an exception:� -ForegroundColor Red
		Write-Host �Exception Type: $($_.Exception.GetType().FullName)� -ForegroundColor Red
		Write-Host �Exception Message: $($_.Exception.Message)� -ForegroundColor Red
		Write-Host "Error: Remove-ObsoleteFile failed. (Connection string: ********, Stored procedure name: $($FileAgeLimitInDays)))"
		exit 1
    }
}

try
{
    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Begin script: Remove-ObsoleteFile" 
    Write-Host "-----------------------------------------------------------------------" 

    Write-Host "Function: Remove-ObsoleteFile (Folder path: $($folderPath), File age in days: $($fileAgeLimitInDays))"

    Remove-ObsoleteFile $folderPath $fileAgeLimitInDays

    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Completed script: Remove-ObsoleteFile" 
    Write-Host "-----------------------------------------------------------------------" 

}
catch 
{
    Write-Host $_
    exit 1
}