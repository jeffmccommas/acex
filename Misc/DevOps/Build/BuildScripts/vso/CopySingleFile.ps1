<# How to call this script from VSTS PowerShell build script.
Example:

Display name: <display_name>
Type: File Path
Script path: $/ACEx/17.12/Misc/DevOps/Build/BuildScripts/vso/CopySingleFile.ps1
Arguments: -sourcePath "$(build.sourcesDirectory)\AppLib\AO.AdvAMI.ConsoleApp\AO.DiagnosticsConsole" -targetPath "$(build.sourcesDirectory)\AppLib\AO.AdvAMI.ConsoleApp\AO.DiagnosticsConsole" -sourceFileName "appSettings_$(BuildConfiguration).config" -targetFileName "appSettings_env.config"

#>


Param(
  [string]$sourcePath,
  [string]$targetPath,
  [string]$sourceFileName,
  [string]$targetFileName
)

Function PerformFileCopy
{

    param($SourcePath, $TargetPath, $SourceFileName, $TargetFileName)

    try
    {

        $sourcePathFileName = join-path -path $SourcePath -childpath $SourceFileName
        $targetPathFileName = join-path -path $TargetPath -childpath $TargetFileName

	    If (!(Test-Path $targetPathFileName))
	    {
		    New-Item -ItemType File -Path $targetPathFileName -Force
	    }

	    Copy-Item -Path $sourcePathFileName -Destination $targetPathFileName -Force

        Write-Host "CopySingleFile (Source file: $($sourcePathFileName), Target file: $($targetPathFileName))" 
    }
    catch {
        Write-Host $_
        exit 1
    }

}

try
{
    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Starting script: CopySingleFile" 
    Write-Host "-----------------------------------------------------------------------" 

    Write-Host "Function: PerformFileCopy (Source path: $($sourcePath), Target path: $($targetPath), Source file name: $($sourceFileName), Target file name: $($targetFileName))" 

    PerformFileCopy $sourcePath $targetPath $sourceFileName $targetFileName

    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Finishing script: CopySingleFile" 
    Write-Host "-----------------------------------------------------------------------" 

}
catch 
{
    Write-Host $_
    exit 1
}
