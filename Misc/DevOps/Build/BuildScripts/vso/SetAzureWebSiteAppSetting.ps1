Param(
    [string]$name,
    [string]$slot,
    [string]$appSettingKey,
    [string]$appSettingValue,
    [string]$appSettingSlotSticky
)

Function Set-ACLAppSettings
{

    param($Name, $Slot, $AppSettingKey, $AppSettingValue, $AppSettingSlotSticky)


    $returnValue = @{}
    $configurationFound = $false

    Write-Host "Get-AzureWebsite (Name: $($Name), Slot: $($Slot))" 

    $azureWebsite = (Get-AzureWebsite -Name $Name -Slot $Slot )
    if($azureWebsite -ne $null)
    {
        $appSettingHashTable = $azureWebsite.AppSettings
        $slotStickyAppSettingNames = $azureWebsite.SlotStickyAppSettingNames
    }

    if( $appSettingHashTable.ContainsKey($appSettingKey))
    {
        Write-Host "Update app setting (key: $($appSettingKey))" 
        $appSettingHashTable.Set_Item($appSettingKey,$AppSettingValue)
    }
    else
    {
        Write-Host "Add app setting (key: $($appSettingKey))" 
		$appSettingHashTable.Add($appSettingKey,$AppSettingValue)
    }

    if($AppSettingSlotSticky -ne $null -and
        $AppSettingSlotSticky -eq "true")
    {
        if($slotStickyAppSettingNames -notcontains $AppSettingKey)
        {
            $slotStickyAppSettingNames += $AppSettingKey
        }
    }

    $parameters = @{ }

    if($appSettingHashTable -ne $null)
    {
        $parameters.Add("AppSettings", $appSettingHashTable)
    }

    if($slotStickyAppSettingNames -ne $null)
    {
        $parameters.Add("SlotStickyAppSettingNames", $slotStickyAppSettingNames)
    }

    if($parameters.count -lt 1)
    {
        return
    }

    $parameters.Add("Name", $Name)
    $parameters.Add("Slot", $Slot)

    try
    {
        $parametersAsText = $parameters | Out-String

        Write-Host "Set-AzureWebsite (Paramters: $($parametersAsText))"
        Set-AzureWebsite @parameters

        Write-Host "Restart-AzureWebsite (Name: $($Name))"
        Restart-AzureWebsite $Name
    }
        catch 
    {
        Write-Host "Error: Set-AzureWebApp failed to update appsettings. (Name: $($Name))."
    }
}

try
{
    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Starting script: SetAzureWebSiteAppSetting" 
    Write-Host "-----------------------------------------------------------------------" 

    Write-Host "Function: Set-ACLAppSettings (Azure Website name: $($name), Slot: $($slot), AppSetting key: $($appSettingKey), AppSetting value: $($appSettingValue))" 

    Set-ACLAppSettings $name $slot $appSettingKey $appSettingValue $appSettingSlotSticky

    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Finishing script: SetAzureWebSiteAppSetting" 
    Write-Host "-----------------------------------------------------------------------" 

}
catch 
{
    Write-Host $_
    exit 1
}
