Param(
    [string]$subscriptionName,
	[string]$projectName
)

Function Invoke-ACLRedgateSQLDataCompare
{

    param($SubscriptionName, $ProjectName)

    try
    {

        Write-Host "Begin function: Invoke-ACLRedgateSQLDataCompare (Subscription name: $($SubscriptionName), Project name: $($ProjectName))"
        #Invoke-Expression TBD

		[string]$exePath = "C:\Program Files (x86)\Red Gate\SQL Data Compare 11\SQLDataCompare.exe"

		#REMOVE BEGIN:
		#[Array]$arguments = "-synchronize", "-Include:Identical", "-Project:$($env:BUILD_SOURCESDIRECTORY)\Misc\Redgate\SQL Data Compare\PaaS_InsightsMetadata_DEV v InsightsMetadata_QA_cmTablesExcludingEM_NO dateUpdated.sdc"
		#REMOVE END:

		[Array]$arguments = "-synchronize", "-Include:Identical", "-Project:$($env:BUILD_SOURCESDIRECTORY)\Misc\Redgate\SQL Data Compare\$($SubscriptionName)\$($ProjectName)"

        Write-Host "Redgate SQL Data Compare: (Executable: $($exePath), Arguments: $($arguments))"

		& $exePath $arguments # 2>&1

		Write-Host "Redgate SQL Data Compare: (Exit code: $LASTEXITCODE)"

		#Exit code: Unknown error.
		if ($LASTEXITCODE -ne 0 -and $LASTEXITCODE -ne 63) {
			Write-Error -Message "##[error]BUILD FAILED: $LASTEXITCODE"
		}

		#Exit code: No differences found.
		if ($LASTEXITCODE -eq 63) {
			Write-Host "Redgate SQL Data Compare found no differences. Exit code: $LASTEXITCODE"
		}

        Write-Host "Completed function: Invoke-ACLRedgateSQLDataCompare (Subscription: $($Subscription), Project name: $($ProjectName))"

		exit 0
    }
        catch 
    {
		Write-Host �Caught an exception:� -ForegroundColor Red
		Write-Host �Exception Type: $($_.Exception.GetType().FullName)� -ForegroundColor Red
		Write-Host �Exception Message: $($_.Exception.Message)� -ForegroundColor Red
		Write-Host "Error: Invoke-ACLRedgateSQLDataCompare failed. (Subscription: $($Subscription), Project name: $($ProjectName))."
		exit 1
    }
}

try
{
    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Begin script: Invoke-RedgateSQLDataCompare" 
    Write-Host "-----------------------------------------------------------------------" 

    Write-Host "Function: Invoke-ACLRedgateSQLDataCompare (Subscription name: $($subscriptionName), Project name: $($projectName))" 

    Invoke-ACLRedgateSQLDataCompare $subscriptionName $projectName

    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Completed script: Invoke-RedgateSQLDataCompare" 
    Write-Host "-----------------------------------------------------------------------" 

}
catch 
{
    Write-Host $_
    exit 1
}