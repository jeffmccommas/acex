Param(
    [string]$name,
    [string]$slot,
	[int]$DelaySeconds=300
)

Function Stop-ACLAzureWebSite
{

    param($Name, $Slot, $DelaySeconds)

    try
    {

        Write-Host "Stop-AzureWebsite (Name: $($Name))"
        Stop-AzureWebsite -Name $Name

        Write-Host "Pause script."

        # Pause script for 1 minute.
        Start-Sleep -s $DelaySeconds
    }
        catch 
    {
        Write-Host "Error: Stop-AzureWebsite failed. (Name: $($Name))."
    }
}

try
{
    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Starting script: StopAzureWebSite" 
    Write-Host "-----------------------------------------------------------------------" 

    Write-Host "Function: Stop-ACLAzureWebSite (Azure Website name: $($name), Slot: $($slot))" 

    Stop-ACLAzureWebSite $name $slot $DelaySeconds

    Write-Host "-----------------------------------------------------------------------" 
    Write-Host "Finishing script: StopAzureWebSite" 
    Write-Host "-----------------------------------------------------------------------" 

}
catch 
{
    Write-Host $_
    exit 1
}
