Param(
    [string]$taskPath
)

Function Enable-ScheduledTaskByTaskPath
{

    param($TaskPath)

    try
    {

        Write-Verbose "Begin function: Enable-ScheduledTaskByTaskPath (Task path string: $($TaskPath))"

        Get-ScheduledTask -TaskPath $TaskPath | Enable-ScheduledTask

        Write-Verbose "Completed function: Enable-ScheduledTaskByTaskPath (Task path string: $($TaskPath))"

		exit 0
    }
        catch 
    {
		Write-Verbose �Caught an exception:� -ForegroundColor Red
		Write-Verbose �Exception Type: $($_.Exception.GetType().FullName)� -ForegroundColor Red
		Write-Verbose �Exception Message: $($_.Exception.Message)� -ForegroundColor Red
		Write-Verbose "Error: Enable-ScheduledTaskByTaskPath failed. (Task path string: $($TaskPath))"
		exit 1
    }
}

try
{
    Write-Verbose "-----------------------------------------------------------------------" 
    Write-Verbose "Begin script: Enable-ScheduledTaskByTaskPath" 
    Write-Verbose "-----------------------------------------------------------------------" 

    Write-Verbose "Function: Enable-ScheduledTaskByTaskPath (Task path string: $($TaskPath))" 

    Enable-ScheduledTaskByTaskPath $taskPath

    Write-Verbose "-----------------------------------------------------------------------" 
    Write-Verbose "Completed script: Enable-ScheduledTaskByTaskPath" 
    Write-Verbose "-----------------------------------------------------------------------" 

}
catch 
{
    Write-Verbose $_
    exit 1
}