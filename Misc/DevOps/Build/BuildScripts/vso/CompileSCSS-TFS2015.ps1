# Enable -Verbose option
[CmdletBinding()]
	
# A script to do run a batch file. The batch file will convert our raw scss files into CSS
#
# Original script that runs as pre-build event
#
#cd "$(ProjectDir)"
#set path=c:\\Ruby23-x64\bin
#set style=compressed
#if $(ConfigurationName)==Debug set style=expanded
#scss.bat --update ./content/scss:./content --style %style%


#Comparison of expected build directories depending on the build
#D:\Builds\52\iiDEAS\TWACS_C3PO-Gated\src\      Dev\C3PO\Common\CommonUI 
#$scssLocation = "$fullSrcPath\                 Common\CommonUI"


$WorkingDir=Get-Location
$scssBatLocation = "C:\Ruby23-x64\bin\sass.bat"
$style = 'compressed'


#For now, we will skip the optional parameter to produce expanded CSS for debug purposes

#if $(ConfigurationName)==Debug set style=expanded
#if ($PSBoundParameters.ContainsKey('Debug'))
#{
	#$style=expanded
#}


Write-Host "Run $scssBatLocation --update $WorkingDir\styles\SCSS:$WorkingDir\styles --style $style"

Write-Host "Working Directory :" $WorkingDir
$PInfo= New-Object System.Diagnostics.ProcessStartInfo 
$PInfo.FileName = $scssBatLocation
$PInfo.UseShellExecute=$false
$PInfo.Arguments="--update ./styles/SCSS:./styles -t $style -f"
$process=New-Object System.Diagnostics.Process
$process.StartInfo=$PInfo
$process.Start()
$process.WaitForExit()
if ($process.ExitCode){
	Write-Host "SCSS Failure"
	throw 1
}

popd