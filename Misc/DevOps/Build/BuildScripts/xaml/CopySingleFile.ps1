Param(
  [string]$sourceRelativePath,
  [string]$targetRelativePath,
  [string]$sourceFileName,
  [string]$targetFileName
)
 
try
{

	$sourcePath = join-path -path $env:TF_BUILD_SOURCESDIRECTORY -childpath $sourceRelativePath
    $targetPath = join-path -path $env:TF_BUILD_SOURCESDIRECTORY -childpath $targetRelativePath

    $sourcePathFileName = join-path -path $sourcePath -childpath $sourceFileName
    $targetPathFileName = join-path -path $targetPath -childpath $targetFileName

	If (!(Test-Path $targetPathFileName))
	{
		New-Item -ItemType File -Path $targetPathFileName -Force
	}

	Copy-Item -Path $sourcePathFileName -Destination $targetPathFileName -Force

    Write-Host "CopySingleFile (Source file: $($sourcePathFileName), Target file: $($targetPathFileName))" 
}
catch {
    Write-Host $_
    exit 1
}