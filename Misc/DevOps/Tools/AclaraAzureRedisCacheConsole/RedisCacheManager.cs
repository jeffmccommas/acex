﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace AclaraAzureRedisCacheConsole
{
    public class RedisCacheManager
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        IContentCacheProvider _contentCacheProvider;

        #endregion

        #region Public Properties
        #endregion

        #region Private Properties

        /// <summary>
        /// Property: Content cache provider.
        /// </summary>
        private IContentCacheProvider ContentCacheProvider
        {
            get { return _contentCacheProvider; }
            set { _contentCacheProvider = value; }
        }

        #endregion

        #region Public Constructors
        public RedisCacheManager(IContentCacheProvider contentCacheProvider)
        {
            _contentCacheProvider = contentCacheProvider;
        }

        #endregion

        #region Private Constructors

        private RedisCacheManager()
        {

        }

        #endregion

        #region Public Methods

        public void ClearCache()
        {
            ContentCacheProvider.FlushAllDatabases();
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }


    /// <summary>
    /// Content cache provider - implemented via Redis Cache Azure.
    /// </summary>
    public class ContentCacheProviderRedisAzure : IContentCacheProvider
    {

        #region Private Constants

        private const string ConfigurationSectionAzureRedisCacheSection = "azureRedisCache";

        #endregion

        #region Private Static Data Members

        /// <summary>
        /// Azure Redis lazy connection multiplexer.
        /// </summary>
        /// <remarks>
        /// Using lazy initialization to ensure only one instance of connection multiplexer is used for all Redis cache operations.
        /// </remarks>
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private static Lazy<ConnectionMultiplexer> _lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        {
            ConnectionMultiplexer connectionMultiplexer = null;
            if (ConfigurationOptions != null)
            {
                connectionMultiplexer = ConnectionMultiplexer.Connect(ConfigurationOptions);
            }
            return connectionMultiplexer;
        });

        /// <summary>
        /// Azure Redis lazy configuration options.
        /// </summary>
        /// <remarks>
        /// Using lazy initialization to ensure only one instance of configuration options is used for all Redis cache operations.
        /// </remarks>
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private static Lazy<ConfigurationOptions> _configurationOptions = new Lazy<ConfigurationOptions>(() =>
        {
            ConfigurationOptions configurationOptions = null;

            var azureRedisCacheSection = (AzureRedisCacheSection)System.Configuration.ConfigurationManager.GetSection(ConfigurationSectionAzureRedisCacheSection);
            if (azureRedisCacheSection != null)
            {
                configurationOptions = ConvertAzureRedisCacheSectionToConfigurationOptions(azureRedisCacheSection);
                configurationOptions.AllowAdmin = true;
            }
            return configurationOptions;
        });

        #endregion

        #region Public Static Properties

        /// <summary>
        /// Property: Redis cache connection.
        /// </summary>
        public static ConnectionMultiplexer Connection
        {
            get { return _lazyConnection.Value; }
        }

        /// <summary>
        /// Property: Redis cache Configuration options.
        /// </summary>
        public static ConfigurationOptions ConfigurationOptions
        {
            get { return _configurationOptions.Value; }
        }

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Determine whether Azure Redis cache is available.
        /// </summary>
        /// <returns>
        /// Verify required Azure Redis configuration options have been provided.
        /// </returns>
        public static bool IsAzureRedisCacheAvailable()
        {
            var result = false;

            //Azure Redis configuration options retrieved.
            if (ConfigurationOptions != null)
            {
                //Required configuration optoins retrieved.
                if ((ConfigurationOptions.EndPoints != null && ConfigurationOptions.EndPoints.Count > 0) &&
                    string.IsNullOrEmpty(ConfigurationOptions.Password) == false)
                {
                    result = true;
                }
            }

            return result;
        }

        #endregion

        #region Private Static Methods

        /// <summary>
        /// Convert Azure Redis cache configuration section to connection multiplexer configuration options.
        /// </summary>
        /// <param name="azureRedisCacheSection"></param>
        /// <returns></returns>
        private static ConfigurationOptions ConvertAzureRedisCacheSectionToConfigurationOptions(AzureRedisCacheSection azureRedisCacheSection)
        {
            var configurationOptions = new ConfigurationOptions();

            //Required: values.

            if (string.IsNullOrEmpty(azureRedisCacheSection.ConfigurationOptions.EndPoint) == false)
            {
                configurationOptions.EndPoints.Add(azureRedisCacheSection.ConfigurationOptions.EndPoint);

            }

            if (string.IsNullOrEmpty(azureRedisCacheSection.ConfigurationOptions.Password) == false)
            {
                configurationOptions.Password = azureRedisCacheSection.ConfigurationOptions.Password;
            }

            //Optional: values:

            if (string.IsNullOrEmpty(azureRedisCacheSection.ConfigurationOptions.Ssl) == false)
            {
                configurationOptions.Ssl = azureRedisCacheSection.ConfigurationOptions.Ssl.ToLower() == "true".ToLower();
            }
            if (string.IsNullOrEmpty(azureRedisCacheSection.ConfigurationOptions.ConnectRetry) == false)
            {
                int connectRetry;
                if (int.TryParse(azureRedisCacheSection.ConfigurationOptions.ConnectRetry, out connectRetry) == false)
                {
                    configurationOptions.ConnectRetry = connectRetry;
                }
            }
            if (string.IsNullOrEmpty(azureRedisCacheSection.ConfigurationOptions.ConnectTimeout) == false)
            {
                int connectTimeout;
                if (int.TryParse(azureRedisCacheSection.ConfigurationOptions.ConnectTimeout, out connectTimeout) == false)
                {
                    configurationOptions.ConnectTimeout = connectTimeout;
                }
            }
            if (string.IsNullOrEmpty(azureRedisCacheSection.ConfigurationOptions.SyncTimeout) == false)
            {
                int syncTimeout;
                if (int.TryParse(azureRedisCacheSection.ConfigurationOptions.SyncTimeout, out syncTimeout))
                {
                    configurationOptions.SyncTimeout = syncTimeout;
                }
            }

            //Silently retry in background rather than throw exception, if connection is unsuccessfull.
            configurationOptions.AbortOnConnectFail = false;

            return configurationOptions;

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Store value in cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="absoluteExpiration"></param>
        /// <param name="regionName"></param>
        public void Set(string key, object value, DateTimeOffset absoluteExpiration, string regionName = null)
        {
            try
            {
                IDatabase cache = Connection.GetDatabase();
                cache.Set(key, value);
                cache.KeyExpire(key, absoluteExpiration.DateTime.ToUniversalTime());
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Listeners[0].WriteLine(string.Format("DEBUG: Exception received from REDIS: {0}", ex.Message));

                throw;
            }

        }

        /// <summary>
        /// Retrieve value from cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="regionName"></param>
        /// <returns></returns>
        public object Get(string key, string regionName = null)
        {
            object result;

            try
            {
                var cache = Connection.GetDatabase();
                result = cache.Get(key);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Listeners[0].WriteLine(string.Format("DEBUG: Exception received from REDIS: {0}", ex.Message));

                throw;
            }

            return result;
        }

        /// <summary>
        /// Remove value from cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="regionName"></param>
        /// <returns></returns>
        public object Remove(string key, string regionName = null)
        {
            object result;

            try
            {
                IDatabase cache = Connection.GetDatabase();
                result = cache.KeyDelete(key);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Listeners[0].WriteLine(string.Format("DEBUG: Exception received from REDIS: {0}", ex.Message));

                throw;
            }

            return result;
        }

        public void FlushAllDatabases()
        {
            EndPoint[] endPoints = Connection.GetEndPoints(true);

            IServer server = Connection.GetServer(endPoints.First());

            server.FlushAllDatabases(CommandFlags.None);
        }

        #endregion

    }

    /// <summary>
    /// StackExchange Redis Cache Extensions.
    /// </summary>
    /// <remarks>
    /// Add support for .NET object serialization/deserialization
    /// For more information, see http://aka.ms/SampleStackExchangeRedisExtensions  
    /// </remarks>
    public static class StackExchangeRedisExtensions
    {

        #region Public Static Methods

        /// <summary>
        /// Retrieve T from cache.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cache"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<T>(this IDatabase cache, string key)
        {
            return Deserialize<T>(cache.StringGet(key));
        }

        /// <summary>
        /// Retrieve object from cache.
        /// </summary>
        /// <param name="cache"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object Get(this IDatabase cache, string key)
        {
            return Deserialize<object>(cache.StringGet(key));
        }

        /// <summary>
        /// Store object in cache.
        /// </summary>
        /// <param name="cache"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void Set(this IDatabase cache, string key, object value)
        {
            cache.StringSet(key, Serialize(value));
        }

        /// <summary>
        /// Append object in cache.
        /// </summary>
        /// <param name="cache"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetAdd(this IDatabase cache, string key, object value)
        {
            cache.SetAdd(key, Serialize(value));
        }

        #endregion

        #region Private Static Methods

        /// <summary>
        /// Serialize object to byte array.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static byte[] Serialize(object value)
        {
            if (value == null)
            {
                return null;
            }

            var binaryFormatter = new BinaryFormatter();
            using (var memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, value);
                var objectDataAsStream = memoryStream.ToArray();
                return objectDataAsStream;
            }
        }

        /// <summary>
        /// Deserialize T from byte array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        private static T Deserialize<T>(byte[] stream)
        {
            if (stream == null)
            {
                return default(T);
            }

            var binaryFormatter = new BinaryFormatter();
            using (var memoryStream = new MemoryStream(stream))
            {
                var result = (T)binaryFormatter.Deserialize(memoryStream);
                return result;
            }
        }

        #endregion

    }

}
