﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace AclaraAzureRedisCacheConsole
{
    public static class RedisCacheManagerFactory
    {

        public static RedisCacheManager CreateRedisCacheManager()
        {
            RedisCacheManager result = null;
            IContentCacheProvider constentCacheProvider = null;
            try
            {
                constentCacheProvider = CreateContentCacheProvider();

                result = new RedisCacheManager(constentCacheProvider);
            }
            catch (Exception)
	        {
                throw;
            }

            return result;
        }

        private static IContentCacheProvider CreateContentCacheProvider()
        {
            IContentCacheProvider result;

            result = new ContentCacheProviderRedisAzure();

            return result;
        }

    }
}
