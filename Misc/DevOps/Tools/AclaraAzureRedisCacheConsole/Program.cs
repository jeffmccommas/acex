﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AclaraAzureRedisCacheConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            RedisCacheManager redisCacheManager = null;

            try
            {

                redisCacheManager = RedisCacheManagerFactory.CreateRedisCacheManager();

                redisCacheManager.ClearCache();
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
