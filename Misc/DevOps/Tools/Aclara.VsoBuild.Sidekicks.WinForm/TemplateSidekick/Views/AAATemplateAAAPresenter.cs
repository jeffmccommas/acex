﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.Vso.Build.Client;
using Aclara.Vso.Build.Client.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using Aclara.VsoBuild.Sidekicks.WinForm.Views;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.VsoBuild.Sidekicks.WinForm.TemplateSidekick.Views
{

    /// <summary>
    /// Build definition: change queue status presenter.
    /// </summary>
    public class AAATemplateAAAPresenter
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private CustomLogger _logger = null;
        private SidekickConfiguration _sidekickConfiguration = null;
        private IAAATemplateAAAView _AAATemplateAAAView;
        private BuildDefinitionManager _buildDefinitionManager;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;
        private ITeamProjectInfo _teamProjectInfo = null;
        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.AAATemplateAAAView.SidekickName,
                                               this.AAATemplateAAAView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Change build definiton queue status view.
        /// </summary>
        public IAAATemplateAAAView AAATemplateAAAView
        {
            get { return _AAATemplateAAAView; }
            set { _AAATemplateAAAView = value; }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Build definition manager.
        /// </summary>
        public BuildDefinitionManager BuildDefinitionManager
        {
            get { return _buildDefinitionManager; }
            set { _buildDefinitionManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamPrjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="AAATemplateAAAView"></param>
        public AAATemplateAAAPresenter(ITeamProjectInfo teamPrjectInfo,
                                                         SidekickConfiguration sidekickConfiguration,
                                                         IAAATemplateAAAView AAATemplateAAAView)
        {
            this.TeamProjectInfo = teamPrjectInfo;
            this.SidekickConfiguration = sidekickConfiguration;
            this.AAATemplateAAAView = AAATemplateAAAView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private AAATemplateAAAPresenter()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalBuildDuration;
            AggregateException aggregateException = null;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalBuildDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.AAATemplateAAAView.BackgroundTaskCompleted("Change build definition queue status canceled.");
                        this.AAATemplateAAAView.ApplyButtonEnable(true);

                        Logger.Info(string.Format("Change build definition queue status cancelled."));


                        //Log background task completed log entry.
                        Logger.Info(string.Format("[*] Change build definition queue status completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                   totalBuildDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.AAATemplateAAAView.BackgroundTaskCompleted("");
                        this.AAATemplateAAAView.ApplyButtonEnable(true);


                        //Log background task completed log entry.

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    //Handle operation canceled exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format("Operation cancelled."));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format("Operation cancelled."));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format("Request failed."));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format("Request failed."));
                                    }

                                }
                            }
                        }

                        Logger.Info(string.Format("[*] Change build definition queue status completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                   totalBuildDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.AAATemplateAAAView.BackgroundTaskCompleted("Change build definition queue status request completed.");
                        this.AAATemplateAAAView.ApplyButtonEnable(true);

                        //Log background task completed log entry.
                        Logger.Info(string.Format("[*] Change build definition queue status completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                   totalBuildDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// AAATemplateMethodAAA.
        /// </summary>
        /// <param name="buildDefinitionSelectorList"></param>
        public void AAATemplateMethodAAA(BuildDefinitionSelectorList buildDefinitionSelectorList)
        {

            List<int> filteredBuidDefinitionIdList = null;
            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.AAATemplateAAAView.BackgroundTaskStatus = "Cancelling AAATemplateMethodAAA request...";
                    this.AAATemplateAAAView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.AAATemplateAAAView.BackgroundTaskStatus = "AAATemplateMethodAAA(s) requested...";
                    this.AAATemplateAAAView.ApplyButtonText = "Cancel";

                    this.BuildDefinitionManager = this.CreateBuildDefinitionManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    //Reterieve fresh build definition list. (In case build definitions have changed since last retrieval).
                    filteredBuidDefinitionIdList = buildDefinitionSelectorList.GetBuildDefinitionIdList();

                    this.BuildDefinitionManager.BuildDefinitionQueueStatusChanged += OnAAATemplateMethodAAACompleted;
                    //TODO: Replace with actual method call.
                    //this.BackgroundTask = new Task(() => this.BuildDefinitionManager.ChangeBuildDefinitionsQueueStatus(filteredBuidDefinitionIdList,
                    //                                                                                                   cancellationToken),
                    //                                                                                                   cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create build definition manager.
        /// </summary>
        /// <returns></returns>
        protected BuildDefinitionManager CreateBuildDefinitionManager()
        {
            BuildDefinitionManager result = null;

            try
            {
                result = BuildDefinitionManagerFactory.CreateBuildDefinitionManager(this.TeamProjectInfo.TeamProjectCollectionUri,
                                                                                    this.TeamProjectInfo.TeamProjectCollectionVsrmUri,
                                                                                    this.TeamProjectInfo.TeamProjectName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIUserProfileName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIPassword);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: AAATemplateMethodAAA.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="changeBuildDefinitionQueueStatusEventArgs"></param>
        protected void OnAAATemplateMethodAAACompleted(Object sender,
                                               ChangeBuildDefinitionQueueStatusEventArgs changeBuildDefinitionQueueStatusEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Build definition details available and status list details available.
                if (changeBuildDefinitionQueueStatusEventArgs.BuildDefinition != null &&
                    changeBuildDefinitionQueueStatusEventArgs.StatusList != null &&
                    changeBuildDefinitionQueueStatusEventArgs.StatusList.Count > 0)
                {
                    message = string.Format("AAATemplateMethodAAA request may have been canceled. (Build definition name: {0}; Status --> {1})",
                                             changeBuildDefinitionQueueStatusEventArgs.BuildDefinition.Name,
                                             changeBuildDefinitionQueueStatusEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    if (changeBuildDefinitionQueueStatusEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error) == true)
                    {
                        Logger.Error(message);

                    }
                    else if (changeBuildDefinitionQueueStatusEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Warning) == true)
                    {
                        Logger.Warn(message);
                    }
                    else
                    {
                        Logger.Info(message);

                    }
                }
                //Build definition details available and status list is unavailable.
                else if (changeBuildDefinitionQueueStatusEventArgs.BuildDefinition != null)
                {
                    message = string.Format("AAATemplateMethodAAA completed. (Build definition name: {0}, Queue status: {1})",
                                             changeBuildDefinitionQueueStatusEventArgs.BuildDefinition.Name,
                                             changeBuildDefinitionQueueStatusEventArgs.BuildDefinition.QueueStatus.ToString());
                    Logger.Info(message);
                }
                //Build definiton details unavailable.
                else
                {
                    message = string.Format("AAATemplateMethodAAA completed. Details unavailable.");
                    Logger.Info(message);
                }

                this.AAATemplateAAAView.UpdateProgressAAATemplateMethodAAA(changeBuildDefinitionQueueStatusEventArgs.BuildDefinitionChangedCount, 
                                                                                          changeBuildDefinitionQueueStatusEventArgs.BuildDefinitionTotalCount);
            }
            catch (Exception)
            {
                throw;
            }


        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                        }
                        else
                        {
                            Logger.Error(string.Format("Operation cancelled.(Unobserved)"));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                }
                else
                {
                    Logger.Error(string.Format("Operation cancelled.(Unobserved)"));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
