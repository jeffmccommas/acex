﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Tools.Common;
using Aclara.VsoBuild.Sidekicks.WinForm.Views;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.VsoBuild.Sidekicks.WinForm.TemplateSidekick.Views
{

    /// <summary>
    /// Build definition clone view - Interface.
    /// </summary>
    public interface IAAATemplateAAAView
    {
        string SidekickName { get; }

        string SidekickDescription { get; }

        SidekickContainerTypes.SidekickWarningLevel SidekickWarningLevel { get; }

        string BackgroundTaskStatus { get; set; }

        string ApplyButtonText { get; set; }

        void ApplyButtonEnable(bool enable);

        bool IsSidekickBusy { get; }

        void RetrieveRemainingDefinitions();

        void TeamProjectChanged();

        void UpdateProgressAAATemplateMethodAAA(int buildDefinitionChangedCount, int buildDefinitionTotalCount);

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);
    }

}
