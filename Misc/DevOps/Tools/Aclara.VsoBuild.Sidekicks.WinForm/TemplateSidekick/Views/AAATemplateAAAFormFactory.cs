﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using Aclara.VsoBuild.Sidekicks.WinForm.Views;


namespace Aclara.VsoBuild.Sidekicks.WinForm.TemplateSidekick.Views
{
    public class AAATemplateAAAFormFactory
    {

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <returns></returns>
        public static AAATemplateAAAForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                                      SidekickConfiguration sidekickConfiguration,
                                                                      BuildDefinitionSearchForm buildDefinitionSearchForm)
        {

            AAATemplateAAAForm result = null;
            AAATemplateAAAPresenter AAATemplateAAAPresenter = null;

            try
            {
                result = new AAATemplateAAAForm(sidekickConfiguration, buildDefinitionSearchForm);
                AAATemplateAAAPresenter = new AAATemplateAAAPresenter(teamProjectInfo, sidekickConfiguration, result);
                result.AAATemplateAAAPresenter = AAATemplateAAAPresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

    }
}
