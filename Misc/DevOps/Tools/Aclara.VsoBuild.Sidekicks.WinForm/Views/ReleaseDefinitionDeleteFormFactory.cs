﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using System;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Release definition delete form factory.
    /// </summary>
    public static class ReleaseDefinitionDeleteFormFactory
    {

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="releaseDefinitionSearchForm"></param>
        /// <returns></returns>
        public static ReleaseDefinitionDeleteForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                             SidekickConfiguration sidekickConfiguration,
                                                             ReleaseDefinitionSearchForm releaseDefinitionSearchForm)
        {

            ReleaseDefinitionDeleteForm result = null;
            ReleaseDefinitionDeletePresenter buildDefinitionClonePresenter = null;

            try
            {
                result = new ReleaseDefinitionDeleteForm(sidekickConfiguration, releaseDefinitionSearchForm);
                buildDefinitionClonePresenter = new ReleaseDefinitionDeletePresenter(teamProjectInfo, sidekickConfiguration, result);
                result.ReleaseDefinitionDeletePresenter = buildDefinitionClonePresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


    }
}
