﻿namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    partial class ReleaseDefinitionDeleteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.ReleaseDefinitionListLabel = new System.Windows.Forms.Label();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.HelpButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.ReleaseDefinitionSelectorListBox = new System.Windows.Forms.ListBox();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.DeleteReleaseBuildDefinitionProgressBar = new System.Windows.Forms.ProgressBar();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.VerticalSplitter = new System.Windows.Forms.Splitter();
            this.ReleaseDefinitionSearchPanel = new System.Windows.Forms.Panel();
            this.ReleaseDefinitionSearchPlaceholderLabel = new System.Windows.Forms.Label();
            this.CloneReleaseDefinitionToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.PropertiesPanel.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.ReleaseDefinitionSearchPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Controls.Add(this.ReleaseDefinitionListLabel);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Controls.Add(this.ReleaseDefinitionSelectorListBox);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesPanel.Location = new System.Drawing.Point(650, 0);
            this.PropertiesPanel.MinimumSize = new System.Drawing.Size(560, 480);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(566, 480);
            this.PropertiesPanel.TabIndex = 2;
            // 
            // ReleaseDefinitionListLabel
            // 
            this.ReleaseDefinitionListLabel.AutoSize = true;
            this.ReleaseDefinitionListLabel.Location = new System.Drawing.Point(6, 28);
            this.ReleaseDefinitionListLabel.Name = "ReleaseDefinitionListLabel";
            this.ReleaseDefinitionListLabel.Size = new System.Drawing.Size(197, 13);
            this.ReleaseDefinitionListLabel.TabIndex = 1;
            this.ReleaseDefinitionListLabel.Text = "Release definitions selected for deletion:";
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.HelpButton);
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(566, 25);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(538, 0);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 4;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(0, 1);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(529, 21);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "Delete Release Definitions";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ReleaseDefinitionSelectorListBox
            // 
            this.ReleaseDefinitionSelectorListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReleaseDefinitionSelectorListBox.FormattingEnabled = true;
            this.ReleaseDefinitionSelectorListBox.Location = new System.Drawing.Point(6, 44);
            this.ReleaseDefinitionSelectorListBox.Name = "ReleaseDefinitionSelectorListBox";
            this.ReleaseDefinitionSelectorListBox.Size = new System.Drawing.Size(552, 381);
            this.ReleaseDefinitionSelectorListBox.TabIndex = 2;
            // 
            // ControlPanel
            // 
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.DeleteReleaseBuildDefinitionProgressBar);
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Controls.Add(this.ApplyButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 440);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(566, 40);
            this.ControlPanel.TabIndex = 3;
            // 
            // DeleteReleaseBuildDefinitionProgressBar
            // 
            this.DeleteReleaseBuildDefinitionProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DeleteReleaseBuildDefinitionProgressBar.Location = new System.Drawing.Point(379, 18);
            this.DeleteReleaseBuildDefinitionProgressBar.Name = "DeleteReleaseBuildDefinitionProgressBar";
            this.DeleteReleaseBuildDefinitionProgressBar.Size = new System.Drawing.Size(100, 10);
            this.DeleteReleaseBuildDefinitionProgressBar.TabIndex = 5;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(485, 11);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 1;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // VerticalSplitter
            // 
            this.VerticalSplitter.BackColor = System.Drawing.SystemColors.ControlDark;
            this.VerticalSplitter.Location = new System.Drawing.Point(644, 0);
            this.VerticalSplitter.Name = "VerticalSplitter";
            this.VerticalSplitter.Size = new System.Drawing.Size(6, 480);
            this.VerticalSplitter.TabIndex = 1;
            this.VerticalSplitter.TabStop = false;
            // 
            // ReleaseDefinitionSearchPanel
            // 
            this.ReleaseDefinitionSearchPanel.AutoScroll = true;
            this.ReleaseDefinitionSearchPanel.Controls.Add(this.ReleaseDefinitionSearchPlaceholderLabel);
            this.ReleaseDefinitionSearchPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.ReleaseDefinitionSearchPanel.Location = new System.Drawing.Point(0, 0);
            this.ReleaseDefinitionSearchPanel.Name = "ReleaseDefinitionSearchPanel";
            this.ReleaseDefinitionSearchPanel.Size = new System.Drawing.Size(644, 480);
            this.ReleaseDefinitionSearchPanel.TabIndex = 0;
            // 
            // ReleaseDefinitionSearchPlaceholderLabel
            // 
            this.ReleaseDefinitionSearchPlaceholderLabel.AutoSize = true;
            this.ReleaseDefinitionSearchPlaceholderLabel.Location = new System.Drawing.Point(12, 9);
            this.ReleaseDefinitionSearchPlaceholderLabel.Name = "ReleaseDefinitionSearchPlaceholderLabel";
            this.ReleaseDefinitionSearchPlaceholderLabel.Size = new System.Drawing.Size(189, 13);
            this.ReleaseDefinitionSearchPlaceholderLabel.TabIndex = 0;
            this.ReleaseDefinitionSearchPlaceholderLabel.Text = "Release Definition Search Placeholder";
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.VsoBuild.Sidekicks.Help.chm";
            // 
            // ReleaseDefinitionDeleteForm
            // 
            this.AcceptButton = this.ApplyButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 480);
            this.ControlBox = false;
            this.Controls.Add(this.PropertiesPanel);
            this.Controls.Add(this.VerticalSplitter);
            this.Controls.Add(this.ReleaseDefinitionSearchPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "ReleaseDefinitionDeleteForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.PropertiesPanel.ResumeLayout(false);
            this.PropertiesPanel.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.ReleaseDefinitionSearchPanel.ResumeLayout(false);
            this.ReleaseDefinitionSearchPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Splitter VerticalSplitter;
        private System.Windows.Forms.Panel ReleaseDefinitionSearchPanel;
        private System.Windows.Forms.Label ReleaseDefinitionSearchPlaceholderLabel;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.ToolTip CloneReleaseDefinitionToolTip;
        private System.Windows.Forms.Label ReleaseDefinitionListLabel;
        private System.Windows.Forms.ListBox ReleaseDefinitionSelectorListBox;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.ProgressBar DeleteReleaseBuildDefinitionProgressBar;
        private System.Windows.Forms.Button HelpButton;
    }
}