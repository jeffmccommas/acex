﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class NotificationPreviewForm : Form, INotificationPreviewView
    {



        #region Private Constants

        private const string UserProfileNameMustBeSpecified = "User profile name is required.";
        private const string PasswordMustBeSpecified = "Password name is required.";
        private const string FormHelp_HelpFileName = "AuthenticationVsoPersonalAccessToken.htm";

        #endregion


        #region Private Data Members

        NotificationPreviewPresenter _notificationPreviewPresenter = null;
        private bool _userProfileNameReadonly;

        #endregion

        #region Public Properties
        /// <summary>
        /// Property: Notification preview presenter.
        /// </summary>
        public NotificationPreviewPresenter NotificationPreviewPresenter
        {
            get { return _notificationPreviewPresenter; }
            set { _notificationPreviewPresenter = value; }
        }

        /// <summary>
        /// Property: Notification preview text.
        /// </summary>
        public string NotificationPreviewText
        {
            get
            {
                return this.NotificationPreviewTextBox.Text;
            }

            set
            {
                this.NotificationPreviewTextBox.Text = value;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public NotificationPreviewForm()
        {
            InitializeComponent();
        }


        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        /// <summary>
        /// Event handler: Close button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

    }
}
