﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public interface IBuildDefinitionDeleteView
    {
        string SidekickName { get; }

        string SidekickDescription { get; }

        SidekickContainerTypes.SidekickWarningLevel SidekickWarningLevel { get; }

        string BackgroundTaskStatus { get; set; }

        string ApplyButtonText { get; set; }

        void ApplyButtonEnable(bool enable);

        bool IsSidekickBusy { get; }

        void RetrieveRemainingDefinitions();

        void TeamProjectChanged();

        void UpdateProgressDeleteBuildDefinition(int buildDefinitionDeletedCount, int buildDefinitionTotalCount);

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);
    }
}
