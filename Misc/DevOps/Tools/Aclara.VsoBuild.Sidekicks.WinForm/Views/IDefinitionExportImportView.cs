﻿using System.IO;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build definition clone view - Interface.
    /// </summary>
    public interface IDefinitionExportImportView
    {
        string SidekickName { get; }

        string SidekickDescription { get; }

        SidekickContainerTypes.SidekickWarningLevel SidekickWarningLevel { get; }

        string BackgroundTaskStatus { get; set; }

        bool IsSidekickBusy { get; }

        void RetrieveRemainingDefinitions();

        void TeamProjectChanged();

        void UpdateProgressExportImportStatus(int buildDefinitionChangedCount, int buildDefinitionTotalCount);

        void UpdateExportZipArchiveMemoryStream(MemoryStream exportZipArchiveMemoryStream);
        
        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);
    }

}
