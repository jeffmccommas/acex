﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class ReleaseDefinitionSearchForm : Form, IReleaseDefinitionSearchView
    {

        #region Private Constants

        private const string ReleaseDefinitionDataGridViewColumn_Check = "CheckColumn";
        private const string ReleaseDefinitionDataGridViewColumn_Name = "NameColumn";
        private const string ReleaseDefinitionDataGridViewColumn_Id = "IdColumn";
        private const string ReleaseDefinitionDataGridViewColumn_Enabled = "EnabledColumn";
        private const string ReleaseDefinitionDataGridViewColumn_Comment = "CommentColumn";
        private const string ReleaseDefinitionDataGridViewColumn_Extra = "ExtraColumn";

        private const string ReleaseDefinitionSelectorProperty_Selected = "Selected";
        private const string ReleaseDefinitionSelectorProperty_Enabled = "Enabled";
        private const string ReleaseDefinitionSelectorProperty_Highlighted = "Highlighted";
        private const string ReleaseDefinitionSelectorProperty_Id = "Id";
        private const string ReleaseDefinitionSelectorProperty_Name = "Name";
        private const string ReleaseDefinitionSelectorProperty_Comment = "Comment";
        private const string ReleaseDefinitionSelectorProperty_Extra = "Extra";

        private const string ReleaseDefinitionTag_Dev = "dev";
        private const string ReleaseDefinitionTag_Qa = "qa";
        private const string ReleaseDefinitionTag_Uat = "uat";
        private const string ReleaseDefinitionTag_Perf = "perf";
        private const string ReleaseDefinitionTag_Production = "prod";
        private const string ReleaseDefinitionTag_DevDR = "devdr";
        private const string ReleaseDefinitionTag_UATDR = "uatdr";
        private const string ReleaseDefinitionTag_ProdDR = "proddr";

        private const string Environment_dev = "dev";
        private const string Environment_qa = "qa";
        private const string Environment_uat = "uat";
        private const string Environment_perf = "perf";
        private const string Environment_prod = "prod";
        private const string Environment_devdr = "devdr";
        private const string Environment_uatdr = "uatdr";
        private const string Environment_proddr = "proddr";

        private const string MatchStatus_ExactMatchFound = "{0} exact match found.";
        private const string MatchStatus_ExactMatchFoundPlural = "{0} exact matches found.";
        private const string MatchStatus_PartialMatchFound = "{0} partial match found.";
        private const string MatchStatus_PartialMatchFoundPlural = "{0} partial matches found.";
        private const string MatchStatus_ExactMatchEnabled = "Exact match enabled.";

        private const string ConfirmOperation_LargeReleaseDefinitionCount_Caption = "Confirmation RequiredConfirmOperationLargeBuildDefinitionCount";
        private const string ConfirmOperation_LargeReleaseDefinitionCount_Message = "{0} release definitions have been checked. Click 'Yes' to continue.";
        private const int ConfirmOperation_MaxReleaseDefinitionCount = 10;

        private const string ContextMenuMessage_NoReleaseDefinitionsChecked = "No release definitions checked.";

        #endregion

        #region Private Data Members

        private SidekickConfiguration _sidekickConfiguration = null;
        private ReleaseDefinitionSelectorList _releaseDefinitionSelectorList = null;
        private SortableBindingList<ReleaseDefinitionSelector> _releaseDefinitionSelectoryListBindingSource;
        private ReleaseDefinitionSearchPresenter _releaseDefinitionSearchPresenter = null;
        private bool _retrieveRemainingDefinitionsEnabled;
        private bool _batchCheckInProgress;
        private ISidekickCoaction _sidekickCoaction;
        private System.Windows.Forms.SortOrder _releaseDefinitionListDataGridViewSortOrder;
        private DataGridViewColumn _releaseDefinitionListDataGridViewSortedColumn;

        #endregion

        #region Public Delegates

        public event EventHandler<ReleaseDefinitionSelectionChangedEventArgs> ReleaseDefinitionSelectionChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Release definition search presenter.
        /// </summary>
        public ReleaseDefinitionSearchPresenter ReleaseDefinitionSearchPresenter
        {
            get { return _releaseDefinitionSearchPresenter; }
            set { _releaseDefinitionSearchPresenter = value; }
        }

        /// <summary>
        /// Property: Release definition name (partial).
        /// </summary>
        public string ReleaseDefinitionNamePattern
        {
            get
            {
                return this.ReleaseDefinitionNameFilterTextBox.Text;
            }
            set
            {
                this.ReleaseDefinitionNameFilterTextBox.Text = value;
            }
        }

        /// <summary>Release definition selector list.
        /// </summary>
        public ReleaseDefinitionSelectorList ReleaseDefinitionSelectorList
        {
            get
            {
                return _releaseDefinitionSelectorList;
            }
            set
            {
                _releaseDefinitionSelectorList = value;
            }
        }

        /// <summary>
        /// Property: Release definition selector list binding source.
        /// </summary>
        public SortableBindingList<ReleaseDefinitionSelector> ReleaseDefinitionSelectorListBindingSource
        {
            get { return _releaseDefinitionSelectoryListBindingSource; }
            set { _releaseDefinitionSelectoryListBindingSource = value; }
        }

        /// <summary>
        /// Property: Sidekick coaction.
        /// </summary>
        public ISidekickCoaction SidekickCoaction
        {
            get { return _sidekickCoaction; }
            set { _sidekickCoaction = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get
            {
                if (this.InvokeRequired == true)
                {
                    return (string)this.Invoke(new Func<string>(() => this.BackgroundTaskStatus));
                }
                else
                {
                    return this.ReleaseDefinitionRetrievalStatusLabel.Text;
                }

            }
            set
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke(new Action(() => { this.BackgroundTaskStatus = value; }));
                }
                else
                {
                    this.ReleaseDefinitionRetrievalStatusLabel.Text = value;
                }
            }

        }

        /// <summary>
        /// Property: Release definition retrieval status.
        /// </summary>
        public string ReleaseDefinitionRetrievalStatus
        {
            get { return this.ReleaseDefinitionRetrievalStatusLabel.Text; }
            set { this.ReleaseDefinitionRetrievalStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Release definition match status.
        /// </summary>
        public string ReleaseDefinitionMatchStatus
        {
            get { return this.ReleaseDefinitionMatchStatusLabel.Text; }
            set { this.ReleaseDefinitionMatchStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Retrieve remaining build definitions enabled.
        /// </summary>
        public bool RetrieveRemainingDefinitionsEnabled
        {
            get
            {
                return _retrieveRemainingDefinitionsEnabled;
            }
            set
            {
                _retrieveRemainingDefinitionsEnabled = value;
            }
        }

        /// <summary>
        /// Property: Batch check in progress.
        /// </summary>
        public bool BatchCheckInProgress
        {
            get { return _batchCheckInProgress; }
            set { _batchCheckInProgress = value; }
        }

        /// <summary>
        /// Property: Exact match (release definition search).
        /// </summary>
        public bool ExactMatch
        {
            get { return this.ExactMatchCheckBox.Checked; }
            set { this.ExactMatchCheckBox.Checked = value; }
        }

        /// <summary>
        /// Property: Release definition list data grid view sort order.
        /// </summary>
        public System.Windows.Forms.SortOrder ReleaseDefinitionListDataGridViewSortOrder
        {
            get { return _releaseDefinitionListDataGridViewSortOrder; }
            set { _releaseDefinitionListDataGridViewSortOrder = value; }
        }

        /// <summary>
        /// Property: Release definition list data grid view sorted column.
        /// </summary>
        public DataGridViewColumn ReleaseDefinitionListDataGridViewSortedColumn
        {
            get { return _releaseDefinitionListDataGridViewSortedColumn; }
            set { _releaseDefinitionListDataGridViewSortedColumn = value; }
        }

        #endregion

        #region Public Constructors
        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="releaseDefinitionSearchPresenter"></param>
        /// <param name="sidekickConfiguration"></param>
        public ReleaseDefinitionSearchForm(ReleaseDefinitionSearchPresenter releaseDefinitionSearchPresenter,
                                           SidekickConfiguration sidekickConfiguration,
                                           ISidekickCoaction sidekickCoaction)
        {
            _sidekickConfiguration = sidekickConfiguration;
            _releaseDefinitionSelectorList = new ReleaseDefinitionSelectorList();

            _releaseDefinitionSearchPresenter = releaseDefinitionSearchPresenter;
            InitializeComponent();
            InitializeControls();
            _releaseDefinitionSelectoryListBindingSource = new SortableBindingList<ReleaseDefinitionSelector>(_releaseDefinitionSelectorList);
            this.ReleaseDefinitionListDataGridView.DataSource = _releaseDefinitionSelectoryListBindingSource;
            _retrieveRemainingDefinitionsEnabled = true;
            _sidekickCoaction = sidekickCoaction;
        }
        #endregion

        #region Protected Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        protected ReleaseDefinitionSearchForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Retrieve remaining build definitions.
        /// </summary>
        public void RetrieveRemainingDefinitions()
        {

            try
            {

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update refresh cancel mode.
        /// </summary>
        /// <param name="enabled"></param>
        public void UpdateRefreshCancelMode(bool enabled)
        {
            try
            {
                if (enabled == true)
                {
                    this.RefreshButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionCancelRefresh;
                }
                else
                {
                    this.RefreshButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {

            try
            {
                this.ExactMatch = false;
                //InitializeBuildDefinitionListDataGridView();
                this.ReleaseDefinitionRetrievalStatusLabel.Text = string.Empty;
                this.ReleaseDefinitionMatchStatusLabel.Text = string.Empty;
                this.ReleaseDefinitionSearchToolTip.OwnerDraw = true;

                this.UpdateRefreshCancelMode(false);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Initialize release definition list data grid view.
        /// </summary>
        protected void InitializeReleaseDefinitionListDataGridView()
        {
            BindingSource bindingSource = null;

            try
            {

                this.ReleaseDefinitionListDataGridView.DataSource = null;
                this.ReleaseDefinitionListDataGridView.AutoGenerateColumns = false;
                this.ReleaseDefinitionListDataGridView.DataSource = bindingSource;

                foreach (DataGridViewColumn column in this.ReleaseDefinitionListDataGridView.Columns)
                {

                    switch (column.Name)
                    {
                        case ReleaseDefinitionDataGridViewColumn_Check:
                            column.DataPropertyName = ReleaseDefinitionSelectorProperty_Selected;
                            break;
                        case ReleaseDefinitionDataGridViewColumn_Enabled:
                            column.DataPropertyName = ReleaseDefinitionSelectorProperty_Enabled;
                            break;
                        case ReleaseDefinitionDataGridViewColumn_Id:
                            column.DataPropertyName = ReleaseDefinitionSelectorProperty_Id;
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            break;
                        case ReleaseDefinitionDataGridViewColumn_Name:
                            column.DataPropertyName = ReleaseDefinitionSelectorProperty_Name;
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            break;
                        case ReleaseDefinitionDataGridViewColumn_Comment:
                            column.DataPropertyName = ReleaseDefinitionSelectorProperty_Comment;
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            break;
                        case ReleaseDefinitionDataGridViewColumn_Extra:
                            column.DataPropertyName = ReleaseDefinitionSelectorProperty_Extra;
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            break;
                    }
                }

                if (this.ReleaseDefinitionListDataGridView.RowCount > 0)
                {
                    EnableControlsDependantOnSearch(true);
                }
                else
                {
                    EnableControlsDependantOnSearch(false);
                }


            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Release definition selection changed.
        /// </summary>
        protected void RaiseReleaseDefinitionSelectionChangedEvent()
        {
            ReleaseDefinitionSelectionChangedEventArgs buildDefinitionSelectionChangedEventArgs = null;
            ReleaseDefinitionSelector buildDefinitionSelector = null;

            try
            {

                buildDefinitionSelectionChangedEventArgs = new ReleaseDefinitionSelectionChangedEventArgs();

                buildDefinitionSelectionChangedEventArgs.OneOrMoreSelected = false;
                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {

                    buildDefinitionSelector = (ReleaseDefinitionSelector)dataGridViewRow.DataBoundItem;
                    if (buildDefinitionSelector.Selected == true)
                    {
                        buildDefinitionSelectionChangedEventArgs.OneOrMoreSelected = true;
                        break;
                    }

                }

                if (ReleaseDefinitionSelectionChanged != null)
                {
                    ReleaseDefinitionSelectionChanged(this, buildDefinitionSelectionChangedEventArgs);
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Toggle display release definition with matching tag.
        /// </summary>
        /// <param name="highlight"></param>
        /// <param name="matchTag"></param>
        protected void ToggleHighlightReleaseDefinitionsWithMatchingTag(bool highlight, string matchTag)
        {
            ReleaseDefinitionSelector releaseDefinitionSelector = null;
            DataGridViewCell dataGridViewCell = null;
            string name = string.Empty;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {
                    dataGridViewCell = dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Name];

                    if (dataGridViewCell != null)
                    {
                        if (dataGridViewCell.Value != null)
                        {
                            name = dataGridViewCell.Value.ToString();
                            releaseDefinitionSelector = (ReleaseDefinitionSelector)ReleaseDefinitionListDataGridView.Rows[dataGridViewRow.Index].DataBoundItem;

                            if (string.IsNullOrEmpty(name) == false && name.ToLower().EndsWith(matchTag.ToLower()) == true)
                            {

                                if (highlight == true)
                                {
                                    dataGridViewRow.DefaultCellStyle.BackColor = System.Drawing.Color.LightGoldenrodYellow;
                                    releaseDefinitionSelector.Highlighted = true;
                                }
                                else
                                {
                                    dataGridViewRow.DefaultCellStyle.BackColor = System.Drawing.Color.White;
                                    releaseDefinitionSelector.Highlighted = false;
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Toggle check release definition with matching tag.
        /// </summary>
        /// <param name="matchTag"></param>
        protected void ToggleCheckReleaseDefinitionsWithMatchingTag(List<string> releaseDefinitionTagMatchList)
        {
            ReleaseDefinitionSelector releaseDefinitionSelector = null;
            DataGridViewCell dataGridViewCell = null;
            string name = string.Empty;
            bool checkChanged = false;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {
                    dataGridViewCell = dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Name];

                    if (dataGridViewCell != null)
                    {
                        if (dataGridViewCell.Value != null)
                        {
                            name = dataGridViewCell.Value.ToString();
                            releaseDefinitionSelector =
                                (ReleaseDefinitionSelector)
                                    ReleaseDefinitionListDataGridView.Rows[dataGridViewRow.Index].DataBoundItem;

                            if (string.IsNullOrEmpty(name) == false &&
                                releaseDefinitionTagMatchList.Any(tag => name.ToLower().EndsWith(tag)) == true)
                            {
                                dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Check].Value = true;
                                checkChanged = true;
                            }
                            else
                            {
                                dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Check].Value = false;
                                checkChanged = true;
                            }

                        }
                    }

                }

                if (checkChanged == true)
                {
                    this.RaiseReleaseDefinitionSelectionChangedEvent();
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        /// <summary>
        /// Toggle display release definition with matching tag.
        /// </summary>
        /// <param name="releaseDefinitionTagMatchList"></param>
        protected void ToggleDisplayReleaseDefinitionsWithMatchingTag(List<string> releaseDefinitionTagMatchList)
        {
            DataGridViewCell dataGridViewCell = null;
            string name = string.Empty;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {
                    dataGridViewCell = dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Name];

                    if (dataGridViewCell != null)
                    {
                        if (dataGridViewCell.Value != null)
                        {
                            name = dataGridViewCell.Value.ToString();
                        }
                    }
                    else
                    {
                        name = string.Empty;
                    }

                    if (string.IsNullOrEmpty(name) == false &&
                        releaseDefinitionTagMatchList.Any(tag => name.ToLower().EndsWith(tag)) == true)
                    {
                        dataGridViewRow.Visible = true;
                    }
                    else
                    {
                        CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[ReleaseDefinitionListDataGridView.DataSource];
                        currencyManager1.SuspendBinding();
                        dataGridViewRow.Visible = false;
                        currencyManager1.ResumeBinding();
                    }
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Remove release definition highlight - all.
        /// </summary>
        protected void RemoveReleaseDefinitionHighlightAll()
        {
            ReleaseDefinitionSelector releaseDefinitionSelector = null;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {
                    releaseDefinitionSelector = (ReleaseDefinitionSelector)ReleaseDefinitionListDataGridView.Rows[dataGridViewRow.Index].DataBoundItem;

                    if (releaseDefinitionSelector.Highlighted == true)
                    {
                        dataGridViewRow.DefaultCellStyle.BackColor = System.Drawing.Color.White;
                        releaseDefinitionSelector.Highlighted = false;
                    }
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Enable controls dependant on team project.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnTeamProject(bool enabled)
        {
            this.ReleaseDefinitionNameFilterTextBox.Enabled = enabled;
            this.SearchButton.Enabled = enabled;
            this.ClearButton.Enabled = enabled;
            this.RefreshButton.Enabled = enabled;
        }

        /// <summary>
        /// Enable controls dependant on search.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnSearch(bool enabled)
        {
            this.CheckSplitButton.Enabled = enabled;
            this.DisplaySplitButton.Enabled = enabled;
            this.HighlightSplitButton.Enabled = enabled;
            this.ViewSplitButton.Enabled = enabled;
            this.CopyButton.Enabled = enabled;
            this.ReleaseDefinitionListDataGridView.Enabled = enabled;
        }

        /// <summary>
        /// Perform search.
        /// </summary>
        public void PerformSearch()
        {
            ReleaseDefinitionSelectorList filteredBuildDefinitionSelectorList = null;
            ListSortDirection sortDirection;

            try
            {
                this.ReleaseDefinitionStatusPanel.BackColor = Color.White;

                if (this.InvokeRequired == true)
                {
                    this.Invoke(new Action(this.PerformSearch));
                }
                else
                {
                        this.ReleaseDefinitionListDataGridViewSortOrder = this.ReleaseDefinitionListDataGridView.SortOrder;
                        this.ReleaseDefinitionListDataGridViewSortedColumn = this.ReleaseDefinitionListDataGridView.SortedColumn;

                    //Reset split buttons context menu state.
                    this.ResetReleaseDefinitionCheckToolStripItemCheckedState();
                    this.ResetReleaseDefinitionDisplayToolStripItemCheckedState();
                    this.ResetReleaseDefinitionHighlightToolStripItemCheckedState();

                    if (this.ReleaseDefinitionSelectorList == null || this.ReleaseDefinitionSelectorList.Count == 0)
                    {
                        this.ReleaseDefinitionListDataGridView.DataSource = null;
                        this.ReleaseDefinitionSearchPresenter.GetReleaseDefinitionSelectorListAsync(this.ReleaseDefinitionNamePattern, null, ReleaseDefinitionRetrievalMode.Reset);
                        this.UpdateRefreshCancelMode(true);
                        return;
                    }

                    filteredBuildDefinitionSelectorList = this.ReleaseDefinitionSelectorList.GetSourceReleaseDefinitionListFilterdByReleaseDefinitionNamePattern(
                                                                                                        this.ReleaseDefinitionNamePattern,
                                                                                                        this.ExactMatch);
                    filteredBuildDefinitionSelectorList.Sort(new ReleaseDefinitionSelector());

                    this.ReleaseDefinitionSelectorListBindingSource = new SortableBindingList<ReleaseDefinitionSelector>(filteredBuildDefinitionSelectorList);
                    this.ReleaseDefinitionListDataGridView.DataSource = this.ReleaseDefinitionSelectorListBindingSource;

                    foreach (DataGridViewColumn column in this.ReleaseDefinitionListDataGridView.Columns)
                    {

                        switch (column.Name)
                        {
                            case ReleaseDefinitionDataGridViewColumn_Check:
                                break;
                            case ReleaseDefinitionDataGridViewColumn_Enabled:
                                break;
                            case ReleaseDefinitionDataGridViewColumn_Id:
                                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                                break;
                            case ReleaseDefinitionDataGridViewColumn_Name:
                                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                                break;
                            case ReleaseDefinitionDataGridViewColumn_Comment:
                                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                                break;
                            case ReleaseDefinitionDataGridViewColumn_Extra:
                                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                                break;
                        }
                    }

                    if (this.ReleaseDefinitionListDataGridView.RowCount > 0)
                    {
                        EnableControlsDependantOnSearch(true);
                    }
                    else
                    {
                        EnableControlsDependantOnSearch(false);
                    }


                    if (this.ExactMatchCheckBox.Checked == true)
                    {
                        if (filteredBuildDefinitionSelectorList.Count == 1)
                        {
                            this.ReleaseDefinitionMatchStatus = string.Format(MatchStatus_ExactMatchFound,
                                                  filteredBuildDefinitionSelectorList.Count);
                        }
                        else
                        {
                            this.ReleaseDefinitionMatchStatus = string.Format(MatchStatus_ExactMatchFoundPlural,
                                                  filteredBuildDefinitionSelectorList.Count);
                        }
                        if (filteredBuildDefinitionSelectorList.Count == 0)
                        {
                            this.ReleaseDefinitionSearchToolTip.Show(MatchStatus_ExactMatchEnabled, this.ReleaseDefinitionMatchStatusLabel, 4000);
                        }
                    }
                    else
                    {
                        if (filteredBuildDefinitionSelectorList.Count == 1)
                        {
                            this.ReleaseDefinitionMatchStatus = string.Format(MatchStatus_PartialMatchFound,
                                                  filteredBuildDefinitionSelectorList.Count);
                        }
                        else
                        {
                            this.ReleaseDefinitionMatchStatus = string.Format(MatchStatus_PartialMatchFoundPlural,
                                                  filteredBuildDefinitionSelectorList.Count);
                        }
                    }

                    this.ReleaseDefinitionStatusPanel.BackColor = Color.OldLace;

                    this.RaiseReleaseDefinitionSelectionChangedEvent();

                    for (int columnIndex = 0; columnIndex < ReleaseDefinitionListDataGridView.Columns.Count; columnIndex++)
                    {
                        int columnWidth = ReleaseDefinitionListDataGridView.Columns[columnIndex].Width;
                        ReleaseDefinitionListDataGridView.Columns[columnIndex].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                        ReleaseDefinitionListDataGridView.Columns[columnIndex].Width = columnWidth;
                    }

                }

                //Restore sort order.
                if (this.ReleaseDefinitionListDataGridViewSortOrder == SortOrder.Ascending)
                {
                    sortDirection = ListSortDirection.Ascending;
                }
                else
                {
                    sortDirection = ListSortDirection.Descending;
                }

                if (this.ReleaseDefinitionListDataGridViewSortedColumn != null)
                {
                    this.ReleaseDefinitionListDataGridView.Sort(this.ReleaseDefinitionListDataGridView.Columns[this.ReleaseDefinitionListDataGridViewSortedColumn.Name], sortDirection); ;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }
    
        /// <summary>
        /// Clear search.
        /// </summary>
        public void ClearSearch()
        {
            try
            {
                this.SearchButton.BackColor = System.Drawing.Color.DarkOrange;
                this.ReleaseDefinitionSelectorList = new ReleaseDefinitionSelectorList();
                this.ReleaseDefinitionNamePattern = string.Empty;
                this.EnableControlsDependantOnSearch(false);
                this.ReleaseDefinitionListDataGridView.DataSource = null;
                this.ReleaseDefinitionStatusPanel.BackColor = Color.White;
                this.ReleaseDefinitionMatchStatus = string.Empty;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {

            try
            {

                this.SearchButton.BackColor = System.Drawing.Color.DarkOrange;

                ClearSearch();
                this.ReleaseDefinitionSearchPresenter.TeamProjectNameChanged();
                InitializeReleaseDefinitionListDataGridView();

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update release definition retrieval status.
        /// </summary>
        /// <param name="releaseDefinitionListRetrievedCount"></param>
        /// <param name="releaseDefinitionListTotalCount"></param>
        /// <param name="cancelled"></param>
        public void UpdateReleaseDefinitionRetrievalStatus(int releaseDefinitionListRetrievedCount, int releaseDefinitionListTotalCount, bool cancelled)
        {
            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        UpdateReleaseDefinitionRetrievalStatus(releaseDefinitionListRetrievedCount,
                                                             releaseDefinitionListTotalCount,
                                                             cancelled);
                    });
                }
                else
                {
                    if (releaseDefinitionListRetrievedCount < releaseDefinitionListTotalCount)
                    {
                        ReleaseDefinitionRetrievalProgressBar.Visible = true;
                        ReleaseDefinitionRetrievalProgressBar.Minimum = 0;
                        ReleaseDefinitionRetrievalProgressBar.Maximum = releaseDefinitionListTotalCount;
                        ReleaseDefinitionRetrievalProgressBar.Value = releaseDefinitionListRetrievedCount;
                    }
                    else
                    {
                        ReleaseDefinitionRetrievalProgressBar.Visible = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                if (this.ReleaseDefinitionSelectorList == null ||
                    this.ReleaseDefinitionSelectorList.Count == 0)
                {
                    this.SearchButton.BackColor = System.Drawing.Color.DarkOrange;
                }
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.UpdateReleaseDefinitionRetrievalStatus(0, 0, true);
                this.UpdateRefreshCancelMode(false);

            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.UpdateRefreshCancelMode(false);
            }
        }

        /// <summary>
        /// Retrieve release definition selectory list.
        /// </summary>
        /// <returns></returns>
        public ReleaseDefinitionSelectorList GetSelectedReleaseDefinitionSelectorList()
        {
            ReleaseDefinitionSelectorList result = null;
            ReleaseDefinitionSelector releaseDefinitionSelector = null;
            ReleaseDefinitionSelectorList releaseDefinitionSelectorList = null;
            bool checkedValue = false;
            int id;
            string name = string.Empty;

            try
            {
                result = new ReleaseDefinitionSelectorList();

                releaseDefinitionSelectorList = this.ReleaseDefinitionSelectorList;

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {
                    if (dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Check].Value is Boolean)
                    {
                        checkedValue = (bool)dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Check].Value;
                        name = (string)dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Name].Value;
                        if (int.TryParse(dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Id].Value.ToString(), out id) == false)
                        {
                            id = 0;
                        }
                        if (checkedValue == true)
                        {
                            releaseDefinitionSelector = (ReleaseDefinitionSelector)dataGridViewRow.DataBoundItem;
                            result.Add(releaseDefinitionSelector);
                        }
                    }
                }

                //Populate remaining properties.
                result.ForEach(rds =>
                {
                    releaseDefinitionSelectorList.ForEach(search =>
                    {
                        if (rds.Name == search.Name)
                        {
                            rds.Enabled = search.Enabled;
                        }
                    });
                });

            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve selected release definition selector count.
        /// </summary>
        /// <returns></returns>
        public int GetSelectedReleaseDefinitionSelectorCount()
        {
            int result = 0;

            ReleaseDefinitionSelectorList releaseDefinitionSelectorList = null;

            try
            {
                releaseDefinitionSelectorList = this.GetSelectedReleaseDefinitionSelectorList();

                result = releaseDefinitionSelectorList.Count;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve selected release definition selector list branch names.
        /// </summary>
        /// <returns></returns>
        public List<string> GetSelectedReleaseDefinitionSelectorListBranchNameList()
        {
            List<string> result = null;
            ReleaseDefinitionSelectorList selectedBuildDefinitionSelectorList = null;

            try
            {

                selectedBuildDefinitionSelectorList = GetSelectedReleaseDefinitionSelectorList();

                result = this.GetBranchNames(selectedBuildDefinitionSelectorList);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve branch name list.
        /// </summary>
        public List<string> GetBranchNames(ReleaseDefinitionSelectorList releaseDefinitionSelectorList)
        {
            List<string> result = null;
            string path = string.Empty;
            string matchedYearMonthBranchName = string.Empty;
            Match match = null;
            string pattern = string.Empty;

            try
            {

                result = new List<string>();

                pattern = @"(\d+).(\d+[I]?)";

                foreach (ReleaseDefinitionSelector releaseDefinitionSelector in releaseDefinitionSelectorList)
                {

                    match = Regex.Match(releaseDefinitionSelector.Name,
                                              pattern,
                                              RegexOptions.IgnoreCase);

                    // Match found.
                    if (match.Success)
                    {
                        if (match.Groups.Count == 3)
                        {
                            matchedYearMonthBranchName = match.Groups[1].Value + "." + match.Groups[2].Value;
                        }

                        if (result.Contains(matchedYearMonthBranchName) == false)
                        {
                            result.Add(matchedYearMonthBranchName);
                        }
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Retrieve environment list.
        /// </summary>
        public List<string> GetEnvironments(ReleaseDefinitionSelectorList buildDefinitionSelectorList)
        {
            List<string> result = null;

            try
            {

                result = new List<string>();

                foreach (ReleaseDefinitionSelector releaseDefinitionSelector in buildDefinitionSelectorList)
                {

                    // Environment - dev match.
                    if (releaseDefinitionSelector.Name.ToLower().EndsWith(Environment_dev) == true)
                    {
                        if (result.Contains(Environment_dev) == false)
                        {
                            result.Add(Environment_dev);
                        }
                    }

                    // Environment - qa match.
                    if (releaseDefinitionSelector.Name.ToLower().EndsWith(Environment_qa) == true)
                    {
                        if (result.Contains(Environment_qa) == false)
                        {
                            result.Add(Environment_qa);
                        }
                    }

                    // Environment - uat match.
                    if (releaseDefinitionSelector.Name.ToLower().EndsWith(Environment_uat) == true)
                    {
                        if (result.Contains(Environment_uat) == false)
                        {
                            result.Add(Environment_uat);
                        }
                    }

                    // Environment - perf match.
                    if (releaseDefinitionSelector.Name.ToLower().EndsWith(Environment_perf) == true)
                    {
                        if (result.Contains(Environment_perf) == false)
                        {
                            result.Add(Environment_perf);
                        }
                    }

                    // Environment - prod match.
                    if (releaseDefinitionSelector.Name.ToLower().EndsWith(Environment_prod) == true)
                    {
                        if (result.Contains(Environment_prod) == false)
                        {
                            result.Add(Environment_prod);
                        }
                    }

                    // Environment - devdr match.
                    if (releaseDefinitionSelector.Name.ToLower().EndsWith(Environment_devdr) == true)
                    {
                        if (result.Contains(Environment_devdr) == false)
                        {
                            result.Add(Environment_devdr);
                        }
                    }

                    // Environment - uatdr match.
                    if (releaseDefinitionSelector.Name.ToLower().EndsWith(Environment_uatdr) == true)
                    {
                        if (result.Contains(Environment_uatdr) == false)
                        {
                            result.Add(Environment_uatdr);
                        }
                    }
                    // Environment - proddr match.
                    if (releaseDefinitionSelector.Name.ToLower().EndsWith(Environment_proddr) == true)
                    {
                        if (result.Contains(Environment_proddr) == false)
                        {
                            result.Add(Environment_proddr);
                        }
                    }

                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Reset build definition check tool strip items - checked state.
        /// </summary>
        protected void ResetReleaseDefinitionCheckToolStripItemCheckedState()
        {

            try
            {
                if (this.ReleaseDefinitionCheckDevToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionCheckDevToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionCheckQAToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionCheckQAToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionCheckUATToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionCheckUATToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionCheckPerfToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionCheckPerfToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionCheckProductionToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionCheckProductionToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionCheckDevDRToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionCheckDevDRToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionCheckUATDRToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionCheckUATDRToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionCheckProdDRToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionCheckProdDRToolStripMenuItem.Checked = false;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Reset build definition highlight tool strip items - checked state.
        /// </summary>
        protected void ResetReleaseDefinitionHighlightToolStripItemCheckedState()
        {

            try
            {
                if (this.ReleaseDefinitionHighlightDevToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionHighlightDevToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionHighlightQAToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionHighlightQAToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionHighlightUATToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionHighlightUATToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionHighlightPerfToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionHighlightPerfToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionHighlightProductionToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionHighlightProductionToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionHighlightDevDRToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionHighlightDevDRToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionHighlightUATDRToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionHighlightUATDRToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionCheckProdDRToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionCheckProdDRToolStripMenuItem.Checked = false;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Reset build definition display tool strip items - checked state.
        /// </summary>
        protected void ResetReleaseDefinitionDisplayToolStripItemCheckedState()
        {

            try
            {

                if (this.ReleaseDefinitionDisplayDevToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionDisplayDevToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionDisplayQAToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionDisplayQAToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionDisplayUATToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionDisplayUATToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionDisplayPerfToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionDisplayPerfToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionDisplayProductionToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionDisplayProductionToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionDisplayDevDRToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionDisplayDevDRToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionDisplayUATDRToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionDisplayUATDRToolStripMenuItem.Checked = false;
                }

                if (this.ReleaseDefinitionDisplayProdDRToolStripMenuItem.Checked == true)
                {
                    this.ReleaseDefinitionDisplayProdDRToolStripMenuItem.Checked = false;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Display all release definitions in build definition Release Definition List DataGridView.
        /// </summary>
        protected void ReleaseDefinitionDisplayAll()
        {
            this.ReleaseDefinitionListDataGridView.CurrentCell = null;

            foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
            {
                dataGridViewRow.Visible = true;
            }
        }

        /// <summary>
        /// Release definition check all.
        /// </summary>
        protected void ReleaseDefinitionCheckAll()
        {
            ReleaseDefinitionSelector releasedDefinitionSelector = null;

            try
            {
                this.Cursor = Cursors.WaitCursor;
                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {
                    releasedDefinitionSelector = (ReleaseDefinitionSelector)dataGridViewRow.DataBoundItem;
                    if (dataGridViewRow.Visible == true)
                    {
                        dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Check].Value = true;
                    }
                }
                this.RaiseReleaseDefinitionSelectionChangedEvent();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Release definition check none.
        /// </summary>
        protected void ReleaseDefinitionCheckNone()
        {
            this.Cursor = Cursors.WaitCursor;
            foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
            {
                dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Check].Value = false;
            }
            this.RaiseReleaseDefinitionSelectionChangedEvent();

        }

        /// <summary>
        /// Retrieve release definition 'Display' tag match list.
        /// </summary>
        /// <returns></returns>
        protected List<string> GetReleaseDefinitionDisplayTagMatchList()
        {
            List<string> result = null;

            try
            {
                result = new List<string>();

                //Non-production environments:
                if (this.ReleaseDefinitionDisplayDevToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_Dev);
                }

                if (this.ReleaseDefinitionDisplayQAToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_Qa);
                }

                if (this.ReleaseDefinitionDisplayUATToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_Uat);
                }

                if (this.ReleaseDefinitionDisplayPerfToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_Perf);
                }

                //Production environmnet:
                if (this.ReleaseDefinitionDisplayProductionToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_Production);
                }

                //Disaster recovery environments:
                if (this.ReleaseDefinitionDisplayDevDRToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_DevDR);
                }

                if (this.ReleaseDefinitionDisplayUATDRToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_UATDR);
                }

                if (this.ReleaseDefinitionDisplayProdDRToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_ProdDR);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Retrieve release definition 'check' tag match list.
        /// </summary>
        /// <returns></returns>
        protected List<string> GetReleaseDefinitionCheckTagMatchList()
        {
            List<string> result = null;

            try
            {
                result = new List<string>();

                //Non-production environments:
                if (this.ReleaseDefinitionCheckDevToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_Dev);
                }

                if (this.ReleaseDefinitionCheckQAToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_Qa);
                }

                if (this.ReleaseDefinitionCheckUATToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_Uat);
                }

                if (this.ReleaseDefinitionCheckPerfToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_Perf);
                }

                //Production environmnet:
                if (this.ReleaseDefinitionCheckProductionToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_Production);
                }

                //Disaster recovery environments:
                if (this.ReleaseDefinitionCheckDevDRToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_DevDR);
                }

                if (this.ReleaseDefinitionCheckUATDRToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_UATDR);
                }

                if (this.ReleaseDefinitionCheckProdDRToolStripMenuItem.Checked == true)
                {
                    result.Add(ReleaseDefinitionTag_ProdDR);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Retrieve checked release definition count.
        /// </summary>
        /// <returns></returns>
        protected int GetCheckedReleaseDefinitionCount()
        {
            int result = 0;
            ReleaseDefinitionSelector releaseDefinitionSelector = null;

            try
            {
                //Retrieve checked count.
                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {
                    if (dataGridViewRow.Visible == true)
                    {
                        releaseDefinitionSelector = (ReleaseDefinitionSelector)dataGridViewRow.DataBoundItem;
                        if (releaseDefinitionSelector.Selected == true)
                        {
                            ++result;
                        }
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Confirm operation on large number of release definitions.
        /// </summary>
        /// <param name="checkedBuildDefinitionCount"></param>
        /// <returns></returns>
        protected bool ConfirmOperationLargeReleaseDefinitionCount(int checkedBuildDefinitionCount)
        {
            bool result = false;
            DialogResult promptDialogResult;

            promptDialogResult = MessageBox.Show(string.Format(ConfirmOperation_LargeReleaseDefinitionCount_Message, checkedBuildDefinitionCount),
                                                 string.Format(ConfirmOperation_LargeReleaseDefinitionCount_Caption),
                                                 MessageBoxButtons.YesNoCancel,
                                                 System.Windows.Forms.MessageBoxIcon.Exclamation);
            if (promptDialogResult == DialogResult.Yes)
            {
                result = true;
            }

            return result;

        }

        #endregion

        #region Private methods

        /// <summary>
        /// Event Hanlder: Release definition search tool tip - Draw.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionSearchToolTip_Draw(object sender, DrawToolTipEventArgs e)
        {
            try
            {
                e.DrawBackground();
                e.DrawBorder();
                e.DrawText();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Search button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchButton_Click(object sender, EventArgs e)
        {

            try
            {

                this.Cursor = Cursors.WaitCursor;

                this.ResetReleaseDefinitionHighlightToolStripItemCheckedState();
                this.ResetReleaseDefinitionDisplayToolStripItemCheckedState();
                this.ResetReleaseDefinitionCheckToolStripItemCheckedState();

                this.PerformSearch();

                this.SearchButton.BackColor = DefaultBackColor;

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        /// <summary>
        /// Event Handler: Release definition list data grive view - cell formatting.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionListDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            try
            {
                if (this.ReleaseDefinitionListDataGridView.Columns[e.ColumnIndex].Name == ReleaseDefinitionDataGridViewColumn_Enabled && e.RowIndex >= 0 && e.RowIndex < this.ReleaseDefinitionListDataGridView.Rows.Count)
                {
                    if (this.ReleaseDefinitionListDataGridView.Rows[e.RowIndex].DataBoundItem != null)
                    {
                        ReleaseDefinitionSelector releaseDefinitionSelector = (ReleaseDefinitionSelector)this.ReleaseDefinitionListDataGridView.Rows[e.RowIndex].DataBoundItem;
                        if (releaseDefinitionSelector.Enabled == true)
                        {
                            e.Value = (System.Drawing.Image)Properties.Resources.QueueStatusEnabled;
                        }
                        else
                        {
                            e.Value = (System.Drawing.Image)Properties.Resources.QueueStatusDisabled;
                        }

                        //NOTE: Currently, VSTS Release Management Rest API does not support release definition enable property. Always show enabled = true.
                        e.Value = (System.Drawing.Image)Properties.Resources.QueueStatusEnabled;
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition filter cut tool strip menu item - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionNameFilterCutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.ReleaseDefinitionNameFilterTextBox.Cut();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition filter copy tool strip menu item - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionNameFilterCopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetText(this.ReleaseDefinitionNameFilterTextBox.SelectedText);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition filter paste tool strip menu item - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionNameFilterPasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Clipboard.ContainsText())
                {
                    this.ReleaseDefinitionNameFilterTextBox.Paste();
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition filter delete tool strip menu item - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionNameFilterDeleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.ReleaseDefinitionNameFilterTextBox.SelectionLength > 0)
                {
                    this.ReleaseDefinitionNameFilterTextBox.SelectedText = string.Empty;
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition filter select all tool strip menu item - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionNameFilterSelectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.ReleaseDefinitionNameFilterTextBox.Focus();
                this.ReleaseDefinitionNameFilterTextBox.SelectAll();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition view all  tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionViewContextMenuStrip_Click(object sender, EventArgs e)
        {
            try
            {
                this.ReleaseDefinitionListDataGridView.Columns[ReleaseDefinitionDataGridViewColumn_Comment].Visible = true;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Check selected release definition tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckSelectedReleaseDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ResetReleaseDefinitionCheckToolStripItemCheckedState();
                this.ReleaseDefinitionCheckNone();

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.SelectedRows)
                {
                    if (dataGridViewRow.Visible == true)
                    {
                        dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Check].Value = true;
                    }
                }
                this.RaiseReleaseDefinitionSelectionChangedEvent();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Copy release definition tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyReleaseDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReleaseDefinitionSelector releaseDefinitionSelector = null;
            StringBuilder releaseDefinitionListStringBuilder = null;
            int checkedReleaseDefinitionCount = 0;

            try
            {
                releaseDefinitionListStringBuilder = new StringBuilder();

                //Retrieve checked build definition count.
                checkedReleaseDefinitionCount = GetCheckedReleaseDefinitionCount();

                //No build definitions checked.
                if (checkedReleaseDefinitionCount == 0)
                {
                    this.ReleaseDefinitionSearchToolTip.Show(ContextMenuMessage_NoReleaseDefinitionsChecked, this.CheckSplitButton, 4000);
                    return;
                }

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {
                    if (dataGridViewRow.Visible == true)
                    {
                        releaseDefinitionSelector = (ReleaseDefinitionSelector)dataGridViewRow.DataBoundItem;
                        if (releaseDefinitionSelector.Selected == true)
                        {

                            releaseDefinitionListStringBuilder.AppendLine(string.Format("{0}",
                                                                                  releaseDefinitionSelector.Name));
                        }
                    }
                }

                Clipboard.SetDataObject(releaseDefinitionListStringBuilder.ToString());

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: View release definition tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewReleaseDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReleaseDefinitionSelector releaseDefinitionSelector = null;
            ProcessStartInfo processStartInfo = null;
            string releaseSummaryAddress = string.Empty;
            string teamProjectCollectionUri = string.Empty;
            int checkedReleaseDefinitionCount = 0;

            try
            {
                teamProjectCollectionUri = this.ReleaseDefinitionSearchPresenter.TeamProjectInfo.TeamProjectCollectionUri.ToString();
                if (teamProjectCollectionUri.EndsWith("/") == false)
                {
                    teamProjectCollectionUri += @"/";
                }

                //Retrieve checked build definition count.
                checkedReleaseDefinitionCount = GetCheckedReleaseDefinitionCount();

                //Prompt user to confirm when the checked release definition count is large.
                if (checkedReleaseDefinitionCount > ConfirmOperation_MaxReleaseDefinitionCount)
                {
                    if (ConfirmOperationLargeReleaseDefinitionCount(checkedReleaseDefinitionCount) == false)
                    {
                        return;
                    }
                }

                //No release definitions checked.
                if (checkedReleaseDefinitionCount == 0)
                {
                    this.ReleaseDefinitionSearchToolTip.Show(ContextMenuMessage_NoReleaseDefinitionsChecked, this.CheckSplitButton, 4000);
                    return;
                }

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {
                    if (dataGridViewRow.Visible == true)
                    {
                        releaseDefinitionSelector = (ReleaseDefinitionSelector)dataGridViewRow.DataBoundItem;
                        if (releaseDefinitionSelector.Selected == true)
                        {
                            releaseSummaryAddress = string.Format("{0}DefaultCollection/{1}/_release?definitionId={2}&_a=releases",
                                                            teamProjectCollectionUri,
                                                            this.ReleaseDefinitionSearchPresenter.TeamProjectInfo.TeamProjectName,
                                                            releaseDefinitionSelector.Id);

                            processStartInfo = new ProcessStartInfo(releaseSummaryAddress);
                            Process.Start(processStartInfo);
                        }
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Release definition list data grid view - cell value changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionListDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.ReleaseDefinitionListDataGridView.Columns[e.ColumnIndex].Name == ReleaseDefinitionDataGridViewColumn_Check && e.RowIndex >= 0 && e.RowIndex < ReleaseDefinitionListDataGridView.Rows.Count)
                {
                    ReleaseDefinitionSelector buildDefinitionSelector = (ReleaseDefinitionSelector)ReleaseDefinitionListDataGridView.Rows[e.RowIndex].DataBoundItem;

                    DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)this.ReleaseDefinitionListDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];

                    if ((bool)cell.Value == true)
                    {
                        buildDefinitionSelector.Selected = true;
                    }
                    else
                    {
                        buildDefinitionSelector.Selected = false;
                    }

                    if (this.BatchCheckInProgress == false)
                    {
                        this.RaiseReleaseDefinitionSelectionChangedEvent();
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition list data grive view - cell clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionListDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.ReleaseDefinitionListDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition list data grive view - cell content clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionListDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.ReleaseDefinitionListDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Refresh button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshButton_Click(object sender, EventArgs e)
        {

            try
            {

                if (this.ReleaseDefinitionSearchPresenter.IsBackgroundTaskRunning() == true)
                {
                    this.ReleaseDefinitionSearchPresenter.CancelBackgroundTask();
                    this.UpdateRefreshCancelMode(false);
                }
                else
                {
                    this.ReleaseDefinitionSelectorList = new ReleaseDefinitionSelectorList();
                    this.ReleaseDefinitionListDataGridView.DataSource = null;
                    this.RaiseReleaseDefinitionSelectionChangedEvent();
                    this.ReleaseDefinitionSearchPresenter.GetReleaseDefinitionSelectorListAsync(this.ReleaseDefinitionNamePattern, null, ReleaseDefinitionRetrievalMode.Reset);
                    this.RetrieveRemainingDefinitionsEnabled = true;
                    this.UpdateRefreshCancelMode(true);
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Event Handler: Clear button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearButton_Click(object sender, EventArgs e)
        {

            try
            {

                this.Cursor = Cursors.WaitCursor;

                if (this.ReleaseDefinitionSearchPresenter.IsBackgroundTaskRunning() == true)
                {
                    this.ReleaseDefinitionSearchPresenter.CancelBackgroundTask();
                    this.UpdateRefreshCancelMode(false);
                }

                this.ClearSearch();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        /// <summary>
        /// Event Handler: Release definitionlist data grid view - Sorted.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionListDataGridView_Sorted(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;

            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionDisplayTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    this.ReleaseDefinitionDisplayAll();
                }
                else
                {
                    this.ToggleDisplayReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Release definition list data grive view - cell double click.
        /// </summary>
        /// <remarks>
        /// Without this override double clicking on checkbox column can force checkbox value 
        /// and build definition selector [Selected] values be become out of sync. 
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionListDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.ReleaseDefinitionListDataGridView.Columns[e.ColumnIndex].Name == ReleaseDefinitionDataGridViewColumn_Check && 
                    e.RowIndex >= 0 && e.RowIndex < this.ReleaseDefinitionListDataGridView.Rows.Count)
                {
                    ReleaseDefinitionSelector buildDefinitionSelector = (ReleaseDefinitionSelector)this.ReleaseDefinitionListDataGridView.Rows[e.RowIndex].DataBoundItem;

                    DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)this.ReleaseDefinitionListDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];

                    if ((bool)cell.Value == true)
                    {
                        cell.EditingCellFormattedValue = false;
                    }
                    else
                    {
                        cell.EditingCellFormattedValue = true;
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release defintion check all tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionCheckAllToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.BatchCheckInProgress = true;

                this.ResetReleaseDefinitionCheckToolStripItemCheckedState();

                this.ReleaseDefinitionCheckAll();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.BatchCheckInProgress = false;
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release defintion check none tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionCheckNoneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                ResetReleaseDefinitionCheckToolStripItemCheckedState();

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {
                    dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Check].Value = false;
                }
                this.RaiseReleaseDefinitionSelectionChangedEvent();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition check disabled tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionCheckDisabledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReleaseDefinitionSelector releaseDefinitionSelector = null;
            bool checkChanged = false;

            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.BatchCheckInProgress = true;

                ResetReleaseDefinitionCheckToolStripItemCheckedState();

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {

                    releaseDefinitionSelector = (ReleaseDefinitionSelector)dataGridViewRow.DataBoundItem;
                    if (dataGridViewRow.Visible == true && releaseDefinitionSelector.Enabled == false)
                    {
                        dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Check].Value = true;
                        checkChanged = true;
                    }
                    else
                    {
                        dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Check].Value = false;
                        checkChanged = true;
                    }

                }

                if (checkChanged == true)
                {
                    RaiseReleaseDefinitionSelectionChangedEvent();
                }
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.BatchCheckInProgress = false;
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition check enabled tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionCheckEnabledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReleaseDefinitionSelector releaseDefinitionSelector = null;
            bool checkChanged = false;

            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.BatchCheckInProgress = true;

                ResetReleaseDefinitionCheckToolStripItemCheckedState();

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {

                    releaseDefinitionSelector = (ReleaseDefinitionSelector)dataGridViewRow.DataBoundItem;
                    if (dataGridViewRow.Visible == true && releaseDefinitionSelector.Enabled == true)
                    {
                        dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Check].Value = true;
                        checkChanged = true;
                    }
                    else
                    {
                        dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Check].Value = false;
                        checkChanged = true;
                    }

                }

                if (checkChanged == true)
                {
                    RaiseReleaseDefinitionSelectionChangedEvent();
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.BatchCheckInProgress = false;
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition check highlighted tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionCheckHighlightedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReleaseDefinitionSelector releaseDefinitionSelector = null;
            bool checkChanged = false;

            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.BatchCheckInProgress = true;

                ResetReleaseDefinitionCheckToolStripItemCheckedState();

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {

                    releaseDefinitionSelector = (ReleaseDefinitionSelector)dataGridViewRow.DataBoundItem;
                    if (dataGridViewRow.Visible == true && releaseDefinitionSelector.Highlighted == true)
                    {
                        dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Check].Value = true;
                        checkChanged = true;
                    }
                    else
                    {
                        dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Check].Value = false;
                        checkChanged = true;
                    }

                }
                if (checkChanged == true)
                {
                    RaiseReleaseDefinitionSelectionChangedEvent();
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.BatchCheckInProgress = false;
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition check dev tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionCheckDevToolStripMenuItem_CheckChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionCheckTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition check QA tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionCheckQAToolStripMenuItem_CheckChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionCheckTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition check UAT tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionCheckUATToolStripMenuItem_CheckChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionCheckTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition check Perf tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionCheckPerfToolStripMenuItem_CheckChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionCheckTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition check production tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionCheckProductionToolStripMenuItem_CheckChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionCheckTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition check dev dr tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionCheckDevDRToolStripMenuItem_CheckChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionCheckTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition check UAT dr tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionCheckUATDRToolStripMenuItem_CheckChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionCheckTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        /// <summary>
        /// Event Handler: Release definition check Production DR tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionCheckProdDRToolStripMenuItem_CheckChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionCheckTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release defintion display all tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionDisplayAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ResetReleaseDefinitionDisplayToolStripItemCheckedState();

                this.ReleaseDefinitionDisplayAll();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release defintion display checked tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionDisplayCheckedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool checkedValue = false;
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ReleaseDefinitionListDataGridView.CurrentCell = null;

                this.ResetReleaseDefinitionDisplayToolStripItemCheckedState();

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {
                    bool.TryParse(dataGridViewRow.Cells[ReleaseDefinitionDataGridViewColumn_Check].Value.ToString(),
                                  out checkedValue);

                    if (checkedValue == true)
                    {
                        dataGridViewRow.Visible = true;
                    }
                    else
                    {
                        dataGridViewRow.Visible = false;
                    };

                }
                this.RaiseReleaseDefinitionSelectionChangedEvent();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release defintion display disabled tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionDisplayDisabledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ResetReleaseDefinitionDisplayToolStripItemCheckedState();

                this.ReleaseDefinitionListDataGridView.CurrentCell = null;

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {
                    ReleaseDefinitionSelector releaseDefinitionSelector = (ReleaseDefinitionSelector)dataGridViewRow.DataBoundItem;


                    if (releaseDefinitionSelector.Enabled == false)
                    {
                        dataGridViewRow.Visible = true;
                    }
                    else
                    {
                        dataGridViewRow.Visible = false;
                    };

                }
                this.RaiseReleaseDefinitionSelectionChangedEvent();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release defintion display enabled tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionDisplayEnabledToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ResetReleaseDefinitionDisplayToolStripItemCheckedState();

                this.ReleaseDefinitionListDataGridView.CurrentCell = null;

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {
                    ReleaseDefinitionSelector releaseDefinitionSelector = (ReleaseDefinitionSelector)dataGridViewRow.DataBoundItem;


                    if (releaseDefinitionSelector.Enabled == true)
                    {
                        dataGridViewRow.Visible = true;
                    }
                    else
                    {
                        dataGridViewRow.Visible = false;
                    };

                }
                this.RaiseReleaseDefinitionSelectionChangedEvent();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release defintion display highlighted tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionDisplayHighlightedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReleaseDefinitionSelector releaseDefinitionSelector = null;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ResetReleaseDefinitionDisplayToolStripItemCheckedState();

                this.ReleaseDefinitionListDataGridView.CurrentCell = null;

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {

                    releaseDefinitionSelector = (ReleaseDefinitionSelector)ReleaseDefinitionListDataGridView.Rows[dataGridViewRow.Index].DataBoundItem;

                    if (releaseDefinitionSelector.Highlighted == true)
                    {
                        dataGridViewRow.Visible = true;
                    }
                    else
                    {
                        dataGridViewRow.Visible = false;
                        releaseDefinitionSelector.Selected = false;
                    };

                }
                this.RaiseReleaseDefinitionSelectionChangedEvent();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Release Definition Display Dev Tool Strip Item - Checked Changed.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionDisplayDevToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionDisplayTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Release Definition Display QA Tool Strip Item - Checked Changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionDisplayQAToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionDisplayTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Release Definition Display UAT Tool Strip Item - Checked Changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionDisplayUATToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionDisplayTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Release Definition Display Perf Tool Strip Item - Checked Changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionDisplayPerfToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionDisplayTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Release Definition Display Production Tool Strip Item - Checked Changed.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionDisplayProductionToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionDisplayTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Release Definition Display Dev DR Tool Strip Item - Checked Changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionDisplayDevDRToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionDisplayTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Release Definition Display UAT DR Tool Strip Item - Checked Changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionDisplayUATDRToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionDisplayTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Release Definition Display Prod DR Tool Strip Item - Checked Changed.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionDisplayProdDRToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> releaseDefinitionTagMatchList = null;
            try
            {
                releaseDefinitionTagMatchList = this.GetReleaseDefinitionDisplayTagMatchList();
                if (releaseDefinitionTagMatchList == null ||
                    releaseDefinitionTagMatchList.Any() == false)
                {
                    ReleaseDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayReleaseDefinitionsWithMatchingTag(releaseDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition highlight none tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionHighlightNoneToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                this.ResetReleaseDefinitionHighlightToolStripItemCheckedState();
                this.ResetReleaseDefinitionDisplayToolStripItemCheckedState();
                this.ResetReleaseDefinitionCheckToolStripItemCheckedState();


            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition highlight dev tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionHighlightDevToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightReleaseDefinitionsWithMatchingTag(this.ReleaseDefinitionHighlightDevToolStripMenuItem.Checked, ReleaseDefinitionTag_Dev);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition highlight QA tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionHighlightQAToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightReleaseDefinitionsWithMatchingTag(this.ReleaseDefinitionHighlightQAToolStripMenuItem.Checked, ReleaseDefinitionTag_Qa);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition highlight UAT tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionHighlightUATToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightReleaseDefinitionsWithMatchingTag(this.ReleaseDefinitionHighlightUATToolStripMenuItem.Checked, ReleaseDefinitionTag_Uat);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition highlight Perf tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionHighlightPerfToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightReleaseDefinitionsWithMatchingTag(this.ReleaseDefinitionHighlightPerfToolStripMenuItem.Checked, ReleaseDefinitionTag_Perf);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition highlight Production tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionHighlightProductionToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightReleaseDefinitionsWithMatchingTag(this.ReleaseDefinitionHighlightProductionToolStripMenuItem.Checked, ReleaseDefinitionTag_Production);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition highlight dev disaster recovery tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionHighlightDevDRToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightReleaseDefinitionsWithMatchingTag(this.ReleaseDefinitionHighlightProductionToolStripMenuItem.Checked, ReleaseDefinitionTag_Production);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition highlight uat disaster recovery tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionHighlightUATDRToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightReleaseDefinitionsWithMatchingTag(this.ReleaseDefinitionHighlightProductionToolStripMenuItem.Checked, ReleaseDefinitionTag_Production);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Release definition highlight prod disaster recovery tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReleaseDefinitionHighlightProdDRToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightReleaseDefinitionsWithMatchingTag(this.ReleaseDefinitionHighlightProductionToolStripMenuItem.Checked, ReleaseDefinitionTag_Production);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Copy button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyButton_Click(object sender, EventArgs e)
        {

            ReleaseDefinitionSelector buildDefinitionSelector = null;
            StringBuilder releaseDefinitionListStringBuilder = null;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                releaseDefinitionListStringBuilder = new StringBuilder();

                foreach (DataGridViewRow dataGridViewRow in this.ReleaseDefinitionListDataGridView.Rows)
                {
                    buildDefinitionSelector = (ReleaseDefinitionSelector)dataGridViewRow.DataBoundItem;

                    if (buildDefinitionSelector.Selected == true)
                    {
                        releaseDefinitionListStringBuilder.AppendLine(string.Format("{0}",
                                                                                  buildDefinitionSelector.Name));
                    }

                }

                Clipboard.SetDataObject(releaseDefinitionListStringBuilder.ToString());

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}
