﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using Aclara.Vso.Build.Client;
using Aclara.Vso.Build.Client.Events;
using Aclara.Vso.Build.Client.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public class ReleaseDefinitionClonePresenter
    {

        #region Private Constants

        private const string BackgroundTaskStatus_CancellingRequest = "Cancelling clone release definition request...";
        private const string BackgroundTaskStatus_Requested = "Clone release definition requested...";
        private const string BackgroundTaskStatus_Completed = "Clone release definition request canceled.";
        private const string BackgroundTaskStatus_Canceled = "Clone release definition request canceled.";

        private const string BackgroundTaskTotalDuration = "[*] Clone release definition request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})";

        private const string ApplyButtonText_Cancel = "Cancel";

        private const string Event_AAAEventAAA_CompletedWithStatus = "Clone release definition completed. (Status --> {1})";
        private const string Event_AAAEventAAA_Completed = "Clone release definition completed.";

        private const string BackgroundTaskException = "Operation cancelled.";
        private const string BackgroundTaskExceptionUnobserved = "Operation cancelled. (Exception unobserved)";

        private const string RequestFailed = "Request failed.";

        #endregion

        #region Private Data Members

        private CustomLogger _logger;
        private SidekickConfiguration _sidekickConfiguration = null;
        private IReleaseDefinitionCloneView _releaseDefintionView = null;
        private ITeamProjectInfo _teamProjectInfo = null;
        private ReleaseDefinitionManager _releasDefinitionManager = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.ReleaseDefinitionCloneView.SidekickName,
                                               this.ReleaseDefinitionCloneView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Release definition clone view.
        /// </summary>
        public IReleaseDefinitionCloneView ReleaseDefinitionCloneView
        {
            get { return _releaseDefintionView; }
            set { _releaseDefintionView = value; }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: AAATemplateAAA manager.
        /// </summary>
        public ReleaseDefinitionManager ReleaseDefinitionManager
        {
            get { return _releasDefinitionManager; }
            set { _releasDefinitionManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="releaseDefinitionCloneView"></param>
        public ReleaseDefinitionClonePresenter(ITeamProjectInfo teamProjectInfo,
                                             SidekickConfiguration sidekickConfiguration,
                                             IReleaseDefinitionCloneView releaseDefinitionCloneView)
        {
            this.TeamProjectInfo = teamProjectInfo;
            this.SidekickConfiguration = sidekickConfiguration;
            this.ReleaseDefinitionCloneView = releaseDefinitionCloneView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private ReleaseDefinitionClonePresenter()
        {
        }

        #endregion

        #region Public Methods

        public void CloneReleaseDefinitions(ReleaseDefinitionSelectorList releaseDefinitionSelectorList,
                                            string sourceReleaseDefinitionNamePrefix,
                                            string targetReleaseDefinitionNamePrefix,
                                            string sourceBranchName,
                                            string targetBranchName)
        {

            ReleaseDefinitionList filteredReleaseDefinitionList = null;
            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.ReleaseDefinitionCloneView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.ReleaseDefinitionCloneView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.ReleaseDefinitionCloneView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;
                    this.ReleaseDefinitionCloneView.ApplyButtonText = ApplyButtonText_Cancel;

                    this.ReleaseDefinitionManager = this.CreateReleaseDefinitionManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.ReleaseDefinitionManager.ReleaseDefinitionCloned += OnReleaseDefinitionCloned;

                    filteredReleaseDefinitionList = releaseDefinitionSelectorList.GetReleaseDefinitionList();

                    this.BackgroundTask = new Task(() => this.ReleaseDefinitionManager.CloneReleaseDefinitions(filteredReleaseDefinitionList,
                                                                                                               sourceReleaseDefinitionNamePrefix,
                                                                                                               targetReleaseDefinitionNamePrefix,
                                                                                                               sourceBranchName,
                                                                                                               targetBranchName, 
                                                                                                               cancellationToken),
                                                                                                               cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalDuration;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.ReleaseDefinitionCloneView.BackgroundTaskCompleted(BackgroundTaskStatus_Completed);
                        this.ReleaseDefinitionCloneView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format(BackgroundTaskStatus_Canceled));

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.ReleaseDefinitionCloneView.BackgroundTaskCompleted(string.Empty);
                        this.ReleaseDefinitionCloneView.ApplyButtonEnable(true);

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(BackgroundTaskException));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(BackgroundTaskException));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(RequestFailed));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(RequestFailed));
                                    }

                                }
                            }
                        }

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.ReleaseDefinitionCloneView.BackgroundTaskCompleted(Event_AAAEventAAA_Completed);
                        this.ReleaseDefinitionCloneView.ApplyButtonEnable(true);

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create AAATemplateAAA manager.
        /// </summary>
        /// <returns></returns>
        protected ReleaseDefinitionManager CreateReleaseDefinitionManager()
        {
            ReleaseDefinitionManager result = null;

            try
            {
                result = ReleaseDefinitionManagerFactory.CreateReleaseDefinitionManager(this.TeamProjectInfo.TeamProjectCollectionUri,
                                                                                        this.TeamProjectInfo.TeamProjectCollectionVsrmUri,
                                                                                        this.TeamProjectInfo.TeamProjectName,
                                                                                        this.TeamProjectInfo.BasicAuthRestAPIUserProfileName,
                                                                                        this.TeamProjectInfo.BasicAuthRestAPIPassword);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: Clone release definition completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="cloneReleaseDefinitionEventArgs"></param>
        protected void OnReleaseDefinitionCloned(Object sender,
                                                 CloneReleaseDefinitionEventArgs cloneReleaseDefinitionEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Log event completed with status.
                if (cloneReleaseDefinitionEventArgs.StatusList != null &&
                    cloneReleaseDefinitionEventArgs.StatusList.Count > 0)
                {
                    message = string.Format(Event_AAAEventAAA_CompletedWithStatus,
                                            cloneReleaseDefinitionEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (cloneReleaseDefinitionEventArgs.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                }
                //Log event completed without status.
                else
                {
                    message = string.Format(Event_AAAEventAAA_Completed);
                    this.Logger.Info(message);
                }

                this.ReleaseDefinitionCloneView.UpdateProgressCloneReleaseDefinition(cloneReleaseDefinitionEventArgs.ReleaseDefinitionClonedCount, 
                                                                                     cloneReleaseDefinitionEventArgs.ReleaseDefinitionTotalCount);
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                        }
                        else
                        {
                            Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                }
                else
                {
                    Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
