﻿namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    partial class BuildDefinitionRequestBuildForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.VerticalSplitter = new System.Windows.Forms.Splitter();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.BuildDefinitionListGroupBox = new System.Windows.Forms.GroupBox();
            this.BuildDefinitionSelectionStatusLabel = new System.Windows.Forms.Label();
            this.BuildDefinitionSelectorListBox = new System.Windows.Forms.ListBox();
            this.BuildNotificationGroupBox = new System.Windows.Forms.GroupBox();
            this.CopySplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.CopyContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CopyAsTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyAsHTMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NewEmailSplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.NewEmailContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.NewEmailAsTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NewEmailAsHTMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EnvironmentGroupBox = new System.Windows.Forms.GroupBox();
            this.EnvironmentPerfCheckBox = new System.Windows.Forms.CheckBox();
            this.EnvironmentProdCheckBox = new System.Windows.Forms.CheckBox();
            this.EnvironmentUatCheckBox = new System.Windows.Forms.CheckBox();
            this.EnvironmentQaCheckBox = new System.Windows.Forms.CheckBox();
            this.EnvironmentDevCheckBox = new System.Windows.Forms.CheckBox();
            this.DREnvironmentGroupBox = new System.Windows.Forms.GroupBox();
            this.EnvironmentProdDrCheckBox = new System.Windows.Forms.CheckBox();
            this.EnvironmentUatDrCheckBox = new System.Windows.Forms.CheckBox();
            this.EnvironmentDevDrCheckBox = new System.Windows.Forms.CheckBox();
            this.PreviewButton = new System.Windows.Forms.Button();
            this.ParametersGroupBox = new System.Windows.Forms.GroupBox();
            this.VerbosityComboBox = new System.Windows.Forms.ComboBox();
            this.VerbostityLabel = new System.Windows.Forms.Label();
            this.ClearParametersButton = new System.Windows.Forms.Button();
            this.ParametersTextBox = new System.Windows.Forms.TextBox();
            this.OptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.OverrideQueueStatusCheckBox = new System.Windows.Forms.CheckBox();
            this.QueuePriorityComboBox = new System.Windows.Forms.ComboBox();
            this.QueuePriorityLabel = new System.Windows.Forms.Label();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.HelpButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.QuickAccessControlPanel = new System.Windows.Forms.Panel();
            this.RequestBuildProgressBar = new System.Windows.Forms.ProgressBar();
            this.CheckBuildStatusQuickAccessSplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.CheckBuildStatusContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CheckBuildStatusSwitchSidekickToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CheckBuildStatusNewWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OverrideQueueStatusQuickAccessCheckBox = new System.Windows.Forms.CheckBox();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.BuildDefinitionSearchPanel = new System.Windows.Forms.Panel();
            this.BuildDefinitionSearchPlaceholderLabel = new System.Windows.Forms.Label();
            this.RequestBuildToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.PropertiesPanel.SuspendLayout();
            this.BuildDefinitionListGroupBox.SuspendLayout();
            this.BuildNotificationGroupBox.SuspendLayout();
            this.CopyContextMenuStrip.SuspendLayout();
            this.NewEmailContextMenuStrip.SuspendLayout();
            this.EnvironmentGroupBox.SuspendLayout();
            this.DREnvironmentGroupBox.SuspendLayout();
            this.ParametersGroupBox.SuspendLayout();
            this.OptionsGroupBox.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.QuickAccessControlPanel.SuspendLayout();
            this.CheckBuildStatusContextMenuStrip.SuspendLayout();
            this.BuildDefinitionSearchPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // VerticalSplitter
            // 
            this.VerticalSplitter.BackColor = System.Drawing.SystemColors.ControlDark;
            this.VerticalSplitter.Location = new System.Drawing.Point(650, 0);
            this.VerticalSplitter.Name = "VerticalSplitter";
            this.VerticalSplitter.Size = new System.Drawing.Size(6, 517);
            this.VerticalSplitter.TabIndex = 1;
            this.VerticalSplitter.TabStop = false;
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Controls.Add(this.BuildDefinitionListGroupBox);
            this.PropertiesPanel.Controls.Add(this.BuildNotificationGroupBox);
            this.PropertiesPanel.Controls.Add(this.ParametersGroupBox);
            this.PropertiesPanel.Controls.Add(this.OptionsGroupBox);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesPanel.Location = new System.Drawing.Point(650, 0);
            this.PropertiesPanel.MinimumSize = new System.Drawing.Size(560, 480);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(566, 517);
            this.PropertiesPanel.TabIndex = 2;
            // 
            // BuildDefinitionListGroupBox
            // 
            this.BuildDefinitionListGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildDefinitionListGroupBox.Controls.Add(this.BuildDefinitionSelectionStatusLabel);
            this.BuildDefinitionListGroupBox.Controls.Add(this.BuildDefinitionSelectorListBox);
            this.BuildDefinitionListGroupBox.Location = new System.Drawing.Point(10, 295);
            this.BuildDefinitionListGroupBox.Name = "BuildDefinitionListGroupBox";
            this.BuildDefinitionListGroupBox.Size = new System.Drawing.Size(546, 176);
            this.BuildDefinitionListGroupBox.TabIndex = 4;
            this.BuildDefinitionListGroupBox.TabStop = false;
            this.BuildDefinitionListGroupBox.Text = "Build definitions";
            // 
            // BuildDefinitionSelectionStatusLabel
            // 
            this.BuildDefinitionSelectionStatusLabel.AutoSize = true;
            this.BuildDefinitionSelectionStatusLabel.Location = new System.Drawing.Point(8, 23);
            this.BuildDefinitionSelectionStatusLabel.Name = "BuildDefinitionSelectionStatusLabel";
            this.BuildDefinitionSelectionStatusLabel.Size = new System.Drawing.Size(180, 13);
            this.BuildDefinitionSelectionStatusLabel.TabIndex = 0;
            this.BuildDefinitionSelectionStatusLabel.Text = "Build definition(s) selected for queue:";
            // 
            // BuildDefinitionSelectorListBox
            // 
            this.BuildDefinitionSelectorListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildDefinitionSelectorListBox.FormattingEnabled = true;
            this.BuildDefinitionSelectorListBox.Location = new System.Drawing.Point(8, 39);
            this.BuildDefinitionSelectorListBox.Name = "BuildDefinitionSelectorListBox";
            this.BuildDefinitionSelectorListBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.BuildDefinitionSelectorListBox.Size = new System.Drawing.Size(524, 121);
            this.BuildDefinitionSelectorListBox.TabIndex = 1;
            // 
            // BuildNotificationGroupBox
            // 
            this.BuildNotificationGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildNotificationGroupBox.Controls.Add(this.CopySplitButton);
            this.BuildNotificationGroupBox.Controls.Add(this.NewEmailSplitButton);
            this.BuildNotificationGroupBox.Controls.Add(this.EnvironmentGroupBox);
            this.BuildNotificationGroupBox.Controls.Add(this.DREnvironmentGroupBox);
            this.BuildNotificationGroupBox.Controls.Add(this.PreviewButton);
            this.BuildNotificationGroupBox.Location = new System.Drawing.Point(10, 167);
            this.BuildNotificationGroupBox.Name = "BuildNotificationGroupBox";
            this.BuildNotificationGroupBox.Size = new System.Drawing.Size(546, 121);
            this.BuildNotificationGroupBox.TabIndex = 3;
            this.BuildNotificationGroupBox.TabStop = false;
            this.BuildNotificationGroupBox.Text = "Build notification";
            // 
            // CopySplitButton
            // 
            this.CopySplitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CopySplitButton.AutoSize = true;
            this.CopySplitButton.ContextMenuStrip = this.CopyContextMenuStrip;
            this.CopySplitButton.Location = new System.Drawing.Point(463, 78);
            this.CopySplitButton.Name = "CopySplitButton";
            this.CopySplitButton.Size = new System.Drawing.Size(75, 23);
            this.CopySplitButton.TabIndex = 3;
            this.CopySplitButton.Text = "Copy";
            this.RequestBuildToolTip.SetToolTip(this.CopySplitButton, "Copy build notification to clipboard");
            this.CopySplitButton.UseVisualStyleBackColor = true;
            // 
            // CopyContextMenuStrip
            // 
            this.CopyContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyAsTextToolStripMenuItem,
            this.CopyAsHTMLToolStripMenuItem});
            this.CopyContextMenuStrip.Name = "CopyContextMenuStrip";
            this.CopyContextMenuStrip.Size = new System.Drawing.Size(108, 48);
            // 
            // CopyAsTextToolStripMenuItem
            // 
            this.CopyAsTextToolStripMenuItem.Name = "CopyAsTextToolStripMenuItem";
            this.CopyAsTextToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.CopyAsTextToolStripMenuItem.Text = "Text";
            this.CopyAsTextToolStripMenuItem.Click += new System.EventHandler(this.CopyAsTextToolStripMenuItem_Click);
            // 
            // CopyAsHTMLToolStripMenuItem
            // 
            this.CopyAsHTMLToolStripMenuItem.Name = "CopyAsHTMLToolStripMenuItem";
            this.CopyAsHTMLToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.CopyAsHTMLToolStripMenuItem.Text = "HTML";
            this.CopyAsHTMLToolStripMenuItem.Click += new System.EventHandler(this.CopyAsHTMLToolStripMenuItem_Click);
            // 
            // NewEmailSplitButton
            // 
            this.NewEmailSplitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NewEmailSplitButton.AutoSize = true;
            this.NewEmailSplitButton.ContextMenuStrip = this.NewEmailContextMenuStrip;
            this.NewEmailSplitButton.Location = new System.Drawing.Point(463, 48);
            this.NewEmailSplitButton.Name = "NewEmailSplitButton";
            this.NewEmailSplitButton.Size = new System.Drawing.Size(75, 23);
            this.NewEmailSplitButton.TabIndex = 2;
            this.NewEmailSplitButton.Text = "New Email";
            this.RequestBuildToolTip.SetToolTip(this.NewEmailSplitButton, "New build notification email");
            this.NewEmailSplitButton.UseVisualStyleBackColor = true;
            // 
            // NewEmailContextMenuStrip
            // 
            this.NewEmailContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewEmailAsTextToolStripMenuItem,
            this.NewEmailAsHTMLToolStripMenuItem});
            this.NewEmailContextMenuStrip.Name = "NewEmailContextMenuStrip";
            this.NewEmailContextMenuStrip.Size = new System.Drawing.Size(108, 48);
            // 
            // NewEmailAsTextToolStripMenuItem
            // 
            this.NewEmailAsTextToolStripMenuItem.Name = "NewEmailAsTextToolStripMenuItem";
            this.NewEmailAsTextToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.NewEmailAsTextToolStripMenuItem.Text = "Text";
            this.NewEmailAsTextToolStripMenuItem.Click += new System.EventHandler(this.NewEmailAsTextToolStripMenuItem_Click);
            // 
            // NewEmailAsHTMLToolStripMenuItem
            // 
            this.NewEmailAsHTMLToolStripMenuItem.Name = "NewEmailAsHTMLToolStripMenuItem";
            this.NewEmailAsHTMLToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.NewEmailAsHTMLToolStripMenuItem.Text = "HTML";
            this.NewEmailAsHTMLToolStripMenuItem.Click += new System.EventHandler(this.NewEmailAsHTMLToolStripMenuItem_Click);
            // 
            // EnvironmentGroupBox
            // 
            this.EnvironmentGroupBox.Controls.Add(this.EnvironmentPerfCheckBox);
            this.EnvironmentGroupBox.Controls.Add(this.EnvironmentProdCheckBox);
            this.EnvironmentGroupBox.Controls.Add(this.EnvironmentUatCheckBox);
            this.EnvironmentGroupBox.Controls.Add(this.EnvironmentQaCheckBox);
            this.EnvironmentGroupBox.Controls.Add(this.EnvironmentDevCheckBox);
            this.EnvironmentGroupBox.Location = new System.Drawing.Point(8, 20);
            this.EnvironmentGroupBox.Name = "EnvironmentGroupBox";
            this.EnvironmentGroupBox.Size = new System.Drawing.Size(259, 42);
            this.EnvironmentGroupBox.TabIndex = 0;
            this.EnvironmentGroupBox.TabStop = false;
            this.EnvironmentGroupBox.Text = "Environments";
            // 
            // EnvironmentPerfCheckBox
            // 
            this.EnvironmentPerfCheckBox.AutoSize = true;
            this.EnvironmentPerfCheckBox.Location = new System.Drawing.Point(159, 19);
            this.EnvironmentPerfCheckBox.Name = "EnvironmentPerfCheckBox";
            this.EnvironmentPerfCheckBox.Size = new System.Drawing.Size(45, 17);
            this.EnvironmentPerfCheckBox.TabIndex = 3;
            this.EnvironmentPerfCheckBox.Text = "Perf";
            this.RequestBuildToolTip.SetToolTip(this.EnvironmentPerfCheckBox, "Performance environment");
            this.EnvironmentPerfCheckBox.UseVisualStyleBackColor = true;
            this.EnvironmentPerfCheckBox.CheckedChanged += new System.EventHandler(this.EnvironmentPerfCheckBox_CheckedChanged);
            // 
            // EnvironmentProdCheckBox
            // 
            this.EnvironmentProdCheckBox.AutoSize = true;
            this.EnvironmentProdCheckBox.Location = new System.Drawing.Point(206, 19);
            this.EnvironmentProdCheckBox.Name = "EnvironmentProdCheckBox";
            this.EnvironmentProdCheckBox.Size = new System.Drawing.Size(48, 17);
            this.EnvironmentProdCheckBox.TabIndex = 4;
            this.EnvironmentProdCheckBox.Text = "Prod";
            this.RequestBuildToolTip.SetToolTip(this.EnvironmentProdCheckBox, "Production environment");
            this.EnvironmentProdCheckBox.UseVisualStyleBackColor = true;
            this.EnvironmentProdCheckBox.CheckedChanged += new System.EventHandler(this.EnvironmentProdCheckBox_CheckedChanged);
            // 
            // EnvironmentUatCheckBox
            // 
            this.EnvironmentUatCheckBox.AutoSize = true;
            this.EnvironmentUatCheckBox.Location = new System.Drawing.Point(105, 20);
            this.EnvironmentUatCheckBox.Name = "EnvironmentUatCheckBox";
            this.EnvironmentUatCheckBox.Size = new System.Drawing.Size(48, 17);
            this.EnvironmentUatCheckBox.TabIndex = 2;
            this.EnvironmentUatCheckBox.Text = "UAT";
            this.RequestBuildToolTip.SetToolTip(this.EnvironmentUatCheckBox, "User acceptance testing environment");
            this.EnvironmentUatCheckBox.UseVisualStyleBackColor = true;
            this.EnvironmentUatCheckBox.CheckedChanged += new System.EventHandler(this.EnvironmentUatCheckBox_CheckedChanged);
            // 
            // EnvironmentQaCheckBox
            // 
            this.EnvironmentQaCheckBox.AutoSize = true;
            this.EnvironmentQaCheckBox.Location = new System.Drawing.Point(58, 20);
            this.EnvironmentQaCheckBox.Name = "EnvironmentQaCheckBox";
            this.EnvironmentQaCheckBox.Size = new System.Drawing.Size(41, 17);
            this.EnvironmentQaCheckBox.TabIndex = 1;
            this.EnvironmentQaCheckBox.Text = "QA";
            this.RequestBuildToolTip.SetToolTip(this.EnvironmentQaCheckBox, "Quality assurance environment");
            this.EnvironmentQaCheckBox.UseVisualStyleBackColor = true;
            this.EnvironmentQaCheckBox.CheckedChanged += new System.EventHandler(this.EnvironmentQaCheckBox_CheckedChanged);
            // 
            // EnvironmentDevCheckBox
            // 
            this.EnvironmentDevCheckBox.AutoSize = true;
            this.EnvironmentDevCheckBox.Location = new System.Drawing.Point(6, 19);
            this.EnvironmentDevCheckBox.Name = "EnvironmentDevCheckBox";
            this.EnvironmentDevCheckBox.Size = new System.Drawing.Size(46, 17);
            this.EnvironmentDevCheckBox.TabIndex = 0;
            this.EnvironmentDevCheckBox.Text = "Dev";
            this.RequestBuildToolTip.SetToolTip(this.EnvironmentDevCheckBox, "Development environment");
            this.EnvironmentDevCheckBox.UseVisualStyleBackColor = true;
            this.EnvironmentDevCheckBox.CheckedChanged += new System.EventHandler(this.EnvironmentDevCheckBox_CheckedChanged);
            // 
            // DREnvironmentGroupBox
            // 
            this.DREnvironmentGroupBox.Controls.Add(this.EnvironmentProdDrCheckBox);
            this.DREnvironmentGroupBox.Controls.Add(this.EnvironmentUatDrCheckBox);
            this.DREnvironmentGroupBox.Controls.Add(this.EnvironmentDevDrCheckBox);
            this.DREnvironmentGroupBox.Location = new System.Drawing.Point(8, 68);
            this.DREnvironmentGroupBox.Name = "DREnvironmentGroupBox";
            this.DREnvironmentGroupBox.Size = new System.Drawing.Size(259, 47);
            this.DREnvironmentGroupBox.TabIndex = 4;
            this.DREnvironmentGroupBox.TabStop = false;
            this.DREnvironmentGroupBox.Text = "Disaster recovery environments";
            // 
            // EnvironmentProdDrCheckBox
            // 
            this.EnvironmentProdDrCheckBox.AutoSize = true;
            this.EnvironmentProdDrCheckBox.Location = new System.Drawing.Point(147, 21);
            this.EnvironmentProdDrCheckBox.Name = "EnvironmentProdDrCheckBox";
            this.EnvironmentProdDrCheckBox.Size = new System.Drawing.Size(67, 17);
            this.EnvironmentProdDrCheckBox.TabIndex = 2;
            this.EnvironmentProdDrCheckBox.Text = "Prod DR";
            this.EnvironmentProdDrCheckBox.UseVisualStyleBackColor = true;
            this.EnvironmentProdDrCheckBox.CheckedChanged += new System.EventHandler(this.EnvironmentProdDrCheckBox_CheckedChanged);
            // 
            // EnvironmentUatDrCheckBox
            // 
            this.EnvironmentUatDrCheckBox.AutoSize = true;
            this.EnvironmentUatDrCheckBox.Location = new System.Drawing.Point(74, 21);
            this.EnvironmentUatDrCheckBox.Name = "EnvironmentUatDrCheckBox";
            this.EnvironmentUatDrCheckBox.Size = new System.Drawing.Size(67, 17);
            this.EnvironmentUatDrCheckBox.TabIndex = 1;
            this.EnvironmentUatDrCheckBox.Text = "UAT DR";
            this.EnvironmentUatDrCheckBox.UseVisualStyleBackColor = true;
            this.EnvironmentUatDrCheckBox.CheckedChanged += new System.EventHandler(this.EnvironmentUatDrCheckBox_CheckedChanged);
            // 
            // EnvironmentDevDrCheckBox
            // 
            this.EnvironmentDevDrCheckBox.AutoSize = true;
            this.EnvironmentDevDrCheckBox.Location = new System.Drawing.Point(6, 21);
            this.EnvironmentDevDrCheckBox.Name = "EnvironmentDevDrCheckBox";
            this.EnvironmentDevDrCheckBox.Size = new System.Drawing.Size(65, 17);
            this.EnvironmentDevDrCheckBox.TabIndex = 0;
            this.EnvironmentDevDrCheckBox.Text = "Dev DR";
            this.RequestBuildToolTip.SetToolTip(this.EnvironmentDevDrCheckBox, "Disaster recovery environment");
            this.EnvironmentDevDrCheckBox.UseVisualStyleBackColor = true;
            this.EnvironmentDevDrCheckBox.CheckedChanged += new System.EventHandler(this.EnvironmentDevDrCheckBox_CheckedChanged_1);
            // 
            // PreviewButton
            // 
            this.PreviewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PreviewButton.Location = new System.Drawing.Point(463, 19);
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(75, 23);
            this.PreviewButton.TabIndex = 1;
            this.PreviewButton.Text = "Preview";
            this.RequestBuildToolTip.SetToolTip(this.PreviewButton, "Preview build notification text");
            this.PreviewButton.UseVisualStyleBackColor = true;
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // ParametersGroupBox
            // 
            this.ParametersGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ParametersGroupBox.Controls.Add(this.VerbosityComboBox);
            this.ParametersGroupBox.Controls.Add(this.VerbostityLabel);
            this.ParametersGroupBox.Controls.Add(this.ClearParametersButton);
            this.ParametersGroupBox.Controls.Add(this.ParametersTextBox);
            this.ParametersGroupBox.Location = new System.Drawing.Point(9, 31);
            this.ParametersGroupBox.Name = "ParametersGroupBox";
            this.ParametersGroupBox.Size = new System.Drawing.Size(547, 79);
            this.ParametersGroupBox.TabIndex = 1;
            this.ParametersGroupBox.TabStop = false;
            this.ParametersGroupBox.Text = "Parameters";
            // 
            // VerbosityComboBox
            // 
            this.VerbosityComboBox.FormattingEnabled = true;
            this.VerbosityComboBox.Location = new System.Drawing.Point(87, 45);
            this.VerbosityComboBox.Name = "VerbosityComboBox";
            this.VerbosityComboBox.Size = new System.Drawing.Size(121, 21);
            this.VerbosityComboBox.TabIndex = 3;
            this.VerbosityComboBox.SelectedIndexChanged += new System.EventHandler(this.VerbosityComboBox_SelectedIndexChanged);
            // 
            // VerbostityLabel
            // 
            this.VerbostityLabel.AutoSize = true;
            this.VerbostityLabel.Location = new System.Drawing.Point(6, 48);
            this.VerbostityLabel.Name = "VerbostityLabel";
            this.VerbostityLabel.Size = new System.Drawing.Size(53, 13);
            this.VerbostityLabel.TabIndex = 2;
            this.VerbostityLabel.Text = "Verbosity:";
            // 
            // ClearParametersButton
            // 
            this.ClearParametersButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ClearParametersButton.Location = new System.Drawing.Point(463, 19);
            this.ClearParametersButton.Name = "ClearParametersButton";
            this.ClearParametersButton.Size = new System.Drawing.Size(75, 23);
            this.ClearParametersButton.TabIndex = 1;
            this.ClearParametersButton.Text = "Clear";
            this.RequestBuildToolTip.SetToolTip(this.ClearParametersButton, "Clear parameters");
            this.ClearParametersButton.UseVisualStyleBackColor = true;
            this.ClearParametersButton.Click += new System.EventHandler(this.ClearParametersButton_Click);
            // 
            // ParametersTextBox
            // 
            this.ParametersTextBox.Location = new System.Drawing.Point(6, 19);
            this.ParametersTextBox.Name = "ParametersTextBox";
            this.ParametersTextBox.Size = new System.Drawing.Size(446, 20);
            this.ParametersTextBox.TabIndex = 0;
            // 
            // OptionsGroupBox
            // 
            this.OptionsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OptionsGroupBox.Controls.Add(this.OverrideQueueStatusCheckBox);
            this.OptionsGroupBox.Controls.Add(this.QueuePriorityComboBox);
            this.OptionsGroupBox.Controls.Add(this.QueuePriorityLabel);
            this.OptionsGroupBox.Location = new System.Drawing.Point(9, 115);
            this.OptionsGroupBox.Name = "OptionsGroupBox";
            this.OptionsGroupBox.Size = new System.Drawing.Size(547, 46);
            this.OptionsGroupBox.TabIndex = 2;
            this.OptionsGroupBox.TabStop = false;
            this.OptionsGroupBox.Text = "Options";
            // 
            // OverrideQueueStatusCheckBox
            // 
            this.OverrideQueueStatusCheckBox.AutoSize = true;
            this.OverrideQueueStatusCheckBox.Location = new System.Drawing.Point(225, 19);
            this.OverrideQueueStatusCheckBox.Name = "OverrideQueueStatusCheckBox";
            this.OverrideQueueStatusCheckBox.Size = new System.Drawing.Size(130, 17);
            this.OverrideQueueStatusCheckBox.TabIndex = 2;
            this.OverrideQueueStatusCheckBox.Text = "Override queue status";
            this.OverrideQueueStatusCheckBox.UseVisualStyleBackColor = true;
            this.OverrideQueueStatusCheckBox.CheckedChanged += new System.EventHandler(this.OverrideQueueStatusCheckBox_CheckedChanged);
            // 
            // QueuePriorityComboBox
            // 
            this.QueuePriorityComboBox.FormattingEnabled = true;
            this.QueuePriorityComboBox.Location = new System.Drawing.Point(87, 17);
            this.QueuePriorityComboBox.Name = "QueuePriorityComboBox";
            this.QueuePriorityComboBox.Size = new System.Drawing.Size(121, 21);
            this.QueuePriorityComboBox.TabIndex = 1;
            // 
            // QueuePriorityLabel
            // 
            this.QueuePriorityLabel.AutoSize = true;
            this.QueuePriorityLabel.Location = new System.Drawing.Point(6, 20);
            this.QueuePriorityLabel.Name = "QueuePriorityLabel";
            this.QueuePriorityLabel.Size = new System.Drawing.Size(75, 13);
            this.QueuePriorityLabel.TabIndex = 0;
            this.QueuePriorityLabel.Text = "Queue priority:";
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.HelpButton);
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(566, 25);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(538, 0);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 2;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(6, 3);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(523, 20);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "Request Build";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ControlPanel
            // 
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.QuickAccessControlPanel);
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Controls.Add(this.ApplyButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 477);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(566, 40);
            this.ControlPanel.TabIndex = 5;
            // 
            // QuickAccessControlPanel
            // 
            this.QuickAccessControlPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.QuickAccessControlPanel.Controls.Add(this.RequestBuildProgressBar);
            this.QuickAccessControlPanel.Controls.Add(this.CheckBuildStatusQuickAccessSplitButton);
            this.QuickAccessControlPanel.Controls.Add(this.OverrideQueueStatusQuickAccessCheckBox);
            this.QuickAccessControlPanel.Location = new System.Drawing.Point(138, 6);
            this.QuickAccessControlPanel.Name = "QuickAccessControlPanel";
            this.QuickAccessControlPanel.Size = new System.Drawing.Size(335, 31);
            this.QuickAccessControlPanel.TabIndex = 1;
            // 
            // RequestBuildProgressBar
            // 
            this.RequestBuildProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RequestBuildProgressBar.Location = new System.Drawing.Point(253, 11);
            this.RequestBuildProgressBar.Name = "RequestBuildProgressBar";
            this.RequestBuildProgressBar.Size = new System.Drawing.Size(75, 10);
            this.RequestBuildProgressBar.TabIndex = 3;
            // 
            // CheckBuildStatusQuickAccessSplitButton
            // 
            this.CheckBuildStatusQuickAccessSplitButton.AutoSize = true;
            this.CheckBuildStatusQuickAccessSplitButton.ContextMenuStrip = this.CheckBuildStatusContextMenuStrip;
            this.CheckBuildStatusQuickAccessSplitButton.Location = new System.Drawing.Point(3, 5);
            this.CheckBuildStatusQuickAccessSplitButton.Name = "CheckBuildStatusQuickAccessSplitButton";
            this.CheckBuildStatusQuickAccessSplitButton.Size = new System.Drawing.Size(113, 23);
            this.CheckBuildStatusQuickAccessSplitButton.TabIndex = 0;
            this.CheckBuildStatusQuickAccessSplitButton.Text = "Check Build Status";
            this.CheckBuildStatusQuickAccessSplitButton.UseVisualStyleBackColor = true;
            // 
            // CheckBuildStatusContextMenuStrip
            // 
            this.CheckBuildStatusContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CheckBuildStatusSwitchSidekickToolStripMenuItem,
            this.CheckBuildStatusNewWindowToolStripMenuItem});
            this.CheckBuildStatusContextMenuStrip.Name = "CheckBuildStatusContextMenuStrip";
            this.CheckBuildStatusContextMenuStrip.Size = new System.Drawing.Size(156, 48);
            // 
            // CheckBuildStatusSwitchSidekickToolStripMenuItem
            // 
            this.CheckBuildStatusSwitchSidekickToolStripMenuItem.Name = "CheckBuildStatusSwitchSidekickToolStripMenuItem";
            this.CheckBuildStatusSwitchSidekickToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.CheckBuildStatusSwitchSidekickToolStripMenuItem.Text = "Switch Sidekick";
            this.CheckBuildStatusSwitchSidekickToolStripMenuItem.Click += new System.EventHandler(this.CheckBuildStatusSwitchSidekickToolStripMenuItem_Click);
            // 
            // CheckBuildStatusNewWindowToolStripMenuItem
            // 
            this.CheckBuildStatusNewWindowToolStripMenuItem.Name = "CheckBuildStatusNewWindowToolStripMenuItem";
            this.CheckBuildStatusNewWindowToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.CheckBuildStatusNewWindowToolStripMenuItem.Text = "New Window";
            this.CheckBuildStatusNewWindowToolStripMenuItem.Click += new System.EventHandler(this.CheckBuildStatusNewWindowToolStripMenuItem_Click);
            // 
            // OverrideQueueStatusQuickAccessCheckBox
            // 
            this.OverrideQueueStatusQuickAccessCheckBox.AutoSize = true;
            this.OverrideQueueStatusQuickAccessCheckBox.ForeColor = System.Drawing.Color.Gold;
            this.OverrideQueueStatusQuickAccessCheckBox.Location = new System.Drawing.Point(122, 8);
            this.OverrideQueueStatusQuickAccessCheckBox.Name = "OverrideQueueStatusQuickAccessCheckBox";
            this.OverrideQueueStatusQuickAccessCheckBox.Size = new System.Drawing.Size(130, 17);
            this.OverrideQueueStatusQuickAccessCheckBox.TabIndex = 1;
            this.OverrideQueueStatusQuickAccessCheckBox.Text = "Override queue status";
            this.OverrideQueueStatusQuickAccessCheckBox.UseVisualStyleBackColor = true;
            this.OverrideQueueStatusQuickAccessCheckBox.CheckedChanged += new System.EventHandler(this.OverrideQueueStatusQuickAccessCheckBox_CheckedChanged);
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ApplyButton.Location = new System.Drawing.Point(479, 11);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 2;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // BuildDefinitionSearchPanel
            // 
            this.BuildDefinitionSearchPanel.AutoScroll = true;
            this.BuildDefinitionSearchPanel.Controls.Add(this.BuildDefinitionSearchPlaceholderLabel);
            this.BuildDefinitionSearchPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.BuildDefinitionSearchPanel.Location = new System.Drawing.Point(0, 0);
            this.BuildDefinitionSearchPanel.Name = "BuildDefinitionSearchPanel";
            this.BuildDefinitionSearchPanel.Size = new System.Drawing.Size(650, 517);
            this.BuildDefinitionSearchPanel.TabIndex = 0;
            // 
            // BuildDefinitionSearchPlaceholderLabel
            // 
            this.BuildDefinitionSearchPlaceholderLabel.AutoSize = true;
            this.BuildDefinitionSearchPlaceholderLabel.Location = new System.Drawing.Point(26, 28);
            this.BuildDefinitionSearchPlaceholderLabel.Name = "BuildDefinitionSearchPlaceholderLabel";
            this.BuildDefinitionSearchPlaceholderLabel.Size = new System.Drawing.Size(173, 13);
            this.BuildDefinitionSearchPlaceholderLabel.TabIndex = 0;
            this.BuildDefinitionSearchPlaceholderLabel.Text = "Build Definition Search Placeholder";
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.VsoBuild.Sidekicks.Help.chm";
            // 
            // BuildDefinitionRequestBuildForm
            // 
            this.AcceptButton = this.ApplyButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 517);
            this.ControlBox = false;
            this.Controls.Add(this.VerticalSplitter);
            this.Controls.Add(this.PropertiesPanel);
            this.Controls.Add(this.BuildDefinitionSearchPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "BuildDefinitionRequestBuildForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Request Build";
            this.PropertiesPanel.ResumeLayout(false);
            this.BuildDefinitionListGroupBox.ResumeLayout(false);
            this.BuildDefinitionListGroupBox.PerformLayout();
            this.BuildNotificationGroupBox.ResumeLayout(false);
            this.BuildNotificationGroupBox.PerformLayout();
            this.CopyContextMenuStrip.ResumeLayout(false);
            this.NewEmailContextMenuStrip.ResumeLayout(false);
            this.EnvironmentGroupBox.ResumeLayout(false);
            this.EnvironmentGroupBox.PerformLayout();
            this.DREnvironmentGroupBox.ResumeLayout(false);
            this.DREnvironmentGroupBox.PerformLayout();
            this.ParametersGroupBox.ResumeLayout(false);
            this.ParametersGroupBox.PerformLayout();
            this.OptionsGroupBox.ResumeLayout(false);
            this.OptionsGroupBox.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.QuickAccessControlPanel.ResumeLayout(false);
            this.QuickAccessControlPanel.PerformLayout();
            this.CheckBuildStatusContextMenuStrip.ResumeLayout(false);
            this.BuildDefinitionSearchPanel.ResumeLayout(false);
            this.BuildDefinitionSearchPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Splitter VerticalSplitter;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Panel BuildDefinitionSearchPanel;
        private System.Windows.Forms.Label BuildDefinitionSearchPlaceholderLabel;
        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.GroupBox ParametersGroupBox;
        private System.Windows.Forms.ComboBox VerbosityComboBox;
        private System.Windows.Forms.Label VerbostityLabel;
        private System.Windows.Forms.Button ClearParametersButton;
        private System.Windows.Forms.TextBox ParametersTextBox;
        private System.Windows.Forms.GroupBox OptionsGroupBox;
        private System.Windows.Forms.ComboBox QueuePriorityComboBox;
        private System.Windows.Forms.Label QueuePriorityLabel;
        private System.Windows.Forms.GroupBox BuildNotificationGroupBox;
        private System.Windows.Forms.Button PreviewButton;
        private System.Windows.Forms.ToolTip RequestBuildToolTip;
        private System.Windows.Forms.CheckBox OverrideQueueStatusCheckBox;
        private System.Windows.Forms.GroupBox DREnvironmentGroupBox;
        private System.Windows.Forms.CheckBox EnvironmentProdDrCheckBox;
        private System.Windows.Forms.CheckBox EnvironmentUatDrCheckBox;
        private System.Windows.Forms.CheckBox EnvironmentDevDrCheckBox;
        private System.Windows.Forms.GroupBox EnvironmentGroupBox;
        private System.Windows.Forms.CheckBox EnvironmentPerfCheckBox;
        private System.Windows.Forms.CheckBox EnvironmentProdCheckBox;
        private System.Windows.Forms.CheckBox EnvironmentUatCheckBox;
        private System.Windows.Forms.CheckBox EnvironmentQaCheckBox;
        private System.Windows.Forms.CheckBox EnvironmentDevCheckBox;
        private System.Windows.Forms.GroupBox BuildDefinitionListGroupBox;
        private System.Windows.Forms.ListBox BuildDefinitionSelectorListBox;
        private System.Windows.Forms.Label BuildDefinitionSelectionStatusLabel;
        private SplitButton CopySplitButton;
        private SplitButton NewEmailSplitButton;
        private System.Windows.Forms.ContextMenuStrip CopyContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CopyAsTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CopyAsHTMLToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip NewEmailContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem NewEmailAsTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem NewEmailAsHTMLToolStripMenuItem;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.ContextMenuStrip CheckBuildStatusContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CheckBuildStatusSwitchSidekickToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CheckBuildStatusNewWindowToolStripMenuItem;
        private System.Windows.Forms.Panel QuickAccessControlPanel;
        private SplitButton CheckBuildStatusQuickAccessSplitButton;
        private System.Windows.Forms.CheckBox OverrideQueueStatusQuickAccessCheckBox;
        private System.Windows.Forms.ProgressBar RequestBuildProgressBar;
        private System.Windows.Forms.Button HelpButton;
    }
}