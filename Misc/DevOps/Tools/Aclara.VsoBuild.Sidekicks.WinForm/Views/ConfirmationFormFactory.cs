﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Confirmation form factory.
    /// </summary>
    public class ConfirmationFormFactory
    {

        #region Private Constants
        #endregion

        #region Public Methods

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="pendingActionMessage"></param>
        /// <param name="confirmationMessage"></param>
        /// <param name="expectedTypedResponseValue"></param>
        /// <param name="requireTypedResponse"></param>
        /// <returns></returns>
        public static ConfirmationForm CreateForm(string pendingActionMessage,
                                                  string confirmationMessage,
                                                  string expectedTypedResponseValue,
                                                  bool requireTypedResponse,
                                                  bool displayWarning,
                                                  string warningMessage)
        {

            ConfirmationForm result = null;
            ConfirmationPresenter ConfirmationPresenter = null;

            try
            {
                result = new ConfirmationForm();

                ConfirmationPresenter = new ConfirmationPresenter(result);

                result.ConfirmationPresenter = ConfirmationPresenter;

                ConfirmationPresenter.Initialize();

                result.PendingActionMessage = pendingActionMessage;
                result.ConfirmationMessage = confirmationMessage;
                result.RequireTypedResponse = requireTypedResponse;
                result.ExpectedTypedResponseValue = expectedTypedResponseValue;
                result.DisplayWarning = displayWarning;
                result.WarningMessage = warningMessage;


            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion
    }
}
