﻿namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    partial class NotificationPreviewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NotificationPreviewForm));
            this.NotificationPreviewPanel = new System.Windows.Forms.Panel();
            this.NotificationPreviewTextBox = new System.Windows.Forms.TextBox();
            this.CloseButton = new System.Windows.Forms.Button();
            this.NotificationPreviewPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // NotificationPreviewPanel
            // 
            this.NotificationPreviewPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NotificationPreviewPanel.Controls.Add(this.NotificationPreviewTextBox);
            this.NotificationPreviewPanel.Location = new System.Drawing.Point(13, 13);
            this.NotificationPreviewPanel.Name = "NotificationPreviewPanel";
            this.NotificationPreviewPanel.Size = new System.Drawing.Size(587, 204);
            this.NotificationPreviewPanel.TabIndex = 0;
            // 
            // NotificationPreviewTextBox
            // 
            this.NotificationPreviewTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.NotificationPreviewTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NotificationPreviewTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NotificationPreviewTextBox.Location = new System.Drawing.Point(0, 0);
            this.NotificationPreviewTextBox.Multiline = true;
            this.NotificationPreviewTextBox.Name = "NotificationPreviewTextBox";
            this.NotificationPreviewTextBox.ReadOnly = true;
            this.NotificationPreviewTextBox.Size = new System.Drawing.Size(587, 204);
            this.NotificationPreviewTextBox.TabIndex = 0;
            this.NotificationPreviewTextBox.TabStop = false;
            // 
            // CloseButton
            // 
            this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CloseButton.Location = new System.Drawing.Point(525, 227);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 0;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // NotificationPreviewForm
            // 
            this.AcceptButton = this.CloseButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CloseButton;
            this.ClientSize = new System.Drawing.Size(612, 262);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.NotificationPreviewPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NotificationPreviewForm";
            this.Text = "Notification Preview";
            this.NotificationPreviewPanel.ResumeLayout(false);
            this.NotificationPreviewPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel NotificationPreviewPanel;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.TextBox NotificationPreviewTextBox;
    }
}