﻿using Aclara.Vsts.Notification.Client.Models;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public interface INotificationAdminView
    {
        string SidekickName { get; }

        string SidekickDescription { get; }

        string BackgroundTaskStatus { get; set; }

        void RefreshButtonEnable(bool enable);

        void TeamProjectChanged();

        NotificationSubscriptionList NotificationSubscriptionList { get; set; }
        
        void PopulateNotificationSubscriptionList();

        void UpdateNotificationSubscriptionOperationStatus(string statusMessage, int notificationSubscriptionListRetrievedCount, int notificationSubscriptionListTotalCount);

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus, NotificationAdminPresenter.BackgroundTaskOperationId BackgroundTaskOperation);
    }
}
