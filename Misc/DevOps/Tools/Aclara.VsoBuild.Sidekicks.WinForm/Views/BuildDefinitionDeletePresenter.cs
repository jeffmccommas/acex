﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.Vso.Build.Client;
using Aclara.Vso.Build.Client.Events;
using Aclara.Vso.Build.Client.Models;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build definition delete - presenter.
    /// </summary>
    public class BuildDefinitionDeletePresenter
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private CustomLogger _logger = null;
        private SidekickConfiguration _sidekickConfiguration = null;
        private IBuildDefinitionDeleteView _buildDefinitionDeleteView;
        private BuildDefinitionManager _buildDefinitionManager;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;
        private ITeamProjectInfo _teamProjectInfo = null;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.BuildDefinitionDeleteView.SidekickName,
                                               this.BuildDefinitionDeleteView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Build definiton delete view.
        /// </summary>
        public IBuildDefinitionDeleteView BuildDefinitionDeleteView
        {
            get { return _buildDefinitionDeleteView; }
            set { _buildDefinitionDeleteView = value; }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Build definition manager.
        /// </summary>
        public BuildDefinitionManager BuildDefinitionManager
        {
            get { return _buildDefinitionManager; }
            set { _buildDefinitionManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamPrjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionDeleteView"></param>
        public BuildDefinitionDeletePresenter(ITeamProjectInfo teamPrjectInfo,
                                              SidekickConfiguration sidekickConfiguration,
                                              IBuildDefinitionDeleteView buildDefinitionDeleteView)
        {
            this.TeamProjectInfo = teamPrjectInfo;
            this.SidekickConfiguration = sidekickConfiguration;
            this.BuildDefinitionDeleteView = buildDefinitionDeleteView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private BuildDefinitionDeletePresenter()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalBuildDuration;
            AggregateException aggregateException = null;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalBuildDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.BuildDefinitionDeleteView.BackgroundTaskCompleted("Delete build definition request canceled.");
                        this.BuildDefinitionDeleteView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format("Delete build definition request cancelled."));

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format("[*] Delete build definition request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                         totalBuildDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.BuildDefinitionDeleteView.BackgroundTaskCompleted("");
                        this.BuildDefinitionDeleteView.ApplyButtonEnable(true);


                        //Log background task completed log entry.

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format("Operation cancelled."));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format("Operation cancelled."));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format("Request failed."));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format("Request failed."));
                                    }

                                }
                            }
                        }

                        this.Logger.Info(string.Format("[*] Delete build definition request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                         totalBuildDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.BuildDefinitionDeleteView.BackgroundTaskCompleted("Delete build definition request completed.");
                        this.BuildDefinitionDeleteView.ApplyButtonEnable(true);

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format("[*] Delete build definition request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                         totalBuildDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Delete build definitions.
        /// </summary>
        /// <param name="buildDefinitionSelectorList"></param>
        public void DeleteBuildDefinitions(BuildDefinitionSelectorList buildDefinitionSelectorList)
        {

            List<int> filteredBuidDefinitionIdList = null;
            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.BuildDefinitionDeleteView.BackgroundTaskStatus = "Cancelling delete build definition request...";
                    this.BuildDefinitionDeleteView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.BuildDefinitionDeleteView.BackgroundTaskStatus = "Delete build definition(s) requested...";
                    this.BuildDefinitionDeleteView.ApplyButtonText = "Cancel";

                    this.BuildDefinitionManager = this.CreateBuildDefinitionManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    //Reterieve fresh build definition list. (In case build definitions have changed since last retrieval).
                    filteredBuidDefinitionIdList = buildDefinitionSelectorList.GetBuildDefinitionIdList();

                    this.BuildDefinitionManager.BuildDefinitionDeleted += OnBuildDefinitionDeleted;

                    this.BackgroundTask = new Task(() => this.BuildDefinitionManager.DeleteBuildDefinitions(filteredBuidDefinitionIdList,
                                                                                                            cancellationToken),
                                                                                                            cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create build definition manager.
        /// </summary>
        /// <returns></returns>
        protected BuildDefinitionManager CreateBuildDefinitionManager()
        {
            BuildDefinitionManager result = null;

            try
            {
                result = BuildDefinitionManagerFactory.CreateBuildDefinitionManager(this.TeamProjectInfo.TeamProjectCollectionUri,
                                                                                    this.TeamProjectInfo.TeamProjectCollectionVsrmUri, 
                                                                                    this.TeamProjectInfo.TeamProjectName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIUserProfileName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIPassword);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: Build definition deleted.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="deleteBuildDefinitionEventArgs"></param>
        protected void OnBuildDefinitionDeleted(Object sender,
                                               DeleteBuildDefinitionEventArgs deleteBuildDefinitionEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Build definition details available and status list details available.
                if (deleteBuildDefinitionEventArgs.BuildDefinition != null &&
                    deleteBuildDefinitionEventArgs.StatusList != null &&
                    deleteBuildDefinitionEventArgs.StatusList.Count > 0)
                {
                    message = string.Format("Build definition delete may have been canceled. (Build definition name: {0}; Status --> {1})",
                                            deleteBuildDefinitionEventArgs.BuildDefinition.Name,
                                            deleteBuildDefinitionEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    if (deleteBuildDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error) == true)
                    {
                        this.Logger.Error(message);

                    }
                    else if (deleteBuildDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Warning) == true)
                    {
                        this.Logger.Warn(message);
                    }
                    else
                    {
                        this.Logger.Info(message);
                    }

                }
                //Build definition details available and status list is unavailable.
                else if (deleteBuildDefinitionEventArgs.BuildDefinition != null)
                {
                    this.Logger.Info(string.Format("Build definition deleted. (Build definition name: {0})",
                                                     deleteBuildDefinitionEventArgs.BuildDefinition.Name));
                }
                //Build definiton details unavailable.
                else
                {
                    this.Logger.Info(string.Format("Build definition deleted. Details unavailable."));
                }

                this.BuildDefinitionDeleteView.UpdateProgressDeleteBuildDefinition(deleteBuildDefinitionEventArgs.BuildDefinitionDeletedCount,
                                                                                   deleteBuildDefinitionEventArgs.BuildDefinitionTotalCount);
            }
            catch (Exception)
            {
                throw;
            }


        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                        }
                        else
                        {
                            Logger.Error(string.Format("Operation cancelled.(Unobserved)"));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                }
                else
                {
                    Logger.Error(string.Format("Operation cancelled.(Unobserved)"));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
