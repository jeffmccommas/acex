﻿using Aclara.Tools.Common;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class BuildDefinitionChangeQueueStatusForm : Form, IBuildDefinitionChangeQueueStatusView, ISidekickView, ISidekickInfo
    {

        #region Private Constants

        private const string SidekickView_SidekickName = "ChangeBuildDefinitionsQueueStatus";
        private const string SidekickView_SidekickDescription = "Change Build Definitions Queue Status";

        private const string ValidationErrorMessage_TeamProjectNameNotSelected = "Team project not selected.";
        private const string ValidationErrorMessage_BranchNameSourceNotEntered = "Branch name source not entered.";
        private const string ValidationErrorMessage_BuildDefinitionNotSelected = "Build definition not selected.";
        private const string ValidationErrorMessage_SourceBranchNameNotEntered = "Branch name - source not entered.";
        private const string ValidationErrorMessage_SourceBuildDefinitionNamePrefixNotEntered = "Build definition name prefix - source not entered.";
        private const string ValidationErrorMessage_TargetBranchNameNotEntered = "Branch name - target not entered.";
        private const string ValidationErrorMessage_TargetBuildDefinitionNamePrefixNotEntered = "Build definition name prefix - target not entered.";
        private const string ValidationErrorMessage_SelectedBuildDefinitionsMustHaveSameNamePrefix = "Selected build definitons must have same name prefix.";

        #endregion

        #region Private Data Members

        private SidekickConfiguration _sidekickConfiguration = null;
        private BuildDefinitionSearchForm _buildDefinitionSearchForm = null;
        private BuildDefinitionChangeQueueStatusPresenter _buildDefinitionChangeQueueStatusPresenter;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickView_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickView_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                if (this.BuildDefinitionChangeQueueStatusPresenter.IsBackgroundTaskBusy == false)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick warning level.
        /// </summary>
        public SidekickContainerTypes.SidekickWarningLevel SidekickWarningLevel
        {
            get
            {
                return SidekickContainerTypes.SidekickWarningLevel.Low;
            }
        }

        /// <summary>
        /// Property: Build defintion change queue status presenter.
        /// </summary>
        public BuildDefinitionChangeQueueStatusPresenter BuildDefinitionChangeQueueStatusPresenter
        {
            get { return _buildDefinitionChangeQueueStatusPresenter; }
            set { _buildDefinitionChangeQueueStatusPresenter = value; }
        }

        /// <summary>
        /// Property: Build definition search form.
        /// </summary>
        public BuildDefinitionSearchForm BuildDefinitionSearchForm
        {
            get { return _buildDefinitionSearchForm; }
            set { _buildDefinitionSearchForm = value; }
        }

        /// <summary>
        /// Property: Build definition queue status.
        /// </summary>
        public BuildDefinitionQueueStatus BuildDefinitionQueueStatus
        {
            get
            {
                return (BuildDefinitionQueueStatus)this.BuildDefinitionQueueStatusComboBox.SelectedValue;
            }
            set
            {
                this.BuildDefinitionQueueStatusComboBox.SelectedValue = value;
            }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get { return this.ApplyButton.Text; }
            set { this.ApplyButton.Text = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildDefinitionChangeQueueStatusForm()
        {
            InitializeComponent();


        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        public BuildDefinitionChangeQueueStatusForm(SidekickConfiguration sidekickConfiguration,
                                                    BuildDefinitionSearchForm buildDefinitionSearchForm)
        {
            InitializeComponent();

            this.SidekickConfiguration = sidekickConfiguration;
            this.InitializeControls();
            this.BuildDefinitionSearchForm = buildDefinitionSearchForm;
            this.BuildDefinitionSearchForm.BuildDefinitionSelectionChanged += OnBuildDefinitionSelectionChanged;
            this.BuildDefinitionSearchPanel.DockControl(this.BuildDefinitionSearchForm);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update progress of change queue status.
        /// </summary>
        /// <param name="buildDefinitionChangedCount"></param>
        /// <param name="buildDefinitionTotalCount"></param>
        public void UpdateProgressChangeQueueStatus(int buildDefinitionChangedCount, int buildDefinitionTotalCount)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<int, int>(this.UpdateProgressChangeQueueStatus), buildDefinitionChangedCount, buildDefinitionTotalCount);
            }
            else
            {
                if (buildDefinitionChangedCount < buildDefinitionTotalCount)
                {
                    this.ChangeQueueStatusProgressBar.Visible = true;
                    this.ChangeQueueStatusProgressBar.Minimum = 0;
                    this.ChangeQueueStatusProgressBar.Maximum = buildDefinitionTotalCount;
                    this.ChangeQueueStatusProgressBar.Value = buildDefinitionChangedCount;
                }
                else if (buildDefinitionChangedCount == buildDefinitionTotalCount)
                {
                    this.ChangeQueueStatusProgressBar.Visible = false;
                }
            }
        }

        /// <summary>
        /// Retrieve remaining build definitions.
        /// </summary>
        public void RetrieveRemainingDefinitions()
        {
            this.BuildDefinitionSearchForm.RetrieveRemainingDefinitions();
        }

        /// <summary>
        /// Apply button enable.
        /// </summary>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            this.BuildDefinitionSearchForm.TeamProjectChanged();
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task Completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {

                this.BuildDefinitionQueueStatusComboBox.DataSource = Enum.GetValues(typeof(BuildDefinitionQueueStatus));
                EnableControlsDependantOnSelectedBuildDefinition(false);
                this.ChangeQueueStatusProgressBar.Visible = false;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Hanlder: Build definition selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="requestBuildEventArgs"></param>
        protected void OnBuildDefinitionSelectionChanged(Object sender,
                                                         BuildDefinitionSelectionChangedEventArgs buildDefinitionSelectionChangedEventArgs)
        {

            try
            {
                if (buildDefinitionSelectionChangedEventArgs != null)
                {
                    if (buildDefinitionSelectionChangedEventArgs.OneOrMoreSelected == true)
                    {
                        EnableControlsDependantOnSelectedBuildDefinition(true);
                    }
                    else
                    {
                        EnableControlsDependantOnSelectedBuildDefinition(false);
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Enable/disable controls dependant on at lease one build definition selected.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnSelectedBuildDefinition(bool enable)
        {
            try
            {
                this.BuildDefinitionQueueStatusComboBox.Enabled = enable;

                this.ApplyButton.Enabled = enable;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelectorList buildDefinitionSelectorList = null;

            try
            {

                if (this.BuildDefinitionChangeQueueStatusPresenter.TeamProjectInfo.TeamProjectCollectionUri == null ||
                    this.BuildDefinitionChangeQueueStatusPresenter.TeamProjectInfo.TeamProjectName == string.Empty)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_TeamProjectNameNotSelected));
                    return;
                }

                buildDefinitionSelectorList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();
                if (buildDefinitionSelectorList == null ||
                    buildDefinitionSelectorList.Count <= 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_BuildDefinitionNotSelected));
                    return;
                }
                this.ChangeQueueStatusProgressBar.Visible = false;
                this.BuildDefinitionChangeQueueStatusPresenter.ChangeBuildDefinitionsQueueStatus(this.BuildDefinitionQueueStatus,
                                                                                                 buildDefinitionSelectorList);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickChangeBuildDefinitionsQueueStatus.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion


    }
}
