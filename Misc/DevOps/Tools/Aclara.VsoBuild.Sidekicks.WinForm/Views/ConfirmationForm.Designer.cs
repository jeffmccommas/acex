﻿namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    partial class ConfirmationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfirmationForm));
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.ConfirmationMessageLabel = new System.Windows.Forms.Label();
            this.TypedResponseValueTextBox = new System.Windows.Forms.TextBox();
            this.ValidationMessageLabel = new System.Windows.Forms.Label();
            this.PendingActionMessageLabel = new System.Windows.Forms.Label();
            this.WarningPanel = new System.Windows.Forms.Panel();
            this.WarningMessageLabel = new System.Windows.Forms.Label();
            this.WarningPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // OKButton
            // 
            this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKButton.Location = new System.Drawing.Point(312, 165);
            this.OKButton.Margin = new System.Windows.Forms.Padding(4);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(100, 28);
            this.OKButton.TabIndex = 5;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(420, 165);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(4);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(100, 28);
            this.CancelButton.TabIndex = 6;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ConfirmationMessageLabel
            // 
            this.ConfirmationMessageLabel.AutoSize = true;
            this.ConfirmationMessageLabel.Location = new System.Drawing.Point(16, 77);
            this.ConfirmationMessageLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ConfirmationMessageLabel.Name = "ConfirmationMessageLabel";
            this.ConfirmationMessageLabel.Size = new System.Drawing.Size(164, 16);
            this.ConfirmationMessageLabel.TabIndex = 2;
            this.ConfirmationMessageLabel.Text = "<*confirmation message*>";
            this.ConfirmationMessageLabel.Enter += new System.EventHandler(this.TypedResponseValueTextBox_Enter);
            // 
            // TypedResponseValueTextBox
            // 
            this.TypedResponseValueTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.TypedResponseValueTextBox.Location = new System.Drawing.Point(16, 97);
            this.TypedResponseValueTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.TypedResponseValueTextBox.Name = "TypedResponseValueTextBox";
            this.TypedResponseValueTextBox.Size = new System.Drawing.Size(103, 22);
            this.TypedResponseValueTextBox.TabIndex = 3;
            this.TypedResponseValueTextBox.Enter += new System.EventHandler(this.TypedResponseValueTextBox_Enter);
            // 
            // ValidationMessageLabel
            // 
            this.ValidationMessageLabel.AutoSize = true;
            this.ValidationMessageLabel.ForeColor = System.Drawing.Color.Red;
            this.ValidationMessageLabel.Location = new System.Drawing.Point(16, 129);
            this.ValidationMessageLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ValidationMessageLabel.Name = "ValidationMessageLabel";
            this.ValidationMessageLabel.Size = new System.Drawing.Size(0, 16);
            this.ValidationMessageLabel.TabIndex = 4;
            this.ValidationMessageLabel.Enter += new System.EventHandler(this.TypedResponseValueTextBox_Enter);
            // 
            // PendingActionMessageLabel
            // 
            this.PendingActionMessageLabel.AutoSize = true;
            this.PendingActionMessageLabel.Location = new System.Drawing.Point(16, 11);
            this.PendingActionMessageLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.PendingActionMessageLabel.Name = "PendingActionMessageLabel";
            this.PendingActionMessageLabel.Size = new System.Drawing.Size(180, 16);
            this.PendingActionMessageLabel.TabIndex = 0;
            this.PendingActionMessageLabel.Text = "<*pending action message*>";
            this.PendingActionMessageLabel.Enter += new System.EventHandler(this.TypedResponseValueTextBox_Enter);
            // 
            // WarningPanel
            // 
            this.WarningPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WarningPanel.BackColor = System.Drawing.Color.Red;
            this.WarningPanel.Controls.Add(this.WarningMessageLabel);
            this.WarningPanel.Location = new System.Drawing.Point(3, 34);
            this.WarningPanel.Name = "WarningPanel";
            this.WarningPanel.Size = new System.Drawing.Size(526, 38);
            this.WarningPanel.TabIndex = 1;
            // 
            // WarningMessageLabel
            // 
            this.WarningMessageLabel.AutoSize = true;
            this.WarningMessageLabel.BackColor = System.Drawing.Color.Red;
            this.WarningMessageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WarningMessageLabel.ForeColor = System.Drawing.Color.Gold;
            this.WarningMessageLabel.Location = new System.Drawing.Point(16, 6);
            this.WarningMessageLabel.Name = "WarningMessageLabel";
            this.WarningMessageLabel.Size = new System.Drawing.Size(203, 24);
            this.WarningMessageLabel.TabIndex = 0;
            this.WarningMessageLabel.Text = "<warning_message>";
            this.WarningMessageLabel.Enter += new System.EventHandler(this.TypedResponseValueTextBox_Enter);
            // 
            // ConfirmationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 206);
            this.Controls.Add(this.WarningPanel);
            this.Controls.Add(this.PendingActionMessageLabel);
            this.Controls.Add(this.ValidationMessageLabel);
            this.Controls.Add(this.TypedResponseValueTextBox);
            this.Controls.Add(this.ConfirmationMessageLabel);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OKButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfirmationForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Aclara VSO Build Sidekicks - Confirmation";
            this.Shown += new System.EventHandler(this.ConfirmationForm_Shown);
            this.WarningPanel.ResumeLayout(false);
            this.WarningPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Label ConfirmationMessageLabel;
        private System.Windows.Forms.TextBox TypedResponseValueTextBox;
        private System.Windows.Forms.Label ValidationMessageLabel;
        private System.Windows.Forms.Label PendingActionMessageLabel;
        private System.Windows.Forms.Panel WarningPanel;
        private System.Windows.Forms.Label WarningMessageLabel;
    }
}