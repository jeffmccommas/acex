﻿namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    partial class ReleaseDefinitionSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.SearchCriteriaGroupBox = new System.Windows.Forms.GroupBox();
            this.ExactMatchCheckBox = new System.Windows.Forms.CheckBox();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.SearchButton = new System.Windows.Forms.Button();
            this.ReleaseDefinitionNameFilterTextBox = new System.Windows.Forms.RichTextBox();
            this.ReleaseDefinitionNameFilterContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ReleaseDefinitionNameFilterCutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionNameFilterCopyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionNameFilterPasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionNameFilterDeleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.ReleaseDefinitionNameFilterSelectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionViewContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ReleaseDefinitionViewAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionStatusPanel = new System.Windows.Forms.Panel();
            this.ReleaseDefinitionRetrievalStatusPanel = new System.Windows.Forms.Panel();
            this.ReleaseDefinitionRetrievalProgressBar = new System.Windows.Forms.ProgressBar();
            this.ReleaseDefinitionRetrievalStatusLabel = new System.Windows.Forms.Label();
            this.ReleaseDefinitionMatchStatusPanel = new System.Windows.Forms.Panel();
            this.ReleaseDefinitionMatchStatusLabel = new System.Windows.Forms.Label();
            this.ReleaseDefinitionHighlightContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ReleaseDefinitionHighlightNoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.ReleaseDefinitionHighlightDevToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionHighlightQAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionHighlightUATToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionHighlightPerfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionHighlightProductionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.ReleaseDefinitionHighlightDevDRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionHighlightUATDRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionHighlightProdDRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionDisplayContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ReleaseDefinitionDisplayAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionDisplayChecedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionDisplayHighlightedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ReleaseDefinitionDisplayDevToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionDisplayQAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionDisplayUATToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionDisplayPerfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionDisplayProductionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.ReleaseDefinitionDisplayDevDRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionDisplayUATDRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionDisplayProdDRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionCheckContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ReleaseDefinitionCheckAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionCheckNoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionCheckHighlightedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ReleaseDefinitionCheckDevToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionCheckQAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionCheckUATToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionCheckPerfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionCheckProductionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ReleaseDefinitionCheckDevDRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionCheckUATDRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionCheckProdDRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyButton = new System.Windows.Forms.Button();
            this.ReleaseDefinitionListDataGridView = new System.Windows.Forms.DataGridView();
            this.CheckColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EnabledColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.NameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CommentColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExtraColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReleaseDefinitionListDataGridViewContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CheckSelectedReleaseDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyReleaseDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewReleaseDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReleaseDefinitionSearchToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.ViewSplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.HighlightSplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.DisplaySplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.CheckSplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.SearchCriteriaGroupBox.SuspendLayout();
            this.ReleaseDefinitionNameFilterContextMenuStrip.SuspendLayout();
            this.ReleaseDefinitionViewContextMenuStrip.SuspendLayout();
            this.ReleaseDefinitionStatusPanel.SuspendLayout();
            this.ReleaseDefinitionRetrievalStatusPanel.SuspendLayout();
            this.ReleaseDefinitionMatchStatusPanel.SuspendLayout();
            this.ReleaseDefinitionHighlightContextMenuStrip.SuspendLayout();
            this.ReleaseDefinitionDisplayContextMenuStrip.SuspendLayout();
            this.ReleaseDefinitionCheckContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReleaseDefinitionListDataGridView)).BeginInit();
            this.ReleaseDefinitionListDataGridViewContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // SearchCriteriaGroupBox
            // 
            this.SearchCriteriaGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchCriteriaGroupBox.Controls.Add(this.ExactMatchCheckBox);
            this.SearchCriteriaGroupBox.Controls.Add(this.RefreshButton);
            this.SearchCriteriaGroupBox.Controls.Add(this.ClearButton);
            this.SearchCriteriaGroupBox.Controls.Add(this.SearchButton);
            this.SearchCriteriaGroupBox.Controls.Add(this.ReleaseDefinitionNameFilterTextBox);
            this.SearchCriteriaGroupBox.Location = new System.Drawing.Point(13, 9);
            this.SearchCriteriaGroupBox.Name = "SearchCriteriaGroupBox";
            this.SearchCriteriaGroupBox.Size = new System.Drawing.Size(475, 133);
            this.SearchCriteriaGroupBox.TabIndex = 0;
            this.SearchCriteriaGroupBox.TabStop = false;
            this.SearchCriteriaGroupBox.Text = "Release definition search criteria";
            // 
            // ExactMatchCheckBox
            // 
            this.ExactMatchCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ExactMatchCheckBox.AutoSize = true;
            this.ExactMatchCheckBox.Location = new System.Drawing.Point(384, 19);
            this.ExactMatchCheckBox.Name = "ExactMatchCheckBox";
            this.ExactMatchCheckBox.Size = new System.Drawing.Size(85, 17);
            this.ExactMatchCheckBox.TabIndex = 5;
            this.ExactMatchCheckBox.Text = "Exact match";
            this.ExactMatchCheckBox.UseVisualStyleBackColor = true;
            // 
            // RefreshButton
            // 
            this.RefreshButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RefreshButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
            this.RefreshButton.Location = new System.Drawing.Point(383, 71);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(32, 23);
            this.RefreshButton.TabIndex = 7;
            this.RefreshButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ClearButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ClearButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionClear;
            this.ClearButton.Location = new System.Drawing.Point(383, 100);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(32, 23);
            this.ClearButton.TabIndex = 8;
            this.ClearButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // SearchButton
            // 
            this.SearchButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionSearch;
            this.SearchButton.Location = new System.Drawing.Point(384, 42);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(32, 23);
            this.SearchButton.TabIndex = 6;
            this.SearchButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // ReleaseDefinitionNameFilterTextBox
            // 
            this.ReleaseDefinitionNameFilterTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReleaseDefinitionNameFilterTextBox.ContextMenuStrip = this.ReleaseDefinitionNameFilterContextMenuStrip;
            this.ReleaseDefinitionNameFilterTextBox.Location = new System.Drawing.Point(6, 19);
            this.ReleaseDefinitionNameFilterTextBox.Name = "ReleaseDefinitionNameFilterTextBox";
            this.ReleaseDefinitionNameFilterTextBox.Size = new System.Drawing.Size(365, 104);
            this.ReleaseDefinitionNameFilterTextBox.TabIndex = 0;
            this.ReleaseDefinitionNameFilterTextBox.Text = "";
            // 
            // ReleaseDefinitionNameFilterContextMenuStrip
            // 
            this.ReleaseDefinitionNameFilterContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ReleaseDefinitionNameFilterCutToolStripMenuItem,
            this.ReleaseDefinitionNameFilterCopyToolStripMenuItem,
            this.ReleaseDefinitionNameFilterPasteToolStripMenuItem,
            this.ReleaseDefinitionNameFilterDeleteToolStripMenuItem,
            this.toolStripSeparator7,
            this.ReleaseDefinitionNameFilterSelectAllToolStripMenuItem});
            this.ReleaseDefinitionNameFilterContextMenuStrip.Name = "ReleaseDefinitionNameFilterContextMenuStrip";
            this.ReleaseDefinitionNameFilterContextMenuStrip.Size = new System.Drawing.Size(123, 120);
            // 
            // ReleaseDefinitionNameFilterCutToolStripMenuItem
            // 
            this.ReleaseDefinitionNameFilterCutToolStripMenuItem.Name = "ReleaseDefinitionNameFilterCutToolStripMenuItem";
            this.ReleaseDefinitionNameFilterCutToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.ReleaseDefinitionNameFilterCutToolStripMenuItem.Text = "Cut";
            this.ReleaseDefinitionNameFilterCutToolStripMenuItem.Click += new System.EventHandler(this.ReleaseDefinitionNameFilterCutToolStripMenuItem_Click);
            // 
            // ReleaseDefinitionNameFilterCopyToolStripMenuItem
            // 
            this.ReleaseDefinitionNameFilterCopyToolStripMenuItem.Name = "ReleaseDefinitionNameFilterCopyToolStripMenuItem";
            this.ReleaseDefinitionNameFilterCopyToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.ReleaseDefinitionNameFilterCopyToolStripMenuItem.Text = "Copy";
            this.ReleaseDefinitionNameFilterCopyToolStripMenuItem.Click += new System.EventHandler(this.ReleaseDefinitionNameFilterCopyToolStripMenuItem_Click);
            // 
            // ReleaseDefinitionNameFilterPasteToolStripMenuItem
            // 
            this.ReleaseDefinitionNameFilterPasteToolStripMenuItem.Name = "ReleaseDefinitionNameFilterPasteToolStripMenuItem";
            this.ReleaseDefinitionNameFilterPasteToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.ReleaseDefinitionNameFilterPasteToolStripMenuItem.Text = "Paste";
            this.ReleaseDefinitionNameFilterPasteToolStripMenuItem.Click += new System.EventHandler(this.ReleaseDefinitionNameFilterPasteToolStripMenuItem_Click);
            // 
            // ReleaseDefinitionNameFilterDeleteToolStripMenuItem
            // 
            this.ReleaseDefinitionNameFilterDeleteToolStripMenuItem.Name = "ReleaseDefinitionNameFilterDeleteToolStripMenuItem";
            this.ReleaseDefinitionNameFilterDeleteToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.ReleaseDefinitionNameFilterDeleteToolStripMenuItem.Text = "Delete";
            this.ReleaseDefinitionNameFilterDeleteToolStripMenuItem.Click += new System.EventHandler(this.ReleaseDefinitionNameFilterDeleteToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(119, 6);
            // 
            // ReleaseDefinitionNameFilterSelectAllToolStripMenuItem
            // 
            this.ReleaseDefinitionNameFilterSelectAllToolStripMenuItem.Name = "ReleaseDefinitionNameFilterSelectAllToolStripMenuItem";
            this.ReleaseDefinitionNameFilterSelectAllToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.ReleaseDefinitionNameFilterSelectAllToolStripMenuItem.Text = "Select All";
            this.ReleaseDefinitionNameFilterSelectAllToolStripMenuItem.Click += new System.EventHandler(this.ReleaseDefinitionNameFilterSelectAllToolStripMenuItem_Click);
            // 
            // ReleaseDefinitionViewContextMenuStrip
            // 
            this.ReleaseDefinitionViewContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ReleaseDefinitionViewAllToolStripMenuItem});
            this.ReleaseDefinitionViewContextMenuStrip.Name = "ReleaseDefinitionViewContextMenuStrip";
            this.ReleaseDefinitionViewContextMenuStrip.Size = new System.Drawing.Size(89, 26);
            this.ReleaseDefinitionViewContextMenuStrip.Click += new System.EventHandler(this.ReleaseDefinitionViewContextMenuStrip_Click);
            // 
            // ReleaseDefinitionViewAllToolStripMenuItem
            // 
            this.ReleaseDefinitionViewAllToolStripMenuItem.Name = "ReleaseDefinitionViewAllToolStripMenuItem";
            this.ReleaseDefinitionViewAllToolStripMenuItem.Size = new System.Drawing.Size(88, 22);
            this.ReleaseDefinitionViewAllToolStripMenuItem.Text = "All";
            // 
            // ReleaseDefinitionStatusPanel
            // 
            this.ReleaseDefinitionStatusPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReleaseDefinitionStatusPanel.BackColor = System.Drawing.Color.White;
            this.ReleaseDefinitionStatusPanel.Controls.Add(this.ReleaseDefinitionRetrievalStatusPanel);
            this.ReleaseDefinitionStatusPanel.Controls.Add(this.ReleaseDefinitionMatchStatusPanel);
            this.ReleaseDefinitionStatusPanel.Location = new System.Drawing.Point(13, 142);
            this.ReleaseDefinitionStatusPanel.Name = "ReleaseDefinitionStatusPanel";
            this.ReleaseDefinitionStatusPanel.Size = new System.Drawing.Size(475, 32);
            this.ReleaseDefinitionStatusPanel.TabIndex = 9;
            // 
            // ReleaseDefinitionRetrievalStatusPanel
            // 
            this.ReleaseDefinitionRetrievalStatusPanel.BackColor = System.Drawing.Color.Transparent;
            this.ReleaseDefinitionRetrievalStatusPanel.Controls.Add(this.ReleaseDefinitionRetrievalProgressBar);
            this.ReleaseDefinitionRetrievalStatusPanel.Controls.Add(this.ReleaseDefinitionRetrievalStatusLabel);
            this.ReleaseDefinitionRetrievalStatusPanel.Location = new System.Drawing.Point(-1, 0);
            this.ReleaseDefinitionRetrievalStatusPanel.MinimumSize = new System.Drawing.Size(320, 20);
            this.ReleaseDefinitionRetrievalStatusPanel.Name = "ReleaseDefinitionRetrievalStatusPanel";
            this.ReleaseDefinitionRetrievalStatusPanel.Size = new System.Drawing.Size(324, 31);
            this.ReleaseDefinitionRetrievalStatusPanel.TabIndex = 5;
            // 
            // ReleaseDefinitionRetrievalProgressBar
            // 
            this.ReleaseDefinitionRetrievalProgressBar.Location = new System.Drawing.Point(6, 20);
            this.ReleaseDefinitionRetrievalProgressBar.Name = "ReleaseDefinitionRetrievalProgressBar";
            this.ReleaseDefinitionRetrievalProgressBar.Size = new System.Drawing.Size(90, 10);
            this.ReleaseDefinitionRetrievalProgressBar.TabIndex = 2;
            this.ReleaseDefinitionRetrievalProgressBar.Visible = false;
            // 
            // ReleaseDefinitionRetrievalStatusLabel
            // 
            this.ReleaseDefinitionRetrievalStatusLabel.AutoSize = true;
            this.ReleaseDefinitionRetrievalStatusLabel.Location = new System.Drawing.Point(6, 3);
            this.ReleaseDefinitionRetrievalStatusLabel.Name = "ReleaseDefinitionRetrievalStatusLabel";
            this.ReleaseDefinitionRetrievalStatusLabel.Size = new System.Drawing.Size(183, 13);
            this.ReleaseDefinitionRetrievalStatusLabel.TabIndex = 3;
            this.ReleaseDefinitionRetrievalStatusLabel.Text = "<Release Definition Retrieval Status>";
            // 
            // ReleaseDefinitionMatchStatusPanel
            // 
            this.ReleaseDefinitionMatchStatusPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ReleaseDefinitionMatchStatusPanel.BackColor = System.Drawing.Color.Transparent;
            this.ReleaseDefinitionMatchStatusPanel.Controls.Add(this.ReleaseDefinitionMatchStatusLabel);
            this.ReleaseDefinitionMatchStatusPanel.Location = new System.Drawing.Point(329, 6);
            this.ReleaseDefinitionMatchStatusPanel.Name = "ReleaseDefinitionMatchStatusPanel";
            this.ReleaseDefinitionMatchStatusPanel.Size = new System.Drawing.Size(140, 20);
            this.ReleaseDefinitionMatchStatusPanel.TabIndex = 3;
            // 
            // ReleaseDefinitionMatchStatusLabel
            // 
            this.ReleaseDefinitionMatchStatusLabel.AutoSize = true;
            this.ReleaseDefinitionMatchStatusLabel.Location = new System.Drawing.Point(3, 3);
            this.ReleaseDefinitionMatchStatusLabel.Name = "ReleaseDefinitionMatchStatusLabel";
            this.ReleaseDefinitionMatchStatusLabel.Size = new System.Drawing.Size(49, 13);
            this.ReleaseDefinitionMatchStatusLabel.TabIndex = 3;
            this.ReleaseDefinitionMatchStatusLabel.Text = "<Status>";
            // 
            // ReleaseDefinitionHighlightContextMenuStrip
            // 
            this.ReleaseDefinitionHighlightContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ReleaseDefinitionHighlightNoneToolStripMenuItem,
            this.toolStripSeparator5,
            this.ReleaseDefinitionHighlightDevToolStripMenuItem,
            this.ReleaseDefinitionHighlightQAToolStripMenuItem,
            this.ReleaseDefinitionHighlightUATToolStripMenuItem,
            this.ReleaseDefinitionHighlightPerfToolStripMenuItem,
            this.ReleaseDefinitionHighlightProductionToolStripMenuItem,
            this.toolStripSeparator6,
            this.ReleaseDefinitionHighlightDevDRToolStripMenuItem,
            this.ReleaseDefinitionHighlightUATDRToolStripMenuItem,
            this.ReleaseDefinitionHighlightProdDRToolStripMenuItem});
            this.ReleaseDefinitionHighlightContextMenuStrip.Name = "ReleaseDefinitionHighlightContextMenuStrip";
            this.ReleaseDefinitionHighlightContextMenuStrip.Size = new System.Drawing.Size(134, 214);
            // 
            // ReleaseDefinitionHighlightNoneToolStripMenuItem
            // 
            this.ReleaseDefinitionHighlightNoneToolStripMenuItem.Name = "ReleaseDefinitionHighlightNoneToolStripMenuItem";
            this.ReleaseDefinitionHighlightNoneToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.ReleaseDefinitionHighlightNoneToolStripMenuItem.Text = "None";
            this.ReleaseDefinitionHighlightNoneToolStripMenuItem.Click += new System.EventHandler(this.ReleaseDefinitionHighlightNoneToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(130, 6);
            // 
            // ReleaseDefinitionHighlightDevToolStripMenuItem
            // 
            this.ReleaseDefinitionHighlightDevToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionHighlightDevToolStripMenuItem.Name = "ReleaseDefinitionHighlightDevToolStripMenuItem";
            this.ReleaseDefinitionHighlightDevToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.ReleaseDefinitionHighlightDevToolStripMenuItem.Text = "Dev";
            this.ReleaseDefinitionHighlightDevToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionHighlightDevToolStripMenuItem_CheckedChanged);
            // 
            // ReleaseDefinitionHighlightQAToolStripMenuItem
            // 
            this.ReleaseDefinitionHighlightQAToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionHighlightQAToolStripMenuItem.Name = "ReleaseDefinitionHighlightQAToolStripMenuItem";
            this.ReleaseDefinitionHighlightQAToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.ReleaseDefinitionHighlightQAToolStripMenuItem.Text = "QA";
            this.ReleaseDefinitionHighlightQAToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionHighlightQAToolStripMenuItem_CheckedChanged);
            // 
            // ReleaseDefinitionHighlightUATToolStripMenuItem
            // 
            this.ReleaseDefinitionHighlightUATToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionHighlightUATToolStripMenuItem.Name = "ReleaseDefinitionHighlightUATToolStripMenuItem";
            this.ReleaseDefinitionHighlightUATToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.ReleaseDefinitionHighlightUATToolStripMenuItem.Text = "UAT";
            this.ReleaseDefinitionHighlightUATToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionHighlightUATToolStripMenuItem_CheckedChanged);
            // 
            // ReleaseDefinitionHighlightPerfToolStripMenuItem
            // 
            this.ReleaseDefinitionHighlightPerfToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionHighlightPerfToolStripMenuItem.Name = "ReleaseDefinitionHighlightPerfToolStripMenuItem";
            this.ReleaseDefinitionHighlightPerfToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.ReleaseDefinitionHighlightPerfToolStripMenuItem.Text = "Perf";
            this.ReleaseDefinitionHighlightPerfToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionHighlightPerfToolStripMenuItem_CheckedChanged);
            // 
            // ReleaseDefinitionHighlightProductionToolStripMenuItem
            // 
            this.ReleaseDefinitionHighlightProductionToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionHighlightProductionToolStripMenuItem.Name = "ReleaseDefinitionHighlightProductionToolStripMenuItem";
            this.ReleaseDefinitionHighlightProductionToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.ReleaseDefinitionHighlightProductionToolStripMenuItem.Text = "Production";
            this.ReleaseDefinitionHighlightProductionToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionHighlightProductionToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(130, 6);
            // 
            // ReleaseDefinitionHighlightDevDRToolStripMenuItem
            // 
            this.ReleaseDefinitionHighlightDevDRToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionHighlightDevDRToolStripMenuItem.Name = "ReleaseDefinitionHighlightDevDRToolStripMenuItem";
            this.ReleaseDefinitionHighlightDevDRToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.ReleaseDefinitionHighlightDevDRToolStripMenuItem.Text = "Dev DR";
            this.ReleaseDefinitionHighlightDevDRToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionHighlightDevDRToolStripMenuItem_CheckedChanged);
            // 
            // ReleaseDefinitionHighlightUATDRToolStripMenuItem
            // 
            this.ReleaseDefinitionHighlightUATDRToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionHighlightUATDRToolStripMenuItem.Name = "ReleaseDefinitionHighlightUATDRToolStripMenuItem";
            this.ReleaseDefinitionHighlightUATDRToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.ReleaseDefinitionHighlightUATDRToolStripMenuItem.Text = "UAT DR ";
            this.ReleaseDefinitionHighlightUATDRToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionHighlightUATDRToolStripMenuItem_CheckedChanged);
            // 
            // ReleaseDefinitionHighlightProdDRToolStripMenuItem
            // 
            this.ReleaseDefinitionHighlightProdDRToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionHighlightProdDRToolStripMenuItem.Name = "ReleaseDefinitionHighlightProdDRToolStripMenuItem";
            this.ReleaseDefinitionHighlightProdDRToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.ReleaseDefinitionHighlightProdDRToolStripMenuItem.Text = "Prod DR";
            this.ReleaseDefinitionHighlightProdDRToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionHighlightProdDRToolStripMenuItem_CheckedChanged);
            // 
            // ReleaseDefinitionDisplayContextMenuStrip
            // 
            this.ReleaseDefinitionDisplayContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ReleaseDefinitionDisplayAllToolStripMenuItem,
            this.ReleaseDefinitionDisplayChecedToolStripMenuItem,
            this.ReleaseDefinitionDisplayHighlightedToolStripMenuItem,
            this.toolStripSeparator3,
            this.ReleaseDefinitionDisplayDevToolStripMenuItem,
            this.ReleaseDefinitionDisplayQAToolStripMenuItem,
            this.ReleaseDefinitionDisplayUATToolStripMenuItem,
            this.ReleaseDefinitionDisplayPerfToolStripMenuItem,
            this.ReleaseDefinitionDisplayProductionToolStripMenuItem,
            this.toolStripSeparator4,
            this.ReleaseDefinitionDisplayDevDRToolStripMenuItem,
            this.ReleaseDefinitionDisplayUATDRToolStripMenuItem,
            this.ReleaseDefinitionDisplayProdDRToolStripMenuItem});
            this.ReleaseDefinitionDisplayContextMenuStrip.Name = "ReleaseDefinitionDisplayContextMenuStrip";
            this.ReleaseDefinitionDisplayContextMenuStrip.Size = new System.Drawing.Size(138, 258);
            // 
            // ReleaseDefinitionDisplayAllToolStripMenuItem
            // 
            this.ReleaseDefinitionDisplayAllToolStripMenuItem.Name = "ReleaseDefinitionDisplayAllToolStripMenuItem";
            this.ReleaseDefinitionDisplayAllToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionDisplayAllToolStripMenuItem.Text = "All";
            this.ReleaseDefinitionDisplayAllToolStripMenuItem.Click += new System.EventHandler(this.ReleaseDefinitionDisplayAllToolStripMenuItem_Click);
            // 
            // ReleaseDefinitionDisplayChecedToolStripMenuItem
            // 
            this.ReleaseDefinitionDisplayChecedToolStripMenuItem.Name = "ReleaseDefinitionDisplayChecedToolStripMenuItem";
            this.ReleaseDefinitionDisplayChecedToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionDisplayChecedToolStripMenuItem.Text = "Checked";
            this.ReleaseDefinitionDisplayChecedToolStripMenuItem.Click += new System.EventHandler(this.ReleaseDefinitionDisplayCheckedToolStripMenuItem_Click);
            // 
            // ReleaseDefinitionDisplayHighlightedToolStripMenuItem
            // 
            this.ReleaseDefinitionDisplayHighlightedToolStripMenuItem.Name = "ReleaseDefinitionDisplayHighlightedToolStripMenuItem";
            this.ReleaseDefinitionDisplayHighlightedToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionDisplayHighlightedToolStripMenuItem.Text = "Highlighted";
            this.ReleaseDefinitionDisplayHighlightedToolStripMenuItem.Click += new System.EventHandler(this.ReleaseDefinitionDisplayHighlightedToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(134, 6);
            // 
            // ReleaseDefinitionDisplayDevToolStripMenuItem
            // 
            this.ReleaseDefinitionDisplayDevToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionDisplayDevToolStripMenuItem.Name = "ReleaseDefinitionDisplayDevToolStripMenuItem";
            this.ReleaseDefinitionDisplayDevToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionDisplayDevToolStripMenuItem.Text = "Dev";
            this.ReleaseDefinitionDisplayDevToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionDisplayDevToolStripMenuItem_CheckedChanged);
            // 
            // ReleaseDefinitionDisplayQAToolStripMenuItem
            // 
            this.ReleaseDefinitionDisplayQAToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionDisplayQAToolStripMenuItem.Name = "ReleaseDefinitionDisplayQAToolStripMenuItem";
            this.ReleaseDefinitionDisplayQAToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionDisplayQAToolStripMenuItem.Text = "QA";
            this.ReleaseDefinitionDisplayQAToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionDisplayQAToolStripMenuItem_CheckedChanged);
            // 
            // ReleaseDefinitionDisplayUATToolStripMenuItem
            // 
            this.ReleaseDefinitionDisplayUATToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionDisplayUATToolStripMenuItem.Name = "ReleaseDefinitionDisplayUATToolStripMenuItem";
            this.ReleaseDefinitionDisplayUATToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionDisplayUATToolStripMenuItem.Text = "UAT";
            this.ReleaseDefinitionDisplayUATToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionDisplayUATToolStripMenuItem_CheckedChanged);
            // 
            // ReleaseDefinitionDisplayPerfToolStripMenuItem
            // 
            this.ReleaseDefinitionDisplayPerfToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionDisplayPerfToolStripMenuItem.Name = "ReleaseDefinitionDisplayPerfToolStripMenuItem";
            this.ReleaseDefinitionDisplayPerfToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionDisplayPerfToolStripMenuItem.Text = "Perf";
            this.ReleaseDefinitionDisplayPerfToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionDisplayPerfToolStripMenuItem_CheckedChanged);
            // 
            // ReleaseDefinitionDisplayProductionToolStripMenuItem
            // 
            this.ReleaseDefinitionDisplayProductionToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionDisplayProductionToolStripMenuItem.Name = "ReleaseDefinitionDisplayProductionToolStripMenuItem";
            this.ReleaseDefinitionDisplayProductionToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionDisplayProductionToolStripMenuItem.Text = "Production";
            this.ReleaseDefinitionDisplayProductionToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionDisplayProductionToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(134, 6);
            // 
            // ReleaseDefinitionDisplayDevDRToolStripMenuItem
            // 
            this.ReleaseDefinitionDisplayDevDRToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionDisplayDevDRToolStripMenuItem.Name = "ReleaseDefinitionDisplayDevDRToolStripMenuItem";
            this.ReleaseDefinitionDisplayDevDRToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionDisplayDevDRToolStripMenuItem.Text = "Dev DR";
            this.ReleaseDefinitionDisplayDevDRToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionDisplayDevDRToolStripMenuItem_CheckedChanged);
            // 
            // ReleaseDefinitionDisplayUATDRToolStripMenuItem
            // 
            this.ReleaseDefinitionDisplayUATDRToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionDisplayUATDRToolStripMenuItem.Name = "ReleaseDefinitionDisplayUATDRToolStripMenuItem";
            this.ReleaseDefinitionDisplayUATDRToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionDisplayUATDRToolStripMenuItem.Text = "UAT DR";
            this.ReleaseDefinitionDisplayUATDRToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionDisplayUATDRToolStripMenuItem_CheckedChanged);
            // 
            // ReleaseDefinitionDisplayProdDRToolStripMenuItem
            // 
            this.ReleaseDefinitionDisplayProdDRToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionDisplayProdDRToolStripMenuItem.Name = "ReleaseDefinitionDisplayProdDRToolStripMenuItem";
            this.ReleaseDefinitionDisplayProdDRToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionDisplayProdDRToolStripMenuItem.Text = "Prod DR";
            this.ReleaseDefinitionDisplayProdDRToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionDisplayProdDRToolStripMenuItem_CheckedChanged);
            // 
            // ReleaseDefinitionCheckContextMenuStrip
            // 
            this.ReleaseDefinitionCheckContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ReleaseDefinitionCheckAllToolStripMenuItem,
            this.ReleaseDefinitionCheckNoneToolStripMenuItem,
            this.ReleaseDefinitionCheckHighlightedToolStripMenuItem,
            this.toolStripSeparator1,
            this.ReleaseDefinitionCheckDevToolStripMenuItem,
            this.ReleaseDefinitionCheckQAToolStripMenuItem,
            this.ReleaseDefinitionCheckUATToolStripMenuItem,
            this.ReleaseDefinitionCheckPerfToolStripMenuItem,
            this.ReleaseDefinitionCheckProductionToolStripMenuItem,
            this.toolStripSeparator2,
            this.ReleaseDefinitionCheckDevDRToolStripMenuItem,
            this.ReleaseDefinitionCheckUATDRToolStripMenuItem,
            this.ReleaseDefinitionCheckProdDRToolStripMenuItem});
            this.ReleaseDefinitionCheckContextMenuStrip.Name = "ReleaseDefinitionCheckContextMenuStrip";
            this.ReleaseDefinitionCheckContextMenuStrip.Size = new System.Drawing.Size(138, 258);
            // 
            // ReleaseDefinitionCheckAllToolStripMenuItem
            // 
            this.ReleaseDefinitionCheckAllToolStripMenuItem.Name = "ReleaseDefinitionCheckAllToolStripMenuItem";
            this.ReleaseDefinitionCheckAllToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionCheckAllToolStripMenuItem.Text = "All";
            this.ReleaseDefinitionCheckAllToolStripMenuItem.Click += new System.EventHandler(this.ReleaseDefinitionCheckAllToolStripMenuItem_Click);
            // 
            // ReleaseDefinitionCheckNoneToolStripMenuItem
            // 
            this.ReleaseDefinitionCheckNoneToolStripMenuItem.Name = "ReleaseDefinitionCheckNoneToolStripMenuItem";
            this.ReleaseDefinitionCheckNoneToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionCheckNoneToolStripMenuItem.Text = "None";
            this.ReleaseDefinitionCheckNoneToolStripMenuItem.Click += new System.EventHandler(this.ReleaseDefinitionCheckNoneToolStripMenuItem_Click);
            // 
            // ReleaseDefinitionCheckHighlightedToolStripMenuItem
            // 
            this.ReleaseDefinitionCheckHighlightedToolStripMenuItem.Name = "ReleaseDefinitionCheckHighlightedToolStripMenuItem";
            this.ReleaseDefinitionCheckHighlightedToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionCheckHighlightedToolStripMenuItem.Text = "Highlighted";
            this.ReleaseDefinitionCheckHighlightedToolStripMenuItem.Click += new System.EventHandler(this.ReleaseDefinitionCheckHighlightedToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(134, 6);
            // 
            // ReleaseDefinitionCheckDevToolStripMenuItem
            // 
            this.ReleaseDefinitionCheckDevToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionCheckDevToolStripMenuItem.Name = "ReleaseDefinitionCheckDevToolStripMenuItem";
            this.ReleaseDefinitionCheckDevToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionCheckDevToolStripMenuItem.Text = "Dev";
            this.ReleaseDefinitionCheckDevToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionCheckDevToolStripMenuItem_CheckChanged);
            // 
            // ReleaseDefinitionCheckQAToolStripMenuItem
            // 
            this.ReleaseDefinitionCheckQAToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionCheckQAToolStripMenuItem.Name = "ReleaseDefinitionCheckQAToolStripMenuItem";
            this.ReleaseDefinitionCheckQAToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionCheckQAToolStripMenuItem.Text = "QA";
            this.ReleaseDefinitionCheckQAToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionCheckQAToolStripMenuItem_CheckChanged);
            // 
            // ReleaseDefinitionCheckUATToolStripMenuItem
            // 
            this.ReleaseDefinitionCheckUATToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionCheckUATToolStripMenuItem.Name = "ReleaseDefinitionCheckUATToolStripMenuItem";
            this.ReleaseDefinitionCheckUATToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionCheckUATToolStripMenuItem.Text = "UAT";
            this.ReleaseDefinitionCheckUATToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionCheckUATToolStripMenuItem_CheckChanged);
            // 
            // ReleaseDefinitionCheckPerfToolStripMenuItem
            // 
            this.ReleaseDefinitionCheckPerfToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionCheckPerfToolStripMenuItem.Name = "ReleaseDefinitionCheckPerfToolStripMenuItem";
            this.ReleaseDefinitionCheckPerfToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionCheckPerfToolStripMenuItem.Text = "Perf";
            this.ReleaseDefinitionCheckPerfToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionCheckPerfToolStripMenuItem_CheckChanged);
            // 
            // ReleaseDefinitionCheckProductionToolStripMenuItem
            // 
            this.ReleaseDefinitionCheckProductionToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionCheckProductionToolStripMenuItem.Name = "ReleaseDefinitionCheckProductionToolStripMenuItem";
            this.ReleaseDefinitionCheckProductionToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionCheckProductionToolStripMenuItem.Text = "Production";
            this.ReleaseDefinitionCheckProductionToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionCheckProductionToolStripMenuItem_CheckChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(134, 6);
            // 
            // ReleaseDefinitionCheckDevDRToolStripMenuItem
            // 
            this.ReleaseDefinitionCheckDevDRToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionCheckDevDRToolStripMenuItem.Name = "ReleaseDefinitionCheckDevDRToolStripMenuItem";
            this.ReleaseDefinitionCheckDevDRToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionCheckDevDRToolStripMenuItem.Text = "Dev DR";
            this.ReleaseDefinitionCheckDevDRToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionCheckDevDRToolStripMenuItem_CheckChanged);
            // 
            // ReleaseDefinitionCheckUATDRToolStripMenuItem
            // 
            this.ReleaseDefinitionCheckUATDRToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionCheckUATDRToolStripMenuItem.Name = "ReleaseDefinitionCheckUATDRToolStripMenuItem";
            this.ReleaseDefinitionCheckUATDRToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionCheckUATDRToolStripMenuItem.Text = "UAT DR";
            this.ReleaseDefinitionCheckUATDRToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionCheckUATDRToolStripMenuItem_CheckChanged);
            // 
            // ReleaseDefinitionCheckProdDRToolStripMenuItem
            // 
            this.ReleaseDefinitionCheckProdDRToolStripMenuItem.CheckOnClick = true;
            this.ReleaseDefinitionCheckProdDRToolStripMenuItem.Name = "ReleaseDefinitionCheckProdDRToolStripMenuItem";
            this.ReleaseDefinitionCheckProdDRToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.ReleaseDefinitionCheckProdDRToolStripMenuItem.Text = "Prod DR";
            this.ReleaseDefinitionCheckProdDRToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ReleaseDefinitionCheckProdDRToolStripMenuItem_CheckChanged);
            // 
            // CopyButton
            // 
            this.CopyButton.Location = new System.Drawing.Point(342, 179);
            this.CopyButton.Name = "CopyButton";
            this.CopyButton.Size = new System.Drawing.Size(76, 23);
            this.CopyButton.TabIndex = 14;
            this.CopyButton.Text = "Copy";
            this.CopyButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.CopyButton.UseVisualStyleBackColor = true;
            this.CopyButton.Click += new System.EventHandler(this.CopyButton_Click);
            // 
            // ReleaseDefinitionListDataGridView
            // 
            this.ReleaseDefinitionListDataGridView.AllowUserToAddRows = false;
            this.ReleaseDefinitionListDataGridView.AllowUserToDeleteRows = false;
            this.ReleaseDefinitionListDataGridView.AllowUserToResizeRows = false;
            this.ReleaseDefinitionListDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReleaseDefinitionListDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.ReleaseDefinitionListDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReleaseDefinitionListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ReleaseDefinitionListDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CheckColumn,
            this.IdColumn,
            this.EnabledColumn,
            this.NameColumn,
            this.CommentColumn,
            this.ExtraColumn});
            this.ReleaseDefinitionListDataGridView.ContextMenuStrip = this.ReleaseDefinitionListDataGridViewContextMenuStrip;
            this.ReleaseDefinitionListDataGridView.EnableHeadersVisualStyles = false;
            this.ReleaseDefinitionListDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.ReleaseDefinitionListDataGridView.Location = new System.Drawing.Point(13, 210);
            this.ReleaseDefinitionListDataGridView.Name = "ReleaseDefinitionListDataGridView";
            this.ReleaseDefinitionListDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ReleaseDefinitionListDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ReleaseDefinitionListDataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            this.ReleaseDefinitionListDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.ReleaseDefinitionListDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ReleaseDefinitionListDataGridView.ShowEditingIcon = false;
            this.ReleaseDefinitionListDataGridView.Size = new System.Drawing.Size(475, 278);
            this.ReleaseDefinitionListDataGridView.TabIndex = 15;
            this.ReleaseDefinitionListDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ReleaseDefinitionListDataGridView_CellClick);
            this.ReleaseDefinitionListDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ReleaseDefinitionListDataGridView_CellContentClick);
            this.ReleaseDefinitionListDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ReleaseDefinitionListDataGridView_CellDoubleClick);
            this.ReleaseDefinitionListDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.ReleaseDefinitionListDataGridView_CellFormatting);
            this.ReleaseDefinitionListDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.ReleaseDefinitionListDataGridView_CellValueChanged);
            this.ReleaseDefinitionListDataGridView.Sorted += new System.EventHandler(this.ReleaseDefinitionListDataGridView_Sorted);
            // 
            // CheckColumn
            // 
            this.CheckColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.CheckColumn.HeaderText = "";
            this.CheckColumn.MinimumWidth = 15;
            this.CheckColumn.Name = "CheckColumn";
            this.CheckColumn.Width = 15;
            // 
            // IdColumn
            // 
            this.IdColumn.HeaderText = "Id";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.Visible = false;
            // 
            // EnabledColumn
            // 
            this.EnabledColumn.HeaderText = "";
            this.EnabledColumn.MinimumWidth = 22;
            this.EnabledColumn.Name = "EnabledColumn";
            this.EnabledColumn.ReadOnly = true;
            this.EnabledColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.EnabledColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.EnabledColumn.Width = 22;
            // 
            // NameColumn
            // 
            this.NameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.NameColumn.HeaderText = "Name";
            this.NameColumn.MinimumWidth = 100;
            this.NameColumn.Name = "NameColumn";
            this.NameColumn.ReadOnly = true;
            // 
            // CommentColumn
            // 
            this.CommentColumn.HeaderText = "Comment";
            this.CommentColumn.Name = "CommentColumn";
            this.CommentColumn.ReadOnly = true;
            // 
            // ExtraColumn
            // 
            this.ExtraColumn.HeaderText = "";
            this.ExtraColumn.Name = "ExtraColumn";
            this.ExtraColumn.ReadOnly = true;
            // 
            // ReleaseDefinitionListDataGridViewContextMenuStrip
            // 
            this.ReleaseDefinitionListDataGridViewContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CheckSelectedReleaseDefinitionToolStripMenuItem,
            this.CopyReleaseDefinitionToolStripMenuItem,
            this.ViewReleaseDefinitionToolStripMenuItem});
            this.ReleaseDefinitionListDataGridViewContextMenuStrip.Name = "ReleaseDefinitionListDataGridViewContextMenuStrip";
            this.ReleaseDefinitionListDataGridViewContextMenuStrip.Size = new System.Drawing.Size(265, 70);
            // 
            // CheckSelectedReleaseDefinitionToolStripMenuItem
            // 
            this.CheckSelectedReleaseDefinitionToolStripMenuItem.Name = "CheckSelectedReleaseDefinitionToolStripMenuItem";
            this.CheckSelectedReleaseDefinitionToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.CheckSelectedReleaseDefinitionToolStripMenuItem.Text = "Check Selected Release Definition(s)";
            this.CheckSelectedReleaseDefinitionToolStripMenuItem.Click += new System.EventHandler(this.CheckSelectedReleaseDefinitionToolStripMenuItem_Click);
            // 
            // CopyReleaseDefinitionToolStripMenuItem
            // 
            this.CopyReleaseDefinitionToolStripMenuItem.Name = "CopyReleaseDefinitionToolStripMenuItem";
            this.CopyReleaseDefinitionToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.CopyReleaseDefinitionToolStripMenuItem.Text = "Copy Release Definition Name";
            this.CopyReleaseDefinitionToolStripMenuItem.Click += new System.EventHandler(this.CopyReleaseDefinitionToolStripMenuItem_Click);
            // 
            // ViewReleaseDefinitionToolStripMenuItem
            // 
            this.ViewReleaseDefinitionToolStripMenuItem.Name = "ViewReleaseDefinitionToolStripMenuItem";
            this.ViewReleaseDefinitionToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.ViewReleaseDefinitionToolStripMenuItem.Text = "View Release Definition";
            this.ViewReleaseDefinitionToolStripMenuItem.Click += new System.EventHandler(this.ViewReleaseDefinitionToolStripMenuItem_Click);
            // 
            // ReleaseDefinitionSearchToolTip
            // 
            this.ReleaseDefinitionSearchToolTip.BackColor = System.Drawing.Color.LightYellow;
            this.ReleaseDefinitionSearchToolTip.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.ReleaseDefinitionSearchToolTip_Draw);
            // 
            // ViewSplitButton
            // 
            this.ViewSplitButton.AutoSize = true;
            this.ViewSplitButton.ContextMenuStrip = this.ReleaseDefinitionViewContextMenuStrip;
            this.ViewSplitButton.Location = new System.Drawing.Point(260, 179);
            this.ViewSplitButton.Name = "ViewSplitButton";
            this.ViewSplitButton.Size = new System.Drawing.Size(76, 25);
            this.ViewSplitButton.TabIndex = 13;
            this.ViewSplitButton.Text = "View";
            this.ViewSplitButton.UseVisualStyleBackColor = true;
            // 
            // HighlightSplitButton
            // 
            this.HighlightSplitButton.AutoSize = true;
            this.HighlightSplitButton.BackColor = System.Drawing.SystemColors.Control;
            this.HighlightSplitButton.ContextMenuStrip = this.ReleaseDefinitionHighlightContextMenuStrip;
            this.HighlightSplitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HighlightSplitButton.Location = new System.Drawing.Point(178, 179);
            this.HighlightSplitButton.Name = "HighlightSplitButton";
            this.HighlightSplitButton.Size = new System.Drawing.Size(76, 25);
            this.HighlightSplitButton.TabIndex = 12;
            this.HighlightSplitButton.Text = "Highlight";
            this.HighlightSplitButton.UseVisualStyleBackColor = false;
            // 
            // DisplaySplitButton
            // 
            this.DisplaySplitButton.AutoSize = true;
            this.DisplaySplitButton.ContextMenuStrip = this.ReleaseDefinitionDisplayContextMenuStrip;
            this.DisplaySplitButton.Location = new System.Drawing.Point(96, 179);
            this.DisplaySplitButton.Name = "DisplaySplitButton";
            this.DisplaySplitButton.Size = new System.Drawing.Size(76, 25);
            this.DisplaySplitButton.TabIndex = 11;
            this.DisplaySplitButton.Text = "Display";
            this.DisplaySplitButton.UseVisualStyleBackColor = true;
            // 
            // CheckSplitButton
            // 
            this.CheckSplitButton.AutoSize = true;
            this.CheckSplitButton.ContextMenuStrip = this.ReleaseDefinitionCheckContextMenuStrip;
            this.CheckSplitButton.Location = new System.Drawing.Point(14, 179);
            this.CheckSplitButton.Name = "CheckSplitButton";
            this.CheckSplitButton.Size = new System.Drawing.Size(76, 25);
            this.CheckSplitButton.TabIndex = 10;
            this.CheckSplitButton.Text = "Check";
            this.CheckSplitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // ReleaseDefinitionSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 500);
            this.ControlBox = false;
            this.Controls.Add(this.ViewSplitButton);
            this.Controls.Add(this.ReleaseDefinitionStatusPanel);
            this.Controls.Add(this.HighlightSplitButton);
            this.Controls.Add(this.DisplaySplitButton);
            this.Controls.Add(this.CheckSplitButton);
            this.Controls.Add(this.CopyButton);
            this.Controls.Add(this.ReleaseDefinitionListDataGridView);
            this.Controls.Add(this.SearchCriteriaGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReleaseDefinitionSearchForm";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.SearchCriteriaGroupBox.ResumeLayout(false);
            this.SearchCriteriaGroupBox.PerformLayout();
            this.ReleaseDefinitionNameFilterContextMenuStrip.ResumeLayout(false);
            this.ReleaseDefinitionViewContextMenuStrip.ResumeLayout(false);
            this.ReleaseDefinitionStatusPanel.ResumeLayout(false);
            this.ReleaseDefinitionRetrievalStatusPanel.ResumeLayout(false);
            this.ReleaseDefinitionRetrievalStatusPanel.PerformLayout();
            this.ReleaseDefinitionMatchStatusPanel.ResumeLayout(false);
            this.ReleaseDefinitionMatchStatusPanel.PerformLayout();
            this.ReleaseDefinitionHighlightContextMenuStrip.ResumeLayout(false);
            this.ReleaseDefinitionDisplayContextMenuStrip.ResumeLayout(false);
            this.ReleaseDefinitionCheckContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ReleaseDefinitionListDataGridView)).EndInit();
            this.ReleaseDefinitionListDataGridViewContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox SearchCriteriaGroupBox;
        private System.Windows.Forms.CheckBox ExactMatchCheckBox;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.RichTextBox ReleaseDefinitionNameFilterTextBox;
        private SplitButton ViewSplitButton;
        private System.Windows.Forms.ContextMenuStrip ReleaseDefinitionViewContextMenuStrip;
        private System.Windows.Forms.Panel ReleaseDefinitionStatusPanel;
        private System.Windows.Forms.Panel ReleaseDefinitionRetrievalStatusPanel;
        private System.Windows.Forms.ProgressBar ReleaseDefinitionRetrievalProgressBar;
        private System.Windows.Forms.Label ReleaseDefinitionRetrievalStatusLabel;
        private System.Windows.Forms.Panel ReleaseDefinitionMatchStatusPanel;
        private System.Windows.Forms.Label ReleaseDefinitionMatchStatusLabel;
        private SplitButton HighlightSplitButton;
        private System.Windows.Forms.ContextMenuStrip ReleaseDefinitionHighlightContextMenuStrip;
        private SplitButton DisplaySplitButton;
        private System.Windows.Forms.ContextMenuStrip ReleaseDefinitionDisplayContextMenuStrip;
        private SplitButton CheckSplitButton;
        private System.Windows.Forms.ContextMenuStrip ReleaseDefinitionCheckContextMenuStrip;
        private System.Windows.Forms.Button CopyButton;
        private System.Windows.Forms.DataGridView ReleaseDefinitionListDataGridView;
        private System.Windows.Forms.ContextMenuStrip ReleaseDefinitionNameFilterContextMenuStrip;
        private System.Windows.Forms.ToolTip ReleaseDefinitionSearchToolTip;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionHighlightNoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionHighlightDevToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionHighlightQAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionHighlightUATToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionHighlightPerfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionHighlightProductionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionHighlightDevDRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionHighlightUATDRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionHighlightProdDRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionDisplayAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionDisplayHighlightedToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionDisplayDevToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionDisplayQAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionDisplayUATToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionDisplayPerfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionDisplayProductionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionDisplayDevDRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionDisplayUATDRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionDisplayProdDRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionCheckAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionCheckNoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionCheckHighlightedToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionCheckDevToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionCheckQAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionCheckUATToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionCheckPerfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionCheckProductionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionCheckDevDRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionCheckUATDRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionCheckProdDRToolStripMenuItem;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewImageColumn EnabledColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CommentColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExtraColumn;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionNameFilterCutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionNameFilterCopyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionNameFilterPasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionNameFilterDeleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionNameFilterSelectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionViewAllToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip ReleaseDefinitionListDataGridViewContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CheckSelectedReleaseDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CopyReleaseDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ViewReleaseDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReleaseDefinitionDisplayChecedToolStripMenuItem;
    }
}