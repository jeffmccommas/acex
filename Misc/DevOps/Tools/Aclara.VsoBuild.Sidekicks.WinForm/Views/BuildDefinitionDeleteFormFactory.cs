﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using System;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build definition delete form factory.
    /// </summary>
    public static class BuildDefinitionDeleteFormFactory
    {

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <returns></returns>
        public static BuildDefinitionDeleteForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                           SidekickConfiguration sidekickConfiguration,
                                                           BuildDefinitionSearchForm buildDefinitionSearchForm)
        {

            BuildDefinitionDeleteForm result = null;
            BuildDefinitionDeletePresenter buildDefinitionClonePresenter = null;

            try
            {
                result = new BuildDefinitionDeleteForm(sidekickConfiguration, buildDefinitionSearchForm);
                buildDefinitionClonePresenter = new BuildDefinitionDeletePresenter(teamProjectInfo, sidekickConfiguration, result);
                result.BuildDefinitionDeletePresenter = buildDefinitionClonePresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


    }
}
