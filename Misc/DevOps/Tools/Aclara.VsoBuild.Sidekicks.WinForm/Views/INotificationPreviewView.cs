﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public interface INotificationPreviewView
    {

        string NotificationPreviewText { get; set; }
    }
}
