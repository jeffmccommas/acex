﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build definition clone view - Interface.
    /// </summary>
    public interface IBuildDefinitionCloneView
    {
        string SidekickName { get; }

        string SidekickDescription { get; }


        SidekickContainerTypes.SidekickWarningLevel SidekickWarningLevel { get; }

        string SourceBranchName { get; set; }

        string SourceBuildDefinitionNamePrefix{ get; set; }

        string TargetBranchName { get; set; }

        string TargetBuildDefinitionNamePrefix { get; set; }

        string BackgroundTaskStatus { get; set; }

        string ApplyButtonText { get; set; }

        void ApplyButtonEnable(bool enable);

        bool IsSidekickBusy { get; }

        void RetrieveRemainingDefinitions();

        void TeamProjectChanged();

        void UpdateProgressCloneBuildDefinition(int buildDefinitionClonedCount, int buildDefinitionTotalCount);

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);
    }

}
