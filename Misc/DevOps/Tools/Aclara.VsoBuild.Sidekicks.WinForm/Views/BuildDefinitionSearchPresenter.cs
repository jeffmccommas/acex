﻿using Aclara.Vso.Build.Client;
using Aclara.Vso.Build.Client.Events;
using Aclara.Vso.Build.Client.Models;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using Aclara.VsoBuild.Sidekicks.WinForm.Utility;
using Microsoft.TeamFoundation.Build.WebApi;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Build definition search presenter.
    /// </summary>
    public class BuildDefinitionSearchPresenter
    {

        #region Private Constants

        private const string TeamProjectCollectionNameDefaultValue = "";
        private const string TeamProjectNameDefaultValue = "";

        private const string Logging_SidekickName = "BuildDefinitionSearchUIComponent";
        private const string Logging_SidekickDescription = "Build Definition Search";

        #endregion

        #region Private Data Members

        private CustomLogger _logger = null;
        private BuildDefinitionSelectorList _buildDefinitionSelectorList;
        private IBuildDefinitionSearchView _buildDefinitionSearchView = null;
        private BuildDefinitionManager _buildDefinitionManager;
        private SidekickConfiguration _sidekickConfiguration = null;
        private ITeamProjectInfo _teamProjectInfo = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;
        private BuildDefinitionRetrievalMode _buildDefinitionRetrievalMode;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Build definition selector list.
        /// </summary>
        public BuildDefinitionSelectorList BuildDefinitionSelectorList
        {
            get { return _buildDefinitionSelectorList; }
            set { _buildDefinitionSelectorList = value; }
        }

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               Logging_SidekickName,
                                               Logging_SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Build defintion view.
        /// </summary>
        public IBuildDefinitionSearchView BuildDefinitionSearchView
        {
            get
            {
                return _buildDefinitionSearchView;
            }
            set
            {
                _buildDefinitionSearchView = value;
            }
        }

        /// <summary>
        /// Property: Team project collection default.
        /// </summary>
        public string TeamProjectCollectionNameDefault
        {
            get
            {
                return TeamProjectCollectionNameDefaultValue;
            }
        }

        /// <summary>
        /// Property: Team project name default.
        /// </summary>
        public string TeamProjectNameDefault
        {
            get
            {
                return TeamProjectNameDefaultValue;
            }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Build definition manager.
        /// </summary>
        public BuildDefinitionManager BuildDefinitionManager
        {
            get { return _buildDefinitionManager; }
            set { _buildDefinitionManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Build definition retrieval mode.
        /// </summary>
        public BuildDefinitionRetrievalMode BuildDefinitionRetrievalMode
        {
            get { return _buildDefinitionRetrievalMode; }
            set { _buildDefinitionRetrievalMode = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchView"></param>
        public BuildDefinitionSearchPresenter(ITeamProjectInfo teamProjectInfo,
                                              SidekickConfiguration sidekickConfiguration,
                                              IBuildDefinitionSearchView buildDefinitionSearchView)
        {
            this.TeamProjectInfo = teamProjectInfo;
            this.SidekickConfiguration = sidekickConfiguration;
            this.BuildDefinitionSearchView = buildDefinitionSearchView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <remarks>
        /// Hide default constructor to enforce dependency injection.
        /// </remarks>
        private BuildDefinitionSearchPresenter()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {
            try
            {

                this.BuildDefinitionSearchView.EnableControlsDependantOnTeamProject(false);
                this.BuildDefinitionSearchView.EnableControlsDependantOnSearch(false);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Team project collection name changed.
        /// </summary>
        public void TeamProjectCollectionNameChanged()
        {

            try
            {

                this.BuildDefinitionSearchView.EnableControlsDependantOnTeamProject(true);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Team project name changed.
        /// </summary>
        public void TeamProjectNameChanged()
        {

            try
            {

                this.BuildDefinitionSearchView.EnableControlsDependantOnTeamProject(true);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalBuildDuration;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalBuildDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:

                        this.BuildDefinitionSearchView.BackgroundTaskCancelled("Retrieve build definition list canceled.");

                        this.Logger.Info(string.Format("Retrieve build definition list canceled."));


                        //Log background task completed log entry.
                        this.Logger.Info(string.Format("[*] Retrieve build definition list completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                        totalBuildDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:

                        this.BuildDefinitionSearchView.BackgroundTaskCompleted("");


                        //Log background task completed log entry.

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    //Handle operation canceled exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception;
                                        this.Logger.Error(innerException, string.Format("Operation cancelled."));
                                    }
                                    else
                                    {
                                        this.Logger.Error(string.Format("Operation cancelled."));
                                    }
                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception;
                                        this.Logger.Error(innerException, string.Format("Request failed."));
                                    }
                                    else
                                    {
                                        this.Logger.Error(string.Format("Operation cancelled."));
                                    }

                                }
                            }
                        }

                        this.Logger.Info(string.Format("[*] Retrieve build definition list completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                        totalBuildDuration));
                        break;

                    case TaskStatus.RanToCompletion:

                        this.BuildDefinitionSearchView.BackgroundTaskCompleted("Get build definition list request completed.");
                        if (this.BuildDefinitionRetrievalMode == BuildDefinitionRetrievalMode.Reset)
                        {
                            this.BuildDefinitionSearchView.BuildDefinitionSelectorList = this.BuildDefinitionSelectorList;
                        }
                        else if (this.BuildDefinitionRetrievalMode == BuildDefinitionRetrievalMode.RetrieveRemaining)
                        {
                            List<BuildDefinitionSelector> list1 = new List<BuildDefinitionSelector>(this.BuildDefinitionSearchView.BuildDefinitionSelectorList);
                            List<BuildDefinitionSelector> list2 = new List<BuildDefinitionSelector>(this.BuildDefinitionSelectorList);
                            List<BuildDefinitionSelector> list3 = list1.Union(list2, new BuildDefinitionSelectorComparer()).ToList<BuildDefinitionSelector>();
                            this.BuildDefinitionSearchView.BuildDefinitionSelectorList.Clear();
                            this.BuildDefinitionSearchView.BuildDefinitionSelectorList.AddRange(list3);
                        }
                        this.BuildDefinitionSearchView.BackgroundTaskStatus = "";

                        if (this.BuildDefinitionRetrievalMode == BuildDefinitionRetrievalMode.Reset)
                        {
                            this.BuildDefinitionSearchView.PerformSearch();
                        }

                        this.BuildDefinitionRetrievalMode = BuildDefinitionRetrievalMode.Unspecified;

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Deterimine if background task is running.
        /// </summary>
        public bool IsBackgroundTaskRunning()
        {
            bool result = false;

            try
            {
                if (this.BackgroundTask != null &&
                    this.BackgroundTask.Status == TaskStatus.Running)
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Cancel background task.
        /// </summary>
        public void CancelBackgroundTask()
        {
            try
            {
                if (this.BackgroundTask != null &&
                    this.BackgroundTask.Status == TaskStatus.Running)
                {
                    this.BackgroundTaskCancellationTokenSource.Cancel();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve build definition selector list async.
        /// </summary>
        /// <param name="buildDefinitionNamePattern"></param>
        /// <param name="buildDefinitionList"></param>
        /// <param name="buildDefinitionRetrievalMode"></param>
        public void GetBuildDefinitionSelectorListAsync(string buildDefinitionNamePattern,
                                                        BuildDefinitionList buildDefinitionList,
                                                        BuildDefinitionRetrievalMode buildDefinitionRetrievalMode)
        {

            CancellationToken cancellationToken;

            try
            {

                //Attempting to start task that is already running.
                if (this.BackgroundTask != null &&
                    this.BackgroundTask.Status == TaskStatus.Running &&
                    buildDefinitionRetrievalMode == BuildDefinitionRetrievalMode.Reset)
                {
                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                    this.BuildDefinitionRetrievalMode = buildDefinitionRetrievalMode;

                    this.BuildDefinitionManager = this.CreateBuildDefinitionManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());


                    this.BuildDefinitionManager.BuildDefinintionListRetrieved += OnBuildDefinintionListRetrieved;

                    this.BackgroundTask = new Task(() => this.BuildDefinitionManager.GetBuildDefinitionList(buildDefinitionNamePattern,
                                                                                                            buildDefinitionList,
                                                                                                            buildDefinitionRetrievalMode,
                                                                                                            cancellationToken),
                                                                                                            cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }

        }

        /// <summary>
        /// Convert build Definition list into build definition selector list.
        /// </summary>
        /// <param name="buildDefinitionList"></param>
        /// <returns></returns>
        public BuildDefinitionSelectorList ConvertBuildDefinitionList(BuildDefinitionList buildDefinitionList)
        {
            BuildDefinitionSelectorList result = null;
            BuildDefinitionSelector buildDefinitionSelector = null;

            try
            {
                result = new BuildDefinitionSelectorList();
                foreach (BuildDefinition buildDefinition in buildDefinitionList)
                {
                    buildDefinitionSelector = new BuildDefinitionSelector();

                    buildDefinitionSelector.Selected = false;
                    buildDefinitionSelector.Id = buildDefinition.Id;
                    buildDefinitionSelector.Name = buildDefinition.Name;
                    buildDefinitionSelector.ScheduleStartTime = ConvertToText.GetFormattedScheduleFromBuildDefinition(buildDefinition);
                    if (buildDefinition.QueueStatus == DefinitionQueueStatus.Enabled)
                    {
                        buildDefinitionSelector.Enabled = true;
                    }
                    else
                    {
                        buildDefinitionSelector.Enabled = false;
                    }
                    buildDefinitionSelector.QueueName = buildDefinition.Queue.Name;

                    buildDefinitionSelector.BuildDefinition = buildDefinition;

                    result.Add(buildDefinitionSelector);
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }
        #endregion

        #region Protected Methods

        /// <summary>
        /// Create build definition manager.
        /// </summary>
        /// <returns></returns>
        protected BuildDefinitionManager CreateBuildDefinitionManager()
        {
            BuildDefinitionManager result = null;

            try
            {
                result = BuildDefinitionManagerFactory.CreateBuildDefinitionManager(this.TeamProjectInfo.TeamProjectCollectionUri,
                                                                                    this.TeamProjectInfo.TeamProjectCollectionVsrmUri,
                                                                                    this.TeamProjectInfo.TeamProjectName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIUserProfileName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIPassword);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: Build definitionlist retrieved event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="getBuildDefinitionListEventArgs"></param>
        protected void OnBuildDefinintionListRetrieved(Object sender,
                                                       GetBuildDefinitionListEventArgs getBuildDefinitionListEventArgs)
        {
            const string BackgroundTaskStatus = "Retrieved {0} of {1} build definitions.";

            int buildDefinitionListTotalCount = 0;
            int buildDefinitionListRetrievedCount = 0;
            string backgroundTaskStatus = string.Empty;

            try
            {

                if (getBuildDefinitionListEventArgs.BuildDefinitionList != null)
                {
                    this.BuildDefinitionSelectorList = this.ConvertBuildDefinitionList(getBuildDefinitionListEventArgs.BuildDefinitionList);
                }

                buildDefinitionListRetrievedCount = getBuildDefinitionListEventArgs.BuildDefinitionRetrievedCount;
                buildDefinitionListTotalCount = getBuildDefinitionListEventArgs.BuildDefinitionTotalCount;

                backgroundTaskStatus = string.Format(BackgroundTaskStatus,
                                                     buildDefinitionListRetrievedCount,
                                                     buildDefinitionListTotalCount);
                this.BuildDefinitionSearchView.BackgroundTaskStatus = backgroundTaskStatus;
                this.BuildDefinitionSearchView.UpdateBuildDefinitionRetrievalStatus(buildDefinitionListRetrievedCount,
                                                                                    buildDefinitionListTotalCount,
                                                                                    false);

            }
            catch (Exception)
            {
                throw;
            }


        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            const string OperationStatus_Cancelled = "Operation cancelled.(Unobserved)";

            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            this.Logger.Error(innerException, string.Format(OperationStatus_Cancelled));
                        }
                        else
                        {
                            this.Logger.Error(string.Format(OperationStatus_Cancelled));
                        }
                    }
                    eventArgs.SetObserved();

                    return;
                }
                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

    }
}
