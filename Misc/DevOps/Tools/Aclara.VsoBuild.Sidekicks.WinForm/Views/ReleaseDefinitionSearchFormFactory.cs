﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public static class ReleaseDefinitionSearchFormFactory
    {
        /// <summary>
        /// Create release definition search form.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="sidekickCoaction"></param>
        /// <returns></returns>
         public static ReleaseDefinitionSearchForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                           SidekickConfiguration sidekickConfiguration,
                                                          ISidekickCoaction sidekickCoaction)
        {

            ReleaseDefinitionSearchForm result = null;
            ReleaseDefinitionSearchPresenter releaseDefinitionSearchPresenter = null;

            try
            {

                releaseDefinitionSearchPresenter = new ReleaseDefinitionSearchPresenter(teamProjectInfo, sidekickConfiguration, result);
                result = new ReleaseDefinitionSearchForm(releaseDefinitionSearchPresenter, sidekickConfiguration, sidekickCoaction);

                result.ReleaseDefinitionSearchPresenter = releaseDefinitionSearchPresenter;
                releaseDefinitionSearchPresenter.ReleaseDefinitionSearchView = result;
                result.TopLevel = false;

                releaseDefinitionSearchPresenter.Initialize();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
