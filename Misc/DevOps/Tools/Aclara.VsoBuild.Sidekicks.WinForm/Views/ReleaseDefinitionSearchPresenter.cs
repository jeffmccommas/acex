﻿using Aclara.Tools.Common;
using Aclara.Tools.Common.StatusManagement;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using Aclara.Vso.Build.Client;
using Aclara.Vso.Build.Client.Events;
using Aclara.Vso.Build.Client.Models;
using Microsoft.VisualStudio.Services.ReleaseManagement.WebApi;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Release definition search presenter.
    /// </summary>
    public class ReleaseDefinitionSearchPresenter
    {
        #region Private Constants

        private const string TeamProjectCollectionNameDefaultValue = "";
        private const string TeamProjectNameDefaultValue = "";

        private const string Logging_SidekickName = "ReleaseDefinitionSearchUIComponent";
        private const string Logging_SidekickDescription = "Release Definition Search";

        #endregion

        #region Private Data Members

        private CustomLogger _logger = null;
        private ReleaseDefinitionSelectorList _releaseDefinitionSelectorList;
        private IReleaseDefinitionSearchView _releaseDefinitionSearchView = null;
        private SidekickConfiguration _sidekickConfiguration = null;
        private ReleaseDefinitionManager _releaseDefinitionManager;
        private ITeamProjectInfo _teamProjectInfo = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;
        private ReleaseDefinitionRetrievalMode _releaseDefinitionRetrievalMode;

        #endregion

        #region Public Delegates
        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               Logging_SidekickName,
                                               Logging_SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Release definition selector list.
        /// </summary>
        public ReleaseDefinitionSelectorList ReleaseDefinitionSelectorList
        {
            get { return _releaseDefinitionSelectorList; }
            set { _releaseDefinitionSelectorList = value; }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: release defintion view.
        /// </summary>
        public IReleaseDefinitionSearchView ReleaseDefinitionSearchView
        {
            get
            {
                return _releaseDefinitionSearchView;
            }
            set
            {
                _releaseDefinitionSearchView = value;
            }
        }

        /// <summary>
        /// Property: Team project collection default.
        /// </summary>
        public string TeamProjectCollectionNameDefault
        {
            get
            {
                return TeamProjectCollectionNameDefaultValue;
            }
        }

        /// <summary>
        /// Property: Team project name default.
        /// </summary>
        public string TeamProjectNameDefault
        {
            get
            {
                return TeamProjectNameDefaultValue;
            }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Release definition manager.
        /// </summary>
        public ReleaseDefinitionManager ReleaseDefinitionManager
        {
            get { return _releaseDefinitionManager; }
            set { _releaseDefinitionManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: release definition retrieval mode.
        /// </summary>
        public ReleaseDefinitionRetrievalMode ReleaseDefinitionRetrievalMode
        {
            get { return _releaseDefinitionRetrievalMode; }
            set { _releaseDefinitionRetrievalMode = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="releaseDefinitionSearchView"></param>
        public ReleaseDefinitionSearchPresenter(ITeamProjectInfo teamProjectInfo,
                                                SidekickConfiguration sidekickConfiguration,
                                                IReleaseDefinitionSearchView releaseDefinitionSearchView)
        {
            this.TeamProjectInfo = teamProjectInfo;
            this.SidekickConfiguration = sidekickConfiguration;
            this.ReleaseDefinitionSearchView = releaseDefinitionSearchView;
        }

        #endregion

        #region Protected Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        protected ReleaseDefinitionSearchPresenter()
        {

        }

        #endregion

        #region Public methods

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {
            try
            {

                this.ReleaseDefinitionSearchView.EnableControlsDependantOnTeamProject(false);
                this.ReleaseDefinitionSearchView.EnableControlsDependantOnSearch(false);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Team project collection name changed.
        /// </summary>
        public void TeamProjectCollectionNameChanged()
        {

            try
            {

                this.ReleaseDefinitionSearchView.EnableControlsDependantOnTeamProject(true);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Team project name changed.
        /// </summary>
        public void TeamProjectNameChanged()
        {

            try
            {

                this.ReleaseDefinitionSearchView.EnableControlsDependantOnTeamProject(true);

            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalBuildDuration;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalBuildDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case System.Threading.Tasks.TaskStatus.Canceled:

                        this.ReleaseDefinitionSearchView.BackgroundTaskCancelled("Retrieve build definition list canceled.");

                        this.Logger.Info(string.Format("Retrieve build definition list canceled."));


                        //Log background task completed log entry.
                        this.Logger.Info(string.Format("[*] Retrieve build definition list completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                        totalBuildDuration));
                        break;

                    case System.Threading.Tasks.TaskStatus.Created:
                        break;

                    case System.Threading.Tasks.TaskStatus.Faulted:

                        this.ReleaseDefinitionSearchView.BackgroundTaskCompleted("");


                        //Log background task completed log entry.

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    //Handle operation canceled exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception;
                                        this.Logger.Error(innerException, string.Format("Operation cancelled."));
                                    }
                                    else
                                    {
                                        this.Logger.Error(string.Format("Operation cancelled."));
                                    }
                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception;
                                        this.Logger.Error(innerException, string.Format("Request failed."));
                                    }
                                    else
                                    {
                                        this.Logger.Error(string.Format("Operation cancelled."));
                                    }

                                }
                            }
                        }

                        this.Logger.Info(string.Format("[*] Retrieve build definition list completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                        totalBuildDuration));
                        break;

                    case System.Threading.Tasks.TaskStatus.RanToCompletion:

                        this.ReleaseDefinitionSearchView.BackgroundTaskCompleted("Get build definition list request completed.");
                        if (this.ReleaseDefinitionRetrievalMode == ReleaseDefinitionRetrievalMode.Reset)
                        {
                            this.ReleaseDefinitionSearchView.ReleaseDefinitionSelectorList = this.ReleaseDefinitionSelectorList;
                        }
                        else if (this.ReleaseDefinitionRetrievalMode == ReleaseDefinitionRetrievalMode.RetrieveRemaining)
                        {
                            List<ReleaseDefinitionSelector> list1 = new List<ReleaseDefinitionSelector>(this.ReleaseDefinitionSearchView.ReleaseDefinitionSelectorList);
                            List<ReleaseDefinitionSelector> list2 = new List<ReleaseDefinitionSelector>(this.ReleaseDefinitionSelectorList);
                            List<ReleaseDefinitionSelector> list3 = list1.Union(list2, new ReleaseDefinitionSelectorComparer()).ToList<ReleaseDefinitionSelector>();
                            this.ReleaseDefinitionSearchView.ReleaseDefinitionSelectorList.Clear();
                            this.ReleaseDefinitionSearchView.ReleaseDefinitionSelectorList.AddRange(list3);
                        }
                        this.ReleaseDefinitionSearchView.BackgroundTaskStatus = "";

                        if (this.ReleaseDefinitionRetrievalMode == ReleaseDefinitionRetrievalMode.Reset)
                        {
                            this.ReleaseDefinitionSearchView.PerformSearch();
                        }

                        this.ReleaseDefinitionRetrievalMode = ReleaseDefinitionRetrievalMode.Unspecified;

                        break;

                    case System.Threading.Tasks.TaskStatus.Running:
                        break;
                    case System.Threading.Tasks.TaskStatus.WaitingForActivation:
                        break;
                    case System.Threading.Tasks.TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case System.Threading.Tasks.TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Deterimine if background task is running.
        /// </summary>
        public bool IsBackgroundTaskRunning()
        {
            bool result = false;

            try
            {
                if (this.BackgroundTask != null &&
                    this.BackgroundTask.Status == System.Threading.Tasks.TaskStatus.Running)
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Cancel background task.
        /// </summary>
        public void CancelBackgroundTask()
        {
            try
            {
                if (this.BackgroundTask != null &&
                    this.BackgroundTask.Status == System.Threading.Tasks.TaskStatus.Running)
                {
                    this.BackgroundTaskCancellationTokenSource.Cancel();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Retrieve release definition selector list async.
        /// </summary>
        /// <param name="releaseDefinitionNamePattern"></param>
        /// <param name="releaseDefinitionList"></param>
        /// <param name="releaseDefinitionRetrievalMode"></param>
        public void GetReleaseDefinitionSelectorListAsync(string releaseDefinitionNamePattern,
                                                          ReleaseDefinitionList releaseDefinitionList,
                                                          ReleaseDefinitionRetrievalMode releaseDefinitionRetrievalMode)
        {

            CancellationToken cancellationToken;

            try
            {

                //Attempting to start task that is already running.
                if (this.BackgroundTask != null &&
                    this.BackgroundTask.Status == System.Threading.Tasks.TaskStatus.Running &&
                    releaseDefinitionRetrievalMode == ReleaseDefinitionRetrievalMode.Reset)
                {
                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                    this.ReleaseDefinitionRetrievalMode = releaseDefinitionRetrievalMode;

                    this.ReleaseDefinitionManager = this.CreateReleaseDefinitionManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());


                    this.ReleaseDefinitionManager.ReleaseDefinintionListRetrieved += OnReleaseDefinintionListRetrieved;

                    this.BackgroundTask = new Task(() => this.ReleaseDefinitionManager.GetReleaseDefinitionList(releaseDefinitionNamePattern,
                                                                                                                releaseDefinitionList,
                                                                                                                releaseDefinitionRetrievalMode,
                                                                                                                cancellationToken),
                                                                                                                cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }

        }

        /// <summary>
        /// Convert release Definition list into release definition selector list.
        /// </summary>
        /// <param name="releaseDefinitionList"></param>
        /// <returns></returns>
        public ReleaseDefinitionSelectorList ConvertReleaseDefinitionList(ReleaseDefinitionList releaseDefinitionList)
        {
            ReleaseDefinitionSelectorList result = null;
            ReleaseDefinitionSelector releaseDefinitionSelector = null;

            try
            {
                result = new ReleaseDefinitionSelectorList();
                foreach (ReleaseDefinition releaseDefinition in releaseDefinitionList)
                {
                    releaseDefinitionSelector = new ReleaseDefinitionSelector();

                    releaseDefinitionSelector.Selected = false;
                    releaseDefinitionSelector.Id = releaseDefinition.Id;
                    releaseDefinitionSelector.Name = releaseDefinition.Name;
                    releaseDefinition.Comment = releaseDefinition.Comment;

                    releaseDefinitionSelector.ReleaseDefinition = releaseDefinition;

                    result.Add(releaseDefinitionSelector);
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Create release definition manager.
        /// </summary>
        /// <returns></returns>
        protected ReleaseDefinitionManager CreateReleaseDefinitionManager()
        {
            ReleaseDefinitionManager result = null;

            try
            {
                result = ReleaseDefinitionManagerFactory.CreateReleaseDefinitionManager(this.TeamProjectInfo.TeamProjectCollectionUri,
                                                                                        this.TeamProjectInfo.TeamProjectCollectionVsrmUri,
                                                                                        this.TeamProjectInfo.TeamProjectName,
                                                                                        this.TeamProjectInfo.BasicAuthRestAPIUserProfileName,
                                                                                        this.TeamProjectInfo.BasicAuthRestAPIPassword);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: release definition list retrieved event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="getReleaseDefinitionListEventArgs"></param>
        protected void OnReleaseDefinintionListRetrieved(Object sender,
                                                         GetReleaseDefinitionListEventArgs getReleaseDefinitionListEventArgs)
        {
            const string BackgroundTaskStatus = "Retrieved {0} of {1} release definitions.";

            int releaseDefinitionListTotalCount = 0;
            int releaseDefinitionListRetrievedCount = 0;
            string backgroundTaskStatus = string.Empty;

            try
            {

                if (getReleaseDefinitionListEventArgs.ReleaseDefinitionList != null)
                {
                    this.ReleaseDefinitionSelectorList = this.ConvertReleaseDefinitionList(getReleaseDefinitionListEventArgs.ReleaseDefinitionList);
                }

                releaseDefinitionListRetrievedCount = getReleaseDefinitionListEventArgs.ReleaseDefinitionRetrievedCount;
                releaseDefinitionListTotalCount = getReleaseDefinitionListEventArgs.ReleaseDefinitionTotalCount;

                backgroundTaskStatus = string.Format(BackgroundTaskStatus,
                                                     releaseDefinitionListRetrievedCount,
                                                     releaseDefinitionListTotalCount);
                this.ReleaseDefinitionSearchView.BackgroundTaskStatus = backgroundTaskStatus;
                this.ReleaseDefinitionSearchView.UpdateReleaseDefinitionRetrievalStatus(releaseDefinitionListRetrievedCount,
                                                                                        releaseDefinitionListTotalCount,
                                                                                        false);

            }
            catch (Exception)
            {
                throw;
            }


        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            const string OperationStatus_Cancelled = "Operation cancelled.(Unobserved)";

            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            this.Logger.Error(innerException, string.Format(OperationStatus_Cancelled));
                        }
                        else
                        {
                            this.Logger.Error(string.Format(OperationStatus_Cancelled));
                        }
                    }
                    eventArgs.SetObserved();

                    return;
                }
                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private methods
        #endregion

    }
}
