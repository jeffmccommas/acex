﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class ConfirmationForm : Form, IConfirmationView
    {

        #region Private Constants

        private const string TypedResponseValueMustBeSpecified = "Confirmation response is required.";

        #endregion


        #region Private Data Members

        ConfirmationPresenter _ConfirmationPresenter = null;
        private string _expectedTypedResponseValue;
        private bool _requireTypedResponse;
        private bool _displayWarning;

        #endregion

        #region Public Properties
        /// <summary>
        /// Property: Confirmation presenter.
        /// </summary>
        public ConfirmationPresenter ConfirmationPresenter
        {
            get { return _ConfirmationPresenter; }
            set { _ConfirmationPresenter = value; }
        }

        /// <summary>
        /// Property: Pending action message.
        /// </summary>
        public string PendingActionMessage
        {
            get { return this.PendingActionMessageLabel.Text; }
            set { this.PendingActionMessageLabel.Text = value; }
        }

        /// <summary>
        /// Property: Confirmation message.
        /// </summary>
        public string ConfirmationMessage
        {
            get { return this.ConfirmationMessageLabel.Text; }
            set { this.ConfirmationMessageLabel.Text = value; }
        }

        /// <summary>
        /// Property: Expected typed response value.
        /// </summary>
        public string ExpectedTypedResponseValue
        {
            get { return _expectedTypedResponseValue; }
            set { _expectedTypedResponseValue = value; }
        }

        /// <summary>
        /// Property: Typed response value.
        /// </summary>
        public string TypedResponseValue
        {
            get
            {
                return this.TypedResponseValueTextBox.Text;
            }

            set
            {
                this.TypedResponseValueTextBox.Text = value;
            }
        }

        /// <summary>
        /// Property: Require typed response.
        /// </summary>
        public bool RequireTypedResponse
        {
            get { return _requireTypedResponse; }
            set
            {
                _requireTypedResponse = value;
            }
        }

        /// <summary>
        /// Property: Display warning.
        /// </summary>
        public bool DisplayWarning
        {
            get { return _displayWarning; }
            set { _displayWarning = value; }
        }

        /// <summary>
        /// Property: Warning message.
        /// </summary>
        public string WarningMessage
        {
            get { return this.WarningMessageLabel.Text; }
            set { this.WarningMessageLabel.Text = value; }
        }

        #endregion

        #region Public Constructors

        public ConfirmationForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: OKButton - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OKButton_Click(object sender, EventArgs e)
        {
            this.ValidationMessageLabel.Text = string.Empty;

            if (this.RequireTypedResponse == true &&
                this.TypedResponseValueTextBox.Text.ToLower() != this.ExpectedTypedResponseValue.ToLower())
            {
                this.ValidationMessageLabel.Text = TypedResponseValueMustBeSpecified;
                this.DialogResult = DialogResult.None;
                return;
            }

            this.Close();
        }

        /// <summary>
        /// Event Handler: CancelButton - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Event Handler: Confirmation form - Shown.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfirmationForm_Shown(object sender, EventArgs e)
        {
            try
            {
                this.WarningPanel.Visible = false;

                if (this.DisplayWarning == true)
                {
                    this.WarningPanel.Visible = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Typed response value text box - Enter.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TypedResponseValueTextBox_Enter(object sender, EventArgs e)
        {
            try
            {
                this.ValidationMessageLabel.Text = string.Empty;

            }
            catch (Exception)
            {
                throw;
            }
        }
        
        #endregion

    }
}
