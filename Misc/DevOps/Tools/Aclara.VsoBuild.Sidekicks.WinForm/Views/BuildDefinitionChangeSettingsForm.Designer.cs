﻿namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    partial class BuildDefinitionChangeSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.DefaultAgentPoolQueuePanel = new System.Windows.Forms.Panel();
            this.RefreshAgentPoolQueueButton = new System.Windows.Forms.Button();
            this.DefaultAgentPoolQueueComboBox = new System.Windows.Forms.ComboBox();
            this.DefaultAgentPoolQueueLabel = new System.Windows.Forms.Label();
            this.SettingComboBox = new System.Windows.Forms.ComboBox();
            this.SettingLabel = new System.Windows.Forms.Label();
            this.TriggersSchedulePanel = new System.Windows.Forms.Panel();
            this.AddScheduleWarningLabel = new System.Windows.Forms.Label();
            this.AllowAddScheduleCheckBox = new System.Windows.Forms.CheckBox();
            this.TimeZoneComboBox = new System.Windows.Forms.ComboBox();
            this.SelectNoDaysButton = new System.Windows.Forms.Button();
            this.SelectAllDaysButton = new System.Windows.Forms.Button();
            this.ScheduleLabel = new System.Windows.Forms.Label();
            this.SaturdayCheckBox = new System.Windows.Forms.CheckBox();
            this.FridayCheckBox = new System.Windows.Forms.CheckBox();
            this.ThursdayCheckBox = new System.Windows.Forms.CheckBox();
            this.WednesdayCheckBox = new System.Windows.Forms.CheckBox();
            this.TuesdayCheckBox = new System.Windows.Forms.CheckBox();
            this.MondayCheckBox = new System.Windows.Forms.CheckBox();
            this.SundayCheckBox = new System.Windows.Forms.CheckBox();
            this.MinuteNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.HourNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.TimeLabel = new System.Windows.Forms.Label();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.HelpButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.ChangeSettingProgressBar = new System.Windows.Forms.ProgressBar();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.BuildDefinitionSearchPanel = new System.Windows.Forms.Panel();
            this.BuildDefinitionSearchPlaceholderLabel = new System.Windows.Forms.Label();
            this.VerticalSplitter = new System.Windows.Forms.Splitter();
            this.ChangeBuildDefinitionQueueStatusToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.TriggersContinuousIntegrationPanel = new System.Windows.Forms.Panel();
            this.PropertiesPanel.SuspendLayout();
            this.DefaultAgentPoolQueuePanel.SuspendLayout();
            this.TriggersSchedulePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MinuteNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HourNumericUpDown)).BeginInit();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.BuildDefinitionSearchPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Controls.Add(this.DefaultAgentPoolQueuePanel);
            this.PropertiesPanel.Controls.Add(this.SettingComboBox);
            this.PropertiesPanel.Controls.Add(this.SettingLabel);
            this.PropertiesPanel.Controls.Add(this.TriggersSchedulePanel);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesPanel.Location = new System.Drawing.Point(650, 0);
            this.PropertiesPanel.MinimumSize = new System.Drawing.Size(560, 480);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(566, 480);
            this.PropertiesPanel.TabIndex = 2;
            // 
            // DefaultAgentPoolQueuePanel
            // 
            this.DefaultAgentPoolQueuePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DefaultAgentPoolQueuePanel.Controls.Add(this.RefreshAgentPoolQueueButton);
            this.DefaultAgentPoolQueuePanel.Controls.Add(this.DefaultAgentPoolQueueComboBox);
            this.DefaultAgentPoolQueuePanel.Controls.Add(this.DefaultAgentPoolQueueLabel);
            this.DefaultAgentPoolQueuePanel.Controls.Add(this.TriggersContinuousIntegrationPanel);
            this.DefaultAgentPoolQueuePanel.Location = new System.Drawing.Point(12, 63);
            this.DefaultAgentPoolQueuePanel.Name = "DefaultAgentPoolQueuePanel";
            this.DefaultAgentPoolQueuePanel.Size = new System.Drawing.Size(542, 283);
            this.DefaultAgentPoolQueuePanel.TabIndex = 3;
            // 
            // RefreshAgentPoolQueueButton
            // 
            this.RefreshAgentPoolQueueButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
            this.RefreshAgentPoolQueueButton.Location = new System.Drawing.Point(348, 11);
            this.RefreshAgentPoolQueueButton.Name = "RefreshAgentPoolQueueButton";
            this.RefreshAgentPoolQueueButton.Size = new System.Drawing.Size(32, 23);
            this.RefreshAgentPoolQueueButton.TabIndex = 2;
            this.RefreshAgentPoolQueueButton.UseVisualStyleBackColor = true;
            this.RefreshAgentPoolQueueButton.Click += new System.EventHandler(this.RefreshAgentPoolQueueButton_Click);
            // 
            // DefaultAgentPoolQueueComboBox
            // 
            this.DefaultAgentPoolQueueComboBox.FormattingEnabled = true;
            this.DefaultAgentPoolQueueComboBox.Location = new System.Drawing.Point(142, 13);
            this.DefaultAgentPoolQueueComboBox.Name = "DefaultAgentPoolQueueComboBox";
            this.DefaultAgentPoolQueueComboBox.Size = new System.Drawing.Size(200, 21);
            this.DefaultAgentPoolQueueComboBox.TabIndex = 1;
            // 
            // DefaultAgentPoolQueueLabel
            // 
            this.DefaultAgentPoolQueueLabel.AutoSize = true;
            this.DefaultAgentPoolQueueLabel.Location = new System.Drawing.Point(6, 16);
            this.DefaultAgentPoolQueueLabel.Name = "DefaultAgentPoolQueueLabel";
            this.DefaultAgentPoolQueueLabel.Size = new System.Drawing.Size(130, 13);
            this.DefaultAgentPoolQueueLabel.TabIndex = 0;
            this.DefaultAgentPoolQueueLabel.Text = "Default agent pool queue:";
            // 
            // SettingComboBox
            // 
            this.SettingComboBox.FormattingEnabled = true;
            this.SettingComboBox.Items.AddRange(new object[] {
            "Default Agent Pool Queue",
            "Triggers - Schedule"});
            this.SettingComboBox.Location = new System.Drawing.Point(61, 31);
            this.SettingComboBox.Name = "SettingComboBox";
            this.SettingComboBox.Size = new System.Drawing.Size(172, 21);
            this.SettingComboBox.TabIndex = 2;
            this.SettingComboBox.SelectedIndexChanged += new System.EventHandler(this.SettingComboBox_SelectedIndexChanged);
            // 
            // SettingLabel
            // 
            this.SettingLabel.AutoSize = true;
            this.SettingLabel.Location = new System.Drawing.Point(12, 34);
            this.SettingLabel.Name = "SettingLabel";
            this.SettingLabel.Size = new System.Drawing.Size(43, 13);
            this.SettingLabel.TabIndex = 1;
            this.SettingLabel.Text = "Setting:";
            // 
            // TriggersSchedulePanel
            // 
            this.TriggersSchedulePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TriggersSchedulePanel.Controls.Add(this.AddScheduleWarningLabel);
            this.TriggersSchedulePanel.Controls.Add(this.AllowAddScheduleCheckBox);
            this.TriggersSchedulePanel.Controls.Add(this.TimeZoneComboBox);
            this.TriggersSchedulePanel.Controls.Add(this.SelectNoDaysButton);
            this.TriggersSchedulePanel.Controls.Add(this.SelectAllDaysButton);
            this.TriggersSchedulePanel.Controls.Add(this.ScheduleLabel);
            this.TriggersSchedulePanel.Controls.Add(this.SaturdayCheckBox);
            this.TriggersSchedulePanel.Controls.Add(this.FridayCheckBox);
            this.TriggersSchedulePanel.Controls.Add(this.ThursdayCheckBox);
            this.TriggersSchedulePanel.Controls.Add(this.WednesdayCheckBox);
            this.TriggersSchedulePanel.Controls.Add(this.TuesdayCheckBox);
            this.TriggersSchedulePanel.Controls.Add(this.MondayCheckBox);
            this.TriggersSchedulePanel.Controls.Add(this.SundayCheckBox);
            this.TriggersSchedulePanel.Controls.Add(this.MinuteNumericUpDown);
            this.TriggersSchedulePanel.Controls.Add(this.HourNumericUpDown);
            this.TriggersSchedulePanel.Controls.Add(this.TimeLabel);
            this.TriggersSchedulePanel.Location = new System.Drawing.Point(12, 63);
            this.TriggersSchedulePanel.Name = "TriggersSchedulePanel";
            this.TriggersSchedulePanel.Size = new System.Drawing.Size(542, 283);
            this.TriggersSchedulePanel.TabIndex = 3;
            // 
            // AddScheduleWarningLabel
            // 
            this.AddScheduleWarningLabel.AutoSize = true;
            this.AddScheduleWarningLabel.Location = new System.Drawing.Point(3, 150);
            this.AddScheduleWarningLabel.Name = "AddScheduleWarningLabel";
            this.AddScheduleWarningLabel.Size = new System.Drawing.Size(518, 13);
            this.AddScheduleWarningLabel.TabIndex = 16;
            this.AddScheduleWarningLabel.Text = "Warning: Some build definitions should not have a schedule. (Example: CI, Prod an" +
    "d xxxDR build definitions).";
            // 
            // AllowAddScheduleCheckBox
            // 
            this.AllowAddScheduleCheckBox.AutoSize = true;
            this.AllowAddScheduleCheckBox.Location = new System.Drawing.Point(13, 112);
            this.AllowAddScheduleCheckBox.Name = "AllowAddScheduleCheckBox";
            this.AllowAddScheduleCheckBox.Size = new System.Drawing.Size(282, 17);
            this.AllowAddScheduleCheckBox.TabIndex = 15;
            this.AllowAddScheduleCheckBox.Text = "Allow adding schedule to build definition that has none";
            this.AllowAddScheduleCheckBox.UseVisualStyleBackColor = true;
            // 
            // TimeZoneComboBox
            // 
            this.TimeZoneComboBox.FormattingEnabled = true;
            this.TimeZoneComboBox.Location = new System.Drawing.Point(139, 38);
            this.TimeZoneComboBox.Name = "TimeZoneComboBox";
            this.TimeZoneComboBox.Size = new System.Drawing.Size(165, 21);
            this.TimeZoneComboBox.TabIndex = 5;
            // 
            // SelectNoDaysButton
            // 
            this.SelectNoDaysButton.Location = new System.Drawing.Point(388, 72);
            this.SelectNoDaysButton.Name = "SelectNoDaysButton";
            this.SelectNoDaysButton.Size = new System.Drawing.Size(75, 23);
            this.SelectNoDaysButton.TabIndex = 14;
            this.SelectNoDaysButton.Text = "None";
            this.SelectNoDaysButton.UseVisualStyleBackColor = true;
            this.SelectNoDaysButton.Click += new System.EventHandler(this.SelectNoDaysButton_Click);
            // 
            // SelectAllDaysButton
            // 
            this.SelectAllDaysButton.Location = new System.Drawing.Point(307, 72);
            this.SelectAllDaysButton.Name = "SelectAllDaysButton";
            this.SelectAllDaysButton.Size = new System.Drawing.Size(75, 23);
            this.SelectAllDaysButton.TabIndex = 13;
            this.SelectAllDaysButton.Text = "All";
            this.SelectAllDaysButton.UseVisualStyleBackColor = true;
            this.SelectAllDaysButton.Click += new System.EventHandler(this.SelectAllDaysButton_Click);
            // 
            // ScheduleLabel
            // 
            this.ScheduleLabel.AutoSize = true;
            this.ScheduleLabel.Location = new System.Drawing.Point(9, 13);
            this.ScheduleLabel.Name = "ScheduleLabel";
            this.ScheduleLabel.Size = new System.Drawing.Size(101, 13);
            this.ScheduleLabel.TabIndex = 0;
            this.ScheduleLabel.Text = "Build at these times.";
            // 
            // SaturdayCheckBox
            // 
            this.SaturdayCheckBox.AutoSize = true;
            this.SaturdayCheckBox.Checked = true;
            this.SaturdayCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SaturdayCheckBox.Location = new System.Drawing.Point(265, 76);
            this.SaturdayCheckBox.Name = "SaturdayCheckBox";
            this.SaturdayCheckBox.Size = new System.Drawing.Size(39, 17);
            this.SaturdayCheckBox.TabIndex = 12;
            this.SaturdayCheckBox.Text = "Sa";
            this.SaturdayCheckBox.UseVisualStyleBackColor = true;
            // 
            // FridayCheckBox
            // 
            this.FridayCheckBox.AutoSize = true;
            this.FridayCheckBox.Checked = true;
            this.FridayCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.FridayCheckBox.Location = new System.Drawing.Point(227, 76);
            this.FridayCheckBox.Name = "FridayCheckBox";
            this.FridayCheckBox.Size = new System.Drawing.Size(32, 17);
            this.FridayCheckBox.TabIndex = 11;
            this.FridayCheckBox.Text = "F";
            this.FridayCheckBox.UseVisualStyleBackColor = true;
            // 
            // ThursdayCheckBox
            // 
            this.ThursdayCheckBox.AutoSize = true;
            this.ThursdayCheckBox.Checked = true;
            this.ThursdayCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ThursdayCheckBox.Location = new System.Drawing.Point(182, 76);
            this.ThursdayCheckBox.Name = "ThursdayCheckBox";
            this.ThursdayCheckBox.Size = new System.Drawing.Size(39, 17);
            this.ThursdayCheckBox.TabIndex = 10;
            this.ThursdayCheckBox.Text = "Th";
            this.ThursdayCheckBox.UseVisualStyleBackColor = true;
            // 
            // WednesdayCheckBox
            // 
            this.WednesdayCheckBox.AutoSize = true;
            this.WednesdayCheckBox.Checked = true;
            this.WednesdayCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.WednesdayCheckBox.Location = new System.Drawing.Point(139, 76);
            this.WednesdayCheckBox.Name = "WednesdayCheckBox";
            this.WednesdayCheckBox.Size = new System.Drawing.Size(37, 17);
            this.WednesdayCheckBox.TabIndex = 9;
            this.WednesdayCheckBox.Text = "W";
            this.WednesdayCheckBox.UseVisualStyleBackColor = true;
            // 
            // TuesdayCheckBox
            // 
            this.TuesdayCheckBox.AutoSize = true;
            this.TuesdayCheckBox.Checked = true;
            this.TuesdayCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.TuesdayCheckBox.Location = new System.Drawing.Point(94, 76);
            this.TuesdayCheckBox.Name = "TuesdayCheckBox";
            this.TuesdayCheckBox.Size = new System.Drawing.Size(39, 17);
            this.TuesdayCheckBox.TabIndex = 8;
            this.TuesdayCheckBox.Text = "Tu";
            this.TuesdayCheckBox.UseVisualStyleBackColor = true;
            // 
            // MondayCheckBox
            // 
            this.MondayCheckBox.AutoSize = true;
            this.MondayCheckBox.Checked = true;
            this.MondayCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MondayCheckBox.Location = new System.Drawing.Point(53, 76);
            this.MondayCheckBox.Name = "MondayCheckBox";
            this.MondayCheckBox.Size = new System.Drawing.Size(35, 17);
            this.MondayCheckBox.TabIndex = 7;
            this.MondayCheckBox.Text = "M";
            this.MondayCheckBox.UseVisualStyleBackColor = true;
            // 
            // SundayCheckBox
            // 
            this.SundayCheckBox.AutoSize = true;
            this.SundayCheckBox.Checked = true;
            this.SundayCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SundayCheckBox.Location = new System.Drawing.Point(13, 76);
            this.SundayCheckBox.Name = "SundayCheckBox";
            this.SundayCheckBox.Size = new System.Drawing.Size(39, 17);
            this.SundayCheckBox.TabIndex = 6;
            this.SundayCheckBox.Text = "Su";
            this.SundayCheckBox.UseVisualStyleBackColor = true;
            // 
            // MinuteNumericUpDown
            // 
            this.MinuteNumericUpDown.Location = new System.Drawing.Point(99, 38);
            this.MinuteNumericUpDown.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.MinuteNumericUpDown.Name = "MinuteNumericUpDown";
            this.MinuteNumericUpDown.Size = new System.Drawing.Size(32, 20);
            this.MinuteNumericUpDown.TabIndex = 4;
            // 
            // HourNumericUpDown
            // 
            this.HourNumericUpDown.Location = new System.Drawing.Point(61, 38);
            this.HourNumericUpDown.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.HourNumericUpDown.Name = "HourNumericUpDown";
            this.HourNumericUpDown.Size = new System.Drawing.Size(32, 20);
            this.HourNumericUpDown.TabIndex = 3;
            // 
            // TimeLabel
            // 
            this.TimeLabel.AutoSize = true;
            this.TimeLabel.Location = new System.Drawing.Point(10, 42);
            this.TimeLabel.Name = "TimeLabel";
            this.TimeLabel.Size = new System.Drawing.Size(50, 13);
            this.TimeLabel.TabIndex = 2;
            this.TimeLabel.Text = "24h time:";
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.HelpButton);
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(566, 25);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(538, 0);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 4;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(6, 1);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(523, 24);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "Change Build Definition Settings";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ControlPanel
            // 
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.ChangeSettingProgressBar);
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Controls.Add(this.ApplyButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 440);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(566, 40);
            this.ControlPanel.TabIndex = 4;
            // 
            // ChangeSettingProgressBar
            // 
            this.ChangeSettingProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ChangeSettingProgressBar.Location = new System.Drawing.Point(373, 17);
            this.ChangeSettingProgressBar.Name = "ChangeSettingProgressBar";
            this.ChangeSettingProgressBar.Size = new System.Drawing.Size(100, 10);
            this.ChangeSettingProgressBar.TabIndex = 3;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(479, 11);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 1;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // BuildDefinitionSearchPanel
            // 
            this.BuildDefinitionSearchPanel.AutoScroll = true;
            this.BuildDefinitionSearchPanel.Controls.Add(this.BuildDefinitionSearchPlaceholderLabel);
            this.BuildDefinitionSearchPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.BuildDefinitionSearchPanel.Location = new System.Drawing.Point(0, 0);
            this.BuildDefinitionSearchPanel.Name = "BuildDefinitionSearchPanel";
            this.BuildDefinitionSearchPanel.Size = new System.Drawing.Size(650, 480);
            this.BuildDefinitionSearchPanel.TabIndex = 0;
            // 
            // BuildDefinitionSearchPlaceholderLabel
            // 
            this.BuildDefinitionSearchPlaceholderLabel.AutoSize = true;
            this.BuildDefinitionSearchPlaceholderLabel.Location = new System.Drawing.Point(52, 167);
            this.BuildDefinitionSearchPlaceholderLabel.Name = "BuildDefinitionSearchPlaceholderLabel";
            this.BuildDefinitionSearchPlaceholderLabel.Size = new System.Drawing.Size(173, 13);
            this.BuildDefinitionSearchPlaceholderLabel.TabIndex = 0;
            this.BuildDefinitionSearchPlaceholderLabel.Text = "Build Definition Search Placeholder";
            // 
            // VerticalSplitter
            // 
            this.VerticalSplitter.BackColor = System.Drawing.SystemColors.ControlDark;
            this.VerticalSplitter.Location = new System.Drawing.Point(650, 0);
            this.VerticalSplitter.Name = "VerticalSplitter";
            this.VerticalSplitter.Size = new System.Drawing.Size(6, 480);
            this.VerticalSplitter.TabIndex = 1;
            this.VerticalSplitter.TabStop = false;
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.VsoBuild.Sidekicks.Help.chm";
            // 
            // TriggersContinuousIntegrationPanel
            // 
            this.TriggersContinuousIntegrationPanel.Location = new System.Drawing.Point(0, 0);
            this.TriggersContinuousIntegrationPanel.Name = "TriggersContinuousIntegrationPanel";
            this.TriggersContinuousIntegrationPanel.Size = new System.Drawing.Size(542, 283);
            this.TriggersContinuousIntegrationPanel.TabIndex = 3;
            // 
            // BuildDefinitionChangeSettingsForm
            // 
            this.AcceptButton = this.ApplyButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 480);
            this.ControlBox = false;
            this.Controls.Add(this.VerticalSplitter);
            this.Controls.Add(this.PropertiesPanel);
            this.Controls.Add(this.BuildDefinitionSearchPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "BuildDefinitionChangeSettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "BuildDefinitionChangeSettingsForm";
            this.PropertiesPanel.ResumeLayout(false);
            this.PropertiesPanel.PerformLayout();
            this.DefaultAgentPoolQueuePanel.ResumeLayout(false);
            this.DefaultAgentPoolQueuePanel.PerformLayout();
            this.TriggersSchedulePanel.ResumeLayout(false);
            this.TriggersSchedulePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MinuteNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HourNumericUpDown)).EndInit();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.BuildDefinitionSearchPanel.ResumeLayout(false);
            this.BuildDefinitionSearchPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Panel BuildDefinitionSearchPanel;
        private System.Windows.Forms.Label BuildDefinitionSearchPlaceholderLabel;
        private System.Windows.Forms.Splitter VerticalSplitter;
        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.ToolTip ChangeBuildDefinitionQueueStatusToolTip;
        private System.Windows.Forms.ComboBox SettingComboBox;
        private System.Windows.Forms.Label SettingLabel;
        private System.Windows.Forms.Panel DefaultAgentPoolQueuePanel;
        private System.Windows.Forms.ComboBox DefaultAgentPoolQueueComboBox;
        private System.Windows.Forms.Label DefaultAgentPoolQueueLabel;
        private System.Windows.Forms.Panel TriggersSchedulePanel;
        private System.Windows.Forms.Label ScheduleLabel;
        private System.Windows.Forms.CheckBox SaturdayCheckBox;
        private System.Windows.Forms.CheckBox FridayCheckBox;
        private System.Windows.Forms.CheckBox ThursdayCheckBox;
        private System.Windows.Forms.CheckBox WednesdayCheckBox;
        private System.Windows.Forms.CheckBox TuesdayCheckBox;
        private System.Windows.Forms.CheckBox MondayCheckBox;
        private System.Windows.Forms.CheckBox SundayCheckBox;
        private System.Windows.Forms.NumericUpDown MinuteNumericUpDown;
        private System.Windows.Forms.NumericUpDown HourNumericUpDown;
        private System.Windows.Forms.Label TimeLabel;
        private System.Windows.Forms.Button RefreshAgentPoolQueueButton;
        private System.Windows.Forms.Button SelectNoDaysButton;
        private System.Windows.Forms.Button SelectAllDaysButton;
        private System.Windows.Forms.ComboBox TimeZoneComboBox;
        private System.Windows.Forms.Label AddScheduleWarningLabel;
        private System.Windows.Forms.CheckBox AllowAddScheduleCheckBox;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.ProgressBar ChangeSettingProgressBar;
        private System.Windows.Forms.Button HelpButton;
        private System.Windows.Forms.Panel TriggersContinuousIntegrationPanel;
    }
}