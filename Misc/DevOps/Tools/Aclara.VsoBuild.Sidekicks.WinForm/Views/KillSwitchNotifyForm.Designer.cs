﻿namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    partial class KillSwitchNotifyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KillSwitchNotifyForm));
            this.OKButton = new System.Windows.Forms.Button();
            this.UpdateButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.UserPromptValueLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ReasonTextBox = new System.Windows.Forms.RichTextBox();
            this.ReasonLabel = new System.Windows.Forms.Label();
            this.VersionImpactedValueLabel = new System.Windows.Forms.Label();
            this.VersionImpactedLabel = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // OKButton
            // 
            this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OKButton.Location = new System.Drawing.Point(622, 227);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 6;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // UpdateButton
            // 
            this.UpdateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.UpdateButton.BackColor = System.Drawing.Color.Gold;
            this.UpdateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UpdateButton.Location = new System.Drawing.Point(12, 227);
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.Size = new System.Drawing.Size(91, 23);
            this.UpdateButton.TabIndex = 5;
            this.UpdateButton.Text = "Update Now";
            this.UpdateButton.UseVisualStyleBackColor = false;
            this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.panel1.Controls.Add(this.UserPromptValueLabel);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(710, 34);
            this.panel1.TabIndex = 8;
            // 
            // UserPromptValueLabel
            // 
            this.UserPromptValueLabel.AutoSize = true;
            this.UserPromptValueLabel.Location = new System.Drawing.Point(3, 10);
            this.UserPromptValueLabel.Name = "UserPromptValueLabel";
            this.UserPromptValueLabel.Size = new System.Drawing.Size(74, 13);
            this.UserPromptValueLabel.TabIndex = 1;
            this.UserPromptValueLabel.Text = "<user prompt>";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.VersionImpactedValueLabel);
            this.panel2.Controls.Add(this.VersionImpactedLabel);
            this.panel2.Controls.Add(this.ReasonLabel);
            this.panel2.Controls.Add(this.ReasonTextBox);
            this.panel2.Location = new System.Drawing.Point(0, 39);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(710, 182);
            this.panel2.TabIndex = 9;
            // 
            // ReasonTextBox
            // 
            this.ReasonTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReasonTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReasonTextBox.Location = new System.Drawing.Point(15, 60);
            this.ReasonTextBox.Name = "ReasonTextBox";
            this.ReasonTextBox.ReadOnly = true;
            this.ReasonTextBox.Size = new System.Drawing.Size(682, 107);
            this.ReasonTextBox.TabIndex = 8;
            this.ReasonTextBox.Text = "";
            // 
            // ReasonLabel
            // 
            this.ReasonLabel.AutoSize = true;
            this.ReasonLabel.Location = new System.Drawing.Point(3, 7);
            this.ReasonLabel.Name = "ReasonLabel";
            this.ReasonLabel.Size = new System.Drawing.Size(59, 13);
            this.ReasonLabel.TabIndex = 9;
            this.ReasonLabel.Text = "Information";
            // 
            // VersionImpactedValueLabel
            // 
            this.VersionImpactedValueLabel.AutoSize = true;
            this.VersionImpactedValueLabel.Location = new System.Drawing.Point(63, 32);
            this.VersionImpactedValueLabel.Name = "VersionImpactedValueLabel";
            this.VersionImpactedValueLabel.Size = new System.Drawing.Size(53, 13);
            this.VersionImpactedValueLabel.TabIndex = 13;
            this.VersionImpactedValueLabel.Text = "<version>";
            // 
            // VersionImpactedLabel
            // 
            this.VersionImpactedLabel.AutoSize = true;
            this.VersionImpactedLabel.Location = new System.Drawing.Point(12, 32);
            this.VersionImpactedLabel.Name = "VersionImpactedLabel";
            this.VersionImpactedLabel.Size = new System.Drawing.Size(45, 13);
            this.VersionImpactedLabel.TabIndex = 12;
            this.VersionImpactedLabel.Text = "Version:";
            // 
            // KillSwitchNotifyForm
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 262);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.UpdateButton);
            this.Controls.Add(this.OKButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "KillSwitchNotifyForm";
            this.Text = "Sidekick Kill Switch";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.KillSwitchNotifyForm_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button UpdateButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label UserPromptValueLabel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label VersionImpactedValueLabel;
        private System.Windows.Forms.Label VersionImpactedLabel;
        private System.Windows.Forms.Label ReasonLabel;
        private System.Windows.Forms.RichTextBox ReasonTextBox;
    }
}