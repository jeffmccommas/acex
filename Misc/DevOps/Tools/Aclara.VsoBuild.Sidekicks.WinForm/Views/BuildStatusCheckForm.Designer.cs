﻿namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    partial class BuildStatusCheckForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.VerticalSplitter = new System.Windows.Forms.Splitter();
            this.BuildDefinitionSearchPanel = new System.Windows.Forms.Panel();
            this.BuildDefinitionSearchPlaceholderLabel = new System.Windows.Forms.Label();
            this.BuildStatusCheckToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.RecentlyCompletedInHoursNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.TopNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.DateFilterComboBox = new System.Windows.Forms.ComboBox();
            this.StopRefreshInMinutesNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.AutoRefreshBuildStatusCheckBox = new System.Windows.Forms.CheckBox();
            this.RefreshInMinutesNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.RecalculateButton = new System.Windows.Forms.Button();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.SwapBuildStatusViewButton = new System.Windows.Forms.Button();
            this.CopySplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.CopyContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CopyAsTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyAsHTMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.HelpButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.BuildStatusPanel = new System.Windows.Forms.Panel();
            this.BuildStatusDataGridView = new System.Windows.Forms.DataGridView();
            this.ReasonColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.ResultColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.BuildNumberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuildDefinitionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QueueTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FinishTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuildDurationColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RequestedForColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QueueNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExtraColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuildStatusContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CopyBuildDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RequestBuildToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewBuildToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewBuildDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MissingBuildStatusPanel = new System.Windows.Forms.Panel();
            this.MissingBuildStatusBuildDefinitionListBox = new System.Windows.Forms.ListBox();
            this.MissingBuildStatusContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CopyMissingBuildStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyAllMissingBuildStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewMissingBuildStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MissingBuildStatusBuildDefinitionListLabel = new System.Windows.Forms.Label();
            this.MissingBuildStatusLabel = new System.Windows.Forms.Label();
            this.QueuedBuildDefinitionsCheckBox = new System.Windows.Forms.CheckBox();
            this.StatusPanel = new System.Windows.Forms.Panel();
            this.BuildListRetrievalResultPanel = new System.Windows.Forms.Panel();
            this.BuildListRetrievalResultLabel = new System.Windows.Forms.Label();
            this.BuildListTotalsPanel = new System.Windows.Forms.Panel();
            this.BuildListTotalClockTimeLabel = new System.Windows.Forms.Label();
            this.BuildListTotalDurationLabel = new System.Windows.Forms.Label();
            this.BuildListTotalClockTimeValueLabel = new System.Windows.Forms.Label();
            this.BuildListTotalDurationValueLabel = new System.Windows.Forms.Label();
            this.BuildListRetrievalProgressPanel = new System.Windows.Forms.Panel();
            this.BuildListRetrievalProgressBar = new System.Windows.Forms.ProgressBar();
            this.BuildListRetrievalProgressLabel = new System.Windows.Forms.Label();
            this.AutoRefreshPanel = new System.Windows.Forms.Panel();
            this.ManualRefreshPanel = new System.Windows.Forms.Panel();
            this.ControlBarStatusLabel = new System.Windows.Forms.Label();
            this.AutoRefreshBuildStatusTimer = new System.Windows.Forms.Timer(this.components);
            this.StatusUpdateTimer = new System.Windows.Forms.Timer(this.components);
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.BuildDefinitionSearchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecentlyCompletedInHoursNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StopRefreshInMinutesNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RefreshInMinutesNumericUpDown)).BeginInit();
            this.CopyContextMenuStrip.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.PropertiesPanel.SuspendLayout();
            this.BuildStatusPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BuildStatusDataGridView)).BeginInit();
            this.BuildStatusContextMenuStrip.SuspendLayout();
            this.MissingBuildStatusPanel.SuspendLayout();
            this.MissingBuildStatusContextMenuStrip.SuspendLayout();
            this.StatusPanel.SuspendLayout();
            this.BuildListRetrievalResultPanel.SuspendLayout();
            this.BuildListTotalsPanel.SuspendLayout();
            this.BuildListRetrievalProgressPanel.SuspendLayout();
            this.AutoRefreshPanel.SuspendLayout();
            this.ManualRefreshPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // VerticalSplitter
            // 
            this.VerticalSplitter.BackColor = System.Drawing.SystemColors.ControlDark;
            this.VerticalSplitter.Location = new System.Drawing.Point(650, 0);
            this.VerticalSplitter.Name = "VerticalSplitter";
            this.VerticalSplitter.Size = new System.Drawing.Size(6, 480);
            this.VerticalSplitter.TabIndex = 1;
            this.VerticalSplitter.TabStop = false;
            // 
            // BuildDefinitionSearchPanel
            // 
            this.BuildDefinitionSearchPanel.AutoScroll = true;
            this.BuildDefinitionSearchPanel.Controls.Add(this.BuildDefinitionSearchPlaceholderLabel);
            this.BuildDefinitionSearchPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.BuildDefinitionSearchPanel.Location = new System.Drawing.Point(0, 0);
            this.BuildDefinitionSearchPanel.Name = "BuildDefinitionSearchPanel";
            this.BuildDefinitionSearchPanel.Size = new System.Drawing.Size(650, 480);
            this.BuildDefinitionSearchPanel.TabIndex = 0;
            // 
            // BuildDefinitionSearchPlaceholderLabel
            // 
            this.BuildDefinitionSearchPlaceholderLabel.AutoSize = true;
            this.BuildDefinitionSearchPlaceholderLabel.Location = new System.Drawing.Point(12, 9);
            this.BuildDefinitionSearchPlaceholderLabel.Name = "BuildDefinitionSearchPlaceholderLabel";
            this.BuildDefinitionSearchPlaceholderLabel.Size = new System.Drawing.Size(173, 13);
            this.BuildDefinitionSearchPlaceholderLabel.TabIndex = 0;
            this.BuildDefinitionSearchPlaceholderLabel.Text = "Build Definition Search Placeholder";
            // 
            // BuildStatusCheckToolTip
            // 
            this.BuildStatusCheckToolTip.AutoPopDelay = 5000;
            this.BuildStatusCheckToolTip.InitialDelay = 250;
            this.BuildStatusCheckToolTip.ReshowDelay = 100;
            // 
            // RecentlyCompletedInHoursNumericUpDown
            // 
            this.RecentlyCompletedInHoursNumericUpDown.Location = new System.Drawing.Point(179, 4);
            this.RecentlyCompletedInHoursNumericUpDown.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.RecentlyCompletedInHoursNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.RecentlyCompletedInHoursNumericUpDown.Name = "RecentlyCompletedInHoursNumericUpDown";
            this.RecentlyCompletedInHoursNumericUpDown.Size = new System.Drawing.Size(33, 20);
            this.RecentlyCompletedInHoursNumericUpDown.TabIndex = 3;
            this.BuildStatusCheckToolTip.SetToolTip(this.RecentlyCompletedInHoursNumericUpDown, "Completed in last (n) hours");
            this.RecentlyCompletedInHoursNumericUpDown.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // TopNumericUpDown
            // 
            this.TopNumericUpDown.Location = new System.Drawing.Point(140, 4);
            this.TopNumericUpDown.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.TopNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.TopNumericUpDown.Name = "TopNumericUpDown";
            this.TopNumericUpDown.Size = new System.Drawing.Size(33, 20);
            this.TopNumericUpDown.TabIndex = 2;
            this.BuildStatusCheckToolTip.SetToolTip(this.TopNumericUpDown, "Top (n) builds");
            this.TopNumericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // DateFilterComboBox
            // 
            this.DateFilterComboBox.Location = new System.Drawing.Point(44, 4);
            this.DateFilterComboBox.Name = "DateFilterComboBox";
            this.DateFilterComboBox.Size = new System.Drawing.Size(90, 21);
            this.DateFilterComboBox.TabIndex = 1;
            this.BuildStatusCheckToolTip.SetToolTip(this.DateFilterComboBox, "Date filter");
            this.DateFilterComboBox.SelectedIndexChanged += new System.EventHandler(this.DateFilterComboBox_SelectedIndexChanged);
            // 
            // StopRefreshInMinutesNumericUpDown
            // 
            this.StopRefreshInMinutesNumericUpDown.Increment = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.StopRefreshInMinutesNumericUpDown.Location = new System.Drawing.Point(137, 5);
            this.StopRefreshInMinutesNumericUpDown.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.StopRefreshInMinutesNumericUpDown.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.StopRefreshInMinutesNumericUpDown.Name = "StopRefreshInMinutesNumericUpDown";
            this.StopRefreshInMinutesNumericUpDown.Size = new System.Drawing.Size(45, 20);
            this.StopRefreshInMinutesNumericUpDown.TabIndex = 2;
            this.BuildStatusCheckToolTip.SetToolTip(this.StopRefreshInMinutesNumericUpDown, "Stop auto-refresh after (n) minutes");
            this.StopRefreshInMinutesNumericUpDown.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.StopRefreshInMinutesNumericUpDown.ValueChanged += new System.EventHandler(this.StopRefreshInMinutesNumericUpDown_ValueChanged);
            // 
            // AutoRefreshBuildStatusCheckBox
            // 
            this.AutoRefreshBuildStatusCheckBox.AutoSize = true;
            this.AutoRefreshBuildStatusCheckBox.Location = new System.Drawing.Point(7, 7);
            this.AutoRefreshBuildStatusCheckBox.Name = "AutoRefreshBuildStatusCheckBox";
            this.AutoRefreshBuildStatusCheckBox.Size = new System.Drawing.Size(83, 17);
            this.AutoRefreshBuildStatusCheckBox.TabIndex = 0;
            this.AutoRefreshBuildStatusCheckBox.Text = "Auto-refresh";
            this.BuildStatusCheckToolTip.SetToolTip(this.AutoRefreshBuildStatusCheckBox, "Toggle auto-refresh");
            this.AutoRefreshBuildStatusCheckBox.UseVisualStyleBackColor = true;
            this.AutoRefreshBuildStatusCheckBox.CheckedChanged += new System.EventHandler(this.AutoRefreshBuildStatusCheckBox_CheckedChanged);
            // 
            // RefreshInMinutesNumericUpDown
            // 
            this.RefreshInMinutesNumericUpDown.Location = new System.Drawing.Point(96, 5);
            this.RefreshInMinutesNumericUpDown.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.RefreshInMinutesNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.RefreshInMinutesNumericUpDown.Name = "RefreshInMinutesNumericUpDown";
            this.RefreshInMinutesNumericUpDown.Size = new System.Drawing.Size(35, 20);
            this.RefreshInMinutesNumericUpDown.TabIndex = 1;
            this.BuildStatusCheckToolTip.SetToolTip(this.RefreshInMinutesNumericUpDown, "Wait (n) minutes before next refresh");
            this.RefreshInMinutesNumericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.RefreshInMinutesNumericUpDown.ValueChanged += new System.EventHandler(this.RefreshInMinutesNumericUpDown_ValueChanged);
            // 
            // RecalculateButton
            // 
            this.RecalculateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RecalculateButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionRecalculate;
            this.RecalculateButton.Location = new System.Drawing.Point(5, 2);
            this.RecalculateButton.Name = "RecalculateButton";
            this.RecalculateButton.Size = new System.Drawing.Size(23, 28);
            this.RecalculateButton.TabIndex = 0;
            this.BuildStatusCheckToolTip.SetToolTip(this.RecalculateButton, "Recalculate totals");
            this.RecalculateButton.UseVisualStyleBackColor = true;
            this.RecalculateButton.Click += new System.EventHandler(this.RecalculateButton_Click);
            // 
            // RefreshButton
            // 
            this.RefreshButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
            this.RefreshButton.Location = new System.Drawing.Point(5, 3);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(32, 23);
            this.RefreshButton.TabIndex = 0;
            this.RefreshButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BuildStatusCheckToolTip.SetToolTip(this.RefreshButton, "Refresh");
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // SwapBuildStatusViewButton
            // 
            this.SwapBuildStatusViewButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionSwapView;
            this.SwapBuildStatusViewButton.Location = new System.Drawing.Point(474, 59);
            this.SwapBuildStatusViewButton.Name = "SwapBuildStatusViewButton";
            this.SwapBuildStatusViewButton.Size = new System.Drawing.Size(42, 23);
            this.SwapBuildStatusViewButton.TabIndex = 6;
            this.BuildStatusCheckToolTip.SetToolTip(this.SwapBuildStatusViewButton, "Swap build status views");
            this.SwapBuildStatusViewButton.UseVisualStyleBackColor = true;
            this.SwapBuildStatusViewButton.Click += new System.EventHandler(this.SwapBuildStatusViewButton_Click);
            // 
            // CopySplitButton
            // 
            this.CopySplitButton.AutoSize = true;
            this.CopySplitButton.ContextMenuStrip = this.CopyContextMenuStrip;
            this.CopySplitButton.Location = new System.Drawing.Point(422, 59);
            this.CopySplitButton.Name = "CopySplitButton";
            this.CopySplitButton.Size = new System.Drawing.Size(46, 23);
            this.CopySplitButton.TabIndex = 5;
            this.CopySplitButton.Text = "Copy";
            this.BuildStatusCheckToolTip.SetToolTip(this.CopySplitButton, "Copy to Clipboard");
            this.CopySplitButton.UseVisualStyleBackColor = true;
            // 
            // CopyContextMenuStrip
            // 
            this.CopyContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyAsTextToolStripMenuItem,
            this.CopyAsHTMLToolStripMenuItem});
            this.CopyContextMenuStrip.Name = "CopyContextMenuStrip";
            this.CopyContextMenuStrip.Size = new System.Drawing.Size(153, 48);
            // 
            // CopyAsTextToolStripMenuItem
            // 
            this.CopyAsTextToolStripMenuItem.Name = "CopyAsTextToolStripMenuItem";
            this.CopyAsTextToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.CopyAsTextToolStripMenuItem.Text = "Copy as Text";
            this.CopyAsTextToolStripMenuItem.Click += new System.EventHandler(this.CopyAsTextToolStripMenuItem_Click);
            // 
            // CopyAsHTMLToolStripMenuItem
            // 
            this.CopyAsHTMLToolStripMenuItem.Name = "CopyAsHTMLToolStripMenuItem";
            this.CopyAsHTMLToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.CopyAsHTMLToolStripMenuItem.Text = "Copy as HTML";
            this.CopyAsHTMLToolStripMenuItem.Click += new System.EventHandler(this.CopyAsHTMLToolStripMenuItem_Click);
            // 
            // ControlPanel
            // 
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 440);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(726, 40);
            this.ControlPanel.TabIndex = 8;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(161, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            this.BackgroundTaskStatusLabel.Text = "<Background Task Status>";
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.HelpButton);
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(726, 25);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(698, 0);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 1;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(0, 1);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(689, 21);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "Check Build Status";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Controls.Add(this.BuildStatusPanel);
            this.PropertiesPanel.Controls.Add(this.MissingBuildStatusPanel);
            this.PropertiesPanel.Controls.Add(this.QueuedBuildDefinitionsCheckBox);
            this.PropertiesPanel.Controls.Add(this.StatusPanel);
            this.PropertiesPanel.Controls.Add(this.AutoRefreshPanel);
            this.PropertiesPanel.Controls.Add(this.ManualRefreshPanel);
            this.PropertiesPanel.Controls.Add(this.CopySplitButton);
            this.PropertiesPanel.Controls.Add(this.ControlBarStatusLabel);
            this.PropertiesPanel.Controls.Add(this.SwapBuildStatusViewButton);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesPanel.Location = new System.Drawing.Point(656, 0);
            this.PropertiesPanel.MinimumSize = new System.Drawing.Size(560, 480);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(726, 480);
            this.PropertiesPanel.TabIndex = 2;
            // 
            // BuildStatusPanel
            // 
            this.BuildStatusPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildStatusPanel.Controls.Add(this.BuildStatusDataGridView);
            this.BuildStatusPanel.Location = new System.Drawing.Point(3, 120);
            this.BuildStatusPanel.Name = "BuildStatusPanel";
            this.BuildStatusPanel.Size = new System.Drawing.Size(714, 314);
            this.BuildStatusPanel.TabIndex = 9;
            // 
            // BuildStatusDataGridView
            // 
            this.BuildStatusDataGridView.AllowUserToAddRows = false;
            this.BuildStatusDataGridView.AllowUserToDeleteRows = false;
            this.BuildStatusDataGridView.AllowUserToOrderColumns = true;
            this.BuildStatusDataGridView.AllowUserToResizeRows = false;
            this.BuildStatusDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.BuildStatusDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BuildStatusDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.BuildStatusDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.BuildStatusDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ReasonColumn,
            this.ResultColumn,
            this.BuildNumberColumn,
            this.BuildDefinitionColumn,
            this.QueueTimeColumn,
            this.StartTimeColumn,
            this.FinishTimeColumn,
            this.BuildDurationColumn,
            this.StatusColumn,
            this.RequestedForColumn,
            this.QueueNameColumn,
            this.ExtraColumn});
            this.BuildStatusDataGridView.ContextMenuStrip = this.BuildStatusContextMenuStrip;
            this.BuildStatusDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BuildStatusDataGridView.EnableHeadersVisualStyles = false;
            this.BuildStatusDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.BuildStatusDataGridView.Location = new System.Drawing.Point(0, 0);
            this.BuildStatusDataGridView.Name = "BuildStatusDataGridView";
            this.BuildStatusDataGridView.ReadOnly = true;
            this.BuildStatusDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BuildStatusDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.BuildStatusDataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            this.BuildStatusDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.BuildStatusDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.BuildStatusDataGridView.ShowEditingIcon = false;
            this.BuildStatusDataGridView.Size = new System.Drawing.Size(714, 314);
            this.BuildStatusDataGridView.TabIndex = 0;
            this.BuildStatusDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.BuildStatusDataGridView_CellFormatting);
            this.BuildStatusDataGridView.SelectionChanged += new System.EventHandler(this.BuildStatusDataGridView_SelectionChanged);
            this.BuildStatusDataGridView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BuildStatusDataGridView_MouseDown);
            // 
            // ReasonColumn
            // 
            this.ReasonColumn.HeaderText = "";
            this.ReasonColumn.Name = "ReasonColumn";
            this.ReasonColumn.ReadOnly = true;
            this.ReasonColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ReasonColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ReasonColumn.ToolTipText = "Reason";
            this.ReasonColumn.Width = 20;
            // 
            // ResultColumn
            // 
            this.ResultColumn.HeaderText = "";
            this.ResultColumn.Name = "ResultColumn";
            this.ResultColumn.ReadOnly = true;
            this.ResultColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ResultColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ResultColumn.ToolTipText = "Result";
            this.ResultColumn.Width = 20;
            // 
            // BuildNumberColumn
            // 
            this.BuildNumberColumn.HeaderText = "Build Number";
            this.BuildNumberColumn.Name = "BuildNumberColumn";
            this.BuildNumberColumn.ReadOnly = true;
            // 
            // BuildDefinitionColumn
            // 
            this.BuildDefinitionColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.BuildDefinitionColumn.HeaderText = "Build Definition";
            this.BuildDefinitionColumn.Name = "BuildDefinitionColumn";
            this.BuildDefinitionColumn.ReadOnly = true;
            this.BuildDefinitionColumn.Width = 102;
            // 
            // QueueTimeColumn
            // 
            this.QueueTimeColumn.HeaderText = "Queued";
            this.QueueTimeColumn.Name = "QueueTimeColumn";
            this.QueueTimeColumn.ReadOnly = true;
            this.QueueTimeColumn.Width = 120;
            // 
            // StartTimeColumn
            // 
            this.StartTimeColumn.HeaderText = "Started";
            this.StartTimeColumn.Name = "StartTimeColumn";
            this.StartTimeColumn.ReadOnly = true;
            this.StartTimeColumn.Width = 120;
            // 
            // FinishTimeColumn
            // 
            this.FinishTimeColumn.HeaderText = "Finished";
            this.FinishTimeColumn.Name = "FinishTimeColumn";
            this.FinishTimeColumn.ReadOnly = true;
            this.FinishTimeColumn.Width = 120;
            // 
            // BuildDurationColumn
            // 
            this.BuildDurationColumn.HeaderText = "Duration";
            this.BuildDurationColumn.Name = "BuildDurationColumn";
            this.BuildDurationColumn.ReadOnly = true;
            this.BuildDurationColumn.ToolTipText = "Build Duration (hh:mm:ss)";
            // 
            // StatusColumn
            // 
            this.StatusColumn.HeaderText = "Status";
            this.StatusColumn.Name = "StatusColumn";
            this.StatusColumn.ReadOnly = true;
            // 
            // RequestedForColumn
            // 
            this.RequestedForColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.RequestedForColumn.HeaderText = "Requested For";
            this.RequestedForColumn.Name = "RequestedForColumn";
            this.RequestedForColumn.ReadOnly = true;
            // 
            // QueueNameColumn
            // 
            this.QueueNameColumn.HeaderText = "Queue Name";
            this.QueueNameColumn.Name = "QueueNameColumn";
            this.QueueNameColumn.ReadOnly = true;
            // 
            // ExtraColumn
            // 
            this.ExtraColumn.HeaderText = "";
            this.ExtraColumn.Name = "ExtraColumn";
            this.ExtraColumn.ReadOnly = true;
            this.ExtraColumn.Width = 10;
            // 
            // BuildStatusContextMenuStrip
            // 
            this.BuildStatusContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyBuildDefinitionToolStripMenuItem,
            this.RequestBuildToolStripMenuItem,
            this.ViewBuildToolStripMenuItem,
            this.ViewBuildDefinitionToolStripMenuItem});
            this.BuildStatusContextMenuStrip.Name = "BuildStatusContextMenuStrip";
            this.BuildStatusContextMenuStrip.ShowImageMargin = false;
            this.BuildStatusContextMenuStrip.Size = new System.Drawing.Size(198, 92);
            // 
            // CopyBuildDefinitionToolStripMenuItem
            // 
            this.CopyBuildDefinitionToolStripMenuItem.Name = "CopyBuildDefinitionToolStripMenuItem";
            this.CopyBuildDefinitionToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.CopyBuildDefinitionToolStripMenuItem.Text = "Copy Build Definition Name";
            this.CopyBuildDefinitionToolStripMenuItem.Click += new System.EventHandler(this.CopyBuildDefinitionToolStripMenuItem_Click);
            // 
            // RequestBuildToolStripMenuItem
            // 
            this.RequestBuildToolStripMenuItem.Name = "RequestBuildToolStripMenuItem";
            this.RequestBuildToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.RequestBuildToolStripMenuItem.Text = "Request Build";
            this.RequestBuildToolStripMenuItem.Click += new System.EventHandler(this.RequestBuildToolStripMenuItem_Click);
            // 
            // ViewBuildToolStripMenuItem
            // 
            this.ViewBuildToolStripMenuItem.Name = "ViewBuildToolStripMenuItem";
            this.ViewBuildToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.ViewBuildToolStripMenuItem.Text = "View Build";
            this.ViewBuildToolStripMenuItem.Click += new System.EventHandler(this.ViewBuildToolStripMenuItem_Click);
            // 
            // ViewBuildDefinitionToolStripMenuItem
            // 
            this.ViewBuildDefinitionToolStripMenuItem.Name = "ViewBuildDefinitionToolStripMenuItem";
            this.ViewBuildDefinitionToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.ViewBuildDefinitionToolStripMenuItem.Text = "View Build Definition";
            this.ViewBuildDefinitionToolStripMenuItem.Click += new System.EventHandler(this.ViewBuildDefinitionToolStripMenuItem_Click);
            // 
            // MissingBuildStatusPanel
            // 
            this.MissingBuildStatusPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MissingBuildStatusPanel.Controls.Add(this.MissingBuildStatusBuildDefinitionListBox);
            this.MissingBuildStatusPanel.Controls.Add(this.MissingBuildStatusBuildDefinitionListLabel);
            this.MissingBuildStatusPanel.Controls.Add(this.MissingBuildStatusLabel);
            this.MissingBuildStatusPanel.Location = new System.Drawing.Point(3, 123);
            this.MissingBuildStatusPanel.Name = "MissingBuildStatusPanel";
            this.MissingBuildStatusPanel.Size = new System.Drawing.Size(714, 311);
            this.MissingBuildStatusPanel.TabIndex = 7;
            this.MissingBuildStatusPanel.Visible = false;
            // 
            // MissingBuildStatusBuildDefinitionListBox
            // 
            this.MissingBuildStatusBuildDefinitionListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MissingBuildStatusBuildDefinitionListBox.ContextMenuStrip = this.MissingBuildStatusContextMenuStrip;
            this.MissingBuildStatusBuildDefinitionListBox.FormattingEnabled = true;
            this.MissingBuildStatusBuildDefinitionListBox.Location = new System.Drawing.Point(6, 37);
            this.MissingBuildStatusBuildDefinitionListBox.Name = "MissingBuildStatusBuildDefinitionListBox";
            this.MissingBuildStatusBuildDefinitionListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.MissingBuildStatusBuildDefinitionListBox.Size = new System.Drawing.Size(700, 264);
            this.MissingBuildStatusBuildDefinitionListBox.TabIndex = 1;
            // 
            // MissingBuildStatusContextMenuStrip
            // 
            this.MissingBuildStatusContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyMissingBuildStatusToolStripMenuItem,
            this.CopyAllMissingBuildStatusToolStripMenuItem,
            this.ViewMissingBuildStatusToolStripMenuItem});
            this.MissingBuildStatusContextMenuStrip.Name = "MissingBuildStatusContextMenuStrip";
            this.MissingBuildStatusContextMenuStrip.Size = new System.Drawing.Size(248, 70);
            this.MissingBuildStatusContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.MissingBuildStatusContextMenuStrip_Opening);
            // 
            // CopyMissingBuildStatusToolStripMenuItem
            // 
            this.CopyMissingBuildStatusToolStripMenuItem.Name = "CopyMissingBuildStatusToolStripMenuItem";
            this.CopyMissingBuildStatusToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.CopyMissingBuildStatusToolStripMenuItem.Text = "Copy Build Definition Name";
            this.CopyMissingBuildStatusToolStripMenuItem.Click += new System.EventHandler(this.CopyMissingBuildStatusToolStripMenuItem_Click);
            // 
            // CopyAllMissingBuildStatusToolStripMenuItem
            // 
            this.CopyAllMissingBuildStatusToolStripMenuItem.Name = "CopyAllMissingBuildStatusToolStripMenuItem";
            this.CopyAllMissingBuildStatusToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.CopyAllMissingBuildStatusToolStripMenuItem.Text = "Copy All  Build Definition Names";
            this.CopyAllMissingBuildStatusToolStripMenuItem.Click += new System.EventHandler(this.CopyAllMissingBuildStatusToolStripMenuItem_Click);
            // 
            // ViewMissingBuildStatusToolStripMenuItem
            // 
            this.ViewMissingBuildStatusToolStripMenuItem.Name = "ViewMissingBuildStatusToolStripMenuItem";
            this.ViewMissingBuildStatusToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.ViewMissingBuildStatusToolStripMenuItem.Text = "View Build Definition";
            this.ViewMissingBuildStatusToolStripMenuItem.Click += new System.EventHandler(this.ViewMissingBuildStatusToolStripMenuItem_Click);
            // 
            // MissingBuildStatusBuildDefinitionListLabel
            // 
            this.MissingBuildStatusBuildDefinitionListLabel.AutoSize = true;
            this.MissingBuildStatusBuildDefinitionListLabel.Location = new System.Drawing.Point(6, 20);
            this.MissingBuildStatusBuildDefinitionListLabel.Name = "MissingBuildStatusBuildDefinitionListLabel";
            this.MissingBuildStatusBuildDefinitionListLabel.Size = new System.Drawing.Size(85, 13);
            this.MissingBuildStatusBuildDefinitionListLabel.TabIndex = 1;
            this.MissingBuildStatusBuildDefinitionListLabel.Text = "Build Definitions:";
            // 
            // MissingBuildStatusLabel
            // 
            this.MissingBuildStatusLabel.AutoSize = true;
            this.MissingBuildStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MissingBuildStatusLabel.Location = new System.Drawing.Point(3, 3);
            this.MissingBuildStatusLabel.Name = "MissingBuildStatusLabel";
            this.MissingBuildStatusLabel.Size = new System.Drawing.Size(121, 13);
            this.MissingBuildStatusLabel.TabIndex = 0;
            this.MissingBuildStatusLabel.Text = "Missing Build Status";
            // 
            // QueuedBuildDefinitionsCheckBox
            // 
            this.QueuedBuildDefinitionsCheckBox.AutoSize = true;
            this.QueuedBuildDefinitionsCheckBox.Location = new System.Drawing.Point(3, 31);
            this.QueuedBuildDefinitionsCheckBox.Name = "QueuedBuildDefinitionsCheckBox";
            this.QueuedBuildDefinitionsCheckBox.Size = new System.Drawing.Size(242, 17);
            this.QueuedBuildDefinitionsCheckBox.TabIndex = 1;
            this.QueuedBuildDefinitionsCheckBox.Text = "Queued/Recently Completed Build Definitions";
            this.QueuedBuildDefinitionsCheckBox.UseVisualStyleBackColor = true;
            this.QueuedBuildDefinitionsCheckBox.CheckedChanged += new System.EventHandler(this.QueuedBuildDefinitionsCheckBox_CheckedChanged);
            // 
            // StatusPanel
            // 
            this.StatusPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.StatusPanel.BackColor = System.Drawing.Color.White;
            this.StatusPanel.Controls.Add(this.BuildListRetrievalResultPanel);
            this.StatusPanel.Controls.Add(this.BuildListTotalsPanel);
            this.StatusPanel.Controls.Add(this.BuildListRetrievalProgressPanel);
            this.StatusPanel.Location = new System.Drawing.Point(3, 85);
            this.StatusPanel.Name = "StatusPanel";
            this.StatusPanel.Size = new System.Drawing.Size(714, 35);
            this.StatusPanel.TabIndex = 12;
            // 
            // BuildListRetrievalResultPanel
            // 
            this.BuildListRetrievalResultPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildListRetrievalResultPanel.BackColor = System.Drawing.Color.Transparent;
            this.BuildListRetrievalResultPanel.Controls.Add(this.BuildListRetrievalResultLabel);
            this.BuildListRetrievalResultPanel.Location = new System.Drawing.Point(1, 0);
            this.BuildListRetrievalResultPanel.Name = "BuildListRetrievalResultPanel";
            this.BuildListRetrievalResultPanel.Size = new System.Drawing.Size(376, 32);
            this.BuildListRetrievalResultPanel.TabIndex = 0;
            // 
            // BuildListRetrievalResultLabel
            // 
            this.BuildListRetrievalResultLabel.AutoSize = true;
            this.BuildListRetrievalResultLabel.Location = new System.Drawing.Point(3, 10);
            this.BuildListRetrievalResultLabel.Name = "BuildListRetrievalResultLabel";
            this.BuildListRetrievalResultLabel.Size = new System.Drawing.Size(49, 13);
            this.BuildListRetrievalResultLabel.TabIndex = 0;
            this.BuildListRetrievalResultLabel.Text = "<results>";
            // 
            // BuildListTotalsPanel
            // 
            this.BuildListTotalsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildListTotalsPanel.BackColor = System.Drawing.Color.Transparent;
            this.BuildListTotalsPanel.Controls.Add(this.RecalculateButton);
            this.BuildListTotalsPanel.Controls.Add(this.BuildListTotalClockTimeLabel);
            this.BuildListTotalsPanel.Controls.Add(this.BuildListTotalDurationLabel);
            this.BuildListTotalsPanel.Controls.Add(this.BuildListTotalClockTimeValueLabel);
            this.BuildListTotalsPanel.Controls.Add(this.BuildListTotalDurationValueLabel);
            this.BuildListTotalsPanel.Location = new System.Drawing.Point(406, 0);
            this.BuildListTotalsPanel.Name = "BuildListTotalsPanel";
            this.BuildListTotalsPanel.Size = new System.Drawing.Size(305, 32);
            this.BuildListTotalsPanel.TabIndex = 1;
            // 
            // BuildListTotalClockTimeLabel
            // 
            this.BuildListTotalClockTimeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildListTotalClockTimeLabel.AutoSize = true;
            this.BuildListTotalClockTimeLabel.Location = new System.Drawing.Point(176, 10);
            this.BuildListTotalClockTimeLabel.Name = "BuildListTotalClockTimeLabel";
            this.BuildListTotalClockTimeLabel.Size = new System.Drawing.Size(63, 13);
            this.BuildListTotalClockTimeLabel.TabIndex = 4;
            this.BuildListTotalClockTimeLabel.Text = "Clock Time:";
            // 
            // BuildListTotalDurationLabel
            // 
            this.BuildListTotalDurationLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildListTotalDurationLabel.AutoSize = true;
            this.BuildListTotalDurationLabel.Location = new System.Drawing.Point(36, 10);
            this.BuildListTotalDurationLabel.Name = "BuildListTotalDurationLabel";
            this.BuildListTotalDurationLabel.Size = new System.Drawing.Size(81, 13);
            this.BuildListTotalDurationLabel.TabIndex = 2;
            this.BuildListTotalDurationLabel.Text = "Build Durations:";
            // 
            // BuildListTotalClockTimeValueLabel
            // 
            this.BuildListTotalClockTimeValueLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildListTotalClockTimeValueLabel.AutoSize = true;
            this.BuildListTotalClockTimeValueLabel.Location = new System.Drawing.Point(239, 10);
            this.BuildListTotalClockTimeValueLabel.Name = "BuildListTotalClockTimeValueLabel";
            this.BuildListTotalClockTimeValueLabel.Size = new System.Drawing.Size(61, 13);
            this.BuildListTotalClockTimeValueLabel.TabIndex = 2;
            this.BuildListTotalClockTimeValueLabel.Text = "9999:99:99";
            // 
            // BuildListTotalDurationValueLabel
            // 
            this.BuildListTotalDurationValueLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildListTotalDurationValueLabel.AutoSize = true;
            this.BuildListTotalDurationValueLabel.Location = new System.Drawing.Point(115, 10);
            this.BuildListTotalDurationValueLabel.Name = "BuildListTotalDurationValueLabel";
            this.BuildListTotalDurationValueLabel.Size = new System.Drawing.Size(61, 13);
            this.BuildListTotalDurationValueLabel.TabIndex = 1;
            this.BuildListTotalDurationValueLabel.Text = "9999:99:99";
            // 
            // BuildListRetrievalProgressPanel
            // 
            this.BuildListRetrievalProgressPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildListRetrievalProgressPanel.Controls.Add(this.BuildListRetrievalProgressBar);
            this.BuildListRetrievalProgressPanel.Controls.Add(this.BuildListRetrievalProgressLabel);
            this.BuildListRetrievalProgressPanel.Location = new System.Drawing.Point(1, 1);
            this.BuildListRetrievalProgressPanel.Name = "BuildListRetrievalProgressPanel";
            this.BuildListRetrievalProgressPanel.Size = new System.Drawing.Size(399, 31);
            this.BuildListRetrievalProgressPanel.TabIndex = 0;
            // 
            // BuildListRetrievalProgressBar
            // 
            this.BuildListRetrievalProgressBar.Location = new System.Drawing.Point(3, 18);
            this.BuildListRetrievalProgressBar.Name = "BuildListRetrievalProgressBar";
            this.BuildListRetrievalProgressBar.Size = new System.Drawing.Size(100, 10);
            this.BuildListRetrievalProgressBar.TabIndex = 1;
            // 
            // BuildListRetrievalProgressLabel
            // 
            this.BuildListRetrievalProgressLabel.AutoSize = true;
            this.BuildListRetrievalProgressLabel.Location = new System.Drawing.Point(3, 3);
            this.BuildListRetrievalProgressLabel.Name = "BuildListRetrievalProgressLabel";
            this.BuildListRetrievalProgressLabel.Size = new System.Drawing.Size(59, 13);
            this.BuildListRetrievalProgressLabel.TabIndex = 0;
            this.BuildListRetrievalProgressLabel.Text = "<progress>";
            // 
            // AutoRefreshPanel
            // 
            this.AutoRefreshPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.AutoRefreshPanel.Controls.Add(this.StopRefreshInMinutesNumericUpDown);
            this.AutoRefreshPanel.Controls.Add(this.AutoRefreshBuildStatusCheckBox);
            this.AutoRefreshPanel.Controls.Add(this.RefreshInMinutesNumericUpDown);
            this.AutoRefreshPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.AutoRefreshPanel.Location = new System.Drawing.Point(231, 55);
            this.AutoRefreshPanel.Name = "AutoRefreshPanel";
            this.AutoRefreshPanel.Size = new System.Drawing.Size(185, 30);
            this.AutoRefreshPanel.TabIndex = 4;
            // 
            // ManualRefreshPanel
            // 
            this.ManualRefreshPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ManualRefreshPanel.Controls.Add(this.RecentlyCompletedInHoursNumericUpDown);
            this.ManualRefreshPanel.Controls.Add(this.TopNumericUpDown);
            this.ManualRefreshPanel.Controls.Add(this.DateFilterComboBox);
            this.ManualRefreshPanel.Controls.Add(this.RefreshButton);
            this.ManualRefreshPanel.Location = new System.Drawing.Point(3, 55);
            this.ManualRefreshPanel.Name = "ManualRefreshPanel";
            this.ManualRefreshPanel.Size = new System.Drawing.Size(222, 30);
            this.ManualRefreshPanel.TabIndex = 2;
            // 
            // ControlBarStatusLabel
            // 
            this.ControlBarStatusLabel.AutoSize = true;
            this.ControlBarStatusLabel.Location = new System.Drawing.Point(522, 64);
            this.ControlBarStatusLabel.Name = "ControlBarStatusLabel";
            this.ControlBarStatusLabel.Size = new System.Drawing.Size(100, 13);
            this.ControlBarStatusLabel.TabIndex = 7;
            this.ControlBarStatusLabel.Text = "<control bar status>";
            // 
            // AutoRefreshBuildStatusTimer
            // 
            this.AutoRefreshBuildStatusTimer.Tick += new System.EventHandler(this.AutoRefreshBuildStatusTimer_Tick);
            // 
            // StatusUpdateTimer
            // 
            this.StatusUpdateTimer.Tick += new System.EventHandler(this.StatusUpdateTimer_Tick);
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.VsoBuild.Sidekicks.Help.chm";
            // 
            // BuildStatusCheckForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1382, 480);
            this.ControlBox = false;
            this.Controls.Add(this.PropertiesPanel);
            this.Controls.Add(this.VerticalSplitter);
            this.Controls.Add(this.BuildDefinitionSearchPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "BuildStatusCheckForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.BuildDefinitionSearchPanel.ResumeLayout(false);
            this.BuildDefinitionSearchPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecentlyCompletedInHoursNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StopRefreshInMinutesNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RefreshInMinutesNumericUpDown)).EndInit();
            this.CopyContextMenuStrip.ResumeLayout(false);
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.PropertiesPanel.ResumeLayout(false);
            this.PropertiesPanel.PerformLayout();
            this.BuildStatusPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BuildStatusDataGridView)).EndInit();
            this.BuildStatusContextMenuStrip.ResumeLayout(false);
            this.MissingBuildStatusPanel.ResumeLayout(false);
            this.MissingBuildStatusPanel.PerformLayout();
            this.MissingBuildStatusContextMenuStrip.ResumeLayout(false);
            this.StatusPanel.ResumeLayout(false);
            this.BuildListRetrievalResultPanel.ResumeLayout(false);
            this.BuildListRetrievalResultPanel.PerformLayout();
            this.BuildListTotalsPanel.ResumeLayout(false);
            this.BuildListTotalsPanel.PerformLayout();
            this.BuildListRetrievalProgressPanel.ResumeLayout(false);
            this.BuildListRetrievalProgressPanel.PerformLayout();
            this.AutoRefreshPanel.ResumeLayout(false);
            this.AutoRefreshPanel.PerformLayout();
            this.ManualRefreshPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Splitter VerticalSplitter;
        private System.Windows.Forms.Panel BuildDefinitionSearchPanel;
        private System.Windows.Forms.Label BuildDefinitionSearchPlaceholderLabel;
        private System.Windows.Forms.ToolTip BuildStatusCheckToolTip;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Panel BuildStatusPanel;
        private System.Windows.Forms.DataGridView BuildStatusDataGridView;
        private System.Windows.Forms.Button SwapBuildStatusViewButton;
        private System.Windows.Forms.Panel MissingBuildStatusPanel;
        private System.Windows.Forms.ListBox MissingBuildStatusBuildDefinitionListBox;
        private System.Windows.Forms.Label MissingBuildStatusBuildDefinitionListLabel;
        private System.Windows.Forms.Label MissingBuildStatusLabel;
        private System.Windows.Forms.ContextMenuStrip BuildStatusContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ViewBuildToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip MissingBuildStatusContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CopyMissingBuildStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ViewMissingBuildStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CopyAllMissingBuildStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ViewBuildDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CopyBuildDefinitionToolStripMenuItem;
        private System.Windows.Forms.Timer AutoRefreshBuildStatusTimer;
        private System.Windows.Forms.Label ControlBarStatusLabel;
        private SplitButton CopySplitButton;
        private System.Windows.Forms.ContextMenuStrip CopyContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CopyAsTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CopyAsHTMLToolStripMenuItem;
        private System.Windows.Forms.Timer StatusUpdateTimer;
        private System.Windows.Forms.Panel AutoRefreshPanel;
        private System.Windows.Forms.Panel ManualRefreshPanel;
        private System.Windows.Forms.NumericUpDown RecentlyCompletedInHoursNumericUpDown;
        private System.Windows.Forms.NumericUpDown TopNumericUpDown;
        private System.Windows.Forms.ComboBox DateFilterComboBox;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.NumericUpDown StopRefreshInMinutesNumericUpDown;
        private System.Windows.Forms.CheckBox AutoRefreshBuildStatusCheckBox;
        private System.Windows.Forms.NumericUpDown RefreshInMinutesNumericUpDown;
        private System.Windows.Forms.ToolStripMenuItem RequestBuildToolStripMenuItem;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.Panel StatusPanel;
        private System.Windows.Forms.Panel BuildListTotalsPanel;
        private System.Windows.Forms.Label BuildListTotalClockTimeLabel;
        private System.Windows.Forms.Label BuildListTotalDurationLabel;
        private System.Windows.Forms.Label BuildListTotalClockTimeValueLabel;
        private System.Windows.Forms.Label BuildListTotalDurationValueLabel;
        private System.Windows.Forms.Panel BuildListRetrievalProgressPanel;
        private System.Windows.Forms.Label BuildListRetrievalProgressLabel;
        private System.Windows.Forms.ProgressBar BuildListRetrievalProgressBar;
        private System.Windows.Forms.Panel BuildListRetrievalResultPanel;
        private System.Windows.Forms.Label BuildListRetrievalResultLabel;
        private System.Windows.Forms.Button RecalculateButton;
        private System.Windows.Forms.CheckBox QueuedBuildDefinitionsCheckBox;
        private System.Windows.Forms.Button HelpButton;
        private System.Windows.Forms.DataGridViewImageColumn ReasonColumn;
        private System.Windows.Forms.DataGridViewImageColumn ResultColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuildNumberColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuildDefinitionColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn QueueTimeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartTimeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn FinishTimeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuildDurationColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RequestedForColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn QueueNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExtraColumn;
    }
}