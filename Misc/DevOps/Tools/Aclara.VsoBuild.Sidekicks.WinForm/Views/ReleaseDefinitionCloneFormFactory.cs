﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public class ReleaseDefinitionCloneFormFactory
    {
        /// <summary>
        /// Create form.
        /// </summary>
        /// <returns></returns>
        public static ReleaseDefinitionCloneForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                            SidekickConfiguration sidekickConfiguration,
                                                            ReleaseDefinitionSearchForm releaseDefinitionSearchForm)
        {

            ReleaseDefinitionCloneForm result = null;
            ReleaseDefinitionClonePresenter ReleaseDefinitionClonePresenter = null;

            try
            {
                result = new ReleaseDefinitionCloneForm(sidekickConfiguration, releaseDefinitionSearchForm);
                ReleaseDefinitionClonePresenter = new ReleaseDefinitionClonePresenter(teamProjectInfo, sidekickConfiguration, result);
                result.ReleaseDefinitionClonePresenter = ReleaseDefinitionClonePresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
