﻿using Aclara.Vsts.TeamsAndProjects.Client.Models;
using Aclara.Vsts.TeamsAndProjects.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    /// <summary>
    /// VSTS project connect presenter.
    /// </summary>
    public class VSTSProjectConnectPresenter
    {
        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve team projects.
        /// </summary>
        /// <returns></returns>
        public TeamProjectList GetTeamProjects(Uri vstsAccountUri,
                                               string basicAuthRestApiUserProfileName,
                                               string basicAuthRestApiPassword)
        {
            TeamProjectList result = null;
            TeamsAndProjectsManager teamsAndProjectsManager = null;

            try
            {
                teamsAndProjectsManager = TeamsAndProjectsManagerFactory.CreateProjectsAndTeamsManager(vstsAccountUri, 
                                                                                                       basicAuthRestApiUserProfileName, 
                                                                                                       basicAuthRestApiPassword);

                result = teamsAndProjectsManager.GetTeamProjects();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }
}
