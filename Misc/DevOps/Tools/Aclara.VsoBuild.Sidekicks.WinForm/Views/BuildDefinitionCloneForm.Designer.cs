﻿namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    partial class BuildDefinitionCloneForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.ReleaseDefinitionOptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.SourceReleaseDefinitionDoesNotExistLabel = new System.Windows.Forms.Label();
            this.DoNotCloneBuildDefinitionsCheckBox = new System.Windows.Forms.CheckBox();
            this.MigrateBuildStepsCheckBox = new System.Windows.Forms.CheckBox();
            this.CreateMissingReleaseDefinitionsCheckBox = new System.Windows.Forms.CheckBox();
            this.CloneReleaseDefinitionsCheckBox = new System.Windows.Forms.CheckBox();
            this.DirectionToTargetPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.HelpButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.DirectionFromSourcePanel = new System.Windows.Forms.Panel();
            this.DirectionLabel = new System.Windows.Forms.Label();
            this.TargetPropertiesPanel = new System.Windows.Forms.Panel();
            this.RepositoryLabel = new System.Windows.Forms.Label();
            this.RepositoryComboBox = new System.Windows.Forms.ComboBox();
            this.BuildDefinitionFolderComboBox = new System.Windows.Forms.ComboBox();
            this.BuildDefinitionFolderLabel = new System.Windows.Forms.Label();
            this.RepositoryTypeLabel = new System.Windows.Forms.Label();
            this.RepositoryTypeComboBox = new System.Windows.Forms.ComboBox();
            this.TargetHeaderPanel = new System.Windows.Forms.Panel();
            this.TargetLabel = new System.Windows.Forms.Label();
            this.ResetButton = new System.Windows.Forms.Button();
            this.BuildDefinitionNamePrefixTargetTextBox = new System.Windows.Forms.TextBox();
            this.BuildDefinitionNamePrefixTargetLabel = new System.Windows.Forms.Label();
            this.BranchNameTargetTextBox = new System.Windows.Forms.TextBox();
            this.BranchNameTargetLabel = new System.Windows.Forms.Label();
            this.SourcePropertiesPanel = new System.Windows.Forms.Panel();
            this.SourceHeaderPanel = new System.Windows.Forms.Panel();
            this.SourceLabel = new System.Windows.Forms.Label();
            this.BuildDefinitionNamePrefixSourceTextBox = new System.Windows.Forms.TextBox();
            this.BuildDefinitionNamePrefixSourceLabel = new System.Windows.Forms.Label();
            this.BranchNameSourceTextBox = new System.Windows.Forms.TextBox();
            this.BranchNameSourceLabel = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.CloneBuildDefinitionProgressBar = new System.Windows.Forms.ProgressBar();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.VerticalSplitter = new System.Windows.Forms.Splitter();
            this.BuildDefinitionSearchPanel = new System.Windows.Forms.Panel();
            this.BuildDefinitionSearchPlaceholderLabel = new System.Windows.Forms.Label();
            this.CloneBuildDefinitionToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.MigrateToGitCheckBox = new System.Windows.Forms.CheckBox();
            this.PropertiesPanel.SuspendLayout();
            this.ReleaseDefinitionOptionsGroupBox.SuspendLayout();
            this.DirectionToTargetPanel.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.DirectionFromSourcePanel.SuspendLayout();
            this.TargetPropertiesPanel.SuspendLayout();
            this.TargetHeaderPanel.SuspendLayout();
            this.SourcePropertiesPanel.SuspendLayout();
            this.SourceHeaderPanel.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.BuildDefinitionSearchPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Controls.Add(this.ReleaseDefinitionOptionsGroupBox);
            this.PropertiesPanel.Controls.Add(this.DirectionToTargetPanel);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Controls.Add(this.DirectionFromSourcePanel);
            this.PropertiesPanel.Controls.Add(this.TargetPropertiesPanel);
            this.PropertiesPanel.Controls.Add(this.SourcePropertiesPanel);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesPanel.Location = new System.Drawing.Point(650, 0);
            this.PropertiesPanel.MinimumSize = new System.Drawing.Size(560, 480);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(566, 706);
            this.PropertiesPanel.TabIndex = 2;
            // 
            // ReleaseDefinitionOptionsGroupBox
            // 
            this.ReleaseDefinitionOptionsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReleaseDefinitionOptionsGroupBox.Controls.Add(this.SourceReleaseDefinitionDoesNotExistLabel);
            this.ReleaseDefinitionOptionsGroupBox.Controls.Add(this.DoNotCloneBuildDefinitionsCheckBox);
            this.ReleaseDefinitionOptionsGroupBox.Controls.Add(this.MigrateBuildStepsCheckBox);
            this.ReleaseDefinitionOptionsGroupBox.Controls.Add(this.CreateMissingReleaseDefinitionsCheckBox);
            this.ReleaseDefinitionOptionsGroupBox.Controls.Add(this.CloneReleaseDefinitionsCheckBox);
            this.ReleaseDefinitionOptionsGroupBox.Location = new System.Drawing.Point(4, 485);
            this.ReleaseDefinitionOptionsGroupBox.Name = "ReleaseDefinitionOptionsGroupBox";
            this.ReleaseDefinitionOptionsGroupBox.Size = new System.Drawing.Size(465, 139);
            this.ReleaseDefinitionOptionsGroupBox.TabIndex = 4;
            this.ReleaseDefinitionOptionsGroupBox.TabStop = false;
            this.ReleaseDefinitionOptionsGroupBox.Text = "Release Definition Options";
            // 
            // SourceReleaseDefinitionDoesNotExistLabel
            // 
            this.SourceReleaseDefinitionDoesNotExistLabel.AutoSize = true;
            this.SourceReleaseDefinitionDoesNotExistLabel.Location = new System.Drawing.Point(28, 59);
            this.SourceReleaseDefinitionDoesNotExistLabel.Name = "SourceReleaseDefinitionDoesNotExistLabel";
            this.SourceReleaseDefinitionDoesNotExistLabel.Size = new System.Drawing.Size(201, 13);
            this.SourceReleaseDefinitionDoesNotExistLabel.TabIndex = 2;
            this.SourceReleaseDefinitionDoesNotExistLabel.Text = "If source release definition does not exist:";
            // 
            // DoNotCloneBuildDefinitionsCheckBox
            // 
            this.DoNotCloneBuildDefinitionsCheckBox.AutoSize = true;
            this.DoNotCloneBuildDefinitionsCheckBox.Location = new System.Drawing.Point(10, 19);
            this.DoNotCloneBuildDefinitionsCheckBox.Name = "DoNotCloneBuildDefinitionsCheckBox";
            this.DoNotCloneBuildDefinitionsCheckBox.Size = new System.Drawing.Size(162, 17);
            this.DoNotCloneBuildDefinitionsCheckBox.TabIndex = 0;
            this.DoNotCloneBuildDefinitionsCheckBox.Text = "Do not clone build definitions";
            this.DoNotCloneBuildDefinitionsCheckBox.UseVisualStyleBackColor = true;
            // 
            // MigrateBuildStepsCheckBox
            // 
            this.MigrateBuildStepsCheckBox.AutoSize = true;
            this.MigrateBuildStepsCheckBox.Location = new System.Drawing.Point(32, 101);
            this.MigrateBuildStepsCheckBox.Name = "MigrateBuildStepsCheckBox";
            this.MigrateBuildStepsCheckBox.Size = new System.Drawing.Size(114, 17);
            this.MigrateBuildStepsCheckBox.TabIndex = 4;
            this.MigrateBuildStepsCheckBox.Text = "Migrate build steps";
            this.MigrateBuildStepsCheckBox.UseVisualStyleBackColor = true;
            // 
            // CreateMissingReleaseDefinitionsCheckBox
            // 
            this.CreateMissingReleaseDefinitionsCheckBox.AutoSize = true;
            this.CreateMissingReleaseDefinitionsCheckBox.Location = new System.Drawing.Point(32, 78);
            this.CreateMissingReleaseDefinitionsCheckBox.Name = "CreateMissingReleaseDefinitionsCheckBox";
            this.CreateMissingReleaseDefinitionsCheckBox.Size = new System.Drawing.Size(144, 17);
            this.CreateMissingReleaseDefinitionsCheckBox.TabIndex = 3;
            this.CreateMissingReleaseDefinitionsCheckBox.Text = "Create release definitions";
            this.CreateMissingReleaseDefinitionsCheckBox.UseVisualStyleBackColor = true;
            this.CreateMissingReleaseDefinitionsCheckBox.CheckedChanged += new System.EventHandler(this.CreateMissingReleaseDefinitionsCheckBox_CheckedChanged);
            // 
            // CloneReleaseDefinitionsCheckBox
            // 
            this.CloneReleaseDefinitionsCheckBox.AutoSize = true;
            this.CloneReleaseDefinitionsCheckBox.Location = new System.Drawing.Point(10, 42);
            this.CloneReleaseDefinitionsCheckBox.Name = "CloneReleaseDefinitionsCheckBox";
            this.CloneReleaseDefinitionsCheckBox.Size = new System.Drawing.Size(291, 17);
            this.CloneReleaseDefinitionsCheckBox.TabIndex = 1;
            this.CloneReleaseDefinitionsCheckBox.Text = "Clone release definitions associated with build definitions";
            this.CloneReleaseDefinitionsCheckBox.UseVisualStyleBackColor = true;
            this.CloneReleaseDefinitionsCheckBox.CheckedChanged += new System.EventHandler(this.CloneReleaseDefinitionsCheckBox_CheckedChanged);
            // 
            // DirectionToTargetPanel
            // 
            this.DirectionToTargetPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DirectionToTargetPanel.BackColor = System.Drawing.Color.Gainsboro;
            this.DirectionToTargetPanel.Controls.Add(this.label1);
            this.DirectionToTargetPanel.Location = new System.Drawing.Point(476, 234);
            this.DirectionToTargetPanel.Name = "DirectionToTargetPanel";
            this.DirectionToTargetPanel.Size = new System.Drawing.Size(83, 21);
            this.DirectionToTargetPanel.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "<< << <<";
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.HelpButton);
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(566, 25);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(538, 0);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 1;
            this.CloneBuildDefinitionToolTip.SetToolTip(this.HelpButton, "Display help.");
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(0, 2);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(529, 21);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "Clone Build Definitions";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DirectionFromSourcePanel
            // 
            this.DirectionFromSourcePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DirectionFromSourcePanel.BackColor = System.Drawing.Color.Gainsboro;
            this.DirectionFromSourcePanel.Controls.Add(this.DirectionLabel);
            this.DirectionFromSourcePanel.Location = new System.Drawing.Point(475, 99);
            this.DirectionFromSourcePanel.Name = "DirectionFromSourcePanel";
            this.DirectionFromSourcePanel.Size = new System.Drawing.Size(83, 21);
            this.DirectionFromSourcePanel.TabIndex = 0;
            // 
            // DirectionLabel
            // 
            this.DirectionLabel.AutoSize = true;
            this.DirectionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DirectionLabel.Location = new System.Drawing.Point(15, 4);
            this.DirectionLabel.Name = "DirectionLabel";
            this.DirectionLabel.Size = new System.Drawing.Size(57, 13);
            this.DirectionLabel.TabIndex = 0;
            this.DirectionLabel.Text = ">> >> >>";
            // 
            // TargetPropertiesPanel
            // 
            this.TargetPropertiesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TargetPropertiesPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TargetPropertiesPanel.Controls.Add(this.MigrateToGitCheckBox);
            this.TargetPropertiesPanel.Controls.Add(this.RepositoryLabel);
            this.TargetPropertiesPanel.Controls.Add(this.RepositoryComboBox);
            this.TargetPropertiesPanel.Controls.Add(this.BuildDefinitionFolderComboBox);
            this.TargetPropertiesPanel.Controls.Add(this.BuildDefinitionFolderLabel);
            this.TargetPropertiesPanel.Controls.Add(this.RepositoryTypeLabel);
            this.TargetPropertiesPanel.Controls.Add(this.RepositoryTypeComboBox);
            this.TargetPropertiesPanel.Controls.Add(this.TargetHeaderPanel);
            this.TargetPropertiesPanel.Controls.Add(this.ResetButton);
            this.TargetPropertiesPanel.Controls.Add(this.BuildDefinitionNamePrefixTargetTextBox);
            this.TargetPropertiesPanel.Controls.Add(this.BuildDefinitionNamePrefixTargetLabel);
            this.TargetPropertiesPanel.Controls.Add(this.BranchNameTargetTextBox);
            this.TargetPropertiesPanel.Controls.Add(this.BranchNameTargetLabel);
            this.TargetPropertiesPanel.Location = new System.Drawing.Point(6, 164);
            this.TargetPropertiesPanel.Name = "TargetPropertiesPanel";
            this.TargetPropertiesPanel.Size = new System.Drawing.Size(463, 315);
            this.TargetPropertiesPanel.TabIndex = 2;
            // 
            // RepositoryLabel
            // 
            this.RepositoryLabel.AutoSize = true;
            this.RepositoryLabel.Location = new System.Drawing.Point(3, 181);
            this.RepositoryLabel.Name = "RepositoryLabel";
            this.RepositoryLabel.Size = new System.Drawing.Size(60, 13);
            this.RepositoryLabel.TabIndex = 10;
            this.RepositoryLabel.Text = "Repository:";
            // 
            // RepositoryComboBox
            // 
            this.RepositoryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RepositoryComboBox.FormattingEnabled = true;
            this.RepositoryComboBox.Location = new System.Drawing.Point(3, 197);
            this.RepositoryComboBox.Name = "RepositoryComboBox";
            this.RepositoryComboBox.Size = new System.Drawing.Size(372, 21);
            this.RepositoryComboBox.TabIndex = 11;
            // 
            // BuildDefinitionFolderComboBox
            // 
            this.BuildDefinitionFolderComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildDefinitionFolderComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BuildDefinitionFolderComboBox.FormattingEnabled = true;
            this.BuildDefinitionFolderComboBox.Location = new System.Drawing.Point(3, 248);
            this.BuildDefinitionFolderComboBox.Name = "BuildDefinitionFolderComboBox";
            this.BuildDefinitionFolderComboBox.Size = new System.Drawing.Size(369, 21);
            this.BuildDefinitionFolderComboBox.TabIndex = 0;
            // 
            // BuildDefinitionFolderLabel
            // 
            this.BuildDefinitionFolderLabel.AutoSize = true;
            this.BuildDefinitionFolderLabel.Location = new System.Drawing.Point(3, 231);
            this.BuildDefinitionFolderLabel.Name = "BuildDefinitionFolderLabel";
            this.BuildDefinitionFolderLabel.Size = new System.Drawing.Size(107, 13);
            this.BuildDefinitionFolderLabel.TabIndex = 12;
            this.BuildDefinitionFolderLabel.Text = "Build definition folder:";
            // 
            // RepositoryTypeLabel
            // 
            this.RepositoryTypeLabel.AutoSize = true;
            this.RepositoryTypeLabel.Location = new System.Drawing.Point(3, 132);
            this.RepositoryTypeLabel.Name = "RepositoryTypeLabel";
            this.RepositoryTypeLabel.Size = new System.Drawing.Size(83, 13);
            this.RepositoryTypeLabel.TabIndex = 8;
            this.RepositoryTypeLabel.Text = "Repository type:";
            // 
            // RepositoryTypeComboBox
            // 
            this.RepositoryTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RepositoryTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RepositoryTypeComboBox.FormattingEnabled = true;
            this.RepositoryTypeComboBox.Location = new System.Drawing.Point(3, 148);
            this.RepositoryTypeComboBox.Name = "RepositoryTypeComboBox";
            this.RepositoryTypeComboBox.Size = new System.Drawing.Size(369, 21);
            this.RepositoryTypeComboBox.TabIndex = 9;
            this.RepositoryTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.RepositoryTypeComboBox_SelectedIndexChanged);
            // 
            // TargetHeaderPanel
            // 
            this.TargetHeaderPanel.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.TargetHeaderPanel.Controls.Add(this.TargetLabel);
            this.TargetHeaderPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TargetHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.TargetHeaderPanel.Name = "TargetHeaderPanel";
            this.TargetHeaderPanel.Size = new System.Drawing.Size(461, 22);
            this.TargetHeaderPanel.TabIndex = 3;
            // 
            // TargetLabel
            // 
            this.TargetLabel.AutoSize = true;
            this.TargetLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TargetLabel.Location = new System.Drawing.Point(4, 2);
            this.TargetLabel.Name = "TargetLabel";
            this.TargetLabel.Size = new System.Drawing.Size(56, 17);
            this.TargetLabel.TabIndex = 0;
            this.TargetLabel.Text = "Target";
            // 
            // ResetButton
            // 
            this.ResetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ResetButton.Location = new System.Drawing.Point(378, 248);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(75, 23);
            this.ResetButton.TabIndex = 1;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // BuildDefinitionNamePrefixTargetTextBox
            // 
            this.BuildDefinitionNamePrefixTargetTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildDefinitionNamePrefixTargetTextBox.Location = new System.Drawing.Point(3, 99);
            this.BuildDefinitionNamePrefixTargetTextBox.Name = "BuildDefinitionNamePrefixTargetTextBox";
            this.BuildDefinitionNamePrefixTargetTextBox.Size = new System.Drawing.Size(454, 20);
            this.BuildDefinitionNamePrefixTargetTextBox.TabIndex = 7;
            // 
            // BuildDefinitionNamePrefixTargetLabel
            // 
            this.BuildDefinitionNamePrefixTargetLabel.AutoSize = true;
            this.BuildDefinitionNamePrefixTargetLabel.Location = new System.Drawing.Point(3, 83);
            this.BuildDefinitionNamePrefixTargetLabel.Name = "BuildDefinitionNamePrefixTargetLabel";
            this.BuildDefinitionNamePrefixTargetLabel.Size = new System.Drawing.Size(106, 13);
            this.BuildDefinitionNamePrefixTargetLabel.TabIndex = 6;
            this.BuildDefinitionNamePrefixTargetLabel.Text = "Build definition prefix:";
            // 
            // BranchNameTargetTextBox
            // 
            this.BranchNameTargetTextBox.Location = new System.Drawing.Point(3, 50);
            this.BranchNameTargetTextBox.Name = "BranchNameTargetTextBox";
            this.BranchNameTargetTextBox.Size = new System.Drawing.Size(53, 20);
            this.BranchNameTargetTextBox.TabIndex = 5;
            this.CloneBuildDefinitionToolTip.SetToolTip(this.BranchNameTargetTextBox, "Example: 16.07");
            this.BranchNameTargetTextBox.Leave += new System.EventHandler(this.BranchNameTargetTextBox_Leave);
            // 
            // BranchNameTargetLabel
            // 
            this.BranchNameTargetLabel.AutoSize = true;
            this.BranchNameTargetLabel.Location = new System.Drawing.Point(3, 34);
            this.BranchNameTargetLabel.Name = "BranchNameTargetLabel";
            this.BranchNameTargetLabel.Size = new System.Drawing.Size(41, 13);
            this.BranchNameTargetLabel.TabIndex = 4;
            this.BranchNameTargetLabel.Text = "Branch";
            // 
            // SourcePropertiesPanel
            // 
            this.SourcePropertiesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SourcePropertiesPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SourcePropertiesPanel.Controls.Add(this.SourceHeaderPanel);
            this.SourcePropertiesPanel.Controls.Add(this.BuildDefinitionNamePrefixSourceTextBox);
            this.SourcePropertiesPanel.Controls.Add(this.BuildDefinitionNamePrefixSourceLabel);
            this.SourcePropertiesPanel.Controls.Add(this.BranchNameSourceTextBox);
            this.SourcePropertiesPanel.Controls.Add(this.BranchNameSourceLabel);
            this.SourcePropertiesPanel.Location = new System.Drawing.Point(6, 29);
            this.SourcePropertiesPanel.Name = "SourcePropertiesPanel";
            this.SourcePropertiesPanel.Size = new System.Drawing.Size(463, 131);
            this.SourcePropertiesPanel.TabIndex = 0;
            // 
            // SourceHeaderPanel
            // 
            this.SourceHeaderPanel.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.SourceHeaderPanel.Controls.Add(this.SourceLabel);
            this.SourceHeaderPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.SourceHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.SourceHeaderPanel.Name = "SourceHeaderPanel";
            this.SourceHeaderPanel.Size = new System.Drawing.Size(461, 22);
            this.SourceHeaderPanel.TabIndex = 0;
            // 
            // SourceLabel
            // 
            this.SourceLabel.AutoSize = true;
            this.SourceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SourceLabel.Location = new System.Drawing.Point(3, 2);
            this.SourceLabel.Name = "SourceLabel";
            this.SourceLabel.Size = new System.Drawing.Size(59, 17);
            this.SourceLabel.TabIndex = 0;
            this.SourceLabel.Text = "Source";
            // 
            // BuildDefinitionNamePrefixSourceTextBox
            // 
            this.BuildDefinitionNamePrefixSourceTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildDefinitionNamePrefixSourceTextBox.Location = new System.Drawing.Point(3, 99);
            this.BuildDefinitionNamePrefixSourceTextBox.Name = "BuildDefinitionNamePrefixSourceTextBox";
            this.BuildDefinitionNamePrefixSourceTextBox.Size = new System.Drawing.Size(456, 20);
            this.BuildDefinitionNamePrefixSourceTextBox.TabIndex = 4;
            this.CloneBuildDefinitionToolTip.SetToolTip(this.BuildDefinitionNamePrefixSourceTextBox, "Example: ACE_16.06_");
            // 
            // BuildDefinitionNamePrefixSourceLabel
            // 
            this.BuildDefinitionNamePrefixSourceLabel.AutoSize = true;
            this.BuildDefinitionNamePrefixSourceLabel.Location = new System.Drawing.Point(3, 83);
            this.BuildDefinitionNamePrefixSourceLabel.Name = "BuildDefinitionNamePrefixSourceLabel";
            this.BuildDefinitionNamePrefixSourceLabel.Size = new System.Drawing.Size(106, 13);
            this.BuildDefinitionNamePrefixSourceLabel.TabIndex = 3;
            this.BuildDefinitionNamePrefixSourceLabel.Text = "Build definition prefix:";
            // 
            // BranchNameSourceTextBox
            // 
            this.BranchNameSourceTextBox.Location = new System.Drawing.Point(3, 50);
            this.BranchNameSourceTextBox.Name = "BranchNameSourceTextBox";
            this.BranchNameSourceTextBox.Size = new System.Drawing.Size(53, 20);
            this.BranchNameSourceTextBox.TabIndex = 2;
            this.CloneBuildDefinitionToolTip.SetToolTip(this.BranchNameSourceTextBox, "Example: 16.06");
            // 
            // BranchNameSourceLabel
            // 
            this.BranchNameSourceLabel.AutoSize = true;
            this.BranchNameSourceLabel.Location = new System.Drawing.Point(3, 34);
            this.BranchNameSourceLabel.Name = "BranchNameSourceLabel";
            this.BranchNameSourceLabel.Size = new System.Drawing.Size(44, 13);
            this.BranchNameSourceLabel.TabIndex = 1;
            this.BranchNameSourceLabel.Text = "Branch:";
            // 
            // ControlPanel
            // 
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.CloneBuildDefinitionProgressBar);
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Controls.Add(this.ApplyButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 666);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(566, 40);
            this.ControlPanel.TabIndex = 5;
            // 
            // CloneBuildDefinitionProgressBar
            // 
            this.CloneBuildDefinitionProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloneBuildDefinitionProgressBar.Location = new System.Drawing.Point(379, 18);
            this.CloneBuildDefinitionProgressBar.Name = "CloneBuildDefinitionProgressBar";
            this.CloneBuildDefinitionProgressBar.Size = new System.Drawing.Size(100, 10);
            this.CloneBuildDefinitionProgressBar.TabIndex = 1;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(485, 11);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 2;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // VerticalSplitter
            // 
            this.VerticalSplitter.BackColor = System.Drawing.SystemColors.ControlDark;
            this.VerticalSplitter.Location = new System.Drawing.Point(644, 0);
            this.VerticalSplitter.Name = "VerticalSplitter";
            this.VerticalSplitter.Size = new System.Drawing.Size(6, 706);
            this.VerticalSplitter.TabIndex = 1;
            this.VerticalSplitter.TabStop = false;
            // 
            // BuildDefinitionSearchPanel
            // 
            this.BuildDefinitionSearchPanel.AutoScroll = true;
            this.BuildDefinitionSearchPanel.Controls.Add(this.BuildDefinitionSearchPlaceholderLabel);
            this.BuildDefinitionSearchPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.BuildDefinitionSearchPanel.Location = new System.Drawing.Point(0, 0);
            this.BuildDefinitionSearchPanel.Name = "BuildDefinitionSearchPanel";
            this.BuildDefinitionSearchPanel.Size = new System.Drawing.Size(644, 706);
            this.BuildDefinitionSearchPanel.TabIndex = 0;
            // 
            // BuildDefinitionSearchPlaceholderLabel
            // 
            this.BuildDefinitionSearchPlaceholderLabel.AutoSize = true;
            this.BuildDefinitionSearchPlaceholderLabel.Location = new System.Drawing.Point(12, 9);
            this.BuildDefinitionSearchPlaceholderLabel.Name = "BuildDefinitionSearchPlaceholderLabel";
            this.BuildDefinitionSearchPlaceholderLabel.Size = new System.Drawing.Size(173, 13);
            this.BuildDefinitionSearchPlaceholderLabel.TabIndex = 0;
            this.BuildDefinitionSearchPlaceholderLabel.Text = "Build Definition Search Placeholder";
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.VsoBuild.Sidekicks.Help.chm";
            // 
            // MigrateToGitCheckBox
            // 
            this.MigrateToGitCheckBox.AutoSize = true;
            this.MigrateToGitCheckBox.Location = new System.Drawing.Point(7, 285);
            this.MigrateToGitCheckBox.Name = "MigrateToGitCheckBox";
            this.MigrateToGitCheckBox.Size = new System.Drawing.Size(148, 17);
            this.MigrateToGitCheckBox.TabIndex = 2;
            this.MigrateToGitCheckBox.Text = "Migrate to Git (from TFVC)";
            this.MigrateToGitCheckBox.UseVisualStyleBackColor = true;
            // 
            // BuildDefinitionCloneForm
            // 
            this.AcceptButton = this.ApplyButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 706);
            this.ControlBox = false;
            this.Controls.Add(this.PropertiesPanel);
            this.Controls.Add(this.VerticalSplitter);
            this.Controls.Add(this.BuildDefinitionSearchPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "BuildDefinitionCloneForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.PropertiesPanel.ResumeLayout(false);
            this.ReleaseDefinitionOptionsGroupBox.ResumeLayout(false);
            this.ReleaseDefinitionOptionsGroupBox.PerformLayout();
            this.DirectionToTargetPanel.ResumeLayout(false);
            this.DirectionToTargetPanel.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.DirectionFromSourcePanel.ResumeLayout(false);
            this.DirectionFromSourcePanel.PerformLayout();
            this.TargetPropertiesPanel.ResumeLayout(false);
            this.TargetPropertiesPanel.PerformLayout();
            this.TargetHeaderPanel.ResumeLayout(false);
            this.TargetHeaderPanel.PerformLayout();
            this.SourcePropertiesPanel.ResumeLayout(false);
            this.SourcePropertiesPanel.PerformLayout();
            this.SourceHeaderPanel.ResumeLayout(false);
            this.SourceHeaderPanel.PerformLayout();
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.BuildDefinitionSearchPanel.ResumeLayout(false);
            this.BuildDefinitionSearchPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Splitter VerticalSplitter;
        private System.Windows.Forms.Panel BuildDefinitionSearchPanel;
        private System.Windows.Forms.Label BuildDefinitionSearchPlaceholderLabel;
        private System.Windows.Forms.Panel TargetPropertiesPanel;
        private System.Windows.Forms.TextBox BuildDefinitionNamePrefixTargetTextBox;
        private System.Windows.Forms.Label BuildDefinitionNamePrefixTargetLabel;
        private System.Windows.Forms.TextBox BranchNameTargetTextBox;
        private System.Windows.Forms.Label BranchNameTargetLabel;
        private System.Windows.Forms.Panel SourcePropertiesPanel;
        private System.Windows.Forms.TextBox BuildDefinitionNamePrefixSourceTextBox;
        private System.Windows.Forms.Label BuildDefinitionNamePrefixSourceLabel;
        private System.Windows.Forms.TextBox BranchNameSourceTextBox;
        private System.Windows.Forms.Label BranchNameSourceLabel;
        private System.Windows.Forms.Panel DirectionFromSourcePanel;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.ToolTip CloneBuildDefinitionToolTip;
        private System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.Panel TargetHeaderPanel;
        private System.Windows.Forms.Label TargetLabel;
        private System.Windows.Forms.Panel SourceHeaderPanel;
        private System.Windows.Forms.Label SourceLabel;
        private System.Windows.Forms.Label DirectionLabel;
        private System.Windows.Forms.Panel DirectionToTargetPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label RepositoryTypeLabel;
        private System.Windows.Forms.ComboBox RepositoryTypeComboBox;
        private System.Windows.Forms.GroupBox ReleaseDefinitionOptionsGroupBox;
        private System.Windows.Forms.CheckBox MigrateBuildStepsCheckBox;
        private System.Windows.Forms.CheckBox CreateMissingReleaseDefinitionsCheckBox;
        private System.Windows.Forms.CheckBox CloneReleaseDefinitionsCheckBox;
        private System.Windows.Forms.ComboBox BuildDefinitionFolderComboBox;
        private System.Windows.Forms.Label BuildDefinitionFolderLabel;
        private System.Windows.Forms.ProgressBar CloneBuildDefinitionProgressBar;
        private System.Windows.Forms.Label SourceReleaseDefinitionDoesNotExistLabel;
        private System.Windows.Forms.CheckBox DoNotCloneBuildDefinitionsCheckBox;
        private System.Windows.Forms.Button HelpButton;
        private System.Windows.Forms.Label RepositoryLabel;
        private System.Windows.Forms.ComboBox RepositoryComboBox;
        private System.Windows.Forms.CheckBox MigrateToGitCheckBox;
    }
}