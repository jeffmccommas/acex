﻿namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    partial class DefinitionExportImportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DefinitionExportImportToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.OperationPanel = new System.Windows.Forms.Panel();
            this.ImportOperationPanel = new System.Windows.Forms.Panel();
            this.ImportMigrateToGitCheckBox = new System.Windows.Forms.CheckBox();
            this.ImportCopySplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.ImportZipArchiveContentsCopyContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ImportZipArchiveContentsCopyBuildDefinitionNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportZipArchiveContentsCopyBuildDefinitionJsonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportRepositoryComboBox = new System.Windows.Forms.ComboBox();
            this.ImportRepositoryLabel = new System.Windows.Forms.Label();
            this.ImportRepositoryTypeLabel = new System.Windows.Forms.Label();
            this.ImportRepositoryTypeComboBox = new System.Windows.Forms.ComboBox();
            this.ImportBuildDefinitionFolderLabel = new System.Windows.Forms.Label();
            this.ImportBuildDefinitionFolderComboBox = new System.Windows.Forms.ComboBox();
            this.ImportDisplaySplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.ImportZipArchiveContentsDisplayContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ImportZipArchiveContentsDisplayAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportZipArchiveContentsDisplayCheckedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportCheckSplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.ImportZipArchiveContentsCheckContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ImportZipArchiveContentsSelectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportZipArchiveContentsSelectNoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportZipArchiveContentsSelectSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportDefinitionNamePrefixTextBox = new System.Windows.Forms.TextBox();
            this.ImportDefinitionNamePrefixLabel = new System.Windows.Forms.Label();
            this.ImportDefinitionZipArchiveContentsDataGridView = new System.Windows.Forms.DataGridView();
            this.CheckColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.NameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExtraColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImportDefinitionZipArchiveContentsDataGridViewContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckNoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ImportDefinitionZipArchiveContentsDataGridViewDisplayAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportDefinitionZipArchiveContentsDataGridViewDisplayCheckedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportButton = new System.Windows.Forms.Button();
            this.OpenImportZipFileButton = new System.Windows.Forms.Button();
            this.ImportZipFileLocationSelectionButton = new System.Windows.Forms.Button();
            this.ImportZipFileLocationTextBox = new System.Windows.Forms.TextBox();
            this.ImportInstructionsLabel = new System.Windows.Forms.Label();
            this.ImportZipFileLabel = new System.Windows.Forms.Label();
            this.ExportOperationPanel = new System.Windows.Forms.Panel();
            this.VerticalSplitter = new System.Windows.Forms.Splitter();
            this.ExportPropertiesPanel = new System.Windows.Forms.Panel();
            this.DefinitionZipArchiveContentsLabel = new System.Windows.Forms.Label();
            this.DefinitionZipArchiveEntryCountLLabel = new System.Windows.Forms.Label();
            this.ResetAfterSaveCheckBox = new System.Windows.Forms.CheckBox();
            this.ExportDefinitionZipArchiveListBox = new System.Windows.Forms.ListBox();
            this.ResetButton = new System.Windows.Forms.Button();
            this.ExportButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.ExportDefinitionSelectionPanel = new System.Windows.Forms.Panel();
            this.BuildDefinitionSelectionPanel = new System.Windows.Forms.Panel();
            this.BuildDefinitionSearchPanel = new System.Windows.Forms.Panel();
            this.BuildDefinitionSearchPlaceholderLabel = new System.Windows.Forms.Label();
            this.ReleaseDefinitionSelectionPanel = new System.Windows.Forms.Panel();
            this.ReleaseDefinitionSearchPanel = new System.Windows.Forms.Panel();
            this.ReleaseDefinitionSearchPlaceholderLabel = new System.Windows.Forms.Label();
            this.OperationLabel = new System.Windows.Forms.Label();
            this.OperationTypeComboBox = new System.Windows.Forms.ComboBox();
            this.DefinitionTypeComboBox = new System.Windows.Forms.ComboBox();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.HelpButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.DefinitionTypeLabel = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.ExportImportStatusProgressBar = new System.Windows.Forms.ProgressBar();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ExportZipArchiveStatusLabel = new System.Windows.Forms.Label();
            this.ExportZipArchiveStatusValueLabel = new System.Windows.Forms.Label();
            this.OperationPanel.SuspendLayout();
            this.ImportOperationPanel.SuspendLayout();
            this.ImportZipArchiveContentsCopyContextMenuStrip.SuspendLayout();
            this.ImportZipArchiveContentsDisplayContextMenuStrip.SuspendLayout();
            this.ImportZipArchiveContentsCheckContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImportDefinitionZipArchiveContentsDataGridView)).BeginInit();
            this.ImportDefinitionZipArchiveContentsDataGridViewContextMenuStrip.SuspendLayout();
            this.ExportOperationPanel.SuspendLayout();
            this.ExportPropertiesPanel.SuspendLayout();
            this.ExportDefinitionSelectionPanel.SuspendLayout();
            this.BuildDefinitionSelectionPanel.SuspendLayout();
            this.BuildDefinitionSearchPanel.SuspendLayout();
            this.ReleaseDefinitionSelectionPanel.SuspendLayout();
            this.ReleaseDefinitionSearchPanel.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.VsoBuild.Sidekicks.Help.chm";
            // 
            // OperationPanel
            // 
            this.OperationPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OperationPanel.Controls.Add(this.ExportOperationPanel);
            this.OperationPanel.Controls.Add(this.ImportOperationPanel);
            this.OperationPanel.Controls.Add(this.OperationLabel);
            this.OperationPanel.Controls.Add(this.OperationTypeComboBox);
            this.OperationPanel.Controls.Add(this.DefinitionTypeComboBox);
            this.OperationPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.OperationPanel.Controls.Add(this.ControlPanel);
            this.OperationPanel.Location = new System.Drawing.Point(0, 2);
            this.OperationPanel.Name = "OperationPanel";
            this.OperationPanel.Size = new System.Drawing.Size(1216, 639);
            this.OperationPanel.TabIndex = 0;
            // 
            // ImportOperationPanel
            // 
            this.ImportOperationPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ImportOperationPanel.Controls.Add(this.ImportMigrateToGitCheckBox);
            this.ImportOperationPanel.Controls.Add(this.ImportCopySplitButton);
            this.ImportOperationPanel.Controls.Add(this.ImportRepositoryComboBox);
            this.ImportOperationPanel.Controls.Add(this.ImportRepositoryLabel);
            this.ImportOperationPanel.Controls.Add(this.ImportRepositoryTypeLabel);
            this.ImportOperationPanel.Controls.Add(this.ImportRepositoryTypeComboBox);
            this.ImportOperationPanel.Controls.Add(this.ImportBuildDefinitionFolderLabel);
            this.ImportOperationPanel.Controls.Add(this.ImportBuildDefinitionFolderComboBox);
            this.ImportOperationPanel.Controls.Add(this.ImportDisplaySplitButton);
            this.ImportOperationPanel.Controls.Add(this.ImportCheckSplitButton);
            this.ImportOperationPanel.Controls.Add(this.ImportDefinitionNamePrefixTextBox);
            this.ImportOperationPanel.Controls.Add(this.ImportDefinitionNamePrefixLabel);
            this.ImportOperationPanel.Controls.Add(this.ImportDefinitionZipArchiveContentsDataGridView);
            this.ImportOperationPanel.Controls.Add(this.ImportButton);
            this.ImportOperationPanel.Controls.Add(this.OpenImportZipFileButton);
            this.ImportOperationPanel.Controls.Add(this.ImportZipFileLocationSelectionButton);
            this.ImportOperationPanel.Controls.Add(this.ImportZipFileLocationTextBox);
            this.ImportOperationPanel.Controls.Add(this.ImportInstructionsLabel);
            this.ImportOperationPanel.Controls.Add(this.ImportZipFileLabel);
            this.ImportOperationPanel.Location = new System.Drawing.Point(0, 29);
            this.ImportOperationPanel.Name = "ImportOperationPanel";
            this.ImportOperationPanel.Size = new System.Drawing.Size(1212, 569);
            this.ImportOperationPanel.TabIndex = 4;
            // 
            // ImportMigrateToGitCheckBox
            // 
            this.ImportMigrateToGitCheckBox.AutoSize = true;
            this.ImportMigrateToGitCheckBox.Location = new System.Drawing.Point(915, 203);
            this.ImportMigrateToGitCheckBox.Name = "ImportMigrateToGitCheckBox";
            this.ImportMigrateToGitCheckBox.Size = new System.Drawing.Size(148, 17);
            this.ImportMigrateToGitCheckBox.TabIndex = 10;
            this.ImportMigrateToGitCheckBox.Text = "Migrate to Git (from TFVC)";
            this.ImportMigrateToGitCheckBox.UseVisualStyleBackColor = true;
            // 
            // ImportCopySplitButton
            // 
            this.ImportCopySplitButton.AutoSize = true;
            this.ImportCopySplitButton.ContextMenuStrip = this.ImportZipArchiveContentsCopyContextMenuStrip;
            this.ImportCopySplitButton.Location = new System.Drawing.Point(197, 65);
            this.ImportCopySplitButton.Name = "ImportCopySplitButton";
            this.ImportCopySplitButton.Size = new System.Drawing.Size(56, 23);
            this.ImportCopySplitButton.TabIndex = 7;
            this.ImportCopySplitButton.Text = "Copy";
            this.ImportCopySplitButton.UseVisualStyleBackColor = true;
            // 
            // ImportZipArchiveContentsCopyContextMenuStrip
            // 
            this.ImportZipArchiveContentsCopyContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ImportZipArchiveContentsCopyBuildDefinitionNameToolStripMenuItem,
            this.ImportZipArchiveContentsCopyBuildDefinitionJsonToolStripMenuItem});
            this.ImportZipArchiveContentsCopyContextMenuStrip.Name = "ImportZipArchiveContentsCopyContextMenuStrip";
            this.ImportZipArchiveContentsCopyContextMenuStrip.Size = new System.Drawing.Size(223, 48);
            // 
            // ImportZipArchiveContentsCopyBuildDefinitionNameToolStripMenuItem
            // 
            this.ImportZipArchiveContentsCopyBuildDefinitionNameToolStripMenuItem.Name = "ImportZipArchiveContentsCopyBuildDefinitionNameToolStripMenuItem";
            this.ImportZipArchiveContentsCopyBuildDefinitionNameToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.ImportZipArchiveContentsCopyBuildDefinitionNameToolStripMenuItem.Text = "Copy Build Definition Name";
            this.ImportZipArchiveContentsCopyBuildDefinitionNameToolStripMenuItem.Click += new System.EventHandler(this.ImportZipArchiveContentsCopyBuildDefinitionNameToolStripMenuItem_Click);
            // 
            // ImportZipArchiveContentsCopyBuildDefinitionJsonToolStripMenuItem
            // 
            this.ImportZipArchiveContentsCopyBuildDefinitionJsonToolStripMenuItem.Name = "ImportZipArchiveContentsCopyBuildDefinitionJsonToolStripMenuItem";
            this.ImportZipArchiveContentsCopyBuildDefinitionJsonToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.ImportZipArchiveContentsCopyBuildDefinitionJsonToolStripMenuItem.Text = "Copy Build Definition JSON";
            this.ImportZipArchiveContentsCopyBuildDefinitionJsonToolStripMenuItem.Click += new System.EventHandler(this.ImportZipArchiveContentsCopyBuildDefinitionJsonToolStripMenuItem_Click);
            // 
            // ImportRepositoryComboBox
            // 
            this.ImportRepositoryComboBox.FormattingEnabled = true;
            this.ImportRepositoryComboBox.Location = new System.Drawing.Point(915, 176);
            this.ImportRepositoryComboBox.Name = "ImportRepositoryComboBox";
            this.ImportRepositoryComboBox.Size = new System.Drawing.Size(288, 21);
            this.ImportRepositoryComboBox.TabIndex = 7;
            // 
            // ImportRepositoryLabel
            // 
            this.ImportRepositoryLabel.AutoSize = true;
            this.ImportRepositoryLabel.Location = new System.Drawing.Point(802, 176);
            this.ImportRepositoryLabel.Name = "ImportRepositoryLabel";
            this.ImportRepositoryLabel.Size = new System.Drawing.Size(60, 13);
            this.ImportRepositoryLabel.TabIndex = 6;
            this.ImportRepositoryLabel.Text = "Repository:";
            // 
            // ImportRepositoryTypeLabel
            // 
            this.ImportRepositoryTypeLabel.AutoSize = true;
            this.ImportRepositoryTypeLabel.Location = new System.Drawing.Point(802, 150);
            this.ImportRepositoryTypeLabel.Name = "ImportRepositoryTypeLabel";
            this.ImportRepositoryTypeLabel.Size = new System.Drawing.Size(83, 13);
            this.ImportRepositoryTypeLabel.TabIndex = 4;
            this.ImportRepositoryTypeLabel.Text = "Repository type:";
            // 
            // ImportRepositoryTypeComboBox
            // 
            this.ImportRepositoryTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ImportRepositoryTypeComboBox.FormattingEnabled = true;
            this.ImportRepositoryTypeComboBox.Location = new System.Drawing.Point(915, 146);
            this.ImportRepositoryTypeComboBox.Name = "ImportRepositoryTypeComboBox";
            this.ImportRepositoryTypeComboBox.Size = new System.Drawing.Size(288, 21);
            this.ImportRepositoryTypeComboBox.TabIndex = 5;
            this.ImportRepositoryTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.ImportRepositoryTypeComboBox_SelectedIndexChanged);
            // 
            // ImportBuildDefinitionFolderLabel
            // 
            this.ImportBuildDefinitionFolderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ImportBuildDefinitionFolderLabel.AutoSize = true;
            this.ImportBuildDefinitionFolderLabel.Location = new System.Drawing.Point(802, 122);
            this.ImportBuildDefinitionFolderLabel.Name = "ImportBuildDefinitionFolderLabel";
            this.ImportBuildDefinitionFolderLabel.Size = new System.Drawing.Size(107, 13);
            this.ImportBuildDefinitionFolderLabel.TabIndex = 2;
            this.ImportBuildDefinitionFolderLabel.Text = "Build definition folder:";
            // 
            // ImportBuildDefinitionFolderComboBox
            // 
            this.ImportBuildDefinitionFolderComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ImportBuildDefinitionFolderComboBox.FormattingEnabled = true;
            this.ImportBuildDefinitionFolderComboBox.Location = new System.Drawing.Point(915, 118);
            this.ImportBuildDefinitionFolderComboBox.Name = "ImportBuildDefinitionFolderComboBox";
            this.ImportBuildDefinitionFolderComboBox.Size = new System.Drawing.Size(288, 21);
            this.ImportBuildDefinitionFolderComboBox.TabIndex = 3;
            // 
            // ImportDisplaySplitButton
            // 
            this.ImportDisplaySplitButton.AutoSize = true;
            this.ImportDisplaySplitButton.ContextMenuStrip = this.ImportZipArchiveContentsDisplayContextMenuStrip;
            this.ImportDisplaySplitButton.Location = new System.Drawing.Point(135, 65);
            this.ImportDisplaySplitButton.Name = "ImportDisplaySplitButton";
            this.ImportDisplaySplitButton.Size = new System.Drawing.Size(56, 23);
            this.ImportDisplaySplitButton.TabIndex = 6;
            this.ImportDisplaySplitButton.Text = "Display";
            this.ImportDisplaySplitButton.UseVisualStyleBackColor = true;
            // 
            // ImportZipArchiveContentsDisplayContextMenuStrip
            // 
            this.ImportZipArchiveContentsDisplayContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ImportZipArchiveContentsDisplayAllToolStripMenuItem,
            this.ImportZipArchiveContentsDisplayCheckedToolStripMenuItem});
            this.ImportZipArchiveContentsDisplayContextMenuStrip.Name = "ImportZipArchiveContentsDisplayContextMenuStrip";
            this.ImportZipArchiveContentsDisplayContextMenuStrip.Size = new System.Drawing.Size(121, 48);
            // 
            // ImportZipArchiveContentsDisplayAllToolStripMenuItem
            // 
            this.ImportZipArchiveContentsDisplayAllToolStripMenuItem.Name = "ImportZipArchiveContentsDisplayAllToolStripMenuItem";
            this.ImportZipArchiveContentsDisplayAllToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.ImportZipArchiveContentsDisplayAllToolStripMenuItem.Text = "All";
            this.ImportZipArchiveContentsDisplayAllToolStripMenuItem.Click += new System.EventHandler(this.ImportZipArchiveContentsDisplayAllToolStripMenuItem_Click);
            // 
            // ImportZipArchiveContentsDisplayCheckedToolStripMenuItem
            // 
            this.ImportZipArchiveContentsDisplayCheckedToolStripMenuItem.Name = "ImportZipArchiveContentsDisplayCheckedToolStripMenuItem";
            this.ImportZipArchiveContentsDisplayCheckedToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.ImportZipArchiveContentsDisplayCheckedToolStripMenuItem.Text = "Checked";
            this.ImportZipArchiveContentsDisplayCheckedToolStripMenuItem.Click += new System.EventHandler(this.ImportZipArchiveContentsDisplayCheckedToolStripMenuItem_Click);
            // 
            // ImportCheckSplitButton
            // 
            this.ImportCheckSplitButton.AutoSize = true;
            this.ImportCheckSplitButton.ContextMenuStrip = this.ImportZipArchiveContentsCheckContextMenuStrip;
            this.ImportCheckSplitButton.Location = new System.Drawing.Point(73, 65);
            this.ImportCheckSplitButton.Name = "ImportCheckSplitButton";
            this.ImportCheckSplitButton.Size = new System.Drawing.Size(56, 23);
            this.ImportCheckSplitButton.TabIndex = 5;
            this.ImportCheckSplitButton.Text = "Check";
            this.ImportCheckSplitButton.UseVisualStyleBackColor = true;
            // 
            // ImportZipArchiveContentsCheckContextMenuStrip
            // 
            this.ImportZipArchiveContentsCheckContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ImportZipArchiveContentsSelectAllToolStripMenuItem,
            this.ImportZipArchiveContentsSelectNoneToolStripMenuItem,
            this.ImportZipArchiveContentsSelectSelectedToolStripMenuItem});
            this.ImportZipArchiveContentsCheckContextMenuStrip.Name = "ZipArchiveContentsCheckContextMenuStrip";
            this.ImportZipArchiveContentsCheckContextMenuStrip.Size = new System.Drawing.Size(119, 70);
            // 
            // ImportZipArchiveContentsSelectAllToolStripMenuItem
            // 
            this.ImportZipArchiveContentsSelectAllToolStripMenuItem.Name = "ImportZipArchiveContentsSelectAllToolStripMenuItem";
            this.ImportZipArchiveContentsSelectAllToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.ImportZipArchiveContentsSelectAllToolStripMenuItem.Text = "All";
            this.ImportZipArchiveContentsSelectAllToolStripMenuItem.Click += new System.EventHandler(this.ImportZipArchiveContentsSelectAllToolStripMenuItem_Click);
            // 
            // ImportZipArchiveContentsSelectNoneToolStripMenuItem
            // 
            this.ImportZipArchiveContentsSelectNoneToolStripMenuItem.Name = "ImportZipArchiveContentsSelectNoneToolStripMenuItem";
            this.ImportZipArchiveContentsSelectNoneToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.ImportZipArchiveContentsSelectNoneToolStripMenuItem.Text = "None";
            this.ImportZipArchiveContentsSelectNoneToolStripMenuItem.Click += new System.EventHandler(this.ImportZipArchiveContentsSelectNoneToolStripMenuItem_Click);
            // 
            // ImportZipArchiveContentsSelectSelectedToolStripMenuItem
            // 
            this.ImportZipArchiveContentsSelectSelectedToolStripMenuItem.Name = "ImportZipArchiveContentsSelectSelectedToolStripMenuItem";
            this.ImportZipArchiveContentsSelectSelectedToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.ImportZipArchiveContentsSelectSelectedToolStripMenuItem.Text = "Selected";
            this.ImportZipArchiveContentsSelectSelectedToolStripMenuItem.Click += new System.EventHandler(this.ImportZipArchiveContentsSelectSelectedToolStripMenuItem_Click);
            // 
            // ImportDefinitionNamePrefixTextBox
            // 
            this.ImportDefinitionNamePrefixTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ImportDefinitionNamePrefixTextBox.Location = new System.Drawing.Point(915, 90);
            this.ImportDefinitionNamePrefixTextBox.Name = "ImportDefinitionNamePrefixTextBox";
            this.ImportDefinitionNamePrefixTextBox.Size = new System.Drawing.Size(15, 20);
            this.ImportDefinitionNamePrefixTextBox.TabIndex = 1;
            this.ImportDefinitionNamePrefixTextBox.Text = "i";
            // 
            // ImportDefinitionNamePrefixLabel
            // 
            this.ImportDefinitionNamePrefixLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ImportDefinitionNamePrefixLabel.AutoSize = true;
            this.ImportDefinitionNamePrefixLabel.Location = new System.Drawing.Point(802, 93);
            this.ImportDefinitionNamePrefixLabel.Name = "ImportDefinitionNamePrefixLabel";
            this.ImportDefinitionNamePrefixLabel.Size = new System.Drawing.Size(66, 13);
            this.ImportDefinitionNamePrefixLabel.TabIndex = 0;
            this.ImportDefinitionNamePrefixLabel.Text = "Name prefix:";
            // 
            // ImportDefinitionZipArchiveContentsDataGridView
            // 
            this.ImportDefinitionZipArchiveContentsDataGridView.AllowUserToAddRows = false;
            this.ImportDefinitionZipArchiveContentsDataGridView.AllowUserToDeleteRows = false;
            this.ImportDefinitionZipArchiveContentsDataGridView.AllowUserToResizeRows = false;
            this.ImportDefinitionZipArchiveContentsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ImportDefinitionZipArchiveContentsDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.ImportDefinitionZipArchiveContentsDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ImportDefinitionZipArchiveContentsDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ImportDefinitionZipArchiveContentsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ImportDefinitionZipArchiveContentsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CheckColumn,
            this.NameColumn,
            this.ExtraColumn});
            this.ImportDefinitionZipArchiveContentsDataGridView.ContextMenuStrip = this.ImportDefinitionZipArchiveContentsDataGridViewContextMenuStrip;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ImportDefinitionZipArchiveContentsDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.ImportDefinitionZipArchiveContentsDataGridView.EnableHeadersVisualStyles = false;
            this.ImportDefinitionZipArchiveContentsDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.ImportDefinitionZipArchiveContentsDataGridView.Location = new System.Drawing.Point(10, 93);
            this.ImportDefinitionZipArchiveContentsDataGridView.Name = "ImportDefinitionZipArchiveContentsDataGridView";
            this.ImportDefinitionZipArchiveContentsDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ImportDefinitionZipArchiveContentsDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.ImportDefinitionZipArchiveContentsDataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            this.ImportDefinitionZipArchiveContentsDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.ImportDefinitionZipArchiveContentsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ImportDefinitionZipArchiveContentsDataGridView.ShowEditingIcon = false;
            this.ImportDefinitionZipArchiveContentsDataGridView.Size = new System.Drawing.Size(776, 466);
            this.ImportDefinitionZipArchiveContentsDataGridView.TabIndex = 8;
            this.ImportDefinitionZipArchiveContentsDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ImportDefinitionZipArchiveContentsDataGridView_CellClick);
            this.ImportDefinitionZipArchiveContentsDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ImportDefinitionZipArchiveContentsDataGridView_CellContentClick);
            this.ImportDefinitionZipArchiveContentsDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ImportDefinitionZipArchiveContentsDataGridView_CellDoubleClick);
            this.ImportDefinitionZipArchiveContentsDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.ImportDefinitionZipArchiveContentsDataGridView_CellValueChanged);
            this.ImportDefinitionZipArchiveContentsDataGridView.Sorted += new System.EventHandler(this.ImportDefinitionZipArchiveContentsDataGridView_Sorted);
            // 
            // CheckColumn
            // 
            this.CheckColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.CheckColumn.HeaderText = "";
            this.CheckColumn.MinimumWidth = 15;
            this.CheckColumn.Name = "CheckColumn";
            this.CheckColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CheckColumn.Width = 15;
            // 
            // NameColumn
            // 
            this.NameColumn.HeaderText = "Name";
            this.NameColumn.MinimumWidth = 100;
            this.NameColumn.Name = "NameColumn";
            this.NameColumn.ReadOnly = true;
            this.NameColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ExtraColumn
            // 
            this.ExtraColumn.HeaderText = "";
            this.ExtraColumn.Name = "ExtraColumn";
            this.ExtraColumn.ReadOnly = true;
            // 
            // ImportDefinitionZipArchiveContentsDataGridViewContextMenuStrip
            // 
            this.ImportDefinitionZipArchiveContentsDataGridViewContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckAllToolStripMenuItem,
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckSelectedToolStripMenuItem,
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckNoneToolStripMenuItem,
            this.toolStripSeparator1,
            this.ImportDefinitionZipArchiveContentsDataGridViewDisplayAllToolStripMenuItem,
            this.ImportDefinitionZipArchiveContentsDataGridViewDisplayCheckedToolStripMenuItem});
            this.ImportDefinitionZipArchiveContentsDataGridViewContextMenuStrip.Name = "ImportDefinitionZipArchiveContentsDataGridViewContextMenuStrip";
            this.ImportDefinitionZipArchiveContentsDataGridViewContextMenuStrip.Size = new System.Drawing.Size(162, 120);
            // 
            // ImportDefinitionZipArchiveContentsDataGridViewCheckAllToolStripMenuItem
            // 
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckAllToolStripMenuItem.Name = "ImportDefinitionZipArchiveContentsDataGridViewCheckAllToolStripMenuItem";
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckAllToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckAllToolStripMenuItem.Text = "Check All";
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckAllToolStripMenuItem.Click += new System.EventHandler(this.ImportDefinitionZipArchiveContentsDataGridViewCheckAllToolStripMenuItem_Click);
            // 
            // ImportDefinitionZipArchiveContentsDataGridViewCheckSelectedToolStripMenuItem
            // 
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckSelectedToolStripMenuItem.Name = "ImportDefinitionZipArchiveContentsDataGridViewCheckSelectedToolStripMenuItem";
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckSelectedToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckSelectedToolStripMenuItem.Text = "Check Selected";
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckSelectedToolStripMenuItem.Click += new System.EventHandler(this.ImportDefinitionZipArchiveContentsDataGridViewCheckSelectedToolStripMenuItem_Click);
            // 
            // ImportDefinitionZipArchiveContentsDataGridViewCheckNoneToolStripMenuItem
            // 
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckNoneToolStripMenuItem.Name = "ImportDefinitionZipArchiveContentsDataGridViewCheckNoneToolStripMenuItem";
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckNoneToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckNoneToolStripMenuItem.Text = "Check None";
            this.ImportDefinitionZipArchiveContentsDataGridViewCheckNoneToolStripMenuItem.Click += new System.EventHandler(this.ImportDefinitionZipArchiveContentsDataGridViewCheckNoneToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(158, 6);
            // 
            // ImportDefinitionZipArchiveContentsDataGridViewDisplayAllToolStripMenuItem
            // 
            this.ImportDefinitionZipArchiveContentsDataGridViewDisplayAllToolStripMenuItem.Name = "ImportDefinitionZipArchiveContentsDataGridViewDisplayAllToolStripMenuItem";
            this.ImportDefinitionZipArchiveContentsDataGridViewDisplayAllToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.ImportDefinitionZipArchiveContentsDataGridViewDisplayAllToolStripMenuItem.Text = "Display All";
            this.ImportDefinitionZipArchiveContentsDataGridViewDisplayAllToolStripMenuItem.Click += new System.EventHandler(this.ImportDefinitionZipArchiveContentsDataGridViewDisplayAllToolStripMenuItem_Click);
            // 
            // ImportDefinitionZipArchiveContentsDataGridViewDisplayCheckedToolStripMenuItem
            // 
            this.ImportDefinitionZipArchiveContentsDataGridViewDisplayCheckedToolStripMenuItem.Name = "ImportDefinitionZipArchiveContentsDataGridViewDisplayCheckedToolStripMenuItem";
            this.ImportDefinitionZipArchiveContentsDataGridViewDisplayCheckedToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.ImportDefinitionZipArchiveContentsDataGridViewDisplayCheckedToolStripMenuItem.Text = "Display Checked";
            this.ImportDefinitionZipArchiveContentsDataGridViewDisplayCheckedToolStripMenuItem.Click += new System.EventHandler(this.ImportDefinitionZipArchiveContentsDataGridViewDisplayCheckedToolStripMenuItem_Click);
            // 
            // ImportButton
            // 
            this.ImportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ImportButton.Location = new System.Drawing.Point(1147, 243);
            this.ImportButton.Name = "ImportButton";
            this.ImportButton.Size = new System.Drawing.Size(56, 23);
            this.ImportButton.TabIndex = 8;
            this.ImportButton.Text = "Import";
            this.ImportButton.UseVisualStyleBackColor = true;
            this.ImportButton.Click += new System.EventHandler(this.ImportButton_Click);
            // 
            // OpenImportZipFileButton
            // 
            this.OpenImportZipFileButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.OpenImportZipFileButton.Location = new System.Drawing.Point(11, 65);
            this.OpenImportZipFileButton.Name = "OpenImportZipFileButton";
            this.OpenImportZipFileButton.Size = new System.Drawing.Size(56, 23);
            this.OpenImportZipFileButton.TabIndex = 4;
            this.OpenImportZipFileButton.Text = "Open";
            this.OpenImportZipFileButton.UseVisualStyleBackColor = true;
            this.OpenImportZipFileButton.Click += new System.EventHandler(this.OpenImportZipFileButton_Click);
            // 
            // ImportZipFileLocationSelectionButton
            // 
            this.ImportZipFileLocationSelectionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ImportZipFileLocationSelectionButton.Location = new System.Drawing.Point(1178, 38);
            this.ImportZipFileLocationSelectionButton.Name = "ImportZipFileLocationSelectionButton";
            this.ImportZipFileLocationSelectionButton.Size = new System.Drawing.Size(26, 23);
            this.ImportZipFileLocationSelectionButton.TabIndex = 3;
            this.ImportZipFileLocationSelectionButton.Text = "...";
            this.ImportZipFileLocationSelectionButton.UseVisualStyleBackColor = true;
            this.ImportZipFileLocationSelectionButton.Click += new System.EventHandler(this.ImportZipFileLocationSelectionButton_Click);
            // 
            // ImportZipFileLocationTextBox
            // 
            this.ImportZipFileLocationTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ImportZipFileLocationTextBox.Location = new System.Drawing.Point(71, 39);
            this.ImportZipFileLocationTextBox.Name = "ImportZipFileLocationTextBox";
            this.ImportZipFileLocationTextBox.Size = new System.Drawing.Size(1101, 20);
            this.ImportZipFileLocationTextBox.TabIndex = 3;
            this.ImportZipFileLocationTextBox.TextChanged += new System.EventHandler(this.ImportZipFileLocationTextBox_TextChanged);
            // 
            // ImportInstructionsLabel
            // 
            this.ImportInstructionsLabel.AutoSize = true;
            this.ImportInstructionsLabel.Location = new System.Drawing.Point(10, 16);
            this.ImportInstructionsLabel.Name = "ImportInstructionsLabel";
            this.ImportInstructionsLabel.Size = new System.Drawing.Size(209, 13);
            this.ImportInstructionsLabel.TabIndex = 0;
            this.ImportInstructionsLabel.Text = "Select zip archive location and click Open.";
            // 
            // ImportZipFileLabel
            // 
            this.ImportZipFileLabel.AutoSize = true;
            this.ImportZipFileLabel.Location = new System.Drawing.Point(10, 43);
            this.ImportZipFileLabel.Name = "ImportZipFileLabel";
            this.ImportZipFileLabel.Size = new System.Drawing.Size(44, 13);
            this.ImportZipFileLabel.TabIndex = 1;
            this.ImportZipFileLabel.Text = "Zip File:";
            // 
            // ExportOperationPanel
            // 
            this.ExportOperationPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ExportOperationPanel.Controls.Add(this.VerticalSplitter);
            this.ExportOperationPanel.Controls.Add(this.ExportPropertiesPanel);
            this.ExportOperationPanel.Controls.Add(this.ExportDefinitionSelectionPanel);
            this.ExportOperationPanel.Location = new System.Drawing.Point(0, 29);
            this.ExportOperationPanel.Name = "ExportOperationPanel";
            this.ExportOperationPanel.Size = new System.Drawing.Size(1215, 569);
            this.ExportOperationPanel.TabIndex = 6;
            // 
            // VerticalSplitter
            // 
            this.VerticalSplitter.BackColor = System.Drawing.SystemColors.ControlDark;
            this.VerticalSplitter.Location = new System.Drawing.Point(490, 0);
            this.VerticalSplitter.Name = "VerticalSplitter";
            this.VerticalSplitter.Size = new System.Drawing.Size(10, 569);
            this.VerticalSplitter.TabIndex = 1;
            this.VerticalSplitter.TabStop = false;
            // 
            // ExportPropertiesPanel
            // 
            this.ExportPropertiesPanel.Controls.Add(this.DefinitionZipArchiveContentsLabel);
            this.ExportPropertiesPanel.Controls.Add(this.DefinitionZipArchiveEntryCountLLabel);
            this.ExportPropertiesPanel.Controls.Add(this.ResetAfterSaveCheckBox);
            this.ExportPropertiesPanel.Controls.Add(this.ExportDefinitionZipArchiveListBox);
            this.ExportPropertiesPanel.Controls.Add(this.ResetButton);
            this.ExportPropertiesPanel.Controls.Add(this.ExportButton);
            this.ExportPropertiesPanel.Controls.Add(this.SaveButton);
            this.ExportPropertiesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExportPropertiesPanel.Location = new System.Drawing.Point(490, 0);
            this.ExportPropertiesPanel.MinimumSize = new System.Drawing.Size(560, 480);
            this.ExportPropertiesPanel.Name = "ExportPropertiesPanel";
            this.ExportPropertiesPanel.Size = new System.Drawing.Size(725, 569);
            this.ExportPropertiesPanel.TabIndex = 1;
            // 
            // DefinitionZipArchiveContentsLabel
            // 
            this.DefinitionZipArchiveContentsLabel.AutoSize = true;
            this.DefinitionZipArchiveContentsLabel.Location = new System.Drawing.Point(15, 16);
            this.DefinitionZipArchiveContentsLabel.Name = "DefinitionZipArchiveContentsLabel";
            this.DefinitionZipArchiveContentsLabel.Size = new System.Drawing.Size(153, 13);
            this.DefinitionZipArchiveContentsLabel.TabIndex = 0;
            this.DefinitionZipArchiveContentsLabel.Text = "Definition Zip Archive Contents";
            // 
            // DefinitionZipArchiveEntryCountLLabel
            // 
            this.DefinitionZipArchiveEntryCountLLabel.AutoSize = true;
            this.DefinitionZipArchiveEntryCountLLabel.Location = new System.Drawing.Point(165, 16);
            this.DefinitionZipArchiveEntryCountLLabel.Name = "DefinitionZipArchiveEntryCountLLabel";
            this.DefinitionZipArchiveEntryCountLLabel.Size = new System.Drawing.Size(22, 13);
            this.DefinitionZipArchiveEntryCountLLabel.TabIndex = 1;
            this.DefinitionZipArchiveEntryCountLLabel.Text = "(0):";
            // 
            // ResetAfterSaveCheckBox
            // 
            this.ResetAfterSaveCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ResetAfterSaveCheckBox.AutoSize = true;
            this.ResetAfterSaveCheckBox.Location = new System.Drawing.Point(375, 545);
            this.ResetAfterSaveCheckBox.Name = "ResetAfterSaveCheckBox";
            this.ResetAfterSaveCheckBox.Size = new System.Drawing.Size(104, 17);
            this.ResetAfterSaveCheckBox.TabIndex = 3;
            this.ResetAfterSaveCheckBox.Text = "Reset after save";
            this.ResetAfterSaveCheckBox.UseVisualStyleBackColor = true;
            // 
            // ExportDefinitionZipArchiveListBox
            // 
            this.ExportDefinitionZipArchiveListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ExportDefinitionZipArchiveListBox.FormattingEnabled = true;
            this.ExportDefinitionZipArchiveListBox.Location = new System.Drawing.Point(15, 36);
            this.ExportDefinitionZipArchiveListBox.Name = "ExportDefinitionZipArchiveListBox";
            this.ExportDefinitionZipArchiveListBox.ScrollAlwaysVisible = true;
            this.ExportDefinitionZipArchiveListBox.Size = new System.Drawing.Size(703, 498);
            this.ExportDefinitionZipArchiveListBox.TabIndex = 2;
            // 
            // ResetButton
            // 
            this.ResetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ResetButton.Location = new System.Drawing.Point(647, 541);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(75, 23);
            this.ResetButton.TabIndex = 6;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // ExportButton
            // 
            this.ExportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ExportButton.Location = new System.Drawing.Point(485, 541);
            this.ExportButton.Name = "ExportButton";
            this.ExportButton.Size = new System.Drawing.Size(75, 23);
            this.ExportButton.TabIndex = 4;
            this.ExportButton.Text = "Export";
            this.ExportButton.UseVisualStyleBackColor = true;
            this.ExportButton.Click += new System.EventHandler(this.ExportButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveButton.Location = new System.Drawing.Point(566, 541);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 5;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // ExportDefinitionSelectionPanel
            // 
            this.ExportDefinitionSelectionPanel.AutoScroll = true;
            this.ExportDefinitionSelectionPanel.Controls.Add(this.BuildDefinitionSelectionPanel);
            this.ExportDefinitionSelectionPanel.Controls.Add(this.ReleaseDefinitionSelectionPanel);
            this.ExportDefinitionSelectionPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.ExportDefinitionSelectionPanel.Location = new System.Drawing.Point(0, 0);
            this.ExportDefinitionSelectionPanel.Name = "ExportDefinitionSelectionPanel";
            this.ExportDefinitionSelectionPanel.Size = new System.Drawing.Size(490, 569);
            this.ExportDefinitionSelectionPanel.TabIndex = 0;
            // 
            // BuildDefinitionSelectionPanel
            // 
            this.BuildDefinitionSelectionPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildDefinitionSelectionPanel.Controls.Add(this.BuildDefinitionSearchPanel);
            this.BuildDefinitionSelectionPanel.Location = new System.Drawing.Point(0, 35);
            this.BuildDefinitionSelectionPanel.Margin = new System.Windows.Forms.Padding(2);
            this.BuildDefinitionSelectionPanel.Name = "BuildDefinitionSelectionPanel";
            this.BuildDefinitionSelectionPanel.Size = new System.Drawing.Size(490, 532);
            this.BuildDefinitionSelectionPanel.TabIndex = 2;
            // 
            // BuildDefinitionSearchPanel
            // 
            this.BuildDefinitionSearchPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildDefinitionSearchPanel.Controls.Add(this.BuildDefinitionSearchPlaceholderLabel);
            this.BuildDefinitionSearchPanel.Location = new System.Drawing.Point(1, 0);
            this.BuildDefinitionSearchPanel.Margin = new System.Windows.Forms.Padding(2);
            this.BuildDefinitionSearchPanel.Name = "BuildDefinitionSearchPanel";
            this.BuildDefinitionSearchPanel.Size = new System.Drawing.Size(489, 534);
            this.BuildDefinitionSearchPanel.TabIndex = 0;
            // 
            // BuildDefinitionSearchPlaceholderLabel
            // 
            this.BuildDefinitionSearchPlaceholderLabel.AutoSize = true;
            this.BuildDefinitionSearchPlaceholderLabel.Location = new System.Drawing.Point(48, 240);
            this.BuildDefinitionSearchPlaceholderLabel.Name = "BuildDefinitionSearchPlaceholderLabel";
            this.BuildDefinitionSearchPlaceholderLabel.Size = new System.Drawing.Size(173, 13);
            this.BuildDefinitionSearchPlaceholderLabel.TabIndex = 0;
            this.BuildDefinitionSearchPlaceholderLabel.Text = "Build Definition Search Placeholder";
            this.BuildDefinitionSearchPlaceholderLabel.Visible = false;
            // 
            // ReleaseDefinitionSelectionPanel
            // 
            this.ReleaseDefinitionSelectionPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReleaseDefinitionSelectionPanel.Controls.Add(this.ReleaseDefinitionSearchPanel);
            this.ReleaseDefinitionSelectionPanel.Location = new System.Drawing.Point(0, 35);
            this.ReleaseDefinitionSelectionPanel.Margin = new System.Windows.Forms.Padding(2);
            this.ReleaseDefinitionSelectionPanel.Name = "ReleaseDefinitionSelectionPanel";
            this.ReleaseDefinitionSelectionPanel.Size = new System.Drawing.Size(490, 532);
            this.ReleaseDefinitionSelectionPanel.TabIndex = 5;
            // 
            // ReleaseDefinitionSearchPanel
            // 
            this.ReleaseDefinitionSearchPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReleaseDefinitionSearchPanel.Controls.Add(this.ReleaseDefinitionSearchPlaceholderLabel);
            this.ReleaseDefinitionSearchPanel.Location = new System.Drawing.Point(0, 0);
            this.ReleaseDefinitionSearchPanel.Margin = new System.Windows.Forms.Padding(2);
            this.ReleaseDefinitionSearchPanel.Name = "ReleaseDefinitionSearchPanel";
            this.ReleaseDefinitionSearchPanel.Size = new System.Drawing.Size(490, 441);
            this.ReleaseDefinitionSearchPanel.TabIndex = 0;
            // 
            // ReleaseDefinitionSearchPlaceholderLabel
            // 
            this.ReleaseDefinitionSearchPlaceholderLabel.AutoSize = true;
            this.ReleaseDefinitionSearchPlaceholderLabel.Location = new System.Drawing.Point(192, 98);
            this.ReleaseDefinitionSearchPlaceholderLabel.Name = "ReleaseDefinitionSearchPlaceholderLabel";
            this.ReleaseDefinitionSearchPlaceholderLabel.Size = new System.Drawing.Size(189, 13);
            this.ReleaseDefinitionSearchPlaceholderLabel.TabIndex = 0;
            this.ReleaseDefinitionSearchPlaceholderLabel.Text = "Release Definition Search Placeholder";
            this.ReleaseDefinitionSearchPlaceholderLabel.Visible = false;
            // 
            // OperationLabel
            // 
            this.OperationLabel.AutoSize = true;
            this.OperationLabel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.OperationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OperationLabel.Location = new System.Drawing.Point(11, 6);
            this.OperationLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.OperationLabel.Name = "OperationLabel";
            this.OperationLabel.Size = new System.Drawing.Size(66, 13);
            this.OperationLabel.TabIndex = 1;
            this.OperationLabel.Text = "Operation:";
            // 
            // OperationTypeComboBox
            // 
            this.OperationTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OperationTypeComboBox.FormattingEnabled = true;
            this.OperationTypeComboBox.Location = new System.Drawing.Point(81, 4);
            this.OperationTypeComboBox.Margin = new System.Windows.Forms.Padding(2);
            this.OperationTypeComboBox.Name = "OperationTypeComboBox";
            this.OperationTypeComboBox.Size = new System.Drawing.Size(81, 21);
            this.OperationTypeComboBox.TabIndex = 2;
            this.OperationTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.OperationTypeComboBox_SelectedIndexChanged);
            // 
            // DefinitionTypeComboBox
            // 
            this.DefinitionTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DefinitionTypeComboBox.FormattingEnabled = true;
            this.DefinitionTypeComboBox.Location = new System.Drawing.Point(257, 4);
            this.DefinitionTypeComboBox.Margin = new System.Windows.Forms.Padding(2);
            this.DefinitionTypeComboBox.Name = "DefinitionTypeComboBox";
            this.DefinitionTypeComboBox.Size = new System.Drawing.Size(198, 21);
            this.DefinitionTypeComboBox.TabIndex = 3;
            this.DefinitionTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.DefinitionTypeComboBox_SelectedIndexChanged);
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.HelpButton);
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Controls.Add(this.DefinitionTypeLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(1213, 28);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(1184, 0);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 2;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(475, 1);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(703, 22);
            this.PropertiesHeaderLabel.TabIndex = 1;
            this.PropertiesHeaderLabel.Text = "<operation> Definition(s)";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DefinitionTypeLabel
            // 
            this.DefinitionTypeLabel.AutoSize = true;
            this.DefinitionTypeLabel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.DefinitionTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DefinitionTypeLabel.Location = new System.Drawing.Point(166, 6);
            this.DefinitionTypeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.DefinitionTypeLabel.Name = "DefinitionTypeLabel";
            this.DefinitionTypeLabel.Size = new System.Drawing.Size(93, 13);
            this.DefinitionTypeLabel.TabIndex = 0;
            this.DefinitionTypeLabel.Text = "Definition type:";
            // 
            // ControlPanel
            // 
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.ExportZipArchiveStatusLabel);
            this.ControlPanel.Controls.Add(this.ExportZipArchiveStatusValueLabel);
            this.ControlPanel.Controls.Add(this.ExportImportStatusProgressBar);
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 599);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(1216, 40);
            this.ControlPanel.TabIndex = 4;
            // 
            // ExportImportStatusProgressBar
            // 
            this.ExportImportStatusProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ExportImportStatusProgressBar.Location = new System.Drawing.Point(803, 17);
            this.ExportImportStatusProgressBar.Name = "ExportImportStatusProgressBar";
            this.ExportImportStatusProgressBar.Size = new System.Drawing.Size(400, 10);
            this.ExportImportStatusProgressBar.TabIndex = 3;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 6);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ExportZipArchiveStatusLabel
            // 
            this.ExportZipArchiveStatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ExportZipArchiveStatusLabel.AutoSize = true;
            this.ExportZipArchiveStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExportZipArchiveStatusLabel.ForeColor = System.Drawing.Color.White;
            this.ExportZipArchiveStatusLabel.Location = new System.Drawing.Point(6, 22);
            this.ExportZipArchiveStatusLabel.Name = "ExportZipArchiveStatusLabel";
            this.ExportZipArchiveStatusLabel.Size = new System.Drawing.Size(174, 13);
            this.ExportZipArchiveStatusLabel.TabIndex = 1;
            this.ExportZipArchiveStatusLabel.Text = "Definition Zip Archive Status:";
            // 
            // ExportZipArchiveStatusValueLabel
            // 
            this.ExportZipArchiveStatusValueLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ExportZipArchiveStatusValueLabel.AutoSize = true;
            this.ExportZipArchiveStatusValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExportZipArchiveStatusValueLabel.ForeColor = System.Drawing.Color.White;
            this.ExportZipArchiveStatusValueLabel.Location = new System.Drawing.Point(183, 22);
            this.ExportZipArchiveStatusValueLabel.Name = "ExportZipArchiveStatusValueLabel";
            this.ExportZipArchiveStatusValueLabel.Size = new System.Drawing.Size(177, 13);
            this.ExportZipArchiveStatusValueLabel.TabIndex = 2;
            this.ExportZipArchiveStatusValueLabel.Text = "<definition zip archive status>";
            // 
            // DefinitionExportImportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 641);
            this.ControlBox = false;
            this.Controls.Add(this.OperationPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "DefinitionExportImportForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "DefinitionExportImportForm";
            this.OperationPanel.ResumeLayout(false);
            this.OperationPanel.PerformLayout();
            this.ImportOperationPanel.ResumeLayout(false);
            this.ImportOperationPanel.PerformLayout();
            this.ImportZipArchiveContentsCopyContextMenuStrip.ResumeLayout(false);
            this.ImportZipArchiveContentsDisplayContextMenuStrip.ResumeLayout(false);
            this.ImportZipArchiveContentsCheckContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ImportDefinitionZipArchiveContentsDataGridView)).EndInit();
            this.ImportDefinitionZipArchiveContentsDataGridViewContextMenuStrip.ResumeLayout(false);
            this.ExportOperationPanel.ResumeLayout(false);
            this.ExportPropertiesPanel.ResumeLayout(false);
            this.ExportPropertiesPanel.PerformLayout();
            this.ExportDefinitionSelectionPanel.ResumeLayout(false);
            this.BuildDefinitionSelectionPanel.ResumeLayout(false);
            this.BuildDefinitionSearchPanel.ResumeLayout(false);
            this.BuildDefinitionSearchPanel.PerformLayout();
            this.ReleaseDefinitionSelectionPanel.ResumeLayout(false);
            this.ReleaseDefinitionSearchPanel.ResumeLayout(false);
            this.ReleaseDefinitionSearchPanel.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.PropertiesHeaderPanel.PerformLayout();
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolTip DefinitionExportImportToolTip;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.Panel OperationPanel;
        private System.Windows.Forms.Panel ExportOperationPanel;
        private System.Windows.Forms.Panel ExportDefinitionSelectionPanel;
        private System.Windows.Forms.Panel ReleaseDefinitionSelectionPanel;
        private System.Windows.Forms.Panel ReleaseDefinitionSearchPanel;
        private System.Windows.Forms.Label ReleaseDefinitionSearchPlaceholderLabel;
        private System.Windows.Forms.Panel BuildDefinitionSelectionPanel;
        private System.Windows.Forms.Panel BuildDefinitionSearchPanel;
        private System.Windows.Forms.Label OperationLabel;
        private System.Windows.Forms.ComboBox OperationTypeComboBox;
        private System.Windows.Forms.ComboBox DefinitionTypeComboBox;
        private System.Windows.Forms.Label DefinitionTypeLabel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.ProgressBar ExportImportStatusProgressBar;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Panel ExportPropertiesPanel;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Button HelpButton;
        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.Panel ImportOperationPanel;
        private System.Windows.Forms.Label BuildDefinitionSearchPlaceholderLabel;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button ExportButton;
        private System.Windows.Forms.TextBox ImportZipFileLocationTextBox;
        private System.Windows.Forms.Label ImportInstructionsLabel;
        private System.Windows.Forms.Label ImportZipFileLabel;
        private System.Windows.Forms.Button OpenImportZipFileButton;
        private System.Windows.Forms.Button ImportZipFileLocationSelectionButton;
        private System.Windows.Forms.Button ImportButton;
        private System.Windows.Forms.Splitter VerticalSplitter;
        private System.Windows.Forms.ListBox ExportDefinitionZipArchiveListBox;
        private System.Windows.Forms.Label DefinitionZipArchiveContentsLabel;
        private System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.CheckBox ResetAfterSaveCheckBox;
        private System.Windows.Forms.Label DefinitionZipArchiveEntryCountLLabel;
        private System.Windows.Forms.DataGridView ImportDefinitionZipArchiveContentsDataGridView;
        private SplitButton ImportCheckSplitButton;
        private System.Windows.Forms.ContextMenuStrip ImportZipArchiveContentsCheckContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ImportZipArchiveContentsSelectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImportZipArchiveContentsSelectNoneToolStripMenuItem;
        private SplitButton ImportDisplaySplitButton;
        private System.Windows.Forms.ContextMenuStrip ImportZipArchiveContentsDisplayContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ImportZipArchiveContentsDisplayAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImportZipArchiveContentsDisplayCheckedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImportZipArchiveContentsSelectSelectedToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip ImportDefinitionZipArchiveContentsDataGridViewContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ImportDefinitionZipArchiveContentsDataGridViewCheckAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImportDefinitionZipArchiveContentsDataGridViewCheckSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImportDefinitionZipArchiveContentsDataGridViewCheckNoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem ImportDefinitionZipArchiveContentsDataGridViewDisplayAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImportDefinitionZipArchiveContentsDataGridViewDisplayCheckedToolStripMenuItem;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExtraColumn;
        private System.Windows.Forms.TextBox ImportDefinitionNamePrefixTextBox;
        private System.Windows.Forms.Label ImportDefinitionNamePrefixLabel;
        private System.Windows.Forms.Label ImportBuildDefinitionFolderLabel;
        private System.Windows.Forms.ComboBox ImportBuildDefinitionFolderComboBox;
        private System.Windows.Forms.Label ImportRepositoryTypeLabel;
        private System.Windows.Forms.ComboBox ImportRepositoryTypeComboBox;
        private System.Windows.Forms.ComboBox ImportRepositoryComboBox;
        private System.Windows.Forms.Label ImportRepositoryLabel;
        private SplitButton ImportCopySplitButton;
        private System.Windows.Forms.ContextMenuStrip ImportZipArchiveContentsCopyContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ImportZipArchiveContentsCopyBuildDefinitionNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImportZipArchiveContentsCopyBuildDefinitionJsonToolStripMenuItem;
        private System.Windows.Forms.CheckBox ImportMigrateToGitCheckBox;
        private System.Windows.Forms.Label ExportZipArchiveStatusLabel;
        private System.Windows.Forms.Label ExportZipArchiveStatusValueLabel;
    }
}