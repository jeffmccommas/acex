﻿namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    partial class HomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.StartPageLabel = new System.Windows.Forms.Label();
            this.NavigationFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.StartPageWebBrowser = new System.Windows.Forms.WebBrowser();
            this.label1 = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.HelpButton = new System.Windows.Forms.Button();
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.PropertiesPanel.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(4, 1);
            this.PropertiesHeaderLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(901, 30);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "Home";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PropertiesPanel.Controls.Add(this.StartPageLabel);
            this.PropertiesPanel.Controls.Add(this.NavigationFlowLayoutPanel);
            this.PropertiesPanel.Controls.Add(this.StartPageWebBrowser);
            this.PropertiesPanel.Controls.Add(this.label1);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PropertiesPanel.MinimumSize = new System.Drawing.Size(747, 591);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(955, 623);
            this.PropertiesPanel.TabIndex = 3;
            // 
            // StartPageLabel
            // 
            this.StartPageLabel.AutoSize = true;
            this.StartPageLabel.Location = new System.Drawing.Point(501, 38);
            this.StartPageLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.StartPageLabel.Name = "StartPageLabel";
            this.StartPageLabel.Size = new System.Drawing.Size(73, 16);
            this.StartPageLabel.TabIndex = 10;
            this.StartPageLabel.Text = "Start page:";
            // 
            // NavigationFlowLayoutPanel
            // 
            this.NavigationFlowLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.NavigationFlowLayoutPanel.AutoScroll = true;
            this.NavigationFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.NavigationFlowLayoutPanel.Location = new System.Drawing.Point(9, 58);
            this.NavigationFlowLayoutPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.NavigationFlowLayoutPanel.Name = "NavigationFlowLayoutPanel";
            this.NavigationFlowLayoutPanel.Size = new System.Drawing.Size(488, 494);
            this.NavigationFlowLayoutPanel.TabIndex = 9;
            this.NavigationFlowLayoutPanel.WrapContents = false;
            // 
            // StartPageWebBrowser
            // 
            this.StartPageWebBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.StartPageWebBrowser.Location = new System.Drawing.Point(505, 58);
            this.StartPageWebBrowser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.StartPageWebBrowser.MinimumSize = new System.Drawing.Size(27, 25);
            this.StartPageWebBrowser.Name = "StartPageWebBrowser";
            this.StartPageWebBrowser.Size = new System.Drawing.Size(439, 494);
            this.StartPageWebBrowser.TabIndex = 8;
            this.StartPageWebBrowser.Url = new System.Uri("", System.UriKind.Relative);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Navigation:";
            // 
            // ControlPanel
            // 
            this.ControlPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 574);
            this.ControlPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(955, 49);
            this.ControlPanel.TabIndex = 5;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(8, 20);
            this.BackgroundTaskStatusLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.HelpButton);
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(367, 27);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(955, 31);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(917, 0);
            this.HelpButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(37, 28);
            this.HelpButton.TabIndex = 12;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.VsoBuild.Sidekicks.Help.chm";
            // 
            // HomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(956, 623);
            this.Controls.Add(this.PropertiesPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(533, 492);
            this.Name = "HomeForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "HomeForm";
            this.PropertiesPanel.ResumeLayout(false);
            this.PropertiesPanel.PerformLayout();
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.WebBrowser StartPageWebBrowser;
        private System.Windows.Forms.FlowLayoutPanel NavigationFlowLayoutPanel;
        private System.Windows.Forms.Label StartPageLabel;
        private System.Windows.Forms.Button HelpButton;
    }
}