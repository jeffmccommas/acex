﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using Aclara.Vsts.Notification.Client;
using Aclara.Vsts.Notification.Client.Events;
using Aclara.Vsts.Notification.Client.Models;
using Aclara.Vsts.Notification.Client.Types;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public class NotificationAdminPresenter
    {

        #region Private Constants

        private const string BackgroundTaskStatus_CancellingRequest = "Cancelling notfication subscription request...";
        private const string BackgroundTaskStatus_Requested = "Notfication subscription requested...";
        private const string BackgroundTaskStatus_Completed = "Notfication subscription request canceled.";
        private const string BackgroundTaskStatus_Canceled = "Notfication subscription request canceled.";

        private const string BackgroundTaskTotalDuration = "[*] Notfication subscription request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})";

        private const string ApplyButtonText_Cancel = "Cancel";

        private const string Event_CreateNotificationSubscription_CompletedWithStatus = "Create notification subscription completed. (Status --> {1})";
        private const string Event_CreateNotificationSubscription_Completed = "Create notification subscription completed.";

        private const string Event_ChangeNotificationSubscription_CompletedWithStatus = "Change notification subscription completed. (Status --> {1})";
        private const string Event_ChangeNotificationSubscription_Completed = "Change notification subscription completed.";

        private const string Event_DeleteNotificationSubscription_CompletedWithStatus = "Delete notification subscription completed. (Status --> {1})";
        private const string Event_DeleteNotificationSubscription_Completed = "Delete notification subscription completed.";

        private const string Event_GetNotificationSubscriptionListRetrieved_CompletedWithStatus = "Get notification subscription list completed. (Status --> {1})";
        private const string Event_GetNotificationSubscriptionListRetrieved_Completed = "Get notification subscription list completed.";

        private const string BackgroundTaskException = "Operation cancelled.";
        private const string BackgroundTaskExceptionUnobserved = "Operation cancelled. (Exception unobserved)";

        private const string RequestFailed = "Request failed.";

        #endregion

        #region Public Types

        /// <summary>
        /// Background task operation.
        /// </summary>
        public enum BackgroundTaskOperationId
        {
            GetNotificationSubscriptionList = 1,
            CreateNotificationSubscriptionList = 2,
            DeleteNotificationSubscriptionList = 3,
            ChangeNotificationSubscriptionList = 4
        }

        #endregion

        #region Private Data Members

        private CustomLogger _logger;
        private SidekickConfiguration _sidekickConfiguration = null;
        private INotificationAdminView _notificationAdminView = null;
        private NotificationSubscriptionList _notificationSubscriptionList;
        private NotificationManager _notificationManager = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;
        private ITeamProjectInfo _teamProjectInfo = null;
        private BackgroundTaskOperationId _backgroundTaskOperation;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.NotificationAdminView.SidekickName,
                                               this.NotificationAdminView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Notification admin view.
        /// </summary>
        public INotificationAdminView NotificationAdminView
        {
            get { return _notificationAdminView; }
            set { _notificationAdminView = value; }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Notification manager.
        /// </summary>
        public NotificationManager NotificationManager
        {
            get { return _notificationManager; }
            set { _notificationManager = value; }
        }

        /// <summary>
        /// Notification subscription list.
        /// </summary>
        public NotificationSubscriptionList NotificationSubscriptionList
        {
            get { return _notificationSubscriptionList; }
            set { _notificationSubscriptionList = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Background task operation.
        /// </summary>
        public BackgroundTaskOperationId BackgroundTaskOperation
        {
            get { return _backgroundTaskOperation; }
            set { _backgroundTaskOperation = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="notificationAdminView"></param>
        public NotificationAdminPresenter(ITeamProjectInfo teamProjectInfo,
                                          SidekickConfiguration sidekickConfiguration,
                                          INotificationAdminView notificationAdminView)
        {
            this.TeamProjectInfo = teamProjectInfo;
            this.SidekickConfiguration = sidekickConfiguration;
            this.NotificationAdminView = notificationAdminView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private NotificationAdminPresenter()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Deterimine if background task is running.
        /// </summary>
        public bool IsBackgroundTaskRunning()
        {
            bool result = false;

            try
            {
                if (this.BackgroundTask != null &&
                    this.BackgroundTask.Status == TaskStatus.Running)
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Cancel background task.
        /// </summary>
        public void CancelBackgroundTask()
        {
            try
            {
                if (this.BackgroundTask != null &&
                    this.BackgroundTask.Status == TaskStatus.Running)
                {
                    this.BackgroundTaskCancellationTokenSource.Cancel();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Create notification subscription list.
        /// </summary>
        /// <param name="buildDefinitionSelectorList"></param>
        /// <param name="teamName"></param>
        /// <param name="emailAddresses"></param>
        /// <param name="tag"></param>
        public void CreateNotificationSubscriptionList(BuildDefinitionSelectorList buildDefinitionSelectorList, string teamName, string emailAddresses, string tag)
        {
            List<string> buildDefinitionNames = null;

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.NotificationAdminView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;

                    return;
                }

                this.BackgroundTaskOperation = BackgroundTaskOperationId.CreateNotificationSubscriptionList;

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.NotificationAdminView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;

                    this.NotificationManager = this.CreateNotificationManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.NotificationManager.CreateNotificationSubscriptionCompleted += OnCreateNotificationSubscriptionCompleted;

                    buildDefinitionNames = buildDefinitionSelectorList.Where(bd => bd.Selected == true).Select(bd => bd.Name).ToList();

                    this.BackgroundTask = new Task(() => this.NotificationManager.CreateNotificationSubscriptionList(buildDefinitionNames,
                                                                                                                     teamName,
                                                                                                                     emailAddresses,
                                                                                                                     tag,
                                                                                                                     cancellationToken),
                                                                                                            cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Change notification subscription list.
        /// </summary>
        /// <param name="notificationSubscriptionIdList"></param>
        /// <param name="notificationSubscriptionChangeProperties"></param>
        public void ChangeNotificationSubscriptionList(List<string> notificationSubscriptionIdList, 
                                                       NotificationSubscriptionChangeProperties notificationSubscriptionChangeProperties  )
        {
            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.NotificationAdminView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;

                    return;
                }

                this.BackgroundTaskOperation = BackgroundTaskOperationId.CreateNotificationSubscriptionList;

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.NotificationAdminView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;

                    this.NotificationManager = this.CreateNotificationManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.NotificationManager.ChangeNotificationSubscriptionCompleted += OnChangeNotificationSubscriptionCompleted;

                    this.BackgroundTask = new Task(() => this.NotificationManager.ChangeNotificationSubscriptionList(notificationSubscriptionIdList,
                                                                                                                     notificationSubscriptionChangeProperties,
                                                                                                                     cancellationToken),
                                                                                                            cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }
        /// <summary>
        /// Delete notification subscription list.
        /// </summary>
        /// <param name="buildDefinitionSelectorList"></param>
        public void DeleteNotificationSubscriptionList(List<string> notificationSubscriptionIdList)
        {
            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.NotificationAdminView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;

                    return;
                }

                this.BackgroundTaskOperation = BackgroundTaskOperationId.DeleteNotificationSubscriptionList;

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.NotificationAdminView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;

                    this.NotificationManager = this.CreateNotificationManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.NotificationManager.DeleteNotificationSubscriptionCompleted += OnDeleteNotificationSubscriptionCompleted;

                    this.BackgroundTask = new Task(() => this.NotificationManager.DeleteNotificationSubscriptionList(notificationSubscriptionIdList,
                                                                                                                  cancellationToken),
                                                                                                            cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Get notification subscription list.
        /// </summary>
        /// <param name="buildDefinitionSelectorList"></param>
        /// <param name="filterOnTag"></param>
        /// <param name="tag"></param>
        public void GetNotificationSubscriptionList(BuildDefinitionSelectorList buildDefinitionSelectorList,
                                                    bool filterOnTag,
                                                    string tag)
        {
            List<string> buildDefinitionNames = null;

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.NotificationAdminView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;

                    return;
                }

                this.BackgroundTaskOperation = BackgroundTaskOperationId.GetNotificationSubscriptionList;

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.NotificationAdminView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;

                    this.NotificationManager = this.CreateNotificationManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.NotificationManager.GetNotificationSubscriptionListRetrieved += OnNotificationSubscriptionListRetrieved;

                    buildDefinitionNames = buildDefinitionSelectorList.Select(bd => bd.Name).ToList();

                    this.BackgroundTask = new Task(() => this.NotificationManager.GetNotificationSubscriptionList(buildDefinitionNames,
                                                                                                                  filterOnTag,
                                                                                                                  tag,
                                                                                                                  cancellationToken),
                                                                                                            cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Retrieve team name list.
        /// </summary>
        /// <returns></returns>
        public List<string> GetTeamNameList()
        {
            List<string> result = null;
            Guid teamProjectId;

            this.NotificationManager = this.CreateNotificationManager();

            teamProjectId = this.NotificationManager.GetProjectId(this.TeamProjectInfo.TeamProjectCollectionUri);

            result = this.NotificationManager.GetTeamNameList(teamProjectId);

            return result;
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalDuration;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.NotificationAdminView.BackgroundTaskCompleted(BackgroundTaskStatus_Completed, this.BackgroundTaskOperation);

                        this.Logger.Info(string.Format(BackgroundTaskStatus_Canceled));

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.NotificationAdminView.BackgroundTaskCompleted(string.Empty, this.BackgroundTaskOperation);

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(BackgroundTaskException));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(BackgroundTaskException));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(RequestFailed));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(RequestFailed));
                                    }

                                }
                            }
                        }

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.NotificationAdminView.BackgroundTaskCompleted(Event_GetNotificationSubscriptionListRetrieved_Completed, this.BackgroundTaskOperation);

                        this.NotificationAdminView.NotificationSubscriptionList = this.NotificationSubscriptionList;
                        this.NotificationAdminView.PopulateNotificationSubscriptionList();
                        this.NotificationAdminView.RefreshButtonEnable(true);

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create notificaiton manager.
        /// </summary>
        /// <returns></returns>
        protected NotificationManager CreateNotificationManager()
        {
            NotificationManager result = null;

            try
            {
                result = NotificationManagerFactory.CreateNotificationManager(this.TeamProjectInfo.TeamProjectCollectionUri,
                                                                              this.TeamProjectInfo.TeamProjectCollectionVsrmUri,
                                                                              this.TeamProjectInfo.TeamProjectName,
                                                                              this.TeamProjectInfo.BasicAuthRestAPIUserProfileName,
                                                                              this.TeamProjectInfo.BasicAuthRestAPIPassword);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: Notification subscription created.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="createNotificationSubscriptionEventArgs"></param>
        protected void OnCreateNotificationSubscriptionCompleted(Object sender,
                                                               CreateNotificationSubscriptionEventArgs createNotificationSubscriptionEventArgs)
        {
            string message = string.Empty;
            int notificationSubscriptionCreatedCount = 0;
            int notificationSubscriptionTotalCount = 0;
            string statusMessage = string.Empty;

            try
            {

                //Log event completed with status.
                if (createNotificationSubscriptionEventArgs.StatusList != null &&
                    createNotificationSubscriptionEventArgs.StatusList.Count > 0)
                {
                    message = string.Format(Event_CreateNotificationSubscription_CompletedWithStatus,
                                            createNotificationSubscriptionEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (createNotificationSubscriptionEventArgs.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                }
                else if (createNotificationSubscriptionEventArgs.Description != null)
                {

                    notificationSubscriptionCreatedCount = createNotificationSubscriptionEventArgs.NotificationSubscriptionCreatedCount;
                    notificationSubscriptionTotalCount = createNotificationSubscriptionEventArgs.NotificationSubscriptionTotalCount;

                    statusMessage = string.Format("Notifcation subscription created. [{1} of {2}] (Description: {0})",
                                                  createNotificationSubscriptionEventArgs.Description,
                                                  notificationSubscriptionCreatedCount,
                                                  notificationSubscriptionTotalCount);

                    this.NotificationAdminView.UpdateNotificationSubscriptionOperationStatus(statusMessage,
                                                                             notificationSubscriptionCreatedCount,
                                                                             notificationSubscriptionTotalCount);

                    this.Logger.Info(statusMessage);

                }
                //Log event completed without status.
                else
                {
                    message = string.Format(Event_CreateNotificationSubscription_Completed);
                    this.Logger.Info(message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Hanlder: Notification subscription changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="changeNotificationSubscriptionEventArgs"></param>
        protected void OnChangeNotificationSubscriptionCompleted(Object sender,
                                                                 ChangeNotificationSubscriptionEventArgs changeNotificationSubscriptionEventArgs)
        {
            string message = string.Empty;
            int notificationSubscriptionChangedCount = 0;
            int notificationSubscriptionTotalCount = 0;
            string statusMessage = string.Empty;

            try
            {

                //Log event completed with status.
                if (changeNotificationSubscriptionEventArgs.StatusList != null &&
                    changeNotificationSubscriptionEventArgs.StatusList.Count > 0)
                {
                    message = string.Format(Event_ChangeNotificationSubscription_CompletedWithStatus,
                                            changeNotificationSubscriptionEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (changeNotificationSubscriptionEventArgs.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                }
                else if (changeNotificationSubscriptionEventArgs.Description != null)
                {

                    notificationSubscriptionChangedCount = changeNotificationSubscriptionEventArgs.NotificationSubscriptionChangedCount;
                    notificationSubscriptionTotalCount = changeNotificationSubscriptionEventArgs.NotificationSubscriptionTotalCount;

                    statusMessage = string.Format("Notifcation subscription changed. [{1} of {2}] (Description: {0})",
                                                  changeNotificationSubscriptionEventArgs.Description,
                                                  notificationSubscriptionChangedCount,
                                                  notificationSubscriptionTotalCount);

                    this.NotificationAdminView.UpdateNotificationSubscriptionOperationStatus(statusMessage,
                                                                             notificationSubscriptionChangedCount,
                                                                             notificationSubscriptionTotalCount);

                    this.Logger.Info(statusMessage);

                }
                //Log event completed without status.
                else
                {
                    message = string.Format(Event_ChangeNotificationSubscription_Completed);
                    this.Logger.Info(message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Hanlder: Notification subscription deleted.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="deleteNotificationSubscriptionEventArgs"></param>
        protected void OnDeleteNotificationSubscriptionCompleted(Object sender,
                                                                 DeleteNotificationSubscriptionEventArgs deleteNotificationSubscriptionEventArgs)
        {
            string message = string.Empty;
            int notificationSubscriptionDeletedCount = 0;
            int notificationSubscriptionTotalCount = 0;
            string statusMessage = string.Empty;

            try
            {

                //Log event completed with status.
                if (deleteNotificationSubscriptionEventArgs.StatusList != null &&
                    deleteNotificationSubscriptionEventArgs.StatusList.Count > 0)
                {
                    message = string.Format(Event_DeleteNotificationSubscription_CompletedWithStatus,
                                            deleteNotificationSubscriptionEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (deleteNotificationSubscriptionEventArgs.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                }
                else if (deleteNotificationSubscriptionEventArgs.Description != null)
                {

                    notificationSubscriptionDeletedCount = deleteNotificationSubscriptionEventArgs.NotificationSubscriptionDeletedCount;
                    notificationSubscriptionTotalCount = deleteNotificationSubscriptionEventArgs.NotificationSubscriptionTotalCount;

                    statusMessage = string.Format("Notifcation subscription deleted. [{1} of {2}] (Description: {0})",
                                                  deleteNotificationSubscriptionEventArgs.Description,
                                                  notificationSubscriptionDeletedCount,
                                                  notificationSubscriptionTotalCount);

                    this.NotificationAdminView.UpdateNotificationSubscriptionOperationStatus(statusMessage,
                                                                             notificationSubscriptionDeletedCount,
                                                                             notificationSubscriptionTotalCount);

                    this.Logger.Info(statusMessage);

                }
                //Log event completed without status.
                else
                {
                    message = string.Format(Event_DeleteNotificationSubscription_Completed);
                    this.Logger.Info(message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }


        /// <summary>
        /// Event Hanlder: Notification subscription list retrieved.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="getNotificationSubscriptionListEventArgs"></param>
        protected void OnNotificationSubscriptionListRetrieved(Object sender,
                                                               GetNotificationSubscriptionListEventArgs getNotificationSubscriptionListEventArgs)
        {
            string message = string.Empty;
            int notificationSubscriptionListRetrievedCount = 0;
            int notificationSubscriptionListTotalCount = 0;
            string statusMessage = string.Empty;

            try
            {

                //Log event completed with status.
                if (getNotificationSubscriptionListEventArgs.StatusList != null &&
                    getNotificationSubscriptionListEventArgs.StatusList.Count > 0)
                {
                    message = string.Format(Event_GetNotificationSubscriptionListRetrieved_CompletedWithStatus,
                                            getNotificationSubscriptionListEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (getNotificationSubscriptionListEventArgs.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                }
                else if (getNotificationSubscriptionListEventArgs.NotificationSubscriptionList != null)
                {

                    if (this.NotificationSubscriptionList == null)
                    {
                        this.NotificationSubscriptionList = getNotificationSubscriptionListEventArgs.NotificationSubscriptionList;
                    }
                    else
                    {
                        this.NotificationSubscriptionList.AddRange(getNotificationSubscriptionListEventArgs.NotificationSubscriptionList);
                    }

                    notificationSubscriptionListRetrievedCount = getNotificationSubscriptionListEventArgs.NotificationSubscriptionRetrievedCount;
                    notificationSubscriptionListTotalCount = getNotificationSubscriptionListEventArgs.NotificationSubscriptionTotalCount;

                    statusMessage = string.Format("Notifcation subscription retrieved.");
                    this.NotificationAdminView.UpdateNotificationSubscriptionOperationStatus(statusMessage,
                                                                             notificationSubscriptionListRetrievedCount,
                                                                             notificationSubscriptionListTotalCount);


                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                        }
                        else
                        {
                            Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                }
                else
                {
                    Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
