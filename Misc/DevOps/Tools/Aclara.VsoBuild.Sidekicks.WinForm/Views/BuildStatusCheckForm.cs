﻿using Aclara.Vso.Build.Client.Models;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using Aclara.VsoBuild.Sidekicks.WinForm.Utility;
using Itenso.TimePeriod;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class BuildStatusCheckForm : Form, IBuildStatusCheckView, ISidekickCoaction, ISidekickView, ISidekickInfo
    {

        #region Private Types

        public enum AutoRefreshStateValue
        {
            Initialized = 0,
            StartedByUser = 1,
            StoppedByUser = 2,
            StoppedAutomatically = 3
        }

        #endregion

        #region Public  Constants

        public const string SidekickView_SidekickName = "CheckBuildStatus";
        public const string SidekickView_SidekickDescription = "Check Build Status";

        #endregion

        #region Private Constants

        private const string ValidationErrorMessage_TeamProjectNameNotSelected = "Team project not selected.";
        private const string ValidationErrorMessage_BranchNameSourceNotEntered = "Branch name source not entered.";
        private const string ValidationErrorMessage_BuildDefinitionNotSelected = "Build definition not selected.";
        private const string ValidationErrorMessage_SourceBranchNameNotEntered = "Branch name - source not entered.";
        private const string ValidationErrorMessage_SourceBuildDefinitionNamePrefixNotEntered = "Build definition name prefix - source not entered.";
        private const string ValidationErrorMessage_TargetBranchNameNotEntered = "Branch name - target not entered.";
        private const string ValidationErrorMessage_TargetBuildDefinitionNamePrefixNotEntered = "Build definition name prefix - target not entered.";
        private const string ValidationErrorMessage_SelectedBuildDefinitionsMustHaveSameNamePrefix = "Selected build definitons must have same name prefix.";

        private const string DateFilter_No_Filter = "No filter";
        private const string DateFilter_Recent = "Recent";
        private const string DateFilter_Last_6_Hours = "Last 6 hours";
        private const string DateFilter_Last_12_Hours = "Last 12 hours";
        private const string DateFilter_Last_24_Hours = "Last 24 hours";
        private const string DateFilter_Last_3_Days = "Last 3 days";
        private const string DateFilter_Last_7_Days = "Last 7 days";

        private const string BuildStatusDataGridView_ReasonColumn = "ReasonColumn";
        private const string BuildStatusDataGridView_ResultColumn = "ResultColumn";
        private const string BuildStatusDataGridView_BuildNumberColumn = "BuildNumberColumn";
        private const string BuildStatusDataGridView_BuildDefinitionColumn = "BuildDefinitionColumn";
        private const string BuildStatusDataGridView_QueueTimeColumn = "QueueTimeColumn";
        private const string BuildStatusDataGridView_FinishTimeColumn = "FinishTimeColumn";
        private const string BuildStatusDataGridView_StartTimeColumn = "StartTimeColumn";
        private const string BuildStatusDataGriveView_BuildDurationColumn = "BuildDurationColumn";
        private const string BuildStatusDataGridView_StatusColumn = "StatusColumn";
        private const string BuildStatusDataGridView_RequestedForColumn = "RequestedForColumn";
        private const string BuildStatusDataGridView_QueueNameColumn = "QueueNameColumn";
        private const string BuildStatusDataGridView_ExtraColumn = "ExtraColumn";

        private const string BuildStatusDataGridView_Reason = "Reason";
        private const string BuildStatusDataGridView_Result = "Result";
        private const string BuildStatusDataGridView_BuildNumber = "BuildNumber";
        private const string BuildStatusDataGridView_BuildDefinition = "BuildDefinitionName";
        private const string BuildStatusDataGriveView_BuildDuration = "BuildDuration";
        private const string BuildStatusDataGridView_QueueTime = "QueueTime";
        private const string BuildStatusDataGridView_FinishTime = "FinishTime";
        private const string BuildStatusDataGridView_StartTime = "StartTime";
        private const string BuildStatusDataGridView_Status = "Status";
        private const string BuildStatusDataGridView_RequestedFor = "RequestedFor";
        private const string BuildStatusDataGridView_QueueName = "QueueName";
        private const string BuildStatusDataGridView_Extra = "Extra";

        private const string BackgroundTaskStatusAutoRefreshBuildStatusInitialized = "Auto-refresh disabled by default.";
        private const string BackgroundTaskStatusAutoRefreshBuildStatusEnabled = "Auto-refresh enabled. Elapsed ({0}h:{1}m:{2}s) Next refresh in ({3}m:{4}s)";
        private const string BackgroundTaskStatusAutoRefreshBuildStatusStoppedByUser = "Auto-refresh stopped by user. Elapsed ({0}h:{1}m:{2}s)";
        private const string BackgroundTaskStatusAutoRefreshBuildStatusStoppedAutomatically = "Auto-refresh stopped automatically. Elapsed ({0}h:{1}m:{2}s)";

        private const string BuildListRetrievalResult_BuildsFound = "{0} build found.";
        private const string BuildListRetrievalResult_BuildsFoundPlural = "{0} builds found.";
        private const string BuildListRetrievalResult_MissingBuild = " {0} build definition with missing build status.";
        private const string BuildListRetrievalResult_MissingBuildPlural = " {0} build definitions with missing build status.";

        private const string BuildListRetrievalStatus_RetrievingQueueAndRecentlyCompleted = "Retreiving queued and recently completed builds";

        private const string NotAvailable = "N/A";

        private const int MaxAutoRefreshBuildStatusDuration = 90;

        private const string ConfirmOperation_LargeBuildCount_Caption = "Confirmation Required";
        private const string ConfirmOperation_LargeBuildCount_Message = "{0} build definitions have been checked. Click 'Yes' to continue.";
        private const int ConfirmOperation_MaxBuildCount = 10;

        #endregion

        #region Private Data Members

        private BuildList _buildList;
        private List<BuildStatusDisplay> _buildStatusDisplayList;
        private SortableBindingList<BuildStatusDisplay> _buildStatusDisplayListBindingSource;
        private SidekickConfiguration _sidekickConfiguration = null;
        private BuildDefinitionSearchForm _buildDefinitionSearchForm = null;
        private ISidekickCoaction _sidekickCoaction;
        private BuildStatusCheckPresenter _BuildStatusCheckPresenter;
        private System.Diagnostics.Stopwatch _autoRefreshBuildStatusElapsedStopWatch;
        private System.Diagnostics.Stopwatch _autoRefreshBuildStatusIntervalStopWatch;
        private AutoRefreshStateValue _autoRefreshState;
        private DataGridViewColumn _sortedColumn;
        private System.Windows.Forms.SortOrder _sortOrder;
        private bool _oneOrMoreBuildDefinitionsSelected;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickView_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickView_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                result = false;

                return result;
            }
        }

        /// <summary>
        /// Property: Build list.
        /// </summary>
        public BuildList BuildList
        {
            get { return _buildList; }
            set { _buildList = value; }
        }

        /// <summary>
        /// Property: Build status display list.
        /// </summary>
        public List<BuildStatusDisplay> BuildStatusDisplayList
        {
            get { return _buildStatusDisplayList; }
            set { _buildStatusDisplayList = value; }
        }

        /// <summary>
        /// Property: Build status display list binding source.
        /// </summary>
        public SortableBindingList<BuildStatusDisplay> BuildStatusDisplayListBindingSource
        {
            get { return _buildStatusDisplayListBindingSource; }
            set { _buildStatusDisplayListBindingSource = value; }
        }

        /// <summary>
        /// Property: Queue priority.
        /// </summary>
        public DateFilter DateFilter
        {
            get
            {
                return (DateFilter)this.DateFilterComboBox.SelectedItem;
            }
            set
            {
                this.DateFilterComboBox.Text = value.ToString();
            }
        }

        /// <summary>
        /// Property: Build list retrieval result.
        /// </summary>
        public string BuildListRetrievalResult
        {
            get { return this.BuildListRetrievalResultLabel.Text; }
            set { this.BuildListRetrievalResultLabel.Text = value; }
        }

        /// <summary>
        /// Property: Build list retrieval progress.
        /// </summary>
        public string BuildListRetrievalProgress
        {
            get { return this.BuildListRetrievalProgressLabel.Text; }
            set
            {
                this.BuildListRetrievalProgressLabel.Text = value;
                this.BuildListTotalClockTimeLabel.Refresh();
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick warning level.
        /// </summary>
        public SidekickContainerTypes.SidekickWarningLevel SidekickWarningLevel
        {
            get
            {
                return SidekickContainerTypes.SidekickWarningLevel.Low;
            }
        }

        /// <summary>
        /// Property: Build status check presenter.
        /// </summary>
        public BuildStatusCheckPresenter BuildStatusCheckPresenter
        {
            get { return _BuildStatusCheckPresenter; }
            set { _BuildStatusCheckPresenter = value; }
        }

        /// <summary>
        /// Property: Build definition search form.
        /// </summary>
        public BuildDefinitionSearchForm BuildDefinitionSearchForm
        {
            get { return _buildDefinitionSearchForm; }
            set { _buildDefinitionSearchForm = value; }
        }

        /// <summary>
        /// Property: Sidekick coaction.
        /// </summary>
        public ISidekickCoaction SidekickCoaction
        {
            get { return _sidekickCoaction; }
            set { _sidekickCoaction = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Auto-refresh build status elapsed stopwatch.
        /// </summary>
        public System.Diagnostics.Stopwatch AutoRefreshBuildStatusStopWatch
        {
            get { return _autoRefreshBuildStatusElapsedStopWatch; }
            set { _autoRefreshBuildStatusElapsedStopWatch = value; }
        }

        /// <summary>
        /// Property: Auto-refresh build status interval stopwatch.
        /// </summary>
        public System.Diagnostics.Stopwatch AutoRefreshBuildStatusIntervalStopWatch
        {
            get { return _autoRefreshBuildStatusIntervalStopWatch; }
            set { _autoRefreshBuildStatusIntervalStopWatch = value; }
        }

        /// <summary>
        /// Property: Auto-refresh state.
        /// </summary>
        public AutoRefreshStateValue AutoRefreshState
        {
            get { return _autoRefreshState; }
            set { _autoRefreshState = value; }
        }

        /// <summary>
        /// Property: Sort order.
        /// </summary>
        public System.Windows.Forms.SortOrder SortOrder
        {
            get { return _sortOrder; }
            set { _sortOrder = value; }
        }

        /// <summary>
        /// Property: Sorted column.
        /// </summary>
        public DataGridViewColumn SortedColumn
        {
            get { return _sortedColumn; }
            set { _sortedColumn = value; }
        }

        /// <summary>
        /// Property: One or more build definitions selected.
        /// </summary>
        public bool OneOrMoreBuildDefinitionsSelected
        {
            get { return _oneOrMoreBuildDefinitionsSelected; }
            set { _oneOrMoreBuildDefinitionsSelected = value; }
        }

        /// <summary>
        /// Property: View - Queued build definitions.
        /// </summary>
        public bool QueuedBuildDefinitions
        {
            get { return this.QueuedBuildDefinitionsCheckBox.Checked; }
            set { this.QueuedBuildDefinitionsCheckBox.Checked = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildStatusCheckForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <para name="sidekickCoaction"></para>
        public BuildStatusCheckForm(SidekickConfiguration sidekickConfiguration,
                                    BuildDefinitionSearchForm buildDefinitionSearchForm,
                                    ISidekickCoaction sidekickCoaction)
        {
            _autoRefreshBuildStatusElapsedStopWatch = new Stopwatch();
            _autoRefreshBuildStatusIntervalStopWatch = new Stopwatch();

            InitializeComponent();
            InitializeControls();

            this.BuildStatusDataGridView.AutoGenerateColumns = false;

            _buildStatusDisplayList = new List<BuildStatusDisplay>();
            _buildStatusDisplayListBindingSource = new SortableBindingList<BuildStatusDisplay>(_buildStatusDisplayList);
            this.BuildStatusDataGridView.DataSource = _buildStatusDisplayListBindingSource;

            this.SidekickConfiguration = sidekickConfiguration;
            this.BuildDefinitionSearchForm = buildDefinitionSearchForm;
            this.BuildDefinitionSearchForm.BuildDefinitionSelectionChanged += OnBuildDefinitionSelectionChanged;
            this.BuildDefinitionSearchPanel.DockControl(this.BuildDefinitionSearchForm);

            this.SidekickCoaction = sidekickCoaction;

            this.MapBuildStatusDataGridViewColumns();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Check build status.
        /// </summary>
        /// <param name="buildDefinitionNameList"></param>
        public void CheckBuildStatus(List<string> buildDefinitionNameList)
        {
            bool exactMatch = true;
            bool refresh = false;
            bool searchAndCheckAll = false;

            try
            {

                searchAndCheckAll = true;

                this.BuildDefinitionSearchForm.SearchBuildDefinitionList(buildDefinitionNameList, 
                    exactMatch, 
                    refresh, 
                    searchAndCheckAll);

                this.DateFilterComboBox.SelectedItem = DateFilter.Recent;

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Request build for another sidekick.
        /// </summary>
        /// <remarks>Not implemented. Not applicable to build status check sidekick.</remarks>
        /// <param name="buildDefinitionNameList"></param>
        public void RequestBuild(List<string> buildDefinitionNameList)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retrieve remaining build definitions.
        /// </summary>
        public void RetrieveRemainingDefinitions()
        {
            this.BuildDefinitionSearchForm.RetrieveRemainingDefinitions();
        }

        /// <summary>
        /// Populate build status.
        /// </summary>
        public void PopulateBuildStatus()
        {
            BuildStatusDisplay buildStatusDisplay = null;
            List<TimeSpan> buildDurationList = null;
            TimeSpan totalBuildDurationTimeSpan;
            TimeSpan totalClockTimeTimeSpan;
            ListSortDirection sortDirection;

            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        PopulateBuildStatus();
                    });

                }
                else
                {
                    this.StatusPanel.BackColor = Color.White;

                    this.BuildStatusDisplayList.Clear();

                    buildDurationList = new List<TimeSpan>();

                    foreach (Microsoft.TeamFoundation.Build.WebApi.Build build in this.BuildList)
                    {
                        buildStatusDisplay = new BuildStatusDisplay(build);
                        this.BuildStatusDisplayList.Add(buildStatusDisplay);
                    }

                    //Calculate build totals.
                    totalBuildDurationTimeSpan = this.CalcTotalBuildsDurationBuildsOverlappingCombined(this.BuildList);
                    totalClockTimeTimeSpan = this.CalcTotalBuildsClockTimeBuilds(this.BuildList);

                    //Update build totals.
                    this.UpdateBuildTotals(totalBuildDurationTimeSpan, totalClockTimeTimeSpan);

                    this.BuildStatusDataGridView.DataSource = null;
                    _buildStatusDisplayListBindingSource = new SortableBindingList<BuildStatusDisplay>(this.BuildStatusDisplayList);
                    this.BuildStatusDataGridView.DataSource = _buildStatusDisplayListBindingSource;

                    //Restore sort order.
                    if (this.SortOrder == SortOrder.Ascending)
                    {
                        sortDirection = ListSortDirection.Ascending;
                    }
                    else
                    {
                        sortDirection = ListSortDirection.Descending;
                    }

                    if (this.SortedColumn != null)
                    {
                        this.BuildStatusDataGridView.Sort(this.SortedColumn, sortDirection); ;
                    }

                    this.MissingBuildStatusBuildDefinitionListBox.Items.Clear();
                    var selectedBuildDefinitions = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();
                    var missingBuildStatusBuildDefinitions = from buildDefinition in selectedBuildDefinitions
                                                             where !BuildList.Any(b => b.Definition.Name == buildDefinition.Name)
                                                             select buildDefinition;

                    this.MissingBuildStatusBuildDefinitionListBox.DisplayMember = "Name";
                    this.MissingBuildStatusBuildDefinitionListBox.ValueMember = "Id";
                    foreach (BuildDefinitionSelector buildDefinitionSelector in missingBuildStatusBuildDefinitions)
                    {
                        this.MissingBuildStatusBuildDefinitionListBox.Items.Add(buildDefinitionSelector);
                    }

                    if (this.BuildList.Count() == 1)
                    {
                        this.BuildListRetrievalResult = string.Format(BuildListRetrievalResult_BuildsFound,
                                                                this.BuildList.Count());
                    }
                    else
                    {
                        this.BuildListRetrievalResult = string.Format(BuildListRetrievalResult_BuildsFoundPlural,
                                                                this.BuildList.Count());
                    }

                    if (this.QueuedBuildDefinitions == false)
                    {
                        if (missingBuildStatusBuildDefinitions.Count() == 1)
                        {
                            this.BuildListRetrievalResult += string.Format(BuildListRetrievalResult_MissingBuild,
                                                                           missingBuildStatusBuildDefinitions.Count());
                        }
                        else
                        {
                            this.BuildListRetrievalResult += string.Format(BuildListRetrievalResult_MissingBuildPlural,
                                                                           missingBuildStatusBuildDefinitions.Count());
                        }
                    }

                    this.StatusPanel.BackColor = Color.OldLace;

                    this.BuildListRetrievalProgressPanel.Visible = false;
                    this.BuildListRetrievalResultPanel.Visible = true;
                    this.BuildListTotalsPanel.Visible = true;

                    this.BuildStatusDataGridView.AutoResizeColumns();
                    this.BuildStatusDataGridView.AllowUserToResizeColumns = true;

                    for (int columnIndex = 0; columnIndex < BuildStatusDataGridView.Columns.Count; columnIndex++)
                    {
                        int columnWidth = BuildStatusDataGridView.Columns[columnIndex].Width;
                        BuildStatusDataGridView.Columns[columnIndex].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                        BuildStatusDataGridView.Columns[columnIndex].Width = columnWidth;
                    }

                    //By default select no rows.
                    if (this.BuildStatusDataGridView.Rows.Count > 0)
                    {
                        this.BuildStatusDataGridView.Rows[0].Selected = false;
                    }


                } //End of thread safe block.

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update build list retrieval status.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="buildListRetrievedCount"></param>
        /// <param name="buildListTotalCount"></param>
        public void UpdateBuildListRetrievalStatus(string statusMessage, 
                                                   int buildListRetrievedCount, 
                                                   int buildListTotalCount)
        {
            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        UpdateBuildListRetrievalStatus(statusMessage,
                                                       buildListRetrievedCount,
                                                       buildListTotalCount);
                    });
                }
                else
                {
                    this.BuildListRetrievalProgressPanel.Visible = true;
                    this.BuildListRetrievalResultPanel.Visible = false;
                    this.BuildListTotalsPanel.Visible = false;

                    this.StatusPanel.BackColor = Color.OldLace;

                    if (buildListTotalCount < buildListRetrievedCount)
                    {
                        buildListRetrievedCount = 1;
                        buildListTotalCount = 1;
                    }

                    if ((buildListRetrievedCount < buildListTotalCount) &&
                        this.QueuedBuildDefinitions == false)
                    {
                        this.BuildListRetrievalProgressPanel.Visible = true;
                        this.BuildListRetrievalResultPanel.Visible = false;
                        this.BuildListTotalsPanel.Visible = false;

                        this.BuildListRetrievalProgressBar.Visible = true;
                        this.BuildListRetrievalProgressBar.Minimum = 0;
                        this.BuildListRetrievalProgressBar.Maximum = buildListTotalCount;
                        this.BuildListRetrievalProgressBar.Value = buildListRetrievedCount;
                        this.BuildListRetrievalProgress = statusMessage;
                    }
                    else if( this.QueuedBuildDefinitions == true)
                    {
                        this.BuildListRetrievalProgressBar.Visible = false;
                        this.BuildListRetrievalProgress = statusMessage;
                    }

                } //End of thread safe block.
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Refresh button enable.
        /// </summary>
        public void RefreshButtonEnable(bool enable)
        {
            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke(new Action<bool>(this.RefreshButtonEnable), enable);
                }
                else
                {
                    this.RefreshButton.Enabled = enable;
                } //End of thread safe block.
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            this.BuildDefinitionSearchForm.TeamProjectChanged();
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
                }
                else
                {
                    this.BackgroundTaskStatus = backgroundTaskStatus;
                    this.BuildListRetrievalProgressPanel.Visible = false;
                    this.BuildListRetrievalProgress = string.Empty;
                    this.UpdateRefreshCancelMode(false);
                } //End of thread safe block.
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Background task Completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {

            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.UpdateRefreshCancelMode(false);
                this.QueuedBuildDefinitionsCheckBox.Enabled = true;
            } //End of thread safe block.
        }

        /// <summary>
        /// Refresh build list.
        /// </summary>
        protected void RefreshBuildList()
        {
            try
            {
                if (this.BuildStatusCheckPresenter.IsBackgroundTaskRunning() == true)
                {
                    this.BuildStatusCheckPresenter.CancelBackgroundTask();
                    this.UpdateRefreshCancelMode(false);
                }
                else
                {
                    this.QueuedBuildDefinitionsCheckBox.Enabled = false;
                    this.StatusPanel.BackColor = Color.White;
                    this.UpdateRefreshCancelMode(true);
                    this.RefreshBuildStatus();
                    this.AutoRefreshBuildStatusIntervalStopWatch.Restart();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Clear build list retrieval result.
        /// </summary>
        protected void ClearBuildListRetrievalResult()
        {
            try
            {
                this.BuildListRetrievalProgressPanel.Visible = false;
                this.BuildListRetrievalProgress = string.Empty;

                this.BuildListRetrievalResultPanel.Visible = false;
                this.BuildListRetrievalResult = string.Empty;

                this.BuildListTotalsPanel.Visible = false;
                this.BuildListTotalDurationValueLabel.Text = string.Empty;
                this.BuildListTotalClockTimeValueLabel.Text = string.Empty;

                this.BuildStatusDataGridView.DataSource = null;
                this.MissingBuildStatusBuildDefinitionListBox.Items.Clear();
                if (this.BuildList != null)
                {
                    this.BuildList.Clear();
                }
                this.StatusPanel.BackColor = Color.White;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        // Convert date filter enum value to text.
        /// </summary>
        /// <param name="dateFilter"></param>
        /// <returns></returns>
        protected string ConvertDateFilterToText(DateFilter dateFilter)
        {
            string result = string.Empty;

            try
            {
                switch (dateFilter)
                {
                    case DateFilter.No_Filter:
                        result = DateFilter_No_Filter;
                        break;
                    case DateFilter.Recent:
                        result = DateFilter_Recent;
                        break;
                    case DateFilter.Last_6_hours:
                        result = DateFilter_Last_6_Hours;
                        break;
                    case DateFilter.Last_12_hours:
                        result = DateFilter_Last_12_Hours;
                        break;
                    case DateFilter.Last_24_hours:
                        result = DateFilter_Last_24_Hours;
                        break;
                    case DateFilter.Last_3_days:
                        result = DateFilter_Last_3_Days;
                        break;
                    case DateFilter.Last_7_days:
                        result = DateFilter_Last_7_Days;
                        break;
                    default:
                        result = DateFilter_No_Filter;
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {

                this.UpdateRefreshCancelMode(false);

                this.BuildListRetrievalProgressPanel.Visible = false;
                this.BuildListRetrievalResultPanel.Visible = false;
                this.BuildListTotalsPanel.Visible = false;

                this.BuildListRetrievalResult = string.Empty;
                this.BuildListRetrievalProgress = string.Empty;
                this.DateFilterComboBox.FormattingEnabled = true;
                this.DateFilterComboBox.Format += delegate (object sender, ListControlConvertEventArgs e)
                {
                    e.Value = ConvertDateFilterToText((DateFilter)e.Value);
                };
                this.DateFilterComboBox.DataSource = Enum.GetValues(typeof(DateFilter));

                this.DateFilterComboBox.SelectedItem = DateFilter.Last_24_hours;
                this.TopNumericUpDown.Value = 5;
                this.RecentlyCompletedInHoursNumericUpDown.Enabled = false;
                this.RefreshInMinutesNumericUpDown.Enabled = false;
                this.StopRefreshInMinutesNumericUpDown.Enabled = false;
                this.CopySplitButton.Enabled = false;
                this.ControlBarStatusLabel.Text = string.Empty;
                this.BuildListTotalDurationValueLabel.Text = string.Empty;
                this.BuildListTotalClockTimeValueLabel.Text = string.Empty;

                this.BackgroundTaskStatusLabel.Text = string.Empty;

                EnableControlsDependantOnSelectedBuildDefinition(false);
                StatusUpdateTimer.Enabled = true;
                AutoRefreshState = AutoRefreshStateValue.Initialized;
                this.UpdateControlBarStatus();

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Update refresh cancel mode.
        /// </summary>
        /// <param name="enabled"></param>
        protected void UpdateRefreshCancelMode(bool enabled)
        {
            try
            {
                if (enabled == true)
                {
                    this.RefreshButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionCancelRefresh;
                }
                else
                {
                    this.RefreshButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Hanlder: Build definition selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="requestBuildEventArgs"></param>
        protected void OnBuildDefinitionSelectionChanged(Object sender,
                                                         BuildDefinitionSelectionChangedEventArgs buildDefinitionSelectionChangedEventArgs)
        {

            BuildDefinitionSelectorList buildDefinitionSelectorList = null;

            try
            {

                if (buildDefinitionSelectionChangedEventArgs != null)
                {
                    if (buildDefinitionSelectionChangedEventArgs.OneOrMoreSelected == true)
                    {
                        buildDefinitionSelectorList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();
                        if (buildDefinitionSelectorList.Count > 0)
                        {

                            if (this.QueuedBuildDefinitions == false)
                            {
                                this.EnableControlsDependantOnSelectedBuildDefinition(true);
                            }
                            this.OneOrMoreBuildDefinitionsSelected = true;

                        }
                    }
                    else
                    {
                        if (this.QueuedBuildDefinitions == false)
                        {
                            this.ClearBuildListRetrievalResult();
                            this.EnableControlsDependantOnSelectedBuildDefinition(false);
                        }
                        this.OneOrMoreBuildDefinitionsSelected = false;
                    }

                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Enable/disable controls dependant on at lease one build definition selected.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnSelectedBuildDefinition(bool enable)
        {
            try
            {

                this.DateFilterComboBox.Enabled = enable;
                this.RefreshButton.Enabled = enable;
                this.TopNumericUpDown.Enabled = enable;
                if (this.DateFilter == DateFilter.Recent)
                {
                    this.RefreshInMinutesNumericUpDown.Enabled = enable;
                }
                else
                {
                    this.RefreshInMinutesNumericUpDown.Enabled = false;

                }
                this.SwapBuildStatusViewButton.Enabled = enable;
                this.AutoRefreshBuildStatusCheckBox.Enabled = enable;
                if (this.AutoRefreshBuildStatusCheckBox.Enabled == false)
                {
                    this.AutoRefreshBuildStatusCheckBox.Checked = false;
                }
                if (this.AutoRefreshBuildStatusCheckBox.Checked == true && enable == true)
                {
                    this.RefreshInMinutesNumericUpDown.Enabled = true;
                    this.StopRefreshInMinutesNumericUpDown.Enabled = true;
                }
                else
                {
                    this.RefreshInMinutesNumericUpDown.Enabled = false;
                    this.StopRefreshInMinutesNumericUpDown.Enabled = false;
                }
                this.CopySplitButton.Enabled = enable;
                this.BuildStatusDataGridView.Enabled = enable;
                if (enable == false)
                {
                    this.BuildListTotalDurationValueLabel.Text = string.Empty;
                    this.BuildListTotalClockTimeValueLabel.Text = string.Empty;
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Enable/disable controls dependant on view queued build definitions checked.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnViewQueuedBuildDefinition(bool enable)
        {
            try
            {
                if (enable == true)
                {
                    this.DateFilterComboBox.Enabled = false;
                    this.RefreshButton.Enabled = true;
                    this.TopNumericUpDown.Enabled = false;
                    this.DateFilter = DateFilter.Recent;
                    this.RefreshInMinutesNumericUpDown.Enabled = true;
                    this.SwapBuildStatusViewButton.Enabled = true;
                    this.AutoRefreshBuildStatusCheckBox.Enabled = true;
                    this.RefreshInMinutesNumericUpDown.Enabled = true;
                    this.StopRefreshInMinutesNumericUpDown.Enabled = true;
                    this.CopySplitButton.Enabled = true;
                    this.BuildStatusDataGridView.Enabled = true;
                }
                else
                {
                    if (this.OneOrMoreBuildDefinitionsSelected == true)
                    {
                        this.EnableControlsDependantOnSelectedBuildDefinition(true);
                    }
                    else
                    {
                        this.EnableControlsDependantOnSelectedBuildDefinition(false);
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Refresh build status.
        /// </summary>
        protected void RefreshBuildStatus()
        {
            BuildDefinitionSelectorList buildDefinitionSelectorList = null;
            int top = 0;
            int recentlyCompletedInHours = 0;

            try
            {

                if (this.BuildStatusCheckPresenter.TeamProjectInfo.TeamProjectCollectionUri == null ||
                    this.BuildStatusCheckPresenter.TeamProjectInfo.TeamProjectName == string.Empty)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_TeamProjectNameNotSelected));
                    return;
                }

                this.ClearBuildListRetrievalResult();

                if (this.QueuedBuildDefinitions == true)
                {
                    this.UpdateBuildListRetrievalStatus(BuildListRetrievalStatus_RetrievingQueueAndRecentlyCompleted, 0, 0);
                }

                this.SortedColumn = this.BuildStatusDataGridView.SortedColumn;
                this.SortOrder = this.BuildStatusDataGridView.SortOrder;

                buildDefinitionSelectorList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();
                if (buildDefinitionSelectorList == null ||
                    buildDefinitionSelectorList.Count <= 0 &&
                    this.QueuedBuildDefinitions == false)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_BuildDefinitionNotSelected));
                    return;
                }

                top = (int)this.TopNumericUpDown.Value;
                recentlyCompletedInHours = (int)this.RecentlyCompletedInHoursNumericUpDown.Value;
                this.BuildStatusCheckPresenter.GetBuildsRestApi(buildDefinitionSelectorList, this.DateFilter, top, recentlyCompletedInHours, this.QueuedBuildDefinitions);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Format Build status data grid view as text.
        /// </summary>
        /// <returns></returns>
        protected string FormatBuildStatusDataGridViewAsText()
        {
            const string BuildNameHeading = "Build Name";
            const string BuildDefinitionNameHeading = "Build Definition Name";
            const string QueueTimeHeading = "Queue Time";
            const string StartTimeHeading = "Start Time";
            const string FinishTimeHeading = "Finish Time";
            const string BuildDurationHeading = "Build Duration";
            const string StatusHeading = "Status";
            const string ResultHeading = "Result";

            string result = string.Empty;
            StringBuilder formattedTextStringBuilder = null;
            BuildStatusDisplay buildStatusDisplay = null;

            try
            {
                formattedTextStringBuilder = new StringBuilder();

                //Add header.
                formattedTextStringBuilder.AppendLine(string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}",
                                                                    BuildNameHeading,
                                                                    BuildDefinitionNameHeading,
                                                                    QueueTimeHeading,
                                                                    StartTimeHeading,
                                                                    FinishTimeHeading,
                                                                    BuildDurationHeading,
                                                                    StatusHeading,
                                                                    ResultHeading));

                foreach (DataGridViewRow dataGridViewRow in this.BuildStatusDataGridView.Rows)
                {

                    buildStatusDisplay = (BuildStatusDisplay)dataGridViewRow.DataBoundItem;
                    formattedTextStringBuilder.AppendLine(string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}",
                                                                        buildStatusDisplay.BuildNumber,
                                                                        buildStatusDisplay.BuildDefinitionName,
                                                                        buildStatusDisplay.QueueTime,
                                                                        buildStatusDisplay.StartTime,
                                                                        buildStatusDisplay.FinishTime,
                                                                        buildStatusDisplay.BuildDuration,
                                                                        buildStatusDisplay.Status,
                                                                        buildStatusDisplay.Result));
                }

                result = formattedTextStringBuilder.ToString();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

            return result;

        }

        /// <summary>
        /// Format Build status data grid view as html.
        /// </summary>
        /// <returns></returns>
        protected string FormatBuildStatusDataGridViewAsHtml()
        {
            const string BuildNameHeading = "Build Name";
            const string BuildDefinitionNameHeading = "Build Definition Name";
            const string QueueTimeHeading = "Queue Time";
            const string StartTimeHeading = "Start Time";
            const string FinishTimeHeading = "Finish Time";
            const string BuildDurationHeading = "Build Duration";
            const string StatusHeading = "Status";
            const string ResultHeading = "Result";
            const string ColorWhite = "#FFFFFF";
            const string ColorGreen = "#A9DFBF";
            const string ColorBlue = "#D6EAF8";
            const string ColorYellow = "#FCF3CF";
            const string ColorGray = "";

            string result = string.Empty;
            StringBuilder formattedTextStringBuilder = null;
            BuildStatusDisplay buildStatusDisplay = null;
            string buildResultClass = string.Empty;

            try
            {
                formattedTextStringBuilder = new StringBuilder();

                formattedTextStringBuilder.AppendLine("<html>");

                formattedTextStringBuilder.AppendLine("<style type='text//css'>");
                formattedTextStringBuilder.AppendLine(".tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}");
                formattedTextStringBuilder.AppendLine(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}");
                formattedTextStringBuilder.AppendLine(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}");
                formattedTextStringBuilder.AppendLine(".tg .tg-7fle{font-weight:bold;background-color:#efefef;text-align:center;vertical-align:top}");
                formattedTextStringBuilder.AppendLine(".tg .tg-3we0{background-color:#ffffff;vertical-align:top}");
                formattedTextStringBuilder.AppendLine(".tg .tg-q9qv{ background-color:#fe0000;vertical-align:top}");
                formattedTextStringBuilder.AppendLine(".tg .tg-a080{background-color:#9aff99;vertical-align:top}");
                formattedTextStringBuilder.AppendLine(".tg .tg-c57o{background-color:#ecf4ff;vertical-align:top}");
                formattedTextStringBuilder.AppendLine(".tg .tg-3we0{background-color:#ffffff;vertical-align:top}");
                formattedTextStringBuilder.AppendLine("</style> ");


                formattedTextStringBuilder.AppendLine("<table class='tg'>");
                formattedTextStringBuilder.AppendLine("<tr>");


                //Add header.
                formattedTextStringBuilder.AppendLine(string.Format("<th class='tg-7fle'>{0}<//th><th class='tg-7fle'>{1}<//th><th class='tg-7fle'>{2}<//th><th class='tg-7fle'>{3}<//th><th class='tg-7fle'>{4}<//th><th class='tg-7fle'>{5}<//th><th class='tg-7fle'>{6}<//th><th class='tg-7fle'>{7}<//th>",
                                                                    BuildNameHeading,
                                                                    BuildDefinitionNameHeading,
                                                                    QueueTimeHeading,
                                                                    StartTimeHeading,
                                                                    FinishTimeHeading,
                                                                    BuildDurationHeading,
                                                                    StatusHeading,
                                                                    ResultHeading));
                formattedTextStringBuilder.AppendLine("</tr>");

                foreach (DataGridViewRow dataGridViewRow in this.BuildStatusDataGridView.Rows)
                {

                    buildStatusDisplay = (BuildStatusDisplay)dataGridViewRow.DataBoundItem;
                    formattedTextStringBuilder.AppendLine("<tr>");
                    if (buildStatusDisplay.Result == BuildStatusDisplay.BuildResult_Succeeded || buildStatusDisplay.Result == BuildStatusDisplay.BuildResult_PartiallySucceeded)
                    {
                        buildResultClass = "tg-a080";
                    }
                    else if (buildStatusDisplay.Result == BuildStatusDisplay.BuildResult_Failed || buildStatusDisplay.Result == BuildStatusDisplay.BuildResult_Canceled)
                    {
                        buildResultClass = "tg-q9qv";
                    }
                    else if (buildStatusDisplay.Result == BuildStatusDisplay.BuildResult_None)
                    {
                        buildResultClass = "tg-c57o";
                    }
                    else
                    {
                        buildResultClass = "tg-3we0";
                    }

                    formattedTextStringBuilder.AppendLine(string.Format("<td class='tg-3we0'>{0}<//td><td class='tg-3we0'>{1}<//td><td class='tg-3we0'>{2}<//td><td class='tg-3we0'>{3}<//td><td class='tg-3we0'>{4}<//td><td class='tg-3we0'>{5}<//td><td class='tg-3we0'>{6}<//td><td class='{8}'>{7}<//td>",
                                                                        buildStatusDisplay.BuildNumber,
                                                                        buildStatusDisplay.BuildDefinitionName,
                                                                        buildStatusDisplay.QueueTime,
                                                                        buildStatusDisplay.StartTime,
                                                                        buildStatusDisplay.FinishTime,
                                                                        buildStatusDisplay.BuildDuration,
                                                                        buildStatusDisplay.Status,
                                                                        buildStatusDisplay.Result,
                                                                        buildResultClass));
                    formattedTextStringBuilder.AppendLine("</tr>");
                }

                formattedTextStringBuilder.AppendLine("</table>");
                formattedTextStringBuilder.AppendLine("</html>");

                result = formattedTextStringBuilder.ToString();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

            return result;

        }

        /// <summary>
        /// Toggle auto-refresh build status.
        /// </summary>
        /// <param name="toggle"></param>
        protected void ToggleAutoRefreshBuildStatus(bool toggle)
        {
            try
            {
                int interval = 0;
                interval = (int)this.RefreshInMinutesNumericUpDown.Value;

                if (toggle == true)
                {
                    this.RefreshInMinutesNumericUpDown.Enabled = true;
                    this.StopRefreshInMinutesNumericUpDown.Enabled = true;
                    this.AutoRefreshBuildStatusTimer.Enabled = true;
                    this.AutoRefreshBuildStatusStopWatch.Restart();
                    this.AutoRefreshBuildStatusIntervalStopWatch.Restart();
                    this.UpdateControlBarStatus();
                    this.AutoRefreshBuildStatusTimer.Interval = (int)TimeSpan.FromMinutes(interval).TotalMilliseconds;
                    this.DateFilterComboBox.SelectedItem = DateFilter.Recent;
                }
                else
                {
                    this.UpdateControlBarStatus();
                    this.RefreshInMinutesNumericUpDown.Enabled = false;
                    this.StopRefreshInMinutesNumericUpDown.Enabled = false;
                    this.AutoRefreshBuildStatusTimer.Enabled = false;
                    this.AutoRefreshBuildStatusStopWatch.Stop();
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Update control bar status.
        /// </summary>
        protected void UpdateControlBarStatus()
        {
            TimeSpan autoRefreshBuildStatusElapsedTimeSpan;
            TimeSpan nextAutoRefreshTimeSpan;

            int refreshInMinutes = 0;
            try
            {
                autoRefreshBuildStatusElapsedTimeSpan = this.AutoRefreshBuildStatusStopWatch.Elapsed;
                refreshInMinutes = (int)this.RefreshInMinutesNumericUpDown.Value;

                nextAutoRefreshTimeSpan = TimeSpan.FromMinutes(refreshInMinutes).Subtract(TimeSpan.FromMilliseconds(this.AutoRefreshBuildStatusIntervalStopWatch.ElapsedMilliseconds));

                switch (this.AutoRefreshState)
                {
                    case AutoRefreshStateValue.Initialized:
                        this.ControlBarStatusLabel.Text = string.Format(BackgroundTaskStatusAutoRefreshBuildStatusInitialized);
                        break;
                    case AutoRefreshStateValue.StartedByUser:
                        this.ControlBarStatusLabel.Text = string.Format(BackgroundTaskStatusAutoRefreshBuildStatusEnabled,
                                                                        autoRefreshBuildStatusElapsedTimeSpan.Hours,
                                                                        autoRefreshBuildStatusElapsedTimeSpan.Minutes,
                                                                        autoRefreshBuildStatusElapsedTimeSpan.Seconds,
                                                                        nextAutoRefreshTimeSpan.Minutes,
                                                                        nextAutoRefreshTimeSpan.Seconds);
                        break;
                    case AutoRefreshStateValue.StoppedByUser:
                        this.ControlBarStatusLabel.Text = string.Format(BackgroundTaskStatusAutoRefreshBuildStatusStoppedByUser,
                                                                        autoRefreshBuildStatusElapsedTimeSpan.Hours,
                                                                        autoRefreshBuildStatusElapsedTimeSpan.Minutes,
                                                                        autoRefreshBuildStatusElapsedTimeSpan.Seconds);
                        break;
                    case AutoRefreshStateValue.StoppedAutomatically:
                        this.ControlBarStatusLabel.Text = string.Format(BackgroundTaskStatusAutoRefreshBuildStatusStoppedAutomatically,
                                                                        autoRefreshBuildStatusElapsedTimeSpan.Hours,
                                                                        autoRefreshBuildStatusElapsedTimeSpan.Minutes,
                                                                        autoRefreshBuildStatusElapsedTimeSpan.Seconds);
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve selected build count.
        /// </summary>
        /// <returns></returns>
        protected int GetSelectedBuildCount()
        {
            int result = 0;

            try
            {
                //Retrieve selected build count.
                foreach (DataGridViewRow dataGridViewRow in this.BuildStatusDataGridView.SelectedRows)
                {
                    ++result;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Confirm operation on large number of builds.
        /// </summary>
        /// <param name="selectedBuildCount"></param>
        /// <returns></returns>
        protected bool ConfirmOperationLargeBuildCount(int selectedBuildCount)
        {
            bool result = false;
            DialogResult promptDialogResult;

            promptDialogResult = MessageBox.Show(string.Format(ConfirmOperation_LargeBuildCount_Message, selectedBuildCount),
                                                 string.Format(ConfirmOperation_LargeBuildCount_Caption),
                                                 MessageBoxButtons.YesNoCancel,
                                                 System.Windows.Forms.MessageBoxIcon.Exclamation);
            if (promptDialogResult == DialogResult.Yes)
            {
                result = true;
            }

            return result;

        }

        /// <summary>
        /// Calculate total builds duration - builds separate.
        /// </summary>
        /// <returns></returns>
        protected TimeSpan CalcTotalBuildsDurationBuildsSeparate(List<Microsoft.TeamFoundation.Build.WebApi.Build> buildList)
        {
            TimeSpan result;
            List<TimeSpan> buildDurationList = null;
            TimeSpan buildDuration;

            try
            {

                buildDurationList = new List<TimeSpan>();

                foreach (Microsoft.TeamFoundation.Build.WebApi.Build build in buildList)
                {

                    if (build.StartTime.HasValue == true && build.FinishTime.HasValue == true)
                    {
                        buildDuration = build.FinishTime.Value - build.StartTime.Value;
                    }
                    else if (build.StartTime.HasValue == true && build.FinishTime.HasValue == false)
                    {
                        buildDuration = DateTime.Now - build.StartTime.Value.ToLocalTime();
                    }
                    else
                    {
                        buildDuration = new TimeSpan();
                    }

                    buildDurationList.Add(buildDuration);
                }

                result = new TimeSpan(buildDurationList.Sum(r => r.Ticks));
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Calculate total builds duration - builds overlapping combined.
        /// </summary>
        protected TimeSpan CalcTotalBuildsDurationBuildsOverlappingCombined(List<Microsoft.TeamFoundation.Build.WebApi.Build> buildList)
        {
            TimeSpan result;
            TimePeriodCollection periods = null;
            TimePeriodCombiner<TimeRange> periodCombiner = null;
            ITimePeriodCollection combinedPeriods = null;
            DateTime startTime = DateTime.MinValue;
            DateTime finishTime = DateTime.MinValue;

            try
            {

                if (buildList.Count == 0)
                {
                    result = TimeSpan.MinValue;
                    return result;
                }

                periods = new TimePeriodCollection();

                foreach (Microsoft.TeamFoundation.Build.WebApi.Build build in buildList)
                {
                    if (build.StartTime == null)
                    {
                        continue;
                    }
                    startTime = build.StartTime.Value.ToLocalTime();

                    if (build.FinishTime == null)
                    {
                        finishTime = DateTime.Now;
                    }
                    else
                    {
                        finishTime = build.FinishTime.Value.ToLocalTime();
                    }

                    periods.Add(new TimeRange(startTime, finishTime));
                }

                if (periods.Count == 0)
                {
                    result = TimeSpan.MinValue;
                    return result;
                }

                periodCombiner = new TimePeriodCombiner<TimeRange>();
                combinedPeriods = periodCombiner.CombinePeriods(periods);

                result = new TimeSpan(combinedPeriods.Sum(r => r.Duration.Ticks));
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Calculate total builds - clock time builds.
        /// </summary>
        /// <returns></returns>
        protected TimeSpan CalcTotalBuildsClockTimeBuilds(List<Microsoft.TeamFoundation.Build.WebApi.Build> buildList)
        {
            TimeSpan result;
            DateTime earliestStartTime = DateTime.MaxValue;
            DateTime latestFinishTime = DateTime.MinValue;
            DateTime startTime = DateTime.MinValue;
            DateTime finishTime = DateTime.MinValue;

            try
            {
                startTime = DateTime.Now;
                finishTime = DateTime.Now;

                if (buildList.Count == 0)
                {
                    result = TimeSpan.MinValue;
                    return result;
                }

                foreach (Microsoft.TeamFoundation.Build.WebApi.Build build in buildList)
                {
                    //Invalid start time.
                    if (build.StartTime == null)
                    {
                        continue;
                    }

                    startTime = build.StartTime.Value;

                    //Invalid finish time.
                    if (build.FinishTime != null)
                    {
                        finishTime = build.FinishTime.Value;
                    }
                    else
                    {
                        finishTime = DateTime.Now;
                    }

                    if (startTime.ToLocalTime() <= earliestStartTime)
                    {
                        earliestStartTime = startTime.ToLocalTime();
                    }

                    if (finishTime.ToLocalTime() >= latestFinishTime)
                    {
                        latestFinishTime = finishTime.ToLocalTime();
                    }
                }

                if (earliestStartTime == DateTime.MaxValue ||
                    latestFinishTime == DateTime.MinValue)
                {
                    result = TimeSpan.MinValue;
                }
                else
                {
                    result = latestFinishTime.Subtract(earliestStartTime);
                }



            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Update build totals.
        /// </summary>
        /// <param name="totalBuildDurationTimeSpan"></param>
        /// <param name="totalClockTimeTimeSpan"></param>
        protected void UpdateBuildTotals(TimeSpan totalBuildDurationTimeSpan, TimeSpan totalClockTimeTimeSpan)
        {

            try
            {
                if (totalBuildDurationTimeSpan != TimeSpan.MinValue)
                {
                    this.BuildListTotalDurationValueLabel.Text = string.Format("{0}",
                                                                               totalBuildDurationTimeSpan.ToString(@"hh\:mm\:ss"));
                }
                else
                {
                    this.BuildListTotalDurationValueLabel.Text = NotAvailable;
                }

                if (totalClockTimeTimeSpan != TimeSpan.MinValue)
                {
                    this.BuildListTotalClockTimeValueLabel.Text = string.Format("{0:D2} {1:D2}:{2:D2}:{3:D2}",
                                                                               totalClockTimeTimeSpan.Days,
                                                                               totalClockTimeTimeSpan.Hours,
                                                                               totalClockTimeTimeSpan.Minutes,
                                                                               totalClockTimeTimeSpan.Seconds);
                }
                else
                {
                    this.BuildListTotalClockTimeValueLabel.Text = NotAvailable;
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Map build status data grid view columns.
        /// </summary>
        protected void MapBuildStatusDataGridViewColumns()
        {
            int columnWidth;

            try
            {
                foreach (DataGridViewColumn column in this.BuildStatusDataGridView.Columns)
                {

                    switch (column.Name)
                    {

                        case BuildStatusDataGridView_ReasonColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            column.DataPropertyName = BuildStatusDataGridView_Reason;
                            break;
                        case BuildStatusDataGridView_ResultColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            column.DataPropertyName = BuildStatusDataGridView_Result;
                            break;
                        case BuildStatusDataGridView_BuildNumberColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            column.DataPropertyName = BuildStatusDataGridView_BuildNumber;
                            break;
                        case BuildStatusDataGridView_BuildDefinitionColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            column.DataPropertyName = BuildStatusDataGridView_BuildDefinition;
                            break;
                        case BuildStatusDataGridView_QueueTimeColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            column.DataPropertyName = BuildStatusDataGridView_QueueTime;
                            break;
                        case BuildStatusDataGridView_StartTimeColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            column.DataPropertyName = BuildStatusDataGridView_StartTime;
                            break;
                        case BuildStatusDataGridView_FinishTimeColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            column.DataPropertyName = BuildStatusDataGridView_FinishTime;
                            break;
                        case BuildStatusDataGriveView_BuildDurationColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            column.DataPropertyName = BuildStatusDataGriveView_BuildDuration;
                            break;
                        case BuildStatusDataGridView_StatusColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            column.DataPropertyName = BuildStatusDataGridView_Status;
                            break;
                        case BuildStatusDataGridView_RequestedForColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            column.DataPropertyName = BuildStatusDataGridView_RequestedFor;
                            break;
                        case BuildStatusDataGridView_QueueNameColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            column.DataPropertyName = BuildStatusDataGridView_QueueName;
                            break;
                        case BuildStatusDataGridView_ExtraColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            break;

                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Refresh button - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshButton_Click(object sender, EventArgs e)
        {

            try
            {
                this.RefreshBuildList();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Build status data grid view - cell formatting.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildStatusDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            try
            {

                if (BuildStatusDataGridView.Columns[e.ColumnIndex].Name == BuildStatusDataGridView_ReasonColumn && e.RowIndex >= 0 && e.RowIndex < BuildStatusDataGridView.Rows.Count)
                {
                    if (BuildStatusDataGridView.Rows[e.RowIndex].DataBoundItem != null)
                    {
                        BuildStatusDisplay buildStatusDisplay = (BuildStatusDisplay)BuildStatusDataGridView.Rows[e.RowIndex].DataBoundItem;

                        switch (buildStatusDisplay.Reason)
                        {
                            case BuildStatusDisplay.BuildReason_None:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildReasonNone;
                                break;
                            case BuildStatusDisplay.BuildReason_Manual:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildReasonManual;
                                break;
                            case BuildStatusDisplay.BuildReason_IndividualCI:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildReasonContinuousIntegration;
                                break;
                            case BuildStatusDisplay.BuildReason_BatchedCI:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildReasonContinuousIntegration;
                                break;
                            case BuildStatusDisplay.BuildReason_Schedule:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildReasonScheduled;
                                break;
                            case BuildStatusDisplay.BuildReason_UserCreated:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildReasonManual;
                                break;
                            case BuildStatusDisplay.BuildReason_ValidateShelveset:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildReasonManual;
                                break;
                            case BuildStatusDisplay.BuildReason_CheckInShelveset:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildReasonManual;
                                break;
                            case BuildStatusDisplay.BuildReason_Triggered:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildReasonScheduled;
                                break;
                            case BuildStatusDisplay.BuildReason_All:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildReasonNone;
                                break;
                            default:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildReasonNone;
                                break;
                        }

                    }
                }

                if (BuildStatusDataGridView.Columns[e.ColumnIndex].Name == BuildStatusDataGridView_ResultColumn && e.RowIndex >= 0 && e.RowIndex < BuildStatusDataGridView.Rows.Count)
                {
                    if (BuildStatusDataGridView.Rows[e.RowIndex].DataBoundItem != null)
                    {
                        BuildStatusDisplay buildStatusDisplay = (BuildStatusDisplay)BuildStatusDataGridView.Rows[e.RowIndex].DataBoundItem;

                        switch (buildStatusDisplay.Status)
                        {
                            case BuildStatusDisplay.BuildStatus_None:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildReasonNone;
                                break;
                            case BuildStatusDisplay.BuildStatus_InProgress:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildStatusInProgress;
                                break;
                            case BuildStatusDisplay.BuildStatus_Completed:
                                switch (buildStatusDisplay.Result)
                                {
                                    case BuildStatusDisplay.BuildResult_Succeeded:
                                        e.Value = (System.Drawing.Image)Properties.Resources.BuildStatusSucceeded;
                                        break;
                                    case BuildStatusDisplay.BuildResult_PartiallySucceeded:
                                        e.Value = (System.Drawing.Image)Properties.Resources.BuildStatusPartial;
                                        break;
                                    case BuildStatusDisplay.BuildResult_Failed:
                                        e.Value = (System.Drawing.Image)Properties.Resources.BuildStatusFailed;
                                        break;
                                    case BuildStatusDisplay.BuildResult_Canceled:
                                        e.Value = (System.Drawing.Image)Properties.Resources.BuildStatusCancelled;
                                        break;
                                    default:
                                        e.Value = (System.Drawing.Image)Properties.Resources.BuildReasonNone;
                                        break;
                                }
                                break;
                            case BuildStatusDisplay.BuildStatus_Cancelling:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildStatusCancelled;
                                break;
                            case BuildStatusDisplay.BuildStatus_Postponed:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildStatusNotStarted;
                                break;
                            case BuildStatusDisplay.BuildStatus_NotStarted:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildStatusNotStarted;
                                break;
                            default:
                                e.Value = (System.Drawing.Image)Properties.Resources.BuildStatusNone;
                                break;
                        }
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Swap build status view button - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SwapBuildStatusViewButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.BuildStatusPanel.Visible == true)
                {
                    this.BuildStatusPanel.Visible = false;
                    this.MissingBuildStatusPanel.Visible = true;
                }
                else
                {
                    this.BuildStatusPanel.Visible = true;
                    this.MissingBuildStatusPanel.Visible = false;
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Build status data grid view - Selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildStatusDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            BuildStatusDisplay buildStatusDisplay = null;
            Microsoft.TeamFoundation.Build.WebApi.Build build = null;
            List<Microsoft.TeamFoundation.Build.WebApi.Build> buildList = null;
            TimeSpan totalBuildDurationTimeSpan;
            TimeSpan totalClockTimeTimeSpan;

            try
            {

                buildList = new List<Microsoft.TeamFoundation.Build.WebApi.Build>();

                foreach (DataGridViewRow dataGridViewRow in this.BuildStatusDataGridView.SelectedRows)
                {
                    buildStatusDisplay = (BuildStatusDisplay)dataGridViewRow.DataBoundItem;
                    build = this.BuildList.Where(b => b.Id == buildStatusDisplay.Id).Single();
                    buildList.Add(build);
                }

                if (buildList.Count == 0)
                {
                    buildList = this.BuildList;
                }

                //Calculate build totals.
                totalBuildDurationTimeSpan = this.CalcTotalBuildsDurationBuildsOverlappingCombined(buildList);
                totalClockTimeTimeSpan = this.CalcTotalBuildsClockTimeBuilds(buildList);

                //Update build totals.
                this.UpdateBuildTotals(totalBuildDurationTimeSpan, totalClockTimeTimeSpan);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Copy build definition tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyBuildDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuildStatusDisplay buildStatusDisplay = null;
            StringBuilder buildDefinitionListStringBuilder = null;

            try
            {
                buildDefinitionListStringBuilder = new StringBuilder();

                foreach (DataGridViewRow dataGridViewRow in this.BuildStatusDataGridView.SelectedRows)
                {
                    buildStatusDisplay = (BuildStatusDisplay)dataGridViewRow.DataBoundItem;
                    buildDefinitionListStringBuilder.AppendLine(string.Format("{0}",
                                                                                  buildStatusDisplay.BuildDefinitionName));


                }

                Clipboard.SetDataObject(buildDefinitionListStringBuilder.ToString());

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: View build tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewBuildToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuildStatusDisplay buildStatusDisplay = null;
            ProcessStartInfo processStartInfo = null;
            string buildSummaryAddress = string.Empty;
            string teamProjectCollectionUri = string.Empty;
            int selectedBuildCount = 0;

            try
            {
                teamProjectCollectionUri = this.BuildStatusCheckPresenter.TeamProjectInfo.TeamProjectCollectionUri.ToString();
                if (teamProjectCollectionUri.EndsWith("/") == false)
                {
                    teamProjectCollectionUri += @"/";
                }

                //Retrieve selected build count.
                selectedBuildCount = GetSelectedBuildCount();

                //Prompt user to confirm requested operation when selected build count is large.
                if (selectedBuildCount > ConfirmOperation_MaxBuildCount)
                {
                    if (ConfirmOperationLargeBuildCount(selectedBuildCount) == false)
                    {
                        return;
                    }
                }

                foreach (DataGridViewRow dataGridViewRow in this.BuildStatusDataGridView.SelectedRows)
                {
                    buildStatusDisplay = (BuildStatusDisplay)dataGridViewRow.DataBoundItem;

                    buildSummaryAddress = string.Format("{0}DefaultCollection/{1}/_build?buildId={2}&_a=summary",
                                                        teamProjectCollectionUri,
                                                        this.BuildStatusCheckPresenter.TeamProjectInfo.TeamProjectName,
                                                        buildStatusDisplay.Id);

                    processStartInfo = new ProcessStartInfo(buildSummaryAddress);
                    Process.Start(processStartInfo);
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: View build definition tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewBuildDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuildStatusDisplay buildStatusDisplay = null;
            ProcessStartInfo processStartInfo = null;
            string buildSummaryAddress = string.Empty;
            string teamProjectCollectionUri = string.Empty;
            int selectedBuildCount = 0;

            try
            {
                teamProjectCollectionUri = this.BuildStatusCheckPresenter.TeamProjectInfo.TeamProjectCollectionUri.ToString();
                if (teamProjectCollectionUri.EndsWith("/") == false)
                {
                    teamProjectCollectionUri += @"/";
                }

                //Retrieve selected build count.
                selectedBuildCount = GetSelectedBuildCount();

                //Prompt user to confirm requested operation when selected build count is large.
                if (selectedBuildCount > ConfirmOperation_MaxBuildCount)
                {
                    if (ConfirmOperationLargeBuildCount(selectedBuildCount) == false)
                    {
                        return;
                    }
                }

                foreach (DataGridViewRow dataGridViewRow in this.BuildStatusDataGridView.SelectedRows)
                {
                    buildStatusDisplay = (BuildStatusDisplay)dataGridViewRow.DataBoundItem;

                    buildSummaryAddress = string.Format("{0}DefaultCollection/{1}/_build?_a=completed&definitionId={2}",
                                                        teamProjectCollectionUri,
                                                        this.BuildStatusCheckPresenter.TeamProjectInfo.TeamProjectName,
                                                        buildStatusDisplay.DefinitionReferenceID);

                    processStartInfo = new ProcessStartInfo(buildSummaryAddress);
                    Process.Start(processStartInfo);
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }


        /// <summary>
        /// Event Handler: Request build tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestBuildToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuildStatusDisplay buildStatusDisplay = null;
            List<string> buildDefinitionNameList = null;

            try
            {
                buildDefinitionNameList = new List<string>();

                foreach (DataGridViewRow dataGridViewRow in this.BuildStatusDataGridView.SelectedRows)
                {
                    buildStatusDisplay = (BuildStatusDisplay)dataGridViewRow.DataBoundItem;
                    buildDefinitionNameList.Add(buildStatusDisplay.BuildDefinitionName);
                }

                this.SidekickCoaction.RequestBuild(buildDefinitionNameList);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Copy missing build status tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyMissingBuildStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelector buildDefinitionSelector = null;

            try
            {
                if (this.MissingBuildStatusBuildDefinitionListBox.SelectedItem is BuildDefinitionSelector)
                {
                    buildDefinitionSelector = (BuildDefinitionSelector)this.MissingBuildStatusBuildDefinitionListBox.SelectedItem;

                    Clipboard.SetDataObject(buildDefinitionSelector.Name);
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Copy all missing build status tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyAllMissingBuildStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelector buildDefinitionSelector = null;
            string buildDefinitionNameList = string.Empty;

            try
            {
                foreach (var item in this.MissingBuildStatusBuildDefinitionListBox.Items)
                {
                    if (item is BuildDefinitionSelector)
                    {
                        buildDefinitionSelector = (BuildDefinitionSelector)item;
                        buildDefinitionNameList += buildDefinitionSelector.Name + Environment.NewLine;
                    }
                }

                Clipboard.SetDataObject(buildDefinitionNameList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: View missing build status tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewMissingBuildStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelector buildDefinitionSelector = null;
            ProcessStartInfo processStartInfo = null;
            string buildSummaryAddress = string.Empty;
            string teamProjectCollectionUri = string.Empty;

            try
            {
                if (this.MissingBuildStatusBuildDefinitionListBox.SelectedItem is BuildDefinitionSelector)
                {
                    teamProjectCollectionUri = this.BuildStatusCheckPresenter.TeamProjectInfo.TeamProjectCollectionUri.ToString();
                    if (teamProjectCollectionUri.EndsWith("/") == false)
                    {
                        teamProjectCollectionUri += @"/";
                    }

                    buildDefinitionSelector = (BuildDefinitionSelector)this.MissingBuildStatusBuildDefinitionListBox.SelectedItem;

                    buildSummaryAddress = string.Format("{0}DefaultCollection/{1}/_build?_a=completed&definitionId={2}",
                                                        teamProjectCollectionUri,
                                                        this.BuildStatusCheckPresenter.TeamProjectInfo.TeamProjectName,
                                                        buildDefinitionSelector.Id);
                    processStartInfo = new ProcessStartInfo(buildSummaryAddress);
                    Process.Start(processStartInfo);
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Missing build status context menu strip - Opening.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MissingBuildStatusContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.MissingBuildStatusBuildDefinitionListBox.SelectedItem == null)
                {
                    this.CopyMissingBuildStatusToolStripMenuItem.Enabled = false;
                    this.ViewMissingBuildStatusToolStripMenuItem.Enabled = false;
                }
                else
                {
                    this.CopyMissingBuildStatusToolStripMenuItem.Enabled = true;
                    this.ViewMissingBuildStatusToolStripMenuItem.Enabled = true;
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Status Update Timer - Tick.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void StatusUpdateTimer_Tick(object sender, EventArgs e)
        {

            try
            {
                this.UpdateControlBarStatus();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Auto Refresh Build Status Timer - Tick.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoRefreshBuildStatusTimer_Tick(object sender, EventArgs e)
        {

            TimeSpan autoRefreshBuildStatusElapsedTimeSpan;
            int maxAutoRefreshBuildStatusDuration = MaxAutoRefreshBuildStatusDuration;

            try
            {
                this.AutoRefreshBuildStatusIntervalStopWatch.Stop();
                autoRefreshBuildStatusElapsedTimeSpan = this.AutoRefreshBuildStatusStopWatch.Elapsed;
                maxAutoRefreshBuildStatusDuration = (int)this.StopRefreshInMinutesNumericUpDown.Value;

                //Ensure auto-refresh before maximum auto-refresh build status duration.
                if (maxAutoRefreshBuildStatusDuration > MaxAutoRefreshBuildStatusDuration)
                {
                    maxAutoRefreshBuildStatusDuration = MaxAutoRefreshBuildStatusDuration;
                }

                if (autoRefreshBuildStatusElapsedTimeSpan.TotalMinutes >= maxAutoRefreshBuildStatusDuration)
                {
                    this.AutoRefreshState = AutoRefreshStateValue.StoppedAutomatically;
                    AutoRefreshBuildStatusCheckBox.Checked = false;
                }
                else
                {
                    RefreshBuildStatus();
                }
                this.AutoRefreshBuildStatusIntervalStopWatch.Restart();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Auto refresh build status checkbox - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoRefreshBuildStatusCheckBox_CheckedChanged(object sender, EventArgs e)
        {

            try
            {

                if (this.AutoRefreshBuildStatusCheckBox.Checked == true)
                {
                    AutoRefreshState = AutoRefreshStateValue.StartedByUser;
                    ToggleAutoRefreshBuildStatus(true);
                    RefreshBuildStatus();
                }
                else
                {
                    if (AutoRefreshState != AutoRefreshStateValue.StoppedAutomatically)
                    {
                        AutoRefreshState = AutoRefreshStateValue.StoppedByUser;
                    }
                    ToggleAutoRefreshBuildStatus(false);
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Refresh in minutes numeric up down - value changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshInMinutesNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            int interval = 0;

            try
            {
                interval = (int)this.RefreshInMinutesNumericUpDown.Value;

                if ((int)this.RefreshInMinutesNumericUpDown.Value >= (int)this.StopRefreshInMinutesNumericUpDown.Value)
                {
                    this.RefreshInMinutesNumericUpDown.Value = (int)this.StopRefreshInMinutesNumericUpDown.Value - 1;
                    if ((int)this.RefreshInMinutesNumericUpDown.Value == interval)
                    {
                        return;
                    }
                }

                this.AutoRefreshBuildStatusTimer.Interval = (int)TimeSpan.FromMinutes(interval).TotalMilliseconds;
                this.AutoRefreshBuildStatusStopWatch.Restart();
                this.AutoRefreshBuildStatusIntervalStopWatch.Restart();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Stop refresh in minutes numeric up down - value changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopRefreshInMinutesNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            int interval = 0;

            try
            {
                if ((int)this.StopRefreshInMinutesNumericUpDown.Value <= (int)this.RefreshInMinutesNumericUpDown.Value)
                {
                    this.StopRefreshInMinutesNumericUpDown.Value = (int)this.RefreshInMinutesNumericUpDown.Value + 1;
                    if ((int)this.StopRefreshInMinutesNumericUpDown.Value == interval)
                    {
                        return;
                    }

                    this.AutoRefreshBuildStatusStopWatch.Restart();
                    this.AutoRefreshBuildStatusIntervalStopWatch.Restart();
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }
        /// <summary>
        /// Event Handler: Date filter combo box - selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DateFilterComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateFilter dateFilter;

            try
            {
                dateFilter = (DateFilter)this.DateFilterComboBox.SelectedItem;
                if (dateFilter == DateFilter.No_Filter)
                {
                    this.TopNumericUpDown.Value = 1;
                }
                if (dateFilter == DateFilter.Recent)
                {
                    RecentlyCompletedInHoursNumericUpDown.Enabled = true;
                }
                else
                {
                    RecentlyCompletedInHoursNumericUpDown.Enabled = false;
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Copy button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyButton_Click(object sender, EventArgs e)
        {
            string buildStatusFormattedAsText = string.Empty;
            try
            {
                buildStatusFormattedAsText = FormatBuildStatusDataGridViewAsText();

                Clipboard.SetDataObject(buildStatusFormattedAsText);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Build status data grid view - Mouse down.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildStatusDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hitTestInfo = null;

            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    if (BuildStatusDataGridView.SelectedRows.Count <= 1)
                    {
                        hitTestInfo = BuildStatusDataGridView.HitTest(e.X, e.Y);
                        if (hitTestInfo.RowIndex > -1)
                        {
                            BuildStatusDataGridView.ClearSelection();
                            BuildStatusDataGridView.Rows[hitTestInfo.RowIndex].Selected = true;
                        }
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Copy as text tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyAsTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string buildStatusFormattedAsText = string.Empty;
            try
            {
                buildStatusFormattedAsText = FormatBuildStatusDataGridViewAsText();

                Clipboard.SetDataObject(buildStatusFormattedAsText);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Copy as HTML tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyAsHTMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string buildStatusFormattedAsText = string.Empty;
            string buildStatusFormattedAsHtml = string.Empty;

            try
            {
                buildStatusFormattedAsText = FormatBuildStatusDataGridViewAsText();
                buildStatusFormattedAsHtml = FormatBuildStatusDataGridViewAsHtml();

                ClipboardHelper.CopyToClipboard(buildStatusFormattedAsHtml, buildStatusFormattedAsText);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickCheckBuildStatus.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Recalculate button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RecalculateButton_Click(object sender, EventArgs e)
        {
            TimeSpan totalBuildDurationTimeSpan;
            TimeSpan totalClockTimeTimeSpan;

            try
            {
                //Calculate build totals.
                totalBuildDurationTimeSpan = this.CalcTotalBuildsDurationBuildsOverlappingCombined(this.BuildList);
                totalClockTimeTimeSpan = this.CalcTotalBuildsClockTimeBuilds(this.BuildList);

                //Update build totals.
                this.UpdateBuildTotals(totalBuildDurationTimeSpan, totalClockTimeTimeSpan);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Queued build definitions check box - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QueuedBuildDefinitionsCheckBox_CheckedChanged(object sender, EventArgs e)
        {

            try
            {
                if (this.QueuedBuildDefinitions == true)
                {
                    this.EnableControlsDependantOnViewQueuedBuildDefinition(true);
                    this.MissingBuildStatusBuildDefinitionListBox.Items.Clear();
                    this.BuildStatusPanel.Visible = true;
                    this.MissingBuildStatusPanel.Visible = false;
                    this.RefreshBuildList();
                }
                else
                {
                    if (this.OneOrMoreBuildDefinitionsSelected == true)
                    {
                        this.EnableControlsDependantOnSelectedBuildDefinition(true);
                    }
                    else
                    {
                        this.EnableControlsDependantOnSelectedBuildDefinition(false);
                    }

                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

    }

}

