﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Aclara.VsoBuild.Sidekicks.WinForm.Types;
using Aclara.Sidekicks.AutoUpdater;
using System.IO;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class InfoForm : Form
    {
        #region Private Constants

        private const string Status_SidekicksAutoUpdater_NotFound = "Warning: Installation package location is not accessible.";
        private const string Status_SidekicksAutoUpdater_Location = "Updater location: {0}";
        private const string Status_SidekicksAutoUpdater_Info = "Updater executable name: {0}, Version: {1}";

        #endregion

        #region Private Data Members

        private Information _information;
        private SidekicksAutoUpdater _sidekicksAutoUpdater;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Information.
        /// </summary>
        public Information Information
        {
            get { return _information; }
            set { _information = value; }
        }

        /// <summary>
        /// Property: Sidekicks auto-updater.
        /// </summary>
        public SidekicksAutoUpdater SidekicksAutoUpdater
        {
            get { return _sidekicksAutoUpdater; }
            set { _sidekicksAutoUpdater = value; }
        }

        #endregion


        #region Public Contructors

        public InfoForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Close button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Info form - Shown.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InfoForm_Shown(object sender, EventArgs e)
        {
            try
            {
                switch (this.Information)
                {
                    case Information.Unspecified:
                        break;
                    case Information.AutoUpdater:

                        this.SidekicksAutoUpdaterInfoPanel.Visible = true;
                        this.SidekicksAutoUpdaterStatusLabel.Text = string.Empty;
                        this.SidekicksAutoUpdaterWarningPictureBox.Visible = false;

                        this.SidekicksAutoUpdaterLocationLabel.Text = string.Format(Status_SidekicksAutoUpdater_Location,
                                                                                                            this.SidekicksAutoUpdater.BootstrapperExePath);
                        this.SidekicksAutoUpdaterInfoLabel.Text = string.Format(Status_SidekicksAutoUpdater_Info,
                                                                                this.SidekicksAutoUpdater.BootstrapperExeName,
                                                                                this.SidekicksAutoUpdater.BootstrapperExeFileVersion);
                        if (Directory.Exists(this.SidekicksAutoUpdater.BootstrapperExePath) == false)
                        {
                            this.SidekicksAutoUpdaterWarningPictureBox.Visible = true;
                            this.SidekicksAutoUpdaterStatusLabel.Text = Status_SidekicksAutoUpdater_NotFound;
                            this.SidekicksAutoUpdaterOpenFolderButton.Enabled = false;
                        }

                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Event Handler: Sidekicks auto-updater copy to cliboard button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SidekicksAutoUpdaterCopyToClipboardButton_Click(object sender, EventArgs e)
        {
            string clipboardText = string.Empty;

            try
            {
                clipboardText = this.SidekicksAutoUpdater.BootstrapperExePath;
                Clipboard.SetDataObject(clipboardText);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Sidekicks auto-updater open folder button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SidekicksAutoUpdaterOpenFolderButton_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(this.SidekicksAutoUpdater.BootstrapperExePath);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
