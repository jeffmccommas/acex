﻿using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using NLog;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class LogForm : Form, ILogView
    {

        #region Private Constants

        private const string LogEventInfoDataGridView_TimeStampColumn = "TimeStampColumn";
        private const string LogEventInfoDataGridView_SeverityColumn = "SeverityColumn";
        private const string LogEventInfoDataGridView_SeverityIconColumn = "SeverityIconColumn";
        private const string LogEventInfoDataGridView_MessageColumn = "MessageColumn";
        private const string LogEventInfoDataGridView_SidekickDescriptionColumn = "SidekickDescriptionColumn";
        private const string LogEventInfoDataGridView_ExtraColumn = "ExtraColumn";

        private const string LogEventInfoDataGridView_TimeStamp = "TimeStamp";
        private const string LogEventInfoDataGridView_Severity = "Severity";
        private const string LogEventInfoDataGridView_Message = "Message";
        private const string LogEventInfoDataGridView_SidekickDescription = "SidekickDescription";
        private const string LogEventInfoDataGridView_Extra = "Extra";

        private const string LogEventInfoLevel_Fatal = "Fatal";
        private const string LogEventInfoLevel_Error = "Error";
        private const string LogEventInfoLevel_Warn = "Warn";
        private const string LogEventInfoLevel_Info = "Info";
        private const string LogEventInfoLevel_Debug = "Debug";
        private const string LogEventInfoLevel_Trace = "Trace";

        private const string LogEventInfoDisplayLevel_Error = "Error";
        private const string LogEventInfoDisplayLevel_Warn = "Warning";
        private const string LogEventInfoDisplayLevel_Info = "Info";
        private const string LogEventInfoDisplayLevel_Unexpected = "Unexpected";

        private const string DisplayErrorsCheckboxText = "Errors";
        private const string DisplayWarningsCheckboxText = "Warnings";
        private const string DisplayInfoCheckboxText = "Informational";

        private const string ShowLogFileErrorMessage_CouldNotLocateLogFile = "Could not locate log file.";

        private const string SidekickFilter_NoSidekickFilter = "No sidekick filter";

        #endregion

        #region Private Data Members

        private LogPresenter _logPresenter;
        private List<LogEventInfoDisplay> _logEventInfoDisplayList;
        private BindingSource _logEventInfoListBindingSource;
        private SidekickContainerForm _sidekickContainerForm;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: 
        /// </summary>
        public LogPresenter LogPresenter
        {
            get { return _logPresenter; }
            set { _logPresenter = value; }
        }

        /// <summary>
        /// Property: Log event info list.
        /// </summary>
        public List<LogEventInfoDisplay> LogEventInfoDisplayList
        {
            get { return _logEventInfoDisplayList; }
            set { _logEventInfoDisplayList = value; }
        }

        /// <summary>
        /// Property: Log event info list binding source.
        /// </summary>
        public BindingSource LogEventInfoListBindingSource
        {
            get { return _logEventInfoListBindingSource; }
            set { _logEventInfoListBindingSource = value; }
        }

        /// <summary>
        /// Property: Sidekick container form.
        /// </summary>
        public SidekickContainerForm SidekickContainerForm
        {
            get { return _sidekickContainerForm; }
            set { _sidekickContainerForm = value; }
        }

        #endregion

        #region Public Constructors

        public LogForm(SidekickContainerForm sidekickContainerForm)
        {

            _sidekickContainerForm = sidekickContainerForm;

            InitializeComponent();

            _logEventInfoDisplayList = new List<LogEventInfoDisplay>();
            _logEventInfoListBindingSource = new BindingSource();
            _logEventInfoListBindingSource.DataSource = _logEventInfoDisplayList;
            this.LogEventInfoDataGridView.AutoGenerateColumns = false;

            foreach (DataGridViewColumn column in this.LogEventInfoDataGridView.Columns)
            {

                switch (column.Name)
                {
                    case LogEventInfoDataGridView_TimeStampColumn:
                        column.DataPropertyName = LogEventInfoDataGridView_TimeStamp;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        break;
                    case LogEventInfoDataGridView_SeverityColumn:
                        column.DataPropertyName = LogEventInfoDataGridView_Severity;
                        break;
                    case LogEventInfoDataGridView_MessageColumn:
                        column.DataPropertyName = LogEventInfoDataGridView_Message;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        break;
                    case LogEventInfoDataGridView_SidekickDescriptionColumn:
                        column.DataPropertyName = LogEventInfoDataGridView_SidekickDescription;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        break;
                    case LogEventInfoDataGridView_ExtraColumn:
                        column.DataPropertyName = LogEventInfoDataGridView_Extra;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        break;
                }
            }
            this.LogEventInfoDataGridView.DataSource = _logEventInfoListBindingSource;

            this.DetailsButton.Enabled = false;
            this.DisplayDetailsToolStripMenuItem.Enabled = false;
            this.CopySplitButton.Enabled = false;
            this.CopyButtonSelectionToolStripMenuItem.Enabled = false;
            this.ClearButton.Enabled = false;
            this.SidekickFilterComboBox.Items.Add(SidekickFilter_NoSidekickFilter);
            this.SidekickFilterComboBox.SelectedItem = SidekickFilter_NoSidekickFilter;

            this.SidekickContainerForm.SidekickSelectionChanged += OnSidekickSelectionChanged;
            this.AutosizeSidekickFilterWidth();
        }



        #endregion

        #region Public Methods

        /// <summary>
        /// Event Handler: Log entry created.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="logEntryCreatedEventArgs"></param>
        public void LogEntryCreate(Object sender, CreateLogEntryEventArgs logEntryCreatedEventArgs)
        {

            LogEventInfo logEventInfo = null;
            LogEventInfoDisplay logEventInfoDisplay = null;

            try
            {

                if (this.InvokeRequired == true)
                {
                    this.Invoke(new Action<Object, CreateLogEntryEventArgs>(this.LogEntryCreate), sender, logEntryCreatedEventArgs);
                }
                else
                {

                    if (logEntryCreatedEventArgs == null)
                    {
                        return;
                    }
                    if (logEntryCreatedEventArgs.LogEventInfo == null)
                    {
                        return;
                    }

                    logEventInfo = logEntryCreatedEventArgs.LogEventInfo;

                    logEventInfoDisplay = ConvertToLogEventInfoDisplay(logEventInfo);

                    this.LogEventInfoDisplayList.Add(logEventInfoDisplay);
                    this.LogEventInfoListBindingSource.ResetBindings(false);
                    this.ToggleLogEventInfoDataViewAllRowsVisible();

                    this.CopySplitButton.Enabled = true;
                    this.ClearButton.Enabled = true;

                    this.UpdateDisplayCheckboxText();

                    this.AutoScrollLogEventInfoDataGridView();

                    for (int columnIndex = 0; columnIndex < LogEventInfoDataGridView.Columns.Count; columnIndex++)
                    {
                        int columnWidth = LogEventInfoDataGridView.Columns[columnIndex].Width;
                        LogEventInfoDataGridView.Columns[columnIndex].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                        LogEventInfoDataGridView.Columns[columnIndex].Width = columnWidth;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Autosize sidekick filter width.
        /// </summary>
        protected void AutosizeSidekickFilterWidth()
        {
            const int MimimumDropDownWith = 80;
            const int WidthOffset = 15;
            int itemMaxWidth = 0;
            int currentMaxWidth = 0;
            Label itemLabel = new Label();

            try
            {
                foreach (var itemValue in this.SidekickFilterComboBox.Items)
                {
                    itemLabel.Text = itemValue.ToString();
                    currentMaxWidth = itemLabel.PreferredWidth;
                    if (currentMaxWidth > itemMaxWidth)
                    {
                        itemMaxWidth = currentMaxWidth;
                    }
                }

                if (itemMaxWidth < MimimumDropDownWith)
                {
                    itemMaxWidth = MimimumDropDownWith;
                }

                this.SidekickFilterComboBox.Width = itemMaxWidth + WidthOffset;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                itemLabel.Dispose();
            }

        }

        /// <summary>
        /// Auto scroll log event info data grid view.
        /// </summary>
        protected void AutoScrollLogEventInfoDataGridView()
        {

            if (this.AutoScrollCheckBox.Checked == true)
            {

                for (int rowIndex = this.LogEventInfoDataGridView.Rows.Count; rowIndex-- > 0;)
                {
                    if (this.LogEventInfoDataGridView.Rows[rowIndex].Visible == true)
                    {
                        this.LogEventInfoDataGridView.FirstDisplayedScrollingRowIndex = rowIndex;
                        break;
                    }
                }

            }

        }

        /// <summary>
        /// Convert log event info to log event info display.
        /// </summary>
        /// <param name="logEventInfo"></param>
        /// <returns></returns>
        protected LogEventInfoDisplay ConvertToLogEventInfoDisplay(LogEventInfo logEventInfo)
        {

            LogEventInfoDisplay result = null;

            try
            {
                result = new LogEventInfoDisplay();

                result.TimeStamp = logEventInfo.TimeStamp;
                switch (logEventInfo.Level.Name)
                {
                    case LogEventInfoLevel_Fatal:
                    case LogEventInfoLevel_Error:
                        result.Severity = LogEventInfoDisplayLevel_Error;
                        break;
                    case LogEventInfoLevel_Warn:
                        result.Severity = LogEventInfoDisplayLevel_Warn;
                        break;
                    case LogEventInfoLevel_Info:
                    case LogEventInfoLevel_Debug:
                    case LogEventInfoLevel_Trace:
                        result.Severity = LogEventInfoDisplayLevel_Info;
                        break;

                    default:
                        result.Severity = LogEventInfoDisplayLevel_Unexpected;
                        break;
                }
                result.Message = logEventInfo.Message;
                if (logEventInfo.Exception != null)
                {
                    result.ExceptionAvailable = true;
                    result.Exception = logEventInfo.Exception;
                }
                else
                {
                    result.ExceptionAvailable = false;
                    result.Exception = null;
                }

                if (logEventInfo.Properties != null && logEventInfo.Properties.ContainsKey(Logging.CustomLogger.LogPropertyName_SidekickDescription) == true)
                {
                    result.SidekickDescription = logEventInfo.Properties.Single(p => (string)p.Key == Logging.CustomLogger.LogPropertyName_SidekickDescription).Value.ToString();

                    if (string.IsNullOrEmpty(result.SidekickDescription) == false)
                    {
                        if (this.SidekickFilterComboBox.Items.Contains(result.SidekickDescription) == false)
                        {
                            this.SidekickFilterComboBox.Items.Add(result.SidekickDescription);
                            this.AutosizeSidekickFilterWidth();
                        }
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Toggle log event info data grid view all rows visible.
        /// </summary>
        protected void ToggleLogEventInfoDataViewAllRowsVisible()
        {
            try
            {
                this.LogEventInfoDataGridView.CurrentCell = null;

                foreach (DataGridViewRow dataGridViewRow in this.LogEventInfoDataGridView.Rows)
                {
                    ToggleLogEventInfoDataGridViewRowVisible(dataGridViewRow);
                    this.UpdateDisplayCheckboxText();
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Toggle log event info data grid view row visible.
        /// </summary>
        /// <param name="dataGridViewRow"></param>
        protected void ToggleLogEventInfoDataGridViewRowVisible(DataGridViewRow dataGridViewRow)
        {
            string sidekickDescriptionFilter = string.Empty;
            bool rowVisible = true;

            try
            {
                if (LogEventInfoDataGridView.Rows[dataGridViewRow.Index].DataBoundItem != null)
                {
                    LogEventInfoDisplay logEventInfoDisplay = (LogEventInfoDisplay)dataGridViewRow.DataBoundItem;


                    sidekickDescriptionFilter = (string)this.SidekickFilterComboBox.SelectedItem;

                    if (sidekickDescriptionFilter != SidekickFilter_NoSidekickFilter &&
                        sidekickDescriptionFilter != logEventInfoDisplay.SidekickDescription)
                    {
                        rowVisible = false;
                    }

                    if (rowVisible == true)
                    {

                        if (logEventInfoDisplay.Severity == LogEventInfoDisplayLevel_Error &&
                            this.DisplayErrorsCheckBox.Checked != true)
                        {
                            rowVisible = false;
                        }
                        else if (logEventInfoDisplay.Severity == LogEventInfoDisplayLevel_Warn &&
                                 this.DisplayWarningsCheckBox.Checked != true)
                        {
                            rowVisible = false;

                        }
                        else if (logEventInfoDisplay.Severity == LogEventInfoDisplayLevel_Info &&
                             this.DisplayInformationCheckBox.Checked != true)
                        {
                            rowVisible = false;
                        }
                    }
                    dataGridViewRow.Visible = rowVisible;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update display (errors, warnings, information) checkbox text.
        /// </summary>
        protected void UpdateDisplayCheckboxText()
        {
            int errorsCount = 0;
            int warningsCount = 0;
            int informationCount = 0;
            string sidekickDescriptionFilter = string.Empty;

            try
            {

                sidekickDescriptionFilter = (string)this.SidekickFilterComboBox.SelectedItem;

                if (sidekickDescriptionFilter != SidekickFilter_NoSidekickFilter)
                {
                    errorsCount = this.LogEventInfoDisplayList.Count(l => l.Severity == LogEventInfoDisplayLevel_Error && l.SidekickDescription == sidekickDescriptionFilter);
                    warningsCount = this.LogEventInfoDisplayList.Count(l => l.Severity == LogEventInfoDisplayLevel_Warn && l.SidekickDescription == sidekickDescriptionFilter);
                    informationCount = this.LogEventInfoDisplayList.Count(l => l.Severity == LogEventInfoDisplayLevel_Info && l.SidekickDescription == sidekickDescriptionFilter);
                }
                else
                {
                    errorsCount = this.LogEventInfoDisplayList.Count(l => l.Severity == LogEventInfoDisplayLevel_Error);
                    warningsCount = this.LogEventInfoDisplayList.Count(l => l.Severity == LogEventInfoDisplayLevel_Warn);
                    informationCount = this.LogEventInfoDisplayList.Count(l => l.Severity == LogEventInfoDisplayLevel_Info);

                }

                this.DisplayErrorsCheckBox.Text = string.Format("{0} {1}",
                                                                errorsCount,
                                                                DisplayErrorsCheckboxText);
                this.DisplayWarningsCheckBox.Text = string.Format("{0} {1}",
                                                                warningsCount,
                                                                DisplayWarningsCheckboxText);
                this.DisplayInformationCheckBox.Text = string.Format("{0} {1}",
                                                                informationCount,
                                                                DisplayInfoCheckboxText);

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Details button - Clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DetailsButton_Click(object sender, EventArgs e)
        {

            ExceptionForm exceptionForm = null;
            LogEventInfoDisplay logEventInfoDisplay = null;

            try
            {

                int selectedRowCount = LogEventInfoDataGridView.Rows.GetRowCount(DataGridViewElementStates.Selected);

                if (selectedRowCount == 1)
                {
                    logEventInfoDisplay = (LogEventInfoDisplay)LogEventInfoDataGridView.SelectedRows[0].DataBoundItem;
                    if (logEventInfoDisplay.ExceptionAvailable == true)
                    {
                        exceptionForm = ExceptionFormFactory.CreateForm(logEventInfoDisplay.Exception);
                        exceptionForm.ShowDialog();
                    }
                }

            }
            catch (Exception exception)
            {
                exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Copy logging text box to clipboard.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyButton_Click(object sender, EventArgs e)
        {
            LogEventInfoDisplay logEventInfoDisplay = null;
            StringBuilder logEventInfoDisplayListStringBuilder = null;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                logEventInfoDisplayListStringBuilder = new StringBuilder();

                foreach (DataGridViewRow dataGridViewRow in this.LogEventInfoDataGridView.Rows)
                {
                    logEventInfoDisplay = (LogEventInfoDisplay)dataGridViewRow.DataBoundItem;

                    logEventInfoDisplayListStringBuilder.AppendLine(string.Format("{0}",
                                                                              logEventInfoDisplay.ToString()));

                }

                Clipboard.SetDataObject(logEventInfoDisplayListStringBuilder.ToString());

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Clear logging text box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearButton_Click(object sender, EventArgs e)
        {
            ExceptionForm exceptionForm = null;

            try
            {
                this.LogEventInfoDisplayList.Clear();
                this.LogEventInfoListBindingSource.ResetBindings(false);
                this.CopySplitButton.Enabled = false;
                this.ClearButton.Enabled = false;
                UpdateDisplayCheckboxText();
            }
            catch (Exception exception)
            {
                exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Log event info data grive view - cell formatting.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LogEventInfoDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            try
            {
                if (LogEventInfoDataGridView.Columns[e.ColumnIndex].Name == LogEventInfoDataGridView_SeverityIconColumn && e.RowIndex >= 0 && e.RowIndex < LogEventInfoDataGridView.Rows.Count)
                {
                    if (LogEventInfoDataGridView.Rows[e.RowIndex].DataBoundItem != null)
                    {
                        LogEventInfoDisplay logEventInfoDisplay = (LogEventInfoDisplay)LogEventInfoDataGridView.Rows[e.RowIndex].DataBoundItem;

                        switch (logEventInfoDisplay.Severity)
                        {
                            case LogEventInfoDisplayLevel_Error:
                                if (logEventInfoDisplay.ExceptionAvailable == true)
                                {
                                    e.Value = (System.Drawing.Image)Properties.Resources.SeverityErrorException;
                                }
                                else
                                {
                                    e.Value = (System.Drawing.Image)Properties.Resources.SeverityErrorNoException;
                                }
                                break;
                            case LogEventInfoDisplayLevel_Warn:
                                e.Value = (System.Drawing.Image)Properties.Resources.SeverityIconWarning;
                                break;
                            case LogEventInfoDisplayLevel_Info:
                                e.Value = (System.Drawing.Image)Properties.Resources.SeverityInformation;
                                break;
                            default:
                                break;
                        }

                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Log event info data grive view - selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LogEventInfoDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            LogEventInfoDisplay logEventInfoDisplay = null;

            try
            {
                this.DetailsButton.Enabled = false;
                this.DisplayDetailsToolStripMenuItem.Enabled = false;
                this.CopyButtonSelectionToolStripMenuItem.Enabled = false;

                int selectedRowCount = LogEventInfoDataGridView.Rows.GetRowCount(DataGridViewElementStates.Selected);

                if (selectedRowCount == 1)
                {
                    logEventInfoDisplay = (LogEventInfoDisplay)LogEventInfoDataGridView.SelectedRows[0].DataBoundItem;
                    if (logEventInfoDisplay.ExceptionAvailable == true)
                    {
                        this.DetailsButton.Enabled = true;
                        this.DisplayDetailsToolStripMenuItem.Enabled = true;
                        this.CopyButtonSelectionToolStripMenuItem.Enabled = true;
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Display errors checkbox - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayErrorsCheckBox_CheckedChanged(object sender, EventArgs e)
        {

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                ToggleLogEventInfoDataViewAllRowsVisible();

                this.AutoScrollLogEventInfoDataGridView();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Display warnings checkbox - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayWarningsCheckBox_CheckedChanged(object sender, EventArgs e)
        {

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                ToggleLogEventInfoDataViewAllRowsVisible();

                this.AutoScrollLogEventInfoDataGridView();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Display information checkbox - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayInformationCheckBox_CheckedChanged(object sender, EventArgs e)
        {

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                ToggleLogEventInfoDataViewAllRowsVisible();

                this.AutoScrollLogEventInfoDataGridView();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Copy all tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LogEventInfoDisplay logEventInfoDisplay = null;
            StringBuilder logEventInfoDisplayListStringBuilder = null;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                logEventInfoDisplayListStringBuilder = new StringBuilder();

                foreach (DataGridViewRow dataGridViewRow in this.LogEventInfoDataGridView.Rows)
                {
                    logEventInfoDisplay = (LogEventInfoDisplay)dataGridViewRow.DataBoundItem;

                    logEventInfoDisplayListStringBuilder.AppendLine(string.Format("{0}",
                                                                              logEventInfoDisplay.ToString()));

                }

                Clipboard.SetDataObject(logEventInfoDisplayListStringBuilder.ToString());

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Copy selection tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopySelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LogEventInfoDisplay logEventInfoDisplay = null;
            StringBuilder logEventInfoDisplayListStringBuilder = null;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                logEventInfoDisplayListStringBuilder = new StringBuilder();

                foreach (DataGridViewRow dataGridViewRow in this.LogEventInfoDataGridView.SelectedRows)
                {
                    logEventInfoDisplay = (LogEventInfoDisplay)dataGridViewRow.DataBoundItem;

                    logEventInfoDisplayListStringBuilder.AppendLine(string.Format("{0}",
                                                                              logEventInfoDisplay.ToString()));

                }

                Clipboard.SetDataObject(logEventInfoDisplayListStringBuilder.ToString());

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Display details tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            ExceptionForm exceptionForm = null;
            LogEventInfoDisplay logEventInfoDisplay = null;
            int selectedRowCount = 0;

            try
            {

                selectedRowCount = LogEventInfoDataGridView.Rows.GetRowCount(DataGridViewElementStates.Selected);

                if (selectedRowCount == 1)
                {
                    logEventInfoDisplay = (LogEventInfoDisplay)LogEventInfoDataGridView.SelectedRows[0].DataBoundItem;
                    if (logEventInfoDisplay.ExceptionAvailable == true)
                    {
                        exceptionForm = ExceptionFormFactory.CreateForm(logEventInfoDisplay.Exception);
                        exceptionForm.ShowDialog();
                    }
                }

            }
            catch (Exception exception)
            {
                exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Clear tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExceptionForm exceptionForm = null;

            try
            {
                this.LogEventInfoDisplayList.Clear();
                this.LogEventInfoListBindingSource.ResetBindings(false);
                UpdateDisplayCheckboxText();
            }
            catch (Exception exception)
            {
                exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Show log file link label - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowLogFileLinkLabel_Click(object sender, EventArgs e)
        {
            FileTarget fileTarget = null;
            string logFileName = string.Empty;
            string logFilePath = string.Empty;
            string executablePath = string.Empty;
            LogEventInfo logEventInfo = null;

            try
            {
                //Retrive NLog FileTarget from configuration.
                fileTarget = (FileTarget)LogManager.Configuration.FindTargetByName("LogFile");

                if (fileTarget == null)
                {
                    MessageBox.Show(string.Format(ShowLogFileErrorMessage_CouldNotLocateLogFile));
                    return;
                }

                logEventInfo = new LogEventInfo();
                logFileName = fileTarget.FileName.Render(logEventInfo);

                if (logFileName == null)
                {
                    MessageBox.Show(string.Format(ShowLogFileErrorMessage_CouldNotLocateLogFile));
                    return;
                }

                executablePath = Path.GetDirectoryName(Application.ExecutablePath);
                logFilePath = Path.Combine(executablePath, logFileName);

                if (File.Exists(logFilePath) == false)
                {
                    MessageBox.Show(string.Format(ShowLogFileErrorMessage_CouldNotLocateLogFile));
                    return;
                }

                System.Diagnostics.Process.Start(logFilePath);


            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Sidekick filter combo box - selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SidekickFilterComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                ToggleLogEventInfoDataViewAllRowsVisible();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Sidekick selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSidekickSelectionChanged(object sender, SidekickSelectionChangedEventArgs e)
        {

            try
            {
                if (string.IsNullOrEmpty(e.SidekickDescription) == true)
                {
                    return;
                }
                if (this.SidekickFilterComboBox.Items.Contains(e.SidekickDescription) == true)
                {
                    this.SidekickFilterComboBox.SelectedItem = e.SidekickDescription;
                }
                else
                {
                    this.SidekickFilterComboBox.SelectedItem = SidekickFilter_NoSidekickFilter;
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion
    }

    /// <summary>
    /// Log event info display.
    /// </summary>
    public class LogEventInfoDisplay
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private string _severity;
        private DateTime _timeStamp;
        private string _message;
        private bool _exceptionAvailable;
        private Exception _exception;
        private string _sidekickName;
        private string _sidekickDescription;
        private string _extra;

        #endregion


        #region Public Properties

        /// <summary>
        /// Property: Severity.
        /// </summary>
        public string Severity
        {
            get { return _severity; }
            set { _severity = value; }
        }

        /// <summary>
        /// Property: Timestamp.
        /// </summary>
        public DateTime TimeStamp
        {
            get { return _timeStamp; }
            set { _timeStamp = value; }
        }

        /// <summary>
        /// Property: Message.
        /// </summary>
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        /// <summary>
        /// Property: Exception available.
        /// </summary>
        public bool ExceptionAvailable
        {
            get { return _exceptionAvailable; }
            set { _exceptionAvailable = value; }
        }

        /// <summary>
        /// Property: Exception.
        /// </summary>
        public Exception Exception
        {
            get { return _exception; }
            set { _exception = value; }
        }

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get { return _sidekickName; }
            set { _sidekickName = value; }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get { return _sidekickDescription; }
            set { _sidekickDescription = value; }
        }

        /// <summary>
        /// Property: Extra.
        /// </summary>
        public string Extra
        {
            get { return _extra; }
            set { _extra = value; }
        }
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public LogEventInfoDisplay()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Override ToString() method.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}\t{1}\t{2}",
                                this.TimeStamp,
                                this.Severity,
                                this.Message);
        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

        #region Private Constants
        #endregion
    }

}
