﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Aclara.Vsts.TeamsAndProjects.Client.Models;


namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class VSTSProjectConnectForm : Form
    {
        #region Private Constants

        private const string VerificationStatus_UserProfileNameMustBeSpecified = "User profile name is required.";
        private const string VerificationStatus_PersonalAccessTokenMustBeSpecified = "Personal access token is required.";
        private const string VerificationStatus_InvalidCredentials = "Invalid credentials.";
        private const string VerificationStatus_CredentialsVerified = "Credentials verified.";

        private const string Exception_UnexpectedCredentialsValidationStatus = "Unexpected credentials validation status. (Credentials validation status: {0})";

        private const string FormHelp_HelpFileName = "AuthenticationVsoPersonalAccessToken.htm";

        #endregion

        #region Public Enumerations

        public enum CredentialsValidationStatusCode
        {
            CredentialsAreInvalid = 0,
            CredentialsAreValid = 1,
            UserProfileNotSpecified = 2,
            PersonalAccessTokenNotSpecified = 3
        }

        #endregion

        #region Private Data Members

        private VSTSProjectConnectPresenter _vstsProjectConnectPresenter;
        string _teamProjectName;
        private Uri _teamProjectCollectionUri;
        private TeamProjectList _teamProjectInfo;
        private CredentialsValidationStatusCode _credentialsValidationStatus;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: VSTS project connect presenter.
        /// </summary>
        public VSTSProjectConnectPresenter VSTSProjectConnectPresenter
        {
            get { return _vstsProjectConnectPresenter; }
            set { _vstsProjectConnectPresenter = value; }
        }

        /// <summary>
        /// Property: Team project collection uri.
        /// </summary>
        public Uri TeamProjectCollectionUri
        {
            get { return _teamProjectCollectionUri; }
            set
            {
                string teamProjectCollectionUriAsText = string.Empty;

                _teamProjectCollectionUri = value;

                teamProjectCollectionUriAsText = value.ToString();
                if (teamProjectCollectionUriAsText.EndsWith("/") == false)
                {
                    teamProjectCollectionUriAsText = value + @"/";
                    _teamProjectCollectionUri = new Uri(teamProjectCollectionUriAsText);
                }
            }
        }

        /// <summary>
        /// Property: Team project name.
        /// </summary>
        public string TeamProjectName
        {
            get { return _teamProjectName; }
            set { _teamProjectName = value; }
        }
        /// <summary>
        /// Property: Team project list.
        /// </summary>
        public TeamProjectList TeamProjectList
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Credtinal validation status.
        /// </summary>
        public CredentialsValidationStatusCode CredentialsValidationStatus
        {
            get { return _credentialsValidationStatus; }
            set { _credentialsValidationStatus = value; }
        }

        /// <summary>
        /// Property: Basic authorization REST API user profile name.
        /// </summary>
        public string BasicAuthRestAPIUserProfileName
        {
            get { return this.UserProfileNameTextBox.Text; }
            set { this.UserProfileNameTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Basic authorization REST API password.
        /// </summary>
        public string BasicAuthRestAPIPassword
        {
            get { return this.PersonalAccessTokenTextBox.Text; }
            set { this.PersonalAccessTokenTextBox.Text = value; }
        }


        /// <summary>
        /// Property: Verification status.
        /// </summary>
        public string VerificationStatus
        {
            get { return this.VerificationStatusLabel.Text; }
            set { this.VerificationStatusLabel.Text = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public VSTSProjectConnectForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {
                this.OverrideCredentialsCheckBox.Checked = false;

                this.ValidateCredentialsAndUpdateUI();

                this.TeamProjectList = this.VSTSProjectConnectPresenter.GetTeamProjects(this.TeamProjectCollectionUri,
                                                                                        this.BasicAuthRestAPIUserProfileName,
                                                                                        this.BasicAuthRestAPIPassword);

                this.AccountComboBox.Items.Clear();
                this.AccountComboBox.Items.Add(this.TeamProjectCollectionUri.ToString());
                this.AccountComboBox.SelectedItem = this.TeamProjectCollectionUri.ToString();

                this.TeamProjectComboBox.DataSource = null;
                if (this.TeamProjectList != null &&
                     this.TeamProjectList.Count > 0)
                {
                    this.TeamProjectComboBox.DataSource = TeamProjectList;
                    this.TeamProjectComboBox.DisplayMember = "Name";
                    this.TeamProjectComboBox.ValueMember = "Name";

                    var teamProject = this.TeamProjectList.Where(tp => tp.Name == this.TeamProjectName).SingleOrDefault();
                    if (teamProject != null)
                    {
                        this.TeamProjectComboBox.SelectedItem = this.TeamProjectList.Where(tp => tp.Name == this.TeamProjectName).SingleOrDefault();
                    }
                }

                this.UpdateUIControlsBasedOnOverrideCredentialsState();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Validate credentials.
        /// </summary>
        /// <returns></returns>
        public void ValidateCredentials()
        {
            TeamProjectList teamProjectList = null;

            try
            {
                this.CredentialsValidationStatus = CredentialsValidationStatusCode.CredentialsAreInvalid;

                if (string.IsNullOrEmpty(this.BasicAuthRestAPIUserProfileName) == true)
                {
                    this.CredentialsValidationStatus = CredentialsValidationStatusCode.UserProfileNotSpecified;
                    return;
                }

                if (string.IsNullOrEmpty(this.BasicAuthRestAPIPassword) == true)
                {
                    this.CredentialsValidationStatus = CredentialsValidationStatusCode.PersonalAccessTokenNotSpecified;
                    return;
                }

                try
                {
                    teamProjectList = this.VSTSProjectConnectPresenter.GetTeamProjects(this.TeamProjectCollectionUri,
                                                                                       this.BasicAuthRestAPIUserProfileName,
                                                                                       this.BasicAuthRestAPIPassword);
                    if (teamProjectList != null &&
                        teamProjectList.Count > 0)
                    {
                        this.CredentialsValidationStatus = CredentialsValidationStatusCode.CredentialsAreValid;
                        return;
                    }
                }
                catch (Exception)
                {
                    this.CredentialsValidationStatus = CredentialsValidationStatusCode.CredentialsAreInvalid;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Protected Methods


        /// <summary>
        /// Validate credentials and update UI.
        /// </summary>
        /// <returns></returns>
        protected void ValidateCredentialsAndUpdateUI()
        {

            try
            {

                this.ValidateCredentials();
                this.UpdateValidationStatusBasedOnCredentialsValidationStatus();
                this.UpdateUIControlsBasedOnCredentialsValidationStatus();
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Update UI contronls based on credentials validation status.
        /// </summary>
        protected void UpdateUIControlsBasedOnCredentialsValidationStatus()
        {
            try
            {
                switch (this.CredentialsValidationStatus)
                {
                    case CredentialsValidationStatusCode.CredentialsAreInvalid:
                    case CredentialsValidationStatusCode.UserProfileNotSpecified:
                    case CredentialsValidationStatusCode.PersonalAccessTokenNotSpecified:
                        this.OverrideCredentialsCheckBox.Checked = true;
                        AccountComboBox.Enabled = false;
                        TeamProjectComboBox.Enabled = false;
                        this.VerificationStatusLabel.ForeColor = Color.DarkRed;
                        this.OKButton.Enabled = false;
                        break;
                    case CredentialsValidationStatusCode.CredentialsAreValid:
                        this.OverrideCredentialsCheckBox.Checked = false;
                        this.VerificationStatusLabel.ForeColor = Color.DarkGreen;
                        AccountComboBox.Enabled = true;
                        TeamProjectComboBox.Enabled = true;
                        this.OKButton.Enabled = true;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format(Exception_UnexpectedCredentialsValidationStatus,
                                                              this.CredentialsValidationStatus));
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Update validation status based on credentials validation status.
        /// </summary>
        protected void UpdateValidationStatusBasedOnCredentialsValidationStatus()
        {
            try
            {

                switch (this.CredentialsValidationStatus)
                {
                    case CredentialsValidationStatusCode.CredentialsAreInvalid:
                        this.UpdateVerificationStatus(VerificationStatus_InvalidCredentials);
                        break;
                    case CredentialsValidationStatusCode.CredentialsAreValid:
                        this.UpdateVerificationStatus(VerificationStatus_CredentialsVerified);
                        break;
                    case CredentialsValidationStatusCode.UserProfileNotSpecified:
                        this.UpdateVerificationStatus(VerificationStatus_UserProfileNameMustBeSpecified);
                        break;
                    case CredentialsValidationStatusCode.PersonalAccessTokenNotSpecified:
                        this.UpdateVerificationStatus(VerificationStatus_PersonalAccessTokenMustBeSpecified);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format(Exception_UnexpectedCredentialsValidationStatus,
                                                              this.CredentialsValidationStatus));
                }


            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update UI controls to override credentials state.
        /// </summary>
        protected void UpdateUIControlsBasedOnOverrideCredentialsState()
        {
            try
            {
                if (this.OverrideCredentialsCheckBox.Checked == true)
                {
                    this.UserProfileNameTextBox.Enabled = true;
                    this.PersonalAccessTokenTextBox.Enabled = true;
                }
                else
                {
                    this.UserProfileNameTextBox.Enabled = false;
                    this.PersonalAccessTokenTextBox.Enabled = false;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Update verification status.
        /// </summary>
        /// <param name="message"></param>
        protected void UpdateVerificationStatus(string message)
        {
            try
            {
                VerificationStatus = message;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Synchronize UI controls to properties.
        /// </summary>
        protected void SynchronizeUIControlsToProperties()
        {
            Microsoft.TeamFoundation.Core.WebApi.TeamProject teamProject = null;

            try
            {
                if (this.TeamProjectComboBox.SelectedItem is Microsoft.TeamFoundation.Core.WebApi.TeamProject)
                {
                    teamProject = (Microsoft.TeamFoundation.Core.WebApi.TeamProject)this.TeamProjectComboBox.SelectedItem;
                    this.TeamProjectName = teamProject.Name;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: OK button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.SynchronizeUIControlsToProperties();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Cancel button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.None;
                this.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: VSTS project connect form - Shown.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VSTSProjectConnectForm_Shown(object sender, EventArgs e)
        {
            try
            {
                this.InitializeControls();
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Event Handler: Validate credentials button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ValidateCredentialsButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.ValidateCredentialsAndUpdateUI();

                if (this.CredentialsValidationStatus == CredentialsValidationStatusCode.CredentialsAreValid)
                {
                    this.SynchronizeUIControlsToProperties();
                    this.InitializeControls();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Override credentials check box - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OverrideCredentialsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.UpdateUIControlsBasedOnOverrideCredentialsState();
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Event Handler: Create personal access token link label - Clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreatePATLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string link = string.Empty;
            string teamProjectCollectionUri = string.Empty;

            try
            {
                teamProjectCollectionUri = this.TeamProjectCollectionUri.AbsoluteUri;

                if (teamProjectCollectionUri.EndsWith("/") == false)
                {
                    teamProjectCollectionUri += @"/";
                }

                link = string.Format("{0}DefaultCollection/_details/security/tokens",
                                     teamProjectCollectionUri,
                                     this.TeamProjectName);
                System.Diagnostics.Process.Start(link);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.HelpProvider.HelpNamespace, FormHelp_HelpFileName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

    }
}
