﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class ReleaseDefinitionCloneForm : Form, ISidekickView, IReleaseDefinitionCloneView, ISidekickInfo
    {

        #region Private Constants

        private const string SidekickInfo_SidekickName = "CloneReleaseDefinitions";
        private const string SidekickInfo_SidekickDescription = "Clone Release Definitions";

        private const string ValidationErrorMessage_TeamProjectNameNotSelected = "Team project not selected.";
        private const string ValidationErrorMessage_BranchNameSourceNotEntered = "Branch name source not entered.";
        private const string ValidationErrorMessage_BuildDefinitionNotSelected = "Build definition not selected.";
        private const string ValidationErrorMessage_SourceBranchNameNotEntered = "Branch name - source not entered.";
        private const string ValidationErrorMessage_SourceBuildDefinitionNamePrefixNotEntered = "Build definition name prefix - source not entered.";
        private const string ValidationErrorMessage_TargetBranchNameNotEntered = "Branch name - target not entered.";
        private const string ValidationErrorMessage_TargetBuildDefinitionNamePrefixNotEntered = "Build definition name prefix - target not entered.";
        private const string ValidationErrorMessage_SelectedBuildDefinitionsMustHaveSameNamePrefix = "Selected build definitons must have same name prefix.";
        private const string ValidationErrorMessage_TargetBuildDefinitionNamePrefixContainsInvalidCharacter = "Target build definition name prefix contains invalid character. ( ; , )";
        private const string ValidationErrorMessage_RepositoryTypeValidSelection = "Select Team Foundation Version Control or Git. No other repository types are supported.";

        #endregion

        #region Private Data Members

        private SidekickConfiguration _sidekickConfiguration = null;
        private ReleaseDefinitionClonePresenter _releaseDefinitionClonePresenter;
        private ReleaseDefinitionSearchForm _releaseDefinitionSearchForm = null;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Release definition clone presenter.
        /// </summary>
        public ReleaseDefinitionClonePresenter ReleaseDefinitionClonePresenter
        {
            get { return _releaseDefinitionClonePresenter; }
            set { _releaseDefinitionClonePresenter = value; }
        }

        /// <summary>
        /// Property: Build definition search form.
        /// </summary>
        public ReleaseDefinitionSearchForm ReleaseDefinitionSearchForm
        {
            get { return _releaseDefinitionSearchForm; }
            set { _releaseDefinitionSearchForm = value; }
        }

        /// <summary>
        /// Property: Branch name source.
        /// </summary>
        public string SourceBranchName
        {
            get { return this.BranchNameSourceTextBox.Text; }
            set { this.BranchNameSourceTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Build definition name prefix - source.
        /// </summary>
        public string SourceBuildDefinitionNamePrefix
        {
            get { return this.ReleaseDefinitionNamePrefixSourceTextBox.Text; }
            set { this.ReleaseDefinitionNamePrefixSourceTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Branch name target.
        /// </summary>
        public string TargetBranchName
        {
            get { return this.BranchNameTargetTextBox.Text; }
            set { this.BranchNameTargetTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Build definition name prefix - target.
        /// </summary>
        public string TargetBuildDefinitionNamePrefix
        {
            get { return this.ReleaseDefinitionNamePrefixTargetTextBox.Text; }
            set { this.ReleaseDefinitionNamePrefixTargetTextBox.Text = value; }
        }

        /// <summary>
        /// Property:  Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get { return this.ApplyButton.Text; }
            set { this.ApplyButton.Text = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                if (this.ReleaseDefinitionClonePresenter.IsBackgroundTaskBusy == false)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickInfo_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickInfo_SidekickDescription;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="releaseDefinitionSearchForm"></param>
        public ReleaseDefinitionCloneForm(SidekickConfiguration sidekickConfiguration,
                                          ReleaseDefinitionSearchForm releaseDefinitionSearchForm)
        {
            InitializeComponent();
            InitializeControls();
            _sidekickConfiguration = sidekickConfiguration;
            _releaseDefinitionSearchForm = releaseDefinitionSearchForm;
            this.ReleaseDefinitionSearchForm.ReleaseDefinitionSelectionChanged += OnReleaseDefinitionSelectionChanged;
            this.ReleaseDefinitionSearchPanel.DockControl(this.ReleaseDefinitionSearchForm);
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <param></param>
        private ReleaseDefinitionCloneForm()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update progress clone release definition.
        /// </summary>
        /// <param name="releaseDefinitionCloneCount"></param>
        /// <param name="releaseDefinitionTotalCount"></param>
        public void UpdateProgressCloneReleaseDefinition(int releaseDefinitionCloneCount, int releaseDefinitionTotalCount)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<int, int>(this.UpdateProgressCloneReleaseDefinition), releaseDefinitionCloneCount, releaseDefinitionTotalCount);
            }
            else
            {
                if (releaseDefinitionCloneCount < releaseDefinitionTotalCount)
                {             
                    this.CloneReleaseDefinitionProgressBar.Visible = true;
                    this.CloneReleaseDefinitionProgressBar.Minimum = 0;
                    this.CloneReleaseDefinitionProgressBar.Maximum = releaseDefinitionTotalCount;
                    this.CloneReleaseDefinitionProgressBar.Value = releaseDefinitionCloneCount;
                }
                else if (releaseDefinitionCloneCount == releaseDefinitionTotalCount)
                {
                    this.CloneReleaseDefinitionProgressBar.Visible = false;
                }
            }
        }
        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            this.ReleaseDefinitionSearchForm.TeamProjectChanged();
        }

        /// <summary>
        /// Retrieve remaining build definitions.
        /// </summary>
        public void RetrieveRemainingDefinitions()
        {
            this.ReleaseDefinitionSearchForm.RetrieveRemainingDefinitions();
        }

        /// <summary>
        /// Apply button enable. 
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {

            try
            {
                this.PropertiesHeaderLabel.Text = this.SidekickDescription;
                this.EnableControlsDependantOnSelectedReleaseDefinition(false);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Hanlder: Release definition selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="requestBuildEventArgs"></param>
        protected void OnReleaseDefinitionSelectionChanged(Object sender,
                                                          ReleaseDefinitionSelectionChangedEventArgs releaseDefinitionSelectionChangedEventArgs)
        {

            ReleaseDefinitionSelectorList releaseDefinitionSelectorList = null;

            try
            {
                if (releaseDefinitionSelectionChangedEventArgs != null)
                {
                    if (releaseDefinitionSelectionChangedEventArgs.OneOrMoreSelected == true)
                    {
                        releaseDefinitionSelectorList = this.ReleaseDefinitionSearchForm.GetSelectedReleaseDefinitionSelectorList();
                        if (releaseDefinitionSelectorList.Count > 0)
                        {
                            this.BranchNameSourceTextBox.Text = releaseDefinitionSelectorList[0].GetYearMonthBranchName();
                            this.ReleaseDefinitionNamePrefixSourceTextBox.Text = releaseDefinitionSelectorList[0].GetNamePrefix();
                            this.EnableControlsDependantOnSelectedReleaseDefinition(true);

                        }

                    }
                    else
                    {
                        this.BranchNameSourceTextBox.Text = string.Empty;
                        this.ReleaseDefinitionNamePrefixSourceTextBox.Text = string.Empty;
                        this.EnableControlsDependantOnSelectedReleaseDefinition(false);
                    }

                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Enable/disable controls dependant on at lease one release definition selected.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnSelectedReleaseDefinition(bool enable)
        {
            try
            {
                this.BranchNameSourceTextBox.Enabled = enable;
                this.ReleaseDefinitionNamePrefixSourceTextBox.Enabled = enable;
                this.BranchNameTargetTextBox.Enabled = enable;
                this.ReleaseDefinitionNamePrefixTargetTextBox.Enabled = enable;
                this.ResetButton.Enabled = enable;

                this.ApplyButton.Enabled = enable;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            ReleaseDefinitionSelectorList releaseDefinitionSelectorList = null;
            List<string> releaseDefinitionNamePrefixList = null;

            try
            {

                if (this.ReleaseDefinitionClonePresenter.TeamProjectInfo.TeamProjectCollectionUri == null ||
                    this.ReleaseDefinitionClonePresenter.TeamProjectInfo.TeamProjectName == string.Empty)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_TeamProjectNameNotSelected));
                    return;
                }

                if (this.SourceBranchName == string.Empty)
                {
                    MessageBox.Show(this, string.Format(ValidationErrorMessage_SourceBranchNameNotEntered));
                    return;
                }

                if (this.SourceBuildDefinitionNamePrefix == string.Empty)
                {
                    MessageBox.Show(this, string.Format(ValidationErrorMessage_SourceBuildDefinitionNamePrefixNotEntered));
                    return;
                }

                if (this.TargetBranchName == string.Empty)
                {
                    MessageBox.Show(this, string.Format(ValidationErrorMessage_TargetBranchNameNotEntered));
                    return;
                }

                if (this.TargetBuildDefinitionNamePrefix == string.Empty)
                {
                    MessageBox.Show(this, string.Format(ValidationErrorMessage_TargetBuildDefinitionNamePrefixNotEntered));
                    return;
                }

                if (this.TargetBuildDefinitionNamePrefix.Contains(",") == true ||
                    this.TargetBuildDefinitionNamePrefix.Contains(";") == true)
                {
                    MessageBox.Show(this, string.Format(ValidationErrorMessage_TargetBuildDefinitionNamePrefixContainsInvalidCharacter));
                    return;
                }

                releaseDefinitionSelectorList = this.ReleaseDefinitionSearchForm.GetSelectedReleaseDefinitionSelectorList();
                if (releaseDefinitionSelectorList == null ||
                    releaseDefinitionSelectorList.Count <= 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_BuildDefinitionNotSelected));
                    return;
                }

                releaseDefinitionNamePrefixList = releaseDefinitionSelectorList.GetReleaseDefinitionNamePrefixList();
                if (releaseDefinitionNamePrefixList != null)
                {
                    if (releaseDefinitionNamePrefixList.Count > 1)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_SelectedBuildDefinitionsMustHaveSameNamePrefix));
                        return;
                    }
                }
                //TODO:
                this.ReleaseDefinitionClonePresenter.CloneReleaseDefinitions(releaseDefinitionSelectorList,
                                                                             this.SourceBuildDefinitionNamePrefix,
                                                                             this.TargetBuildDefinitionNamePrefix,
                                                                             this.SourceBranchName,
                                                                             this.TargetBranchName
                                                                             );

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Reset button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.ReleaseDefinitionNamePrefixTargetTextBox.Text = string.Empty;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();

                throw;
            }
        }

        /// <summary>
        /// Event Handler: Branch name target textbox - leave.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BranchNameTargetTextBox_Leave(object sender, EventArgs e)
        {
            ReleaseDefinitionSelectorList releaseDefinitionSelectorList = null;
            string releaseDefinitionNamePrefixTarget = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(this.ReleaseDefinitionNamePrefixTargetTextBox.Text) == false)
                {
                    return;
                }

                releaseDefinitionSelectorList = this.ReleaseDefinitionSearchForm.GetSelectedReleaseDefinitionSelectorList();
                if (releaseDefinitionSelectorList.Count > 0)
                {
                    releaseDefinitionNamePrefixTarget = string.Format("{0}_{1}_",
                                                                    releaseDefinitionSelectorList[0].GetNamePrefixWithoutYearMonthBranch(),
                                                                    this.BranchNameTargetTextBox.Text);
                    this.ReleaseDefinitionNamePrefixTargetTextBox.Text = releaseDefinitionNamePrefixTarget;
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();

                throw;
            }
        }

        #endregion

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickCloneReleaseDefinitions.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }
    }
}
