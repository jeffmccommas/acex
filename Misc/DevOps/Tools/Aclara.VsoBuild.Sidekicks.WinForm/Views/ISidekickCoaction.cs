﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    //Sidekick cooperation interface.
    public interface ISidekickCoaction
    {

        void CheckBuildStatus(List<string> buildDefinitionNameList);

        void RequestBuild(List<string> buildDefinitionNameList);

    }
}
