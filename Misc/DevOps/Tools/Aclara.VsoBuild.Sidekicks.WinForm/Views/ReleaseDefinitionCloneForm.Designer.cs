﻿namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    partial class ReleaseDefinitionCloneForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ReleaseDefinitionSearchPanel = new System.Windows.Forms.Panel();
            this.ReleaseDefinitionSearchPlaceholderLabel = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.DirectionToTargetPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.DirectionFromSourcePanel = new System.Windows.Forms.Panel();
            this.DirectionLabel = new System.Windows.Forms.Label();
            this.TargetPropertiesPanel = new System.Windows.Forms.Panel();
            this.TargetHeaderPanel = new System.Windows.Forms.Panel();
            this.TargetLabel = new System.Windows.Forms.Label();
            this.ResetButton = new System.Windows.Forms.Button();
            this.ReleaseDefinitionNamePrefixTargetTextBox = new System.Windows.Forms.TextBox();
            this.ReleaseDefinitionNamePrefixTargetLabel = new System.Windows.Forms.Label();
            this.BranchNameTargetTextBox = new System.Windows.Forms.TextBox();
            this.BranchNameTargetLabel = new System.Windows.Forms.Label();
            this.SourcePropertiesPanel = new System.Windows.Forms.Panel();
            this.SourceHeaderPanel = new System.Windows.Forms.Panel();
            this.SourceLabel = new System.Windows.Forms.Label();
            this.ReleaseDefinitionNamePrefixSourceTextBox = new System.Windows.Forms.TextBox();
            this.ReleaseDefinitionNamePrefixSourceLabel = new System.Windows.Forms.Label();
            this.BranchNameSourceTextBox = new System.Windows.Forms.TextBox();
            this.BranchNameSourceLabel = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.CloneReleaseDefinitionProgressBar = new System.Windows.Forms.ProgressBar();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.HelpButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.ReleaseDefinitionSearchPanel.SuspendLayout();
            this.PropertiesPanel.SuspendLayout();
            this.DirectionToTargetPanel.SuspendLayout();
            this.DirectionFromSourcePanel.SuspendLayout();
            this.TargetPropertiesPanel.SuspendLayout();
            this.TargetHeaderPanel.SuspendLayout();
            this.SourcePropertiesPanel.SuspendLayout();
            this.SourceHeaderPanel.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ReleaseDefinitionSearchPanel
            // 
            this.ReleaseDefinitionSearchPanel.Controls.Add(this.ReleaseDefinitionSearchPlaceholderLabel);
            this.ReleaseDefinitionSearchPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.ReleaseDefinitionSearchPanel.Location = new System.Drawing.Point(0, 0);
            this.ReleaseDefinitionSearchPanel.Name = "ReleaseDefinitionSearchPanel";
            this.ReleaseDefinitionSearchPanel.Size = new System.Drawing.Size(644, 550);
            this.ReleaseDefinitionSearchPanel.TabIndex = 5;
            // 
            // ReleaseDefinitionSearchPlaceholderLabel
            // 
            this.ReleaseDefinitionSearchPlaceholderLabel.AutoSize = true;
            this.ReleaseDefinitionSearchPlaceholderLabel.Location = new System.Drawing.Point(255, 455);
            this.ReleaseDefinitionSearchPlaceholderLabel.Name = "ReleaseDefinitionSearchPlaceholderLabel";
            this.ReleaseDefinitionSearchPlaceholderLabel.Size = new System.Drawing.Size(189, 13);
            this.ReleaseDefinitionSearchPlaceholderLabel.TabIndex = 1;
            this.ReleaseDefinitionSearchPlaceholderLabel.Text = "Release Definition Search Placeholder";
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter1.Location = new System.Drawing.Point(644, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(6, 550);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PropertiesPanel.Controls.Add(this.DirectionToTargetPanel);
            this.PropertiesPanel.Controls.Add(this.DirectionFromSourcePanel);
            this.PropertiesPanel.Controls.Add(this.TargetPropertiesPanel);
            this.PropertiesPanel.Controls.Add(this.SourcePropertiesPanel);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesPanel.Location = new System.Drawing.Point(650, 0);
            this.PropertiesPanel.MinimumSize = new System.Drawing.Size(560, 480);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(566, 550);
            this.PropertiesPanel.TabIndex = 7;
            // 
            // DirectionToTargetPanel
            // 
            this.DirectionToTargetPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DirectionToTargetPanel.BackColor = System.Drawing.Color.Gainsboro;
            this.DirectionToTargetPanel.Controls.Add(this.label1);
            this.DirectionToTargetPanel.Location = new System.Drawing.Point(476, 236);
            this.DirectionToTargetPanel.Name = "DirectionToTargetPanel";
            this.DirectionToTargetPanel.Size = new System.Drawing.Size(83, 21);
            this.DirectionToTargetPanel.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "<< << <<";
            // 
            // DirectionFromSourcePanel
            // 
            this.DirectionFromSourcePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DirectionFromSourcePanel.BackColor = System.Drawing.Color.Gainsboro;
            this.DirectionFromSourcePanel.Controls.Add(this.DirectionLabel);
            this.DirectionFromSourcePanel.Location = new System.Drawing.Point(475, 101);
            this.DirectionFromSourcePanel.Name = "DirectionFromSourcePanel";
            this.DirectionFromSourcePanel.Size = new System.Drawing.Size(83, 21);
            this.DirectionFromSourcePanel.TabIndex = 7;
            // 
            // DirectionLabel
            // 
            this.DirectionLabel.AutoSize = true;
            this.DirectionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DirectionLabel.Location = new System.Drawing.Point(15, 4);
            this.DirectionLabel.Name = "DirectionLabel";
            this.DirectionLabel.Size = new System.Drawing.Size(57, 13);
            this.DirectionLabel.TabIndex = 0;
            this.DirectionLabel.Text = ">> >> >>";
            // 
            // TargetPropertiesPanel
            // 
            this.TargetPropertiesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TargetPropertiesPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TargetPropertiesPanel.Controls.Add(this.TargetHeaderPanel);
            this.TargetPropertiesPanel.Controls.Add(this.ResetButton);
            this.TargetPropertiesPanel.Controls.Add(this.ReleaseDefinitionNamePrefixTargetTextBox);
            this.TargetPropertiesPanel.Controls.Add(this.ReleaseDefinitionNamePrefixTargetLabel);
            this.TargetPropertiesPanel.Controls.Add(this.BranchNameTargetTextBox);
            this.TargetPropertiesPanel.Controls.Add(this.BranchNameTargetLabel);
            this.TargetPropertiesPanel.Location = new System.Drawing.Point(6, 166);
            this.TargetPropertiesPanel.Name = "TargetPropertiesPanel";
            this.TargetPropertiesPanel.Size = new System.Drawing.Size(463, 184);
            this.TargetPropertiesPanel.TabIndex = 9;
            // 
            // TargetHeaderPanel
            // 
            this.TargetHeaderPanel.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.TargetHeaderPanel.Controls.Add(this.TargetLabel);
            this.TargetHeaderPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TargetHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.TargetHeaderPanel.Name = "TargetHeaderPanel";
            this.TargetHeaderPanel.Size = new System.Drawing.Size(461, 22);
            this.TargetHeaderPanel.TabIndex = 0;
            // 
            // TargetLabel
            // 
            this.TargetLabel.AutoSize = true;
            this.TargetLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TargetLabel.Location = new System.Drawing.Point(4, 2);
            this.TargetLabel.Name = "TargetLabel";
            this.TargetLabel.Size = new System.Drawing.Size(56, 17);
            this.TargetLabel.TabIndex = 0;
            this.TargetLabel.Text = "Target";
            // 
            // ResetButton
            // 
            this.ResetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ResetButton.Location = new System.Drawing.Point(381, 148);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(75, 23);
            this.ResetButton.TabIndex = 7;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // ReleaseDefinitionNamePrefixTargetTextBox
            // 
            this.ReleaseDefinitionNamePrefixTargetTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReleaseDefinitionNamePrefixTargetTextBox.Location = new System.Drawing.Point(4, 99);
            this.ReleaseDefinitionNamePrefixTargetTextBox.Name = "ReleaseDefinitionNamePrefixTargetTextBox";
            this.ReleaseDefinitionNamePrefixTargetTextBox.Size = new System.Drawing.Size(454, 20);
            this.ReleaseDefinitionNamePrefixTargetTextBox.TabIndex = 4;
            // 
            // ReleaseDefinitionNamePrefixTargetLabel
            // 
            this.ReleaseDefinitionNamePrefixTargetLabel.AutoSize = true;
            this.ReleaseDefinitionNamePrefixTargetLabel.Location = new System.Drawing.Point(4, 83);
            this.ReleaseDefinitionNamePrefixTargetLabel.Name = "ReleaseDefinitionNamePrefixTargetLabel";
            this.ReleaseDefinitionNamePrefixTargetLabel.Size = new System.Drawing.Size(122, 13);
            this.ReleaseDefinitionNamePrefixTargetLabel.TabIndex = 3;
            this.ReleaseDefinitionNamePrefixTargetLabel.Text = "Release definition prefix:";
            // 
            // BranchNameTargetTextBox
            // 
            this.BranchNameTargetTextBox.Location = new System.Drawing.Point(4, 50);
            this.BranchNameTargetTextBox.Name = "BranchNameTargetTextBox";
            this.BranchNameTargetTextBox.Size = new System.Drawing.Size(53, 20);
            this.BranchNameTargetTextBox.TabIndex = 2;
            this.BranchNameTargetTextBox.Leave += new System.EventHandler(this.BranchNameTargetTextBox_Leave);
            // 
            // BranchNameTargetLabel
            // 
            this.BranchNameTargetLabel.AutoSize = true;
            this.BranchNameTargetLabel.Location = new System.Drawing.Point(4, 34);
            this.BranchNameTargetLabel.Name = "BranchNameTargetLabel";
            this.BranchNameTargetLabel.Size = new System.Drawing.Size(41, 13);
            this.BranchNameTargetLabel.TabIndex = 1;
            this.BranchNameTargetLabel.Text = "Branch";
            // 
            // SourcePropertiesPanel
            // 
            this.SourcePropertiesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SourcePropertiesPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SourcePropertiesPanel.Controls.Add(this.SourceHeaderPanel);
            this.SourcePropertiesPanel.Controls.Add(this.ReleaseDefinitionNamePrefixSourceTextBox);
            this.SourcePropertiesPanel.Controls.Add(this.ReleaseDefinitionNamePrefixSourceLabel);
            this.SourcePropertiesPanel.Controls.Add(this.BranchNameSourceTextBox);
            this.SourcePropertiesPanel.Controls.Add(this.BranchNameSourceLabel);
            this.SourcePropertiesPanel.Location = new System.Drawing.Point(6, 31);
            this.SourcePropertiesPanel.Name = "SourcePropertiesPanel";
            this.SourcePropertiesPanel.Size = new System.Drawing.Size(463, 131);
            this.SourcePropertiesPanel.TabIndex = 6;
            // 
            // SourceHeaderPanel
            // 
            this.SourceHeaderPanel.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.SourceHeaderPanel.Controls.Add(this.SourceLabel);
            this.SourceHeaderPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.SourceHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.SourceHeaderPanel.Name = "SourceHeaderPanel";
            this.SourceHeaderPanel.Size = new System.Drawing.Size(461, 22);
            this.SourceHeaderPanel.TabIndex = 0;
            // 
            // SourceLabel
            // 
            this.SourceLabel.AutoSize = true;
            this.SourceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SourceLabel.Location = new System.Drawing.Point(3, 2);
            this.SourceLabel.Name = "SourceLabel";
            this.SourceLabel.Size = new System.Drawing.Size(59, 17);
            this.SourceLabel.TabIndex = 0;
            this.SourceLabel.Text = "Source";
            // 
            // ReleaseDefinitionNamePrefixSourceTextBox
            // 
            this.ReleaseDefinitionNamePrefixSourceTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReleaseDefinitionNamePrefixSourceTextBox.Location = new System.Drawing.Point(3, 99);
            this.ReleaseDefinitionNamePrefixSourceTextBox.Name = "ReleaseDefinitionNamePrefixSourceTextBox";
            this.ReleaseDefinitionNamePrefixSourceTextBox.Size = new System.Drawing.Size(456, 20);
            this.ReleaseDefinitionNamePrefixSourceTextBox.TabIndex = 4;
            // 
            // ReleaseDefinitionNamePrefixSourceLabel
            // 
            this.ReleaseDefinitionNamePrefixSourceLabel.AutoSize = true;
            this.ReleaseDefinitionNamePrefixSourceLabel.Location = new System.Drawing.Point(3, 83);
            this.ReleaseDefinitionNamePrefixSourceLabel.Name = "ReleaseDefinitionNamePrefixSourceLabel";
            this.ReleaseDefinitionNamePrefixSourceLabel.Size = new System.Drawing.Size(122, 13);
            this.ReleaseDefinitionNamePrefixSourceLabel.TabIndex = 3;
            this.ReleaseDefinitionNamePrefixSourceLabel.Text = "Release definition prefix:";
            // 
            // BranchNameSourceTextBox
            // 
            this.BranchNameSourceTextBox.Location = new System.Drawing.Point(3, 50);
            this.BranchNameSourceTextBox.Name = "BranchNameSourceTextBox";
            this.BranchNameSourceTextBox.Size = new System.Drawing.Size(53, 20);
            this.BranchNameSourceTextBox.TabIndex = 2;
            // 
            // BranchNameSourceLabel
            // 
            this.BranchNameSourceLabel.AutoSize = true;
            this.BranchNameSourceLabel.Location = new System.Drawing.Point(3, 34);
            this.BranchNameSourceLabel.Name = "BranchNameSourceLabel";
            this.BranchNameSourceLabel.Size = new System.Drawing.Size(44, 13);
            this.BranchNameSourceLabel.TabIndex = 1;
            this.BranchNameSourceLabel.Text = "Branch:";
            // 
            // ControlPanel
            // 
            this.ControlPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.CloneReleaseDefinitionProgressBar);
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Controls.Add(this.ApplyButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 510);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(566, 40);
            this.ControlPanel.TabIndex = 5;
            // 
            // CloneReleaseDefinitionProgressBar
            // 
            this.CloneReleaseDefinitionProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloneReleaseDefinitionProgressBar.Location = new System.Drawing.Point(379, 18);
            this.CloneReleaseDefinitionProgressBar.Name = "CloneReleaseDefinitionProgressBar";
            this.CloneReleaseDefinitionProgressBar.Size = new System.Drawing.Size(100, 10);
            this.CloneReleaseDefinitionProgressBar.TabIndex = 6;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(485, 11);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 1;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.HelpButton);
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(566, 25);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(538, 0);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 3;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(3, 1);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(526, 24);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "<sidekick name>";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.VsoBuild.Sidekicks.Help.chm";
            // 
            // ReleaseDefinitionCloneForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 550);
            this.Controls.Add(this.PropertiesPanel);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.ReleaseDefinitionSearchPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "ReleaseDefinitionCloneForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "AAATemplateAAAForm";
            this.ReleaseDefinitionSearchPanel.ResumeLayout(false);
            this.ReleaseDefinitionSearchPanel.PerformLayout();
            this.PropertiesPanel.ResumeLayout(false);
            this.DirectionToTargetPanel.ResumeLayout(false);
            this.DirectionToTargetPanel.PerformLayout();
            this.DirectionFromSourcePanel.ResumeLayout(false);
            this.DirectionFromSourcePanel.PerformLayout();
            this.TargetPropertiesPanel.ResumeLayout(false);
            this.TargetPropertiesPanel.PerformLayout();
            this.TargetHeaderPanel.ResumeLayout(false);
            this.TargetHeaderPanel.PerformLayout();
            this.SourcePropertiesPanel.ResumeLayout(false);
            this.SourcePropertiesPanel.PerformLayout();
            this.SourceHeaderPanel.ResumeLayout(false);
            this.SourceHeaderPanel.PerformLayout();
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel ReleaseDefinitionSearchPanel;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.Label ReleaseDefinitionSearchPlaceholderLabel;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.Panel DirectionToTargetPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel DirectionFromSourcePanel;
        private System.Windows.Forms.Label DirectionLabel;
        private System.Windows.Forms.Panel TargetPropertiesPanel;
        private System.Windows.Forms.Panel TargetHeaderPanel;
        private System.Windows.Forms.Label TargetLabel;
        private System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.TextBox ReleaseDefinitionNamePrefixTargetTextBox;
        private System.Windows.Forms.Label ReleaseDefinitionNamePrefixTargetLabel;
        private System.Windows.Forms.TextBox BranchNameTargetTextBox;
        private System.Windows.Forms.Label BranchNameTargetLabel;
        private System.Windows.Forms.Panel SourcePropertiesPanel;
        private System.Windows.Forms.Panel SourceHeaderPanel;
        private System.Windows.Forms.Label SourceLabel;
        private System.Windows.Forms.TextBox ReleaseDefinitionNamePrefixSourceTextBox;
        private System.Windows.Forms.Label ReleaseDefinitionNamePrefixSourceLabel;
        private System.Windows.Forms.TextBox BranchNameSourceTextBox;
        private System.Windows.Forms.Label BranchNameSourceLabel;
        private System.Windows.Forms.ProgressBar CloneReleaseDefinitionProgressBar;
        private System.Windows.Forms.Button HelpButton;
    }
}