﻿using System;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Exception form factory.
    /// </summary>
    public class ExceptionFormFactory
    {

        #region Private Constants
        #endregion

        #region Public Methods

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public static ExceptionForm CreateForm(Exception exception)
        {

            ExceptionForm result = null;
            ExceptionPresenter exceptionPresenter = null;
            string context = string.Empty;

            try
            {
                result = CreateForm(exception, context);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public static ExceptionForm CreateForm(Exception exception, string context)
        {

            ExceptionForm result = null;
            ExceptionPresenter exceptionPresenter = null;

            try
            {
                result = new ExceptionForm();

                exceptionPresenter = new ExceptionPresenter(result);
                exceptionPresenter.Exception = exception;
                exceptionPresenter.Context = context;
                result.ExceptionPresenter = exceptionPresenter;

                exceptionPresenter.Initialize();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


        #endregion

    }
}
