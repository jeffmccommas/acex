﻿using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public interface IReleaseDefinitionSearchView
    {

        string ReleaseDefinitionNamePattern { get; set; }

        void EnableControlsDependantOnTeamProject(bool enabled);

        void EnableControlsDependantOnSearch(bool enabled);

        void PerformSearch();

        void ClearSearch();

        void TeamProjectChanged();

        string BackgroundTaskStatus { get; set; }

        void UpdateReleaseDefinitionRetrievalStatus(int releaseDefinitionListRetrievedCount, int releaseDefinitionListTotalCount, bool cancelled);

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);

        ReleaseDefinitionSelectorList ReleaseDefinitionSelectorList { get; set; }

        ReleaseDefinitionSelectorList GetSelectedReleaseDefinitionSelectorList();

        int GetSelectedReleaseDefinitionSelectorCount();

        List<String> GetSelectedReleaseDefinitionSelectorListBranchNameList();

    }
}
