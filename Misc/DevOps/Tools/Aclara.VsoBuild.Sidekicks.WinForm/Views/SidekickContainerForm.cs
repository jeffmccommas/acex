﻿using Aclara.Sidekicks.AutoUpdater;
using Aclara.Tools.Configuration.DocumentConfig;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using Aclara.VsoBuild.Sidekicks.WinForm.Types;
using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class SidekickContainerForm : Form, ITeamProjectInfo, ISidekickCoaction
    {

        #region Private Constants

        private const string VSTS_Build_Domain = ".visualstudio.com";
        private const string VSTS_Release_Domain = ".vsrm.visualstudio.com";

        private const string SidekickContainerFormTitleSuffix = "Aclara Vso Build Sidekicks";

        private const string SidekickCategory_Builds = "Builds";
        private const string SidekickCategory_BuildDefinitions = "Build Definitions";
        private const string SidekickCategory_ReleaseDefintions = "Release Definitions";
        private const string SidekickCategory_Home = "Home";
        private const string SidekickCategory_Notification = "Notification";
        private const string SidekickCategory_Utility = "Utility";

        private const string UsageType_Restricted = "[Restricted Use]";
        private const string UsageType_GeneralUse = "[General Use]";

        private const string ChangeLogFileName = "ChangeLog.txt";

        private const string ShowLogToolStripButton_ShowLog = "Show Log";
        private const string ShowLogToolStripButton_HideLog = "Hide Log";

        private const string ExpandCollapseInfoPanel_Expand = "More";
        private const string ExpandCollapseInfoPanel_Collapse = "Less";

        private const string InfoPanelMessage_WarningsFound = "Warnings.";
        private const string InfoPanelMessage_WarningsNotFound = "No warnings.";

        private const int InfoPanelSizeOffset = 5;

        private const string LoggedInAs_Label = "Logged In As:";
        private const string TeamProjectCollectionUri_Label = "Team Project Collection:";
        private const string TeamProject_Label = "Team Project Name:";
        private const string TeamProjectSelectionPrompt = "Select Team Project to start.";

        private const string SidekicksAutoUpdater_NewVersionAvailableShort = "A new version is available.";
        private const string SidekicksAutoUpdater_UpdaterFailed = "Auto-updater failed.";
        private const string SidekicksAutoUpdater_UpToDate = "Sidekicks are up to date.";
        private const string SidekicksAutoUpdater_MoreInfo = "More info...";
        private const string SidekicksAutoUpdater_NewVersionAvailablePrompt = "A new version of Aclara.VsoBuild.Sidekicks is available. Update now?";
        private const string SidekicksAutoUpdater_CurrentVersionAlreadyInstalledPrompt = "Current version of Aclara.VsoBuild.Sidekicks is already installed.";

        private const string ApplicationExit_SidekickStillRunningForceExitPrompt = "The sidekicks listed below are busy. Cancel running sidekick requests or force application shutdown. \r\n\r\n {0} \r\n\r\nForce application shutdown?";

        private const string ValidationMessage_OnPremiseTFSNotSupported = "On-premise Team Foundation Server not supported.";

        private const string ShowLogFileErrorMessage_CouldNotLocateLogFile = "Could not locate log file.";
        private const string ShowChangeLogFileErrorMessage_CouldNotLocateLogFile = "Could not locate change log file.";

        private const string KeepAliveStatus_On = "Keep Alive: On";
        private const string KeepAliveStatus_Off = "Keep Alive: Off";

        private const int NextKeepAliveCheckIsDueInMinutes = 3;
        private const int NextAutoUpdaterCheckIsDueInMinutes = 15;
        private const int NextConnectTeamProjectCheckIsDueInSeconds = 15;
        private const int NextRetrieveRemainingDefinitionsCheckIsDueInMinutes = 1;

        private const string Information_SidekicksAutoUpdater = "Sidekicks Auto-Updater - Information";

        private const string KillSwitch_ConfigurationFileName = "killswitch.json";

        private const string ConnectTeamProjectToolStripButton_Tooltip_SidekicksBusy = "One or more Sidekick(s) running, cannot change connection at this time.";
        private const string ConnectTeamProjectToolStripButton_Tooltip_SidekicksReady = "Connect to Team Project";

        #endregion

        #region Private Data Members

        private LogForm _logForm = null;
        private SidekickConfiguration _sidekickConfiguration;
        private Uri _teamProjectCollectionUri;
        private Uri _teamProjectCollectionVsrmUri;
        private string _teamProjectName;
        private bool _haveCredentialsAndTeamProjectInfoBeenValidated;
        private string _sidekicksBootstrapperExeRootPath;
        private string _sidekicksBootstrapperExeName;
        private string _sidekicksAutoUpdaterVersion;
        private SidekicksAutoUpdater _sidekicksAutoUpdater;
        private SidekickViewList _sidekickViewList;
        private SidekickView _homeSidekickView;
        private bool _infoPanelSizeToggle;
        string _basicAuthRestAPIUserProfileName = string.Empty;
        string _basicAuthRestAPIPassword = string.Empty;
        private DateTime _autoUpdaterLastCheckDateTime;
        private DateTime _connectTeamProjectLastCheckDateTime;
        private DateTime _keepAliveLastCheckDateTime;
        private DateTime _retrieveRemainingDefinitionsLastCheckDateTime;
        private SidekickViewHistoryList _sidekickViewHistoryList;

        #endregion

        #region Public Delegates

        public event EventHandler<SidekickSelectionChangedEventArgs> SidekickSelectionChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Log form.
        /// </summary>
        public LogForm LogForm
        {
            get { return _logForm; }
            set { _logForm = value; }
        }

        /// <summary>
        /// Property: Team project collection uri.
        /// </summary>
        public Uri TeamProjectCollectionUri
        {
            get { return _teamProjectCollectionUri; }
            set
            {
                _teamProjectCollectionUri = value;
                //NOTE: TeamProjectCollectionVsrmUri is synchronized with TeamProjectCollectionUri.
                _teamProjectCollectionVsrmUri = new Uri(value.ToString().Replace(VSTS_Build_Domain, VSTS_Release_Domain));

            }
        }

        /// <summary>
        /// Property: Team project collection Vsrm uri.
        /// </summary>
        public Uri TeamProjectCollectionVsrmUri
        {
            get { return _teamProjectCollectionVsrmUri; }
            //NOTE: TeamProjectCollectionVsrmUri is synchronized with TeamProjectCollectionUri.
            set { }
        }

        /// <summary>
        /// Property: Team project name.
        /// </summary>
        public string TeamProjectName
        {
            get { return _teamProjectName; }
            set
            {
                _teamProjectName = value;
            }
        }

        /// <summary>
        /// Property: Credentials and team project info validate.
        /// </summary>
        public bool HaveCredentialsAndTeamProjectInfoBeenValidated
        {
            get { return _haveCredentialsAndTeamProjectInfoBeenValidated; }
            set { _haveCredentialsAndTeamProjectInfoBeenValidated = value; }
        }


        /// <summary>
        /// Property: Team project name.
        /// </summary>
        public string BasicAuthRestAPIUserProfileName
        {
            get { return _basicAuthRestAPIUserProfileName; }
            set
            {
                _basicAuthRestAPIUserProfileName = value;
            }
        }

        /// <summary>
        /// Property: Team project name.
        /// </summary>
        public string BasicAuthRestAPIPassword
        {
            get { return _basicAuthRestAPIPassword; }
            set
            {
                _basicAuthRestAPIPassword = value;
            }
        }
        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick view list.
        /// </summary>
        public SidekickViewList SidekickViewList
        {
            get { return _sidekickViewList; }
            set { _sidekickViewList = value; }
        }

        /// <summary>
        /// Property: Home sidekick view.
        /// </summary>
        public SidekickView HomeSidekickView
        {
            get { return _homeSidekickView; }
            set { _homeSidekickView = value; }
        }

        /// <summary>
        /// Property: Keep alive enabled.
        /// </summary>
        public bool KeepAliveEnabled
        {
            get { return KeepAliveSidekickContainerToolStripMenuItem.Checked; }
            set { KeepAliveSidekickContainerToolStripMenuItem.Checked = value; }
        }

        /// <summary>
        /// Property: Auto updater last check date time.
        /// </summary>
        public DateTime AutoUpdaterLastCheckDateTime
        {
            get { return _autoUpdaterLastCheckDateTime; }
            set { _autoUpdaterLastCheckDateTime = value; }
        }



        /// <summary>
        /// Property: Connect team project last check date time.
        /// </summary>
        public DateTime ConnectTeamProjectLastCheckDateTime
        {
            get { return _connectTeamProjectLastCheckDateTime; }
            set { _connectTeamProjectLastCheckDateTime = value; }
        }

        /// <summary>
        /// Property: Keep alive last check date time.
        /// </summary>
        public DateTime KeepAliveLastCheckDateTime
        {
            get { return _keepAliveLastCheckDateTime; }
            set { _keepAliveLastCheckDateTime = value; }
        }

        /// <summary>
        /// Property: Retrieve remaining build definitions last check date time.
        /// </summary>
        public DateTime RetrieveRemainingDefinitionsLastCheckDateTime
        {
            get { return _retrieveRemainingDefinitionsLastCheckDateTime; }
            set { _retrieveRemainingDefinitionsLastCheckDateTime = value; }
        }

        /// <summary>
        /// Property: Sidekickview history list.
        /// </summary>
        public SidekickViewHistoryList SidekickViewHistoryList
        {
            get { return _sidekickViewHistoryList; }
            set { _sidekickViewHistoryList = value; }
        }

        #endregion

        #region Protected Properties

        /// <summary>
        /// Property: Sidekicks bootstrapper executable root path.
        /// </summary>
        protected string SidekicksBootstrapperExeRootPath
        {
            get
            {
                try
                {
                    _sidekicksBootstrapperExeRootPath = Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksBootstrapperExeRootPath;

                }
                catch (Exception)
                {
                    throw;
                }
                return _sidekicksBootstrapperExeRootPath;
            }
        }

        /// <summary>
        /// Property: Sidekicks bootstrapper executable name.
        /// </summary>
        protected string SidekicksBootstrapperExeName
        {
            get
            {
                try
                {
                    _sidekicksBootstrapperExeName = Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksBootstrapperExeName;

                }
                catch (Exception)
                {
                    throw;
                }
                return _sidekicksBootstrapperExeName;
            }
        }

        /// <summary>
        /// Property: Sidekicks auto updater version.
        /// </summary>
        protected string SidekicksAutoUpdaterVersion
        {
            get
            {
                try
                {
                    _sidekicksAutoUpdaterVersion = Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksAutoUpdaterVersion;

                }
                catch (Exception)
                {
                    throw;
                }
                return _sidekicksAutoUpdaterVersion;
            }
        }

        /// <summary>
        /// Property: Sidekicks auto-updater.
        /// </summary>
        protected SidekicksAutoUpdater SidekicksAutoUpdater
        {
            get
            {
                if (_sidekicksAutoUpdater == null)
                {
                    _sidekicksAutoUpdater = new SidekicksAutoUpdater();
                }
                return _sidekicksAutoUpdater;
            }
            set { _sidekicksAutoUpdater = value; }
        }

        /// <summary>
        /// Property: Info panel size toggle.
        /// </summary>
        /// <remarks>
        /// 0 = Large.
        /// 1 = Small.
        /// </remarks>
        public bool InfoPanelSizeToggle
        {
            get { return _infoPanelSizeToggle; }
            set { _infoPanelSizeToggle = value; }
        }

        /// <summary>
        /// Property: Team project name.
        /// </summary>
        string ITeamProjectInfo.TeamProjectName
        {
            get
            {
                return _teamProjectName;
            }
        }

        /// <summary>
        /// Property: Team project collection uri.
        /// </summary>
        Uri ITeamProjectInfo.TeamProjectCollectionUri
        {
            get
            {
                return _teamProjectCollectionUri;
            }
        }

        /// <summary>
        /// Property: Basic authentication REST API user profile name.
        /// </summary>
        string ITeamProjectInfo.BasicAuthRestAPIUserProfileName
        {
            get
            {
                return _basicAuthRestAPIUserProfileName;
            }
        }

        /// <summary>
        /// Property: Basic authentication REST API password.
        /// </summary>
        string ITeamProjectInfo.BasicAuthRestAPIPassword
        {
            get
            {
                return _basicAuthRestAPIPassword;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekickContainerForm(string[] args)
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            SidekickConfigurationManager sidekickConfigurationManager = null;

            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionHandler);

            try
            {
                _sidekickViewList = new SidekickViewList();
                _sidekickViewHistoryList = new SidekickViewHistoryList();

                InitializeComponent();

                sidekickConfigurationManager = SidekickConfigurationFactory.CreateSidekickConfiguration("AppSettings.json", AppDomain.CurrentDomain.BaseDirectory);
                sidekickConfigurationManager.InitializeConfiguration();
                _sidekickConfiguration = sidekickConfigurationManager.SidekickConfiguration;

                InitializeViews();
                InitializeControls();

                this.PerformKillSwitchCheck();

                ConnectToTeamProject();

                if (args != null && args.Length > 0)
                {
                    this.HandleApplicationArguments(args);
                }

                this.RestoreFormSize();
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }

        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor .
        /// </summary>
        private SidekickContainerForm()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Check build status.
        /// </summary>
        /// <param name="buildDefinitionNameList"></param>
        public void CheckBuildStatus(List<string> buildDefinitionNameList)
        {
            ISidekickCoaction SidekickCoaction = null;

            try
            {
                var sidekickView = this.SidekickViewList.Single(skv => skv.Description == BuildStatusCheckForm.SidekickView_SidekickDescription);
                if (sidekickView == null)
                {
                    return;
                }
                this.ActivateSidekickView(sidekickView);
                SidekickCoaction = (ISidekickCoaction)sidekickView.Form;
                SidekickCoaction.CheckBuildStatus(buildDefinitionNameList);
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Request build.
        /// </summary>
        /// <param name="buildDefinitionNameList"></param>
        public void RequestBuild(List<string> buildDefinitionNameList)
        {
            ISidekickCoaction SidekickCoaction = null;
            try
            {
                var sidekickView = this.SidekickViewList.Single(skv => skv.Description == BuildDefinitionRequestBuildForm.SidekickView_SidekickDescription);
                if (sidekickView == null)
                {
                    return;
                }
                this.ActivateSidekickView(sidekickView);
                SidekickCoaction = (ISidekickCoaction)sidekickView.Form;
                SidekickCoaction.RequestBuild(buildDefinitionNameList);
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }


        /// <summary>
        /// Team project name changed.
        /// </summary>
        /// <param name="teamProjectCollectionUri"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="userName"></param>
        public void TeamProjectNameChanged()
        {
            ISidekickView sidekickView = null;

            try
            {
                this.LoggedInAsToolStripStatusLabel.Text = string.Format("{0} {1}",
                                                                 LoggedInAs_Label,
                                                                 this.BasicAuthRestAPIUserProfileName);

                this.TeamProjectCollectionUriToolStripStatusLabel.Text = string.Format("{0} {1}",
                                                                                       TeamProjectCollectionUri_Label,
                                                                                       this.TeamProjectCollectionUri);

                this.TeamProjectNameToolStripStatusLabel.Text = string.Format("{0} {1}",
                                                                              TeamProject_Label,
                                                                              this.TeamProjectName);

                this.ConnectTeamProjectToolStripButton.BackColor = Color.LightSteelBlue;

                this.VisualStudioOnlineToolStripSplitButton.Enabled = true;

                foreach (SidekickView sidekickViewItem in this.SidekickViewList)
                {
                    sidekickView = (ISidekickView)sidekickViewItem.Form;
                    sidekickView.TeamProjectChanged();
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Activate sidekick view.
        /// </summary>
        /// <param name="sidekickViewList"></param>
        public void ActivateSidekickView(SidekickView sidekickView)
        {
            SidekickSelectionChangedEventArgs sidekickSelectionChangedEventArgs = null;

            try
            {
                ChangeSidekickContainerFormTitle(sidekickView);

                UpdateSplitButtonSidekicksToolStripMenuItemChecked(sidekickView);

                UpdateViewSidekicksToolStripMenuItemChecked(sidekickView);

                this.SidekickContainerPanel.DockControl(sidekickView.Form);

                if (SidekickSelectionChanged != null)
                {
                    sidekickSelectionChangedEventArgs = new SidekickSelectionChangedEventArgs(sidekickView.Name, sidekickView.Description);
                    this.SidekickSelectionChanged(this, sidekickSelectionChangedEventArgs);
                }

                this.SidekickViewHistoryList.Add(sidekickView);
                this.UpdateBackAndForwardButton();
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

        #region Protected Methods


        /// <summary>
        /// Update back and forward buttons.
        /// </summary>
        protected void UpdateBackAndForwardButton()
        {

            try
            {
                if (this.SidekickViewHistoryList.IsBackAvailable() == true)
                {
                    this.BackToolStripButton.Enabled = true;
                }
                else
                {
                    this.BackToolStripButton.Enabled = false;
                }

                if (this.SidekickViewHistoryList.IsForwardAvailable() == true)
                {
                    this.ForwardToolStripButton.Enabled = true;
                }
                else
                {
                    this.ForwardToolStripButton.Enabled = false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Move mouse one pixel.
        /// </summary>
        protected void MoveMouseOnePixel()
        {
            Point pos = System.Windows.Forms.Cursor.Position;

            System.Windows.Forms.Cursor.Position = new System.Drawing.Point(pos.X + 1, pos.Y);
        }

        /// <summary>
        /// Handle application arguments.
        /// </summary>
        /// <param name="args"></param>
        protected void HandleApplicationArguments(string[] args)
        {
            List<string> sidekickCoActionMajorMinorList = null;
            List<string> sidekickCoActionParameterList = null;
            List<string> parameterBuildDefinitionList = null;
            List<string> buildDefinitionNameList = null;
            try
            {
                sidekickCoActionMajorMinorList = args[0].Split('~').ToList();

                if (sidekickCoActionMajorMinorList[0].Contains("CheckBuildStatus") == true)
                {
                    sidekickCoActionParameterList = sidekickCoActionMajorMinorList[1].Split('=').ToList();
                    if (sidekickCoActionParameterList[0].Contains("BuildDefinitionList") == true)
                    {
                        parameterBuildDefinitionList = sidekickCoActionParameterList[0].Split('|').ToList();
                        buildDefinitionNameList = parameterBuildDefinitionList[1].Split(',').ToList();
                        this.CheckBuildStatus(buildDefinitionNameList);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Initialize views.
        /// </summary>
        protected void InitializeViews()
        {
            BuildDefinitionSearchForm buildDefinitionSearchForm = null;
            ReleaseDefinitionSearchForm releaseDefinitionSearchForm = null;
            bool defaultView = false;
            Form sidekickForm = null;
            ISidekickInfo sidekickInfo = null;

            try
            {
                this.LogForm = LogFormFactory.CreateForm(this);
                this.LogForm.TopLevel = false;

                InitializeLogWriter();

                //Add build definition change queue status view.
                defaultView = false;
                buildDefinitionSearchForm = BuildDefinitionSearchFormFactory.CreateForm((ITeamProjectInfo)this, this.SidekickConfiguration, this);
                sidekickForm = BuildDefinitionChangeQueueStatusFormFactory.CreateForm((ITeamProjectInfo)this,
                                                                                       this.SidekickConfiguration,
                                                                                       buildDefinitionSearchForm);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_BuildDefinitions, SidekickCategory_BuildDefinitions)));
                //Add build definition change settings view.
                defaultView = false;
                buildDefinitionSearchForm = BuildDefinitionSearchFormFactory.CreateForm((ITeamProjectInfo)this, this.SidekickConfiguration, this);
                sidekickForm = BuildDefinitionChangeSettingsFormFactory.CreateForm((ITeamProjectInfo)this,
                                                                                   this.SidekickConfiguration,
                                                                                   buildDefinitionSearchForm);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_BuildDefinitions, SidekickCategory_BuildDefinitions)));
                //Add build status check view.
                defaultView = false;
                buildDefinitionSearchForm = BuildDefinitionSearchFormFactory.CreateForm((ITeamProjectInfo)this, this.SidekickConfiguration, this);
                sidekickForm = BuildStatusCheckFormFactory.CreateForm((ITeamProjectInfo)this,
                                                                      this.SidekickConfiguration,
                                                                      buildDefinitionSearchForm,
                                                                      this);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_Builds, SidekickCategory_Builds)));
                //Add build definition clone view.
                defaultView = false;
                buildDefinitionSearchForm = BuildDefinitionSearchFormFactory.CreateForm((ITeamProjectInfo)this, this.SidekickConfiguration, this);
                sidekickForm = BuildDefinitionCloneFormFactory.CreateForm((ITeamProjectInfo)this,
                                                                          this.SidekickConfiguration,
                                                                          buildDefinitionSearchForm);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_BuildDefinitions, SidekickCategory_BuildDefinitions)));
                //Add build definition delete view.
                defaultView = false;
                buildDefinitionSearchForm = BuildDefinitionSearchFormFactory.CreateForm((ITeamProjectInfo)this, this.SidekickConfiguration, this);
                sidekickForm = BuildDefinitionDeleteFormFactory.CreateForm((ITeamProjectInfo)this,
                                                                           this.SidekickConfiguration,
                                                                           buildDefinitionSearchForm);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_BuildDefinitions, SidekickCategory_BuildDefinitions)));
                //Add build definition request build view.
                defaultView = false;
                buildDefinitionSearchForm = BuildDefinitionSearchFormFactory.CreateForm((ITeamProjectInfo)this, this.SidekickConfiguration, this);
                sidekickForm = BuildDefinitionRequestBuildFormFactory.CreateForm((ITeamProjectInfo)this,
                                                                           this.SidekickConfiguration,
                                                                           buildDefinitionSearchForm,
                                                                           this);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_Builds, SidekickCategory_Builds)));
                //Add release definition clone view.
                defaultView = false;
                releaseDefinitionSearchForm = ReleaseDefinitionSearchFormFactory.CreateForm((ITeamProjectInfo)this, this.SidekickConfiguration, this);
                sidekickForm = ReleaseDefinitionCloneFormFactory.CreateForm((ITeamProjectInfo)this,
                                                                           this.SidekickConfiguration,
                                                                           releaseDefinitionSearchForm);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_ReleaseDefintions, SidekickCategory_ReleaseDefintions)));
                //Add release definition delete view.
                defaultView = false;
                releaseDefinitionSearchForm = ReleaseDefinitionSearchFormFactory.CreateForm((ITeamProjectInfo)this, this.SidekickConfiguration, this);
                sidekickForm = ReleaseDefinitionDeleteFormFactory.CreateForm((ITeamProjectInfo)this,
                                                                             this.SidekickConfiguration,
                                                                             releaseDefinitionSearchForm);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_ReleaseDefintions, SidekickCategory_ReleaseDefintions)));

                //Add notification admin view.
                defaultView = false;
                buildDefinitionSearchForm = BuildDefinitionSearchFormFactory.CreateForm((ITeamProjectInfo)this, this.SidekickConfiguration, this);
                sidekickForm = NotificationAdminFormFactory.CreateForm((ITeamProjectInfo)this,
                                                                       this.SidekickConfiguration,
                                                                       buildDefinitionSearchForm);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_Notification, SidekickCategory_Notification)));

                //Add export/import definition view.
                defaultView = false;
                buildDefinitionSearchForm = BuildDefinitionSearchFormFactory.CreateForm((ITeamProjectInfo)this, this.SidekickConfiguration, this);
                releaseDefinitionSearchForm = ReleaseDefinitionSearchFormFactory.CreateForm((ITeamProjectInfo)this, this.SidekickConfiguration, this);
                sidekickForm = DefinitionExportImportFormFactory.CreateForm((ITeamProjectInfo)this,
                                                                            this.SidekickConfiguration,
                                                                            buildDefinitionSearchForm,
                                                                            releaseDefinitionSearchForm);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_Utility, SidekickCategory_Utility)));

                //--------------------------------------------------------------------------------------------------------------------------------------------------
                //NOTE: Home sidekick view is added after all other sidekick views inorder to render sidekick view navigation.
                //--------------------------------------------------------------------------------------------------------------------------------------------------

                //Add home sidekick view.
                defaultView = true;
                sidekickForm = HomeFormFactory.CreateForm(this, this.SidekickConfiguration);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.HomeSidekickView = new SidekickView(sidekickInfo.SidekickName,
                                                         sidekickInfo.SidekickDescription,
                                                         sidekickForm,
                                                         defaultView,
                                                         new SidekickCategory(SidekickCategory_Home, SidekickCategory_Home));
                this.SidekickViewList.Add(this.HomeSidekickView);

                this.SidekickContainerPanel.DockControl(this.SidekickViewList.First().Form);

                this.LogPanel.DockControl(this.LogForm);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            DocumentConfigList documentConfigList = null;

            try
            {
                //Populate auto-updater state in UI components.
                PopulateUIComponentsWithAutoUpdaterState();

                //Populate document list in UI components.
                PopulateUIComponentsWithDocumentList();

                //Populate sidekick view list in UI components.
                PopulateUIComponentsWithSidekickViewList(this.SidekickViewList);

            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Populate UI components with auto-updater state.
        /// </summary>
        protected void PopulateUIComponentsWithAutoUpdaterState()
        {
            try
            {
                this.AutoUpdaterToolStripLabel.Visible = false;
                this.AutoUpdaterShowChangeLogToolStripButton.Visible = false;
                this.AutoUpdaterToolStripButton.Visible = false;


                //Retrieve sidekicks auto-updater.
                this.SidekicksAutoUpdater = this.GetSidekicksAutoUpdater();

                if (this.SidekicksAutoUpdater.Status.RunState == RunState.OK &&
                    this.SidekicksAutoUpdater.IsNewVersionAvailable(this.SidekicksAutoUpdaterVersion) == true)
                {
                    //New sidekicks auto-updater version is available.
                    this.AutoUpdaterToolStripLabel.Visible = true;
                    this.AutoUpdaterShowChangeLogToolStripButton.Visible = true;
                    this.AutoUpdaterToolStripButton.Visible = true;
                    this.AutoUpdaterToolStripLabel.Text = SidekicksAutoUpdater_NewVersionAvailableShort;
                }
                else if (this.SidekicksAutoUpdater.Status.RunState == RunState.OK &&
                         this.SidekicksAutoUpdater.IsNewVersionAvailable(this.SidekicksAutoUpdaterVersion) == false)
                {
                    //Auto-updater version up-to-date.
                    this.AutoUpdaterToolStripLabel.Visible = true;
                    this.AutoUpdaterShowChangeLogToolStripButton.Visible = false;
                    this.AutoUpdaterToolStripButton.Visible = false;
                    this.AutoUpdaterToolStripLabel.Text = SidekicksAutoUpdater_UpToDate;
                }

                //Sidekicks auto-updater failed.
                if (this.SidekicksAutoUpdater.Status.RunState == RunState.Failed)
                {
                    this.AutoUpdaterToolStripLabel.Visible = true;
                    this.AutoUpdaterShowChangeLogToolStripButton.Visible = true;
                    this.AutoUpdaterToolStripButton.Visible = true;
                    this.AutoUpdaterToolStripButton.Text = SidekicksAutoUpdater_MoreInfo;
                    this.AutoUpdaterToolStripLabel.Text = SidekicksAutoUpdater_UpdaterFailed;
                }

            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Populate UI components with document list.
        /// </summary>
        protected void PopulateUIComponentsWithDocumentList(DocumentConfigList documentConfigList)
        {
            const string DocumentNamePrefix = "Document";
            int documentNameIndex = 0;
            ToolStripMenuItem helpMenuToolStripMenuItem = null;
            ToolStripMenuItem toolStripSplitButtonMenuToolStripMenuItem = null;

            try
            {
                //Iterate through document list.
                foreach (DocumentConfig documentConfig in documentConfigList)
                {
                    documentNameIndex++;

                    //Create and populate menu item - help.documents menu.
                    helpMenuToolStripMenuItem = new ToolStripMenuItem();
                    helpMenuToolStripMenuItem.Name = string.Format("{0}{1}",
                                                  DocumentNamePrefix, documentNameIndex);
                    helpMenuToolStripMenuItem.Text = documentConfig.Name;
                    helpMenuToolStripMenuItem.ToolTipText = documentConfig.FileName;

                    helpMenuToolStripMenuItem.Click += new EventHandler(DocumentToolStripMenuItem_Click);

                    this.DocumentsSidekickContainerToolStripMenuItem.DropDownItems.Add(helpMenuToolStripMenuItem);

                    //Create and populate menu item - tool strip split button documents.
                    toolStripSplitButtonMenuToolStripMenuItem = new ToolStripMenuItem();
                    toolStripSplitButtonMenuToolStripMenuItem.Name = string.Format("{0}{1}",
                                                                          DocumentNamePrefix, documentNameIndex);
                    toolStripSplitButtonMenuToolStripMenuItem.Text = documentConfig.Name;
                    toolStripSplitButtonMenuToolStripMenuItem.ToolTipText = documentConfig.FileName;

                    toolStripSplitButtonMenuToolStripMenuItem.Click += new EventHandler(DocumentToolStripMenuItem_Click);

                    this.DocumentToolStripSplitButton.DropDownItems.Add(toolStripSplitButtonMenuToolStripMenuItem);

                }

            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }


        /// <summary>
        /// Populate UI components with document list.
        /// </summary>
        protected void PopulateUIComponentsWithDocumentList()
        {
            const string DocumentNamePrefix = "Document";
            int documentNameIndex = 0;
            ToolStripMenuItem helpMenuToolStripMenuItem = null;
            ToolStripMenuItem toolStripSplitButtonMenuToolStripMenuItem = null;

            try
            {
                //Iterate through document list.
                foreach (var documentConfig in this.SidekickConfiguration.Documents.Document.Where(doc => doc.Visible == "true"))
                {
                    documentNameIndex++;

                    //Create and populate menu item - help.documents menu.
                    helpMenuToolStripMenuItem = new ToolStripMenuItem();
                    helpMenuToolStripMenuItem.Name = string.Format("{0}{1}",
                                                  DocumentNamePrefix, documentNameIndex);
                    helpMenuToolStripMenuItem.Text = documentConfig.Name;
                    helpMenuToolStripMenuItem.ToolTipText = documentConfig.FileName;

                    helpMenuToolStripMenuItem.Click += new EventHandler(DocumentToolStripMenuItem_Click);

                    this.DocumentsSidekickContainerToolStripMenuItem.DropDownItems.Add(helpMenuToolStripMenuItem);

                    //Create and populate menu item - tool strip split button documents.
                    toolStripSplitButtonMenuToolStripMenuItem = new ToolStripMenuItem();
                    toolStripSplitButtonMenuToolStripMenuItem.Name = string.Format("{0}{1}",
                                                                          DocumentNamePrefix, documentNameIndex);
                    toolStripSplitButtonMenuToolStripMenuItem.Text = documentConfig.Name;
                    toolStripSplitButtonMenuToolStripMenuItem.ToolTipText = documentConfig.FileName;

                    toolStripSplitButtonMenuToolStripMenuItem.Click += new EventHandler(DocumentToolStripMenuItem_Click);

                    this.DocumentToolStripSplitButton.DropDownItems.Add(toolStripSplitButtonMenuToolStripMenuItem);

                }

            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Populate UI components with sidekick view list.
        /// </summary>
        protected void PopulateUIComponentsWithSidekickViewList(SidekickViewList sidekickViewList)
        {
            SidekickView defaultSidekickView = null;
            ToolStripMenuItem splitButtonParentToolStripMenuItem = null;
            ToolStripMenuItem splitButtonChildToolStripMenuItem = null;
            ToolStripMenuItem viewMenuParentToolStripMenuItem = null;
            ToolStripMenuItem viewMenuChildToolStripMenuItem = null;

            try
            {
                //Populate view menu items and sidekick selector split button.
                var categories = sidekickViewList.OrderBy(skv => skv.SidekickCategory.Description).Select(skv => skv.SidekickCategory);
                foreach (var category in categories.DistinctBy(c => c.Description))
                {
                    splitButtonParentToolStripMenuItem = new ToolStripMenuItem();
                    splitButtonParentToolStripMenuItem.Name = category.Name;
                    splitButtonParentToolStripMenuItem.Text = category.Description;
                    splitButtonParentToolStripMenuItem.ToolTipText = category.Description;
                    this.MainSidekickSelectorContextMenuStrip.Items.Add(splitButtonParentToolStripMenuItem);

                    viewMenuParentToolStripMenuItem = new ToolStripMenuItem();
                    viewMenuParentToolStripMenuItem.Name = category.Name;
                    viewMenuParentToolStripMenuItem.Text = category.Description;
                    viewMenuParentToolStripMenuItem.ToolTipText = category.Description;
                    this.SidekicksSidekickContainerToolStripMenuItem.DropDownItems.Add(viewMenuParentToolStripMenuItem);

                    foreach (var sidekickView in sidekickViewList.Where(skv => skv.SidekickCategory.Name == category.Name).OrderBy(skv => skv.SidekickCategory))
                    {
                        splitButtonChildToolStripMenuItem = new ToolStripMenuItem();
                        splitButtonChildToolStripMenuItem.Name = sidekickView.Name;
                        splitButtonChildToolStripMenuItem.Text = sidekickView.Description;
                        splitButtonChildToolStripMenuItem.ToolTipText = sidekickView.Description;
                        splitButtonChildToolStripMenuItem.Click += new EventHandler(ViewSidekicksSidekickViewToolStripMenuItem_Click);
                        splitButtonParentToolStripMenuItem.DropDownItems.Add(splitButtonChildToolStripMenuItem);

                        viewMenuChildToolStripMenuItem = new ToolStripMenuItem();
                        viewMenuChildToolStripMenuItem.Name = sidekickView.Name;
                        viewMenuChildToolStripMenuItem.Text = sidekickView.Description;
                        viewMenuChildToolStripMenuItem.ToolTipText = sidekickView.Description;
                        viewMenuChildToolStripMenuItem.Click += new EventHandler(ViewSidekicksSidekickViewToolStripMenuItem_Click);
                        viewMenuParentToolStripMenuItem.DropDownItems.Add(viewMenuChildToolStripMenuItem);

                    }
                }

                //Set default sidekick view.
                defaultSidekickView = sidekickViewList.Single(skv => skv.DefaultView == true);
                ActivateSidekickView(defaultSidekickView);
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Show sidekick view history list context menu.
        /// </summary>
        protected void ShowSidekickViewHistoryListContextMenu()
        {
            List<SidekickViewHistory> sidekickViewHistories = null;
            ToolStripMenuItem toolStripMenuItem = null;

            try
            {

                sidekickViewHistories = this.SidekickViewHistoryList.GetSidekickViewHistoryList();

                var sortedSidekickViewHistoryList = sidekickViewHistories.OrderBy(skv => skv.SequenceNumber);

                this.SidekickViewHistoryContextMenuStrip.Items.Clear();
                foreach (SidekickViewHistory category in sortedSidekickViewHistoryList)
                {
                    toolStripMenuItem = new ToolStripMenuItem();
                    toolStripMenuItem.Name = category.SidekickView.Name;
                    toolStripMenuItem.Text = category.SidekickView.Description;
                    toolStripMenuItem.ToolTipText = category.SidekickView.Description;
                    toolStripMenuItem.Click += new EventHandler(ViewSidekicksSidekickViewToolStripMenuItem_Click);

                    this.SidekickViewHistoryContextMenuStrip.Items.Add(toolStripMenuItem);

                }

                this.SidekickViewHistoryContextMenuStrip.Show(Cursor.Position);
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Retrieve sidekicks auto-updater.
        /// </summary>
        protected SidekicksAutoUpdater GetSidekicksAutoUpdater()
        {

            SidekicksAutoUpdater result = null;
            SidekicksAutoUpdaterManager sidekicksAutoUpdaterManager = null;
            string bootstrapperExeRootPath = string.Empty;
            string bootstrapperExeName = string.Empty;

            try
            {
                sidekicksAutoUpdaterManager = SidekicksAutoUpdaterManagerFactory.CreateSidekicksUpdateManager();

                //Retrieve sidekicks auto-updater.
                bootstrapperExeRootPath = this.SidekicksBootstrapperExeRootPath;
                bootstrapperExeName = this.SidekicksBootstrapperExeName;
                result = sidekicksAutoUpdaterManager.GetSidekicksAutoUpdater(this.SidekicksBootstrapperExeRootPath,
                                                                            bootstrapperExeName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Initialize log writer.
        /// </summary>
        protected void InitializeLogWriter()
        {
            //target
            ConfigurationItemFactory.Default.Targets
                                    .RegisterDefinition("LogEventTarget", typeof(Aclara.VsoBuild.Sidekicks.WinForm.Logging.LogEventTarget));
            var target = (LogEventTarget)LogManager.Configuration.FindTargetByName("logevent");
            target.LogEntryCreated += this.LogForm.LogEntryCreate;

        }

        /// <summary>
        /// Update split button - sidekicks tool strip menu item checked.
        /// </summary>
        /// <param name="sidekickView"></param>
        protected void UpdateSplitButtonSidekicksToolStripMenuItemChecked(SidekickView sidekickView)
        {
            try
            {
                foreach (ToolStripMenuItem parentToolStripMenuItem in this.MainSidekickSelectorToolStripSplitButton.DropDownItems)
                {
                    foreach (ToolStripMenuItem childToolStripMenitem in parentToolStripMenuItem.DropDownItems)
                        if (sidekickView.Name == childToolStripMenitem.Name)
                        {
                            childToolStripMenitem.Checked = true;
                        }
                        else
                        {
                            childToolStripMenitem.Checked = false;
                        }

                }
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Update view - sidekicks tool strip menu item checked.
        /// </summary>
        /// <param name="sidekickView"></param>
        protected void UpdateViewSidekicksToolStripMenuItemChecked(SidekickView sidekickView)
        {
            try
            {
                foreach (ToolStripMenuItem parentToolStripMenuItem in this.SidekicksSidekickContainerToolStripMenuItem.DropDownItems)
                {
                    foreach (ToolStripMenuItem childToolStripMenitem in parentToolStripMenuItem.DropDownItems)
                        if (sidekickView.Name == childToolStripMenitem.Name)
                        {
                            childToolStripMenitem.Checked = true;
                        }
                        else
                        {
                            childToolStripMenitem.Checked = false;
                        }

                }
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Change sidekick container form title.
        /// </summary>
        /// <param name="sidekickName"></param>
        protected void ChangeSidekickContainerFormTitle(SidekickView sidekickView)
        {
            try
            {
                this.Text = string.Format("{0} - {1}",
                                  sidekickView.Description,
                                  SidekickContainerFormTitleSuffix);
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Retrieve busy sidekick list.
        /// </summary>
        /// <returns></returns>
        protected List<string> GetBusySidekickList()
        {
            List<string> result = null;
            ISidekickView sidekickView = null;
            ISidekickInfo sidekickInfo = null;

            try
            {
                result = new List<string>();

                foreach (SidekickView sidekickViewItem in this.SidekickViewList)
                {
                    sidekickView = (ISidekickView)sidekickViewItem.Form;
                    sidekickInfo = (ISidekickInfo)sidekickViewItem.Form;

                    if (sidekickView.IsSidekickBusy == true)
                    {
                        result.Add(sidekickInfo.SidekickDescription);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve remaining build defintions.
        /// </summary>
        protected void RetrieveRemainingDefinitions()
        {
            ISidekickView sidekickView = null;

            try
            {

                foreach (SidekickView sidekickViewItem in this.SidekickViewList)
                {
                    sidekickView = (ISidekickView)sidekickViewItem.Form;
                    if (sidekickView.IsSidekickBusy == false)
                    {
                        sidekickView.RetrieveRemainingDefinitions();

                    }

                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Connect to team project.
        /// </summary>
        protected void ConnectToTeamProject()
        {
            DialogResult dialogResult;
            string teamProjectCollectionVsrmUrl = string.Empty;

            try
            {

                this.HaveCredentialsAndTeamProjectInfoBeenValidated = false;
                if (AreCredentialsAndTeamProjectInfoValid() == false)
                {
                    dialogResult = PromptTeamProjectAndCredentialsExtended();
                    if (dialogResult != DialogResult.OK)
                    {
                        return;
                    }
                }

                this.TeamProjectCollectionUri = new Uri(Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectCollection);
                this.TeamProjectName = Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectName;
                this.BasicAuthRestAPIUserProfileName = Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksUserProfileName;
                this.BasicAuthRestAPIPassword = GetAndDecryptPersonalAccessToken();

                this.HaveCredentialsAndTeamProjectInfoBeenValidated = true;

                this.TeamProjectNameChanged();

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Determine whether credentials and team project info are valid.
        /// </summary>
        /// <returns></returns>
        protected bool AreCredentialsAndTeamProjectInfoValid()
        {
            bool result = false;
            try
            {
                if (string.IsNullOrEmpty(Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectCollection) == true ||
                    string.IsNullOrEmpty(Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectName) == true ||
                    string.IsNullOrEmpty(Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksUserProfileName) ||
                    string.IsNullOrEmpty(GetAndDecryptPersonalAccessToken()) == true)
                {
                    result = false;
                    return result;
                }
                else
                {
                    result = ValidateCredentials();
                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Prompt for team project and credentials (Extended).
        /// </summary>
        /// <returns></returns>
        protected DialogResult PromptTeamProjectAndCredentialsExtended()
        {
            DialogResult result = DialogResult.Cancel;
            VSTSProjectConnectForm vstsProjectConnectForm = null;
            Uri teamProjectCollectionUri;
            string teamProjectName = string.Empty;
            string basicAuthRestAPIUserProfileName = string.Empty;
            string basicAuthRestAPIPassword = string.Empty;
            string teamProjectCollectionDefault = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectCollection) == true)
                {
                    teamProjectCollectionDefault = this.SidekickConfiguration.VSTSAccountItemList.VSTSAccountItem.Where(vai => vai.Default == true).FirstOrDefault().URL;
                    teamProjectCollectionUri = new Uri(teamProjectCollectionDefault);
                }
                else
                {
                    teamProjectCollectionUri = new Uri(Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectCollection);
                }

                teamProjectName = Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectName;
                basicAuthRestAPIUserProfileName = Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksUserProfileName;
                basicAuthRestAPIPassword = GetAndDecryptPersonalAccessToken();

                vstsProjectConnectForm = VSTSProjectConnectFormFactory.CreateForm(teamProjectCollectionUri, teamProjectName, basicAuthRestAPIUserProfileName, basicAuthRestAPIPassword);

                vstsProjectConnectForm.StartPosition = FormStartPosition.CenterParent;
                result = vstsProjectConnectForm.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    this.TeamProjectCollectionUri = vstsProjectConnectForm.TeamProjectCollectionUri;
                    this.TeamProjectName = vstsProjectConnectForm.TeamProjectName;
                    this.BasicAuthRestAPIUserProfileName = vstsProjectConnectForm.BasicAuthRestAPIUserProfileName;
                    this.BasicAuthRestAPIPassword = vstsProjectConnectForm.BasicAuthRestAPIPassword;

                    Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectCollection = vstsProjectConnectForm.TeamProjectCollectionUri.ToString();
                    Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectName = vstsProjectConnectForm.TeamProjectName;
                    Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksUserProfileName = vstsProjectConnectForm.BasicAuthRestAPIUserProfileName;
                    this.EncryptAndSavePersonalAccessToken(vstsProjectConnectForm.BasicAuthRestAPIPassword);

                }

            }
            catch (Exception)
            {
            }

            return result;
        }

        /// <summary>
        /// Validate credentials.
        /// </summary>
        /// <returns></returns>
        protected bool ValidateCredentials()
        {

            bool result = false;
            VSTSProjectConnectForm vstsProjectConnectForm = null;
            Uri teamProjectCollectionUri;
            string teamProjectName = string.Empty;
            string basicAuthRestAPIUserProfileName = string.Empty;
            string basicAuthRestAPIPassword = string.Empty;
            string teamProjectCollectionDefault = string.Empty;

            try
            {

                if (string.IsNullOrEmpty(Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectCollection) == true)
                {
                    teamProjectCollectionDefault = this.SidekickConfiguration.VSTSAccountItemList.VSTSAccountItem.Where(vai => vai.Default == true).FirstOrDefault().URL;
                    teamProjectCollectionUri = new Uri(teamProjectCollectionDefault);
                }
                else
                {
                    teamProjectCollectionUri = new Uri(Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectCollection);
                }

                teamProjectName = Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectName;
                basicAuthRestAPIUserProfileName = Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksUserProfileName;
                basicAuthRestAPIPassword = GetAndDecryptPersonalAccessToken();

                vstsProjectConnectForm = VSTSProjectConnectFormFactory.CreateForm(teamProjectCollectionUri, teamProjectName, basicAuthRestAPIUserProfileName, basicAuthRestAPIPassword);

                vstsProjectConnectForm.ValidateCredentials();
                if (vstsProjectConnectForm.CredentialsValidationStatus == VSTSProjectConnectForm.CredentialsValidationStatusCode.CredentialsAreValid)
                {
                    result = true;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;



        }


        /// <summary>
        /// Encrypt and save personal access token.
        /// </summary>
        /// <param name="personalAccessToken"></param>
        /// <returns></returns>
        protected void EncryptAndSavePersonalAccessToken(string personalAccessToken)
        {
            string encryptedPersonalAccessToken = string.Empty;
            ProtectedDataManager protectedDataManager = null;

            try
            {
                protectedDataManager = new ProtectedDataManager();

                encryptedPersonalAccessToken = protectedDataManager.EncryptString(personalAccessToken);

                //Persist personal access token.
                Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksPersonalAccessToken = encryptedPersonalAccessToken;
                Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.Save();

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve and decrypt personal access token.
        /// </summary>
        /// <param name="personalAccessToken"></param>
        /// <returns></returns>
        protected string GetAndDecryptPersonalAccessToken()
        {
            string result = string.Empty;
            string encryptedPersonalAccessToken = string.Empty;
            ProtectedDataManager protectedDataManager = null;

            try
            {
                protectedDataManager = new ProtectedDataManager();

                //Retrieve personal access token.
                encryptedPersonalAccessToken = Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekicksPersonalAccessToken;

                try
                {
                    //Decrypt personal access token.
                    result = protectedDataManager.DecryptString(encryptedPersonalAccessToken);
                }
                catch (Exception)
                {
                    //Unable to decrypt.
                    result = string.Empty;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Retrieve remaining build definitions scheduled by timer.
        /// </summary>
        protected void RetrieveRemainingDefinitionsScheduledByTimer()
        {
            TimeSpan timeSinceLastCheckTimeSpan;

            try
            {
                timeSinceLastCheckTimeSpan = (this.RetrieveRemainingDefinitionsLastCheckDateTime - DateTime.Now).Duration();

                if (timeSinceLastCheckTimeSpan.TotalMinutes < NextRetrieveRemainingDefinitionsCheckIsDueInMinutes)
                {
                    return;
                }
                else
                {
                    this.RetrieveRemainingDefinitionsLastCheckDateTime = DateTime.Now;
                }

                //User credentials and/or team project info has not been valided.
                if (this.HaveCredentialsAndTeamProjectInfoBeenValidated == false)
                {
                    return;
                }

                this.RetrieveRemainingDefinitions();

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Connect team project scheduled by timer.
        /// </summary>
        protected void ConnectTeamProjectScheduledByTimer()
        {
            string busySidekicks = string.Empty;
            List<string> busySidekickList = null;
            string message = string.Empty;
            TimeSpan timeSinceLastCheckTimeSpan;

            try
            {
                timeSinceLastCheckTimeSpan = (this.ConnectTeamProjectLastCheckDateTime - DateTime.Now).Duration();

                if (timeSinceLastCheckTimeSpan.TotalSeconds < NextConnectTeamProjectCheckIsDueInSeconds)
                {
                    return;
                }
                else
                {
                    this.ConnectTeamProjectLastCheckDateTime = DateTime.Now;
                }

                //User credentials and/or team project info has not been valided.
                if (this.HaveCredentialsAndTeamProjectInfoBeenValidated == false)
                {
                    return;
                }

                //Retrieve busy sidekick list.
                busySidekickList = GetBusySidekickList();

                //Sidekick is still running. Not safe to exit application.
                if (busySidekickList.Count > 0)
                {
                    if (this.ConnectTeamProjectToolStripButton.Enabled == true)
                    {
                        this.ConnectTeamProjectToolStripButton.Enabled = false;
                        this.ConnectTeamProjectToolStripButton.ToolTipText = ConnectTeamProjectToolStripButton_Tooltip_SidekicksBusy;
                    }
                }
                else
                {
                    if (this.ConnectTeamProjectToolStripButton.Enabled == false)
                    {
                        this.ConnectTeamProjectToolStripButton.Enabled = true;
                        this.ConnectTeamProjectToolStripButton.ToolTipText = ConnectTeamProjectToolStripButton_Tooltip_SidekicksReady;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Check auto-updater scheduled by timer.
        /// </summary>
        protected void CheckAutoUpdaterScheduledByTimer()
        {
            TimeSpan timeSinceLastCheckTimeSpan;

            try
            {
                timeSinceLastCheckTimeSpan = (this.AutoUpdaterLastCheckDateTime - DateTime.Now).Duration();

                if (timeSinceLastCheckTimeSpan.TotalMinutes < NextAutoUpdaterCheckIsDueInMinutes)
                {
                    return;
                }
                else
                {
                    this.AutoUpdaterLastCheckDateTime = DateTime.Now;
                }

                //User credentials and/or team project info has not been valided.
                if (this.HaveCredentialsAndTeamProjectInfoBeenValidated == false)
                {
                    return;
                }

                this.PopulateUIComponentsWithAutoUpdaterState();

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Check keep alive scheduled by timer.
        /// </summary>
        protected void KeepAliveScheduledByTimer()
        {
            TimeSpan timeSinceLastCheckTimeSpan;

            try
            {
                timeSinceLastCheckTimeSpan = (this.KeepAliveLastCheckDateTime - DateTime.Now).Duration();

                if (timeSinceLastCheckTimeSpan.TotalMinutes < NextKeepAliveCheckIsDueInMinutes)
                {
                    return;
                }
                else
                {
                    this.KeepAliveLastCheckDateTime = DateTime.Now;
                }

                //User credentials and/or team project info has not been valided.
                if (this.HaveCredentialsAndTeamProjectInfoBeenValidated == false)
                {
                    return;
                }

                if (this.KeepAliveEnabled == true)
                {
                    this.KeepAliveStatusToolStripStatusLabel.Text = KeepAliveStatus_On;
                    this.MoveMouseOnePixel();
                }
                else
                {
                    this.KeepAliveStatusToolStripStatusLabel.Text = KeepAliveStatus_Off;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Restore form size.
        /// </summary>
        protected void RestoreFormSize()
        {
            int width = 0;
            int height = 0;

            try
            {


                if (Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekickContainerFormSizeWidth >= this.MinimumSize.Width &&
                    Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekickContainerFormSizeHeight >= this.MinimumSize.Height)
                {
                    width = Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekickContainerFormSizeWidth;
                    height = Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekickContainerFormSizeHeight;
                    this.Size = new Size(width, height);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Save form size.
        /// </summary>
        protected void SaveFormSize()
        {
            try
            {

                if (this.Width >= this.MinimumSize.Width &&
                    this.Height >= this.MinimumSize.Height)
                {
                    Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekickContainerFormSizeWidth = this.Width;
                    Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.SidekickContainerFormSizeHeight = this.Height;
                    Aclara.VsoBuild.Sidekicks.WinForm.Properties.Settings.Default.Save();
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Perform kill switch check.
        /// </summary>
        protected void PerformKillSwitchCheck()
        {
            KillSwitchConfiguration killSwitchConfiguration = null;
            KillSwitchConfigurationManager killSwitchConfigurationManager = null;
            Document document = null;

            KillSwitchNotifyForm killSwitchNotifyForm = null;
            KillSwitch killSwitch = null;
            KillSwitchRunStatus killSwitchRunStatus;

            try
            {
                document = this.SidekickConfiguration.Documents.Document.Where(doc => doc.FileName == KillSwitch_ConfigurationFileName).SingleOrDefault();
                if (document == null)
                {
                    return;
                }

                killSwitchConfigurationManager = KillSwitchConfigurationFactory.CreateSidekickConfiguration(document.FileName, document.Path);
                killSwitchConfigurationManager.InitializeConfiguration();
                killSwitchConfiguration = killSwitchConfigurationManager.KillSwitchConfiguration;
                if (killSwitchConfiguration == null)
                {
                    return;
                }
                if (killSwitchConfiguration.KillSwitches == null)
                {
                    return;
                }

                killSwitch = killSwitchConfiguration.KillSwitches.KillSwitch.Where(ks => ks.VersionImpacted == this.SidekicksAutoUpdaterVersion).SingleOrDefault();
                if (killSwitch == null)
                {
                    return;
                }

                //Kill switch found.
                Enum.TryParse(killSwitch.RunStatus, out killSwitchRunStatus);
                this.SidekicksAutoUpdater = this.GetSidekicksAutoUpdater();
                killSwitchNotifyForm = KillSwitchNotifyFormFactory.CreateForm(SidekickContainerFormTitleSuffix,
                                                                              this.SidekicksAutoUpdaterVersion,
                                                                              killSwitch.Reason,
                                                                              killSwitchRunStatus,
                                                                              this.SidekicksAutoUpdater);
                killSwitchNotifyForm.ShowDialog();

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods 

        /// <summary>
        /// Unhandled exception - handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        static private void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
            exceptionForm.ShowDialog();
        }

        /// <summary>
        /// Event Handler: Exit application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitSidekickContainerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Event Handler: About <app> click.</app>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AboutSidekickContainerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBoxForm aboutBox = null;

            try
            {
                aboutBox = new AboutBoxForm();

                aboutBox.Show(this);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Document menu item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string stdout = string.Empty;
            string stderr = string.Empty;
            List<Document> documents = null;
            string documentFilePath = string.Empty;

            try
            {

                ToolStripMenuItem menuItem = (ToolStripMenuItem)sender;
                documents = this.SidekickConfiguration.Documents.Document;

                foreach (Document documentConfig in documents.Where(doc => doc.Visible == "true"))
                {
                    if (documentConfig.Name == menuItem.Text)
                    {
                        documentFilePath = Path.Combine(documentConfig.Path, documentConfig.FileName);

                        System.Diagnostics.Process.Start(documentFilePath);
                        break;
                    }

                }

            }
            catch (Exception ex)
            {
                string context = string.Format("Document path: {0}",
                                               documentFilePath);
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex, context);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: SidekickContainerForm - Form closing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SidekickContainerForm_FormClosing(object sender, FormClosingEventArgs e)
        {

            string busySidekicks = string.Empty;
            List<string> busySidekickList = null;
            string message = string.Empty;

            try
            {
                this.SaveFormSize();

                //Retrieve busy sidekick list.
                busySidekickList = GetBusySidekickList();

                //Sidekick is still running. Not safe to exit application.
                if (busySidekickList.Count > 0)
                {

                    busySidekicks = string.Join(Environment.NewLine, busySidekickList.ToArray());
                    message = String.Format(ApplicationExit_SidekickStillRunningForceExitPrompt,
                                            busySidekicks);
                    if (MessageBox.Show(this,
                                        message,
                                        SidekickContainerFormTitleSuffix,
                                        MessageBoxButtons.YesNoCancel,
                                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        Application.Exit();
                    }
                    else
                    {
                        e.Cancel = true;
                    }


                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Expand/collapse info panel picture box - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExpandCollapseInfoPanelPictureBox_Click(object sender, EventArgs e)
        {

            try
            {
                if (this.InfoPanelSizeToggle == true)
                {
                    this.InfoPanelSizeToggle = false;
                }
                else
                {
                    this.InfoPanelSizeToggle = true;
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        private void SidekickContainerStatusStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        /// <summary>
        /// Event Handler: Auto-updater tool strip button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoUpdaterToolStripButton_Click(object sender, EventArgs e)
        {
            string stdout = string.Empty;
            string stderr = string.Empty;

            try
            {
                if (this.SidekicksAutoUpdater.Status.RunState == RunState.OK ||
                   this.SidekicksAutoUpdater.Status.RunState == RunState.Warning)
                {
                    this.SidekicksAutoUpdater.Run(out stdout, out stderr);

                    Application.Exit();
                }
                if (this.SidekicksAutoUpdater.Status.RunState == RunState.Failed)
                {
                    ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(this.SidekicksAutoUpdater.Status.Exception, this.SidekicksAutoUpdater.AutoUpdaterInfo);
                    exceptionForm.ShowDialog();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Show log tool strip button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowLogToolStripButton_Click(object sender, EventArgs e)
        {

            ToolStripButton toolStripButton = null;

            try
            {
                if (sender is ToolStripButton)
                {
                    toolStripButton = (ToolStripButton)sender;
                    if (toolStripButton.Checked == true)
                    {
                        this.LogPanel.Hide();
                        toolStripButton.Checked = false;
                        this.ShowLogToolStripMenuItem.Checked = false;
                        toolStripButton.Text = ShowLogToolStripButton_ShowLog;
                    }
                    else
                    {
                        this.LogPanel.Show();
                        toolStripButton.Checked = true;
                        this.ShowLogToolStripMenuItem.Checked = true;
                        toolStripButton.Text = ShowLogToolStripButton_HideLog;
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Help tool strip button Clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpToolStripButton_Click(object sender, EventArgs e)
        {

            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, this.SidekickHelpProvider.GetHelpKeyword(this));

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Help menu item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpSidekickContainerToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, this.SidekickHelpProvider.GetHelpKeyword(this));

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Change log menu item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeLogSidekickContainerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string changeLogFilePath = string.Empty;
            string executablePath = string.Empty;

            try
            {
                executablePath = Path.GetDirectoryName(Application.ExecutablePath);
                changeLogFilePath = Path.Combine(executablePath, ChangeLogFileName);

                System.Diagnostics.Process.Start(changeLogFilePath);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Connect team project tool strip button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConnectTeamProjectToolStripButton_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = DialogResult.Cancel;

            try
            {
                dialogResult = PromptTeamProjectAndCredentialsExtended();
                if (dialogResult != DialogResult.OK)
                {
                    return;
                }

                TeamProjectNameChanged();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Show log tool strip menu item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowLogToolStripMenuItem_Click(object sender, EventArgs e)
        {

            ToolStripMenuItem toolStripMenuItem = null;

            try
            {
                if (sender is ToolStripMenuItem)
                {
                    toolStripMenuItem = (ToolStripMenuItem)sender;
                    if (toolStripMenuItem.Checked == true)
                    {
                        this.LogPanel.Hide();
                        toolStripMenuItem.Checked = false;
                        this.ShowLogToolStripButton.Text = ShowLogToolStripButton_ShowLog;

                    }
                    else
                    {
                        this.LogPanel.Show();
                        toolStripMenuItem.Checked = true;
                        this.ShowLogToolStripButton.Text = ShowLogToolStripButton_HideLog;
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Check for new version menu item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void CheckForNewVersionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string stdout = string.Empty;
            string stderr = string.Empty;
            try
            {
                //Retrieve sidekicks auto-updater.
                this.SidekicksAutoUpdater = this.GetSidekicksAutoUpdater();



                //A new sidekicks auto-updater version is available.
                if (this.SidekicksAutoUpdater.Status.RunState == RunState.OK &&
                    this.SidekicksAutoUpdater.IsNewVersionAvailable(this.SidekicksAutoUpdaterVersion) == true)
                {
                    if (MessageBox.Show(SidekicksAutoUpdater_NewVersionAvailablePrompt,
                                        SidekickContainerFormTitleSuffix,
                                        MessageBoxButtons.YesNoCancel,
                                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        this.SidekicksAutoUpdater.Run(out stdout, out stderr);
                        Application.Exit();
                    }
                }
                else if (this.SidekicksAutoUpdater.Status.RunState == RunState.Failed)
                {
                    ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(this.SidekicksAutoUpdater.Status.Exception, this.SidekicksAutoUpdater.AutoUpdaterInfo);
                    exceptionForm.ShowDialog();
                }
                else
                {
                    MessageBox.Show(SidekicksAutoUpdater_CurrentVersionAlreadyInstalledPrompt,
                                    SidekickContainerFormTitleSuffix,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Visual studio online builds tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VisualStudioOnlineBuildsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string link = string.Empty;
            string teamProjectCollectionUri = string.Empty;

            try
            {
                teamProjectCollectionUri = this.TeamProjectCollectionUri.AbsoluteUri;

                if (teamProjectCollectionUri.EndsWith("/") == false)
                {
                    teamProjectCollectionUri += @"/";
                }

                link = string.Format("{0}DefaultCollection/{1}/_build/index?path=%5C&_a=allDefinitions",
                                     teamProjectCollectionUri,
                                     this.TeamProjectName);
                System.Diagnostics.Process.Start(link);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Visual studio online queued builds tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VisualStudioOnlineQueuedBuildsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string link = string.Empty;
            string teamProjectCollectionUri = string.Empty;

            try
            {
                teamProjectCollectionUri = this.TeamProjectCollectionUri.AbsoluteUri;

                if (teamProjectCollectionUri.EndsWith("/") == false)
                {
                    teamProjectCollectionUri += @"/";
                }

                link = string.Format("{0}DefaultCollection/{1}/_build/index?_a=queued",
                                     teamProjectCollectionUri,
                                     this.TeamProjectName);
                System.Diagnostics.Process.Start(link);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Shwo log file tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowLogFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileTarget fileTarget = null;
            string logFileName = string.Empty;
            string logFilePath = string.Empty;
            string executablePath = string.Empty;
            LogEventInfo logEventInfo = null;

            try
            {
                //Retrive NLog FileTarget from configuration.
                fileTarget = (FileTarget)LogManager.Configuration.FindTargetByName("LogFile");

                if (fileTarget == null)
                {
                    MessageBox.Show(string.Format(ShowLogFileErrorMessage_CouldNotLocateLogFile));
                    return;
                }

                logEventInfo = new LogEventInfo();
                logFileName = fileTarget.FileName.Render(logEventInfo);

                if (logFileName == null)
                {
                    MessageBox.Show(string.Format(ShowLogFileErrorMessage_CouldNotLocateLogFile));
                    return;
                }

                executablePath = Path.GetDirectoryName(Application.ExecutablePath);
                logFilePath = Path.Combine(executablePath, logFileName);

                if (File.Exists(logFilePath) == false)
                {
                    MessageBox.Show(string.Format(ShowLogFileErrorMessage_CouldNotLocateLogFile));
                    return;
                }

                System.Diagnostics.Process.Start(logFilePath);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: View sidekicks - sidekick view menu item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewSidekicksSidekickViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = null;

            try
            {
                menuItem = (ToolStripMenuItem)sender;
                var sidekickView = this.SidekickViewList.Single(skv => skv.Description == menuItem.Text);
                if (sidekickView == null)
                {
                    return;
                }
                this.ActivateSidekickView(sidekickView);

            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Home tool strip button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HomeToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.ActivateSidekickView(this.HomeSidekickView);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Main sidekick selector tool strip split button - Button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainSidekickSelectorToolStripSplitButton_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                this.MainSidekickSelectorToolStripSplitButton.ShowDropDown();
            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Sidekick timer - Tick.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SidekickTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                this.RetrieveRemainingDefinitionsScheduledByTimer();
                this.ConnectTeamProjectScheduledByTimer();
                this.CheckAutoUpdaterScheduledByTimer();
                this.KeepAliveScheduledByTimer();
            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Show change log tool strip button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoUpdaterShowChangeLogToolStripButton_Click(object sender, EventArgs e)
        {
            string logFileName = string.Empty;
            string logFilePath = string.Empty;

            try
            {
                logFilePath = Path.Combine(this.SidekicksAutoUpdater.ChangeLogFilePath, this.SidekicksAutoUpdater.ChangeLogFileName);

                if (File.Exists(logFilePath) == false)
                {
                    MessageBox.Show(string.Format(ShowChangeLogFileErrorMessage_CouldNotLocateLogFile));
                    return;
                }

                System.Diagnostics.Process.Start(logFilePath);

            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Back tool strip button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackToolStripButton_Click(object sender, EventArgs e)
        {
            SidekickViewHistory sidekickViewHistory = null;

            try
            {
                if (this.SidekickViewHistoryList.IsBackAvailable() == true)
                {
                    sidekickViewHistory = this.SidekickViewHistoryList.Back();
                    this.ActivateSidekickView(sidekickViewHistory.SidekickView);
                }

            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Back tool strip button - Mouse down.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackToolStripButton_MouseDown(object sender, MouseEventArgs e)
        {

            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    this.ShowSidekickViewHistoryListContextMenu();
                }
            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Forward tool strip button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ForwardToolStripButton_Click(object sender, EventArgs e)
        {
            SidekickViewHistory sidekickViewHistory = null;

            try
            {
                if (this.SidekickViewHistoryList.IsForwardAvailable() == true)
                {
                    sidekickViewHistory = this.SidekickViewHistoryList.Forward();
                    this.ActivateSidekickView(sidekickViewHistory.SidekickView);
                }
            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Forward tool strip button - Mouse down.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ForwardToolStripButton_MouseDown(object sender, MouseEventArgs e)
        {

            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    this.ShowSidekickViewHistoryListContextMenu();
                }
            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Sidekicks auto-updater info tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void SidekicksAutoUpdaterInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                InfoForm infoForm = null;

                try
                {
                    infoForm = new InfoForm();
                    infoForm.Information = Types.Information.AutoUpdater;
                    infoForm.SidekicksAutoUpdater = this.SidekicksAutoUpdater;
                    infoForm.Text = Information_SidekicksAutoUpdater;
                    infoForm.ShowDialog();
                }
                catch (Exception ex)
                {
                    ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                    exceptionForm.ShowDialog();
                }
            }
        }

        #endregion

    }

    /// <summary>
    /// Panel control extensions.
    /// </summary>
    public static class PanelExtensions
    {
        /// <summary>
        /// Dock control.
        /// </summary>
        /// <param name="thisControl"></param>
        /// <param name="controlToDock"></param>
        public static void DockControl(this Panel thisControl,
                                       Control controlToDock)
        {
            thisControl.Controls.Clear();
            thisControl.Controls.Add(controlToDock);
            controlToDock.Dock = DockStyle.Fill;
            controlToDock.Show();
        }
    }
}
