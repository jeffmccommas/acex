﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Notification preview form factory.
    /// </summary>
    public class NotificationPreviewFormFactory
    {

        #region Private Constants
        #endregion

        #region Public Methods

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public static NotificationPreviewForm CreateForm()
        {

            NotificationPreviewForm result = null;

            try
            {
                result = CreateForm(string.Empty);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public static NotificationPreviewForm CreateForm(string notificationPreviewText)
        {

            NotificationPreviewForm result = null;
            NotificationPreviewPresenter basicAuthRestAPIPresenter = null;

            try
            {
                result = new NotificationPreviewForm();

                basicAuthRestAPIPresenter = new NotificationPreviewPresenter(result);

                result.NotificationPreviewPresenter = basicAuthRestAPIPresenter;
                basicAuthRestAPIPresenter.Initialize();
                result.NotificationPreviewText = notificationPreviewText;


            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

    }
}
