﻿using Aclara.Vso.Build.Client.Models;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using System;
using System.Windows.Forms;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class BuildDefinitionChangeSettingsForm : Form, IBuildDefinitionChangeSettingsView, ISidekickView, ISidekickInfo
    {

        #region Private Constants

        private const string SidekickView_SidekickName = "ChangeBuildDefinitionsSettings";
        private const string SidekickView_SidekickDescription = "Change Build Definitions Settings";

        private const string ValidationErrorMessage_TeamProjectNameNotSelected = "Team project not selected.";
        private const string ValidationErrorMessage_BranchNameSourceNotEntered = "Branch name source not entered.";
        private const string ValidationErrorMessage_BuildDefinitionNotSelected = "Build definition not selected.";
        private const string ValidationErrorMessage_SourceBranchNameNotEntered = "Branch name - source not entered.";
        private const string ValidationErrorMessage_SourceBuildDefinitionNamePrefixNotEntered = "Build definition name prefix - source not entered.";
        private const string ValidationErrorMessage_TargetBranchNameNotEntered = "Branch name - target not entered.";
        private const string ValidationErrorMessage_TargetBuildDefinitionNamePrefixNotEntered = "Build definition name prefix - target not entered.";
        private const string ValidationErrorMessage_SelectedBuildDefinitionsMustHaveSameNamePrefix = "Selected build definitons must have same name prefix.";

        private const string BuildDefinitionSettings_Unspecified = "<Select Setting>";
        private const string BuildDefinitionSettings_DefaultAgentPoolQueue = "Default Agent Pool Queue";
        private const string BuildDefinitionSettings_TriggersSchedule = "Schedule Trigger";
        private const string BuildDefinitionSettings_TriggersContinuousIntegration = "Continuous Integration Trigger";

        private const string TimeZone_EasterStandardTime = "Eastern Standard Time";

        #endregion

        #region Private Data Members

        private SidekickConfiguration _sidekickConfiguration = null;
        private BuildDefinitionSearchForm _buildDefinitionSearchForm = null;
        private BuildDefinitionChangeSettingsPresenter _BuildDefinitionChangeSettingsPresenter;
        private string _defaultAgentQueueName;
        private AgentPoolQueueSelectorList _agentPoolQueueSelectorList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickView_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickView_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                if (this.BuildDefinitionChangeSettingsPresenter.IsBackgroundTaskBusy == false)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick warning level.
        /// </summary>
        public SidekickContainerTypes.SidekickWarningLevel SidekickWarningLevel
        {
            get
            {
                return SidekickContainerTypes.SidekickWarningLevel.Low;
            }
        }

        /// <summary>
        /// Property: Build defintion change settings presenter.
        /// </summary>
        public BuildDefinitionChangeSettingsPresenter BuildDefinitionChangeSettingsPresenter
        {
            get { return _BuildDefinitionChangeSettingsPresenter; }
            set { _BuildDefinitionChangeSettingsPresenter = value; }
        }

        /// <summary>
        /// Property: Build definition search form.
        /// </summary>
        public BuildDefinitionSearchForm BuildDefinitionSearchForm
        {
            get { return _buildDefinitionSearchForm; }
            set { _buildDefinitionSearchForm = value; }
        }

        /// <summary>
        /// Property: Default queue agent name.
        /// </summary>
        public string DefaultAgentQueueName
        {
            get { return _defaultAgentQueueName; }
            set { _defaultAgentQueueName = value; }
        }

        /// <summary>
        /// Property: Agent pool queue selector list.
        /// </summary>
        public AgentPoolQueueSelectorList AgentPoolQueueSelectorList
        {
            get { return _agentPoolQueueSelectorList; }
            set { _agentPoolQueueSelectorList = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get { return this.ApplyButton.Text; }
            set { this.ApplyButton.Text = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildDefinitionChangeSettingsForm()
        {
            InitializeComponent();


        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        public BuildDefinitionChangeSettingsForm(SidekickConfiguration sidekickConfiguration,
                                                    BuildDefinitionSearchForm buildDefinitionSearchForm)
        {
            InitializeComponent();

            this.SidekickConfiguration = sidekickConfiguration;
            this.InitializeControls();
            this.BuildDefinitionSearchForm = buildDefinitionSearchForm;
            this.BuildDefinitionSearchForm.BuildDefinitionSelectionChanged += OnBuildDefinitionSelectionChanged;
            this.BuildDefinitionSearchPanel.DockControl(this.BuildDefinitionSearchForm);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update progress of change queue status.
        /// </summary>
        /// <param name="buildDefinitionChangedCount"></param>
        /// <param name="buildDefinitionTotalCount"></param>
        public void UpdateProgressChangeSetting(int buildDefinitionChangedCount, int buildDefinitionTotalCount)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<int, int>(this.UpdateProgressChangeSetting), buildDefinitionChangedCount, buildDefinitionTotalCount);
            }
            else
            {
                if (buildDefinitionChangedCount < buildDefinitionTotalCount)
                {
                    this.ChangeSettingProgressBar.Visible = true;
                    this.ChangeSettingProgressBar.Minimum = 0;
                    this.ChangeSettingProgressBar.Maximum = buildDefinitionTotalCount;
                    this.ChangeSettingProgressBar.Value = buildDefinitionChangedCount;
                }
                else if (buildDefinitionChangedCount == buildDefinitionTotalCount)
                {
                    this.ChangeSettingProgressBar.Visible = false;
                }
            }
        }
        /// <summary>
        /// Populate Settings.
        /// </summary>
        public void PopulateSettings()
        {
            this.DefaultAgentPoolQueueComboBox.DataSource = this.AgentPoolQueueSelectorList;
            this.DefaultAgentPoolQueueComboBox.DisplayMember = "Name";
            this.DefaultAgentPoolQueueComboBox.ValueMember = "Id";
        }

        /// <summary>
        /// Retrieve remaining build definitions.
        /// </summary>
        public void RetrieveRemainingDefinitions()
        {
            this.BuildDefinitionSearchForm.RetrieveRemainingDefinitions();
        }

        /// <summary>
        /// Apply button enable.
        /// </summary>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            this.BuildDefinitionSearchForm.TeamProjectChanged();
            this.AgentPoolQueueSelectorList = this.BuildDefinitionChangeSettingsPresenter.GetDefaultAgentQueueSelectors();
            this.PopulateSettings();

        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task Completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {
                this.SettingComboBox.FormattingEnabled = true;
                this.SettingComboBox.Format += delegate (object sender, ListControlConvertEventArgs e)
                {
                    e.Value = ConvertBuildDefinitionSettingsToText((BuildDefinitionSettingsGroup)e.Value);
                };
                this.SettingComboBox.DataSource = Enum.GetValues(typeof(BuildDefinitionSettingsGroup));

                this.SettingComboBox.SelectedItem = BuildDefinitionSettingsGroup.DefaultAgentPoolQueue;

                this.TimeZoneComboBox.Items.Add(this.TimeZoneComboBox);
                this.TimeZoneComboBox.Text = TimeZone_EasterStandardTime;

                EnableControlsDependantOnSelectedBuildDefinition(false);

                this.ChangeSettingProgressBar.Visible = false;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        // Convert build definition settings enum value to text.
        /// </summary>
        /// <param name="dateFilter"></param>
        /// <returns></returns>
        protected string ConvertBuildDefinitionSettingsToText(BuildDefinitionSettingsGroup buildDefinitionSettings)
        {
            string result = string.Empty;

            try
            {
                switch (buildDefinitionSettings)
                {
                    case BuildDefinitionSettingsGroup.Unspecified:
                        result = BuildDefinitionSettings_Unspecified;
                        break;
                    case BuildDefinitionSettingsGroup.DefaultAgentPoolQueue:
                        result = BuildDefinitionSettings_DefaultAgentPoolQueue;
                        break;
                    case BuildDefinitionSettingsGroup.TriggersSchedule:
                        result = BuildDefinitionSettings_TriggersSchedule;
                        break;
                    case BuildDefinitionSettingsGroup.TriggersContinousIntegration:
                        result = BuildDefinitionSettings_TriggersContinuousIntegration;
                        break;
                    default:
                        result = BuildDefinitionSettings_Unspecified;
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: Build definition selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="requestBuildEventArgs"></param>
        protected void OnBuildDefinitionSelectionChanged(Object sender,
                                                         BuildDefinitionSelectionChangedEventArgs buildDefinitionSelectionChangedEventArgs)
        {

            try
            {
                if (buildDefinitionSelectionChangedEventArgs != null)
                {
                    if (buildDefinitionSelectionChangedEventArgs.OneOrMoreSelected == true)
                    {
                        EnableControlsDependantOnSelectedBuildDefinition(true);
                    }
                    else
                    {
                        EnableControlsDependantOnSelectedBuildDefinition(false);
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Enable/disable controls dependant on at lease one build definition selected.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnSelectedBuildDefinition(bool enable)
        {
            try
            {
                this.SettingComboBox.Enabled = enable;

                this.DefaultAgentPoolQueueComboBox.Enabled = enable;
                this.RefreshAgentPoolQueueButton.Enabled = enable;

                this.HourNumericUpDown.Enabled = enable;
                this.MinuteNumericUpDown.Enabled = enable;
                this.TimeZoneComboBox.Enabled = enable;
                this.SundayCheckBox.Enabled = enable;
                this.MondayCheckBox.Enabled = enable;
                this.TuesdayCheckBox.Enabled = enable;
                this.WednesdayCheckBox.Enabled = enable;
                this.ThursdayCheckBox.Enabled = enable;
                this.FridayCheckBox.Enabled = enable;
                this.SaturdayCheckBox.Enabled = enable;
                this.SelectAllDaysButton.Enabled = enable;
                this.SelectNoDaysButton.Enabled = enable;

                this.ApplyButton.Enabled = enable;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Retrieve build definition settings from user interface controls.
        /// </summary>
        /// <param name="buildDefinitionSettingsGroup"></param>
        /// <returns></returns>
        protected BuildDefinitionSettings GetBuildDefinitionSettings(BuildDefinitionSettingsGroup buildDefinitionSettingsGroup)
        {
            BuildDefinitionSettings result = null;
            int agentPoolQueueId = 0;
            AgentPoolQueueSelector agentPoolQueueSelector;
            DefaultAgentPoolQueueSettings defaultAgentPoolQueueSettings = null;
            TriggerSettingsSchedule triggerSettingsSchedule = null;
            TriggerSettingsContinuousIntegration triggerSettingsContinuousIntegration = null;

            try
            {

                switch (buildDefinitionSettingsGroup)
                {
                    case BuildDefinitionSettingsGroup.DefaultAgentPoolQueue:

                        if (this.DefaultAgentPoolQueueComboBox.SelectedItem is AgentPoolQueueSelector)
                        {
                            agentPoolQueueSelector = (AgentPoolQueueSelector)this.DefaultAgentPoolQueueComboBox.SelectedItem;
                            agentPoolQueueId = agentPoolQueueSelector.Id;
                            defaultAgentPoolQueueSettings = new DefaultAgentPoolQueueSettings();
                            defaultAgentPoolQueueSettings.AgentPoolQueueId = agentPoolQueueId;

                            result = new BuildDefinitionSettings(defaultAgentPoolQueueSettings);
                        }

                        break;

                    case BuildDefinitionSettingsGroup.TriggersSchedule:

                        triggerSettingsSchedule = new TriggerSettingsSchedule();

                        triggerSettingsSchedule.Hours = (int)this.HourNumericUpDown.Value;
                        triggerSettingsSchedule.Minutes = (int)this.MinuteNumericUpDown.Value;
                        triggerSettingsSchedule.TimeZoneId = this.TimeZoneComboBox.Text;

                        triggerSettingsSchedule.DaystoBuildSchedule.Monday = this.MondayCheckBox.Checked;
                        triggerSettingsSchedule.DaystoBuildSchedule.Tuesday = this.TuesdayCheckBox.Checked;
                        triggerSettingsSchedule.DaystoBuildSchedule.Wednesday = this.WednesdayCheckBox.Checked;
                        triggerSettingsSchedule.DaystoBuildSchedule.Thursday = this.ThursdayCheckBox.Checked;
                        triggerSettingsSchedule.DaystoBuildSchedule.Friday = this.FridayCheckBox.Checked;
                        triggerSettingsSchedule.DaystoBuildSchedule.Saturday = this.SaturdayCheckBox.Checked;
                        triggerSettingsSchedule.DaystoBuildSchedule.Sunday = this.SundayCheckBox.Checked;

                        triggerSettingsSchedule.TeamProjectName = this.BuildDefinitionChangeSettingsPresenter.TeamProjectInfo.TeamProjectName;

                        triggerSettingsSchedule.AllowAddSchedule = this.AllowAddScheduleCheckBox.Checked;

                        result = new BuildDefinitionSettings(triggerSettingsSchedule);

                        break;

                    case BuildDefinitionSettingsGroup.TriggersContinousIntegration:

                        triggerSettingsContinuousIntegration = new TriggerSettingsContinuousIntegration();

                        result = new BuildDefinitionSettings(triggerSettingsContinuousIntegration);

                        break;

                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Build definition settings group unexpected. (Build definition settings group: {0}",
                                                                            buildDefinitionSettingsGroup));
                }


            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelectorList buildDefinitionSelectorList = null;
            BuildDefinitionSettingsGroup buildDefinitionSettingsGroup = BuildDefinitionSettingsGroup.Unspecified;
            BuildDefinitionSettings buildDefinitionSettings = null;

            try
            {

                if (this.BuildDefinitionChangeSettingsPresenter.TeamProjectInfo.TeamProjectCollectionUri == null ||
                    this.BuildDefinitionChangeSettingsPresenter.TeamProjectInfo.TeamProjectName == string.Empty)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_TeamProjectNameNotSelected));
                    return;
                }

                buildDefinitionSelectorList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();
                if (buildDefinitionSelectorList == null ||
                    buildDefinitionSelectorList.Count <= 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_BuildDefinitionNotSelected));
                    return;
                }

                this.ChangeSettingProgressBar.Visible = false;

                if (this.SettingComboBox.SelectedItem is BuildDefinitionSettingsGroup)
                {
                    buildDefinitionSettingsGroup = (BuildDefinitionSettingsGroup)this.SettingComboBox.SelectedItem;
                }

                buildDefinitionSettings = GetBuildDefinitionSettings(buildDefinitionSettingsGroup);

                this.BuildDefinitionChangeSettingsPresenter.ChangeBuildDefinitionsSettings(buildDefinitionSelectorList,
                                                                                           buildDefinitionSettings);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Setting combobox - selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            BuildDefinitionSettingsGroup buildDefinitionSettings = BuildDefinitionSettingsGroup.Unspecified;

            try
            {
                if (this.SettingComboBox.SelectedItem is BuildDefinitionSettingsGroup)
                {
                    buildDefinitionSettings = (BuildDefinitionSettingsGroup)this.SettingComboBox.SelectedItem;

                    switch (buildDefinitionSettings)
                    {
                        case BuildDefinitionSettingsGroup.DefaultAgentPoolQueue:
                            this.DefaultAgentPoolQueuePanel.Visible = true;
                            this.TriggersSchedulePanel.Visible = false;
                            this.TriggersContinuousIntegrationPanel.Visible = false;
                            break;
                        case BuildDefinitionSettingsGroup.TriggersSchedule:
                            this.DefaultAgentPoolQueuePanel.Visible = false;
                            this.TriggersSchedulePanel.Visible = true;
                            this.TriggersContinuousIntegrationPanel.Visible = false;
                            break;
                        case BuildDefinitionSettingsGroup.TriggersContinousIntegration:
                            this.DefaultAgentPoolQueuePanel.Visible = false;
                            this.TriggersSchedulePanel.Visible = false;
                            this.TriggersContinuousIntegrationPanel.Visible = true;
                            break;
                        default:
                            this.DefaultAgentPoolQueuePanel.Visible = true;
                            this.TriggersSchedulePanel.Visible = false;
                            this.TriggersContinuousIntegrationPanel.Visible = false;
                            break;
                    }

                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Refresh agent pool queue button - Clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshAgentPoolQueueButton_Click(object sender, EventArgs e)
        {
            try
            {

                this.AgentPoolQueueSelectorList = this.BuildDefinitionChangeSettingsPresenter.GetDefaultAgentQueueSelectors();
                this.DefaultAgentPoolQueueComboBox.DataSource = this.AgentPoolQueueSelectorList;
                this.DefaultAgentPoolQueueComboBox.DisplayMember = "Name";
                this.DefaultAgentPoolQueueComboBox.ValueMember = "Id";

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Select all days button - Clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectAllDaysButton_Click(object sender, EventArgs e)
        {
            try
            {
                SundayCheckBox.Checked = true;
                MondayCheckBox.Checked = true;
                TuesdayCheckBox.Checked = true;
                WednesdayCheckBox.Checked = true;
                ThursdayCheckBox.Checked = true;
                FridayCheckBox.Checked = true;
                SaturdayCheckBox.Checked = true;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: None button - Clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectNoDaysButton_Click(object sender, EventArgs e)
        {
            try
            {
                SundayCheckBox.Checked = false;
                MondayCheckBox.Checked = false;
                TuesdayCheckBox.Checked = false;
                WednesdayCheckBox.Checked = false;
                ThursdayCheckBox.Checked = false;
                FridayCheckBox.Checked = false;
                SaturdayCheckBox.Checked = false;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickChangeBuildDefinitionsSettings.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion


    }
}
