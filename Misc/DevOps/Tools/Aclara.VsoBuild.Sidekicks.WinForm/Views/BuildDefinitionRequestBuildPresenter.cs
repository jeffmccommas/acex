﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.Vso.Build.Client;
using Aclara.Vso.Build.Client.Events;
using Aclara.Vso.Build.Client.Models;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build definition: request build presenter.
    /// </summary>
    public class BuildDefinitionRequestBuildPresenter
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private CustomLogger _logger = null;
        private SidekickConfiguration _sidekickConfiguration = null;
        private IBuildDefinitionRequestBuildView _buildDefinitionRequestBuildView;
        private BuildDefinitionManager _buildDefinitionManager;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;
        private ITeamProjectInfo _teamProjectInfo = null;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.BuildDefinitionRequestBuildView.SidekickName,
                                               this.BuildDefinitionRequestBuildView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Change build definiton request build view.
        /// </summary>
        public IBuildDefinitionRequestBuildView BuildDefinitionRequestBuildView
        {
            get { return _buildDefinitionRequestBuildView; }
            set { _buildDefinitionRequestBuildView = value; }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Build definition manager.
        /// </summary>
        public BuildDefinitionManager BuildDefinitionManager
        {
            get { return _buildDefinitionManager; }
            set { _buildDefinitionManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamPrjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionRequestBuildView"></param>
        public BuildDefinitionRequestBuildPresenter(ITeamProjectInfo teamPrjectInfo,
                                                    SidekickConfiguration sidekickConfiguration,
                                                    IBuildDefinitionRequestBuildView buildDefinitionRequestBuildView)
        {
            this.TeamProjectInfo = teamPrjectInfo;
            this.SidekickConfiguration = sidekickConfiguration;
            this.BuildDefinitionRequestBuildView = buildDefinitionRequestBuildView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private BuildDefinitionRequestBuildPresenter()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalBuildDuration;
            AggregateException aggregateException = null;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalBuildDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.BuildDefinitionRequestBuildView.BackgroundTaskCompleted("Request build canceled.");
                        this.BuildDefinitionRequestBuildView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format("Request build cancelled."));


                        //Log background task completed log entry.
                        this.Logger.Info(string.Format("[*] Request build completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                        totalBuildDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.BuildDefinitionRequestBuildView.BackgroundTaskCompleted("");
                        this.BuildDefinitionRequestBuildView.ApplyButtonEnable(true);


                        //Log background task completed log entry.

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    //Handle operation canceled exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception;
                                        this.Logger.Error(innerException, string.Format("Operation cancelled."));
                                    }
                                    else
                                    {
                                        this.Logger.Error(string.Format("Operation cancelled."));
                                    }
                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception;
                                        this.Logger.Error(innerException, string.Format("Request failed."));
                                    }
                                    else
                                    {
                                        this.Logger.Error(string.Format("Operation cancelled."));
                                    }

                                }
                            }
                        }

                        this.Logger.Info(string.Format("[*] Build request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                        totalBuildDuration));
                        break;

                    case TaskStatus.RanToCompletion:
                        this.BuildDefinitionRequestBuildView.BackgroundTaskCompleted("Build request completed.");
                        this.BuildDefinitionRequestBuildView.ApplyButtonEnable(true);

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format("[*] Build request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                        totalBuildDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Request build.
        /// </summary>
        /// <param name="buildDefinitionSelectorList"></param>
        /// <param name="parameters"></param>
        /// <param name="overrideQueueStatus"></param>
        /// <param name="queuePriority"></param>
        public void RequestBuilds(BuildDefinitionSelectorList buildDefinitionSelectorList,
                                  string parameters,
                                  bool overrideQueueStatus,
                                  Aclara.Vso.Build.Client.Types.Enumerations.QueuePriority queuePriority)
        {

            List<int> filteredBuidDefinitionIdList = null;
            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.BuildDefinitionRequestBuildView.BackgroundTaskStatus = "Cancelling request build request...";
                    this.BuildDefinitionRequestBuildView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.BuildDefinitionRequestBuildView.BackgroundTaskStatus = "Build(s) requested...";
                    this.BuildDefinitionRequestBuildView.ApplyButtonText = "Cancel";

                    this.BuildDefinitionManager = this.CreateBuildDefinitionManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    //Reterieve fresh build definition list. (In case build definitions have changed since last retrieval).
                    filteredBuidDefinitionIdList = buildDefinitionSelectorList.GetBuildDefinitionIdList();

                    this.BuildDefinitionManager.BuildRequested += OnBuildRequested;

                    this.BackgroundTask = new Task(() => this.BuildDefinitionManager.RequestBuilds(filteredBuidDefinitionIdList,
                                                                                                   parameters,
                                                                                                   overrideQueueStatus,
                                                                                                   queuePriority,
                                                                                                   cancellationToken),
                                                                                                   cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create build definition manager.
        /// </summary>
        /// <returns></returns>
        protected BuildDefinitionManager CreateBuildDefinitionManager()
        {
            BuildDefinitionManager result = null;

            try
            {
                result = BuildDefinitionManagerFactory.CreateBuildDefinitionManager(this.TeamProjectInfo.TeamProjectCollectionUri,
                                                                                    this.TeamProjectInfo.TeamProjectCollectionVsrmUri, 
                                                                                    this.TeamProjectInfo.TeamProjectName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIUserProfileName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIPassword);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: Build requested.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="requestBuildEventArgs"></param>
        protected void OnBuildRequested(Object sender,
                                       RequestBuildEventArgs requestBuildEventArgs)
        {

            try
            {

                //Build definition details available and status list details available.
                if (requestBuildEventArgs.BuildDefinition != null &&
                    requestBuildEventArgs.StatusList != null &&
                    requestBuildEventArgs.StatusList.Count > 0)
                {

                    if (requestBuildEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error) == true)
                    {
                        this.Logger.Error(string.Format("Build request may have been canceled or failed. (Build definition name: {0}; Status --> {1})",
                                                        requestBuildEventArgs.BuildDefinition.Name,
                                                        requestBuildEventArgs.StatusList.Format(StatusTypes.FormatOption.Full)));
                    }
                    else if (requestBuildEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Warning) == true)
                    {
                        this.Logger.Warn(string.Format("Build request may have been canceled. (Build definition name: {0}; Status --> {1})",
                                                       requestBuildEventArgs.BuildDefinition.Name,
                                                       requestBuildEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum)));
                    }
                    else
                    {
                        this.Logger.Info(string.Format("Build request may have been canceled. (Build definition name: {0}; Status --> {1})",
                                                       requestBuildEventArgs.BuildDefinition.Name,
                                                       requestBuildEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum)));
                    }

                }
                else if (requestBuildEventArgs.StatusList != null &&
                         requestBuildEventArgs.StatusList.Count > 0)
                {


                    if (requestBuildEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error) == true)
                    {
                        this.Logger.Error(string.Format("Build request may have been canceled or failed. (Status --> {0})",
                                                        requestBuildEventArgs.StatusList.Format(StatusTypes.FormatOption.Full)));
                    }
                    else if (requestBuildEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Warning) == true)
                    {
                        this.Logger.Warn(string.Format("Build request may have been canceled. (Status --> {0})",
                                                       requestBuildEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum)));
                    }
                    else
                    {
                        this.Logger.Info(string.Format("Build request may have been canceled. (Status --> {0})",
                                                       requestBuildEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum)));
                    }
                }
                //Build definition details available and status list is unavailable.
                else if (requestBuildEventArgs.BuildDefinition != null)
                {
                    this.Logger.Info(string.Format("Build requested. (Build definition name: {0})",
                                                     requestBuildEventArgs.BuildDefinition.Name));
                }
                //Build definiton details unavailable.
                else
                {
                    this.Logger.Info(string.Format("Build requested. Details unavailable."));
                }

                this.BuildDefinitionRequestBuildView.UpdateProgressRequestBuild(requestBuildEventArgs.BuildDefinitionBuildRequestedCount, 
                                                                                requestBuildEventArgs.BuildDefinitionTotalCount);
            }
            catch (Exception)
            {
                throw;
            }


        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            this.Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));
                        }
                        else
                        {
                            this.Logger.Error(string.Format("Operation cancelled.(Unobserved)"));
                        }
                    }
                    eventArgs.SetObserved();

                    return;
                }
                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
