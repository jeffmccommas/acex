﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Notification preview presenter.
    /// </summary>
    public class NotificationPreviewPresenter
    {
        #region Private Constants

        #endregion

        #region Private Data Members

        private INotificationPreviewView _notificationPreviewView;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Notification preview view.
        /// </summary>
        public INotificationPreviewView NotificationPreviewView
        {
            get { return _notificationPreviewView; }
            set { _notificationPreviewView = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        public NotificationPreviewPresenter(INotificationPreviewView notificationPreviewView)
        {
            _notificationPreviewView = notificationPreviewView;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {
            this.NotificationPreviewView.NotificationPreviewText = string.Empty;

        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion
    }


}
