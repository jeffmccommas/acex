﻿using Aclara.Vso.Build.Client.Models;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build status check view - Interface.
    /// </summary>
    public interface IBuildStatusCheckView
    {
        string SidekickName { get; }

        string SidekickDescription { get; }


        SidekickContainerTypes.SidekickWarningLevel SidekickWarningLevel { get; }

        string BackgroundTaskStatus { get; set; }

        void RefreshButtonEnable(bool enable);

        bool IsSidekickBusy { get; }

        void RetrieveRemainingDefinitions();

        void TeamProjectChanged();

        void UpdateBuildListRetrievalStatus(string statusMessage, int buildListRetrievedCount, int buildListTotalCount);

        void PopulateBuildStatus();

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);

        BuildList BuildList { get; set; }
    }

}
