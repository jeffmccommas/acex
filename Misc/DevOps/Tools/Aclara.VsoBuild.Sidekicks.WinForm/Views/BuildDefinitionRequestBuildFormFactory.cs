﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using System;
using System.Collections.Generic;
using System.Linq;



namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public class BuildDefinitionRequestBuildFormFactory
    {

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <param name="sidekickCoaction"></param>
        /// <returns></returns>
        public static BuildDefinitionRequestBuildForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                                 SidekickConfiguration sidekickConfiguration,
                                                                 BuildDefinitionSearchForm buildDefinitionSearchForm,
                                                                 ISidekickCoaction sidekickCoaction)
        {

            BuildDefinitionRequestBuildForm result = null;
            BuildDefinitionRequestBuildPresenter buildDefinitionRequestBuildPresenter = null;

            try
            {
                result = new BuildDefinitionRequestBuildForm(sidekickConfiguration, buildDefinitionSearchForm, sidekickCoaction);
                buildDefinitionRequestBuildPresenter = new BuildDefinitionRequestBuildPresenter(teamProjectInfo, sidekickConfiguration, result);
                result.BuildDefinitionRequestBuildPresenter = buildDefinitionRequestBuildPresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

    }
}
