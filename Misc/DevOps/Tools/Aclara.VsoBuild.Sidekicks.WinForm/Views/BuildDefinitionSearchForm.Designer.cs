﻿namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    partial class BuildDefinitionSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.BuildDefinitionDisplayContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.BuildDefinitionDisplayAllToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionDisplayDisabledToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionDisplayEnabledToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionDisplayHighlightedToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionDisplayCheckedToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BuildDefinitionDisplayCIToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionDisplayDevToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionDisplayQAToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionDisplayUATToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionDisplayPerfToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionDisplayProductionToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.BuildDefinitionDisplayPOCToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionDisplayTESTToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.BuildDefinitionDisplayDevDRToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionDisplayUATDRToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionDisplayProdDRToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.CopyButton = new System.Windows.Forms.Button();
            this.BuildDefinitionHighlightContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.BuildDefinitionHighlightNoneToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionHighlightDisabledToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.BuildDefinitionHighlightCIToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionHighlightDevToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionHighlightQAToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionHighlightUATToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionHighlightPerfToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionHighlightProductionToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.BuildDefinitionHighlightPOCToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionHighlightTESTToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BuildDefinitionHighlightDevDRToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionHighlightUATDRToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionHighlightProdDRToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SearchCriteriaGroupBox = new System.Windows.Forms.GroupBox();
            this.ExactMatchCheckBox = new System.Windows.Forms.CheckBox();
            this.BuildDefinitionNameFilterTextBox = new System.Windows.Forms.RichTextBox();
            this.BuildDefinitionNameFilterContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.BuildDefinitionNameFilterCutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionNameFilterCopyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionNameFilterPasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionNameFilterDeleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.BuildDefinitionNameFilterSelectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.SearchButton = new System.Windows.Forms.Button();
            this.BuildDefinitionCheckContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.BuildDefinitionSelectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionSelectNoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionCheckDisabledToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionSelectEnabledToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionSelectHighlightedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BuildDefinitionCheckCIToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionCheckDevToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionCheckQAToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionCheckUATToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionCheckPerfToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionCheckProductionToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.BuildDefinitionCheckPOCToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionCheckTESTToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.BuildDefinitionCheckDevDRToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionCheckUATDRToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionCheckProdDRToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionListDataGridView = new System.Windows.Forms.DataGridView();
            this.CheckColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EnabledColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.NameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TriggerColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QueueNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExtraColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuildDefinitionListDataGridViewContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CheckSelectedBuildDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyBuildDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewBuildDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RequestBuildBuildDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildDefinitionSearchToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.ViewSplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.ViewContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ViewAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewBasicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewDefaultQueueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewScheduleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HighlightSplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.DisplaySplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.CheckSplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.BuildDefinitionStatusPanel = new System.Windows.Forms.Panel();
            this.BuildDefinitionRetrievalStatusPanel = new System.Windows.Forms.Panel();
            this.BuildDefinitionRetrievalProgressBar = new System.Windows.Forms.ProgressBar();
            this.BuildDefinitionRetrievalStatusLabel = new System.Windows.Forms.Label();
            this.BuildDefinitionMatchStatusPanel = new System.Windows.Forms.Panel();
            this.BuildDefinitionMatchStatusLabel = new System.Windows.Forms.Label();
            this.SidekickToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.BuildDefinitionDisplayContextMenuStrip.SuspendLayout();
            this.BuildDefinitionHighlightContextMenuStrip.SuspendLayout();
            this.SearchCriteriaGroupBox.SuspendLayout();
            this.BuildDefinitionNameFilterContextMenuStrip.SuspendLayout();
            this.BuildDefinitionCheckContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BuildDefinitionListDataGridView)).BeginInit();
            this.BuildDefinitionListDataGridViewContextMenuStrip.SuspendLayout();
            this.ViewContextMenuStrip.SuspendLayout();
            this.BuildDefinitionStatusPanel.SuspendLayout();
            this.BuildDefinitionRetrievalStatusPanel.SuspendLayout();
            this.BuildDefinitionMatchStatusPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BuildDefinitionDisplayContextMenuStrip
            // 
            this.BuildDefinitionDisplayContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BuildDefinitionDisplayAllToolStripItem,
            this.BuildDefinitionDisplayDisabledToolStripItem,
            this.BuildDefinitionDisplayEnabledToolStripItem,
            this.BuildDefinitionDisplayHighlightedToolStripItem,
            this.BuildDefinitionDisplayCheckedToolStripItem,
            this.toolStripSeparator3,
            this.BuildDefinitionDisplayCIToolStripItem,
            this.BuildDefinitionDisplayDevToolStripItem,
            this.BuildDefinitionDisplayQAToolStripItem,
            this.BuildDefinitionDisplayUATToolStripItem,
            this.BuildDefinitionDisplayPerfToolStripItem,
            this.BuildDefinitionDisplayProductionToolStripItem,
            this.toolStripSeparator8,
            this.BuildDefinitionDisplayPOCToolStripItem,
            this.BuildDefinitionDisplayTESTToolStripItem,
            this.toolStripSeparator9,
            this.BuildDefinitionDisplayDevDRToolStripItem,
            this.BuildDefinitionDisplayUATDRToolStripItem,
            this.BuildDefinitionDisplayProdDRToolStripItem});
            this.BuildDefinitionDisplayContextMenuStrip.Name = "BuildDefinitionDisplayContextMenuStrip";
            this.BuildDefinitionDisplayContextMenuStrip.ShowCheckMargin = true;
            this.BuildDefinitionDisplayContextMenuStrip.ShowImageMargin = false;
            this.BuildDefinitionDisplayContextMenuStrip.Size = new System.Drawing.Size(138, 374);
            // 
            // BuildDefinitionDisplayAllToolStripItem
            // 
            this.BuildDefinitionDisplayAllToolStripItem.Name = "BuildDefinitionDisplayAllToolStripItem";
            this.BuildDefinitionDisplayAllToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayAllToolStripItem.Text = "All";
            this.BuildDefinitionDisplayAllToolStripItem.Click += new System.EventHandler(this.BuildDefinitionDisplayAllToolStripItem_Click);
            // 
            // BuildDefinitionDisplayDisabledToolStripItem
            // 
            this.BuildDefinitionDisplayDisabledToolStripItem.Name = "BuildDefinitionDisplayDisabledToolStripItem";
            this.BuildDefinitionDisplayDisabledToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayDisabledToolStripItem.Text = "Disabled";
            this.BuildDefinitionDisplayDisabledToolStripItem.Click += new System.EventHandler(this.BuildDefinitionDisplayDisabledToolStripItem_Click);
            // 
            // BuildDefinitionDisplayEnabledToolStripItem
            // 
            this.BuildDefinitionDisplayEnabledToolStripItem.Name = "BuildDefinitionDisplayEnabledToolStripItem";
            this.BuildDefinitionDisplayEnabledToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayEnabledToolStripItem.Text = "Enabled";
            this.BuildDefinitionDisplayEnabledToolStripItem.Click += new System.EventHandler(this.BuildDefinitionDisplayEnabledToolStripItem_Click);
            // 
            // BuildDefinitionDisplayHighlightedToolStripItem
            // 
            this.BuildDefinitionDisplayHighlightedToolStripItem.Name = "BuildDefinitionDisplayHighlightedToolStripItem";
            this.BuildDefinitionDisplayHighlightedToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayHighlightedToolStripItem.Text = "Highlighted";
            this.BuildDefinitionDisplayHighlightedToolStripItem.Click += new System.EventHandler(this.BuildDefinitionDisplayHighlightedToolStripItem_Click);
            // 
            // BuildDefinitionDisplayCheckedToolStripItem
            // 
            this.BuildDefinitionDisplayCheckedToolStripItem.Name = "BuildDefinitionDisplayCheckedToolStripItem";
            this.BuildDefinitionDisplayCheckedToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayCheckedToolStripItem.Text = "Checked";
            this.BuildDefinitionDisplayCheckedToolStripItem.Click += new System.EventHandler(this.BuildDefinitionDisplayCheckedToolStripItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(134, 6);
            // 
            // BuildDefinitionDisplayCIToolStripItem
            // 
            this.BuildDefinitionDisplayCIToolStripItem.CheckOnClick = true;
            this.BuildDefinitionDisplayCIToolStripItem.Name = "BuildDefinitionDisplayCIToolStripItem";
            this.BuildDefinitionDisplayCIToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayCIToolStripItem.Text = "CI";
            this.BuildDefinitionDisplayCIToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionDisplayCIToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionDisplayDevToolStripItem
            // 
            this.BuildDefinitionDisplayDevToolStripItem.CheckOnClick = true;
            this.BuildDefinitionDisplayDevToolStripItem.Name = "BuildDefinitionDisplayDevToolStripItem";
            this.BuildDefinitionDisplayDevToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayDevToolStripItem.Text = "DEV";
            this.BuildDefinitionDisplayDevToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionDisplayDevToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionDisplayQAToolStripItem
            // 
            this.BuildDefinitionDisplayQAToolStripItem.CheckOnClick = true;
            this.BuildDefinitionDisplayQAToolStripItem.Name = "BuildDefinitionDisplayQAToolStripItem";
            this.BuildDefinitionDisplayQAToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayQAToolStripItem.Text = "QA";
            this.BuildDefinitionDisplayQAToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionDisplayQAToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionDisplayUATToolStripItem
            // 
            this.BuildDefinitionDisplayUATToolStripItem.CheckOnClick = true;
            this.BuildDefinitionDisplayUATToolStripItem.Name = "BuildDefinitionDisplayUATToolStripItem";
            this.BuildDefinitionDisplayUATToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayUATToolStripItem.Text = "UAT";
            this.BuildDefinitionDisplayUATToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionDisplayUATToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionDisplayPerfToolStripItem
            // 
            this.BuildDefinitionDisplayPerfToolStripItem.CheckOnClick = true;
            this.BuildDefinitionDisplayPerfToolStripItem.Name = "BuildDefinitionDisplayPerfToolStripItem";
            this.BuildDefinitionDisplayPerfToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayPerfToolStripItem.Text = "PERF";
            this.BuildDefinitionDisplayPerfToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionDisplayPerfToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionDisplayProductionToolStripItem
            // 
            this.BuildDefinitionDisplayProductionToolStripItem.CheckOnClick = true;
            this.BuildDefinitionDisplayProductionToolStripItem.Name = "BuildDefinitionDisplayProductionToolStripItem";
            this.BuildDefinitionDisplayProductionToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayProductionToolStripItem.Text = "PROD";
            this.BuildDefinitionDisplayProductionToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionDisplayProductionToolStripItem_CheckedChanged);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(134, 6);
            // 
            // BuildDefinitionDisplayPOCToolStripItem
            // 
            this.BuildDefinitionDisplayPOCToolStripItem.CheckOnClick = true;
            this.BuildDefinitionDisplayPOCToolStripItem.Name = "BuildDefinitionDisplayPOCToolStripItem";
            this.BuildDefinitionDisplayPOCToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayPOCToolStripItem.Text = "POC";
            this.BuildDefinitionDisplayPOCToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionDisplayPOCToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionDisplayTESTToolStripItem
            // 
            this.BuildDefinitionDisplayTESTToolStripItem.CheckOnClick = true;
            this.BuildDefinitionDisplayTESTToolStripItem.Name = "BuildDefinitionDisplayTESTToolStripItem";
            this.BuildDefinitionDisplayTESTToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayTESTToolStripItem.Text = "TEST";
            this.BuildDefinitionDisplayTESTToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionDisplayTESTToolStripItem_CheckedChanged);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(134, 6);
            // 
            // BuildDefinitionDisplayDevDRToolStripItem
            // 
            this.BuildDefinitionDisplayDevDRToolStripItem.CheckOnClick = true;
            this.BuildDefinitionDisplayDevDRToolStripItem.Name = "BuildDefinitionDisplayDevDRToolStripItem";
            this.BuildDefinitionDisplayDevDRToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayDevDRToolStripItem.Text = "DEV DR";
            this.BuildDefinitionDisplayDevDRToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionDisplayDevDRToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionDisplayUATDRToolStripItem
            // 
            this.BuildDefinitionDisplayUATDRToolStripItem.CheckOnClick = true;
            this.BuildDefinitionDisplayUATDRToolStripItem.Name = "BuildDefinitionDisplayUATDRToolStripItem";
            this.BuildDefinitionDisplayUATDRToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayUATDRToolStripItem.Text = "UAT DR";
            this.BuildDefinitionDisplayUATDRToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionDisplayUATDRToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionDisplayProdDRToolStripItem
            // 
            this.BuildDefinitionDisplayProdDRToolStripItem.CheckOnClick = true;
            this.BuildDefinitionDisplayProdDRToolStripItem.Name = "BuildDefinitionDisplayProdDRToolStripItem";
            this.BuildDefinitionDisplayProdDRToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionDisplayProdDRToolStripItem.Text = "PROD DR";
            this.BuildDefinitionDisplayProdDRToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionDisplayProdDRToolStripItem_CheckedChanged);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "Enabled";
            this.dataGridViewImageColumn1.MinimumWidth = 50;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.ReadOnly = true;
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewImageColumn1.Width = 50;
            // 
            // CopyButton
            // 
            this.CopyButton.Location = new System.Drawing.Point(342, 180);
            this.CopyButton.Name = "CopyButton";
            this.CopyButton.Size = new System.Drawing.Size(76, 23);
            this.CopyButton.TabIndex = 6;
            this.CopyButton.Text = "Copy";
            this.CopyButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BuildDefinitionSearchToolTip.SetToolTip(this.CopyButton, "Copy selected build definition name(s)");
            this.CopyButton.UseVisualStyleBackColor = true;
            this.CopyButton.Click += new System.EventHandler(this.CopyButton_Click);
            // 
            // BuildDefinitionHighlightContextMenuStrip
            // 
            this.BuildDefinitionHighlightContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BuildDefinitionHighlightNoneToolStripItem,
            this.BuildDefinitionHighlightDisabledToolStripItem,
            this.toolStripSeparator6,
            this.BuildDefinitionHighlightCIToolStripItem,
            this.BuildDefinitionHighlightDevToolStripItem,
            this.BuildDefinitionHighlightQAToolStripItem,
            this.BuildDefinitionHighlightUATToolStripItem,
            this.BuildDefinitionHighlightPerfToolStripItem,
            this.BuildDefinitionHighlightProductionToolStripItem,
            this.toolStripSeparator10,
            this.BuildDefinitionHighlightPOCToolStripItem,
            this.BuildDefinitionHighlightTESTToolStripItem,
            this.toolStripSeparator1,
            this.BuildDefinitionHighlightDevDRToolStripItem,
            this.BuildDefinitionHighlightUATDRToolStripItem,
            this.BuildDefinitionHighlightProdDRToolStripItem});
            this.BuildDefinitionHighlightContextMenuStrip.Name = "BuildDefinitionHighlightContextMenuStrip";
            this.BuildDefinitionHighlightContextMenuStrip.ShowCheckMargin = true;
            this.BuildDefinitionHighlightContextMenuStrip.ShowImageMargin = false;
            this.BuildDefinitionHighlightContextMenuStrip.Size = new System.Drawing.Size(124, 308);
            // 
            // BuildDefinitionHighlightNoneToolStripItem
            // 
            this.BuildDefinitionHighlightNoneToolStripItem.Name = "BuildDefinitionHighlightNoneToolStripItem";
            this.BuildDefinitionHighlightNoneToolStripItem.Size = new System.Drawing.Size(123, 22);
            this.BuildDefinitionHighlightNoneToolStripItem.Text = "None";
            this.BuildDefinitionHighlightNoneToolStripItem.Click += new System.EventHandler(this.BuildDefinitionHighlightNoneToolStripItem_Click);
            // 
            // BuildDefinitionHighlightDisabledToolStripItem
            // 
            this.BuildDefinitionHighlightDisabledToolStripItem.CheckOnClick = true;
            this.BuildDefinitionHighlightDisabledToolStripItem.Name = "BuildDefinitionHighlightDisabledToolStripItem";
            this.BuildDefinitionHighlightDisabledToolStripItem.Size = new System.Drawing.Size(123, 22);
            this.BuildDefinitionHighlightDisabledToolStripItem.Text = "Disabled";
            this.BuildDefinitionHighlightDisabledToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionHighlightDisabledToolStripItem_CheckedChanged);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(120, 6);
            // 
            // BuildDefinitionHighlightCIToolStripItem
            // 
            this.BuildDefinitionHighlightCIToolStripItem.CheckOnClick = true;
            this.BuildDefinitionHighlightCIToolStripItem.Name = "BuildDefinitionHighlightCIToolStripItem";
            this.BuildDefinitionHighlightCIToolStripItem.Size = new System.Drawing.Size(123, 22);
            this.BuildDefinitionHighlightCIToolStripItem.Text = "CI";
            this.BuildDefinitionHighlightCIToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionHighlightCIToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionHighlightDevToolStripItem
            // 
            this.BuildDefinitionHighlightDevToolStripItem.CheckOnClick = true;
            this.BuildDefinitionHighlightDevToolStripItem.Name = "BuildDefinitionHighlightDevToolStripItem";
            this.BuildDefinitionHighlightDevToolStripItem.Size = new System.Drawing.Size(123, 22);
            this.BuildDefinitionHighlightDevToolStripItem.Text = "DEV";
            this.BuildDefinitionHighlightDevToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionHighlightDevToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionHighlightQAToolStripItem
            // 
            this.BuildDefinitionHighlightQAToolStripItem.CheckOnClick = true;
            this.BuildDefinitionHighlightQAToolStripItem.Name = "BuildDefinitionHighlightQAToolStripItem";
            this.BuildDefinitionHighlightQAToolStripItem.Size = new System.Drawing.Size(123, 22);
            this.BuildDefinitionHighlightQAToolStripItem.Text = "QA";
            this.BuildDefinitionHighlightQAToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionHighlightQAToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionHighlightUATToolStripItem
            // 
            this.BuildDefinitionHighlightUATToolStripItem.CheckOnClick = true;
            this.BuildDefinitionHighlightUATToolStripItem.Name = "BuildDefinitionHighlightUATToolStripItem";
            this.BuildDefinitionHighlightUATToolStripItem.Size = new System.Drawing.Size(123, 22);
            this.BuildDefinitionHighlightUATToolStripItem.Text = "UAT";
            this.BuildDefinitionHighlightUATToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionHighlightUATToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionHighlightPerfToolStripItem
            // 
            this.BuildDefinitionHighlightPerfToolStripItem.CheckOnClick = true;
            this.BuildDefinitionHighlightPerfToolStripItem.Name = "BuildDefinitionHighlightPerfToolStripItem";
            this.BuildDefinitionHighlightPerfToolStripItem.Size = new System.Drawing.Size(123, 22);
            this.BuildDefinitionHighlightPerfToolStripItem.Text = "PERF";
            this.BuildDefinitionHighlightPerfToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionHighlightPerfToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionHighlightProductionToolStripItem
            // 
            this.BuildDefinitionHighlightProductionToolStripItem.CheckOnClick = true;
            this.BuildDefinitionHighlightProductionToolStripItem.Name = "BuildDefinitionHighlightProductionToolStripItem";
            this.BuildDefinitionHighlightProductionToolStripItem.Size = new System.Drawing.Size(123, 22);
            this.BuildDefinitionHighlightProductionToolStripItem.Text = "PROD";
            this.BuildDefinitionHighlightProductionToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionHighlightProductionToolStripItem_CheckedChanged);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(120, 6);
            // 
            // BuildDefinitionHighlightPOCToolStripItem
            // 
            this.BuildDefinitionHighlightPOCToolStripItem.CheckOnClick = true;
            this.BuildDefinitionHighlightPOCToolStripItem.Name = "BuildDefinitionHighlightPOCToolStripItem";
            this.BuildDefinitionHighlightPOCToolStripItem.Size = new System.Drawing.Size(123, 22);
            this.BuildDefinitionHighlightPOCToolStripItem.Text = "POC";
            this.BuildDefinitionHighlightPOCToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionHighlightPOCToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionHighlightTESTToolStripItem
            // 
            this.BuildDefinitionHighlightTESTToolStripItem.CheckOnClick = true;
            this.BuildDefinitionHighlightTESTToolStripItem.Name = "BuildDefinitionHighlightTESTToolStripItem";
            this.BuildDefinitionHighlightTESTToolStripItem.Size = new System.Drawing.Size(123, 22);
            this.BuildDefinitionHighlightTESTToolStripItem.Text = "TEST";
            this.BuildDefinitionHighlightTESTToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionHighlightTESTToolStripItem_CheckedChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(120, 6);
            // 
            // BuildDefinitionHighlightDevDRToolStripItem
            // 
            this.BuildDefinitionHighlightDevDRToolStripItem.CheckOnClick = true;
            this.BuildDefinitionHighlightDevDRToolStripItem.Name = "BuildDefinitionHighlightDevDRToolStripItem";
            this.BuildDefinitionHighlightDevDRToolStripItem.Size = new System.Drawing.Size(123, 22);
            this.BuildDefinitionHighlightDevDRToolStripItem.Text = "DEV DR";
            this.BuildDefinitionHighlightDevDRToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionHighlightDevDRToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionHighlightUATDRToolStripItem
            // 
            this.BuildDefinitionHighlightUATDRToolStripItem.CheckOnClick = true;
            this.BuildDefinitionHighlightUATDRToolStripItem.Name = "BuildDefinitionHighlightUATDRToolStripItem";
            this.BuildDefinitionHighlightUATDRToolStripItem.Size = new System.Drawing.Size(123, 22);
            this.BuildDefinitionHighlightUATDRToolStripItem.Text = "UAT  DR";
            this.BuildDefinitionHighlightUATDRToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionHighlightUATDRToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionHighlightProdDRToolStripItem
            // 
            this.BuildDefinitionHighlightProdDRToolStripItem.CheckOnClick = true;
            this.BuildDefinitionHighlightProdDRToolStripItem.Name = "BuildDefinitionHighlightProdDRToolStripItem";
            this.BuildDefinitionHighlightProdDRToolStripItem.Size = new System.Drawing.Size(123, 22);
            this.BuildDefinitionHighlightProdDRToolStripItem.Text = "PROD DR";
            this.BuildDefinitionHighlightProdDRToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionHighlightProdDRToolStripItem_CheckedChanged);
            // 
            // SearchCriteriaGroupBox
            // 
            this.SearchCriteriaGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchCriteriaGroupBox.Controls.Add(this.ExactMatchCheckBox);
            this.SearchCriteriaGroupBox.Controls.Add(this.BuildDefinitionNameFilterTextBox);
            this.SearchCriteriaGroupBox.Controls.Add(this.RefreshButton);
            this.SearchCriteriaGroupBox.Controls.Add(this.ClearButton);
            this.SearchCriteriaGroupBox.Controls.Add(this.SearchButton);
            this.SearchCriteriaGroupBox.Location = new System.Drawing.Point(13, 9);
            this.SearchCriteriaGroupBox.Name = "SearchCriteriaGroupBox";
            this.SearchCriteriaGroupBox.Size = new System.Drawing.Size(475, 133);
            this.SearchCriteriaGroupBox.TabIndex = 0;
            this.SearchCriteriaGroupBox.TabStop = false;
            this.SearchCriteriaGroupBox.Text = "Build definition search criteria";
            // 
            // ExactMatchCheckBox
            // 
            this.ExactMatchCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ExactMatchCheckBox.AutoSize = true;
            this.ExactMatchCheckBox.Location = new System.Drawing.Point(384, 19);
            this.ExactMatchCheckBox.Name = "ExactMatchCheckBox";
            this.ExactMatchCheckBox.Size = new System.Drawing.Size(85, 17);
            this.ExactMatchCheckBox.TabIndex = 1;
            this.ExactMatchCheckBox.Text = "Exact match";
            this.BuildDefinitionSearchToolTip.SetToolTip(this.ExactMatchCheckBox, "Only build definition names with exact match to search criteria are displayed");
            this.ExactMatchCheckBox.UseVisualStyleBackColor = true;
            // 
            // BuildDefinitionNameFilterTextBox
            // 
            this.BuildDefinitionNameFilterTextBox.AcceptsTab = true;
            this.BuildDefinitionNameFilterTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildDefinitionNameFilterTextBox.ContextMenuStrip = this.BuildDefinitionNameFilterContextMenuStrip;
            this.BuildDefinitionNameFilterTextBox.Location = new System.Drawing.Point(12, 19);
            this.BuildDefinitionNameFilterTextBox.Name = "BuildDefinitionNameFilterTextBox";
            this.BuildDefinitionNameFilterTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.BuildDefinitionNameFilterTextBox.ShowSelectionMargin = true;
            this.BuildDefinitionNameFilterTextBox.Size = new System.Drawing.Size(365, 104);
            this.BuildDefinitionNameFilterTextBox.TabIndex = 0;
            this.BuildDefinitionNameFilterTextBox.Text = "";
            this.SidekickToolTip.SetToolTip(this.BuildDefinitionNameFilterTextBox, "Search criteria: Wildcards (*, ?) supported.");
            // 
            // BuildDefinitionNameFilterContextMenuStrip
            // 
            this.BuildDefinitionNameFilterContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BuildDefinitionNameFilterCutToolStripMenuItem,
            this.BuildDefinitionNameFilterCopyToolStripMenuItem,
            this.BuildDefinitionNameFilterPasteToolStripMenuItem,
            this.BuildDefinitionNameFilterDeleteToolStripMenuItem,
            this.toolStripSeparator7,
            this.BuildDefinitionNameFilterSelectAllToolStripMenuItem});
            this.BuildDefinitionNameFilterContextMenuStrip.Name = "BuildDefinitionNameFilterContextMenuStrip";
            this.BuildDefinitionNameFilterContextMenuStrip.ShowImageMargin = false;
            this.BuildDefinitionNameFilterContextMenuStrip.Size = new System.Drawing.Size(98, 120);
            // 
            // BuildDefinitionNameFilterCutToolStripMenuItem
            // 
            this.BuildDefinitionNameFilterCutToolStripMenuItem.Name = "BuildDefinitionNameFilterCutToolStripMenuItem";
            this.BuildDefinitionNameFilterCutToolStripMenuItem.Size = new System.Drawing.Size(97, 22);
            this.BuildDefinitionNameFilterCutToolStripMenuItem.Text = "Cut";
            this.BuildDefinitionNameFilterCutToolStripMenuItem.Click += new System.EventHandler(this.BuildDefinitionNameFilterCutToolStripMenuItem_Click);
            // 
            // BuildDefinitionNameFilterCopyToolStripMenuItem
            // 
            this.BuildDefinitionNameFilterCopyToolStripMenuItem.Name = "BuildDefinitionNameFilterCopyToolStripMenuItem";
            this.BuildDefinitionNameFilterCopyToolStripMenuItem.Size = new System.Drawing.Size(97, 22);
            this.BuildDefinitionNameFilterCopyToolStripMenuItem.Text = "Copy";
            this.BuildDefinitionNameFilterCopyToolStripMenuItem.Click += new System.EventHandler(this.BuildDefinitionNameFilterCopyToolStripMenuItem_Click);
            // 
            // BuildDefinitionNameFilterPasteToolStripMenuItem
            // 
            this.BuildDefinitionNameFilterPasteToolStripMenuItem.Name = "BuildDefinitionNameFilterPasteToolStripMenuItem";
            this.BuildDefinitionNameFilterPasteToolStripMenuItem.Size = new System.Drawing.Size(97, 22);
            this.BuildDefinitionNameFilterPasteToolStripMenuItem.Text = "Paste";
            this.BuildDefinitionNameFilterPasteToolStripMenuItem.Click += new System.EventHandler(this.BuildDefinitionNameFilterPasteToolStripMenuItem_Click);
            // 
            // BuildDefinitionNameFilterDeleteToolStripMenuItem
            // 
            this.BuildDefinitionNameFilterDeleteToolStripMenuItem.Name = "BuildDefinitionNameFilterDeleteToolStripMenuItem";
            this.BuildDefinitionNameFilterDeleteToolStripMenuItem.Size = new System.Drawing.Size(97, 22);
            this.BuildDefinitionNameFilterDeleteToolStripMenuItem.Text = "Delete";
            this.BuildDefinitionNameFilterDeleteToolStripMenuItem.Click += new System.EventHandler(this.BuildDefinitionNameFilterDeleteToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(94, 6);
            // 
            // BuildDefinitionNameFilterSelectAllToolStripMenuItem
            // 
            this.BuildDefinitionNameFilterSelectAllToolStripMenuItem.Name = "BuildDefinitionNameFilterSelectAllToolStripMenuItem";
            this.BuildDefinitionNameFilterSelectAllToolStripMenuItem.Size = new System.Drawing.Size(97, 22);
            this.BuildDefinitionNameFilterSelectAllToolStripMenuItem.Text = "Select All";
            this.BuildDefinitionNameFilterSelectAllToolStripMenuItem.Click += new System.EventHandler(this.BuildDefinitionNameFilterSelectAllToolStripMenuItem_Click);
            // 
            // RefreshButton
            // 
            this.RefreshButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RefreshButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
            this.RefreshButton.Location = new System.Drawing.Point(383, 71);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(32, 23);
            this.RefreshButton.TabIndex = 3;
            this.RefreshButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BuildDefinitionSearchToolTip.SetToolTip(this.RefreshButton, "Refresh");
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ClearButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ClearButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionClear;
            this.ClearButton.Location = new System.Drawing.Point(383, 100);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(32, 23);
            this.ClearButton.TabIndex = 4;
            this.ClearButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.BuildDefinitionSearchToolTip.SetToolTip(this.ClearButton, "Clear");
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // SearchButton
            // 
            this.SearchButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionSearch;
            this.SearchButton.Location = new System.Drawing.Point(384, 42);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(32, 23);
            this.SearchButton.TabIndex = 2;
            this.SearchButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BuildDefinitionSearchToolTip.SetToolTip(this.SearchButton, "Search");
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // BuildDefinitionCheckContextMenuStrip
            // 
            this.BuildDefinitionCheckContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BuildDefinitionSelectAllToolStripMenuItem,
            this.BuildDefinitionSelectNoneToolStripMenuItem,
            this.BuildDefinitionCheckDisabledToolStripItem,
            this.BuildDefinitionSelectEnabledToolStripMenuItem,
            this.BuildDefinitionSelectHighlightedToolStripMenuItem,
            this.toolStripSeparator2,
            this.BuildDefinitionCheckCIToolStripItem,
            this.BuildDefinitionCheckDevToolStripItem,
            this.BuildDefinitionCheckQAToolStripItem,
            this.BuildDefinitionCheckUATToolStripItem,
            this.BuildDefinitionCheckPerfToolStripItem,
            this.BuildDefinitionCheckProductionToolStripItem,
            this.toolStripSeparator4,
            this.BuildDefinitionCheckPOCToolStripItem,
            this.BuildDefinitionCheckTESTToolStripItem,
            this.toolStripSeparator11,
            this.BuildDefinitionCheckDevDRToolStripItem,
            this.BuildDefinitionCheckUATDRToolStripItem,
            this.BuildDefinitionCheckProdDRToolStripItem});
            this.BuildDefinitionCheckContextMenuStrip.Name = "BuildDefinitionCheckContextMenuStrip";
            this.BuildDefinitionCheckContextMenuStrip.ShowCheckMargin = true;
            this.BuildDefinitionCheckContextMenuStrip.ShowImageMargin = false;
            this.BuildDefinitionCheckContextMenuStrip.Size = new System.Drawing.Size(138, 374);
            // 
            // BuildDefinitionSelectAllToolStripMenuItem
            // 
            this.BuildDefinitionSelectAllToolStripMenuItem.Name = "BuildDefinitionSelectAllToolStripMenuItem";
            this.BuildDefinitionSelectAllToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionSelectAllToolStripMenuItem.Text = "All";
            this.BuildDefinitionSelectAllToolStripMenuItem.Click += new System.EventHandler(this.BuildDefinitionCheckAllToolStripItem_Click);
            // 
            // BuildDefinitionSelectNoneToolStripMenuItem
            // 
            this.BuildDefinitionSelectNoneToolStripMenuItem.Name = "BuildDefinitionSelectNoneToolStripMenuItem";
            this.BuildDefinitionSelectNoneToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionSelectNoneToolStripMenuItem.Text = "None";
            this.BuildDefinitionSelectNoneToolStripMenuItem.Click += new System.EventHandler(this.BuildDefinitionChecktNoneToolStripItem_Click);
            // 
            // BuildDefinitionCheckDisabledToolStripItem
            // 
            this.BuildDefinitionCheckDisabledToolStripItem.Name = "BuildDefinitionCheckDisabledToolStripItem";
            this.BuildDefinitionCheckDisabledToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionCheckDisabledToolStripItem.Text = "Disabled";
            this.BuildDefinitionCheckDisabledToolStripItem.Click += new System.EventHandler(this.BuildDefinitionCheckDisabledToolStripItem_Click);
            // 
            // BuildDefinitionSelectEnabledToolStripMenuItem
            // 
            this.BuildDefinitionSelectEnabledToolStripMenuItem.Name = "BuildDefinitionSelectEnabledToolStripMenuItem";
            this.BuildDefinitionSelectEnabledToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionSelectEnabledToolStripMenuItem.Text = "Enabled";
            this.BuildDefinitionSelectEnabledToolStripMenuItem.Click += new System.EventHandler(this.BuildDefinitionCheckEnabledToolStripItem_Click);
            // 
            // BuildDefinitionSelectHighlightedToolStripMenuItem
            // 
            this.BuildDefinitionSelectHighlightedToolStripMenuItem.Name = "BuildDefinitionSelectHighlightedToolStripMenuItem";
            this.BuildDefinitionSelectHighlightedToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionSelectHighlightedToolStripMenuItem.Text = "Highlighted";
            this.BuildDefinitionSelectHighlightedToolStripMenuItem.Click += new System.EventHandler(this.BuildDefinitionCheckHighlightedToolStripItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(134, 6);
            // 
            // BuildDefinitionCheckCIToolStripItem
            // 
            this.BuildDefinitionCheckCIToolStripItem.CheckOnClick = true;
            this.BuildDefinitionCheckCIToolStripItem.Name = "BuildDefinitionCheckCIToolStripItem";
            this.BuildDefinitionCheckCIToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionCheckCIToolStripItem.Text = "CI";
            this.BuildDefinitionCheckCIToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionCheckCIToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionCheckDevToolStripItem
            // 
            this.BuildDefinitionCheckDevToolStripItem.CheckOnClick = true;
            this.BuildDefinitionCheckDevToolStripItem.Name = "BuildDefinitionCheckDevToolStripItem";
            this.BuildDefinitionCheckDevToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionCheckDevToolStripItem.Text = "DEV";
            this.BuildDefinitionCheckDevToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionCheckDevToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionCheckQAToolStripItem
            // 
            this.BuildDefinitionCheckQAToolStripItem.CheckOnClick = true;
            this.BuildDefinitionCheckQAToolStripItem.Name = "BuildDefinitionCheckQAToolStripItem";
            this.BuildDefinitionCheckQAToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionCheckQAToolStripItem.Text = "QA";
            this.BuildDefinitionCheckQAToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionCheckQAToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionCheckUATToolStripItem
            // 
            this.BuildDefinitionCheckUATToolStripItem.CheckOnClick = true;
            this.BuildDefinitionCheckUATToolStripItem.Name = "BuildDefinitionCheckUATToolStripItem";
            this.BuildDefinitionCheckUATToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionCheckUATToolStripItem.Text = "UAT";
            this.BuildDefinitionCheckUATToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionCheckUATToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionCheckPerfToolStripItem
            // 
            this.BuildDefinitionCheckPerfToolStripItem.CheckOnClick = true;
            this.BuildDefinitionCheckPerfToolStripItem.Name = "BuildDefinitionCheckPerfToolStripItem";
            this.BuildDefinitionCheckPerfToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionCheckPerfToolStripItem.Text = "PERF";
            this.BuildDefinitionCheckPerfToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionCheckPerfToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionCheckProductionToolStripItem
            // 
            this.BuildDefinitionCheckProductionToolStripItem.CheckOnClick = true;
            this.BuildDefinitionCheckProductionToolStripItem.Name = "BuildDefinitionCheckProductionToolStripItem";
            this.BuildDefinitionCheckProductionToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionCheckProductionToolStripItem.Text = "PROD";
            this.BuildDefinitionCheckProductionToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionCheckProductionToolStripItem_CheckedChanged);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(134, 6);
            // 
            // BuildDefinitionCheckPOCToolStripItem
            // 
            this.BuildDefinitionCheckPOCToolStripItem.CheckOnClick = true;
            this.BuildDefinitionCheckPOCToolStripItem.Name = "BuildDefinitionCheckPOCToolStripItem";
            this.BuildDefinitionCheckPOCToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionCheckPOCToolStripItem.Text = "POC";
            this.BuildDefinitionCheckPOCToolStripItem.Click += new System.EventHandler(this.BuildDefinitionCheckPOCToolStripItem_Click);
            // 
            // BuildDefinitionCheckTESTToolStripItem
            // 
            this.BuildDefinitionCheckTESTToolStripItem.CheckOnClick = true;
            this.BuildDefinitionCheckTESTToolStripItem.Name = "BuildDefinitionCheckTESTToolStripItem";
            this.BuildDefinitionCheckTESTToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionCheckTESTToolStripItem.Text = "TEST";
            this.BuildDefinitionCheckTESTToolStripItem.Click += new System.EventHandler(this.BuildDefinitionCheckTESTToolStripItem_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(134, 6);
            // 
            // BuildDefinitionCheckDevDRToolStripItem
            // 
            this.BuildDefinitionCheckDevDRToolStripItem.CheckOnClick = true;
            this.BuildDefinitionCheckDevDRToolStripItem.Name = "BuildDefinitionCheckDevDRToolStripItem";
            this.BuildDefinitionCheckDevDRToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionCheckDevDRToolStripItem.Text = "DEV DR";
            this.BuildDefinitionCheckDevDRToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionCheckDevDRToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionCheckUATDRToolStripItem
            // 
            this.BuildDefinitionCheckUATDRToolStripItem.CheckOnClick = true;
            this.BuildDefinitionCheckUATDRToolStripItem.Name = "BuildDefinitionCheckUATDRToolStripItem";
            this.BuildDefinitionCheckUATDRToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionCheckUATDRToolStripItem.Text = "UAT DR";
            this.BuildDefinitionCheckUATDRToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionCheckUATDRToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionCheckProdDRToolStripItem
            // 
            this.BuildDefinitionCheckProdDRToolStripItem.CheckOnClick = true;
            this.BuildDefinitionCheckProdDRToolStripItem.Name = "BuildDefinitionCheckProdDRToolStripItem";
            this.BuildDefinitionCheckProdDRToolStripItem.Size = new System.Drawing.Size(137, 22);
            this.BuildDefinitionCheckProdDRToolStripItem.Text = "PROD DR";
            this.BuildDefinitionCheckProdDRToolStripItem.CheckedChanged += new System.EventHandler(this.BuildDefinitionCheckProdDRToolStripItem_CheckedChanged);
            // 
            // BuildDefinitionListDataGridView
            // 
            this.BuildDefinitionListDataGridView.AllowUserToAddRows = false;
            this.BuildDefinitionListDataGridView.AllowUserToDeleteRows = false;
            this.BuildDefinitionListDataGridView.AllowUserToResizeRows = false;
            this.BuildDefinitionListDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildDefinitionListDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.BuildDefinitionListDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BuildDefinitionListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.BuildDefinitionListDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CheckColumn,
            this.IdColumn,
            this.EnabledColumn,
            this.NameColumn,
            this.TriggerColumn,
            this.QueueNameColumn,
            this.ExtraColumn});
            this.BuildDefinitionListDataGridView.ContextMenuStrip = this.BuildDefinitionListDataGridViewContextMenuStrip;
            this.BuildDefinitionListDataGridView.EnableHeadersVisualStyles = false;
            this.BuildDefinitionListDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.BuildDefinitionListDataGridView.Location = new System.Drawing.Point(13, 210);
            this.BuildDefinitionListDataGridView.Name = "BuildDefinitionListDataGridView";
            this.BuildDefinitionListDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BuildDefinitionListDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.BuildDefinitionListDataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            this.BuildDefinitionListDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.BuildDefinitionListDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.BuildDefinitionListDataGridView.ShowEditingIcon = false;
            this.BuildDefinitionListDataGridView.Size = new System.Drawing.Size(475, 278);
            this.BuildDefinitionListDataGridView.TabIndex = 8;
            this.BuildDefinitionListDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.BuildDefinitionListDataGridView_CellClick);
            this.BuildDefinitionListDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.BuildDefinitionListDataGridView_CellContentClick);
            this.BuildDefinitionListDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.BuildDefinitionListDataGridView_CellDoubleClick);
            this.BuildDefinitionListDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.BuildDefinitionListDataGridView_CellFormatting);
            this.BuildDefinitionListDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.BuildDefinitionListDataGridView_CellValueChanged);
            this.BuildDefinitionListDataGridView.Sorted += new System.EventHandler(this.BuildDefinitionListDataGridView_Sorted);
            // 
            // CheckColumn
            // 
            this.CheckColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.CheckColumn.HeaderText = "";
            this.CheckColumn.MinimumWidth = 15;
            this.CheckColumn.Name = "CheckColumn";
            this.CheckColumn.Width = 15;
            // 
            // IdColumn
            // 
            this.IdColumn.HeaderText = "Id";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.Visible = false;
            // 
            // EnabledColumn
            // 
            this.EnabledColumn.HeaderText = "";
            this.EnabledColumn.MinimumWidth = 22;
            this.EnabledColumn.Name = "EnabledColumn";
            this.EnabledColumn.ReadOnly = true;
            this.EnabledColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.EnabledColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.EnabledColumn.Width = 22;
            // 
            // NameColumn
            // 
            this.NameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.NameColumn.HeaderText = "Name";
            this.NameColumn.MinimumWidth = 100;
            this.NameColumn.Name = "NameColumn";
            this.NameColumn.ReadOnly = true;
            // 
            // TriggerColumn
            // 
            this.TriggerColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.TriggerColumn.HeaderText = "Trigger";
            this.TriggerColumn.MinimumWidth = 60;
            this.TriggerColumn.Name = "TriggerColumn";
            this.TriggerColumn.ReadOnly = true;
            this.TriggerColumn.ToolTipText = "Scheduled or Continuous Integration or None";
            this.TriggerColumn.Width = 65;
            // 
            // QueueNameColumn
            // 
            this.QueueNameColumn.HeaderText = "Queue";
            this.QueueNameColumn.Name = "QueueNameColumn";
            this.QueueNameColumn.ReadOnly = true;
            // 
            // ExtraColumn
            // 
            this.ExtraColumn.HeaderText = "";
            this.ExtraColumn.Name = "ExtraColumn";
            this.ExtraColumn.ReadOnly = true;
            // 
            // BuildDefinitionListDataGridViewContextMenuStrip
            // 
            this.BuildDefinitionListDataGridViewContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CheckSelectedBuildDefinitionToolStripMenuItem,
            this.CopyBuildDefinitionToolStripMenuItem,
            this.ViewBuildDefinitionToolStripMenuItem,
            this.RequestBuildBuildDefinitionToolStripMenuItem});
            this.BuildDefinitionListDataGridViewContextMenuStrip.Name = "BuildDefinitionListDataGridViewContextMenuStrip";
            this.BuildDefinitionListDataGridViewContextMenuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.BuildDefinitionListDataGridViewContextMenuStrip.ShowImageMargin = false;
            this.BuildDefinitionListDataGridViewContextMenuStrip.Size = new System.Drawing.Size(215, 92);
            // 
            // CheckSelectedBuildDefinitionToolStripMenuItem
            // 
            this.CheckSelectedBuildDefinitionToolStripMenuItem.Name = "CheckSelectedBuildDefinitionToolStripMenuItem";
            this.CheckSelectedBuildDefinitionToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.CheckSelectedBuildDefinitionToolStripMenuItem.Text = "Check Selected Build Definition";
            this.CheckSelectedBuildDefinitionToolStripMenuItem.Click += new System.EventHandler(this.CheckSelectedBuildDefinitionToolStripMenuItem_Click);
            // 
            // CopyBuildDefinitionToolStripMenuItem
            // 
            this.CopyBuildDefinitionToolStripMenuItem.Name = "CopyBuildDefinitionToolStripMenuItem";
            this.CopyBuildDefinitionToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.CopyBuildDefinitionToolStripMenuItem.Text = "Copy  Build Definition Name";
            this.CopyBuildDefinitionToolStripMenuItem.Click += new System.EventHandler(this.CopyBuildDefinitionToolStripMenuItem_Click);
            // 
            // ViewBuildDefinitionToolStripMenuItem
            // 
            this.ViewBuildDefinitionToolStripMenuItem.Name = "ViewBuildDefinitionToolStripMenuItem";
            this.ViewBuildDefinitionToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.ViewBuildDefinitionToolStripMenuItem.Text = "View Build Definition";
            this.ViewBuildDefinitionToolStripMenuItem.Click += new System.EventHandler(this.ViewBuildDefinitionToolStripMenuItem_Click);
            // 
            // RequestBuildBuildDefinitionToolStripMenuItem
            // 
            this.RequestBuildBuildDefinitionToolStripMenuItem.Name = "RequestBuildBuildDefinitionToolStripMenuItem";
            this.RequestBuildBuildDefinitionToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.RequestBuildBuildDefinitionToolStripMenuItem.Text = "Request Build";
            this.RequestBuildBuildDefinitionToolStripMenuItem.Click += new System.EventHandler(this.RequestBuildBuildDefinitionToolStripMenuItem_Click);
            // 
            // BuildDefinitionSearchToolTip
            // 
            this.BuildDefinitionSearchToolTip.BackColor = System.Drawing.Color.LightYellow;
            this.BuildDefinitionSearchToolTip.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.BuildDefinitionSearchToolTip_Draw);
            // 
            // ViewSplitButton
            // 
            this.ViewSplitButton.AutoSize = true;
            this.ViewSplitButton.ContextMenuStrip = this.ViewContextMenuStrip;
            this.ViewSplitButton.Location = new System.Drawing.Point(260, 179);
            this.ViewSplitButton.Name = "ViewSplitButton";
            this.ViewSplitButton.Size = new System.Drawing.Size(76, 25);
            this.ViewSplitButton.TabIndex = 5;
            this.ViewSplitButton.Text = "View";
            this.BuildDefinitionSearchToolTip.SetToolTip(this.ViewSplitButton, "Column Views");
            this.ViewSplitButton.UseVisualStyleBackColor = true;
            // 
            // ViewContextMenuStrip
            // 
            this.ViewContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ViewAllToolStripMenuItem,
            this.ViewBasicToolStripMenuItem,
            this.ViewDefaultQueueToolStripMenuItem,
            this.ViewScheduleToolStripMenuItem});
            this.ViewContextMenuStrip.Name = "ViewContextMenuStrip";
            this.ViewContextMenuStrip.Size = new System.Drawing.Size(151, 92);
            // 
            // ViewAllToolStripMenuItem
            // 
            this.ViewAllToolStripMenuItem.Name = "ViewAllToolStripMenuItem";
            this.ViewAllToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.ViewAllToolStripMenuItem.Text = "All";
            this.ViewAllToolStripMenuItem.Click += new System.EventHandler(this.ViewAllToolStripMenuItem_Click);
            // 
            // ViewBasicToolStripMenuItem
            // 
            this.ViewBasicToolStripMenuItem.Name = "ViewBasicToolStripMenuItem";
            this.ViewBasicToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.ViewBasicToolStripMenuItem.Text = "Basic";
            this.ViewBasicToolStripMenuItem.Click += new System.EventHandler(this.ViewBasicToolStripMenuItem_Click);
            // 
            // ViewDefaultQueueToolStripMenuItem
            // 
            this.ViewDefaultQueueToolStripMenuItem.Name = "ViewDefaultQueueToolStripMenuItem";
            this.ViewDefaultQueueToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.ViewDefaultQueueToolStripMenuItem.Text = "Default Queue";
            this.ViewDefaultQueueToolStripMenuItem.Click += new System.EventHandler(this.ViewDefaultQueueToolStripMenuItem_Click);
            // 
            // ViewScheduleToolStripMenuItem
            // 
            this.ViewScheduleToolStripMenuItem.Name = "ViewScheduleToolStripMenuItem";
            this.ViewScheduleToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.ViewScheduleToolStripMenuItem.Text = "Schedule";
            this.ViewScheduleToolStripMenuItem.Click += new System.EventHandler(this.ViewScheduleToolStripMenuItem_Click);
            // 
            // HighlightSplitButton
            // 
            this.HighlightSplitButton.AutoSize = true;
            this.HighlightSplitButton.BackColor = System.Drawing.SystemColors.Control;
            this.HighlightSplitButton.ContextMenuStrip = this.BuildDefinitionHighlightContextMenuStrip;
            this.HighlightSplitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HighlightSplitButton.Location = new System.Drawing.Point(178, 179);
            this.HighlightSplitButton.Name = "HighlightSplitButton";
            this.HighlightSplitButton.Size = new System.Drawing.Size(76, 25);
            this.HighlightSplitButton.TabIndex = 4;
            this.HighlightSplitButton.Text = "Highlight";
            this.BuildDefinitionSearchToolTip.SetToolTip(this.HighlightSplitButton, "Highlight build definitions");
            this.HighlightSplitButton.UseVisualStyleBackColor = false;
            // 
            // DisplaySplitButton
            // 
            this.DisplaySplitButton.AutoSize = true;
            this.DisplaySplitButton.ContextMenuStrip = this.BuildDefinitionDisplayContextMenuStrip;
            this.DisplaySplitButton.Location = new System.Drawing.Point(96, 179);
            this.DisplaySplitButton.Name = "DisplaySplitButton";
            this.DisplaySplitButton.Size = new System.Drawing.Size(76, 25);
            this.DisplaySplitButton.TabIndex = 3;
            this.DisplaySplitButton.Text = "Display";
            this.BuildDefinitionSearchToolTip.SetToolTip(this.DisplaySplitButton, "Select which build definition(s) to display");
            this.DisplaySplitButton.UseVisualStyleBackColor = true;
            // 
            // CheckSplitButton
            // 
            this.CheckSplitButton.AutoSize = true;
            this.CheckSplitButton.ContextMenuStrip = this.BuildDefinitionCheckContextMenuStrip;
            this.CheckSplitButton.Location = new System.Drawing.Point(14, 179);
            this.CheckSplitButton.Name = "CheckSplitButton";
            this.CheckSplitButton.Size = new System.Drawing.Size(76, 25);
            this.CheckSplitButton.TabIndex = 2;
            this.CheckSplitButton.Text = "Check";
            this.CheckSplitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BuildDefinitionSearchToolTip.SetToolTip(this.CheckSplitButton, "Check build definition(s)");
            // 
            // BuildDefinitionStatusPanel
            // 
            this.BuildDefinitionStatusPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildDefinitionStatusPanel.BackColor = System.Drawing.Color.White;
            this.BuildDefinitionStatusPanel.Controls.Add(this.BuildDefinitionRetrievalStatusPanel);
            this.BuildDefinitionStatusPanel.Controls.Add(this.BuildDefinitionMatchStatusPanel);
            this.BuildDefinitionStatusPanel.Location = new System.Drawing.Point(13, 142);
            this.BuildDefinitionStatusPanel.Name = "BuildDefinitionStatusPanel";
            this.BuildDefinitionStatusPanel.Size = new System.Drawing.Size(475, 32);
            this.BuildDefinitionStatusPanel.TabIndex = 1;
            // 
            // BuildDefinitionRetrievalStatusPanel
            // 
            this.BuildDefinitionRetrievalStatusPanel.BackColor = System.Drawing.Color.Transparent;
            this.BuildDefinitionRetrievalStatusPanel.Controls.Add(this.BuildDefinitionRetrievalProgressBar);
            this.BuildDefinitionRetrievalStatusPanel.Controls.Add(this.BuildDefinitionRetrievalStatusLabel);
            this.BuildDefinitionRetrievalStatusPanel.Location = new System.Drawing.Point(-1, 0);
            this.BuildDefinitionRetrievalStatusPanel.MinimumSize = new System.Drawing.Size(320, 20);
            this.BuildDefinitionRetrievalStatusPanel.Name = "BuildDefinitionRetrievalStatusPanel";
            this.BuildDefinitionRetrievalStatusPanel.Size = new System.Drawing.Size(324, 31);
            this.BuildDefinitionRetrievalStatusPanel.TabIndex = 5;
            // 
            // BuildDefinitionRetrievalProgressBar
            // 
            this.BuildDefinitionRetrievalProgressBar.Location = new System.Drawing.Point(6, 20);
            this.BuildDefinitionRetrievalProgressBar.Name = "BuildDefinitionRetrievalProgressBar";
            this.BuildDefinitionRetrievalProgressBar.Size = new System.Drawing.Size(90, 10);
            this.BuildDefinitionRetrievalProgressBar.TabIndex = 2;
            this.BuildDefinitionRetrievalProgressBar.Visible = false;
            // 
            // BuildDefinitionRetrievalStatusLabel
            // 
            this.BuildDefinitionRetrievalStatusLabel.AutoSize = true;
            this.BuildDefinitionRetrievalStatusLabel.Location = new System.Drawing.Point(6, 3);
            this.BuildDefinitionRetrievalStatusLabel.Name = "BuildDefinitionRetrievalStatusLabel";
            this.BuildDefinitionRetrievalStatusLabel.Size = new System.Drawing.Size(167, 13);
            this.BuildDefinitionRetrievalStatusLabel.TabIndex = 3;
            this.BuildDefinitionRetrievalStatusLabel.Text = "<Build Definition Retrieval Status>";
            // 
            // BuildDefinitionMatchStatusPanel
            // 
            this.BuildDefinitionMatchStatusPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildDefinitionMatchStatusPanel.BackColor = System.Drawing.Color.Transparent;
            this.BuildDefinitionMatchStatusPanel.Controls.Add(this.BuildDefinitionMatchStatusLabel);
            this.BuildDefinitionMatchStatusPanel.Location = new System.Drawing.Point(329, 6);
            this.BuildDefinitionMatchStatusPanel.Name = "BuildDefinitionMatchStatusPanel";
            this.BuildDefinitionMatchStatusPanel.Size = new System.Drawing.Size(140, 20);
            this.BuildDefinitionMatchStatusPanel.TabIndex = 3;
            // 
            // BuildDefinitionMatchStatusLabel
            // 
            this.BuildDefinitionMatchStatusLabel.AutoSize = true;
            this.BuildDefinitionMatchStatusLabel.Location = new System.Drawing.Point(3, 3);
            this.BuildDefinitionMatchStatusLabel.Name = "BuildDefinitionMatchStatusLabel";
            this.BuildDefinitionMatchStatusLabel.Size = new System.Drawing.Size(49, 13);
            this.BuildDefinitionMatchStatusLabel.TabIndex = 3;
            this.BuildDefinitionMatchStatusLabel.Text = "<Status>";
            // 
            // BuildDefinitionSearchForm
            // 
            this.AcceptButton = this.SearchButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 500);
            this.ControlBox = false;
            this.Controls.Add(this.ViewSplitButton);
            this.Controls.Add(this.BuildDefinitionStatusPanel);
            this.Controls.Add(this.HighlightSplitButton);
            this.Controls.Add(this.DisplaySplitButton);
            this.Controls.Add(this.CheckSplitButton);
            this.Controls.Add(this.CopyButton);
            this.Controls.Add(this.SearchCriteriaGroupBox);
            this.Controls.Add(this.BuildDefinitionListDataGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(500, 500);
            this.Name = "BuildDefinitionSearchForm";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.BuildDefinitionDisplayContextMenuStrip.ResumeLayout(false);
            this.BuildDefinitionHighlightContextMenuStrip.ResumeLayout(false);
            this.SearchCriteriaGroupBox.ResumeLayout(false);
            this.SearchCriteriaGroupBox.PerformLayout();
            this.BuildDefinitionNameFilterContextMenuStrip.ResumeLayout(false);
            this.BuildDefinitionCheckContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BuildDefinitionListDataGridView)).EndInit();
            this.BuildDefinitionListDataGridViewContextMenuStrip.ResumeLayout(false);
            this.ViewContextMenuStrip.ResumeLayout(false);
            this.BuildDefinitionStatusPanel.ResumeLayout(false);
            this.BuildDefinitionRetrievalStatusPanel.ResumeLayout(false);
            this.BuildDefinitionRetrievalStatusPanel.PerformLayout();
            this.BuildDefinitionMatchStatusPanel.ResumeLayout(false);
            this.BuildDefinitionMatchStatusPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip BuildDefinitionDisplayContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayCheckedToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayHighlightedToolStripItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayAllToolStripItem;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.Button CopyButton;
        private System.Windows.Forms.ContextMenuStrip BuildDefinitionHighlightContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionHighlightDisabledToolStripItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionHighlightCIToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionHighlightDevToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionHighlightQAToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionHighlightUATToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionHighlightProductionToolStripItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionHighlightNoneToolStripItem;
        private System.Windows.Forms.GroupBox SearchCriteriaGroupBox;
        private System.Windows.Forms.RichTextBox BuildDefinitionNameFilterTextBox;
        private System.Windows.Forms.ContextMenuStrip BuildDefinitionNameFilterContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionNameFilterCutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionNameFilterCopyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionNameFilterPasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionNameFilterDeleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionNameFilterSelectAllToolStripMenuItem;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.ContextMenuStrip BuildDefinitionCheckContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionSelectEnabledToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionSelectHighlightedToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionSelectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionSelectNoneToolStripMenuItem;
        private System.Windows.Forms.DataGridView BuildDefinitionListDataGridView;
        private System.Windows.Forms.ContextMenuStrip BuildDefinitionListDataGridViewContextMenuStrip;
        private SplitButton CheckSplitButton;
        private SplitButton DisplaySplitButton;
        private SplitButton HighlightSplitButton;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayEnabledToolStripItem;
        private System.Windows.Forms.ToolTip BuildDefinitionSearchToolTip;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayDisabledToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionCheckDisabledToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionHighlightDevDRToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionHighlightUATDRToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionHighlightProdDRToolStripItem;
        private System.Windows.Forms.Panel BuildDefinitionStatusPanel;
        private System.Windows.Forms.ToolStripMenuItem CopyBuildDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ViewBuildDefinitionToolStripMenuItem;
        private System.Windows.Forms.CheckBox ExactMatchCheckBox;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayCIToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayDevToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayQAToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayUATToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayProductionToolStripItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayDevDRToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayUATDRToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayProdDRToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionCheckCIToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionCheckDevToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionCheckQAToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionCheckUATToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionCheckProductionToolStripItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionCheckDevDRToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionCheckUATDRToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionCheckProdDRToolStripItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem CheckSelectedBuildDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RequestBuildBuildDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionCheckPerfToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayPerfToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionHighlightPerfToolStripItem;
        private System.Windows.Forms.ContextMenuStrip ViewContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ViewScheduleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ViewDefaultQueueToolStripMenuItem;
        private SplitButton ViewSplitButton;
        private System.Windows.Forms.ToolStripMenuItem ViewBasicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ViewAllToolStripMenuItem;
        private System.Windows.Forms.ToolTip SidekickToolTip;
        private System.Windows.Forms.Panel BuildDefinitionMatchStatusPanel;
        private System.Windows.Forms.Label BuildDefinitionMatchStatusLabel;
        private System.Windows.Forms.Panel BuildDefinitionRetrievalStatusPanel;
        private System.Windows.Forms.ProgressBar BuildDefinitionRetrievalProgressBar;
        private System.Windows.Forms.Label BuildDefinitionRetrievalStatusLabel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayPOCToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionDisplayTESTToolStripItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionHighlightPOCToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionHighlightTESTToolStripItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionCheckPOCToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem BuildDefinitionCheckTESTToolStripItem;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewImageColumn EnabledColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TriggerColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn QueueNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExtraColumn;
    }
}