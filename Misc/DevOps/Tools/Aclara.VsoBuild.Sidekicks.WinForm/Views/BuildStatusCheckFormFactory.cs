﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using System;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build definition clone form factory.
    /// </summary>
    public static class BuildStatusCheckFormFactory
    {

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <param name="sidekickCoaction"></param>
        /// <returns></returns>
        public static BuildStatusCheckForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                          SidekickConfiguration sidekickConfiguration,
                                                          BuildDefinitionSearchForm buildDefinitionSearchForm,
                                                          ISidekickCoaction sidekickCoaction)
        {

            BuildStatusCheckForm result = null;
            BuildStatusCheckPresenter BuildStatusCheckPresenter = null;

            try
            {
                result = new BuildStatusCheckForm(sidekickConfiguration, buildDefinitionSearchForm, sidekickCoaction);
                BuildStatusCheckPresenter = new BuildStatusCheckPresenter(teamProjectInfo, sidekickConfiguration, result);
                result.BuildStatusCheckPresenter = BuildStatusCheckPresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


    }
}
