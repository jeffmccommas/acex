﻿using Aclara.Tools.Common;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build definition clone view - Interface.
    /// </summary>
    public interface IBuildDefinitionChangeSettingsView
    {
        string SidekickName { get; }

        string SidekickDescription { get; }

        SidekickContainerTypes.SidekickWarningLevel SidekickWarningLevel { get; }

        string DefaultAgentQueueName { get; set; }

        string BackgroundTaskStatus { get; set; }

        string ApplyButtonText { get; set; }

        void ApplyButtonEnable(bool enable);

        bool IsSidekickBusy { get; }

        void RetrieveRemainingDefinitions();

        void TeamProjectChanged();

        void UpdateProgressChangeSetting(int buildDefinitionChangedCount, int buildDefinitionTotalCount);

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);
    }

}