﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class BuildDefinitionDeleteForm : Form, IBuildDefinitionDeleteView, ISidekickView, ISidekickInfo
    {

        #region Private Constants
        private const string SidekickView_SidekickName = "DeleteBuildDefinitions";
        private const string SidekickView_SidekickDescription = "Delete Build Definitions";

        private const string ValidationErrorMessage_TeamProjectNameNotSelected = "Team project not selected.";
        private const string ValidationErrorMessage_BranchNameSourceNotEntered = "Branch name source not entered.";
        private const string ValidationErrorMessage_BuildDefinitionNotSelected = "Build definition not selected.";
        private const string ValidationErrorMessage_SourceBranchNameNotEntered = "Branch name - source not entered.";
        private const string ValidationErrorMessage_SourceBuildDefinitionNamePrefixNotEntered = "Build definition name prefix - source not entered.";
        private const string ValidationErrorMessage_TargetBranchNameNotEntered = "Branch name - target not entered.";
        private const string ValidationErrorMessage_TargetBuildDefinitionNamePrefixNotEntered = "Build definition name prefix - target not entered.";
        private const string ValidationErrorMessage_SelectedBuildDefinitionsMustHaveSameNamePrefix = "Selected build definitons must have same name prefix.";

        private const string DeleteBuildDefinitionConfirmation_PendingAction = "Are you sure you want to to delete selected build definitions?";
        private const string DeleteBuildDefinitionConfirmation_ConfirmationMessage = @"Type ""YES"" to confirm build definition deletion.";
        private const string DeleteBuildDefinitionConfirmation_ExpectedTypeResponse = "YES";

        private const string BuildDefinitionDeletionStatus_Suffix = "{0} build definition(s) selected for deletion:";

        #endregion

        #region Private Data Members

        private SidekickConfiguration _sidekickConfiguration = null;
        private BuildDefinitionSearchForm _buildDefinitionSearchForm = null;
        private BuildDefinitionDeletePresenter _buildDefinitionClonePresenter;
        private BuildDefinitionSelectorList _buildDefinitionSelectorList;
        private BindingSource _buildDefinitionSelectorListBindingSource;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickView_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickView_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                if (this.BuildDefinitionDeletePresenter.IsBackgroundTaskBusy == false)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick warning level.
        /// </summary>
        public SidekickContainerTypes.SidekickWarningLevel SidekickWarningLevel
        {
            get
            {
                return SidekickContainerTypes.SidekickWarningLevel.Low;
            }
        }

        /// <summary>
        /// Property: Build defintion delete presenter.
        /// </summary>
        public BuildDefinitionDeletePresenter BuildDefinitionDeletePresenter
        {
            get { return _buildDefinitionClonePresenter; }
            set { _buildDefinitionClonePresenter = value; }
        }

        /// <summary>
        /// Property: Build definition search form.
        /// </summary>
        public BuildDefinitionSearchForm BuildDefinitionSearchForm
        {
            get { return _buildDefinitionSearchForm; }
            set { _buildDefinitionSearchForm = value; }
        }

        /// <summary>
        /// Property: Build definition deletion status.
        /// </summary>
        public string BuildDefinitionDeletionStatus
        {
            get { return this.BuildDefinitionListLabel.Text; }
            set { this.BuildDefinitionListLabel.Text = value; }
        }

        /// <summary>
        /// Property: Build definition selector list.
        /// </summary>
        public BuildDefinitionSelectorList BuildDefinitionSelectorList
        {
            get { return _buildDefinitionSelectorList; }
            set { _buildDefinitionSelectorList = value; }
        }

        /// <summary>
        /// Property: Build build definition selector list binding source.
        /// </summary>
        public BindingSource BuildDefinitionSelectorListBindingSource
        {
            get { return _buildDefinitionSelectorListBindingSource; }
            set { _buildDefinitionSelectorListBindingSource = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get { return this.ApplyButton.Text; }
            set { this.ApplyButton.Text = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildDefinitionDeleteForm()
        {
            InitializeComponent();
            InitializeControls();

        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        public BuildDefinitionDeleteForm(SidekickConfiguration sidekickConfiguration,
                                        BuildDefinitionSearchForm buildDefinitionSearchForm)
        {
            InitializeComponent();
            InitializeControls();
            this.SidekickConfiguration = sidekickConfiguration;
            this.BuildDefinitionSearchForm = buildDefinitionSearchForm;
            this.BuildDefinitionSearchForm.BuildDefinitionSelectionChanged += OnBuildDefinitionSelectionChanged;

            _buildDefinitionSelectorList = new BuildDefinitionSelectorList();
            _buildDefinitionSelectorListBindingSource = new BindingSource();
            _buildDefinitionSelectorListBindingSource.DataSource = _buildDefinitionSelectorList;
            this.BuildDefinitionSelectorListBox.DisplayMember = "Name";
            this.BuildDefinitionSelectorListBox.DataSource = _buildDefinitionSelectorListBindingSource;

            this.BuildDefinitionSearchPanel.DockControl(this.BuildDefinitionSearchForm);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update progress delete build definition.
        /// </summary>
        /// <param name="buildDefinitionDeletedCount"></param>
        /// <param name="buildDefinitionTotalCount"></param>
        public void UpdateProgressDeleteBuildDefinition(int buildDefinitionDeletedCount, int buildDefinitionTotalCount)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<int, int>(this.UpdateProgressDeleteBuildDefinition), buildDefinitionDeletedCount, buildDefinitionTotalCount);
            }
            else
            {
                if (buildDefinitionDeletedCount < buildDefinitionTotalCount)
                {
                    this.DeleteBuildDefinitionProgressBar.Visible = true;
                    this.DeleteBuildDefinitionProgressBar.Minimum = 0;
                    this.DeleteBuildDefinitionProgressBar.Maximum = buildDefinitionTotalCount;
                    this.DeleteBuildDefinitionProgressBar.Value = buildDefinitionDeletedCount;
                }
                else if (buildDefinitionDeletedCount == buildDefinitionTotalCount)
                {
                    this.DeleteBuildDefinitionProgressBar.Visible = false;
                }
            }
        }

        /// <summary>
        /// Retrieve remaining build definitions.
        /// </summary>
        public void RetrieveRemainingDefinitions()
        {
            this.BuildDefinitionSearchForm.RetrieveRemainingDefinitions();
        }

        /// <summary>
        /// Apply button enable.
        /// </summary>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            this.BuildDefinitionSearchForm.TeamProjectChanged();
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task Completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {
                this.EnableControlsDependantOnSelectedBuildDefinition(false);
                this.UpdateBuildDefinitionDeletionStatus();
                this.DeleteBuildDefinitionProgressBar.Visible = false;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Update build definition deletion status.
        /// </summary>
        protected void UpdateBuildDefinitionDeletionStatus()
        {
            try
            {
                BuildDefinitionDeletionStatus = string.Format(BuildDefinitionDeletionStatus_Suffix,
                                                              this.BuildDefinitionSelectorListBox.Items.Count);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Hanlder: Build definition selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="requestBuildEventArgs"></param>
        protected void OnBuildDefinitionSelectionChanged(Object sender,
                                                         BuildDefinitionSelectionChangedEventArgs buildDefinitionSelectionChangedEventArgs)
        {

            BuildDefinitionSelectorList buildDefinitionSelectorList = null;

            try
            {
                this.BuildDefinitionSelectorList.Clear();

                if (buildDefinitionSelectionChangedEventArgs != null)
                {

                    if (buildDefinitionSelectionChangedEventArgs.OneOrMoreSelected == true)
                    {
                        buildDefinitionSelectorList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();

                        foreach (BuildDefinitionSelector buildDefinitionSelector in buildDefinitionSelectorList)
                        {
                            this.BuildDefinitionSelectorList.Add(buildDefinitionSelector);
                        }

                        if (buildDefinitionSelectorList.Count > 0)
                        {
                            this.EnableControlsDependantOnSelectedBuildDefinition(true);
                        }

                    }
                    else
                    {
                        this.EnableControlsDependantOnSelectedBuildDefinition(false);
                    }

                }
                this.BuildDefinitionSelectorListBindingSource.ResetBindings(false);

                UpdateBuildDefinitionDeletionStatus();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Enable/disable controls dependant on at lease one build definition selected.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnSelectedBuildDefinition(bool enable)
        {
            try
            {

                this.ApplyButton.Enabled = enable;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelectorList buildDefinitionSelectorList = null;
            DialogResult dialogResult;
            ConfirmationForm confirmationForm = null;

            try
            {

                if (this.BuildDefinitionDeletePresenter.TeamProjectInfo.TeamProjectCollectionUri == null ||
                    this.BuildDefinitionDeletePresenter.TeamProjectInfo.TeamProjectName == string.Empty)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_TeamProjectNameNotSelected));
                    return;
                }

                buildDefinitionSelectorList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();
                if (buildDefinitionSelectorList == null ||
                    buildDefinitionSelectorList.Count <= 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_BuildDefinitionNotSelected));
                    return;
                }

                //Request confirmation.
                confirmationForm = ConfirmationFormFactory.CreateForm(DeleteBuildDefinitionConfirmation_PendingAction,
                                                                      DeleteBuildDefinitionConfirmation_ConfirmationMessage,
                                                                      DeleteBuildDefinitionConfirmation_ExpectedTypeResponse,
                                                                      true,
                                                                      false,
                                                                      string.Empty);
                confirmationForm.StartPosition = FormStartPosition.CenterParent;
                dialogResult = confirmationForm.ShowDialog(this);
                if (dialogResult == DialogResult.OK)
                {
                    this.DeleteBuildDefinitionProgressBar.Visible = false;
                    this.BuildDefinitionDeletePresenter.DeleteBuildDefinitions(buildDefinitionSelectorList);
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickDeleteBuildDefinitions.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

    }
}
