﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.Vso.Build.Client;
using Aclara.Vso.Build.Client.Events;
using Aclara.Vso.Build.Client.Types;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using Microsoft.TeamFoundation.SourceControl.WebApi;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build definition clone - presenter.
    /// </summary>
    public class BuildDefinitionClonePresenter
    {

        #region Private Constants

        private const string NotAvailable = "N/A";

        #endregion

        #region Private Data Members

        private CustomLogger _logger = null; private SidekickConfiguration _sidekickConfiguration = null;
        private IBuildDefinitionCloneView _buildDefinitionCloneView;
        private ITeamProjectInfo _teamProjectInfo = null;
        private BuildDefinitionManager _buildDefinitionManager;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.BuildDefinitionCloneView.SidekickName,
                                               this.BuildDefinitionCloneView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Build definiton clone view.
        /// </summary>
        public IBuildDefinitionCloneView BuildDefinitionCloneView
        {
            get { return _buildDefinitionCloneView; }
            set { _buildDefinitionCloneView = value; }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Build definition manager.
        /// </summary>
        public BuildDefinitionManager BuildDefinitionManager
        {
            get
            {
                if (_buildDefinitionManager == null)
                {
                    _buildDefinitionManager = this.CreateBuildDefinitionManager();
                }
                return _buildDefinitionManager;
            }
            set { _buildDefinitionManager = value; }

        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionCloneView"></param>
        public BuildDefinitionClonePresenter(ITeamProjectInfo teamProjectInfo,
                                             SidekickConfiguration sidekickConfiguration,
                                             IBuildDefinitionCloneView buildDefinitionCloneView)
        {
            this.TeamProjectInfo = teamProjectInfo;
            this.SidekickConfiguration = sidekickConfiguration;
            this.BuildDefinitionCloneView = buildDefinitionCloneView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private BuildDefinitionClonePresenter()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalBuildDuration;
            AggregateException aggregateException = null;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalBuildDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.BuildDefinitionCloneView.BackgroundTaskCompleted("Clone build definition request canceled.");
                        this.BuildDefinitionCloneView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format("Clone build definition request cancelled."));

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format("[*] Clone build definition request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                         totalBuildDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.BuildDefinitionCloneView.BackgroundTaskCompleted("");
                        this.BuildDefinitionCloneView.ApplyButtonEnable(true);


                        //Log background task completed log entry.

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format("Operation cancelled."));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format("Operation cancelled."));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format("Request failed."));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format("Request failed."));
                                    }

                                }
                            }
                        }

                        this.Logger.Info(string.Format("[*] Clone build definition request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                         totalBuildDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.BuildDefinitionCloneView.BackgroundTaskCompleted("Clone build definition request completed.");
                        this.BuildDefinitionCloneView.ApplyButtonEnable(true);

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format("[*] Clone build definition request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                         totalBuildDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Clone build definitions.        /// </summary>
        /// <param name="sourceBuildDefinitionNamePrefix"></param>
        /// <param name="targetBuildDefinitionNamePrefix"></param>
        /// <param name="sourceBranchName"></param>
        /// <param name="targetBranchName"></param>
        /// <param name="buildDefinitionCloneProperties"></param>
        /// <param name="buildDefinitionSelectorList"></param>
        public void CloneBuildDefinitions(string sourceBuildDefinitionNamePrefix,
                                          string targetBuildDefinitionNamePrefix,
                                          string sourceBranchName,
                                          string targetBranchName,
                                          BuildDefinitionCloneProperties buildDefinitionCloneProperties,
                                          BuildDefinitionSelectorList buildDefinitionSelectorList)
        {

            List<int> filteredBuidDefinitionIdList = null;
            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.BuildDefinitionCloneView.BackgroundTaskStatus = "Cancelling clone build definition request...";
                    this.BuildDefinitionCloneView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.BuildDefinitionCloneView.BackgroundTaskStatus = "Clone build definition(s) requested...";
                    this.BuildDefinitionCloneView.ApplyButtonText = "Cancel";

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    //Reterieve fresh build definition list. (In case build definitions have changed since last retrieval).
                    filteredBuidDefinitionIdList = buildDefinitionSelectorList.GetBuildDefinitionIdList();

                    this.BuildDefinitionManager.BuildDefinitionCloned += OnBuildDefinitionCloned;

                    this.BackgroundTask = new Task(() => this.BuildDefinitionManager.CloneBuildDefinitions(filteredBuidDefinitionIdList,
                                                                                                           sourceBuildDefinitionNamePrefix,
                                                                                                           targetBuildDefinitionNamePrefix,
                                                                                                           sourceBranchName,
                                                                                                           targetBranchName,
                                                                                                           buildDefinitionCloneProperties,
                                                                                                           cancellationToken),
                                                                                                           cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Retrieve list of Git repository selectors.
        /// </summary>
        /// <returns></returns>
        public GitRepositorySelectorList GetGitRepositorySelectorList()
        {
            GitRepositorySelectorList result = null;
            List<GitRepository> gitRepositoryList = null;
            GitRepositorySelector gitRepositorySelector = null;
            try
            {
                result = new GitRepositorySelectorList();

                gitRepositoryList = this.BuildDefinitionManager.GetGitRepositoriesRestApi();

                foreach (GitRepository gitRepository in gitRepositoryList)
                {
                    gitRepositorySelector = new GitRepositorySelector(gitRepository);

                    result.Add(gitRepositorySelector);
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            this.BuildDefinitionManager = null;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create build definition manager.
        /// </summary>
        /// <returns></returns>
        protected BuildDefinitionManager CreateBuildDefinitionManager()
        {
            BuildDefinitionManager result = null;

            try
            {
                result = BuildDefinitionManagerFactory.CreateBuildDefinitionManager(this.TeamProjectInfo.TeamProjectCollectionUri,
                                                                                    this.TeamProjectInfo.TeamProjectCollectionVsrmUri, 
                                                                                    this.TeamProjectInfo.TeamProjectName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIUserProfileName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIPassword);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: Build definition cloned.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="cloneBuildDefinitionEventArgs"></param>
        protected void OnBuildDefinitionCloned(Object sender,
                                               CloneBuildDefinitionEventArgs cloneBuildDefinitionEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Status list details available.
                if (cloneBuildDefinitionEventArgs.StatusList != null &&
                    cloneBuildDefinitionEventArgs.StatusList.Count > 0)
                {
                    if (cloneBuildDefinitionEventArgs.SourceBuildDefinition != null)
                    {
                        message = string.Format("Build definition clone canceled or failed. (Source build definition name: {0}; Status --> {1})",
                                                cloneBuildDefinitionEventArgs.SourceBuildDefinition.Name ?? NotAvailable,
                                                cloneBuildDefinitionEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));
                    }
                    else
                    {
                        message = string.Format("Build definition clone canceled or failed. (Status --> {0})",
                                                cloneBuildDefinitionEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));
                    }

                    if (cloneBuildDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error) == true)
                    {
                        if (cloneBuildDefinitionEventArgs.SourceBuildDefinition != null)
                        {
                            message = string.Format("Build definition clone canceled or failed. (Source build definition name: {0}; Status --> {1})",
                                                    cloneBuildDefinitionEventArgs.SourceBuildDefinition.Name ?? NotAvailable,
                                                    cloneBuildDefinitionEventArgs.StatusList.Format(StatusTypes.FormatOption.Full));
                        }
                        else
                        {
                            message = string.Format("Build definition clone canceled or failed. (Status --> {0})",
                                                    cloneBuildDefinitionEventArgs.StatusList.Format(StatusTypes.FormatOption.Full));
                        }
                        this.Logger.Error(message);

                    }
                    else if (cloneBuildDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Warning) == true)
                    {
                        this.Logger.Warn(message);
                    }
                    else
                    {
                        this.Logger.Info(message);
                    }

                }
                //Build definition details available and status list is unavailable.
                else
                {
                    this.Logger.Info(string.Format("Build definition cloned. (Build definition name: {0})",
                                                     cloneBuildDefinitionEventArgs.TargetBuildDefinition.Name ?? NotAvailable));
                }

                this.BuildDefinitionCloneView.UpdateProgressCloneBuildDefinition(cloneBuildDefinitionEventArgs.BuildDefinitionClonedCount,
                                                                                 cloneBuildDefinitionEventArgs.BuildDefinitionTotalCount);

            }
            catch (Exception)
            {
                throw;
            }


        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                        }
                        else
                        {
                            Logger.Error(string.Format("Operation cancelled.(Unobserved)"));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                }
                else
                {
                    Logger.Error(string.Format("Operation cancelled.(Unobserved)"));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
