﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Views;
using System;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public class NotificationAdminFormFactory
    {
        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <returns></returns>
        public static NotificationAdminForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                       SidekickConfiguration sidekickConfiguration,
                                                       BuildDefinitionSearchForm buildDefinitionSearchForm)
        {

            NotificationAdminForm result = null;
            NotificationAdminPresenter notificationAdminPresenter = null;

            try
            {
                result = new NotificationAdminForm(sidekickConfiguration,
                                                   buildDefinitionSearchForm);
                notificationAdminPresenter = new NotificationAdminPresenter(teamProjectInfo, 
                                                                            sidekickConfiguration, 
                                                                            result);
                result.NotificationAdminPresenter = notificationAdminPresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
