﻿namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    partial class VSTSProjectConnectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VSTSProjectConnectForm));
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.AccountComboBox = new System.Windows.Forms.ComboBox();
            this.AccountLabel = new System.Windows.Forms.Label();
            this.AccountsAndProjectsPanel = new System.Windows.Forms.Panel();
            this.AccoutAndProjectsLabel = new System.Windows.Forms.Label();
            this.TeamProjectComboBox = new System.Windows.Forms.ComboBox();
            this.ProjectLabel = new System.Windows.Forms.Label();
            this.CredentialsPanel = new System.Windows.Forms.Panel();
            this.HelpButton = new System.Windows.Forms.Button();
            this.ValidateCredentialsButton = new System.Windows.Forms.Button();
            this.VerificationStatusLabel = new System.Windows.Forms.Label();
            this.CreatePATLinkLabel = new System.Windows.Forms.LinkLabel();
            this.PersonalAccessTokenTextBox = new System.Windows.Forms.TextBox();
            this.UserProfileNameTextBox = new System.Windows.Forms.TextBox();
            this.PersonalAccessTokenLabel = new System.Windows.Forms.Label();
            this.UserProfileNameLabel = new System.Windows.Forms.Label();
            this.OverrideCredentialsCheckBox = new System.Windows.Forms.CheckBox();
            this.CredentialsLabel = new System.Windows.Forms.Label();
            this.HelpProvider = new System.Windows.Forms.HelpProvider();
            this.AccountsAndProjectsPanel.SuspendLayout();
            this.CredentialsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // OKButton
            // 
            this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OKButton.Location = new System.Drawing.Point(473, 240);
            this.OKButton.Margin = new System.Windows.Forms.Padding(2);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(56, 23);
            this.OKButton.TabIndex = 2;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(534, 240);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(2);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(56, 23);
            this.CancelButton.TabIndex = 3;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // AccountComboBox
            // 
            this.AccountComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AccountComboBox.FormattingEnabled = true;
            this.AccountComboBox.Location = new System.Drawing.Point(56, 31);
            this.AccountComboBox.Margin = new System.Windows.Forms.Padding(2);
            this.AccountComboBox.Name = "AccountComboBox";
            this.AccountComboBox.Size = new System.Drawing.Size(516, 21);
            this.AccountComboBox.TabIndex = 2;
            // 
            // AccountLabel
            // 
            this.AccountLabel.AutoSize = true;
            this.AccountLabel.Location = new System.Drawing.Point(7, 35);
            this.AccountLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.AccountLabel.Name = "AccountLabel";
            this.AccountLabel.Size = new System.Drawing.Size(50, 13);
            this.AccountLabel.TabIndex = 1;
            this.AccountLabel.Text = "Account:";
            // 
            // AccountsAndProjectsPanel
            // 
            this.AccountsAndProjectsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AccountsAndProjectsPanel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.AccountsAndProjectsPanel.Controls.Add(this.AccoutAndProjectsLabel);
            this.AccountsAndProjectsPanel.Controls.Add(this.AccountComboBox);
            this.AccountsAndProjectsPanel.Controls.Add(this.AccountLabel);
            this.AccountsAndProjectsPanel.Controls.Add(this.TeamProjectComboBox);
            this.AccountsAndProjectsPanel.Controls.Add(this.ProjectLabel);
            this.AccountsAndProjectsPanel.Location = new System.Drawing.Point(9, 146);
            this.AccountsAndProjectsPanel.Margin = new System.Windows.Forms.Padding(2);
            this.AccountsAndProjectsPanel.Name = "AccountsAndProjectsPanel";
            this.AccountsAndProjectsPanel.Size = new System.Drawing.Size(581, 89);
            this.AccountsAndProjectsPanel.TabIndex = 1;
            // 
            // AccoutAndProjectsLabel
            // 
            this.AccoutAndProjectsLabel.AutoSize = true;
            this.AccoutAndProjectsLabel.Location = new System.Drawing.Point(7, 9);
            this.AccoutAndProjectsLabel.Name = "AccoutAndProjectsLabel";
            this.AccoutAndProjectsLabel.Size = new System.Drawing.Size(108, 13);
            this.AccoutAndProjectsLabel.TabIndex = 0;
            this.AccoutAndProjectsLabel.Text = "Account and projects";
            // 
            // TeamProjectComboBox
            // 
            this.TeamProjectComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TeamProjectComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TeamProjectComboBox.FormattingEnabled = true;
            this.TeamProjectComboBox.Location = new System.Drawing.Point(56, 58);
            this.TeamProjectComboBox.Name = "TeamProjectComboBox";
            this.TeamProjectComboBox.Size = new System.Drawing.Size(517, 21);
            this.TeamProjectComboBox.TabIndex = 4;
            // 
            // ProjectLabel
            // 
            this.ProjectLabel.AutoSize = true;
            this.ProjectLabel.Location = new System.Drawing.Point(7, 62);
            this.ProjectLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ProjectLabel.Name = "ProjectLabel";
            this.ProjectLabel.Size = new System.Drawing.Size(43, 13);
            this.ProjectLabel.TabIndex = 3;
            this.ProjectLabel.Text = "Project:";
            // 
            // CredentialsPanel
            // 
            this.CredentialsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CredentialsPanel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.CredentialsPanel.Controls.Add(this.HelpButton);
            this.CredentialsPanel.Controls.Add(this.ValidateCredentialsButton);
            this.CredentialsPanel.Controls.Add(this.VerificationStatusLabel);
            this.CredentialsPanel.Controls.Add(this.CreatePATLinkLabel);
            this.CredentialsPanel.Controls.Add(this.PersonalAccessTokenTextBox);
            this.CredentialsPanel.Controls.Add(this.UserProfileNameTextBox);
            this.CredentialsPanel.Controls.Add(this.PersonalAccessTokenLabel);
            this.CredentialsPanel.Controls.Add(this.UserProfileNameLabel);
            this.CredentialsPanel.Controls.Add(this.OverrideCredentialsCheckBox);
            this.CredentialsPanel.Controls.Add(this.CredentialsLabel);
            this.CredentialsPanel.Location = new System.Drawing.Point(9, 8);
            this.CredentialsPanel.Margin = new System.Windows.Forms.Padding(2);
            this.CredentialsPanel.Name = "CredentialsPanel";
            this.CredentialsPanel.Size = new System.Drawing.Size(581, 134);
            this.CredentialsPanel.TabIndex = 0;
            // 
            // HelpButton
            // 
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(545, 62);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 6;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // ValidateCredentialsButton
            // 
            this.ValidateCredentialsButton.Location = new System.Drawing.Point(452, 91);
            this.ValidateCredentialsButton.Name = "ValidateCredentialsButton";
            this.ValidateCredentialsButton.Size = new System.Drawing.Size(121, 23);
            this.ValidateCredentialsButton.TabIndex = 8;
            this.ValidateCredentialsButton.Text = "Validate Credentials";
            this.ValidateCredentialsButton.UseVisualStyleBackColor = true;
            this.ValidateCredentialsButton.Click += new System.EventHandler(this.ValidateCredentialsButton_Click);
            // 
            // VerificationStatusLabel
            // 
            this.VerificationStatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.VerificationStatusLabel.AutoSize = true;
            this.VerificationStatusLabel.Location = new System.Drawing.Point(5, 107);
            this.VerificationStatusLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.VerificationStatusLabel.Name = "VerificationStatusLabel";
            this.VerificationStatusLabel.Size = new System.Drawing.Size(104, 13);
            this.VerificationStatusLabel.TabIndex = 9;
            this.VerificationStatusLabel.Text = "<verification_status>";
            // 
            // CreatePATLinkLabel
            // 
            this.CreatePATLinkLabel.AutoSize = true;
            this.CreatePATLinkLabel.Location = new System.Drawing.Point(119, 87);
            this.CreatePATLinkLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.CreatePATLinkLabel.Name = "CreatePATLinkLabel";
            this.CreatePATLinkLabel.Size = new System.Drawing.Size(154, 13);
            this.CreatePATLinkLabel.TabIndex = 7;
            this.CreatePATLinkLabel.TabStop = true;
            this.CreatePATLinkLabel.Text = "Create Personal Access Token";
            this.CreatePATLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.CreatePATLinkLabel_LinkClicked);
            // 
            // PersonalAccessTokenTextBox
            // 
            this.PersonalAccessTokenTextBox.Location = new System.Drawing.Point(122, 65);
            this.PersonalAccessTokenTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.PersonalAccessTokenTextBox.Name = "PersonalAccessTokenTextBox";
            this.PersonalAccessTokenTextBox.PasswordChar = '*';
            this.PersonalAccessTokenTextBox.Size = new System.Drawing.Size(423, 20);
            this.PersonalAccessTokenTextBox.TabIndex = 5;
            // 
            // UserProfileNameTextBox
            // 
            this.UserProfileNameTextBox.Location = new System.Drawing.Point(122, 41);
            this.UserProfileNameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.UserProfileNameTextBox.Name = "UserProfileNameTextBox";
            this.UserProfileNameTextBox.Size = new System.Drawing.Size(423, 20);
            this.UserProfileNameTextBox.TabIndex = 3;
            // 
            // PersonalAccessTokenLabel
            // 
            this.PersonalAccessTokenLabel.AutoSize = true;
            this.PersonalAccessTokenLabel.Location = new System.Drawing.Point(5, 67);
            this.PersonalAccessTokenLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.PersonalAccessTokenLabel.Name = "PersonalAccessTokenLabel";
            this.PersonalAccessTokenLabel.Size = new System.Drawing.Size(118, 13);
            this.PersonalAccessTokenLabel.TabIndex = 4;
            this.PersonalAccessTokenLabel.Text = "Personal access token:";
            // 
            // UserProfileNameLabel
            // 
            this.UserProfileNameLabel.AutoSize = true;
            this.UserProfileNameLabel.Location = new System.Drawing.Point(5, 44);
            this.UserProfileNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.UserProfileNameLabel.Name = "UserProfileNameLabel";
            this.UserProfileNameLabel.Size = new System.Drawing.Size(92, 13);
            this.UserProfileNameLabel.TabIndex = 2;
            this.UserProfileNameLabel.Text = "User profile name:";
            // 
            // OverrideCredentialsCheckBox
            // 
            this.OverrideCredentialsCheckBox.AutoSize = true;
            this.OverrideCredentialsCheckBox.Location = new System.Drawing.Point(8, 20);
            this.OverrideCredentialsCheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.OverrideCredentialsCheckBox.Name = "OverrideCredentialsCheckBox";
            this.OverrideCredentialsCheckBox.Size = new System.Drawing.Size(120, 17);
            this.OverrideCredentialsCheckBox.TabIndex = 1;
            this.OverrideCredentialsCheckBox.Text = "Override credentials";
            this.OverrideCredentialsCheckBox.UseVisualStyleBackColor = true;
            this.OverrideCredentialsCheckBox.CheckedChanged += new System.EventHandler(this.OverrideCredentialsCheckBox_CheckedChanged);
            // 
            // CredentialsLabel
            // 
            this.CredentialsLabel.AutoSize = true;
            this.CredentialsLabel.Location = new System.Drawing.Point(3, 3);
            this.CredentialsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.CredentialsLabel.Name = "CredentialsLabel";
            this.CredentialsLabel.Size = new System.Drawing.Size(59, 13);
            this.CredentialsLabel.TabIndex = 0;
            this.CredentialsLabel.Text = "Credentials";
            // 
            // HelpProvider
            // 
            this.HelpProvider.HelpNamespace = "Aclara.VsoBuild.Sidekicks.Help.chm";
            // 
            // VSTSProjectConnectForm
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 268);
            this.Controls.Add(this.CredentialsPanel);
            this.Controls.Add(this.AccountsAndProjectsPanel);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OKButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VSTSProjectConnectForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Connect to Visal Studio Team Services Project";
            this.TopMost = true;
            this.Shown += new System.EventHandler(this.VSTSProjectConnectForm_Shown);
            this.AccountsAndProjectsPanel.ResumeLayout(false);
            this.AccountsAndProjectsPanel.PerformLayout();
            this.CredentialsPanel.ResumeLayout(false);
            this.CredentialsPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.ComboBox AccountComboBox;
        private System.Windows.Forms.Label AccountLabel;
        private System.Windows.Forms.Panel AccountsAndProjectsPanel;
        private System.Windows.Forms.Label ProjectLabel;
        private System.Windows.Forms.Panel CredentialsPanel;
        private System.Windows.Forms.LinkLabel CreatePATLinkLabel;
        private System.Windows.Forms.TextBox PersonalAccessTokenTextBox;
        private System.Windows.Forms.TextBox UserProfileNameTextBox;
        private System.Windows.Forms.Label PersonalAccessTokenLabel;
        private System.Windows.Forms.Label UserProfileNameLabel;
        private System.Windows.Forms.CheckBox OverrideCredentialsCheckBox;
        private System.Windows.Forms.Label CredentialsLabel;
        private System.Windows.Forms.ComboBox TeamProjectComboBox;
        private System.Windows.Forms.Label VerificationStatusLabel;
        private System.Windows.Forms.Button ValidateCredentialsButton;
        private System.Windows.Forms.Label AccoutAndProjectsLabel;
        private System.Windows.Forms.HelpProvider HelpProvider;
        private System.Windows.Forms.Button HelpButton;
    }
}