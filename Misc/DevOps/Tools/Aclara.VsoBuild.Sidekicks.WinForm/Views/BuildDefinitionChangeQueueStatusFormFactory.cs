﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using System;
using System.Collections.Generic;
using System.Linq;



namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public class BuildDefinitionChangeQueueStatusFormFactory
    {

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <returns></returns>
        public static BuildDefinitionChangeQueueStatusForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                                      SidekickConfiguration sidekickConfiguration,
                                                                      BuildDefinitionSearchForm buildDefinitionSearchForm)
        {

            BuildDefinitionChangeQueueStatusForm result = null;
            BuildDefinitionChangeQueueStatusPresenter buildDefinitionChangeQueueStatusPresenter = null;

            try
            {
                result = new BuildDefinitionChangeQueueStatusForm(sidekickConfiguration, buildDefinitionSearchForm);
                buildDefinitionChangeQueueStatusPresenter = new BuildDefinitionChangeQueueStatusPresenter(teamProjectInfo, sidekickConfiguration, result);
                result.BuildDefinitionChangeQueueStatusPresenter = buildDefinitionChangeQueueStatusPresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

    }
}
