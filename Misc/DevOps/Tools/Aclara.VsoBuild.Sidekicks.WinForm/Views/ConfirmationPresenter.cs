﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public class ConfirmationPresenter
    {
        #region Private Constants

        #endregion

        #region Private Data Members

        private IConfirmationView _ConfirmationView;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Exception view.
        /// </summary>
        public IConfirmationView ConfirmationView
        {
            get { return _ConfirmationView; }
            set { _ConfirmationView = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        public ConfirmationPresenter(IConfirmationView ConfirmationView)
        {
            _ConfirmationView = ConfirmationView;
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {

            this.ConfirmationView.TypedResponseValue = string.Empty;

        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

    }
}
