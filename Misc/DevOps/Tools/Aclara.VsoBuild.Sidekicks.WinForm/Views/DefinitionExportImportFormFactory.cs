﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using System;
using System.Collections.Generic;
using System.Linq;



namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public class DefinitionExportImportFormFactory
    {

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <returns></returns>
        public static DefinitionExportImportForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                            SidekickConfiguration sidekickConfiguration,
                                                            BuildDefinitionSearchForm buildDefinitionSearchForm,
                                                            ReleaseDefinitionSearchForm releaseDefinitionSearchForm)
        {

            DefinitionExportImportForm result = null;
            DefinitionExportImportPresenter DefinitionExportImportPresenter = null;

            try
            {
                result = new DefinitionExportImportForm(sidekickConfiguration, 
                                                        buildDefinitionSearchForm, 
                                                        releaseDefinitionSearchForm);
                DefinitionExportImportPresenter = new DefinitionExportImportPresenter(teamProjectInfo, sidekickConfiguration, result);
                result.DefinitionExportImportPresenter = DefinitionExportImportPresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

    }
}
