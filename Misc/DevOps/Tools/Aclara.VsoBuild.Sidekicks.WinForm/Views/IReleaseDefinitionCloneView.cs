﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public interface IReleaseDefinitionCloneView
    {
        string SidekickName { get; }

        string SidekickDescription { get; }

        string BackgroundTaskStatus { get; set; }

        string ApplyButtonText { get; set; }

        void ApplyButtonEnable(bool enable);

        void TeamProjectChanged();

        void UpdateProgressCloneReleaseDefinition(int releaseDefinitionClonedCount, int releaseDefinitionTotalCount);

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);
    }
}
