﻿using Aclara.Vso.Build.Client.Types;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class BuildDefinitionCloneForm : Form, IBuildDefinitionCloneView, ISidekickView, ISidekickInfo
    {

        #region Private Constants

        private const string SidekickView_SidekickName = "CloneBuildDefinitions";
        private const string SidekickView_SidekickDescription = "Clone Build Definitions";

        private const string ValidationErrorMessage_TeamProjectNameNotSelected = "Team project not selected.";
        private const string ValidationErrorMessage_BranchNameSourceNotEntered = "Branch name source not entered.";
        private const string ValidationErrorMessage_BuildDefinitionNotSelected = "Build definition not selected.";
        private const string ValidationErrorMessage_SourceBranchNameNotEntered = "Branch name - source not entered.";
        private const string ValidationErrorMessage_SourceBuildDefinitionNamePrefixNotEntered = "Build definition name prefix - source not entered.";
        private const string ValidationErrorMessage_TargetBranchNameNotEntered = "Branch name - target not entered.";
        private const string ValidationErrorMessage_TargetBuildDefinitionNamePrefixNotEntered = "Build definition name prefix - target not entered.";
        private const string ValidationErrorMessage_SelectedBuildDefinitionsMustHaveSameNamePrefix = "Selected build definitons must have same name prefix.";
        private const string ValidationErrorMessage_TargetBuildDefinitionNamePrefixContainsInvalidCharacter = "Target build definition name prefix contains invalid character. ( ; , )";
        private const string ValidationErrorMessage_RepositoryTypeValidSelection = "Select Team Foundation Version Control or Git. No other repository types are supported.";

        private const string BuildDefinitionFolder_Name = "Name";

        #endregion

        #region Private Data Members

        private SidekickConfiguration _sidekickConfiguration = null;
        private BuildDefinitionSearchForm _buildDefinitionSearchForm = null;
        private BuildDefinitionClonePresenter _buildDefinitionClonePresenter;
        private GitRepositorySelectorList _gitRepositoryList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickView_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickView_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                if (this.BuildDefinitionClonePresenter.IsBackgroundTaskBusy == false)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick warning level.
        /// </summary>
        public SidekickContainerTypes.SidekickWarningLevel SidekickWarningLevel
        {
            get
            {
                return SidekickContainerTypes.SidekickWarningLevel.Low;
            }
        }

        /// <summary>
        /// Property: Build defintion clone presenter.
        /// </summary>
        public BuildDefinitionClonePresenter BuildDefinitionClonePresenter
        {
            get { return _buildDefinitionClonePresenter; }
            set { _buildDefinitionClonePresenter = value; }
        }

        /// <summary>
        /// Property: Git repository list.
        /// </summary>
        public GitRepositorySelectorList RepositoryNameList
        {
            get
            {
                if (_gitRepositoryList == null &&
                    this.BuildDefinitionClonePresenter != null)
                {
                    _gitRepositoryList = this.BuildDefinitionClonePresenter.GetGitRepositorySelectorList();
                }
                return _gitRepositoryList;
            }
            set { _gitRepositoryList = value; }
        }

        /// <summary>
        /// Property: Build definition search form.
        /// </summary>
        public BuildDefinitionSearchForm BuildDefinitionSearchForm
        {
            get { return _buildDefinitionSearchForm; }
            set { _buildDefinitionSearchForm = value; }
        }

        /// <summary>
        /// Property: Branch name source.
        /// </summary>
        public string SourceBranchName
        {
            get { return this.BranchNameSourceTextBox.Text; }
            set { this.BranchNameSourceTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Build definition name prefix - source.
        /// </summary>
        public string SourceBuildDefinitionNamePrefix
        {
            get { return this.BuildDefinitionNamePrefixSourceTextBox.Text; }
            set { this.BuildDefinitionNamePrefixSourceTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Branch name target.
        /// </summary>
        public string TargetBranchName
        {
            get { return this.BranchNameTargetTextBox.Text; }
            set { this.BranchNameTargetTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Build definition name prefix - target.
        /// </summary>
        public string TargetBuildDefinitionNamePrefix
        {
            get { return this.BuildDefinitionNamePrefixTargetTextBox.Text; }
            set { this.BuildDefinitionNamePrefixTargetTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Repository type.
        /// </summary>
        public RepositoryType RepositoryType
        {
            get
            {
                return (RepositoryType)RepositoryTypeComboBox.SelectedValue;
            }
            set
            {
                RepositoryTypeComboBox.SelectedValue = value;
            }
        }

        /// <summary>
        /// Property: Build definition folder.
        /// </summary>
        public BuildDefinitionFolder BuildDefinitionFolder
        {
            
            get
            {
                BuildDefinitionFolder buildDefinitionFolder = null;
                if(this.BuildDefinitionFolderComboBox.SelectedItem.GetType() == typeof(BuildDefinitionFolder))
                {
                    buildDefinitionFolder = (BuildDefinitionFolder)this.BuildDefinitionFolderComboBox.SelectedItem;
                }
                return buildDefinitionFolder;
            }
            set
            {
                this.BuildDefinitionFolderComboBox.SelectedValue = value;
            }
        }

        /// <summary>
        /// Property: Migrate to git.
        /// </summary>
        public bool MigrateToGit
        {
            get
            {
                return this.MigrateToGitCheckBox.Checked;
            }
            set
            {
                this.MigrateToGitCheckBox.Checked = value;
            }
        }

        /// <summary>
        /// Property: Do not clone build definitions.
        /// </summary>
        public bool DoNotCloneBuildDefinitions
        {
            get { return this.DoNotCloneBuildDefinitionsCheckBox.Checked; }
            set { this.DoNotCloneBuildDefinitionsCheckBox.Checked = value; }
        }

        /// <summary>
        /// Property: Clone release definitions.
        /// </summary>
        public bool CloneReleaseDefinitions
        {
            get { return this.CloneReleaseDefinitionsCheckBox.Checked; }
            set { this.CloneReleaseDefinitionsCheckBox.Checked = value; }
        }

        /// <summary>
        /// Property: Create missing release definitions.
        /// </summary>
        public bool CreateMissingReleaseDefinitions
        {
            get { return this.CreateMissingReleaseDefinitionsCheckBox.Checked; }
            set { this.CreateMissingReleaseDefinitionsCheckBox.Checked = value; }
        }

        /// <summary>
        /// Property: Migrate build steps.
        /// </summary>
        public bool MigrateBuildSteps
        {
            get { return this.MigrateBuildStepsCheckBox.Checked; }
            set { this.MigrateBuildStepsCheckBox.Checked = value; }
        }


        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get { return this.ApplyButton.Text; }
            set { this.ApplyButton.Text = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildDefinitionCloneForm()
        {
            InitializeComponent();
            InitializeControls();

        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        public BuildDefinitionCloneForm(SidekickConfiguration sidekickConfiguration,
                                        BuildDefinitionSearchForm buildDefinitionSearchForm)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.BuildDefinitionSearchForm = buildDefinitionSearchForm;

            InitializeComponent();
            InitializeControls();

            this.BuildDefinitionSearchForm.BuildDefinitionSelectionChanged += OnBuildDefinitionSelectionChanged;
            this.BuildDefinitionSearchPanel.DockControl(this.BuildDefinitionSearchForm);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update progress clone build definition.
        /// </summary>
        /// <param name="buildDefinitionCloneCount"></param>
        /// <param name="buildDefinitionTotalCount"></param>
        public void UpdateProgressCloneBuildDefinition(int buildDefinitionCloneCount, int buildDefinitionTotalCount)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<int, int>(this.UpdateProgressCloneBuildDefinition), buildDefinitionCloneCount, buildDefinitionTotalCount);
            }
            else
            {
                if (buildDefinitionCloneCount < buildDefinitionTotalCount)
                {
                    this.CloneBuildDefinitionProgressBar.Visible = true;
                    this.CloneBuildDefinitionProgressBar.Minimum = 0;
                    this.CloneBuildDefinitionProgressBar.Maximum = buildDefinitionTotalCount;
                    this.CloneBuildDefinitionProgressBar.Value = buildDefinitionCloneCount;
                }
                else if (buildDefinitionCloneCount == buildDefinitionTotalCount)
                {
                    this.CloneBuildDefinitionProgressBar.Visible = false;
                }
            }
        }

        /// <summary>
        /// Retrieve remaining build definitions.
        /// </summary>
        public void RetrieveRemainingDefinitions()
        {
            this.BuildDefinitionSearchForm.RetrieveRemainingDefinitions();
        }

        /// <summary>
        /// Apply button enable.
        /// </summary>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            this.BuildDefinitionSearchForm.TeamProjectChanged();
            this.BuildDefinitionClonePresenter.TeamProjectChanged();
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task Completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            BindingSource buildDefinitionFolderBindingSource = null;
            BuildDefinitionFolder buildDefinitionFolder = null;
            try
            {
                buildDefinitionFolderBindingSource = new BindingSource();

                EnableControlsDependantOnSelectedBuildDefinition(false);

                this.RepositoryTypeComboBox.FormattingEnabled = true;
                this.RepositoryTypeComboBox.Format += delegate (object sender, ListControlConvertEventArgs e)
                {
                    e.Value = ConvertRepositoryTypeToText((RepositoryType)e.Value);
                };
                this.RepositoryTypeComboBox.DataSource = Enum.GetValues(typeof(RepositoryType));

                this.RepositoryTypeComboBox.SelectedItem = RepositoryType.SourceBuildDefinitionRepositoryType;

                buildDefinitionFolderBindingSource.DataSource = this.SidekickConfiguration.BuildDefinitionFolders.BuildDefinitionFolder;
                this.BuildDefinitionFolderComboBox.DataSource = buildDefinitionFolderBindingSource;
                this.BuildDefinitionFolderComboBox.DisplayMember = BuildDefinitionFolder_Name;
                this.BuildDefinitionFolderComboBox.ValueMember = BuildDefinitionFolder_Name;

                buildDefinitionFolder  = this.SidekickConfiguration.BuildDefinitionFolders.BuildDefinitionFolder.Single(bdf => bdf.IsDefault == true);
                if (buildDefinitionFolder != null)
                {
                    this.BuildDefinitionFolderComboBox.SelectedValue = buildDefinitionFolder.Name;
                }

                this.CloneBuildDefinitionProgressBar.Visible = false;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Hanlder: Build definition selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="requestBuildEventArgs"></param>
        protected void OnBuildDefinitionSelectionChanged(Object sender,
                                                         BuildDefinitionSelectionChangedEventArgs buildDefinitionSelectionChangedEventArgs)
        {

            BuildDefinitionSelectorList buildDefinitionSelectorList = null;

            try
            {
                if (buildDefinitionSelectionChangedEventArgs != null)
                {
                    if (buildDefinitionSelectionChangedEventArgs.OneOrMoreSelected == true)
                    {
                        buildDefinitionSelectorList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();
                        if (buildDefinitionSelectorList.Count > 0)
                        {
                            this.BranchNameSourceTextBox.Text = buildDefinitionSelectorList[0].GetYearMonthBranchName();
                            this.BuildDefinitionNamePrefixSourceTextBox.Text = buildDefinitionSelectorList[0].GetNamePrefix();
                            EnableControlsDependantOnSelectedBuildDefinition(true);

                        }

                    }
                    else
                    {
                        this.BranchNameSourceTextBox.Text = string.Empty;
                        this.BuildDefinitionNamePrefixSourceTextBox.Text = string.Empty;
                        EnableControlsDependantOnSelectedBuildDefinition(false);
                    }

                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Enable/disable controls dependant on at lease one build definition selected.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnSelectedBuildDefinition(bool enable)
        {
            try
            {
                this.BranchNameSourceTextBox.Enabled = enable;
                this.BuildDefinitionNamePrefixSourceTextBox.Enabled = enable;
                this.BranchNameTargetTextBox.Enabled = enable;
                this.BuildDefinitionNamePrefixTargetTextBox.Enabled = enable;
                this.ResetButton.Enabled = enable;
                this.RepositoryTypeComboBox.Enabled = enable;
                this.BuildDefinitionFolderComboBox.Enabled = enable;
                this.DoNotCloneBuildDefinitionsCheckBox.Enabled = enable;
                this.CloneReleaseDefinitionsCheckBox.Enabled = enable;
                this.SourceReleaseDefinitionDoesNotExistLabel.Enabled = enable;
                if (enable == true)
                {
                    this.EnableControlsDependantOnCloneReleaseDefinitions(this.CloneReleaseDefinitionsCheckBox.Checked);
                }
                else
                {
                    this.CreateMissingReleaseDefinitionsCheckBox.Enabled = false;
                    this.MigrateBuildStepsCheckBox.Enabled = false;
                }

                this.ApplyButton.Enabled = enable;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Enable/disable controls dependant on clone release definitions.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnCloneReleaseDefinitions(bool enable)
        {
            try
            {
                this.CreateMissingReleaseDefinitionsCheckBox.Enabled = enable;
                this.EnableControlsDependantOnCreateMissingReleaseDefinitions(CreateMissingReleaseDefinitionsCheckBox.Checked);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable/disable controls dependant on clone release definitions.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnCreateMissingReleaseDefinitions(bool enable)
        {
            try
            {
                this.MigrateBuildStepsCheckBox.Enabled = enable;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        // Convert repository type enum value to text.
        /// </summary>
        /// <param name="dateFilter"></param>
        /// <returns></returns>
        protected string ConvertRepositoryTypeToText(RepositoryType repositoryType)
        {
            string result = string.Empty;

            try
            {
                FieldInfo field = repositoryType.GetType().GetField(repositoryType.ToString());
                object[] attribs = field.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attribs.Length > 0)
                {
                    result = ((DescriptionAttribute)attribs[0]).Description;
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Populate Git repository selection.
        /// </summary>
        protected void PopulateGitRepositorySelection()
        {

            try
            {
                this.RepositoryComboBox.DataSource = this.RepositoryNameList;
                this.RepositoryComboBox.DisplayMember = "Name";
                this.RepositoryComboBox.ValueMember = "Id";
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelectorList buildDefinitionSelectorList = null;
            List<string> buildDefinitionNamePrefixList = null;
            RepositoryType repositoryType = this.RepositoryType;
            BuildDefinitionCloneProperties buildDefinitionCloneProperties = null;
            GitRepositorySelector gitRepositorySelector = null;

            try
            {
                buildDefinitionCloneProperties = new BuildDefinitionCloneProperties();

                if (this.BuildDefinitionClonePresenter.TeamProjectInfo.TeamProjectCollectionUri == null ||
                    this.BuildDefinitionClonePresenter.TeamProjectInfo.TeamProjectName == string.Empty)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_TeamProjectNameNotSelected));
                    return;
                }

                if (this.SourceBranchName == string.Empty)
                {
                    MessageBox.Show(this, string.Format(ValidationErrorMessage_SourceBranchNameNotEntered));
                    return;
                }

                if (this.SourceBuildDefinitionNamePrefix == string.Empty)
                {
                    MessageBox.Show(this, string.Format(ValidationErrorMessage_SourceBuildDefinitionNamePrefixNotEntered));
                    return;
                }

                if (this.TargetBranchName == string.Empty)
                {
                    MessageBox.Show(this, string.Format(ValidationErrorMessage_TargetBranchNameNotEntered));
                    return;
                }

                if (this.TargetBuildDefinitionNamePrefix == string.Empty)
                {
                    MessageBox.Show(this, string.Format(ValidationErrorMessage_TargetBuildDefinitionNamePrefixNotEntered));
                    return;
                }

                if (this.TargetBuildDefinitionNamePrefix.Contains(",") == true ||
                    this.TargetBuildDefinitionNamePrefix.Contains(";") == true)
                {
                    MessageBox.Show(this, string.Format(ValidationErrorMessage_TargetBuildDefinitionNamePrefixContainsInvalidCharacter));
                    return;
                }
                buildDefinitionCloneProperties.MigrateToGit = this.MigrateToGit;
                buildDefinitionCloneProperties.DoNotCloneBuildDefinitions = this.DoNotCloneBuildDefinitions;
                buildDefinitionCloneProperties.CloneReleaseDefinitions = this.CloneReleaseDefinitions;
                buildDefinitionCloneProperties.CreateMissingReleaseDefinitions = this.CreateMissingReleaseDefinitions;
                buildDefinitionCloneProperties.MigrateBuildSteps = this.MigrateBuildSteps;

                buildDefinitionCloneProperties.RepositoryType = this.RepositoryType;

                if (repositoryType != RepositoryType.SourceBuildDefinitionRepositoryType &&
                    repositoryType != RepositoryType.TeamFoundationVersionControl &&
                    repositoryType != RepositoryType.Git)
                {
                    MessageBox.Show(this, string.Format(ValidationErrorMessage_RepositoryTypeValidSelection));
                    return;
                }

                buildDefinitionCloneProperties.RepositoryType = RepositoryType;

                if (repositoryType == RepositoryType.Git)
                {
                    gitRepositorySelector = (GitRepositorySelector)this.RepositoryComboBox.SelectedItem;

                    if (gitRepositorySelector != null)
                    {
                        buildDefinitionCloneProperties.GitRepository = gitRepositorySelector.GitRepository;
                    }
                }

                if (this.BuildDefinitionFolder.IsNotSpecified == true)
                {
                    buildDefinitionCloneProperties.BuildDefinitionFolderName = string.Empty;
                }
                else
                {
                    buildDefinitionCloneProperties.BuildDefinitionFolderName = this.BuildDefinitionFolder.Name;
                }

                buildDefinitionSelectorList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();
                if (buildDefinitionSelectorList == null ||
                    buildDefinitionSelectorList.Count <= 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_BuildDefinitionNotSelected));
                    return;
                }

                buildDefinitionNamePrefixList = buildDefinitionSelectorList.GetBuildDefinitionNamePrefixList();
                if (buildDefinitionNamePrefixList != null)
                {
                    if (buildDefinitionNamePrefixList.Count > 1)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_SelectedBuildDefinitionsMustHaveSameNamePrefix));
                        return;
                    }
                }

                this.CloneBuildDefinitionProgressBar.Visible = false;

                this.BuildDefinitionClonePresenter.CloneBuildDefinitions(this.SourceBuildDefinitionNamePrefix,
                                                                         this.TargetBuildDefinitionNamePrefix,
                                                                         this.SourceBranchName,
                                                                         this.TargetBranchName,
                                                                         buildDefinitionCloneProperties,
                                                                         buildDefinitionSelectorList);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Branch name target textbox - leave.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BranchNameTargetTextBox_Leave(object sender, EventArgs e)
        {
            BuildDefinitionSelectorList buildDefinitionSelectorList = null;
            string buildDefinitionNamePrefixTarget = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(this.BuildDefinitionNamePrefixTargetTextBox.Text) == false)
                {
                    return;
                }

                buildDefinitionSelectorList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();
                if (buildDefinitionSelectorList.Count > 0)
                {
                    buildDefinitionNamePrefixTarget = string.Format("{0}_{1}_",
                                                                    buildDefinitionSelectorList[0].GetNamePrefixWithoutYearMonthBranch(),
                                                                    this.BranchNameTargetTextBox.Text);
                    this.BuildDefinitionNamePrefixTargetTextBox.Text = buildDefinitionNamePrefixTarget;
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();

                throw;
            }
        }

        /// <summary>
        /// Event Handler: Reset button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetButton_Click(object sender, EventArgs e)
        {
            try
            {
                BuildDefinitionNamePrefixTargetTextBox.Text = string.Empty;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();

                throw;
            }
        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickCloneBuildDefinitions.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Clone release definitions checkBox - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloneReleaseDefinitionsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.EnableControlsDependantOnCloneReleaseDefinitions(this.CloneReleaseDefinitionsCheckBox.Checked);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Create missing release definitions checkBox - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateMissingReleaseDefinitionsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.EnableControlsDependantOnCreateMissingReleaseDefinitions(this.CreateMissingReleaseDefinitionsCheckBox.Checked);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Repository type combo box - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RepositoryTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            RepositoryType repositoryType = RepositoryType.SourceBuildDefinitionRepositoryType;
            try
            {
                if (sender is ComboBox)
                {
                    repositoryType = (RepositoryType)this.RepositoryTypeComboBox.SelectedItem;
                    switch (repositoryType)
                    {
                        case RepositoryType.SourceBuildDefinitionRepositoryType:
                            this.RepositoryComboBox.Enabled = false;
                            this.MigrateToGitCheckBox.Enabled = false;
                            this.MigrateToGitCheckBox.Checked = false;
                            break;
                        case RepositoryType.Git:
                            this.RepositoryComboBox.Enabled = true;
                            this.MigrateToGitCheckBox.Enabled = true;
                            this.PopulateGitRepositorySelection();
                            break;
                        case RepositoryType.TeamFoundationVersionControl:
                            this.RepositoryComboBox.Enabled = false;
                            this.MigrateToGitCheckBox.Enabled = false;
                            this.MigrateToGitCheckBox.Checked = false;
                            break;
                        case RepositoryType.GitHub:
                            this.RepositoryComboBox.Enabled = false;
                            this.MigrateToGitCheckBox.Enabled = false;
                            this.MigrateToGitCheckBox.Checked = false;
                            break;
                        case RepositoryType.ExternalGit:
                            this.RepositoryComboBox.Enabled = false;
                            this.MigrateToGitCheckBox.Enabled = false;
                            this.MigrateToGitCheckBox.Checked = false;
                            break;
                        case RepositoryType.Subversion:
                            this.RepositoryComboBox.Enabled = false;
                            this.MigrateToGitCheckBox.Enabled = false;
                            this.MigrateToGitCheckBox.Checked = false;
                            break;
                        default:
                            this.RepositoryComboBox.Enabled = false;
                            this.MigrateToGitCheckBox.Enabled = false;
                            this.MigrateToGitCheckBox.Checked = false;
                            break;
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }
        #endregion


    }
}
