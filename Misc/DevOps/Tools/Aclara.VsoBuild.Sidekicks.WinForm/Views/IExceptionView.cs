﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Exception view - interface.
    /// </summary>
    public interface IExceptionView
    {
        string ExceptionMessage { get; set; }
        string ExceptionText { get; set; }
    }
}
