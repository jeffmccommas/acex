﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public class BuildDefinitionChangeSettingsFormFactory
    {

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <returns></returns>
        public static BuildDefinitionChangeSettingsForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                                      SidekickConfiguration sidekickConfiguration,
                                                                      BuildDefinitionSearchForm buildDefinitionSearchForm)
        {

            BuildDefinitionChangeSettingsForm result = null;
            BuildDefinitionChangeSettingsPresenter BuildDefinitionChangeSettingsPresenter = null;

            try
            {
                result = new BuildDefinitionChangeSettingsForm(sidekickConfiguration, buildDefinitionSearchForm);
                BuildDefinitionChangeSettingsPresenter = new BuildDefinitionChangeSettingsPresenter(teamProjectInfo, sidekickConfiguration, result);
                result.BuildDefinitionChangeSettingsPresenter = BuildDefinitionChangeSettingsPresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

    }
}
