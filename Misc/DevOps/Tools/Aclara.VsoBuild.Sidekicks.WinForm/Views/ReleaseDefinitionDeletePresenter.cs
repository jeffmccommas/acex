﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.Vso.Build.Client;
using Aclara.Vso.Build.Client.Events;
using Aclara.Vso.Build.Client.Models;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Release definition delete - presenter.
    /// </summary>
    public class ReleaseDefinitionDeletePresenter
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private CustomLogger _logger = null;
        private SidekickConfiguration _sidekickConfiguration = null;
        private IReleaseDefinitionDeleteView _releaseDefinitionDeleteView;
        private ReleaseDefinitionManager _releaseDefinitionManager;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;
        private ITeamProjectInfo _teamProjectInfo = null;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.ReleaseDefinitionDeleteView.SidekickName,
                                               this.ReleaseDefinitionDeleteView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Release definiton delete view.
        /// </summary>
        public IReleaseDefinitionDeleteView ReleaseDefinitionDeleteView
        {
            get { return _releaseDefinitionDeleteView; }
            set { _releaseDefinitionDeleteView = value; }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Release definition manager.
        /// </summary>
        public ReleaseDefinitionManager ReleaseDefinitionManager
        {
            get { return _releaseDefinitionManager; }
            set { _releaseDefinitionManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamPrjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="releaseDefinitionDeleteView"></param>
        public ReleaseDefinitionDeletePresenter(ITeamProjectInfo teamPrjectInfo,
                                             SidekickConfiguration sidekickConfiguration,
                                             IReleaseDefinitionDeleteView releaseDefinitionDeleteView)
        {
            this.TeamProjectInfo = teamPrjectInfo;
            this.SidekickConfiguration = sidekickConfiguration;
            this.ReleaseDefinitionDeleteView = releaseDefinitionDeleteView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private ReleaseDefinitionDeletePresenter()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalReleaseDuration;
            AggregateException aggregateException = null;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalReleaseDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.ReleaseDefinitionDeleteView.BackgroundTaskCompleted("Delete release definition request canceled.");
                        this.ReleaseDefinitionDeleteView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format("Delete release definition request cancelled."));

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format("[*] Delete release definition request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                         totalReleaseDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.ReleaseDefinitionDeleteView.BackgroundTaskCompleted("");
                        this.ReleaseDefinitionDeleteView.ApplyButtonEnable(true);


                        //Log background task completed log entry.

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format("Operation cancelled."));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format("Operation cancelled."));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format("Request failed."));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format("Request failed."));
                                    }

                                }
                            }
                        }

                        this.Logger.Info(string.Format("[*] Delete release definition request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                         totalReleaseDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.ReleaseDefinitionDeleteView.BackgroundTaskCompleted("Delete release definition request completed.");
                        this.ReleaseDefinitionDeleteView.ApplyButtonEnable(true);

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format("[*] Delete release definition request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                         totalReleaseDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Delete release definitions.
        /// </summary>
        /// <param name="releaseDefinitionSelectorList"></param>
        public void DeleteReleaseDefinitions(ReleaseDefinitionSelectorList releaseDefinitionSelectorList)
        {

            List<int> filteredBuidDefinitionIdList = null;
            ReleaseDefinitionList filteredReleaseDefinitionList = null;
            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.ReleaseDefinitionDeleteView.BackgroundTaskStatus = "Cancelling delete release definition request...";
                    this.ReleaseDefinitionDeleteView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.ReleaseDefinitionDeleteView.BackgroundTaskStatus = "Delete release definition(s) requested...";
                    this.ReleaseDefinitionDeleteView.ApplyButtonText = "Cancel";

                    this.ReleaseDefinitionManager = this.CreateReleaseDefinitionManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    //Reterieve fresh release definition list. (In case release definitions have changed since last retrieval).
                    filteredBuidDefinitionIdList = releaseDefinitionSelectorList.GetReleaseDefinitionIdList();
                    filteredReleaseDefinitionList = this.ReleaseDefinitionManager.GetDefinitionsRestApi(filteredBuidDefinitionIdList);

                    this.ReleaseDefinitionManager.ReleaseDefinitionDeleted += OnReleaseDefinitionDeleted;

                    this.BackgroundTask = new Task(() => this.ReleaseDefinitionManager.DeleteReleaseDefinitions(filteredReleaseDefinitionList,
                                                                                                            cancellationToken),
                                                                                                            cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create release definition manager.
        /// </summary>
        /// <returns></returns>
        protected ReleaseDefinitionManager CreateReleaseDefinitionManager()
        {
            ReleaseDefinitionManager result = null;

            try
            {
                result = ReleaseDefinitionManagerFactory.CreateReleaseDefinitionManager(this.TeamProjectInfo.TeamProjectCollectionUri,
                                                                                    this.TeamProjectInfo.TeamProjectCollectionVsrmUri, 
                                                                                    this.TeamProjectInfo.TeamProjectName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIUserProfileName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIPassword);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: Release definition deleted.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="deleteReleaseDefinitionEventArgs"></param>
        protected void OnReleaseDefinitionDeleted(Object sender,
                                               DeleteReleaseDefinitionEventArgs deleteReleaseDefinitionEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Release definition details unavailable and status list details available.
                if (deleteReleaseDefinitionEventArgs.ReleaseDefinition == null &&
                    deleteReleaseDefinitionEventArgs.StatusList != null &&
                    deleteReleaseDefinitionEventArgs.StatusList.Count > 0)
                {
                    message = string.Format("Release definition delete may have been canceled. (Status --> {0})",
                                            deleteReleaseDefinitionEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    if (deleteReleaseDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error) == true)
                    {
                        this.Logger.Error(message);

                    }
                    else if (deleteReleaseDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Warning) == true)
                    {
                        this.Logger.Warn(message);
                    }
                    else
                    {
                        this.Logger.Info(message);
                    }

                }
                //Release definition details available and status list details available.
                if (deleteReleaseDefinitionEventArgs.ReleaseDefinition != null &&
                    deleteReleaseDefinitionEventArgs.StatusList != null &&
                    deleteReleaseDefinitionEventArgs.StatusList.Count > 0)
                {
                    message = string.Format("Release definition delete may have been canceled. (Release definition name: {0}; Status --> {1})",
                                            deleteReleaseDefinitionEventArgs.ReleaseDefinition.Name,
                                            deleteReleaseDefinitionEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    if (deleteReleaseDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error) == true)
                    {
                        this.Logger.Error(message);

                    }
                    else if (deleteReleaseDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Warning) == true)
                    {
                        this.Logger.Warn(message);
                    }
                    else
                    {
                        this.Logger.Info(message);
                    }

                }
                //Release definition details available and status list is unavailable.
                else if (deleteReleaseDefinitionEventArgs.ReleaseDefinition != null)
                {
                    this.Logger.Info(string.Format("Release definition deleted. (Release definition name: {0})",
                                                     deleteReleaseDefinitionEventArgs.ReleaseDefinition.Name));
                }
                //Release definiton details unavailable.
                else
                {
                    this.Logger.Info(string.Format("Release definition deleted. Details unavailable."));
                }

                this.ReleaseDefinitionDeleteView.UpdateProgressDeleteReleaseDefinition(deleteReleaseDefinitionEventArgs.ReleaseDefinitionDeletedCount, 
                                                                                       deleteReleaseDefinitionEventArgs.ReleaseDefinitionTotalCount);
            }
            catch (Exception)
            {
                throw;
            }


        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                        }
                        else
                        {
                            Logger.Error(string.Format("Operation cancelled.(Unobserved)"));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                }
                else
                {
                    Logger.Error(string.Format("Operation cancelled.(Unobserved)"));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
