﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class ReleaseDefinitionDeleteForm : Form, IReleaseDefinitionDeleteView, ISidekickView, ISidekickInfo
    {

        #region Private Constants
        private const string SidekickView_SidekickName = "DeleteReleaseDefinitions";
        private const string SidekickView_SidekickDescription = "Delete Release Definitions";

        private const string ValidationErrorMessage_TeamProjectNameNotSelected = "Team project not selected.";
        private const string ValidationErrorMessage_BranchNameSourceNotEntered = "Branch name source not entered.";
        private const string ValidationErrorMessage_ReleaseDefinitionNotSelected = "Release definition not selected.";
        private const string ValidationErrorMessage_SourceBranchNameNotEntered = "Branch name - source not entered.";
        private const string ValidationErrorMessage_SourceReleaseDefinitionNamePrefixNotEntered = "Release definition name prefix - source not entered.";
        private const string ValidationErrorMessage_TargetBranchNameNotEntered = "Branch name - target not entered.";
        private const string ValidationErrorMessage_TargetReleaseDefinitionNamePrefixNotEntered = "Release definition name prefix - target not entered.";
        private const string ValidationErrorMessage_SelectedReleaseDefinitionsMustHaveSameNamePrefix = "Selected build definitons must have same name prefix.";

        private const string DeleteReleaseDefinitionConfirmation_PendingAction = "Are you sure you want to to delete selected build definitions?";
        private const string DeleteReleaseDefinitionConfirmation_ConfirmationMessage = @"Type ""YES"" to confirm build definition deletion.";
        private const string DeleteReleaseDefinitionConfirmation_ExpectedTypeResponse = "YES";

        private const string ReleaseDefinitionDeletionStatus_Suffix = "{0} build definition(s) selected for deletion:";

        #endregion

        #region Private Data Members

        private SidekickConfiguration _sidekickConfiguration = null;
        private ReleaseDefinitionSearchForm _buildDefinitionSearchForm = null;
        private ReleaseDefinitionDeletePresenter _buildDefinitionClonePresenter;
        private ReleaseDefinitionSelectorList _buildDefinitionSelectorList;
        private BindingSource _buildDefinitionSelectorListBindingSource;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickView_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickView_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                if (this.ReleaseDefinitionDeletePresenter.IsBackgroundTaskBusy == false)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick warning level.
        /// </summary>
        public SidekickContainerTypes.SidekickWarningLevel SidekickWarningLevel
        {
            get
            {
                return SidekickContainerTypes.SidekickWarningLevel.Low;
            }
        }

        /// <summary>
        /// Property: Release defintion delete presenter.
        /// </summary>
        public ReleaseDefinitionDeletePresenter ReleaseDefinitionDeletePresenter
        {
            get { return _buildDefinitionClonePresenter; }
            set { _buildDefinitionClonePresenter = value; }
        }

        /// <summary>
        /// Property: Release definition search form.
        /// </summary>
        public ReleaseDefinitionSearchForm ReleaseDefinitionSearchForm
        {
            get { return _buildDefinitionSearchForm; }
            set { _buildDefinitionSearchForm = value; }
        }

        /// <summary>
        /// Property: Release definition deletion status.
        /// </summary>
        public string ReleaseDefinitionDeletionStatus
        {
            get { return this.ReleaseDefinitionListLabel.Text; }
            set { this.ReleaseDefinitionListLabel.Text = value; }
        }

        /// <summary>
        /// Property: Release definition selector list.
        /// </summary>
        public ReleaseDefinitionSelectorList ReleaseDefinitionSelectorList
        {
            get { return _buildDefinitionSelectorList; }
            set { _buildDefinitionSelectorList = value; }
        }

        /// <summary>
        /// Property: Release build definition selector list binding source.
        /// </summary>
        public BindingSource ReleaseDefinitionSelectorListBindingSource
        {
            get { return _buildDefinitionSelectorListBindingSource; }
            set { _buildDefinitionSelectorListBindingSource = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get { return this.ApplyButton.Text; }
            set { this.ApplyButton.Text = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ReleaseDefinitionDeleteForm()
        {
            InitializeComponent();
            InitializeControls();

        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        public ReleaseDefinitionDeleteForm(SidekickConfiguration sidekickConfiguration,
                                        ReleaseDefinitionSearchForm buildDefinitionSearchForm)
        {
            InitializeComponent();
            InitializeControls();
            this.SidekickConfiguration = sidekickConfiguration;
            this.ReleaseDefinitionSearchForm = buildDefinitionSearchForm;
            this.ReleaseDefinitionSearchForm.ReleaseDefinitionSelectionChanged += OnReleaseDefinitionSelectionChanged;

            _buildDefinitionSelectorList = new ReleaseDefinitionSelectorList();
            _buildDefinitionSelectorListBindingSource = new BindingSource();
            _buildDefinitionSelectorListBindingSource.DataSource = _buildDefinitionSelectorList;
            this.ReleaseDefinitionSelectorListBox.DisplayMember = "Name";
            this.ReleaseDefinitionSelectorListBox.DataSource = _buildDefinitionSelectorListBindingSource;

            this.ReleaseDefinitionSearchPanel.DockControl(this.ReleaseDefinitionSearchForm);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update progress delete release definition.
        /// </summary>
        /// <param name="releaseDefinitionDeletedCount"></param>
        /// <param name="releaseDefinitionTotalCount"></param>
        public void UpdateProgressDeleteReleaseDefinition(int releaseDefinitionDeletedCount, int releaseDefinitionTotalCount)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<int, int>(this.UpdateProgressDeleteReleaseDefinition), releaseDefinitionDeletedCount, releaseDefinitionTotalCount);
            }
            else
            {
                if (releaseDefinitionDeletedCount < releaseDefinitionTotalCount)
                {
                    this.DeleteReleaseBuildDefinitionProgressBar.Visible = true;
                    this.DeleteReleaseBuildDefinitionProgressBar.Minimum = 0;
                    this.DeleteReleaseBuildDefinitionProgressBar.Maximum = releaseDefinitionTotalCount;
                    this.DeleteReleaseBuildDefinitionProgressBar.Value = releaseDefinitionDeletedCount;
                }
                else if (releaseDefinitionDeletedCount == releaseDefinitionTotalCount)
                {
                    this.DeleteReleaseBuildDefinitionProgressBar.Visible = false;
                }
            }
        }
        /// <summary>
        /// Retrieve remaining build definitions.
        /// </summary>
        public void RetrieveRemainingDefinitions()
        {
            this.ReleaseDefinitionSearchForm.RetrieveRemainingDefinitions();
        }

        /// <summary>
        /// Apply button enable.
        /// </summary>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            this.ReleaseDefinitionSearchForm.TeamProjectChanged();
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task Completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {
                EnableControlsDependantOnSelectedReleaseDefinition(false);
                UpdateReleaseDefinitionDeletionStatus();
                this.DeleteReleaseBuildDefinitionProgressBar.Visible = false;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Update build definition deletion status.
        /// </summary>
        protected void UpdateReleaseDefinitionDeletionStatus()
        {
            try
            {
                ReleaseDefinitionDeletionStatus = string.Format(ReleaseDefinitionDeletionStatus_Suffix,
                                                              this.ReleaseDefinitionSelectorListBox.Items.Count);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Hanlder: Release definition selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="requestReleaseEventArgs"></param>
        protected void OnReleaseDefinitionSelectionChanged(Object sender,
                                                         ReleaseDefinitionSelectionChangedEventArgs buildDefinitionSelectionChangedEventArgs)
        {

            ReleaseDefinitionSelectorList buildDefinitionSelectorList = null;

            try
            {
                this.ReleaseDefinitionSelectorList.Clear();

                if (buildDefinitionSelectionChangedEventArgs != null)
                {

                    if (buildDefinitionSelectionChangedEventArgs.OneOrMoreSelected == true)
                    {
                        buildDefinitionSelectorList = this.ReleaseDefinitionSearchForm.GetSelectedReleaseDefinitionSelectorList();

                        foreach (ReleaseDefinitionSelector buildDefinitionSelector in buildDefinitionSelectorList)
                        {
                            this.ReleaseDefinitionSelectorList.Add(buildDefinitionSelector);
                        }

                        if (buildDefinitionSelectorList.Count > 0)
                        {
                            this.EnableControlsDependantOnSelectedReleaseDefinition(true);
                        }

                    }
                    else
                    {
                        this.EnableControlsDependantOnSelectedReleaseDefinition(false);
                    }

                }
                this.ReleaseDefinitionSelectorListBindingSource.ResetBindings(false);

                UpdateReleaseDefinitionDeletionStatus();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Enable/disable controls dependant on at lease one build definition selected.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnSelectedReleaseDefinition(bool enable)
        {
            try
            {

                this.ApplyButton.Enabled = enable;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            ReleaseDefinitionSelectorList buildDefinitionSelectorList = null;
            DialogResult dialogResult;
            ConfirmationForm confirmationForm = null;

            try
            {

                if (this.ReleaseDefinitionDeletePresenter.TeamProjectInfo.TeamProjectCollectionUri == null ||
                    this.ReleaseDefinitionDeletePresenter.TeamProjectInfo.TeamProjectName == string.Empty)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_TeamProjectNameNotSelected));
                    return;
                }

                buildDefinitionSelectorList = this.ReleaseDefinitionSearchForm.GetSelectedReleaseDefinitionSelectorList();
                if (buildDefinitionSelectorList == null ||
                    buildDefinitionSelectorList.Count <= 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_ReleaseDefinitionNotSelected));
                    return;
                }

                //Request confirmation.
                confirmationForm = ConfirmationFormFactory.CreateForm(DeleteReleaseDefinitionConfirmation_PendingAction,
                                                                      DeleteReleaseDefinitionConfirmation_ConfirmationMessage,
                                                                      DeleteReleaseDefinitionConfirmation_ExpectedTypeResponse,
                                                                      true,
                                                                      false,
                                                                      string.Empty);
                confirmationForm.StartPosition = FormStartPosition.CenterParent;
                dialogResult = confirmationForm.ShowDialog(this);
                if (dialogResult == DialogResult.OK)
                {
                    this.DeleteReleaseBuildDefinitionProgressBar.Visible = false;
                    this.ReleaseDefinitionDeletePresenter.DeleteReleaseDefinitions(buildDefinitionSelectorList);
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickDeleteReleaseDefinitions.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion
    }
}
