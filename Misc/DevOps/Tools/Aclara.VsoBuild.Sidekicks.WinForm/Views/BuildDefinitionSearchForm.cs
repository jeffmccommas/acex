﻿using Aclara.Vso.Build.Client.Models;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class BuildDefinitionSearchForm : Form, IBuildDefinitionSearchView
    {

        #region Private Constants

        private const string BuildDefinitionDataGridViewColumn_Check = "CheckColumn";
        private const string BuildDefinitionDataGridViewColumn_Name = "NameColumn";
        private const string BuildDefinitionDataGridViewColumn_Id = "IdColumn";
        private const string BuildDefinitionDataGridViewColumn_Enabled = "EnabledColumn";
        private const string BuildDefinitionDataGridViewColumn_Trigger = "TriggerColumn";
        private const string BuildDefinitionDataGridViewColumn_QueueName = "QueueNameColumn";
        private const string BuildDefinitionDataGridViewColumn_Extra = "ExtraColumn";

        private const string BuildDefinitionSelectorProperty_Selected = "Selected";
        private const string BuildDefinitionSelectorProperty_Enabled = "Enabled";
        private const string BuildDefinitionSelectorProperty_Highlighted = "Highlighted";
        private const string BuildDefinitionSelectorProperty_Id = "Id";
        private const string BuildDefinitionSelectorProperty_Name = "Name";
        private const string BuildDefinitionSelectorProperty_ScheduleStartTime = "ScheduleStartTime";
        private const string BuildDefinitionSelectorProperty_QueueName = "QueueName";
        private const string BuildDefinitionSelectorProperty_Extra = "Extra";

        private const string BuildDefinitionTag_ContinuousIntegration = "ci";
        private const string BuildDefinitionTag_DEV = "dev";
        private const string BuildDefinitionTag_QA = "qa";
        private const string BuildDefinitionTag_UAT = "uat";
        private const string BuildDefinitionTag_PERF = "perf";
        private const string BuildDefinitionTag_PROD = "prod";
        private const string BuildDefinitionTag_POC = "poc";
        private const string BuildDefinitionTag_TEST = "test";
        private const string BuildDefinitionTag_DEVDR = "devdr";
        private const string BuildDefinitionTag_UATDR = "uatdr";
        private const string BuildDefinitionTag_PRODDR = "proddr";

        private const string Environment_dev = "dev";
        private const string Environment_qa = "qa";
        private const string Environment_uat = "uat";
        private const string Environment_perf = "perf";
        private const string Environment_prod = "prod";
        private const string Environment_devdr = "devdr";
        private const string Environment_uatdr = "uatdr";
        private const string Environment_proddr = "proddr";

        private const string ConfirmOperation_LargeBuildDefinitionCount_Caption = "Confirmation RequiredConfirmOperationLargeBuildDefinitionCount";
        private const string ConfirmOperation_LargeBuildDefinitionCount_Message = "{0} build definitions have been checked. Click 'Yes' to continue.";
        private const int ConfirmOperation_MaxBuildDefinitionCount = 10;

        private const string MatchStatus_ExactMatchFound = "{0} exact match found.";
        private const string MatchStatus_ExactMatchFoundPlural = "{0} exact matches found.";
        private const string MatchStatus_PartialMatchFound = "{0} partial match found.";
        private const string MatchStatus_PartialMatchFoundPlural = "{0} partial matches found.";
        private const string MatchStatus_ExactMatchEnabled = "Exact match enabled.";

        private const string ContextMenuMessage_NoBuildDefinitionsChecked = "No build definitions checked.";

        #endregion

        #region Private Data Members

        private SidekickConfiguration _sidekickConfiguration = null;
        private BuildDefinitionSelectorList _buildDefinitionSelectorList = null;
        private SortableBindingList<BuildDefinitionSelector> _buildDefinitionSelectoryListBindingSource;
        private BuildDefinitionSearchPresenter _buildDefinitionSearchPresenter = null;
        private bool _retrieveRemainingDefinitionsEnabled;
        private bool _batchCheckInProgress;
        private ISidekickCoaction _sidekickCoaction;
        private System.Windows.Forms.SortOrder _buildDefinitionListDataGridViewSortOrder;
        private DataGridViewColumn _buildDefinitionListDataGridViewSortedColumn;
        private bool _coActionSearchAndCheckAll;

        #endregion

        #region Public Delegates

        public event EventHandler<BuildDefinitionSelectionChangedEventArgs> BuildDefinitionSelectionChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Build definition search presenter.
        /// </summary>
        public BuildDefinitionSearchPresenter BuildDefinitionSearchPresenter
        {
            get { return _buildDefinitionSearchPresenter; }
            set { _buildDefinitionSearchPresenter = value; }
        }

        /// <summary>
        /// Property: Build definition name (partial).
        /// </summary>
        public string BuildDefinitionNamePattern
        {
            get
            {
                return this.BuildDefinitionNameFilterTextBox.Text;
            }
            set
            {
                this.BuildDefinitionNameFilterTextBox.Text = value;
            }
        }

        /// <summary>
        /// Property: Build definition selector list.
        /// </summary>
        public BuildDefinitionSelectorList BuildDefinitionSelectorList
        {
            get
            {
                return _buildDefinitionSelectorList;
            }
            set
            {
                _buildDefinitionSelectorList = value;
            }
        }

        /// <summary>
        /// Property: Build definition selector list binding source.
        /// </summary>
        public SortableBindingList<BuildDefinitionSelector> BuildDefinitionSelectorListBindingSource
        {
            get { return _buildDefinitionSelectoryListBindingSource; }
            set { _buildDefinitionSelectoryListBindingSource = value; }
        }

        /// <summary>
        /// Property: Sidekick coaction.
        /// </summary>
        public ISidekickCoaction SidekickCoaction
        {
            get { return _sidekickCoaction; }
            set { _sidekickCoaction = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get
            {
                if (this.InvokeRequired == true)
                {
                    return (string)this.Invoke(new Func<string>(() => this.BackgroundTaskStatus));
                }
                else
                {
                    return this.BuildDefinitionRetrievalStatusLabel.Text;
                }

            }
            set
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke(new Action(() => { this.BackgroundTaskStatus = value; }));
                }
                else
                {
                    this.BuildDefinitionRetrievalStatusLabel.Text = value;
                }
            }

        }

        /// <summary>
        /// Property: Build definition retrieval status.
        /// </summary>
        public string BuildDefinitionRetrievalStatus
        {
            get { return this.BuildDefinitionRetrievalStatusLabel.Text; }
            set { this.BuildDefinitionRetrievalStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Build definition match status.
        /// </summary>
        public string BuildDefinitionMatchStatus
        {
            get { return this.BuildDefinitionMatchStatusLabel.Text; }
            set { this.BuildDefinitionMatchStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Retrieve remaining build definitions enabled.
        /// </summary>
        public bool RetrieveRemainingDefinitionsEnabled
        {
            get
            {
                return _retrieveRemainingDefinitionsEnabled;
            }
            set
            {
                _retrieveRemainingDefinitionsEnabled = value;
            }
        }

        /// <summary>
        /// Property: Batch check in progress.
        /// </summary>
        public bool BatchCheckInProgress
        {
            get { return _batchCheckInProgress; }
            set { _batchCheckInProgress = value; }
        }

        /// <summary>
        /// Property: Exact match (build definition search).
        /// </summary>
        public bool ExactMatch
        {
            get { return this.ExactMatchCheckBox.Checked; }
            set { this.ExactMatchCheckBox.Checked = value; }
        }

        /// <summary>
        /// Property: Build definition list data grid view sort order.
        /// </summary>
        public System.Windows.Forms.SortOrder BuildDefinitionListDataGridViewSortOrder
        {
            get { return _buildDefinitionListDataGridViewSortOrder; }
            set { _buildDefinitionListDataGridViewSortOrder = value; }
        }

        /// <summary>
        /// Property: Build definition list data grid view sorted column.
        /// </summary>
        public DataGridViewColumn BuildDefinitionListDataGridViewSortedColumn
        {
            get { return _buildDefinitionListDataGridViewSortedColumn; }
            set { _buildDefinitionListDataGridViewSortedColumn = value; }
        }

        /// <summary>
        /// Property: Coaction - Search and check all.
        /// </summary>
        public bool ExternalRequest_SearchAndCheckAll
        {
            get { return _coActionSearchAndCheckAll; }
            set { _coActionSearchAndCheckAll = value; }
        }

        #endregion

        #region public constructors

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="buildDefinitionSearchPresenter"></param>
        /// <param name="sidekickConfiguration"></param>
        public BuildDefinitionSearchForm(BuildDefinitionSearchPresenter buildDefinitionSearchPresenter,
                                         SidekickConfiguration sidekickConfiguration,
                                         ISidekickCoaction sidekickCoaction)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.BuildDefinitionSelectorList = new BuildDefinitionSelectorList();

            this.BuildDefinitionSearchPresenter = buildDefinitionSearchPresenter;
            InitializeComponent();
            InitializeControls();
            _buildDefinitionSelectoryListBindingSource = new SortableBindingList<BuildDefinitionSelector>(_buildDefinitionSelectorList);
            this.BuildDefinitionListDataGridView.DataSource = _buildDefinitionSelectoryListBindingSource;
            this.RetrieveRemainingDefinitionsEnabled = true;
            this.SidekickCoaction = sidekickCoaction;
        }

        #endregion

        #region Protected Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        protected BuildDefinitionSearchForm()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Search build definition list.
        /// </summary>
        /// <param name="buildDefinitionNameList"></param>
        /// <param name="exactMatch"></param>
        /// <param name="refresh"></param>
        /// <param name="searchAndCheckAll"></param>
        public void SearchBuildDefinitionList(List<string> buildDefinitionNameList,
                                              bool exactMatch,
                                              bool refresh,
                                              bool searchAndCheckAll)
        {
            try
            {

                this.BuildDefinitionNamePattern = String.Join(Environment.NewLine, buildDefinitionNameList);
                this.ExactMatch = exactMatch;


                if (searchAndCheckAll == true)
                {
                    this.ExternalRequest_SearchAndCheckAll = true;
                    this.ResetBuildDefinitionHighlightToolStripItemCheckedState();
                    this.ResetBuildDefinitionDisplayToolStripItemCheckedState();
                    this.ResetBuildDefinitionCheckToolStripItemCheckedState();

                    this.PerformSearch();

                    this.SearchButton.BackColor = DefaultBackColor;

                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Retrieve remaining build definitions.
        /// </summary>
        public void RetrieveRemainingDefinitions()
        {
            BuildDefinitionList buildDefinitionList = null;

            try
            {
                if (this.RetrieveRemainingDefinitionsEnabled == false)
                {
                    return;
                }

                if (this.BuildDefinitionSelectorList == null || this.BuildDefinitionSelectorList.Count == 0)
                {
                    return;
                }

                buildDefinitionList = this.BuildDefinitionSelectorList.GetBuildDefinitionList();
                if (buildDefinitionList != null && buildDefinitionList.Count > 0)
                {
                    BuildDefinitionSearchPresenter.GetBuildDefinitionSelectorListAsync(this.BuildDefinitionNamePattern, buildDefinitionList, BuildDefinitionRetrievalMode.RetrieveRemaining);
                    this.RetrieveRemainingDefinitionsEnabled = false;
                    this.UpdateRefreshCancelMode(true);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update build definition retrieval status.
        /// </summary>
        /// <param name="buildDefinitionListRetrievedCount"></param>
        /// <param name="buildDefinitionListTotalCount"></param>
        /// <param name="cancelled"></param>
        public void UpdateBuildDefinitionRetrievalStatus(int buildDefinitionListRetrievedCount,
                                                         int buildDefinitionListTotalCount,
                                                         bool cancelled)
        {
            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        UpdateBuildDefinitionRetrievalStatus(buildDefinitionListRetrievedCount,
                                                             buildDefinitionListTotalCount,
                                                             cancelled);
                    });
                }
                else
                {
                    if (buildDefinitionListRetrievedCount < buildDefinitionListTotalCount)
                    {
                        BuildDefinitionRetrievalProgressBar.Visible = true;
                        BuildDefinitionRetrievalProgressBar.Minimum = 0;
                        BuildDefinitionRetrievalProgressBar.Maximum = buildDefinitionListTotalCount;
                        BuildDefinitionRetrievalProgressBar.Value = buildDefinitionListRetrievedCount;
                    }
                    else
                    {
                        BuildDefinitionRetrievalProgressBar.Visible = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update refresh cancel mode.
        /// </summary>
        /// <param name="enabled"></param>
        public void UpdateRefreshCancelMode(bool enabled)
        {
            try
            {
                if (enabled == true)
                {
                    this.RefreshButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionCancelRefresh;
                }
                else
                {
                    this.RefreshButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Enable controls dependant on team project.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnTeamProject(bool enabled)
        {
            this.BuildDefinitionNameFilterTextBox.Enabled = enabled;
            this.SearchButton.Enabled = enabled;
            this.ClearButton.Enabled = enabled;
            this.RefreshButton.Enabled = enabled;
        }

        /// <summary>
        /// Enable controls dependant on search.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnSearch(bool enabled)
        {
            this.CheckSplitButton.Enabled = enabled;
            this.DisplaySplitButton.Enabled = enabled;
            this.HighlightSplitButton.Enabled = enabled;
            this.ViewSplitButton.Enabled = enabled;
            this.CopyButton.Enabled = enabled;
            this.BuildDefinitionListDataGridView.Enabled = enabled;
        }

        /// <summary>
        /// Clear search.
        /// </summary>
        public void ClearSearch()
        {
            try
            {
                this.SearchButton.BackColor = System.Drawing.Color.DarkOrange;
                this.BuildDefinitionSelectorList = new BuildDefinitionSelectorList();
                this.BuildDefinitionNamePattern = string.Empty;
                this.EnableControlsDependantOnSearch(false);
                this.BuildDefinitionListDataGridView.DataSource = null;
                this.BuildDefinitionStatusPanel.BackColor = Color.White;
                this.BuildDefinitionMatchStatus = string.Empty;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {

            try
            {

                this.SearchButton.BackColor = System.Drawing.Color.DarkOrange;

                ClearSearch();
                this.BuildDefinitionSearchPresenter.TeamProjectNameChanged();
                InitializeBuildDefinitionListDataGridView();

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                if (this.BuildDefinitionSelectorList == null ||
                    this.BuildDefinitionSelectorList.Count == 0)
                {
                    this.SearchButton.BackColor = System.Drawing.Color.DarkOrange;
                }
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.UpdateBuildDefinitionRetrievalStatus(0, 0, true);
                this.UpdateRefreshCancelMode(false);

            }
        }

        /// <summary>
        /// Background task Completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                try
                {
                    this.UpdateRefreshCancelMode(false);
                }
                catch (Exception)
                {
                    throw;
                }


            }

        }

        /// <summary>
        /// Retrieve selected build definition selectory list.
        /// </summary>
        /// <returns></returns>
        public BuildDefinitionSelectorList GetSelectedBuildDefinitionSelectorList()
        {
            BuildDefinitionSelectorList result = null;
            BuildDefinitionSelector buildDefinitionSelector = null;
            BuildDefinitionSelectorList buildDefinitionSelectorList = null;
            bool checkedValue = false;
            int id;
            string name = string.Empty;

            try
            {
                result = new BuildDefinitionSelectorList();

                buildDefinitionSelectorList = this.BuildDefinitionSelectorList;

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    if (dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Check].Value is Boolean)
                    {
                        checkedValue = (bool)dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Check].Value;
                        name = (string)dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Name].Value;
                        if (int.TryParse(dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Id].Value.ToString(), out id) == false)
                        {
                            id = 0;
                        }
                        if (checkedValue == true)
                        {
                            buildDefinitionSelector = new BuildDefinitionSelector();
                            buildDefinitionSelector.Id = id;
                            buildDefinitionSelector.Name = name;
                            buildDefinitionSelector.Selected = true;
                            result.Add(buildDefinitionSelector);
                        }
                    }
                }

                //Populate remaining properties.
                result.ForEach(bds =>
                {
                    buildDefinitionSelectorList.ForEach(search =>
                    {
                        if (bds.Name == search.Name)
                        {
                            bds.Enabled = search.Enabled;
                        }
                    });
                });


            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve selected build definition selector count.
        /// </summary>
        /// <returns></returns>
        public int GetSelectedBuildDefinitionSelectorCount()
        {
            int result = 0;

            BuildDefinitionSelectorList buildDefinitionSelectorList = null;

            try
            {
                buildDefinitionSelectorList = this.GetSelectedBuildDefinitionSelectorList();

                result = buildDefinitionSelectorList.Count;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve selected build definition selector list branch names.
        /// </summary>
        /// <returns></returns>
        public List<string> GetSelectedBuildDefinitionSelectorListBranchNameList()
        {
            List<string> result = null;
            BuildDefinitionSelectorList selectedBuildDefinitionSelectorList = null;

            try
            {

                selectedBuildDefinitionSelectorList = GetSelectedBuildDefinitionSelectorList();

                result = this.GetBranchNames(selectedBuildDefinitionSelectorList);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve selected build definition selector list environments.
        /// </summary>
        /// <returns></returns>
        public List<string> GetSelectedBuildDefinitionSelectorListEnvironmentList()
        {
            List<string> result = null;
            BuildDefinitionSelectorList selectedBuildDefinitionSelectorList = null;

            try
            {

                selectedBuildDefinitionSelectorList = GetSelectedBuildDefinitionSelectorList();

                result = this.GetEnvironments(selectedBuildDefinitionSelectorList);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve branch name list.
        /// </summary>
        public List<string> GetBranchNames(BuildDefinitionSelectorList buildDefinitionSelectorList)
        {
            List<string> result = null;
            string path = string.Empty;
            string matchedYearMonthBranchName = string.Empty;
            Match match = null;
            string pattern = string.Empty;

            try
            {

                result = new List<string>();

                pattern = @"(\d+).(\d+[I]?)";

                foreach (BuildDefinitionSelector buildDefinitionSelector in buildDefinitionSelectorList)
                {

                    match = Regex.Match(buildDefinitionSelector.Name,
                                              pattern,
                                              RegexOptions.IgnoreCase);

                    // Match found.
                    if (match.Success)
                    {
                        if (match.Groups.Count == 3)
                        {
                            matchedYearMonthBranchName = match.Groups[1].Value + "." + match.Groups[2].Value;
                        }

                        if (result.Contains(matchedYearMonthBranchName) == false)
                        {
                            result.Add(matchedYearMonthBranchName);
                        }
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Retrieve environment list.
        /// </summary>
        public List<string> GetEnvironments(BuildDefinitionSelectorList buildDefinitionSelectorList)
        {
            List<string> result = null;

            try
            {

                result = new List<string>();

                foreach (BuildDefinitionSelector buildDefinitionSelector in buildDefinitionSelectorList)
                {

                    // Environment - dev match.
                    if (buildDefinitionSelector.Name.ToLower().EndsWith(Environment_dev) == true)
                    {
                        if (result.Contains(Environment_dev) == false)
                        {
                            result.Add(Environment_dev);
                        }
                    }

                    // Environment - qa match.
                    if (buildDefinitionSelector.Name.ToLower().EndsWith(Environment_qa) == true)
                    {
                        if (result.Contains(Environment_qa) == false)
                        {
                            result.Add(Environment_qa);
                        }
                    }

                    // Environment - uat match.
                    if (buildDefinitionSelector.Name.ToLower().EndsWith(Environment_uat) == true)
                    {
                        if (result.Contains(Environment_uat) == false)
                        {
                            result.Add(Environment_uat);
                        }
                    }

                    // Environment - perf match.
                    if (buildDefinitionSelector.Name.ToLower().EndsWith(Environment_perf) == true)
                    {
                        if (result.Contains(Environment_perf) == false)
                        {
                            result.Add(Environment_perf);
                        }
                    }

                    // Environment - prod match.
                    if (buildDefinitionSelector.Name.ToLower().EndsWith(Environment_prod) == true)
                    {
                        if (result.Contains(Environment_prod) == false)
                        {
                            result.Add(Environment_prod);
                        }
                    }

                    // Environment - devdr match.
                    if (buildDefinitionSelector.Name.ToLower().EndsWith(Environment_devdr) == true)
                    {
                        if (result.Contains(Environment_devdr) == false)
                        {
                            result.Add(Environment_devdr);
                        }
                    }

                    // Environment - uatdr match.
                    if (buildDefinitionSelector.Name.ToLower().EndsWith(Environment_uatdr) == true)
                    {
                        if (result.Contains(Environment_uatdr) == false)
                        {
                            result.Add(Environment_uatdr);
                        }
                    }
                    // Environment - proddr match.
                    if (buildDefinitionSelector.Name.ToLower().EndsWith(Environment_proddr) == true)
                    {
                        if (result.Contains(Environment_proddr) == false)
                        {
                            result.Add(Environment_proddr);
                        }
                    }

                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Perform search.
        /// </summary>
        public void PerformSearch()
        {
            BuildDefinitionSelectorList filteredBuildDefinitionSelectorList = null;
            ListSortDirection sortDirection;

            this.BuildDefinitionStatusPanel.BackColor = Color.White;

            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action(this.PerformSearch));
            }
            else
            {
                try
                {
                    this.BuildDefinitionListDataGridViewSortOrder = this.BuildDefinitionListDataGridView.SortOrder;
                    this.BuildDefinitionListDataGridViewSortedColumn = this.BuildDefinitionListDataGridView.SortedColumn;

                    //Reset split buttons context menu state.
                    this.ResetBuildDefinitionCheckToolStripItemCheckedState();
                    this.ResetBuildDefinitionDisplayToolStripItemCheckedState();
                    this.ResetBuildDefinitionHighlightToolStripItemCheckedState();

                    if (BuildDefinitionSelectorList == null || BuildDefinitionSelectorList.Count == 0)
                    {
                        this.BuildDefinitionListDataGridView.DataSource = null;
                        BuildDefinitionSearchPresenter.GetBuildDefinitionSelectorListAsync(this.BuildDefinitionNamePattern, null, BuildDefinitionRetrievalMode.Reset);
                        this.UpdateRefreshCancelMode(true);
                        return;
                    }

                    filteredBuildDefinitionSelectorList = this.BuildDefinitionSelectorList.GetSourceBuildDefinitionListFilterdByBuildDefinitionNamePattern(
                                                                                                        this.BuildDefinitionNamePattern,
                                                                                                        this.ExactMatch);
                    filteredBuildDefinitionSelectorList.Sort(new BuildDefinitionSelector());

                    this.BuildDefinitionSelectorListBindingSource = new SortableBindingList<BuildDefinitionSelector>(filteredBuildDefinitionSelectorList);
                    this.BuildDefinitionListDataGridView.DataSource = this.BuildDefinitionSelectorListBindingSource;

                    foreach (DataGridViewColumn column in this.BuildDefinitionListDataGridView.Columns)
                    {

                        switch (column.Name)
                        {
                            case BuildDefinitionDataGridViewColumn_Check:
                                break;
                            case BuildDefinitionDataGridViewColumn_Enabled:
                                break;
                            case BuildDefinitionDataGridViewColumn_Id:
                                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                                break;
                            case BuildDefinitionDataGridViewColumn_Name:
                                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                                break;
                            case BuildDefinitionDataGridViewColumn_Trigger:
                                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                                break;
                            case BuildDefinitionDataGridViewColumn_QueueName:
                                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                                break;
                            case BuildDefinitionDataGridViewColumn_Extra:
                                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                                break;
                        }
                    }

                    if (this.BuildDefinitionListDataGridView.RowCount > 0)
                    {
                        EnableControlsDependantOnSearch(true);
                    }
                    else
                    {
                        EnableControlsDependantOnSearch(false);
                    }


                    if (this.ExactMatchCheckBox.Checked == true)
                    {
                        if (filteredBuildDefinitionSelectorList.Count == 1)
                        {
                            this.BuildDefinitionMatchStatus = string.Format(MatchStatus_ExactMatchFound,
                                                  filteredBuildDefinitionSelectorList.Count);
                        }
                        else
                        {
                            this.BuildDefinitionMatchStatus = string.Format(MatchStatus_ExactMatchFoundPlural,
                                                  filteredBuildDefinitionSelectorList.Count);
                        }
                        if (filteredBuildDefinitionSelectorList.Count == 0)
                        {
                            this.BuildDefinitionSearchToolTip.Show(MatchStatus_ExactMatchEnabled, this.BuildDefinitionMatchStatusLabel, 4000);
                        }
                    }
                    else
                    {
                        if (filteredBuildDefinitionSelectorList.Count == 1)
                        {
                            this.BuildDefinitionMatchStatus = string.Format(MatchStatus_PartialMatchFound,
                                                  filteredBuildDefinitionSelectorList.Count);
                        }
                        else
                        {
                            this.BuildDefinitionMatchStatus = string.Format(MatchStatus_PartialMatchFoundPlural,
                                                  filteredBuildDefinitionSelectorList.Count);
                        }
                    }

                    this.BuildDefinitionStatusPanel.BackColor = Color.OldLace;

                    this.RaiseBuildDefinitionSelectionChangedEvent();

                    for (int columnIndex = 0; columnIndex < BuildDefinitionListDataGridView.Columns.Count; columnIndex++)
                    {
                        int columnWidth = BuildDefinitionListDataGridView.Columns[columnIndex].Width;
                        BuildDefinitionListDataGridView.Columns[columnIndex].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                        BuildDefinitionListDataGridView.Columns[columnIndex].Width = columnWidth;
                    }



                    //Restore sort order.
                    if (this.BuildDefinitionListDataGridViewSortOrder == SortOrder.Ascending)
                    {
                        sortDirection = ListSortDirection.Ascending;
                    }
                    else
                    {
                        sortDirection = ListSortDirection.Descending;
                    }

                    if (this.BuildDefinitionListDataGridViewSortedColumn != null)
                    {
                        this.BuildDefinitionListDataGridView.Sort(this.BuildDefinitionListDataGridView.Columns[this.BuildDefinitionListDataGridViewSortedColumn.Name], sortDirection); ;
                    }

                    if (this.ExternalRequest_SearchAndCheckAll == true)
                    {
                        this.Cursor = Cursors.WaitCursor;
                        this.BatchCheckInProgress = true;

                        this.ResetBuildDefinitionCheckToolStripItemCheckedState();

                        this.BuildDefinitionCheckAll();

                    }
                    this.ExternalRequest_SearchAndCheckAll = false;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    this.BatchCheckInProgress = false;
                    this.Cursor = Cursors.Default;
                }
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {

            try
            {
                this.ExactMatch = false;
                InitializeBuildDefinitionListDataGridView();
                this.BuildDefinitionRetrievalStatusLabel.Text = string.Empty;
                this.BuildDefinitionMatchStatusLabel.Text = string.Empty;
                this.BuildDefinitionSearchToolTip.OwnerDraw = true;

                this.UpdateRefreshCancelMode(false);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Initialize build definition list data grid view.
        /// </summary>
        protected void InitializeBuildDefinitionListDataGridView()
        {
            BindingSource bindingSource = null;

            try
            {

                this.BuildDefinitionListDataGridView.DataSource = null;
                this.BuildDefinitionListDataGridView.AutoGenerateColumns = false;
                this.BuildDefinitionListDataGridView.DataSource = bindingSource;

                foreach (DataGridViewColumn column in this.BuildDefinitionListDataGridView.Columns)
                {

                    switch (column.Name)
                    {
                        case BuildDefinitionDataGridViewColumn_Check:
                            column.DataPropertyName = BuildDefinitionSelectorProperty_Selected;
                            break;
                        case BuildDefinitionDataGridViewColumn_Enabled:
                            column.DataPropertyName = BuildDefinitionSelectorProperty_Enabled;
                            break;
                        case BuildDefinitionDataGridViewColumn_Id:
                            column.DataPropertyName = BuildDefinitionSelectorProperty_Id;
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            break;
                        case BuildDefinitionDataGridViewColumn_Name:
                            column.DataPropertyName = BuildDefinitionSelectorProperty_Name;
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            break;
                        case BuildDefinitionDataGridViewColumn_Trigger:
                            column.DataPropertyName = BuildDefinitionSelectorProperty_ScheduleStartTime;
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            break;
                        case BuildDefinitionDataGridViewColumn_QueueName:
                            column.DataPropertyName = BuildDefinitionSelectorProperty_QueueName;
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            break;
                        case BuildDefinitionDataGridViewColumn_Extra:
                            column.DataPropertyName = BuildDefinitionSelectorProperty_Extra;
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            break;
                    }
                }

                if (this.BuildDefinitionListDataGridView.RowCount > 0)
                {
                    EnableControlsDependantOnSearch(true);
                }
                else
                {
                    EnableControlsDependantOnSearch(false);
                }

                this.BuildDefinitionListDataGridView.Columns[BuildDefinitionDataGridViewColumn_Trigger].Visible = false;
                this.BuildDefinitionListDataGridView.Columns[BuildDefinitionDataGridViewColumn_QueueName].Visible = false;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Build definition selection changed.
        /// </summary>
        protected void RaiseBuildDefinitionSelectionChangedEvent()
        {
            BuildDefinitionSelectionChangedEventArgs buildDefinitionSelectionChangedEventArgs = null;
            BuildDefinitionSelector buildDefinitionSelector = null;

            try
            {

                buildDefinitionSelectionChangedEventArgs = new BuildDefinitionSelectionChangedEventArgs();

                buildDefinitionSelectionChangedEventArgs.OneOrMoreSelected = false;
                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {

                    buildDefinitionSelector = (BuildDefinitionSelector)dataGridViewRow.DataBoundItem;
                    if (buildDefinitionSelector.Selected == true)
                    {
                        buildDefinitionSelectionChangedEventArgs.OneOrMoreSelected = true;
                        break;
                    }

                }

                if (BuildDefinitionSelectionChanged != null)
                {
                    BuildDefinitionSelectionChanged(this, buildDefinitionSelectionChangedEventArgs);
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Toggle display build definition with matching tag.
        /// </summary>
        /// <param name="highlight"></param>
        /// <param name="matchTag"></param>
        protected void ToggleHighlightBuildDefinitionsWithMatchingTag(bool highlight, string matchTag)
        {
            BuildDefinitionSelector buildDefinitionSelector = null;
            DataGridViewCell dataGridViewCell = null;
            string name = string.Empty;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    dataGridViewCell = dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Name];

                    if (dataGridViewCell != null)
                    {
                        if (dataGridViewCell.Value != null)
                        {
                            name = dataGridViewCell.Value.ToString();
                            buildDefinitionSelector = (BuildDefinitionSelector)BuildDefinitionListDataGridView.Rows[dataGridViewRow.Index].DataBoundItem;

                            if (string.IsNullOrEmpty(name) == false && name.ToLower().EndsWith(matchTag.ToLower()) == true)
                            {

                                if (highlight == true)
                                {
                                    dataGridViewRow.DefaultCellStyle.BackColor = System.Drawing.Color.LightGoldenrodYellow;
                                    buildDefinitionSelector.Highlighted = true;
                                }
                                else
                                {
                                    dataGridViewRow.DefaultCellStyle.BackColor = System.Drawing.Color.White;
                                    buildDefinitionSelector.Highlighted = false;
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Toggle check build definition with matching tag.
        /// </summary>
        /// <param name="matchTag"></param>
        protected void ToggleCheckBuildDefinitionsWithMatchingTag(List<string> buildDefinitionTagMatchList)
        {
            BuildDefinitionSelector buildDefinitionSelector = null;
            DataGridViewCell dataGridViewCell = null;
            string name = string.Empty;
            bool checkChanged = false;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    dataGridViewCell = dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Name];

                    if (dataGridViewCell != null)
                    {
                        if (dataGridViewCell.Value != null)
                        {
                            name = dataGridViewCell.Value.ToString();
                            buildDefinitionSelector =
                                (BuildDefinitionSelector)
                                    BuildDefinitionListDataGridView.Rows[dataGridViewRow.Index].DataBoundItem;

                            if (string.IsNullOrEmpty(name) == false &&
                                buildDefinitionTagMatchList.Any(name.ToLower().EndsWith) == true)
                            {
                                dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Check].Value = true;
                                checkChanged = true;
                            }
                            else
                            {
                                dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Check].Value = false;
                                checkChanged = true;
                            }

                        }
                    }

                }

                if (checkChanged == true)
                {
                    this.RaiseBuildDefinitionSelectionChangedEvent();
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        /// <summary>
        /// Toggle display build definition with matching tag.
        /// </summary>
        /// <param name="buildDefinitionTagMatchList"></param>
        protected void ToggleDisplayBuildDefinitionsWithMatchingTag(List<string> buildDefinitionTagMatchList)
        {
            DataGridViewCell dataGridViewCell = null;
            string name = string.Empty;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    dataGridViewCell = dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Name];

                    if (dataGridViewCell != null)
                    {
                        if (dataGridViewCell.Value != null)
                        {
                            name = dataGridViewCell.Value.ToString();
                        }
                    }
                    else
                    {
                        name = string.Empty;
                    }

                    if (string.IsNullOrEmpty(name) == false &&
                        buildDefinitionTagMatchList.Any(name.ToLower().EndsWith) == true)
                    {
                        dataGridViewRow.Visible = true;
                    }
                    else
                    {
                        CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[BuildDefinitionListDataGridView.DataSource];
                        currencyManager1.SuspendBinding();
                        dataGridViewRow.Visible = false;
                        currencyManager1.ResumeBinding();
                    }
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Remove build definition highlight - all.
        /// </summary>
        protected void RemoveBuildDefinitionHighlightAll()
        {
            BuildDefinitionSelector buildDefinitionSelector = null;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    buildDefinitionSelector = (BuildDefinitionSelector)BuildDefinitionListDataGridView.Rows[dataGridViewRow.Index].DataBoundItem;

                    if (buildDefinitionSelector.Highlighted == true)
                    {
                        dataGridViewRow.DefaultCellStyle.BackColor = System.Drawing.Color.White;
                        buildDefinitionSelector.Highlighted = false;
                    }
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Reset build definition highlight tool strip items - checked state.
        /// </summary>
        protected void ResetBuildDefinitionHighlightToolStripItemCheckedState()
        {

            if (this.BuildDefinitionHighlightDisabledToolStripItem.Checked == true)
            {
                this.BuildDefinitionHighlightDisabledToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionHighlightCIToolStripItem.Checked == true)
            {
                this.BuildDefinitionHighlightCIToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionHighlightDevToolStripItem.Checked == true)
            {
                this.BuildDefinitionHighlightDevToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionHighlightQAToolStripItem.Checked == true)
            {
                this.BuildDefinitionHighlightQAToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionHighlightUATToolStripItem.Checked == true)
            {
                this.BuildDefinitionHighlightUATToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionHighlightPerfToolStripItem.Checked == true)
            {
                this.BuildDefinitionHighlightPerfToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionHighlightProductionToolStripItem.Checked == true)
            {
                this.BuildDefinitionHighlightProductionToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionHighlightPOCToolStripItem.Checked == true)
            {
                this.BuildDefinitionHighlightPOCToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionHighlightTESTToolStripItem.Checked == true)
            {
                this.BuildDefinitionHighlightTESTToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionHighlightDevDRToolStripItem.Checked == true)
            {
                this.BuildDefinitionHighlightDevDRToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionHighlightUATDRToolStripItem.Checked == true)
            {
                this.BuildDefinitionHighlightUATDRToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionHighlightProdDRToolStripItem.Checked == true)
            {
                this.BuildDefinitionHighlightProdDRToolStripItem.Checked = false;
            }

        }

        /// <summary>
        /// Reset build definition display tool strip items - checked state.
        /// </summary>
        protected void ResetBuildDefinitionDisplayToolStripItemCheckedState()
        {

            if (this.BuildDefinitionDisplayDisabledToolStripItem.Checked == true)
            {
                this.BuildDefinitionDisplayDisabledToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionDisplayCIToolStripItem.Checked == true)
            {
                this.BuildDefinitionDisplayCIToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionDisplayDevToolStripItem.Checked == true)
            {
                this.BuildDefinitionDisplayDevToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionDisplayQAToolStripItem.Checked == true)
            {
                this.BuildDefinitionDisplayQAToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionDisplayUATToolStripItem.Checked == true)
            {
                this.BuildDefinitionDisplayUATToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionDisplayPerfToolStripItem.Checked == true)
            {
                this.BuildDefinitionDisplayPerfToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionDisplayProductionToolStripItem.Checked == true)
            {
                this.BuildDefinitionDisplayProductionToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionDisplayPOCToolStripItem.Checked == true)
            {
                this.BuildDefinitionDisplayPOCToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionDisplayTESTToolStripItem.Checked == true)
            {
                this.BuildDefinitionDisplayTESTToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionDisplayDevDRToolStripItem.Checked == true)
            {
                this.BuildDefinitionDisplayDevDRToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionDisplayUATDRToolStripItem.Checked == true)
            {
                this.BuildDefinitionDisplayUATDRToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionDisplayProdDRToolStripItem.Checked == true)
            {
                this.BuildDefinitionDisplayProdDRToolStripItem.Checked = false;
            }

        }

        /// <summary>
        /// Reset build definition check tool strip items - checked state.
        /// </summary>
        protected void ResetBuildDefinitionCheckToolStripItemCheckedState()
        {

            if (this.BuildDefinitionCheckDisabledToolStripItem.Checked == true)
            {
                this.BuildDefinitionCheckDisabledToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionCheckCIToolStripItem.Checked == true)
            {
                this.BuildDefinitionCheckCIToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionCheckDevToolStripItem.Checked == true)
            {
                this.BuildDefinitionCheckDevToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionCheckQAToolStripItem.Checked == true)
            {
                this.BuildDefinitionCheckQAToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionCheckUATToolStripItem.Checked == true)
            {
                this.BuildDefinitionCheckUATToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionCheckPerfToolStripItem.Checked == true)
            {
                this.BuildDefinitionCheckPerfToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionCheckProductionToolStripItem.Checked == true)
            {
                this.BuildDefinitionCheckProductionToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionCheckPOCToolStripItem.Checked == true)
            {
                this.BuildDefinitionCheckPOCToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionCheckTESTToolStripItem.Checked == true)
            {
                this.BuildDefinitionCheckTESTToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionCheckDevDRToolStripItem.Checked == true)
            {
                this.BuildDefinitionCheckDevDRToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionCheckUATDRToolStripItem.Checked == true)
            {
                this.BuildDefinitionCheckUATDRToolStripItem.Checked = false;
            }

            if (this.BuildDefinitionCheckProdDRToolStripItem.Checked == true)
            {
                this.BuildDefinitionCheckProdDRToolStripItem.Checked = false;
            }

        }

        /// <summary>
        /// Display all build definitions in build definition Build Definition List DataGridView.
        /// </summary>
        protected void BuildDefinitionDisplayAll()
        {
            this.BuildDefinitionListDataGridView.CurrentCell = null;

            foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
            {
                dataGridViewRow.Visible = true;
            }
        }

        /// <summary>
        /// Build definition check all.
        /// </summary>
        protected void BuildDefinitionCheckAll()
        {
            BuildDefinitionSelector buildDefinitionSelector = null;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    buildDefinitionSelector = (BuildDefinitionSelector)dataGridViewRow.DataBoundItem;
                    if (dataGridViewRow.Visible == true)
                    {
                        dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Check].Value = true;
                    }
                }
                this.RaiseBuildDefinitionSelectionChangedEvent();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Build definition check none.
        /// </summary>
        protected void BuildDefinitionCheckNone()
        {
            this.Cursor = Cursors.WaitCursor;
            foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
            {
                dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Check].Value = false;
            }
            this.RaiseBuildDefinitionSelectionChangedEvent();

        }

        /// <summary>
        /// Retrieve build definition 'Display' tag match list.
        /// </summary>
        /// <returns></returns>
        protected List<string> GetBuildDefinitionDisplayTagMatchList()
        {
            List<string> result = null;

            try
            {
                result = new List<string>();

                //Continous integration:
                if (this.BuildDefinitionDisplayCIToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_ContinuousIntegration);
                }

                //Non-production environments:
                if (this.BuildDefinitionDisplayDevToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_DEV);
                }

                if (this.BuildDefinitionDisplayQAToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_QA);
                }

                if (this.BuildDefinitionDisplayUATToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_UAT);
                }

                if (this.BuildDefinitionDisplayPerfToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_PERF);
                }

                //Production environmnet:
                if (this.BuildDefinitionDisplayProductionToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_PROD);
                }

                //Non-environment:
                if (this.BuildDefinitionDisplayPOCToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_POC);
                }
                if (this.BuildDefinitionDisplayTESTToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_TEST);
                }

                //Disaster recovery environments:
                if (this.BuildDefinitionDisplayDevDRToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_DEVDR);
                }

                if (this.BuildDefinitionDisplayUATDRToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_UATDR);
                }

                if (this.BuildDefinitionDisplayProdDRToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_PRODDR);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Retrieve build definition 'check' tag match list.
        /// </summary>
        /// <returns></returns>
        protected List<string> GetBuildDefinitionCheckTagMatchList()
        {
            List<string> result = null;

            try
            {
                result = new List<string>();

                //Continous integration:
                if (this.BuildDefinitionCheckCIToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_ContinuousIntegration);
                }

                //Non-production environments:
                if (this.BuildDefinitionCheckDevToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_DEV);
                }

                if (this.BuildDefinitionCheckQAToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_QA);
                }

                if (this.BuildDefinitionCheckUATToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_UAT);
                }

                if (this.BuildDefinitionCheckPerfToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_PERF);
                }

                //Production environmnet:
                if (this.BuildDefinitionCheckProductionToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_PROD);
                }

                //Non-environment:
                if (this.BuildDefinitionCheckPOCToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_POC);
                }
                if (this.BuildDefinitionCheckTESTToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_TEST);
                }

                //Disaster recovery environments:
                if (this.BuildDefinitionCheckDevDRToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_DEVDR);
                }

                if (this.BuildDefinitionCheckUATDRToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_UATDR);
                }

                if (this.BuildDefinitionCheckProdDRToolStripItem.Checked == true)
                {
                    result.Add(BuildDefinitionTag_PRODDR);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Retrieve checked build definition count.
        /// </summary>
        /// <returns></returns>
        protected int GetCheckedBuildDefinitionCount()
        {
            int result = 0;
            BuildDefinitionSelector buildDefinitionSelector = null;

            try
            {
                //Retrieve checked count.
                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    if (dataGridViewRow.Visible == true)
                    {
                        buildDefinitionSelector = (BuildDefinitionSelector)dataGridViewRow.DataBoundItem;
                        if (buildDefinitionSelector.Selected == true)
                        {
                            ++result;
                        }
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Confirm operation on large number of build definitions.
        /// </summary>
        /// <param name="checkedBuildDefinitionCount"></param>
        /// <returns></returns>
        protected bool ConfirmOperationLargeBuildDefinitionCount(int checkedBuildDefinitionCount)
        {
            bool result = false;
            DialogResult promptDialogResult;

            promptDialogResult = MessageBox.Show(string.Format(ConfirmOperation_LargeBuildDefinitionCount_Message, checkedBuildDefinitionCount),
                                                 string.Format(ConfirmOperation_LargeBuildDefinitionCount_Caption),
                                                 MessageBoxButtons.YesNoCancel,
                                                 System.Windows.Forms.MessageBoxIcon.Exclamation);
            if (promptDialogResult == DialogResult.Yes)
            {
                result = true;
            }

            return result;

        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Search button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchButton_Click(object sender, EventArgs e)
        {

            try
            {

                this.Cursor = Cursors.WaitCursor;

                this.ResetBuildDefinitionHighlightToolStripItemCheckedState();
                this.ResetBuildDefinitionDisplayToolStripItemCheckedState();
                this.ResetBuildDefinitionCheckToolStripItemCheckedState();

                this.PerformSearch();

                this.SearchButton.BackColor = DefaultBackColor;

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Clear button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearButton_Click(object sender, EventArgs e)
        {

            try
            {

                this.Cursor = Cursors.WaitCursor;

                if (this.BuildDefinitionSearchPresenter.IsBackgroundTaskRunning() == true)
                {
                    this.BuildDefinitionSearchPresenter.CancelBackgroundTask();
                    this.UpdateRefreshCancelMode(false);
                }

                ClearSearch();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Refresh button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void RefreshButton_Click(object sender, EventArgs e)
        {

            try
            {

                if (this.BuildDefinitionSearchPresenter.IsBackgroundTaskRunning() == true)
                {
                    this.BuildDefinitionSearchPresenter.CancelBackgroundTask();
                    this.UpdateRefreshCancelMode(false);
                }
                else
                {
                    this.BuildDefinitionSelectorList = new BuildDefinitionSelectorList();
                    this.BuildDefinitionListDataGridView.DataSource = null;
                    RaiseBuildDefinitionSelectionChangedEvent();
                    BuildDefinitionSearchPresenter.GetBuildDefinitionSelectorListAsync(this.BuildDefinitionNamePattern, null, BuildDefinitionRetrievalMode.Reset);
                    this.RetrieveRemainingDefinitionsEnabled = true;
                    this.UpdateRefreshCancelMode(true);
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
            }

        }

        /// <summary>
        /// Event Handler: Copy button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyButton_Click(object sender, EventArgs e)
        {

            BuildDefinitionSelector buildDefinitionSelector = null;
            StringBuilder buildDefinitionListStringBuilder = null;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                buildDefinitionListStringBuilder = new StringBuilder();

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    buildDefinitionSelector = (BuildDefinitionSelector)dataGridViewRow.DataBoundItem;

                    if (buildDefinitionSelector.Selected == true)
                    {
                        buildDefinitionListStringBuilder.AppendLine(string.Format("{0}",
                                                                                  buildDefinitionSelector.Name));
                    }

                }

                Clipboard.SetDataObject(buildDefinitionListStringBuilder.ToString());

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Build defintion check all tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionCheckAllToolStripItem_Click(object sender, EventArgs e)
        {

            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.BatchCheckInProgress = true;

                this.ResetBuildDefinitionCheckToolStripItemCheckedState();

                this.BuildDefinitionCheckAll();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.BatchCheckInProgress = false;
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build defintion check none tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionChecktNoneToolStripItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                ResetBuildDefinitionCheckToolStripItemCheckedState();

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Check].Value = false;
                }
                this.RaiseBuildDefinitionSelectionChangedEvent();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition check enabled tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionCheckEnabledToolStripItem_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelector buildDefinitionSelector = null;
            bool checkChanged = false;

            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.BatchCheckInProgress = true;

                ResetBuildDefinitionCheckToolStripItemCheckedState();

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {

                    buildDefinitionSelector = (BuildDefinitionSelector)dataGridViewRow.DataBoundItem;
                    if (dataGridViewRow.Visible == true && buildDefinitionSelector.Enabled == true)
                    {
                        dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Check].Value = true;
                        checkChanged = true;
                    }
                    else
                    {
                        dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Check].Value = false;
                        checkChanged = true;
                    }

                }

                if (checkChanged == true)
                {
                    RaiseBuildDefinitionSelectionChangedEvent();
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.BatchCheckInProgress = false;
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition check disabled tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionCheckDisabledToolStripItem_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelector buildDefinitionSelector = null;
            bool checkChanged = false;

            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.BatchCheckInProgress = true;

                ResetBuildDefinitionCheckToolStripItemCheckedState();

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {

                    buildDefinitionSelector = (BuildDefinitionSelector)dataGridViewRow.DataBoundItem;
                    if (dataGridViewRow.Visible == true && buildDefinitionSelector.Enabled == false)
                    {
                        dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Check].Value = true;
                        checkChanged = true;
                    }
                    else
                    {
                        dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Check].Value = false;
                        checkChanged = true;
                    }

                }

                if (checkChanged == true)
                {
                    RaiseBuildDefinitionSelectionChangedEvent();
                }
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.BatchCheckInProgress = false;
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition check highlighted tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionCheckHighlightedToolStripItem_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelector buildDefinitionSelector = null;
            bool checkChanged = false;

            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.BatchCheckInProgress = true;

                ResetBuildDefinitionCheckToolStripItemCheckedState();

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {

                    buildDefinitionSelector = (BuildDefinitionSelector)dataGridViewRow.DataBoundItem;
                    if (dataGridViewRow.Visible == true && buildDefinitionSelector.Highlighted == true)
                    {
                        dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Check].Value = true;
                        checkChanged = true;
                    }
                    else
                    {
                        dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Check].Value = false;
                        checkChanged = true;
                    }

                }
                if (checkChanged == true)
                {
                    RaiseBuildDefinitionSelectionChangedEvent();
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.BatchCheckInProgress = false;
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition check continuous integration tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void BuildDefinitionCheckCIToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionCheckTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition check dev tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void BuildDefinitionCheckDevToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionCheckTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition check QA tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void BuildDefinitionCheckQAToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionCheckTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition check UAT tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionCheckUATToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionCheckTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        /// <summary>
        /// Event Handler: Build definition check Perf tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionCheckPerfToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionCheckTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition check production tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionCheckProductionToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionCheckTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition check POC tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionCheckPOCToolStripItem_Click(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionCheckTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        /// <summary>
        /// Event Handler: Build definition check TEST tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionCheckTESTToolStripItem_Click(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionCheckTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition check dev dr tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void BuildDefinitionCheckDevDRToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionCheckTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition check UAT dr tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void BuildDefinitionCheckUATDRToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionCheckTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition check Production DR tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionCheckProdDRToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionCheckTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionCheckNone();
                    return;
                }

                this.ToggleCheckBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build defintion display all tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionDisplayAllToolStripItem_Click(object sender, EventArgs e)
        {

            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ResetBuildDefinitionDisplayToolStripItemCheckedState();

                this.BuildDefinitionDisplayAll();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Build defintion display checked tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionDisplayCheckedToolStripItem_Click(object sender, EventArgs e)
        {
            bool checkedValue = false;
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.BuildDefinitionListDataGridView.CurrentCell = null;

                this.ResetBuildDefinitionDisplayToolStripItemCheckedState();

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    bool.TryParse(dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Check].Value.ToString(),
                                  out checkedValue);

                    if (checkedValue == true)
                    {
                        dataGridViewRow.Visible = true;
                    }
                    else
                    {
                        dataGridViewRow.Visible = false;
                    };

                }
                this.RaiseBuildDefinitionSelectionChangedEvent();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build defintion display enabled tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionDisplayEnabledToolStripItem_Click(object sender, EventArgs e)
        {

            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ResetBuildDefinitionDisplayToolStripItemCheckedState();

                this.BuildDefinitionListDataGridView.CurrentCell = null;

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    BuildDefinitionSelector buildDefinitionSelector = (BuildDefinitionSelector)dataGridViewRow.DataBoundItem;


                    if (buildDefinitionSelector.Enabled == true)
                    {
                        dataGridViewRow.Visible = true;
                    }
                    else
                    {
                        dataGridViewRow.Visible = false;
                    };

                }
                this.RaiseBuildDefinitionSelectionChangedEvent();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build defintion display disabled tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionDisplayDisabledToolStripItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ResetBuildDefinitionDisplayToolStripItemCheckedState();

                this.BuildDefinitionListDataGridView.CurrentCell = null;

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    BuildDefinitionSelector buildDefinitionSelector = (BuildDefinitionSelector)dataGridViewRow.DataBoundItem;


                    if (buildDefinitionSelector.Enabled == false)
                    {
                        dataGridViewRow.Visible = true;
                    }
                    else
                    {
                        dataGridViewRow.Visible = false;
                    };

                }
                this.RaiseBuildDefinitionSelectionChangedEvent();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Build defintion display highlighted tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionDisplayHighlightedToolStripItem_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelector buildDefinitionSelector = null;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ResetBuildDefinitionDisplayToolStripItemCheckedState();

                this.BuildDefinitionListDataGridView.CurrentCell = null;

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {

                    buildDefinitionSelector = (BuildDefinitionSelector)BuildDefinitionListDataGridView.Rows[dataGridViewRow.Index].DataBoundItem;

                    if (buildDefinitionSelector.Highlighted == true)
                    {
                        dataGridViewRow.Visible = true;
                    }
                    else
                    {
                        dataGridViewRow.Visible = false;
                        buildDefinitionSelector.Selected = false;
                    };

                }
                this.RaiseBuildDefinitionSelectionChangedEvent();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Build Definition Display CI Tool Strip Item - Checked Changed.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionDisplayCIToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionDisplayTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Build Definition Display Dev Tool Strip Item - Checked Changed.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void BuildDefinitionDisplayDevToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionDisplayTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Build Definition Display QA Tool Strip Item - Checked Changed.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void BuildDefinitionDisplayQAToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionDisplayTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Build Definition Display UAT Tool Strip Item - Checked Changed.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void BuildDefinitionDisplayUATToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionDisplayTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Build Definition Display Perf Tool Strip Item - Checked Changed.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionDisplayPerfToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionDisplayTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Build Definition Display Production Tool Strip Item - Checked Changed.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void BuildDefinitionDisplayProductionToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionDisplayTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Build Definition Display POC Tool Strip Item - Checked Changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionDisplayPOCToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionDisplayTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler:Build Definition Display Test Tool Strip Item - Checked Changed.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionDisplayTESTToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionDisplayTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Build Definition Display Dev DR Tool Strip Item - Checked Changed.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void BuildDefinitionDisplayDevDRToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionDisplayTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Build Definition Display UAT DR Tool Strip Item - Checked Changed.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void BuildDefinitionDisplayUATDRToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionDisplayTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler:Build Definition Display Prod DR Tool Strip Item - Checked Changed.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionDisplayProdDRToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;
            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionDisplayTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    BuildDefinitionDisplayAll();
                    return;
                }

                this.ToggleDisplayBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition list data grive view - cell formatting.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionListDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            try
            {
                if (BuildDefinitionListDataGridView.Columns[e.ColumnIndex].Name == BuildDefinitionDataGridViewColumn_Enabled && e.RowIndex >= 0 && e.RowIndex < BuildDefinitionListDataGridView.Rows.Count)
                {
                    if (BuildDefinitionListDataGridView.Rows[e.RowIndex].DataBoundItem != null)
                    {
                        BuildDefinitionSelector buildDefinitionSelector = (BuildDefinitionSelector)BuildDefinitionListDataGridView.Rows[e.RowIndex].DataBoundItem;
                        if (buildDefinitionSelector.Enabled == true)
                        {
                            e.Value = (System.Drawing.Image)Properties.Resources.QueueStatusEnabled;
                        }
                        else
                        {
                            e.Value = (System.Drawing.Image)Properties.Resources.QueueStatusDisabled;
                        }
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition list data grive view - cell clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionListDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                this.BuildDefinitionListDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);


            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition list data grive view - cell double click.
        /// </summary>
        /// <remarks>
        /// Without this override double clicking on checkbox column can force checkbox value 
        /// and build definition selector [Selected] values be become out of sync. 
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionListDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (BuildDefinitionListDataGridView.Columns[e.ColumnIndex].Name == BuildDefinitionDataGridViewColumn_Check && e.RowIndex >= 0 && e.RowIndex < BuildDefinitionListDataGridView.Rows.Count)
                {
                    BuildDefinitionSelector buildDefinitionSelector = (BuildDefinitionSelector)BuildDefinitionListDataGridView.Rows[e.RowIndex].DataBoundItem;

                    DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)BuildDefinitionListDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];

                    if ((bool)cell.Value == true)
                    {
                        cell.EditingCellFormattedValue = false;
                    }
                    else
                    {
                        cell.EditingCellFormattedValue = true;
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition list data grive view - cell content clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionListDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.BuildDefinitionListDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition list data grid view - cell value changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionListDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (BuildDefinitionListDataGridView.Columns[e.ColumnIndex].Name == BuildDefinitionDataGridViewColumn_Check && e.RowIndex >= 0 && e.RowIndex < BuildDefinitionListDataGridView.Rows.Count)
                {
                    BuildDefinitionSelector buildDefinitionSelector = (BuildDefinitionSelector)BuildDefinitionListDataGridView.Rows[e.RowIndex].DataBoundItem;

                    DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)BuildDefinitionListDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];

                    if ((bool)cell.Value == true)
                    {
                        buildDefinitionSelector.Selected = true;
                    }
                    else
                    {
                        buildDefinitionSelector.Selected = false;
                    }

                    if (this.BatchCheckInProgress == false)
                    {
                        this.RaiseBuildDefinitionSelectionChangedEvent();
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition name parital text box - text changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionNamePartialTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.SearchButton.BackColor = System.Drawing.Color.DarkOrange;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition display disabled tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionHighlightDisabledToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ResetBuildDefinitionDisplayToolStripItemCheckedState();

                this.BuildDefinitionListDataGridView.CurrentCell = null;

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    BuildDefinitionSelector buildDefinitionSelector = (BuildDefinitionSelector)dataGridViewRow.DataBoundItem;


                    if (buildDefinitionSelector.Enabled == true)
                    {
                        dataGridViewRow.Visible = false;
                    }
                    else
                    {
                        dataGridViewRow.Visible = true;
                    };

                }
                this.RaiseBuildDefinitionSelectionChangedEvent();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Build definition display CI tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionHighlightCIToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightBuildDefinitionsWithMatchingTag(this.BuildDefinitionHighlightCIToolStripItem.Checked, BuildDefinitionTag_ContinuousIntegration);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition display dev tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionHighlightDevToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightBuildDefinitionsWithMatchingTag(this.BuildDefinitionHighlightDevToolStripItem.Checked, BuildDefinitionTag_DEV);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition display QA tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionHighlightQAToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightBuildDefinitionsWithMatchingTag(this.BuildDefinitionHighlightQAToolStripItem.Checked, BuildDefinitionTag_QA);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition display UAT tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionHighlightUATToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightBuildDefinitionsWithMatchingTag(this.BuildDefinitionHighlightUATToolStripItem.Checked, BuildDefinitionTag_UAT);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition display Perf tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionHighlightPerfToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightBuildDefinitionsWithMatchingTag(this.BuildDefinitionHighlightPerfToolStripItem.Checked, BuildDefinitionTag_PERF);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition display Production tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionHighlightProductionToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightBuildDefinitionsWithMatchingTag(this.BuildDefinitionHighlightProductionToolStripItem.Checked, BuildDefinitionTag_PROD);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition display POC tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionHighlightPOCToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightBuildDefinitionsWithMatchingTag(this.BuildDefinitionHighlightProductionToolStripItem.Checked, BuildDefinitionTag_POC);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition display Test tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionHighlightTESTToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightBuildDefinitionsWithMatchingTag(this.BuildDefinitionHighlightProductionToolStripItem.Checked, BuildDefinitionTag_TEST);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition display dev disaster recovery tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionHighlightDevDRToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightBuildDefinitionsWithMatchingTag(this.BuildDefinitionHighlightDevDRToolStripItem.Checked, BuildDefinitionTag_DEVDR);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition display uat disaster recovery tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionHighlightUATDRToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightBuildDefinitionsWithMatchingTag(this.BuildDefinitionHighlightUATDRToolStripItem.Checked, BuildDefinitionTag_UATDR);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition display prod disaster recovery tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionHighlightProdDRToolStripItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.ToggleHighlightBuildDefinitionsWithMatchingTag(this.BuildDefinitionHighlightProdDRToolStripItem.Checked, BuildDefinitionTag_PRODDR);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition high none tool strip item - checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionHighlightNoneToolStripItem_Click(object sender, EventArgs e)
        {

            try
            {
                this.ResetBuildDefinitionHighlightToolStripItemCheckedState();
                this.ResetBuildDefinitionDisplayToolStripItemCheckedState();
                this.ResetBuildDefinitionCheckToolStripItemCheckedState();


            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Build definition filter tool strip menu item - opening.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionNameFilterContextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {

                if (this.BuildDefinitionNameFilterTextBox.SelectionLength == 0)
                {
                    this.BuildDefinitionNameFilterDeleteToolStripMenuItem.Enabled = false;
                }
                else
                {
                    this.BuildDefinitionNameFilterDeleteToolStripMenuItem.Enabled = true;
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition filter cut tool strip menu item - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionNameFilterCutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.BuildDefinitionNameFilterTextBox.Cut();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition filter copy tool strip menu item - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionNameFilterCopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetText(this.BuildDefinitionNameFilterTextBox.SelectedText);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition filter paste tool strip menu item - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionNameFilterPasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Clipboard.ContainsText())
                {
                    this.BuildDefinitionNameFilterTextBox.Paste();
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition filter delete tool strip menu item - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionNameFilterDeleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.BuildDefinitionNameFilterTextBox.SelectionLength > 0)
                {
                    this.BuildDefinitionNameFilterTextBox.SelectedText = string.Empty;
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Build definition filter select all tool strip menu item - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionNameFilterSelectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.BuildDefinitionNameFilterTextBox.Focus();
                this.BuildDefinitionNameFilterTextBox.SelectAll();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Copy build definition tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyBuildDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelector buildDefinitionSelector = null;
            StringBuilder buildDefinitionListStringBuilder = null;
            int checkedBuildDefinitionCount = 0;

            try
            {
                buildDefinitionListStringBuilder = new StringBuilder();

                //Retrieve checked build definition count.
                checkedBuildDefinitionCount = GetCheckedBuildDefinitionCount();

                //No build definitions checked.
                if (checkedBuildDefinitionCount == 0)
                {
                    this.BuildDefinitionSearchToolTip.Show(ContextMenuMessage_NoBuildDefinitionsChecked, this.CheckSplitButton, 4000);
                    return;
                }

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    if (dataGridViewRow.Visible == true)
                    {
                        buildDefinitionSelector = (BuildDefinitionSelector)dataGridViewRow.DataBoundItem;
                        if (buildDefinitionSelector.Selected == true)
                        {

                            buildDefinitionListStringBuilder.AppendLine(string.Format("{0}",
                                                                                  buildDefinitionSelector.Name));
                        }
                    }
                }

                Clipboard.SetDataObject(buildDefinitionListStringBuilder.ToString());

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: View build definition tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewBuildDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelector buildDefinitionSelector = null;
            ProcessStartInfo processStartInfo = null;
            string buildSummaryAddress = string.Empty;
            string teamProjectCollectionUri = string.Empty;
            int checkedBuildDefinitionCount = 0;

            try
            {
                teamProjectCollectionUri = this.BuildDefinitionSearchPresenter.TeamProjectInfo.TeamProjectCollectionUri.ToString();
                if (teamProjectCollectionUri.EndsWith("/") == false)
                {
                    teamProjectCollectionUri += @"/";
                }

                //Retrieve checked build definition count.
                checkedBuildDefinitionCount = GetCheckedBuildDefinitionCount();

                //Prompt user to confirm when the checked build definition count is large.
                if (checkedBuildDefinitionCount > ConfirmOperation_MaxBuildDefinitionCount)
                {
                    if (ConfirmOperationLargeBuildDefinitionCount(checkedBuildDefinitionCount) == false)
                    {
                        return;
                    }
                }

                //No build definitions checked.
                if (checkedBuildDefinitionCount == 0)
                {
                    this.BuildDefinitionSearchToolTip.Show(ContextMenuMessage_NoBuildDefinitionsChecked, this.CheckSplitButton, 4000);
                    return;
                }

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    if (dataGridViewRow.Visible == true)
                    {
                        buildDefinitionSelector = (BuildDefinitionSelector)dataGridViewRow.DataBoundItem;
                        if (buildDefinitionSelector.Selected == true)
                        {
                            buildSummaryAddress = string.Format("{0}DefaultCollection/{1}/_build?_a=completed&definitionId={2}",
                                                            teamProjectCollectionUri,
                                                            this.BuildDefinitionSearchPresenter.TeamProjectInfo.TeamProjectName,
                                                            buildDefinitionSelector.Id);

                            processStartInfo = new ProcessStartInfo(buildSummaryAddress);
                            Process.Start(processStartInfo);
                        }
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Check selected build definition tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckSelectedBuildDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                ResetBuildDefinitionCheckToolStripItemCheckedState();
                BuildDefinitionCheckNone();

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.SelectedRows)
                {
                    if (dataGridViewRow.Visible == true)
                    {
                        dataGridViewRow.Cells[BuildDefinitionDataGridViewColumn_Check].Value = true;
                    }
                }
                this.RaiseBuildDefinitionSelectionChangedEvent();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Hanlder: Request build build definition tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestBuildBuildDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelector buildDefinitionSelector = null;
            List<string> buildDefinitionNameList = null;

            try
            {
                buildDefinitionNameList = new List<string>();

                foreach (DataGridViewRow dataGridViewRow in this.BuildDefinitionListDataGridView.Rows)
                {
                    if (dataGridViewRow.Visible == true)
                    {
                        buildDefinitionSelector = (BuildDefinitionSelector)dataGridViewRow.DataBoundItem;
                        if (buildDefinitionSelector.Selected == true)
                        {
                            buildDefinitionNameList.Add(buildDefinitionSelector.Name);
                        }
                    }
                }

                //No build definitions checked.
                if (buildDefinitionNameList.Count == 0)
                {
                    this.BuildDefinitionSearchToolTip.Show(ContextMenuMessage_NoBuildDefinitionsChecked, this.CheckSplitButton, 4000);
                    return;
                }

                this.SidekickCoaction.RequestBuild(buildDefinitionNameList);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Hanlder: Build definition search tool tip - Draw.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionSearchToolTip_Draw(object sender, DrawToolTipEventArgs e)
        {
            try
            {
                e.DrawBackground();
                e.DrawBorder();
                e.DrawText();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: View all  tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.BuildDefinitionListDataGridView.Columns[BuildDefinitionDataGridViewColumn_Trigger].Visible = true;
                this.BuildDefinitionListDataGridView.Columns[BuildDefinitionDataGridViewColumn_QueueName].Visible = true;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: View basic tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewBasicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.BuildDefinitionListDataGridView.Columns[BuildDefinitionDataGridViewColumn_Trigger].Visible = false;
                this.BuildDefinitionListDataGridView.Columns[BuildDefinitionDataGridViewColumn_QueueName].Visible = false;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: View default queue tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewDefaultQueueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.BuildDefinitionListDataGridView.Columns[BuildDefinitionDataGridViewColumn_Trigger].Visible = false;
                this.BuildDefinitionListDataGridView.Columns[BuildDefinitionDataGridViewColumn_QueueName].Visible = true;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: View schedule tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewScheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.BuildDefinitionListDataGridView.Columns[BuildDefinitionDataGridViewColumn_Trigger].Visible = true;
                this.BuildDefinitionListDataGridView.Columns[BuildDefinitionDataGridViewColumn_QueueName].Visible = false;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Build definitionlist data grid view - Sorted.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildDefinitionListDataGridView_Sorted(object sender, EventArgs e)
        {
            List<string> buildDefinitionTagMatchList = null;

            try
            {
                buildDefinitionTagMatchList = this.GetBuildDefinitionDisplayTagMatchList();
                if (buildDefinitionTagMatchList == null ||
                    buildDefinitionTagMatchList.Any() == false)
                {
                    this.BuildDefinitionDisplayAll();
                }
                else
                {
                    this.ToggleDisplayBuildDefinitionsWithMatchingTag(buildDefinitionTagMatchList);
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }


        #endregion

    }
}
