﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.Vso.Build.Client;
using Aclara.Vso.Build.Client.Events;
using Aclara.Vso.Build.Client.Models;
using Aclara.Vso.Build.Client.Types;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using Microsoft.TeamFoundation.SourceControl.WebApi;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;


namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build definition: export import presenter.
    /// </summary>
    public class DefinitionExportImportPresenter
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private CustomLogger _logger = null;
        private SidekickConfiguration _sidekickConfiguration = null;
        private IDefinitionExportImportView _definitionExportImportView;
        private BuildDefinitionManager _buildDefinitionManager;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;
        private ITeamProjectInfo _teamProjectInfo = null;

        #endregion

        #region Public Delegates

        public event EventHandler<BackgroundTaskStateChangedEventArgs> BackgroundTaskStateChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.DefinitionExportImportView.SidekickName,
                                               this.DefinitionExportImportView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Definiton export/import view.
        /// </summary>
        public IDefinitionExportImportView DefinitionExportImportView
        {
            get { return _definitionExportImportView; }
            set { _definitionExportImportView = value; }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Build definition manager.
        /// </summary>
        public BuildDefinitionManager BuildDefinitionManager
        {
            get
            {
                if (_buildDefinitionManager == null)
                {
                    _buildDefinitionManager  = this.CreateBuildDefinitionManager();
                }
                return _buildDefinitionManager;
            }
            set { _buildDefinitionManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamPrjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="ExportImportView"></param>
        public DefinitionExportImportPresenter(ITeamProjectInfo teamPrjectInfo,
                                                         SidekickConfiguration sidekickConfiguration,
                                                         IDefinitionExportImportView ExportImportView)
        {
            this.TeamProjectInfo = teamPrjectInfo;
            this.SidekickConfiguration = sidekickConfiguration;
            this.DefinitionExportImportView = ExportImportView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private DefinitionExportImportPresenter()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalBuildDuration;
            AggregateException aggregateException = null;
            Exception innerException = null;
            BackgroundTaskStateChangedEventArgs backgroundTaskStateChangedEventArgs = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalBuildDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.DefinitionExportImportView.BackgroundTaskCompleted("Definition export/import cancelled.");
                        if (BackgroundTaskStateChanged != null)
                        {
                            backgroundTaskStateChangedEventArgs = new BackgroundTaskStateChangedEventArgs();
                            BackgroundTaskStateChanged(this, backgroundTaskStateChangedEventArgs);
                        }

                        Logger.Info(string.Format("Definition export/import cancelled."));


                        //Log background task completed log entry.
                        Logger.Info(string.Format("[*] Definition export/import completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                   totalBuildDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.DefinitionExportImportView.BackgroundTaskCompleted("");
                        if (BackgroundTaskStateChanged != null)
                        {
                            backgroundTaskStateChangedEventArgs = new BackgroundTaskStateChangedEventArgs();
                            BackgroundTaskStateChanged(this, backgroundTaskStateChangedEventArgs);
                        }

                        //Log background task completed log entry.

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    //Handle operation canceled exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format("Operation cancelled."));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format("Operation cancelled."));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format("Request failed."));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format("Request failed."));
                                    }

                                }
                            }
                        }

                        Logger.Info(string.Format("[*] Definition export/import completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                   totalBuildDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.DefinitionExportImportView.BackgroundTaskCompleted("Definition export/import request completed.");
                        if (BackgroundTaskStateChanged != null)
                        {
                            backgroundTaskStateChangedEventArgs = new BackgroundTaskStateChangedEventArgs();
                            BackgroundTaskStateChanged(this, backgroundTaskStateChangedEventArgs);
                        }

                        //Log background task completed log entry.
                        Logger.Info(string.Format("[*] Definition export/import completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                   totalBuildDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Export build definitions.
        /// </summary>
        /// <param name="buildDefinitionSelectorList"></param>
        public void ExportBuildDefinitions(BuildDefinitionSelectorList buildDefinitionSelectorList)
        {

            List<int> filteredBuidDefinitionIdList = null;
            CancellationToken cancellationToken;
            BackgroundTaskStateChangedEventArgs backgroundTaskStateChangedEventArgs = null;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.DefinitionExportImportView.BackgroundTaskStatus = "Cancelling export build definition request...";
                    if (BackgroundTaskStateChanged != null)
                    {
                        backgroundTaskStateChangedEventArgs = new BackgroundTaskStateChangedEventArgs();
                        BackgroundTaskStateChanged(this, backgroundTaskStateChangedEventArgs);
                    }

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.DefinitionExportImportView.BackgroundTaskStatus = "Export build definition requested...";
                    if (BackgroundTaskStateChanged != null)
                    {
                        backgroundTaskStateChangedEventArgs = new BackgroundTaskStateChangedEventArgs();
                        BackgroundTaskStateChanged(this, backgroundTaskStateChangedEventArgs);
                    }

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    //Reterieve fresh build definition list. (In case build definitions have changed since last retrieval).
                    filteredBuidDefinitionIdList = buildDefinitionSelectorList.GetBuildDefinitionIdList();

                    this.BuildDefinitionManager.BuildDefinitionExported += OnBuildDefinitionExported;
                    this.BuildDefinitionManager.BuildDefinitionExportCompleted += OnBuildDefinitionExportCompleted;

                    this.BackgroundTask = new Task(() => this.BuildDefinitionManager.ExportBuildDefinitions(filteredBuidDefinitionIdList,
                                                                                                                       cancellationToken),
                                                                                                                       cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Import build definitions.
        /// </summary>
        /// <param name="definitionImportList"></param>
        public void ImportBuildDefinitions(DefinitionImportList definitionImportList, 
                                           BuildDefinitionCloneProperties buildDefinitionCloneProperties)
        {

            CancellationToken cancellationToken;
            BackgroundTaskStateChangedEventArgs backgroundTaskStateChangedEventArgs = null;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.DefinitionExportImportView.BackgroundTaskStatus = "Cancelling import build definition request...";
                    if (BackgroundTaskStateChanged != null)
                    {
                        backgroundTaskStateChangedEventArgs = new BackgroundTaskStateChangedEventArgs();
                        BackgroundTaskStateChanged(this, backgroundTaskStateChangedEventArgs);
                    }

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.DefinitionExportImportView.BackgroundTaskStatus = "Import build definition requested...";
                    if (BackgroundTaskStateChanged != null)
                    {
                        backgroundTaskStateChangedEventArgs = new BackgroundTaskStateChangedEventArgs();
                        BackgroundTaskStateChanged(this, backgroundTaskStateChangedEventArgs);
                    }

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.BuildDefinitionManager.BuildDefinitionImported += OnBuildDefinitionImported;
                    this.BuildDefinitionManager.BuildDefinitionImportCompleted += OnBuildDefinitionImportCompleted;

                    this.BackgroundTask = new Task(() => this.BuildDefinitionManager.ImportBuildDefinitions(definitionImportList,
                                                                                                            buildDefinitionCloneProperties,
                                                                                                                       cancellationToken),
                                                                                                                       cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Retrieve list of Git repository selectors.
        /// </summary>
        /// <returns></returns>
        public GitRepositorySelectorList GetGitRepositorySelectorList()
        {
            GitRepositorySelectorList result = null;
            List<GitRepository> gitRepositoryList = null;
            GitRepositorySelector gitRepositorySelector = null;
            try
            {
                result = new GitRepositorySelectorList();

                gitRepositoryList = this.BuildDefinitionManager.GetGitRepositoriesRestApi();

                foreach (GitRepository gitRepository in gitRepositoryList)
                {
                    gitRepositorySelector = new GitRepositorySelector(gitRepository);

                    result.Add(gitRepositorySelector);
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            this.BuildDefinitionManager = null;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create build definition manager.
        /// </summary>
        /// <returns></returns>
        protected BuildDefinitionManager CreateBuildDefinitionManager()
        {
            BuildDefinitionManager result = null;

            try
            {
                result = BuildDefinitionManagerFactory.CreateBuildDefinitionManager(this.TeamProjectInfo.TeamProjectCollectionUri,
                                                                                    this.TeamProjectInfo.TeamProjectCollectionVsrmUri,
                                                                                    this.TeamProjectInfo.TeamProjectName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIUserProfileName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIPassword);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: Build definition exported.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="exportBuildDefinitionEventArgs"></param>
        protected void OnBuildDefinitionExported(Object sender,
                                                 ExportBuildDefinitionEventArgs exportBuildDefinitionEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Build definition details available and status list details available.
                if (exportBuildDefinitionEventArgs.BuildDefinition != null &&
                    exportBuildDefinitionEventArgs.StatusList != null &&
                    exportBuildDefinitionEventArgs.StatusList.Count > 0)
                {
                    message = string.Format("Export build definition request may have been canceled. (Build definition name: {0}; Status --> {1})",
                                             exportBuildDefinitionEventArgs.BuildDefinition.Name,
                                             exportBuildDefinitionEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    if (exportBuildDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error) == true)
                    {
                        Logger.Error(message);

                    }
                    else if (exportBuildDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Warning) == true)
                    {
                        Logger.Warn(message);
                    }
                    else
                    {
                        Logger.Info(message);

                    }
                }
                //Build definition details available and status list is unavailable.
                else if (exportBuildDefinitionEventArgs.BuildDefinition != null)
                {
                    message = string.Format("Build definition added to zip archive. (Build definition name: {0})",
                                             exportBuildDefinitionEventArgs.BuildDefinition.Name);
                    Logger.Info(message);
                }
                //Build definiton details unavailable.
                else
                {
                    message = string.Format("Export build definition. Details unavailable.");
                    Logger.Info(message);
                }

                this.DefinitionExportImportView.UpdateProgressExportImportStatus(exportBuildDefinitionEventArgs.BuildDefinitionAddedToZipArchiveCount, 
                                                                       exportBuildDefinitionEventArgs.BuildDefinitionTotalCount);
            }
            catch (Exception)
            {
                throw;
            }


        }

        /// <summary>
        /// Event Hanlder: Build definition export completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="exportBuildDefinitionEventArgs"></param>
        protected void OnBuildDefinitionExportCompleted(Object sender,
                                                        ExportBuildDefinitionEventArgs exportBuildDefinitionEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Build definition details available and status list details available.
                if (exportBuildDefinitionEventArgs.BuildDefinition != null &&
                    exportBuildDefinitionEventArgs.StatusList != null &&
                    exportBuildDefinitionEventArgs.StatusList.Count > 0)
                {
                    message = string.Format("Export build definition request may have been canceled. (Build definition name: {0}; Status --> {1})",
                                             exportBuildDefinitionEventArgs.BuildDefinition.Name,
                                             exportBuildDefinitionEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    if (exportBuildDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error) == true)
                    {
                        Logger.Error(message);

                    }
                    else if (exportBuildDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Warning) == true)
                    {
                        Logger.Warn(message);
                    }
                    else
                    {
                        Logger.Info(message);

                    }
                }
                //Build definition details available and status list is unavailable.
                else if (exportBuildDefinitionEventArgs.ZipArchiveMemoryStream != null)
                {
                    message = string.Format("Export build definitions added to zip archive completed.");
                    Logger.Info(message);

                    this.DefinitionExportImportView.UpdateExportZipArchiveMemoryStream(exportBuildDefinitionEventArgs.ZipArchiveMemoryStream);
                }
                //Build definiton details unavailable.
                else
                {
                    message = string.Format("Export build definition. Details unavailable.");
                    Logger.Info(message);
                }

                this.DefinitionExportImportView.UpdateProgressExportImportStatus(exportBuildDefinitionEventArgs.BuildDefinitionAddedToZipArchiveCount,
                                                                       exportBuildDefinitionEventArgs.BuildDefinitionTotalCount);
            }
            catch (Exception)
            {
                throw;
            }


        }

        /// <summary>
        /// Event Hanlder: Build definition imported.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="importBuildDefinitionEventArgs"></param>
        protected void OnBuildDefinitionImported(Object sender,
                                                 ImportBuildDefinitionEventArgs importBuildDefinitionEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Build definition details available and status list details available.
                if (importBuildDefinitionEventArgs.BuildDefinition != null &&
                    importBuildDefinitionEventArgs.StatusList != null &&
                    importBuildDefinitionEventArgs.StatusList.Count > 0)
                {
                    message = string.Format("Import build definition request may have been canceled. (Build definition name: {0}; Status --> {1})",
                                             importBuildDefinitionEventArgs.BuildDefinition.Name,
                                             importBuildDefinitionEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    if (importBuildDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error) == true)
                    {
                        Logger.Error(message);

                    }
                    else if (importBuildDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Warning) == true)
                    {
                        Logger.Warn(message);
                    }
                    else
                    {
                        Logger.Info(message);

                    }
                }
                //Build definition details available and status list is unavailable.
                else if (importBuildDefinitionEventArgs.BuildDefinition != null)
                {
                    message = string.Format("Build definition imported. (Build definition name: {0})",
                                             importBuildDefinitionEventArgs.BuildDefinition.Name);
                    Logger.Info(message);
                }
                //Build definiton details unavailable.
                else
                {
                    message = string.Format("Import build definition. Details unavailable.");
                    Logger.Info(message);
                }

                this.DefinitionExportImportView.UpdateProgressExportImportStatus(importBuildDefinitionEventArgs.BuildDefinitionImportedCount,
                                                                       importBuildDefinitionEventArgs.BuildDefinitionTotalCount);
            }
            catch (Exception)
            {
                throw;
            }


        }

        /// <summary>
        /// Event Hanlder: Build definition import completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="importBuildDefinitionEventArgs"></param>
        protected void OnBuildDefinitionImportCompleted(Object sender,
                                                        ImportBuildDefinitionEventArgs importBuildDefinitionEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Build definition details available and status list details available.
                if (importBuildDefinitionEventArgs.BuildDefinition != null &&
                    importBuildDefinitionEventArgs.StatusList != null &&
                    importBuildDefinitionEventArgs.StatusList.Count > 0)
                {
                    message = string.Format("Import build definition request may have been canceled. (Build definition name: {0}; Status --> {1})",
                                             importBuildDefinitionEventArgs.BuildDefinition.Name,
                                             importBuildDefinitionEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    if (importBuildDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error) == true)
                    {
                        Logger.Error(message);

                    }
                    else if (importBuildDefinitionEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Warning) == true)
                    {
                        Logger.Warn(message);
                    }
                    else
                    {
                        Logger.Info(message);

                    }
                }
                ////Build definition details available and status list is unavailable.
                //else if (importBuildDefinitionEventArgs.ZipArchiveMemoryStream != null)
                //{
                //    message = string.Format("Import build definition completed.");
                //    Logger.Info(message);

                //}
                //Build definiton details unavailable.
                else
                {
                    message = string.Format("Import build definition. Details unavailable.");
                    Logger.Info(message);
                }

                this.DefinitionExportImportView.UpdateProgressExportImportStatus(importBuildDefinitionEventArgs.BuildDefinitionImportedCount,
                                                                                 importBuildDefinitionEventArgs.BuildDefinitionTotalCount);
            }
            catch (Exception)
            {
                throw;
            }


        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                        }
                        else
                        {
                            Logger.Error(string.Format("Operation cancelled.(Unobserved)"));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                }
                else
                {
                    Logger.Error(string.Format("Operation cancelled.(Unobserved)"));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
