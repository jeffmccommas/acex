﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using System;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Build definition search form factory.
    /// </summary>
    static public class BuildDefinitionSearchFormFactory
    {

        /// <summary>
        /// Create build definition search form.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="sidekickCoaction"></param>
        /// <returns></returns>
        static public BuildDefinitionSearchForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                           SidekickConfiguration sidekickConfiguration,
                                                          ISidekickCoaction sidekickCoaction)
        {

            BuildDefinitionSearchForm result = null;
            BuildDefinitionSearchPresenter buildDefinitionSearchPresenter = null;

            try
            {

                buildDefinitionSearchPresenter = new BuildDefinitionSearchPresenter(teamProjectInfo, sidekickConfiguration, result);
                result = new BuildDefinitionSearchForm(buildDefinitionSearchPresenter, sidekickConfiguration, sidekickCoaction);

                result.BuildDefinitionSearchPresenter = buildDefinitionSearchPresenter;
                buildDefinitionSearchPresenter.BuildDefinitionSearchView = result;
                result.TopLevel = false;

                buildDefinitionSearchPresenter.Initialize();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
