﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using System;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build definition clone form factory.
    /// </summary>
    public static class BuildDefinitionCloneFormFactory
    {

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <returns></returns>
        public static BuildDefinitionCloneForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                          SidekickConfiguration sidekickConfiguration,
                                                          BuildDefinitionSearchForm buildDefinitionSearchForm)
        {

            BuildDefinitionCloneForm result = null;
            BuildDefinitionClonePresenter buildDefinitionClonePresenter = null;

            try
            {
                result = new BuildDefinitionCloneForm(sidekickConfiguration, buildDefinitionSearchForm);
                buildDefinitionClonePresenter = new BuildDefinitionClonePresenter(teamProjectInfo, sidekickConfiguration, result);
                result.BuildDefinitionClonePresenter = buildDefinitionClonePresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


    }
}
