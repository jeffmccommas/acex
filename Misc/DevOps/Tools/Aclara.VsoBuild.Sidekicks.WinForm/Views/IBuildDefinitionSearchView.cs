﻿using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoBuild.Sidekicks.WinForm
{
    /// <summary>
    /// Build definition search view - interface.
    /// </summary>
    public interface IBuildDefinitionSearchView
    {

        string BuildDefinitionNamePattern { get; set; }

        void EnableControlsDependantOnTeamProject(bool enabled);

        void EnableControlsDependantOnSearch(bool enabled);

        void PerformSearch();

        void ClearSearch();

        void TeamProjectChanged();

        string BackgroundTaskStatus { get; set; }

        void UpdateBuildDefinitionRetrievalStatus(int buildDefinitionListRetrievedCount, int buildDefinitionListTotalCount, bool cancelled);

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);

        BuildDefinitionSelectorList BuildDefinitionSelectorList { get; set; }

        BuildDefinitionSelectorList GetSelectedBuildDefinitionSelectorList();
        
        int GetSelectedBuildDefinitionSelectorCount();

        List<String> GetSelectedBuildDefinitionSelectorListBranchNameList();
    }
}
