﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.Vso.Build.Client;
using Aclara.Vso.Build.Client.Events;
using Aclara.Vso.Build.Client.Models;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using Aclara.VsoBuild.Sidekicks.WinForm.Utility;
using Microsoft.TeamFoundation.Build.WebApi;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build definition: change settings presenter.
    /// </summary>
    public class BuildDefinitionChangeSettingsPresenter
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private CustomLogger _logger = null;
        private SidekickConfiguration _sidekickConfiguration = null;
        private IBuildDefinitionChangeSettingsView _BuildDefinitionChangeSettingsView;
        private BuildDefinitionManager _buildDefinitionManager;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;
        private ITeamProjectInfo _teamProjectInfo = null;
        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.BuildDefinitionChangeSettingsView.SidekickName,
                                               this.BuildDefinitionChangeSettingsView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Change build definiton settings view.
        /// </summary>
        public IBuildDefinitionChangeSettingsView BuildDefinitionChangeSettingsView
        {
            get { return _BuildDefinitionChangeSettingsView; }
            set { _BuildDefinitionChangeSettingsView = value; }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Build definition manager.
        /// </summary>
        public BuildDefinitionManager BuildDefinitionManager
        {
            get { return _buildDefinitionManager; }
            set { _buildDefinitionManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamPrjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="BuildDefinitionChangeSettingsView"></param>
        public BuildDefinitionChangeSettingsPresenter(ITeamProjectInfo teamPrjectInfo,
                                                      SidekickConfiguration sidekickConfiguration,
                                                      IBuildDefinitionChangeSettingsView BuildDefinitionChangeSettingsView)
        {
            this.TeamProjectInfo = teamPrjectInfo;
            this.SidekickConfiguration = sidekickConfiguration;
            this.BuildDefinitionChangeSettingsView = BuildDefinitionChangeSettingsView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private BuildDefinitionChangeSettingsPresenter()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve default agent queue selector list.
        /// </summary>
        /// <returns></returns>
        public AgentPoolQueueSelectorList GetDefaultAgentQueueSelectors()
        {

            AgentPoolQueueSelectorList result = null;
            List<AgentPoolQueue> agentPoolQueueList = null;
            AgentPoolQueueSelector agentPoolQueueSelector = null;

            try
            {
                result = new AgentPoolQueueSelectorList();

                this.BuildDefinitionManager = this.CreateBuildDefinitionManager();

                agentPoolQueueList = this.BuildDefinitionManager.GetAgentPoolQueuesRestApi();

                //No default agent pool queues.
                if (agentPoolQueueList == null)
                {
                    return result;
                }

                foreach (AgentPoolQueue agentPoolQueue in agentPoolQueueList)
                {
                    agentPoolQueueSelector = new AgentPoolQueueSelector(agentPoolQueue);

                    result.Add(agentPoolQueueSelector);

                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalBuildDuration;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalBuildDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.BuildDefinitionChangeSettingsView.BackgroundTaskCompleted("Change build definition settings canceled.");
                        this.BuildDefinitionChangeSettingsView.ApplyButtonEnable(true);

                        Logger.Info(string.Format("Change build definition settings cancelled."));


                        //Log background task completed log entry.
                        Logger.Info(string.Format("[*] Change build definition settings completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                   totalBuildDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.BuildDefinitionChangeSettingsView.BackgroundTaskCompleted("");
                        this.BuildDefinitionChangeSettingsView.ApplyButtonEnable(true);


                        //Log background task completed log entry.

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    //Handle operation canceled exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format("Operation cancelled."));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format("Operation cancelled."));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format("Request failed."));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format("Request failed."));
                                    }

                                }
                            }
                        }

                        Logger.Info(string.Format("[*] Change build definition settings completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                   totalBuildDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.BuildDefinitionChangeSettingsView.BackgroundTaskCompleted("Change build definition settings request completed.");
                        this.BuildDefinitionChangeSettingsView.ApplyButtonEnable(true);

                        //Log background task completed log entry.
                        Logger.Info(string.Format("[*] Change build definition settings completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                   totalBuildDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Change build definitions settings.
        /// </summary>
        /// <param name="buildDefinitionSelectorList"></param>
        /// <param name="buildDefinitionSettings"></param>
        public void ChangeBuildDefinitionsSettings(BuildDefinitionSelectorList buildDefinitionSelectorList,
                                                   BuildDefinitionSettings buildDefinitionSettings)
        {

            List<int> filteredBuidDefinitionIdList = null;
            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.BuildDefinitionChangeSettingsView.BackgroundTaskStatus = "Cancelling change build definition settings request...";
                    this.BuildDefinitionChangeSettingsView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.BuildDefinitionChangeSettingsView.BackgroundTaskStatus = "Change build definition settings(s) requested...";
                    this.BuildDefinitionChangeSettingsView.ApplyButtonText = "Cancel";

                    this.BuildDefinitionManager = this.CreateBuildDefinitionManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    //Reterieve fresh build definition list. (In case build definitions have changed since last retrieval).
                    filteredBuidDefinitionIdList = buildDefinitionSelectorList.GetBuildDefinitionIdList();

                    this.BuildDefinitionManager.BuildDefinitionSettingsChanged += OnBuildDefinitionSettingsChanged;

                    this.BackgroundTask = new Task(() => this.BuildDefinitionManager.ChangeBuildDefinitionsSettings(filteredBuidDefinitionIdList,
                                                                                                                    buildDefinitionSettings,
                                                                                                                    cancellationToken),
                                                                                                                       cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create build definition manager.
        /// </summary>
        /// <returns></returns>
        protected BuildDefinitionManager CreateBuildDefinitionManager()
        {
            BuildDefinitionManager result = null;

            try
            {
                result = BuildDefinitionManagerFactory.CreateBuildDefinitionManager(this.TeamProjectInfo.TeamProjectCollectionUri,
                                                                                    this.TeamProjectInfo.TeamProjectCollectionVsrmUri,
                                                                                    this.TeamProjectInfo.TeamProjectName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIUserProfileName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIPassword);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: Build definition settings changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="changeBuildDefinitionSettingsEventArgs"></param>
        protected void OnBuildDefinitionSettingsChanged(Object sender,
                                               ChangeBuildDefinitionSettingsEventArgs changeBuildDefinitionSettingsEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Build definition details available and status list details available.
                if (changeBuildDefinitionSettingsEventArgs.BuildDefinition != null &&
                    changeBuildDefinitionSettingsEventArgs.StatusList != null &&
                    changeBuildDefinitionSettingsEventArgs.StatusList.Count > 0)
                {
                    message = string.Format("Change build definition settings request may have been canceled. (Build definition name: {0}; Status --> {1})",
                                             changeBuildDefinitionSettingsEventArgs.BuildDefinition.Name,
                                             changeBuildDefinitionSettingsEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    if (changeBuildDefinitionSettingsEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error) == true)
                    {
                        Logger.Error(message);

                    }
                    else if (changeBuildDefinitionSettingsEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Warning) == true)
                    {
                        Logger.Warn(message);
                    }
                    else
                    {
                        Logger.Info(message);

                    }
                }
                //Build definition details available and status list is unavailable.
                else if (changeBuildDefinitionSettingsEventArgs.BuildDefinition != null)
                {
                    message = string.Format("Build definition settings changed. (Build definition name: {0}, Queue: {1}, Trigger: {2})",
                                             changeBuildDefinitionSettingsEventArgs.BuildDefinition.Name,
                                             changeBuildDefinitionSettingsEventArgs.BuildDefinition.Queue,
                                             ConvertToText.GetFormattedScheduleFromBuildDefinition(changeBuildDefinitionSettingsEventArgs.BuildDefinition));
                    Logger.Info(message);
                }
                //Build definiton details unavailable.
                else
                {
                    message = string.Format("Build definition settings changed. Details unavailable.");
                    Logger.Info(message);
                }
                this.BuildDefinitionChangeSettingsView.UpdateProgressChangeSetting(changeBuildDefinitionSettingsEventArgs.BuildDefinitionChangedCount,
                                                                                   changeBuildDefinitionSettingsEventArgs.BuildDefinitionTotalCount);

            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                        }
                        else
                        {
                            Logger.Error(string.Format("Operation cancelled.(Unobserved)"));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                }
                else
                {
                    Logger.Error(string.Format("Operation cancelled.(Unobserved)"));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

    }

    #endregion

    #region Private Methods

    #endregion

}
