﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.Vso.Build.Client;
using Aclara.Vso.Build.Client.Events;
using Aclara.Vso.Build.Client.Models;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build stratus check - presenter.
    /// </summary>
    public class BuildStatusCheckPresenter
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private CustomLogger _logger = null;
        private BuildList _buildList;
        private SidekickConfiguration _sidekickConfiguration = null;
        private IBuildStatusCheckView _buildStatusCheckView;
        private BuildDefinitionManager _buildDefinitionManager;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;
        private ITeamProjectInfo _teamProjectInfo = null;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.BuildStatusCheckView.SidekickName,
                                               this.BuildStatusCheckView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Build list.
        /// </summary>
        public BuildList BuildList
        {
            get { return _buildList; }
            set { _buildList = value; }
        }


        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Build status check view.
        /// </summary>
        public IBuildStatusCheckView BuildStatusCheckView
        {
            get { return _buildStatusCheckView; }
            set { _buildStatusCheckView = value; }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Build definition manager.
        /// </summary>
        public BuildDefinitionManager BuildDefinitionManager
        {
            get { return _buildDefinitionManager; }
            set { _buildDefinitionManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamPrjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="BuildStatusCheckView"></param>
        public BuildStatusCheckPresenter(ITeamProjectInfo teamPrjectInfo,
                                             SidekickConfiguration sidekickConfiguration,
                                             IBuildStatusCheckView BuildStatusCheckView)
        {
            this.TeamProjectInfo = teamPrjectInfo;
            this.SidekickConfiguration = sidekickConfiguration;
            this.BuildStatusCheckView = BuildStatusCheckView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private BuildStatusCheckPresenter()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalBuildDuration;
            AggregateException aggregateException = null;
            Exception innerException = null;
            string statusMessage = string.Empty;
            int buildListRetrievedCount = 0;
            int buildListTotalCount = 0;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalBuildDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.BuildStatusCheckView.BackgroundTaskCancelled("Get builds request cancelled.");
                        this.BuildStatusCheckView.BackgroundTaskStatus = "Get builds reqeust cancelled.";
                        this.BuildStatusCheckView.RefreshButtonEnable(true);

                        this.Logger.Info(string.Format("Get builds request cancelled."));

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format("[*] Get builds request cancelled. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                        totalBuildDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.BuildStatusCheckView.BackgroundTaskCancelled(string.Empty);
                        this.BuildStatusCheckView.RefreshButtonEnable(true);


                        //Log background task completed log entry.

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format("Operation cancelled."));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format("Operation cancelled."));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format("Request failed."));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format("Request failed."));
                                    }

                                }
                            }
                        }

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format("[*] Get builds request faulted. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                        totalBuildDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.BuildStatusCheckView.BackgroundTaskCompleted(string.Format("Get builds request completed {0:yyyy/MM/dd HH:mm:ss}.",
                                                                                        DateTime.Now));
                        this.BuildStatusCheckView.BuildList = this.BuildList;
                        this.BuildStatusCheckView.PopulateBuildStatus();
                        this.BuildStatusCheckView.RefreshButtonEnable(true);

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Deterimine if background task is running.
        /// </summary>
        public bool IsBackgroundTaskRunning()
        {
            bool result = false;

            try
            {
                if (this.BackgroundTask != null &&
                    this.BackgroundTask.Status == TaskStatus.Running)
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Cancel background task.
        /// </summary>
        public void CancelBackgroundTask()
        {
            try
            {
                if (this.BackgroundTask != null &&
                    this.BackgroundTask.Status == TaskStatus.Running)
                {
                    this.BackgroundTaskCancellationTokenSource.Cancel();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get builds.
        /// </summary>
        /// <param name="buildDefinitionSelectorList"></param>
        /// <param name="dateFilter"></param>
        /// <param name="top"></param>
        /// <param name="recentlyCompletedInHours"
        /// <param name="queuedBuildDefinitions"></param>
        public void GetBuildsRestApi(BuildDefinitionSelectorList buildDefinitionSelectorList,
                                     DateFilter dateFilter,
                                     int top,
                                     int recentlyCompletedInHours,
                                     bool queuedBuildDefinitions)
        {

            List<int> filteredBuildDefinitionIdList = null;
            CancellationToken cancellationToken;

            try
            {

                //Attempting to start task that is already running.
                if (this.BackgroundTask != null &&
                    this.BackgroundTask.Status == TaskStatus.Running)
                {
                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.BuildStatusCheckView.BackgroundTaskStatus = "Get builds requested...";

                    this.BuildDefinitionManager = this.CreateBuildDefinitionManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    //Retrieve build definition id list from build definition selector list.
                    filteredBuildDefinitionIdList = buildDefinitionSelectorList.GetBuildDefinitionIdList();

                    this.BuildDefinitionManager.BuildListRetrieved += OnBuildListRetrieved;

                    this.BackgroundTask = new Task(() => this.BuildDefinitionManager.GetBuildList(filteredBuildDefinitionIdList,
                                                                                                  dateFilter,
                                                                                                  top,
                                                                                                  recentlyCompletedInHours,
                                                                                                  queuedBuildDefinitions,
                                                                                                  cancellationToken),
                                                                                                           cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create build definition manager.
        /// </summary>
        /// <returns></returns>
        protected BuildDefinitionManager CreateBuildDefinitionManager()
        {
            BuildDefinitionManager result = null;

            try
            {
                result = BuildDefinitionManagerFactory.CreateBuildDefinitionManager(this.TeamProjectInfo.TeamProjectCollectionUri,
                                                                                    this.TeamProjectInfo.TeamProjectCollectionVsrmUri, 
                                                                                    this.TeamProjectInfo.TeamProjectName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIUserProfileName,
                                                                                    this.TeamProjectInfo.BasicAuthRestAPIPassword);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: Builds retrieved.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="getBuildListEventArgs"></param>
        protected void OnBuildListRetrieved(Object sender,
                                            GetBuildListEventArgs getBuildListEventArgs)
        {
            string message = string.Empty;
            int buildListRetrievedCount = 0;
            int buildListTotalCount = 0;
            string statusMessage = string.Empty;

            try
            {

                //Build definition details available and status list details available.
                if (getBuildListEventArgs.StatusList != null &&
                    getBuildListEventArgs.StatusList.Count > 0)
                {
                    if (getBuildListEventArgs.BuildDefinition != null)
                    {
                        message = string.Format("Get builds may have been canceled. (Build definition name: {0}; Status --> {1})",
                                                getBuildListEventArgs.BuildDefinition.Name,
                                                getBuildListEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));
                    }
                    else
                    {
                        message = string.Format("Get builds may have been canceled. (Status --> {0})",
                                                getBuildListEventArgs.StatusList.Format(StatusTypes.FormatOption.Minimum));
                    }
                    if (getBuildListEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error) == true)
                    {
                        this.Logger.Error(message);

                    }
                    else if (getBuildListEventArgs.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Warning) == true)
                    {
                        this.Logger.Warn(message);
                    }
                    else
                    {
                        this.Logger.Info(message);
                    }

                }
                //Build definition details available and status list is unavailable.
                else if (getBuildListEventArgs.BuildDefinition != null)
                {

                    if (this.BuildList == null)
                    {
                        this.BuildList = getBuildListEventArgs.BuildList;
                    }
                    else
                    {
                        this.BuildList.AddRange(getBuildListEventArgs.BuildList);
                    }
                    buildListRetrievedCount = getBuildListEventArgs.BuildListRetrievedCount;
                    buildListTotalCount = getBuildListEventArgs.BuildListTotalCount;

                    statusMessage = string.Format("Builds retrieved. (Build definition name: {0})",
                                                     getBuildListEventArgs.BuildDefinition.Name);
                    this.BuildStatusCheckView.UpdateBuildListRetrievalStatus(statusMessage,
                                                                             buildListRetrievedCount,
                                                                             buildListTotalCount);

                }
                //Queued build definitions.
                else if (getBuildListEventArgs.QueuedBuildDefinitions == true)
                {

                    if (this.BuildList == null)
                    {
                        this.BuildList = getBuildListEventArgs.BuildList;
                    }
                    else
                    {
                        this.BuildList.AddRange(getBuildListEventArgs.BuildList);
                    }
                    buildListRetrievedCount = getBuildListEventArgs.BuildListRetrievedCount;
                    buildListTotalCount = getBuildListEventArgs.BuildListTotalCount;

                    statusMessage = string.Format("Queued or recently completed builds retrieved.");
                    this.BuildStatusCheckView.UpdateBuildListRetrievalStatus(statusMessage,
                                                                             buildListRetrievedCount,
                                                                             buildListTotalCount);

                }

                //Build definiton details unavailable.
                else
                {
                    this.Logger.Info(string.Format("Builds retrieved. Details unavailable."));
                }

            }
            catch (Exception)
            {
                throw;
            }


        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                        }
                        else
                        {
                            Logger.Error(string.Format("Operation cancelled.(Unobserved)"));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                }
                else
                {
                    Logger.Error(string.Format("Operation cancelled.(Unobserved)"));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
