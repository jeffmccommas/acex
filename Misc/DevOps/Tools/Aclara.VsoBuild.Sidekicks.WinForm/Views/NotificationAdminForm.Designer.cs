﻿namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    partial class NotificationAdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.HelpButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.NotificationSubscriptionPanel = new System.Windows.Forms.Panel();
            this.CreateNotificationSubscriptionPanel = new System.Windows.Forms.Panel();
            this.TagValueLabel = new System.Windows.Forms.Label();
            this.TagLabel = new System.Windows.Forms.Label();
            this.CreateNotificationSubscriptionCancelButton = new System.Windows.Forms.Button();
            this.CreateNotificationSubscriptionOKButton = new System.Windows.Forms.Button();
            this.TeamNameComboBox = new System.Windows.Forms.ComboBox();
            this.TeamNameLabel = new System.Windows.Forms.Label();
            this.EmailAddressesTextBox = new System.Windows.Forms.TextBox();
            this.EmailAddressLabel = new System.Windows.Forms.Label();
            this.StatusPanel = new System.Windows.Forms.Panel();
            this.NotificationSubscriptionOperationResultPanel = new System.Windows.Forms.Panel();
            this.NotificationSubscriptionOperationResultLabel = new System.Windows.Forms.Label();
            this.NotificationSubscriptionOperationProgressPanel = new System.Windows.Forms.Panel();
            this.NotificationSubscriptionOperationProgressBar = new System.Windows.Forms.ProgressBar();
            this.NotificationSubscriptionOperationProgressLabel = new System.Windows.Forms.Label();
            this.NotificationSubscriptionDataGridView = new System.Windows.Forms.DataGridView();
            this.CheckColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.StatusAsTextColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExtraColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ManualRefreshPanel = new System.Windows.Forms.Panel();
            this.FilterOnTagCheckBox = new System.Windows.Forms.CheckBox();
            this.ChangeSplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.NotificationSubscriptionChangeContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.NotificationSubscriptionChangeDisableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NotificationSubscriptionChangeEnableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.CreateButton = new System.Windows.Forms.Button();
            this.CheckSplitButton = new Aclara.VsoBuild.Sidekicks.WinForm.SplitButton();
            this.NotificationSubscriptionCheckContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.NotificationSubscriptionSelectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NotificationSubscriptionSelectNoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.NotificationSubscriptionSelectDisabledToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NotificationSubscriptionSelectEnabledToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.VerticalSplitter = new System.Windows.Forms.Splitter();
            this.BuildDefinitionSearchPanel = new System.Windows.Forms.Panel();
            this.BuildDefinitionSearchPlaceholderLabel = new System.Windows.Forms.Label();
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.PropertiesPanel.SuspendLayout();
            this.NotificationSubscriptionPanel.SuspendLayout();
            this.CreateNotificationSubscriptionPanel.SuspendLayout();
            this.StatusPanel.SuspendLayout();
            this.NotificationSubscriptionOperationResultPanel.SuspendLayout();
            this.NotificationSubscriptionOperationProgressPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NotificationSubscriptionDataGridView)).BeginInit();
            this.ManualRefreshPanel.SuspendLayout();
            this.NotificationSubscriptionChangeContextMenuStrip.SuspendLayout();
            this.NotificationSubscriptionCheckContextMenuStrip.SuspendLayout();
            this.BuildDefinitionSearchPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.HelpButton);
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(726, 22);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(698, 0);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 1;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(0, 1);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(692, 21);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "Notification Admin";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ControlPanel
            // 
            this.ControlPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 440);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(726, 40);
            this.ControlPanel.TabIndex = 3;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PropertiesPanel.Controls.Add(this.splitter1);
            this.PropertiesPanel.Controls.Add(this.NotificationSubscriptionPanel);
            this.PropertiesPanel.Controls.Add(this.ManualRefreshPanel);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesPanel.Location = new System.Drawing.Point(656, 0);
            this.PropertiesPanel.MinimumSize = new System.Drawing.Size(100, 480);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(726, 480);
            this.PropertiesPanel.TabIndex = 8;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 440);
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // NotificationSubscriptionPanel
            // 
            this.NotificationSubscriptionPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NotificationSubscriptionPanel.Controls.Add(this.CreateNotificationSubscriptionPanel);
            this.NotificationSubscriptionPanel.Controls.Add(this.StatusPanel);
            this.NotificationSubscriptionPanel.Controls.Add(this.NotificationSubscriptionDataGridView);
            this.NotificationSubscriptionPanel.Location = new System.Drawing.Point(0, 87);
            this.NotificationSubscriptionPanel.Name = "NotificationSubscriptionPanel";
            this.NotificationSubscriptionPanel.Size = new System.Drawing.Size(726, 353);
            this.NotificationSubscriptionPanel.TabIndex = 2;
            // 
            // CreateNotificationSubscriptionPanel
            // 
            this.CreateNotificationSubscriptionPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateNotificationSubscriptionPanel.Controls.Add(this.TagValueLabel);
            this.CreateNotificationSubscriptionPanel.Controls.Add(this.TagLabel);
            this.CreateNotificationSubscriptionPanel.Controls.Add(this.CreateNotificationSubscriptionCancelButton);
            this.CreateNotificationSubscriptionPanel.Controls.Add(this.CreateNotificationSubscriptionOKButton);
            this.CreateNotificationSubscriptionPanel.Controls.Add(this.TeamNameComboBox);
            this.CreateNotificationSubscriptionPanel.Controls.Add(this.TeamNameLabel);
            this.CreateNotificationSubscriptionPanel.Controls.Add(this.EmailAddressesTextBox);
            this.CreateNotificationSubscriptionPanel.Controls.Add(this.EmailAddressLabel);
            this.CreateNotificationSubscriptionPanel.Location = new System.Drawing.Point(0, 46);
            this.CreateNotificationSubscriptionPanel.Name = "CreateNotificationSubscriptionPanel";
            this.CreateNotificationSubscriptionPanel.Size = new System.Drawing.Size(726, 128);
            this.CreateNotificationSubscriptionPanel.TabIndex = 1;
            // 
            // TagValueLabel
            // 
            this.TagValueLabel.AutoSize = true;
            this.TagValueLabel.Location = new System.Drawing.Point(95, 70);
            this.TagValueLabel.Name = "TagValueLabel";
            this.TagValueLabel.Size = new System.Drawing.Size(0, 13);
            this.TagValueLabel.TabIndex = 4;
            // 
            // TagLabel
            // 
            this.TagLabel.AutoSize = true;
            this.TagLabel.Location = new System.Drawing.Point(1, 70);
            this.TagLabel.Name = "TagLabel";
            this.TagLabel.Size = new System.Drawing.Size(29, 13);
            this.TagLabel.TabIndex = 3;
            this.TagLabel.Text = "Tag:";
            // 
            // CreateNotificationSubscriptionCancelButton
            // 
            this.CreateNotificationSubscriptionCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateNotificationSubscriptionCancelButton.Location = new System.Drawing.Point(640, 95);
            this.CreateNotificationSubscriptionCancelButton.Name = "CreateNotificationSubscriptionCancelButton";
            this.CreateNotificationSubscriptionCancelButton.Size = new System.Drawing.Size(75, 23);
            this.CreateNotificationSubscriptionCancelButton.TabIndex = 6;
            this.CreateNotificationSubscriptionCancelButton.Text = "Cancel";
            this.CreateNotificationSubscriptionCancelButton.UseVisualStyleBackColor = true;
            this.CreateNotificationSubscriptionCancelButton.Click += new System.EventHandler(this.CreateNotificationSubscriptionCancelButton_Click);
            // 
            // CreateNotificationSubscriptionOKButton
            // 
            this.CreateNotificationSubscriptionOKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateNotificationSubscriptionOKButton.Location = new System.Drawing.Point(559, 95);
            this.CreateNotificationSubscriptionOKButton.Name = "CreateNotificationSubscriptionOKButton";
            this.CreateNotificationSubscriptionOKButton.Size = new System.Drawing.Size(75, 23);
            this.CreateNotificationSubscriptionOKButton.TabIndex = 5;
            this.CreateNotificationSubscriptionOKButton.Text = "OK";
            this.CreateNotificationSubscriptionOKButton.UseVisualStyleBackColor = true;
            this.CreateNotificationSubscriptionOKButton.Click += new System.EventHandler(this.CreateNotificationSubscriptionOKButton_Click);
            // 
            // TeamNameComboBox
            // 
            this.TeamNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TeamNameComboBox.FormattingEnabled = true;
            this.TeamNameComboBox.Location = new System.Drawing.Point(95, 9);
            this.TeamNameComboBox.Name = "TeamNameComboBox";
            this.TeamNameComboBox.Size = new System.Drawing.Size(200, 21);
            this.TeamNameComboBox.TabIndex = 1;
            // 
            // TeamNameLabel
            // 
            this.TeamNameLabel.AutoSize = true;
            this.TeamNameLabel.Location = new System.Drawing.Point(1, 13);
            this.TeamNameLabel.Name = "TeamNameLabel";
            this.TeamNameLabel.Size = new System.Drawing.Size(66, 13);
            this.TeamNameLabel.TabIndex = 0;
            this.TeamNameLabel.Text = "Team name:";
            // 
            // EmailAddressesTextBox
            // 
            this.EmailAddressesTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EmailAddressesTextBox.Location = new System.Drawing.Point(95, 39);
            this.EmailAddressesTextBox.Name = "EmailAddressesTextBox";
            this.EmailAddressesTextBox.Size = new System.Drawing.Size(584, 20);
            this.EmailAddressesTextBox.TabIndex = 3;
            // 
            // EmailAddressLabel
            // 
            this.EmailAddressLabel.AutoSize = true;
            this.EmailAddressLabel.Location = new System.Drawing.Point(1, 43);
            this.EmailAddressLabel.Name = "EmailAddressLabel";
            this.EmailAddressLabel.Size = new System.Drawing.Size(86, 13);
            this.EmailAddressLabel.TabIndex = 2;
            this.EmailAddressLabel.Text = "Email addresses:";
            // 
            // StatusPanel
            // 
            this.StatusPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.StatusPanel.BackColor = System.Drawing.Color.White;
            this.StatusPanel.Controls.Add(this.NotificationSubscriptionOperationResultPanel);
            this.StatusPanel.Controls.Add(this.NotificationSubscriptionOperationProgressPanel);
            this.StatusPanel.Location = new System.Drawing.Point(0, 3);
            this.StatusPanel.Name = "StatusPanel";
            this.StatusPanel.Size = new System.Drawing.Size(723, 37);
            this.StatusPanel.TabIndex = 0;
            // 
            // NotificationSubscriptionOperationResultPanel
            // 
            this.NotificationSubscriptionOperationResultPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NotificationSubscriptionOperationResultPanel.BackColor = System.Drawing.Color.Transparent;
            this.NotificationSubscriptionOperationResultPanel.Controls.Add(this.NotificationSubscriptionOperationResultLabel);
            this.NotificationSubscriptionOperationResultPanel.Location = new System.Drawing.Point(3, 3);
            this.NotificationSubscriptionOperationResultPanel.Name = "NotificationSubscriptionOperationResultPanel";
            this.NotificationSubscriptionOperationResultPanel.Size = new System.Drawing.Size(561, 31);
            this.NotificationSubscriptionOperationResultPanel.TabIndex = 0;
            // 
            // NotificationSubscriptionOperationResultLabel
            // 
            this.NotificationSubscriptionOperationResultLabel.AutoSize = true;
            this.NotificationSubscriptionOperationResultLabel.Location = new System.Drawing.Point(3, 10);
            this.NotificationSubscriptionOperationResultLabel.Name = "NotificationSubscriptionOperationResultLabel";
            this.NotificationSubscriptionOperationResultLabel.Size = new System.Drawing.Size(49, 13);
            this.NotificationSubscriptionOperationResultLabel.TabIndex = 0;
            this.NotificationSubscriptionOperationResultLabel.Text = "<results>";
            // 
            // NotificationSubscriptionOperationProgressPanel
            // 
            this.NotificationSubscriptionOperationProgressPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NotificationSubscriptionOperationProgressPanel.Controls.Add(this.NotificationSubscriptionOperationProgressBar);
            this.NotificationSubscriptionOperationProgressPanel.Controls.Add(this.NotificationSubscriptionOperationProgressLabel);
            this.NotificationSubscriptionOperationProgressPanel.Location = new System.Drawing.Point(3, 3);
            this.NotificationSubscriptionOperationProgressPanel.Name = "NotificationSubscriptionOperationProgressPanel";
            this.NotificationSubscriptionOperationProgressPanel.Size = new System.Drawing.Size(561, 31);
            this.NotificationSubscriptionOperationProgressPanel.TabIndex = 1;
            // 
            // NotificationSubscriptionOperationProgressBar
            // 
            this.NotificationSubscriptionOperationProgressBar.Location = new System.Drawing.Point(3, 18);
            this.NotificationSubscriptionOperationProgressBar.Name = "NotificationSubscriptionOperationProgressBar";
            this.NotificationSubscriptionOperationProgressBar.Size = new System.Drawing.Size(100, 10);
            this.NotificationSubscriptionOperationProgressBar.TabIndex = 1;
            // 
            // NotificationSubscriptionOperationProgressLabel
            // 
            this.NotificationSubscriptionOperationProgressLabel.AutoSize = true;
            this.NotificationSubscriptionOperationProgressLabel.Location = new System.Drawing.Point(3, 3);
            this.NotificationSubscriptionOperationProgressLabel.Name = "NotificationSubscriptionOperationProgressLabel";
            this.NotificationSubscriptionOperationProgressLabel.Size = new System.Drawing.Size(59, 13);
            this.NotificationSubscriptionOperationProgressLabel.TabIndex = 0;
            this.NotificationSubscriptionOperationProgressLabel.Text = "<progress>";
            // 
            // NotificationSubscriptionDataGridView
            // 
            this.NotificationSubscriptionDataGridView.AllowUserToAddRows = false;
            this.NotificationSubscriptionDataGridView.AllowUserToDeleteRows = false;
            this.NotificationSubscriptionDataGridView.AllowUserToOrderColumns = true;
            this.NotificationSubscriptionDataGridView.AllowUserToResizeRows = false;
            this.NotificationSubscriptionDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NotificationSubscriptionDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.NotificationSubscriptionDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NotificationSubscriptionDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.NotificationSubscriptionDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CheckColumn,
            this.StatusAsTextColumn,
            this.DescriptionColumn,
            this.ExtraColumn});
            this.NotificationSubscriptionDataGridView.EnableHeadersVisualStyles = false;
            this.NotificationSubscriptionDataGridView.Location = new System.Drawing.Point(3, 46);
            this.NotificationSubscriptionDataGridView.Name = "NotificationSubscriptionDataGridView";
            this.NotificationSubscriptionDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.NotificationSubscriptionDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.NotificationSubscriptionDataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.NotificationSubscriptionDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.NotificationSubscriptionDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.NotificationSubscriptionDataGridView.ShowEditingIcon = false;
            this.NotificationSubscriptionDataGridView.Size = new System.Drawing.Size(720, 304);
            this.NotificationSubscriptionDataGridView.TabIndex = 0;
            this.NotificationSubscriptionDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.NotificationSubscriptionDataGridView_CellContentClick);
            this.NotificationSubscriptionDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.NotificationSubscriptionDataGridView_CellValueChanged);
            // 
            // CheckColumn
            // 
            this.CheckColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.CheckColumn.HeaderText = "";
            this.CheckColumn.MinimumWidth = 15;
            this.CheckColumn.Name = "CheckColumn";
            this.CheckColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CheckColumn.Width = 15;
            // 
            // StatusAsTextColumn
            // 
            this.StatusAsTextColumn.HeaderText = "Status";
            this.StatusAsTextColumn.MinimumWidth = 10;
            this.StatusAsTextColumn.Name = "StatusAsTextColumn";
            this.StatusAsTextColumn.ReadOnly = true;
            this.StatusAsTextColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // DescriptionColumn
            // 
            this.DescriptionColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.DescriptionColumn.HeaderText = "Description";
            this.DescriptionColumn.MinimumWidth = 100;
            this.DescriptionColumn.Name = "DescriptionColumn";
            this.DescriptionColumn.ReadOnly = true;
            // 
            // ExtraColumn
            // 
            this.ExtraColumn.HeaderText = "";
            this.ExtraColumn.Name = "ExtraColumn";
            this.ExtraColumn.ReadOnly = true;
            // 
            // ManualRefreshPanel
            // 
            this.ManualRefreshPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ManualRefreshPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ManualRefreshPanel.Controls.Add(this.FilterOnTagCheckBox);
            this.ManualRefreshPanel.Controls.Add(this.ChangeSplitButton);
            this.ManualRefreshPanel.Controls.Add(this.DeleteButton);
            this.ManualRefreshPanel.Controls.Add(this.CreateButton);
            this.ManualRefreshPanel.Controls.Add(this.CheckSplitButton);
            this.ManualRefreshPanel.Controls.Add(this.RefreshButton);
            this.ManualRefreshPanel.Location = new System.Drawing.Point(0, 28);
            this.ManualRefreshPanel.Name = "ManualRefreshPanel";
            this.ManualRefreshPanel.Size = new System.Drawing.Size(726, 57);
            this.ManualRefreshPanel.TabIndex = 1;
            // 
            // FilterOnTagCheckBox
            // 
            this.FilterOnTagCheckBox.AutoSize = true;
            this.FilterOnTagCheckBox.Location = new System.Drawing.Point(6, 33);
            this.FilterOnTagCheckBox.Name = "FilterOnTagCheckBox";
            this.FilterOnTagCheckBox.Size = new System.Drawing.Size(84, 17);
            this.FilterOnTagCheckBox.TabIndex = 1;
            this.FilterOnTagCheckBox.Text = "Filter on tag:";
            this.FilterOnTagCheckBox.UseVisualStyleBackColor = true;
            // 
            // ChangeSplitButton
            // 
            this.ChangeSplitButton.AutoSize = true;
            this.ChangeSplitButton.ContextMenuStrip = this.NotificationSubscriptionChangeContextMenuStrip;
            this.ChangeSplitButton.Location = new System.Drawing.Point(273, 4);
            this.ChangeSplitButton.Name = "ChangeSplitButton";
            this.ChangeSplitButton.Size = new System.Drawing.Size(75, 23);
            this.ChangeSplitButton.TabIndex = 5;
            this.ChangeSplitButton.Text = "Change";
            this.ChangeSplitButton.UseVisualStyleBackColor = true;
            // 
            // NotificationSubscriptionChangeContextMenuStrip
            // 
            this.NotificationSubscriptionChangeContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NotificationSubscriptionChangeDisableToolStripMenuItem,
            this.NotificationSubscriptionChangeEnableToolStripMenuItem});
            this.NotificationSubscriptionChangeContextMenuStrip.Name = "NotificationSubscriptionChangeContextMenuStrip";
            this.NotificationSubscriptionChangeContextMenuStrip.Size = new System.Drawing.Size(113, 48);
            // 
            // NotificationSubscriptionChangeDisableToolStripMenuItem
            // 
            this.NotificationSubscriptionChangeDisableToolStripMenuItem.Name = "NotificationSubscriptionChangeDisableToolStripMenuItem";
            this.NotificationSubscriptionChangeDisableToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.NotificationSubscriptionChangeDisableToolStripMenuItem.Text = "Disable";
            this.NotificationSubscriptionChangeDisableToolStripMenuItem.Click += new System.EventHandler(this.NotificationSubscriptionChangeDisableToolStripMenuItem_Click);
            // 
            // NotificationSubscriptionChangeEnableToolStripMenuItem
            // 
            this.NotificationSubscriptionChangeEnableToolStripMenuItem.Name = "NotificationSubscriptionChangeEnableToolStripMenuItem";
            this.NotificationSubscriptionChangeEnableToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.NotificationSubscriptionChangeEnableToolStripMenuItem.Text = "Enable";
            this.NotificationSubscriptionChangeEnableToolStripMenuItem.Click += new System.EventHandler(this.NotificationSubscriptionChangeEnableToolStripMenuItem_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(192, 4);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(75, 23);
            this.DeleteButton.TabIndex = 4;
            this.DeleteButton.Text = "Delete";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // CreateButton
            // 
            this.CreateButton.Location = new System.Drawing.Point(111, 4);
            this.CreateButton.Name = "CreateButton";
            this.CreateButton.Size = new System.Drawing.Size(75, 23);
            this.CreateButton.TabIndex = 3;
            this.CreateButton.Text = "Create";
            this.CreateButton.UseVisualStyleBackColor = true;
            this.CreateButton.Click += new System.EventHandler(this.CreateButton_Click);
            // 
            // CheckSplitButton
            // 
            this.CheckSplitButton.AutoSize = true;
            this.CheckSplitButton.ContextMenuStrip = this.NotificationSubscriptionCheckContextMenuStrip;
            this.CheckSplitButton.Location = new System.Drawing.Point(43, 4);
            this.CheckSplitButton.Name = "CheckSplitButton";
            this.CheckSplitButton.Size = new System.Drawing.Size(62, 23);
            this.CheckSplitButton.TabIndex = 2;
            this.CheckSplitButton.Text = "Check";
            this.CheckSplitButton.UseVisualStyleBackColor = true;
            // 
            // NotificationSubscriptionCheckContextMenuStrip
            // 
            this.NotificationSubscriptionCheckContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NotificationSubscriptionSelectAllToolStripMenuItem,
            this.NotificationSubscriptionSelectNoneToolStripMenuItem,
            this.toolStripSeparator1,
            this.NotificationSubscriptionSelectDisabledToolStripMenuItem,
            this.NotificationSubscriptionSelectEnabledToolStripMenuItem});
            this.NotificationSubscriptionCheckContextMenuStrip.Name = "NotificationSubscriptionCheckContextMenuStrip";
            this.NotificationSubscriptionCheckContextMenuStrip.Size = new System.Drawing.Size(120, 98);
            // 
            // NotificationSubscriptionSelectAllToolStripMenuItem
            // 
            this.NotificationSubscriptionSelectAllToolStripMenuItem.Name = "NotificationSubscriptionSelectAllToolStripMenuItem";
            this.NotificationSubscriptionSelectAllToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.NotificationSubscriptionSelectAllToolStripMenuItem.Text = "All";
            this.NotificationSubscriptionSelectAllToolStripMenuItem.Click += new System.EventHandler(this.NotificationSubscriptionSelectAllToolStripMenuItem_Click);
            // 
            // NotificationSubscriptionSelectNoneToolStripMenuItem
            // 
            this.NotificationSubscriptionSelectNoneToolStripMenuItem.Name = "NotificationSubscriptionSelectNoneToolStripMenuItem";
            this.NotificationSubscriptionSelectNoneToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.NotificationSubscriptionSelectNoneToolStripMenuItem.Text = "None";
            this.NotificationSubscriptionSelectNoneToolStripMenuItem.Click += new System.EventHandler(this.NotificationSubscriptionSelectNoneToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(116, 6);
            // 
            // NotificationSubscriptionSelectDisabledToolStripMenuItem
            // 
            this.NotificationSubscriptionSelectDisabledToolStripMenuItem.Name = "NotificationSubscriptionSelectDisabledToolStripMenuItem";
            this.NotificationSubscriptionSelectDisabledToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.NotificationSubscriptionSelectDisabledToolStripMenuItem.Text = "Disabled";
            this.NotificationSubscriptionSelectDisabledToolStripMenuItem.Click += new System.EventHandler(this.NotificationSubscriptionSelectDisabledToolStripMenuItem_Click);
            // 
            // NotificationSubscriptionSelectEnabledToolStripMenuItem
            // 
            this.NotificationSubscriptionSelectEnabledToolStripMenuItem.Name = "NotificationSubscriptionSelectEnabledToolStripMenuItem";
            this.NotificationSubscriptionSelectEnabledToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.NotificationSubscriptionSelectEnabledToolStripMenuItem.Text = "Enabled";
            this.NotificationSubscriptionSelectEnabledToolStripMenuItem.Click += new System.EventHandler(this.NotificationSubscriptionSelectEnabledToolStripMenuItem_Click);
            // 
            // RefreshButton
            // 
            this.RefreshButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
            this.RefreshButton.Location = new System.Drawing.Point(5, 4);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(32, 23);
            this.RefreshButton.TabIndex = 0;
            this.RefreshButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // VerticalSplitter
            // 
            this.VerticalSplitter.BackColor = System.Drawing.SystemColors.ControlDark;
            this.VerticalSplitter.Location = new System.Drawing.Point(650, 0);
            this.VerticalSplitter.Name = "VerticalSplitter";
            this.VerticalSplitter.Size = new System.Drawing.Size(6, 480);
            this.VerticalSplitter.TabIndex = 1;
            this.VerticalSplitter.TabStop = false;
            // 
            // BuildDefinitionSearchPanel
            // 
            this.BuildDefinitionSearchPanel.AutoScroll = true;
            this.BuildDefinitionSearchPanel.Controls.Add(this.BuildDefinitionSearchPlaceholderLabel);
            this.BuildDefinitionSearchPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.BuildDefinitionSearchPanel.Location = new System.Drawing.Point(0, 0);
            this.BuildDefinitionSearchPanel.Name = "BuildDefinitionSearchPanel";
            this.BuildDefinitionSearchPanel.Size = new System.Drawing.Size(650, 480);
            this.BuildDefinitionSearchPanel.TabIndex = 0;
            // 
            // BuildDefinitionSearchPlaceholderLabel
            // 
            this.BuildDefinitionSearchPlaceholderLabel.AutoSize = true;
            this.BuildDefinitionSearchPlaceholderLabel.Location = new System.Drawing.Point(12, 9);
            this.BuildDefinitionSearchPlaceholderLabel.Name = "BuildDefinitionSearchPlaceholderLabel";
            this.BuildDefinitionSearchPlaceholderLabel.Size = new System.Drawing.Size(173, 13);
            this.BuildDefinitionSearchPlaceholderLabel.TabIndex = 0;
            this.BuildDefinitionSearchPlaceholderLabel.Text = "Build Definition Search Placeholder";
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.VsoBuild.Sidekicks.Help.chm";
            // 
            // NotificationAdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1382, 480);
            this.Controls.Add(this.PropertiesPanel);
            this.Controls.Add(this.VerticalSplitter);
            this.Controls.Add(this.BuildDefinitionSearchPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "NotificationAdminForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "AAATemplateAAAForm";
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.PropertiesPanel.ResumeLayout(false);
            this.NotificationSubscriptionPanel.ResumeLayout(false);
            this.CreateNotificationSubscriptionPanel.ResumeLayout(false);
            this.CreateNotificationSubscriptionPanel.PerformLayout();
            this.StatusPanel.ResumeLayout(false);
            this.NotificationSubscriptionOperationResultPanel.ResumeLayout(false);
            this.NotificationSubscriptionOperationResultPanel.PerformLayout();
            this.NotificationSubscriptionOperationProgressPanel.ResumeLayout(false);
            this.NotificationSubscriptionOperationProgressPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NotificationSubscriptionDataGridView)).EndInit();
            this.ManualRefreshPanel.ResumeLayout(false);
            this.ManualRefreshPanel.PerformLayout();
            this.NotificationSubscriptionChangeContextMenuStrip.ResumeLayout(false);
            this.NotificationSubscriptionCheckContextMenuStrip.ResumeLayout(false);
            this.BuildDefinitionSearchPanel.ResumeLayout(false);
            this.BuildDefinitionSearchPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Splitter VerticalSplitter;
        private System.Windows.Forms.Panel BuildDefinitionSearchPanel;
        private System.Windows.Forms.Label BuildDefinitionSearchPlaceholderLabel;
        private System.Windows.Forms.Panel ManualRefreshPanel;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.Button HelpButton;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.Panel NotificationSubscriptionPanel;
        private System.Windows.Forms.DataGridView NotificationSubscriptionDataGridView;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusAsTextColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescriptionColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExtraColumn;
        private System.Windows.Forms.ContextMenuStrip NotificationSubscriptionCheckContextMenuStrip;
        private SplitButton CheckSplitButton;
        private System.Windows.Forms.ToolStripMenuItem NotificationSubscriptionSelectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem NotificationSubscriptionSelectNoneToolStripMenuItem;
        private SplitButton ChangeSplitButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button CreateButton;
        private System.Windows.Forms.ContextMenuStrip NotificationSubscriptionChangeContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem NotificationSubscriptionChangeDisableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem NotificationSubscriptionChangeEnableToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem NotificationSubscriptionSelectDisabledToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem NotificationSubscriptionSelectEnabledToolStripMenuItem;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel StatusPanel;
        private System.Windows.Forms.Panel NotificationSubscriptionOperationResultPanel;
        private System.Windows.Forms.Label NotificationSubscriptionOperationResultLabel;
        private System.Windows.Forms.Panel NotificationSubscriptionOperationProgressPanel;
        private System.Windows.Forms.ProgressBar NotificationSubscriptionOperationProgressBar;
        private System.Windows.Forms.Label NotificationSubscriptionOperationProgressLabel;
        private System.Windows.Forms.Panel CreateNotificationSubscriptionPanel;
        private System.Windows.Forms.Button CreateNotificationSubscriptionCancelButton;
        private System.Windows.Forms.Button CreateNotificationSubscriptionOKButton;
        private System.Windows.Forms.ComboBox TeamNameComboBox;
        private System.Windows.Forms.Label TeamNameLabel;
        private System.Windows.Forms.TextBox EmailAddressesTextBox;
        private System.Windows.Forms.Label EmailAddressLabel;
        private System.Windows.Forms.CheckBox FilterOnTagCheckBox;
        private System.Windows.Forms.Label TagValueLabel;
        private System.Windows.Forms.Label TagLabel;
    }
}