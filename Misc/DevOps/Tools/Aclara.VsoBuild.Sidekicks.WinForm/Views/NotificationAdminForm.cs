﻿using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using Aclara.Vsts.Notification.Client.Models;
using Aclara.Vsts.Notification.Client.Types;
using Aclara.VsoBuild.Sidekicks.WinForm.Exceptions;
using Microsoft.VisualStudio.Services.Notifications.WebApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class NotificationAdminForm : Form, ISidekickView, INotificationAdminView, ISidekickInfo
    {

        #region Private Constants

        private const string SidekickInfo_SidekickName = "AdminNotifications";
        private const string SidekickInfo_SidekickDescription = "Administer Notifications";

        private const string NotificationSubscriptionDataGridView_CheckColumn = "CheckColumn";
        private const string NotificationSubscriptionDataGridView_StatusAsTextColumn = "StatusAsTextColumn";
        private const string NotificationSubscriptionDataGridView_DescriptionColumn = "DescriptionColumn";
        private const string BuildStatusDataGridView_ExtraColumn = "ExtraColumn";

        private const string NotificationSubscriptionDataGridView_Selected = "Selected";
        private const string NotificationSubscriptionDataGridView_StatusAsText = "StatusAsText";
        private const string NotificationSubscriptionDataGridView_Description = "Description";
        private const string BuildStatusDataGridView_Extra = "Extra";

        private const string ValidationErrorMessage_BuildDefinitionNotSelected = "Build definition not selected.";
        private const string ValidationErrorMessage_NotificationSubscriptionNotSelected = "Notification not selected.";
        private const string ValidationErrorMessage_TeamNameIsRequired = "Team name is required.";
        private const string ValidationErrorMessage_OneOrMoreEmailAddressesAreRequired = "One or more email addresses are required.";

        private const string NotificationSubscriptionOperationResult_NotificationSubscriptionFound = "{0} notification subscription found.";
        private const string NotificationSubscriptionOperationResult_NotificationSubscriptionsFoundPlural = "{0} notification subscriptions found.";

        private const string NotificationSubscriptionStatus_Enabled = "enabled";
        private const string NotificationSubscriptionStatus_Disabled = "disabled";

        private const string DeleteNotificationSubscriptionConfirmation_PendingAction = "Are you sure you want to to delete selected notification subscription(s)?";
        private const string DeleteNotificationSubscriptionConfirmation_ConfirmationMessage = @"Type ""YES"" to confirm notification subscription deletion.";
        private const string DeleteNotificationSubscriptionConfirmation_ExpectedTypeResponse = "YES";

        private const string FilterOnTag_Label = "Filter on tag:";

        private const string ConfigurationNotFoundException_ErrorText = "Configuration setting not found. (Setting: NotificationSubscriptionDefaults_VSTSTeamProject, VstsTeamProjectName: {0}";

        #endregion

        #region Private Data Members

        private SidekickConfiguration _sidekickConfiguration;
        private NotificationAdminPresenter _notificationAdminPresenter;
        private BuildDefinitionSearchForm _buildDefinitionSearchForm = null;
        private NotificationSubscriptionList _notificationSubscriptionList;
        private bool _oneOrMoreBuildDefinitionsSelected;
        private List<NotificationSubscriptionDisplay> _notificationSubscriptionDisplayList;
        private SortableBindingList<BuildStatusDisplay> _buildStatusDisplayListBindingSource;
        private SortableBindingList<NotificationSubscriptionDisplay> _notificationSubscriptionDisplayListBindingSource;
        private System.Windows.Forms.SortOrder _sortOrder;
        private DataGridViewColumn _sortedColumn;
        private List<string> _teamNameList;
        private Size _maxNotificationSubscriptionPanelSize;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Notification admin presenter.
        /// </summary>
        public NotificationAdminPresenter NotificationAdminPresenter
        {
            get { return _notificationAdminPresenter; }
            set { _notificationAdminPresenter = value; }
        }

        /// <summary>
        /// Property: Build definition search form.
        /// </summary>
        public BuildDefinitionSearchForm BuildDefinitionSearchForm
        {
            get { return _buildDefinitionSearchForm; }
            set { _buildDefinitionSearchForm = value; }
        }

        /// <summary>
        /// Property: Notification subscription list.
        /// </summary>
        public NotificationSubscriptionList NotificationSubscriptionList
        {
            get { return _notificationSubscriptionList; }
            set { _notificationSubscriptionList = value; }
        }

        /// <summary>
        /// Property: Notification subscription display list.
        /// </summary>
        public List<NotificationSubscriptionDisplay> NotificationSubscriptionDisplayList
        {
            get { return _notificationSubscriptionDisplayList; }
            set { _notificationSubscriptionDisplayList = value; }
        }

        /// <summary>
        /// Property: Notification subscription display list binding source.
        /// </summary>
        public SortableBindingList<NotificationSubscriptionDisplay> NotificationSubscriptionDisplayListBindingSource
        {
            get { return _notificationSubscriptionDisplayListBindingSource; }
            set { _notificationSubscriptionDisplayListBindingSource = value; }
        }

        /// <summary>
        /// Property: Sort order.
        /// </summary>
        public System.Windows.Forms.SortOrder SortOrder
        {
            get { return _sortOrder; }
            set { _sortOrder = value; }
        }

        /// <summary>
        /// Property: Sorted column.
        /// </summary>
        public DataGridViewColumn SortedColumn
        {
            get { return _sortedColumn; }
            set { _sortedColumn = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Notification subscription list retrieval result.
        /// </summary>
        public string NotificationSubscriptionOperationResult
        {
            get { return this.NotificationSubscriptionOperationResultLabel.Text; }
            set { this.NotificationSubscriptionOperationResultLabel.Text = value; }
        }

        /// <summary>
        /// Property: Notification subscription list retrieval progress.
        /// </summary>
        public string NotificationSubscriptionOperationProgress
        {
            get { return this.NotificationSubscriptionOperationProgressLabel.Text; }
            set
            {
                this.NotificationSubscriptionOperationProgressLabel.Text = value;
            }
        }
        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickInfo_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickInfo_SidekickDescription;
            }
        }
        /// <summary>
        /// Property: One or more build definitions selected.
        /// </summary>
        public bool OneOrMoreBuildDefinitionsSelected
        {
            get { return _oneOrMoreBuildDefinitionsSelected; }
            set { _oneOrMoreBuildDefinitionsSelected = value; }
        }

        /// <summary>
        /// Property: Team name.
        /// </summary>
        public string TeamName
        {
            get { return this.TeamNameComboBox.Text; }
            set { this.TeamNameComboBox.Text = value; }
        }

        /// <summary>
        /// Property: Email addresses.
        /// </summary>
        public string EmailAddresses
        {
            get { return this.EmailAddressesTextBox.Text; }
            set { this.EmailAddressesTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Team name list.
        /// </summary>
        public List<string> TeamNameList
        {
            get { return _teamNameList; }
            set { _teamNameList = value; }
        }

        /// <summary>
        /// Property: Maximum notification subscription panel size.
        /// </summary>
        public Size MaxNotificationSubscriptionPanelSize
        {
            get { return _maxNotificationSubscriptionPanelSize; }
            set { _maxNotificationSubscriptionPanelSize = value; }
        }

        /// <summary>
        /// Property: Filter on tag.
        /// </summary>
        public bool FilterOnTag
        {
            get { return FilterOnTagCheckBox.Checked; }
            set { FilterOnTagCheckBox.Checked = value; }
        }


        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <param></param>
        public NotificationAdminForm(SidekickConfiguration sidekickConfiguration,
                                     BuildDefinitionSearchForm buildDefinitionSearchForm)
        {
            _sidekickConfiguration = sidekickConfiguration;

            InitializeComponent();

            this.InitializeControls();

            this.NotificationSubscriptionDataGridView.AutoGenerateColumns = false;
            _notificationSubscriptionDisplayList = new List<NotificationSubscriptionDisplay>();
            _notificationSubscriptionDisplayListBindingSource = new SortableBindingList<NotificationSubscriptionDisplay>(_notificationSubscriptionDisplayList);
            this.NotificationSubscriptionDataGridView.DataSource = _buildStatusDisplayListBindingSource;
            this.MapNotificationSubscriptionDataGridViewColumns();

            this.BuildDefinitionSearchForm = buildDefinitionSearchForm;
            this.BuildDefinitionSearchForm.BuildDefinitionSelectionChanged += OnBuildDefinitionSelectionChanged;
            this.BuildDefinitionSearchPanel.DockControl(this.BuildDefinitionSearchForm);


        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Refresh button enable.
        /// </summary>
        public void RefreshButtonEnable(bool enable)
        {
            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke(new Action<bool>(this.RefreshButtonEnable), enable);
                }
                else
                {
                    this.RefreshButton.Enabled = enable;
                } //End of thread safe block.
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            NotificationSubscriptionDefaults_VSTSTeamProject vstsTeamProject = null;

            try
            {
                this.BuildDefinitionSearchForm.TeamProjectChanged();

                vstsTeamProject = this.GetVSTSTeamProjectDefaults();

                this.FilterOnTagCheckBox.Text = string.Format("{0} {1}",
                                                               FilterOnTag_Label,
                                                                vstsTeamProject.Tag);

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Retrieve remaining build definitions.
        /// </summary>
        public void RetrieveRemainingDefinitions()
        {
            this.BuildDefinitionSearchForm.RetrieveRemainingDefinitions();
        }
        /// <summary>
        /// Update notification subscription operation status.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="notificationSubscriptionListRetrievedCount"></param>
        /// <param name="notificationSubscriptionListTotalCount"></param>
        public void UpdateNotificationSubscriptionOperationStatus(string statusMessage,
                                                                      int notificationSubscriptionListRetrievedCount,
                                                                      int notificationSubscriptionListTotalCount)
        {
            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        UpdateNotificationSubscriptionOperationStatus(statusMessage,
                                                                          notificationSubscriptionListRetrievedCount,
                                                                          notificationSubscriptionListTotalCount);
                    });
                }
                else
                {
                    this.NotificationSubscriptionOperationProgressPanel.Visible = true;
                    this.NotificationSubscriptionOperationResultPanel.Visible = false;

                    this.StatusPanel.BackColor = Color.OldLace;

                    if (notificationSubscriptionListTotalCount < notificationSubscriptionListRetrievedCount)
                    {
                        notificationSubscriptionListRetrievedCount = 1;
                        notificationSubscriptionListTotalCount = 1;
                    }

                    if ((notificationSubscriptionListRetrievedCount < notificationSubscriptionListTotalCount))
                    {
                        this.NotificationSubscriptionOperationProgressPanel.Visible = true;
                        this.NotificationSubscriptionOperationResultPanel.Visible = false;

                        this.NotificationSubscriptionOperationProgressBar.Visible = true;
                        this.NotificationSubscriptionOperationProgressBar.Minimum = 0;
                        this.NotificationSubscriptionOperationProgressBar.Maximum = notificationSubscriptionListTotalCount;
                        this.NotificationSubscriptionOperationProgressBar.Value = notificationSubscriptionListRetrievedCount;
                        this.NotificationSubscriptionOperationProgress = statusMessage;
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Sidekick was activated.
        /// </summary>
        public void SidekickActivated()
        {
            try
            {
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.UpdateRefreshCancelMode(false);
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus, NotificationAdminPresenter.BackgroundTaskOperationId backgroundTaskOperation)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string, NotificationAdminPresenter.BackgroundTaskOperationId>(this.BackgroundTaskCompleted), backgroundTaskStatus, backgroundTaskOperation);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.UpdateRefreshCancelMode(false);
                switch (backgroundTaskOperation)
                {
                    case NotificationAdminPresenter.BackgroundTaskOperationId.GetNotificationSubscriptionList:
                        break;
                    case NotificationAdminPresenter.BackgroundTaskOperationId.CreateNotificationSubscriptionList:
                        this.RefreshNotificationSubscriptionList();
                        break;
                    case NotificationAdminPresenter.BackgroundTaskOperationId.DeleteNotificationSubscriptionList:
                        this.RefreshNotificationSubscriptionList();
                        break;
                    case NotificationAdminPresenter.BackgroundTaskOperationId.ChangeNotificationSubscriptionList:
                        this.RefreshNotificationSubscriptionList();
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Populate notification subscription status.
        /// </summary>
        public void PopulateNotificationSubscriptionList()
        {
            NotificationSubscriptionDisplay notificationSubscriptionStatusDisplay = null;
            ListSortDirection sortDirection;

            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        PopulateNotificationSubscriptionList();
                    });

                }
                else
                {
                    this.StatusPanel.BackColor = Color.White;

                    this.NotificationSubscriptionDisplayList.Clear();

                    foreach (NotificationSubscription notificationSubscription in this.NotificationSubscriptionList)
                    {
                        notificationSubscriptionStatusDisplay = new NotificationSubscriptionDisplay(notificationSubscription);
                        this.NotificationSubscriptionDisplayList.Add(notificationSubscriptionStatusDisplay);
                    }

                    this.NotificationSubscriptionDataGridView.DataSource = null;
                    _notificationSubscriptionDisplayListBindingSource = new SortableBindingList<NotificationSubscriptionDisplay>(this.NotificationSubscriptionDisplayList);
                    this.NotificationSubscriptionDataGridView.DataSource = _notificationSubscriptionDisplayListBindingSource;

                    //Restore sort order.
                    if (this.SortOrder == SortOrder.Ascending)
                    {
                        sortDirection = ListSortDirection.Ascending;
                    }
                    else
                    {
                        sortDirection = ListSortDirection.Descending;
                    }

                    if (this.SortedColumn != null)
                    {
                        this.NotificationSubscriptionDataGridView.Sort(this.SortedColumn, sortDirection); ;
                    }

                    if (this.NotificationSubscriptionList.Count == 1)
                    {
                        this.NotificationSubscriptionOperationResult = string.Format(NotificationSubscriptionOperationResult_NotificationSubscriptionFound,
                                                                                         this.NotificationSubscriptionList.Count);
                    }
                    else
                    {
                        this.NotificationSubscriptionOperationResult = string.Format(NotificationSubscriptionOperationResult_NotificationSubscriptionsFoundPlural,
                                                                                         this.NotificationSubscriptionList.Count);
                    }

                    this.StatusPanel.BackColor = Color.OldLace;

                    this.NotificationSubscriptionOperationProgressPanel.Visible = false;
                    this.NotificationSubscriptionOperationResultPanel.Visible = true;

                    this.NotificationSubscriptionDataGridView.AutoResizeColumns();
                    this.NotificationSubscriptionDataGridView.AllowUserToResizeColumns = true;

                    for (int columnIndex = 0; columnIndex < NotificationSubscriptionDataGridView.Columns.Count; columnIndex++)
                    {
                        int columnWidth = NotificationSubscriptionDataGridView.Columns[columnIndex].Width;
                        NotificationSubscriptionDataGridView.Columns[columnIndex].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                        NotificationSubscriptionDataGridView.Columns[columnIndex].Width = columnWidth;
                    }

                    //By default select no rows.
                    if (this.NotificationSubscriptionDataGridView.Rows.Count > 0)
                    {
                        this.NotificationSubscriptionDataGridView.Rows[0].Selected = false;
                    }


                } //End of thread safe block.
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {
                this.UpdateRefreshCancelMode(false);

                this.NotificationSubscriptionOperationProgressPanel.Visible = false;
                this.NotificationSubscriptionOperationResultPanel.Visible = false;

                this.EnableControlsDependantOnSelectedBuildDefinition(false);

                this.CreateNotificationSubscriptionPanel.Visible = false;

                this.FilterOnTagCheckBox.Checked = false;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Populate defaults for create notification subscription.
        /// </summary>
        protected void PopulateDefaultsCreateNotificationSubscription()
        {
            NotificationSubscriptionDefaults_VSTSTeamProject vstsTeamProject = null;
            string teamNameDefault = string.Empty;
            string emailAddressesDefault = string.Empty;
            int maximumWidthFound = 0;

            try
            {

                vstsTeamProject = this.GetVSTSTeamProjectDefaults();
                if (vstsTeamProject != null)
                {
                    teamNameDefault = vstsTeamProject.SubscriberTeamName;
                    emailAddressesDefault = vstsTeamProject.CustomAddress;
                }

                this.TeamNameList = this.NotificationAdminPresenter.GetTeamNameList();
                this.TeamNameComboBox.DataSource = this.TeamNameList;

                foreach (string teamName in this.TeamNameComboBox.Items)
                {
                    var currentWidthFound = TextRenderer.MeasureText(teamName, TeamNameComboBox.Font).Width;
                    if (currentWidthFound > maximumWidthFound)
                    {
                        maximumWidthFound = currentWidthFound;
                    }
                    if (teamName == teamNameDefault)
                    {
                        this.TeamNameComboBox.SelectedItem = teamName;
                    }
                }
                this.TeamNameComboBox.Size = new Size(maximumWidthFound, this.TeamNameComboBox.Size.Height);

                this.EmailAddresses = emailAddressesDefault;

                this.TagValueLabel.Text = vstsTeamProject.Tag;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Hanlder: Build definition selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="requestBuildEventArgs"></param>
        protected void OnBuildDefinitionSelectionChanged(Object sender,
                                                         BuildDefinitionSelectionChangedEventArgs buildDefinitionSelectionChangedEventArgs)
        {

            try
            {

                if (buildDefinitionSelectionChangedEventArgs != null)
                {
                    if (buildDefinitionSelectionChangedEventArgs.OneOrMoreSelected == true)
                    {
                        if (this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList().Count > 0)
                        {

                            this.EnableControlsDependantOnSelectedBuildDefinition(true);
                            this.OneOrMoreBuildDefinitionsSelected = true;

                        }
                    }
                    else
                    {
                        this.ClearNotificationSubscriptionOperationResult();
                        this.EnableControlsDependantOnSelectedBuildDefinition(false);
                        this.OneOrMoreBuildDefinitionsSelected = false;
                        this.EnableCreateNotificationSubscriptionMode(false);
                    }

                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Clear notification subscription operation result.
        /// </summary>
        protected void ClearNotificationSubscriptionOperationResult()
        {
            try
            {
                this.NotificationSubscriptionOperationProgressPanel.Visible = false;
                this.NotificationSubscriptionOperationProgress = string.Empty;

                this.NotificationSubscriptionOperationResultPanel.Visible = false;
                this.NotificationSubscriptionOperationResult = string.Empty;

                this.StatusPanel.BackColor = Color.White;

                this.NotificationSubscriptionDisplayList.Clear();
                this.NotificationSubscriptionDataGridView.DataSource = null;
                if (this.NotificationSubscriptionList != null)
                {
                    this.NotificationSubscriptionList.Clear();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable/disable controls dependant on at lease one build definition selected.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnSelectedBuildDefinition(bool enable)
        {

            try
            {

                this.RefreshButton.Enabled = enable;
                this.FilterOnTagCheckBox.Enabled = enable;
                this.CheckSplitButton.Enabled = enable;
                this.CreateButton.Enabled = enable;
                if (enable == false)
                {
                    this.EnableControlsDependantOnSelectedNotificationSubscription(enable);
                }
                else
                {
                    if (this.GetSelectedNotificationSubscriptionDisplayList().Count > 0)
                    {
                        this.EnableControlsDependantOnSelectedNotificationSubscription(enable);
                    }
                }
                this.TeamNameComboBox.Enabled = enable;
                this.EmailAddressesTextBox.Enabled = enable;
                this.NotificationSubscriptionDataGridView.Enabled = enable;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Enable/disable controls dependant on at lease one notification subscription selected.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnSelectedNotificationSubscription(bool enable)
        {
            try
            {

                this.DeleteButton.Enabled = enable;
                this.ChangeSplitButton.Enabled = enable;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Refresh notification subscription list.
        /// </summary>
        protected void RefreshNotificationSubscriptionList()
        {
            BuildDefinitionSelectorList buildDefinitionSelectorList = null;
            NotificationSubscriptionDefaults_VSTSTeamProject vstsTeamProject = null;

            try
            {
                if (this.NotificationAdminPresenter.IsBackgroundTaskRunning() == true)
                {
                    this.NotificationAdminPresenter.CancelBackgroundTask();
                    this.UpdateRefreshCancelMode(false);
                }
                else
                {
                    buildDefinitionSelectorList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();
                    if (buildDefinitionSelectorList == null ||
                        buildDefinitionSelectorList.Count <= 0)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_BuildDefinitionNotSelected));
                        return;
                    }

                    this.ClearNotificationSubscriptionOperationResult();

                    this.StatusPanel.BackColor = Color.White;
                    this.UpdateRefreshCancelMode(true);

                    vstsTeamProject = this.GetVSTSTeamProjectDefaults();

                    this.NotificationAdminPresenter.GetNotificationSubscriptionList(buildDefinitionSelectorList, this.FilterOnTag, vstsTeamProject.Tag);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Update refresh cancel mode.
        /// </summary>
        /// <param name="enabled"></param>
        protected void UpdateRefreshCancelMode(bool enabled)
        {
            try
            {
                if (enabled == true)
                {
                    this.RefreshButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionCancelRefresh;
                }
                else
                {
                    this.RefreshButton.Image = global::Aclara.VsoBuild.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Map notification subscription data grid view columns.
        /// </summary>
        protected void MapNotificationSubscriptionDataGridViewColumns()
        {
            try
            {
                foreach (DataGridViewColumn column in this.NotificationSubscriptionDataGridView.Columns)
                {

                    switch (column.Name)
                    {
                        case NotificationSubscriptionDataGridView_CheckColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                            column.DataPropertyName = NotificationSubscriptionDataGridView_Selected;
                            break;

                        case NotificationSubscriptionDataGridView_StatusAsTextColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            column.DataPropertyName = NotificationSubscriptionDataGridView_StatusAsText;
                            break;

                        case NotificationSubscriptionDataGridView_DescriptionColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            column.DataPropertyName = NotificationSubscriptionDataGridView_Description;
                            break;

                        case BuildStatusDataGridView_ExtraColumn:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            break;

                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Check all notification subscription (in data grid view).
        /// </summary>
        protected void NotificationSubscriptionCheckAll()
        {
            NotificationSubscriptionDisplay notificationSubscriptionDisplay = null;
            try
            {
                foreach (DataGridViewRow dataGridViewRow in this.NotificationSubscriptionDataGridView.Rows)
                {
                    notificationSubscriptionDisplay = (NotificationSubscriptionDisplay)dataGridViewRow.DataBoundItem;
                    dataGridViewRow.Cells[NotificationSubscriptionDataGridView_CheckColumn].Value = true;
                    notificationSubscriptionDisplay.Selected = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Check none notification subscription (in data grid view).
        /// </summary>
        protected void NotificationSubscriptionCheckNone()
        {
            NotificationSubscriptionDisplay notificationSubscriptionDisplay = null;
            try
            {
                foreach (DataGridViewRow dataGridViewRow in this.NotificationSubscriptionDataGridView.Rows)
                {
                    notificationSubscriptionDisplay = (NotificationSubscriptionDisplay)dataGridViewRow.DataBoundItem;
                    dataGridViewRow.Cells[NotificationSubscriptionDataGridView_CheckColumn].Value = false;
                    notificationSubscriptionDisplay.Selected = false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Check disabled notification subscription (in data grid view).
        /// </summary>
        protected void NotificationSubscriptionCheckDisabled()
        {
            NotificationSubscriptionDisplay notificationSubscriptionDisplay = null;

            try
            {
                this.NotificationSubscriptionCheckNone();

                foreach (DataGridViewRow dataGridViewRow in this.NotificationSubscriptionDataGridView.Rows)
                {
                    notificationSubscriptionDisplay = (NotificationSubscriptionDisplay)dataGridViewRow.DataBoundItem;
                    if (notificationSubscriptionDisplay.StatusAsText.ToLower().Contains(NotificationSubscriptionStatus_Disabled) == true)
                    {
                        dataGridViewRow.Cells[NotificationSubscriptionDataGridView_CheckColumn].Value = true;
                        notificationSubscriptionDisplay.Selected = true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Check enabled notification subscription (in data grid view).
        /// </summary>
        protected void NotificationSubscriptionCheckEnabled()
        {
            NotificationSubscriptionDisplay notificationSubscriptionDisplay = null;

            try
            {
                this.NotificationSubscriptionCheckNone();

                foreach (DataGridViewRow dataGridViewRow in this.NotificationSubscriptionDataGridView.Rows)
                {
                    notificationSubscriptionDisplay = (NotificationSubscriptionDisplay)dataGridViewRow.DataBoundItem;
                    if (notificationSubscriptionDisplay.StatusAsText.ToLower().Contains(NotificationSubscriptionStatus_Enabled) == true)
                    {
                        dataGridViewRow.Cells[NotificationSubscriptionDataGridView_CheckColumn].Value = true;
                        notificationSubscriptionDisplay.Selected = true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable create notification subscription mode.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableCreateNotificationSubscriptionMode(bool enable)
        {
            try
            {
                if (enable == true)
                {
                    this.CreateNotificationSubscriptionPanel.Visible = true;
                    this.RefreshButton.Enabled = false;
                    this.CheckSplitButton.Enabled = false;
                    this.NotificationSubscriptionDataGridView.Enabled = false;
                    this.NotificationSubscriptionDataGridView.Size = new Size(this.NotificationSubscriptionDataGridView.Size.Width,
                                                                              this.NotificationSubscriptionDataGridView.Size.Height - (this.CreateNotificationSubscriptionPanel.Size.Height));
                    this.NotificationSubscriptionDataGridView.Location = new Point(this.NotificationSubscriptionDataGridView.Location.X,
                                                                                   this.NotificationSubscriptionDataGridView.Location.Y + (this.CreateNotificationSubscriptionPanel.Size.Height + 5));
                }
                else
                {
                    CreateNotificationSubscriptionPanel.Visible = false;
                    this.RefreshButton.Enabled = true;
                    this.CheckSplitButton.Enabled = true;
                    this.NotificationSubscriptionDataGridView.Enabled = true;
                    this.NotificationSubscriptionDataGridView.Size = new Size(this.NotificationSubscriptionDataGridView.Size.Width,
                                                                              this.NotificationSubscriptionDataGridView.Size.Height + (this.CreateNotificationSubscriptionPanel.Size.Height));
                    this.NotificationSubscriptionDataGridView.Location = new Point(this.NotificationSubscriptionDataGridView.Location.X,
                                                                                   this.NotificationSubscriptionDataGridView.Location.Y - (this.CreateNotificationSubscriptionPanel.Size.Height + 5));
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve selected notification subscription display list.
        /// </summary>
        /// <returns></returns>
        protected NotificationSubscriptionDisplayList GetSelectedNotificationSubscriptionDisplayList()
        {
            NotificationSubscriptionDisplayList result = null;
            NotificationSubscriptionDisplay notificationSubscriptionDisplay = null;
            try
            {

                result = new NotificationSubscriptionDisplayList();
                foreach (DataGridViewRow dataGridViewRow in this.NotificationSubscriptionDataGridView.Rows)
                {
                    notificationSubscriptionDisplay = (NotificationSubscriptionDisplay)dataGridViewRow.DataBoundItem;
                    if (notificationSubscriptionDisplay.Selected == true)
                    {
                        result.Add(notificationSubscriptionDisplay);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve VSTS team project defaults from configuration.
        /// </summary>
        /// <returns></returns>
        protected NotificationSubscriptionDefaults_VSTSTeamProject GetVSTSTeamProjectDefaults()
        {

            NotificationSubscriptionDefaults_VSTSTeamProject result = null;

            try
            {
                result = this.SidekickConfiguration.NotificationSubscriptionDefaults.VSTSTeamProject.Where(vtp => vtp.VSTSTeamProjectName == this.NotificationAdminPresenter.TeamProjectInfo.TeamProjectName).SingleOrDefault();

                if (result == null)
                {
                    throw new ConfigurationNotFoundException(string.Format(ConfigurationNotFoundException_ErrorText,
                                                                           this.NotificationAdminPresenter.TeamProjectInfo.TeamProjectName));
                }

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {

            try
            {


                //this.NotificationAdminPresenter.AAATemplateAAA_RenameThisMethod(operationCount, timePerOperation);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickAdministerNotifications.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Refresh button - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshButton_Click(object sender, EventArgs e)
        {

            try
            {
                this.RefreshNotificationSubscriptionList();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

        /// <summary>
        /// Event Handler: Notification subscription select all tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotificationSubscriptionSelectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                NotificationSubscriptionCheckAll();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Notification subscription select none tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotificationSubscriptionSelectNoneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                NotificationSubscriptionCheckNone();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Notification subscription select disabled tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotificationSubscriptionSelectDisabledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                NotificationSubscriptionCheckDisabled();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Notification subscription select enabled tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotificationSubscriptionSelectEnabledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.NotificationSubscriptionCheckEnabled();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Create button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateButton_Click(object sender, EventArgs e)
        {

            try
            {

                this.CreateButton.Enabled = false;
                this.DeleteButton.Enabled = false;
                this.ChangeSplitButton.Enabled = false;

                this.PopulateDefaultsCreateNotificationSubscription();

                this.EnableCreateNotificationSubscriptionMode(true);

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Delete button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteButton_Click(object sender, EventArgs e)
        {
            NotificationSubscriptionDisplayList notificationSubscriptionDisplayList = null;
            List<string> notificationSubscriptionIdList = null;
            DialogResult dialogResult;
            ConfirmationForm confirmationForm = null;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                notificationSubscriptionIdList = new List<string>();

                notificationSubscriptionDisplayList = this.GetSelectedNotificationSubscriptionDisplayList();

                if (notificationSubscriptionDisplayList == null ||
                    notificationSubscriptionDisplayList.Count <= 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_NotificationSubscriptionNotSelected));
                    return;
                }

                notificationSubscriptionIdList = notificationSubscriptionDisplayList.GetNotificationSubscriptionIdList();

                //Request confirmation.
                confirmationForm = ConfirmationFormFactory.CreateForm(DeleteNotificationSubscriptionConfirmation_PendingAction,
                                                                      DeleteNotificationSubscriptionConfirmation_ConfirmationMessage,
                                                                      DeleteNotificationSubscriptionConfirmation_ExpectedTypeResponse,
                                                                      true,
                                                                      false,
                                                                      string.Empty);
                confirmationForm.StartPosition = FormStartPosition.CenterParent;
                dialogResult = confirmationForm.ShowDialog(this);
                if (dialogResult == DialogResult.OK)
                {
                    this.NotificationAdminPresenter.DeleteNotificationSubscriptionList(notificationSubscriptionIdList);
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Notification subscription data grid view - Cell value changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotificationSubscriptionDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            List<NotificationSubscriptionDisplay> selectedNotificationSubscriptionDisplayList = null;
            try
            {
                if (NotificationSubscriptionDataGridView.Columns[e.ColumnIndex].Name == NotificationSubscriptionDataGridView_CheckColumn && e.RowIndex >= 0 && e.RowIndex < NotificationSubscriptionDataGridView.Rows.Count)
                {
                    NotificationSubscriptionDisplay notificationSubscriptionDisplay = (NotificationSubscriptionDisplay)NotificationSubscriptionDataGridView.Rows[e.RowIndex].DataBoundItem;

                    DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)NotificationSubscriptionDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];

                    if ((bool)cell.Value == true)
                    {
                        notificationSubscriptionDisplay.Selected = true;
                    }
                    else
                    {
                        notificationSubscriptionDisplay.Selected = false;
                    }

                    selectedNotificationSubscriptionDisplayList = this.NotificationSubscriptionDisplayList.Where(nsd => nsd.Selected == true).ToList();

                    if (this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorCount() > 0 &&
                        selectedNotificationSubscriptionDisplayList.Count > 0)
                    {
                        this.EnableControlsDependantOnSelectedNotificationSubscription(true);
                    }
                    else
                    {
                        this.EnableControlsDependantOnSelectedNotificationSubscription(false);
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Notification subscription data grid view - Cell content click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotificationSubscriptionDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.NotificationSubscriptionDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Create notification subscription OK button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateNotificationSubscriptionOKButton_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelectorList buildDefinitionSelectorList = null;
            string teamName = string.Empty;
            string emailAddresses = string.Empty;
            NotificationSubscriptionDefaults_VSTSTeamProject vstsTeamProject = null;

            try
            {

                this.Cursor = Cursors.WaitCursor;

                buildDefinitionSelectorList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();

                teamName = this.TeamName;
                //Validate: Team name.
                if (string.IsNullOrEmpty(teamName) == true)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_TeamNameIsRequired));
                    return;
                }

                emailAddresses = this.EmailAddresses;
                //Validate email address(es).
                if (string.IsNullOrEmpty(emailAddresses) == true)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_OneOrMoreEmailAddressesAreRequired));
                    return;
                }

                vstsTeamProject = this.GetVSTSTeamProjectDefaults();

                this.NotificationAdminPresenter.CreateNotificationSubscriptionList(buildDefinitionSelectorList, teamName, emailAddresses, vstsTeamProject.Tag);

                this.EnableCreateNotificationSubscriptionMode(false);
                if (this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList().Count > 0)
                {
                    this.EnableControlsDependantOnSelectedBuildDefinition(true);
                }
                else
                {
                    this.EnableControlsDependantOnSelectedBuildDefinition(true);
                }
                if (this.GetSelectedNotificationSubscriptionDisplayList().Count > 0)
                {
                    this.EnableControlsDependantOnSelectedNotificationSubscription(true);
                }
                else
                {
                    this.EnableControlsDependantOnSelectedNotificationSubscription(false);
                }
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        /// <summary>
        /// Event Handler: Create notification subscription cancel button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateNotificationSubscriptionCancelButton_Click(object sender, EventArgs e)
        {

            try
            {
                this.EnableCreateNotificationSubscriptionMode(false);
                if (this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList().Count > 0)
                {
                    this.EnableControlsDependantOnSelectedBuildDefinition(true);
                }
                else
                {
                    this.EnableControlsDependantOnSelectedBuildDefinition(true);
                }
                if (this.GetSelectedNotificationSubscriptionDisplayList().Count > 0)
                {
                    this.EnableControlsDependantOnSelectedNotificationSubscription(true);
                }
                else
                {
                    this.EnableControlsDependantOnSelectedNotificationSubscription(false);
                }
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Notification subscription change disable tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotificationSubscriptionChangeDisableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NotificationSubscriptionDisplayList notificationSubscriptionDisplayList = null;
            List<string> notificationSubscriptionIdList = null;
            NotificationSubscriptionChangeProperties notificationSubscriptionChangeProperties = null;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                notificationSubscriptionIdList = new List<string>();
                notificationSubscriptionChangeProperties = new NotificationSubscriptionChangeProperties();

                notificationSubscriptionDisplayList = this.GetSelectedNotificationSubscriptionDisplayList();
                if (notificationSubscriptionDisplayList == null ||
                    notificationSubscriptionDisplayList.Count <= 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_NotificationSubscriptionNotSelected));
                    return;
                }

                notificationSubscriptionIdList = notificationSubscriptionDisplayList.GetNotificationSubscriptionIdList();

                notificationSubscriptionChangeProperties.ChangeNotificationSubscriptionType = Enumerations.ChangeNotificationSubscriptionType.ChangeStatus;
                notificationSubscriptionChangeProperties.NotificationSubscriptionStatus = Enumerations.NotificationSubscriptionStatus.Disabled;

                this.NotificationAdminPresenter.ChangeNotificationSubscriptionList(notificationSubscriptionIdList,
                                                                                   notificationSubscriptionChangeProperties);
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Notification subscription change enable tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotificationSubscriptionChangeEnableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NotificationSubscriptionDisplayList notificationSubscriptionDisplayList = null;
            List<string> notificationSubscriptionIdList = null;
            NotificationSubscriptionChangeProperties notificationSubscriptionChangeProperties = null;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                notificationSubscriptionIdList = new List<string>();
                notificationSubscriptionChangeProperties = new NotificationSubscriptionChangeProperties();

                notificationSubscriptionDisplayList = this.GetSelectedNotificationSubscriptionDisplayList();
                if (notificationSubscriptionDisplayList == null ||
                    notificationSubscriptionDisplayList.Count <= 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_NotificationSubscriptionNotSelected));
                    return;
                }

                notificationSubscriptionIdList = notificationSubscriptionDisplayList.GetNotificationSubscriptionIdList();

                notificationSubscriptionChangeProperties.ChangeNotificationSubscriptionType = Enumerations.ChangeNotificationSubscriptionType.ChangeStatus;
                notificationSubscriptionChangeProperties.NotificationSubscriptionStatus = Enumerations.NotificationSubscriptionStatus.Enabled;

                this.NotificationAdminPresenter.ChangeNotificationSubscriptionList(notificationSubscriptionIdList,
                                                                                   notificationSubscriptionChangeProperties);

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }
    }

}
