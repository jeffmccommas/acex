﻿using Aclara.Vso.Build.Client.Models;
using Aclara.Vso.Build.Client.Types;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using NLog;
using System;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class DefinitionExportImportForm : Form, IDefinitionExportImportView, ISidekickView, ISidekickInfo
    {

        #region Private Constants

        private const string SidekickView_SidekickName = "ExportImportDefinition";
        private const string SidekickView_SidekickDescription = "Export / Import Definition";

        private const string DefinitionType_BuildDefinition = "Build Definition";
        private const string DefinitionType_ReleaseDefinition = "Release Definition";

        private const string OperationType_Export = "Export";
        private const string OperationType_Import = "Import";

        private const string BackgroundProcessButtonExport_Ready = "Export";
        private const string BackgroundProcessButtonExport_Running = "Cancel";
        private const string BackgroundProcessButtonImport_Ready = "Import";
        private const string BackgroundProcessButtonImport_Running = "Cancel";

        private const string PropertiesHeaderFormat = "{0} {1}(s)";

        private const string ExportZipArchiveStatus_Ready = "Definition archive is ready. Click Save.";
        private const string ExportZipArchiveStatus_NotReady = "Definition archive is not ready.";

        private const string ExportZipFileNamePrefixPattern = "BuildDefinitionArchive_{0}.zip";
        private const string ExportZipFileNameDatePattern = "yyyyMMddHHmmss";
        private const string ExportZipFileFolderLocationPrompt = "Select folder to save zip file.";

        private const string ImportDefinitionZipArchiveContentsDataGridViewColumn_Check = "CheckColumn";
        private const string ImportDefinitionZipArchiveContentsDataGridViewColumn_Name = "NameColumn";
        private const string ImportDefinitionZipArchiveContentsDataGridViewColumn_Extra = "ExtraColumn";

        private const string ZipArchiveEntryDisplayProperty_Selected = "Selected";
        private const string ZipArchiveEntryDisplayProperty_Name = "Name";
        private const string ZipArchiveEntryDisplayProperty_Extra = "Extra";

        private const string JSON_File_Extention = ".json";

        private const string ZipArchiveContents_Count = "({0}):";
        private const string ValidationErrorMessage_TeamProjectNameNotSelected = "Team project not selected.";
        private const string ValidationErrorMessage_BranchNameSourceNotEntered = "Branch name source not entered.";
        private const string ValidationErrorMessage_BuildDefinitionNotSelected = "Build definition not selected.";
        private const string ValidationErrorMessage_ReleaseDefinitionNotSelected = "Release definition not selected.";
        private const string ValidationErrorMessage_DefinitionZipArchiveIsEmpty = "Definition zip archive is empty.";
        private const string ValidationErrorMessage_DefinitionTypeReleaseDefinition_NotSupported = "Definition Type: Release Definition is not supported.";
        private const string ValidationErrorMessage_ZipArchiveFileDoesNotExist = "Zip archive file does not exist. (File: {0})";
        private const string ValidationErrorMessage_NoZipArchiveEntryDisplaySelected = "At least one definition from zip archive must be selected.";
        private const string ValidationErrorMessage_RepositoryTypeValidSelection = "Select Team Foundation Version Control or Git. No other repository types are supported.";

        private const string ImportBuildDefinitionFolder_Name = "Name";

        private const string LogEntry_ZipArchiveFile_Saved = "Exported definitions saved to zip archive file. (File name: {0})";

        #endregion

        #region Public Enumerations

        public enum DefinitionTypes
        {
            BuildDefinition = 1,
            ReleaseDefintion = 2
        }

        public enum OperationTypes
        {
            Export = 1,
            Import = 2
        }

        #endregion

        #region Private Data Members

        private CustomLogger _logger = null;
        private SidekickConfiguration _sidekickConfiguration = null;
        private BuildDefinitionSearchForm _buildDefinitionSearchForm = null;
        private ReleaseDefinitionSearchForm _releaseDefinitionSearchForm = null;
        private DefinitionExportImportPresenter _definitionExportImportPresenter;
        private MemoryStream _exportDefinitionsZipArchiveMemoryStream;
        private bool _oneOrMoreBuildDefinitionsSelected;
        private bool _oneOrMoreReleaseDefinitionsSelected;
        private ZipArchiveEntryDisplayList _importDefinitionsZipArchiveList = null;
        private SortableBindingList<ZipArchiveEntryDisplay> _zipArchiveEntryListBindingSource;
        private System.Windows.Forms.SortOrder _importDefinitionsZipArchiveContentsDataGridViewSortOrder;
        private DataGridViewColumn _importDefinitionsZipArchiveContentsDataGridViewSortedColumn;
        private ZipArchive _importDefinitionsZipArchive;
        private GitRepositorySelectorList _gitRepositoryList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.SidekickName,
                                               this.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickView_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickView_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                if (this.DefinitionExportImportPresenter.IsBackgroundTaskBusy == false)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick warning level.
        /// </summary>
        public SidekickContainerTypes.SidekickWarningLevel SidekickWarningLevel
        {
            get
            {
                return SidekickContainerTypes.SidekickWarningLevel.Low;
            }
        }

        /// <summary>
        /// Property: Build defintion change queue status presenter.
        /// </summary>
        public DefinitionExportImportPresenter DefinitionExportImportPresenter
        {
            get { return _definitionExportImportPresenter; }
            set
            {
                _definitionExportImportPresenter = value;
                this.DefinitionExportImportPresenter.BackgroundTaskStateChanged += OnBackgroundTaskStateChanged;
            }
        }

        /// <summary>
        /// Property: Build definition search form.
        /// </summary>
        public BuildDefinitionSearchForm BuildDefinitionSearchForm
        {
            get { return _buildDefinitionSearchForm; }
            set { _buildDefinitionSearchForm = value; }
        }

        /// <summary>
        /// Property: Release definition search form.
        /// </summary>
        public ReleaseDefinitionSearchForm ReleaseDefinitionSearchForm
        {
            get { return _releaseDefinitionSearchForm; }
            set { _releaseDefinitionSearchForm = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Import definitions zip archive entry display list.
        /// </summary>
        public ZipArchiveEntryDisplayList ImportDefinitionsZipArchiveList
        {
            get
            {
                return _importDefinitionsZipArchiveList;
            }
            set
            {
                _importDefinitionsZipArchiveList = value;
            }
        }

        /// <summary>
        /// Property: Git repository list.
        /// </summary>
        public GitRepositorySelectorList RepositoryNameList
        {
            get
            {
                if (_gitRepositoryList == null &&
                    this.DefinitionExportImportPresenter != null)
                {
                    _gitRepositoryList = this.DefinitionExportImportPresenter.GetGitRepositorySelectorList();
                }
                return _gitRepositoryList;
            }
            set { _gitRepositoryList = value; }
        }

        /// <summary>
        /// Property: Zip archive entry display list binding source.
        /// </summary>
        public SortableBindingList<ZipArchiveEntryDisplay> BuildDefinitionSelectorListBindingSource
        {
            get { return _zipArchiveEntryListBindingSource; }
            set { _zipArchiveEntryListBindingSource = value; }
        }

        /// <summary>
        /// Property: Import definition zip archive contents data grid view sort order.
        /// </summary>
        public System.Windows.Forms.SortOrder ImportDefinitionZipArchiveContentsDataGridViewSortOrder
        {
            get { return _importDefinitionsZipArchiveContentsDataGridViewSortOrder; }
            set { _importDefinitionsZipArchiveContentsDataGridViewSortOrder = value; }
        }

        /// <summary>
        /// Property: Import definition zip archive contents data grid view sorted column.
        /// </summary>
        public DataGridViewColumn ImportDefinitionZipArchiveContentsDataGridViewColumn
        {
            get { return _importDefinitionsZipArchiveContentsDataGridViewSortedColumn; }
            set { _importDefinitionsZipArchiveContentsDataGridViewSortedColumn = value; }
        }

        /// <summary>
        /// Property: Definition type.
        /// </summary>
        public DefinitionTypes DefinitionType
        {

            get
            {
                DefinitionTypes definitionType = DefinitionTypes.BuildDefinition;

                if (this.DefinitionTypeComboBox.SelectedItem is DefinitionTypes)
                {
                    definitionType = (DefinitionTypes)this.DefinitionTypeComboBox.SelectedItem;
                }
                return definitionType;
            }
            set
            {
                this.DefinitionTypeComboBox.SelectedItem = value;
            }
        }

        /// <summary>
        /// Property: Operation type.
        /// </summary>
        public OperationTypes OperationType
        {
            get
            {
                OperationTypes definitionType = OperationTypes.Export;

                if (this.DefinitionTypeComboBox.SelectedItem is DefinitionTypes)
                {
                    definitionType = (OperationTypes)this.OperationTypeComboBox.SelectedItem;
                }
                return definitionType;
            }
            set { this.OperationTypeComboBox.SelectedItem = value; }
        }

        /// <summary>
        /// Property: Properties header.
        /// </summary>
        public string PropertiesHeader
        {
            get
            {
                return this.PropertiesHeaderLabel.Text;
            }
            set
            {
                this.PropertiesHeaderLabel.Text = value;
            }
        }

        /// <summary>
        /// Property: Import zip file location.
        /// </summary>
        public string ImportZipFileLocation
        {
            get
            {
                return this.ImportZipFileLocationTextBox.Text;
            }
            set
            {
                ImportZipFileLocationTextBox.Text = value;
            }
        }

        /// <summary>
        /// Property: Zip archive status.
        /// </summary>
        public string ZipArchiveStatus
        {
            get { return this.ExportZipArchiveStatusValueLabel.Text; }
            set { this.ExportZipArchiveStatusValueLabel.Text = value; }
        }

        /// <summary>
        /// Property: Import definition name prefix.
        /// </summary>
        public string ImportDefinitionNamePrefix
        {
            get { return this.ImportDefinitionNamePrefixTextBox.Text; }
            set { this.ImportDefinitionNamePrefixTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Repository type.
        /// </summary>
        public RepositoryType RepositoryType
        {
            get
            {
                return (RepositoryType)ImportRepositoryTypeComboBox.SelectedValue;
            }
            set
            {
                ImportRepositoryTypeComboBox.SelectedValue = value;
            }
        }
        /// <summary>
        /// Property: Reset after save.
        /// </summary>
        public bool ResetAfterSave
        {
            get { return this.ResetAfterSaveCheckBox.Checked; }
            set { this.ResetAfterSaveCheckBox.Checked = value; }
        }

        /// <summary>
        /// Property: Build definition folder.
        /// </summary>
        public BuildDefinitionFolder ImportBuildDefinitionFolder
        {

            get
            {
                BuildDefinitionFolder importBuildDefinitionFolder = null;
                if (this.ImportBuildDefinitionFolderComboBox.SelectedItem.GetType() == typeof(BuildDefinitionFolder))
                {
                    importBuildDefinitionFolder = (BuildDefinitionFolder)this.ImportBuildDefinitionFolderComboBox.SelectedItem;
                }
                return importBuildDefinitionFolder;
            }
            set
            {
                this.ImportBuildDefinitionFolderComboBox.SelectedValue = value;
            }
        }
        /// <summary>
        /// Property: Export definitions zip archive memory stream.
        /// </summary>
        public MemoryStream ExportDefinitionsZipArchiveMemoryStream
        {
            get { return _exportDefinitionsZipArchiveMemoryStream; }
            set { _exportDefinitionsZipArchiveMemoryStream = value; }
        }

        /// <summary>
        /// Property: Import definitions zip archive.
        /// </summary>
        public ZipArchive ImportDefinitionsZipArchive
        {
            get { return _importDefinitionsZipArchive; }
            set { _importDefinitionsZipArchive = value; }
        }

        /// <summary>
        /// Property: One or more build definition(s) selected.
        /// </summary>
        public bool OneOrMoreBuildDefinitionsSelected
        {
            get { return _oneOrMoreBuildDefinitionsSelected; }
            set { _oneOrMoreBuildDefinitionsSelected = value; }
        }

        /// <summary>
        /// Property: One or more release definition(s) selected.
        /// </summary>
        public bool OneOrMoreReleaseDefinitionsSelected
        {
            get { return _oneOrMoreReleaseDefinitionsSelected; }
            set { _oneOrMoreReleaseDefinitionsSelected = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DefinitionExportImportForm()
        {
            InitializeComponent();


        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        public DefinitionExportImportForm(SidekickConfiguration sidekickConfiguration,
                                          BuildDefinitionSearchForm buildDefinitionSearchForm,
                                          ReleaseDefinitionSearchForm releaseDefintionSearchForm)
        {
            _exportDefinitionsZipArchiveMemoryStream = new MemoryStream();
            _importDefinitionsZipArchive = null;
            _importDefinitionsZipArchiveList = new ZipArchiveEntryDisplayList();

            InitializeComponent();

            this.SidekickConfiguration = sidekickConfiguration;

            this.InitializeControls();

            this.BuildDefinitionSearchForm = buildDefinitionSearchForm;
            this.ReleaseDefinitionSearchForm = releaseDefintionSearchForm;

            this.BuildDefinitionSearchForm.BuildDefinitionSelectionChanged += OnBuildDefinitionSelectionChanged;
            this.BuildDefinitionSearchPanel.DockControl(this.BuildDefinitionSearchForm);

            this.ReleaseDefinitionSearchForm.ReleaseDefinitionSelectionChanged += OnReleaseDefinitionSelectionChanged;
            this.ReleaseDefinitionSearchPanel.DockControl(this.ReleaseDefinitionSearchForm);

            _zipArchiveEntryListBindingSource = new SortableBindingList<ZipArchiveEntryDisplay>(_importDefinitionsZipArchiveList);
            this.ImportDefinitionZipArchiveContentsDataGridView.DataSource = _zipArchiveEntryListBindingSource;

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update zip archive memory stream.
        /// </summary>
        /// <param name="zipArchiveMemoryStream"></param>
        public void UpdateExportZipArchiveMemoryStream(MemoryStream zipArchiveMemoryStream)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<MemoryStream>(this.UpdateExportZipArchiveMemoryStream), zipArchiveMemoryStream);
            }
            else
            {
                this.ExportDefinitionsZipArchiveMemoryStream = zipArchiveMemoryStream;

                if (_exportDefinitionsZipArchiveMemoryStream != null &&
                    _exportDefinitionsZipArchiveMemoryStream.Length != 0)
                {
                    this.UpdateZipArchiveStatus(ExportZipArchiveStatus_Ready);
                    PopulateExportDefinitionZipArchiveContents();
                }
                else
                {
                    this.UpdateZipArchiveStatus(ExportZipArchiveStatus_NotReady);
                }
            }
        }

        /// <summary>
        /// Update progress of export/import status.
        /// </summary>
        /// <param name="buildDefinitionChangedCount"></param>
        /// <param name="buildDefinitionTotalCount"></param>
        public void UpdateProgressExportImportStatus(int buildDefinitionChangedCount, int buildDefinitionTotalCount)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<int, int>(this.UpdateProgressExportImportStatus), buildDefinitionChangedCount, buildDefinitionTotalCount);
            }
            else
            {
                if (buildDefinitionChangedCount < buildDefinitionTotalCount)
                {
                    this.ExportImportStatusProgressBar.Visible = true;
                    this.ExportImportStatusProgressBar.Minimum = 0;
                    this.ExportImportStatusProgressBar.Maximum = buildDefinitionTotalCount;
                    this.ExportImportStatusProgressBar.Value = buildDefinitionChangedCount;
                }
                else if (buildDefinitionChangedCount == buildDefinitionTotalCount)
                {
                    this.ExportImportStatusProgressBar.Visible = false;
                }
            }
        }

        /// <summary>
        /// Retrieve remaining build definitions.
        /// </summary>
        public void RetrieveRemainingDefinitions()
        {
            this.BuildDefinitionSearchForm.RetrieveRemainingDefinitions();
        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            this.DefinitionExportImportPresenter.TeamProjectChanged();
            this.UpdateControlsDependantOnTeamProject();
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ExportButton.Text = BackgroundProcessButtonExport_Ready;
            }
        }

        /// <summary>
        /// Background task Completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ExportButton.Text = BackgroundProcessButtonExport_Ready;
            }
        }

        /// <summary>
        /// Display all import definition zip archive contents in data grid view.
        /// </summary>
        protected void ImportDefinitionZipArchiveContentsDisplayAll()
        {
            this.ImportDefinitionZipArchiveContentsDataGridView.CurrentCell = null;

            foreach (DataGridViewRow dataGridViewRow in this.ImportDefinitionZipArchiveContentsDataGridView.Rows)
            {
                dataGridViewRow.Visible = true;
            }
        }

        protected void ImportDefinitionZipArchiveContentsDisplayChecked()
        {
            bool checkedValue = false;
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ImportDefinitionZipArchiveContentsDataGridView.CurrentCell = null;

                foreach (DataGridViewRow dataGridViewRow in this.ImportDefinitionZipArchiveContentsDataGridView.Rows)
                {
                    bool.TryParse(dataGridViewRow.Cells[ImportDefinitionZipArchiveContentsDataGridViewColumn_Check].Value.ToString(),
                                  out checkedValue);

                    if (checkedValue == true)
                    {
                        dataGridViewRow.Visible = true;
                    }
                    else
                    {
                        dataGridViewRow.Visible = false;
                    };

                }
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }
        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            BindingSource importBuildDefinitionFolderBindingSource = null;
            BuildDefinitionFolder importBuildDefinitionFolder = null;

            try
            {
                importBuildDefinitionFolderBindingSource = new BindingSource();

                this.OperationTypeComboBox.FormattingEnabled = true;
                this.OperationTypeComboBox.Format += delegate (object sender, ListControlConvertEventArgs e)
                {
                    e.Value = ConvertOperationTypesToText((OperationTypes)e.Value);
                };
                this.OperationTypeComboBox.DataSource = Enum.GetValues(typeof(DefinitionTypes));

                this.OperationTypeComboBox.SelectedItem = OperationTypes.Export;

                this.DefinitionTypeComboBox.FormattingEnabled = true;
                this.DefinitionTypeComboBox.Format += delegate (object sender, ListControlConvertEventArgs e)
                {
                    e.Value = ConvertDefinitionTypesToText((DefinitionTypes)e.Value);
                };
                this.DefinitionTypeComboBox.DataSource = Enum.GetValues(typeof(DefinitionTypes));

                this.DefinitionTypeComboBox.SelectedItem = DefinitionTypes.BuildDefinition;

                this.ResetAfterSave = true;
                this.InitializeImportDefinitionZipArchiveContentsDataGridView();

                importBuildDefinitionFolderBindingSource.DataSource = this.SidekickConfiguration.BuildDefinitionFolders.BuildDefinitionFolder;
                this.ImportBuildDefinitionFolderComboBox.DataSource = importBuildDefinitionFolderBindingSource;
                this.ImportBuildDefinitionFolderComboBox.DisplayMember = ImportBuildDefinitionFolder_Name;
                this.ImportBuildDefinitionFolderComboBox.ValueMember = ImportBuildDefinitionFolder_Name;

                importBuildDefinitionFolder = this.SidekickConfiguration.BuildDefinitionFolders.BuildDefinitionFolder.Single(bdf => bdf.IsDefault == true);
                if (importBuildDefinitionFolder != null)
                {
                    this.ImportBuildDefinitionFolderComboBox.SelectedValue = importBuildDefinitionFolder.Name;
                }

                this.ImportRepositoryTypeComboBox.FormattingEnabled = true;
                this.ImportRepositoryTypeComboBox.Format += delegate (object sender, ListControlConvertEventArgs e)
                {
                    e.Value = ConvertRepositoryTypeToText((RepositoryType)e.Value);
                };
                this.ImportRepositoryTypeComboBox.DataSource = Enum.GetValues(typeof(RepositoryType));

                this.ImportRepositoryTypeComboBox.SelectedItem = RepositoryType.SourceBuildDefinitionRepositoryType;

                this.UpdateControlsBasedOnDependencies();

                this.UpdatePropertiesHeader();

                this.SwitchExportOperationView();

                this.UpdateZipArchiveStatus(ExportZipArchiveStatus_NotReady);

                this.ExportImportStatusProgressBar.Visible = false;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Hanlder: Build definition selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="requestBuildEventArgs"></param>
        protected void OnBuildDefinitionSelectionChanged(Object sender,
                                                         BuildDefinitionSelectionChangedEventArgs buildDefinitionSelectionChangedEventArgs)
        {

            try
            {
                if (buildDefinitionSelectionChangedEventArgs != null)
                {
                    if (buildDefinitionSelectionChangedEventArgs.OneOrMoreSelected == true)
                    {
                        this.OneOrMoreBuildDefinitionsSelected = true;
                    }
                    else
                    {
                        this.OneOrMoreBuildDefinitionsSelected = false;
                    }
                    this.UpdateControlsBasedOnDependencies();
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Hanlder: Release definition selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="requestBuildEventArgs"></param>
        protected void OnReleaseDefinitionSelectionChanged(Object sender,
                                                           ReleaseDefinitionSelectionChangedEventArgs releaseDefinitionSelectionChangedEventArgs)
        {

            try
            {
                if (releaseDefinitionSelectionChangedEventArgs != null)
                {
                    if (releaseDefinitionSelectionChangedEventArgs.OneOrMoreSelected == true)
                    {
                        this.OneOrMoreReleaseDefinitionsSelected = true;
                    }
                    else
                    {
                        this.OneOrMoreReleaseDefinitionsSelected = true;
                    }
                    this.UpdateControlsBasedOnDependencies();
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Update controls based on dependencies
        /// </summary>
        /// <param name="enable"></param>
        protected void UpdateControlsBasedOnDependencies()
        {

            try
            {
                switch (this.OperationType)
                {
                    case OperationTypes.Export:
                        switch (this.DefinitionType)
                        {
                            case DefinitionTypes.BuildDefinition:
                            case DefinitionTypes.ReleaseDefintion:

                                //Update "Save" button.
                                if (this.ExportDefinitionsZipArchiveMemoryStream != null &&
                                    this.ExportDefinitionsZipArchiveMemoryStream.Length != 0)
                                {
                                    this.SaveButton.Enabled = true;
                                }
                                else
                                {
                                    this.SaveButton.Enabled = false;
                                }

                                //Update "Export" button.
                                if (this.OneOrMoreBuildDefinitionsSelected == true)
                                {
                                    this.ExportButton.Enabled = true;
                                }
                                else
                                {
                                    this.ExportButton.Enabled = false;
                                }
                                if (this.DefinitionExportImportPresenter != null &&
                                    this.DefinitionExportImportPresenter.IsBackgroundTaskBusy == true)
                                {
                                    this.ExportButton.Text = BackgroundProcessButtonExport_Running;
                                }
                                else
                                {
                                    this.ExportButton.Text = BackgroundProcessButtonExport_Ready;
                                }

                                //Update "Reset" button.;
                                if (this.DefinitionExportImportPresenter != null &&
                                    this.DefinitionExportImportPresenter.IsBackgroundTaskBusy == true)
                                {
                                    this.ResetButton.Enabled = false;
                                }
                                else
                                {
                                    this.ResetButton.Enabled = true;
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    case OperationTypes.Import:
                        switch (this.DefinitionType)
                        {
                            case DefinitionTypes.BuildDefinition:
                            case DefinitionTypes.ReleaseDefintion:

                                //Update "Open" button.
                                if (string.IsNullOrEmpty(this.ImportZipFileLocation) != true)
                                {
                                    this.OpenImportZipFileButton.Enabled = true;
                                }
                                else
                                {
                                    this.OpenImportZipFileButton.Enabled = false;
                                }

                                //Update "Check" button.
                                if (ImportDefinitionZipArchiveContentsDataGridView.Rows.Count > 0)
                                {
                                    this.ImportCheckSplitButton.Enabled = true;
                                }
                                else
                                {
                                    this.ImportCheckSplitButton.Enabled = false;
                                }

                                //Update "Display" button.
                                if (ImportDefinitionZipArchiveContentsDataGridView.Rows.Count > 0)
                                {
                                    this.ImportDisplaySplitButton.Enabled = true;
                                }
                                else
                                {
                                    this.ImportDisplaySplitButton.Enabled = false;
                                }

                                //Update import "Copy" button.
                                if (this.GetSelectedZipArchiveEntryDisplayList().Count > 0)
                                {
                                    this.ImportCopySplitButton.Enabled = true;
                                }
                                else
                                {
                                    this.ImportCopySplitButton.Enabled = false;
                                }

                                //Update "Import Definition Zip Archive Contents" data grid view.
                                if (ImportDefinitionZipArchiveContentsDataGridView.Rows.Count > 0)
                                {
                                    this.ImportDefinitionZipArchiveContentsDataGridView.Enabled = true;
                                }
                                else
                                {
                                    this.ImportDefinitionZipArchiveContentsDataGridView.Enabled = true;
                                }

                                //Update "Import definition name prefix" text box.
                                if (ImportDefinitionZipArchiveContentsDataGridView.Rows.Count > 0)
                                {
                                    this.ImportDefinitionNamePrefixTextBox.Enabled = true;
                                }
                                else
                                {
                                    this.ImportDefinitionNamePrefixTextBox.Enabled = false;
                                }

                                //Update "Import build definition folder" combo box.
                                if (ImportDefinitionZipArchiveContentsDataGridView.Rows.Count > 0)
                                {
                                    this.ImportBuildDefinitionFolderComboBox.Enabled = true;
                                }
                                else
                                {
                                    this.ImportBuildDefinitionFolderComboBox.Enabled = false;
                                }

                                //Update "Import repository type" combo box.
                                if (ImportDefinitionZipArchiveContentsDataGridView.Rows.Count > 0)
                                {
                                    this.ImportRepositoryTypeComboBox.Enabled = true;
                                }
                                else
                                {
                                    this.ImportRepositoryTypeComboBox.Enabled = false;
                                }

                                //Update "Import repository" combo box.
                                if (ImportDefinitionZipArchiveContentsDataGridView.Rows.Count > 0 &&
                                    (RepositoryType)this.ImportRepositoryTypeComboBox.SelectedItem == RepositoryType.Git)
                                {
                                    this.ImportRepositoryComboBox.Enabled = true;
                                }
                                else
                                {
                                    this.ImportRepositoryComboBox.Enabled = false;
                                }

                                //Update "Import migrate to git (from TFVC)" check box.
                                if (this.ImportDefinitionZipArchiveContentsDataGridView.Rows.Count > 0 &&
                                    (RepositoryType)this.ImportRepositoryTypeComboBox.SelectedItem == RepositoryType.Git)
                                {
                                    this.ImportMigrateToGitCheckBox.Enabled = true;
                                }
                                else
                                {
                                    this.ImportMigrateToGitCheckBox.Enabled = false;
                                }

                                //Update "Import" button.
                                if (this.GetSelectedZipArchiveEntryDisplayList().Count > 0)
                                {
                                    this.ImportButton.Enabled = true;
                                }
                                else
                                {
                                    this.ImportButton.Enabled = false;
                                }
                                if (this.DefinitionExportImportPresenter != null &&
                                    this.DefinitionExportImportPresenter.IsBackgroundTaskBusy == true)
                                {
                                    this.ImportButton.Text = BackgroundProcessButtonImport_Running;
                                }
                                else
                                {
                                    this.ImportButton.Text = BackgroundProcessButtonImport_Ready;
                                }

                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        // Convert definition types enum value to text.
        /// </summary>
        /// <param name="definitionTypes"></param>
        /// <returns></returns>
        protected string ConvertDefinitionTypesToText(DefinitionTypes definitionTypes)
        {
            string result = string.Empty;

            try
            {
                switch (definitionTypes)
                {
                    case DefinitionTypes.BuildDefinition:
                        result = DefinitionType_BuildDefinition;
                        break;
                    case DefinitionTypes.ReleaseDefintion:
                        result = DefinitionType_ReleaseDefinition;
                        break;
                    default:
                        result = DefinitionType_BuildDefinition;
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Clear zip archive memory stream.
        /// </summary>
        protected void ClearZipArchiveMemoryStream()
        {
            try
            {
                this.ExportDefinitionsZipArchiveMemoryStream.SetLength(0);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Convert export import types enum to text. 
        /// </summary>
        /// <param name="operationTypes"></param>
        /// <returns></returns>
        protected string ConvertOperationTypesToText(OperationTypes operationTypes)
        {
            string result = string.Empty;

            try
            {
                switch (operationTypes)
                {
                    case OperationTypes.Export:
                        result = OperationType_Export;
                        break;
                    case OperationTypes.Import:
                        result = OperationType_Import;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        /// <summary>
        /// Switch operation selection view.
        /// </summary>
        protected void SwitchOperationView()
        {

            OperationTypes definitionType;

            if (this.OperationTypeComboBox.SelectedItem is DefinitionTypes)
            {
                definitionType = this.OperationType;

                switch (definitionType)
                {
                    case OperationTypes.Export:
                        this.SwitchExportOperationView();
                        break;
                    case OperationTypes.Import:
                        this.SwitchImportOperationView();
                        break;
                    default:
                        break;
                }
            }

        }

        /// <summary>
        /// Switch export operation view.
        /// </summary>
        protected void SwitchExportOperationView()
        {
            try
            {
                this.ExportOperationPanel.Visible = true;
                this.ImportOperationPanel.Visible = false;

                switch (this.DefinitionType)
                {
                    case DefinitionTypes.BuildDefinition:
                        this.BuildDefinitionSelectionPanel.Visible = true;
                        this.ReleaseDefinitionSelectionPanel.Visible = false;
                        break;
                    case DefinitionTypes.ReleaseDefintion:
                        this.BuildDefinitionSelectionPanel.Visible = false;
                        this.ReleaseDefinitionSelectionPanel.Visible = true;
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Switch import operation view.
        /// </summary>
        protected void SwitchImportOperationView()
        {
            try
            {
                this.ExportOperationPanel.Visible = false;
                this.ImportOperationPanel.Visible = true;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update properties header (text).
        /// </summary>
        protected void UpdatePropertiesHeader()
        {
            string operationType = string.Empty;
            string definitionType = string.Empty;

            try
            {
                operationType = ConvertOperationTypesToText(this.OperationType);
                definitionType = ConvertDefinitionTypesToText(this.DefinitionType);

                this.PropertiesHeader = string.Format(PropertiesHeaderFormat,
                                                      operationType,
                                                      definitionType);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update zip archive status.
        /// </summary>
        /// <param name="zipArchiveStatus"></param>
        protected void UpdateZipArchiveStatus(string zipArchiveStatus)
        {

            try
            {
                this.ZipArchiveStatus = zipArchiveStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Background task state changed.
        /// </summary>
        protected void OnBackgroundTaskStateChanged(Object sender,
                                                    BackgroundTaskStateChangedEventArgs backgroundTaskStateChangedEventArgs)
        {
            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke(new Action<Object, BackgroundTaskStateChangedEventArgs>(this.OnBackgroundTaskStateChanged), sender, backgroundTaskStateChangedEventArgs);
                }
                else
                {
                    this.UpdateControlsBasedOnDependencies();

                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }




        }

        /// <summary>
        /// Populate definition zip archive list.
        /// </summary>
        protected void PopulateExportDefinitionZipArchiveContents()
        {
            ZipArchive zipArchive = null;

            try
            {
                this.ExportDefinitionZipArchiveListBox.Items.Clear();

                if (this.ExportDefinitionsZipArchiveMemoryStream != null &&
                    this.ExportDefinitionsZipArchiveMemoryStream.Length > 0)
                {
                    zipArchive = new ZipArchive(this.ExportDefinitionsZipArchiveMemoryStream, ZipArchiveMode.Read);
                    foreach (ZipArchiveEntry zipArchiveEntry in zipArchive.Entries)
                    {
                        this.ExportDefinitionZipArchiveListBox.Items.Add(zipArchiveEntry.Name);

                    }
                    //Update "Definition Zip Archive Contents (count).

                    this.DefinitionZipArchiveEntryCountLLabel.Text = string.Format(ZipArchiveContents_Count,
                                                                                   zipArchive.Entries.Count);

                }
                else
                {
                    //Update "Definition Zip Archive Contents (count).

                    this.DefinitionZipArchiveEntryCountLLabel.Text = string.Format(ZipArchiveContents_Count,
                                                                                   0);

                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Reset all.
        /// </summary>
        protected void ResetAll()
        {
            try
            {
                this.ClearZipArchiveMemoryStream();
                this.UpdateZipArchiveStatus(ExportZipArchiveStatus_NotReady);
                this.PopulateExportDefinitionZipArchiveContents();
                this.UpdateControlsBasedOnDependencies();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Populate Import definitions zip archive list.
        /// </summary>
        protected void PopulateImportDefinitionsZipArchiveList()
        {
            ZipArchiveEntryDisplay zipArchiveEntryDisplay = null;

            try
            {
                this.ImportDefinitionsZipArchiveList.Clear();
                if (this.ImportDefinitionsZipArchive != null)
                {
                    foreach (ZipArchiveEntry zipArchiveEntry in this.ImportDefinitionsZipArchive.Entries)
                    {
                        zipArchiveEntryDisplay = new ZipArchiveEntryDisplay(zipArchiveEntry);
                        this.ImportDefinitionsZipArchiveList.Add(zipArchiveEntryDisplay);
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Populate import definition zip archive contents.
        /// </summary>
        protected void PopulateImportDefinitionZipArchiveContents()
        {
            ListSortDirection sortDirection;

            try
            {
                if (this.ImportDefinitionZipArchiveContentsDataGridView.SortedColumn == null)
                {
                    this.ImportDefinitionZipArchiveContentsDataGridView.Sort(this.ImportDefinitionZipArchiveContentsDataGridView.Columns[ImportDefinitionZipArchiveContentsDataGridViewColumn_Name],
                                                             ListSortDirection.Ascending);
                }

                this.ImportDefinitionZipArchiveContentsDataGridViewSortOrder = this.ImportDefinitionZipArchiveContentsDataGridView.SortOrder;
                this.ImportDefinitionZipArchiveContentsDataGridViewColumn = this.ImportDefinitionZipArchiveContentsDataGridView.SortedColumn;

                this.BuildDefinitionSelectorListBindingSource = new SortableBindingList<ZipArchiveEntryDisplay>(this.ImportDefinitionsZipArchiveList);
                this.ImportDefinitionZipArchiveContentsDataGridView.DataSource = this.BuildDefinitionSelectorListBindingSource;

                foreach (DataGridViewColumn column in this.ImportDefinitionZipArchiveContentsDataGridView.Columns)
                {

                    switch (column.Name)
                    {
                        case ImportDefinitionZipArchiveContentsDataGridViewColumn_Check:
                            break;
                        case ImportDefinitionZipArchiveContentsDataGridViewColumn_Name:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            break;
                        case ImportDefinitionZipArchiveContentsDataGridViewColumn_Extra:
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            break;
                    }
                }

                for (int columnIndex = 0; columnIndex < ImportDefinitionZipArchiveContentsDataGridView.Columns.Count; columnIndex++)
                {
                    int columnWidth = ImportDefinitionZipArchiveContentsDataGridView.Columns[columnIndex].Width;
                    ImportDefinitionZipArchiveContentsDataGridView.Columns[columnIndex].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    ImportDefinitionZipArchiveContentsDataGridView.Columns[columnIndex].Width = columnWidth;
                }

                //Restore sort order.
                if (this.ImportDefinitionZipArchiveContentsDataGridViewSortOrder == SortOrder.Ascending)
                {
                    sortDirection = ListSortDirection.Ascending;
                }
                else
                {
                    sortDirection = ListSortDirection.Descending;
                }

                if (this.ImportDefinitionZipArchiveContentsDataGridViewColumn != null)
                {
                    this.ImportDefinitionZipArchiveContentsDataGridView.Sort(this.ImportDefinitionZipArchiveContentsDataGridView.Columns[this.ImportDefinitionZipArchiveContentsDataGridViewColumn.Name], sortDirection); ;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Initialize import definitions zip archive contents data grid view.
        /// </summary>
        protected void InitializeImportDefinitionZipArchiveContentsDataGridView()
        {
            BindingSource bindingSource = null;

            try
            {

                this.ImportDefinitionZipArchiveContentsDataGridView.DataSource = null;
                this.ImportDefinitionZipArchiveContentsDataGridView.AutoGenerateColumns = false;
                this.ImportDefinitionZipArchiveContentsDataGridView.DataSource = bindingSource;

                foreach (DataGridViewColumn column in this.ImportDefinitionZipArchiveContentsDataGridView.Columns)
                {

                    switch (column.Name)
                    {
                        case ImportDefinitionZipArchiveContentsDataGridViewColumn_Check:
                            column.DataPropertyName = ZipArchiveEntryDisplayProperty_Selected;
                            break;
                        case ImportDefinitionZipArchiveContentsDataGridViewColumn_Name:
                            column.DataPropertyName = ZipArchiveEntryDisplayProperty_Name;
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                            break;
                        case ImportDefinitionZipArchiveContentsDataGridViewColumn_Extra:
                            column.DataPropertyName = ZipArchiveEntryDisplayProperty_Extra;
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            column.HeaderText = string.Empty;
                            break;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Retrieve selected import definitions zip archive entry display list.
        /// </summary>
        /// <returns></returns>
        protected ZipArchiveEntryDisplayList GetSelectedZipArchiveEntryDisplayList()
        {
            ZipArchiveEntryDisplayList result = null;
            bool checkedValue = false;
            string name = string.Empty;
            ZipArchiveEntryDisplay zipArchiveEntryDisplay = null;

            try
            {
                result = new ZipArchiveEntryDisplayList();

                foreach (DataGridViewRow dataGridViewRow in this.ImportDefinitionZipArchiveContentsDataGridView.Rows)
                {
                    if (dataGridViewRow.Cells[ImportDefinitionZipArchiveContentsDataGridViewColumn_Check].Value is Boolean)
                    {
                        checkedValue = (bool)dataGridViewRow.Cells[ImportDefinitionZipArchiveContentsDataGridViewColumn_Check].Value;
                        name = (string)dataGridViewRow.Cells[ImportDefinitionZipArchiveContentsDataGridViewColumn_Name].Value;
                        if (checkedValue == true)
                        {
                            zipArchiveEntryDisplay = (ZipArchiveEntryDisplay)dataGridViewRow.DataBoundItem;
                            result.Add(zipArchiveEntryDisplay);
                        }
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
            return result;

        }

        /// <summary>
        /// Import definition zip archive contents check none.
        /// </summary>
        protected void ImportDefinitionZipArchiveContentsCheckNone()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                foreach (DataGridViewRow dataGridViewRow in this.ImportDefinitionZipArchiveContentsDataGridView.Rows)
                {
                    dataGridViewRow.Cells[ImportDefinitionZipArchiveContentsDataGridViewColumn_Check].Value = false;
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Import definition zip archive contents check all.
        /// </summary>
        protected void ImportDefinitionZipArchiveContentsCheckAll()
        {
            ZipArchiveEntryDisplay zipArchiveEntryDisplay = null;

            try
            {
                this.Cursor = Cursors.WaitCursor;
                foreach (DataGridViewRow dataGridViewRow in this.ImportDefinitionZipArchiveContentsDataGridView.Rows)
                {
                    zipArchiveEntryDisplay = (ZipArchiveEntryDisplay)dataGridViewRow.DataBoundItem;
                    if (dataGridViewRow.Visible == true)
                    {
                        dataGridViewRow.Cells[ImportDefinitionZipArchiveContentsDataGridViewColumn_Check].Value = true;
                    }
                }
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Import definition zip archive contents check selected.
        /// </summary>
        protected void ImportDefinitionZipArchiveContentsCheckSelected()
        {

            try
            {
                foreach (DataGridViewRow dataGridViewRow in this.ImportDefinitionZipArchiveContentsDataGridView.SelectedRows)
                {
                    if (dataGridViewRow.Visible == true)
                    {
                        dataGridViewRow.Cells[ImportDefinitionZipArchiveContentsDataGridViewColumn_Check].Value = true;
                    }
                }
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Import definition(s).
        /// </summary>
        protected void ImportDefinitions()
        {
            DefinitionImport definitionImport = null;
            DefinitionImportList definitionImportList = null;
            ZipArchiveEntryDisplayList zipArchiveEntryDisplayList = null;
            BuildDefinitionCloneProperties buildDefinitionCloneProperties = null;
            RepositoryType repositoryType;
            GitRepositorySelector gitRepositorySelector = null;

            try
            {

                buildDefinitionCloneProperties = new BuildDefinitionCloneProperties();

                buildDefinitionCloneProperties.DoNotCloneBuildDefinitions = false;
                buildDefinitionCloneProperties.CloneReleaseDefinitions = false;
                buildDefinitionCloneProperties.CreateMissingReleaseDefinitions = false;
                buildDefinitionCloneProperties.MigrateBuildSteps = false;
                buildDefinitionCloneProperties.RepositoryType = Enumerations.RepositoryType.SourceBuildDefinitionRepositoryType;
                buildDefinitionCloneProperties.BuildDefinitionFolderName = this.ImportBuildDefinitionFolder.Name;

                switch (this.DefinitionType)
                {
                    case DefinitionTypes.BuildDefinition:

                        definitionImportList = new DefinitionImportList();

                        repositoryType = this.RepositoryType;

                        if (repositoryType != RepositoryType.SourceBuildDefinitionRepositoryType &&
                            repositoryType != RepositoryType.TeamFoundationVersionControl &&
                            repositoryType != RepositoryType.Git)
                        {
                            MessageBox.Show(this, string.Format(ValidationErrorMessage_RepositoryTypeValidSelection));
                            return;
                        }

                        buildDefinitionCloneProperties.RepositoryType = RepositoryType;

                        if (repositoryType == RepositoryType.Git)
                        {
                            gitRepositorySelector = (GitRepositorySelector)this.ImportRepositoryComboBox.SelectedItem;

                            if (gitRepositorySelector != null)
                            {
                                buildDefinitionCloneProperties.GitRepository = gitRepositorySelector.GitRepository;
                            }
                        }

                        buildDefinitionCloneProperties.MigrateToGit = this.ImportMigrateToGitCheckBox.Checked;

                        zipArchiveEntryDisplayList = GetSelectedZipArchiveEntryDisplayList();
                        if (zipArchiveEntryDisplayList == null ||
                            zipArchiveEntryDisplayList.Count == 0)
                        {
                            MessageBox.Show(string.Format(ValidationErrorMessage_NoZipArchiveEntryDisplaySelected));
                            return;
                        }

                        foreach (ZipArchiveEntryDisplay zipArchiveEntryDisplay in zipArchiveEntryDisplayList)
                        {
                            definitionImport = new DefinitionImport();

                            definitionImport.Name = string.Format("{0}{1}",
                                                                  ImportDefinitionNamePrefix,
                                                                  zipArchiveEntryDisplay.Name).Replace(JSON_File_Extention, string.Empty);
                            definitionImport.OriginalName = zipArchiveEntryDisplay.Name.Replace(JSON_File_Extention, string.Empty);
                            definitionImport.DefinitionAsJson = zipArchiveEntryDisplay.DefinitionAsJson;

                            definitionImportList.Add(definitionImport);
                        }

                        this.DefinitionExportImportPresenter.ImportBuildDefinitions(definitionImportList,
                                                                                    buildDefinitionCloneProperties);
                        this.UpdateControlsBasedOnDependencies();

                        break;
                    case DefinitionTypes.ReleaseDefintion:

                        break;
                    default:
                        break;
                }

                this.ExportImportStatusProgressBar.Visible = false;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        // Convert repository type enum value to text.
        /// </summary>
        /// <param name="dateFilter"></param>
        /// <returns></returns>
        protected string ConvertRepositoryTypeToText(RepositoryType repositoryType)
        {
            string result = string.Empty;

            try
            {
                FieldInfo field = repositoryType.GetType().GetField(repositoryType.ToString());
                object[] attribs = field.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attribs.Length > 0)
                {
                    result = ((DescriptionAttribute)attribs[0]).Description;
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Populate Git repository selection.
        /// </summary>
        protected void PopulateGitRepositorySelection()
        {

            try
            {
                this.ImportRepositoryComboBox.DataSource = this.RepositoryNameList;
                this.ImportRepositoryComboBox.DisplayMember = "Name";
                this.ImportRepositoryComboBox.ValueMember = "Id";
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update controls dependant on team project.
        /// </summary>
        protected void UpdateControlsDependantOnTeamProject()
        {

            this.BuildDefinitionSearchForm.TeamProjectChanged();
            this.ReleaseDefinitionSearchForm.TeamProjectChanged();

            this.RepositoryNameList = null;
            this.ImportRepositoryComboBox.DataSource = null;
            this.ImportRepositoryTypeComboBox.SelectedItem = RepositoryType.SourceBuildDefinitionRepositoryType;

        }

        /// <summary>
        /// Generate zip archive file - path and file name.
        /// </summary>
        protected string GenerateZipArchiveFilePathAndFileName()
        {
            string result = string.Empty;
            string zipFileName = string.Empty;
            string rootFolder = string.Empty;

            try
            {
                rootFolder = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                zipFileName = string.Format(ExportZipFileNamePrefixPattern, DateTime.Now.ToString(ExportZipFileNameDatePattern));
                result = System.IO.Path.Combine(rootFolder, zipFileName);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

            return result;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Export button - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportButton_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelectorList buildDefinitionSelectorList = null;
            ReleaseDefinitionSelectorList releaseDefinitionSelectorList = null;
            GitRepositorySelector gitRepositorySelector = null;

            try
            {

                if (this.DefinitionExportImportPresenter.TeamProjectInfo.TeamProjectCollectionUri == null ||
                    this.DefinitionExportImportPresenter.TeamProjectInfo.TeamProjectName == string.Empty)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_TeamProjectNameNotSelected));
                    return;
                }

                switch (this.DefinitionType)
                {
                    case DefinitionTypes.BuildDefinition:
                        buildDefinitionSelectorList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();
                        if (buildDefinitionSelectorList == null ||
                            buildDefinitionSelectorList.Count <= 0)
                        {
                            MessageBox.Show(string.Format(ValidationErrorMessage_BuildDefinitionNotSelected));
                            return;
                        }

                        this.ResetAll();
                        this.UpdateZipArchiveStatus(ExportZipArchiveStatus_NotReady);
                        this.UpdateControlsBasedOnDependencies();
                        this.DefinitionExportImportPresenter.ExportBuildDefinitions(buildDefinitionSelectorList);
                        this.UpdateControlsBasedOnDependencies();

                        break;
                    case DefinitionTypes.ReleaseDefintion:

                        releaseDefinitionSelectorList = this.ReleaseDefinitionSearchForm.GetSelectedReleaseDefinitionSelectorList();
                        if (releaseDefinitionSelectorList == null ||
                            releaseDefinitionSelectorList.Count <= 0)
                        {
                            MessageBox.Show(string.Format(ValidationErrorMessage_ReleaseDefinitionNotSelected));
                            return;
                        }
                        //this.DefinitionExportImportPresenter.ChangeBuildDefinitionsQueueStatus(this.BuildDefinitionQueueStatus,
                        //                                                                                 buildDefinitionSelectorList);
                        break;
                    default:
                        break;
                }

                this.ExportImportStatusProgressBar.Visible = false;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickExportImportDefinitions.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Definition type combo box - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DefinitionTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.DefinitionTypeComboBox.SelectedItem is DefinitionTypes)
                {
                    if (this.DefinitionType == DefinitionTypes.ReleaseDefintion)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_DefinitionTypeReleaseDefinition_NotSupported));
                    }
                    this.SwitchOperationView();
                    this.UpdatePropertiesHeader();
                    this.UpdateControlsBasedOnDependencies();
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Operation type combo box - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OperationTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.DefinitionTypeComboBox.SelectedItem is DefinitionTypes)
                {
                    this.SwitchOperationView();
                    this.UpdatePropertiesHeader();
                    this.UpdateControlsBasedOnDependencies();
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Save button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = null;
            string zipArchiveFilePathAndName = string.Empty;

            try
            {
                if (this.ExportDefinitionsZipArchiveMemoryStream == null ||
                    this.ExportDefinitionsZipArchiveMemoryStream.Length == 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_DefinitionZipArchiveIsEmpty));
                    return;
                }

                zipArchiveFilePathAndName = this.GenerateZipArchiveFilePathAndFileName();

                saveFileDialog = new SaveFileDialog();

                saveFileDialog.InitialDirectory = zipArchiveFilePathAndName;
                saveFileDialog.FileName = Path.GetFileName(zipArchiveFilePathAndName);

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {

                    using (var fileStream = new FileStream(saveFileDialog.FileName, FileMode.Create))
                    {
                        this.ExportDefinitionsZipArchiveMemoryStream.Seek(0, SeekOrigin.Begin);
                        this.ExportDefinitionsZipArchiveMemoryStream.CopyTo(fileStream);

                        Logger.Info(string.Format(LogEntry_ZipArchiveFile_Saved,
                                                  saveFileDialog.FileName));

                        if (this.ResetAfterSave == true)
                        {
                            ResetAll();
                        }

                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Zip file location text box - Text change.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZipFileLocationTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                UpdateControlsBasedOnDependencies();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Reset button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.ResetAll();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Import zip file location selection button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportZipFileLocationSelectionButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = null;
            DialogResult dialogResult = DialogResult.Cancel;
            string zipFileName = string.Empty;

            try
            {
                this.ImportDefinitionsZipArchiveList.Clear();
                this.ImportDefinitionZipArchiveContentsDataGridView.DataSource = null;

                openFileDialog = new OpenFileDialog();

                dialogResult = openFileDialog.ShowDialog();
                if (dialogResult == DialogResult.OK)
                {
                    this.ImportZipFileLocation = openFileDialog.FileName;
                    UpdateControlsBasedOnDependencies();
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Import definition zip archive contents data grid view - Cell value changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportDefinitionZipArchiveContentsDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (ImportDefinitionZipArchiveContentsDataGridView.Columns[e.ColumnIndex].Name == ImportDefinitionZipArchiveContentsDataGridViewColumn_Check && e.RowIndex >= 0 && e.RowIndex < ImportDefinitionZipArchiveContentsDataGridView.Rows.Count)
                {
                    ZipArchiveEntryDisplay zipArchiveEntryDisplay = (ZipArchiveEntryDisplay)ImportDefinitionZipArchiveContentsDataGridView.Rows[e.RowIndex].DataBoundItem;

                    DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)ImportDefinitionZipArchiveContentsDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];

                    if ((bool)cell.Value == true)
                    {
                        zipArchiveEntryDisplay.Selected = true;
                    }
                    else
                    {
                        zipArchiveEntryDisplay.Selected = false;
                    }
                    this.UpdateControlsBasedOnDependencies();
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Import definition zip archive contents data grid view - Cell click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportDefinitionZipArchiveContentsDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.ImportDefinitionZipArchiveContentsDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Import definition zip archive contents data grid view - Cell content click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportDefinitionZipArchiveContentsDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.ImportDefinitionZipArchiveContentsDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Import definition zip archive contents data grive view - cell double click.
        /// </summary>
        /// <remarks>
        /// Without this override double clicking on checkbox column can force checkbox value 
        /// and build definition selector [Selected] values be become out of sync. 
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportDefinitionZipArchiveContentsDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (ImportDefinitionZipArchiveContentsDataGridView.Columns[e.ColumnIndex].Name == ImportDefinitionZipArchiveContentsDataGridViewColumn_Check && e.RowIndex >= 0 && e.RowIndex < ImportDefinitionZipArchiveContentsDataGridView.Rows.Count)
                {
                    ZipArchiveEntryDisplay zipArchiveEntryDisplay = (ZipArchiveEntryDisplay)ImportDefinitionZipArchiveContentsDataGridView.Rows[e.RowIndex].DataBoundItem;

                    DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)ImportDefinitionZipArchiveContentsDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];

                    if ((bool)cell.Value == true)
                    {
                        cell.EditingCellFormattedValue = false;
                    }
                    else
                    {
                        cell.EditingCellFormattedValue = true;
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Open import zip file button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenImportZipFileButton_Click(object sender, EventArgs e)
        {

            try
            {
                if (File.Exists(this.ImportZipFileLocation) != true)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_ZipArchiveFileDoesNotExist,
                                                  this.ImportZipFileLocation));
                    return;
                }

                using (FileStream fileStream = new FileStream(this.ImportZipFileLocation, FileMode.Open))
                {
                    using (ZipArchive zipArchive = new ZipArchive(fileStream, ZipArchiveMode.Read))
                    {
                        this.ImportDefinitionsZipArchive = zipArchive;
                        this.PopulateImportDefinitionsZipArchiveList();
                        this.PopulateImportDefinitionZipArchiveContents();
                        this.UpdateControlsBasedOnDependencies();
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }


        private void ImportDefinitionZipArchiveContentsDataGridView_Sorted(object sender, EventArgs e)
        {

            try
            {

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Import zip archive contents select all tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportZipArchiveContentsSelectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ImportDefinitionZipArchiveContentsCheckAll();
                this.UpdateControlsBasedOnDependencies();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Import zip archive contents select none tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportZipArchiveContentsSelectNoneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ImportDefinitionZipArchiveContentsCheckNone();
                this.UpdateControlsBasedOnDependencies();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Import zip archive contents select selected tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportZipArchiveContentsSelectSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ImportDefinitionZipArchiveContentsCheckSelected();
                this.UpdateControlsBasedOnDependencies();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Import zip archive contents display all tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportZipArchiveContentsDisplayAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ImportDefinitionZipArchiveContentsDisplayAll();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Import zip archive contents display checked tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportZipArchiveContentsDisplayCheckedToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ImportDefinitionZipArchiveContentsDisplayChecked();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        /// <summary>
        /// Event Handler: Import zip file location text box - Text changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportZipFileLocationTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ImportDefinitionsZipArchiveList.Clear();
                this.ImportDefinitionZipArchiveContentsDataGridView.DataSource = null;
                this.UpdateControlsBasedOnDependencies();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Import definition zip archive contents data grid view check all tool strip menu item - Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportDefinitionZipArchiveContentsDataGridViewCheckAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ImportDefinitionZipArchiveContentsCheckAll();
                this.UpdateControlsBasedOnDependencies();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Import definition zip archive contents data grid view check selected tool strip menu item - Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportDefinitionZipArchiveContentsDataGridViewCheckSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ImportDefinitionZipArchiveContentsCheckSelected();
                this.UpdateControlsBasedOnDependencies();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Import definition zip archive contents data grid view check none tool strip menu item - Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportDefinitionZipArchiveContentsDataGridViewCheckNoneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ImportDefinitionZipArchiveContentsCheckNone();
                this.UpdateControlsBasedOnDependencies();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Import definition zip archive contents data grid view display all tool strip menu item - Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportDefinitionZipArchiveContentsDataGridViewDisplayAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ImportDefinitionZipArchiveContentsDisplayAll();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Import definition zip archive contents data grid view display checked tool strip menu item - Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportDefinitionZipArchiveContentsDataGridViewDisplayCheckedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ImportDefinitionZipArchiveContentsDisplayChecked();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Import button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ImportDefinitions();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Import repository type combo box - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportRepositoryTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            RepositoryType repositoryType = RepositoryType.SourceBuildDefinitionRepositoryType;
            try
            {
                if (sender is ComboBox)
                {
                    repositoryType = (RepositoryType)this.ImportRepositoryTypeComboBox.SelectedItem;
                    switch (repositoryType)
                    {
                        case RepositoryType.SourceBuildDefinitionRepositoryType:
                        case RepositoryType.TeamFoundationVersionControl:
                        case RepositoryType.GitHub:
                        case RepositoryType.ExternalGit:
                        case RepositoryType.Subversion:
                            this.ImportMigrateToGitCheckBox.Checked = false;
                            break;
                        case RepositoryType.Git:
                            this.PopulateGitRepositorySelection();
                            break;
                        default:
                            break;
                    }
                    this.UpdateControlsBasedOnDependencies();
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Import Zip Archive Contents Copy Build Definition Name Tool Strip Menu Item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportZipArchiveContentsCopyBuildDefinitionNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string buildRequestSummaryAsText = string.Empty;
            ZipArchiveEntryDisplay zipArchiveEntryDisplay = null;
            StringBuilder definitionsAsJsonStringBuilder = null;
            ZipArchiveEntryDisplayList zipArchiveEntryDisplayList = null;
            try
            {
                this.Cursor = Cursors.WaitCursor;

                definitionsAsJsonStringBuilder = new StringBuilder();

                zipArchiveEntryDisplayList = this.GetSelectedZipArchiveEntryDisplayList();

                for (int index = 0; index < zipArchiveEntryDisplayList.Count; index++)
                {
                    zipArchiveEntryDisplay = zipArchiveEntryDisplayList[index];
                    if (zipArchiveEntryDisplay != null)
                    {
                        definitionsAsJsonStringBuilder.Append(zipArchiveEntryDisplay.Name);
                    }
                    if (index < this.ImportDefinitionZipArchiveContentsDataGridView.SelectedRows.Count - 1)
                    {
                        definitionsAsJsonStringBuilder.AppendLine();
                    }
                }

                Clipboard.SetDataObject(definitionsAsJsonStringBuilder.ToString());

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        /// <summary>
        /// Event Handler: Import Zip Archive Contents Copy Build Definition Json Tool Strip Menu Item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportZipArchiveContentsCopyBuildDefinitionJsonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string buildRequestSummaryAsText = string.Empty;
            ZipArchiveEntryDisplay zipArchiveEntryDisplay = null;
            StringBuilder definitionsAsJsonStringBuilder = null;
            ZipArchiveEntryDisplayList zipArchiveEntryDisplayList = null;
            try
            {
                this.Cursor = Cursors.WaitCursor;

                definitionsAsJsonStringBuilder = new StringBuilder();

                zipArchiveEntryDisplayList = this.GetSelectedZipArchiveEntryDisplayList();

                for (int index = 0; index < zipArchiveEntryDisplayList.Count; index++)
                {
                    zipArchiveEntryDisplay = zipArchiveEntryDisplayList[index];
                    if (zipArchiveEntryDisplay != null)
                    {
                        definitionsAsJsonStringBuilder.Append(zipArchiveEntryDisplay.DefinitionAsJson);
                    }
                    if (index < this.ImportDefinitionZipArchiveContentsDataGridView.SelectedRows.Count - 1)
                    {
                        definitionsAsJsonStringBuilder.AppendLine();
                    }
                }

                Clipboard.SetDataObject(definitionsAsJsonStringBuilder.ToString());

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion
    }
}
