﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Sidekick container types.
    /// </summary>
    public class SidekickContainerTypes
    {
        //Sidekick warning level.
        public enum SidekickWarningLevel
        {
            Unspecified = 0,
            Low = 1,
            High = 2,
            Severe = 3
        }
    }

}
