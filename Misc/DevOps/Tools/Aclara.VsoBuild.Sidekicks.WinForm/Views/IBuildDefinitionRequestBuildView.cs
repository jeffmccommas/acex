﻿using Aclara.Tools.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build definition request build view - Interface.
    /// </summary>
    public interface IBuildDefinitionRequestBuildView
    {
        string SidekickName { get; }

        string SidekickDescription { get; }

        SidekickContainerTypes.SidekickWarningLevel SidekickWarningLevel { get; }

        string Parameters { get; set; }

        Verbosity Verbosity { get; set; }

        bool OverrideQueueStatus { get; set; }

        QueuePriority QueuePriority { get; set; }

        string BackgroundTaskStatus { get; set; }

        string ApplyButtonText { get; set; }

        void ApplyButtonEnable(bool enable);

        bool IsSidekickBusy { get; }

        void RetrieveRemainingDefinitions();

        void TeamProjectChanged();

        void UpdateProgressRequestBuild(int buildDefinitionBuildRequestedCount, int buildDefinitionTotalCount);

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);
    }

}
