﻿using Aclara.Tools.Common;
using Aclara.VsoBuild.Sidekicks.WinForm.Configuration;
using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using Aclara.VsoBuild.Sidekicks.WinForm.Logging;
using Aclara.VsoBuild.Sidekicks.WinForm.Models;
using Aclara.VsoBuild.Sidekicks.WinForm.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Views
{
    public partial class BuildDefinitionRequestBuildForm : Form, IBuildDefinitionRequestBuildView, ISidekickCoaction, ISidekickView, ISidekickInfo
    {

        #region Public Constants

        public const string SidekickView_SidekickName = "Request Build";
        public const string SidekickView_SidekickDescription = "Request Build";

        private const string BackgroundProcessButtonApply_Ready = "Apply";
        private const string BackgroundProcessButtonApply_Running = "Cancel";

        private const string BuildDefinitionTag_ContinuousIntegration = "ci";
        private const string BuildDefinitionTag_DEV = "dev";
        private const string BuildDefinitionTag_QA = "qa";
        private const string BuildDefinitionTag_UAT = "uat";
        private const string BuildDefinitionTag_PERF = "perf";
        private const string BuildDefinitionTag_PROD = "prod";
        private const string BuildDefinitionTag_POC = "poc";
        private const string BuildDefinitionTag_TEST = "test";
        private const string BuildDefinitionTag_DEVDR = "devdr";
        private const string BuildDefinitionTag_UATDR = "uatdr";
        private const string BuildDefinitionTag_PRODDR = "proddr";

        private const string RequestBuildDefinitionBuildConfirmation_PendingAction = "Are you sure you want to build/deploy to environments ({0})?";
        private const string RequestBuildDefinitionBuildConfirmation_ConfirmationMessage = @"Type ""YES"" to confirm build request.";
        private const string RequestBuildDefinitionBuildConfirmation_ExpectedTypeResponse = "YES";
        private const string RequestBuildDefinitionBuildConfirmation_WarningMessage = "*WARNING* *WARNING* *WARNING*";

        #endregion

        #region Private Constants

        private const string ValidationErrorMessage_TeamProjectNameNotSelected = "Team project not selected.";
        private const string ValidationErrorMessage_BranchNameSourceNotEntered = "Branch name source not entered.";
        private const string ValidationErrorMessage_BuildDefinitionNotSelected = "Build definition not selected.";
        private const string ValidationErrorMessage_SourceBranchNameNotEntered = "Branch name - source not entered.";
        private const string ValidationErrorMessage_SourceBuildDefinitionNamePrefixNotEntered = "Build definition name prefix - source not entered.";
        private const string ValidationErrorMessage_TargetBranchNameNotEntered = "Branch name - target not entered.";
        private const string ValidationErrorMessage_TargetBuildDefinitionNamePrefixNotEntered = "Build definition name prefix - target not entered.";
        private const string ValidationErrorMessage_SelectedBuildDefinitionsMustHaveSameNamePrefix = "Selected build definitons must have same name prefix.";

        private const string Parameters_Switch_Separator = ":";
        private const string Parameters_Switch_Verbosity = "verbosity";

        private const string Environment_dev = "dev";
        private const string Environment_qa = "qa";
        private const string Environment_uat = "uat";
        private const string Environment_perf = "perf";
        private const string Environment_prod = "prod";
        private const string Environment_devdr = "devdr";
        private const string Environment_uatdr = "uatdr";
        private const string Environment_proddr = "proddr";

        private const string BuildRequestNotification_To = "#ACE-TEAM";
        private const string BuildRequestNotification_SubjectCore = " ACE Build/Deploy [see list] to ";
        private const string BuildRequestNotification_SubjectSuffix = "(BEGIN)";

        private const string ValidationPromptCaption = "Request Build Validation";
        private const string ValidationErrorMessage_OverrideQueueNotSelected = "Disabled build definitions have been selected. Enable the 'Override queue status' option and try again.";
        private const string ValidationPromptMessage_OverrideQueueStatus = "'Override queue status' option has been enabled. Click 'Yes' to request builds for disabled build definitions.";

        private const string BuildDefinitionSelectionStatus_Suffix = "{0} build definition(s) selected for queue:";

        #endregion

        #region Private Data Members

        private SidekickConfiguration _sidekickConfiguration = null;
        private BuildDefinitionSearchForm _buildDefinitionSearchForm = null;
        private ISidekickCoaction _sidekickCoaction = null;
        private BuildDefinitionRequestBuildPresenter _buildDefinitionRequestBuildPresenter;
        private string _buildRequestTitle;
        private string _buildRequestSummaryAsText = string.Empty;
        private string _buildRequestSummaryHtml = string.Empty;
        private BuildDefinitionSelectorList _buildDefinitionSelectorList;
        private BindingSource _buildDefinitionSelectorListBindingSource;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickView_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickView_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                if (this.BuildDefinitionRequestBuildPresenter.IsBackgroundTaskBusy == false)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick coaction.
        /// </summary>
        public ISidekickCoaction SidekickCoation
        {
            get { return _sidekickCoaction; }
            set { _sidekickCoaction = value; }
        }

        /// <summary>
        /// Property: Sidekick warning level.
        /// </summary>
        public SidekickContainerTypes.SidekickWarningLevel SidekickWarningLevel
        {
            get
            {
                return SidekickContainerTypes.SidekickWarningLevel.Low;
            }
        }

        /// <summary>
        /// Property: Parameters.
        /// </summary>
        public string Parameters
        {
            get { return this.ParametersTextBox.Text; }
            set { this.ParametersTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Verbosity.
        /// </summary>
        public Verbosity Verbosity
        {
            get
            {
                return (Verbosity)this.VerbosityComboBox.SelectedItem;
            }
            set
            {
                this.VerbosityComboBox.Text = value.ToString();
            }
        }

        /// <summary>
        /// Property: Override queue status.
        /// </summary>
        public bool OverrideQueueStatus
        {
            get
            {
                return this.OverrideQueueStatusCheckBox.Checked;
            }
            set
            {
                this.OverrideQueueStatusCheckBox.Checked = value;
            }
        }

        /// <summary>
        /// Property: Queue priority.
        /// </summary>
        public QueuePriority QueuePriority
        {
            get
            {
                return (QueuePriority)this.QueuePriorityComboBox.SelectedItem;
            }
            set
            {
                this.QueuePriorityComboBox.Text = value.ToString();
            }
        }

        /// <summary>
        /// Property: Build definition selector list.
        /// </summary>
        public BuildDefinitionSelectorList BuildDefinitionSelectorList
        {
            get { return _buildDefinitionSelectorList; }
            set { _buildDefinitionSelectorList = value; }
        }

        /// <summary>
        /// Property: Build definition selection status.
        /// </summary>
        public string BuildDefinitionSelectionStatus
        {
            get { return BuildDefinitionSelectionStatusLabel.Text; }
            set { BuildDefinitionSelectionStatusLabel.Text = value; }
        }


        /// <summary>
        /// Property: Build build definition selector list binding source.
        /// </summary>
        public BindingSource BuildDefinitionSelectorListBindingSource
        {
            get { return _buildDefinitionSelectorListBindingSource; }
            set { _buildDefinitionSelectorListBindingSource = value; }
        }

        /// <summary>
        /// Property: Build defintion request build presenter.
        /// </summary>
        public BuildDefinitionRequestBuildPresenter BuildDefinitionRequestBuildPresenter
        {
            get { return _buildDefinitionRequestBuildPresenter; }
            set { _buildDefinitionRequestBuildPresenter = value; }
        }

        /// <summary>
        /// Property: Build definition search form.
        /// </summary>
        public BuildDefinitionSearchForm BuildDefinitionSearchForm
        {
            get { return _buildDefinitionSearchForm; }
            set { _buildDefinitionSearchForm = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get { return this.ApplyButton.Text; }
            set { this.ApplyButton.Text = value; }
        }

        /// <summary>
        /// Property: Build request title.
        /// </summary>
        public string BuildRequestTitle
        {
            get { return _buildRequestTitle; }
            set { _buildRequestTitle = value; }
        }

        /// <summary>
        /// Property: Build request summary as text.
        /// </summary>
        public string BuildRequestSummaryAsText
        {
            get { return _buildRequestSummaryAsText; }
            set { _buildRequestSummaryAsText = value; }
        }

        /// <summary>
        /// Property: Build request summary as html.
        /// </summary>
        public string BuildRequestSummaryAsHtml
        {
            get { return _buildRequestSummaryHtml; }
            set { _buildRequestSummaryHtml = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildDefinitionRequestBuildForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <param></param>
        public BuildDefinitionRequestBuildForm(SidekickConfiguration sidekickConfiguration,
                                               BuildDefinitionSearchForm buildDefinitionSearchForm,
                                               ISidekickCoaction sidekickCoaction)
        {
            InitializeComponent();

            this.SidekickConfiguration = sidekickConfiguration;
            this.SidekickCoation = sidekickCoaction;
            this.InitializeControls();
            this.BuildDefinitionSearchForm = buildDefinitionSearchForm;
            this.BuildDefinitionSearchForm.BuildDefinitionSelectionChanged += OnBuildDefinitionSelectionChanged;

            _buildDefinitionSelectorList = new BuildDefinitionSelectorList();
            _buildDefinitionSelectorListBindingSource = new BindingSource();
            _buildDefinitionSelectorListBindingSource.DataSource = _buildDefinitionSelectorList;
            this.BuildDefinitionSelectorListBox.DisplayMember = "Name";
            this.BuildDefinitionSelectorListBox.DataSource = _buildDefinitionSelectorListBindingSource;

            this.BuildDefinitionSearchPanel.DockControl(this.BuildDefinitionSearchForm);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update progress of request build.
        /// </summary>
        /// <param name="buildDefinitionBuildRequestedCount"></param>
        /// <param name="buildDefinitionTotalCount"></param>
        public void UpdateProgressRequestBuild(int buildDefinitionBuildRequestedCount, int buildDefinitionTotalCount)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<int, int>(this.UpdateProgressRequestBuild), buildDefinitionBuildRequestedCount, buildDefinitionTotalCount);
            }
            else
            {
                if (buildDefinitionBuildRequestedCount < buildDefinitionTotalCount)
                {
                    this.RequestBuildProgressBar.Visible = true;
                    this.RequestBuildProgressBar.Minimum = 0;
                    this.RequestBuildProgressBar.Maximum = buildDefinitionTotalCount;
                    this.RequestBuildProgressBar.Value = buildDefinitionBuildRequestedCount;
                }
                else if (buildDefinitionBuildRequestedCount == buildDefinitionTotalCount)
                {
                    this.RequestBuildProgressBar.Visible = false;
                }
            }
        }

        /// <summary>
        /// Check build status.
        /// </summary>
        /// <param name="buildDefinitionNameList"></param>
        public void CheckBuildStatus(List<string> buildDefinitionNameList)
        {
            throw new NotImplementedException("Method not applicable to buil definition request build sidekick.");
        }

        /// <summary>
        /// Request build for another sidekick.
        /// </summary>
        /// <param name="buildDefinitionNameList"></param>
        public void RequestBuild(List<string> buildDefinitionNameList)
        {
            bool exactMatch = true;
            bool refresh = false;
            bool searchAndCheckAll = false;

            try
            {
                searchAndCheckAll = true;
                this.BuildDefinitionSearchForm.SearchBuildDefinitionList(buildDefinitionNameList, 
                                                                         exactMatch, 
                                                                         refresh, 
                                                                         searchAndCheckAll);

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Retrieve remaining build definitions.
        /// </summary>
        public void RetrieveRemainingDefinitions()
        {
            this.BuildDefinitionSearchForm.RetrieveRemainingDefinitions();
        }

        /// <summary>
        /// Apply button enable.
        /// </summary>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            this.BuildDefinitionSearchForm.TeamProjectChanged();
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = BackgroundProcessButtonApply_Ready;
            }
        }

        /// <summary>
        /// Background task Completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = BackgroundProcessButtonApply_Ready;

            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {

                this.VerbosityComboBox.DataSource = Enum.GetValues(typeof(Verbosity));

                this.VerbosityComboBox.Text = Verbosity.Normal.ToString();

                this.QueuePriorityComboBox.DataSource = Enum.GetValues(typeof(QueuePriority));

                this.QueuePriorityComboBox.Text = QueuePriority.Normal.ToString();

                this.EnableControlsDependantOnSelectedBuildDefinition(false);
                this.UpdateBuildDefinitionDeletionStatus();
                this.RequestBuildProgressBar.Visible = false;

            }
            catch (Exception)
            {
                throw;
            }

        }


        /// <summary>
        /// Update build definition deletion status.
        /// </summary>
        protected void UpdateBuildDefinitionDeletionStatus()
        {
            try
            {
                BuildDefinitionSelectionStatus = string.Format(BuildDefinitionSelectionStatus_Suffix,
                                                              this.BuildDefinitionSelectorListBox.Items.Count);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Hanlder: Build definition selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="requestBuildEventArgs"></param>
        protected void OnBuildDefinitionSelectionChanged(Object sender,
                                                         BuildDefinitionSelectionChangedEventArgs buildDefinitionSelectionChangedEventArgs)
        {

            BuildDefinitionSelectorList buildDefinitionSelectorList = null;

            try
            {
                this.BuildDefinitionSelectorList.Clear();

                if (buildDefinitionSelectionChangedEventArgs != null)
                {
                    if (buildDefinitionSelectionChangedEventArgs.OneOrMoreSelected == true)
                    {
                        this.FormatBuildRequestTitle();
                        this.FormatBuildRequestSummaryAsText();
                        this.FormatBuildRequestSummaryAsHtml();
                        EnableControlsDependantOnSelectedBuildDefinition(true);
                        PreselectEnvironmentCheckBoxes();
                        buildDefinitionSelectorList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();

                        foreach (BuildDefinitionSelector buildDefinitionSelector in buildDefinitionSelectorList)
                        {
                            this.BuildDefinitionSelectorList.Add(buildDefinitionSelector);
                        }

                        if (buildDefinitionSelectorList.Count > 0)
                        {
                            this.EnableControlsDependantOnSelectedBuildDefinition(true);
                        }
                    }
                    else
                    {
                        this.BuildRequestSummaryAsText = string.Empty;
                        EnableControlsDependantOnSelectedBuildDefinition(false);
                        PreselectEnvironmentCheckBoxes();
                    }
                }

                this.BuildDefinitionSelectorListBindingSource.ResetBindings(false);
                this.UpdateBuildDefinitionDeletionStatus();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Formate parameters.
        /// </summary>
        protected void FormatParameters()
        {

            try
            {

                this.ParametersTextBox.Text = string.Format("\"{0}\"{1}\"{2}\"",
                                                            Parameters_Switch_Verbosity,
                                                            Parameters_Switch_Separator,
                                                            this.VerbosityComboBox.Text.ToLower()); ;


            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        protected List<string> GetDeployToEnvironmentList()
        {
            List<string> result = null;

            try
            {
                result = new List<string>();

                if (EnvironmentDevCheckBox.Checked == true)
                {
                    result.Add(Environment_dev);
                }

                if (EnvironmentQaCheckBox.Checked == true)
                {
                    result.Add(Environment_qa);
                }

                if (EnvironmentUatCheckBox.Checked == true)
                {
                    result.Add(Environment_uat);
                }

                if (EnvironmentPerfCheckBox.Checked == true)
                {
                    result.Add(Environment_perf);
                }

                if (EnvironmentProdCheckBox.Checked == true)
                {
                    result.Add(Environment_prod);
                }

                if (EnvironmentDevDrCheckBox.Checked == true)
                {
                    result.Add(Environment_devdr);
                }

                if (EnvironmentUatDrCheckBox.Checked == true)
                {
                    result.Add(Environment_uatdr);
                }

                if (EnvironmentProdDrCheckBox.Checked == true)
                {
                    result.Add(Environment_proddr);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Format build request title.
        /// </summary>
        protected void FormatBuildRequestTitle()

        {

            string buildRequestTitle = string.Empty;
            List<string> branchNameList = null;
            List<string> deployToEnvList = null;

            //Environment list.
            deployToEnvList = this.GetDeployToEnvironmentList();

            //Branch list.
            branchNameList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorListBranchNameList();
            if (branchNameList.Count == 0)
            {
                return;
            }
            branchNameList.Sort();

            buildRequestTitle = string.Format("{0}{1}{2}",
                                       string.Join(",", branchNameList.ToArray()),
                                       BuildRequestNotification_SubjectCore,
                                       string.Join(",", deployToEnvList.ToArray()));
            this.BuildRequestTitle = buildRequestTitle;
        }

        /// <summary>
        /// Format build request summary as text.
        /// </summary>
        protected void FormatBuildRequestSummaryAsText()
        {

            string buildRequestSummary = string.Empty;
            StringBuilder buildRequestSummaryStringBuilder = null;
            List<string> branchNameList = null;

            List<string> deployToEnvList = null;
            BuildDefinitionSelectorList buildDefinitionSelectorList = null;

            try
            {

                if (BuildDefinitionSearchForm == null)
                {
                    return;
                }

                buildRequestSummaryStringBuilder = new StringBuilder();


                //Add title:

                //Branch list.
                branchNameList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorListBranchNameList();
                if (branchNameList.Count == 0)
                {
                    return;
                }
                branchNameList.Sort();

                buildRequestSummaryStringBuilder.Append(string.Join(",", branchNameList.ToArray()));

                //Application package.
                buildRequestSummaryStringBuilder.Append(BuildRequestNotification_SubjectCore);

                //Environment list.
                deployToEnvList = this.GetDeployToEnvironmentList();
                if (deployToEnvList.Count > 0)
                {
                    buildRequestSummaryStringBuilder.Append(string.Join(",", deployToEnvList.ToArray()));
                }
                else
                {
                    buildRequestSummaryStringBuilder.Append("<REPLACE WITH ENVIRONMENT LIST>");
                }

                buildRequestSummaryStringBuilder.Append("\r\n");
                buildRequestSummaryStringBuilder.Append("\r\n");

                //Add reference/reason:
                buildRequestSummaryStringBuilder.AppendLine("Reference/Reason:");
                buildRequestSummaryStringBuilder.AppendLine("<Enter reference/reason.>");
                buildRequestSummaryStringBuilder.Append("\r\n");

                //Add selected build definition list:
                buildRequestSummaryStringBuilder.AppendLine("Build Definition List:");
                buildDefinitionSelectorList = BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();
                foreach (BuildDefinitionSelector buildDefinitionSelector in buildDefinitionSelectorList)
                {
                    buildRequestSummaryStringBuilder.Append(buildDefinitionSelector.Name + "\r\n");
                }
                buildRequestSummaryStringBuilder.Append("\r\n");

                //Add parameters:
                buildRequestSummaryStringBuilder.AppendLine("Parameters:");
                buildRequestSummaryStringBuilder.AppendLine(this.Parameters);

                buildRequestSummary = buildRequestSummaryStringBuilder.ToString();

                this.BuildRequestSummaryAsText = buildRequestSummary;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Format build request summery as html.
        /// </summary>
        /// <returns></returns>
        protected void FormatBuildRequestSummaryAsHtml()
        {
            const string BuildDefinitionNameHeading = "Build Definition Name";

            string buildRequestSummary = string.Empty;
            StringBuilder buildRequestSummaryStringBuilder = null;
            List<string> branchNameList = null;
            string branchNameListAsText = string.Empty;

            List<string> deployToEnvList = null;
            string deployToEnvListAsText = string.Empty;

            BuildDefinitionSelectorList buildDefinitionSelectorList = null;

            try
            {

                if (BuildDefinitionSearchForm == null)
                {
                    return;
                }

                buildRequestSummaryStringBuilder = new StringBuilder();

                buildRequestSummaryStringBuilder.AppendLine("<html>");

                //Add style.

                buildRequestSummaryStringBuilder.AppendLine("<style type='text//css'>");
                buildRequestSummaryStringBuilder.AppendLine(".tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}");
                buildRequestSummaryStringBuilder.AppendLine(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}");
                buildRequestSummaryStringBuilder.AppendLine(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}");
                buildRequestSummaryStringBuilder.AppendLine(".tg .tg-7fle{font-weight:bold;background-color:#efefef;text-align:center;vertical-align:top}");
                buildRequestSummaryStringBuilder.AppendLine(".tg .tg-3we0{background-color:#ffffff;vertical-align:top}");
                buildRequestSummaryStringBuilder.AppendLine("</style> ");

                //Add title:

                //Branch list.
                branchNameList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorListBranchNameList();
                if (branchNameList.Count == 0)
                {
                    return;
                }
                branchNameList.Sort();

                branchNameListAsText = string.Join(",", branchNameList.ToArray());

                buildRequestSummaryStringBuilder.Append(string.Format("{0}",
                                                                      branchNameListAsText));

                //Application package.
                buildRequestSummaryStringBuilder.Append(BuildRequestNotification_SubjectCore);

                //Environment list.
                deployToEnvList = this.GetDeployToEnvironmentList();
                if (deployToEnvList.Count > 0)
                {
                    deployToEnvListAsText = string.Join(",", deployToEnvList.ToArray());
                    buildRequestSummaryStringBuilder.Append(string.Format("{0}",
                                                                          deployToEnvListAsText));
                }
                else
                {
                    buildRequestSummaryStringBuilder.Append(string.Format(HttpUtility.HtmlEncode(HttpUtility.HtmlEncode("<Replace with Environment List.>"))));
                }

                //Add reference/reason:
                buildRequestSummaryStringBuilder.Append("<p>");
                buildRequestSummaryStringBuilder.Append("<b>Reference/Reason:</b>");
                buildRequestSummaryStringBuilder.Append("<br>");
                buildRequestSummaryStringBuilder.Append(HttpUtility.HtmlEncode("<Enter reference/reason.>"));

                //Add selected build definition list:
                buildRequestSummaryStringBuilder.Append("<p>");
                buildRequestSummaryStringBuilder.Append("<b>Build Definition List:</b>");

                buildRequestSummaryStringBuilder.Append("<p>");
                buildRequestSummaryStringBuilder.AppendLine("<table class='tg'>");

                //Add header.
                buildRequestSummaryStringBuilder.AppendLine("<tr>");
                buildRequestSummaryStringBuilder.AppendLine(string.Format("<th class='tg-7fle'>{0}<//th>",
                                                                          BuildDefinitionNameHeading));
                buildRequestSummaryStringBuilder.AppendLine("</tr>");

                buildDefinitionSelectorList = BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();

                foreach (BuildDefinitionSelector buildDefinitionSelector in buildDefinitionSelectorList)
                {
                    buildRequestSummaryStringBuilder.AppendLine("<tr>");

                    buildRequestSummaryStringBuilder.AppendLine(string.Format("<td class='tg-3we0'>{0}<//td>",
                                                                        buildDefinitionSelector.Name));

                    buildRequestSummaryStringBuilder.AppendLine("</tr>");
                }
                buildRequestSummaryStringBuilder.AppendLine("</table>");

                //Add parameters:
                buildRequestSummaryStringBuilder.Append("<p>");
                buildRequestSummaryStringBuilder.AppendLine("<b>Parameters:</b>");
                buildRequestSummaryStringBuilder.Append("<br>");
                buildRequestSummaryStringBuilder.Append(this.Parameters);
                buildRequestSummaryStringBuilder.Append("<p>");

                buildRequestSummaryStringBuilder.AppendLine("</html>");

                buildRequestSummary = buildRequestSummaryStringBuilder.ToString();

                this.BuildRequestSummaryAsHtml = buildRequestSummary;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Enable/disable controls dependant on at lease one build definition selected.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnSelectedBuildDefinition(bool enable)
        {
            try
            {
                this.ParametersTextBox.Enabled = enable;
                this.ClearParametersButton.Enabled = enable;
                this.VerbosityComboBox.Enabled = enable;
                this.QueuePriorityComboBox.Enabled = enable;
                this.OverrideQueueStatusCheckBox.Enabled = enable;

                this.EnvironmentDevCheckBox.Enabled = enable;
                this.EnvironmentQaCheckBox.Enabled = enable;
                this.EnvironmentUatCheckBox.Enabled = enable;
                this.EnvironmentPerfCheckBox.Enabled = enable;
                this.EnvironmentProdCheckBox.Enabled = enable;
                this.EnvironmentDevDrCheckBox.Enabled = enable;
                this.EnvironmentUatDrCheckBox.Enabled = enable;
                this.EnvironmentProdDrCheckBox.Enabled = enable;

                this.PreviewButton.Enabled = enable;
                this.NewEmailSplitButton.Enabled = enable;
                this.CopySplitButton.Enabled = enable;
                this.CheckBuildStatusQuickAccessSplitButton.Enabled = enable;
                this.OverrideQueueStatusQuickAccessCheckBox.Enabled = enable;
                this.ApplyButton.Enabled = enable;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Preselete environemnt checkboxes (based on selected build definitions).
        /// </summary>
        protected void PreselectEnvironmentCheckBoxes()
        {

            List<string> environmentList = null;

            try
            {
                //Environment list.
                environmentList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorListEnvironmentList();


                if (environmentList.Contains(Environment_dev) == true)
                {
                    this.EnvironmentDevCheckBox.Checked = true;
                }
                else
                {
                    this.EnvironmentDevCheckBox.Checked = false;
                }
                if (environmentList.Contains(Environment_qa) == true)
                {
                    this.EnvironmentQaCheckBox.Checked = true;
                }
                else
                {
                    this.EnvironmentQaCheckBox.Checked = false;
                }
                if (environmentList.Contains(Environment_uat) == true)
                {
                    this.EnvironmentUatCheckBox.Checked = true;
                }
                else
                {
                    this.EnvironmentUatCheckBox.Checked = false;
                }
                if (environmentList.Contains(Environment_perf) == true)
                {
                    this.EnvironmentPerfCheckBox.Checked = true;
                }
                else
                {
                    this.EnvironmentPerfCheckBox.Checked = false;
                }
                if (environmentList.Contains(Environment_prod) == true)
                {
                    this.EnvironmentProdCheckBox.Checked = true;
                }
                else
                {
                    this.EnvironmentProdCheckBox.Checked = false;
                }
                if (environmentList.Contains(Environment_devdr) == true)
                {
                    this.EnvironmentDevDrCheckBox.Checked = true;
                }
                else
                {
                    this.EnvironmentDevDrCheckBox.Checked = false;
                }
                if (environmentList.Contains(Environment_uatdr) == true)
                {
                    this.EnvironmentUatDrCheckBox.Checked = true;
                }
                else
                {
                    this.EnvironmentUatDrCheckBox.Checked = false;
                }
                if (environmentList.Contains(Environment_proddr) == true)
                {
                    this.EnvironmentProdDrCheckBox.Checked = true;
                }
                else
                {
                    this.EnvironmentProdDrCheckBox.Checked = false;
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// New email in Outlook as text.
        /// </summary>
        protected void NewEmailInOutlookAsText(string to, string subject, string body)
        {

            const int Outlook_olMailItem = 0;
            const int Outlook_OlBodyFormat_olFormatUnspecified = 0;
            const int Outlook_OlBodyFormat_olFormatPlain = 1;
            const int Outlook_OlBodyFormat_olFormatHTML = 2;
            const int Outlook_OlBodyFormat_olFormatRichText = 3;

            try
            {
                dynamic outlookApplication;
                dynamic mailItem;

                outlookApplication = Activator.CreateInstance(Type.GetTypeFromProgID("Outlook.Application"));

                mailItem = outlookApplication.CreateItem(Outlook_olMailItem);

                mailItem.To = to;
                mailItem.Subject = subject;
                mailItem.Body = body;
                mailItem.BodyFormat = Outlook_OlBodyFormat_olFormatPlain;

                mailItem.Display(false);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// New email in Outlook as html.
        /// </summary>
        protected void NewEmailInOutlookAsHtml(string to, string subject, string body)
        {

            const int Outlook_olMailItem = 0;
            const int Outlook_OlBodyFormat_olFormatUnspecified = 0;
            const int Outlook_OlBodyFormat_olFormatPlain = 1;
            const int Outlook_OlBodyFormat_olFormatHTML = 2;
            const int Outlook_OlBodyFormat_olFormatRichText = 3;

            try
            {
                dynamic outlookApplication;
                dynamic mailItem;

                outlookApplication = Activator.CreateInstance(Type.GetTypeFromProgID("Outlook.Application"));

                mailItem = outlookApplication.CreateItem(Outlook_olMailItem);

                mailItem.To = to;
                mailItem.Subject = subject;
                mailItem.BodyFormat = Outlook_OlBodyFormat_olFormatHTML;
                mailItem.HTMLBody = body;

                mailItem.Display(false);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler Apply button - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            BuildDefinitionSelectorList buildDefinitionSelectorList = null;
            DialogResult promptDialogResult;
            ConfirmationForm confirmationForm = null;
            List<string> buildDefinitionTagMatchList = null;

            string RequestBuildDefinitionBuildConfirmation_PendingAction_RestrictedEnvironment = string.Empty;

            try
            {
                buildDefinitionTagMatchList = new List<string>();

                if (this.BuildDefinitionRequestBuildPresenter.TeamProjectInfo.TeamProjectCollectionUri == null ||
                    this.BuildDefinitionRequestBuildPresenter.TeamProjectInfo.TeamProjectName == string.Empty)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_TeamProjectNameNotSelected));
                    return;
                }

                buildDefinitionSelectorList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();
                if (buildDefinitionSelectorList == null ||
                    buildDefinitionSelectorList.Count <= 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_BuildDefinitionNotSelected));
                    return;
                }

                //Validation - Error: Override queue disabled build definitions NOT checked and disabled build definitions selected.
                buildDefinitionSelectorList = this.BuildDefinitionSearchForm.GetSelectedBuildDefinitionSelectorList();
                if (this.OverrideQueueStatusCheckBox.Checked == false &&
                    buildDefinitionSelectorList.Any(bd => bd.Enabled == false))
                {

                    promptDialogResult = MessageBox.Show(string.Format(ValidationErrorMessage_OverrideQueueNotSelected),
                                                         string.Format(ValidationPromptCaption),
                                                         MessageBoxButtons.OK,
                                                         System.Windows.Forms.MessageBoxIcon.Error);
                    return;

                }

                //Validation - Confirm: Prompt user to confirm build request for disabled build definitions.
                if (this.OverrideQueueStatusCheckBox.Checked == true)
                {

                    promptDialogResult = MessageBox.Show(string.Format(ValidationPromptMessage_OverrideQueueStatus),
                                                         string.Format(ValidationPromptCaption),
                                                         MessageBoxButtons.YesNoCancel,
                                                         System.Windows.Forms.MessageBoxIcon.Exclamation);
                    if (promptDialogResult != DialogResult.Yes)
                    {
                        return;
                    }

                }

                buildDefinitionTagMatchList.Add(BuildDefinitionTag_PROD);
                if (buildDefinitionSelectorList.ContainsBuildDefinitionTagMatch(buildDefinitionTagMatchList) == true)
                {
                    RequestBuildDefinitionBuildConfirmation_PendingAction_RestrictedEnvironment = string.Format(RequestBuildDefinitionBuildConfirmation_PendingAction, string.Join(",", buildDefinitionTagMatchList));
                    //Request confirmation.
                    confirmationForm = ConfirmationFormFactory.CreateForm(RequestBuildDefinitionBuildConfirmation_PendingAction_RestrictedEnvironment,
                                                                          RequestBuildDefinitionBuildConfirmation_ConfirmationMessage,
                                                                          RequestBuildDefinitionBuildConfirmation_ExpectedTypeResponse,
                                                                          true,
                                                                          true,
                                                                          RequestBuildDefinitionBuildConfirmation_WarningMessage);
                    confirmationForm.StartPosition = FormStartPosition.CenterParent;
                    promptDialogResult = confirmationForm.ShowDialog(this);
                    if (promptDialogResult != DialogResult.OK)
                    {
                        return;
                    }


                }


                this.RequestBuildProgressBar.Visible = false;

                this.BuildDefinitionRequestBuildPresenter.RequestBuilds(buildDefinitionSelectorList, this.Parameters, this.OverrideQueueStatus, this.QueuePriority);

                //Reset override queue status.
                this.OverrideQueueStatus = false;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler Verbosity combobox - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VerbosityComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                FormatParameters();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler Clear button - Clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearParametersButton_Click(object sender, EventArgs e)
        {
            try
            {

                this.ParametersTextBox.Text = string.Empty;

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler Preview button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PreviewButton_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult;

            NotificationPreviewForm NotificationPreviewForm = null;

            try
            {

                NotificationPreviewForm = NotificationPreviewFormFactory.CreateForm(this.BuildRequestSummaryAsText);
                NotificationPreviewForm.StartPosition = FormStartPosition.CenterParent;

                dialogResult = NotificationPreviewForm.ShowDialog(this);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler Environment dev checkbox - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnvironmentDevCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.FormatBuildRequestTitle();
                this.FormatBuildRequestSummaryAsText();
                this.FormatBuildRequestSummaryAsHtml();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler Environment qa checkbox - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnvironmentQaCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.FormatBuildRequestTitle();
                this.FormatBuildRequestSummaryAsText();
                this.FormatBuildRequestSummaryAsHtml();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler Environment uat checkbox - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnvironmentUatCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.FormatBuildRequestTitle();
                this.FormatBuildRequestSummaryAsText();
                this.FormatBuildRequestSummaryAsHtml();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler Environment perforance checkbox - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnvironmentPerfCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.FormatBuildRequestTitle();
                this.FormatBuildRequestSummaryAsText();
                this.FormatBuildRequestSummaryAsHtml();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler Environment prod checkbox - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnvironmentProdCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.FormatBuildRequestTitle();
                this.FormatBuildRequestSummaryAsText();
                this.FormatBuildRequestSummaryAsHtml();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler Environment dev dr checkbox - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnvironmentDevDrCheckBox_CheckedChanged_1(object sender, EventArgs e)
        {
            try
            {
                this.FormatBuildRequestTitle();
                this.FormatBuildRequestSummaryAsText();
                this.FormatBuildRequestSummaryAsHtml();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler Environment uat dr checkbox - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnvironmentUatDrCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.FormatBuildRequestTitle();
                this.FormatBuildRequestSummaryAsText();
                this.FormatBuildRequestSummaryAsHtml();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler Environment prod dr checkbox - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnvironmentProdDrCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.FormatBuildRequestTitle();
                this.FormatBuildRequestSummaryAsText();
                this.FormatBuildRequestSummaryAsHtml();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }


        /// <summary>
        /// Event Handler Copy as text tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyAsTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string buildRequestSummaryAsText = string.Empty;

            try
            {
                buildRequestSummaryAsText = string.Format("{0} {1}\r\n{2}",
                                                          this.BuildRequestTitle,
                                                          BuildRequestNotification_SubjectSuffix,
                                                          this.BuildRequestSummaryAsText);

                Clipboard.SetDataObject(buildRequestSummaryAsText);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler Copy as html tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyAsHTMLToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {

                ClipboardHelper.CopyToClipboard(this.BuildRequestSummaryAsHtml, this.BuildRequestSummaryAsText);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler New email as text tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewEmailAsTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string subject = string.Empty;

            try
            {
                subject = string.Format("{0} {1}",
                                        this.BuildRequestTitle,
                                        BuildRequestNotification_SubjectSuffix);
                this.NewEmailInOutlookAsText(BuildRequestNotification_To, subject, this.BuildRequestSummaryAsText);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler New email as html tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewEmailAsHTMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                this.NewEmailInOutlookAsHtml(BuildRequestNotification_To, this.BuildRequestTitle, this.BuildRequestSummaryAsHtml);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Check build status - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBuildStatusButton_Click(object sender, EventArgs e)
        {
            List<string> buildDefinitionNameList = null;
            List<BuildDefinitionSelector> buildDefintionSelectorList = null;
            try
            {
                buildDefinitionNameList = new List<string>();

                buildDefintionSelectorList = this.BuildDefinitionSelectorListBox.Items.Cast<BuildDefinitionSelector>().ToList();
                foreach (BuildDefinitionSelector buildDefinitionSelctor in buildDefintionSelectorList)
                {
                    buildDefinitionNameList.Add(buildDefinitionSelctor.Name);
                }

                this.SidekickCoation.CheckBuildStatus(buildDefinitionNameList);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickRequestBuild.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Check build status switch sidekick tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBuildStatusSwitchSidekickToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<string> buildDefinitionNameList = null;
            List<BuildDefinitionSelector> buildDefintionSelectorList = null;
            try
            {
                buildDefinitionNameList = new List<string>();

                buildDefintionSelectorList = this.BuildDefinitionSelectorListBox.Items.Cast<BuildDefinitionSelector>().ToList();
                foreach (BuildDefinitionSelector buildDefinitionSelctor in buildDefintionSelectorList)
                {
                    buildDefinitionNameList.Add(buildDefinitionSelctor.Name);
                }

                this.SidekickCoation.CheckBuildStatus(buildDefinitionNameList);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Check build status new window tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBuildStatusNewWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<string> buildDefinitionNameList = null;
            List<BuildDefinitionSelector> buildDefintionSelectorList = null;
            string executablePath = string.Empty;
            string executableName = string.Empty;
            string fileName = string.Empty;
            string arguments = string.Empty;

            try
            {
                buildDefinitionNameList = new List<string>();

                buildDefintionSelectorList = this.BuildDefinitionSelectorListBox.Items.Cast<BuildDefinitionSelector>().ToList();
                foreach (BuildDefinitionSelector buildDefinitionSelctor in buildDefintionSelectorList)
                {
                    buildDefinitionNameList.Add(buildDefinitionSelctor.Name);
                }

                arguments = string.Format("SidekickCoAction=CheckBuildStatus~BuildDefinitionList|{0}",
                                          string.Join(",", buildDefinitionNameList.ToArray()));

                executablePath = Path.GetDirectoryName(Application.ExecutablePath);
                executableName = System.AppDomain.CurrentDomain.FriendlyName;
                fileName = Path.Combine(executablePath, executableName);


                System.Diagnostics.Process.Start(fileName, arguments);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Override queue status check box - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OverrideQueueStatusCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.OverrideQueueStatusQuickAccessCheckBox.Checked = OverrideQueueStatusCheckBox.Checked;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }


        #endregion

        /// <summary>
        /// Event Handler: Override queue status quick access check box - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OverrideQueueStatusQuickAccessCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.OverrideQueueStatusCheckBox.Checked = OverrideQueueStatusQuickAccessCheckBox.Checked;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }
    }
}
