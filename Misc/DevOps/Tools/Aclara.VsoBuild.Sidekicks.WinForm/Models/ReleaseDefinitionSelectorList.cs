﻿using Aclara.Vso.Build.Client.Models;
using Aclara.Vso.Build.Client.Utilities;
using Microsoft.VisualStudio.Services.ReleaseManagement.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Models
{
    public class ReleaseDefinitionSelectorList : List<ReleaseDefinitionSelector>
    {


        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ReleaseDefinitionSelectorList()
            : base()
        {

        }

        #endregion

        #region Public Members

        /// <summary>
        /// Retrieve source release definition selector list filtered by release definition name prefix.
        /// </summary>
        /// <param name="releaseDefinitionNamePattern"></param>
        /// <param name="exactMatch"></param>
        /// <returns></returns>
        public ReleaseDefinitionSelectorList GetSourceReleaseDefinitionListFilterdByReleaseDefinitionNamePattern(string releaseDefinitionNamePattern,
                                                                                                                 bool exactMatch)
        {
            ReleaseDefinitionSelectorList result = null;

            try
            {
                result = new ReleaseDefinitionSelectorList();

                var filteredReleaseDefinitionList = Wildcard.FilterListByNamePropertyAndPattern(this, releaseDefinitionNamePattern, exactMatch);

                foreach (ReleaseDefinitionSelector releaseDefinitionSelector in filteredReleaseDefinitionList)
                {
                    result.Add(releaseDefinitionSelector);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Return list of release definition identifiers.
        /// </summary>
        /// <returns></returns>
        public List<int> GetReleaseDefinitionIdList()
        {
            List<int> result = null;

            try
            {
                result = this.Select(releaseDefinition => releaseDefinition.Id).Distinct().ToList();
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        /// <summary>
        /// Filter release definition list by release definition selector list.
        /// </summary>
        /// <param name="releaseDefinitionList"></param>
        /// <returns></returns>
        public ReleaseDefinitionList GetReleaseDefinitionListFilteredByBuildDefinitionSelectorList(ReleaseDefinitionList releaseDefinitionList)
        {
            ReleaseDefinitionList result = null;

            try
            {
                result = new ReleaseDefinitionList();

                var filteredReleaseDefinitionList = from releaseDefinition in releaseDefinitionList
                                                  where
                                                       (from releaseDefinitionSelector in this select releaseDefinitionSelector.Name).Contains(releaseDefinition.Name)
                                                  select releaseDefinition;

                foreach (ReleaseDefinition releaseDefinition in filteredReleaseDefinitionList)
                {
                    result.Add(releaseDefinition);
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve release definition list.
        /// </summary>
        /// <returns></returns>
        public ReleaseDefinitionList GetReleaseDefinitionList()
        {
            ReleaseDefinitionList result = null;

            try
            {
                result = new ReleaseDefinitionList();

                foreach (ReleaseDefinitionSelector releaseDefinitionSelector in this)
                {
                    result.Add(releaseDefinitionSelector.ReleaseDefinition);
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


        /// <summary>
        /// Get release definition name prefix list.
        /// </summary>
        /// <returns></returns>
        public List<string> GetReleaseDefinitionNamePrefixList()
        {
            List<string> result = null;
            string releaseDefinitionNamePrefix = string.Empty;

            try
            {
                result = new List<string>();

                foreach (ReleaseDefinitionSelector releaseDefinitionSelector in this)
                {
                    releaseDefinitionNamePrefix = releaseDefinitionSelector.GetNamePrefix();
                    if (result.Contains(releaseDefinitionNamePrefix) != true)
                    {
                        result.Add(releaseDefinitionNamePrefix);
                    }
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Members
        #endregion

    }
}
