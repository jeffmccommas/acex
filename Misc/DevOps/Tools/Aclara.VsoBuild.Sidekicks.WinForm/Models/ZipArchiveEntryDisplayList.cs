﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Models
{
    /// <summary>
    /// Zip archive entry display list.
    /// </summary>
    public class ZipArchiveEntryDisplayList : List<ZipArchiveEntryDisplay>
    {
        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        public ZipArchiveEntryDisplayList()
             : base()
        {

        }

        #endregion

        #region Public Members
        #endregion

        #region Protected Members
        #endregion

        #region Private Members
        #endregion

    }
}
