﻿using Microsoft.TeamFoundation.SourceControl.WebApi;
using System;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Models
{
    /// <summary>
    /// Git repository selector.
    /// </summary>
    public class GitRepositorySelector
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private Guid _id;
        private string _name;
        private GitRepository _gitRepository;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Identifier.
        /// </summary>
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// Property: Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Property: Git repository.
        /// </summary>
        public GitRepository GitRepository
        {
            get { return _gitRepository; }
            set { _gitRepository = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="gitRepository"></param>
        public GitRepositorySelector(GitRepository gitRepository)
        {
            _id = gitRepository.Id;
            _name = gitRepository.Name;
            _gitRepository = gitRepository;
        }

        #endregion

        #region Protected Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <remarks>
        /// Not perferred constructor.
        /// </remarks>
        protected GitRepositorySelector()
        {

        }

        #endregion

        #region Public Methods
        #endregion

        #region Protect Methods
        #endregion

        #region Private Methods
        #endregion
    }
    }
