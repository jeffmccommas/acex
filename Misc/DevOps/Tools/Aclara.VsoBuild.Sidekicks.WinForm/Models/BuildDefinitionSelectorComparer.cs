﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Models
{
    /// <summary>
    /// Build definition selector - comparer.
    /// </summary>
    public class BuildDefinitionSelectorComparer : IEqualityComparer<BuildDefinitionSelector>
    {
        public bool Equals(BuildDefinitionSelector buildDefinitionSelector1, BuildDefinitionSelector buildDefinitionSelector2)
        {
            return buildDefinitionSelector1.BuildDefinition.Id == buildDefinitionSelector2.BuildDefinition.Id;

        }

        public int GetHashCode(BuildDefinitionSelector buildDefinitionSelector)
        {
            return buildDefinitionSelector.BuildDefinition.Id;
        }
    }
}
