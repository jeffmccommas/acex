﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.Services.Notifications.WebApi;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Models
{

    /// <summary>
    /// Notification subscription display.
    /// </summary>
    public class NotificationSubscriptionDisplay
    {

        #region Private Constants

        private string SubscriptionStatus_JailedByNotificationsVolume = "Jailed By Notification Volume";
        private string SubscriptionStatus_PendingDeletion = "Pending Deletion";
        private string SubscriptionStatus_DisabledBySystem = "Disabled By System";
        private string SubscriptionStatus_DisabledInactiveIdentity = "Disabled Inactive Identity";
        private string SubscriptionStatus_DisabledMessageQueueNotSupported = "Disabled Message Queue Not Supported";
        private string SubscriptionStatus_DisabledMissingIdentity = "Disabled Missing Identity";
        private string SubscriptionStatus_DisabledInvalidRoleExpression = "Disabled Invalid Role Expression";
        private string SubscriptionStatus_DisabledInvalidPathClause = "Disabled Invalid Path Clause";
        private string SubscriptionStatus_DisabledAsDuplicateOfDefault = "Disabled As Duplicate Of Default";
        private string SubscriptionStatus_DisabledByAdmin = "Disabled By Admin";
        private string SubscriptionStatus_Disabled = "Disabled";
        private string SubscriptionStatus_Enabled = "Enabled";
        private string SubscriptionStatus_EnabledOnProbation = "Enabled On Probation";

        #endregion

        #region Public Constants
        #endregion

        #region Private Data Members

        private bool _selected;
        private string _id;
        private string _statusAsText;
        private string _description;
        private string _extra;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Selected.
        /// </summary>
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        /// <summary>
        /// Property: Id.
        /// </summary>
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// Property: Status.
        /// </summary>
        public string StatusAsText
        {
            get { return _statusAsText; }
            set { _statusAsText = value; }
        }

        /// <summary>
        /// Property: Description.
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// Property: Extra.
        /// </summary>
        public string Extra
        {
            get { return _extra; }
            set { _extra = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public NotificationSubscriptionDisplay()
        {
        }

        /// <summary>
        /// Create from build.
        /// </summary>
        public NotificationSubscriptionDisplay(NotificationSubscription notificationSubscription)
        {

            if (notificationSubscription == null)
            {
                throw new ArgumentNullException(string.Format("Build is required."));
            }

            _id = notificationSubscription.Id;
            _statusAsText = this.ConvertStatusToText(notificationSubscription.Status);
            _description = notificationSubscription.Description;
        }

        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        protected string ConvertStatusToText(SubscriptionStatus subscriptionStatus)
        {
            string result = string.Empty;

            try
            {
                switch (subscriptionStatus)
                {
                    case SubscriptionStatus.JailedByNotificationsVolume:
                        result = SubscriptionStatus_JailedByNotificationsVolume;
                        break;
                    case SubscriptionStatus.PendingDeletion:
                        result = SubscriptionStatus_PendingDeletion;
                        break;
                    case SubscriptionStatus.DisabledBySystem:
                        result = SubscriptionStatus_DisabledBySystem;
                        break;
                    case SubscriptionStatus.DisabledInactiveIdentity:
                        result = SubscriptionStatus_DisabledInactiveIdentity;
                        break;
                    case SubscriptionStatus.DisabledMessageQueueNotSupported:
                        result = SubscriptionStatus_DisabledMessageQueueNotSupported;
                        break;
                    case SubscriptionStatus.DisabledMissingIdentity:
                        result = SubscriptionStatus_DisabledMissingIdentity;
                        break;
                    case SubscriptionStatus.DisabledInvalidRoleExpression:
                        result = SubscriptionStatus_DisabledInvalidRoleExpression;
                        break;
                    case SubscriptionStatus.DisabledInvalidPathClause:
                        result = SubscriptionStatus_DisabledInvalidPathClause;
                        break;
                    case SubscriptionStatus.DisabledAsDuplicateOfDefault:
                        result = SubscriptionStatus_DisabledAsDuplicateOfDefault;
                        break;
                    case SubscriptionStatus.DisabledByAdmin:
                        result = SubscriptionStatus_DisabledByAdmin;
                        break;
                    case SubscriptionStatus.Disabled:
                        result = SubscriptionStatus_Disabled;
                        break;
                    case SubscriptionStatus.Enabled:
                        result = SubscriptionStatus_Enabled;
                        break;
                    case SubscriptionStatus.EnabledOnProbation:
                        result = SubscriptionStatus_EnabledOnProbation;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        #endregion

        #region Private Methods
        #endregion
    }
}
