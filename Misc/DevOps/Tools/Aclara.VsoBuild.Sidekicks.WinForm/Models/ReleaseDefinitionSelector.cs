﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.VisualStudio.Services.ReleaseManagement.WebApi;


namespace Aclara.VsoBuild.Sidekicks.WinForm.Models
{
    public class ReleaseDefinitionSelector : IComparer<ReleaseDefinitionSelector>
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private bool _selected;
        private bool _enabled = false;
        private bool _highlighted;
        private int _id;
        private string _name = string.Empty;
        private string _comment;
        private ReleaseDefinition _releaseDefinition;
        private string _extra;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Selected.
        /// </summary>
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        /// <summary>
        /// Property: Enabled.
        /// </summary>
        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        /// <summary>
        /// Property: Highlighted.
        /// </summary>
        public bool Highlighted
        {
            get { return _highlighted; }
            set { _highlighted = value; }
        }

        /// <summary>
        /// Property: definition identifier.
        /// </summary>
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// Property: Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Property: Comment.
        /// </summary>
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        /// <summary>
        /// Property: Release definition.
        /// </summary>
        public ReleaseDefinition ReleaseDefinition
        {
            get { return _releaseDefinition; }
            set { _releaseDefinition = value; }
        }

        /// <summary>
        /// Property: Extra.
        /// </summary>
        public string Extra
        {
            get { return _extra; }
            set { _extra = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ReleaseDefinitionSelector()
        {

        }

        #endregion

        #region Public Methods


        /// <summary>
        /// Compare between two release definition selector instances.
        /// </summary>
        /// <param name="releaseDefinitionSelector1"></param>
        /// <param name="releaseDefinitionSelector2"></param>
        /// <returns></returns>
        public int Compare(ReleaseDefinitionSelector releaseDefinitionSelector1,
                           ReleaseDefinitionSelector releaseDefinitionSelector2)
        {
            int result = 1;

            if (releaseDefinitionSelector1 != null && releaseDefinitionSelector2 != null)
            {
                result = releaseDefinitionSelector1.Name.CompareTo(releaseDefinitionSelector2.Name);
            }

            return result;
        }

        /// <summary>
        /// Retrieve year month branch name from release definition selector.
        /// </summary>
        /// <returns></returns>
        public string GetYearMonthBranchName()
        {
            string result = string.Empty;
            string matchedYearMonthBranchName = string.Empty;
            Match match = null;
            string pattern = string.Empty;
            string yearMonthBranchName = string.Empty;

            try
            {

                pattern = @"(\w+)_(\d+).(\d+[I]?)_";

                match = Regex.Match(this.Name, pattern, RegexOptions.IgnoreCase);

                //Match found.
                if (match.Success == true)
                {
                    if (match.Groups.Count == 4)
                    {
                        yearMonthBranchName = match.Groups[2].Value + "." + match.Groups[3].Value;
                        if (result.Contains(yearMonthBranchName) == false)
                        {
                            result = yearMonthBranchName;
                        }
                    }
                }

                return result;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Retrieve release definition name prefix from release definition selector.
        /// </summary>
        /// <returns></returns>
        public string GetNamePrefix()
        {
            string result = string.Empty;
            string matchedYearMonthBranchName = string.Empty;
            Match match = null;
            string pattern = string.Empty;
            string branchNamePrefix = string.Empty;

            try
            {

                pattern = @"(\w+)_(\d+).(\d+[I]?)_";

                match = Regex.Match(this.Name, pattern, RegexOptions.IgnoreCase);

                //Match found.
                if (match.Success == true)
                {
                    if (match.Groups.Count == 4)
                    {
                        branchNamePrefix = match.Groups[0].Value;
                        if (result.Contains(branchNamePrefix) == false)
                        {
                            result = branchNamePrefix;
                        }
                    }
                }

                return result;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Retrieve release definition name prefix without year.month branch from release definition selector.
        /// </summary>
        /// <returns></returns>
        public string GetNamePrefixWithoutYearMonthBranch()
        {
            string result = string.Empty;
            string matchedYearMonthBranchName = string.Empty;
            Match match = null;
            string pattern = string.Empty;
            string branchNamePrefix = string.Empty;

            try
            {

                pattern = @"(\w+)_(\d+).(\d+[I]?)_";

                match = Regex.Match(this.Name, pattern, RegexOptions.IgnoreCase);

                //Match found.
                if (match.Success == true)
                {
                    if (match.Groups.Count == 4)
                    {
                        branchNamePrefix = match.Groups[1].Value;
                        if (result.Contains(branchNamePrefix) == false)
                        {
                            result = branchNamePrefix;
                        }
                    }
                }

                return result;

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }
}
