﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Models
{
    /// <summary>
    /// This class provides methods to encrypt and decrypt strings which
    /// allows storing encrypted network passwords in plain text configuration files.
    /// </summary>
    public class ProtectedDataManager
    {
        #region Private Constaints

        #endregion

        #region Private Data Members

        private static byte[] entropyFromSaltValue = System.Text.Encoding.Unicode.GetBytes("{33262706-7AA0-4962-A58E-07BC3700D52C}");

        #endregion

        #region Public Properties

        #endregion

        #region Public Constructors

        #endregion

        #region Private Constructors

        #endregion

        #region Public Methods

        /// <summary>
        /// Encrypt string.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public string EncryptString(string plainText)
        {
            string result = string.Empty;
            SimpleAES simpleAES = null;

            try
            {

                //Validate input parameter: Plain text is required.
                if (string.IsNullOrEmpty(plainText) == true)
                {
                    throw new ArgumentNullException(string.Format("Argument 'plainText' is required."));
                }

                simpleAES = new SimpleAES();

                result = simpleAES.EncryptToString(plainText);

            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        /// <summary>
        /// Decrypt string.
        /// </summary>
        /// <param name="encryptedData"></param>
        /// <returns></returns>
        public String DecryptString(string encryptedData)
        {
            String result = string.Empty;
            SimpleAES simpleAES = null;

            try
            {

                //Validate input parameter: Plain text is required.
                if (string.IsNullOrEmpty(encryptedData) == true)
                {
                    throw new ArgumentNullException(string.Format("Argument 'encryptedData' is required."));
                }

                simpleAES = new SimpleAES();

                result = simpleAES.DecryptString(encryptedData);

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        #endregion

        #region Private Methods

        #endregion

    }

    #region Embedded Encryption Class

    /// <summary>
    /// Simple AES (Advanced Encryption Standard).
    /// </summary>
    public class SimpleAES
    {
        //Reference: http://stackoverflow.com/questions/165808/simple-two-way-encryption-for-c-sharp

        #region Private Data Members

        private byte[] Key = { 101, 141, 212, 27, 60, 141, 191, 205, 177, 167, 45, 252, 39, 236, 88, 186, 122, 188, 138, 239, 235, 23, 33, 167, 39, 169, 142, 240, 29, 7, 96, 56 };
        private byte[] Vector = { 160, 145, 243, 134, 91, 77, 32, 27, 218, 64, 204, 233, 91, 26, 36, 117 };

        private ICryptoTransform EncryptorTransform, DecryptorTransform;
        private System.Text.UTF8Encoding UTFEncoder;

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SimpleAES()
        {
            //This is our encryption method
            RijndaelManaged rm = new RijndaelManaged();

            //Create an encryptor and a decryptor using our encryption method, key, and vector.
            EncryptorTransform = rm.CreateEncryptor(this.Key, this.Vector);
            DecryptorTransform = rm.CreateDecryptor(this.Key, this.Vector);

            //Used to translate bytes to text and vice versa
            UTFEncoder = new System.Text.UTF8Encoding();
        }

        #endregion

        #region Public Static Methods

        /// Generates an encryption key.
        static public byte[] GenerateEncryptionKey()
        {
            //Generate a Key.
            RijndaelManaged rm = new RijndaelManaged();
            rm.GenerateKey();
            return rm.Key;
        }

        /// Generates a unique encryption vector.
        static public byte[] GenerateEncryptionVector()
        {
            //Generate a Vector
            RijndaelManaged rm = new RijndaelManaged();
            rm.GenerateIV();
            return rm.IV;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Encrypt plain text into encrypted string.
        /// </summary>
        /// <param name="TextValue"></param>
        /// <returns></returns>
        public string EncryptToString(string TextValue)
        {
            return ByteArrToString(Encrypt(TextValue));
        }

        /// <summary>
        /// Encrypt plain text into an encrypted byte array.
        /// </summary>
        /// <param name="TextValue"></param>
        /// <returns></returns>
        public byte[] Encrypt(string TextValue)
        {
            //Translates our text value into a byte array.
            Byte[] bytes = UTFEncoder.GetBytes(TextValue);

            //Used to stream the data in and out of the CryptoStream.
            MemoryStream memoryStream = new MemoryStream();

            /*
             * We will have to write the unencrypted bytes to the stream,
             * then read the encrypted result back from the stream.
             */
            #region Write the decrypted value to the encryption stream
            CryptoStream cs = new CryptoStream(memoryStream, EncryptorTransform, CryptoStreamMode.Write);
            cs.Write(bytes, 0, bytes.Length);
            cs.FlushFinalBlock();
            #endregion

            #region Read encrypted value back out of the stream
            memoryStream.Position = 0;
            byte[] encrypted = new byte[memoryStream.Length];
            memoryStream.Read(encrypted, 0, encrypted.Length);
            #endregion

            //Clean up.
            cs.Close();
            memoryStream.Close();

            return encrypted;
        }

        /// <summary>
        /// Decrypt encrypted string into plain text.
        /// </summary>
        /// <param name="EncryptedString"></param>
        /// <returns></returns>
        public string DecryptString(string EncryptedString)
        {
            return Decrypt(StrToByteArray(EncryptedString));
        }


        /// <summary>
        /// Decrypte encrypted byte array into plain text.
        /// </summary>
        /// <param name="EncryptedValue"></param>
        /// <returns></returns>
        public string Decrypt(byte[] EncryptedValue)
        {
            #region Write the encrypted value to the decryption stream
            MemoryStream encryptedStream = new MemoryStream();
            CryptoStream decryptStream = new CryptoStream(encryptedStream, DecryptorTransform, CryptoStreamMode.Write);
            decryptStream.Write(EncryptedValue, 0, EncryptedValue.Length);
            decryptStream.FlushFinalBlock();
            #endregion

            #region Read the decrypted value from the stream.
            encryptedStream.Position = 0;
            Byte[] decryptedBytes = new Byte[encryptedStream.Length];
            encryptedStream.Read(decryptedBytes, 0, decryptedBytes.Length);
            encryptedStream.Close();
            #endregion
            return UTFEncoder.GetString(decryptedBytes);
        }

        /// <summary>
        /// Convert string into byte array.
        /// </summary>
        /// <remarks>
        /// Convert a string to a byte array.  NOTE: Normally we'd create a Byte Array from a string using an ASCII encoding (like so).
        /// System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
        /// return encoding.GetBytes(str);
        /// However, this results in character values that cannot be passed in a URL.  So, instead, I just
        /// lay out all of the byte values in a long string of numbers (three per - must pad numbers less than 100).
        /// </remarks>
        /// <param name="str"></param>
        /// <returns></returns>
        public byte[] StrToByteArray(string str)
        {
            if (str.Length == 0)
                throw new Exception("Invalid string value in StrToByteArray");

            byte val;
            byte[] byteArr = new byte[str.Length / 3];
            int i = 0;
            int j = 0;
            do
            {
                val = byte.Parse(str.Substring(i, 3));
                byteArr[j++] = val;
                i += 3;
            }
            while (i < str.Length);
            return byteArr;
        }

        /// <summary>
        /// Convert byte array to string.
        /// </summary>
        /// <remarks>
        /// Same comment as above.  Normally the conversion would use an ASCII encoding in the other direction:
        /// System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
        /// return enc.GetString(byteArr);    
        /// </remarks>
        /// <param name="byteArr"></param>
        /// <returns></returns>
        public string ByteArrToString(byte[] byteArr)
        {
            byte val;
            string tempStr = "";
            for (int i = 0; i <= byteArr.GetUpperBound(0); i++)
            {
                val = byteArr[i];
                if (val < (byte)10)
                    tempStr += "00" + val.ToString();
                else if (val < (byte)100)
                    tempStr += "0" + val.ToString();
                else
                    tempStr += val.ToString();
            }
            return tempStr;
        }
    }

        #endregion

    #endregion
}
