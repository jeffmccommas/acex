﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Models
{

    /// <summary>
    /// Build status display.
    /// </summary>
    public class BuildStatusDisplay
    {

        #region Private Constants
        #endregion

        #region Public Constants

        public const string BuildReason_None = "None";
        public const string BuildReason_Manual = "Manual";
        public const string BuildReason_IndividualCI = "IndividualCI";
        public const string BuildReason_BatchedCI = "BatchedCI#";
        public const string BuildReason_Schedule = "Schedule";
        public const string BuildReason_UserCreated = "UserCreated";
        public const string BuildReason_ValidateShelveset = "alidateShelveset";
        public const string BuildReason_CheckInShelveset = "CheckInShelveset";
        public const string BuildReason_Triggered = "Triggered";
        public const string BuildReason_All = "All";

        public const string BuildResult_None = "None";
        public const string BuildResult_Succeeded = "Succeeded";
        public const string BuildResult_PartiallySucceeded = "PartiallySucceeded";
        public const string BuildResult_Failed = "Failed";
        public const string BuildResult_Canceled = "Canceled";

        public const string BuildStatus_None = "None";
        public const string BuildStatus_InProgress = "InProgress";
        public const string BuildStatus_Completed = "Completed";
        public const string BuildStatus_Cancelling = "Cancelling";
        public const string BuildStatus_Postponed = "Postponed";
        public const string BuildStatus_NotStarted = "NotStarted";
        public const string BuildStatus_All = "All";

        #endregion

        #region Private Data Members

        private int _id;
        private int _definitionReferenceID;
        private string _reason;
        private string _result;
        private string _buildNumber;
        private string _buildDefinitionName;
        private string _queueTime;
        private string _startTime;
        private string _finishTime;
        private string _buildDuration;
        private string _status;
        private string _requestedFor;
        private string _queueName;
        private string _extra;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Id.
        /// </summary>
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// Property: Definition reference id.
        /// </summary>
        public int DefinitionReferenceID
        {
            get { return _definitionReferenceID; }
            set { _definitionReferenceID = value; }
        }

        /// <summary>
        /// Property: Reason.
        /// </summary>
        public string Reason
        {
            get { return _reason; }
            set { _reason = value; }
        }

        /// <summary>
        /// Property: Result.
        /// </summary>
        public string Result
        {
            get { return _result; }
            set { _result = value; }
        }

        /// <summary>
        /// Property: Build number.
        /// </summary>
        public string BuildNumber
        {
            get { return _buildNumber; }
            set { _buildNumber = value; }
        }

        /// <summary>
        /// Property: Build definition name.
        /// </summary>
        public string BuildDefinitionName
        {
            get { return _buildDefinitionName; }
            set { _buildDefinitionName = value; }
        }

        /// <summary>
        /// Property: Queue time.
        /// </summary>
        public string QueueTime
        {
            get { return _queueTime; }
            set { _queueTime = value; }
        }

        /// <summary>
        /// Property: Start time.
        /// </summary>
        public string StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        /// <summary>
        /// Property: Finish time.
        /// </summary>
        public string FinishTime
        {
            get { return _finishTime; }
            set { _finishTime = value; }
        }

        /// <summary>
        /// Property: Build duration.
        /// </summary>
        public string BuildDuration
        {
            get { return _buildDuration; }
            set { _buildDuration = value; }
        }

        /// <summary>
        /// Property: Status.
        /// </summary>
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        /// <summary>
        /// Property: Requested for.
        /// </summary>
        public string RequestedFor
        {
            get { return _requestedFor; }
            set { _requestedFor = value; }
        }

        /// <summary>
        /// Property: Queue name.
        /// </summary>
        public string QueueName
        {
            get { return _queueName; }
            set { _queueName = value; }
        }

        /// <summary>
        /// Property: Extra.
        /// </summary>
        public string Extra
        {
            get { return _extra; }
            set { _extra = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildStatusDisplay()
        {
        }

        /// <summary>
        /// Create from build.
        /// </summary>
        public BuildStatusDisplay(Microsoft.TeamFoundation.Build.WebApi.Build build)
        {

            if (build == null)
            {
                throw new ArgumentNullException(string.Format("Build is required."));
            }

            _id = build.Id;
            _definitionReferenceID = build.Definition.Id;
            _reason = ConvertBuildReasonToText(build.Reason);
            _result = ConvertBuildResultToText(build.Result);
            _buildNumber = build.BuildNumber;
            _buildDefinitionName = build.Definition.Name;
            _queueTime = FormatNullableDateTime(build.QueueTime);
            _startTime = FormatNullableDateTime(build.StartTime);
            _finishTime = FormatNullableDateTime(build.FinishTime);
            _buildDuration = CalculateBuildDuration(build.StartTime, build.FinishTime);
            _status = ConvertBuildStatusToText(build.Status.Value);
            _requestedFor = build.RequestedFor.DisplayName;
            if(build.Queue != null)
            { 
                _queueName = build.Queue.Name;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Override ToString() method.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}\t{1}\t{2}",
                                this.Result,
                                this.Reason,
                                this.BuildNumber);
        }

        #endregion

        #region Protected Methods

        protected string FormatNullableDateTime(DateTime? dateTime)
        {
            string result = string.Empty;
            DateTime localDateTime;

            try
            {
                if (dateTime.HasValue == true)
                {
                    localDateTime = (DateTime)dateTime;
                    localDateTime = localDateTime.ToLocalTime();
                    result = string.Format("{0:yyyy/MM/dd HH:mm:ss}", localDateTime);
                }
                else
                {
                    result = "N/A";
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


        /// <summary>
        /// Convert build reason to text.
        /// </summary>
        /// <param name="buildReason"></param>
        /// <returns></returns>
        protected string ConvertBuildReasonToText(Microsoft.TeamFoundation.Build.WebApi.BuildReason buildReason)
        {
            string result = string.Empty;

            try
            {
                switch (buildReason)
                {
                    case Microsoft.TeamFoundation.Build.WebApi.BuildReason.None:
                        result = BuildReason_None;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildReason.Manual:
                        result = BuildReason_Manual;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildReason.IndividualCI:
                        result = BuildReason_IndividualCI;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildReason.BatchedCI:
                        result = BuildReason_BatchedCI;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildReason.Schedule:
                        result = BuildReason_Schedule;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildReason.UserCreated:
                        result = BuildReason_UserCreated;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildReason.ValidateShelveset:
                        result = BuildReason_ValidateShelveset;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildReason.CheckInShelveset:
                        result = BuildReason_CheckInShelveset;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildReason.Triggered:
                        result = BuildReason_Triggered;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildReason.All:
                        result = BuildReason_All;
                        break;
                    default:
                        result = BuildReason_None;
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Convert build result to ext.
        /// </summary>
        /// <param name="buildResult"></param>
        /// <returns></returns>
        protected string ConvertBuildResultToText(Microsoft.TeamFoundation.Build.WebApi.BuildResult? buildResult)
        {
            string result = string.Empty;

            try
            {
                switch (buildResult)
                {
                    case Microsoft.TeamFoundation.Build.WebApi.BuildResult.None:
                        result = BuildResult_None;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildResult.Succeeded:
                        result = BuildResult_Succeeded;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildResult.PartiallySucceeded:
                        result = BuildResult_PartiallySucceeded;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildResult.Failed:
                        result = BuildResult_Failed;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildResult.Canceled:
                        result = BuildResult_Canceled;
                        break;
                    default:
                        result = BuildResult_None;
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Convert build status to text.
        /// </summary>
        /// <param name="buildStatus"></param>
        /// <returns></returns>
        protected string ConvertBuildStatusToText(Microsoft.TeamFoundation.Build.WebApi.BuildStatus buildStatus)
        {
            string result = string.Empty;

            try
            {
                switch (buildStatus)
                {
                    case Microsoft.TeamFoundation.Build.WebApi.BuildStatus.None:
                        result = BuildStatus_None;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildStatus.InProgress:
                        result = BuildStatus_InProgress;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildStatus.Completed:
                        result = BuildStatus_Completed;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildStatus.Cancelling:
                        result = BuildStatus_Cancelling;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildStatus.Postponed:
                        result = BuildStatus_Postponed;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildStatus.NotStarted:
                        result = BuildStatus_NotStarted;
                        break;
                    case Microsoft.TeamFoundation.Build.WebApi.BuildStatus.All:
                        result = BuildStatus_All;
                        break;
                    default:
                        result = BuildStatus_None;
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Calculate build duration.
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="finishTime"></param>
        /// <returns></returns>
        protected string CalculateBuildDuration(DateTime? startTime, DateTime? finishTime)
        {
            string result = string.Empty;
            TimeSpan duration;

            try
            {

                if (startTime.HasValue == true && finishTime.HasValue == true)
                {
                    duration = finishTime.Value - startTime.Value;
                    result = duration.ToString(@"hh\:mm\:ss");
                }
                else if (startTime.HasValue == true && finishTime.HasValue == false)
                {
                    duration = DateTime.Now - startTime.Value.ToLocalTime();
                    result = duration.ToString(@"hh\:mm\:ss") + @"*";
                }
                else
                {
                    result = @"N/A";
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Private Methods

        #endregion

    }

}
