﻿using Aclara.Vso.Build.Client.Models;
using Aclara.Vso.Build.Client.Utilities;
using Microsoft.TeamFoundation.Build.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Models
{
    /// <summary>
    /// Build definition selector list.
    /// </summary>
    public class BuildDefinitionSelectorList : List<BuildDefinitionSelector>
    {

        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildDefinitionSelectorList()
            : base()
        {

        }

        #endregion

        #region Public Members

        /// <summary>
        /// Retrieve source build definition selector list filtered by build definition name prefix.
        /// </summary>
        /// <param name="sourceBuildDefinitionNamePrefix"></param>
        /// <param name="exactMatch"></param>
        /// <returns></returns>
        public BuildDefinitionSelectorList GetSourceBuildDefinitionListFilterdByBuildDefinitionNamePattern(string buildDefinitionNamePattern, 
                                                                                                           bool exactMatch)
        {
            BuildDefinitionSelectorList result = null;

            try
            {
                result = new BuildDefinitionSelectorList();

                var filteredBuildDefinitionList = Wildcard.FilterListByNamePropertyAndPattern(this, buildDefinitionNamePattern, exactMatch);

                foreach (BuildDefinitionSelector buildDefinitionSelector in filteredBuildDefinitionList)
                {
                    result.Add(buildDefinitionSelector);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Return list of build definition identifiers.
        /// </summary>
        /// <returns></returns>
        public List<int> GetBuildDefinitionIdList()
        {
            List<int> result = null;

            try
            {
                result = this.Select(buildDefinition => buildDefinition.Id).Distinct().ToList();
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        /// <summary>
        /// Filter build definition list by build definition selector list.
        /// </summary>
        /// <param name="buildDefinitionList"></param>
        /// <returns></returns>
        public BuildDefinitionList GetBuildDefinitionListFilteredByBuildDefinitionSelectorList(BuildDefinitionList buildDefinitionList)
        {
            BuildDefinitionList result = null;

            try
            {
                result = new BuildDefinitionList();

                var filteredBuildDefinitionList = from buildDefinition in buildDefinitionList
                                                  where
                                                       (from buildDefinitionSelector in this select buildDefinitionSelector.Name).Contains(buildDefinition.Name)
                                                  select buildDefinition;

                foreach (BuildDefinition buildDefinition in filteredBuildDefinitionList)
                {
                    result.Add(buildDefinition);
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build definition list.
        /// </summary>
        /// <param name="buildDefinitionList"></param>
        /// <returns></returns>
        public BuildDefinitionList GetBuildDefinitionList()
        {
            BuildDefinitionList result = null;

            try
            {
                result = new BuildDefinitionList();

                foreach (BuildDefinitionSelector buildDefinitionSelector in this)
                {
                    result.Add(buildDefinitionSelector.BuildDefinition);
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


        /// <summary>
        /// Get build definition name prefix list.
        /// </summary>
        /// <returns></returns>
        public List<string> GetBuildDefinitionNamePrefixList()
        {
            List<string> result = null;
            string buildDefinitionNamePrefix = string.Empty;

            try
            {
                result = new List<string>();

                foreach (BuildDefinitionSelector buildDefinitionSelector in this)
                {
                    buildDefinitionNamePrefix = buildDefinitionSelector.GetNamePrefix();
                    if (result.Contains(buildDefinitionNamePrefix) != true)
                    {
                        result.Add(buildDefinitionNamePrefix);
                    }
                }

                return result;     
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Does build definition list contain build definition(s) matching tag list?
        /// </summary>
        /// <returns></returns>
        public bool ContainsBuildDefinitionTagMatch(List<string> buildDefinitionTagMatchList)
        {
            bool result = false;

            try
            {
                foreach (BuildDefinitionSelector buildDefinitionSelector in this)
                {
                    //Iterate list. BEGIN.
                    if (string.IsNullOrEmpty(buildDefinitionSelector.Name) == false &&
                        buildDefinitionTagMatchList.Any(buildDefinitionSelector.Name.ToLower().EndsWith) == true)
                    {
                        result = true;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        #endregion

        #region Protected Members
        #endregion

    }

}
