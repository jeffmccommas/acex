﻿using System;
using System.IO.Compression;
using System.IO;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Models
{
    /// <summary>
    /// Zip archive entry display.
    /// </summary>
    public class ZipArchiveEntryDisplay
    {

        #region Private Constants
        #endregion

        #region Public Constants
        #endregion

        #region Private Data Members

        private bool _selected;
        private string _name;
        private string _definitionAsJson;
        private string _extra;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Selected.
        /// </summary>
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        /// <summary>
        /// Property: Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Property: Definition as json.
        /// </summary>
        public string DefinitionAsJson
        {
            get { return _definitionAsJson; }
            set { _definitionAsJson = value; }
        }

        /// <summary>
        /// Property: Extra.
        /// </summary>
        public string Extra
        {
            get { return _extra; }
            set { _extra = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ZipArchiveEntryDisplay()
        {
        }

        /// <summary>
        /// Create from zip archive entry.
        /// </summary>
        public ZipArchiveEntryDisplay(ZipArchiveEntry zipArchiveEntry)
        {

            try
            {
                if (zipArchiveEntry == null)
                {
                    throw new ArgumentNullException(string.Format("Zip archive entry is required."));
                }

                _name = zipArchiveEntry.Name;
                using (Stream zipArchiveEntryStream = zipArchiveEntry.Open())
                {
                    using (StreamReader zipArchiveEntryStreamReader = new StreamReader(zipArchiveEntryStream))
                    {
                        _definitionAsJson = zipArchiveEntryStreamReader.ReadToEnd();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }
}
