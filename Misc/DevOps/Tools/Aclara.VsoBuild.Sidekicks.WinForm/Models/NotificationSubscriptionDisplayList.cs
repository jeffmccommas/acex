﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Models
{
    public class NotificationSubscriptionDisplayList : List<NotificationSubscriptionDisplay>
    {

        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public NotificationSubscriptionDisplayList()
            : base()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Return list of notification subscription identifiers.
        /// </summary>
        /// <returns></returns>
        public List<string> GetNotificationSubscriptionIdList()
        {
            List<string> result = null;

            try
            {
                result = this.Select(notficationSubscriptionDisplay => notficationSubscriptionDisplay.Id).Distinct().ToList();
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        #endregion
    }
}
