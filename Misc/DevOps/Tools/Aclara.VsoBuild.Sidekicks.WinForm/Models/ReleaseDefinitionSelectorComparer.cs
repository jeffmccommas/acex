﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Models
{
    public class ReleaseDefinitionSelectorComparer : IEqualityComparer<ReleaseDefinitionSelector>
    {
        public bool Equals(ReleaseDefinitionSelector releaseDefinitionSelector1, ReleaseDefinitionSelector releaseDefinitionSelector2)
        {
            return releaseDefinitionSelector1.ReleaseDefinition.Id == releaseDefinitionSelector2.ReleaseDefinition.Id;

        }

        public int GetHashCode(ReleaseDefinitionSelector releaseDefinitionSelector)
        {
            return releaseDefinitionSelector.ReleaseDefinition.Id;
        }
    }
}
