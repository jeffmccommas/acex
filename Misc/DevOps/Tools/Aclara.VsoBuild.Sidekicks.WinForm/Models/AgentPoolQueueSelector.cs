﻿using Microsoft.TeamFoundation.Build.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Models
{
    /// <summary>
    /// Agent pool queue selector.
    /// </summary>
    public class AgentPoolQueueSelector
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private int _id;
        private string _name;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Identifier.
        /// </summary>
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// Property: Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="agentPoolQueue"></param>
        public AgentPoolQueueSelector(AgentPoolQueue agentPoolQueue)
        {
            _id = agentPoolQueue.Id;
            _name = agentPoolQueue.Name;
        }

        #endregion

        #region Protected Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <remarks>
        /// Not perferred constructor.
        /// </remarks>
        protected AgentPoolQueueSelector()
        {

        }

        #endregion

        #region Public Methods
        #endregion

        #region Protect Methods
        #endregion

        #region Private Methods
        #endregion

    }
}
