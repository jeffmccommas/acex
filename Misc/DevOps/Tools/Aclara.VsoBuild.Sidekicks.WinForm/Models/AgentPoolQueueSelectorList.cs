﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Models
{

    /// <summary>
    /// Agent pool queue selector list.
    /// </summary>
    public class AgentPoolQueueSelectorList : List<AgentPoolQueueSelector>
    {

        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AgentPoolQueueSelectorList()
            : base()
        {

        }

        #endregion

        #region Public Members

        #endregion

        #region Protected Members
        #endregion
    }
}
