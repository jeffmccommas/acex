﻿using System;
using System.Runtime.Serialization;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Exceptions
{
    [Serializable]
    public class ConfigurationNotFoundException : Exception
    {

        #region Public Constructors

        public ConfigurationNotFoundException()
            : base()
        {
        }

        public ConfigurationNotFoundException(string message)
            : base(message)
        {
        }

        public ConfigurationNotFoundException(string message,
                                       Exception innerException)
            : base(message, innerException)
        {
        }

        #endregion

        #region Protected Constructors

        protected ConfigurationNotFoundException(SerializationInfo serializationInfo,
                                                 StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }

        #endregion

    }
}
