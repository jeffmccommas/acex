﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Events
{
    public class ReleaseDefinitionSelectionChangedEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private bool _oneOrMoreSelected = false;

        #endregion

        #region Public Properties

        public bool OneOrMoreSelected
        {
            get { return _oneOrMoreSelected; }
            set { _oneOrMoreSelected = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ReleaseDefinitionSelectionChangedEventArgs()
        {
            _oneOrMoreSelected = false;
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="oneOrMoreSelected"></param>
        public ReleaseDefinitionSelectionChangedEventArgs(bool oneOrMoreSelected)
        {
            _oneOrMoreSelected = oneOrMoreSelected;
        }

        #endregion
    }
}
