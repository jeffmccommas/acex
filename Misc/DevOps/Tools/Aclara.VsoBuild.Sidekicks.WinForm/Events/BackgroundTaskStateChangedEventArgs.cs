﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.VsoBuild.Sidekicks.WinForm.Types;


namespace Aclara.VsoBuild.Sidekicks.WinForm.Events
{
    public class BackgroundTaskStateChangedEventArgs : EventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private BackgroundTaskState _backgroundTaskState;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Background task state.
        /// </summary>
        public BackgroundTaskState BackgroundTaskState
        {
            get { return _backgroundTaskState; }
            set { _backgroundTaskState = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BackgroundTaskStateChangedEventArgs()
        {
            _backgroundTaskState = BackgroundTaskState.Unspecified;
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="oneOrMoreSelected"></param>
        public BackgroundTaskStateChangedEventArgs(BackgroundTaskState backgroundTaskState)
        {
            _backgroundTaskState = backgroundTaskState;
        }

        #endregion

    }
}