-------------------------------------------------------------------------------------------------------
Tool Installation
-------------------------------------------------------------------------------------------------------

1. WiX Toolset Install
v3.10.3 (Stable)
https://wix.codeplex.com/releases/view/624906
Description: Windows Installer XML (WiX) is a Windows installer setup program generator based on XML.

2.Wix Toolset Visual Studio 2017 Extension
Tools --> Extensions and Updates. Click Online and search for Wix Toolset Visual Studio 2017 Extension (or greater vs version).

-------------------------------------------------------------------------------------------------------
How to Deploy Aclara.VsoBuild.Sidekicks.WinForm
-------------------------------------------------------------------------------------------------------

1.0. Determine current version.
1.1. Open Settings.settings in project Aclara.VsoBuild.Sidekicks.WinForm
1.2. Application setting: SidekicksAutoUpdaterVersion contains the current version.

2.0. Increment current version.
2.1. Search/Replace Entire Solution without trailing .0
Example: 
      Replace all "1.0.4", "1.0.5", Subfolders, Find Results 1, "Entire Solution ( Including External Items )", "*.*"
        C:\vso\ACEx\15.12\Misc\DevOps\Tools\Aclara.VsoBuild.Sidekicks\Aclara.VsoBuild.Sidekicks.WinForm\app.config(63,24):                <value>1.0.5.0</value>
        C:\vso\ACEx\15.12\Misc\DevOps\Tools\Aclara.VsoBuild.Sidekicks\Aclara.VsoBuild.Sidekicks.WinForm\Aclara.VsoBuild.Sidekicks.WinForm.Setup\Product.wxs(9):           Version='1.0.2'
        C:\vso\ACEx\15.12\Misc\DevOps\Tools\Aclara.VsoBuild.Sidekicks\Aclara.VsoBuild.Sidekicks.WinForm\Properties\Settings.settings(12,34):      <Value Profile="(Default)">1.0.5.0</Value>
        C:\vso\ACEx\15.12\Misc\DevOps\Tools\Aclara.VsoBuild.Sidekicks\Aclara.VsoBuild.Sidekicks.WinForm\Aclara.VsoBuild.Sidekicks.WinForm.Setup\Product.wxs(9,21):           Version='1.0.5'
        C:\vso\ACEx\15.12\Misc\DevOps\Tools\Aclara.VsoBuild.Sidekicks\Aclara.VsoBuild.Sidekicks.WinForm\Aclara.VsoBuild.Sidekicks.Bootstrapper\Bundle.wxs(5,20):          Version='1.0.5.0'
        Total replaced: 4  Matching files: 4  Total files searched: 173
  
3.0. Build Solution.

4.0. Deploy Setup Boostrapper Program.
4.1. Create current version folder in the following location:
     \\discovery.energyguide.com\public\AclaraSW\Aclara VsoBuild Sidekicks
     Example:
     \\discovery.energyguide.com\public\AclaraSW\Aclara VsoBuild Sidekicks\v1.0.5.0
4.2.1 Copy following files to the current version folder:

      File #1: ChangeLog.txt
      Location: C:\vso\ACEx\15.12\Misc\DevOps\Tools\Aclara.VsoBuild.Sidekicks\Aclara.VsoBuild.Sidekicks.WinForm\
      
      File #2: Aclara.VsoBuild.Sidekicks.Bootstrapper.exe
      Location: C:\vso\ACEx\15.12\Misc\DevOps\Tools\Aclara.VsoBuild.Sidekicks\Aclara.VsoBuild.Sidekicks.WinForm\Aclara.VsoBuild.Sidekicks.Bootstrapper\bin\Debug
      
*** END ***      