﻿using Aclara.VsoBuild.Sidekicks.WinForm.Events;
using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Logging
{
    [Target("LogEventTarget")]
    public sealed class LogEventTarget : TargetWithLayout
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private string _host;

        #endregion

        #region Public Events

        public event EventHandler<CreateLogEntryEventArgs> LogEntryCreated;

        #endregion

        #region Public Properities

        /// <summary>
        /// Property: host.
        /// </summary>
        [RequiredParameter]
        public string Host
        {
            get
            {
                return _host;
            }
            set
            {
                _host = value;
            }

        }

        #endregion

        #region  Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public LogEventTarget()
        {
            this.Host = "localhost";
        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        protected override void Write(LogEventInfo logEvent)
        {
            CreateLogEntryEventArgs createLogEntryEventArgs = null;

            if (logEvent != null)
            {

                createLogEntryEventArgs = new CreateLogEntryEventArgs();

                createLogEntryEventArgs.LogEventInfo = logEvent;

                LogEntryCreated(this, createLogEntryEventArgs);

            }

        }

        #endregion

        #region Private Methods


        #endregion
    }
}
