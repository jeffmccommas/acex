﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Configuration
{
    public class SidekickConfigurationManager
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private string _configurationFileName;
        private string _configurationFilePath;
        private SidekickConfiguration _sidekickConfiguration;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Configuration file name.
        /// </summary>
        public string ConfigurationFileName
        {
            get { return _configurationFileName; }
            set { _configurationFileName = value; }
        }

        /// <summary>
        /// Property: Configuration file path.
        /// </summary>
        public string ConfigurationFilePath
        {
            get { return _configurationFilePath; }
            set { _configurationFilePath = value; }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="configurationFileName"></param>
        /// <param name="configurationFilePath"></param>
        public SidekickConfigurationManager(string configurationFileName, string configurationFilePath)
        {
            _configurationFileName = configurationFileName;
            _configurationFilePath = configurationFilePath;
            _sidekickConfiguration = new SidekickConfiguration();
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekickConfigurationManager()
        {

        }

        #endregion

        #region Public Methods

        public void InitializeConfiguration()
        {
            string configurationPathFileName = string.Empty;

            try
            {
                configurationPathFileName = Path.Combine(this.ConfigurationFilePath, this.ConfigurationFileName);

                using (StreamReader file = File.OpenText(configurationPathFileName))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    this.SidekickConfiguration = (SidekickConfiguration)serializer.Deserialize(file, typeof(SidekickConfiguration));
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }

    /// <summary>
    /// Sidekick configuration.
    /// </summary>
    public class SidekickConfiguration
    {
        #region Public Properties

        [JsonProperty("Documents")]
        public Documents Documents { get; set; }

        [JsonProperty("BuildDefinitionFolders")]
        public BuildDefinitionFolders BuildDefinitionFolders { get; set; }

        [JsonProperty("NotificationSubscriptionDefaults")]
        public NotificationSubscriptionDefaults NotificationSubscriptionDefaults { get; set; }

        [JsonProperty("VSTSAccountItemList")]
        public VSTSAccountItemList VSTSAccountItemList { get; set; }

        #endregion
    }

    /// <summary>
    /// Documents.
    /// </summary>
    public class Documents
    {

        [JsonProperty("Document")]
        public List<Document> Document { get; set; }

    }

    /// <summary>
    /// Document configuration information.
    /// </summary>
    public class Document
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("FileName")]
        public string FileName { get; set; }

        [JsonProperty("Path")]
        public string Path { get; set; }

        [JsonProperty("Visible")]
        public string Visible { get; set; }

    }

    /// <summary>
    /// Build definition folders.
    /// </summary>
    public class BuildDefinitionFolders
    {

        [JsonProperty("BuildDefinitionFolder")]
        public List<BuildDefinitionFolder> BuildDefinitionFolder { get; set; }

    }

    /// <summary>
    /// Build definition folder.
    /// </summary>
    public class BuildDefinitionFolder
    {
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("IsDefault")]
        public bool IsDefault { get; set; }
        [JsonProperty("IsNotSpecified")]
        public bool IsNotSpecified { get; set; }
    }

    /// <summary>
    /// Notification subscription defaults.
    /// </summary>
    public class NotificationSubscriptionDefaults
    {

        [JsonProperty("VSTSTeamProject")]
        public List<NotificationSubscriptionDefaults_VSTSTeamProject> VSTSTeamProject { get; set; }

    }

    /// <summary>
    /// Visual studio team service accounts.
    /// </summary>
    public class VSTSAccountItemList
    {

        [JsonProperty("VSTSAccountItem")]
        public List<VSTSAccountItem> VSTSAccountItem { get; set; }

    }

    /// <summary>
    /// Visual Studio Team Services Project.
    /// </summary>
    public class NotificationSubscriptionDefaults_VSTSTeamProject
    {

        [JsonProperty("VSTSTeamProjectName")]
        public string VSTSTeamProjectName { get; set; }

        [JsonProperty("SubscriberTeamName")]
        public string SubscriberTeamName { get; set; }

        [JsonProperty("CustomAddress")]
        public string CustomAddress { get; set; }

        [JsonProperty("Tag")]
        public string Tag { get; set; }
    }

    /// <summary>
    /// Visual studio team services account.
    /// </summary>
    public class VSTSAccountItem
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("URL")]
        public string URL { get; set; }

        [JsonProperty("Default")]
        public bool Default { get; set; }

    }
}
