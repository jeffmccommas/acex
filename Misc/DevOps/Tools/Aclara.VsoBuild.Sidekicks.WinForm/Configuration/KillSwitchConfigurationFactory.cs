﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Configuration
{

    public static class KillSwitchConfigurationFactory
    {
        /// <summary>
        /// Create sidekick configuration.
        /// </summary>
        /// <returns></returns>
        public static KillSwitchConfigurationManager CreateSidekickConfiguration(string configurationFileName, string configurationFilePath)
        {
            KillSwitchConfigurationManager result = null;
            try
            {
                result = new KillSwitchConfigurationManager(configurationFileName, configurationFilePath);

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}

