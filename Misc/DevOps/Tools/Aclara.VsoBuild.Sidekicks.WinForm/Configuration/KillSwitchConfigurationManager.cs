﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;


namespace Aclara.VsoBuild.Sidekicks.WinForm.Configuration
{
    /// <summary>
    /// Kill switch manager.
    /// </summary>
    public class KillSwitchConfigurationManager
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private string _configurationFileName;
        private string _configurationFilePath;
        private KillSwitchConfiguration _KillSwitchConfiguration;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Configuration file name.
        /// </summary>
        public string ConfigurationFileName
        {
            get { return _configurationFileName; }
            set { _configurationFileName = value; }
        }

        /// <summary>
        /// Property: Configuration file path.
        /// </summary>
        public string ConfigurationFilePath
        {
            get { return _configurationFilePath; }
            set { _configurationFilePath = value; }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public KillSwitchConfiguration KillSwitchConfiguration
        {
            get { return _KillSwitchConfiguration; }
            set { _KillSwitchConfiguration = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="configurationFileName"></param>
        /// <param name="configurationFilePath"></param>
        public KillSwitchConfigurationManager(string configurationFileName, string configurationFilePath)
        {
            _configurationFileName = configurationFileName;
            _configurationFilePath = configurationFilePath;
            _KillSwitchConfiguration = new KillSwitchConfiguration();
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public KillSwitchConfigurationManager()
        {

        }

        #endregion

        #region Public Methods

        public void InitializeConfiguration()
        {
            string configurationPathFileName = string.Empty;

            try
            {
                configurationPathFileName = Path.Combine(this.ConfigurationFilePath, this.ConfigurationFileName);

                using (StreamReader file = File.OpenText(configurationPathFileName))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    this.KillSwitchConfiguration = (KillSwitchConfiguration)serializer.Deserialize(file, typeof(KillSwitchConfiguration));
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }



    /// <summary>
    /// Sidekick configuration.
    /// </summary>
    public class KillSwitchConfiguration
    {
        [JsonProperty("KillSwitches")]
        public KillSwitches KillSwitches { get; set; }

    }

    /// <summary>
    /// Documents.
    /// </summary>
    public class KillSwitches
    {

        [JsonProperty("KillSwitch")]
        public List<KillSwitch> KillSwitch { get; set; }

    }

    /// <summary>
    /// Document configuration information.
    /// </summary>
    public class KillSwitch
    {
        [JsonProperty("VersionImpacted")]
        public string VersionImpacted { get; set; }

        [JsonProperty("Reason")]
        public string Reason { get; set; }

        [JsonProperty("RunStatus")]
        public string RunStatus { get; set; }

    }
}
