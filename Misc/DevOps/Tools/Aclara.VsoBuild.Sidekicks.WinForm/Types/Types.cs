﻿using System.ComponentModel;

namespace Aclara.VsoBuild.Sidekicks.WinForm.Types
{
    /// <summary>
    /// Information.
    /// </summary>
    public enum Information
    {
        Unspecified = 0,
        AutoUpdater = 1
    }

    public enum BackgroundTaskState
    {
        Unspecified = 0,
        Ready = 1,
        Running = 2,
        Completed = 3
    }

    public enum KillSwitchRunStatus
    {
        [Description("Do not run")]
        DoNotRun = 1,
        [Description("Run with warning")]
        RunWithWarning = 2
    }

}
