﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration
{
    public static class SidekickConfigurationManagerFactory
    {
        /// <summary>
        /// Create sidekick configuration.
        /// </summary>
        /// <returns></returns>
        public static SidekickConfigurationManager CreateSidekickConfiguration(string configurationFileName, string configurationFilePath)
        {
            SidekickConfigurationManager result = null;
            try
            {
                result = new SidekickConfigurationManager(configurationFileName, configurationFilePath);

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
