﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration
{
    public class SidekickConfigurationManager
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private string _configurationFileName;
        private string _configurationFilePath;
        private SidekickConfiguration _sidekickConfiguration;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Configuration file name.
        /// </summary>
        public string ConfigurationFileName
        {
            get { return _configurationFileName; }
            set { _configurationFileName = value; }
        }

        /// <summary>
        /// Property: Configuration file path.
        /// </summary>
        public string ConfigurationFilePath
        {
            get { return _configurationFilePath; }
            set { _configurationFilePath = value; }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="configurationFileName"></param>
        /// <param name="configurationFilePath"></param>
        public SidekickConfigurationManager(string configurationFileName, string configurationFilePath)
        {
            _configurationFileName = configurationFileName;
            _configurationFilePath = configurationFilePath;
            _sidekickConfiguration = new SidekickConfiguration();
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekickConfigurationManager()
        {

        }

        #endregion

        #region Public Methods

        public void InitializeConfiguration()
        {
            string configurationPathFileName = string.Empty;

            try
            {
                configurationPathFileName = Path.Combine(this.ConfigurationFilePath, this.ConfigurationFileName);

                using (StreamReader file = File.OpenText(configurationPathFileName))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    this.SidekickConfiguration = (SidekickConfiguration)serializer.Deserialize(file, typeof(SidekickConfiguration));
                }

            }
            catch (Exception)
            {

                throw;
            }
        }
        
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }

    /// <summary>
    /// Sidekick configuration.
    /// </summary>
    public class SidekickConfiguration
    {
        #region Private Constants

        //Regular expression replacement tokens.
        private const string RegEx_Replace_Pattern_WorkingFolder = @"\$\[WorkingFolder\]";
        private const string RegEx_Replace_Pattern_TeamProjectName = @"\$\[TeamProjectName\]";
        private const string RegEx_Replace_Pattern_BranchName = @"\$\[BranchName\]";

        #endregion

        #region Public Properties

        [JsonProperty("SolutionListConfigItem")]
        public SolutionListConfigItem SolutionListConfig { get; set; }

        [JsonProperty("WebAppConfigItem")]
        public WebAppConfigItem WebAppConfig { get; set; }

        [JsonProperty("AzureRedisCacheConfigItemList")]
        public AzureRedisCacheConfigItemList AzureRedisCacheConfigItemList { get; set; }

        [JsonProperty("Documents")]
        public DocumentItemList Documents { get; set; }

        [JsonProperty("VSTSAccountItemList")]
        public VSTSAccountItemList VSTSAccountItemList { get; set; }

        #endregion

        #region Public Methods
        /// Retrieve solution list configuration file path.
        /// </summary>
        /// <param name="teamProjectWorkingFolder"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        public string GetSolutionListConfigFilePath(string teamProjectWorkingFolder,
                                                    string teamProjectName,
                                                    string branchName)
        {
            string result = string.Empty;

            try
            {

                result = this.SolutionListConfig.Path;

                //Replace tokens.
                result = this.ReplaceTokenInTextWithValue(result, RegEx_Replace_Pattern_WorkingFolder, teamProjectWorkingFolder);
                result = this.ReplaceTokenInTextWithValue(result, RegEx_Replace_Pattern_TeamProjectName, teamProjectName);
                result = this.ReplaceTokenInTextWithValue(result, RegEx_Replace_Pattern_BranchName, branchName);

                result = Path.Combine(result, this.SolutionListConfig.FileName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve web application configuration file path.
        /// </summary>
        /// <param name="teamProjectWorkingFolder"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        public string GetWebAppConfigFilePath(string teamProjectWorkingFolder,
                                              string teamProjectName,
                                              string branchName)
        {
            string result = string.Empty;

            try
            {

                result = this.WebAppConfig.Path;

                //Replace tokens.
                result = this.ReplaceTokenInTextWithValue(result, RegEx_Replace_Pattern_WorkingFolder, teamProjectWorkingFolder);
                result = this.ReplaceTokenInTextWithValue(result, RegEx_Replace_Pattern_TeamProjectName, teamProjectName);
                result = this.ReplaceTokenInTextWithValue(result, RegEx_Replace_Pattern_BranchName, branchName);

                result = Path.Combine(result, this.WebAppConfig.FileName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Replace token in text with specified value.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="token"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string ReplaceTokenInTextWithValue(string text, string token, string value)
        {
            string result = string.Empty;

            result = Regex.Replace(text, token, value);

            return result;
        }

        #endregion
    }

    /// <summary>
    /// Solution list configuration item.
    /// </summary>
    public class SolutionListConfigItem
    {

        [JsonProperty("FileName")]
        public string FileName { get; set; }
        [JsonProperty("Path")]
        public string Path { get; set; }
    }

    /// <summary>
    /// Web application configuration item.
    /// </summary>
    public class WebAppConfigItem
    {

        [JsonProperty("FileName")]
        public string FileName { get; set; }
        [JsonProperty("Path")]
        public string Path { get; set; }
    }

    /// <summary>
    /// Azure Redis cache configuration item list.
    /// </summary>
    public class AzureRedisCacheConfigItemList
    {

        [JsonProperty("AzureRedisCacheConfigItem")]
        public List<AzureRedisCacheConfigItem> AzureRedisCacheConfigItem { get; set; }

    }

    /// <summary>
    /// Documents.
    /// </summary>
    public class DocumentItemList
    {

        [JsonProperty("DocumentItem")]
        public List<DocumentItem> DocumentItem { get; set; }

    }

    /// <summary>
    /// Visual studio team service accounts.
    /// </summary>
    public class VSTSAccountItemList
    {

        [JsonProperty("VSTSAccountItem")]
        public List<VSTSAccountItem> VSTSAccountItem{ get; set; }

    }

    /// <summary>
    /// Visual Studio Team Services Project.
    /// </summary>
    public class AzureRedisCacheConfigItem
    {

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Endpoint")]
        public string Endpoint { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("SSL")]
        public bool SSL { get; set; }

        [JsonProperty("ConnectRetry")]
        public int ConnectRetry { get; set; }

        [JsonProperty("ConnectTimeout")]
        public int ConnectTimeout { get; set; }

        [JsonProperty("SyncTimeout")]
        public int SyncTimeout { get; set; }
    }

    /// <summary>
    /// Document configuration information.
    /// </summary>
    public class DocumentItem
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("FileName")]
        public string FileName { get; set; }

        [JsonProperty("Path")]
        public string Path { get; set; }

        [JsonProperty("Visible")]
        public string Visible { get; set; }

    }

    /// <summary>
    /// Visual studio team services account.
    /// </summary>
    public class VSTSAccountItem
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("URL")]
        public string URL{ get; set; }

        [JsonProperty("Default")]
        public bool Default { get; set; }

    }
}
