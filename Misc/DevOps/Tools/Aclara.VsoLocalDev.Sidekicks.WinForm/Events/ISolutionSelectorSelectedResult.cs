﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Events
{
    public interface ISolutionSelectorSelectedResult
    {
        SolutionSelector SolutionSelector { get; set; }
    }
}
