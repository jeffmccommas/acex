﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aclara.NuGet.Core.Client.Models;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Events
{
    public class NuGetPackageInfoSelectedEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private NuGetPackageInfo _nuGetPackageInfo;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: NuGet package information.
        /// </summary>
        public NuGetPackageInfo NuGetPackageInfo
        {
            get { return _nuGetPackageInfo; }
            set { _nuGetPackageInfo = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public NuGetPackageInfoSelectedEventArgs()
        {
        }

        #endregion

    }
}
