﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Events
{
    public class SidekickSelectionChangedEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private string _sidekickName;
        private string _sidekickDescription;

        #endregion


        #region Public Properties

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get { return _sidekickName; }
            set { _sidekickName = value; }
        }

        /// <summary>
        /// Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get { return _sidekickDescription; }
            set { _sidekickDescription = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekickSelectionChangedEventArgs()
        {
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="logEntry"></param>
        public SidekickSelectionChangedEventArgs(string sidekickName, string sidekickDescription)
        {
            _sidekickName = sidekickName;
            _sidekickDescription = sidekickDescription;
        }

        #endregion
    }
}
