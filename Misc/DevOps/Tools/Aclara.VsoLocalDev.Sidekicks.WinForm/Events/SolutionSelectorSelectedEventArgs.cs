﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Events
{
    /// <summary>
    /// Solution selector selected event arguments.
    /// </summary>
    public class SolutionSelectorSelectedEventArgs : EventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private ISolutionSelectorSelectedResult _solutionSelectorSelectedResult;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Solution selector selected result.
        /// </summary>
        public ISolutionSelectorSelectedResult SolutionSelectorSelectedResult
        {
            get { return _solutionSelectorSelectedResult; }
            set { _solutionSelectorSelectedResult = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SolutionSelectorSelectedEventArgs()
        {
            _solutionSelectorSelectedResult = new SolutionSelectorSelectedResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="buildCompletedResult"></param>
        public SolutionSelectorSelectedEventArgs(ISolutionSelectorSelectedResult buildCompletedResult)
        {
            _solutionSelectorSelectedResult = buildCompletedResult;
        }

        #endregion
    }
}
