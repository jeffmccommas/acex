﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Events
{
    public class SolutionSelectorSelectedResult : ISolutionSelectorSelectedResult
    {

        #region Private Data Members

        private SolutionSelector myVar;

        #endregion

        #region Public Methods

        /// <summary>
        /// Property: Solution selector.
        /// </summary>
        public SolutionSelector SolutionSelector
        {
            get { return myVar; }
            set { myVar = value; }
        }

        #endregion
    }
}
