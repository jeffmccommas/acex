﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Events
{
    public class CreateLogEntryEventArgs : EventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private LogEventInfo _logEventInfo = null;

        #endregion

        #region Public Properties

        public LogEventInfo LogEventInfo
        {
            get { return _logEventInfo; }
            set { _logEventInfo = value; }
        }

        #endregion

        #region Public Constructors
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CreateLogEntryEventArgs()
        {
            _logEventInfo = new LogEventInfo();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="logEntry"></param>
        public CreateLogEntryEventArgs(LogEventInfo logEntry)
        {
            _logEventInfo = logEntry;
        }

        #endregion

    }
}
