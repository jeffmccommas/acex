﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aclara.NuGet.Core.Client.Models;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Events
{
    public class NuGetPackageVersionSelectionChangedEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private NuGetPackageInfo _nuGetPackageInfo;
        private NuGetPackageVersion _nuGetPackageVersion;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: NuGet package information.
        /// </summary>
        public NuGetPackageInfo NuGetPackageInfo
        {
            get { return _nuGetPackageInfo; }
            set { _nuGetPackageInfo = value; }
        }

        /// <summary>
        /// Property: NuGet package version.
        /// </summary>
        public NuGetPackageVersion NuGetPackageVersion
        {
            get { return _nuGetPackageVersion; }
            set { _nuGetPackageVersion = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public NuGetPackageVersionSelectionChangedEventArgs()
        {
        }

        #endregion
    }

}
