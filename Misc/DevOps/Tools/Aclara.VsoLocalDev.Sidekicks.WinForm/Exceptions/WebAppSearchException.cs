﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Exceptions
{
    [Serializable]
    public class WebAppSearchException : Exception
    {

        #region Public Constructors

        public WebAppSearchException()
            : base()
        {
        }

        public WebAppSearchException(string message)
            : base(message)
        {
        }

        public WebAppSearchException(string message,
                                       Exception innerException)
            : base(message, innerException)
        {
        }

        #endregion

        #region Protected Constructors

        protected WebAppSearchException(SerializationInfo serializationInfo,
                                          StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }

        #endregion

    }
}
