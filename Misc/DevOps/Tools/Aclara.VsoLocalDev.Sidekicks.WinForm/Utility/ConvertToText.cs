﻿using Microsoft.TeamFoundation.Build.WebApi;
using System;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Utility
{
    public static class ConvertToText
    {
        #region Private Constants

        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve formatted schedule from build defintion.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <returns></returns>
        public static string GetFormattedScheduleFromBuildDefinition(BuildDefinition buildDefinition)
        {
            const string Trigger_CheckIn = "Check-in";
            const string Trigger_None = "None";

            string result = string.Empty;
            ScheduleTrigger scheduleTrigger = null;
            string daysToBuildAsText = string.Empty;
            string timeZoneName = string.Empty;

            try
            {
                result = string.Format(Trigger_None);

                foreach (BuildTrigger buildTrigger in buildDefinition.Triggers)
                {
                    if (buildTrigger == null)
                    {
                        continue;
                    }

                    //Continuous itegration triggers.
                    if (buildTrigger.GetType() == typeof(ContinuousIntegrationTrigger))
                    {
                        result = string.Format(Trigger_CheckIn);
                    }

                    //Scheduled triggers.
                    if (buildTrigger.GetType() == typeof(ScheduleTrigger))
                    {

                        scheduleTrigger = (ScheduleTrigger)buildTrigger;
                        foreach (Schedule schedule in scheduleTrigger.Schedules)
                        {

                            result = string.Format("{0:00}:{1:00} {2} [{3}]",
                                                   schedule.StartHours,
                                                   schedule.StartMinutes,
                                                   GetTimeZoneName(schedule.TimeZoneId),
                                                   GetScheduleDays(schedule.DaysToBuild));
                            break;
                        }

                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
            return result;

        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Retrieve schedule days to build as formatted text.
        /// </summary>
        /// <param name="scheduleDays"></param>
        /// <returns></returns>
        private static string GetScheduleDays(ScheduleDays scheduleDays)
        {
            const string ScheduleDays_DayNotScheduled = "";
            const string ScheduleDays_Monday = "Mon";
            const string ScheduleDays_Tuesday = "Tue";
            const string ScheduleDays_Wednesday = "Wed";
            const string ScheduleDays_Thursday = "Thu";
            const string ScheduleDays_Friday = "Fri";
            const string ScheduleDays_Saturday = "Sat";
            const string ScheduleDays_Sunday = "Sun";

            string results = string.Empty;
            string timeZoneName = string.Empty;

            try
            {

                if (scheduleDays.HasFlag(ScheduleDays.Monday))
                {
                    results += ScheduleDays_Monday;
                }
                else
                {
                    results += ScheduleDays_DayNotScheduled;
                }
                if (scheduleDays.HasFlag(ScheduleDays.Tuesday))
                {
                    results += ScheduleDays_Tuesday;
                }
                else
                {
                    results += ScheduleDays_DayNotScheduled;
                }
                if (scheduleDays.HasFlag(ScheduleDays.Wednesday))
                {
                    results += ScheduleDays_Wednesday;
                }
                else
                {
                    results += ScheduleDays_DayNotScheduled;
                }
                if (scheduleDays.HasFlag(ScheduleDays.Thursday))
                {
                    results += ScheduleDays_Thursday;
                }
                else
                {
                    results += ScheduleDays_DayNotScheduled;
                }
                if (scheduleDays.HasFlag(ScheduleDays.Friday))
                {
                    results += ScheduleDays_Friday;
                }
                else
                {
                    results += ScheduleDays_DayNotScheduled;
                }
                if (scheduleDays.HasFlag(ScheduleDays.Saturday))
                {
                    results += ScheduleDays_Saturday;
                }
                else
                {
                    results += ScheduleDays_DayNotScheduled;
                }
                if (scheduleDays.HasFlag(ScheduleDays.Sunday))
                {
                    results += ScheduleDays_Sunday;
                }
                else
                {
                    results += ScheduleDays_DayNotScheduled;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return results;
        }

        /// <summary>
        /// Retrieve time zone name from identifier.
        /// </summary>
        /// <param name="timeZoneId"></param>
        /// <returns></returns>
        private static string GetTimeZoneName(string timeZoneId)
        {
            const string TimeZoneName_AtlanticStandardTime = "Atlantic Standard Time";
            const string TimeZoneName_AtlanticDaylightTime = "Atlantic Daylight Time";
            const string TimeZoneName_EasternStandardTime = "Eastern Standard Time";
            const string TimeZoneName_EasternDaylightTime = "Eastern Daylight Time";
            const string TimeZoneName_CentralStandardTime = "Central Standard Time";
            const string TimeZoneName_CentralDaylightTime = "Central Daylight Time";
            const string TimeZoneName_MountainStandardTime = "Mountain Standard Time";
            const string TimeZoneName_MountainDaylightTime = "Mountain Daylight Time";
            const string TimeZoneName_PacificStandardTime = "Pacific Standard Time";
            const string TimeZoneName_PacificDaylightTime = "Pacific Daylight Time";
            const string TimeZoneName_AlaskanStandardTime = "Alaskan Standard Time";
            const string TimeZoneName_AlaskanDaylighTime = "Alaskan Dayligh Time";
            const string TimeZoneName_HawaiianStandardTime = "Hawaiian Standard Time";
            const string TimeZoneName_HawaiianDaylightTime = "Hawaiian Daylight Time";

            const string TimeZoneName_AtlanticStandardTimeAbbr = "AST";
            const string TimeZoneName_AtlanticDaylightTimAbbre = "ADT";
            const string TimeZoneName_EasternStandardTimeAbbr = "EST";
            const string TimeZoneName_EasternDaylightTimeAbbr = "EDT";
            const string TimeZoneName_CentralStandardTimeAbbr = "CST";
            const string TimeZoneName_CentralDaylightTimeAbbr = "CDT";
            const string TimeZoneName_MountainStandardTimeAbbr = "MST";
            const string TimeZoneName_MountainDaylightTimeAbbr = "MDT";
            const string TimeZoneName_PacificStandardTimeAbbr = "PST";
            const string TimeZoneName_PacificDaylightTimeAbbr = "PDT";
            const string TimeZoneName_AlaskanStandardTimeAbbr = "AST";
            const string TimeZoneName_AlaskanDaylighTimeAbbr = "ADT";
            const string TimeZoneName_HawaiianStandardTimeAbbr = "HST";
            const string TimeZoneName_HawaiianDaylightTimeAbbr = "HDT";

            string result = string.Empty;

            try
            {
                switch (timeZoneId)
                {
                    case TimeZoneName_AtlanticStandardTime:
                        result = TimeZoneName_AtlanticStandardTimeAbbr;
                        break;
                    case TimeZoneName_AtlanticDaylightTime:
                        result = TimeZoneName_AtlanticDaylightTimAbbre;
                        break;

                    case TimeZoneName_EasternStandardTime:
                        result = TimeZoneName_EasternStandardTimeAbbr;
                        break;
                    case TimeZoneName_EasternDaylightTime:
                        result = TimeZoneName_EasternDaylightTimeAbbr;
                        break;

                    case TimeZoneName_CentralStandardTime:
                        result = TimeZoneName_CentralStandardTimeAbbr;
                        break;
                    case TimeZoneName_CentralDaylightTime:
                        result = TimeZoneName_CentralDaylightTimeAbbr;
                        break;

                    case TimeZoneName_MountainStandardTime:
                        result = TimeZoneName_MountainStandardTimeAbbr;
                        break;
                    case TimeZoneName_MountainDaylightTime:
                        result = TimeZoneName_MountainDaylightTimeAbbr;
                        break;

                    case TimeZoneName_PacificStandardTime:
                        result = TimeZoneName_PacificStandardTimeAbbr;
                        break;
                    case TimeZoneName_PacificDaylightTime:
                        result = TimeZoneName_PacificDaylightTimeAbbr;
                        break;

                    case TimeZoneName_AlaskanStandardTime:
                        result = TimeZoneName_AlaskanStandardTimeAbbr;
                        break;
                    case TimeZoneName_AlaskanDaylighTime:
                        result = TimeZoneName_AlaskanDaylighTimeAbbr;
                        break;

                    case TimeZoneName_HawaiianStandardTime:
                        result = TimeZoneName_HawaiianStandardTimeAbbr;
                        break;
                    case TimeZoneName_HawaiianDaylightTime:
                        result = TimeZoneName_HawaiianDaylightTimeAbbr;
                        break;

                    default:
                        result = timeZoneId;
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        #endregion
    }
}
