﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Utility
{

    /// <summary>
    /// Convert wildcard * and ? to regular expression.
    /// </summary>
    public class Wildcard : Regex
    {

        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        /// <summary>
        /// Initializes a wildcard with the given search pattern.
        /// </summary>
        /// <param name="pattern">The wildcard pattern to match.</param>
        public Wildcard(string pattern)
         : base(WildcardToRegex(pattern))
        {
        }

        /// <summary>
        /// Initializes a wildcard with the given search pattern and options.
        /// </summary>
        /// <param name="pattern">The wildcard pattern to match.</param>
        /// <param name="options">A combination of one or more
        /// <see cref="System.Text.RegexOptions"/>.</param>
        public Wildcard(string pattern, RegexOptions options)
         : base(WildcardToRegex(pattern), options)
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Converts a wildcard to a regex.
        /// </summary>
        /// <param name="pattern">The wildcard pattern to convert.</param>
        /// <returns>A regex equivalent of the given wildcard.</returns>
        public static string WildcardToRegex(string pattern)
        {
            return "^" + Regex.Escape(pattern).
             Replace("\\*", ".*").
             Replace("\\?", ".") + "$";
        }

        /// <summary>
        /// Filter list by "Name" property and pattern.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fullList"></param>
        /// <param name="buildDefinitionNamePattern"></param>
        /// <param name="exactMatch"></param>
        /// <returns></returns>
        public static IEnumerable<T> FilterListByNamePropertyAndPattern<T>(IEnumerable<T> fullList, string buildDefinitionNamePattern, bool exactMatch)
        {
            IEnumerable<T> result = null;
            List<string> tempBuildDefinitionNamePatternList = null;
            string tempBuildDefinitionNamePattern = string.Empty;
            List<string> buildDefinitionNamePatternList = null;
            List<Wildcard> wildCardList = null;
            string propertyName = "Name";
            PropertyInfo propertyInfo = null;

            try
            {
                buildDefinitionNamePattern = buildDefinitionNamePattern.ToLower();

                if (buildDefinitionNamePattern.Contains("\n") == true ||
                    buildDefinitionNamePattern.Contains("\r\n") == true)
                {
                    //Split multiple patterns into separate elements.
                    buildDefinitionNamePattern = buildDefinitionNamePattern.Replace(",", "");
                    buildDefinitionNamePattern = buildDefinitionNamePattern.Replace(";", "");
                    tempBuildDefinitionNamePatternList = buildDefinitionNamePattern.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                }
                else if (buildDefinitionNamePattern.Contains(",") == true)
                {
                    //Split patterns separated by comma into separate elements.
                    buildDefinitionNamePattern = buildDefinitionNamePattern.Replace(";", "");
                    tempBuildDefinitionNamePatternList = buildDefinitionNamePattern.Split(',').ToList();
                }
                else if (buildDefinitionNamePattern.Contains(";") == true)
                {
                    //Split patterns separated by semi-colon into separate elements.
                    buildDefinitionNamePattern = buildDefinitionNamePattern.Replace(",", "");
                    tempBuildDefinitionNamePatternList = buildDefinitionNamePattern.Split(';').ToList();
                }
                else
                {
                    //Keep the single pattern as single element.
                    tempBuildDefinitionNamePatternList = new List<string>();
                    tempBuildDefinitionNamePatternList.Add(buildDefinitionNamePattern);
                }

                buildDefinitionNamePatternList = new List<string>();

                foreach (string pattern in tempBuildDefinitionNamePatternList)
                {
                    tempBuildDefinitionNamePattern = pattern;

                    if (exactMatch == true)
                    {
                        tempBuildDefinitionNamePattern = tempBuildDefinitionNamePattern.Replace("*", string.Empty);
                        tempBuildDefinitionNamePattern = tempBuildDefinitionNamePattern.Replace("?", string.Empty);
                    }
                    else
                    {
                        //Add missing implied wildcard charaters.
                        if (tempBuildDefinitionNamePattern.StartsWith("*") == false)
                        {
                            tempBuildDefinitionNamePattern = "*" + tempBuildDefinitionNamePattern;
                        }
                        if (tempBuildDefinitionNamePattern.EndsWith("*") == false)
                        {
                            tempBuildDefinitionNamePattern += "*";
                        }
                    }
                    buildDefinitionNamePatternList.Add(tempBuildDefinitionNamePattern);
                }

                propertyInfo = fullList.First().GetType().GetProperty(propertyName);

                wildCardList = new List<Wildcard>();

                //Convert definition name patter list into wildcard list.
                foreach (string pattern1 in buildDefinitionNamePatternList)
                {
                    Wildcard wildcard = new Wildcard(pattern1, RegexOptions.IgnoreCase);
                    wildCardList.Add(wildcard);
                }

                var subsetBuildDefinitionSelectorList = from bd in fullList from w in wildCardList where w.IsMatch(propertyInfo.GetValue(bd).ToString()) == true select bd;
                result = subsetBuildDefinitionSelectorList;

                result.OrderBy(bd => propertyInfo.GetValue(bd).ToString());

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods
        #endregion
    }
}
