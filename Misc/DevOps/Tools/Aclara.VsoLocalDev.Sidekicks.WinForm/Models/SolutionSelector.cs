﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Build.Engine.Client;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Models
{
 
    /// <summary>
    /// Solution selector.
    /// </summary>
    public class SolutionSelector : IComparer<SolutionSelector>
    {

        #region Private Constants
        
        #endregion

        #region Private Data Members

        private bool _selected = false;
        private string _solutionName;
        private string _solutionPath;
        private string _solutionListName;
        private Dictionary<string, string> _msBuildProperties;
        private BuildStatus _buildStatus;
        private string buildOutput;
        private List<string> _buildWarningList;
        private List<string> _buildErrorList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Solution list name.
        /// </summary>
        public string SolutionListName
        {
            get { return _solutionListName; }
            set { _solutionListName = value; }
        }

        /// <summary>
        /// Property: Selected.
        /// </summary>
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        /// <summary>
        /// Property: Solution name.
        /// </summary>
        public string SolutionName
        {
            get { return _solutionName; }
            set { _solutionName = value; }
        }

        /// <summary>
        /// Property: Solution path.
        /// </summary>
        public string SolutionPath
        {
            get { return _solutionPath; }
            set { _solutionPath = value; }
        }

        /// <summary>
        /// Property: MSBuild Properties.
        /// </summary>
        public Dictionary<string, string> MSBuildProperties
        {
            get { return _msBuildProperties; }
            set { _msBuildProperties = value; }
        }

        /// <summary>
        /// Property: Build status.
        /// </summary>
        public BuildStatus BuildStatus
        {
            get { return _buildStatus; }
            set { _buildStatus = value; }
        }

        /// <summary>
        /// Property: Build output.
        /// </summary>
        public string BuildOutput
        {
            get { return buildOutput; }
            set { buildOutput = value; }
        }

        /// <summary>
        /// Property: Build warning list.
        /// </summary>
        public List<string> BuildWarningList
        {
            get { return _buildWarningList; }
            set { _buildWarningList = value; }
        }

        /// <summary>
        /// Property: Build error list.
        /// </summary>
        public List<string> BuildErrorList
        {
            get { return _buildErrorList; }
            set { _buildErrorList = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SolutionSelector()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Compare between two solution selector instances.
        /// </summary>
        /// <param name="solutionSelector1"></param>
        /// <param name="solutionSelector2"></param>
        /// <returns></returns>
        public int Compare(SolutionSelector solutionSelector1,
                           SolutionSelector solutionSelector2)
        {
            int result = 1;

            if (solutionSelector1 != null && solutionSelector2 != null)
            {
                result = solutionSelector1.SolutionName.CompareTo(solutionSelector2.SolutionName);
            }

            return result;
        }

        #endregion

        #region Protected Methods
        
        #endregion
    }
}
