﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Models
{
    public class SidekickViewHistoryList
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private List<SidekickViewHistory> _internalSidekickViewHistoryList;
        private SidekickViewHistory _currentSidekickViewHistory;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Current sidekick view history.
        /// </summary>
        public SidekickViewHistory CurrentSidekickViewHistory
        {
            get { return _currentSidekickViewHistory; }
            set { _currentSidekickViewHistory = value; }
        }

        #endregion

        #region Protected Properties

        /// <summary>
        /// Property: Internal sidekick view history list.
        /// </summary>
        protected List<SidekickViewHistory> InternalSidekickViewHistoryList
        {
            get { return _internalSidekickViewHistoryList; }
            set { _internalSidekickViewHistoryList = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekickViewHistoryList()
        {
            _currentSidekickViewHistory = new SidekickViewHistory();
            _internalSidekickViewHistoryList = new List<SidekickViewHistory>();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Add sidekick view to history list.
        /// </summary>
        /// <param name="sidekickView"></param>
        public void Add(SidekickView sidekickView)
        {
            SidekickViewHistory sidekickViewHistory = null;

            try
            {
                sidekickViewHistory = this.InternalSidekickViewHistoryList.Where(svh => svh.SidekickView.Name == sidekickView.Name).SingleOrDefault();

                if (sidekickViewHistory == null)
                {
                    sidekickViewHistory = new SidekickViewHistory();

                    sidekickViewHistory.SidekickView = sidekickView;
                    sidekickViewHistory.SequenceNumber = this.InternalSidekickViewHistoryList.Count + 1;
                    this.InternalSidekickViewHistoryList.Add(sidekickViewHistory);
                }

                this.CurrentSidekickViewHistory = sidekickViewHistory;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Is back available.
        /// </summary>
        /// <returns></returns>
        public bool IsBackAvailable()
        {
            bool result = false;

            try
            {
                if (this.CurrentSidekickViewHistory.SequenceNumber > 1)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Is forward available.
        /// </summary>
        /// <returns></returns>
        public bool IsForwardAvailable()
        {
            bool result = false;

            try
            {
                if (this.CurrentSidekickViewHistory.SequenceNumber < this.InternalSidekickViewHistoryList.Count)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Back.
        /// </summary>
        /// <returns></returns>
        public SidekickViewHistory Back()
        {
            SidekickViewHistory result = null;

            try
            {
                if (this.IsBackAvailable() == true)
                {
                    result = this.InternalSidekickViewHistoryList.Where(svh => svh.SequenceNumber == this.CurrentSidekickViewHistory.SequenceNumber - 1).SingleOrDefault();
                    this.CurrentSidekickViewHistory = result;
                }
                else
                {
                    result = this.CurrentSidekickViewHistory;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


        /// <summary>
        /// Forward.
        /// </summary>
        /// <returns></returns>
        public SidekickViewHistory Forward()
        {
            SidekickViewHistory result = null;

            try
            {
                if (this.IsForwardAvailable() == true)
                {
                    result = this.InternalSidekickViewHistoryList.Where(svh => svh.SequenceNumber == this.CurrentSidekickViewHistory.SequenceNumber + 1).SingleOrDefault();
                    this.CurrentSidekickViewHistory = result;

                }
                else
                {
                    result = this.CurrentSidekickViewHistory;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve sidekick view history list.
        /// </summary>
        /// <returns></returns>
        public List<SidekickViewHistory> GetSidekickViewHistoryList()
        {
            List<SidekickViewHistory> result = null;

            try
            {
                result = new List<SidekickViewHistory>();

                foreach (SidekickViewHistory sidekickViewHistory in this.InternalSidekickViewHistoryList)
                {
                    result.Add(sidekickViewHistory);

                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }
}
