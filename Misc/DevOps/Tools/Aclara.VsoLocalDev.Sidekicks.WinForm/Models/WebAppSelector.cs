﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Web.Administration.Client;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Models
{
    /// <summary>
    /// Web application selector.
    /// </summary>
    public class WebAppSelector : IComparer<WebAppSelector>
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private bool _selected = false;
        private string _siteName;
        private Guid _appID;
        private string _alias = string.Empty;
        private string _applicationPoolName = string.Empty;
        private string _path = string.Empty;
        private VirtualDirectoryList _virtualDirectoryList;
        private string _physicalPathActual = string.Empty;
        private bool _existsInIIS;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Selected.
        /// </summary>
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        /// <summary>
        /// Property: Site name.
        /// </summary>
        public string SiteName
        {
            get { return _siteName; }
            set { _siteName = value; }
        }

        /// <summary>
        /// Property: Application identifier.
        /// </summary>
        public Guid AppID
        {
            get { return _appID; }
            set { _appID = value; }
        }

        /// <summary>
        /// Property: Alias.
        /// </summary>
        public string Alias
        {
            get { return _alias; }
            set { _alias = value; }
        }

        /// <summary>
        /// Property: Application pool name.
        /// </summary>
        public string ApplicationPoolName
        {
            get { return _applicationPoolName; }
            set { _applicationPoolName = value; }
        }

        /// <summary>
        /// Property: Path.
        /// </summary>
        public string Path
        {
            get { return _path; }
            set { _path = value; }
        }

        /// <summary>
        /// Property: Virtual directory list.
        /// </summary>
        public VirtualDirectoryList VirtualDirectoryList
        {
            get { return _virtualDirectoryList; }
            set { _virtualDirectoryList = value; }
        }

        /// <summary>
        /// Property: Physical path actual.
        /// </summary>
        public string PhysicalPathActual
        {
            get { return _physicalPathActual; }
            set { _physicalPathActual = value; }

        }

        /// <summary>
        /// Property: Exists in IIS.
        /// </summary>
        public bool ExistsInIIS
        {
            get { return _existsInIIS; }
            set { _existsInIIS = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public WebAppSelector()
        {
            _virtualDirectoryList = new VirtualDirectoryList();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Compare between two web application selector instances.
        /// </summary>
        /// <param name="webAppSelector1"></param>
        /// <param name="webAppSelector2"></param>
        /// <returns></returns>
        public int Compare(WebAppSelector webAppSelector1,
                           WebAppSelector webAppSelector2)
        {
            int result = 1;

            if (webAppSelector1 != null && webAppSelector2 != null)
            {
                result = webAppSelector1.Path.CompareTo(webAppSelector2.Path);
            }

            return result;
        }

        /// <summary>
        /// Return web app from web app selector.
        /// </summary>
        /// <returns></returns>
        public WebApp GetWebAppFromWebAppSelector()
        {
            WebApp result = null;

            try
            {
                result = new WebApp();

                result.SiteName = this.SiteName;
                result.AppID = this.AppID;
                result.Alias = this.Alias;
                result.ApplicationPoolName = this.ApplicationPoolName;
                result.Path = this.Path;
                result.VirtualDirectoryList = this.VirtualDirectoryList;
                
                
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        #endregion
    }
}
