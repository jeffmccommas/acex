﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Models
{
    /// <summary>
    /// Solution selector list.
    /// </summary>
    public class SolutionSelectorList : List<SolutionSelector>
    {
        #region Private Constants
        
        #endregion

        #region Private Data Members

        private string _name = string.Empty;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public SolutionSelectorList()
        {

        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods
        
        #endregion
    }

}
