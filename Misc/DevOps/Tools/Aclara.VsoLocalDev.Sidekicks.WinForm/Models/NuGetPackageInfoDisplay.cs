﻿using System.ComponentModel;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Models
{
    /// <summary>
    /// NuGet package info display.
    /// </summary>
    public class NuGetPackageInfoDisplay
    {
        #region Private Constants

        private string _packageId;
        private string _version;
        private string _referencedPath;
        private string _extra;

        #endregion

        #region Private Data Members

        /// <summary>
        /// Property: Package id.
        /// </summary>
        [DisplayName("Package Id")]
        public string PackageId
        {
            get { return _packageId; }
            set { _packageId = value; }
        }

        /// <summary>
        /// Property: Verison.
        /// </summary>
        [DisplayName("Version")]
        public string Version
        {
            get { return _version; }
            set { _version = value; }
        }

        /// <summary>
        /// Property: Referenced path.
        /// </summary>
        [DisplayName("Referenced Path")]
        public string ReferencedPath
        {
            get { return _referencedPath; }
            set { _referencedPath = value; }
        }

        /// <summary>
        /// Property: Extra
        /// </summary>
        [DisplayName(" ")]
        public string Extra
        {
            get { return _extra; }
            set { _extra = value; }
        }

        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors
        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }
}
