﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Models
{
    /// <summary>
    /// Web application selector list.
    /// </summary>
    public class WebAppSelectorList : List<WebAppSelector>
    {
        #region Private Constants
        
        #endregion

        #region Private Data Members
        
        #endregion

        #region Public Properties
        
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public WebAppSelectorList()
        {

        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve source web application selector list filtered by web application path prefix.
        /// </summary>
        /// <param name="webAppPathPrefix"></param>
        /// <returns></returns>
        public WebAppSelectorList GetSourceWebAppListFilterdByWebAppPath(string webAppPathPrefix)
        {
            WebAppSelectorList result = null;

            try
            {

                var subsetWebAppSelectorList = this.Where(bd => bd.Path.ToLower().Contains(webAppPathPrefix.ToLower()));

                result = new WebAppSelectorList();

                foreach (WebAppSelector webAppSelector in subsetWebAppSelectorList)
                {
                    result.Add(webAppSelector);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Protected Methods
        
        #endregion
    }
}
