﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    
    /// <summary>
    /// Web app search form factory.
    /// </summary>
    public class WebAppSearchFormFactory
    {

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public WebAppSearchFormFactory()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create web app search form.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="selectableWebAppCriteria"></param>
        /// <param name="hideApplicationsSiteLevel"></param>
        /// <returns></returns>
        static public WebAppSearchForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                  SidekickConfiguration sidekickConfiguration,
                                                  WebAppSearchTypes.WebAppSelectability selectableWebAppCriteria,
                                                  bool hideApplicationsSiteLevel)
        {

            WebAppSearchForm result = null;
            WebAppSearchPresenter webAppSearchPresenter = null;

            try
            {

                result = new WebAppSearchForm(sidekickConfiguration, hideApplicationsSiteLevel);
                webAppSearchPresenter = new WebAppSearchPresenter(teamProjectInfo, sidekickConfiguration, result);

                result.WebAppSearchPresenter = webAppSearchPresenter;
                result.TopLevel = false;
                result.WebAppSelectability = selectableWebAppCriteria;

                webAppSearchPresenter.Initialize();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        } 

        #endregion
    }
}
