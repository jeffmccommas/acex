﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Events;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    public partial class SwitchLocalDevBranchForm : Form, ISwitchLocalDevBranchView, ISidekickInfo, ISidekickView
    {

        #region Private Constants

        private const string SidekickView_SidekickName = "SwitchLocalDevBranch";
        private const string SidekickView_SidekickDescription = "Switch Local Development Branch";

        private const string ValidationErrorMessage_TeamProjectNameNotSelected = "Team project not selected.";
        private const string ValidationErrorMessage_BranchNameSourceNotEntered = "Branch name source not entered.";
        private const string ValidationErrorMessage_WebApplicationNotSelected = "Web application not selected.";

        private const string ToolTips_Title_AdditionalInformation = "Additional Information.";

        private const string ToolTips_Text_WebAppicationPhysicalPath = "Check to switch web application physical path.";
        private const string ToolTips_Text_ItemsToSwitch = "Check items you wish to switch to specified branch.";
        private const string ToolTips_Text_StopDefaultWebSite = "Check to stop web site.";
        private const string ToolTips_Text_StartDefaultWebSite = "Check to start web site.";

        private const string SwitchableItem_WebAppVirtualDirectoryPath = "Web app virtual directory physical path";

        private const string ItemsToSwitchCheckedListBox_DisplayMember = "Text";
        private const string ItemsToSwitchCheckedListBox_ValueMember = "SwitchableItem";

        private const string RegEx_Replace_Pattern_SiteName = @"\$\[DefaultWebSite\]";

        private const string ValidationPromptCaption = "Switch Local Development Validation";

        private const string ValidationPromptMessage_SwitchWebAppVirtualDirectoryPhysicalPathButNotWebAppsSelected = "At least one web app must be selected to switch web app virtual directory physical path.";
        private const string ValidationPromptMessage_SelectedItemsRequireSiteStop = "Stop $[DefaultWebSite] is required with Items to Switch selected. Click 'Yes' to continue.";
        private const string ValidationPromptMessage_SiteStopSelectedButSiteStartWasNot = "Stop $[DefaultWebSite] requested by Start $[DefaultWebSite] was not. Click 'Yes' to continue.";

        #endregion

        #region Private Data Members

        private SidekickConfiguration _sidekickConfiguration;
        private WebAppSearchForm _webAppSearchForm = null;
        private SwitchLocalDevBranchPresenter _switchLocalDevBranchPresenter;

        #endregion

        #region Protected Embedded Classes

        protected class ItemToSwitch
        {

            #region Private Data Members

            private SwitchableItem _switchableItem;
            private string _text;

            #endregion

            #region Public Properties

            /// <summary>
            /// Property: Identifier.
            /// </summary>
            public SwitchableItem SwitchableItem
            {
                get { return _switchableItem; }
                set { _switchableItem = value; }
            }

            /// <summary>
            /// Property: Text.
            /// </summary>
            public string Text
            {
                get { return _text; }
                set { _text = value; }
            }

            #endregion

            #region Public Constructors

            /// <summary>
            /// Preferred constructor.
            /// </summary>
            public ItemToSwitch(SwitchableItem id, string text)
            {
                this.SwitchableItem = id;
                this.Text = text;
            }

            #endregion

            #region Protected Constructors

            /// <summary>
            /// Default constructor.
            /// </summary>
            protected ItemToSwitch()
            {

            }

            #endregion
        }

        #endregion

        #region Public Types

        public enum SwitchableItem
        {
            Unspecified = 0,
            WebAppVirtualDirectoryPhysicalPath = 1,
            RegisterGacAssmeblies = 2,
            RegisterComESO = 3,
            RegisterComMisc = 4,
            RegisterComSBE = 5,
            SwitchMachineConfigX86AppSettings = 6,
            SwitchMachineConfigX64AppSettings = 7
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickView_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickView_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Switch local development branch presenter.
        /// </summary>
        public SwitchLocalDevBranchPresenter SwitchLocalDevBranchPresenter
        {
            get { return _switchLocalDevBranchPresenter; }
            set { _switchLocalDevBranchPresenter = value; }
        }

        /// <summary>
        /// Property: Web app search form.
        /// </summary>
        public WebAppSearchForm WebAppSearchForm
        {
            get { return _webAppSearchForm; }
            set { _webAppSearchForm = value; }
        }

        /// <summary>
        /// Property: Switch web application virtual directories physical path.
        /// </summary>
        public bool SwitchWebAppVirtualDirectoryPhysicalPath
        {
            get
            {
                return GetItemsToSwitchCheckBoxListChecked(SwitchableItem.WebAppVirtualDirectoryPhysicalPath);
            }
            set
            {
                SetItemsToSwitchCheckBoxListChecked(SwitchableItem.WebAppVirtualDirectoryPhysicalPath, value);
            }
        }

        /// <summary>
        /// Property: Register shared bin.
        /// </summary>
        public bool RegisterGacAssmeblies
        {
            get
            {
                return GetItemsToSwitchCheckBoxListChecked(SwitchableItem.RegisterGacAssmeblies);
            }
            set
            {
                SetItemsToSwitchCheckBoxListChecked(SwitchableItem.RegisterGacAssmeblies, value);
            }
        }

        /// <summary>
        /// Property: Register COM ESO.
        /// </summary>
        public bool RegisterComESO
        {
            get
            {
                return GetItemsToSwitchCheckBoxListChecked(SwitchableItem.RegisterComESO);
            }
            set
            {
                SetItemsToSwitchCheckBoxListChecked(SwitchableItem.RegisterComESO, value);
            }
        }

        /// <summary>
        /// Property: Register COM Misc.
        /// </summary>
        public bool RegisterComMisc
        {
            get
            {
                return GetItemsToSwitchCheckBoxListChecked(SwitchableItem.RegisterComMisc);
            }
            set
            {
                SetItemsToSwitchCheckBoxListChecked(SwitchableItem.RegisterComMisc, value);
            }
        }

        /// <summary>
        /// Property: Register COM SBE.
        /// </summary>
        public bool RegisterComSBE
        {
            get
            {
                return GetItemsToSwitchCheckBoxListChecked(SwitchableItem.RegisterComSBE);
            }
            set
            {
                SetItemsToSwitchCheckBoxListChecked(SwitchableItem.RegisterComSBE, value);
            }
        }


        /// <summary>
        /// Property: Switch machine.config app settings (x86)
        /// </summary>
        public bool SwitchMachineConfigX86AppSettings
        {
            get
            {
                return GetItemsToSwitchCheckBoxListChecked(SwitchableItem.SwitchMachineConfigX86AppSettings);
            }
            set
            {
                SetItemsToSwitchCheckBoxListChecked(SwitchableItem.SwitchMachineConfigX86AppSettings, value);
            }
        }

        /// <summary>
        /// Property: Switch machine.config app settings (x64)
        /// </summary>
        public bool SwitchMachineConfigX64AppSettings
        {
            get
            {
                return GetItemsToSwitchCheckBoxListChecked(SwitchableItem.SwitchMachineConfigX64AppSettings);
            }
            set
            {
                SetItemsToSwitchCheckBoxListChecked(SwitchableItem.SwitchMachineConfigX64AppSettings, value);
            }
        }

        /// <summary>
        /// Property: Stop site.
        /// </summary>
        public bool StopSite
        {
            get
            {
                return this.StopSiteCheckBox.Checked;
            }
            set
            {
                this.StopSiteCheckBox.Checked = value;
            }
        }

        /// <summary>
        /// Property: Start site.
        /// </summary>
        public bool StartSite
        {
            get
            {
                return this.StartSiteCheckBox.Checked;
            }
            set
            {
                this.StartSiteCheckBox.Checked = value;
            }
        }

        /// <summary>
        /// Property: Create missing web apps.
        /// </summary>
        public bool CreateMissingWebApps
        {
            get
            {
                return this.CreateMissingWebAppsCheckBox.Checked;
            }
            set
            {
                this.CreateMissingWebAppsCheckBox.Checked = value;
            }
        }

        /// <summary>
        /// Property: Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get
            {
                return this.ApplyButton.Text;
            }
            set
            {
                this.ApplyButton.Text = value;
            }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get
            {
                return this.BackgroundTaskStatusLabel.Text;
            }
            set
            {
                this.BackgroundTaskStatusLabel.Text = value;
            }
        }

        /// <summary>
        /// Property: Is sidekick busy.
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;
                result = this.SwitchLocalDevBranchPresenter.IsBackgroundTaskBusy;
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="webAppSearchForm"></param>
        public SwitchLocalDevBranchForm(SidekickConfiguration sidekickConfiguration,
                                        WebAppSearchForm webAppSearchForm)
        {
            InitializeComponent();

            this.SidekickConfiguration = sidekickConfiguration;
            this.WebAppSearchForm = webAppSearchForm;
            this.InitializeControls();
            this.WebAppSearchPanel.DockControl(this.WebAppSearchForm);
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private SwitchLocalDevBranchForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            this.WebAppSearchForm.TeamProjectChanged();
            this.EnableControlsDependantOnTeamProject(true);

        }

        /// <summary>
        /// Apply button enable.
        /// </summary>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Refresh web application list.
        /// </summary>
        public void Refresh()
        {
            try
            {
                this.WebAppSearchForm.Refresh();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                //Reset IIS selected.
                if (this.StartSite == true)
                {
                    this.SwitchLocalDevBranchPresenter.StopSite(this.WebAppSearchForm.SiteName);
                }
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {

                if (this.StartSite == true)
                {
                    this.SwitchLocalDevBranchPresenter.StartSite(this.WebAppSearchForm.SiteName);
                }

                //Refresh view.
                this.Refresh();

                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {

                SwitchLocalDevBranchToolTip.SetToolTip(this.StopSiteCheckBox, ToolTips_Text_StopDefaultWebSite);
                SwitchLocalDevBranchToolTip.SetToolTip(this.ItemsToSwitchCheckedListBox, ToolTips_Text_ItemsToSwitch);
                SwitchLocalDevBranchToolTip.SetToolTip(this.StartSiteCheckBox, ToolTips_Text_StartDefaultWebSite);

                List<ItemToSwitch> itemToSwitchList = new List<ItemToSwitch>();

                itemToSwitchList.Add(new ItemToSwitch(SwitchableItem.WebAppVirtualDirectoryPhysicalPath, SwitchableItem_WebAppVirtualDirectoryPath));

                ((ListBox)this.ItemsToSwitchCheckedListBox).DataSource = itemToSwitchList;
                ((ListBox)this.ItemsToSwitchCheckedListBox).DisplayMember = ItemsToSwitchCheckedListBox_DisplayMember;
                ((ListBox)this.ItemsToSwitchCheckedListBox).ValueMember = ItemsToSwitchCheckedListBox_ValueMember;

                this.StopSiteCheckBox.Text = this.ReplaceTokenInTextWithValue(this.StopSiteCheckBox.Text, RegEx_Replace_Pattern_SiteName, this.WebAppSearchForm.SiteName);
                this.StartSiteCheckBox.Text = this.ReplaceTokenInTextWithValue(this.StartSiteCheckBox.Text, RegEx_Replace_Pattern_SiteName, this.WebAppSearchForm.SiteName);

                EnableControlsDependantOnTeamProject(false);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable (disable) controls dependant on team project.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnTeamProject(bool enable)
        {
            this.StopSiteCheckBox.Enabled = enable;
            this.ItemsToSwitchCheckedListBox.Enabled = enable;
            this.StartSiteCheckBox.Enabled = enable;
            this.ApplyButton.Enabled = enable;
        }

        /// <summary>
        /// Set items to switch checkbox list checked state.
        /// </summary>
        /// <param name="switchableItem"></param>
        /// <param name="checkedState"></param>
        protected void SetItemsToSwitchCheckBoxListChecked(SwitchableItem switchableItem, bool checkedState)
        {
            ItemToSwitch itemToSwitch = null;

            try
            {

                for (int i = 0; i < this.ItemsToSwitchCheckedListBox.Items.Count; i++)
                {
                    itemToSwitch = (ItemToSwitch)this.ItemsToSwitchCheckedListBox.Items[i];

                    if (itemToSwitch.SwitchableItem == switchableItem)
                    {
                        this.ItemsToSwitchCheckedListBox.SetItemChecked(i, checkedState);
                        break;
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Retrieve items to switch checkedbox list checked state.
        /// </summary>
        /// <param name="switchableItem"></param>
        /// <returns></returns>
        protected bool GetItemsToSwitchCheckBoxListChecked(SwitchableItem switchableItem)
        {
            bool result = false;
            ItemToSwitch itemToSwitch = null;

            try
            {

                for (int i = 0; i < this.ItemsToSwitchCheckedListBox.Items.Count; i++)
                {
                    itemToSwitch = (ItemToSwitch)this.ItemsToSwitchCheckedListBox.Items[i];

                    if (itemToSwitch.SwitchableItem == switchableItem)
                    {
                        result = this.ItemsToSwitchCheckedListBox.GetItemChecked(i);
                        break;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Replace token in text with specified value.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="token"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string ReplaceTokenInTextWithValue(string text, string token, string value)
        {
            string result = string.Empty;

            result = Regex.Replace(text, token, value);

            return result;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Hanlder: Apply button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            WebAppSelectorList webAppSelectorList = null;
            DialogResult promptDialogResult;
            string messageBoxText = string.Empty;

            try
            {

                //Validation: Team project collection or team project name are not specified.
                if (this.SwitchLocalDevBranchPresenter.TeamProjectInfo.TeamProjectCollectionUri == null ||
                    this.SwitchLocalDevBranchPresenter.TeamProjectInfo.TeamProjectName == string.Empty)
                {

                    MessageBox.Show(string.Format(ValidationErrorMessage_TeamProjectNameNotSelected),
                                    string.Format(ValidationPromptCaption),
                                    MessageBoxButtons.OK,
                                    System.Windows.Forms.MessageBoxIcon.Exclamation);
                    return;

                }

                //Retrieve web app selector list.
                webAppSelectorList = this.WebAppSearchForm.GetSelectedWebAppSelectorList();

                //Validation - Confirm: Prompt user to confirm switch web app virtual directory physical path but no web apps selected.
                if (this.SwitchWebAppVirtualDirectoryPhysicalPath == true &&
                   (webAppSelectorList.Count == 0))
                {

                    promptDialogResult = MessageBox.Show(string.Format(ValidationPromptMessage_SwitchWebAppVirtualDirectoryPhysicalPathButNotWebAppsSelected),
                                                         string.Format(ValidationPromptCaption),
                                                         MessageBoxButtons.OK,
                                                         System.Windows.Forms.MessageBoxIcon.Exclamation);
                    return;

                }

                //Validation - Confirm: Prompt user to confirm register with NO stop site.
                if (this.StopSite == false &&
                   (this.RegisterComESO == true ||
                    this.RegisterComMisc == true ||
                    this.RegisterComSBE == true))
                {
                    messageBoxText = this.ReplaceTokenInTextWithValue(ValidationPromptMessage_SelectedItemsRequireSiteStop,
                                                                      RegEx_Replace_Pattern_SiteName,
                                                                      this.WebAppSearchForm.SiteName);

                    promptDialogResult = MessageBox.Show(string.Format(messageBoxText),
                                                         string.Format(ValidationPromptCaption),
                                                         MessageBoxButtons.YesNoCancel,
                                                         System.Windows.Forms.MessageBoxIcon.Warning);
                    if (promptDialogResult != DialogResult.Yes)
                    {
                        return;
                    }

                }

                //Validation - Confirm: Prompt user to confirm stop site with NO start site.
                if (this.StopSite == true &&
                   (this.StartSite == false))
                {

                    messageBoxText = this.ReplaceTokenInTextWithValue(ValidationPromptMessage_SiteStopSelectedButSiteStartWasNot,
                                                                      RegEx_Replace_Pattern_SiteName,
                                                                      this.WebAppSearchForm.SiteName);
                    promptDialogResult = MessageBox.Show(string.Format(messageBoxText),
                                                         string.Format(ValidationPromptCaption),
                                                         MessageBoxButtons.YesNoCancel,
                                                         System.Windows.Forms.MessageBoxIcon.Warning);
                    if (promptDialogResult != DialogResult.Yes)
                    {
                        return;
                    }

                }

                if (this.StopSite == true)
                {
                    this.SwitchLocalDevBranchPresenter.StopSite(this.WebAppSearchForm.SiteName);
                }

                if (this.CreateMissingWebApps == true)
                {

                    this.SwitchLocalDevBranchPresenter.CreateMissingWebApps(webAppSelectorList);

                }

                //Execute switch local development tasks in background.
                if (this.SwitchWebAppVirtualDirectoryPhysicalPath == true)
                {

                    this.SwitchLocalDevBranchPresenter.ExecuteSwitchLocalDevBranchTasks(this.SwitchLocalDevBranchPresenter.TeamProjectInfo.TeamProjectCollectionUri,
                                                                                        this.SwitchLocalDevBranchPresenter.TeamProjectInfo.TeamProjectName,
                                                                                        this.WebAppSearchForm.BranchName,
                                                                                        webAppSelectorList);
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }
        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickSwitchBranch.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

    }
}
