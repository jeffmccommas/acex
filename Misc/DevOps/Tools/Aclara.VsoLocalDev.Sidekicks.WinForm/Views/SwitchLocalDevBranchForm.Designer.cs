﻿namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    partial class SwitchLocalDevBranchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.VerticalSplitter = new System.Windows.Forms.Splitter();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.ItemsToSwitchGroupBox = new System.Windows.Forms.GroupBox();
            this.ItemsToSwitchCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.AfterBranchSwitchGroupBox = new System.Windows.Forms.GroupBox();
            this.StartSiteCheckBox = new System.Windows.Forms.CheckBox();
            this.OptionsLabel = new System.Windows.Forms.Label();
            this.BeforeBranchSwitchGroupBox = new System.Windows.Forms.GroupBox();
            this.CreateMissingWebAppsCheckBox = new System.Windows.Forms.CheckBox();
            this.StopSiteCheckBox = new System.Windows.Forms.CheckBox();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.HelpButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.WebAppSearchPanel = new System.Windows.Forms.Panel();
            this.WebAppSearchPlaceholderLabel = new System.Windows.Forms.Label();
            this.SwitchLocalDevBranchToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.PropertiesPanel.SuspendLayout();
            this.ItemsToSwitchGroupBox.SuspendLayout();
            this.AfterBranchSwitchGroupBox.SuspendLayout();
            this.BeforeBranchSwitchGroupBox.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.WebAppSearchPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // VerticalSplitter
            // 
            this.VerticalSplitter.BackColor = System.Drawing.SystemColors.ControlDark;
            this.VerticalSplitter.Location = new System.Drawing.Point(700, 0);
            this.VerticalSplitter.Name = "VerticalSplitter";
            this.VerticalSplitter.Size = new System.Drawing.Size(6, 480);
            this.VerticalSplitter.TabIndex = 1;
            this.VerticalSplitter.TabStop = false;
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Controls.Add(this.ItemsToSwitchGroupBox);
            this.PropertiesPanel.Controls.Add(this.AfterBranchSwitchGroupBox);
            this.PropertiesPanel.Controls.Add(this.OptionsLabel);
            this.PropertiesPanel.Controls.Add(this.BeforeBranchSwitchGroupBox);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesPanel.Location = new System.Drawing.Point(700, 0);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(516, 480);
            this.PropertiesPanel.TabIndex = 0;
            // 
            // ItemsToSwitchGroupBox
            // 
            this.ItemsToSwitchGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ItemsToSwitchGroupBox.Controls.Add(this.ItemsToSwitchCheckedListBox);
            this.ItemsToSwitchGroupBox.Location = new System.Drawing.Point(15, 127);
            this.ItemsToSwitchGroupBox.Name = "ItemsToSwitchGroupBox";
            this.ItemsToSwitchGroupBox.Size = new System.Drawing.Size(273, 135);
            this.ItemsToSwitchGroupBox.TabIndex = 3;
            this.ItemsToSwitchGroupBox.TabStop = false;
            this.ItemsToSwitchGroupBox.Text = "Items to switch";
            // 
            // ItemsToSwitchCheckedListBox
            // 
            this.ItemsToSwitchCheckedListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ItemsToSwitchCheckedListBox.CheckOnClick = true;
            this.ItemsToSwitchCheckedListBox.FormattingEnabled = true;
            this.ItemsToSwitchCheckedListBox.HorizontalScrollbar = true;
            this.ItemsToSwitchCheckedListBox.Location = new System.Drawing.Point(6, 19);
            this.ItemsToSwitchCheckedListBox.Name = "ItemsToSwitchCheckedListBox";
            this.ItemsToSwitchCheckedListBox.Size = new System.Drawing.Size(261, 109);
            this.ItemsToSwitchCheckedListBox.TabIndex = 0;
            // 
            // AfterBranchSwitchGroupBox
            // 
            this.AfterBranchSwitchGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AfterBranchSwitchGroupBox.Controls.Add(this.StartSiteCheckBox);
            this.AfterBranchSwitchGroupBox.Location = new System.Drawing.Point(15, 268);
            this.AfterBranchSwitchGroupBox.Name = "AfterBranchSwitchGroupBox";
            this.AfterBranchSwitchGroupBox.Size = new System.Drawing.Size(273, 42);
            this.AfterBranchSwitchGroupBox.TabIndex = 4;
            this.AfterBranchSwitchGroupBox.TabStop = false;
            this.AfterBranchSwitchGroupBox.Text = "After branch switch";
            // 
            // StartSiteCheckBox
            // 
            this.StartSiteCheckBox.AutoSize = true;
            this.StartSiteCheckBox.Location = new System.Drawing.Point(6, 19);
            this.StartSiteCheckBox.Name = "StartSiteCheckBox";
            this.StartSiteCheckBox.Size = new System.Drawing.Size(138, 17);
            this.StartSiteCheckBox.TabIndex = 0;
            this.StartSiteCheckBox.Text = "Start $[DefaultWebSite]";
            this.StartSiteCheckBox.UseVisualStyleBackColor = true;
            // 
            // OptionsLabel
            // 
            this.OptionsLabel.AutoSize = true;
            this.OptionsLabel.Location = new System.Drawing.Point(12, 32);
            this.OptionsLabel.Name = "OptionsLabel";
            this.OptionsLabel.Size = new System.Drawing.Size(46, 13);
            this.OptionsLabel.TabIndex = 1;
            this.OptionsLabel.Text = "Options:";
            // 
            // BeforeBranchSwitchGroupBox
            // 
            this.BeforeBranchSwitchGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BeforeBranchSwitchGroupBox.Controls.Add(this.CreateMissingWebAppsCheckBox);
            this.BeforeBranchSwitchGroupBox.Controls.Add(this.StopSiteCheckBox);
            this.BeforeBranchSwitchGroupBox.Location = new System.Drawing.Point(15, 52);
            this.BeforeBranchSwitchGroupBox.Name = "BeforeBranchSwitchGroupBox";
            this.BeforeBranchSwitchGroupBox.Size = new System.Drawing.Size(273, 69);
            this.BeforeBranchSwitchGroupBox.TabIndex = 2;
            this.BeforeBranchSwitchGroupBox.TabStop = false;
            this.BeforeBranchSwitchGroupBox.Text = "Before branch switch";
            // 
            // CreateMissingWebAppsCheckBox
            // 
            this.CreateMissingWebAppsCheckBox.AutoSize = true;
            this.CreateMissingWebAppsCheckBox.Location = new System.Drawing.Point(6, 43);
            this.CreateMissingWebAppsCheckBox.Name = "CreateMissingWebAppsCheckBox";
            this.CreateMissingWebAppsCheckBox.Size = new System.Drawing.Size(143, 17);
            this.CreateMissingWebAppsCheckBox.TabIndex = 1;
            this.CreateMissingWebAppsCheckBox.Text = "Create missing web apps";
            this.CreateMissingWebAppsCheckBox.UseVisualStyleBackColor = true;
            // 
            // StopSiteCheckBox
            // 
            this.StopSiteCheckBox.AutoSize = true;
            this.StopSiteCheckBox.Location = new System.Drawing.Point(6, 19);
            this.StopSiteCheckBox.Name = "StopSiteCheckBox";
            this.StopSiteCheckBox.Size = new System.Drawing.Size(138, 17);
            this.StopSiteCheckBox.TabIndex = 0;
            this.StopSiteCheckBox.Text = "Stop $[DefaultWebSite]";
            this.StopSiteCheckBox.UseVisualStyleBackColor = true;
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.HelpButton);
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(6, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(510, 24);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(482, -1);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 7;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(476, 24);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "Switch Local Dev Branch";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ControlPanel
            // 
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Controls.Add(this.ApplyButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 440);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(516, 40);
            this.ControlPanel.TabIndex = 5;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(438, 11);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 1;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // WebAppSearchPanel
            // 
            this.WebAppSearchPanel.AutoScroll = true;
            this.WebAppSearchPanel.Controls.Add(this.WebAppSearchPlaceholderLabel);
            this.WebAppSearchPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.WebAppSearchPanel.Location = new System.Drawing.Point(0, 0);
            this.WebAppSearchPanel.Name = "WebAppSearchPanel";
            this.WebAppSearchPanel.Size = new System.Drawing.Size(700, 480);
            this.WebAppSearchPanel.TabIndex = 0;
            // 
            // WebAppSearchPlaceholderLabel
            // 
            this.WebAppSearchPlaceholderLabel.AutoSize = true;
            this.WebAppSearchPlaceholderLabel.Location = new System.Drawing.Point(164, 294);
            this.WebAppSearchPlaceholderLabel.Name = "WebAppSearchPlaceholderLabel";
            this.WebAppSearchPlaceholderLabel.Size = new System.Drawing.Size(148, 13);
            this.WebAppSearchPlaceholderLabel.TabIndex = 0;
            this.WebAppSearchPlaceholderLabel.Text = "Web App Search Placeholder";
            // 
            // SwitchLocalDevBranchToolTip
            // 
            this.SwitchLocalDevBranchToolTip.AutoPopDelay = 8000;
            this.SwitchLocalDevBranchToolTip.InitialDelay = 500;
            this.SwitchLocalDevBranchToolTip.IsBalloon = true;
            this.SwitchLocalDevBranchToolTip.ReshowDelay = 100;
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.VsoLocalDev.Sidekicks.Help.chm";
            // 
            // SwitchLocalDevBranchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 480);
            this.ControlBox = false;
            this.Controls.Add(this.VerticalSplitter);
            this.Controls.Add(this.PropertiesPanel);
            this.Controls.Add(this.WebAppSearchPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "SwitchLocalDevBranchForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Switch Branch";
            this.PropertiesPanel.ResumeLayout(false);
            this.PropertiesPanel.PerformLayout();
            this.ItemsToSwitchGroupBox.ResumeLayout(false);
            this.AfterBranchSwitchGroupBox.ResumeLayout(false);
            this.AfterBranchSwitchGroupBox.PerformLayout();
            this.BeforeBranchSwitchGroupBox.ResumeLayout(false);
            this.BeforeBranchSwitchGroupBox.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.WebAppSearchPanel.ResumeLayout(false);
            this.WebAppSearchPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Splitter VerticalSplitter;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Panel WebAppSearchPanel;
        private System.Windows.Forms.Label WebAppSearchPlaceholderLabel;
        private System.Windows.Forms.GroupBox ItemsToSwitchGroupBox;
        private System.Windows.Forms.CheckedListBox ItemsToSwitchCheckedListBox;
        private System.Windows.Forms.GroupBox AfterBranchSwitchGroupBox;
        private System.Windows.Forms.CheckBox StartSiteCheckBox;
        private System.Windows.Forms.Label OptionsLabel;
        private System.Windows.Forms.GroupBox BeforeBranchSwitchGroupBox;
        private System.Windows.Forms.CheckBox StopSiteCheckBox;
        private System.Windows.Forms.ToolTip SwitchLocalDevBranchToolTip;
        private System.Windows.Forms.CheckBox CreateMissingWebAppsCheckBox;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.Button HelpButton;
    }
}