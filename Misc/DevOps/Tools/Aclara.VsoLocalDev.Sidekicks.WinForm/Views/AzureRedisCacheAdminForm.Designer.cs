﻿namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    partial class AzureRedisCacheAdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.AzureRedisCacheOptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.AzureRedisCacheComboBox = new System.Windows.Forms.ComboBox();
            this.AzureRedisCacheLabel = new System.Windows.Forms.Label();
            this.OperationComboBox = new System.Windows.Forms.ComboBox();
            this.OperationLabel = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.HelpButton = new System.Windows.Forms.Button();
            this.PropertiesPanel.SuspendLayout();
            this.AzureRedisCacheOptionsGroupBox.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Controls.Add(this.AzureRedisCacheOptionsGroupBox);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(1000, 600);
            this.PropertiesPanel.TabIndex = 0;
            // 
            // AzureRedisCacheOptionsGroupBox
            // 
            this.AzureRedisCacheOptionsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AzureRedisCacheOptionsGroupBox.Controls.Add(this.AzureRedisCacheComboBox);
            this.AzureRedisCacheOptionsGroupBox.Controls.Add(this.AzureRedisCacheLabel);
            this.AzureRedisCacheOptionsGroupBox.Controls.Add(this.OperationComboBox);
            this.AzureRedisCacheOptionsGroupBox.Controls.Add(this.OperationLabel);
            this.AzureRedisCacheOptionsGroupBox.Location = new System.Drawing.Point(4, 30);
            this.AzureRedisCacheOptionsGroupBox.Name = "AzureRedisCacheOptionsGroupBox";
            this.AzureRedisCacheOptionsGroupBox.Size = new System.Drawing.Size(984, 525);
            this.AzureRedisCacheOptionsGroupBox.TabIndex = 0;
            this.AzureRedisCacheOptionsGroupBox.TabStop = false;
            this.AzureRedisCacheOptionsGroupBox.Text = "Azure Redis cache options";
            // 
            // AzureRedisCacheComboBox
            // 
            this.AzureRedisCacheComboBox.FormattingEnabled = true;
            this.AzureRedisCacheComboBox.Location = new System.Drawing.Point(117, 25);
            this.AzureRedisCacheComboBox.Name = "AzureRedisCacheComboBox";
            this.AzureRedisCacheComboBox.Size = new System.Drawing.Size(366, 21);
            this.AzureRedisCacheComboBox.TabIndex = 1;
            // 
            // AzureRedisCacheLabel
            // 
            this.AzureRedisCacheLabel.AutoSize = true;
            this.AzureRedisCacheLabel.Location = new System.Drawing.Point(10, 25);
            this.AzureRedisCacheLabel.Name = "AzureRedisCacheLabel";
            this.AzureRedisCacheLabel.Size = new System.Drawing.Size(100, 13);
            this.AzureRedisCacheLabel.TabIndex = 0;
            this.AzureRedisCacheLabel.Text = "Azure Redis cache:";
            // 
            // OperationComboBox
            // 
            this.OperationComboBox.FormattingEnabled = true;
            this.OperationComboBox.Location = new System.Drawing.Point(117, 53);
            this.OperationComboBox.Name = "OperationComboBox";
            this.OperationComboBox.Size = new System.Drawing.Size(367, 21);
            this.OperationComboBox.TabIndex = 3;
            // 
            // OperationLabel
            // 
            this.OperationLabel.AutoSize = true;
            this.OperationLabel.Location = new System.Drawing.Point(10, 56);
            this.OperationLabel.Name = "OperationLabel";
            this.OperationLabel.Size = new System.Drawing.Size(56, 13);
            this.OperationLabel.TabIndex = 2;
            this.OperationLabel.Text = "Operation:";
            // 
            // ControlPanel
            // 
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Controls.Add(this.ApplyButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 560);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(1000, 40);
            this.ControlPanel.TabIndex = 1;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(922, 11);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 1;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.VsoLocalDev.Sidekicks.Help.chm";
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.HelpButton);
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(1000, 24);
            this.PropertiesHeaderPanel.TabIndex = 2;
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(966, 24);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "Azure Redis Cache Admin";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(972, 1);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 4;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // AzureRedisCacheAdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 600);
            this.ControlBox = false;
            this.Controls.Add(this.PropertiesPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "AzureRedisCacheAdminForm";
            this.Text = "AzureRedisCacheAdminForm";
            this.PropertiesPanel.ResumeLayout(false);
            this.AzureRedisCacheOptionsGroupBox.ResumeLayout(false);
            this.AzureRedisCacheOptionsGroupBox.PerformLayout();
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.GroupBox AzureRedisCacheOptionsGroupBox;
        private System.Windows.Forms.ComboBox OperationComboBox;
        private System.Windows.Forms.Label OperationLabel;
        private System.Windows.Forms.ComboBox AzureRedisCacheComboBox;
        private System.Windows.Forms.Label AzureRedisCacheLabel;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.Button HelpButton;
    }
}