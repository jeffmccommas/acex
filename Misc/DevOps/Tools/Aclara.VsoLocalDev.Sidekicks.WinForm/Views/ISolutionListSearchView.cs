﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISolutionListSearchView
    {

        void TeamProjectChanged();

        void PopulateBranchNameList(List<string> branchNameList);

        void EnableControlsDependantOnTeamProject(bool enabled);

        SolutionSelectorLists SolutionSelectorLists { get; set; }

        SolutionSelectorList GetSelectedSolutionSelectorList();
    }
}
