﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Web application search types.
    /// </summary>
    public class WebAppSearchTypes
    {

        #region Public Types

        /// <summary>
        /// Enumeration: Web app selectability.
        /// </summary>
        public enum WebAppSelectability
        {
            Unspecified = 0,
            OnlySelectableWhenExistInIIS = 1,
            OnlySelectableWhenDoesNotExistInIIS = 2,
            AlwaysSelectable = 3
        }

        #endregion

    }
}
