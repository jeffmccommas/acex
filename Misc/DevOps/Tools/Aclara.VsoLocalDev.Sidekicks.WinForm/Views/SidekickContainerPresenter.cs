﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using Aclara.TeamFoundation.VersionControl.Client;
using Aclara.Tools.Configuration;
using Aclara.Web.Administration.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    public class SidekickContainerPresenter
    {
        #region Private Constants

        #endregion

        #region Private Data Members

        private ProjectManager _projectManager = null;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Project manager.
        /// </summary>
        public ProjectManager ProjectManager
        {
            get { return _projectManager; }
            set { _projectManager = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="solutionListSearchView"></param>
        /// <param name="sidekickConfiguration"></param>
        public SidekickContainerPresenter()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Determine whether team project is mapped to local folder.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        [Obsolete("Remove as method is not used.")]
        public bool IsTeamProjectMapped(string tfsServerAddress,
                                        string teamProjectName,
                                        string workspaceName,
                                        string userName)
        {
            bool result = false;

            try
            {
                this.ProjectManager = CreateProjectManager(tfsServerAddress, teamProjectName);

                result = this.ProjectManager.IsTeamProjectMappedToLocalFolder(teamProjectName, workspaceName, userName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Metods

        /// <summary>
        /// Create project manager.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        protected ProjectManager CreateProjectManager(string tfsServerAddress,
                                                      string teamProjectName)
        {
            ProjectManager result = null;
            Uri tfsServerUri = null;

            try
            {

                tfsServerUri = new Uri(tfsServerAddress);

                result = ProjectManagerFactory.CreateProjectManager(tfsServerUri, teamProjectName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


        #endregion
    }
}
