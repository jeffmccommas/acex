﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Basic authorization Rest API view - interface.
    /// </summary>
    public interface IBasicAuthRestAPIView
    {
        string UserProfileName { get; set; }
        string Password { get; set; }
        bool CredentialsandProjectPropertiesReadonly { get; set; }
    }
}
