﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// NuGet package analyzer view interface.
    /// </summary>
    public interface INuGetPackageAnalyzerView
    {

        string BackgroundTaskStatus { get; set; }

        bool IsSidekickBusy { get; }

        void TeamProjectChanged();

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);
    }
}
