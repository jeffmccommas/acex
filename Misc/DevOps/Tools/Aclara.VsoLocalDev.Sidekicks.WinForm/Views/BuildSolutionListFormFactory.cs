﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build solution list form factory.
    /// </summary>
    public class BuildSolutionListFormFactory
    {

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="logWriter"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="solutionListSearchForm"></param>
        /// <returns></returns>
        public static BuildSolutionListForm CreateForm(SidekickConfiguration sidekickConfiguration,
                                                       SolutionListSearchForm solutionListSearchForm)
        {

            BuildSolutionListForm result = null;
            BuildSolutionListPresenter buildSolutionListPresenter = null;

            try
            {
                result = new BuildSolutionListForm(sidekickConfiguration, solutionListSearchForm);

                buildSolutionListPresenter = new BuildSolutionListPresenter(sidekickConfiguration, result);

                result.BuildSolutionListPresenter = buildSolutionListPresenter;
                result.TopLevel = false;

                buildSolutionListPresenter.Initialize();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

    }
}
