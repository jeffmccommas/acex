﻿namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    partial class WebAppSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.WebAppDisplayContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.DisplayAllToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DisplaySelectedToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WebAppSelectContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SelectAllToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SelectNoneToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SelectContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.BranchLabel = new System.Windows.Forms.Label();
            this.BranchComboBox = new System.Windows.Forms.ComboBox();
            this.SiteNameLabel = new System.Windows.Forms.Label();
            this.SiteNameValueLabel = new System.Windows.Forms.Label();
            this.ApplicationsGroupBox = new System.Windows.Forms.GroupBox();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.WebAppPathPartialTextBox = new System.Windows.Forms.TextBox();
            this.WebAppPathFilterLabel = new System.Windows.Forms.Label();
            this.SearchButton = new System.Windows.Forms.Button();
            this.WebAppDataGridView = new System.Windows.Forms.DataGridView();
            this.ApplicationsSiteLevelGroupBox = new System.Windows.Forms.GroupBox();
            this.SiteLevelWebAppDataGridView = new System.Windows.Forms.DataGridView();
            this.SiteLevelWebAppSelectColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SiteLevelWebAppPathColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SiteLevelWebAppPhysicalPathColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShowTeamProjectPickerToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.DisplayWebAppConfigFileToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.WebAppConfigContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.OpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenContainingFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WebAppSelectColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.WebAppExistsInIISColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.WebAppPathColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WebAppPhysicalPathColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisplayWebAppConfigFileButton = new Aclara.VsoLocalDev.Sidekicks.WinForm.SplitButton();
            this.SelectSplitButton = new Aclara.VsoLocalDev.Sidekicks.WinForm.SplitButton();
            this.DisplaySplitButton = new Aclara.VsoLocalDev.Sidekicks.WinForm.SplitButton();
            this.WebAppDisplayContextMenuStrip.SuspendLayout();
            this.WebAppSelectContextMenuStrip.SuspendLayout();
            this.SelectContextMenuStrip.SuspendLayout();
            this.ApplicationsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WebAppDataGridView)).BeginInit();
            this.ApplicationsSiteLevelGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SiteLevelWebAppDataGridView)).BeginInit();
            this.WebAppConfigContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // WebAppDisplayContextMenuStrip
            // 
            this.WebAppDisplayContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DisplayAllToolStripItem,
            this.DisplaySelectedToolStripItem});
            this.WebAppDisplayContextMenuStrip.Name = "BuildDefinitionDisplayContextMenuStrip";
            this.WebAppDisplayContextMenuStrip.Size = new System.Drawing.Size(119, 48);
            // 
            // DisplayAllToolStripItem
            // 
            this.DisplayAllToolStripItem.Name = "DisplayAllToolStripItem";
            this.DisplayAllToolStripItem.Size = new System.Drawing.Size(118, 22);
            this.DisplayAllToolStripItem.Text = "All";
            this.DisplayAllToolStripItem.Click += new System.EventHandler(this.DisplayAllToolStripItem_Click);
            // 
            // DisplaySelectedToolStripItem
            // 
            this.DisplaySelectedToolStripItem.Name = "DisplaySelectedToolStripItem";
            this.DisplaySelectedToolStripItem.Size = new System.Drawing.Size(118, 22);
            this.DisplaySelectedToolStripItem.Text = "Selected";
            this.DisplaySelectedToolStripItem.Click += new System.EventHandler(this.DisplaySelectedToolStripItem_Click);
            // 
            // WebAppSelectContextMenuStrip
            // 
            this.WebAppSelectContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SelectAllToolStripItem,
            this.SelectNoneToolStripItem});
            this.WebAppSelectContextMenuStrip.Name = "BuildDefinitionSelectContextMenuStrip";
            this.WebAppSelectContextMenuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.WebAppSelectContextMenuStrip.Size = new System.Drawing.Size(104, 48);
            // 
            // SelectAllToolStripItem
            // 
            this.SelectAllToolStripItem.Name = "SelectAllToolStripItem";
            this.SelectAllToolStripItem.Size = new System.Drawing.Size(103, 22);
            this.SelectAllToolStripItem.Text = "All";
            this.SelectAllToolStripItem.Click += new System.EventHandler(this.SelectAllToolStripItem_Click);
            // 
            // SelectNoneToolStripItem
            // 
            this.SelectNoneToolStripItem.Name = "SelectNoneToolStripItem";
            this.SelectNoneToolStripItem.Size = new System.Drawing.Size(103, 22);
            this.SelectNoneToolStripItem.Text = "None";
            this.SelectNoneToolStripItem.Click += new System.EventHandler(this.SelectNoneToolStripItem_Click);
            // 
            // SelectContextMenuStrip
            // 
            this.SelectContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AllToolStripMenuItem,
            this.NoneToolStripMenuItem});
            this.SelectContextMenuStrip.Name = "SelectContextMenuStrip";
            this.SelectContextMenuStrip.Size = new System.Drawing.Size(104, 48);
            // 
            // AllToolStripMenuItem
            // 
            this.AllToolStripMenuItem.Name = "AllToolStripMenuItem";
            this.AllToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.AllToolStripMenuItem.Text = "All";
            this.AllToolStripMenuItem.Click += new System.EventHandler(this.AllToolStripMenuItem_Click);
            // 
            // NoneToolStripMenuItem
            // 
            this.NoneToolStripMenuItem.Name = "NoneToolStripMenuItem";
            this.NoneToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.NoneToolStripMenuItem.Text = "None";
            this.NoneToolStripMenuItem.Click += new System.EventHandler(this.NoneToolStripMenuItem_Click);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.OK;
            this.dataGridViewImageColumn1.MinimumWidth = 22;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.ReadOnly = true;
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewImageColumn1.Width = 22;
            // 
            // BranchLabel
            // 
            this.BranchLabel.AutoSize = true;
            this.BranchLabel.Location = new System.Drawing.Point(13, 9);
            this.BranchLabel.Name = "BranchLabel";
            this.BranchLabel.Size = new System.Drawing.Size(44, 13);
            this.BranchLabel.TabIndex = 0;
            this.BranchLabel.Text = "Branch:";
            // 
            // BranchComboBox
            // 
            this.BranchComboBox.FormattingEnabled = true;
            this.BranchComboBox.Location = new System.Drawing.Point(63, 6);
            this.BranchComboBox.Name = "BranchComboBox";
            this.BranchComboBox.Size = new System.Drawing.Size(121, 21);
            this.BranchComboBox.TabIndex = 1;
            this.BranchComboBox.TextChanged += new System.EventHandler(this.BranchComboBox_TextChanged);
            // 
            // SiteNameLabel
            // 
            this.SiteNameLabel.AutoSize = true;
            this.SiteNameLabel.Location = new System.Drawing.Point(190, 9);
            this.SiteNameLabel.Name = "SiteNameLabel";
            this.SiteNameLabel.Size = new System.Drawing.Size(57, 13);
            this.SiteNameLabel.TabIndex = 2;
            this.SiteNameLabel.Text = "Site name:";
            // 
            // SiteNameValueLabel
            // 
            this.SiteNameValueLabel.AutoSize = true;
            this.SiteNameValueLabel.Location = new System.Drawing.Point(253, 9);
            this.SiteNameValueLabel.Name = "SiteNameValueLabel";
            this.SiteNameValueLabel.Size = new System.Drawing.Size(93, 13);
            this.SiteNameValueLabel.TabIndex = 3;
            this.SiteNameValueLabel.Text = "<site name value>";
            // 
            // ApplicationsGroupBox
            // 
            this.ApplicationsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplicationsGroupBox.Controls.Add(this.RefreshButton);
            this.ApplicationsGroupBox.Controls.Add(this.ClearButton);
            this.ApplicationsGroupBox.Controls.Add(this.WebAppPathPartialTextBox);
            this.ApplicationsGroupBox.Controls.Add(this.WebAppPathFilterLabel);
            this.ApplicationsGroupBox.Controls.Add(this.SearchButton);
            this.ApplicationsGroupBox.Controls.Add(this.SelectSplitButton);
            this.ApplicationsGroupBox.Controls.Add(this.DisplaySplitButton);
            this.ApplicationsGroupBox.Controls.Add(this.WebAppDataGridView);
            this.ApplicationsGroupBox.Location = new System.Drawing.Point(5, 172);
            this.ApplicationsGroupBox.Name = "ApplicationsGroupBox";
            this.ApplicationsGroupBox.Size = new System.Drawing.Size(668, 311);
            this.ApplicationsGroupBox.TabIndex = 6;
            this.ApplicationsGroupBox.TabStop = false;
            this.ApplicationsGroupBox.Text = "Applications";
            // 
            // RefreshButton
            // 
            this.RefreshButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RefreshButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RefreshButton.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.refresh;
            this.RefreshButton.Location = new System.Drawing.Point(592, 17);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(32, 23);
            this.RefreshButton.TabIndex = 3;
            this.RefreshButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ClearButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ClearButton.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.DeleteHS;
            this.ClearButton.Location = new System.Drawing.Point(630, 17);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(32, 23);
            this.ClearButton.TabIndex = 4;
            this.ClearButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // WebAppPathPartialTextBox
            // 
            this.WebAppPathPartialTextBox.AcceptsReturn = true;
            this.WebAppPathPartialTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WebAppPathPartialTextBox.Location = new System.Drawing.Point(73, 19);
            this.WebAppPathPartialTextBox.Name = "WebAppPathPartialTextBox";
            this.WebAppPathPartialTextBox.Size = new System.Drawing.Size(475, 20);
            this.WebAppPathPartialTextBox.TabIndex = 1;
            this.WebAppPathPartialTextBox.TextChanged += new System.EventHandler(this.WebAppPathPartialTextBox_TextChanged);
            // 
            // WebAppPathFilterLabel
            // 
            this.WebAppPathFilterLabel.AutoSize = true;
            this.WebAppPathFilterLabel.Location = new System.Drawing.Point(7, 22);
            this.WebAppPathFilterLabel.Name = "WebAppPathFilterLabel";
            this.WebAppPathFilterLabel.Size = new System.Drawing.Size(60, 13);
            this.WebAppPathFilterLabel.TabIndex = 0;
            this.WebAppPathFilterLabel.Text = "Path (filter):";
            // 
            // SearchButton
            // 
            this.SearchButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchButton.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.ZoomHS;
            this.SearchButton.Location = new System.Drawing.Point(554, 17);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(32, 23);
            this.SearchButton.TabIndex = 2;
            this.SearchButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // WebAppDataGridView
            // 
            this.WebAppDataGridView.AllowUserToAddRows = false;
            this.WebAppDataGridView.AllowUserToDeleteRows = false;
            this.WebAppDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WebAppDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.WebAppDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.WebAppDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.WebAppDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.WebAppDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WebAppSelectColumn,
            this.WebAppExistsInIISColumn,
            this.WebAppPathColumn,
            this.StatusColumn,
            this.WebAppPhysicalPathColumn});
            this.WebAppDataGridView.ContextMenuStrip = this.SelectContextMenuStrip;
            this.WebAppDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.WebAppDataGridView.Location = new System.Drawing.Point(6, 74);
            this.WebAppDataGridView.MultiSelect = false;
            this.WebAppDataGridView.Name = "WebAppDataGridView";
            this.WebAppDataGridView.RowHeadersVisible = false;
            this.WebAppDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.WebAppDataGridView.Size = new System.Drawing.Size(656, 231);
            this.WebAppDataGridView.TabIndex = 7;
            this.WebAppDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.WebAppDataGridView_CellFormatting);
            // 
            // ApplicationsSiteLevelGroupBox
            // 
            this.ApplicationsSiteLevelGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplicationsSiteLevelGroupBox.Controls.Add(this.SiteLevelWebAppDataGridView);
            this.ApplicationsSiteLevelGroupBox.Location = new System.Drawing.Point(6, 33);
            this.ApplicationsSiteLevelGroupBox.Name = "ApplicationsSiteLevelGroupBox";
            this.ApplicationsSiteLevelGroupBox.Size = new System.Drawing.Size(667, 133);
            this.ApplicationsSiteLevelGroupBox.TabIndex = 5;
            this.ApplicationsSiteLevelGroupBox.TabStop = false;
            this.ApplicationsSiteLevelGroupBox.Text = "Site level applications";
            this.ApplicationsSiteLevelGroupBox.Visible = false;
            // 
            // SiteLevelWebAppDataGridView
            // 
            this.SiteLevelWebAppDataGridView.AllowUserToAddRows = false;
            this.SiteLevelWebAppDataGridView.AllowUserToDeleteRows = false;
            this.SiteLevelWebAppDataGridView.AllowUserToResizeRows = false;
            this.SiteLevelWebAppDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SiteLevelWebAppDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.SiteLevelWebAppDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SiteLevelWebAppDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.SiteLevelWebAppDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SiteLevelWebAppDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SiteLevelWebAppSelectColumn,
            this.SiteLevelWebAppPathColumn,
            this.SiteLevelWebAppPhysicalPathColumn});
            this.SiteLevelWebAppDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.SiteLevelWebAppDataGridView.Location = new System.Drawing.Point(5, 19);
            this.SiteLevelWebAppDataGridView.MultiSelect = false;
            this.SiteLevelWebAppDataGridView.Name = "SiteLevelWebAppDataGridView";
            this.SiteLevelWebAppDataGridView.RowHeadersVisible = false;
            this.SiteLevelWebAppDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SiteLevelWebAppDataGridView.ShowEditingIcon = false;
            this.SiteLevelWebAppDataGridView.Size = new System.Drawing.Size(656, 101);
            this.SiteLevelWebAppDataGridView.TabIndex = 0;
            this.SiteLevelWebAppDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SiteLevelWebAppDataGridView_CellClick);
            // 
            // SiteLevelWebAppSelectColumn
            // 
            this.SiteLevelWebAppSelectColumn.HeaderText = "";
            this.SiteLevelWebAppSelectColumn.MinimumWidth = 20;
            this.SiteLevelWebAppSelectColumn.Name = "SiteLevelWebAppSelectColumn";
            this.SiteLevelWebAppSelectColumn.Width = 20;
            // 
            // SiteLevelWebAppPathColumn
            // 
            this.SiteLevelWebAppPathColumn.HeaderText = "Path";
            this.SiteLevelWebAppPathColumn.MinimumWidth = 100;
            this.SiteLevelWebAppPathColumn.Name = "SiteLevelWebAppPathColumn";
            this.SiteLevelWebAppPathColumn.ReadOnly = true;
            this.SiteLevelWebAppPathColumn.Width = 250;
            // 
            // SiteLevelWebAppPhysicalPathColumn
            // 
            this.SiteLevelWebAppPhysicalPathColumn.HeaderText = "Physical Path";
            this.SiteLevelWebAppPhysicalPathColumn.Name = "SiteLevelWebAppPhysicalPathColumn";
            this.SiteLevelWebAppPhysicalPathColumn.ReadOnly = true;
            this.SiteLevelWebAppPhysicalPathColumn.Width = 484;
            // 
            // ShowTeamProjectPickerToolTip
            // 
            this.ShowTeamProjectPickerToolTip.ToolTipTitle = "Pick team project";
            // 
            // DisplayWebAppConfigFileToolTip
            // 
            this.DisplayWebAppConfigFileToolTip.ToolTipTitle = "Web Application Configuration";
            // 
            // WebAppConfigContextMenuStrip
            // 
            this.WebAppConfigContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenToolStripMenuItem,
            this.OpenContainingFolderToolStripMenuItem});
            this.WebAppConfigContextMenuStrip.Name = "WebAppConfigContextMenuStrip";
            this.WebAppConfigContextMenuStrip.Size = new System.Drawing.Size(202, 48);
            // 
            // OpenToolStripMenuItem
            // 
            this.OpenToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem";
            this.OpenToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.OpenToolStripMenuItem.Text = "Open";
            this.OpenToolStripMenuItem.Click += new System.EventHandler(this.OpenToolStripMenuItem_Click);
            // 
            // OpenContainingFolderToolStripMenuItem
            // 
            this.OpenContainingFolderToolStripMenuItem.Name = "OpenContainingFolderToolStripMenuItem";
            this.OpenContainingFolderToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.OpenContainingFolderToolStripMenuItem.Text = "Open Containing Folder";
            this.OpenContainingFolderToolStripMenuItem.Click += new System.EventHandler(this.OpenContainingFolderToolStripMenuItem_Click);
            // 
            // WebAppSelectColumn
            // 
            this.WebAppSelectColumn.HeaderText = "";
            this.WebAppSelectColumn.MinimumWidth = 20;
            this.WebAppSelectColumn.Name = "WebAppSelectColumn";
            this.WebAppSelectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.WebAppSelectColumn.Width = 20;
            // 
            // WebAppExistsInIISColumn
            // 
            this.WebAppExistsInIISColumn.HeaderText = "";
            this.WebAppExistsInIISColumn.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.ExistsInIISTrue;
            this.WebAppExistsInIISColumn.MinimumWidth = 22;
            this.WebAppExistsInIISColumn.Name = "WebAppExistsInIISColumn";
            this.WebAppExistsInIISColumn.ReadOnly = true;
            this.WebAppExistsInIISColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.WebAppExistsInIISColumn.Width = 22;
            // 
            // WebAppPathColumn
            // 
            this.WebAppPathColumn.HeaderText = "Path";
            this.WebAppPathColumn.MinimumWidth = 100;
            this.WebAppPathColumn.Name = "WebAppPathColumn";
            this.WebAppPathColumn.ReadOnly = true;
            this.WebAppPathColumn.Width = 250;
            // 
            // StatusColumn
            // 
            this.StatusColumn.HeaderText = "Status";
            this.StatusColumn.MinimumWidth = 100;
            this.StatusColumn.Name = "StatusColumn";
            this.StatusColumn.ReadOnly = true;
            // 
            // WebAppPhysicalPathColumn
            // 
            this.WebAppPhysicalPathColumn.HeaderText = "Physical Path";
            this.WebAppPhysicalPathColumn.MinimumWidth = 100;
            this.WebAppPhysicalPathColumn.Name = "WebAppPhysicalPathColumn";
            this.WebAppPhysicalPathColumn.ReadOnly = true;
            this.WebAppPhysicalPathColumn.Width = 484;
            // 
            // DisplayWebAppConfigFileButton
            // 
            this.DisplayWebAppConfigFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DisplayWebAppConfigFileButton.AutoSize = true;
            this.DisplayWebAppConfigFileButton.ContextMenuStrip = this.WebAppConfigContextMenuStrip;
            this.DisplayWebAppConfigFileButton.Location = new System.Drawing.Point(554, 9);
            this.DisplayWebAppConfigFileButton.Name = "DisplayWebAppConfigFileButton";
            this.DisplayWebAppConfigFileButton.Size = new System.Drawing.Size(119, 25);
            this.DisplayWebAppConfigFileButton.TabIndex = 4;
            this.DisplayWebAppConfigFileButton.Text = "Web App Config";
            this.DisplayWebAppConfigFileButton.UseVisualStyleBackColor = true;
            // 
            // SelectSplitButton
            // 
            this.SelectSplitButton.AutoSize = true;
            this.SelectSplitButton.ContextMenuStrip = this.WebAppSelectContextMenuStrip;
            this.SelectSplitButton.Location = new System.Drawing.Point(7, 45);
            this.SelectSplitButton.Name = "SelectSplitButton";
            this.SelectSplitButton.Size = new System.Drawing.Size(75, 23);
            this.SelectSplitButton.TabIndex = 5;
            this.SelectSplitButton.Text = "Select";
            this.SelectSplitButton.UseVisualStyleBackColor = true;
            // 
            // DisplaySplitButton
            // 
            this.DisplaySplitButton.AutoSize = true;
            this.DisplaySplitButton.ContextMenuStrip = this.WebAppDisplayContextMenuStrip;
            this.DisplaySplitButton.Location = new System.Drawing.Point(88, 45);
            this.DisplaySplitButton.Name = "DisplaySplitButton";
            this.DisplaySplitButton.Size = new System.Drawing.Size(75, 23);
            this.DisplaySplitButton.TabIndex = 6;
            this.DisplaySplitButton.Text = "Display";
            this.DisplaySplitButton.UseVisualStyleBackColor = true;
            // 
            // WebAppSearchForm
            // 
            this.AcceptButton = this.SearchButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 495);
            this.ControlBox = false;
            this.Controls.Add(this.DisplayWebAppConfigFileButton);
            this.Controls.Add(this.ApplicationsSiteLevelGroupBox);
            this.Controls.Add(this.ApplicationsGroupBox);
            this.Controls.Add(this.BranchComboBox);
            this.Controls.Add(this.BranchLabel);
            this.Controls.Add(this.SiteNameValueLabel);
            this.Controls.Add(this.SiteNameLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WebAppSearchForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "WebAppSearchForm";
            this.WebAppDisplayContextMenuStrip.ResumeLayout(false);
            this.WebAppSelectContextMenuStrip.ResumeLayout(false);
            this.SelectContextMenuStrip.ResumeLayout(false);
            this.ApplicationsGroupBox.ResumeLayout(false);
            this.ApplicationsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WebAppDataGridView)).EndInit();
            this.ApplicationsSiteLevelGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SiteLevelWebAppDataGridView)).EndInit();
            this.WebAppConfigContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip WebAppDisplayContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem DisplayAllToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem DisplaySelectedToolStripItem;
        private System.Windows.Forms.ContextMenuStrip WebAppSelectContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem SelectAllToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem SelectNoneToolStripItem;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.ContextMenuStrip SelectContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem AllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem NoneToolStripMenuItem;
        private System.Windows.Forms.Label BranchLabel;
        private System.Windows.Forms.ComboBox BranchComboBox;
        private System.Windows.Forms.Label SiteNameLabel;
        private System.Windows.Forms.Label SiteNameValueLabel;
        private System.Windows.Forms.GroupBox ApplicationsGroupBox;
        private System.Windows.Forms.DataGridView WebAppDataGridView;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.TextBox WebAppPathPartialTextBox;
        private System.Windows.Forms.Label WebAppPathFilterLabel;
        private System.Windows.Forms.Button SearchButton;
        private SplitButton SelectSplitButton;
        private SplitButton DisplaySplitButton;
        private System.Windows.Forms.GroupBox ApplicationsSiteLevelGroupBox;
        private System.Windows.Forms.DataGridView SiteLevelWebAppDataGridView;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SiteLevelWebAppSelectColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SiteLevelWebAppPathColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SiteLevelWebAppPhysicalPathColumn;
        private System.Windows.Forms.ToolTip ShowTeamProjectPickerToolTip;
        private System.Windows.Forms.ToolTip DisplayWebAppConfigFileToolTip;
        private System.Windows.Forms.ContextMenuStrip WebAppConfigContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem OpenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OpenContainingFolderToolStripMenuItem;
        private SplitButton DisplayWebAppConfigFileButton;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.DataGridViewCheckBoxColumn WebAppSelectColumn;
        private System.Windows.Forms.DataGridViewImageColumn WebAppExistsInIISColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn WebAppPathColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn WebAppPhysicalPathColumn;
    }
}