﻿using Aclara.Build.Engine.Client;
using Aclara.VsoLocalDev.Sidekicks.WinForm;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Events;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Exceptions;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Solution list search form.
    /// </summary>
    public partial class SolutionListSearchForm : Form, ISolutionListSearchView
    {

        #region Private Constants

        private const string SolutionListSearchFormTitle = "Solution List Search";
        private const string OpenSolution_MoreThanSafeSolutionCountSelected = "{0} solutions have be selected. Click Yes to continue, click No to open first {1} selected solutions.";
        private const string OpenContainingFolder_MoreThanSafeSolutionCountSelected = "{0} solutions have be selected. Click Yes to continue, click No to open containing folder of first {1} selected solutions.";

        private const string BranchComboBoxPreset_None = "<Select Branch>";
        private const int SolutionCountSafeToOpen = 2;

        #endregion

        #region Private Data Members

        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private SidekickConfiguration _sidekickConfiguration;
        private SolutionListSearchPresenter _solutionListSearchPresenter = null;
        private SolutionSelectorLists _solutionSelectorLists;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public Logger Logger
        {
            get
            {
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Solution list search presenter.
        /// </summary>
        public SolutionListSearchPresenter SolutionListSearchPresenter
        {
            get { return _solutionListSearchPresenter; }
            set { _solutionListSearchPresenter = value; }
        }

        /// <summary>
        /// Property: Branch name.
        /// </summary>
        public string BranchName
        {
            get
            {
                string branchName = string.Empty;
                if (this.BranchComboBox.Text == null)
                {
                    branchName = string.Empty;
                }
                else
                {
                    branchName = this.BranchComboBox.Text;
                }
                return branchName;
            }
            set { this.BranchComboBox.SelectedItem = value; }
        }

        /// <summary>
        /// Property: Solution selector lists.
        /// </summary>
        public SolutionSelectorLists SolutionSelectorLists
        {
            get
            {

                _solutionSelectorLists = this.SolutionListSearchPresenter.GetSolutionSelectorLists(this.SolutionListSearchPresenter.TeamProjectInfo.TeamProjectCollectionUri.ToString(),
                                                                                                   this.SolutionListSearchPresenter.TeamProjectInfo.TeamProjectName,
                                                                                                   this.SolutionListSearchPresenter.TeamProjectInfo.WorkspaceName,
                                                                                                   this.SolutionListSearchPresenter.TeamProjectInfo.BasicAuthRestAPIUserProfileName,
                                                                                                   this.BranchName);

                return _solutionSelectorLists;
            }
            set
            {
                _solutionSelectorLists = value;
            }
        }

        #endregion

        #region Public Delegates

        public event EventHandler<SolutionSelectorSelectedEventArgs> SolutionSelectorSelected;

        #endregion


        #region Public Constructors


        /// <summary>
        /// Overridden constructor.
        /// </summary>
        public SolutionListSearchForm(SidekickConfiguration sidekickConfiguration)
        {
            this.SidekickConfiguration = sidekickConfiguration;

            InitializeComponent();

            //Disable controls dependant on team project.
            EnableControlsDependantOnTeamProject(false);
            //Disable controls dependant on branch.
            EnableControlsDependantOnBranch(false);

        }

        #endregion

        #region Protected Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        protected SolutionListSearchForm()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {

            try
            {

                this.SolutionListSearchPresenter.TeamProjectCollectionNameChanged();
                this.SolutionListSearchPresenter.TeamProjectNameChanged();

                this.SolutionListSearchPresenter.PopulateBrachNameList(this.SolutionListSearchPresenter.TeamProjectInfo.TeamProjectCollectionUri.ToString(),
                                                                       this.SolutionListSearchPresenter.TeamProjectInfo.TeamProjectName);

            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Populate branch name list.
        /// </summary>
        /// <param name="branchNameList"></param>
        public void PopulateBranchNameList(List<string> branchNameList)
        {
            int index = 0;
            try
            {
                this.BranchComboBox.Items.Clear();
                this.BranchComboBox.Items.Insert(0, BranchComboBoxPreset_None);
                foreach (string branchName in branchNameList)
                {
                    this.BranchComboBox.Items.Insert(index++, branchName);
                }
                this.BranchComboBox.SelectedItem = BranchComboBoxPreset_None;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable controls dependant on team project.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnTeamProject(bool enabled)
        {
            try
            {
                this.BranchComboBox.Enabled = enabled;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable controls dependant on branch.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnBranch(bool enabled)
        {
            try
            {
                this.SelectSplitButton.Enabled = enabled;
                this.ToggleExpandSplitButton.Enabled = enabled;
                this.DisplaySolutionListConfigFileSplitButton.Enabled = enabled;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable controls dependant solution node(s) checked.
        /// </summary>
        public void EnableControlsDependantOnSolutionNodeChecked()
        {
            SolutionSelector solutionSelector = null;
            string solutionFullPath = string.Empty;
            SolutionSelectorList solutionSelectorList = null;

            try
            {

                solutionSelectorList = new SolutionSelectorList();

                foreach (TreeNode parentTreeNode in this.SolutionListTreeView.Nodes)
                {
                    if (parentTreeNode.Nodes.Count > 0)
                    {
                        foreach (TreeNode treeNode in parentTreeNode.Nodes)
                        {
                            if (treeNode.Checked == true &&
                                treeNode.Tag != null &&
                                treeNode.Tag.GetType() == typeof(SolutionSelector))
                            {
                                solutionSelector = (SolutionSelector)treeNode.Tag;
                                solutionSelectorList.Add(solutionSelector);
                            }
                        }
                    }
                }

                if (solutionSelectorList.Count > 0)
                {
                    this.OpenSplitButton.Enabled = true;
                }
                else
                {
                    this.OpenSplitButton.Enabled = false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Clear branch dependent controls.
        /// </summary>
        public void ClearBranchDependentControls()
        {
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve selected soltion selector list.
        /// </summary>
        /// <returns></returns>
        public SolutionSelectorList GetSelectedSolutionSelectorList()
        {
            SolutionSelectorList result = null;
            SolutionSelector solutionSelector = null;

            try
            {
                result = new SolutionSelectorList();

                foreach (TreeNode parentTreeNode in this.SolutionListTreeView.Nodes)
                {
                    if (parentTreeNode.Nodes.Count > 0)
                    {
                        foreach (TreeNode treeNode in parentTreeNode.Nodes)
                        {
                            if (treeNode.Checked == true &&
                                treeNode.Tag != null &&
                                treeNode.Tag.GetType() == typeof(SolutionSelector))
                            {
                                solutionSelector = (SolutionSelector)treeNode.Tag;
                                result.Add(solutionSelector);
                            }
                        }
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Update solution build status.
        /// </summary>
        /// <param name="solutionSelector"></param>
        /// <param name="hightlight"></param>
        public void UpdateSolutionBuildStatus(SolutionSelector solutionSelector)
        {
            SolutionSelector currentSolutionSelector = null;

            try
            {

                foreach (TreeNode parentTreeNode in this.SolutionListTreeView.Nodes)
                {
                    if (parentTreeNode.Nodes.Count > 0)
                    {
                        foreach (TreeNode treeNode in parentTreeNode.Nodes)
                        {

                            if (treeNode.Tag.GetType() == typeof(SolutionSelector))
                            {
                                currentSolutionSelector = (SolutionSelector)treeNode.Tag;
                            }

                            if (parentTreeNode.Text == solutionSelector.SolutionListName &&
                                currentSolutionSelector.SolutionName == solutionSelector.SolutionName)
                            {
                                //Update solution selector.
                                currentSolutionSelector.BuildStatus = solutionSelector.BuildStatus;
                                currentSolutionSelector.BuildOutput = solutionSelector.BuildOutput;
                                currentSolutionSelector.BuildWarningList = solutionSelector.BuildWarningList;
                                currentSolutionSelector.BuildErrorList = solutionSelector.BuildErrorList;

                                //Update tree node text with current build status.
                                switch (currentSolutionSelector.BuildStatus)
                                {

                                    case BuildStatus.Started:
                                        treeNode.Text = string.Format("{0} - [Building...]",
                                                                      currentSolutionSelector.SolutionName);
                                        treeNode.BackColor = Color.LightBlue;
                                        treeNode.ForeColor = Color.Black;
                                        break;

                                    case BuildStatus.Failed:
                                        treeNode.Text = string.Format("{0} - [Failed.]",
                                                                      currentSolutionSelector.SolutionName);
                                        treeNode.BackColor = Color.Red;
                                        treeNode.ForeColor = Color.White;
                                        break;

                                    case BuildStatus.Succeeded:
                                        if (currentSolutionSelector.BuildWarningList.Count > 0)
                                        {
                                            treeNode.Text = string.Format("{0} - [Succeeded with warnings.]",
                                                                          currentSolutionSelector.SolutionName);
                                            treeNode.BackColor = Color.LightGoldenrodYellow;
                                            treeNode.ForeColor = Color.Black;
                                        }
                                        else
                                        {
                                            treeNode.Text = string.Format("{0} - [Succeeded.]",
                                                                          currentSolutionSelector.SolutionName);
                                            treeNode.BackColor = Color.LightGreen;
                                            treeNode.ForeColor = Color.Black;
                                        }
                                        break;

                                    default:
                                        treeNode.Text = string.Format("{0}",
                                                                      currentSolutionSelector.SolutionName);
                                        treeNode.BackColor = Color.White;
                                        treeNode.ForeColor = Color.Black;
                                        break;
                                }

                            }
                        }
                    }
                }

                this.SolutionListTreeView.SelectedNode = null;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Reset all solution build status.
        /// </summary>
        public void ResetAllSolutionBuildStatus()
        {
            Font regularFont = null;

            try
            {

                foreach (TreeNode parentTreeNode in this.SolutionListTreeView.Nodes)
                {
                    if (parentTreeNode.Nodes.Count > 0)
                    {
                        foreach (TreeNode treeNode in parentTreeNode.Nodes)
                        {
                            regularFont = new System.Drawing.Font(this.SolutionListTreeView.Font, FontStyle.Regular);
                            treeNode.NodeFont = regularFont;
                            treeNode.Text = treeNode.Text;

                            treeNode.BackColor = Color.White;
                            treeNode.ForeColor = Color.Black;

                            //TODO: Reset build status.
                        }
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize solution list tree view.
        /// </summary>
        protected void InitializeSolutionListTreeView()
        {
            TreeNode parentNode = null;
            TreeNode childNode = null;
            try
            {
                this.SolutionListTreeView.Nodes.Clear();
                this.SolutionListTreeView.Controls.Clear();

                foreach (SolutionSelectorList solutionSelectorList in this.SolutionSelectorLists)
                {

                    parentNode = this.SolutionListTreeView.Nodes.Add(solutionSelectorList.Name);

                    foreach (SolutionSelector solutionSelector in solutionSelectorList)
                    {
                        childNode = new TreeNode();

                        childNode.Name = solutionSelector.SolutionName;
                        childNode.Text = solutionSelector.SolutionName;
                        childNode.Tag = solutionSelector;

                        parentNode.Nodes.Add(childNode);
                    }
                }

                this.ExpandAllTreeNodes();

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Expand all tree nodes.
        /// </summary>
        protected void ExpandAllTreeNodes()
        {
            try
            {
                foreach (TreeNode treeNode in this.SolutionListTreeView.Nodes)
                {
                    treeNode.Expand();
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Collapse all tree nodes.
        /// </summary>
        protected void CollapseAllTreeNodes()
        {
            try
            {
                foreach (TreeNode treeNode in this.SolutionListTreeView.Nodes)
                {
                    treeNode.Collapse();
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Open solution list with Visual Studio.
        /// </summary>
        /// <param name="solutionSelectorList"></param>
        protected void OpenSolutionListWithVisualStudio(SolutionSelectorList solutionSelectorList)
        {

            string solutionFullPath = string.Empty;

            try
            {
                if (solutionSelectorList.Count > 0)
                {

                    foreach (SolutionSelector solutionSelector1 in solutionSelectorList)
                    {
                        solutionFullPath = Path.Combine(solutionSelector1.SolutionPath, solutionSelector1.SolutionName);
                        System.Diagnostics.Process.Start(solutionFullPath);
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Open solution list containing folder.
        /// </summary>
        /// <param name="solutionSelectorList"></param>
        protected void OpenSolutionListContainingFolder(SolutionSelectorList solutionSelectorList)
        {

            try
            {
                if (solutionSelectorList.Count > 0)
                {

                    foreach (SolutionSelector solutionSelector1 in solutionSelectorList)
                    {
                        System.Diagnostics.Process.Start(solutionSelector1.SolutionPath);
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods


        /// <summary>
        /// Event Handler: Branch combo box - Text changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BranchComboBox_TextChanged(object sender, EventArgs e)
        {
            ComboBox senderComboBox = null;

            try
            {
                if (sender is ComboBox != true)
                {
                    return;
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                this.Cursor = Cursors.WaitCursor;

                senderComboBox = (ComboBox)sender;
                if (senderComboBox.Text == BranchComboBoxPreset_None)
                {

                    EnableControlsDependantOnBranch(false);
                    this.ClearBranchDependentControls();
                    return;
                }

                this.InitializeSolutionListTreeView();

                EnableControlsDependantOnBranch(true);

                this.Cursor = Cursors.Default;

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
                this.ClearBranchDependentControls();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Solution list tree view - After check.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SolutionListTreeView_AfterCheck(object sender, TreeViewEventArgs e)
        {

            try
            {
                if (e.Action != TreeViewAction.Unknown)
                {
                    if (e.Node.Nodes.Count > 0)
                    {
                        foreach (TreeNode treeNode in e.Node.Nodes)
                        {
                            treeNode.Checked = e.Node.Checked;
                        }
                    }
                }
                this.EnableControlsDependantOnSolutionNodeChecked();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Select none tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SolutionSelectAllToolStripItem_Click(object sender, EventArgs e)
        {
            try
            {

                foreach (TreeNode parentTreeNode in this.SolutionListTreeView.Nodes)
                {
                    parentTreeNode.Checked = true;

                    foreach (TreeNode treeNode in parentTreeNode.Nodes)
                    {
                        treeNode.Checked = true;
                    }
                }
                this.EnableControlsDependantOnSolutionNodeChecked();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Select all tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SolutionSelectNoneToolStripItem_Click(object sender, EventArgs e)
        {
            try
            {

                foreach (TreeNode parentTreeNode in this.SolutionListTreeView.Nodes)
                {
                    parentTreeNode.Checked = false;

                    foreach (TreeNode treeNode in parentTreeNode.Nodes)
                    {
                        treeNode.Checked = false;
                    }
                }
                this.EnableControlsDependantOnSolutionNodeChecked();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Open open tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string solutionListConfigFilePath = string.Empty;

            try
            {
                solutionListConfigFilePath = this.SolutionListSearchPresenter.GetSolutionListConfigFilePath(this.SolutionListSearchPresenter.TeamProjectInfo.TeamProjectName,
                                                                                                            this.BranchName);

                System.Diagnostics.Process.Start(solutionListConfigFilePath);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Open containing folder tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenContainingFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string webAppConfigFilePath = string.Empty;
            string webAppConfigPath = string.Empty;

            try
            {
                webAppConfigFilePath = this.SolutionListSearchPresenter.GetSolutionListConfigFilePath(this.SolutionListSearchPresenter.TeamProjectInfo.TeamProjectName,
                                                                                                      this.BranchName);
                webAppConfigPath = Path.GetDirectoryName(webAppConfigFilePath);
                System.Diagnostics.Process.Start(webAppConfigPath);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Expand all tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExpandAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.ExpandAllTreeNodes();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Collapse all tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CollapseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.CollapseAllTreeNodes();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Solution open visual studio menu strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SolutionOpenVisualStudioMenuStrip_Click(object sender, EventArgs e)
        {
            string changeLogFilePath = string.Empty;
            string executablePath = string.Empty;
            SolutionSelector solutionSelector = null;
            string solutionFullPath = string.Empty;
            SolutionSelectorList solutionSelectorList = null;
            SolutionSelectorList subsetSolutionSelectorList = null;
            DialogResult dialogResult = DialogResult.Cancel;

            try
            {
                solutionSelectorList = new SolutionSelectorList();

                foreach (TreeNode parentTreeNode in this.SolutionListTreeView.Nodes)
                {
                    if (parentTreeNode.Nodes.Count > 0)
                    {
                        foreach (TreeNode treeNode in parentTreeNode.Nodes)
                        {
                            if (treeNode.Checked == true &&
                                treeNode.Tag != null &&
                                treeNode.Tag.GetType() == typeof(SolutionSelector))
                            {
                                solutionSelector = (SolutionSelector)treeNode.Tag;
                                solutionSelectorList.Add(solutionSelector);
                            }
                        }
                    }
                }

                if (solutionSelectorList.Count > 0)
                {

                    //Selected solution count exceeds safe number to open.
                    if (solutionSelectorList.Count > SolutionCountSafeToOpen)
                    {
                        dialogResult = MessageBox.Show(string.Format(OpenSolution_MoreThanSafeSolutionCountSelected,
                                                                     solutionSelectorList.Count,
                                                                     SolutionCountSafeToOpen),
                                            SolutionListSearchFormTitle,
                                            MessageBoxButtons.YesNoCancel,
                                            MessageBoxIcon.Question);

                        //User chose to open entire solution list.
                        if (dialogResult == DialogResult.Yes)
                        {
                            OpenSolutionListWithVisualStudio(solutionSelectorList);
                        }
                        //User chose to open safe solution count.
                        if (dialogResult == DialogResult.No)
                        {
                            subsetSolutionSelectorList = new SolutionSelectorList();
                            subsetSolutionSelectorList.AddRange(solutionSelectorList.Take(SolutionCountSafeToOpen));
                            OpenSolutionListWithVisualStudio(subsetSolutionSelectorList);
                        }
                    }
                    else
                    {
                        OpenSolutionListWithVisualStudio(solutionSelectorList);
                    }

                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Solution open containing folder menu strip - Clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SolutionOpenContainingFolderMenuStrip_Click(object sender, EventArgs e)
        {
            string changeLogFilePath = string.Empty;
            string executablePath = string.Empty;
            SolutionSelector solutionSelector = null;
            string solutionFullPath = string.Empty;
            SolutionSelectorList solutionSelectorList = null;
            SolutionSelectorList subsetSolutionSelectorList = null;
            DialogResult dialogResult = DialogResult.Cancel;

            try
            {
                solutionSelectorList = new SolutionSelectorList();

                foreach (TreeNode parentTreeNode in this.SolutionListTreeView.Nodes)
                {
                    if (parentTreeNode.Nodes.Count > 0)
                    {
                        foreach (TreeNode treeNode in parentTreeNode.Nodes)
                        {
                            if (treeNode.Checked == true &&
                                treeNode.Tag != null &&
                                treeNode.Tag.GetType() == typeof(SolutionSelector))
                            {
                                solutionSelector = (SolutionSelector)treeNode.Tag;
                                solutionSelectorList.Add(solutionSelector);
                            }
                        }
                    }
                }

                if (solutionSelectorList.Count > 0)
                {

                    //Selected solution count exceeds safe number to open.
                    if (solutionSelectorList.Count > SolutionCountSafeToOpen)
                    {
                        dialogResult = MessageBox.Show(string.Format(OpenContainingFolder_MoreThanSafeSolutionCountSelected,
                                                                     solutionSelectorList.Count,
                                                                     SolutionCountSafeToOpen),
                                            SolutionListSearchFormTitle,
                                            MessageBoxButtons.YesNoCancel,
                                            MessageBoxIcon.Question);

                        //User chose to open entire solution list.
                        if (dialogResult == DialogResult.Yes)
                        {
                            OpenSolutionListContainingFolder(solutionSelectorList);
                        }
                        //User chose to open safe solution count.
                        if (dialogResult == DialogResult.No)
                        {
                            subsetSolutionSelectorList = new SolutionSelectorList();
                            subsetSolutionSelectorList.AddRange(solutionSelectorList.Take(SolutionCountSafeToOpen));
                            OpenSolutionListContainingFolder(subsetSolutionSelectorList);
                        }
                    }
                    else
                    {
                        OpenSolutionListContainingFolder(solutionSelectorList);
                    }


                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Solution open visual studio tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SolutionOpenWithVisualStudioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode treeNode = null;
            SolutionSelector solutionSelector = null;
            SolutionSelectorList solutionSelectorList = null;

            try
            {
                solutionSelectorList = new SolutionSelectorList();

                treeNode = SolutionListTreeView.SelectedNode;
                if (treeNode != null &&
                    treeNode.Tag != null &&
                    treeNode.Tag.GetType() == typeof(SolutionSelector))
                {
                    solutionSelector = (SolutionSelector)treeNode.Tag;
                    solutionSelectorList.Add(solutionSelector);
                    this.OpenSolutionListWithVisualStudio(solutionSelectorList);
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Solution open containing folder tool strip menu item - Clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SolutionOpenContainingFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode treeNode = null;
            SolutionSelector solutionSelector = null;
            SolutionSelectorList solutionSelectorList = null;

            try
            {
                solutionSelectorList = new SolutionSelectorList();

                treeNode = SolutionListTreeView.SelectedNode;
                if (treeNode != null &&
                    treeNode.Tag != null &&
                    treeNode.Tag.GetType() == typeof(SolutionSelector))
                {
                    solutionSelector = (SolutionSelector)treeNode.Tag;
                    solutionSelectorList.Add(solutionSelector);
                    this.OpenSolutionListContainingFolder(solutionSelectorList);
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Solution list tree view  - Node mouse click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SolutionListTreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode treeNode = null;

            try
            {
                treeNode = e.Node;
                this.SolutionListTreeView.SelectedNode = treeNode;

                if (e.Button == MouseButtons.Right)
                {

                    if (treeNode != null &&
                        treeNode.Tag != null &&
                        treeNode.Tag.GetType() == typeof(SolutionSelector))
                    {
                        this.SolutionContextMenuStrip.Show(this.SolutionListTreeView, e.X, e.Y);
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: SolutionlistTreeView - After select.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SolutionListTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            SolutionSelector solutionSelector = null;
            TreeNode treeNode = null;
            ISolutionSelectorSelectedResult solutionSelectorSelectedResult = null;
            SolutionSelectorSelectedEventArgs solutionSelectorSelectedEventArgs = null;

            solutionSelectorSelectedEventArgs = new SolutionSelectorSelectedEventArgs();
            solutionSelectorSelectedResult = new SolutionSelectorSelectedResult();

            treeNode = e.Node;

            if (treeNode != null &&
                treeNode.Tag != null &&
                treeNode.Tag.GetType() == typeof(SolutionSelector))
            {
                solutionSelector = (SolutionSelector)treeNode.Tag;
                solutionSelectorSelectedEventArgs.SolutionSelectorSelectedResult = solutionSelectorSelectedResult;
                solutionSelectorSelectedResult.SolutionSelector = solutionSelector;

                if (this.SolutionSelectorSelected != null)
                {
                    this.SolutionSelectorSelected(this, solutionSelectorSelectedEventArgs);
                }
            }

        }

        #endregion


    }
}
