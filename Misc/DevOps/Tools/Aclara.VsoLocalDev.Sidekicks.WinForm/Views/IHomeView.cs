﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    public interface IHomeView
    {
        string SidekickName { get; }

        string SidekickDescription { get; }

    }
}
