﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// NuGet package search form factory.
    /// </summary>
    public class NuGetPackageSearchFormFactory
    {

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public NuGetPackageSearchFormFactory()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create NuGet package search form.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <returns></returns>
        static public NuGetPackageSearchForm CreateForm(ITeamProjectInfo teamProjectInfo, SidekickConfiguration sidekickConfiguration)
        {

            NuGetPackageSearchForm result = null;
            NuGetPackageSearchPresenter webAppSearchPresenter = null;

            try
            {

                result = new NuGetPackageSearchForm(sidekickConfiguration);
                webAppSearchPresenter = new NuGetPackageSearchPresenter(teamProjectInfo, sidekickConfiguration, result);

                result.NuGetPackageSearchPresenter = webAppSearchPresenter;
                result.TopLevel = false;

                webAppSearchPresenter.Initialize();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion
    }

}
