﻿using Aclara.AzureRedisCache.Client;
using Aclara.AzureRedisCache.Client.Events;
using Aclara.Tools.Configuration.AzureRedisCacheConfig;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using static Aclara.AzureRedisCache.Client.Types.Enumerations;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Build solution list presenter.
    /// </summary>
    public class AzureRedisCacheAdminPresenter
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private SidekickConfiguration _sidekickConfiguration;
        private IAzureRedisCacheAdminView _azureRedisCacheAdminView;
        private AzureRedisCacheManager _azureRedisCacheManager = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public Logger Logger
        {
            get
            {
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Build solution list view.
        /// </summary>
        public IAzureRedisCacheAdminView AzureRedisCacheAdminView
        {
            get { return _azureRedisCacheAdminView; }
            set { _azureRedisCacheAdminView = value; }
        }


        /// <summary>
        /// Property: Azure Redis cache manager.
        /// </summary>
        public AzureRedisCacheManager AzureRedisCacheManager
        {
            get { return _azureRedisCacheManager; }
            set { _azureRedisCacheManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildSolutionListView"></param>
        public AzureRedisCacheAdminPresenter(ITeamProjectInfo teamProjectInfo,
                                             SidekickConfiguration sidekickConfiguration,
                                             IAzureRedisCacheAdminView buildSolutionListView)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.AzureRedisCacheAdminView = buildSolutionListView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private AzureRedisCacheAdminPresenter()
        {

        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalBuildDuration;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalBuildDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.AzureRedisCacheAdminView.BackgroundTaskCompleted("Azure Redis cache administration operation canceled.");
                        this.AzureRedisCacheAdminView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format("Azure Redis cache administration operation cancelled."));


                        //Log background task completed log entry.

                        this.Logger.Info(string.Format("[*] Azure Redis cache administration operation completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                       totalBuildDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.AzureRedisCacheAdminView.BackgroundTaskCompleted("");
                        this.AzureRedisCacheAdminView.ApplyButtonEnable(true);


                        //Log background task completed log entry.

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    //Handle operation canceled exception.
                                    this.Logger.Error(innerException, string.Format("Azure Redis cache administration operation cancelled."));
                                }

                            }
                        }

                        this.Logger.Info(string.Format("[*] Azure Redis cache administration operation completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                       totalBuildDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.AzureRedisCacheAdminView.BackgroundTaskCompleted("Azure Redis cache administration operation completed.");
                        this.AzureRedisCacheAdminView.ApplyButtonEnable(true);

                        //Log background task completed log entry.

                        this.Logger.Info(string.Format("[*]Azure Redis cache administration operation completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                       totalBuildDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }


            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Perform Azure Redis cache administration opertation.
        /// </summary>
        /// <param name="azureRedisCacheConfig"></param>
        /// <param name="azureRedisCacheAdminOption"></param>
        public void PerformAzureRedisCacheAdminOperation(AzureRedisCacheConfigItem azureRedisCacheConfig,
                                                         AzureRedisCacheAdminOperation azureRedisCacheAdminOption)
        {

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.AzureRedisCacheAdminView.BackgroundTaskStatus = "Cancelling Azure Redis cache admin request...";
                    this.AzureRedisCacheAdminView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {

                    this.AzureRedisCacheManager = this.CreateAzureRedisCacheManager(azureRedisCacheConfig);

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => { this.BackgroundTaskCompleted(); });


                    this.AzureRedisCacheManager.AzureRedisCacheAdminStatusUpdated += OnAzureRedisCacheAdminStatusUpdated;
                    this.BackgroundTask = new Task(() => this.AzureRedisCacheManager.PerformAzureRedisCacheAdminOperation(azureRedisCacheAdminOption, cancellationToken), cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;
                    this.AzureRedisCacheAdminView.BackgroundTaskStatus = "Azure Redis cache operation requested...";
                    this.AzureRedisCacheAdminView.ApplyButtonText = "Cancel";

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Event Handler: On Azure Redis cache administration status status updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="azureRedisCacheAdminStatusEventArgs"></param>
        protected void OnAzureRedisCacheAdminStatusUpdated(Object sender,
                                                           AzureRedisCacheAdminStatusEventArgs azureRedisCacheAdminStatusEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Status list details available.
                if (azureRedisCacheAdminStatusEventArgs.AzureRedisCacheAdminStatusResult.StatusMessage != null)
                {


                    //Status list is not empty.
                    if (azureRedisCacheAdminStatusEventArgs.AzureRedisCacheAdminStatusResult.StatusList.Count > 0)
                    {
                        message = string.Format("Azure Redis cache administration operation with error(s). (Status message: {0}, Status --> {2})",
                                               azureRedisCacheAdminStatusEventArgs.AzureRedisCacheAdminStatusResult.StatusMessage,
                                               azureRedisCacheAdminStatusEventArgs.AzureRedisCacheAdminStatusResult.StatusList.Format(Aclara.Tools.Common.StatusManagement.StatusTypes.FormatOption.Minimum));

                        //Status list has errors or build failed.
                        if (azureRedisCacheAdminStatusEventArgs.AzureRedisCacheAdminStatusResult.StatusList.HasStatusSeverity(Aclara.Tools.Common.StatusManagement.StatusTypes.StatusSeverity.Error))
                        {
                            this.Logger.Error(message);

                        }
                        else if (azureRedisCacheAdminStatusEventArgs.AzureRedisCacheAdminStatusResult.StatusList.HasStatusSeverity(Aclara.Tools.Common.StatusManagement.StatusTypes.StatusSeverity.Warning))
                        {
                            this.Logger.Warn(message);
                        }
                        else
                        {
                            this.Logger.Info(message);
                        }

                    }
                    else if (azureRedisCacheAdminStatusEventArgs.AzureRedisCacheAdminStatusResult.StatusMessage != null)
                    {
                        message = string.Format("Azure Redis cache administration operation. (Status message: {0})",
                                                  azureRedisCacheAdminStatusEventArgs.AzureRedisCacheAdminStatusResult.StatusMessage);
                        this.Logger.Info(message);
                    }

                }
                else
                {
                    this.Logger.Info(string.Format("Azure Redis cache administration operation status. Details unavailable."));
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            AggregateException aggregateException = null;
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.

                        aggregateException = eventArgs.Exception;
                        innerException = aggregateException.InnerException;
                        eventArgs.SetObserved();

                        this.Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                    }

                    return;
                }


                aggregateException = eventArgs.Exception;
                innerException = aggregateException.InnerException;
                eventArgs.SetObserved();

                this.Logger.Error(innerException, string.Format("Background task failed. --> Inner exception: {0}",
                                                 innerException));

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        /// <summary>
        /// Create Azure Redis cache manager.
        /// </summary>
        /// <param name="azureRedisCacheConfigItem"></param>
        /// <returns></returns>
        protected AzureRedisCacheManager CreateAzureRedisCacheManager(AzureRedisCacheConfigItem azureRedisCacheConfigItem)
        {
            AzureRedisCacheManager result = null;
            AzureRedisCacheConfig azureRedisCacheConfig = null;

            try
            {
                azureRedisCacheConfig = new AzureRedisCacheConfig();

                azureRedisCacheConfig.Name = azureRedisCacheConfigItem.Name;
                azureRedisCacheConfig.Password = azureRedisCacheConfigItem.Password;
                azureRedisCacheConfig.Endpoint = azureRedisCacheConfigItem.Endpoint;
                azureRedisCacheConfig.Ssl = azureRedisCacheConfigItem.SSL;
                azureRedisCacheConfig.ConnectRetry = azureRedisCacheConfigItem.ConnectRetry;
                azureRedisCacheConfig.ConnectTimeout = azureRedisCacheConfigItem.ConnectTimeout;
                azureRedisCacheConfig.SyncTimeout = azureRedisCacheConfigItem.SyncTimeout;

                result = AzureRedisCacheManagerFactory.CreateRedisCacheManager(azureRedisCacheConfig);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
