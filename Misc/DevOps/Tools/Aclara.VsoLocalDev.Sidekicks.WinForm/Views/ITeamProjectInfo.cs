﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Team project information interface.
    /// </summary>
    public interface ITeamProjectInfo
    {
        string TeamProjectName { get; }

        Uri TeamProjectCollectionUri { get; }

        string WorkspaceName { get; }

        string BasicAuthRestAPIUserProfileName { get; }

        string BasicAuthRestAPIPassword { get; }
    }
}
