﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Solution list search form factory.
    /// </summary>
    public class SolutionListSearchFormFactory
    {

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SolutionListSearchFormFactory()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create solution list search form.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <returns></returns>
        static public SolutionListSearchForm CreateForm(ITeamProjectInfo teamProjectInfo, SidekickConfiguration sidekickConfiguration)
        {

            SolutionListSearchForm result = null;
            SolutionListSearchPresenter webAppSearchPresenter = null;

            try
            {

                result = new SolutionListSearchForm(sidekickConfiguration);
                webAppSearchPresenter = new SolutionListSearchPresenter(teamProjectInfo, sidekickConfiguration, result);

                result.SolutionListSearchPresenter = webAppSearchPresenter;
                result.TopLevel = false;

                webAppSearchPresenter.Initialize();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion
    }

}
