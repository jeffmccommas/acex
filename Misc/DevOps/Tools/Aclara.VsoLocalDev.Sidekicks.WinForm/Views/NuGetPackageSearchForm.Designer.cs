﻿namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    partial class NuGetPackageSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BranchComboBox = new System.Windows.Forms.ComboBox();
            this.BranchLabel = new System.Windows.Forms.Label();
            this.NuGetPackageInfoSelectContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.NuGetPackageInfoSelectNoneToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToggleExpandMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ExpandAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CollapseAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NuGetPackageInfoListsGroupBox = new System.Windows.Forms.GroupBox();
            this.SortOrderSplitButton = new Aclara.VsoLocalDev.Sidekicks.WinForm.SplitButton();
            this.SearchButton = new System.Windows.Forms.Button();
            this.PackageIdPatternTextBox = new System.Windows.Forms.TextBox();
            this.CopyToClipboardButton = new Aclara.VsoLocalDev.Sidekicks.WinForm.SplitButton();
            this.NuGetPackageInfoCopyContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.NuGetPackageInfoCopyTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToggleExpandSplitButton = new Aclara.VsoLocalDev.Sidekicks.WinForm.SplitButton();
            this.SelectSplitButton = new Aclara.VsoLocalDev.Sidekicks.WinForm.SplitButton();
            this.NuGetPackageInfoListTreeView = new System.Windows.Forms.TreeView();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.StatusPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.NuGetPackageInfoSearchToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.NuGetPackageInfoSortContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.NuGetPackageInfoSortPackageIdToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NuGetPackageInfoSortVersionCountToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NuGetPackageInfoSortReferenceCountToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NuGetPackageInfoSelectContextMenuStrip.SuspendLayout();
            this.ToggleExpandMenuStrip.SuspendLayout();
            this.NuGetPackageInfoListsGroupBox.SuspendLayout();
            this.NuGetPackageInfoCopyContextMenuStrip.SuspendLayout();
            this.StatusPanel.SuspendLayout();
            this.NuGetPackageInfoSortContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // BranchComboBox
            // 
            this.BranchComboBox.FormattingEnabled = true;
            this.BranchComboBox.Location = new System.Drawing.Point(50, 5);
            this.BranchComboBox.Name = "BranchComboBox";
            this.BranchComboBox.Size = new System.Drawing.Size(121, 21);
            this.BranchComboBox.TabIndex = 1;
            this.BranchComboBox.TextChanged += new System.EventHandler(this.BranchComboBox_TextChanged);
            // 
            // BranchLabel
            // 
            this.BranchLabel.AutoSize = true;
            this.BranchLabel.Location = new System.Drawing.Point(0, 9);
            this.BranchLabel.Name = "BranchLabel";
            this.BranchLabel.Size = new System.Drawing.Size(44, 13);
            this.BranchLabel.TabIndex = 0;
            this.BranchLabel.Text = "Branch:";
            // 
            // NuGetPackageInfoSelectContextMenuStrip
            // 
            this.NuGetPackageInfoSelectContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NuGetPackageInfoSelectNoneToolStripItem});
            this.NuGetPackageInfoSelectContextMenuStrip.Name = "NuGetPackageInfoSelectContextMenuStrip";
            this.NuGetPackageInfoSelectContextMenuStrip.Size = new System.Drawing.Size(104, 26);
            // 
            // NuGetPackageInfoSelectNoneToolStripItem
            // 
            this.NuGetPackageInfoSelectNoneToolStripItem.Name = "NuGetPackageInfoSelectNoneToolStripItem";
            this.NuGetPackageInfoSelectNoneToolStripItem.Size = new System.Drawing.Size(103, 22);
            this.NuGetPackageInfoSelectNoneToolStripItem.Text = "None";
            this.NuGetPackageInfoSelectNoneToolStripItem.Click += new System.EventHandler(this.NuGetPackageInfoSelectNoneToolStripItem_Click);
            // 
            // ToggleExpandMenuStrip
            // 
            this.ToggleExpandMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExpandAllToolStripMenuItem,
            this.CollapseAllToolStripMenuItem});
            this.ToggleExpandMenuStrip.Name = "ToggleExpandMenuStrip";
            this.ToggleExpandMenuStrip.Size = new System.Drawing.Size(137, 48);
            // 
            // ExpandAllToolStripMenuItem
            // 
            this.ExpandAllToolStripMenuItem.Name = "ExpandAllToolStripMenuItem";
            this.ExpandAllToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.ExpandAllToolStripMenuItem.Text = "Expand All";
            this.ExpandAllToolStripMenuItem.Click += new System.EventHandler(this.ExpandAllToolStripMenuItem_Click);
            // 
            // CollapseAllToolStripMenuItem
            // 
            this.CollapseAllToolStripMenuItem.Name = "CollapseAllToolStripMenuItem";
            this.CollapseAllToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.CollapseAllToolStripMenuItem.Text = "Collapse All";
            this.CollapseAllToolStripMenuItem.Click += new System.EventHandler(this.CollapseAllToolStripMenuItem_Click);
            // 
            // NuGetPackageInfoListsGroupBox
            // 
            this.NuGetPackageInfoListsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NuGetPackageInfoListsGroupBox.Controls.Add(this.SortOrderSplitButton);
            this.NuGetPackageInfoListsGroupBox.Controls.Add(this.SearchButton);
            this.NuGetPackageInfoListsGroupBox.Controls.Add(this.PackageIdPatternTextBox);
            this.NuGetPackageInfoListsGroupBox.Controls.Add(this.CopyToClipboardButton);
            this.NuGetPackageInfoListsGroupBox.Controls.Add(this.ToggleExpandSplitButton);
            this.NuGetPackageInfoListsGroupBox.Controls.Add(this.SelectSplitButton);
            this.NuGetPackageInfoListsGroupBox.Controls.Add(this.NuGetPackageInfoListTreeView);
            this.NuGetPackageInfoListsGroupBox.Location = new System.Drawing.Point(0, 63);
            this.NuGetPackageInfoListsGroupBox.Name = "NuGetPackageInfoListsGroupBox";
            this.NuGetPackageInfoListsGroupBox.Size = new System.Drawing.Size(688, 407);
            this.NuGetPackageInfoListsGroupBox.TabIndex = 4;
            this.NuGetPackageInfoListsGroupBox.TabStop = false;
            this.NuGetPackageInfoListsGroupBox.Text = "NuGet Packages";
            // 
            // SortOrderSplitButton
            // 
            this.SortOrderSplitButton.AutoSize = true;
            this.SortOrderSplitButton.ContextMenuStrip = this.NuGetPackageInfoSortContextMenuStrip;
            this.SortOrderSplitButton.Location = new System.Drawing.Point(172, 19);
            this.SortOrderSplitButton.Name = "SortOrderSplitButton";
            this.SortOrderSplitButton.Size = new System.Drawing.Size(75, 23);
            this.SortOrderSplitButton.TabIndex = 2;
            this.SortOrderSplitButton.Text = "Sort";
            this.SortOrderSplitButton.UseVisualStyleBackColor = true;
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(503, 19);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(75, 23);
            this.SearchButton.TabIndex = 4;
            this.SearchButton.Text = "Search";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // PackageIdPatternTextBox
            // 
            this.PackageIdPatternTextBox.Location = new System.Drawing.Point(253, 21);
            this.PackageIdPatternTextBox.Name = "PackageIdPatternTextBox";
            this.PackageIdPatternTextBox.Size = new System.Drawing.Size(244, 20);
            this.PackageIdPatternTextBox.TabIndex = 3;
            // 
            // CopyToClipboardButton
            // 
            this.CopyToClipboardButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CopyToClipboardButton.AutoSize = true;
            this.CopyToClipboardButton.ContextMenuStrip = this.NuGetPackageInfoCopyContextMenuStrip;
            this.CopyToClipboardButton.Location = new System.Drawing.Point(620, 19);
            this.CopyToClipboardButton.Name = "CopyToClipboardButton";
            this.CopyToClipboardButton.Size = new System.Drawing.Size(59, 23);
            this.CopyToClipboardButton.TabIndex = 5;
            this.CopyToClipboardButton.Text = "Copy";
            this.CopyToClipboardButton.UseVisualStyleBackColor = true;
            // 
            // NuGetPackageInfoCopyContextMenuStrip
            // 
            this.NuGetPackageInfoCopyContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NuGetPackageInfoCopyTextToolStripMenuItem});
            this.NuGetPackageInfoCopyContextMenuStrip.Name = "NuGetPackageInfoCopyContextMenuStrip";
            this.NuGetPackageInfoCopyContextMenuStrip.Size = new System.Drawing.Size(197, 26);
            // 
            // NuGetPackageInfoCopyTextToolStripMenuItem
            // 
            this.NuGetPackageInfoCopyTextToolStripMenuItem.Name = "NuGetPackageInfoCopyTextToolStripMenuItem";
            this.NuGetPackageInfoCopyTextToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.NuGetPackageInfoCopyTextToolStripMenuItem.Text = "Copy Text to Clipboard";
            this.NuGetPackageInfoCopyTextToolStripMenuItem.Click += new System.EventHandler(this.NuGetPackageInfoCopyTextToolStripMenuItem_Click);
            // 
            // ToggleExpandSplitButton
            // 
            this.ToggleExpandSplitButton.AutoSize = true;
            this.ToggleExpandSplitButton.ContextMenuStrip = this.ToggleExpandMenuStrip;
            this.ToggleExpandSplitButton.Location = new System.Drawing.Point(90, 19);
            this.ToggleExpandSplitButton.Name = "ToggleExpandSplitButton";
            this.ToggleExpandSplitButton.Size = new System.Drawing.Size(75, 23);
            this.ToggleExpandSplitButton.TabIndex = 1;
            this.ToggleExpandSplitButton.Text = "Expand";
            this.ToggleExpandSplitButton.UseVisualStyleBackColor = true;
            // 
            // SelectSplitButton
            // 
            this.SelectSplitButton.AutoSize = true;
            this.SelectSplitButton.ContextMenuStrip = this.NuGetPackageInfoSelectContextMenuStrip;
            this.SelectSplitButton.Location = new System.Drawing.Point(9, 19);
            this.SelectSplitButton.Name = "SelectSplitButton";
            this.SelectSplitButton.Size = new System.Drawing.Size(75, 23);
            this.SelectSplitButton.TabIndex = 0;
            this.SelectSplitButton.Text = "Select";
            this.SelectSplitButton.UseVisualStyleBackColor = true;
            // 
            // NuGetPackageInfoListTreeView
            // 
            this.NuGetPackageInfoListTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NuGetPackageInfoListTreeView.BackColor = System.Drawing.SystemColors.Window;
            this.NuGetPackageInfoListTreeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NuGetPackageInfoListTreeView.CheckBoxes = true;
            this.NuGetPackageInfoListTreeView.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NuGetPackageInfoListTreeView.LineColor = System.Drawing.Color.DodgerBlue;
            this.NuGetPackageInfoListTreeView.Location = new System.Drawing.Point(9, 48);
            this.NuGetPackageInfoListTreeView.Name = "NuGetPackageInfoListTreeView";
            this.NuGetPackageInfoListTreeView.Size = new System.Drawing.Size(670, 351);
            this.NuGetPackageInfoListTreeView.TabIndex = 6;
            this.NuGetPackageInfoSearchToolTip.SetToolTip(this.NuGetPackageInfoListTreeView, "[##|###] = Version Count | Total Reference Count");
            this.NuGetPackageInfoListTreeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.NuGetPackageInfoListTreeView_AfterCheck);
            this.NuGetPackageInfoListTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.NuGetPackageInfoListTreeView_AfterSelect);
            this.NuGetPackageInfoListTreeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.NuGetPackageInfoListTreeView_NodeMouseClick);
            // 
            // RefreshButton
            // 
            this.RefreshButton.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
            this.RefreshButton.Location = new System.Drawing.Point(181, 5);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(32, 23);
            this.RefreshButton.TabIndex = 2;
            this.RefreshButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // StatusPanel
            // 
            this.StatusPanel.BackColor = System.Drawing.Color.White;
            this.StatusPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.StatusPanel.Location = new System.Drawing.Point(0, 32);
            this.StatusPanel.Name = "StatusPanel";
            this.StatusPanel.Size = new System.Drawing.Size(688, 25);
            this.StatusPanel.TabIndex = 3;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(5, 5);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // NuGetPackageInfoSortContextMenuStrip
            // 
            this.NuGetPackageInfoSortContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NuGetPackageInfoSortPackageIdToolStripItem,
            this.NuGetPackageInfoSortVersionCountToolStripItem,
            this.NuGetPackageInfoSortReferenceCountToolStripItem});
            this.NuGetPackageInfoSortContextMenuStrip.Name = "NuGetPackageInfoSortContextMenuStrip";
            this.NuGetPackageInfoSortContextMenuStrip.Size = new System.Drawing.Size(163, 70);
            // 
            // NuGetPackageInfoSortPackageIdToolStripItem
            // 
            this.NuGetPackageInfoSortPackageIdToolStripItem.CheckOnClick = true;
            this.NuGetPackageInfoSortPackageIdToolStripItem.Name = "NuGetPackageInfoSortPackageIdToolStripItem";
            this.NuGetPackageInfoSortPackageIdToolStripItem.Size = new System.Drawing.Size(162, 22);
            this.NuGetPackageInfoSortPackageIdToolStripItem.Text = "Package Id";
            this.NuGetPackageInfoSortPackageIdToolStripItem.Click += new System.EventHandler(this.NuGetPackageInfoSortPackageIdToolStripItem_Click);
            // 
            // NuGetPackageInfoSortVersionCountToolStripItem
            // 
            this.NuGetPackageInfoSortVersionCountToolStripItem.CheckOnClick = true;
            this.NuGetPackageInfoSortVersionCountToolStripItem.Name = "NuGetPackageInfoSortVersionCountToolStripItem";
            this.NuGetPackageInfoSortVersionCountToolStripItem.Size = new System.Drawing.Size(162, 22);
            this.NuGetPackageInfoSortVersionCountToolStripItem.Text = "Version Count";
            this.NuGetPackageInfoSortVersionCountToolStripItem.Click += new System.EventHandler(this.NuGetPackageInfoSortVersionCountToolStripItem_Click);
            // 
            // NuGetPackageInfoSortReferenceCountToolStripItem
            // 
            this.NuGetPackageInfoSortReferenceCountToolStripItem.CheckOnClick = true;
            this.NuGetPackageInfoSortReferenceCountToolStripItem.Name = "NuGetPackageInfoSortReferenceCountToolStripItem";
            this.NuGetPackageInfoSortReferenceCountToolStripItem.Size = new System.Drawing.Size(162, 22);
            this.NuGetPackageInfoSortReferenceCountToolStripItem.Text = "Reference Count";
            this.NuGetPackageInfoSortReferenceCountToolStripItem.Click += new System.EventHandler(this.NuGetPackageInfoSortReferenceCountToolStripItem_Click);
            // 
            // NuGetPackageSearchForm
            // 
            this.AcceptButton = this.SearchButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 495);
            this.ControlBox = false;
            this.Controls.Add(this.StatusPanel);
            this.Controls.Add(this.RefreshButton);
            this.Controls.Add(this.NuGetPackageInfoListsGroupBox);
            this.Controls.Add(this.BranchComboBox);
            this.Controls.Add(this.BranchLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NuGetPackageSearchForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "NuGetPackageSearchForm";
            this.NuGetPackageInfoSelectContextMenuStrip.ResumeLayout(false);
            this.ToggleExpandMenuStrip.ResumeLayout(false);
            this.NuGetPackageInfoListsGroupBox.ResumeLayout(false);
            this.NuGetPackageInfoListsGroupBox.PerformLayout();
            this.NuGetPackageInfoCopyContextMenuStrip.ResumeLayout(false);
            this.StatusPanel.ResumeLayout(false);
            this.StatusPanel.PerformLayout();
            this.NuGetPackageInfoSortContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox BranchComboBox;
        private System.Windows.Forms.Label BranchLabel;
        private System.Windows.Forms.ContextMenuStrip NuGetPackageInfoSelectContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem NuGetPackageInfoSelectNoneToolStripItem;
        private System.Windows.Forms.ContextMenuStrip ToggleExpandMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ExpandAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CollapseAllToolStripMenuItem;
        private System.Windows.Forms.GroupBox NuGetPackageInfoListsGroupBox;
        private SplitButton ToggleExpandSplitButton;
        private SplitButton SelectSplitButton;
        private System.Windows.Forms.TreeView NuGetPackageInfoListTreeView;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.Panel StatusPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private SplitButton CopyToClipboardButton;
        private System.Windows.Forms.ContextMenuStrip NuGetPackageInfoCopyContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem NuGetPackageInfoCopyTextToolStripMenuItem;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.TextBox PackageIdPatternTextBox;
        private System.Windows.Forms.ToolTip NuGetPackageInfoSearchToolTip;
        private SplitButton SortOrderSplitButton;
        private System.Windows.Forms.ContextMenuStrip NuGetPackageInfoSortContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem NuGetPackageInfoSortPackageIdToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem NuGetPackageInfoSortVersionCountToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem NuGetPackageInfoSortReferenceCountToolStripItem;
    }
}