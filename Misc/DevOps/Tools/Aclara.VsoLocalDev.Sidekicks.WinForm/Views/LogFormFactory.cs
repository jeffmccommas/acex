﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Views;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Log form factory.
    /// </summary>
    public class LogFormFactory
    {

        #region Private Constants
        #endregion

        #region Public Methods

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="sidekickContainerForm"></param>
        /// <returns></returns>
        public static LogForm CreateForm(SidekickContainerForm sidekickContainerForm)
        {

            LogForm result = null;
            LogPresenter logPresenter = null;

            try
            {
                result = new LogForm(sidekickContainerForm);

                logPresenter = new LogPresenter(result);

                result.LogPresenter = logPresenter;
                result.TopLevel = false;

                logPresenter.Initialize();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

    }
}
