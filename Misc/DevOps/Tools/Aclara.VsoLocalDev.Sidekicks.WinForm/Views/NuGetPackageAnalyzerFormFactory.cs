﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build solution list form factory.
    /// </summary>
    public class NuGetPackageAnalyzerFormFactory
    {

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="logWriter"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="solutionListSearchForm"></param>
        /// <returns></returns>
        public static NuGetPackageAnalyzerForm CreateForm(SidekickConfiguration sidekickConfiguration,
                                                          NuGetPackageSearchForm solutionListSearchForm)
        {

            NuGetPackageAnalyzerForm result = null;
            NuGetPackageAnalyzerPresenter NuGetPackageAnalyzerPresenter = null;

            try
            {
                result = new NuGetPackageAnalyzerForm(sidekickConfiguration, solutionListSearchForm);

                NuGetPackageAnalyzerPresenter = new NuGetPackageAnalyzerPresenter(sidekickConfiguration, result);

                result.NuGetPackageAnalyzerPresenter = NuGetPackageAnalyzerPresenter;
                result.TopLevel = false;

                NuGetPackageAnalyzerPresenter.Initialize();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

    }
}
