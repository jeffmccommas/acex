﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using Aclara.TeamFoundation.VersionControl.Client;
using Aclara.Tools.Configuration;
using Aclara.Tools.Configuration.SolutionListConfig;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Solution list search presenter.
    /// </summary>
    public class SolutionListSearchPresenter
    {

        #region Private Constants

        private const string TeamProjectCollectionNameDefaultValue = "";
        private const string TeamProjectNameDefaultValue = "";

        private const string ReplacementToken_TeamProjectWorkingFolder = @"\$\(TeamProjectWorkingFolder\)";
        private const string ReplacementToken_TeamProjectName = @"\$\(TeamProjectName\)";
        private const string ReplacementToken_BranchName = @"\$\(BranchName\)";

        #endregion

        #region Private Data Members

        private SidekickConfiguration _sidekickConfiguration;
        private ISolutionListSearchView _solutionListSearchView = null;
        private ProjectManager _projectManager = null;
        private ITeamProjectInfo _teamProjectInfo = null;

        #endregion

        #region Public Types

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Solution list search view.
        /// </summary>
        public ISolutionListSearchView SolutionListSearchView
        {
            get
            {
                return _solutionListSearchView;
            }
            private set
            {
                _solutionListSearchView = value;
            }
        }

        /// <summary>
        /// Property: Project manager.
        /// </summary>
        public ProjectManager ProjectManager
        {
            get { return _projectManager; }
            set { _projectManager = value; }
        }

        /// <summary>
        /// Property: Team project collection default.
        /// </summary>
        public string TeamProjectCollectionNameDefault
        {
            get { return TeamProjectCollectionNameDefaultValue; }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Team project name default.
        /// </summary>
        public string TeamProjectNameDefault
        {
            get { return TeamProjectNameDefaultValue; }
        }

        /// <summary>
        /// Populate branch name list.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        public void PopulateBrachNameList(string tfsServerAddress,
                                          string teamProjectName)
        {
            List<string> branchNameList = null;


            try
            {

                this.ProjectManager = CreateProjectManager(tfsServerAddress, teamProjectName);

                branchNameList = this.ProjectManager.GetTeamProjectYearMonthBranchNames();
                if (branchNameList == null)
                {
                    branchNameList = new List<string>();
                }

                this.SolutionListSearchView.PopulateBranchNameList(branchNameList);

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="solutionListSearchView"></param>
        /// <param name="sidekickConfiguration"></param>
        public SolutionListSearchPresenter(ITeamProjectInfo teamProjectInfo,
                                           SidekickConfiguration sidekickConfiguration,
                                           ISolutionListSearchView solutionListSearchView)
        {
            this.TeamProjectInfo = teamProjectInfo;
            this.SidekickConfiguration = sidekickConfiguration;
            this.SolutionListSearchView = solutionListSearchView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <remarks>
        /// Hide default constructor to enforce dependency injection.
        /// </remarks>
        private SolutionListSearchPresenter()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {
            try
            {

                this.SolutionListSearchView.EnableControlsDependantOnTeamProject(false);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Team project collection name changed.
        /// </summary>
        public void TeamProjectCollectionNameChanged()
        {

            try
            {
                if (this.TeamProjectInfo.TeamProjectCollectionUri.ToString() != string.Empty &&
                    this.TeamProjectInfo.TeamProjectCollectionUri.ToString() != TeamProjectCollectionNameDefault &&
                    this.TeamProjectInfo.TeamProjectName != string.Empty &&
                    this.TeamProjectInfo.TeamProjectName != TeamProjectNameDefault)
                {
                    this.SolutionListSearchView.EnableControlsDependantOnTeamProject(true);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Team project name changed.
        /// </summary>
        public void TeamProjectNameChanged()
        {

            try
            {
                if (this.TeamProjectInfo.TeamProjectCollectionUri.ToString() != string.Empty &&
                    this.TeamProjectInfo.TeamProjectCollectionUri.ToString() != TeamProjectCollectionNameDefault &&
                    this.TeamProjectInfo.TeamProjectName != string.Empty &&
                    this.TeamProjectInfo.TeamProjectName != TeamProjectNameDefault)
                {
                    this.SolutionListSearchView.EnableControlsDependantOnTeamProject(true);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve solution selector lists.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="workspaceName"></param>
        /// <param name="userName"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        public SolutionSelectorLists GetSolutionSelectorLists(string tfsServerAddress,
                                                              string teamProjectName,
                                                              string workspaceName,
                                                              string userName,
                                                              string branchName)
        {
            SolutionSelectorLists result = null;
            SolutionLists solutionLists = null;
            SolutionSelectorList solutionSelectorList = null;
            SolutionSelector solutionSelector = null;
            SolutionConfigManager solutionConfigManager = null;
            SolutionConfigManagerFactory solutionConfigManagerFactory = null;
            string configurationFileName = string.Empty;
            string teamProjectWorkingFolder = string.Empty;

            try
            {

                result = new SolutionSelectorLists();

                solutionConfigManagerFactory = new SolutionConfigManagerFactory();
                solutionConfigManager = solutionConfigManagerFactory.CreateSolutionConfigManager();

                this.ProjectManager = this.CreateProjectManager(tfsServerAddress, teamProjectName);

                teamProjectWorkingFolder = this.ProjectManager.GetTeamProjectWorkingFolder(teamProjectName, workspaceName, branchName, userName);

                configurationFileName = this.GetSolutionListConfigFilePath(teamProjectName,
                                                                           branchName);

                if (File.Exists(configurationFileName) == false)
                {
                    throw new FileNotFoundException(string.Format("Solution list configuration file not found. (ConfigurationFileName: {0})",
                                                                  configurationFileName));
                }

                //Retrieve solution lists.
                solutionLists = solutionConfigManager.GetSolutionGroupList(configurationFileName);

                foreach (SolutionList solutionList in solutionLists)
                {
                    solutionSelectorList = new SolutionSelectorList();

                    solutionSelectorList.Name = solutionList.Name;

                    foreach (SolutionConfig solutionConfig in solutionList)
                    {
                        solutionSelector = new SolutionSelector();

                        solutionSelector.SolutionListName = solutionList.Name;
                        solutionSelector.SolutionName = solutionConfig.SolutionName;
                        solutionSelector.SolutionPath = solutionConfig.SolutionPath;
                        solutionSelector.MSBuildProperties = solutionConfig.MSBuildProperties;
                        this.ReplaceTextInSolutionSelector(ref solutionSelector,
                                                           teamProjectWorkingFolder,
                                                           teamProjectName,
                                                           branchName);
                        solutionSelectorList.Add(solutionSelector);
                    }

                    result.Add(solutionSelectorList);
                }

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Retrieve solution list configuration file path.
        /// </summary>
        /// <param name="teamProjectWorkingFolder"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        public string GetSolutionListConfigFilePath(string teamProjectName,
                                                    string branchName)
        {
            string result = string.Empty;
            string teamProjectWorkingFolder = string.Empty;

            try
            {
                teamProjectWorkingFolder = this.ProjectManager.GetTeamProjectWorkingFolder(this.TeamProjectInfo.TeamProjectName,
                                                                                           this.TeamProjectInfo.WorkspaceName, 
                                                                                           branchName,
                                                                                           this.TeamProjectInfo.BasicAuthRestAPIUserProfileName);

                result = this.SidekickConfiguration.GetSolutionListConfigFilePath(teamProjectWorkingFolder, teamProjectName, branchName);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve solution path.
        /// </summary>
        /// <param name="teamProjectWorkingFolder"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        /// 
        [Obsolete("Remove as method is not used.")]
        public string GetSolutionPath(string teamProjectName,
                                     string branchName)
        {
            string result = string.Empty;
            string teamProjectWorkingFolder = string.Empty;

            try
            {
                teamProjectWorkingFolder = this.ProjectManager.GetTeamProjectWorkingFolder(this.TeamProjectInfo.TeamProjectName,
                                                                                           this.TeamProjectInfo.WorkspaceName, "",
                                                                                           this.TeamProjectInfo.BasicAuthRestAPIUserProfileName);
                result = Path.Combine(teamProjectWorkingFolder,
                                      teamProjectName,
                                      branchName);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Determine whether team project is mapped to local folder.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        [Obsolete("Remove as method is not used.")]
        public bool IsTeamProjectMapped(string tfsServerAddress,
                                        string teamProjectName)
        {
            bool result = false;

            try
            {
                this.ProjectManager = CreateProjectManager(tfsServerAddress, this.TeamProjectInfo.TeamProjectName);

                result = this.ProjectManager.IsTeamProjectMappedToLocalFolder(this.TeamProjectInfo.TeamProjectName,
                                                                              this.TeamProjectInfo.WorkspaceName,
                                                                              this.TeamProjectInfo.BasicAuthRestAPIUserProfileName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create project manager.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        protected ProjectManager CreateProjectManager(string tfsServerAddress,
                                                      string teamProjectName)
        {
            ProjectManager result = null;
            Uri tfsServerUri = null;

            try
            {

                tfsServerUri = new Uri(tfsServerAddress);

                result = ProjectManagerFactory.CreateProjectManager(tfsServerUri, teamProjectName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Replace text in solution selector.
        /// </summary>
        /// <param name="solutionSelector"></param>
        /// <param name="teamProjectWorkingFolder"></param>
        /// <param name="branchName"></param>
        protected void ReplaceTextInSolutionSelector(ref SolutionSelector solutionSelector,
                                                    string teamProjectWorkingFolder,
                                                    string teamProjectName,
                                                    string branchName)
        {

            try
            {

                solutionSelector.SolutionPath = ReplaceTokenInTextWithValue(solutionSelector.SolutionPath,
                                                                           ReplacementToken_TeamProjectWorkingFolder,
                                                                           teamProjectWorkingFolder);
                solutionSelector.SolutionPath = ReplaceTokenInTextWithValue(solutionSelector.SolutionPath,
                                                                           ReplacementToken_TeamProjectName,
                                                                           teamProjectName);
                solutionSelector.SolutionPath = ReplaceTokenInTextWithValue(solutionSelector.SolutionPath,
                                                                           ReplacementToken_BranchName,
                                                                           branchName);


            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Replace token in text with specified value.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="token"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string ReplaceTokenInTextWithValue(string text, string token, string value)
        {
            string result = string.Empty;

            try
            {

                if (string.IsNullOrEmpty(text) == true)
                {
                    result = text;
                    return result;
                }

                result = Regex.Replace(text, token, value, RegexOptions.IgnoreCase);

                return result;

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

    }
}
