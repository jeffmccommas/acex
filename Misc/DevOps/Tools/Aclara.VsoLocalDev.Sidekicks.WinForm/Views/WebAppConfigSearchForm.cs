﻿using Aclara.VsoLocalDev.Sidekicks.WinForm;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Exceptions;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.Server;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    public partial class WebAppConfigSearchForm : Form, IWebAppConfigSearchView
    {

        #region Private Constancts
        #endregion

        #region Private Data Members

        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private SidekickConfiguration _sidekickConfiguration;
        private WebAppSelectorList _siteLevelWebAppSelectorList = null;
        private WebAppSelectorList _webAppSelectorList = null;
        private WebAppConfigSearchPresenter _webAppConfigSearchPresenter = null;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Web application search presenter.
        /// </summary>
        public WebAppConfigSearchPresenter WebAppConfigSearchPresenter
        {
            get { return _webAppConfigSearchPresenter; }
            set { _webAppConfigSearchPresenter = value; }
        }

        /// <summary>
        /// Property: TfsServerAddress.
        /// </summary>
        public string TeamProjectCollectionUri
        {
            get
            {
                if (this.TeamProjectCollectionNameSelectedLabel.Text == this.WebAppConfigSearchPresenter.TeamProjectCollectionNameDefault)
                {
                    return string.Empty;
                }
                return this.TeamProjectCollectionNameSelectedLabel.Text;
            }
            set { this.TeamProjectCollectionNameSelectedLabel.Text = value; }
        }

        /// <summary>
        /// Property: Team project name.
        /// </summary>
        public string TeamProjectName
        {
            get
            {
                if (this.TeamProjectNameSelectedLabel.Text == this.WebAppConfigSearchPresenter.TeamProjectNameDefault)
                {
                    return string.Empty;
                }
                return this.TeamProjectNameSelectedLabel.Text;
            }
            set
            {
                this.TeamProjectNameSelectedLabel.Text = value;
            }
        }

        /// <summary>
        /// Property: Site name.
        /// </summary>
        public string SiteName
        {
            get
            {
                return this.SiteNameValueLabel.Text;
            }
            set
            {
                this.SiteNameValueLabel.Text = value;
            }
        }

        /// <summary>
        /// Property: Web application path (partial).
        /// </summary>
        public string WebAppPathPartial
        {
            get
            {
                return this.WebAppPathPartialTextBox.Text;
            }
            set
            {
                this.WebAppPathPartialTextBox.Text = value;
            }
        }

        /// <summary>
        /// Property: Branch name.
        /// </summary>
        public string BranchName
        {
            get
            {
                string branchName = string.Empty;
                if (this.BranchComboBox.Text == null)
                {
                    branchName = string.Empty;
                }
                else
                {
                    branchName = this.BranchComboBox.Text;
                }
                return branchName;
            }
            set { this.BranchComboBox.SelectedItem = value; }
        }

        /// <summary>
        /// Property: Site level web application selector list.
        /// </summary>
        public WebAppSelectorList SiteLevelWebAppSelectorList
        {
            get
            {

                _webAppSelectorList = this.WebAppConfigSearchPresenter.GetWebAppSelectorList(this.TeamProjectCollectionUri,
                                                                                       this.TeamProjectName,
                                                                                       this.BranchName,
                                                                                       this.SiteName,
                                                                                       WebAppConfigSearchPresenter.WepApplicationLevel.SiteLevel);

                return _webAppSelectorList;
            }
            set
            {
                _siteLevelWebAppSelectorList = value;
            }
        }

        /// <summary>
        /// Property: Web app selector list.
        /// </summary>
        public WebAppSelectorList WebAppSelectorList
        {
            get
            {

                _webAppSelectorList = this.WebAppConfigSearchPresenter.GetWebAppSelectorList(this.TeamProjectCollectionUri,
                                                                                       this.TeamProjectName,
                                                                                       this.BranchName,
                                                                                       this.SiteName,
                                                                                       WebAppConfigSearchPresenter.WepApplicationLevel.ApplicationLevel);

                return _webAppSelectorList;
            }
            set
            {
                _webAppSelectorList = value;
            }
        }

        #endregion        

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public WebAppConfigSearchForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        public WebAppConfigSearchForm(SidekickConfiguration sidekickConfiguration)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.WebAppSelectorList = new WebAppSelectorList();

            InitializeComponent();

            //Disable controls dependant on team project.
            EnableControlsDependantOnTeamProject(false);
            //Disable controls dependant on branch.
            EnableControlsDependantOnBranch(false);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Enable controls dependant on team project.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnTeamProject(bool enabled)
        {
            try
            {
                this.BranchComboBox.Enabled = enabled;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable controls dependant on branch.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnBranch(bool enabled)
        {
            try
            {
                this.WebAppPathPartialTextBox.Enabled = enabled;
                this.SearchButton.Enabled = enabled;
                this.ClearButton.Enabled = enabled;
                this.DisplayWebAppConfigFileButton.Enabled = enabled;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable controls dependant on search.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnSearch(bool enabled)
        {
            try
            {
                this.SelectSplitButton.Enabled = enabled;
                this.DisplaySplitButton.Enabled = enabled;
                this.WebAppDataGridView.Enabled = enabled;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ClearSearch()
        {
        }

        public void PopulateBranchNameList(List<string> branchNameList)
        { 
        }

        public WebAppSelectorList GetSelectedWebAppSelectorList()
        {
            WebAppSelectorList result = null;

            return result;
        }

        public WebAppSelectorList GetSelectedSiteLevelWebAppSelectorList()
        {
            WebAppSelectorList result = null;

            return result;
        }
        #endregion

    }
}
