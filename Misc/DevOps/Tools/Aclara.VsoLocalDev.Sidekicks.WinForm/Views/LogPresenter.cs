﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Log presenter.
    /// </summary>
    public class LogPresenter
    {

        #region Private Constants
        
        #endregion

        #region Private Data Members

        private ILogView _logView;

        #endregion
                
        #region Public Properties

        /// <summary>
        /// Property: Log view.
        /// </summary>
        public ILogView LogView
        {
            get { return _logView; }
            set { _logView = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Overriden constructor.
        /// </summary>
        public LogPresenter(ILogView logView)
        {
            _logView = logView;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {
        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

    }
}
