﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Events;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    public partial class BuildSolutionListForm : Form, IBuildSolutionListView, ISidekickInfo, ISidekickView
    {

        #region Private Constants

        private const string SidekickView_SidekickName = "BuildSolutionList";
        private const string SidekickView_SidekickDescription = "Build Solution List";

        private const string BuildErrorsDataGridView_SeverityColumn = "SeverityColumn";
        private const string BuildErrorsDataGridView_SeverityIconColumn = "SeverityIconColumn";
        private const string BuildErrorsDataGridView_MessageColumn = "MessageColumn";

        private const string BuildErrorsDataGridView_Severity = "Severity";
        private const string BuildErrorsDataGridView_Message = "Message";

        private const string LogEventInfoLevel_Error = "Error";
        private const string LogEventInfoLevel_Warn = "Warn";

        private const string LogEventInfoDisplayLevel_Error = "Error";
        private const string LogEventInfoDisplayLevel_Warn = "Warning";

        private const string DisplayErrorsCheckboxText = "Errors";
        private const string DisplayWarningsCheckboxText = "Warnings";

        private const string ValidationErrorMessage_TeamProjectNameNotSelected = "Team project not selected.";
        private const string ValidationErrorMessage_SolutionNotSelected = "Solution not selected.";

        private const string Sidekick_Purpose = "Build related solutions for specified branch.";
        private const string Sidekick_Directions = "Pick Team Project, select branch, select one or more solutions and click Apply.";
        private const string Sidekick_Warnings = "No warnings. NOTE: If build fails then build solution with Visual Studio to find/fix build errors.";

        #endregion

        #region Private Data Members

        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private SidekickConfiguration _sidekickConfiguration;
        private SolutionListSearchForm _solutionListSearchForm = null;
        private BuildSolutionListPresenter _buildSolutionListPresenter;
        private SolutionSelector _selectedSolutionSelector;

        #endregion

        #region Public Types

        public enum BuildTargets
        {
            Build = 0,
            Clean = 1,
            Rebuild = 2
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickView_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickView_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public Logger Logger
        {
            get
            {
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick warnings.
        /// </summary>
        public string SidekickWarnings
        {
            get { return Sidekick_Warnings; }
        }

        /// <summary>
        /// Property: Sidekick purpose.
        /// </summary>
        public string SidekickPurpose
        {
            get { return Sidekick_Purpose; }
        }

        /// <summary>
        /// Property: Sidekick directions.
        /// </summary>
        public string SidekickDirections
        {
            get { return Sidekick_Directions; }
        }

        /// <summary>
        /// Property: Build solution list presenter.
        /// </summary>
        public BuildSolutionListPresenter BuildSolutionListPresenter
        {
            get { return _buildSolutionListPresenter; }
            set { _buildSolutionListPresenter = value; }
        }

        /// <summary>
        /// Property: Selected solution selector.
        /// </summary>
        public SolutionSelector SelectedSolutionSelector
        {
            get { return _selectedSolutionSelector; }
            set { _selectedSolutionSelector = value; }
        }

        /// <summary>
        /// Property: Web app search form.
        /// </summary>
        public SolutionListSearchForm SolutionListSearchForm
        {
            get { return _solutionListSearchForm; }
            set { _solutionListSearchForm = value; }
        }

        /// <summary>
        /// Property: Build target.
        /// </summary>
        public BuildTargets BuildTarget
        {
            get
            {
                return (BuildTargets)this.BuildTargetComboBox.SelectedValue;
            }
            set
            {
                this.BuildTargetComboBox.SelectedValue = value;
            }
        }

        /// <summary>
        /// Property: Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get
            {
                return this.ApplyButton.Text;
            }
            set
            {
                this.ApplyButton.Text = value;
            }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get
            {
                return this.BackgroundTaskStatusLabel.Text;
            }
            set
            {
                this.BackgroundTaskStatusLabel.Text = value;
            }
        }

        /// <summary>
        /// Property: Is sidekick busy.
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;
                result = this.BuildSolutionListPresenter.IsBackgroundTaskBusy;
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="solutionListSearchForm"></param>
        public BuildSolutionListForm(SidekickConfiguration sidekickConfiguration,
                                     SolutionListSearchForm solutionListSearchForm)
        {
            InitializeComponent();

            this.SidekickConfiguration = sidekickConfiguration;
            this.InitializeControls();
            this.SolutionListSearchForm = solutionListSearchForm;
            this.SolutionListSearchPanel.DockControl(this.SolutionListSearchForm);

            foreach (DataGridViewColumn column in this.BuildErrorsDataGridView.Columns)
            {

                switch (column.Name)
                {
                    case BuildErrorsDataGridView_SeverityColumn:
                        column.DataPropertyName = BuildErrorsDataGridView_Severity;
                        break;
                    case BuildErrorsDataGridView_MessageColumn:
                        column.DataPropertyName = BuildErrorsDataGridView_Message;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        break;

                }
            }

            this.SolutionListSearchForm.SolutionSelectorSelected += OnSolutionSelectorSelected;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private BuildSolutionListForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Apply button enable.
        /// </summary>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            this.SolutionListSearchForm.TeamProjectChanged();
            this.EnableControlsDependantOnTeamProject(true);
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Highlight solution.
        /// </summary>
        /// <param name="solutionSelector"></param>
        public void UpdateSolutionBuildStatus(SolutionSelector solutionSelector)
        {
            try
            {

                if (this.InvokeRequired == true)
                {
                    this.Invoke(new Action<SolutionSelector>(this.UpdateSolutionBuildStatus), solutionSelector);
                }
                else
                {
                    this.SolutionListSearchForm.UpdateSolutionBuildStatus(solutionSelector);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Reset all solution build status.
        /// </summary>
        public void ResetAllSolutionBuildStatus()
        {
            try
            {

                if (this.InvokeRequired == true)
                {
                    this.Invoke(new Action(this.ResetAllSolutionBuildStatus));
                }
                else
                {
                    this.SolutionListSearchForm.ResetAllSolutionBuildStatus();
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {

                this.BuildTargetComboBox.DataSource = Enum.GetValues(typeof(BuildTargets));
                this.BuildTargetComboBox.SelectedItem = BuildTargets.Rebuild;

                this.EnableControlsDependantOnTeamProject(false);
                this.EnableControlsDependantOnSolutionSelector(false);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Solution selector selected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="solutionSelectorSelectedEventArgs"></param>
        protected void OnSolutionSelectorSelected(Object sender,
                                                  SolutionSelectorSelectedEventArgs solutionSelectorSelectedEventArgs)
        {
            try
            {
                this.ClearBuildErrorsDataGridView();
                this.UpdateDisplayCheckboxText();

                this.SelectedSolutionSelector = solutionSelectorSelectedEventArgs.SolutionSelectorSelectedResult.SolutionSelector;
                if (this.SelectedSolutionSelector == null)
                {
                    EnableControlsDependantOnSolutionSelector(false);
                    return;
                }

                if (this.SelectedSolutionSelector.BuildErrorList == null &&
                    this.SelectedSolutionSelector.BuildWarningList == null)
                {
                    EnableControlsDependantOnSolutionSelector(false);
                    return;
                }
                else
                {
                    this.PopulateBuildErrorsDataGridView();
                    EnableControlsDependantOnSolutionSelector(true);
                    this.UpdateDisplayCheckboxText();
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Populate build errors data grid view.
        /// </summary>
        protected void PopulateBuildErrorsDataGridView()
        {
            try
            {

                if (this.SelectedSolutionSelector != null)
                {
                    if (this.DisplayErrorsCheckBox.Checked == true)
                    {
                        if (this.SelectedSolutionSelector.BuildErrorList != null)
                        {
                            foreach (string buildError in this.SelectedSolutionSelector.BuildErrorList)
                            {
                                this.BuildErrorsDataGridView.Rows.Add((System.Drawing.Image)Properties.Resources.Severity_Icon_Error_Exception, "Error", buildError);
                            }

                        }
                    }

                    if (this.DisplayWarningsCheckBox.Checked == true)
                    {
                        if (this.SelectedSolutionSelector.BuildWarningList != null)
                        {
                            foreach (string buildWarning in this.SelectedSolutionSelector.BuildWarningList)
                            {
                                this.BuildErrorsDataGridView.Rows.Add((System.Drawing.Image)Properties.Resources.Severity_Icon_Warning, "Warning", buildWarning);
                            }

                        }
                    }

                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Clear build errors data grid view.
        /// </summary>
        protected void ClearBuildErrorsDataGridView()
        {
            try
            {
                this.BuildErrorsDataGridView.DataSource = null;
                this.BuildErrorsDataGridView.Rows.Clear();
                this.BuildErrorsDataGridView.Refresh();

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update display (errors, warnings, information) checkbox text.
        /// </summary>
        protected void UpdateDisplayCheckboxText()
        {
            int errorsCount = 0;
            int warningsCount = 0;

            try
            {
                if (this.SelectedSolutionSelector != null &&
                     this.SelectedSolutionSelector.BuildErrorList != null)
                {
                    errorsCount = this.SelectedSolutionSelector.BuildErrorList.Count();
                }
                if (this.SelectedSolutionSelector != null &&
                    this.SelectedSolutionSelector.BuildWarningList != null)
                {
                    warningsCount = this.SelectedSolutionSelector.BuildWarningList.Count();
                }

                this.DisplayErrorsCheckBox.Text = string.Format("{0} {1}",
                                                                errorsCount,
                                                                DisplayErrorsCheckboxText);
                this.DisplayWarningsCheckBox.Text = string.Format("{0} {1}",
                                                                warningsCount,
                                                                DisplayWarningsCheckboxText);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable (disable) controls dependant on team project.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnTeamProject(bool enable)
        {
            this.BuildTargetComboBox.Enabled = enable;
            this.ApplyButton.Enabled = enable;
        }

        /// <summary>
        /// Enable (disable) controls dependant on solution selector.
        /// </summary>
        protected void EnableControlsDependantOnSolutionSelector(bool enable)
        {
            this.CopyButton.Enabled = enable;
            this.DisplayErrorsCheckBox.Enabled = enable;
            this.DisplayWarningsCheckBox.Enabled = enable;
            this.BuildErrorsDataGridView.Enabled = enable;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            SolutionSelectorList solutionSelectorList = null;
            string buildTarget = string.Empty;

            try
            {

                if (this.SolutionListSearchForm.SolutionListSearchPresenter.TeamProjectInfo.TeamProjectCollectionUri.ToString() == string.Empty ||
                   (this.SolutionListSearchForm.SolutionListSearchPresenter.TeamProjectInfo.TeamProjectName == string.Empty))
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_TeamProjectNameNotSelected));
                    return;
                }

                solutionSelectorList = this.SolutionListSearchForm.GetSelectedSolutionSelectorList();
                if (solutionSelectorList == null ||
                    solutionSelectorList.Count == 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_SolutionNotSelected));
                    return;
                }

                this.ClearBuildErrorsDataGridView();
                this.UpdateDisplayCheckboxText();
                this.ResetAllSolutionBuildStatus();

                buildTarget = this.BuildTarget.ToString();
                this.BuildSolutionListPresenter.BuildSolutionList(this.SolutionListSearchForm.SolutionListSearchPresenter.TeamProjectInfo.TeamProjectCollectionUri.ToString(),
                                                                  this.SolutionListSearchForm.SolutionListSearchPresenter.TeamProjectInfo.TeamProjectName,
                                                                  this.SolutionListSearchForm.BranchName,
                                                                  buildTarget,
                                                                  solutionSelectorList);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Display errors checkbox - Changed changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayErrorsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            this.PopulateBuildErrorsDataGridView();
        }

        /// <summary>
        /// Event Handler: Display warnings checkbox - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayWarningsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            this.PopulateBuildErrorsDataGridView();

        }

        /// <summary>
        /// Event Handler: Copy button - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyButton_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            StringBuilder messageStringBuilder = null;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                messageStringBuilder = new StringBuilder();

                foreach (DataGridViewRow dataGridViewRow in this.BuildErrorsDataGridView.Rows)
                {
                    message = (string)dataGridViewRow.Cells[BuildErrorsDataGridView_MessageColumn].Value;

                    messageStringBuilder.AppendLine(string.Format("{0}",
                                                                  message.ToString()));

                }

                Clipboard.SetDataObject(messageStringBuilder.ToString());

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickBuildSolutionList.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion
    }
}
