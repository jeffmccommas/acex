﻿namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    partial class SidekickContainerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SidekickContainerForm));
            this.SidekickContainerMenuStrip = new System.Windows.Forms.MenuStrip();
            this.FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitSidekickContainerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SidekicksSidekickContainerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowLogFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpSidekickContainerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ChangeLogSidekickContainerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CheckForNewVersionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DocumentsSidekickContainerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutSidekickContainerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip = new System.Windows.Forms.ToolStrip();
            this.HomeToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.BackToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ForwardToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ConnectTeamProjectToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.MainSidekickSelectorToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.MainSidekickSelectorContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.WorkspaceToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.ShowLogToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.HelpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.AutoUpdaterToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.AutoUpdaterToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.VisualStudioOnlineToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.VisualStudioTeamServicesConnectedProjectOnlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.VisualStudioTeamServicesManageRepositoriesOnlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SidekickContainerStatusStrip = new System.Windows.Forms.StatusStrip();
            this.LoggedInAsToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.TeamProjectCollectionUriToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.TeamProjectNameToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.SidekickContainerPanel = new System.Windows.Forms.Panel();
            this.HorizontalSplitter = new System.Windows.Forms.Splitter();
            this.LogPanel = new System.Windows.Forms.Panel();
            this.OutputLabel = new System.Windows.Forms.Label();
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.SidekickViewHistoryContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SidekickContainerMenuStrip.SuspendLayout();
            this.ToolStrip.SuspendLayout();
            this.SidekickContainerStatusStrip.SuspendLayout();
            this.LogPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // SidekickContainerMenuStrip
            // 
            this.SidekickContainerMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileToolStripMenuItem,
            this.ViewToolStripMenuItem,
            this.HelpToolStripMenuItem});
            this.SidekickContainerMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.SidekickContainerMenuStrip.Name = "SidekickContainerMenuStrip";
            this.SidekickContainerMenuStrip.Size = new System.Drawing.Size(1228, 24);
            this.SidekickContainerMenuStrip.TabIndex = 0;
            this.SidekickContainerMenuStrip.Text = "menuStrip1";
            // 
            // FileToolStripMenuItem
            // 
            this.FileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExitSidekickContainerToolStripMenuItem});
            this.FileToolStripMenuItem.Name = "FileToolStripMenuItem";
            this.FileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.FileToolStripMenuItem.Text = "File";
            // 
            // ExitSidekickContainerToolStripMenuItem
            // 
            this.ExitSidekickContainerToolStripMenuItem.Name = "ExitSidekickContainerToolStripMenuItem";
            this.ExitSidekickContainerToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.ExitSidekickContainerToolStripMenuItem.Text = "Exit";
            this.ExitSidekickContainerToolStripMenuItem.Click += new System.EventHandler(this.ExitSidekickContainerToolStripMenuItem_Click);
            // 
            // ViewToolStripMenuItem
            // 
            this.ViewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SidekicksSidekickContainerToolStripMenuItem,
            this.ShowLogToolStripMenuItem,
            this.ShowLogFileToolStripMenuItem});
            this.ViewToolStripMenuItem.Name = "ViewToolStripMenuItem";
            this.ViewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.ViewToolStripMenuItem.Text = "View";
            // 
            // SidekicksSidekickContainerToolStripMenuItem
            // 
            this.SidekicksSidekickContainerToolStripMenuItem.Name = "SidekicksSidekickContainerToolStripMenuItem";
            this.SidekicksSidekickContainerToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.SidekicksSidekickContainerToolStripMenuItem.Text = "Sidekicks";
            // 
            // ShowLogToolStripMenuItem
            // 
            this.ShowLogToolStripMenuItem.Checked = true;
            this.ShowLogToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowLogToolStripMenuItem.Name = "ShowLogToolStripMenuItem";
            this.ShowLogToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.ShowLogToolStripMenuItem.Text = "Show Log";
            this.ShowLogToolStripMenuItem.Click += new System.EventHandler(this.ShowLogToolStripMenuItem_Click);
            // 
            // ShowLogFileToolStripMenuItem
            // 
            this.ShowLogFileToolStripMenuItem.Name = "ShowLogFileToolStripMenuItem";
            this.ShowLogFileToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.ShowLogFileToolStripMenuItem.Text = "Show Log File";
            this.ShowLogFileToolStripMenuItem.Click += new System.EventHandler(this.ShowLogFileToolStripMenuItem_Click);
            // 
            // HelpToolStripMenuItem
            // 
            this.HelpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HelpSidekickContainerToolStripMenuItem,
            this.ChangeLogSidekickContainerToolStripMenuItem,
            this.CheckForNewVersionToolStripMenuItem,
            this.DocumentsSidekickContainerToolStripMenuItem,
            this.AboutSidekickContainerToolStripMenuItem});
            this.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem";
            this.HelpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.HelpToolStripMenuItem.Text = "Help";
            // 
            // HelpSidekickContainerToolStripMenuItem
            // 
            this.HelpSidekickContainerToolStripMenuItem.Name = "HelpSidekickContainerToolStripMenuItem";
            this.HelpSidekickContainerToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.HelpSidekickContainerToolStripMenuItem.Text = "View Help";
            this.HelpSidekickContainerToolStripMenuItem.Click += new System.EventHandler(this.HelpSidekickContainerToolStripMenuItem_Click);
            // 
            // ChangeLogSidekickContainerToolStripMenuItem
            // 
            this.ChangeLogSidekickContainerToolStripMenuItem.Name = "ChangeLogSidekickContainerToolStripMenuItem";
            this.ChangeLogSidekickContainerToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.ChangeLogSidekickContainerToolStripMenuItem.Text = "View Change Log...";
            this.ChangeLogSidekickContainerToolStripMenuItem.Click += new System.EventHandler(this.ChangeLogSidekickContainerToolStripMenuItem_Click);
            // 
            // CheckForNewVersionToolStripMenuItem
            // 
            this.CheckForNewVersionToolStripMenuItem.Name = "CheckForNewVersionToolStripMenuItem";
            this.CheckForNewVersionToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.CheckForNewVersionToolStripMenuItem.Text = "Check for New Version";
            this.CheckForNewVersionToolStripMenuItem.Click += new System.EventHandler(this.CheckForNewVersionToolStripMenuItem_Click);
            // 
            // DocumentsSidekickContainerToolStripMenuItem
            // 
            this.DocumentsSidekickContainerToolStripMenuItem.Name = "DocumentsSidekickContainerToolStripMenuItem";
            this.DocumentsSidekickContainerToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.DocumentsSidekickContainerToolStripMenuItem.Text = "Documents";
            // 
            // AboutSidekickContainerToolStripMenuItem
            // 
            this.AboutSidekickContainerToolStripMenuItem.Name = "AboutSidekickContainerToolStripMenuItem";
            this.AboutSidekickContainerToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.AboutSidekickContainerToolStripMenuItem.Text = "About Aclara VSO LocalDev Sidekicks";
            this.AboutSidekickContainerToolStripMenuItem.Click += new System.EventHandler(this.AboutSidekickContainerToolStripMenuItem_Click);
            // 
            // ToolStrip
            // 
            this.ToolStrip.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HomeToolStripButton,
            this.BackToolStripButton,
            this.ForwardToolStripButton,
            this.ConnectTeamProjectToolStripButton,
            this.MainSidekickSelectorToolStripSplitButton,
            this.WorkspaceToolStripComboBox,
            this.ShowLogToolStripButton,
            this.HelpToolStripButton,
            this.AutoUpdaterToolStripButton,
            this.AutoUpdaterToolStripLabel,
            this.VisualStudioOnlineToolStripSplitButton});
            this.ToolStrip.Location = new System.Drawing.Point(0, 24);
            this.ToolStrip.Name = "ToolStrip";
            this.ToolStrip.Size = new System.Drawing.Size(1228, 25);
            this.ToolStrip.TabIndex = 1;
            this.ToolStrip.Text = "toolStrip1";
            // 
            // HomeToolStripButton
            // 
            this.HomeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.HomeToolStripButton.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.home;
            this.HomeToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.HomeToolStripButton.Name = "HomeToolStripButton";
            this.HomeToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.HomeToolStripButton.Text = "toolStripButton1";
            this.HomeToolStripButton.Click += new System.EventHandler(this.HomeToolStripButton_Click);
            // 
            // BackToolStripButton
            // 
            this.BackToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BackToolStripButton.Enabled = false;
            this.BackToolStripButton.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.ActionBack;
            this.BackToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BackToolStripButton.Name = "BackToolStripButton";
            this.BackToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.BackToolStripButton.Text = "Back";
            this.BackToolStripButton.ToolTipText = "Click to go back";
            this.BackToolStripButton.Click += new System.EventHandler(this.BackToolStripButton_Click);
            this.BackToolStripButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BackToolStripButton_MouseDown);
            // 
            // ForwardToolStripButton
            // 
            this.ForwardToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ForwardToolStripButton.Enabled = false;
            this.ForwardToolStripButton.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.ActionForward;
            this.ForwardToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ForwardToolStripButton.Name = "ForwardToolStripButton";
            this.ForwardToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.ForwardToolStripButton.Text = "Forward";
            this.ForwardToolStripButton.ToolTipText = "Click to go forward";
            this.ForwardToolStripButton.Click += new System.EventHandler(this.ForwardToolStripButton_Click);
            this.ForwardToolStripButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ForwardToolStripButton_MouseDown);
            // 
            // ConnectTeamProjectToolStripButton
            // 
            this.ConnectTeamProjectToolStripButton.BackColor = System.Drawing.Color.DarkOrange;
            this.ConnectTeamProjectToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("ConnectTeamProjectToolStripButton.Image")));
            this.ConnectTeamProjectToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ConnectTeamProjectToolStripButton.Name = "ConnectTeamProjectToolStripButton";
            this.ConnectTeamProjectToolStripButton.Size = new System.Drawing.Size(72, 22);
            this.ConnectTeamProjectToolStripButton.Text = "Connect";
            this.ConnectTeamProjectToolStripButton.Click += new System.EventHandler(this.ConnectTeamProjectToolStripButton_Click);
            // 
            // MainSidekickSelectorToolStripSplitButton
            // 
            this.MainSidekickSelectorToolStripSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.MainSidekickSelectorToolStripSplitButton.DropDown = this.MainSidekickSelectorContextMenuStrip;
            this.MainSidekickSelectorToolStripSplitButton.Image = ((System.Drawing.Image)(resources.GetObject("MainSidekickSelectorToolStripSplitButton.Image")));
            this.MainSidekickSelectorToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MainSidekickSelectorToolStripSplitButton.Name = "MainSidekickSelectorToolStripSplitButton";
            this.MainSidekickSelectorToolStripSplitButton.Size = new System.Drawing.Size(66, 22);
            this.MainSidekickSelectorToolStripSplitButton.Text = "Sidekick";
            this.MainSidekickSelectorToolStripSplitButton.ButtonClick += new System.EventHandler(this.MainSidekickSelectorToolStripSplitButton_ButtonClick);
            // 
            // MainSidekickSelectorContextMenuStrip
            // 
            this.MainSidekickSelectorContextMenuStrip.Name = "MainSidekickSelectorContextMenuStrip";
            this.MainSidekickSelectorContextMenuStrip.OwnerItem = this.MainSidekickSelectorToolStripSplitButton;
            this.MainSidekickSelectorContextMenuStrip.Size = new System.Drawing.Size(61, 4);
            // 
            // WorkspaceToolStripComboBox
            // 
            this.WorkspaceToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.WorkspaceToolStripComboBox.Name = "WorkspaceToolStripComboBox";
            this.WorkspaceToolStripComboBox.Size = new System.Drawing.Size(190, 25);
            this.WorkspaceToolStripComboBox.ToolTipText = "Workspaces";
            this.WorkspaceToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.WorkspaceToolStripComboBox_SelectedIndexChanged);
            // 
            // ShowLogToolStripButton
            // 
            this.ShowLogToolStripButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ShowLogToolStripButton.Checked = true;
            this.ShowLogToolStripButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowLogToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ShowLogToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("ShowLogToolStripButton.Image")));
            this.ShowLogToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ShowLogToolStripButton.Name = "ShowLogToolStripButton";
            this.ShowLogToolStripButton.Size = new System.Drawing.Size(59, 22);
            this.ShowLogToolStripButton.Text = "Hide Log";
            this.ShowLogToolStripButton.Click += new System.EventHandler(this.ShowLogToolStripButton_Click);
            // 
            // HelpToolStripButton
            // 
            this.HelpToolStripButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.HelpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.HelpToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("HelpToolStripButton.Image")));
            this.HelpToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.HelpToolStripButton.Name = "HelpToolStripButton";
            this.HelpToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.HelpToolStripButton.Text = "View Help";
            this.HelpToolStripButton.Click += new System.EventHandler(this.HelpToolStripButton_Click);
            // 
            // AutoUpdaterToolStripButton
            // 
            this.AutoUpdaterToolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.AutoUpdaterToolStripButton.BackColor = System.Drawing.Color.Gold;
            this.AutoUpdaterToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.AutoUpdaterToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("AutoUpdaterToolStripButton.Image")));
            this.AutoUpdaterToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AutoUpdaterToolStripButton.Name = "AutoUpdaterToolStripButton";
            this.AutoUpdaterToolStripButton.Size = new System.Drawing.Size(77, 22);
            this.AutoUpdaterToolStripButton.Text = "Update Now";
            this.AutoUpdaterToolStripButton.Click += new System.EventHandler(this.AutoUpdaterToolStripButton_Click);
            // 
            // AutoUpdaterToolStripLabel
            // 
            this.AutoUpdaterToolStripLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.AutoUpdaterToolStripLabel.BackColor = System.Drawing.Color.LightSteelBlue;
            this.AutoUpdaterToolStripLabel.Name = "AutoUpdaterToolStripLabel";
            this.AutoUpdaterToolStripLabel.Size = new System.Drawing.Size(144, 22);
            this.AutoUpdaterToolStripLabel.Text = "A new version is available.";
            // 
            // VisualStudioOnlineToolStripSplitButton
            // 
            this.VisualStudioOnlineToolStripSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.VisualStudioOnlineToolStripSplitButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.VisualStudioTeamServicesConnectedProjectOnlineToolStripMenuItem,
            this.VisualStudioTeamServicesManageRepositoriesOnlineToolStripMenuItem});
            this.VisualStudioOnlineToolStripSplitButton.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.vsts_logo;
            this.VisualStudioOnlineToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.VisualStudioOnlineToolStripSplitButton.Name = "VisualStudioOnlineToolStripSplitButton";
            this.VisualStudioOnlineToolStripSplitButton.Size = new System.Drawing.Size(32, 22);
            this.VisualStudioOnlineToolStripSplitButton.Text = "VisualStudioTeamServicesToolStripSplitButton";
            this.VisualStudioOnlineToolStripSplitButton.ToolTipText = "Visual Studio Team Services";
            this.VisualStudioOnlineToolStripSplitButton.Click += new System.EventHandler(this.VisualStudioOnlineToolStripSplitButton_Click);
            // 
            // VisualStudioTeamServicesConnectedProjectOnlineToolStripMenuItem
            // 
            this.VisualStudioTeamServicesConnectedProjectOnlineToolStripMenuItem.Name = "VisualStudioTeamServicesConnectedProjectOnlineToolStripMenuItem";
            this.VisualStudioTeamServicesConnectedProjectOnlineToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.VisualStudioTeamServicesConnectedProjectOnlineToolStripMenuItem.Text = "Connected Project";
            this.VisualStudioTeamServicesConnectedProjectOnlineToolStripMenuItem.Click += new System.EventHandler(this.VisualStudioTeamServicesConnectedProjectOnlineToolStripMenuItem_Click);
            // 
            // VisualStudioTeamServicesManageRepositoriesOnlineToolStripMenuItem
            // 
            this.VisualStudioTeamServicesManageRepositoriesOnlineToolStripMenuItem.Name = "VisualStudioTeamServicesManageRepositoriesOnlineToolStripMenuItem";
            this.VisualStudioTeamServicesManageRepositoriesOnlineToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.VisualStudioTeamServicesManageRepositoriesOnlineToolStripMenuItem.Text = "Manage Repositories";
            this.VisualStudioTeamServicesManageRepositoriesOnlineToolStripMenuItem.Click += new System.EventHandler(this.VisualStudioTeamServicesManageRepositoriesOnlineToolStripMenuItem_Click);
            // 
            // SidekickContainerStatusStrip
            // 
            this.SidekickContainerStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LoggedInAsToolStripStatusLabel,
            this.TeamProjectCollectionUriToolStripStatusLabel,
            this.TeamProjectNameToolStripStatusLabel});
            this.SidekickContainerStatusStrip.Location = new System.Drawing.Point(0, 813);
            this.SidekickContainerStatusStrip.Name = "SidekickContainerStatusStrip";
            this.SidekickContainerStatusStrip.Size = new System.Drawing.Size(1228, 22);
            this.SidekickContainerStatusStrip.TabIndex = 7;
            // 
            // LoggedInAsToolStripStatusLabel
            // 
            this.LoggedInAsToolStripStatusLabel.Name = "LoggedInAsToolStripStatusLabel";
            this.LoggedInAsToolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // TeamProjectCollectionUriToolStripStatusLabel
            // 
            this.TeamProjectCollectionUriToolStripStatusLabel.Name = "TeamProjectCollectionUriToolStripStatusLabel";
            this.TeamProjectCollectionUriToolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // TeamProjectNameToolStripStatusLabel
            // 
            this.TeamProjectNameToolStripStatusLabel.Name = "TeamProjectNameToolStripStatusLabel";
            this.TeamProjectNameToolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // SidekickContainerPanel
            // 
            this.SidekickContainerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SidekickContainerPanel.Location = new System.Drawing.Point(0, 49);
            this.SidekickContainerPanel.Name = "SidekickContainerPanel";
            this.SidekickContainerPanel.Size = new System.Drawing.Size(1228, 654);
            this.SidekickContainerPanel.TabIndex = 11;
            // 
            // HorizontalSplitter
            // 
            this.HorizontalSplitter.BackColor = System.Drawing.SystemColors.ControlDark;
            this.HorizontalSplitter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.HorizontalSplitter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.HorizontalSplitter.Location = new System.Drawing.Point(0, 703);
            this.HorizontalSplitter.MinExtra = 500;
            this.HorizontalSplitter.MinSize = 120;
            this.HorizontalSplitter.Name = "HorizontalSplitter";
            this.HorizontalSplitter.Size = new System.Drawing.Size(1228, 10);
            this.HorizontalSplitter.TabIndex = 10;
            this.HorizontalSplitter.TabStop = false;
            // 
            // LogPanel
            // 
            this.LogPanel.Controls.Add(this.OutputLabel);
            this.LogPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LogPanel.Location = new System.Drawing.Point(0, 713);
            this.LogPanel.Name = "LogPanel";
            this.LogPanel.Size = new System.Drawing.Size(1228, 100);
            this.LogPanel.TabIndex = 12;
            // 
            // OutputLabel
            // 
            this.OutputLabel.AutoSize = true;
            this.OutputLabel.Location = new System.Drawing.Point(3, 3);
            this.OutputLabel.Name = "OutputLabel";
            this.OutputLabel.Size = new System.Drawing.Size(39, 13);
            this.OutputLabel.TabIndex = 0;
            this.OutputLabel.Text = "Output";
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.VsoLocalDev.Sidekicks.Help.chm";
            // 
            // SidekickViewHistoryContextMenuStrip
            // 
            this.SidekickViewHistoryContextMenuStrip.Name = "SidekickViewHistoryContextMenuStrip";
            this.SidekickViewHistoryContextMenuStrip.Size = new System.Drawing.Size(61, 4);
            // 
            // SidekickContainerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1228, 835);
            this.Controls.Add(this.SidekickContainerPanel);
            this.Controls.Add(this.HorizontalSplitter);
            this.Controls.Add(this.LogPanel);
            this.Controls.Add(this.SidekickContainerStatusStrip);
            this.Controls.Add(this.ToolStrip);
            this.Controls.Add(this.SidekickContainerMenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.SidekickContainerMenuStrip;
            this.MinimumSize = new System.Drawing.Size(16, 750);
            this.Name = "SidekickContainerForm";
            this.Text = "* - Aclara Vso LocalDev Sidekicks";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SidekickContainerForm_FormClosing);
            this.SidekickContainerMenuStrip.ResumeLayout(false);
            this.SidekickContainerMenuStrip.PerformLayout();
            this.ToolStrip.ResumeLayout(false);
            this.ToolStrip.PerformLayout();
            this.SidekickContainerStatusStrip.ResumeLayout(false);
            this.SidekickContainerStatusStrip.PerformLayout();
            this.LogPanel.ResumeLayout(false);
            this.LogPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip SidekickContainerMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExitSidekickContainerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ViewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SidekicksSidekickContainerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HelpToolStripMenuItem;
        private System.Windows.Forms.ToolStrip ToolStrip;
        private System.Windows.Forms.ToolStripButton ShowLogToolStripButton;
        private System.Windows.Forms.ToolStripButton HelpToolStripButton;
        private System.Windows.Forms.ToolStripButton AutoUpdaterToolStripButton;
        private System.Windows.Forms.ToolStripLabel AutoUpdaterToolStripLabel;
        private System.Windows.Forms.StatusStrip SidekickContainerStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel LoggedInAsToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel TeamProjectCollectionUriToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel TeamProjectNameToolStripStatusLabel;
        private System.Windows.Forms.Panel SidekickContainerPanel;
        private System.Windows.Forms.Splitter HorizontalSplitter;
        private System.Windows.Forms.Panel LogPanel;
        private System.Windows.Forms.Label OutputLabel;
        private System.Windows.Forms.ToolStripMenuItem AboutSidekickContainerToolStripMenuItem;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.ToolStripMenuItem HelpSidekickContainerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ChangeLogSidekickContainerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CheckForNewVersionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DocumentsSidekickContainerToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton ConnectTeamProjectToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem ShowLogFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton HomeToolStripButton;
        private System.Windows.Forms.ToolStripSplitButton MainSidekickSelectorToolStripSplitButton;
        private System.Windows.Forms.ContextMenuStrip MainSidekickSelectorContextMenuStrip;
        private System.Windows.Forms.ToolStripComboBox WorkspaceToolStripComboBox;
        private System.Windows.Forms.ToolStripButton ForwardToolStripButton;
        private System.Windows.Forms.ToolStripButton BackToolStripButton;
        private System.Windows.Forms.ContextMenuStrip SidekickViewHistoryContextMenuStrip;
        private System.Windows.Forms.ToolStripSplitButton VisualStudioOnlineToolStripSplitButton;
        private System.Windows.Forms.ToolStripMenuItem VisualStudioTeamServicesConnectedProjectOnlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem VisualStudioTeamServicesManageRepositoriesOnlineToolStripMenuItem;
    }
}

