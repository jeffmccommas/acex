﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Build solution list view interface.
    /// </summary>
    public interface IBuildSolutionListView
    {

        string BackgroundTaskStatus { get; set; }

        string ApplyButtonText { get; set; }

        void ApplyButtonEnable(bool enable);

        bool IsSidekickBusy { get; }

        void TeamProjectChanged();

        void UpdateSolutionBuildStatus(SolutionSelector solutionSelector);

        void ResetAllSolutionBuildStatus();

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);
    }
}
