﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using Aclara.TeamFoundation.VersionControl.Client;
using Aclara.Tools.Configuration;
using Aclara.Web.Administration.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Web app config search presenter.
    /// </summary>
    public class WebAppConfigSearchPresenter
    {

        #region Private Constants

        private const string TeamProjectCollectionNameDefaultValue = "";
        private const string TeamProjectNameDefaultValue = "";
        private const string SiteName_DefaultWebSiteName = "Default Web Site";

        //Regular expression replacement tokens.
        private const string RegEx_Replace_Pattern_WorkingFolder = @"\$\[WorkingFolder\]";
        private const string RegEx_Replace_Pattern_TeamProjectName = @"\$\[TeamProjectName\]";
        private const string RegEx_Replace_Pattern_BranchName = @"\$\[BranchName\]";

        #endregion

        #region Private Data Members

        private SidekickConfiguration _sidekickConfiguration; 
        private IWebAppConfigSearchView _webAppConfigSearchView = null;
        private WebAppManager _webAppManager = null;
        private ProjectManager _projectManager = null;
        private ITeamProjectInfo _teamProjectInfo;

        #endregion

        #region Public Types

        /// <summary>
        /// Web application level.
        /// </summary>
        public enum WepApplicationLevel
        {
            Unspecified = 0,
            SiteLevel = 1,
            ApplicationLevel = 2,
            All = 3
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Web application configuration search view.
        /// </summary>
        public IWebAppConfigSearchView WebAppConfigSearchView
        {
            get
            {
                return _webAppConfigSearchView;
            }
            private set
            {
                _webAppConfigSearchView = value;
            }
        }

        /// <summary>
        /// Property: Web application manager.
        /// </summary>
        public WebAppManager WebAppManager 
        {
            get { return _webAppManager; }
            set { _webAppManager = value; } 
        }

        /// <summary>
        /// Property: Project manager.
        /// </summary>
        public ProjectManager ProjectManager 
        { 
            get { return _projectManager; }
            set { _projectManager = value; } 
        }

        /// <summary>
        /// Property: Team project collection default.
        /// </summary>
        public string TeamProjectCollectionNameDefault
        {
            get{ return TeamProjectCollectionNameDefaultValue; }
        }

        /// <summary>
        /// Property: Team project name default.
        /// </summary>
        public string TeamProjectNameDefault
        {
            get { return TeamProjectNameDefaultValue; }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Site name default.
        /// </summary>
        public string SiteNameDefault 
        {
            get { return SiteName_DefaultWebSiteName; }
        }

        /// <summary>
        /// Populate branch name list.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        public void PopulateBrachNameList(string tfsServerAddress,
                                          string teamProjectName)
        {
            List<string> branchNameList = null;
            

            try
            {

                this.ProjectManager = CreateProjectManager(tfsServerAddress, teamProjectName);

                branchNameList = this.ProjectManager.GetTeamProjectYearMonthBranchNames();
                if (branchNameList == null)
                {
                    branchNameList = new List<string>();
                }

                this.WebAppConfigSearchView.PopulateBranchNameList(branchNameList);

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        public WebAppConfigSearchPresenter(SidekickConfiguration sidekickConfiguration,
                                           IWebAppConfigSearchView webAppConfigSearchView)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.WebAppConfigSearchView = webAppConfigSearchView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <remarks>
        /// Hide default constructor to enforce dependency injection.
        /// </remarks>
        private WebAppConfigSearchPresenter()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {
            try
            {
                this.WebAppConfigSearchView.TeamProjectCollectionUri = this.TeamProjectCollectionNameDefault;
                this.WebAppConfigSearchView.TeamProjectName = this.TeamProjectNameDefault;
                this.WebAppConfigSearchView.SiteName = this.SiteNameDefault;

                this.WebAppConfigSearchView.EnableControlsDependantOnTeamProject(false);
                this.WebAppConfigSearchView.EnableControlsDependantOnSearch(false);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Team project collection name changed.
        /// </summary>
        public void TeamProjectCollectionNameChanged()
        {

            try
            {
                if (this.WebAppConfigSearchView.TeamProjectCollectionUri != string.Empty &&
                    this.WebAppConfigSearchView.TeamProjectCollectionUri != TeamProjectCollectionNameDefault &&
                    this.WebAppConfigSearchView.TeamProjectName != string.Empty &&
                    this.WebAppConfigSearchView.TeamProjectName != TeamProjectNameDefault)
                {
                    this.WebAppConfigSearchView.EnableControlsDependantOnTeamProject(true);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Team project name changed.
        /// </summary>
        public void TeamProjectNameChanged()
        {

            try
            {
                if (this.WebAppConfigSearchView.TeamProjectCollectionUri != string.Empty &&
                    this.WebAppConfigSearchView.TeamProjectCollectionUri != TeamProjectCollectionNameDefault &&
                    this.WebAppConfigSearchView.TeamProjectName != string.Empty &&
                    this.WebAppConfigSearchView.TeamProjectName != TeamProjectNameDefault)
                {
                    this.WebAppConfigSearchView.EnableControlsDependantOnTeamProject(true);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve web application selector list.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="branchName"></param>
        /// <param name="siteName"></param>
        /// <param name="webApplicationLevel"></param>
        /// <returns></returns>
        public WebAppSelectorList GetWebAppSelectorList(string tfsServerAddress,
                                                        string teamProjectName,
                                                        string branchName,
                                                        string siteName, 
                                                        WepApplicationLevel webApplicationLevel)
        {
            const string Path_Root = @"/";

            WebAppSelectorList result = null;
            WebAppSelectorList webAppSelectorList = null;
            WebAppSelector noneWebAppSelector = null;
            try
            {

                webAppSelectorList = this.GetWebAppSelectorList(tfsServerAddress, teamProjectName, branchName, siteName);

                switch (webApplicationLevel)
                {

                    case WepApplicationLevel.All:
                        result = webAppSelectorList; 
                        break;

                    case WepApplicationLevel.SiteLevel:

                        result = new WebAppSelectorList();

                        noneWebAppSelector = new WebAppSelector();
                        noneWebAppSelector.Selected = true;
                        noneWebAppSelector.Path = "None";
                        noneWebAppSelector.PhysicalPathActual = "**";
                        result.Add(noneWebAppSelector);

                        foreach(WebAppSelector webAppSelector in webAppSelectorList)
                        {
                            if (webAppSelector.Path == Path_Root)
                            {
                                result.Add(webAppSelector);
                            }
                        }
                        
                        break;

                    case WepApplicationLevel.ApplicationLevel:

                        result = new WebAppSelectorList();

                        foreach(WebAppSelector webAppSelector in webAppSelectorList)
                        {
                            if (webAppSelector.Path != Path_Root)
                            {
                                result.Add(webAppSelector);
                            }
                        }
                        break;

                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected web application level. (WebApplicationLevel: {0})",
                                                                            webApplicationLevel.ToString()));
                }


            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Retrieve web application selector list.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="branchName"></param>
        /// <param name="siteName"></param>
        /// <returns></returns>
        public WebAppSelectorList GetWebAppSelectorList(string tfsServerAddress,
                                                        string teamProjectName,
                                                        string branchName,
                                                        string siteName)
        {
            WebAppSelectorList result = null;
            WebAppSelector webAppSelector = null;
            WebAppConfigManager webAppConfigManager = null;
            WebAppConfigManagerFactory webAppConfigManagerFactory = null;
            WebAppConfigList webAppConfigList = null;
            string configurationFileName = string.Empty;
            WebApp webApp = null;
            bool existsInIIS = false;
            string teamProjectWorkingFolder = string.Empty;
            try
            {

                result = new WebAppSelectorList();

                webAppConfigManagerFactory = new WebAppConfigManagerFactory();
                webAppConfigManager = webAppConfigManagerFactory.CreateWebAppConfigManager();

                this.WebAppManager = this.CreateWebAppManager();
                this.ProjectManager = this.CreateProjectManager(tfsServerAddress, teamProjectName);

                teamProjectWorkingFolder = this.ProjectManager.GetTeamProjectWorkingFolder(this.TeamProjectInfo.TeamProjectName,
                                                                                           this.TeamProjectInfo.WorkspaceName, 
                                                                                           branchName, 
                                                                                           this.TeamProjectInfo.BasicAuthRestAPIUserProfileName);

                configurationFileName = GetWebAppConfigFilePath(teamProjectName,
                                                                branchName);

                if (File.Exists(configurationFileName) == false)
                {
                    throw new FileNotFoundException(string.Format("Web Application Configuration file not found. (ConfigurationFileName: {0})",
                                                                  configurationFileName));
                }

                webAppConfigList = webAppConfigManager.GetWebAppConfigList(configurationFileName, siteName);

                foreach (WebAppConfig webAppConfig in webAppConfigList)
                {
                    webAppSelector = new WebAppSelector();

                    webApp = new WebApp();

                    webApp.SiteName = siteName;
                    webApp.Path = webAppConfig.Path;

                    existsInIIS = this.WebAppManager.ExistsInIIS(webApp);

                    webAppSelector.Selected = false;
                    webAppSelector.SiteName = siteName;
                    webAppSelector.Alias = webAppConfig.Alias;
                    webAppSelector.ApplicationPoolName = webAppConfig.ApplicationPoolName;
                    webAppSelector.Path = webAppConfig.Path;
                    webAppSelector.VirtualDirectoryList = this.ReplaceTokenInVirtualDirectoryList(webAppConfig.VirtualDirectoryConfigList, 
                                                                                                  teamProjectWorkingFolder, 
                                                                                                  teamProjectName,
                                                                                                  branchName);
                    webAppSelector.ExistsInIIS = existsInIIS;
                    webAppSelector.PhysicalPathActual = this.WebAppManager.GetAppPhysicalPath(webApp);

                    result.Add(webAppSelector);
                }

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Retrieve web application configuration file path.
        /// </summary>
        /// <param name="teamProjectWorkingFolder"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        public string GetWebAppConfigFilePath(string teamProjectName,
                                              string branchName)
        {
            string result = string.Empty;
            string teamProjectWorkingFolder = string.Empty;

            try
            {
                teamProjectWorkingFolder = this.ProjectManager.GetTeamProjectWorkingFolder(this.TeamProjectInfo.TeamProjectName,
                                                                                           this.TeamProjectInfo.WorkspaceName, 
                                                                                           branchName,
                                                                                           this.TeamProjectInfo.BasicAuthRestAPIUserProfileName);

                result = this.SidekickConfiguration.GetWebAppConfigFilePath(teamProjectWorkingFolder, teamProjectName, branchName);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Determine whether team project is mapped to local folder.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        [Obsolete("Remove as mehtod is not used.")]
        public bool IsTeamProjectMapped(string tfsServerAddress,
                                        string teamProjectName)
        {
            bool result = false;

            try
            {
                this.ProjectManager = CreateProjectManager(tfsServerAddress, teamProjectName);

                result = this.ProjectManager.IsTeamProjectMappedToLocalFolder(this.TeamProjectInfo.TeamProjectName,
                                                                              this.TeamProjectInfo.WorkspaceName,
                                                                              this.TeamProjectInfo.BasicAuthRestAPIUserProfileName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create web application manager.
        /// </summary>
        /// <returns></returns>
        protected WebAppManager CreateWebAppManager()
        {
            WebAppManager result = null;
            WebAppManagerFactory webAppManagerFactory = null;

            try
            {

                webAppManagerFactory = new WebAppManagerFactory();

                result = webAppManagerFactory.CreateWebAppManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create project manager.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        protected ProjectManager CreateProjectManager(string tfsServerAddress,
                                                      string teamProjectName)
        {
            ProjectManager result = null;
            Uri tfsServerUri = null;

            try
            {

                tfsServerUri = new Uri(tfsServerAddress);

                result = ProjectManagerFactory.CreateProjectManager(tfsServerUri, teamProjectName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Replace tokens in virtual directory list.
        /// </summary>
        /// <param name="sourceVirtualDirectoryConfigList"></param>
        /// <param name="workingFolder"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        protected VirtualDirectoryList ReplaceTokenInVirtualDirectoryList(VirtualDirectoryConfigList sourceVirtualDirectoryConfigList, 
                                                                          string workingFolder, 
                                                                          string teamProjectName,
                                                                          string branchName)
        {
            VirtualDirectoryList result = null;
            VirtualDirectory virtualDirectory = null;

            try
            {
                result = new VirtualDirectoryList();

                foreach (VirtualDirectoryConfig sourceVirtualDirectoryConfig in sourceVirtualDirectoryConfigList)
                {
                    virtualDirectory = new VirtualDirectory();

                    virtualDirectory.Path = sourceVirtualDirectoryConfig.Path;
                    virtualDirectory.PhysicalPath = sourceVirtualDirectoryConfig.PhysicalPath;
                    virtualDirectory.PhysicalPath = this.ReplaceTokenInTextWithValue(virtualDirectory.PhysicalPath, RegEx_Replace_Pattern_WorkingFolder, workingFolder);
                    virtualDirectory.PhysicalPath = this.ReplaceTokenInTextWithValue(virtualDirectory.PhysicalPath, RegEx_Replace_Pattern_TeamProjectName, teamProjectName);
                    virtualDirectory.PhysicalPath = this.ReplaceTokenInTextWithValue(virtualDirectory.PhysicalPath, RegEx_Replace_Pattern_BranchName, branchName);

                    result.Add(virtualDirectory);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Replace token in text with specified value.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="token"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string ReplaceTokenInTextWithValue(string text, string token, string value)
        {
            string result = string.Empty;

            result = Regex.Replace(text, token, value);

            return result;
        }


        #endregion

    }
}
