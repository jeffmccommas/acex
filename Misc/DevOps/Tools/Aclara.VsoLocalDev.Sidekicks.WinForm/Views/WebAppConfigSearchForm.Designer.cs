﻿namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    partial class WebAppConfigSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WebAppConfigSearchForm));
            this.ApplicationsGroupBox = new System.Windows.Forms.GroupBox();
            this.ClearButton = new System.Windows.Forms.Button();
            this.WebAppPathPartialTextBox = new System.Windows.Forms.TextBox();
            this.WebAppPathFilterLabel = new System.Windows.Forms.Label();
            this.SearchButton = new System.Windows.Forms.Button();
            this.WebAppSelectContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SelectAllToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SelectNoneToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WebAppDisplayContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.DisplayAllToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DisplaySelectedToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WebAppDataGridView = new System.Windows.Forms.DataGridView();
            this.WebAppSelectColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.WebAppExistsInIISColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.WebAppPathColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WebAppPhysicalPathColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SelectContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SiteNameLabel = new System.Windows.Forms.Label();
            this.OpenContainingFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowTeamProjectPickerToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.ShowTeamProjectPickerButton = new System.Windows.Forms.Button();
            this.DisplayWebAppConfigFileToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.WebAppConfigContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.OpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SiteNameValueLabel = new System.Windows.Forms.Label();
            this.TeamProjectGroupBox = new System.Windows.Forms.GroupBox();
            this.TeamProjectCollectionNameSelectedLabel = new System.Windows.Forms.Label();
            this.TeamProjectCollectionLabel = new System.Windows.Forms.Label();
            this.TeamProjectNameSelectedLabel = new System.Windows.Forms.Label();
            this.TeamProjectNameLabel = new System.Windows.Forms.Label();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.BranchComboBox = new System.Windows.Forms.ComboBox();
            this.BranchLabel = new System.Windows.Forms.Label();
            this.SelectSplitButton = new Aclara.VsoLocalDev.Sidekicks.WinForm.SplitButton();
            this.DisplaySplitButton = new Aclara.VsoLocalDev.Sidekicks.WinForm.SplitButton();
            this.DisplayWebAppConfigFileButton = new Aclara.VsoLocalDev.Sidekicks.WinForm.SplitButton();
            this.ApplicationsGroupBox.SuspendLayout();
            this.WebAppSelectContextMenuStrip.SuspendLayout();
            this.WebAppDisplayContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WebAppDataGridView)).BeginInit();
            this.SelectContextMenuStrip.SuspendLayout();
            this.WebAppConfigContextMenuStrip.SuspendLayout();
            this.TeamProjectGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ApplicationsGroupBox
            // 
            this.ApplicationsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplicationsGroupBox.Controls.Add(this.ClearButton);
            this.ApplicationsGroupBox.Controls.Add(this.WebAppPathPartialTextBox);
            this.ApplicationsGroupBox.Controls.Add(this.WebAppPathFilterLabel);
            this.ApplicationsGroupBox.Controls.Add(this.SearchButton);
            this.ApplicationsGroupBox.Controls.Add(this.SelectSplitButton);
            this.ApplicationsGroupBox.Controls.Add(this.DisplaySplitButton);
            this.ApplicationsGroupBox.Controls.Add(this.WebAppDataGridView);
            this.ApplicationsGroupBox.Location = new System.Drawing.Point(12, 126);
            this.ApplicationsGroupBox.Name = "ApplicationsGroupBox";
            this.ApplicationsGroupBox.Size = new System.Drawing.Size(667, 357);
            this.ApplicationsGroupBox.TabIndex = 15;
            this.ApplicationsGroupBox.TabStop = false;
            this.ApplicationsGroupBox.Text = "Applications";
            // 
            // ClearButton
            // 
            this.ClearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ClearButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ClearButton.Location = new System.Drawing.Point(577, 17);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 3;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            // 
            // WebAppPathPartialTextBox
            // 
            this.WebAppPathPartialTextBox.AcceptsReturn = true;
            this.WebAppPathPartialTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WebAppPathPartialTextBox.Location = new System.Drawing.Point(73, 19);
            this.WebAppPathPartialTextBox.Name = "WebAppPathPartialTextBox";
            this.WebAppPathPartialTextBox.Size = new System.Drawing.Size(417, 20);
            this.WebAppPathPartialTextBox.TabIndex = 1;
            // 
            // WebAppPathFilterLabel
            // 
            this.WebAppPathFilterLabel.AutoSize = true;
            this.WebAppPathFilterLabel.Location = new System.Drawing.Point(7, 22);
            this.WebAppPathFilterLabel.Name = "WebAppPathFilterLabel";
            this.WebAppPathFilterLabel.Size = new System.Drawing.Size(60, 13);
            this.WebAppPathFilterLabel.TabIndex = 0;
            this.WebAppPathFilterLabel.Text = "Path (filter):";
            // 
            // SearchButton
            // 
            this.SearchButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchButton.Location = new System.Drawing.Point(496, 17);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(75, 23);
            this.SearchButton.TabIndex = 2;
            this.SearchButton.Text = "Search";
            this.SearchButton.UseVisualStyleBackColor = true;
            // 
            // WebAppSelectContextMenuStrip
            // 
            this.WebAppSelectContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SelectAllToolStripItem,
            this.SelectNoneToolStripItem});
            this.WebAppSelectContextMenuStrip.Name = "BuildDefinitionSelectContextMenuStrip";
            this.WebAppSelectContextMenuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.WebAppSelectContextMenuStrip.Size = new System.Drawing.Size(104, 48);
            // 
            // SelectAllToolStripItem
            // 
            this.SelectAllToolStripItem.Name = "SelectAllToolStripItem";
            this.SelectAllToolStripItem.Size = new System.Drawing.Size(103, 22);
            this.SelectAllToolStripItem.Text = "All";
            // 
            // SelectNoneToolStripItem
            // 
            this.SelectNoneToolStripItem.Name = "SelectNoneToolStripItem";
            this.SelectNoneToolStripItem.Size = new System.Drawing.Size(103, 22);
            this.SelectNoneToolStripItem.Text = "None";
            // 
            // WebAppDisplayContextMenuStrip
            // 
            this.WebAppDisplayContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DisplayAllToolStripItem,
            this.DisplaySelectedToolStripItem});
            this.WebAppDisplayContextMenuStrip.Name = "BuildDefinitionDisplayContextMenuStrip";
            this.WebAppDisplayContextMenuStrip.Size = new System.Drawing.Size(119, 48);
            // 
            // DisplayAllToolStripItem
            // 
            this.DisplayAllToolStripItem.Name = "DisplayAllToolStripItem";
            this.DisplayAllToolStripItem.Size = new System.Drawing.Size(118, 22);
            this.DisplayAllToolStripItem.Text = "All";
            // 
            // DisplaySelectedToolStripItem
            // 
            this.DisplaySelectedToolStripItem.Name = "DisplaySelectedToolStripItem";
            this.DisplaySelectedToolStripItem.Size = new System.Drawing.Size(118, 22);
            this.DisplaySelectedToolStripItem.Text = "Selected";
            // 
            // WebAppDataGridView
            // 
            this.WebAppDataGridView.AllowUserToAddRows = false;
            this.WebAppDataGridView.AllowUserToDeleteRows = false;
            this.WebAppDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WebAppDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.WebAppDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.WebAppDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.WebAppDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.WebAppDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WebAppSelectColumn,
            this.WebAppExistsInIISColumn,
            this.WebAppPathColumn,
            this.WebAppPhysicalPathColumn});
            this.WebAppDataGridView.ContextMenuStrip = this.SelectContextMenuStrip;
            this.WebAppDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.WebAppDataGridView.Location = new System.Drawing.Point(6, 74);
            this.WebAppDataGridView.MultiSelect = false;
            this.WebAppDataGridView.Name = "WebAppDataGridView";
            this.WebAppDataGridView.RowHeadersVisible = false;
            this.WebAppDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.WebAppDataGridView.Size = new System.Drawing.Size(655, 277);
            this.WebAppDataGridView.TabIndex = 6;
            // 
            // WebAppSelectColumn
            // 
            this.WebAppSelectColumn.HeaderText = "";
            this.WebAppSelectColumn.MinimumWidth = 20;
            this.WebAppSelectColumn.Name = "WebAppSelectColumn";
            this.WebAppSelectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.WebAppSelectColumn.Width = 20;
            // 
            // WebAppExistsInIISColumn
            // 
            this.WebAppExistsInIISColumn.HeaderText = "";
            this.WebAppExistsInIISColumn.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.ExistsInIISTrue;
            this.WebAppExistsInIISColumn.MinimumWidth = 22;
            this.WebAppExistsInIISColumn.Name = "WebAppExistsInIISColumn";
            this.WebAppExistsInIISColumn.ReadOnly = true;
            this.WebAppExistsInIISColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.WebAppExistsInIISColumn.Width = 22;
            // 
            // WebAppPathColumn
            // 
            this.WebAppPathColumn.HeaderText = "Path";
            this.WebAppPathColumn.MinimumWidth = 100;
            this.WebAppPathColumn.Name = "WebAppPathColumn";
            this.WebAppPathColumn.ReadOnly = true;
            this.WebAppPathColumn.Width = 250;
            // 
            // WebAppPhysicalPathColumn
            // 
            this.WebAppPhysicalPathColumn.HeaderText = "Physical Path";
            this.WebAppPhysicalPathColumn.MinimumWidth = 100;
            this.WebAppPhysicalPathColumn.Name = "WebAppPhysicalPathColumn";
            this.WebAppPhysicalPathColumn.ReadOnly = true;
            this.WebAppPhysicalPathColumn.Width = 484;
            // 
            // SelectContextMenuStrip
            // 
            this.SelectContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AllToolStripMenuItem,
            this.NoneToolStripMenuItem});
            this.SelectContextMenuStrip.Name = "SelectContextMenuStrip";
            this.SelectContextMenuStrip.Size = new System.Drawing.Size(104, 48);
            // 
            // AllToolStripMenuItem
            // 
            this.AllToolStripMenuItem.Name = "AllToolStripMenuItem";
            this.AllToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.AllToolStripMenuItem.Text = "All";
            // 
            // NoneToolStripMenuItem
            // 
            this.NoneToolStripMenuItem.Name = "NoneToolStripMenuItem";
            this.NoneToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.NoneToolStripMenuItem.Text = "None";
            // 
            // SiteNameLabel
            // 
            this.SiteNameLabel.AutoSize = true;
            this.SiteNameLabel.Location = new System.Drawing.Point(191, 95);
            this.SiteNameLabel.Name = "SiteNameLabel";
            this.SiteNameLabel.Size = new System.Drawing.Size(57, 13);
            this.SiteNameLabel.TabIndex = 11;
            this.SiteNameLabel.Text = "Site name:";
            // 
            // OpenContainingFolderToolStripMenuItem
            // 
            this.OpenContainingFolderToolStripMenuItem.Name = "OpenContainingFolderToolStripMenuItem";
            this.OpenContainingFolderToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.OpenContainingFolderToolStripMenuItem.Text = "Open Containing Folder";
            // 
            // ShowTeamProjectPickerToolTip
            // 
            this.ShowTeamProjectPickerToolTip.ToolTipTitle = "Pick team project";
            // 
            // ShowTeamProjectPickerButton
            // 
            this.ShowTeamProjectPickerButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ShowTeamProjectPickerButton.AutoSize = true;
            this.ShowTeamProjectPickerButton.Image = ((System.Drawing.Image)(resources.GetObject("ShowTeamProjectPickerButton.Image")));
            this.ShowTeamProjectPickerButton.Location = new System.Drawing.Point(625, 25);
            this.ShowTeamProjectPickerButton.Name = "ShowTeamProjectPickerButton";
            this.ShowTeamProjectPickerButton.Size = new System.Drawing.Size(36, 23);
            this.ShowTeamProjectPickerButton.TabIndex = 2;
            this.ShowTeamProjectPickerButton.Text = "...";
            this.ShowTeamProjectPickerToolTip.SetToolTip(this.ShowTeamProjectPickerButton, "Click Team Project Picker to select team project.");
            this.ShowTeamProjectPickerButton.UseVisualStyleBackColor = true;
            // 
            // DisplayWebAppConfigFileToolTip
            // 
            this.DisplayWebAppConfigFileToolTip.ToolTipTitle = "Web Application Configuration";
            // 
            // WebAppConfigContextMenuStrip
            // 
            this.WebAppConfigContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenToolStripMenuItem,
            this.OpenContainingFolderToolStripMenuItem});
            this.WebAppConfigContextMenuStrip.Name = "WebAppConfigContextMenuStrip";
            this.WebAppConfigContextMenuStrip.Size = new System.Drawing.Size(202, 48);
            // 
            // OpenToolStripMenuItem
            // 
            this.OpenToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem";
            this.OpenToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.OpenToolStripMenuItem.Text = "Open";
            // 
            // SiteNameValueLabel
            // 
            this.SiteNameValueLabel.AutoSize = true;
            this.SiteNameValueLabel.Location = new System.Drawing.Point(254, 95);
            this.SiteNameValueLabel.Name = "SiteNameValueLabel";
            this.SiteNameValueLabel.Size = new System.Drawing.Size(93, 13);
            this.SiteNameValueLabel.TabIndex = 12;
            this.SiteNameValueLabel.Text = "<site name value>";
            // 
            // TeamProjectGroupBox
            // 
            this.TeamProjectGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TeamProjectGroupBox.Controls.Add(this.TeamProjectCollectionNameSelectedLabel);
            this.TeamProjectGroupBox.Controls.Add(this.TeamProjectCollectionLabel);
            this.TeamProjectGroupBox.Controls.Add(this.TeamProjectNameSelectedLabel);
            this.TeamProjectGroupBox.Controls.Add(this.ShowTeamProjectPickerButton);
            this.TeamProjectGroupBox.Controls.Add(this.TeamProjectNameLabel);
            this.TeamProjectGroupBox.Location = new System.Drawing.Point(12, 12);
            this.TeamProjectGroupBox.Name = "TeamProjectGroupBox";
            this.TeamProjectGroupBox.Size = new System.Drawing.Size(667, 74);
            this.TeamProjectGroupBox.TabIndex = 8;
            this.TeamProjectGroupBox.TabStop = false;
            this.TeamProjectGroupBox.Text = "Team project";
            // 
            // TeamProjectCollectionNameSelectedLabel
            // 
            this.TeamProjectCollectionNameSelectedLabel.AutoSize = true;
            this.TeamProjectCollectionNameSelectedLabel.Location = new System.Drawing.Point(170, 25);
            this.TeamProjectCollectionNameSelectedLabel.Name = "TeamProjectCollectionNameSelectedLabel";
            this.TeamProjectCollectionNameSelectedLabel.Size = new System.Drawing.Size(0, 13);
            this.TeamProjectCollectionNameSelectedLabel.TabIndex = 1;
            // 
            // TeamProjectCollectionLabel
            // 
            this.TeamProjectCollectionLabel.AutoSize = true;
            this.TeamProjectCollectionLabel.Location = new System.Drawing.Point(6, 25);
            this.TeamProjectCollectionLabel.Name = "TeamProjectCollectionLabel";
            this.TeamProjectCollectionLabel.Size = new System.Drawing.Size(149, 13);
            this.TeamProjectCollectionLabel.TabIndex = 0;
            this.TeamProjectCollectionLabel.Text = "Team project collection name:";
            // 
            // TeamProjectNameSelectedLabel
            // 
            this.TeamProjectNameSelectedLabel.AutoSize = true;
            this.TeamProjectNameSelectedLabel.Location = new System.Drawing.Point(170, 49);
            this.TeamProjectNameSelectedLabel.Name = "TeamProjectNameSelectedLabel";
            this.TeamProjectNameSelectedLabel.Size = new System.Drawing.Size(0, 13);
            this.TeamProjectNameSelectedLabel.TabIndex = 4;
            // 
            // TeamProjectNameLabel
            // 
            this.TeamProjectNameLabel.AutoSize = true;
            this.TeamProjectNameLabel.Location = new System.Drawing.Point(6, 49);
            this.TeamProjectNameLabel.Name = "TeamProjectNameLabel";
            this.TeamProjectNameLabel.Size = new System.Drawing.Size(101, 13);
            this.TeamProjectNameLabel.TabIndex = 3;
            this.TeamProjectNameLabel.Text = "Team project name:";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.OK;
            this.dataGridViewImageColumn1.MinimumWidth = 22;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.ReadOnly = true;
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewImageColumn1.Width = 22;
            // 
            // BranchComboBox
            // 
            this.BranchComboBox.FormattingEnabled = true;
            this.BranchComboBox.Location = new System.Drawing.Point(64, 92);
            this.BranchComboBox.Name = "BranchComboBox";
            this.BranchComboBox.Size = new System.Drawing.Size(121, 21);
            this.BranchComboBox.TabIndex = 10;
            // 
            // BranchLabel
            // 
            this.BranchLabel.AutoSize = true;
            this.BranchLabel.Location = new System.Drawing.Point(14, 95);
            this.BranchLabel.Name = "BranchLabel";
            this.BranchLabel.Size = new System.Drawing.Size(44, 13);
            this.BranchLabel.TabIndex = 9;
            this.BranchLabel.Text = "Branch:";
            // 
            // SelectSplitButton
            // 
            this.SelectSplitButton.AutoSize = true;
            this.SelectSplitButton.ContextMenuStrip = this.WebAppSelectContextMenuStrip;
            this.SelectSplitButton.Location = new System.Drawing.Point(10, 45);
            this.SelectSplitButton.Name = "SelectSplitButton";
            this.SelectSplitButton.Size = new System.Drawing.Size(75, 23);
            this.SelectSplitButton.TabIndex = 4;
            this.SelectSplitButton.Text = "Select";
            this.SelectSplitButton.UseVisualStyleBackColor = true;
            // 
            // DisplaySplitButton
            // 
            this.DisplaySplitButton.AutoSize = true;
            this.DisplaySplitButton.ContextMenuStrip = this.WebAppDisplayContextMenuStrip;
            this.DisplaySplitButton.Location = new System.Drawing.Point(91, 45);
            this.DisplaySplitButton.Name = "DisplaySplitButton";
            this.DisplaySplitButton.Size = new System.Drawing.Size(75, 23);
            this.DisplaySplitButton.TabIndex = 5;
            this.DisplaySplitButton.Text = "Display";
            this.DisplaySplitButton.UseVisualStyleBackColor = true;
            // 
            // DisplayWebAppConfigFileButton
            // 
            this.DisplayWebAppConfigFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DisplayWebAppConfigFileButton.AutoSize = true;
            this.DisplayWebAppConfigFileButton.ContextMenuStrip = this.WebAppConfigContextMenuStrip;
            this.DisplayWebAppConfigFileButton.Location = new System.Drawing.Point(560, 95);
            this.DisplayWebAppConfigFileButton.Name = "DisplayWebAppConfigFileButton";
            this.DisplayWebAppConfigFileButton.Size = new System.Drawing.Size(119, 25);
            this.DisplayWebAppConfigFileButton.TabIndex = 13;
            this.DisplayWebAppConfigFileButton.Text = "Web App Config";
            this.DisplayWebAppConfigFileButton.UseVisualStyleBackColor = true;
            // 
            // WebAppConfigSearchForm
            // 
            this.AcceptButton = this.SearchButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 495);
            this.ControlBox = false;
            this.Controls.Add(this.ApplicationsGroupBox);
            this.Controls.Add(this.SiteNameLabel);
            this.Controls.Add(this.DisplayWebAppConfigFileButton);
            this.Controls.Add(this.SiteNameValueLabel);
            this.Controls.Add(this.TeamProjectGroupBox);
            this.Controls.Add(this.BranchComboBox);
            this.Controls.Add(this.BranchLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WebAppConfigSearchForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "WebAppConfigSearchForm";
            this.ApplicationsGroupBox.ResumeLayout(false);
            this.ApplicationsGroupBox.PerformLayout();
            this.WebAppSelectContextMenuStrip.ResumeLayout(false);
            this.WebAppDisplayContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WebAppDataGridView)).EndInit();
            this.SelectContextMenuStrip.ResumeLayout(false);
            this.WebAppConfigContextMenuStrip.ResumeLayout(false);
            this.TeamProjectGroupBox.ResumeLayout(false);
            this.TeamProjectGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox ApplicationsGroupBox;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.TextBox WebAppPathPartialTextBox;
        private System.Windows.Forms.Label WebAppPathFilterLabel;
        private System.Windows.Forms.Button SearchButton;
        private SplitButton SelectSplitButton;
        private System.Windows.Forms.ContextMenuStrip WebAppSelectContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem SelectAllToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem SelectNoneToolStripItem;
        private SplitButton DisplaySplitButton;
        private System.Windows.Forms.ContextMenuStrip WebAppDisplayContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem DisplayAllToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem DisplaySelectedToolStripItem;
        private System.Windows.Forms.DataGridView WebAppDataGridView;
        private System.Windows.Forms.DataGridViewCheckBoxColumn WebAppSelectColumn;
        private System.Windows.Forms.DataGridViewImageColumn WebAppExistsInIISColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn WebAppPathColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn WebAppPhysicalPathColumn;
        private System.Windows.Forms.ContextMenuStrip SelectContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem AllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem NoneToolStripMenuItem;
        private System.Windows.Forms.Label SiteNameLabel;
        private System.Windows.Forms.ToolStripMenuItem OpenContainingFolderToolStripMenuItem;
        private System.Windows.Forms.ToolTip ShowTeamProjectPickerToolTip;
        private System.Windows.Forms.Button ShowTeamProjectPickerButton;
        private System.Windows.Forms.ToolTip DisplayWebAppConfigFileToolTip;
        private System.Windows.Forms.ContextMenuStrip WebAppConfigContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem OpenToolStripMenuItem;
        private SplitButton DisplayWebAppConfigFileButton;
        private System.Windows.Forms.Label SiteNameValueLabel;
        private System.Windows.Forms.GroupBox TeamProjectGroupBox;
        private System.Windows.Forms.Label TeamProjectCollectionNameSelectedLabel;
        private System.Windows.Forms.Label TeamProjectCollectionLabel;
        private System.Windows.Forms.Label TeamProjectNameSelectedLabel;
        private System.Windows.Forms.Label TeamProjectNameLabel;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.ComboBox BranchComboBox;
        private System.Windows.Forms.Label BranchLabel;
    }
}