﻿namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    partial class SolutionListSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BranchComboBox = new System.Windows.Forms.ComboBox();
            this.BranchLabel = new System.Windows.Forms.Label();
            this.DisplaySolutionListConfigFileMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.OpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenContainingFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SolutionSelectContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SolutionSelectAllToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SolutionSelectNoneToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToggleExpandMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ExpandAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CollapseAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SolutionListsGroupBox = new System.Windows.Forms.GroupBox();
            this.OpenSplitButton = new Aclara.VsoLocalDev.Sidekicks.WinForm.SplitButton();
            this.SolutionOpenContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SolutionOpenVisualStudioMenuStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.SolutionOpenContainingFolderMenuStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.ToggleExpandSplitButton = new Aclara.VsoLocalDev.Sidekicks.WinForm.SplitButton();
            this.SelectSplitButton = new Aclara.VsoLocalDev.Sidekicks.WinForm.SplitButton();
            this.SolutionListTreeView = new System.Windows.Forms.TreeView();
            this.SolutionContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SolutionOpenWithVisualStudioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SolutionOpenContainingFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DisplaySolutionListConfigFileSplitButton = new Aclara.VsoLocalDev.Sidekicks.WinForm.SplitButton();
            this.DisplaySolutionListConfigFileMenuStrip.SuspendLayout();
            this.SolutionSelectContextMenuStrip.SuspendLayout();
            this.ToggleExpandMenuStrip.SuspendLayout();
            this.SolutionListsGroupBox.SuspendLayout();
            this.SolutionOpenContextMenuStrip.SuspendLayout();
            this.SolutionContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // BranchComboBox
            // 
            this.BranchComboBox.FormattingEnabled = true;
            this.BranchComboBox.Location = new System.Drawing.Point(50, 5);
            this.BranchComboBox.Name = "BranchComboBox";
            this.BranchComboBox.Size = new System.Drawing.Size(121, 21);
            this.BranchComboBox.TabIndex = 2;
            this.BranchComboBox.TextChanged += new System.EventHandler(this.BranchComboBox_TextChanged);
            // 
            // BranchLabel
            // 
            this.BranchLabel.AutoSize = true;
            this.BranchLabel.Location = new System.Drawing.Point(0, 9);
            this.BranchLabel.Name = "BranchLabel";
            this.BranchLabel.Size = new System.Drawing.Size(44, 13);
            this.BranchLabel.TabIndex = 1;
            this.BranchLabel.Text = "Branch:";
            // 
            // DisplaySolutionListConfigFileMenuStrip
            // 
            this.DisplaySolutionListConfigFileMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenToolStripMenuItem,
            this.OpenContainingFolderToolStripMenuItem});
            this.DisplaySolutionListConfigFileMenuStrip.Name = "DisplaySolutionListConfigFileMenuStrip";
            this.DisplaySolutionListConfigFileMenuStrip.Size = new System.Drawing.Size(202, 48);
            // 
            // OpenToolStripMenuItem
            // 
            this.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem";
            this.OpenToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.OpenToolStripMenuItem.Text = "Open";
            this.OpenToolStripMenuItem.Click += new System.EventHandler(this.OpenToolStripMenuItem_Click);
            // 
            // OpenContainingFolderToolStripMenuItem
            // 
            this.OpenContainingFolderToolStripMenuItem.Name = "OpenContainingFolderToolStripMenuItem";
            this.OpenContainingFolderToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.OpenContainingFolderToolStripMenuItem.Text = "Open Containing Folder";
            this.OpenContainingFolderToolStripMenuItem.Click += new System.EventHandler(this.OpenContainingFolderToolStripMenuItem_Click);
            // 
            // SolutionSelectContextMenuStrip
            // 
            this.SolutionSelectContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SolutionSelectAllToolStripItem,
            this.SolutionSelectNoneToolStripItem});
            this.SolutionSelectContextMenuStrip.Name = "SolutionSelectContextMenuStrip";
            this.SolutionSelectContextMenuStrip.Size = new System.Drawing.Size(104, 48);
            // 
            // SolutionSelectAllToolStripItem
            // 
            this.SolutionSelectAllToolStripItem.Name = "SolutionSelectAllToolStripItem";
            this.SolutionSelectAllToolStripItem.Size = new System.Drawing.Size(103, 22);
            this.SolutionSelectAllToolStripItem.Text = "All";
            this.SolutionSelectAllToolStripItem.Click += new System.EventHandler(this.SolutionSelectAllToolStripItem_Click);
            // 
            // SolutionSelectNoneToolStripItem
            // 
            this.SolutionSelectNoneToolStripItem.Name = "SolutionSelectNoneToolStripItem";
            this.SolutionSelectNoneToolStripItem.Size = new System.Drawing.Size(103, 22);
            this.SolutionSelectNoneToolStripItem.Text = "None";
            this.SolutionSelectNoneToolStripItem.Click += new System.EventHandler(this.SolutionSelectNoneToolStripItem_Click);
            // 
            // ToggleExpandMenuStrip
            // 
            this.ToggleExpandMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExpandAllToolStripMenuItem,
            this.CollapseAllToolStripMenuItem});
            this.ToggleExpandMenuStrip.Name = "ToggleExpandMenuStrip";
            this.ToggleExpandMenuStrip.Size = new System.Drawing.Size(137, 48);
            // 
            // ExpandAllToolStripMenuItem
            // 
            this.ExpandAllToolStripMenuItem.Name = "ExpandAllToolStripMenuItem";
            this.ExpandAllToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.ExpandAllToolStripMenuItem.Text = "Expand All";
            this.ExpandAllToolStripMenuItem.Click += new System.EventHandler(this.ExpandAllToolStripMenuItem_Click);
            // 
            // CollapseAllToolStripMenuItem
            // 
            this.CollapseAllToolStripMenuItem.Name = "CollapseAllToolStripMenuItem";
            this.CollapseAllToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.CollapseAllToolStripMenuItem.Text = "Collapse All";
            this.CollapseAllToolStripMenuItem.Click += new System.EventHandler(this.CollapseAllToolStripMenuItem_Click);
            // 
            // SolutionListsGroupBox
            // 
            this.SolutionListsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SolutionListsGroupBox.Controls.Add(this.OpenSplitButton);
            this.SolutionListsGroupBox.Controls.Add(this.ToggleExpandSplitButton);
            this.SolutionListsGroupBox.Controls.Add(this.SelectSplitButton);
            this.SolutionListsGroupBox.Controls.Add(this.SolutionListTreeView);
            this.SolutionListsGroupBox.Location = new System.Drawing.Point(3, 33);
            this.SolutionListsGroupBox.Name = "SolutionListsGroupBox";
            this.SolutionListsGroupBox.Size = new System.Drawing.Size(685, 455);
            this.SolutionListsGroupBox.TabIndex = 4;
            this.SolutionListsGroupBox.TabStop = false;
            this.SolutionListsGroupBox.Text = "Solution lists";
            // 
            // OpenSplitButton
            // 
            this.OpenSplitButton.AutoSize = true;
            this.OpenSplitButton.ContextMenuStrip = this.SolutionOpenContextMenuStrip;
            this.OpenSplitButton.Enabled = false;
            this.OpenSplitButton.Location = new System.Drawing.Point(171, 19);
            this.OpenSplitButton.Name = "OpenSplitButton";
            this.OpenSplitButton.Size = new System.Drawing.Size(75, 23);
            this.OpenSplitButton.TabIndex = 2;
            this.OpenSplitButton.Text = "Open";
            this.OpenSplitButton.UseVisualStyleBackColor = true;
            // 
            // SolutionOpenContextMenuStrip
            // 
            this.SolutionOpenContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SolutionOpenVisualStudioMenuStrip,
            this.SolutionOpenContainingFolderMenuStrip});
            this.SolutionOpenContextMenuStrip.Name = "SolutionOpenContextMenuStrip";
            this.SolutionOpenContextMenuStrip.Size = new System.Drawing.Size(170, 48);
            // 
            // SolutionOpenVisualStudioMenuStrip
            // 
            this.SolutionOpenVisualStudioMenuStrip.Name = "SolutionOpenVisualStudioMenuStrip";
            this.SolutionOpenVisualStudioMenuStrip.Size = new System.Drawing.Size(169, 22);
            this.SolutionOpenVisualStudioMenuStrip.Text = "Visual Studio";
            this.SolutionOpenVisualStudioMenuStrip.Click += new System.EventHandler(this.SolutionOpenVisualStudioMenuStrip_Click);
            // 
            // SolutionOpenContainingFolderMenuStrip
            // 
            this.SolutionOpenContainingFolderMenuStrip.Name = "SolutionOpenContainingFolderMenuStrip";
            this.SolutionOpenContainingFolderMenuStrip.Size = new System.Drawing.Size(169, 22);
            this.SolutionOpenContainingFolderMenuStrip.Text = "Containing Folder";
            this.SolutionOpenContainingFolderMenuStrip.Click += new System.EventHandler(this.SolutionOpenContainingFolderMenuStrip_Click);
            // 
            // ToggleExpandSplitButton
            // 
            this.ToggleExpandSplitButton.AutoSize = true;
            this.ToggleExpandSplitButton.ContextMenuStrip = this.ToggleExpandMenuStrip;
            this.ToggleExpandSplitButton.Location = new System.Drawing.Point(90, 19);
            this.ToggleExpandSplitButton.Name = "ToggleExpandSplitButton";
            this.ToggleExpandSplitButton.Size = new System.Drawing.Size(75, 23);
            this.ToggleExpandSplitButton.TabIndex = 1;
            this.ToggleExpandSplitButton.Text = "Expand";
            this.ToggleExpandSplitButton.UseVisualStyleBackColor = true;
            // 
            // SelectSplitButton
            // 
            this.SelectSplitButton.AutoSize = true;
            this.SelectSplitButton.ContextMenuStrip = this.SolutionSelectContextMenuStrip;
            this.SelectSplitButton.Location = new System.Drawing.Point(9, 19);
            this.SelectSplitButton.Name = "SelectSplitButton";
            this.SelectSplitButton.Size = new System.Drawing.Size(75, 23);
            this.SelectSplitButton.TabIndex = 0;
            this.SelectSplitButton.Text = "Select";
            this.SelectSplitButton.UseVisualStyleBackColor = true;
            // 
            // SolutionListTreeView
            // 
            this.SolutionListTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SolutionListTreeView.BackColor = System.Drawing.SystemColors.Window;
            this.SolutionListTreeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SolutionListTreeView.CheckBoxes = true;
            this.SolutionListTreeView.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SolutionListTreeView.LineColor = System.Drawing.Color.DodgerBlue;
            this.SolutionListTreeView.Location = new System.Drawing.Point(9, 48);
            this.SolutionListTreeView.Name = "SolutionListTreeView";
            this.SolutionListTreeView.Size = new System.Drawing.Size(667, 399);
            this.SolutionListTreeView.TabIndex = 3;
            this.SolutionListTreeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.SolutionListTreeView_AfterCheck);
            this.SolutionListTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.SolutionListTreeView_AfterSelect);
            this.SolutionListTreeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.SolutionListTreeView_NodeMouseClick);
            // 
            // SolutionContextMenuStrip
            // 
            this.SolutionContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SolutionOpenWithVisualStudioToolStripMenuItem,
            this.SolutionOpenContainingFolderToolStripMenuItem});
            this.SolutionContextMenuStrip.Name = "SolutionContextMenuStrip";
            this.SolutionContextMenuStrip.Size = new System.Drawing.Size(202, 48);
            // 
            // SolutionOpenWithVisualStudioToolStripMenuItem
            // 
            this.SolutionOpenWithVisualStudioToolStripMenuItem.Name = "SolutionOpenWithVisualStudioToolStripMenuItem";
            this.SolutionOpenWithVisualStudioToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.SolutionOpenWithVisualStudioToolStripMenuItem.Text = "Open with Visual Studio";
            this.SolutionOpenWithVisualStudioToolStripMenuItem.Click += new System.EventHandler(this.SolutionOpenWithVisualStudioToolStripMenuItem_Click);
            // 
            // SolutionOpenContainingFolderToolStripMenuItem
            // 
            this.SolutionOpenContainingFolderToolStripMenuItem.Name = "SolutionOpenContainingFolderToolStripMenuItem";
            this.SolutionOpenContainingFolderToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.SolutionOpenContainingFolderToolStripMenuItem.Text = "Open Containing Folder";
            this.SolutionOpenContainingFolderToolStripMenuItem.Click += new System.EventHandler(this.SolutionOpenContainingFolderToolStripMenuItem_Click);
            // 
            // DisplaySolutionListConfigFileSplitButton
            // 
            this.DisplaySolutionListConfigFileSplitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DisplaySolutionListConfigFileSplitButton.AutoSize = true;
            this.DisplaySolutionListConfigFileSplitButton.ContextMenuStrip = this.DisplaySolutionListConfigFileMenuStrip;
            this.DisplaySolutionListConfigFileSplitButton.Location = new System.Drawing.Point(563, 5);
            this.DisplaySolutionListConfigFileSplitButton.Name = "DisplaySolutionListConfigFileSplitButton";
            this.DisplaySolutionListConfigFileSplitButton.Size = new System.Drawing.Size(125, 23);
            this.DisplaySolutionListConfigFileSplitButton.TabIndex = 3;
            this.DisplaySolutionListConfigFileSplitButton.Text = "Solution List Config";
            this.DisplaySolutionListConfigFileSplitButton.UseVisualStyleBackColor = true;
            // 
            // SolutionListSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 495);
            this.ControlBox = false;
            this.Controls.Add(this.SolutionListsGroupBox);
            this.Controls.Add(this.DisplaySolutionListConfigFileSplitButton);
            this.Controls.Add(this.BranchComboBox);
            this.Controls.Add(this.BranchLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SolutionListSearchForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "SolutionListSearchForm";
            this.DisplaySolutionListConfigFileMenuStrip.ResumeLayout(false);
            this.SolutionSelectContextMenuStrip.ResumeLayout(false);
            this.ToggleExpandMenuStrip.ResumeLayout(false);
            this.SolutionListsGroupBox.ResumeLayout(false);
            this.SolutionListsGroupBox.PerformLayout();
            this.SolutionOpenContextMenuStrip.ResumeLayout(false);
            this.SolutionContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox BranchComboBox;
        private System.Windows.Forms.Label BranchLabel;
        private SplitButton DisplaySolutionListConfigFileSplitButton;
        private System.Windows.Forms.ContextMenuStrip DisplaySolutionListConfigFileMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem OpenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OpenContainingFolderToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip SolutionSelectContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem SolutionSelectAllToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem SolutionSelectNoneToolStripItem;
        private System.Windows.Forms.ContextMenuStrip ToggleExpandMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ExpandAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CollapseAllToolStripMenuItem;
        private System.Windows.Forms.GroupBox SolutionListsGroupBox;
        private SplitButton ToggleExpandSplitButton;
        private SplitButton SelectSplitButton;
        private System.Windows.Forms.TreeView SolutionListTreeView;
        private SplitButton OpenSplitButton;
        private System.Windows.Forms.ContextMenuStrip SolutionOpenContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem SolutionOpenVisualStudioMenuStrip;
        private System.Windows.Forms.ContextMenuStrip SolutionContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem SolutionOpenContainingFolderMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem SolutionOpenWithVisualStudioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SolutionOpenContainingFolderToolStripMenuItem;
    }
}