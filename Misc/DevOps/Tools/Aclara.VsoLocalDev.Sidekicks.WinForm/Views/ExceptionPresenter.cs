﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Exception presenter.
    /// </summary>
    public class ExceptionPresenter
    {

        #region Private Constants
        private const string NoInformationAvailable = "No information available.";
        #endregion

        #region Private Data Members

        private IExceptionView _exceptionView;
        private Exception _exception;
        private string _context;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Exception view.
        /// </summary>
        public IExceptionView ExceptionView
        {
            get { return _exceptionView; }
            set { _exceptionView = value; }
        }

        /// <summary>
        /// Property: Exception.
        /// </summary>
        public Exception Exception
        {
            get { return _exception; }
            set { _exception = value; }
        }

        /// <summary>
        /// Property: Context;
        /// </summary>
        public string Context
        {
            get { return _context; }
            set { _context = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Overriden constructor.
        /// </summary>
        public ExceptionPresenter(IExceptionView exceptionView)
        {
            _exceptionView = exceptionView;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {

            string exceptionMessage = string.Empty;
            string exceptionText = string.Empty;

            if (this.Exception != null)
            {
                exceptionMessage = this.Exception.Message;
            }
            else
            {
                exceptionMessage = NoInformationAvailable;
            }

            if (String.IsNullOrEmpty(Context) != true)
            {
                exceptionMessage += string.Format("{1}{0}{1}",
                                               Context,
                                               Environment.NewLine);
            }

            if (this.Exception != null)
            {
                exceptionText += this.Exception.ToString();
            }
            else
            {
                exceptionText = NoInformationAvailable;
            }

            this.ExceptionView.ExceptionMessage = exceptionMessage;
            this.ExceptionView.ExceptionText = exceptionText;

        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

    }
}
