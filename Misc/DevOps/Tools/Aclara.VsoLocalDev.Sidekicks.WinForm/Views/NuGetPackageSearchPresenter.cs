﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using Aclara.NuGet.Core.Client;
using Aclara.NuGet.Core.Client.Events;
using Aclara.NuGet.Core.Client.Models;
using Aclara.TeamFoundation.VersionControl.Client;
using Aclara.Tools.Configuration;
using Aclara.Tools.Configuration.SolutionListConfig;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// NuGet package search presenter.
    /// </summary>
    public class NuGetPackageSearchPresenter
    {

        #region Private Constants

        private const string TeamProjectCollectionNameDefaultValue = "";
        private const string TeamProjectNameDefaultValue = "";

        private const string ReplacementToken_TeamProjectWorkingFolder = @"\$\(TeamProjectWorkingFolder\)";
        private const string ReplacementToken_TeamProjectName = @"\$\(TeamProjectName\)";
        private const string ReplacementToken_BranchName = @"\$\(BranchName\)";

        #endregion

        #region Private Data Members

        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private SidekickConfiguration _sidekickConfiguration;
        private INuGetPackageSearchView _nuGetPackageSearchView = null;
        private NuGetPackageInfoList _nugetPackageInfoList;
        private ProjectManager _projectManager = null;
        private NuGetCoreManager _nugetCoreManager;
        private ITeamProjectInfo _teamProjectInfo = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;

        #endregion

        #region Public Types

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public Logger Logger
        {
            get
            {
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: NuGet package search view.
        /// </summary>
        public INuGetPackageSearchView NuGetPackageSearchView
        {
            get
            {
                return _nuGetPackageSearchView;
            }
            private set
            {
                _nuGetPackageSearchView = value;
            }
        }

        /// <summary>
        /// Property: NuGet package information list.
        /// </summary>
        public NuGetPackageInfoList NuGetPackageInfoList
        {
            get { return _nugetPackageInfoList; }
            set { _nugetPackageInfoList = value; }
        }

        /// <summary>
        /// Property: Project manager.
        /// </summary>
        public ProjectManager ProjectManager
        {
            get { return _projectManager; }
            set { _projectManager = value; }
        }

        /// <summary>
        /// Property: NuGet core manager.
        /// </summary>
        public NuGetCoreManager NuGetCoreManager
        {
            get { return _nugetCoreManager; }
            set { _nugetCoreManager = value; }
        }

        /// <summary>
        /// Property: Team project collection default.
        /// </summary>
        public string TeamProjectCollectionNameDefault
        {
            get { return TeamProjectCollectionNameDefaultValue; }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Team project name default.
        /// </summary>
        public string TeamProjectNameDefault
        {
            get { return TeamProjectNameDefaultValue; }
        }

        /// <summary>
        /// Populate branch name list.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        public void PopulateBrachNameList(string tfsServerAddress,
                                          string teamProjectName)
        {
            List<string> branchNameList = null;


            try
            {

                this.ProjectManager = CreateProjectManager(tfsServerAddress, teamProjectName);

                branchNameList = this.ProjectManager.GetTeamProjectYearMonthBranchNames();
                if (branchNameList == null)
                {
                    branchNameList = new List<string>();
                }

                this.NuGetPackageSearchView.PopulateBranchNameList(branchNameList);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }


        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="NuGetPackageSearchView"></param>
        /// <param name="sidekickConfiguration"></param>
        public NuGetPackageSearchPresenter(ITeamProjectInfo teamProjectInfo,
                                           SidekickConfiguration sidekickConfiguration,
                                           INuGetPackageSearchView NuGetPackageSearchView)
        {
            this.TeamProjectInfo = teamProjectInfo;
            this.SidekickConfiguration = sidekickConfiguration;
            this.NuGetPackageSearchView = NuGetPackageSearchView;
            this.NuGetPackageInfoList = new NuGetPackageInfoList();
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <remarks>
        /// Hide default constructor to enforce dependency injection.
        /// </remarks>
        private NuGetPackageSearchPresenter()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {
            try
            {

                this.NuGetPackageSearchView.EnableControlsDependantOnTeamProject(false);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Team project collection name changed.
        /// </summary>
        public void TeamProjectCollectionNameChanged()
        {

            try
            {
                if (this.TeamProjectInfo.TeamProjectCollectionUri.ToString() != string.Empty &&
                    this.TeamProjectInfo.TeamProjectCollectionUri.ToString() != TeamProjectCollectionNameDefault &&
                    this.TeamProjectInfo.TeamProjectName != string.Empty &&
                    this.TeamProjectInfo.TeamProjectName != TeamProjectNameDefault)
                {
                    this.NuGetPackageSearchView.EnableControlsDependantOnTeamProject(true);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Team project name changed.
        /// </summary>
        public void TeamProjectNameChanged()
        {

            try
            {
                if (this.TeamProjectInfo.TeamProjectCollectionUri.ToString() != string.Empty &&
                    this.TeamProjectInfo.TeamProjectCollectionUri.ToString() != TeamProjectCollectionNameDefault &&
                    this.TeamProjectInfo.TeamProjectName != string.Empty &&
                    this.TeamProjectInfo.TeamProjectName != TeamProjectNameDefault)
                {
                    this.NuGetPackageSearchView.EnableControlsDependantOnTeamProject(true);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalRequestDuration;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalRequestDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.NuGetPackageSearchView.BackgroundTaskCompleted("NuGet package  list request canceled.");
                        this.NuGetPackageSearchView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format("NuGet package  list request cancelled."));


                        //Log background task completed log entry.

                        this.Logger.Info(string.Format("[*] NuGet package  list request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                       totalRequestDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.NuGetPackageSearchView.BackgroundTaskCompleted("");
                        this.NuGetPackageSearchView.ApplyButtonEnable(true);


                        //Log background task completed log entry.

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    //Handle operation canceled exception.
                                    this.Logger.Error(innerException, string.Format("Operation cancelled."));
                                }

                            }
                        }

                        this.Logger.Info(string.Format("[*] NuGet package list request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                       totalRequestDuration));

                        break;

                    case TaskStatus.RanToCompletion:


                        this.NuGetPackageSearchView.NuGetPackageInfoList = this.NuGetPackageInfoList;
                        this.NuGetPackageSearchView.BackgroundTaskCompleted("NuGet package  list request completed.");
                        this.NuGetPackageSearchView.ApplyButtonEnable(true);

                        //Log background task completed log entry.

                        this.Logger.Info(string.Format("[*] NuGet package list request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                       totalRequestDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }


            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Retrieve NuGet package information list.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="workspaceName"></param>
        /// <param name="userName"></param>
        /// <param name="branchName"></param>
        public void GetNuGetPackageInfoList(string tfsServerAddress,
                                            string teamProjectName,
                                            string workspaceName,
                                            string userName,
                                            string branchName)
        {

            string teamProjectBranchWorkingFolder = string.Empty;
            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.NuGetPackageSearchView.BackgroundTaskStatus = "Cancelling NuGet package request...";
                    this.NuGetPackageSearchView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {

                    this.ProjectManager = this.CreateProjectManager(tfsServerAddress, teamProjectName);
                    this.NuGetCoreManager = this.CreateNuGetCoreManager();

                    teamProjectBranchWorkingFolder = this.ProjectManager.GetTeamProjectBranchWorkingFolder(teamProjectName, workspaceName, branchName, userName);

                    this.NuGetPackageInfoList.Clear();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => { this.BackgroundTaskCompleted(); });

                    this.NuGetCoreManager.GetNuGetPackageInformationCompleted += OnGetNuGetPackageInformationCompleted;
                    this.BackgroundTask = new Task(() => this.NuGetCoreManager.GetNuGetPackageInformation(teamProjectBranchWorkingFolder, cancellationToken), cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;
                    this.NuGetPackageSearchView.BackgroundTaskStatus = "NuGet package information requested...";

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Retrieve solution selector lists.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="workspaceName"></param>
        /// <param name="userName"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        public SolutionSelectorLists GetSolutionSelectorLists(string tfsServerAddress,
                                                              string teamProjectName,
                                                              string workspaceName,
                                                              string userName,
                                                              string branchName)
        {
            SolutionSelectorLists result = null;
            SolutionLists solutionLists = null;
            SolutionSelectorList solutionSelectorList = null;
            SolutionSelector solutionSelector = null;
            SolutionConfigManager solutionConfigManager = null;
            SolutionConfigManagerFactory solutionConfigManagerFactory = null;
            string configurationFileName = string.Empty;
            string teamProjectWorkingFolder = string.Empty;

            try
            {

                result = new SolutionSelectorLists();

                solutionConfigManagerFactory = new SolutionConfigManagerFactory();
                solutionConfigManager = solutionConfigManagerFactory.CreateSolutionConfigManager();

                this.ProjectManager = this.CreateProjectManager(tfsServerAddress, teamProjectName);

                teamProjectWorkingFolder = this.ProjectManager.GetTeamProjectWorkingFolder(teamProjectName, workspaceName, branchName, userName);

                configurationFileName = this.GetSolutionListConfigFilePath(teamProjectName,
                                                                           branchName);

                if (File.Exists(configurationFileName) == false)
                {
                    throw new FileNotFoundException(string.Format("Solution list configuration file not found. (ConfigurationFileName: {0})",
                                                                  configurationFileName));
                }

                //Retrieve solution lists.
                solutionLists = solutionConfigManager.GetSolutionGroupList(configurationFileName);

                foreach (SolutionList solutionList in solutionLists)
                {
                    solutionSelectorList = new SolutionSelectorList();

                    solutionSelectorList.Name = solutionList.Name;

                    foreach (SolutionConfig solutionConfig in solutionList)
                    {
                        solutionSelector = new SolutionSelector();

                        solutionSelector.SolutionListName = solutionList.Name;
                        solutionSelector.SolutionName = solutionConfig.SolutionName;
                        solutionSelector.SolutionPath = solutionConfig.SolutionPath;
                        solutionSelector.MSBuildProperties = solutionConfig.MSBuildProperties;
                        this.ReplaceTextInSolutionSelector(ref solutionSelector,
                                                           teamProjectWorkingFolder,
                                                           teamProjectName,
                                                           branchName);
                        solutionSelectorList.Add(solutionSelector);
                    }

                    result.Add(solutionSelectorList);
                }

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Retrieve solution list configuration file path.
        /// </summary>
        /// <param name="teamProjectWorkingFolder"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        public string GetSolutionListConfigFilePath(string teamProjectName,
                                                    string branchName)
        {
            string result = string.Empty;
            string teamProjectWorkingFolder = string.Empty;

            try
            {
                teamProjectWorkingFolder = this.ProjectManager.GetTeamProjectWorkingFolder(this.TeamProjectInfo.TeamProjectName,
                                                                                           this.TeamProjectInfo.WorkspaceName,
                                                                                           branchName,
                                                                                           this.TeamProjectInfo.BasicAuthRestAPIUserProfileName);

                result = this.SidekickConfiguration.GetSolutionListConfigFilePath(teamProjectWorkingFolder, teamProjectName, branchName);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve solution path.
        /// </summary>
        /// <param name="teamProjectWorkingFolder"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        /// 
        [Obsolete("Remove as method is not used.")]
        public string GetSolutionPath(string teamProjectName,
                                     string branchName)
        {
            string result = string.Empty;
            string teamProjectWorkingFolder = string.Empty;

            try
            {
                teamProjectWorkingFolder = this.ProjectManager.GetTeamProjectWorkingFolder(this.TeamProjectInfo.TeamProjectName,
                                                                                           this.TeamProjectInfo.WorkspaceName, "",
                                                                                           this.TeamProjectInfo.BasicAuthRestAPIUserProfileName);
                result = Path.Combine(teamProjectWorkingFolder,
                                      teamProjectName,
                                      branchName);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Determine whether team project is mapped to local folder.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        [Obsolete("Remove as method is not used.")]
        public bool IsTeamProjectMapped(string tfsServerAddress,
                                        string teamProjectName)
        {
            bool result = false;

            try
            {
                this.ProjectManager = CreateProjectManager(tfsServerAddress, this.TeamProjectInfo.TeamProjectName);

                result = this.ProjectManager.IsTeamProjectMappedToLocalFolder(this.TeamProjectInfo.TeamProjectName,
                                                                              this.TeamProjectInfo.WorkspaceName,
                                                                              this.TeamProjectInfo.BasicAuthRestAPIUserProfileName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Event Handler: Get NuGet package information completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="getNuGetPackageInformationEventArgs"></param>
        protected void OnGetNuGetPackageInformationCompleted(Object sender,
                                                             GetNuGetPackageInformationEventArgs getNuGetPackageInformationEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Status list details available.
                if (getNuGetPackageInformationEventArgs.StatusList != null)
                {

                    //Status list is not empty.
                    if (getNuGetPackageInformationEventArgs.StatusList.Count > 0)
                    {
                        message = string.Format("NuGet package retrieval completed with error(s). (Status --> {3})",
                                                getNuGetPackageInformationEventArgs.StatusList.Format(Tools.Common.StatusManagement.StatusTypes.FormatOption.Minimum));

                        //Status list has errors or build failed.
                        if (getNuGetPackageInformationEventArgs.StatusList.HasStatusSeverity(Tools.Common.StatusManagement.StatusTypes.StatusSeverity.Error))
                        {
                            this.Logger.Error(message);
                        }
                        else if (getNuGetPackageInformationEventArgs.StatusList.HasStatusSeverity(Tools.Common.StatusManagement.StatusTypes.StatusSeverity.Warning))
                        {
                            this.Logger.Warn(message);
                        }
                        else
                        {
                            this.Logger.Info(message);
                        }


                    }
                    else if (getNuGetPackageInformationEventArgs.NuGetPackageInfo != null)
                    {
                        this.NuGetPackageInfoList.AddOrReplace(getNuGetPackageInformationEventArgs.NuGetPackageInfo);

                        message = string.Format("NuGet package retrieved. (Id: {0})",
                                                   getNuGetPackageInformationEventArgs.NuGetPackageInfo.PackageId);
                        this.NuGetPackageSearchView.UpdateBackgroundTaskStatus(message);
                    }

                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Create project manager.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        protected ProjectManager CreateProjectManager(string tfsServerAddress,
                                                      string teamProjectName)
        {
            ProjectManager result = null;
            Uri tfsServerUri = null;

            try
            {

                tfsServerUri = new Uri(tfsServerAddress);

                result = ProjectManagerFactory.CreateProjectManager(tfsServerUri, teamProjectName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create NuGet core manager.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        protected NuGetCoreManager CreateNuGetCoreManager()
        {
            NuGetCoreManager result = null;

            try
            {

                result = NuGetCoreManagerFactory.CreateNuGetCoreManager();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            AggregateException aggregateException = null;
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.

                        aggregateException = eventArgs.Exception;
                        innerException = aggregateException.InnerException;
                        eventArgs.SetObserved();

                        this.Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                    }

                    return;
                }


                aggregateException = eventArgs.Exception;
                innerException = aggregateException.InnerException;
                eventArgs.SetObserved();

                this.Logger.Error(innerException, string.Format("Background task failed. --> Inner exception: {0}",
                                                 innerException));

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        /// <summary>
        /// Replace text in solution selector.
        /// </summary>
        /// <param name="solutionSelector"></param>
        /// <param name="teamProjectWorkingFolder"></param>
        /// <param name="branchName"></param>
        protected void ReplaceTextInSolutionSelector(ref SolutionSelector solutionSelector,
                                                    string teamProjectWorkingFolder,
                                                    string teamProjectName,
                                                    string branchName)
        {

            try
            {

                solutionSelector.SolutionPath = ReplaceTokenInTextWithValue(solutionSelector.SolutionPath,
                                                                           ReplacementToken_TeamProjectWorkingFolder,
                                                                           teamProjectWorkingFolder);
                solutionSelector.SolutionPath = ReplaceTokenInTextWithValue(solutionSelector.SolutionPath,
                                                                           ReplacementToken_TeamProjectName,
                                                                           teamProjectName);
                solutionSelector.SolutionPath = ReplaceTokenInTextWithValue(solutionSelector.SolutionPath,
                                                                           ReplacementToken_BranchName,
                                                                           branchName);


            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Replace token in text with specified value.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="token"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string ReplaceTokenInTextWithValue(string text, string token, string value)
        {
            string result = string.Empty;

            try
            {

                if (string.IsNullOrEmpty(text) == true)
                {
                    result = text;
                    return result;
                }

                result = Regex.Replace(text, token, value, RegexOptions.IgnoreCase);

                return result;

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

    }
}
