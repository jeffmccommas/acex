﻿using Aclara.Build.Engine.Client;
using Aclara.Build.Engine.Client.Events;
using Aclara.NuGet.Core.Client;
using Aclara.NuGet.Core.Client.Events;
using Aclara.TeamFoundation.VersionControl.Client;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using NLog;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Build solution list presenter.
    /// </summary>
    public class NuGetPackageAnalyzerPresenter
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private SidekickConfiguration _sidekickConfiguration;
        private INuGetPackageAnalyzerView _nugetPackageAnalyzerView;
        private ProjectManager _projectManager = null;
        private NuGetCoreManager _nugetCoreManager;
        private BuildManager _buildManager = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public Logger Logger
        {
            get
            {
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Build solution list view.
        /// </summary>
        public INuGetPackageAnalyzerView NuGetPackageAnalyzerView
        {
            get { return _nugetPackageAnalyzerView; }
            set { _nugetPackageAnalyzerView = value; }
        }

        /// <summary>
        /// Property: Project manager.
        /// </summary>
        public ProjectManager ProjectManager
        {
            get { return _projectManager; }
            set { _projectManager = value; }
        }

        /// <summary>
        /// Property: NuGet core manager.
        /// </summary>
        public NuGetCoreManager NuGetCoreManager
        {
            get { return _nugetCoreManager; }
            set { _nugetCoreManager = value; }
        }

        /// <summary>
        /// Property: Build manager.
        /// </summary>
        public BuildManager BuildManager
        {
            get { return _buildManager; }
            set { _buildManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="nugetPackageAnalyzerView"></param>
        public NuGetPackageAnalyzerPresenter(SidekickConfiguration sidekickConfiguration,
                                             INuGetPackageAnalyzerView nugetPackageAnalyzerView)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.NuGetPackageAnalyzerView = nugetPackageAnalyzerView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private NuGetPackageAnalyzerPresenter()
        {

        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalRequestDuration;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalRequestDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.NuGetPackageAnalyzerView.BackgroundTaskCompleted("NuGet package  list request canceled.");

                        this.Logger.Info(string.Format("NuGet package  list request cancelled."));


                        //Log background task completed log entry.

                        this.Logger.Info(string.Format("[*] NuGet package  list request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                       totalRequestDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.NuGetPackageAnalyzerView.BackgroundTaskCompleted("");


                        //Log background task completed log entry.

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    //Handle operation canceled exception.
                                    this.Logger.Error(innerException, string.Format("Operation cancelled."));
                                }

                            }
                        }

                        this.Logger.Info(string.Format("[*] NuGet package list request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                       totalRequestDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.NuGetPackageAnalyzerView.BackgroundTaskCompleted("NuGet package  list request completed.");

                        //Log background task completed log entry.

                        this.Logger.Info(string.Format("[*] NuGet package list request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                       totalRequestDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }


            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Build solution list.
        /// </summary>
        /// <param name="teamProjectCollectionName"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="brachName"></param>
        /// <param name="buildTarget"></param>
        /// <param name="solutionSelectorList"></param>
        public void BuildSolutionList(string teamProjectCollectionName,
                                      string teamProjectName,
                                      string brachName,
                                      string buildTarget,
                                      SolutionSelectorList solutionSelectorList)
        {


            BuildSolutionRequestDataList buildSolutionRequestDataList = null;
            BuildSolutionRequestData buildSolutionRequestData = null;
            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.NuGetPackageAnalyzerView.BackgroundTaskStatus = "Cancelling build request...";

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {

                    this.BuildManager = this.CreateBuildManager();
                    buildSolutionRequestDataList = new BuildSolutionRequestDataList();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => { this.BackgroundTaskCompleted(); });

                    //Convert solution selector list to build solution request data list.
                    foreach (SolutionSelector solutionSelector in solutionSelectorList)
                    {
                        buildSolutionRequestData = new BuildSolutionRequestData();
                        buildSolutionRequestData.SolutionListName = solutionSelector.SolutionListName;
                        buildSolutionRequestData.SolutionName = solutionSelector.SolutionName;
                        buildSolutionRequestData.SolutionPath = solutionSelector.SolutionPath;
                        buildSolutionRequestData.MSBuildProperties = solutionSelector.MSBuildProperties;
                        buildSolutionRequestDataList.Add(buildSolutionRequestData);
                    }

                    this.BuildManager.BuildRequestStarted += OnBuildRequestStarted;
                    this.BuildManager.BuildRequestCompleted += OnBuildRequestCompleted;
                    this.BackgroundTask = new Task(() => this.BuildManager.RunBuild(buildSolutionRequestDataList, buildTarget, cancellationToken), cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;
                    this.NuGetPackageAnalyzerView.BackgroundTaskStatus = "Build requested...";

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Retrieve NuGet package information list.
        /// </summary>
        /// <param name="teamProjectCollectionName"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="brachName"></param>
        /// <param name="buildTarget"></param>
        /// <param name="solutionSelectorList"></param>
        public void GetNuGetPackageInfoList(string rootPathFolderName)
        {


            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.NuGetPackageAnalyzerView.BackgroundTaskStatus = "Cancelling NuGet package request...";

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {

                    this.NuGetCoreManager = this.CreateNuGetCoreManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => { this.BackgroundTaskCompleted(); });

                    this.NuGetCoreManager.GetNuGetPackageInformationCompleted += OnGetNuGetPackageInformationCompleted;
                    this.BackgroundTask = new Task(() => this.NuGetCoreManager.GetNuGetPackageInformation(rootPathFolderName, cancellationToken), cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;
                    this.NuGetPackageAnalyzerView.BackgroundTaskStatus = "NuGet package information requested...";

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion

        #region Protected Methods

        /// <summary>
        /// Event Handler: Solution build started.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="buildRequestStartedEventArgs"></param>
        protected void OnBuildRequestStarted(Object sender,
                                             BuildRequestStartedEventArgs buildRequestStartedEventArgs)
        {
            SolutionSelector solutionSelector = null;
            string message = string.Empty;

            try
            {

                //Build solution name and status list details available.
                if (buildRequestStartedEventArgs.BuildStartedResult.SolutionName != null &&
                    buildRequestStartedEventArgs.BuildStartedResult.StatusList != null)
                {

                    //Update solution build status.
                    solutionSelector = new SolutionSelector();
                    solutionSelector.SolutionListName = buildRequestStartedEventArgs.BuildStartedResult.SolutionListName;
                    solutionSelector.SolutionName = buildRequestStartedEventArgs.BuildStartedResult.SolutionName;
                    solutionSelector.BuildStatus = buildRequestStartedEventArgs.BuildStartedResult.BuildStatus;
                    //this.NuGetPackageAnalyzerView.UpdateSolutionBuildStatus(solutionSelector);

                    //Status list is not empty.
                    if (buildRequestStartedEventArgs.BuildStartedResult.StatusList.Count > 0)
                    {
                        message = string.Format("Build request started with error(s). (Solution name: {0}, Status --> {2})",
                                               buildRequestStartedEventArgs.BuildStartedResult.SolutionName,
                                               buildRequestStartedEventArgs.BuildStartedResult.StatusList.Format(Aclara.Build.Engine.Client.StatusManagement.StatusTypes.FormatOption.Minimum));

                        //Status list has errors or build failed.
                        if (buildRequestStartedEventArgs.BuildStartedResult.StatusList.HasStatusSeverity(Build.Engine.Client.StatusManagement.StatusTypes.StatusSeverity.Error))
                        {
                            this.Logger.Error(message);

                        }
                        else if (buildRequestStartedEventArgs.BuildStartedResult.StatusList.HasStatusSeverity(Build.Engine.Client.StatusManagement.StatusTypes.StatusSeverity.Warning))
                        {
                            this.Logger.Warn(message);
                        }
                        else
                        {
                            this.Logger.Info(message);
                        }

                    }
                    else if (buildRequestStartedEventArgs.BuildStartedResult.SolutionName != null)
                    {
                        message = string.Format("Build request started. (Solution name: {0})",
                                                  buildRequestStartedEventArgs.BuildStartedResult.SolutionName);
                        this.Logger.Info(message);
                    }

                }
                else
                {
                    this.Logger.Info(string.Format("Build request started. Details unavailable."));
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Handler: Solution build completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="buildRequestCompletedEventArgs"></param>
        protected void OnBuildRequestCompleted(Object sender,
                                             BuildRequestCompletedEventArgs buildRequestCompletedEventArgs)
        {
            SolutionSelector solutionSelector = null;
            string message = string.Empty;

            try
            {

                //Build solution name and status list details available.
                if (buildRequestCompletedEventArgs.BuildCompletedResult.SolutionName != null &&
                    buildRequestCompletedEventArgs.BuildCompletedResult.StatusList != null)
                {
                    //Update solution build status.
                    solutionSelector = new SolutionSelector();
                    solutionSelector.SolutionListName = buildRequestCompletedEventArgs.BuildCompletedResult.SolutionListName;
                    solutionSelector.SolutionName = buildRequestCompletedEventArgs.BuildCompletedResult.SolutionName;

                    solutionSelector.BuildOutput = buildRequestCompletedEventArgs.BuildCompletedResult.BuildOutput;
                    solutionSelector.BuildWarningList = buildRequestCompletedEventArgs.BuildCompletedResult.BuildWarningList;
                    solutionSelector.BuildErrorList = buildRequestCompletedEventArgs.BuildCompletedResult.BuildErrorList;
                    solutionSelector.BuildStatus = buildRequestCompletedEventArgs.BuildCompletedResult.BuildStatus;
                    ////this.NuGetPackageAnalyzerView.UpdateSolutionBuildStatus(solutionSelector);

                    //Status list is not empty.
                    if (buildRequestCompletedEventArgs.BuildCompletedResult.StatusList.Count > 0)
                    {
                        message = string.Format("Build request completed with error(s). (Solution name: {0}, Build result: {1}, Duration: {2:dd\\.hh\\:mm\\:ss}), Status --> {3})",
                                                buildRequestCompletedEventArgs.BuildCompletedResult.SolutionName,
                                                buildRequestCompletedEventArgs.BuildCompletedResult.BuildStatus.ToString(),
                                                buildRequestCompletedEventArgs.BuildCompletedResult.BuildDuration,
                                                buildRequestCompletedEventArgs.BuildCompletedResult.StatusList.Format(Aclara.Build.Engine.Client.StatusManagement.StatusTypes.FormatOption.Minimum));

                        //Status list has errors or build failed.
                        if (buildRequestCompletedEventArgs.BuildCompletedResult.StatusList.HasStatusSeverity(Build.Engine.Client.StatusManagement.StatusTypes.StatusSeverity.Error) ||
                            buildRequestCompletedEventArgs.BuildCompletedResult.BuildStatus == Build.Engine.Client.BuildStatus.Failed)
                        {
                            this.Logger.Error(message);
                        }
                        else if (buildRequestCompletedEventArgs.BuildCompletedResult.StatusList.HasStatusSeverity(Build.Engine.Client.StatusManagement.StatusTypes.StatusSeverity.Warning))
                        {
                            this.Logger.Warn(message);
                        }
                        else
                        {
                            this.Logger.Info(message);
                        }


                    }
                    else if (buildRequestCompletedEventArgs.BuildCompletedResult.SolutionName != null)
                    {
                        message = string.Format("Build request completed. (Solution name: {0}, Build result: {1}, Duration: {2:dd\\.hh\\:mm\\:ss})",
                                                buildRequestCompletedEventArgs.BuildCompletedResult.SolutionName,
                                                buildRequestCompletedEventArgs.BuildCompletedResult.BuildStatus.ToString(),
                                                buildRequestCompletedEventArgs.BuildCompletedResult.BuildDuration);
                        if (buildRequestCompletedEventArgs.BuildCompletedResult.BuildStatus == BuildStatus.Failed)
                        {
                            this.Logger.Error(message);
                        }
                        else
                        {
                            this.Logger.Info(message);
                        }

                    }

                }
                else
                {
                    this.Logger.Info(message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Handler: Get NuGet package information completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="buildRequestCompletedEventArgs"></param>
        protected void OnGetNuGetPackageInformationCompleted(Object sender,
                                                             GetNuGetPackageInformationEventArgs buildRequestCompletedEventArgs)
        {
            SolutionSelector solutionSelector = null;
            string message = string.Empty;

            try
            {

                //Build solution name and status list details available.
                if (buildRequestCompletedEventArgs.StatusList != null)
                {
                    //this.NuGetPackageAnalyzerView.UpdateSolutionBuildStatus(solutionSelector);

                    //Status list is not empty.
                    if (buildRequestCompletedEventArgs.StatusList.Count > 0)
                    {
                        message = string.Format("Build request completed with error(s). (Status --> {3})",
                                                buildRequestCompletedEventArgs.StatusList.Format(Tools.Common.StatusManagement.StatusTypes.FormatOption.Minimum));

                        //Status list has errors or build failed.
                        if (buildRequestCompletedEventArgs.StatusList.HasStatusSeverity(Tools.Common.StatusManagement.StatusTypes.StatusSeverity.Error))
                        {
                            this.Logger.Error(message);
                        }
                        else if (buildRequestCompletedEventArgs.StatusList.HasStatusSeverity(Tools.Common.StatusManagement.StatusTypes.StatusSeverity.Warning))
                        {
                            this.Logger.Warn(message);
                        }
                        else
                        {
                            this.Logger.Info(message);
                        }


                    }

                }
                else
                {
                    this.Logger.Info(message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            AggregateException aggregateException = null;
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.

                        aggregateException = eventArgs.Exception;
                        innerException = aggregateException.InnerException;
                        eventArgs.SetObserved();

                        this.Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                    }

                    return;
                }


                aggregateException = eventArgs.Exception;
                innerException = aggregateException.InnerException;
                eventArgs.SetObserved();

                this.Logger.Error(innerException, string.Format("Background task failed. --> Inner exception: {0}",
                                                 innerException));

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        /// <summary>
        /// Create build manager.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        protected BuildManager CreateBuildManager()
        {
            BuildManager result = null;
            BuildManagerFactory buildManagerFactory = null;

            try
            {

                buildManagerFactory = new BuildManagerFactory();
                result = buildManagerFactory.CreateBuildManager();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create NuGet core manager.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        protected NuGetCoreManager CreateNuGetCoreManager()
        {
            NuGetCoreManager result = null;

            try
            {
               
                result = NuGetCoreManagerFactory.CreateNuGetCoreManager();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create project manager.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        protected ProjectManager CreateProjectManager(string tfsServerAddress,
                                                      string teamProjectName)
        {
            ProjectManager result = null;
            Uri tfsServerUri = null;

            try
            {

                tfsServerUri = new Uri(tfsServerAddress);

                result = ProjectManagerFactory.CreateProjectManager(tfsServerUri, teamProjectName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
