﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm
{
    /// <summary>
    /// Web application search view - interface.
    /// </summary>
    public interface IWebAppSearchView
    {

        string SiteName { get; set; }

        string WebAppPathPartial { get; set; }

        void TeamProjectChanged();

        void EnableControlsDependantOnTeamProject(bool enabled);

        void EnableControlsDependantOnSearch(bool enabled);

        void ClearSearch();

        void Refresh();

        void PopulateBranchNameList(List<string> branchNameList);
        
        WebAppSelectorList WebAppSelectorList { get; set; }

        WebAppSelectorList GetSelectedWebAppSelectorList();

        WebAppSelectorList GetSelectedSiteLevelWebAppSelectorList();
    }
}
