﻿using Aclara.Sidekicks.AutoUpdater;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Events;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.Server;
using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Aclara.TeamFoundation.VersionControl.Client;
using Microsoft.TeamFoundation.VersionControl.Client;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    public partial class SidekickContainerForm : Form, ITeamProjectInfo
    {

        #region Private Constansts

        private const string SidekickContainerFormTitleSuffix = "Aclara Vso LocalDev Sidekicks";

        private const string SidekickCategory_Build = "Build";
        private const string SidekickCategory_Branch = "Branch";
        private const string SidekickCategory_Home = "Home";
        private const string SidekickCategory_Utility = "Utility";

        private const string ChangeLogFileName = "ChangeLog.txt";

        private const string LoggedInAs_Label = "Logged In As:";
        private const string TeamProjectCollectionUri_Label = "Team Project Collection:";
        private const string TeamProject_Label = "Team Project Name:";
        private const string TeamProjectSelectionPrompt = "Select Team Project to start.";

        private const string ShowLogToolStripButton_ShowLog = "Show Log";
        private const string ShowLogToolStripButton_HideLog = "Hide Log";

        private const string Sidekick_SwitchLocalDevBranch = "Switch Local Dev Branch";
        private const string Sidekick_BuildSolutionList = "Build Solution List";
        private const string Sidekick_AzureRedisCacheAdmim = "Azure Redis Cache Admin";

        private const string SidekicksAutoUpdater_NewVersionAvailableShort = "A new version is available.";
        private const string SidekicksAutoUpdater_UpdaterFailed = "Auto-updater failed.";
        private const string SidekicksAutoUpdater_UpToDate = "Sidekicks are up to date.";
        private const string SidekicksAutoUpdater_MoreInfo = "More info...";
        private const string SidekicksAutoUpdater_NewVersionAvailablePrompt = "A new version of Aclara.VsoLocalDev.Sidekicks is available. Update now?";
        private const string SidekicksAutoUpdater_CurrentVersionAlreadyInstalledPrompt = "Current version of Aclara.VsoLocalDev.Sidekicks is already installed.";

        private const string ApplicationExit_SidekickStillRunningForceExitPrompt = "The sidekicks listed below are busy. Cancel running sidekick requests or force application shutdown. \r\n\r\n {0} \r\n\r\nForce application shutdown?";

        private const string VisualStudioOnlineBaseUri = ".visualstudio.com";
        private const string ValidationMessage_OnPremiseTFSNotSupported = "On-premise Team Foundation Server not supported.";

        private const string ShowLogFileErrorMessage_CouldNotLocateLogFile = "Could not locate log file.";

        private const string VSTS_Build_Domain = ".visualstudio.com";

        private const string TryAlternateProjectAndCredentialsPrompt = "Do you wish to try the alternate project and credentials prompt?";

        #endregion

        #region Private Data Members

        private LogForm _logForm = null;
        private string _sidekicksBootstrapperExeRootPath;
        private string _sidekicksBootstrapperExeName;
        private string _sidekicksAutoUpdaterVersion;
        private SidekicksAutoUpdater _sidekicksAutoUpdater;
        private SidekickConfiguration _sidekickConfiguration;
        private SidekickViewList _sidekickViewList;
        private SidekickView _homeSidekickView;
        string _basicAuthRestAPIUserProfileName = string.Empty;
        string _basicAuthRestAPIPassword = string.Empty;
        private ProjectManager _projectManager = null;
        private Uri _teamProjectCollectionUri;
        private string _teamProjectName;
        private string _workspaceName;
        private bool _haveCredentialsAndTeamProjectInfoBeenValidated;
        private SidekickViewHistoryList _sidekickViewHistoryList;

        #endregion

        #region Public Delegates

        public event EventHandler<SidekickSelectionChangedEventArgs> SidekickSelectionChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick view list.
        /// </summary>
        public SidekickViewList SidekickViewList
        {
            get { return _sidekickViewList; }
            set { _sidekickViewList = value; }
        }

        /// <summary>
        /// Property: Home sidekick view.
        /// </summary>
        public SidekickView HomeSidekickView
        {
            get { return _homeSidekickView; }
            set { _homeSidekickView = value; }
        }

        /// <summary>
        /// Property: Log form.
        /// </summary>
        public LogForm LogForm
        {
            get { return _logForm; }
            set { _logForm = value; }
        }

        /// <summary>
        /// Property: Project manager.
        /// </summary>
        public ProjectManager ProjectManager
        {
            get { return _projectManager; }
            set { _projectManager = value; }
        }
        /// <summary>
        /// Property: Team project collection uri.
        /// </summary>
        public Uri TeamProjectCollectionUri
        {
            get { return _teamProjectCollectionUri; }
            set
            {
                _teamProjectCollectionUri = value;
            }
        }

        /// <summary>
        /// Property: Team project name.
        /// </summary>
        public string TeamProjectName
        {
            get { return _teamProjectName; }
            set
            {
                _teamProjectName = value;
            }
        }

        /// <summary>
        /// Property: Credentials and team project info validate.
        /// </summary>
        public bool HaveCredentialsAndTeamProjectInfoBeenValidated
        {
            get { return _haveCredentialsAndTeamProjectInfoBeenValidated; }
            set { _haveCredentialsAndTeamProjectInfoBeenValidated = value; }
        }

        /// <summary>
        /// Property: Basic authorization REST API user profile name.
        /// </summary>
        public string BasicAuthRestAPIUserProfileName
        {
            get { return _basicAuthRestAPIUserProfileName; }
            set
            {
                _basicAuthRestAPIUserProfileName = value;
            }
        }

        /// <summary>
        /// Property: Basic authorization REST API password.
        /// </summary>
        public string BasicAuthRestAPIPassword
        {
            get { return _basicAuthRestAPIPassword; }
            set
            {
                _basicAuthRestAPIPassword = value;
            }
        }

        /// <summary>
        /// Property: Workspace name.
        /// </summary>
        public string WorkspaceName
        {
            get { return _workspaceName; }
            set { _workspaceName = value; }
        }

        /// <summary>
        /// Property: Sidekickview history list.
        /// </summary>
        public SidekickViewHistoryList SidekickViewHistoryList
        {
            get { return _sidekickViewHistoryList; }
            set { _sidekickViewHistoryList = value; }
        }

        #endregion

        #region Protected Properties

        /// <summary>
        /// Property: Sidekicks bootstrapper executable root path.
        /// </summary>
        protected string SidekicksBootstrapperExeRootPath
        {
            get
            {
                try
                {
                    _sidekicksBootstrapperExeRootPath = Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksBootstrapperExeRootPath;

                }
                catch (Exception)
                {
                    throw;
                }
                return _sidekicksBootstrapperExeRootPath;
            }
        }

        /// <summary>
        /// Property: Sidekicks bootstrapper executable name.
        /// </summary>
        protected string SidekicksBootstrapperExeName
        {
            get
            {
                try
                {
                    _sidekicksBootstrapperExeName = Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksBootstrapperExeName;

                }
                catch (Exception)
                {
                    throw;
                }
                return _sidekicksBootstrapperExeName;
            }
        }

        /// <summary>
        /// Property: Sidekicks auto updater version.
        /// </summary>
        protected string SidekicksAutoUpdaterVersion
        {
            get
            {
                try
                {
                    _sidekicksAutoUpdaterVersion = Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksAutoUpdaterVersion;

                }
                catch (Exception)
                {
                    throw;
                }
                return _sidekicksAutoUpdaterVersion;
            }
        }

        /// <summary>
        /// Property: Sidekicks auto-updater.
        /// </summary>
        protected SidekicksAutoUpdater SidekicksAutoUpdater
        {
            get
            {
                if (_sidekicksAutoUpdater == null)
                {
                    _sidekicksAutoUpdater = new SidekicksAutoUpdater();
                }
                return _sidekicksAutoUpdater;
            }
            set { _sidekicksAutoUpdater = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekickContainerForm()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            SidekickConfigurationManager sidekickConfigurationManager = null;

            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionHandler);

            try
            {
                _sidekickViewList = new SidekickViewList();
                _sidekickViewHistoryList = new SidekickViewHistoryList();


                InitializeComponent();

                sidekickConfigurationManager = SidekickConfigurationManagerFactory.CreateSidekickConfiguration("AppSettings.json", AppDomain.CurrentDomain.BaseDirectory);
                sidekickConfigurationManager.InitializeConfiguration();
                this.SidekickConfiguration = sidekickConfigurationManager.SidekickConfiguration;

                InitializeViews();
                InitializeControls();

                ConnectToTeamProject();

                this.PopulateWorkspaceComboBox();
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Team project name changed.
        /// </summary>
        /// <param name="teamProjectCollectionUri"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="userName"></param>
        public void TeamProjectNameChanged()
        {
            ISidekickView sidekickView = null;

            try
            {
                this.LoggedInAsToolStripStatusLabel.Text = string.Format("{0} {1}",
                                                                 LoggedInAs_Label,
                                                                 this.BasicAuthRestAPIUserProfileName);

                this.TeamProjectCollectionUriToolStripStatusLabel.Text = string.Format("{0} {1}",
                                                                                       TeamProjectCollectionUri_Label,
                                                                                       this.TeamProjectCollectionUri);

                this.TeamProjectNameToolStripStatusLabel.Text = string.Format("{0} {1}",
                                                                              TeamProject_Label,
                                                                              this.TeamProjectName);

                this.ConnectTeamProjectToolStripButton.BackColor = this.ShowLogToolStripButton.BackColor;
                this.VisualStudioOnlineToolStripSplitButton.Enabled = true;

                foreach (SidekickView sidekickViewItem in this.SidekickViewList)
                {
                    sidekickView = (ISidekickView)sidekickViewItem.Form;
                    sidekickView.TeamProjectChanged();
                }

                //if (SwitchLocalDevBranchForm != null)
                //{
                //    this.SwitchLocalDevBranchForm.TeamProjectChanged();
                //}

                //if (BuildSolutionListForm != null)
                //{
                //    this.BuildSolutionListForm.TeamProjectChanged();
                //}

                //if (AzureRedisCacheAdminForm != null)
                //{
                //    this.AzureRedisCacheAdminForm.TeamProjectChanged();
                //}

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Activate sidekick view.
        /// </summary>
        /// <param name="sidekickViewList"></param>
        public void ActivateSidekickView(SidekickView sidekickView)
        {
            SidekickSelectionChangedEventArgs sidekickSelectionChangedEventArgs = null;

            try
            {
                ChangeSidekickContainerFormTitle(sidekickView);

                UpdateSplitButtonSidekicksToolStripMenuItemChecked(sidekickView);

                UpdateViewSidekicksToolStripMenuItemChecked(sidekickView);

                this.SidekickContainerPanel.DockControl(sidekickView.Form);

                if (SidekickSelectionChanged != null)
                {
                    sidekickSelectionChangedEventArgs = new SidekickSelectionChangedEventArgs(sidekickView.Name, sidekickView.Description);
                    this.SidekickSelectionChanged(this, sidekickSelectionChangedEventArgs);
                }

                this.SidekickViewHistoryList.Add(sidekickView);
                this.UpdateBackAndForwardButton();
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Update back and forward buttons.
        /// </summary>
        protected void UpdateBackAndForwardButton()
        {

            try
            {
                if (this.SidekickViewHistoryList.IsBackAvailable() == true)
                {
                    this.BackToolStripButton.Enabled = true;
                }
                else
                {
                    this.BackToolStripButton.Enabled = false;
                }

                if (this.SidekickViewHistoryList.IsForwardAvailable() == true)
                {
                    this.ForwardToolStripButton.Enabled = true;
                }
                else
                {
                    this.ForwardToolStripButton.Enabled = false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Initialize views.
        /// </summary>
        protected void InitializeViews()
        {

            WebAppSearchForm webAppSearchForm = null;
            SolutionListSearchForm solutionListSearchForm = null;
            NuGetPackageSearchForm nuGetPackageSearchForm = null;
            bool defaultView = false;
            Form sidekickForm = null;
            ISidekickInfo sidekickInfo = null;
            bool hideApplicationsSiteLevel = false;

            try
            {

                this.LogForm = LogFormFactory.CreateForm(this);
                this.LogForm.TopLevel = false;

                InitializeLogWriter();

                //Add Azure redis cache admin view.
                defaultView = false;
                hideApplicationsSiteLevel = true;
                webAppSearchForm = WebAppSearchFormFactory.CreateForm(this,
                                                                      this.SidekickConfiguration,
                                                                      WebAppSearchTypes.WebAppSelectability.AlwaysSelectable,
                                                                      hideApplicationsSiteLevel);
                sidekickForm = AzureRedisCacheAdminFormFactory.CreateForm(this,
                                                                          this.SidekickConfiguration);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_Utility, SidekickCategory_Utility)));

                //Add switch local dev branch view.
                defaultView = false;
                webAppSearchForm = WebAppSearchFormFactory.CreateForm(this,
                                                                      this.SidekickConfiguration,
                                                                      WebAppSearchTypes.WebAppSelectability.AlwaysSelectable,
                                                                      hideApplicationsSiteLevel);
                sidekickForm = SwitchLocalDevBranchFormFactory.CreateForm(this,
                                                                          this.SidekickConfiguration,
                                                                          webAppSearchForm);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_Branch, SidekickCategory_Branch)));

                //Add build solution list view.
                defaultView = false;
                solutionListSearchForm = SolutionListSearchFormFactory.CreateForm(this,
                                                                      this.SidekickConfiguration);
                sidekickForm = BuildSolutionListFormFactory.CreateForm(this.SidekickConfiguration,
                                                                       solutionListSearchForm);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_Build, SidekickCategory_Build)));

                //Add NuGet package analyzer view.
                defaultView = false;
                nuGetPackageSearchForm = NuGetPackageSearchFormFactory.CreateForm(this,
                                                                      this.SidekickConfiguration);
                sidekickForm = NuGetPackageAnalyzerFormFactory.CreateForm(this.SidekickConfiguration,
                                                                          nuGetPackageSearchForm);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_Utility, SidekickCategory_Utility)));


                //--------------------------------------------------------------------------------------------------------------------------------------------------
                //NOTE: Home sidekick view is added after all other sidekick views inorder to render sidekick view navigation.
                //--------------------------------------------------------------------------------------------------------------------------------------------------

                //Add home sidekick view.
                defaultView = true;
                sidekickForm = HomeFormFactory.CreateForm(this, this.SidekickConfiguration);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.HomeSidekickView = new SidekickView(sidekickInfo.SidekickName,
                                                         sidekickInfo.SidekickDescription,
                                                         sidekickForm,
                                                         defaultView,
                                                         new SidekickCategory(SidekickCategory_Home, SidekickCategory_Home));
                this.SidekickViewList.Add(this.HomeSidekickView);

                this.SidekickContainerPanel.DockControl(this.SidekickViewList.Where(skv => skv.DefaultView == true).FirstOrDefault().Form);

                this.LogPanel.DockControl(this.LogForm);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {

            try
            {

                //Populate auto-updater state in UI components.
                PopulateUIComponentsWithAutoUpdaterState();

                //Populate sidekick view list in UI components.
                PopulateUIComponentsWithSidekickViewList(this.SidekickViewList);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Populate UI components with auto-updater state.
        /// </summary>
        protected void PopulateUIComponentsWithAutoUpdaterState()
        {
            try
            {
                this.AutoUpdaterToolStripLabel.Visible = false;
                this.AutoUpdaterToolStripButton.Visible = false;

                //Retrieve sidekicks auto-updater.
                this.SidekicksAutoUpdater = this.GetSidekicksAutoUpdater();

                //A new sidekicks auto-updater version is available.
                if (this.SidekicksAutoUpdater.Status.RunState == RunState.OK &&
                    this.SidekicksAutoUpdater.IsNewVersionAvailable(this.SidekicksAutoUpdaterVersion) == true)
                {
                    this.AutoUpdaterToolStripLabel.Visible = true;
                    this.AutoUpdaterToolStripButton.Visible = true;
                }
                else if (this.SidekicksAutoUpdater.Status.RunState == RunState.OK &&
                         this.SidekicksAutoUpdater.IsNewVersionAvailable(this.SidekicksAutoUpdaterVersion) == false)
                {
                    this.AutoUpdaterToolStripLabel.Visible = true;
                    this.AutoUpdaterToolStripButton.Visible = false;

                    this.AutoUpdaterToolStripLabel.Text = SidekicksAutoUpdater_UpToDate;
                }

                //Sidekicks auto-updater failed.
                if (this.SidekicksAutoUpdater.Status.RunState == RunState.Failed)
                {
                    this.AutoUpdaterToolStripLabel.Visible = true;
                    this.AutoUpdaterToolStripButton.Visible = true;

                    this.AutoUpdaterToolStripButton.Text = SidekicksAutoUpdater_MoreInfo;
                    this.AutoUpdaterToolStripLabel.Text = SidekicksAutoUpdater_UpdaterFailed;
                }

            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Initialize log writer.
        /// </summary>
        protected void InitializeLogWriter()
        {
            //target
            ConfigurationItemFactory.Default.Targets
                                    .RegisterDefinition("LogEventTarget", typeof(Aclara.VsoLocalDev.Sidekicks.WinForm.Logging.LogEventTarget));
            var target = (LogEventTarget)LogManager.Configuration.FindTargetByName("logevent");
            target.LogEntryCreated += this.LogForm.LogEntryCreate;

        }

        /// <summary>
        /// Populate UI components with sidekick view list.
        /// </summary>
        protected void PopulateUIComponentsWithSidekickViewList(SidekickViewList sidekickViewList)
        {
            SidekickView defaultSidekickView = null;
            ToolStripMenuItem splitButtonParentToolStripMenuItem = null;
            ToolStripMenuItem splitButtonChildToolStripMenuItem = null;
            ToolStripMenuItem viewMenuParentToolStripMenuItem = null;
            ToolStripMenuItem viewMenuChildToolStripMenuItem = null;

            try
            {

                //Populate view menu items and sidekick selector split button.
                var categories = sidekickViewList.OrderBy(skv => skv.SidekickCategory.Description).Select(skv => skv.SidekickCategory);
                foreach (var category in categories.DistinctBy(c => c.Description))
                {
                    splitButtonParentToolStripMenuItem = new ToolStripMenuItem();
                    splitButtonParentToolStripMenuItem.Name = category.Name;
                    splitButtonParentToolStripMenuItem.Text = category.Description;
                    splitButtonParentToolStripMenuItem.ToolTipText = category.Description;
                    this.MainSidekickSelectorContextMenuStrip.Items.Add(splitButtonParentToolStripMenuItem);

                    viewMenuParentToolStripMenuItem = new ToolStripMenuItem();
                    viewMenuParentToolStripMenuItem.Name = category.Name;
                    viewMenuParentToolStripMenuItem.Text = category.Description;
                    viewMenuParentToolStripMenuItem.ToolTipText = category.Description;
                    this.SidekicksSidekickContainerToolStripMenuItem.DropDownItems.Add(viewMenuParentToolStripMenuItem);

                    foreach (var sidekickView in sidekickViewList.Where(skv => skv.SidekickCategory.Name == category.Name).OrderBy(skv => skv.SidekickCategory))
                    {
                        splitButtonChildToolStripMenuItem = new ToolStripMenuItem();
                        splitButtonChildToolStripMenuItem.Name = sidekickView.Name;
                        splitButtonChildToolStripMenuItem.Text = sidekickView.Description;
                        splitButtonChildToolStripMenuItem.ToolTipText = sidekickView.Description;
                        splitButtonChildToolStripMenuItem.Click += new EventHandler(ViewSidekicksSidekickViewToolStripMenuItem_Click);
                        splitButtonParentToolStripMenuItem.DropDownItems.Add(splitButtonChildToolStripMenuItem);

                        viewMenuChildToolStripMenuItem = new ToolStripMenuItem();
                        viewMenuChildToolStripMenuItem.Name = sidekickView.Name;
                        viewMenuChildToolStripMenuItem.Text = sidekickView.Description;
                        viewMenuChildToolStripMenuItem.ToolTipText = sidekickView.Description;
                        viewMenuChildToolStripMenuItem.Click += new EventHandler(ViewSidekicksSidekickViewToolStripMenuItem_Click);
                        viewMenuParentToolStripMenuItem.DropDownItems.Add(viewMenuChildToolStripMenuItem);

                    }
                }

                //Set default sidekick view.
                defaultSidekickView = sidekickViewList.Single(skv => skv.DefaultView == true);
                ActivateSidekickView(defaultSidekickView);

            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Show sidekick view history list context menu.
        /// </summary>
        protected void ShowSidekickViewHistoryListContextMenu()
        {
            List<SidekickViewHistory> sidekickViewHistories = null;
            ToolStripMenuItem toolStripMenuItem = null;

            try
            {

                sidekickViewHistories = this.SidekickViewHistoryList.GetSidekickViewHistoryList();

                var sortedSidekickViewHistoryList = sidekickViewHistories.OrderBy(skv => skv.SequenceNumber);

                this.SidekickViewHistoryContextMenuStrip.Items.Clear();
                foreach (SidekickViewHistory category in sortedSidekickViewHistoryList)
                {
                    toolStripMenuItem = new ToolStripMenuItem();
                    toolStripMenuItem.Name = category.SidekickView.Name;
                    toolStripMenuItem.Text = category.SidekickView.Description;
                    toolStripMenuItem.ToolTipText = category.SidekickView.Description;
                    toolStripMenuItem.Click += new EventHandler(ViewSidekicksSidekickViewToolStripMenuItem_Click);

                    this.SidekickViewHistoryContextMenuStrip.Items.Add(toolStripMenuItem);

                }

                this.SidekickViewHistoryContextMenuStrip.Show(Cursor.Position);
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }


        /// <summary>
        /// Retrieve sidekicks auto-updater.
        /// </summary>
        protected SidekicksAutoUpdater GetSidekicksAutoUpdater()
        {

            SidekicksAutoUpdater result = null;
            SidekicksAutoUpdaterManager sidekicksAutoUpdaterManager = null;
            string bootstrapperExeRootPath = string.Empty;
            string bootstrapperExeName = string.Empty;

            try
            {
                sidekicksAutoUpdaterManager = SidekicksAutoUpdaterManagerFactory.CreateSidekicksUpdateManager();

                //Retrieve sidekicks auto-updater.
                bootstrapperExeRootPath = this.SidekicksBootstrapperExeRootPath;
                bootstrapperExeName = this.SidekicksBootstrapperExeName;
                result = sidekicksAutoUpdaterManager.GetSidekicksAutoUpdater(this.SidekicksBootstrapperExeRootPath,
                                                                            bootstrapperExeName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Update split button - sidekicks tool strip menu item checked.
        /// </summary>
        /// <param name="sidekickView"></param>
        protected void UpdateSplitButtonSidekicksToolStripMenuItemChecked(SidekickView sidekickView)
        {
            try
            {
                foreach (ToolStripMenuItem parentToolStripMenuItem in this.MainSidekickSelectorToolStripSplitButton.DropDownItems)
                {
                    foreach (ToolStripMenuItem childToolStripMenitem in parentToolStripMenuItem.DropDownItems)
                        if (sidekickView.Name == childToolStripMenitem.Name)
                        {
                            childToolStripMenitem.Checked = true;
                        }
                        else
                        {
                            childToolStripMenitem.Checked = false;
                        }

                }
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Change sidekick container form title.
        /// </summary>
        /// <param name="sidekickName"></param>
        protected void ChangeSidekickContainerFormTitle(SidekickView sidekickView)
        {
            try
            {
                this.Text = string.Format("{0} - {1}",
                                  sidekickView.Description,
                                  SidekickContainerFormTitleSuffix);
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Update view - sidekicks tool strip menu item checked.
        /// </summary>
        /// <param name="sidekickView"></param>
        protected void UpdateViewSidekicksToolStripMenuItemChecked(SidekickView sidekickView)
        {
            try
            {
                foreach (ToolStripMenuItem parentToolStripMenuItem in this.SidekicksSidekickContainerToolStripMenuItem.DropDownItems)
                {
                    foreach (ToolStripMenuItem childToolStripMenitem in parentToolStripMenuItem.DropDownItems)
                        if (sidekickView.Name == childToolStripMenitem.Name)
                        {
                            childToolStripMenitem.Checked = true;
                        }
                        else
                        {
                            childToolStripMenitem.Checked = false;
                        }

                }
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }


        /// <summary>
        /// Retrieve busy sidekick list.
        /// </summary>
        /// <returns></returns>
        protected List<string> GetBusySidekickList()
        {
            List<string> result = null;
            ISidekickView sidekickView = null;
            ISidekickInfo sidekickInfo = null;

            try
            {
                result = new List<string>();

                foreach (SidekickView sidekickViewItem in this.SidekickViewList)
                {
                    sidekickView = (ISidekickView)sidekickViewItem.Form;
                    sidekickInfo = (ISidekickInfo)sidekickViewItem.Form;

                    if (sidekickView.IsSidekickBusy == true)
                    {
                        result.Add(sidekickInfo.SidekickDescription);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Connect to team project.
        /// </summary>
        protected void ConnectToTeamProject()
        {
            DialogResult dialogResult;
            string teamProjectCollectionVsrmUrl = string.Empty;

            try
            {

                this.HaveCredentialsAndTeamProjectInfoBeenValidated = false;
                if (AreCredentialsAndTeamProjectInfoValid() == false)
                {
                    dialogResult = PromptTeamProjectAndCredentialsExtended();
                    if (dialogResult != DialogResult.OK)
                    {
                        return;
                    }
                }

                this.TeamProjectCollectionUri = new Uri(Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectCollection);
                this.TeamProjectName = Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectName;
                this.BasicAuthRestAPIUserProfileName = Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksUserProfileName;
                this.BasicAuthRestAPIPassword = GetAndDecryptPersonalAccessToken();

                this.HaveCredentialsAndTeamProjectInfoBeenValidated = true;

                this.WorkspaceName = Environment.MachineName;

                this.TeamProjectNameChanged();

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Determine whether credentials and team project info are valid.
        /// </summary>
        /// <returns></returns>
        protected bool AreCredentialsAndTeamProjectInfoValid()
        {
            bool result = false;
            try
            {
                if (string.IsNullOrEmpty(Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectCollection) == true ||
                    string.IsNullOrEmpty(Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectName) == true ||
                    string.IsNullOrEmpty(Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksUserProfileName) ||
                    string.IsNullOrEmpty(GetAndDecryptPersonalAccessToken()) == true)
                {
                    result = false;
                    return result;
                }
                else
                {
                    result = ValidateCredentials();
                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Prompt for team project and credentials (Extended).
        /// </summary>
        /// <returns></returns>
        protected DialogResult PromptTeamProjectAndCredentialsExtended()
        {
            DialogResult result = DialogResult.Cancel;
            VSTSProjectConnectForm vstsProjectConnectForm = null;
            Uri teamProjectCollectionUri;
            string teamProjectName = string.Empty;
            string basicAuthRestAPIUserProfileName = string.Empty;
            string basicAuthRestAPIPassword = string.Empty;
            string teamProjectCollectionDefault = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectCollection) == true)
                {
                    teamProjectCollectionDefault = this.SidekickConfiguration.VSTSAccountItemList.VSTSAccountItem.Where(vai => vai.Default == true).FirstOrDefault().URL;
                    teamProjectCollectionUri = new Uri(teamProjectCollectionDefault);
                }
                else
                {
                    teamProjectCollectionUri = new Uri(Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectCollection);
                }

                teamProjectName = Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectName;
                basicAuthRestAPIUserProfileName = Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksUserProfileName;
                basicAuthRestAPIPassword = GetAndDecryptPersonalAccessToken();

                vstsProjectConnectForm = VSTSProjectConnectFormFactory.CreateForm(teamProjectCollectionUri, teamProjectName, basicAuthRestAPIUserProfileName, basicAuthRestAPIPassword);

                vstsProjectConnectForm.StartPosition = FormStartPosition.CenterParent;
                result = vstsProjectConnectForm.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    this.TeamProjectCollectionUri = vstsProjectConnectForm.TeamProjectCollectionUri;
                    this.TeamProjectName = vstsProjectConnectForm.TeamProjectName;
                    this.BasicAuthRestAPIUserProfileName = vstsProjectConnectForm.BasicAuthRestAPIUserProfileName;
                    this.BasicAuthRestAPIPassword = vstsProjectConnectForm.BasicAuthRestAPIPassword;

                    Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectCollection = vstsProjectConnectForm.TeamProjectCollectionUri.ToString();
                    Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectName = vstsProjectConnectForm.TeamProjectName;
                    Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksUserProfileName = vstsProjectConnectForm.BasicAuthRestAPIUserProfileName;
                    this.EncryptAndSavePersonalAccessToken(vstsProjectConnectForm.BasicAuthRestAPIPassword);

                }

            }
            catch (Exception)
            {
            }

            return result;
        }

        /// <summary>
        /// Validate credentials.
        /// </summary>
        /// <returns></returns>
        protected bool ValidateCredentials()
        {

            bool result = false;
            VSTSProjectConnectForm vstsProjectConnectForm = null;
            Uri teamProjectCollectionUri;
            string teamProjectName = string.Empty;
            string basicAuthRestAPIUserProfileName = string.Empty;
            string basicAuthRestAPIPassword = string.Empty;
            string teamProjectCollectionDefault = string.Empty;

            try
            {

                if (string.IsNullOrEmpty(Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectCollection) == true)
                {
                    teamProjectCollectionDefault = this.SidekickConfiguration.VSTSAccountItemList.VSTSAccountItem.Where(vai => vai.Default == true).FirstOrDefault().URL;
                    teamProjectCollectionUri = new Uri(teamProjectCollectionDefault);
                }
                else
                {
                    teamProjectCollectionUri = new Uri(Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectCollection);
                }

                teamProjectName = Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksTeamProjectName;
                basicAuthRestAPIUserProfileName = Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksUserProfileName;
                basicAuthRestAPIPassword = GetAndDecryptPersonalAccessToken();

                vstsProjectConnectForm = VSTSProjectConnectFormFactory.CreateForm(teamProjectCollectionUri, teamProjectName, basicAuthRestAPIUserProfileName, basicAuthRestAPIPassword);

                vstsProjectConnectForm.ValidateCredentials();
                if (vstsProjectConnectForm.CredentialsValidationStatus == VSTSProjectConnectForm.CredentialsValidationStatusCode.CredentialsAreValid)
                {
                    result = true;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Encrypt and save personal access token.
        /// </summary>
        /// <param name="personalAccessToken"></param>
        /// <returns></returns>
        protected void EncryptAndSavePersonalAccessToken(string personalAccessToken)
        {
            string encryptedPersonalAccessToken = string.Empty;
            ProtectedDataManager protectedDataManager = null;

            try
            {
                protectedDataManager = new ProtectedDataManager();

                encryptedPersonalAccessToken = protectedDataManager.EncryptString(personalAccessToken);

                //Persist personal access token.
                Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksPersonalAccessToken = encryptedPersonalAccessToken;
                Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.Save();

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve and decrypt personal access token.
        /// </summary>
        /// <param name="personalAccessToken"></param>
        /// <returns></returns>
        protected string GetAndDecryptPersonalAccessToken()
        {
            string result = string.Empty;
            string encryptedPersonalAccessToken = string.Empty;
            ProtectedDataManager protectedDataManager = null;

            try
            {
                protectedDataManager = new ProtectedDataManager();

                //Retrieve personal access token.
                encryptedPersonalAccessToken = Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Settings.Default.SidekicksPersonalAccessToken;

                try
                {
                    //Decrypt personal access token.
                    result = protectedDataManager.DecryptString(encryptedPersonalAccessToken);
                }
                catch (Exception)
                {
                    //Unable to decrypt.
                    result = string.Empty;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Create project manager.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        protected ProjectManager CreateProjectManager(string tfsServerAddress,
                                                      string teamProjectName)
        {
            ProjectManager result = null;
            Uri tfsServerUri = null;

            try
            {

                tfsServerUri = new Uri(tfsServerAddress);

                result = ProjectManagerFactory.CreateProjectManager(tfsServerUri, teamProjectName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Populate workspace list in UI.
        /// </summary>
        protected void PopulateWorkspaceComboBox()
        {
            List<string> computerList = null;

            try
            {

                computerList = new List<string>();
                computerList.Add(Environment.MachineName);

                this.ProjectManager = this.CreateProjectManager(this.TeamProjectCollectionUri.ToString(), this.TeamProjectName);

                var workspaceList = this.ProjectManager.GetWorkspaces(string.Empty, computerList);
                this.WorkspaceToolStripComboBox.ComboBox.DataSource = workspaceList;
                this.WorkspaceToolStripComboBox.ComboBox.DisplayMember = "Name";
                this.WorkspaceToolStripComboBox.ComboBox.Text = string.Empty;

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Unhandled exception - handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        static private void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
            exceptionForm.ShowDialog();
        }

        /// <summary>
        /// Event Handler: Exit sidekick container tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitSidekickContainerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Event Handler: About <app> click.</app>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void AboutSidekickContainerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBoxForm aboutBox = null;

            try
            {
                aboutBox = new AboutBoxForm();

                aboutBox.Show(this);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler:Help tool strip button - Click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpToolStripButton_Click(object sender, EventArgs e)
        {

            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, this.SidekickHelpProvider.GetHelpKeyword(this));

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Help sidekick container tool strip menu item - Click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpSidekickContainerToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, this.SidekickHelpProvider.GetHelpKeyword(this));

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Change log sidekick container tool strip menu item - Click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeLogSidekickContainerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string changeLogFilePath = string.Empty;
            string executablePath = string.Empty;

            try
            {
                executablePath = Path.GetDirectoryName(Application.ExecutablePath);
                changeLogFilePath = Path.Combine(executablePath, ChangeLogFileName);

                System.Diagnostics.Process.Start(changeLogFilePath);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }


        }

        /// <summary>
        /// Event Handler: Show log tool strip button - Click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowLogToolStripButton_Click(object sender, EventArgs e)
        {

            ToolStripButton toolStripButton = null;

            try
            {
                if (sender is ToolStripButton)
                {
                    toolStripButton = (ToolStripButton)sender;
                    if (toolStripButton.Checked == true)
                    {
                        this.LogPanel.Hide();
                        toolStripButton.Checked = false;
                        this.ShowLogToolStripMenuItem.Checked = false;
                        toolStripButton.Text = ShowLogToolStripButton_ShowLog;
                    }
                    else
                    {
                        this.LogPanel.Show();
                        toolStripButton.Checked = true;
                        this.ShowLogToolStripMenuItem.Checked = true;
                        toolStripButton.Text = ShowLogToolStripButton_HideLog;
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Show log tool strip button - Click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowLogToolStripMenuItem_Click(object sender, EventArgs e)
        {

            ToolStripMenuItem toolStripMenuItem = null;

            try
            {
                if (sender is ToolStripMenuItem)
                {
                    toolStripMenuItem = (ToolStripMenuItem)sender;
                    if (toolStripMenuItem.Checked == true)
                    {
                        this.LogPanel.Hide();
                        toolStripMenuItem.Checked = false;
                        this.ShowLogToolStripButton.Text = ShowLogToolStripButton_ShowLog;

                    }
                    else
                    {
                        this.LogPanel.Show();
                        toolStripMenuItem.Checked = true;
                        this.ShowLogToolStripButton.Text = ShowLogToolStripButton_HideLog;
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Auto-updater tool strip button - Click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoUpdaterToolStripButton_Click(object sender, EventArgs e)
        {
            string stdout = string.Empty;
            string stderr = string.Empty;

            try
            {
                if (this.SidekicksAutoUpdater.Status.RunState == RunState.OK ||
                   this.SidekicksAutoUpdater.Status.RunState == RunState.Warning)
                {
                    this.SidekicksAutoUpdater.Run(out stdout, out stderr);

                    Application.Exit();
                }
                if (this.SidekicksAutoUpdater.Status.RunState == RunState.Failed)
                {
                    ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(this.SidekicksAutoUpdater.Status.Exception, this.SidekicksAutoUpdater.AutoUpdaterInfo);
                    exceptionForm.ShowDialog();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Check for new version menu item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void CheckForNewVersionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string stdout = string.Empty;
            string stderr = string.Empty;
            try
            {
                //Retrieve sidekicks auto-updater.
                this.SidekicksAutoUpdater = this.GetSidekicksAutoUpdater();



                //A new sidekicks auto-updater version is available.
                if (this.SidekicksAutoUpdater.Status.RunState == RunState.OK &&
                    this.SidekicksAutoUpdater.IsNewVersionAvailable(this.SidekicksAutoUpdaterVersion) == true)
                {
                    if (MessageBox.Show(SidekicksAutoUpdater_NewVersionAvailablePrompt,
                                        SidekickContainerFormTitleSuffix,
                                        MessageBoxButtons.YesNoCancel,
                                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        this.SidekicksAutoUpdater.Run(out stdout, out stderr);
                        Application.Exit();
                    }
                }
                else if (this.SidekicksAutoUpdater.Status.RunState == RunState.Failed)
                {
                    ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(this.SidekicksAutoUpdater.Status.Exception, this.SidekicksAutoUpdater.AutoUpdaterInfo);
                    exceptionForm.ShowDialog();
                }
                else
                {
                    MessageBox.Show(SidekicksAutoUpdater_CurrentVersionAlreadyInstalledPrompt,
                                    SidekickContainerFormTitleSuffix,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Connect team project tool strip button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConnectTeamProjectToolStripButton_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = DialogResult.Cancel;

            try
            {
                dialogResult = PromptTeamProjectAndCredentialsExtended();
                if (dialogResult != DialogResult.OK)
                {
                    return;
                }

                this.WorkspaceName = Environment.MachineName;

                TeamProjectNameChanged();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Shwo log file tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowLogFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileTarget fileTarget = null;
            string logFileName = string.Empty;
            string logFilePath = string.Empty;
            string executablePath = string.Empty;
            LogEventInfo logEventInfo = null;

            try
            {
                //Retrive NLog FileTarget from configuration.
                fileTarget = (FileTarget)LogManager.Configuration.FindTargetByName("LogFile");

                if (fileTarget == null)
                {
                    MessageBox.Show(string.Format(ShowLogFileErrorMessage_CouldNotLocateLogFile));
                    return;
                }

                logEventInfo = new LogEventInfo();
                logFileName = fileTarget.FileName.Render(logEventInfo);

                if (logFileName == null)
                {
                    MessageBox.Show(string.Format(ShowLogFileErrorMessage_CouldNotLocateLogFile));
                    return;
                }

                executablePath = Path.GetDirectoryName(Application.ExecutablePath);
                logFilePath = Path.Combine(executablePath, logFileName);

                if (File.Exists(logFilePath) == false)
                {
                    MessageBox.Show(string.Format(ShowLogFileErrorMessage_CouldNotLocateLogFile));
                    return;
                }

                System.Diagnostics.Process.Start(logFilePath);


            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: SidekickContainerForm - Form closing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void SidekickContainerForm_FormClosing(object sender, FormClosingEventArgs e)
        {

            string busySidekicks = string.Empty;
            List<string> busySidekickList = null;
            string message = string.Empty;

            try
            {

                //Retrieve busy sidekick list.
                busySidekickList = GetBusySidekickList();

                //Sidekick is still running. Not safe to exit application.
                if (busySidekickList.Count > 0)
                {

                    busySidekicks = string.Join(Environment.NewLine, busySidekickList.ToArray());
                    message = String.Format(ApplicationExit_SidekickStillRunningForceExitPrompt,
                                            busySidekicks);
                    if (MessageBox.Show(this,
                                        message,
                                        SidekickContainerFormTitleSuffix,
                                        MessageBoxButtons.YesNoCancel,
                                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        Application.Exit();
                    }
                    else
                    {
                        e.Cancel = true;
                    }

                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Home tool strip button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HomeToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.ActivateSidekickView(this.HomeSidekickView);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: View sidekicks - sidekick view menu item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewSidekicksSidekickViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = null;

            try
            {
                menuItem = (ToolStripMenuItem)sender;
                var sidekickView = this.SidekickViewList.Single(skv => skv.Description == menuItem.Text);
                if (sidekickView == null)
                {
                    return;
                }
                this.ActivateSidekickView(sidekickView);

            }
            catch (Exception ex)
            {
                string context = string.Format("TBD");
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex, context);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Main sidekick selector tool strip split button - Button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainSidekickSelectorToolStripSplitButton_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                this.MainSidekickSelectorToolStripSplitButton.ShowDropDown();
            }
            catch (Exception ex)
            {
                string context = string.Format("TBD");
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex, context);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Workspace tool strip combo box - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkspaceToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Workspace workspace = null;

            try
            {
                if (this.WorkspaceToolStripComboBox.ComboBox.SelectedItem.GetType() == typeof(Workspace))
                {

                    workspace = (Workspace)this.WorkspaceToolStripComboBox.ComboBox.SelectedItem;
                    if (workspace != null)
                    {
                        this.WorkspaceName = workspace.Name;
                        this.TeamProjectNameChanged();
                    }
                }
            }
            catch (Exception ex)
            {
                string context = string.Format("TBD");
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex, context);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Back tool strip button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackToolStripButton_Click(object sender, EventArgs e)
        {
            SidekickViewHistory sidekickViewHistory = null;

            try
            {
                if (this.SidekickViewHistoryList.IsBackAvailable() == true)
                {
                    sidekickViewHistory = this.SidekickViewHistoryList.Back();
                    this.ActivateSidekickView(sidekickViewHistory.SidekickView);
                }

            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Back tool strip button - Mouse down.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackToolStripButton_MouseDown(object sender, MouseEventArgs e)
        {

            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    this.ShowSidekickViewHistoryListContextMenu();
                }
            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Forward tool strip button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ForwardToolStripButton_Click(object sender, EventArgs e)
        {
            SidekickViewHistory sidekickViewHistory = null;

            try
            {
                if (this.SidekickViewHistoryList.IsForwardAvailable() == true)
                {
                    sidekickViewHistory = this.SidekickViewHistoryList.Forward();
                    this.ActivateSidekickView(sidekickViewHistory.SidekickView);
                }
            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Forward tool strip button - Mouse down.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ForwardToolStripButton_MouseDown(object sender, MouseEventArgs e)
        {

            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    this.ShowSidekickViewHistoryListContextMenu();
                }
            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

        /// <summary>
        /// Event Handler: Visual team services studio connected project tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        

        private void VisualStudioTeamServicesConnectedProjectOnlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string link = string.Empty;

            try
            {

                link = string.Format("{0}/{1}",
                                     this.TeamProjectCollectionUri.AbsoluteUri,
                                     this.TeamProjectName);
                System.Diagnostics.Process.Start(link);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Visual team services studio manage repositories tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        private void VisualStudioTeamServicesManageRepositoriesOnlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string link = string.Empty;

            try
            {

                link = string.Format("{0}/{1}/_admin/_versionControl",
                                     this.TeamProjectCollectionUri.AbsoluteUri,
                                     this.TeamProjectName);
                System.Diagnostics.Process.Start(link);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        private void VisualStudioOnlineToolStripSplitButton_Click(object sender, EventArgs e)
        {
            VisualStudioOnlineToolStripSplitButton.ShowDropDown();
        }
    }

    /// <summary>
    /// Panel control extensions.
    /// </summary>
    public static class PanelExtensions
    {
        /// <summary>
        /// Dock control.
        /// </summary>
        /// <param name="thisControl"></param>
        /// <param name="controlToDock"></param>
        public static void DockControl(this Panel thisControl,
                                       Control controlToDock)
        {
            thisControl.Controls.Clear();
            thisControl.Controls.Add(controlToDock);
            controlToDock.Dock = DockStyle.Fill;
            controlToDock.Show();
        }
    }

}
