﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    public static class VSTSProjectConnectFormFactory
    {
        #region Public Methods

        /// <summary>
        /// Create form.
        /// </summary>
        /// <returns></returns>
        public static VSTSProjectConnectForm CreateForm()
        {
            VSTSProjectConnectForm result = null;
            VSTSProjectConnectPresenter vstsProjectConnectPresenter = null;

            try
            {
                vstsProjectConnectPresenter = new VSTSProjectConnectPresenter();
                result = new VSTSProjectConnectForm();
                result.VSTSProjectConnectPresenter = vstsProjectConnectPresenter;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="teamProjectCollectionUri"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="basicAuthRestAPIUserProfileName"></param>
        /// <param name="basicAuthRestAPIPassword"></param>
        /// <returns></returns>
        public static VSTSProjectConnectForm CreateForm(Uri teamProjectCollectionUri,
                                                        string teamProjectName,
                                                        string basicAuthRestAPIUserProfileName, 
                                                        string basicAuthRestAPIPassword)
        {
            VSTSProjectConnectForm result = null;
            VSTSProjectConnectPresenter vstsProjectConnectPresenter = null;

            try
            {
                vstsProjectConnectPresenter = new VSTSProjectConnectPresenter();

                result = new VSTSProjectConnectForm();

                result.TeamProjectCollectionUri = teamProjectCollectionUri;
                result.TeamProjectName = teamProjectName;
                result.BasicAuthRestAPIUserProfileName = basicAuthRestAPIUserProfileName;
                result.BasicAuthRestAPIPassword = basicAuthRestAPIPassword;
                result.VSTSProjectConnectPresenter = vstsProjectConnectPresenter;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion
    }
}
