﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Web app configuration search form factory.
    /// </summary>
    public class WebAppConfigSearchFormFactory
    {

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public WebAppConfigSearchFormFactory()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create web app search form.
        /// </summary>
        /// <param name="selectableWebAppCriteria"></param>
        /// <param name="hideApplicationsSiteLevel"></param>
        /// <returns></returns>
        static public WebAppConfigSearchForm CreateForm(SidekickConfiguration sidekickConfiguration)
        {

            WebAppConfigSearchForm result = null;
            WebAppConfigSearchPresenter webAppSearchPresenter = null;

            try
            {

                result = new WebAppConfigSearchForm(sidekickConfiguration);
                webAppSearchPresenter = new WebAppConfigSearchPresenter(sidekickConfiguration, result);

                result.WebAppConfigSearchPresenter = webAppSearchPresenter;
                result.TopLevel = false;

                webAppSearchPresenter.Initialize();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion
    }
}
