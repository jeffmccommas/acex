﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    public class HomeFormFactory
    {
        /// <summary>
        /// Create form.
        /// </summary>
        /// <returns></returns>
        public static HomeForm CreateForm(SidekickContainerForm sidekickContainerForm, SidekickConfiguration sidekickConfiguration)
        {

            HomeForm result = null;
            HomePresenter HomePresenter = null;

            try
            {
                result = new HomeForm(sidekickContainerForm, sidekickConfiguration);
                HomePresenter = new HomePresenter(sidekickConfiguration, result);
                result.HomePresenter = HomePresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
