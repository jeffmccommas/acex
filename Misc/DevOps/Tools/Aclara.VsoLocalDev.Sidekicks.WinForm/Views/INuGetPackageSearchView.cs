﻿using Aclara.NuGet.Core.Client.Models;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm
{
    /// <summary>
    /// 
    /// </summary>
    public interface INuGetPackageSearchView
    {

        string BackgroundTaskStatus { get; set; }

        void ApplyButtonEnable(bool enable);

        bool IsSidekickBusy { get; }

        void TeamProjectChanged();
        
        void PopulateBranchNameList(List<string> branchNameList);

        void EnableControlsDependantOnTeamProject(bool enabled);

        NuGetPackageInfoList NuGetPackageInfoList { get; set; }

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);

        void UpdateBackgroundTaskStatus(string backgroundTaskStatus);
    }
}
