﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Events;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using Aclara.Tools.Configuration.AzureRedisCacheConfig;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static Aclara.AzureRedisCache.Client.Types.Enumerations;


namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    public partial class AzureRedisCacheAdminForm : Form, IAzureRedisCacheAdminView, ISidekickInfo, ISidekickView
    {

        #region Private Constants

        private const string SidekickView_SidekickName = "AzureRedisCacheAdmin";
        private const string SidekickView_SidekickDescription = "Azure Redis Cache Admin";

        private const string BuildErrorsDataGridView_SeverityColumn = "SeverityColumn";
        private const string BuildErrorsDataGridView_SeverityIconColumn = "SeverityIconColumn";
        private const string BuildErrorsDataGridView_MessageColumn = "MessageColumn";

        private const string BuildErrorsDataGridView_Severity = "Severity";
        private const string BuildErrorsDataGridView_Message = "Message";

        private const string LogEventInfoLevel_Error = "Error";
        private const string LogEventInfoLevel_Warn = "Warn";

        private const string LogEventInfoDisplayLevel_Error = "Error";
        private const string LogEventInfoDisplayLevel_Warn = "Warning";

        private const string DisplayErrorsCheckboxText = "Errors";
        private const string DisplayWarningsCheckboxText = "Warnings";

        private const string ValidationErrorMessage_TeamProjectNameNotSelected = "Team project not selected.";
        private const string ValidationErrorMessage_SolutionNotSelected = "Solution not selected.";
        private const string ValidationErrorMessage_OperationIsRequired = "Operation is required.";

        private const string Sidekick_Purpose = "Build related solutions for specified branch.";
        private const string Sidekick_Directions = "Pick Team Project, select branch, select one or more solutions and click Apply.";
        private const string Sidekick_Warnings = "No warnings. NOTE: If build fails then build solution with Visual Studio to find/fix build errors.";

        private const string AzureRedisCacheAdminOperation_Unspecified = "<Select Operation>";
        private const string AzureRedisCacheAdminOperation_ClearAzureRedisCache = "Clear Azure Redis Cache";

        #endregion

        #region Private Data Members

        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private SidekickConfiguration _sidekickConfiguration;
        private AzureRedisCacheAdminPresenter _azureRedisCacheAdminPresenter;
        private BindingSource _azureRedisCacheConfigListBindingSource;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickView_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickView_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public Logger Logger
        {
            get
            {
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick warnings.
        /// </summary>
        public string SidekickWarnings
        {
            get { return Sidekick_Warnings; }
        }

        /// <summary>
        /// Property: Sidekick purpose.
        /// </summary>
        public string SidekickPurpose
        {
            get { return Sidekick_Purpose; }
        }

        /// <summary>
        /// Property: Sidekick directions.
        /// </summary>
        public string SidekickDirections
        {
            get { return Sidekick_Directions; }
        }

        /// <summary>
        /// Property: Azure Redis cache admin presenter.
        /// </summary>
        public AzureRedisCacheAdminPresenter AzureRedisCacheAdminPresenter
        {
            get { return _azureRedisCacheAdminPresenter; }
            set { _azureRedisCacheAdminPresenter = value; }
        }

        /// <summary>
        /// Property: Azure Redis cache configuration list binding source.
        /// </summary>
        public BindingSource AzureRedisCacheConfigListBindingSource
        {
            get { return _azureRedisCacheConfigListBindingSource; }
            set { _azureRedisCacheConfigListBindingSource = value; }
        }

        /// <summary>
        /// Property: Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get
            {
                return this.ApplyButton.Text;
            }
            set
            {
                this.ApplyButton.Text = value;
            }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get
            {
                return this.BackgroundTaskStatusLabel.Text;
            }
            set
            {
                this.BackgroundTaskStatusLabel.Text = value;
            }
        }

        /// <summary>
        /// Property: Is sidekick busy.
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;
                result = this.AzureRedisCacheAdminPresenter.IsBackgroundTaskBusy;
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="solutionListSearchForm"></param>
        public AzureRedisCacheAdminForm(SidekickConfiguration sidekickConfiguration)
        {
            InitializeComponent();

            this.SidekickConfiguration = sidekickConfiguration;
            _azureRedisCacheConfigListBindingSource = new BindingSource();
            this.InitializeControls();
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private AzureRedisCacheAdminForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Apply button enable.
        /// </summary>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            this.EnableControlsDependantOnTeamProject(true);
            this.EnableControlsDependantOnAzureRedisCacheOptions();
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {

                this.AzureRedisCacheComboBox.DisplayMember = "Name";
                this.AzureRedisCacheComboBox.ValueMember = "Name";
                _azureRedisCacheConfigListBindingSource.DataSource = this.SidekickConfiguration.AzureRedisCacheConfigItemList.AzureRedisCacheConfigItem;
                this.AzureRedisCacheComboBox.DataSource = AzureRedisCacheConfigListBindingSource;


                this.OperationComboBox.FormattingEnabled = true;
                this.OperationComboBox.Format += delegate (object sender, ListControlConvertEventArgs e)
                {
                    e.Value = ConvertAzureRedisCacheAdminOperationsToText((AzureRedisCacheAdminOperation)e.Value);
                };
                this.OperationComboBox.DataSource = Enum.GetValues(typeof(AzureRedisCacheAdminOperation));

                this.OperationComboBox.SelectedItem = AzureRedisCacheAdminOperation.ClearAzureRedisCache;


                this.EnableControlsDependantOnTeamProject(false);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable (disable) controls dependant on team project.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnTeamProject(bool enable)
        {
            try
            {
                //Currently this sidekick is not dependant on team project.
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable (disable) controls dependant on Azure Redis cache options.
        /// </summary>
        protected void EnableControlsDependantOnAzureRedisCacheOptions()
        {
            try
            {
                this.ApplyButton.Enabled = true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        // Convert Azure Redis cache admin operations enum value to text.
        /// </summary>
        /// <param name="azureRedisCacheAdminOperation"></param>
        /// <returns></returns>
        protected string ConvertAzureRedisCacheAdminOperationsToText(AzureRedisCacheAdminOperation azureRedisCacheAdminOperation)
        {
            string result = string.Empty;

            try
            {
                switch (azureRedisCacheAdminOperation)
                {
                    case AzureRedisCacheAdminOperation.Unspecified:
                        result = AzureRedisCacheAdminOperation_Unspecified;
                        break;
                    case AzureRedisCacheAdminOperation.ClearAzureRedisCache:
                        result = AzureRedisCacheAdminOperation_ClearAzureRedisCache;
                        break;
                    default:
                        result = AzureRedisCacheAdminOperation_Unspecified;
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            AzureRedisCacheAdminOperation azureRedisCacheAdminOption = AzureRedisCacheAdminOperation.Unspecified;
            AzureRedisCacheConfigItem azureRedisCacheConfig = null;

            try
            {

                if (this.AzureRedisCacheComboBox.SelectedItem is AzureRedisCacheConfigItem)
                {
                    azureRedisCacheConfig = (AzureRedisCacheConfigItem)this.AzureRedisCacheComboBox.SelectedItem;
                }

                if (this.OperationComboBox.SelectedItem is AzureRedisCacheAdminOperation)
                {
                    if ((AzureRedisCacheAdminOperation)this.OperationComboBox.SelectedItem == AzureRedisCacheAdminOperation.Unspecified)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_OperationIsRequired));
                        return;
                    }
                    else
                    {
                        azureRedisCacheAdminOption = (AzureRedisCacheAdminOperation)this.OperationComboBox.SelectedItem;
                    }
                }

                this.AzureRedisCacheAdminPresenter.PerformAzureRedisCacheAdminOperation(azureRedisCacheConfig, azureRedisCacheAdminOption);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: End Point textbox - Text changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EndPointTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                EnableControlsDependantOnAzureRedisCacheOptions();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Password textbox - Tech changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PasswordTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                EnableControlsDependantOnAzureRedisCacheOptions();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickAzureRedisCacheAdmin.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion
    }
}
