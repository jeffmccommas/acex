﻿namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    partial class NuGetPackageAnalyzerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.SolutionListSearchPanel = new System.Windows.Forms.Panel();
            this.SolutionListSearchPlaceholderLabel = new System.Windows.Forms.Label();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.ReferenceCountValueLabel = new System.Windows.Forms.Label();
            this.ReferenceCountLabel = new System.Windows.Forms.Label();
            this.NuGetPackageVersionReferencePanel = new System.Windows.Forms.Panel();
            this.NuGetPackageReferencedPathDataGridView = new System.Windows.Forms.DataGridView();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.HelpButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.VerticalSplitter = new System.Windows.Forms.Splitter();
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.SolutionListSearchPanel.SuspendLayout();
            this.PropertiesPanel.SuspendLayout();
            this.NuGetPackageVersionReferencePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NuGetPackageReferencedPathDataGridView)).BeginInit();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // SolutionListSearchPanel
            // 
            this.SolutionListSearchPanel.AutoScroll = true;
            this.SolutionListSearchPanel.Controls.Add(this.SolutionListSearchPlaceholderLabel);
            this.SolutionListSearchPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.SolutionListSearchPanel.Location = new System.Drawing.Point(0, 0);
            this.SolutionListSearchPanel.Name = "SolutionListSearchPanel";
            this.SolutionListSearchPanel.Size = new System.Drawing.Size(698, 600);
            this.SolutionListSearchPanel.TabIndex = 1;
            // 
            // SolutionListSearchPlaceholderLabel
            // 
            this.SolutionListSearchPlaceholderLabel.AutoSize = true;
            this.SolutionListSearchPlaceholderLabel.Location = new System.Drawing.Point(164, 294);
            this.SolutionListSearchPlaceholderLabel.Name = "SolutionListSearchPlaceholderLabel";
            this.SolutionListSearchPlaceholderLabel.Size = new System.Drawing.Size(160, 13);
            this.SolutionListSearchPlaceholderLabel.TabIndex = 0;
            this.SolutionListSearchPlaceholderLabel.Text = "Solution List Search Placeholder";
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Controls.Add(this.ReferenceCountValueLabel);
            this.PropertiesPanel.Controls.Add(this.ReferenceCountLabel);
            this.PropertiesPanel.Controls.Add(this.NuGetPackageVersionReferencePanel);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesPanel.Location = new System.Drawing.Point(698, 0);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(536, 600);
            this.PropertiesPanel.TabIndex = 5;
            // 
            // ReferenceCountValueLabel
            // 
            this.ReferenceCountValueLabel.AutoSize = true;
            this.ReferenceCountValueLabel.Location = new System.Drawing.Point(107, 31);
            this.ReferenceCountValueLabel.Name = "ReferenceCountValueLabel";
            this.ReferenceCountValueLabel.Size = new System.Drawing.Size(0, 13);
            this.ReferenceCountValueLabel.TabIndex = 1;
            // 
            // ReferenceCountLabel
            // 
            this.ReferenceCountLabel.AutoSize = true;
            this.ReferenceCountLabel.Location = new System.Drawing.Point(13, 31);
            this.ReferenceCountLabel.Name = "ReferenceCountLabel";
            this.ReferenceCountLabel.Size = new System.Drawing.Size(87, 13);
            this.ReferenceCountLabel.TabIndex = 0;
            this.ReferenceCountLabel.Text = "Total references:";
            // 
            // NuGetPackageVersionReferencePanel
            // 
            this.NuGetPackageVersionReferencePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NuGetPackageVersionReferencePanel.Controls.Add(this.NuGetPackageReferencedPathDataGridView);
            this.NuGetPackageVersionReferencePanel.Location = new System.Drawing.Point(13, 47);
            this.NuGetPackageVersionReferencePanel.Name = "NuGetPackageVersionReferencePanel";
            this.NuGetPackageVersionReferencePanel.Size = new System.Drawing.Size(515, 507);
            this.NuGetPackageVersionReferencePanel.TabIndex = 2;
            // 
            // NuGetPackageReferencedPathDataGridView
            // 
            this.NuGetPackageReferencedPathDataGridView.AllowUserToAddRows = false;
            this.NuGetPackageReferencedPathDataGridView.AllowUserToDeleteRows = false;
            this.NuGetPackageReferencedPathDataGridView.AllowUserToOrderColumns = true;
            this.NuGetPackageReferencedPathDataGridView.AllowUserToResizeRows = false;
            this.NuGetPackageReferencedPathDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NuGetPackageReferencedPathDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.NuGetPackageReferencedPathDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.NuGetPackageReferencedPathDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.NuGetPackageReferencedPathDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.NuGetPackageReferencedPathDataGridView.EnableHeadersVisualStyles = false;
            this.NuGetPackageReferencedPathDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.NuGetPackageReferencedPathDataGridView.Location = new System.Drawing.Point(4, 3);
            this.NuGetPackageReferencedPathDataGridView.Name = "NuGetPackageReferencedPathDataGridView";
            this.NuGetPackageReferencedPathDataGridView.ReadOnly = true;
            this.NuGetPackageReferencedPathDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.NuGetPackageReferencedPathDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.NuGetPackageReferencedPathDataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            this.NuGetPackageReferencedPathDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.NuGetPackageReferencedPathDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.NuGetPackageReferencedPathDataGridView.ShowEditingIcon = false;
            this.NuGetPackageReferencedPathDataGridView.Size = new System.Drawing.Size(507, 501);
            this.NuGetPackageReferencedPathDataGridView.TabIndex = 0;
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.HelpButton);
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(536, 24);
            this.PropertiesHeaderPanel.TabIndex = 7;
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(508, 1);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 1;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(502, 24);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "NuGet Package Analyzer";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ControlPanel
            // 
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 560);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(536, 40);
            this.ControlPanel.TabIndex = 3;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // VerticalSplitter
            // 
            this.VerticalSplitter.BackColor = System.Drawing.SystemColors.ControlDark;
            this.VerticalSplitter.Location = new System.Drawing.Point(698, 0);
            this.VerticalSplitter.Name = "VerticalSplitter";
            this.VerticalSplitter.Size = new System.Drawing.Size(6, 600);
            this.VerticalSplitter.TabIndex = 2;
            this.VerticalSplitter.TabStop = false;
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.VsoLocalDev.Sidekicks.Help.chm";
            // 
            // NuGetPackageAnalyzerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1234, 600);
            this.ControlBox = false;
            this.Controls.Add(this.VerticalSplitter);
            this.Controls.Add(this.PropertiesPanel);
            this.Controls.Add(this.SolutionListSearchPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "NuGetPackageAnalyzerForm";
            this.Text = "NuGetPackageAnalyzerForm";
            this.SolutionListSearchPanel.ResumeLayout(false);
            this.SolutionListSearchPanel.PerformLayout();
            this.PropertiesPanel.ResumeLayout(false);
            this.PropertiesPanel.PerformLayout();
            this.NuGetPackageVersionReferencePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NuGetPackageReferencedPathDataGridView)).EndInit();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel SolutionListSearchPanel;
        private System.Windows.Forms.Label SolutionListSearchPlaceholderLabel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Splitter VerticalSplitter;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.Button HelpButton;
        private System.Windows.Forms.Panel NuGetPackageVersionReferencePanel;
        private System.Windows.Forms.DataGridView NuGetPackageReferencedPathDataGridView;
        private System.Windows.Forms.Label ReferenceCountValueLabel;
        private System.Windows.Forms.Label ReferenceCountLabel;
    }
}