﻿using Aclara.VsoLocalDev.Sidekicks.WinForm;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Exceptions;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.Server;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    public partial class WebAppSearchForm : Form, IWebAppSearchView
    {

        #region Private Constants

        private const string ValidationErrorMessage_TeamProjectNameNotSelected = "Team project not selected.";

        private const string BranchComboBoxPreset_None = "<Select Branch>";

        private const string SiteLevelWebAppDataGridViewColumn_AppID = "SiteLevelWebAppAppIDColumn";
        private const string SiteLevelWebAppDataGridViewColumn_Select = "SiteLevelWebAppSelectColumn";
        private const string SiteLevelWebAppDataGridViewColumn_Path = "SiteLevelWebAppPathColumn";
        private const string SiteLevelWebAppDataGridViewColumn_PhysicalPath = "SiteLevelWebAppPhysicalPathColumn";

        private const string WebAppDataGridViewColumn_AppID = "WebAppAppIDColumn";
        private const string WebAppDataGridViewColumn_Select = "WebAppSelectColumn";
        private const string WebAppDataGridViewColumn_ExistsInIIS = "WebAppExistsInIISColumn";
        private const string WebAppDataGridViewColumn_Path = "WebAppPathColumn";
        private const string WebAppDataGridViewColumn_Status = "StatusColumn";
        private const string WebAppDataGridViewColumn_PhysicalPath = "WebAppPhysicalPathColumn";

        private const string WebAppSelectorProperty_AppID = "AppID";
        private const string WebAppSelectorProperty_Selected = "Selected";
        private const string WebAppSelectorProperty_Path = "Path";
        private const string WebAppSelectorProperty_Status = "Status";
        private const string WebAppSelectorProperty_ExistsInIIS = "ExistsInIIS";
        private const string WebAppSelectorProperty_PhysicalPathActual = "PhysicalPathActual";

        private const string WebAppStatus_NotFound = "Not found";
        private const string WebAppStatus_Found = "Found";

        #endregion

        #region Private Data Members

        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private SidekickConfiguration _sidekickConfiguration;
        private WebAppSelectorList _siteLevelWebAppSelectorList = null;
        private WebAppSelectorList _webAppSelectorList = null;
        private WebAppSearchPresenter _webAppSearchPresenter = null;
        private WebAppSearchTypes.WebAppSelectability _webAppSelectability;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public Logger Logger
        {
            get
            {
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Web application search presenter.
        /// </summary>
        public WebAppSearchPresenter WebAppSearchPresenter
        {
            get { return _webAppSearchPresenter; }
            set { _webAppSearchPresenter = value; }
        }

        /// <summary>
        /// Property: Site name.
        /// </summary>
        public string SiteName
        {
            get
            {
                return this.SiteNameValueLabel.Text;
            }
            set
            {
                this.SiteNameValueLabel.Text = value;
            }
        }

        /// <summary>
        /// Property: Web application path (partial).
        /// </summary>
        public string WebAppPathPartial
        {
            get
            {
                return this.WebAppPathPartialTextBox.Text;
            }
            set
            {
                this.WebAppPathPartialTextBox.Text = value;
            }
        }

        /// <summary>
        /// Property: Branch name.
        /// </summary>
        public string BranchName
        {
            get
            {
                string branchName = string.Empty;
                if (this.BranchComboBox.Text == null)
                {
                    branchName = string.Empty;
                }
                else
                {
                    branchName = this.BranchComboBox.Text;
                }
                return branchName;
            }
            set { this.BranchComboBox.SelectedItem = value; }
        }

        /// <summary>
        /// Property: Site level web application selector list.
        /// </summary>
        public WebAppSelectorList SiteLevelWebAppSelectorList
        {
            get
            {

                _webAppSelectorList = this.WebAppSearchPresenter.GetWebAppSelectorList(this.WebAppSearchPresenter.TeamProjectInfo.TeamProjectCollectionUri.ToString(),
                                                                                       this.WebAppSearchPresenter.TeamProjectInfo.TeamProjectName,
                                                                                       this.BranchName,
                                                                                       this.SiteName,
                                                                                       WebAppSearchPresenter.WebApplicationLevel.SiteLevel);

                return _webAppSelectorList;
            }
            set
            {
                _siteLevelWebAppSelectorList = value;
            }
        }

        /// <summary>
        /// Property: Web app selector list.
        /// </summary>
        public WebAppSelectorList WebAppSelectorList
        {
            get
            {

                _webAppSelectorList = this.WebAppSearchPresenter.GetWebAppSelectorList(this.WebAppSearchPresenter.TeamProjectInfo.TeamProjectCollectionUri.ToString(),
                                                                                       this.WebAppSearchPresenter.TeamProjectInfo.TeamProjectName,
                                                                                       this.BranchName,
                                                                                       this.SiteName,
                                                                                       WebAppSearchPresenter.WebApplicationLevel.ApplicationLevel);

                return _webAppSelectorList;
            }
            set
            {
                _webAppSelectorList = value;
            }
        }

        /// <summary>
        /// Property: Web application selectability.
        /// </summary>
        public WebAppSearchTypes.WebAppSelectability WebAppSelectability
        {
            get { return _webAppSelectability; }
            set { _webAppSelectability = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="hideApplicationsSiteLevel"></param>
        public WebAppSearchForm(SidekickConfiguration sidekickConfiguration,
                                bool hideApplicationsSiteLevel)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.WebAppSelectorList = new WebAppSelectorList();

            InitializeComponent();

            //Disable controls dependant on team project.
            EnableControlsDependantOnTeamProject(false);

            //Disable controls dependant on branch.
            EnableControlsDependantOnBranch(false);

            if (hideApplicationsSiteLevel == true)
            {
                this.ApplicationsSiteLevelGroupBox.Visible = false;
                this.ApplicationsGroupBox.Height = this.ApplicationsGroupBox.Height +
                                                   (this.ApplicationsGroupBox.Top - this.ApplicationsSiteLevelGroupBox.Top);
                this.ApplicationsGroupBox.Top = this.ApplicationsSiteLevelGroupBox.Top;
                this.ApplicationsGroupBox.Left = this.ApplicationsSiteLevelGroupBox.Left;
            }
        }

        #endregion

        #region Protected Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        protected WebAppSearchForm()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Team project changed.
        /// </summary>
        /// <param name="teamProjectCollectionUri"></param>
        /// <param name="teamProjectName"></param>
        public void TeamProjectChanged()
        {

            try
            {

                this.WebAppSearchPresenter.TeamProjectCollectionNameChanged();
                this.WebAppSearchPresenter.TeamProjectNameChanged();

                this.WebAppSearchPresenter.PopulateBrachNameList(this.WebAppSearchPresenter.TeamProjectInfo.TeamProjectCollectionUri.ToString(),
                                                                 this.WebAppSearchPresenter.TeamProjectInfo.TeamProjectName);

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Refresh web application list.
        /// </summary>
        public void Refresh()
        {
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                this.Cursor = Cursors.WaitCursor;

                this.ClearSearch();

                this.PerformSearch();

                this.SearchButton.BackColor = DefaultBackColor;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Populate branch name list.
        /// </summary>
        /// <param name="branchNameList"></param>
        public void PopulateBranchNameList(List<string> branchNameList)
        {
            int index = 0;
            try
            {
                this.BranchComboBox.Items.Clear();
                this.BranchComboBox.Items.Insert(0, BranchComboBoxPreset_None);
                foreach (string branchName in branchNameList)
                {
                    this.BranchComboBox.Items.Insert(index++, branchName);
                }
                this.BranchComboBox.SelectedItem = BranchComboBoxPreset_None;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable controls dependant on team project.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnTeamProject(bool enabled)
        {
            try
            {
                this.BranchComboBox.Enabled = enabled;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable controls dependant on branch.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnBranch(bool enabled)
        {
            try
            {
                this.WebAppPathPartialTextBox.Enabled = enabled;
                this.SearchButton.Enabled = enabled;
                this.ClearButton.Enabled = enabled;
                this.RefreshButton.Enabled = enabled;
                this.DisplayWebAppConfigFileButton.Enabled = enabled;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable controls dependant on search.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnSearch(bool enabled)
        {
            try
            {
                this.SelectSplitButton.Enabled = enabled;
                this.DisplaySplitButton.Enabled = enabled;
                this.SiteLevelWebAppDataGridView.Enabled = enabled;
                this.WebAppDataGridView.Enabled = enabled;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Clear branch dependent controls.
        /// </summary>
        public void ClearBranchDependentControls()
        {
            try
            {
                this.WebAppSelectorList = new WebAppSelectorList();
                this.WebAppPathPartial = string.Empty;
                this.EnableControlsDependantOnSearch(false);
                this.SiteLevelWebAppDataGridView.DataSource = null;
                this.WebAppDataGridView.DataSource = null;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Clear search controls.
        /// </summary>
        public void ClearSearch()
        {
            try
            {
                this.SearchButton.BackColor = System.Drawing.Color.DarkOrange;
                this.WebAppSelectorList = new WebAppSelectorList();
                this.WebAppPathPartial = string.Empty;
                this.EnableControlsDependantOnSearch(false);
                this.WebAppDataGridView.DataSource = null;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve selected site level web application selector list.
        /// </summary>
        /// <returns></returns>
        public Models.WebAppSelectorList GetSelectedSiteLevelWebAppSelectorList()
        {
            WebAppSelectorList result = null;
            WebAppSelector webAppSelector = null;
            Guid appID;
            bool selected = false;
            string path = string.Empty;

            try
            {
                result = new WebAppSelectorList();
                foreach (DataGridViewRow dataGridViewRow in this.SiteLevelWebAppDataGridView.Rows)
                {
                    if (dataGridViewRow.Cells[WebAppDataGridViewColumn_Select].Value is Boolean)
                    {
                        appID = (Guid)dataGridViewRow.Cells[WebAppDataGridViewColumn_AppID].Value;
                        selected = (bool)dataGridViewRow.Cells[WebAppDataGridViewColumn_Select].Value;
                        path = (string)dataGridViewRow.Cells[WebAppDataGridViewColumn_Path].Value;
                        if (selected == true)
                        {
                            webAppSelector = new WebAppSelector();
                            webAppSelector.AppID = appID;
                            webAppSelector.Path = path;
                            webAppSelector.Selected = true;
                            result.Add(webAppSelector);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve selected web application selector list.
        /// </summary>
        /// <returns></returns>
        public Models.WebAppSelectorList GetSelectedWebAppSelectorList()
        {
            WebAppSelectorList result = null;
            WebAppSelector webAppSelector = null;
            bool selected = false;
            string path = string.Empty;

            try
            {
                result = new WebAppSelectorList();
                foreach (DataGridViewRow dataGridViewRow in this.WebAppDataGridView.Rows)
                {

                    if (dataGridViewRow.DataBoundItem is WebAppSelector == true)
                    {
                        webAppSelector = (WebAppSelector)dataGridViewRow.DataBoundItem;
                    }
                    if (webAppSelector == null)
                    {
                        continue;
                    }

                    if (dataGridViewRow.Cells[WebAppDataGridViewColumn_Select].Value is Boolean)
                    {
                        selected = (bool)dataGridViewRow.Cells[WebAppDataGridViewColumn_Select].Value;
                        if (selected == true)
                        {
                            result.Add(webAppSelector);
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize site level web app data grid view.
        /// </summary>
        protected void InitializeSiteLevelWebAppDataGridView()
        {
            BindingSource bindingSource = null;

            try
            {

                bindingSource = new BindingSource();
                bindingSource.DataSource = this.SiteLevelWebAppSelectorList;
                this.SiteLevelWebAppDataGridView.DataSource = null;
                this.SiteLevelWebAppDataGridView.AutoGenerateColumns = false;
                this.SiteLevelWebAppDataGridView.DataSource = bindingSource;

                foreach (DataGridViewColumn column in this.SiteLevelWebAppDataGridView.Columns)
                {
                    switch (column.Name)
                    {
                        case SiteLevelWebAppDataGridViewColumn_AppID:
                            column.DataPropertyName = WebAppSelectorProperty_AppID;
                            break;
                        case SiteLevelWebAppDataGridViewColumn_Select:
                            column.DataPropertyName = WebAppSelectorProperty_Selected;
                            break;
                        case SiteLevelWebAppDataGridViewColumn_Path:
                            column.DataPropertyName = WebAppSelectorProperty_Path;
                            break;
                        case SiteLevelWebAppDataGridViewColumn_PhysicalPath:
                            column.DataPropertyName = WebAppSelectorProperty_PhysicalPathActual;
                            break;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Initialize web app data grid view.
        /// </summary>
        protected void InitializeWebAppDataGridView()
        {
            WebAppSelectorList filteredWebAppSelectorList = null;
            BindingSource bindingSource = null;

            try
            {

                bindingSource = new BindingSource();
                filteredWebAppSelectorList = this.WebAppSelectorList;
                filteredWebAppSelectorList.Sort(new WebAppSelector());
                bindingSource.DataSource = filteredWebAppSelectorList;
                this.WebAppDataGridView.DataSource = null;
                this.WebAppDataGridView.AutoGenerateColumns = false;
                this.WebAppDataGridView.DataSource = bindingSource;

                foreach (DataGridViewColumn column in this.WebAppDataGridView.Columns)
                {
                    switch (column.Name)
                    {
                        case WebAppDataGridViewColumn_AppID:
                            column.DataPropertyName = WebAppSelectorProperty_AppID;
                            break;
                        case WebAppDataGridViewColumn_Select:
                            column.DataPropertyName = WebAppSelectorProperty_Selected;
                            break;
                        case WebAppDataGridViewColumn_ExistsInIIS:
                            column.DataPropertyName = WebAppSelectorProperty_ExistsInIIS;
                            break;
                        case WebAppDataGridViewColumn_Path:
                            column.DataPropertyName = WebAppSelectorProperty_Path;
                            break;
                        case WebAppDataGridViewColumn_Status:
                            column.DataPropertyName = WebAppSelectorProperty_Path;
                            break;
                        case WebAppDataGridViewColumn_PhysicalPath:
                            column.DataPropertyName = WebAppSelectorProperty_PhysicalPathActual;
                            break;
                    }
                }

                if (this.WebAppDataGridView.RowCount > 0)
                {
                    EnableControlsDependantOnSearch(true);
                }
                else
                {
                    EnableControlsDependantOnSearch(false);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Perform search.
        /// </summary>
        public void PerformSearch()
        {

            WebAppSelectorList filteredWebAppSelectorList = null;

            try
            {

                filteredWebAppSelectorList = this.WebAppSelectorList.GetSourceWebAppListFilterdByWebAppPath(
                                                                                                    this.WebAppPathPartial);
                filteredWebAppSelectorList.Sort(new WebAppSelector());

                this.WebAppDataGridView.DataSource = filteredWebAppSelectorList;

                if (this.WebAppDataGridView.RowCount > 0)
                {
                    EnableControlsDependantOnSearch(true);
                }
                else
                {
                    EnableControlsDependantOnSearch(false);
                }

            }
            catch (Exception)
            {
            }

        }


        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Web app path parital text box - text changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WebAppPathPartialTextBox_TextChanged(object sender, EventArgs e)
        {
            this.SearchButton.BackColor = System.Drawing.Color.DarkOrange;

        }

        /// <summary>
        /// Event Handler: web app data grid view - cell formatting.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WebAppDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridViewRow dataGridViewRow = null;
            DataGridViewCell dataGridViewCell = null;
            WebAppSelector webAppSelector = null;

            try
            {
                if (this.WebAppDataGridView.Columns[e.ColumnIndex].Name == WebAppDataGridViewColumn_ExistsInIIS &&
                    e.RowIndex >= 0 &&
                    e.RowIndex < this.WebAppDataGridView.Rows.Count)
                {

                    if (this.WebAppDataGridView.Rows[e.RowIndex].DataBoundItem != null)
                    {
                        if (this.WebAppDataGridView.Rows[e.RowIndex].DataBoundItem is WebAppSelector == true)
                        {
                            webAppSelector = (WebAppSelector)this.WebAppDataGridView.Rows[e.RowIndex].DataBoundItem;
                        }
                        else
                        {
                            return;
                        }
                        dataGridViewRow = this.WebAppDataGridView.Rows[e.RowIndex];
                        dataGridViewCell = this.WebAppDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];

                        if (webAppSelector.ExistsInIIS == true)
                        {
                            e.Value = (System.Drawing.Image)Properties.Resources.ExistsInIISTrue;

                            if (dataGridViewRow != null)
                            {

                                switch (this.WebAppSelectability)
                                {
                                    case WebAppSearchTypes.WebAppSelectability.AlwaysSelectable:
                                        //No-op.
                                        break;
                                    case WebAppSearchTypes.WebAppSelectability.OnlySelectableWhenDoesNotExistInIIS:
                                        dataGridViewRow.ReadOnly = true;
                                        break;
                                    case WebAppSearchTypes.WebAppSelectability.OnlySelectableWhenExistInIIS:
                                        //No-op.
                                        break;
                                    default:
                                        //No-op.
                                        break;
                                }

                                if (dataGridViewCell != null)
                                {
                                    dataGridViewCell.ToolTipText = "Web app exists in IIS.";
                                }

                            }
                        }
                        else
                        {
                            e.Value = (System.Drawing.Image)Properties.Resources.ExistsInIISFalse;

                            if (dataGridViewRow != null)
                            {

                                switch (this.WebAppSelectability)
                                {
                                    case WebAppSearchTypes.WebAppSelectability.AlwaysSelectable:
                                        //No-op.
                                        break;
                                    case WebAppSearchTypes.WebAppSelectability.OnlySelectableWhenDoesNotExistInIIS:
                                        //No-op.
                                        break;
                                    case WebAppSearchTypes.WebAppSelectability.OnlySelectableWhenExistInIIS:
                                        dataGridViewRow.ReadOnly = true;
                                        break;
                                    default:
                                        //No-op.
                                        break;
                                }

                                if (dataGridViewRow.ReadOnly == true)
                                {
                                    dataGridViewCell.ToolTipText = "Web app does not exist in IIS.";
                                }
                            }
                        }

                    }
                }

                if (this.WebAppDataGridView.Columns[e.ColumnIndex].Name == WebAppDataGridViewColumn_Status &&
                    e.RowIndex >= 0 &&
                    e.RowIndex < this.WebAppDataGridView.Rows.Count)
                {

                    if (this.WebAppDataGridView.Rows[e.RowIndex].DataBoundItem != null)
                    {
                        if (this.WebAppDataGridView.Rows[e.RowIndex].DataBoundItem is WebAppSelector == true)
                        {
                            webAppSelector = (WebAppSelector)this.WebAppDataGridView.Rows[e.RowIndex].DataBoundItem;
                        }
                        else
                        {
                            return;
                        }
                        dataGridViewRow = this.WebAppDataGridView.Rows[e.RowIndex];
                        dataGridViewCell = this.WebAppDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];

                        if (webAppSelector.ExistsInIIS == true)
                        {
                            e.Value = WebAppStatus_Found;
                        }
                        else
                        {
                            e.Value = WebAppStatus_NotFound;
                        }

                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Site level web application data grid view - cell clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SiteLevelWebAppDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                foreach (DataGridViewRow dataGridViewRow in this.SiteLevelWebAppDataGridView.Rows)
                {
                    if (dataGridViewRow.Index != e.RowIndex)
                    {
                        dataGridViewRow.Cells[SiteLevelWebAppDataGridViewColumn_Select].Value = false;
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Search button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchButton_Click(object sender, EventArgs e)
        {

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                this.Cursor = Cursors.WaitCursor;

                this.PerformSearch();

                this.SearchButton.BackColor = DefaultBackColor;

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Hanlder: Clear button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearButton_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                this.Cursor = Cursors.WaitCursor;

                this.ClearSearch();

                this.PerformSearch();

                this.SearchButton.BackColor = DefaultBackColor;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Refresh button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshButton_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                this.Cursor = Cursors.WaitCursor;

                this.PerformSearch();

                this.SearchButton.BackColor = DefaultBackColor;

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Envent Handler: Web application data grid view select "All" tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectAllToolStripItem_Click(object sender, EventArgs e)
        {

            WebAppSelector webAppSelector = null;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                foreach (DataGridViewRow dataGridViewRow in this.WebAppDataGridView.Rows)
                {
                    if (dataGridViewRow.DataBoundItem is WebAppSelector == true)
                    {
                        webAppSelector = (WebAppSelector)dataGridViewRow.DataBoundItem;
                    }
                    else
                    {
                        continue;
                    }

                    if (this.WebAppSelectability == WebAppSearchTypes.WebAppSelectability.AlwaysSelectable ||
                       (this.WebAppSelectability == WebAppSearchTypes.WebAppSelectability.OnlySelectableWhenExistInIIS &&
                        webAppSelector.ExistsInIIS == true) ||
                       (this.WebAppSelectability == WebAppSearchTypes.WebAppSelectability.OnlySelectableWhenDoesNotExistInIIS &&
                        webAppSelector.ExistsInIIS == false))
                    {
                        dataGridViewRow.Cells[WebAppDataGridViewColumn_Select].Value = true;
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Envent Handler: Web application data grid view select "None" tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectNoneToolStripItem_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                foreach (DataGridViewRow dataGridViewRow in this.WebAppDataGridView.Rows)
                {
                    dataGridViewRow.Cells[WebAppDataGridViewColumn_Select].Value = false;
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Envent Handler: Web application data grid view select "All" tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                foreach (DataGridViewRow dataGridViewRow in this.WebAppDataGridView.Rows)
                {
                    if (dataGridViewRow.ReadOnly == false)
                    {
                        dataGridViewRow.Cells[WebAppDataGridViewColumn_Select].Value = true;
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Envent Handler: Web application data grid view select "None" tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NoneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                foreach (DataGridViewRow dataGridViewRow in this.WebAppDataGridView.Rows)
                {
                    dataGridViewRow.Cells[WebAppDataGridViewColumn_Select].Value = false;
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Web app display all tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayAllToolStripItem_Click(object sender, EventArgs e)
        {

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                this.WebAppDataGridView.CurrentCell = null;

                foreach (DataGridViewRow dataGridViewRow in this.WebAppDataGridView.Rows)
                {
                    dataGridViewRow.Visible = true;
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Web app display selected tool strip item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplaySelectedToolStripItem_Click(object sender, EventArgs e)
        {
            bool selected = false;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                this.WebAppDataGridView.CurrentCell = null;

                foreach (DataGridViewRow dataGridViewRow in this.WebAppDataGridView.Rows)
                {
                    bool.TryParse(dataGridViewRow.Cells[WebAppDataGridViewColumn_Select].Value.ToString(),
                                  out selected);

                    if (selected == true)
                    {
                        dataGridViewRow.Visible = true;
                    }
                    else
                    {
                        dataGridViewRow.Visible = false;
                    };

                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Branch combo box - Text changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BranchComboBox_TextChanged(object sender, EventArgs e)
        {
            ComboBox senderComboBox = null;

            try
            {
                if (sender is ComboBox != true)
                {
                    return;
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                this.Cursor = Cursors.WaitCursor;

                senderComboBox = (ComboBox)sender;
                if (senderComboBox.Text == BranchComboBoxPreset_None)
                {

                    EnableControlsDependantOnBranch(false);
                    this.ClearBranchDependentControls();
                    return;
                }

                ClearSearch();

                InitializeSiteLevelWebAppDataGridView();
                InitializeWebAppDataGridView();

                EnableControlsDependantOnBranch(true);

                this.Cursor = Cursors.Default;

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
                this.ClearBranchDependentControls();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Open tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string webAppConfigFilePath = string.Empty;

            try
            {
                webAppConfigFilePath = this.WebAppSearchPresenter.GetWebAppConfigFilePath(this.WebAppSearchPresenter.TeamProjectInfo.TeamProjectName,
                                                                                           this.BranchName);

                System.Diagnostics.Process.Start(webAppConfigFilePath);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Open containing folder tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenContainingFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string webAppConfigFilePath = string.Empty;
            string webAppConfigPath = string.Empty;

            try
            {
                webAppConfigFilePath = this.WebAppSearchPresenter.GetWebAppConfigFilePath(this.WebAppSearchPresenter.TeamProjectInfo.TeamProjectName,
                                                                                           this.BranchName);
                webAppConfigPath = Path.GetDirectoryName(webAppConfigFilePath);
                System.Diagnostics.Process.Start(webAppConfigPath);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        #endregion

    }
}
