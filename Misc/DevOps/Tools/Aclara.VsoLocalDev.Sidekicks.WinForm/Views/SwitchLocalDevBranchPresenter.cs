﻿using Aclara.Computer.Administrator.Client;
using Aclara.Script.Host;
using Aclara.Script.Host.Events;
using Aclara.TeamFoundation.VersionControl.Client;
using Aclara.Tools.Configuration;
using Aclara.Tools.Configuration.ScriptConfig;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using Aclara.Web.Administration.Client;
using Aclara.Web.Administration.Client.Events;
using Aclara.Web.Administration.Client.StatusManagement;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Switch local dev branch presenter.
    /// </summary>
   public class SwitchLocalDevBranchPresenter
    {

        #region Private Constants


        //Service controller operations.
        protected enum ServiceControllerOperation
        {
            Unspecified = 0,
            Start = 1,
            Stop = 2
        }

        #endregion

        #region Private Data Members

        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private SidekickConfiguration _sidekickConfiguration = null;
        private ISwitchLocalDevBranchView _switchLocalDevBranchView;
        private WebAppManager _webAppManager = null;
        private ScriptHostManager _scriptHostManager = null;
        private ProjectManager _projectManager = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;
        private ITeamProjectInfo _teamProjectInfo;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public Logger Logger
        {
            get
            {
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Team project info.
        /// </summary>
        public ITeamProjectInfo TeamProjectInfo
        {
            get { return _teamProjectInfo; }
            set { _teamProjectInfo = value; }
        }

        /// <summary>
        /// Property: Generate client test data view.
        /// </summary>
        public ISwitchLocalDevBranchView SwitchLocalDevBranchView
        {
            get { return _switchLocalDevBranchView; }
            set { _switchLocalDevBranchView = value; }
        }

        /// <summary>
        /// Property: Web application manager.
        /// </summary>
        public WebAppManager WebAppManager
        {
            get { return _webAppManager; }
            set { _webAppManager = value; }
        }

        /// <summary>
        /// Property: Script host manager.
        /// </summary>
        public ScriptHostManager ScriptHostManager
        {
            get { return _scriptHostManager; }
            set { _scriptHostManager = value; }
        }

        /// <summary>
        /// Property: Project manager.
        /// </summary>
        public ProjectManager ProjectManager
        {
            get { return _projectManager; }
            set { _projectManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="switchLocalDevBranchView"></param>
        public SwitchLocalDevBranchPresenter(ITeamProjectInfo teamProjectInfo,
                                             SidekickConfiguration sidekickConfiguration,
                                             ISwitchLocalDevBranchView switchLocalDevBranchView)
        {
            this.TeamProjectInfo = teamProjectInfo;
            this.SidekickConfiguration = sidekickConfiguration;
            this.SwitchLocalDevBranchView = switchLocalDevBranchView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private SwitchLocalDevBranchPresenter()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {
            try
            {

                this.SwitchLocalDevBranchView.SwitchWebAppVirtualDirectoryPhysicalPath = true;
                this.SwitchLocalDevBranchView.StopSite = true;
                this.SwitchLocalDevBranchView.StartSite = true;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalDuration;
            AggregateException aggregateException = null;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.SwitchLocalDevBranchView.BackgroundTaskCompleted("Switch local development branch request canceled.");
                        this.SwitchLocalDevBranchView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format("switch local development branch  request cancelled."));

                        //Log switch local development branch completed log entry.
                        this.Logger.Info(string.Format("[*] Switch local development branch request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                        totalDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.SwitchLocalDevBranchView.BackgroundTaskCompleted("");
                        this.SwitchLocalDevBranchView.ApplyButtonEnable(true);


                        //Log switch local development branch completed log entry.

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    //Handle operation canceled exception.
                                    this.Logger.Info(string.Format("Operation cancelled."));
                                }

                            }
                        }

                        this.Logger.Info(string.Format("[*] Switch local development branch request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                       totalDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.SwitchLocalDevBranchView.BackgroundTaskCompleted("Switch local development branch request completed.");
                        this.SwitchLocalDevBranchView.ApplyButtonEnable(true);
                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }


            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Execute switch local dev branch tasks.
        /// </summary>
        /// <param name="teamProjectCollectionName"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="branchName"></param>
        /// <param name="webAppSelectorList"></param>
        public void ExecuteSwitchLocalDevBranchTasks(Uri teamProjectCollectionName,
                                                     string teamProjectName,
                                                     string branchName,
                                                     WebAppSelectorList webAppSelectorList)
        {


            WebAppList webAppList = null;
            WebApp webApp = null;
            ScriptConfig scriptConfig = null;
            ScriptHostCMD scriptHostCMD = null;
            ScriptHostList scriptHostList = null;
            //string teamProjectWorkingFolder = string.Empty;
            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.SwitchLocalDevBranchView.BackgroundTaskStatus = "Cancelling switch local development branch request...";
                    this.SwitchLocalDevBranchView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {

                    this.WebAppManager = this.CreateWebAppManager();
                    this.ScriptHostManager = this.CreateScriptHostManager();
                    webAppList = new WebAppList();
                    scriptHostList = new ScriptHostList();

                    this.ProjectManager = this.CreateProjectManager(teamProjectCollectionName.ToString(), teamProjectName);
                    //teamProjectWorkingFolder = this.ProjectManager.GetTeamProjectWorkingFolder(this.TeamProjectInfo.TeamProjectName,
                    //                                                                           this.TeamProjectInfo.WorkspaceName, 
                    //                                                                           this.TeamProjectInfo.BasicAuthRestAPIUserProfileName);

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    //Convert web app selector list to web app list.
                    foreach (WebAppSelector webAppSelector in webAppSelectorList)
                    {
                        webApp = new WebApp();
                        webApp.SiteName = webAppSelector.SiteName;
                        webApp.AppID = webAppSelector.AppID;
                        webApp.Alias = webAppSelector.Alias;
                        webApp.ApplicationPoolName = webAppSelector.ApplicationPoolName;
                        webApp.Path = webAppSelector.Path;
                        webApp.VirtualDirectoryList = webAppSelector.VirtualDirectoryList;
                        webAppList.Add(webApp);
                    }

                    this.WebAppManager.PhysicalPathChanged += OnPhysicalPathChanged;
                    this.ScriptHostManager.ExecuteScriptStarted += OnExecuteScriptStarted;
                    this.ScriptHostManager.ExecuteScriptCompleted += OnExecuteScriptCompleted;

                    if (scriptHostList.Count > 0)
                    {
                        this.BackgroundTask = new Task(() => { this.WebAppManager.ChangePhysicalPath(webAppList, cancellationToken); this.ScriptHostManager.ExecuteScript(scriptHostList, cancellationToken); }, cancellationToken);
                    }
                    else
                    {
                        this.BackgroundTask = new Task(() => { this.WebAppManager.ChangePhysicalPath(webAppList, cancellationToken); }, cancellationToken);
                    }

                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;
                    this.SwitchLocalDevBranchView.BackgroundTaskStatus = "Switch local development branch requested...";
                    this.SwitchLocalDevBranchView.ApplyButtonText = "Cancel";

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Stop site.
        /// </summary>
        /// <param name="siteName"></param>
        public void StopSite(string siteName)
        {

            try
            {

                this.WebAppManager = this.CreateWebAppManager();

                this.WebAppManager.StopSite(siteName);

                this.Logger.Info(string.Format("Site stopped. (SiteName: {0})",
                                               siteName));

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Create missing web apps.
        /// </summary>
        /// <param name="webAppSelectorList"></param>
        public void CreateMissingWebApps(WebAppSelectorList webAppSelectorList)
        {
            WebApp webApp = null;

            try
            {

                foreach (WebAppSelector webAppSelector in webAppSelectorList)
                {
                    if (webAppSelector.ExistsInIIS == false)
                    {
                        webApp = webAppSelector.GetWebAppFromWebAppSelector();
                        this.WebAppManager.CreateWebApp(webApp, webAppSelector.ApplicationPoolName);
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Start site.
        /// </summary>
        /// <param name="siteName"></param>
        public void StartSite(string siteName)
        {

            try
            {

                this.WebAppManager = this.CreateWebAppManager();

                this.WebAppManager.StartSite(siteName);
                this.Logger.Info(string.Format("Site start. (SiteName: {0})",
                                               siteName));

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Stop/start site.
        /// </summary>
        /// <param name="siteName"></param>
        public void StopStartSite(string siteName)
        {

            try
            {

                this.WebAppManager = this.CreateWebAppManager();

                this.WebAppManager.RestartSite(siteName);

                this.Logger.Info(string.Format("Site stopped/started. (SiteName: {0})",
                                               siteName));

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create web application manager.
        /// </summary>
        /// <returns></returns>
        protected WebAppManager CreateWebAppManager()
        {
            WebAppManager result = null;
            WebAppManagerFactory webAppManagerFactory = null;

            try
            {

                webAppManagerFactory = new WebAppManagerFactory();

                result = webAppManagerFactory.CreateWebAppManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create script host manager.
        /// </summary>
        /// <returns></returns>
        protected ScriptHostManager CreateScriptHostManager()
        {
            ScriptHostManager result = null;
            ScriptHostManagerFactory scriptHostManagerFactory = null;

            try
            {
                scriptHostManagerFactory = new ScriptHostManagerFactory();

                result = scriptHostManagerFactory.CreateScriptHostManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        /// <summary>
        /// Event Handler: Physical path changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="changePhysicalPathEventArgs"></param>
        protected void OnPhysicalPathChanged(Object sender,
                                             ChangePhysicalPathEventArgs changePhysicalPathEventArgs)
        {

            try
            {

                //Physical path details available and status list details available.
                if (changePhysicalPathEventArgs.ChangePhysicalPathResult.WebApp != null &&
                    changePhysicalPathEventArgs.ChangePhysicalPathResult.StatusList != null)
                {


                    if (changePhysicalPathEventArgs.ChangePhysicalPathResult.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error) == true)
                    {
                        this.Logger.Error(string.Format("Web application virtual directory switched to selected branch with error(s). (Path: {0}, Status --> {1})",
                                                        changePhysicalPathEventArgs.ChangePhysicalPathResult.WebApp.Path,
                                                        changePhysicalPathEventArgs.ChangePhysicalPathResult.StatusList.Format(StatusTypes.FormatOption.Minimum)));

                    }
                    else if (changePhysicalPathEventArgs.ChangePhysicalPathResult.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Warning) == true)
                    {
                        this.Logger.Warn(string.Format("Web application virtual directory switched to selected branch with warnings. (Path: {0}, Status --> {1})",
                                                       changePhysicalPathEventArgs.ChangePhysicalPathResult.WebApp.Path,
                                                       changePhysicalPathEventArgs.ChangePhysicalPathResult.StatusList.Format(StatusTypes.FormatOption.Minimum)));
                    }
                    else
                    {
                        if (changePhysicalPathEventArgs.ChangePhysicalPathResult.StatusList.Count > 0)
                        {
                            this.Logger.Info(string.Format("Web application virtual directory switched to selected branch. (Path: {0}, Status --> {1})",
                                                           changePhysicalPathEventArgs.ChangePhysicalPathResult.WebApp.Path,
                                                           changePhysicalPathEventArgs.ChangePhysicalPathResult.StatusList.Format(StatusTypes.FormatOption.Minimum)));
                        }
                        else
                        {
                            this.Logger.Info(string.Format("Web application virtual directory switched to selected branch. (Path: {0})",
                                                           changePhysicalPathEventArgs.ChangePhysicalPathResult.WebApp.Path,
                                                           changePhysicalPathEventArgs.ChangePhysicalPathResult.StatusList.Format(StatusTypes.FormatOption.Minimum)));
                        }
                    }
                }
                //Web app details unavailable.
                else
                {
                    this.Logger.Warn(string.Format("Web app virtual director(ies) physical path changed. Details unavailable."));
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Execute script started.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="executeScriptStartedEventArgs"></param>
        protected void OnExecuteScriptStarted(Object sender,
                                             ExecuteScriptStartedEventArgs executeScriptStartedEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Script name and status list details available.
                if (executeScriptStartedEventArgs.ExecuteScriptStartedResult.ScriptName != null &&
                    executeScriptStartedEventArgs.ExecuteScriptStartedResult.StatusList != null)
                {

                    //Status list is not empty.
                    if (executeScriptStartedEventArgs.ExecuteScriptStartedResult.StatusList.Count > 0)
                    {
                        message = string.Format("Execute script started with error(s). (Script name: {0}, Status --> {2})",
                                                executeScriptStartedEventArgs.ExecuteScriptStartedResult.ScriptName,
                                                executeScriptStartedEventArgs.ExecuteScriptStartedResult.StatusList.Format(Aclara.Script.Host.StatusManagement.StatusTypes.FormatOption.Minimum));

                        //Status list has errors or execute script failed.
                        if (executeScriptStartedEventArgs.ExecuteScriptStartedResult.StatusList.HasStatusSeverity(Aclara.Script.Host.StatusManagement.StatusTypes.StatusSeverity.Error))
                        {
                            this.Logger.Error(message);
                        }
                        else if (executeScriptStartedEventArgs.ExecuteScriptStartedResult.StatusList.HasStatusSeverity(Aclara.Script.Host.StatusManagement.StatusTypes.StatusSeverity.Warning))
                        {
                            this.Logger.Warn(message);
                        }
                        else
                        {
                            this.Logger.Info(message);
                        }


                    }
                    else if (executeScriptStartedEventArgs.ExecuteScriptStartedResult.ScriptName != null)
                    {
                        this.Logger.Info(string.Format("Execute script started. (Script name: {0})",
                                                         executeScriptStartedEventArgs.ExecuteScriptStartedResult.ScriptName));
                    }
                }
                else
                {
                    this.Logger.Warn(string.Format("Execute script started. Details unavailable."));
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Handler: Execute script completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="executeScriptCompletedEventArgs"></param>
        protected void OnExecuteScriptCompleted(Object sender,
                                                ExecuteScriptCompletedEventArgs executeScriptCompletedEventArgs)
        {

            string message = string.Empty;

            try
            {

                //Script name and status list details available.
                if (executeScriptCompletedEventArgs.ExecuteScriptCompletedResult.ScriptName != null &&
                    executeScriptCompletedEventArgs.ExecuteScriptCompletedResult.StatusList != null)
                {

                    //Status list is not empty.
                    if (executeScriptCompletedEventArgs.ExecuteScriptCompletedResult.StatusList.Count > 0)
                    {
                        message = string.Format("Execute script completed with error(s). (Script name: {0}, Duration: {1:dd\\.hh\\:mm\\:ss}, Status --> {2})",
                                                executeScriptCompletedEventArgs.ExecuteScriptCompletedResult.ScriptName,
                                                executeScriptCompletedEventArgs.ExecuteScriptCompletedResult.Duration,
                                                executeScriptCompletedEventArgs.ExecuteScriptCompletedResult.StatusList.Format(Aclara.Script.Host.StatusManagement.StatusTypes.FormatOption.Minimum));

                        //Status list has errors or execute script failed.
                        if (executeScriptCompletedEventArgs.ExecuteScriptCompletedResult.StatusList.HasStatusSeverity(Aclara.Script.Host.StatusManagement.StatusTypes.StatusSeverity.Error))
                        {
                            this.Logger.Error(message);
                        }
                        else if (executeScriptCompletedEventArgs.ExecuteScriptCompletedResult.StatusList.HasStatusSeverity(Aclara.Script.Host.StatusManagement.StatusTypes.StatusSeverity.Warning))
                        {
                            this.Logger.Warn(message);
                        }
                        else
                        {
                            this.Logger.Info(message);
                        }


                    }
                    else if (executeScriptCompletedEventArgs.ExecuteScriptCompletedResult.ScriptName != null)
                    {
                        this.Logger.Info(string.Format("Execute script completed. (Script name: {0}, Duration: {1:dd\\.hh\\:mm\\:ss})",
                                                       executeScriptCompletedEventArgs.ExecuteScriptCompletedResult.ScriptName,
                                                       executeScriptCompletedEventArgs.ExecuteScriptCompletedResult.Duration));
                    }

                }
                else
                {
                    this.Logger.Info(string.Format("Execute script completed. Details unavailable."));
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            AggregateException aggregateException = null;
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        aggregateException = eventArgs.Exception;
                        innerException = aggregateException.InnerException;
                        eventArgs.SetObserved();

                        this.Logger.Info(string.Format("Operation cancelled.(Unobserved)"));

                    }

                    return;
                }

                aggregateException = eventArgs.Exception;
                innerException = aggregateException.InnerException;
                eventArgs.SetObserved();

                this.Logger.Error(innerException, string.Format("Background task failed. --> Inner exception: {0}"));

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        /// <summary>
        /// Create project manager.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        protected ProjectManager CreateProjectManager(string tfsServerAddress,
                                                      string teamProjectName)
        {
            ProjectManager result = null;
            Uri tfsServerUri = null;

            try
            {

                tfsServerUri = new Uri(tfsServerAddress);

                result = ProjectManagerFactory.CreateProjectManager(tfsServerUri, teamProjectName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
