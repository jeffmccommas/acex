﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Switch local dev branch view interface.
    /// </summary>
    public interface ISwitchLocalDevBranchView
    {
        bool SwitchWebAppVirtualDirectoryPhysicalPath { get; set; }

        bool StopSite { get; set; }

        bool StartSite { get; set; }

        string BackgroundTaskStatus { get; set; }

        string ApplyButtonText { get; set; }

        void ApplyButtonEnable(bool enable);

        bool IsSidekickBusy { get; }

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);
    }
}
