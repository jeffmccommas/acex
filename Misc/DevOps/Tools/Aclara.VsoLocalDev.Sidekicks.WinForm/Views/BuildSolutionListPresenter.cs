﻿using Aclara.Build.Engine.Client;
using Aclara.Build.Engine.Client.Events;
using Aclara.Computer.Administrator.Client;
using Aclara.TeamFoundation.VersionControl.Client;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using Aclara.Web.Administration.Client;
using Aclara.Web.Administration.Client.Events;
using Aclara.Web.Administration.Client.StatusManagement;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Build solution list presenter.
    /// </summary>
    public class BuildSolutionListPresenter
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private SidekickConfiguration _sidekickConfiguration;
        private IBuildSolutionListView _buildSolutionListView;
        private ProjectManager _projectManager = null;
        private BuildManager _buildManager = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public Logger Logger
        {
            get
            {
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Build solution list view.
        /// </summary>
        public IBuildSolutionListView BuildSolutionListView
        {
            get { return _buildSolutionListView; }
            set { _buildSolutionListView = value; }
        }

        /// <summary>
        /// Property: Project manager.
        /// </summary>
        public ProjectManager ProjectManager
        {
            get { return _projectManager; }
            set { _projectManager = value; }
        }

        /// <summary>
        /// Property: Build manager.
        /// </summary>
        public BuildManager BuildManager
        {
            get { return _buildManager; }
            set { _buildManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildSolutionListView"></param>
        public BuildSolutionListPresenter(SidekickConfiguration sidekickConfiguration,
                                          IBuildSolutionListView buildSolutionListView)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.BuildSolutionListView = buildSolutionListView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private BuildSolutionListPresenter()
        {

        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalBuildDuration;
            AggregateException aggregateException = null;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalBuildDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.BuildSolutionListView.BackgroundTaskCompleted("Build solution list request canceled.");
                        this.BuildSolutionListView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format("Build solution list request cancelled."));


                        //Log background task completed log entry.

                        this.Logger.Info(string.Format("[*] Build solution list request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                       totalBuildDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.BuildSolutionListView.BackgroundTaskCompleted("");
                        this.BuildSolutionListView.ApplyButtonEnable(true);


                        //Log background task completed log entry.

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    //Handle operation canceled exception.
                                    this.Logger.Error(innerException, string.Format("Operation cancelled."));
                                }

                            }
                        }

                        this.Logger.Info(string.Format("[*] Build solution list request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                       totalBuildDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.BuildSolutionListView.BackgroundTaskCompleted("Build solution list request completed.");
                        this.BuildSolutionListView.ApplyButtonEnable(true);

                        //Log background task completed log entry.

                        this.Logger.Info(string.Format("[*] Build solution list request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})",
                                                       totalBuildDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }


            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Build solution list.
        /// </summary>
        /// <param name="teamProjectCollectionName"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="brachName"></param>
        /// <param name="buildTarget"></param>
        /// <param name="solutionSelectorList"></param>
        public void BuildSolutionList(string teamProjectCollectionName,
                                      string teamProjectName,
                                      string brachName,
                                      string buildTarget,
                                      SolutionSelectorList solutionSelectorList)
        {


            BuildSolutionRequestDataList buildSolutionRequestDataList = null;
            BuildSolutionRequestData buildSolutionRequestData = null;
            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.BuildSolutionListView.BackgroundTaskStatus = "Cancelling build request...";
                    this.BuildSolutionListView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {

                    this.BuildManager = this.CreateBuildManager();
                    buildSolutionRequestDataList = new BuildSolutionRequestDataList();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => { this.BackgroundTaskCompleted(); });

                    //Convert solution selector list to build solution request data list.
                    foreach (SolutionSelector solutionSelector in solutionSelectorList)
                    {
                        buildSolutionRequestData = new BuildSolutionRequestData();
                        buildSolutionRequestData.SolutionListName = solutionSelector.SolutionListName;
                        buildSolutionRequestData.SolutionName = solutionSelector.SolutionName;
                        buildSolutionRequestData.SolutionPath = solutionSelector.SolutionPath;
                        buildSolutionRequestData.MSBuildProperties = solutionSelector.MSBuildProperties;
                        buildSolutionRequestDataList.Add(buildSolutionRequestData);
                    }

                    this.BuildManager.BuildRequestStarted += OnBuildRequestStarted;
                    this.BuildManager.BuildRequestCompleted += OnBuildRequestCompleted;
                    this.BackgroundTask = new Task(() => this.BuildManager.RunBuild(buildSolutionRequestDataList, buildTarget, cancellationToken), cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;
                    this.BuildSolutionListView.BackgroundTaskStatus = "Build requested...";
                    this.BuildSolutionListView.ApplyButtonText = "Cancel";

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Event Handler: Solution build started.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="buildRequestStartedEventArgs"></param>
        protected void OnBuildRequestStarted(Object sender,
                                             BuildRequestStartedEventArgs buildRequestStartedEventArgs)
        {
            SolutionSelector solutionSelector = null;
            string message = string.Empty;

            try
            {

                //Build solution name and status list details available.
                if (buildRequestStartedEventArgs.BuildStartedResult.SolutionName != null &&
                    buildRequestStartedEventArgs.BuildStartedResult.StatusList != null)
                {

                    //Update solution build status.
                    solutionSelector = new SolutionSelector();
                    solutionSelector.SolutionListName = buildRequestStartedEventArgs.BuildStartedResult.SolutionListName;
                    solutionSelector.SolutionName = buildRequestStartedEventArgs.BuildStartedResult.SolutionName;
                    solutionSelector.BuildStatus = buildRequestStartedEventArgs.BuildStartedResult.BuildStatus;
                    this.BuildSolutionListView.UpdateSolutionBuildStatus(solutionSelector);

                    //Status list is not empty.
                    if (buildRequestStartedEventArgs.BuildStartedResult.StatusList.Count > 0)
                    {
                        message = string.Format("Build request started with error(s). (Solution name: {0}, Status --> {2})",
                                               buildRequestStartedEventArgs.BuildStartedResult.SolutionName,
                                               buildRequestStartedEventArgs.BuildStartedResult.StatusList.Format(Aclara.Build.Engine.Client.StatusManagement.StatusTypes.FormatOption.Minimum));

                        //Status list has errors or build failed.
                        if (buildRequestStartedEventArgs.BuildStartedResult.StatusList.HasStatusSeverity(Build.Engine.Client.StatusManagement.StatusTypes.StatusSeverity.Error))
                        {
                            this.Logger.Error(message);

                        }
                        else if (buildRequestStartedEventArgs.BuildStartedResult.StatusList.HasStatusSeverity(Build.Engine.Client.StatusManagement.StatusTypes.StatusSeverity.Warning))
                        {
                            this.Logger.Warn(message);
                        }
                        else
                        {
                            this.Logger.Info(message);
                        }

                    }
                    else if (buildRequestStartedEventArgs.BuildStartedResult.SolutionName != null)
                    {
                        message = string.Format("Build request started. (Solution name: {0})",
                                                  buildRequestStartedEventArgs.BuildStartedResult.SolutionName);
                        this.Logger.Info(message);
                    }

                }
                else
                {
                    this.Logger.Info(string.Format("Build request started. Details unavailable."));
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Handler: Solution build completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="buildRequestCompletedEventArgs"></param>
        protected void OnBuildRequestCompleted(Object sender,
                                             BuildRequestCompletedEventArgs buildRequestCompletedEventArgs)
        {
            SolutionSelector solutionSelector = null;
            string message = string.Empty;

            try
            {

                //Build solution name and status list details available.
                if (buildRequestCompletedEventArgs.BuildCompletedResult.SolutionName != null &&
                    buildRequestCompletedEventArgs.BuildCompletedResult.StatusList != null)
                {
                    //Update solution build status.
                    solutionSelector = new SolutionSelector();
                    solutionSelector.SolutionListName = buildRequestCompletedEventArgs.BuildCompletedResult.SolutionListName;
                    solutionSelector.SolutionName = buildRequestCompletedEventArgs.BuildCompletedResult.SolutionName;

                    solutionSelector.BuildOutput = buildRequestCompletedEventArgs.BuildCompletedResult.BuildOutput;
                    solutionSelector.BuildWarningList = buildRequestCompletedEventArgs.BuildCompletedResult.BuildWarningList;
                    solutionSelector.BuildErrorList = buildRequestCompletedEventArgs.BuildCompletedResult.BuildErrorList;
                    solutionSelector.BuildStatus = buildRequestCompletedEventArgs.BuildCompletedResult.BuildStatus;
                    this.BuildSolutionListView.UpdateSolutionBuildStatus(solutionSelector);

                    //Status list is not empty.
                    if (buildRequestCompletedEventArgs.BuildCompletedResult.StatusList.Count > 0)
                    {
                        message = string.Format("Build request completed with error(s). (Solution name: {0}, Build result: {1}, Duration: {2:dd\\.hh\\:mm\\:ss}), Status --> {3})",
                                                buildRequestCompletedEventArgs.BuildCompletedResult.SolutionName,
                                                buildRequestCompletedEventArgs.BuildCompletedResult.BuildStatus.ToString(),
                                                buildRequestCompletedEventArgs.BuildCompletedResult.BuildDuration,
                                                buildRequestCompletedEventArgs.BuildCompletedResult.StatusList.Format(Aclara.Build.Engine.Client.StatusManagement.StatusTypes.FormatOption.Minimum));

                        //Status list has errors or build failed.
                        if (buildRequestCompletedEventArgs.BuildCompletedResult.StatusList.HasStatusSeverity(Build.Engine.Client.StatusManagement.StatusTypes.StatusSeverity.Error) ||
                            buildRequestCompletedEventArgs.BuildCompletedResult.BuildStatus == Build.Engine.Client.BuildStatus.Failed)
                        {
                            this.Logger.Error(message);
                        }
                        else if (buildRequestCompletedEventArgs.BuildCompletedResult.StatusList.HasStatusSeverity(Build.Engine.Client.StatusManagement.StatusTypes.StatusSeverity.Warning))
                        {
                            this.Logger.Warn(message);
                        }
                        else
                        {
                            this.Logger.Info(message);
                        }


                    }
                    else if (buildRequestCompletedEventArgs.BuildCompletedResult.SolutionName != null)
                    {
                        message = string.Format("Build request completed. (Solution name: {0}, Build result: {1}, Duration: {2:dd\\.hh\\:mm\\:ss})",
                                                buildRequestCompletedEventArgs.BuildCompletedResult.SolutionName,
                                                buildRequestCompletedEventArgs.BuildCompletedResult.BuildStatus.ToString(),
                                                buildRequestCompletedEventArgs.BuildCompletedResult.BuildDuration);
                        if (buildRequestCompletedEventArgs.BuildCompletedResult.BuildStatus == BuildStatus.Failed)
                        {
                            this.Logger.Error(message);
                        }
                        else
                        {
                            this.Logger.Info(message);
                        }

                    }

                }
                else
                {
                    this.Logger.Info(message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            AggregateException aggregateException = null;
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.

                        aggregateException = eventArgs.Exception;
                        innerException = aggregateException.InnerException;
                        eventArgs.SetObserved();

                        this.Logger.Error(innerException, string.Format("Operation cancelled.(Unobserved)"));

                    }

                    return;
                }


                aggregateException = eventArgs.Exception;
                innerException = aggregateException.InnerException;
                eventArgs.SetObserved();

                this.Logger.Error(innerException, string.Format("Background task failed. --> Inner exception: {0}",
                                                 innerException));

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        /// <summary>
        /// Create build manager.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        protected BuildManager CreateBuildManager()
        {
            BuildManager result = null;
            BuildManagerFactory buildManagerFactory = null;

            try
            {

                buildManagerFactory = new BuildManagerFactory();
                result = buildManagerFactory.CreateBuildManager();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create project manager.
        /// </summary>
        /// <param name="tfsServerAddress"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        protected ProjectManager CreateProjectManager(string tfsServerAddress,
                                                      string teamProjectName)
        {
            ProjectManager result = null;
            Uri tfsServerUri = null;

            try
            {

                tfsServerUri = new Uri(tfsServerAddress);

                result = ProjectManagerFactory.CreateProjectManager(tfsServerUri, teamProjectName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
