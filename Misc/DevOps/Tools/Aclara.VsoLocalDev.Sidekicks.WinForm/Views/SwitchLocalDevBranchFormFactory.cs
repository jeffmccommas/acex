﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Switch local dev branch form factory.
    /// </summary>
    public static class SwitchLocalDevBranchFormFactory
    {

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="teamProjectInfo"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="webAppSearchForm"></param>
        /// <returns></returns>
        public static SwitchLocalDevBranchForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                          SidekickConfiguration sidekickConfiguration,
                                                          WebAppSearchForm webAppSearchForm)
        {

            SwitchLocalDevBranchForm result = null;
            SwitchLocalDevBranchPresenter switchLocalDevBranchPresenter = null;

            try
            {
                result = new SwitchLocalDevBranchForm(sidekickConfiguration,
                                                      webAppSearchForm);
                switchLocalDevBranchPresenter = new SwitchLocalDevBranchPresenter(teamProjectInfo,
                                                                                 sidekickConfiguration,
                                                                                 result);
                result.SwitchLocalDevBranchPresenter = switchLocalDevBranchPresenter;
                result.TopLevel = false;

                switchLocalDevBranchPresenter.Initialize();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

    }
}
