﻿using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Logging;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Build solution list form factory.
    /// </summary>
    public class AzureRedisCacheAdminFormFactory
    {

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="logWriter"></param>
        /// <param name="sidekickConfiguration"></param>
        /// <returns></returns>
        public static AzureRedisCacheAdminForm CreateForm(ITeamProjectInfo teamProjectInfo,
                                                          SidekickConfiguration sidekickConfiguration)
        {

            AzureRedisCacheAdminForm result = null;
            AzureRedisCacheAdminPresenter AzureRedisCacheAdminPresenter = null;

            try
            {
                result = new AzureRedisCacheAdminForm(sidekickConfiguration);

                AzureRedisCacheAdminPresenter = new AzureRedisCacheAdminPresenter(teamProjectInfo, sidekickConfiguration, result);

                result.AzureRedisCacheAdminPresenter = AzureRedisCacheAdminPresenter;
                result.TopLevel = false;

                AzureRedisCacheAdminPresenter.Initialize();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

    }
}
