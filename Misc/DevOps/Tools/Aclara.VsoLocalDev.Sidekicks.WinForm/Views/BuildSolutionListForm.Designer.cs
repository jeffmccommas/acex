﻿namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    partial class BuildSolutionListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.SolutionListSearchPanel = new System.Windows.Forms.Panel();
            this.SolutionListSearchPlaceholderLabel = new System.Windows.Forms.Label();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.ErrorListLabel = new System.Windows.Forms.Label();
            this.CopyButton = new System.Windows.Forms.Button();
            this.BuildErrorsDataGridView = new System.Windows.Forms.DataGridView();
            this.SeverityIconColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.SeverityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MessageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisplayWarningsCheckBox = new System.Windows.Forms.CheckBox();
            this.DisplayErrorsCheckBox = new System.Windows.Forms.CheckBox();
            this.OptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.BuildTargetComboBox = new System.Windows.Forms.ComboBox();
            this.BuildTargetLabel = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.VerticalSplitter = new System.Windows.Forms.Splitter();
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.HelpButton = new System.Windows.Forms.Button();
            this.SolutionListSearchPanel.SuspendLayout();
            this.PropertiesPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BuildErrorsDataGridView)).BeginInit();
            this.OptionsGroupBox.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // SolutionListSearchPanel
            // 
            this.SolutionListSearchPanel.AutoScroll = true;
            this.SolutionListSearchPanel.Controls.Add(this.SolutionListSearchPlaceholderLabel);
            this.SolutionListSearchPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.SolutionListSearchPanel.Location = new System.Drawing.Point(0, 0);
            this.SolutionListSearchPanel.Name = "SolutionListSearchPanel";
            this.SolutionListSearchPanel.Size = new System.Drawing.Size(500, 600);
            this.SolutionListSearchPanel.TabIndex = 0;
            // 
            // SolutionListSearchPlaceholderLabel
            // 
            this.SolutionListSearchPlaceholderLabel.AutoSize = true;
            this.SolutionListSearchPlaceholderLabel.Location = new System.Drawing.Point(164, 294);
            this.SolutionListSearchPlaceholderLabel.Name = "SolutionListSearchPlaceholderLabel";
            this.SolutionListSearchPlaceholderLabel.Size = new System.Drawing.Size(160, 13);
            this.SolutionListSearchPlaceholderLabel.TabIndex = 0;
            this.SolutionListSearchPlaceholderLabel.Text = "Solution List Search Placeholder";
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Controls.Add(this.ErrorListLabel);
            this.PropertiesPanel.Controls.Add(this.CopyButton);
            this.PropertiesPanel.Controls.Add(this.BuildErrorsDataGridView);
            this.PropertiesPanel.Controls.Add(this.DisplayWarningsCheckBox);
            this.PropertiesPanel.Controls.Add(this.DisplayErrorsCheckBox);
            this.PropertiesPanel.Controls.Add(this.OptionsGroupBox);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesPanel.Location = new System.Drawing.Point(500, 0);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(500, 600);
            this.PropertiesPanel.TabIndex = 5;
            // 
            // ErrorListLabel
            // 
            this.ErrorListLabel.AutoSize = true;
            this.ErrorListLabel.Location = new System.Drawing.Point(12, 105);
            this.ErrorListLabel.Name = "ErrorListLabel";
            this.ErrorListLabel.Size = new System.Drawing.Size(48, 13);
            this.ErrorListLabel.TabIndex = 6;
            this.ErrorListLabel.Text = "Error List";
            // 
            // CopyButton
            // 
            this.CopyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CopyButton.Location = new System.Drawing.Point(422, 121);
            this.CopyButton.Name = "CopyButton";
            this.CopyButton.Size = new System.Drawing.Size(75, 23);
            this.CopyButton.TabIndex = 3;
            this.CopyButton.Text = "Copy";
            this.CopyButton.UseVisualStyleBackColor = true;
            this.CopyButton.Click += new System.EventHandler(this.CopyButton_Click);
            // 
            // BuildErrorsDataGridView
            // 
            this.BuildErrorsDataGridView.AllowUserToAddRows = false;
            this.BuildErrorsDataGridView.AllowUserToDeleteRows = false;
            this.BuildErrorsDataGridView.AllowUserToResizeRows = false;
            this.BuildErrorsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuildErrorsDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.BuildErrorsDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BuildErrorsDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.BuildErrorsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.BuildErrorsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SeverityIconColumn,
            this.SeverityColumn,
            this.MessageColumn});
            this.BuildErrorsDataGridView.EnableHeadersVisualStyles = false;
            this.BuildErrorsDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.BuildErrorsDataGridView.Location = new System.Drawing.Point(12, 150);
            this.BuildErrorsDataGridView.Name = "BuildErrorsDataGridView";
            this.BuildErrorsDataGridView.ReadOnly = true;
            this.BuildErrorsDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BuildErrorsDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.BuildErrorsDataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            this.BuildErrorsDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.BuildErrorsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.BuildErrorsDataGridView.ShowEditingIcon = false;
            this.BuildErrorsDataGridView.Size = new System.Drawing.Size(480, 404);
            this.BuildErrorsDataGridView.TabIndex = 4;
            // 
            // SeverityIconColumn
            // 
            this.SeverityIconColumn.HeaderText = "";
            this.SeverityIconColumn.Name = "SeverityIconColumn";
            this.SeverityIconColumn.ReadOnly = true;
            this.SeverityIconColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SeverityIconColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SeverityIconColumn.ToolTipText = "Severity Icon";
            this.SeverityIconColumn.Width = 20;
            // 
            // SeverityColumn
            // 
            this.SeverityColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.SeverityColumn.HeaderText = "Severity";
            this.SeverityColumn.MinimumWidth = 50;
            this.SeverityColumn.Name = "SeverityColumn";
            this.SeverityColumn.ReadOnly = true;
            this.SeverityColumn.ToolTipText = "Severity";
            this.SeverityColumn.Width = 70;
            // 
            // MessageColumn
            // 
            this.MessageColumn.HeaderText = "Message";
            this.MessageColumn.MinimumWidth = 400;
            this.MessageColumn.Name = "MessageColumn";
            this.MessageColumn.ReadOnly = true;
            this.MessageColumn.ToolTipText = "Message";
            this.MessageColumn.Width = 400;
            // 
            // DisplayWarningsCheckBox
            // 
            this.DisplayWarningsCheckBox.AutoSize = true;
            this.DisplayWarningsCheckBox.Checked = true;
            this.DisplayWarningsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DisplayWarningsCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.DisplayWarningsCheckBox.Location = new System.Drawing.Point(79, 124);
            this.DisplayWarningsCheckBox.Name = "DisplayWarningsCheckBox";
            this.DisplayWarningsCheckBox.Size = new System.Drawing.Size(78, 17);
            this.DisplayWarningsCheckBox.TabIndex = 2;
            this.DisplayWarningsCheckBox.Text = "0 Warnings";
            this.DisplayWarningsCheckBox.UseVisualStyleBackColor = true;
            this.DisplayWarningsCheckBox.CheckedChanged += new System.EventHandler(this.DisplayWarningsCheckBox_CheckedChanged);
            // 
            // DisplayErrorsCheckBox
            // 
            this.DisplayErrorsCheckBox.AutoSize = true;
            this.DisplayErrorsCheckBox.Checked = true;
            this.DisplayErrorsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DisplayErrorsCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.DisplayErrorsCheckBox.Location = new System.Drawing.Point(13, 124);
            this.DisplayErrorsCheckBox.Name = "DisplayErrorsCheckBox";
            this.DisplayErrorsCheckBox.Size = new System.Drawing.Size(60, 17);
            this.DisplayErrorsCheckBox.TabIndex = 1;
            this.DisplayErrorsCheckBox.Text = "0 Errors";
            this.DisplayErrorsCheckBox.UseVisualStyleBackColor = true;
            this.DisplayErrorsCheckBox.CheckedChanged += new System.EventHandler(this.DisplayErrorsCheckBox_CheckedChanged);
            // 
            // OptionsGroupBox
            // 
            this.OptionsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OptionsGroupBox.Controls.Add(this.BuildTargetComboBox);
            this.OptionsGroupBox.Controls.Add(this.BuildTargetLabel);
            this.OptionsGroupBox.Location = new System.Drawing.Point(12, 29);
            this.OptionsGroupBox.Name = "OptionsGroupBox";
            this.OptionsGroupBox.Size = new System.Drawing.Size(480, 70);
            this.OptionsGroupBox.TabIndex = 0;
            this.OptionsGroupBox.TabStop = false;
            this.OptionsGroupBox.Text = "Options";
            // 
            // BuildTargetComboBox
            // 
            this.BuildTargetComboBox.FormattingEnabled = true;
            this.BuildTargetComboBox.Location = new System.Drawing.Point(7, 36);
            this.BuildTargetComboBox.Name = "BuildTargetComboBox";
            this.BuildTargetComboBox.Size = new System.Drawing.Size(121, 21);
            this.BuildTargetComboBox.TabIndex = 1;
            // 
            // BuildTargetLabel
            // 
            this.BuildTargetLabel.AutoSize = true;
            this.BuildTargetLabel.Location = new System.Drawing.Point(7, 20);
            this.BuildTargetLabel.Name = "BuildTargetLabel";
            this.BuildTargetLabel.Size = new System.Drawing.Size(63, 13);
            this.BuildTargetLabel.TabIndex = 0;
            this.BuildTargetLabel.Text = "Build target:";
            // 
            // ControlPanel
            // 
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Controls.Add(this.ApplyButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 560);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(500, 40);
            this.ControlPanel.TabIndex = 5;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(422, 11);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 1;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // VerticalSplitter
            // 
            this.VerticalSplitter.BackColor = System.Drawing.SystemColors.ControlDark;
            this.VerticalSplitter.Location = new System.Drawing.Point(500, 0);
            this.VerticalSplitter.Name = "VerticalSplitter";
            this.VerticalSplitter.Size = new System.Drawing.Size(6, 600);
            this.VerticalSplitter.TabIndex = 1;
            this.VerticalSplitter.TabStop = false;
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.VsoLocalDev.Sidekicks.Help.chm";
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.HelpButton);
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(500, 24);
            this.PropertiesHeaderPanel.TabIndex = 7;
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(466, 24);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "Build Solution List";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(472, 1);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 4;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // BuildSolutionListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 600);
            this.ControlBox = false;
            this.Controls.Add(this.VerticalSplitter);
            this.Controls.Add(this.PropertiesPanel);
            this.Controls.Add(this.SolutionListSearchPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "BuildSolutionListForm";
            this.Text = "BuildSolutionListForm";
            this.SolutionListSearchPanel.ResumeLayout(false);
            this.SolutionListSearchPanel.PerformLayout();
            this.PropertiesPanel.ResumeLayout(false);
            this.PropertiesPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BuildErrorsDataGridView)).EndInit();
            this.OptionsGroupBox.ResumeLayout(false);
            this.OptionsGroupBox.PerformLayout();
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel SolutionListSearchPanel;
        private System.Windows.Forms.Label SolutionListSearchPlaceholderLabel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Splitter VerticalSplitter;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.GroupBox OptionsGroupBox;
        private System.Windows.Forms.ComboBox BuildTargetComboBox;
        private System.Windows.Forms.Label BuildTargetLabel;
        private System.Windows.Forms.DataGridView BuildErrorsDataGridView;
        private System.Windows.Forms.CheckBox DisplayWarningsCheckBox;
        private System.Windows.Forms.CheckBox DisplayErrorsCheckBox;
        private System.Windows.Forms.DataGridViewImageColumn SeverityIconColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SeverityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn MessageColumn;
        private System.Windows.Forms.Button CopyButton;
        private System.Windows.Forms.Label ErrorListLabel;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.Button HelpButton;
    }
}