﻿using Aclara.NuGet.Core.Client.Models;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Events;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using NLog;
using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    public partial class NuGetPackageAnalyzerForm : Form, INuGetPackageAnalyzerView, ISidekickInfo, ISidekickView
    {

        #region Private Constants

        private const string SidekickView_SidekickName = "NuGetPackageAnalyzer";
        private const string SidekickView_SidekickDescription = "NuGet Package Analyzer";

        private const string BuildErrorsDataGridView_SeverityColumn = "SeverityColumn";
        private const string BuildErrorsDataGridView_SeverityIconColumn = "SeverityIconColumn";
        private const string BuildErrorsDataGridView_MessageColumn = "MessageColumn";

        private const string BuildErrorsDataGridView_Severity = "Severity";
        private const string BuildErrorsDataGridView_Message = "Message";

        private const string LogEventInfoLevel_Error = "Error";
        private const string LogEventInfoLevel_Warn = "Warn";

        private const string LogEventInfoDisplayLevel_Error = "Error";
        private const string LogEventInfoDisplayLevel_Warn = "Warning";

        private const string DisplayErrorsCheckboxText = "Errors";
        private const string DisplayWarningsCheckboxText = "Warnings";

        private const string ValidationErrorMessage_TeamProjectNameNotSelected = "Team project not selected.";
        private const string ValidationErrorMessage_SolutionNotSelected = "Solution not selected.";

        private const string Sidekick_Purpose = "Build related solutions for specified branch.";
        private const string Sidekick_Directions = "Pick Team Project, select branch, select one or more solutions and click Apply.";
        private const string Sidekick_Warnings = "No warnings. NOTE: If build fails then build solution with Visual Studio to find/fix build errors.";

        private const string NuGetPackageReferencedPathDataGridViewColumn_Extra = "Extra";

        #endregion

        #region Private Data Members

        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private SidekickConfiguration _sidekickConfiguration;
        private NuGetPackageSearchForm _nuGetPackageSearchForm = null;
        private NuGetPackageAnalyzerPresenter _nuGetPackageAnalyzerPresenter;
        private NuGetPackageInfoDisplayList _nuGetPackageInfoDisplayList;

        #endregion

        #region Public Types

        public enum BuildTargets
        {
            Build = 0,
            Clean = 1,
            Rebuild = 2
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickView_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickView_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public Logger Logger
        {
            get
            {
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick warnings.
        /// </summary>
        public string SidekickWarnings
        {
            get { return Sidekick_Warnings; }
        }

        /// <summary>
        /// Property: Sidekick purpose.
        /// </summary>
        public string SidekickPurpose
        {
            get { return Sidekick_Purpose; }
        }

        /// <summary>
        /// Property: Sidekick directions.
        /// </summary>
        public string SidekickDirections
        {
            get { return Sidekick_Directions; }
        }

        /// <summary>
        /// Property: Build solution list presenter.
        /// </summary>
        public NuGetPackageAnalyzerPresenter NuGetPackageAnalyzerPresenter
        {
            get { return _nuGetPackageAnalyzerPresenter; }
            set { _nuGetPackageAnalyzerPresenter = value; }
        }

        /// <summary>
        /// Property: NuGet package info display list.
        /// </summary>
        public NuGetPackageInfoDisplayList NuGetPackageInfoDisplayList
        {
            get { return _nuGetPackageInfoDisplayList; }
            set { _nuGetPackageInfoDisplayList = value; }
        }

        /// <summary>
        /// Property: NuGet package search form.
        /// </summary>
        public NuGetPackageSearchForm NuGetPackageSearchForm
        {
            get { return _nuGetPackageSearchForm; }
            set { _nuGetPackageSearchForm = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get
            {
                return this.BackgroundTaskStatusLabel.Text;
            }
            set
            {
                this.BackgroundTaskStatusLabel.Text = value;
            }
        }

        /// <summary>
        /// Property: Is sidekick busy.
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;
                result = this.NuGetPackageAnalyzerPresenter.IsBackgroundTaskBusy;
                return result;
            }
        }

        /// <summary>
        /// Property: Reference count.
        /// </summary>
        public string ReferenceCount
        {
            get { return this.ReferenceCountValueLabel.Text; }
            set { this.ReferenceCountValueLabel.Text = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="solutionListSearchForm"></param>
        public NuGetPackageAnalyzerForm(SidekickConfiguration sidekickConfiguration,
                                        NuGetPackageSearchForm solutionListSearchForm)
        {
            InitializeComponent();


            this.SidekickConfiguration = sidekickConfiguration;
            this.NuGetPackageSearchForm = solutionListSearchForm;

            this.InitializeControls();

            this.NuGetPackageSearchForm.NuGetPackageVersionSelectionChanged += OnNuGetPackageVersionSelectionChanged;

            this.SolutionListSearchPanel.DockControl(this.NuGetPackageSearchForm);

            foreach (DataGridViewColumn column in this.NuGetPackageReferencedPathDataGridView.Columns)
            {

                switch (column.Name)
                {
                    case BuildErrorsDataGridView_SeverityColumn:
                        column.DataPropertyName = BuildErrorsDataGridView_Severity;
                        break;
                    case BuildErrorsDataGridView_MessageColumn:
                        column.DataPropertyName = BuildErrorsDataGridView_Message;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        break;

                }
            }

        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private NuGetPackageAnalyzerForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {
            this.NuGetPackageSearchForm.TeamProjectChanged();
            this.EnableControlsDependantOnTeamProject(true);
        }
        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {

                this.EnableControlsDependantOnTeamProject(false);
                this.EnableControlsDependantOnNuGetPackageVersion(false);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: NuGet package version selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="nuGetPackageVersionSelectionChangedEventArgs"></param>
        protected void OnNuGetPackageVersionSelectionChanged(Object sender,
                                                             NuGetPackageVersionSelectionChangedEventArgs nuGetPackageVersionSelectionChangedEventArgs)
        {
            try
            {

                this.ClearBuildErrorsDataGridView();

                this.NuGetPackageInfoDisplayList = this.NuGetPackageSearchForm.GetSelectedNuGetPackageDisplayList();

                this.PopulateNuGetPackageReferencedPathDataGridView();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Populate NuGet package referenced path data grid view.
        /// </summary>
        protected void PopulateNuGetPackageReferencedPathDataGridView()
        {
            int columnWidth = 0;
            try
            {
                if (this.NuGetPackageInfoDisplayList == null ||
                    this.NuGetPackageInfoDisplayList.Count == 0)
                {
                    this.EnableControlsDependantOnNuGetPackageVersion(false);
                }
                else
                {
                    this.EnableControlsDependantOnNuGetPackageVersion(true);
                }

                this.NuGetPackageReferencedPathDataGridView.DataSource = this.NuGetPackageInfoDisplayList;

                for (int columnIndex = 0; columnIndex < NuGetPackageReferencedPathDataGridView.Columns.Count; columnIndex++)
                {
                    if (NuGetPackageReferencedPathDataGridView.Columns[columnIndex].Name == NuGetPackageReferencedPathDataGridViewColumn_Extra)
                    {
                        NuGetPackageReferencedPathDataGridView.Columns[columnIndex].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    }
                    else
                    {
                        NuGetPackageReferencedPathDataGridView.Columns[columnIndex].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        columnWidth = NuGetPackageReferencedPathDataGridView.Columns[columnIndex].Width;
                        NuGetPackageReferencedPathDataGridView.Columns[columnIndex].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                        NuGetPackageReferencedPathDataGridView.Columns[columnIndex].Width = columnWidth;
                    }

                }
                this.ReferenceCount = NuGetPackageInfoDisplayList.Count.ToString();

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Clear NuGet package referenced path data grid view.
        /// </summary>
        protected void ClearBuildErrorsDataGridView()
        {
            try
            {
                this.NuGetPackageReferencedPathDataGridView.DataSource = null;
                this.NuGetPackageReferencedPathDataGridView.Rows.Clear();
                this.NuGetPackageReferencedPathDataGridView.Refresh();

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update display (errors, warnings, information) checkbox text.
        /// </summary>
        protected void UpdateDisplayCheckboxText()
        {
            int errorsCount = 0;
            int warningsCount = 0;

            try
            {
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable (disable) controls dependant on team project.
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableControlsDependantOnTeamProject(bool enable)
        {
        }

        /// <summary>
        /// Enable (disable) controls dependant on solution selector.
        /// </summary>
        protected void EnableControlsDependantOnNuGetPackageVersion(bool enable)
        {
            this.NuGetPackageReferencedPathDataGridView.Enabled = enable;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Display errors checkbox - Changed changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayErrorsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            this.PopulateNuGetPackageReferencedPathDataGridView();
        }

        /// <summary>
        /// Event Handler: Display warnings checkbox - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayWarningsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            this.PopulateNuGetPackageReferencedPathDataGridView();

        }

        /// <summary>
        /// Event Handler: Copy button - clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyButton_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            StringBuilder messageStringBuilder = null;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                messageStringBuilder = new StringBuilder();

                foreach (DataGridViewRow dataGridViewRow in this.NuGetPackageReferencedPathDataGridView.Rows)
                {
                    message = (string)dataGridViewRow.Cells[BuildErrorsDataGridView_MessageColumn].Value;

                    messageStringBuilder.AppendLine(string.Format("{0}",
                                                                  message.ToString()));

                }

                Clipboard.SetDataObject(messageStringBuilder.ToString());

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickNuGetPackageAnalyzer.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion
    }
}
