﻿using Aclara.NuGet.Core.Client.Models;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Configuration;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Events;
using Aclara.VsoLocalDev.Sidekicks.WinForm.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aclara.VsoLocalDev.Sidekicks.WinForm.Views
{
    /// <summary>
    /// NuGet package search form.
    /// </summary>
    public partial class NuGetPackageSearchForm : Form, INuGetPackageSearchView
    {

        #region Private Constants

        private const string NuGetPackageSearchFormTitle = "NuGet Package List Search";
        private const string BranchComboBoxPreset_None = "<Select Branch>";

        #endregion

        #region Private Data Members

        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private SidekickConfiguration _sidekickConfiguration;
        private NuGetPackageSearchPresenter _nuGetPackageSearchPresenter = null;
        private NuGetPackageInfoList _nuGetPackageInfoList;
        private SortOrders _sortOrder;


        #endregion

        #region Public Delegates

        public event EventHandler<NuGetPackageInfoSelectedEventArgs> NuGetPackageInfoSelected;
        public event EventHandler<NuGetPackageVersionSelectedEventArgs> NuGetPackageVersionSelected;
        public event EventHandler<NuGetPackageVersionSelectionChangedEventArgs> NuGetPackageVersionSelectionChanged;

        #endregion

        #region Public Enumerations

        public enum SortOrders
        {
            PackageId = 0,
            VersionCount = 1,
            ReferenceCount = 2
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public Logger Logger
        {
            get
            {
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: NuGet package search presenter.
        /// </summary>
        public NuGetPackageSearchPresenter NuGetPackageSearchPresenter
        {
            get { return _nuGetPackageSearchPresenter; }
            set { _nuGetPackageSearchPresenter = value; }
        }

        /// <summary>
        /// Property: Branch name.
        /// </summary>
        public string BranchName
        {
            get
            {
                string branchName = string.Empty;
                if (this.BranchComboBox.Text == null)
                {
                    branchName = string.Empty;
                }
                else
                {
                    branchName = this.BranchComboBox.Text;
                }
                return branchName;
            }
            set { this.BranchComboBox.SelectedItem = value; }
        }

        /// <summary>
        /// Property: NuGet package information list.
        /// </summary>
        public NuGetPackageInfoList NuGetPackageInfoList
        {
            get { return _nuGetPackageInfoList; }
            set { _nuGetPackageInfoList = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get
            {
                return this.BackgroundTaskStatusLabel.Text;
            }
            set
            {
                this.BackgroundTaskStatusLabel.Text = value;
            }
        }

        /// <summary>
        /// Property: Package id pattern.
        /// </summary>
        public string PackageIdPattern
        {
            get { return this.PackageIdPatternTextBox.Text; }
            set { this.PackageIdPatternTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Sort order.
        /// </summary>
        public SortOrders SortOrder
        {
            get { return _sortOrder; }
            set { _sortOrder = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        public NuGetPackageSearchForm(SidekickConfiguration sidekickConfiguration)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.NuGetPackageInfoList = new NuGetPackageInfoList();

            this.SortOrder = SortOrders.PackageId;

            this.InitializeComponent();

            this.EnableControlsDependantOnTeamProject(false);

            this.EnableControlsDependantOnBranch(false);

            this.EnableControlsDependantOnOrderedAndFilteredNuGetPackageInfoList(false);

            this.EnableControlsDependantOnNuGetPackageInfoList(false);

        }

        #endregion

        #region Protected Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        protected NuGetPackageSearchForm()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update background task status.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void UpdateBackgroundTaskStatus(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.UpdateBackgroundTaskStatus), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
            }
        }

        /// <summary>
        /// Apply button enable.
        /// </summary>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.RefreshButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Property: Is sidekick busy.
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;
                result = this.NuGetPackageSearchPresenter.IsBackgroundTaskBusy;
                return result;
            }
        }

        /// <summary>
        /// Team project changed.
        /// </summary>
        public void TeamProjectChanged()
        {

            try
            {

                this.NuGetPackageSearchPresenter.TeamProjectCollectionNameChanged();
                this.NuGetPackageSearchPresenter.TeamProjectNameChanged();

                this.NuGetPackageSearchPresenter.PopulateBrachNameList(this.NuGetPackageSearchPresenter.TeamProjectInfo.TeamProjectCollectionUri.ToString(),
                                                                       this.NuGetPackageSearchPresenter.TeamProjectInfo.TeamProjectName);

            }
            catch (Exception)
            {
                throw;
            }

        }


        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.EnableRefreshCancelMode(false);
                this.BackgroundTaskStatus = backgroundTaskStatus;
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.StatusPanel.BackColor = Color.White;
                this.EnableRefreshCancelMode(false);
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.BackgroundTaskStatus = string.Format("NuGet packages retrieved: {0}",
                                                          this.NuGetPackageInfoList.Count());
                this.PopulateNuGetPackageInfoListTreeView();
            }
        }

        /// <summary>
        /// Populate branch name list.
        /// </summary>
        /// <param name="branchNameList"></param>
        public void PopulateBranchNameList(List<string> branchNameList)
        {
            int index = 0;
            try
            {
                this.BranchComboBox.Items.Clear();
                this.BranchComboBox.Items.Insert(0, BranchComboBoxPreset_None);
                foreach (string branchName in branchNameList)
                {
                    this.BranchComboBox.Items.Insert(index++, branchName);
                }
                this.BranchComboBox.SelectedItem = BranchComboBoxPreset_None;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable controls dependant on team project.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnTeamProject(bool enabled)
        {
            try
            {
                this.BranchComboBox.Enabled = enabled;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable controls dependant on branch.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnBranch(bool enabled)
        {
            try
            {
                this.RefreshButton.Enabled = enabled;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable controls dependant on ordered and filtered NuGet package info list.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnOrderedAndFilteredNuGetPackageInfoList(bool enabled)
        {
            try
            {
                this.SelectSplitButton.Enabled = enabled;
                this.ToggleExpandSplitButton.Enabled = enabled;
                this.SortOrderSplitButton.Enabled = enabled;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable controls dependant on NuGet package info list.
        /// </summary>
        /// <param name="enabled"></param>
        public void EnableControlsDependantOnNuGetPackageInfoList(bool enabled)
        {
            try
            {
                this.SearchButton.Enabled = enabled;
                this.CopyToClipboardButton.Enabled = enabled;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable controls dependant NuGet package version node(s) checked.
        /// </summary>
        public void EnableControlsDependantOnNuGetPackageVersionNodeChecked()
        {
            NuGetPackageVersion nuGetPackageVersion = null;
            NuGetPackageVersionList nuGetPackageVersionList = null;

            try
            {

                nuGetPackageVersionList = new NuGetPackageVersionList();

                foreach (TreeNode parentTreeNode in this.NuGetPackageInfoListTreeView.Nodes)
                {
                    if (parentTreeNode.Nodes.Count > 0)
                    {
                        foreach (TreeNode treeNode in parentTreeNode.Nodes)
                        {
                            if (treeNode.Checked == true &&
                                treeNode.Tag != null &&
                                treeNode.Tag.GetType() == typeof(NuGetPackageVersion))
                            {
                                nuGetPackageVersion = (NuGetPackageVersion)treeNode.Tag;
                                nuGetPackageVersionList.Add(nuGetPackageVersion);
                            }
                        }
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Clear branch dependent controls.
        /// </summary>
        public void ClearBranchDependentControls()
        {
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve selected NuGet package display list.
        /// </summary>
        /// <returns></returns>
        public NuGetPackageInfoDisplayList GetSelectedNuGetPackageDisplayList()
        {
            NuGetPackageInfoDisplayList result = null;
            NuGetPackageInfoDisplay nuGetPackageInfoDisplay = null;
            NuGetPackageInfo nuGetPackageInfo = null;
            NuGetPackageVersion nuGetPackageVersion = null;

            try
            {
                result = new NuGetPackageInfoDisplayList();

                foreach (TreeNode parentTreeNode in this.NuGetPackageInfoListTreeView.Nodes)
                {
                    if (parentTreeNode.Nodes.Count > 0)
                    {
                        if (parentTreeNode.Tag != null &&
                            parentTreeNode.Tag.GetType() == typeof(NuGetPackageInfo))
                        {
                            nuGetPackageInfo = (NuGetPackageInfo)parentTreeNode.Tag;

                            foreach (TreeNode childtreeNode in parentTreeNode.Nodes)
                            {
                                if (childtreeNode.Checked == true &&
                                    childtreeNode.Tag != null &&
                                    childtreeNode.Tag.GetType() == typeof(NuGetPackageVersion))
                                {
                                    nuGetPackageVersion = (NuGetPackageVersion)childtreeNode.Tag;
                                    foreach (string referencePath in nuGetPackageVersion.ReferencedPathList)
                                    {
                                        nuGetPackageInfoDisplay = new NuGetPackageInfoDisplay();
                                        nuGetPackageInfoDisplay.PackageId = nuGetPackageInfo.PackageId;
                                        nuGetPackageInfoDisplay.Version = nuGetPackageVersion.Version;
                                        nuGetPackageInfoDisplay.ReferencedPath = referencePath;
                                        result.Add(nuGetPackageInfoDisplay);
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Sort NuGet package info list.
        /// </summary>
        /// <param name="filteredNuGetPackageInfoList"></param>
        /// <returns></returns>
        protected IOrderedEnumerable<NuGetPackageInfo> SortNuGetPackageInfoList(NuGetPackageInfoList filteredNuGetPackageInfoList)
        {
            IOrderedEnumerable<NuGetPackageInfo> result = null;

            try
            {
                switch (this.SortOrder)
                {
                    case SortOrders.PackageId:
                        result = filteredNuGetPackageInfoList.OrderBy(npi => npi.PackageId);
                        break;
                    case SortOrders.VersionCount:
                        result = filteredNuGetPackageInfoList.OrderByDescending(npi => npi.NuGetPackageVersionList.Count);

                        break;
                    case SortOrders.ReferenceCount:
                        result = filteredNuGetPackageInfoList.OrderByDescending(npi => npi.NuGetPackageVersionList.Sum(npv => npv.ReferencedPathList.Count()));
                        break;
                    default:
                        result = filteredNuGetPackageInfoList.OrderBy(npi => npi.PackageId);
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Change sort order.
        /// </summary>
        /// <param name="sortOrder"></param>
        protected void ChangeSortOrder(SortOrders sortOrder)
        {
            try
            {
                this.SortOrder = sortOrder;

                this.NuGetPackageInfoSortPackageIdToolStripItem.Checked = false;
                this.NuGetPackageInfoSortVersionCountToolStripItem.Checked = false;
                this.NuGetPackageInfoSortReferenceCountToolStripItem.Checked = false;

                switch (sortOrder)
                {
                    case SortOrders.PackageId:
                        this.NuGetPackageInfoSortPackageIdToolStripItem.Checked = true;
                        break;
                    case SortOrders.VersionCount:
                        this.NuGetPackageInfoSortVersionCountToolStripItem.Checked = true;
                        break;
                    case SortOrders.ReferenceCount:
                        this.NuGetPackageInfoSortReferenceCountToolStripItem.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Populate Nuget package info list tree view.
        /// </summary>
        protected void PopulateNuGetPackageInfoListTreeView()
        {
            TreeNode parentNode = null;
            TreeNode childNode = null;
            int nuGetPackageInfoTotalReferencesCount = 0;
            NuGetPackageInfoList filteredNuGetPackageInfoList = null;
            IOrderedEnumerable<NuGetPackageInfo> orderedAndFilteredNuGetPackageInfoList = null;
            bool exactMatch = false;
            NuGetPackageVersionSelectionChangedEventArgs nuGetPackageVersionSelectionChangedEventArgs = null;

            try
            {
                this.NuGetPackageInfoListTreeView.Nodes.Clear();
                this.NuGetPackageInfoListTreeView.Controls.Clear();

                filteredNuGetPackageInfoList = this.NuGetPackageInfoList.GetNuGetPackageInfoListFilterdByPackageIdPattern(PackageIdPattern,
                                                                                                                          exactMatch);

                orderedAndFilteredNuGetPackageInfoList = this.SortNuGetPackageInfoList(filteredNuGetPackageInfoList);

                foreach (NuGetPackageInfo nuGetPackageInfo in orderedAndFilteredNuGetPackageInfoList)
                {
                    nuGetPackageInfoTotalReferencesCount = nuGetPackageInfo.NuGetPackageVersionList.Sum(npv => npv.ReferencedPathList.Count());
                    parentNode = this.NuGetPackageInfoListTreeView.Nodes.Add(nuGetPackageInfo.PackageId);
                    parentNode.Text = string.Format("[{0}|{1}] {2}",
                                                    nuGetPackageInfo.NuGetPackageVersionList.Count.ToString().PadLeft(2, ' '),
                                                    nuGetPackageInfoTotalReferencesCount.ToString().PadLeft(3, ' '),
                                                    nuGetPackageInfo.PackageId);
                    parentNode.Tag = nuGetPackageInfo;

                    foreach (NuGetPackageVersion nuGetPackageVersion in nuGetPackageInfo.NuGetPackageVersionList.OrderBy(npv => npv.Version))
                    {
                        childNode = new TreeNode();

                        childNode.Name = nuGetPackageVersion.Version;
                        childNode.Text = string.Format("{0} ({1})",
                                                       nuGetPackageVersion.Version,
                                                       nuGetPackageVersion.ReferencedPathList.Count);
                        childNode.Tag = nuGetPackageVersion;

                        parentNode.Nodes.Add(childNode);
                    }
                }

                this.CollapseAllTreeNodes();

                if (orderedAndFilteredNuGetPackageInfoList == null ||
                    orderedAndFilteredNuGetPackageInfoList.Count() == 0)
                {
                    this.EnableControlsDependantOnOrderedAndFilteredNuGetPackageInfoList(false);
                }
                else
                {
                    this.EnableControlsDependantOnOrderedAndFilteredNuGetPackageInfoList(true);
                }

                if (this.NuGetPackageInfoList == null ||
                    this.NuGetPackageInfoList.Count() == 0)
                {
                    this.EnableControlsDependantOnNuGetPackageInfoList(false);
                }
                else
                {
                    this.EnableControlsDependantOnNuGetPackageInfoList(true);
                }

                if (this.NuGetPackageVersionSelectionChanged != null)
                {
                    this.NuGetPackageVersionSelectionChanged(this, nuGetPackageVersionSelectionChangedEventArgs);
                }


            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Expand all tree nodes.
        /// </summary>
        protected void ExpandAllTreeNodes()
        {
            try
            {
                foreach (TreeNode treeNode in this.NuGetPackageInfoListTreeView.Nodes)
                {
                    treeNode.Expand();
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Collapse all tree nodes.
        /// </summary>
        protected void CollapseAllTreeNodes()
        {
            try
            {
                foreach (TreeNode treeNode in this.NuGetPackageInfoListTreeView.Nodes)
                {
                    treeNode.Collapse();
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update refresh cancel mode.
        /// </summary>
        /// <param name="enabled"></param>
        protected void EnableRefreshCancelMode(bool enabled)
        {
            try
            {
                if (enabled == true)
                {
                    this.RefreshButton.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.ActionCancelRefresh;
                }
                else
                {
                    this.RefreshButton.Image = global::Aclara.VsoLocalDev.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Format NuGet package info as text.
        /// </summary>
        /// <returns></returns>
        protected string FormatNuGetPackageInfoAsText()
        {
            const string Header_PackageId = "PackagId";
            const string Header_Path = "Path";
            const string Header_Version = "Version";

            string result = string.Empty;
            StringBuilder formattedTextStringBuilder = null;
            NuGetPackageInfoList filteredNuGetPackageInfoList = null;
            IOrderedEnumerable<NuGetPackageInfo> orderedAndFilteredNuGetPackageInfoList = null;
            bool exactMatch = false;

            try
            {
                formattedTextStringBuilder = new StringBuilder();

                filteredNuGetPackageInfoList = this.NuGetPackageInfoList.GetNuGetPackageInfoListFilterdByPackageIdPattern(PackageIdPattern,
                                                                                                          exactMatch);

                orderedAndFilteredNuGetPackageInfoList = this.SortNuGetPackageInfoList(filteredNuGetPackageInfoList);

                //Add header.
                formattedTextStringBuilder.AppendLine(string.Format("{0}\t{1}\t{2}",
                                                                    Header_PackageId,
                                                                    Header_Path,
                                                                    Header_Version));

                foreach (NuGetPackageInfo nuGetPackageInfo in orderedAndFilteredNuGetPackageInfoList)
                {

                    foreach (NuGetPackageVersion nuGetPackageVersion in nuGetPackageInfo.NuGetPackageVersionList)
                    {
                        foreach (string path in nuGetPackageVersion.ReferencedPathList)
                        {
                            formattedTextStringBuilder.AppendLine(string.Format("{0}\t{1}\t{2}",
                                                                                nuGetPackageInfo.PackageId,
                                                                                path,
                                                                                nuGetPackageVersion.Version));
                        }
                    }
                }

                result = formattedTextStringBuilder.ToString();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

            return result;

        }

        #endregion

        #region Private Methods


        /// <summary>
        /// Event Handler: Branch combo box - Text changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BranchComboBox_TextChanged(object sender, EventArgs e)
        {
            ComboBox senderComboBox = null;

            try
            {
                if (sender is ComboBox != true)
                {
                    return;
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                this.Cursor = Cursors.WaitCursor;

                senderComboBox = (ComboBox)sender;
                if (senderComboBox.Text == BranchComboBoxPreset_None)
                {

                    EnableControlsDependantOnBranch(false);
                    this.ClearBranchDependentControls();
                    return;
                }

                EnableControlsDependantOnBranch(true);

                this.Cursor = Cursors.Default;

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
                this.ClearBranchDependentControls();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: NuGet package info list tree view - After check.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NuGetPackageInfoListTreeView_AfterCheck(object sender, TreeViewEventArgs e)
        {
            TreeNode treeNode = null;
            NuGetPackageInfo nuGetPackageInfo = null;
            NuGetPackageVersion nuGetPackageVersion = null;
            NuGetPackageVersionSelectionChangedEventArgs nuGetPackageVersionSelectionChangedEventArgs = null;

            try
            {

                treeNode = e.Node;

                if (treeNode == null ||
                    treeNode.Tag == null)
                {
                    return;
                }

                if (treeNode.Tag.GetType() == typeof(NuGetPackageVersion))
                {
                    nuGetPackageVersionSelectionChangedEventArgs = new NuGetPackageVersionSelectionChangedEventArgs();

                    nuGetPackageVersion = (NuGetPackageVersion)treeNode.Tag;
                    nuGetPackageVersionSelectionChangedEventArgs.NuGetPackageVersion = nuGetPackageVersion;
                    if (treeNode.Parent != null &&
                        treeNode.Parent.Tag != null &
                        treeNode.Parent.Tag.GetType() == typeof(NuGetPackageInfo))
                    {
                        nuGetPackageInfo = (NuGetPackageInfo)treeNode.Parent.Tag;
                        nuGetPackageVersionSelectionChangedEventArgs.NuGetPackageInfo = nuGetPackageInfo;

                    }

                    if (this.NuGetPackageVersionSelectionChanged != null)
                    {
                        this.NuGetPackageVersionSelectionChanged(this, nuGetPackageVersionSelectionChangedEventArgs);
                    }

                }

                if (e.Action != TreeViewAction.Unknown)
                {
                    if (e.Node.Nodes.Count > 0)
                    {
                        foreach (TreeNode childTreeNode in e.Node.Nodes)
                        {
                            childTreeNode.Checked = e.Node.Checked;
                        }
                    }


                }
                this.EnableControlsDependantOnNuGetPackageVersionNodeChecked();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: NuGet package info select none tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NuGetPackageInfoSelectNoneToolStripItem_Click(object sender, EventArgs e)
        {
            try
            {

                foreach (TreeNode parentTreeNode in this.NuGetPackageInfoListTreeView.Nodes)
                {
                    parentTreeNode.Checked = false;

                    foreach (TreeNode treeNode in parentTreeNode.Nodes)
                    {
                        treeNode.Checked = false;
                    }
                }
                this.EnableControlsDependantOnNuGetPackageVersionNodeChecked();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Expand all tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExpandAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.ExpandAllTreeNodes();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Collapse all tool strip menu item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CollapseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.CollapseAllTreeNodes();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: NuGet package info list tree view  - Node mouse click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NuGetPackageInfoListTreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode treeNode = null;

            try
            {
                treeNode = e.Node;
                this.NuGetPackageInfoListTreeView.SelectedNode = treeNode;

                if (e.Button == MouseButtons.Right)
                {

                    if (treeNode != null &&
                        treeNode.Tag != null &&
                        treeNode.Tag.GetType() == typeof(NuGetPackageInfo))
                    {
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: NuGet package info tree view - After select.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NuGetPackageInfoListTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //TreeNode treeNode = null;
            //NuGetPackageInfo nuGetPackageInfo = null;
            //NuGetPackageVersion nuGetPackageVersion = null;
            //NuGetPackageInfoSelectedEventArgs nuGetPackageInfoSelectedEventArgs = null;
            //NuGetPackageVersionSelectedEventArgs nuGetPackageVersionSelectedEventArgs = null;


            //treeNode = e.Node;

            //if (treeNode == null ||
            //    treeNode.Tag == null)
            //{
            //    return;
            //}

            //if (treeNode.Tag.GetType() == typeof(NuGetPackageInfo))
            //{
            //    nuGetPackageInfoSelectedEventArgs = new NuGetPackageInfoSelectedEventArgs();

            //    nuGetPackageInfo = (NuGetPackageInfo)treeNode.Tag;
            //    nuGetPackageInfoSelectedEventArgs.NuGetPackageInfo = nuGetPackageInfo;

            //    if (this.NuGetPackageInfoSelected != null)
            //    {
            //        this.NuGetPackageInfoSelected(this, nuGetPackageInfoSelectedEventArgs);
            //    }
            //}
            //else if (treeNode.Tag.GetType() == typeof(NuGetPackageVersion))
            //{
            //    nuGetPackageVersionSelectedEventArgs = new NuGetPackageVersionSelectedEventArgs();

            //    nuGetPackageVersion = (NuGetPackageVersion)treeNode.Tag;
            //    nuGetPackageVersionSelectedEventArgs.NuGetPackageVersion = nuGetPackageVersion;
            //    if (treeNode.Parent != null &&
            //        treeNode.Parent.Tag != null &
            //        treeNode.Parent.Tag.GetType() == typeof(NuGetPackageInfo))
            //    {
            //        nuGetPackageInfo = (NuGetPackageInfo)treeNode.Parent.Tag;
            //        nuGetPackageVersionSelectedEventArgs.NuGetPackageInfo = nuGetPackageInfo;

            //    }

            //    if (this.NuGetPackageVersionSelected != null)
            //    {
            //        this.NuGetPackageVersionSelected(this, nuGetPackageVersionSelectedEventArgs);
            //    }

            //}

        }

        /// <summary>
        /// Event Handler: Refresh button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.EnableRefreshCancelMode(true);
                this.StatusPanel.BackColor = Color.OldLace;
                this.NuGetPackageSearchPresenter.GetNuGetPackageInfoList(this.NuGetPackageSearchPresenter.TeamProjectInfo.TeamProjectCollectionUri.ToString(),
                                                                         this.NuGetPackageSearchPresenter.TeamProjectInfo.TeamProjectName,
                                                                         this.NuGetPackageSearchPresenter.TeamProjectInfo.WorkspaceName,
                                                                         this.NuGetPackageSearchPresenter.TeamProjectInfo.BasicAuthRestAPIUserProfileName,
                                                                         this.BranchName);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: NuGet package info copy text tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NuGetPackageInfoCopyTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string nuGetPackageInfoFormattedAsText = string.Empty;

            try
            {
                nuGetPackageInfoFormattedAsText = this.FormatNuGetPackageInfoAsText();

                Clipboard.SetText(nuGetPackageInfoFormattedAsText);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Search button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.PopulateNuGetPackageInfoListTreeView();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: NuGet package info sort package id tool strip item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NuGetPackageInfoSortPackageIdToolStripItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.ChangeSortOrder(SortOrders.PackageId);
                this.PopulateNuGetPackageInfoListTreeView();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: NuGet package info sort version count tool strip item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NuGetPackageInfoSortVersionCountToolStripItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.ChangeSortOrder(SortOrders.VersionCount);
                this.PopulateNuGetPackageInfoListTreeView();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: NuGet package info sort reference count tool strip item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NuGetPackageInfoSortReferenceCountToolStripItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.ChangeSortOrder(SortOrders.ReferenceCount);
                this.PopulateNuGetPackageInfoListTreeView();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion
    }

}
