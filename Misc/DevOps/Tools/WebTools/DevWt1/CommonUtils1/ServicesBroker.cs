﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;
using Microsoft.ServiceBus.Messaging;

// change for a git-tfs sync check
// above worked.
// another change in a clond of the original repo used for the above.

namespace AO.TimeSeriesProcessing {
    public class ServicesBroker {

        static bool _done;
        private static int _infoTimeSecs = 30;
        private static int _infoTimeMins = 30;


        public static string[] TestProcess(string[] args, string dataContent = null) {
            ProcessingArgs pa = new ProcessingArgs(args);
            return TestProcess(pa, dataContent);
        }



        public static string[] TestProcess(ProcessingArgs pa, string dataContent) {
            if (!string.IsNullOrEmpty(dataContent)) {
                pa["dataContent"] = dataContent;
                if (char.IsDigit(dataContent[0])) {
                    // assume no header row
                    pa["hasheaders"] = "0";
                }
            }
            return TestProcess(pa);
        }


        /// <summary>
        /// Entry point to support executing an operation against the series management subsystem including unit tests and validation
        /// </summary>
        /// <param name="pa"></param>
        /// <returns></returns>
        public static string[] TestProcess(ProcessingArgs pa) {
            string[] rVal = { "initRval" };
            try {
                DateTime dtStt = DateTime.Now;
                Console.WriteLine($"Starting at {dtStt}");
                if (pa["sqlMeterMgtCxnStg"] == null) {
                    pa["sqlMeterMgtCxnStg"] = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
                }
                ContextData.InitContextStrings("evc", pa["aclenv"], "none", pa, null);
                ProcessingArgs.AccumulateRequestPars("evc", pa["aclenv"], "none", pa);
                ContextData context = new ContextData(pa);
                bool excepted = false;
                try {
                    DateTime dt = DateTime.Now;
                    context.ContextWrite("ProcessOptions invoke", "Begin");
                    ProcessOptions(pa, context, dtStt);
                    context.ContextWrite("ProcessOptions invoke", "End", dt);
                }
                catch (Exception exc) {
                    excepted = true;
                    rVal = new[] { $"Exception in TestProcess {exc.Message}, {exc.InnerException?.Message}", };
                }
                finally {
                    if (!excepted) {
                        Console.WriteLine(context.Snag());
                        if (pa.GetControlVal("retdatapa", "yes").Equals("yes") && (pa["retdatatxt"] != null)) {
                            rVal = new[] {
                                context.ContexFileNamePath, context.Validation.FileName, context.AutoFixes.FileName,
                                context.NoFixes.FileName, context.Snag(), pa["retdatatxt"]
                            };
                        }
                        else {
                            rVal = new[] {
                                context.ContexFileNamePath, context.Validation.FileName, context.AutoFixes.FileName,
                                context.NoFixes.FileName, context.Snag()
                            };
                        }
                    }
                    context.End();
                }
            }
            catch (Exception exc) {
                rVal = new[] { $"Exception outer in TestProcess {exc.Message}, {exc.InnerException?.Message}", };
            }
            return rVal;
        }


        public static string[] ImportProcess(string[] args, ref string retContent, string dataContent = null) {
            string[] rVal = { "initRval" };
            try {
                ProcessingArgs pa = new ProcessingArgs(args);
                DateTime dtStt = DateTime.Now;
                Console.WriteLine($"Starting at {dtStt}");
                if (pa["sqlMeterMgtCxnStg"] == null) {
                    pa["sqlMeterMgtCxnStg"] = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
                }
                ContextData.InitContextStrings("evc", pa["aclenv"], "none", pa, null);
                ProcessingArgs.AccumulateRequestPars("evc", pa["aclenv"], "none", pa);
                if (dataContent != null) {
                    pa["dataContent"] = dataContent;
                }
                pa["retContent"] = retContent;
                ContextData context = new ContextData(pa);
                bool excepted = false;
                try {
                    DateTime dt = DateTime.Now;
                    context.ContextWrite("ImportProcess.ProcessOptions", "Begin");
                    ProcessOptions(pa, context, dtStt);
                    context.ContextWrite("ImportProcess.ProcessOptions", "End", dt);
                    if (retContent.Equals("retContent")) {
                        retContent = pa["retContent"];
                    }
                }
                catch (Exception exc) {
                    excepted = true;
                    rVal = new[] { $"Exception in ImportProcess {exc.Message}, {exc.InnerException?.Message}", };
                }
                finally {
                    if (!excepted) {
                        Console.WriteLine(context.Snag());
                        rVal = new[] {
                            context.ContexFileNamePath, context.Validation.FileName, context.AutoFixes.FileName, context.NoFixes.FileName,
                            context.Snag()
                        };
                    }
                    context.End();
                }
            }
            catch (Exception exc) {
                rVal = new[] { $"Exception outer in ImportProcess {exc.Message}, {exc.InnerException?.Message}", };
            }
            return rVal;
        }

        public static string DeleteOlderLogs(ProcessingArgs pa, ContextData context = null ) {
            pa.GetControlVal("purgeShowOnly", "no");
            int sizeInMb = pa.GetControlVal("purgeSizeThreshold", 10000); // default to 10 GB
            int daysOld = pa.GetControlVal("purgeLogAgeDays", 14);

            DateTime oldTime = DateTime.Now.Subtract(TimeSpan.FromDays(daysOld));  // anything older then this is target for deletion
            string purgeDirectory = pa.GetControlVal("purgeDirectory", "getcurrentlog");
            if (purgeDirectory.Equals("getcurrentlog")) {
                if (context != null) {
                    purgeDirectory = context.ContexFileNamePath;
                    pa["purgeDirectory"] = purgeDirectory;
                }
                else {
                    pa["purgeDirectory"] = "NoConfiguredDirectoryForDefaultLogPath";
                }
            }
            if (!string.IsNullOrEmpty(purgeDirectory)) {
                FileAttributes attr = File.GetAttributes(purgeDirectory);
                //detect whether its a directory or file
                if ((attr & FileAttributes.Directory) == FileAttributes.Directory) {
                }
                else {
                    purgeDirectory = Path.GetDirectoryName(purgeDirectory);
                }
            }

            StringBuilder sb = new StringBuilder();
            long purgedLength = 0;
            long purgedCount = 0;
            long leftLength = 0;
            long leftCount = 0;
            string modOp = pa.GetControlVal("justshow", "no");
            const int maxDisplayLeft = 200;
            if (Directory.Exists(purgeDirectory)) {
                IEnumerable<string> fnames = Directory.EnumerateFiles(purgeDirectory);
                try {
                    foreach (string fname in fnames) {
                        FileInfo fi = new FileInfo(fname);
                        if ((fi.CreationTime < oldTime) || ((fi.Length/1000000)> sizeInMb)) {
                            purgedLength += fi.Length;
                            purgedCount++;
                            sb.AppendLine($" #1 SB DeleteOlderLogs {modOp} delete log file {fname},{fi.CreationTime},{fi.Length}");
                            if (!modOp.Equals("justshow")) File.Delete(fname);
                        }
                        else {
                            leftLength += fi.Length;
                            leftCount++;
                            if (leftCount < maxDisplayLeft) {
                                sb.AppendLine($" #1 SB DeleteOlderLogs {modOp} leave log file {fname},{fi.CreationTime},{fi.Length}");
                            }
                            else if (leftCount == maxDisplayLeft) {
                                sb.AppendLine($" #1 SB DeleteOlderLogs stopping recording");
                            }
                        }
                    }
                }
                catch (Exception exc) {
                    string msg = $" SB DeleteOlderLogs, exception {exc.Message}, {exc.InnerException?.Message}";
                    sb.AppendLine(msg);
                    Debug.WriteLine(msg);
                }
            }
            else {
                sb.AppendLine($"SB DeleteOlderLogs nodirectory {purgeDirectory}");
            }
            return
                $"(purged={purgedCount} items for total length {(double)purgedLength / (1024 * 1024)}MB, left={leftCount} items for total length {leftLength / (1024 * 1024)}MB)\n" +
                sb;
        }

        /// <summary>
        /// process the operation indicated in the request.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="context"></param>
        /// <param name="dtStt"></param>
        public static string ProcessOptions(ProcessingArgs pa, ContextData context, DateTime dtStt) {
            string rVal = "";
            DateTime startTime = DateTime.Now;
            context.ContextWrite("ProcessOptions", "Begin");
            if (pa.GetControlVal("disablelog", "no").Equals("yes")) {
                context.ContextWrite("ProcessOptions", "disable logging requested", dtStt);
                AnalysisOutputFileStream.LoggingDisabled = true;
            }
            if (pa.GetControlVal("restRoot", "nada").Equals("api") && pa.GetControlVal("restVersion", "nada").Equals("current")) {
                // using the new restful URI format
                string resourceName = pa.GetControlVal("restResource", "nada");
                string resourceVerb = pa.GetControlVal("restVerb", "nada").ToLower();
                string resourceId = pa.GetControlVal("restResId", "nada");
                switch (resourceName) {
                    case "ami": {
                            pa["collid"] = resourceId;
                        }
                        break;
                }
                switch (resourceVerb) {
                    case "get": {
                            switch (resourceName) {
                                case "ami": {
                                        pa["oper"] = "recallseries";
                                    }
                                    break;
                                case "meter": {
                                        pa["oper"] = "seemeters";
                                    }
                                    break;
                                case "statistics": {
                                        pa["oper"] = "seestats";
                                    }
                                    break;
                                case "config": {
                                        pa["oper"] = "getconfig";
                                    }
                                    break;
                            }
                        }
                        break;
                    case "put":
                    case "post": {
                            switch (resourceName) {
                                case "ami": {
                                        pa["oper"] = "recallseries";  // tentative as we do not currently allow post
                                    }
                                    break;
                                case "config": {
                                        pa["oper"] = "setconfig";
                                    }
                                    break;
                            }
                            if (pa.GetControlVal("bodyOverride", "nope").Equals("yes")) {  // initialize params from the body payload.
                                if (pa["reqContent"] != null) {
                                    string varStg;
                                    if (pa["reqContent"].Length > 8192) {
                                        varStg = pa["reqContent"].Substring(0, 8192);
                                        pa.Override(varStg);
                                    }
                                    else {
                                        pa.Override(pa["reqContent"]);
                                    }
                                }
                            }
                        }
                        break;
                }

            }
            string todo = pa["oper"] ?? "nada";
            pa["retdatapa"] = pa["retdatapa"] ?? "yes";
            pa["aclenv"] = pa["aclenv"] ?? ConfigurationManager.AppSettings["AMiCassandraKey"] ?? "work";
            try {
                switch (todo) {
                    case "recallseries": {
                            try {

                                string timezone = pa.GetControlVal("timezone", "EST");
                                bool adjustTimezone = Convert.ToBoolean(pa.GetControlVal("adjusttimezone", "false"));
                                SeriesCollectionSpecification collSpec = new SeriesCollectionSpecification(pa);
                                collSpec.InitializeCache(pa, pa.GetControlVal("resetcache", "no").Equals("yes"), context);
                                SeriesSpecification seriesSpec = new SeriesSpecification(pa, null as DataRow);
                                // if force the above should do it seriesSpec.InitializeCache(pa, pa.GetControlVal("resetcache", "no").Equals("yes"));
                                DataTable dt = collSpec.GetSeriesData(pa, context, pa["collid"], seriesSpec);

                                // convert time zone
                                if(adjustTimezone)
                                    ConvertTimeZone(dt, timezone);

                                int retRows = 0;
                                if (dt != null) {
                                    //int seriesBucketSizeSeconds = pa.GetControlVal("seriesBucketSizeSeconds", -1);
                                    AmiAccumulator amiAccum = new AmiAccumulator(pa);
                                    if (amiAccum.RequestBucketize(pa)) {
                                        dt = amiAccum.BucketizeAmiData(context, pa, dt);
                                    }
                                    else {
                                        bool doMeta = pa.GetControlVal("wantMetadata", -1) > 0;
                                        bool wantCassinfo = pa.GetControlVal("wantCassinfo", -1) > 0;
                                        if (!wantCassinfo) {
                                            dt.Columns.Remove(AmiAccumulator.TsColId);
                                            dt.Columns.Remove(AmiAccumulator.TsColDid);
                                        }
                                        if (!doMeta) {
                                            dt.Columns.Remove(AmiAccumulator.TsColMeta);
                                        }
                                        if (pa.GetControlVal("seriesSort", "asc").Equals("asc")) {
                                            AmiAccumulator.MakeAsc(ref dt);
                                        }
                                    }
                                    string dataOut = DataUtils1.FormatResultsData(pa, dt);
                                    context.NoFixes.AddLine(dataOut);
                                    if (pa.GetControlVal("retdatapa", "yes").Equals("yes")) {
                                        pa["retdatatxt"] = dataOut;
                                    }
                                    retRows = dt.Rows.Count;
                                }
                                if (retRows > 0) {
                                    context.ContextWrite("tseries returned data", $" rows={retRows}");
                                    context.Add2Counter("retrows", retRows);
                                }
                                else {
                                    context.ContextWrite("warn_tseries", " no returned data");
                                }
                            }
                            catch (Exception exc) {
                                pa["retdatapa"] = pa["retdatapa"] ?? "yes";
                                context.ContextWrite("error_tseries", $" exception {exc.Message},{exc.InnerException?.Message} ");
                            }
                        }
                        break;
                    case "tseries": {
                            SeriesSpecification.Initialize(pa, context);
                            //samples:
                            //oper=genmeters nummeters=10 numseries=3 
                            DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "seestats", "start");
                            SeriesCollectionSpecification collSpec = new SeriesCollectionSpecification(pa);
                            collSpec.InitializeCache(pa, false, context);
                            SeriesSpecification seriesSpec = new SeriesSpecification(pa, null as DataRow);
                            seriesSpec.InitializeCache(pa);
                            stest = TimingHelper.RunningTimes(context, dtStt, stest, "tseries", "initcache");
                            string colidstg = pa["collid"];
                            string[] colids = new[] { colidstg };
                            if (colidstg.IndexOf(",", StringComparison.InvariantCultureIgnoreCase) > 0) {
                                colids = colidstg.Split(',');
                            }
                            else {
                                // maybe another real recall test
                                if (colidstg != "collectionStarterId") {
                                    try {
                                        DataTable dt = collSpec.GetSeriesData(pa, context, colidstg, seriesSpec);
                                        int retRows = dt.Rows.Count;
                                        if (retRows > 0) {
                                            context.ContextWrite("tseries returned data", $" rows={retRows}");
                                        }
                                        else {
                                            context.ContextWrite("error_tseries", " no returned data");
                                        }
                                    }
                                    catch (Exception exc) {
                                        context.ContextWrite("error_tseries", $" exception {exc.Message},{exc.InnerException?.Message} ");
                                    }
                                }
                            }
                            foreach (string colid in colids) {
                                string res = collSpec.TestGetData(pa, colid);
                                stest = TimingHelper.RunningTimes(context, dtStt, stest, "tseries", "getone");
                                Console.WriteLine(res);
                            }
                            foreach (string colid in colids) {
                                string id = collSpec.GetDataSeriesKey(pa, colid);
                                Console.WriteLine(id);
                                string res = seriesSpec.GetData(pa, id);
                                stest = TimingHelper.RunningTimes(context, dtStt, stest, "tseries", "getseriesspec");
                                Console.WriteLine(res);
                            }
                            DateTime testStt = pa.GetControlVal("dtstt", DateTime.Parse("1/1/2017"));
                            DateTime testStp = pa.GetControlVal("dtstp", DateTime.Parse("1/2/2017"));
                            List<string> qryStgs = new List<string>();
                            foreach (string colid in colids) {
                                string id = collSpec.GetDataSeriesKey(pa, colid);
                                Console.WriteLine(id);
                                List<string> queryStgs = seriesSpec.GetQuery(pa, id, testStt, testStp);
                                foreach (string queryStg in queryStgs) {
                                    qryStgs.Add(queryStg);
                                }
                                stest = TimingHelper.RunningTimes(context, dtStt, stest, "tseries", "getseriesspec");
                            }
                            stest = TimingHelper.RunningTimes(context, dtStt, stest, "tseries", "end");
                            if (qryStgs.Count > 0) {
                                //int retRowsCnt = 0;
                                SeriesStorePersistenceInfo cc1 = SeriesStorePersistenceInfo.GetCacheCxn(pa);
                                //cc1.Connect(pa);
                                stest = TimingHelper.RunningTimes(context, dtStt, stest, "tseries", "connected");
                                BatchExecutor batch = new BatchExecutor();
                                string res = batch.ExecuteGet(pa, context, qryStgs, cc1);
                                stest = TimingHelper.RunningTimes(context, dtStt, stest, "query 3", $"end, res={res}");
                                DataTable dt = batch.ExecuteGetTable(pa, context, qryStgs, cc1);
                                TimingHelper.RunningTimes(context, dtStt, stest, "query 1", $"end table, rows={dt.Rows.Count}");

                                // cassandra map doesn't come into XML cleanly
                                //TODO: http://theburningmonk.com/2010/05/net-tips-xml-serialize-or-deserialize-dictionary-in-csharp/

                                string jsonString = JsonConvert.SerializeObject(dt);
                                context.NoFixes.AddLine(jsonString);
                            }
                        }
                        break;
                    case "purgelogweek": {
                            pa["purgeLogAgeDays"] = pa.GetControlVal("purgeLogAgeDays", "8");
                            PurgeLogOperation(pa, context);
                        }
                        break;

                    case "purgelogday": {
                            pa["purgeLogAgeDays"] = pa.GetControlVal("purgeLogAgeDays", "2");
                            PurgeLogOperation(pa, context);
                        }
                        break;

                    case "time":
                        SomeTests.TimeTests();
                        break;
                    case "see_events":
                        EventsProcessing(pa);
                        break;

                    case "avpreproc": {
                            //samples:
                            AvistaPreProc(context, dtStt, pa);
                            rVal = pa["retContent"];
                        }
                        break;

                    case "avfullprocess": {
                            StringBuilder sb = new StringBuilder();
                            StreamReader referenceStream = new StreamReader(pa.GetControlVal("importFile", null));
                            while (!referenceStream.EndOfStream) {
                                string inpd = referenceStream.ReadLine();
                                if (inpd == null)
                                    throw new Exception("input file stream is null");
                                if (!string.IsNullOrEmpty(inpd.Trim())) {
                                    sb.Append(inpd + "\n");
                                }
                            }
                            string dataBody = sb.ToString().Trim();
                            Debug.WriteLine($" 8.0  AmiAdvancedConnector.ImportAmiDataValidate : for\n{dataBody}");
                            string cvtData = ConvertCassandraDataTestl(dataBody, pa);
                            Debug.WriteLine($" 8.0.1  AmiAdvancedConnector.ImportAmiDataValidate : datlen = {cvtData.Length}  ");
                            InsertCassandraDataTestl(cvtData, pa);
                            Debug.WriteLine($" 8.0.1  AmiAdvancedConnector.ImportAmiDataValidate : ");
                        }
                        break;

                    case "seemeters": {
                            //samples:
                            //oper=seemeters quantity=100
                            DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "seemeters", "start");
                            DataTable dt = new SeriesCollectionSpecification(pa).GetSome(pa);
                            TimingHelper.RunningTimes(context, dtStt, stest, "seemeters", "end");
                            StringWriter sw = new StringWriter();
                            dt.WriteXml(sw, true);
                            string retMedia = pa.GetControlVal("Accept", "text/plain").ToLower();
                            if (pa.GetControlVal("retdatapa", "yes").Equals("yes")) {
                                switch (retMedia) {
                                    case "application/xml": {
                                            pa["retdatatxt"] = sw.ToString();
                                        }
                                        break;
                                    case "application/json": {
                                            XmlDocument doc = new XmlDocument();
                                            doc.LoadXml(sw.ToString());
                                            pa["retdatatxt"] = JsonConvert.SerializeObject(doc);
                                            //Console.WriteLine(pa["retdatatxt"]);
                                        }
                                        break;
                                    default: {
                                            pa["retdatatxt"] = sw.ToString();
                                        }
                                        break;
                                }
                            }
                        }
                        break;
                    case "getconfig": {
                            if (pa.GetControlVal("custom", "nope").Equals("yes")) {
                                string result = pa.GetCustomConfigFile();
                                pa["retdatatxt"] = result;
                                pa["Accept"] = "application/xml";
                            }
                            else {
                                string retMedia = pa.GetControlVal("Accept", "text/plain").ToLower();
                                switch (retMedia) {
                                    case "application/xml": {
                                            XmlDocument doc = JsonConvert.DeserializeXmlNode(pa.AsJson(), "config");
                                            pa["retdatatxt"] = doc.OuterXml;
                                        }
                                        break;
                                    //case "application/json":
                                    default: {
                                            pa["retdatatxt"] = pa.AsJson();
                                        }
                                        break;
                                }
                            }
                        }
                        break;
                    case "setconfig": {
                            string setResult = "noSetResult";
                            if (pa["reqContent"] != null) {
                                try {
                                    XmlDocument doc = new XmlDocument();
                                    doc.LoadXml(pa["reqContent"]);  // check content structure
                                    setResult = pa.SetCustomConfig(doc);
                                }
                                catch (Exception exc) {
                                    setResult = $"setconfig exception : {exc.Message}, {exc.InnerException?.Message}";
                                }
                            }
                            pa["retdatatxt"] = setResult;
                            pa["Accept"] = "text/plain";
                        }
                        break;
                    case "seeseries": {
                            //samples:
                            //oper=seeseries quantity=100
                            DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "seeseries", "start");
                            DataTable dt = new SeriesSpecification(pa).GetSome(pa);
                            TimingHelper.RunningTimes(context, dtStt, stest, "seeseries", "end");
                            StringWriter sw = new StringWriter();
                            dt.WriteXml(sw, true);
                            if (pa.GetControlVal("usejson", "no") == "yes") {
                                XmlDocument doc = new XmlDocument();
                                doc.LoadXml(sw.ToString());
                                Console.WriteLine(JsonConvert.SerializeObject(doc));
                            }
                            else {
                                Console.WriteLine(sw.ToString());
                            }
                        }
                        break;

                    case "seestats": {
                            //samples:
                            //oper=seestats 
                            DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "seestats", "start");
                            SeriesCollectionSpecification col = new SeriesCollectionSpecification(pa);
                            string cinfo = col.Statistics(pa);
                            SeriesSpecification ser = new SeriesSpecification(pa);
                            string sinfo = ser.Statistics(pa);
                            TimingHelper.RunningTimes(context, dtStt, stest, "seestats", "end");
                            Console.WriteLine($" colls:{cinfo}, series:{sinfo}");
                        }
                        break;

                    case "genseries": {
                            //samples:
                            //oper=genseries nummeters=10 numseries=3 
                            SeriesSpecification spec = new SeriesSpecification(pa);
                            spec.Generate(pa);
                        }
                        break;
                    case "genmeters": {
                            SeriesSpecification.Initialize(pa, context);
                            //samples:
                            //oper=genmeters nummeters=10 numseries=3 
                            SeriesCollectionSpecification spec = new SeriesCollectionSpecification(pa);
                            spec.Generate(pa);
                        }
                        break;
                    case "genmetersfrombilling": {
                            string cfgMeterCsvCols = ConfigurationManager.AppSettings["AmiDistinctMeterImportCols"];
                            if (string.IsNullOrEmpty(cfgMeterCsvCols)) {
                                cfgMeterCsvCols = "meter=0,spoint=0,client=-1,cust=-1,acct=-1";
                            }
                            pa["namecolidx"] = cfgMeterCsvCols;
                            SeriesSpecification.Initialize(pa, context);
                            //samples:
                            //oper=genmeters nummeters=10 numseries=3 
                            DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "seestats", "start");
                            SeriesCollectionSpecification spec = new SeriesCollectionSpecification(pa);
                            spec.InitializeCache(pa, pa.GetControlVal("resetcache", "no").Equals("yes"), context);
                            spec.GenerateFromCsvData(pa, context, spec);
                            spec.InitializeCache(pa, true, context);
                            TimingHelper.RunningTimes(context, dtStt, stest, "seestats", "end");
                        }
                        break;
                    case "genmetersfromami": {
                            pa["namecolidx"] = "client=0,cust=-1,meter=1,spoint=3,acct=2";
                            SeriesSpecification.Initialize(pa, context);
                            //samples:
                            //oper=genmeters nummeters=10 numseries=3 
                            DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "seestats", "start");
                            SeriesCollectionSpecification spec = new SeriesCollectionSpecification(pa);
                            spec.InitializeCache(pa, pa.GetControlVal("resetcache", "no").Equals("yes"), context);
                            spec.GenerateFromCsvData(pa, context, spec);
                            spec.InitializeCache(pa, true, context);
                            TimingHelper.RunningTimes(context, dtStt, stest, "seestats", "end");
                        }
                        break;
                    case "genseriesdata": {
                            //samples:
                            //oper=genseriesdata minutes=60 numseries=10000 seriesstart=100 dtstt=2/1/2017  dtstp=2/7/2017 qualreps=1 gendata=10000
                            DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "genseriesdata", "start");
                            //SeriesSpecification spec = new SeriesSpecification(pa);
                            SeriesCollectionSpecification spec = new SeriesCollectionSpecification(pa);
                            spec.InitializeCache(pa, pa.GetControlVal("resetcache", "no").Equals("yes"), context);
                            spec.GenerateDataFromSeries(pa, context);
                            TimingHelper.RunningTimes(context, dtStt, stest, "genseriesdata", "end");
                        }
                        break;
                    case "cassimport": {
                            context.ContextWrite("cassimport", "begin");
                            // samples
                            // oper=cassimport procLimit=50 importfile=C:\junk\DevWt1\seriesvalues_168_5_100.csv
                            //  oper=cassimport minutes=60 numseries=10000 seriesstart=100 dtstt=2/1/2017  dtstp=2/2/2017 qualreps=1 gendata=100 procLimit=50
                            string nvpairsStg =
                                "procTableTag=value_series_meta procEnvTag=work isInsert=1 maxRows=30 procLimit=500000 allowFilter=0 sortCol=last_modified_date procResp=1 outputTag=test1 userPath=anon2 contextRecs=25 contextRet=1 expectRows=0 matchtype=get";
                            pa.AddIfMissing(nvpairsStg);
                            DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "cassimport", "start");
                            CassImportProcessing(context, pa);
                            TimingHelper.RunningTimes(context, dtStt, stest, "cassimport", "end");
                            context.ContextWrite("cassimport", "end", startTime);
                        }
                        break;
                    case "gseries":
                        GenTemplateSeriesData(pa);
                        break;
                    case "OldFilesProcessing":
                        FilesProcessingOld(pa);
                        break;
                }
            }
            catch (Exception exc) {
                if (pa.GetControlVal("disablelog", "no").Equals("yes")) {
                    AnalysisOutputFileStream.LoggingDisabled = false;
                    context.ContextWrite("ProcessOptions", "temp enable logging", dtStt);
                    Console.WriteLine($"ProcessOptions.{todo}, Exception {exc.Message}, inner={exc.InnerException?.Message}");
                    AnalysisOutputFileStream.LoggingDisabled = true;
                }
                else {
                    Console.WriteLine($"ProcessOptions.{todo}, Exception {exc.Message}, inner={exc.InnerException?.Message}");
                }
            }
            finally {
                DateTime dtStp = DateTime.Now;
                Console.WriteLine($"total time = {dtStp.Subtract(dtStt).TotalMilliseconds} ms at {DateTime.Now}");
                Console.WriteLine($"context data from {context.ContexFileNamePath}");
            }
            if (pa.GetControlVal("disablelog", "no").Equals("yes")) {
                AnalysisOutputFileStream.LoggingDisabled = false;
                context.ContextWrite("ProcessOptions", "enabling logging", dtStt);
            }
            context.ContextWrite("counters", context.Counters());
            context.ContextWrite("ProcessOptions", "End", startTime);
            return rVal;
        }


        public static void ConvertTimeZone(DataTable dt, string timezone)
        {
            var timezoneDesc = TimeZone.GetTimeZoneDescription(timezone);
            var timezoneId = TimeZoneInfo.FindSystemTimeZoneById(timezoneDesc);
            
            foreach (DataRow row in dt.Rows)
            {
                var timeStamp = (DateTimeOffset)row["ts"];
                var convertedTs = TimeZoneInfo.ConvertTime(timeStamp, timezoneId);

                row["ts"] = convertedTs;
                
            }
        }
        private static void PurgeLogOperation(ProcessingArgs pa, ContextData context) {
            //string justShow = pa.GetControlVal("purgeShowOnly", null);
            string purgeDirectory = pa.GetControlVal("purgeDirectory", null);
            pa["purgeLogAgeDays"] = pa.GetControlVal("purgeLogAgeDays", "14");

            context.ContextWrite("PurgeLogOperation", $" daysold={pa["purgeLogAgeDays"] }, purgeDirectory='{purgeDirectory}'");
            if (purgeDirectory.Equals("getcurrentlog")) {
                purgeDirectory = context.ContexFileNamePath;
                pa["purgeDirectory"] = purgeDirectory;
            }
            if (!string.IsNullOrEmpty(purgeDirectory)) {
                FileAttributes attr = File.GetAttributes(purgeDirectory);
                //detect whether its a directory or file
                if ((attr & FileAttributes.Directory) == FileAttributes.Directory) {
                }
                else {
                    purgeDirectory = Path.GetDirectoryName(purgeDirectory);
                }
                context.ContextWrite("PurgeLogOperation", $" actual purgeDirectory='{purgeDirectory}");
                string results = DeleteOlderLogs(pa, context );
                context.ContextWrite("PurgeLogOperation", results);
                Console.WriteLine(results);
                Debug.WriteLine(results);
            }
        }


        private static void GenTemplateSeriesData(ProcessingArgs pa) {
            int counter = 0;
            int procLimit = pa.GetControlVal("procLimit", 10);
            int rseed = pa.GetControlVal("rseed", 999);
            double minutes = pa.GetControlVal("minutes", 5);
            DateTime dtStt = pa.GetControlVal("dtstt", DateTime.Parse("1/1/2017"));
            DateTime dtStp = pa.GetControlVal("dtstp", DateTime.Parse("1/2/2017"));
            string spid = pa.GetControlVal("spid", "36023832");
            string outfile = pa.GetControlVal("outfile", ".\\seriesoutdata.csv");
            string listfile = pa.GetControlVal("listfile", ".\\CollidList.csv");
            Random rand = new Random(rseed);
            string CsvSeriesDataHeaderCols =
                "Service_Agreement_ID,MDM_SP_ID,Service_Point_ID,UOM,SQI,Seconds per interval,Measurement Value,DateTime,Estimate Description,Estimate Indicator";
            //string seriesRowTemplate = $"90290762,{0},90658614,KWH,,300,{1},2/23/2017,00.05.00,D2NO,No";
            string seriesRowTemplate = "90290762,{0},90658614,KWH,,300,{1},{2},D2NO,No";
            string outputRecord;
            using (StreamWriter sw = new StreamWriter(outfile)) {
                sw.WriteLine(CsvSeriesDataHeaderCols);
                using (StreamReader sr = new StreamReader(listfile))
                {
                    sr.ReadLine();
                    while (!sr.EndOfStream && (procLimit-- > 0)) {
                        var readLine = sr.ReadLine();
                        if (readLine != null) spid = readLine.Trim();
                        DateTime dtOper = dtStt;
                        while (dtOper.CompareTo(dtStp) < 0) {
                            var val = rand.Next(0, 1000) / 10.0;
                            string dateTime = dtOper.ToString("yyyy-MM-ddTHH:mm:ss.fffzz00");
                            outputRecord = string.Format(seriesRowTemplate, spid, val, dateTime);
                            if ((counter % 10000) == 0) Console.WriteLine($"{counter}, {outputRecord}");
                            sw.WriteLine(outputRecord);
                            dtOper = dtOper.AddMinutes(minutes);
                            counter++;
                        }
                    }
                }
            }


        }

        private static void InsertCassandraDataTestl(string dat, ProcessingArgs pa) {
            string clientId = pa.GetControlVal("clientId", "87");
            string procLimit = pa.GetControlVal("procLimit", "5000000");
            procLimit = pa.GetControlVal("insertLimit", procLimit);
            //string stg = $"oper=cassimport procLimit=5000000 importfile={tmpFile} doAsyncBatch=1 aclenv=work";
            Debug.WriteLine($" ins 1 AmiAdvancedConnector.InsertCassandraDataTest impContent length={dat?.Length}");
            string stg = $"oper=cassimport clientId={clientId} procLimit={procLimit} importLength={dat?.Length} doAsyncBatch=1 aclenv=work";
            string[] rVals = TestProcess(stg.Split(' '), dat);
            string sval = "";
            foreach (string rVal in rVals) {
                sval += rVal + "; ";
            }
            Debug.WriteLine($" ins 2 AmiAdvancedConnector.InsertCassandraDataTest impContent rval={sval}");
        }

        private static string ConvertCassandraDataTestl(string dat, ProcessingArgs pa) {
            string clientId = pa.GetControlVal("clientId", "87");
            string procLimit = pa.GetControlVal("procLimit", "5000000");
            procLimit = pa.GetControlVal("convertLimit", procLimit);
            //string stg = $"oper=cassimport procLimit=5000000 importfile={tmpFile} doAsyncBatch=1 aclenv=work";
            Debug.WriteLine($" cvt 1 AmiAdvancedConnector.ConvertCassandraDataTest impContent length={dat?.Length}");
            string stg = $"oper=avpreproc clientId={clientId} procLimit={procLimit} importLength={dat?.Length} doAsyncBatch=1 aclenv=work retContent=retContent";
            string retData = "retContent";
            string[] rVals = ImportProcess(stg.Split(' '), ref retData, dat);
            string sval = "";
            foreach (string rVal in rVals) {
                sval += rVal + "; ";
            }
            Debug.WriteLine($" cvt 2 AmiAdvancedConnector.ConvertCassandraDataTest impContent rval={sval}");
            return retData;
        }



        private static void AvistaPreProc(ContextData context, DateTime dtStt, ProcessingArgs pa) {
            //oper=avpreproc importfile=C:/_Test/Avista/Avista5minSample.csv
            DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "avpreproc", "start");
            SeriesCollectionSpecification collSpec = new SeriesCollectionSpecification(pa);
            TimingHelper.RunningTimes(context, dtStt, stest, "avpreproc", "initialize");
            collSpec.Transform2SeriesImport(pa, context);
            TimingHelper.RunningTimes(context, dtStt, stest, "avpreproc", "end");
        }


        //private static string UniqueNameFrag() {
        //    return DateTime.Now.ToString("MMddyyyy-HHmmss.ffff");
        //}

        private static string importFileName = "C:\\junk\\DevWt1\\seriesvalues_144_5_10000_0201.csv";
        private static string cassImportQuery = "(id, did, ts, val, meta)   values({0},{1},'{2}',{3},{5})";


        static void CassImportProcessing(ContextData contextData, ProcessingArgs pa) {
            contextData.ContextWrite("CassImportProcessing", "begin");

            string procTableTag = pa["procTableTag"];
            string procEnvTag = pa["procEnvTag"];
            string procAltTag = "5";
            pa["reqVanal"] = "batch";
            pa["qSel"] = pa.GetControlVal("importquery", cassImportQuery);
            pa["qSel0"] = pa["qSel"];
            pa["cassquery"] = pa.GetControlVal("importquery", cassImportQuery);
            pa["fiOper0"] = pa.GetControlVal("importfile", importFileName);

            ProcessingArgs.AccumulateRequestPars(procTableTag, procEnvTag, procAltTag, pa);

            BatchUtils.AnalysisBatch(contextData, pa, null, null);
            contextData.ContextWrite("CassImportProcessing", "end");
        }



        private static void FilesProcessingOld(ProcessingArgs pa) {
            int counter = 1;
            double val;
            string dataPath = pa["outputPath"];
            Random rand = new Random(100);
            string headerCols = "id,ds,ts,val,qual,meta";


            double minutes;
            if (!Double.TryParse(pa.GetControlVal("minutes", 5).ToString(), out minutes))
            {
                throw  new Exception("Unable to convert control value minutes to double");
            }
            int qualControl = pa.GetControlVal("qualcontrol", -1);
            int numSeries = pa.GetControlVal("numseries", 1000);
            int seriesStart = pa.GetControlVal("seriesstart", 1);
            int qualReps = pa.GetControlVal("qualreps", 1);

            DateTime dtStt = pa.GetControlVal("dtstt", DateTime.Parse("1/1/2017"));
            DateTime dtStp = pa.GetControlVal("dtstp", DateTime.Parse("1/2/2017"));

            double hours = dtStp.Subtract(dtStt).TotalHours;

            DateTime dtOper = dtStt;
            string meta = "(*( 'm1':'{0}' (*)'m2':'{1}' )*) ";
            string fileName = $"seriesvalues_{hours}_{minutes}_{numSeries}_{dtStt:yyyyMMdd}_{dtStp:yyyyMMdd}.csv";
            string outputFile = Path.Combine(dataPath, fileName);
            using (StreamWriter sw = new StreamWriter(outputFile)) {
                sw.WriteLine(headerCols);
                for (int seriesId = seriesStart; seriesId < (seriesStart + numSeries); seriesId++) {
                    dtOper = dtStt;
                    while (dtOper.CompareTo(dtStp) < 0) {
                        val = rand.Next(0, 1000) / 10.0;
                        string tstg = dtOper.ToString(ContextData.StandardTimeFormat);
                        for (int qr = 1; qr <= qualReps; qr++) {
                            string metaVal = string.Format(meta, seriesId, qr);
                            int qualifier = counter;
                            if (qualControl >= 0) {
                                qualifier *= qr;
                            }
                            string outputRecord = $"{seriesId},{tstg},{tstg},{val},{qualifier},{metaVal}";
                            if ((counter % 1000) == 0) Console.WriteLine(outputRecord);
                            sw.WriteLine(outputRecord);
                        }
                        dtOper = dtOper.AddMinutes(minutes);
                        counter++;
                    }
                }
            }

            Console.WriteLine($" count={counter} last={dtOper.ToString(CultureInfo.InvariantCulture)}");
        }


        private static void EventsProcessing(ProcessingArgs pa) {
            string env = pa["env"] ?? "work";
            SimpleEventProcessor.Init(pa, env);
            Console.CancelKeyPress += myHandler;
            if (pa["infoTimeSecs"] != null) int.TryParse(pa["infoTimeSecs"], out _infoTimeSecs);
            if (pa["infoTimeMins"] != null) int.TryParse(pa["infoTimeMins"], out _infoTimeMins);


            string eventHubConnectionString = pa[env + ".hubCxn"];
            string eventHubName = "aclaoimportdataeh" + env;
            string storageConnectionString = pa[env + ".storeCxn"];
            //string storageAccountName = "aclaoaclaraonestg" + env;
            //string storageAccountKey = pa[env + ".storeAcctKey"];
            //string storageConnectionString = $"DefaultEndpointsProtocol=https;AccountName={storageAccountName};AccountKey={storageAccountKey}";

            //SimpleEventProcessor.LogStg2($"env={env}, eventHubName={eventHubName}, storageAccountName={storageAccountName} ");
            SimpleEventProcessor.LogStg2($"env={env}, eventHubName={eventHubName} ");

            string eventProcessorHostName = Guid.NewGuid().ToString();
            EventProcessorHost eventProcessorHost = new EventProcessorHost(eventProcessorHostName, eventHubName,
                EventHubConsumerGroup.DefaultGroupName, eventHubConnectionString, storageConnectionString);
            SimpleEventProcessor.LogStg2("Registering EventProcessor...");
            var options = new EventProcessorOptions();
            options.ExceptionReceived += (sender, e) => { Console.WriteLine(e.Exception); };
            eventProcessorHost.RegisterEventProcessorAsync<SimpleEventProcessor>(options).Wait();

            SimpleEventProcessor.LogStg2("Receiving. cntl-break to end");
            DateTime beginTime = DateTime.Now;
            DateTime nextInfoTime = DateTime.Now.AddSeconds(_infoTimeSecs);
            DateTime nextLogTime = DateTime.Now.AddMinutes(_infoTimeMins);
            while (!_done) {
                Thread.Sleep(1000);
                if (DateTime.Now > nextInfoTime) {
                    nextInfoTime = DateTime.Now.AddSeconds(_infoTimeSecs);
                    SimpleEventProcessor.ShowInfo(
                        $"EventsConsole.running env={env}, for secs={DateTime.Now.Subtract(beginTime).TotalSeconds}");
                }
                if (DateTime.Now > nextLogTime) {
                    nextLogTime = DateTime.Now.AddMinutes(_infoTimeMins);
                    SimpleEventProcessor.ShowLog();
                }
            }
            SimpleEventProcessor.LogStg2("\n Shutting down... \n");
            SimpleEventProcessor.ShowLog();
            eventProcessorHost.UnregisterEventProcessorAsync().Wait();
            SimpleEventProcessor.ShowLog();
            SimpleEventProcessor.FlushData();
            Thread.Sleep(2000);
            SimpleEventProcessor.FlushData();
        }


        protected static void myHandler(object sender, ConsoleCancelEventArgs args) {
            Console.WriteLine("  Key pressed: {0}", args.SpecialKey);
            if (args.SpecialKey == ConsoleSpecialKey.ControlBreak) {
                _done = true;
                args.Cancel = true;
            }
            else {
                Console.WriteLine("press cntl-break to quit");
                SimpleEventProcessor.ShowLog();
                args.Cancel = true;
            }
        }


        //private static void MeterGenerator(ProcessingArgs pa, StreamWriter sw, DataRow dataRow) {

        //}

        //private static void SeriesGenerator(ProcessingArgs pa, StreamWriter sw, DataRow dataRow) {

        //}


    }


    internal class SimpleEventProcessor : IEventProcessor {

        public static string Mode { get; set; }
        private static string _partitionOperationsLog = "";
        private static int _partitionsOpenedCount;
        private static int _partitionsClosedCount;
        private static StreamWriter _logStream;

        private static int _consFull = 100000;
        private static int _consInfo = 10000;
        private static bool _doCheckpoint;

        // watch it --= assumes 32
        private static readonly int[] PartMessages = {
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0
        };

        private static string _environ = "na";

        /// <summary>
        /// DO THIS BEFORE USE -- static initialization of logging, etc.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="env"></param>
        public static void Init(ProcessingArgs pa, string env) {
            _environ = env;
            if (pa["consFull"] != null) int.TryParse(pa["consFull"], out _consFull);
            if (pa["consInfo"] != null) int.TryParse(pa["consInfo"], out _consInfo);
            if (pa["doCheckpoint"] != null) _doCheckpoint = pa["doCheckpoint"].Equals("1");

            string fName =
                $"evConsLog_{DateTime.Now.Month}_{DateTime.Now.Day}_{DateTime.Now.Hour}_{DateTime.Now.Minute}_{DateTime.Now.Second}_{_environ}.txt";
            _logStream = new StreamWriter(fName);
            LogStg2($"SimpleEventProcessor, now={DateTime.Now}, name={fName}");
        }

        private static void LogStg(string stg) {
            lock (_logStream) {
                _logStream.WriteLine(stg);
            }
        }

        public static void LogStg2(string stg) {
            LogStg(stg);
            Console.WriteLine(stg);
        }

        public static void FlushData() {
            lock (_logStream) {
                _logStream.Flush();
            }
        }

        public static void ShowInfo(string info) {
            LogStg2($"at '{info}', total messages ={_totalMsgsCounter}  total rx Size = {_totalMsgsSize}");

        }


        public static void ShowLog() {
            LogStg2(
                $"\npartitions opened={_partitionsOpenedCount} closed={_partitionsClosedCount},  checkpoint={_doCheckpoint} now={DateTime.Now}");
            LogStg2("\n partitionsLog: \n" + _partitionOperationsLog);
            for (int i = 0; i < PartMessages.Length; i++) {
                LogStg2($"partitions[{i}]={PartMessages[i]}");
            }
            LogStg2($"total messages ={_totalMsgsCounter}");
            FlushData();
        }

        Stopwatch _checkpointStopWatch;

        async Task IEventProcessor.CloseAsync(PartitionContext context, CloseReason reason) {
            string stg =
                $"Processor Shutting Down. Partition: '{context.Lease.PartitionId}', Reason: '{reason}'', time={DateTime.Now}  checkpoint={_doCheckpoint}";
            _partitionOperationsLog += stg + "\n";
            LogStg2(stg);
            _partitionsClosedCount++;
            if (reason == CloseReason.Shutdown) {
                if (_doCheckpoint) {
                    await context.CheckpointAsync();
                }
            }
        }

        Task IEventProcessor.OpenAsync(PartitionContext context) {
            string stg =
                $"SimpleEventProcessor initialized.  Partition: '{context.Lease.PartitionId}', Offset: '{context.Lease.Offset}',  checkpoint={_doCheckpoint} time={DateTime.Now}";
            _partitionOperationsLog += stg + "\n";
            LogStg2(stg);
            _partitionsOpenedCount++;
            _checkpointStopWatch = new Stopwatch();
            _checkpointStopWatch.Start();
            return Task.FromResult<object>(null);
        }

        static int _totalMsgsCounter;
        static long _totalMsgsSize;

        async Task IEventProcessor.ProcessEventsAsync(PartitionContext context, IEnumerable<EventData> messages) {
            foreach (EventData eventData in messages) {
                PartMessages[int.Parse(context.Lease.PartitionId)] += 1;
                _totalMsgsSize += eventData.SerializedSizeInBytes;
                LogStg(
                    $"Message received. Count {_totalMsgsCounter} totalsize {_totalMsgsSize} Partition: '{context.Lease.PartitionId}', Data: '{Encoding.UTF8.GetString(eventData.GetBytes())}'");
                if (_totalMsgsCounter % _consFull == 0) {
                    string data = Encoding.UTF8.GetString(eventData.GetBytes());
                    Console.WriteLine(
                        $"Message received. Count {_totalMsgsCounter}  Partition: '{context.Lease.PartitionId}', Data: '{data}'");
                }
                else {
                    if (_totalMsgsCounter % _consInfo == 0) {
                        Console.WriteLine($"[{_totalMsgsCounter},{context.Lease.PartitionId},{eventData.SerializedSizeInBytes}]");
                    }
                }
                _totalMsgsCounter++;
            }

            //Call checkpoint every 5 minutes, so that worker can resume processing from 5 minutes back if it restarts.
            if (_checkpointStopWatch.Elapsed > TimeSpan.FromMinutes(5)) {
                string stg =
                    $"SimpleEventProcessor CheckpointAsync.  Partition: '{context.Lease.PartitionId}', time={DateTime.Now} checkpoint={_doCheckpoint}";
                _partitionOperationsLog += stg + "\n";
                LogStg2(stg);
                if (_doCheckpoint) {
                    await context.CheckpointAsync();
                }
                _checkpointStopWatch.Restart();
            }
        }
    }

    public delegate void DLogMessage(string msg);

    public abstract class SimpleWebServer {
        private static DLogMessage _logFunction;
        public virtual string WhoamiStg() {
            return "[I am no SimpleWebServer]";
        }

        private static void LocalLogMsg(String msg) {
            _logFunction?.Invoke(msg);
        }

        public virtual void Logit(string msg) {
            Console.WriteLine(msg);
        }

        public SimpleWebServer(ProcessingArgs pa) {
            Begin(pa);
        }

        public void Begin(ProcessingArgs pa) {
            //CheckEndpoints(pa);
        }

        public void SetLogFn(DLogMessage logger) {
            _logFunction = logger;
        }

        /// <summary>
        /// Spin up the web endpoint
        /// </summary>
        /// <param name="pa"></param>
        public string InitializeServer(ProcessingArgs pa) {
            string hostIp = "127.0.0.1";
            string port = "4443";
            string prefixesUrl = "nada";
            hostIp = pa.GetControlVal("hostIp", hostIp);
            port = pa.GetControlVal("port", port);
            if (!HttpListener.IsSupported) {
                Logit(
                    $"Event Process Worker NOT SUPPORTED a SimpleWebServer.InitializeServer NOT SUPPORTED for IP:{hostIp}, Port:{port}");
            }
            else {
                Logit($"Event Process Worker SimpleWebServer.InitializeServer starting at public IP:{hostIp}, Port:{port}");
                prefixesUrl = $"https://{hostIp}:{port}/";
                Logit($"Event Process Worker SimpleWebServer.InitializeServer encoding prefix {prefixesUrl}");
                string overrideAmiPrefix = pa.GetControlVal("overrideAMIPrefix", "");
                if (!string.IsNullOrEmpty(overrideAmiPrefix)) {
                    Logit($"Event Process Worker SimpleWebServer.InitializeServer override prefix with {overrideAmiPrefix}");
                    prefixesUrl = overrideAmiPrefix;
                }
                Console.WriteLine($"Event Process Worker SimpleWebServer.InitializeServer adding prefix {prefixesUrl}", null);
                _listener = new HttpListener();
                _listener.Prefixes.Add(prefixesUrl);
                _responderMethod = SendResponse;
                _listener.Start();
                StartWebServer();
                Logit($"Event Process Worker SimpleWebServer.InitializeServer started");

                //ws.Stop();
            }
            return prefixesUrl;
        }


        
        private HttpListener _listener;
        private Func<HttpListenerRequest, string[]> _responderMethod;

        Thread _webServerThread;
        private string myTestId = "weblisten";

        public void StartWebServer() {
            _webServerThread = new Thread(ThreadRoutine);
            _webServerThread.Name = "TestThread:" + myTestId;
            _webServerThread.IsBackground = true;
            _webServerThread.Start();
        }


        public void ThreadRoutine() {
            try {
                Run();
            }
            catch (ThreadAbortException exc) {
                int dbgBreak = 1;
                if (dbgBreak > 1) {
                    Console.WriteLine(exc.Message);
                }
            }
            catch (Exception exc) {
                Debug.WriteLine($"TestThread ThreadRoutine exception {exc.Message}, {exc.InnerException?.Message},\n{exc.StackTrace}");
            }
        }

        //pa["reqVsrc"] = vsrc;
        //    pa["reqVenv"] = venv;
        //    pa["reqVval"] = vval;

        public static string [] SendResponse(HttpListenerRequest request) {
            //string respBody = "nada";
            string reqBody = "nothing=nada";
            ProcessingArgs pa = new ProcessingArgs();
            //pa["requireLogForce"] = "yes";  // start off assuming we don't want logging unless forced (better performance this way)
            string[] respArr = new string[2];
            respArr[0] = "nothing=nada";
            respArr[1] = "text/csv";
            try {
                string baseUrl = request.RawUrl;
                if (baseUrl.IndexOf('?') >= 0) {
                    baseUrl = baseUrl.Substring(0, baseUrl.IndexOf('?'));
                }
                string[] urlParts = baseUrl.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

                pa["restVerb"] = request.HttpMethod;
                pa["restContType"] = request.ContentType;
                for (int i = 0; i < urlParts.Length; i++) {
                    switch (i) {
                        case 0:
                            pa["reqVsrc"] = urlParts[i];
                            pa["restRoot"] = urlParts[i];
                            break;
                        case 1:
                            pa["reqVenv"] = urlParts[i];
                            pa["restVersion"] = urlParts[i];
                            break;
                        case 2:
                            pa["reqVval"] = urlParts[i];
                            pa["restResource"] = urlParts[i];
                            break;
                        case 3 :
                            pa[$"reqVPar{i}"] = urlParts[i];
                            pa["restResId"] = urlParts[i];
                            break;
                        default:
                            pa[$"reqVPar{i}"] = urlParts[i];
                            break;
                    }
                }
                ProcessingArgs.AccumulateRequestPars(request, pa);
                if (pa.GetControlVal("requireLogForce", "nope").Equals("nope")) { // if not overridden via config
                    pa["requireLogForce"] = "yes"; // start off assuming we don't want logging unless forced (better performance this way)
                }

                if (pa.GetControlVal("reqVPar3", "nope").Equals("body") || pa.GetControlVal("allowBodyOverride", "nope").Equals("yes")) {  // initialize params from the body payload.
                    if (pa["reqContent"] != null) {
                        string varStg;
                        if (pa["reqContent"].Length > 8192) {
                            varStg = pa["reqContent"].Substring(0, 8192);
                            pa.Override(varStg);
                        }
                        else {
                            pa.Override(pa["reqContent"]);
                        }
                    }
                }
                string[] rVals = ServicesBroker.TestProcess(pa);
                respArr[0] = "no data back from ServicesBroker";
                if (rVals.Length > 0) {
                    respArr[0] = rVals[rVals.Length - 1];
                    //if (pa.GetControlVal("Accept", "nada").ToLower().Equals("application/json")) {
                    //    respArr[1] = "application/json";
                    //}
                    respArr[1] = pa.GetControlVal("Accept", "text/plain");
                }
                //LocalLogMsg($" got request {reqBody}");
            }
            catch (Exception exc) {
                respArr[0] = $"exception for '{reqBody}' SimpleWebServer SendResponse {exc.Message}, {exc.InnerException?.Message}";
                LocalLogMsg($" {respArr[0]}");
            }
            return respArr;
        }

        public void Run() {
            ThreadPool.QueueUserWorkItem((o) => {
                Console.WriteLine("Webserver running...");
                while (_listener.IsListening) {
                    try {
                        ThreadPool.QueueUserWorkItem((c) => {
                            var ctx = c as HttpListenerContext;

                            if(ctx == null)
                                throw new Exception("HttpListenerContext is null");
                            string[] rstr = _responderMethod(ctx.Request);
                            if (!string.IsNullOrEmpty(rstr[0]))
                            {
                                byte[] buf = Encoding.UTF8.GetBytes(rstr[0]);
                                ctx.Response.ContentType = rstr[1];
                                ctx.Response.ContentLength64 = buf.Length;
                                ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                            }
                            else
                            {
                                byte[] buf = Encoding.UTF8.GetBytes(" null response from web responder ");
                                ctx.Response.ContentLength64 = buf.Length;
                                ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                            }
                        }, _listener.GetContext());
                    }
                    catch (Exception exc) {
                        Trace.WriteLine($" SimpleWebServer.Run exception {exc.Message}, {exc.InnerException?.Message}");
                    }
                }

            });
        }

        public void Stop() {
            _listener.Stop();
            _listener.Close();
        }
    }


    class SomeTests {

        public static void TimeTests() {
            DateTime thisDate = new DateTime(2007, 3, 10, 0, 0, 0);
            DateTime dstDate = new DateTime(2007, 6, 10, 0, 0, 0);
            DateTimeOffset thisTime;

            thisTime = new DateTimeOffset(dstDate, new TimeSpan(-7, 0, 0));
            ShowPossibleTimeZones(thisTime);

            thisTime = new DateTimeOffset(thisDate, new TimeSpan(-6, 0, 0));
            ShowPossibleTimeZones(thisTime);

            thisTime = new DateTimeOffset(thisDate, new TimeSpan(+1, 0, 0));
            ShowPossibleTimeZones(thisTime);
        }

        private static void ShowPossibleTimeZones(DateTimeOffset offsetTime) {
            TimeSpan offset = offsetTime.Offset;
            ReadOnlyCollection<TimeZoneInfo> timeZones;

            Console.WriteLine("{0} could belong to the following time zones:",
                              offsetTime.ToString());
            // Get all time zones defined on local system
            timeZones = TimeZoneInfo.GetSystemTimeZones();
            // Iterate time zones 
            foreach (TimeZoneInfo timeZone in timeZones) {
                // Compare offset with offset for that date in that time zone
                if (timeZone.GetUtcOffset(offsetTime.DateTime).Equals(offset))
                    Console.WriteLine("   {0}", timeZone.DisplayName);
            }
            Console.WriteLine();
        }
    }


}
