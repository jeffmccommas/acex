﻿using System.Web;

// ReSharper disable once CheckNamespace
namespace AO.TimeSeriesProcessing {
    class CustomHelpers {
    }

    public static class AlertsCustomHelpers {
        public static IHtmlString Create_Label(string Content) {
            string LableStr = $"<label style=\"background-color:gray;color:yellow;font-size:24px\">{Content}</label>";
            return new HtmlString(LableStr);
        }


        public static IHtmlString ColorResultsString(string Content) {
            string coloredString = "";
            if (Content.Contains("**analMiss**")) {
                coloredString = $"<div style=\"background-white:gray;color:red\">{Content}</div>";
            }
            else if (Content.Contains("**analHit**")) {
                coloredString = $"<div style=\"background-white:gray;color:green\">{Content}</div>";
            }
            else if (Content.Contains("**analComplete**")) {
                coloredString = $"<div style=\"background-color:green;color:white\">{Content}</div>";
            }
            else if (Content.Contains("**miss**")) {
                coloredString = $"<div style=\"background-color:yellow;color:black\">{Content}</div>";
            }
            else if (Content.Contains("**missed**")) {
                coloredString = $"<div style=\"background-color:red;color:white\">{Content}</div>";
            }
            else if (Content.Contains("**hits**")) {
                coloredString = $"<div style=\"background-color:green;color:white\">{Content}</div>";
            }
            else {
                coloredString = $"<div style=\"background-color:white;color:black\">{Content}</div>";
            }
            return new HtmlString(coloredString);
        }
        public static IHtmlString StatusResultsString(string Content) {
            string coloredString = "";
            if (Content.Contains("**analMiss**")) {
                coloredString = $"<div style=\"background-white:gray;color:red\">{Content}</div>";
            }
            else if (Content.Contains("**analComplete**")) {
                coloredString = $"<div style=\"background-color:green;color:white\">{Content}</div>";
            }
            else if (Content.Contains("**miss**")) {
                coloredString = $"<div style=\"background-color:yellow;color:black\">{Content}</div>";
            }
            else if (Content.Contains("**missed**")) {
                coloredString = $"<div style=\"background-color:red;color:white\">{Content}</div>";
            }
            else if (Content.Contains("**hits**")) {
                coloredString = $"<div style=\"background-color:green;color:white\">{Content}</div>";
            }
            else {
                coloredString = $"";
            }
            return new HtmlString(coloredString);
        }

    }
}
