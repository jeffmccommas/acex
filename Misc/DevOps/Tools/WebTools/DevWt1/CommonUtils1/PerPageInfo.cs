﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

// ReSharper disable once CheckNamespace
namespace AO.TimeSeriesProcessing
{
    public class PerPageInfo {
        public int PerPage;
        public int Page;
        public int Start;
        public int Total;

        private HybridDictionary _hpars;
        private StringDictionary _pars;

        public StringDictionary Values {
            get { return _pars; }
        }

        public PerPageInfo(NameValueCollection qparams) {
            _hpars = new HybridDictionary();
            _pars = new StringDictionary();
            PerPage = 250;
            Page = 0;
            Start = 0;
            foreach (string key in qparams.Keys) {
                if (!string.IsNullOrEmpty(key)) {
                    _pars.Add(key, qparams[key]);
                    _hpars[key] = qparams[key];
                }
            }
            if (_pars["format"] == null) {
                _pars["format"] = "xml";
            }
            //if (pars["user"] == null)
            //{
            //    pars["user"] = SubsystemConfig.GetConfigSetting("[$UrmRestCfg]/DefUser", "admin");
            //}
            //if (pars["pwd"] == null)
            //{
            //    pars["pwd"] = SubsystemConfig.GetConfigSetting("[$UrmRestCfg]/DefPwd", "$Recover123");
            //}
            if (qparams["per_page"] != null) {
                int.TryParse(qparams["per_page"], out PerPage);
                int MaxPage = 250;
                if (qparams["max_page"] != null)  int.TryParse(qparams["max_page"], out MaxPage);
                if (PerPage > MaxPage) PerPage = MaxPage;
                else if (PerPage < 1) PerPage = 1;
            }
            if (qparams["start"] != null) {
                int.TryParse(qparams["start"], out Start);
            }
            if (qparams["page"] != null) {
                if (int.TryParse(qparams["page"], out Page)) {
                    Page -= 1; // internally  use zero based
                }
            }
            if (Page > 0) {
                Start = Page*PerPage;
            }
            if (Page < 0) {
                Page = 0;
            }

            Total = PerPage;
        }

        public string this[string key] => _pars[key];

        private const string NoEmitParams = "format,user,pwd,http,rest,urlFormatStg";

        public string AsQueryParams(string excludeList) {
            List<string> avoidParams;
            if (excludeList != null) {
                avoidParams =
                    new List<string>(excludeList.Split(new[] {','}, 50, StringSplitOptions.RemoveEmptyEntries));
            }
            else {
                avoidParams =
                    new List<string>(NoEmitParams.Split(new[] {','}, 50, StringSplitOptions.RemoveEmptyEntries));
            }
            List<string> parVal = new List<string>();
            if (_pars.Count > 0) {
                foreach (string par in _pars.Keys) {
                    if (!avoidParams.Contains(par)) {
                        parVal.Add(par + "=" + _pars[par]);
                    }
                }
            }
            string rval = "";
            if (parVal.Count > 0) {
                rval = "?";
                foreach (string pv in parVal) {
                    rval += pv;
                    rval += "&";
                }
                rval = rval.Substring(0, rval.Length - 1); // strip off trailing &
            }

            return rval;
        }





        public HybridDictionary AsHybridDictionary() {
            return _hpars;
        }

    }


}

