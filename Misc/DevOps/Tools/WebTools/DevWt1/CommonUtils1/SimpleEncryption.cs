﻿using System;
using System.Security.Cryptography;
using System.Text;

// ReSharper disable once CheckNamespace
namespace AO.TimeSeriesProcessing
{
    public class SimpleEncryption
    {

        public static string Crypt(string text, byte[] key = null, byte[] iv = null)
        {
            if (key == null) key = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            if (iv == null) key = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            SymmetricAlgorithm algorithm = DES.Create();
            ICryptoTransform transform = algorithm.CreateEncryptor(key, iv);
            byte[] inputbuffer = Encoding.Unicode.GetBytes(text);
            byte[] outputBuffer = transform.TransformFinalBlock(inputbuffer, 0, inputbuffer.Length);
            return Convert.ToBase64String(outputBuffer);
        }

        public static string Decrypt(string text, byte[] key = null, byte[] iv = null)
        {
            if (key == null) key = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            if (iv == null) key = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            SymmetricAlgorithm algorithm = DES.Create();
            ICryptoTransform transform = algorithm.CreateDecryptor(key, iv);
            byte[] inputbuffer = Convert.FromBase64String(text);
            byte[] outputBuffer = transform.TransformFinalBlock(inputbuffer, 0, inputbuffer.Length);
            return Encoding.Unicode.GetString(outputBuffer);
        }
    }

}
