﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Cassandra;

namespace AO.TimeSeriesProcessing
{


    public static class SafeAwaiter {
        public static T GetResultSafe<T>(this Task<T> task) {
            if (SynchronizationContext.Current == null)
                return task.Result;

            if (task.IsCompleted)
                return task.Result;

            var tcs = new TaskCompletionSource<T>();
            task.ContinueWith(t =>
            {
                var ex = t.Exception;
                if (ex != null)
                    tcs.SetException(ex);
                else
                    tcs.SetResult(t.Result);
            }, TaskScheduler.Default);

            return tcs.Task.Result;
        }
    }


    /// <summary>
    /// isolate persistance connection information for caching of different connections.
    /// </summary>
    public class SeriesStorePersistenceInfo : IDisposable {

        static private Dictionary<string,SeriesStorePersistenceInfo> persistenceData = new Dictionary<string, SeriesStorePersistenceInfo>();

        private Cluster Cluster { get; set; }
        public ISession Session { get; set; }

        public void Dispose() {
            try {
                Cluster.Shutdown();
                if (persistenceData[_cacheKey] != null) {
                    persistenceData.Remove(_cacheKey);
                }
            }
            catch (Exception exc) {
                Debug.WriteLine($" CassConn1.Dispose exception {exc.Message}, {exc.InnerException?.Message}");
            }
        }

        /// <summary>
        /// Maybe discard existing cached connection
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="doDiscard"></param>
        /// <returns></returns>
        public static bool Discard(ProcessingArgs pa, bool doDiscard = true) {
            string cassUsername;
            string cassPassword;
            string keySpace;
            string cacheKey;
            string cassAddresses;
            GetCxnPars(pa, out cassUsername, out cassPassword, out keySpace, out cassAddresses, out cacheKey);
            if (persistenceData.ContainsKey(cacheKey)) {
                if (doDiscard) {
                    persistenceData[cacheKey].Dispose();
                }
                return true;
            }
            return false;
        }

        private static void GetCxnPars(ProcessingArgs pa, out string cassUsername, out string cassPassword, out string keySpace, out string addresses, out string cacheKey) {
            string env = pa.GetControlVal("aclenv", "work");
            cassUsername = pa.GetControlVal($"{env}cassandraUsername", "cassDefaultSeriesMgtUser");
            cassUsername = pa.GetControlVal("specCassandraUsername", cassUsername);
            cassPassword = pa.GetControlVal($"{env}cassandraPassword", "cassDefaultSeriesMgtPwd");
            cassPassword = pa.GetControlVal("specCassandraPassword", cassPassword);
            keySpace = pa.GetControlVal($"{env}cassandraKeyspace", "defaultSeriesMgtKeyspace");  // environment or default
            keySpace = pa.GetControlVal("specCassandraKeyspace", keySpace);  // with possible override
            addresses = pa.GetControlVal($"{env}cassandraIP", "unspecified");
            addresses = pa.GetControlVal("specCassandraAddresses", addresses);  // with possible override
            cacheKey = $"{cassUsername}|{cassPassword}|{keySpace}|{addresses}";
        }


        public static SeriesStorePersistenceInfo GetCacheCxn(ProcessingArgs pa) {
            string cassUsername;
            string cassPassword;
            string keySpace;
            string cacheKey;
            string cassAddresses;
            GetCxnPars(pa, out cassUsername, out cassPassword, out keySpace, out cassAddresses, out cacheKey);
            SeriesStorePersistenceInfo ssp = null;
            if (persistenceData.ContainsKey(cacheKey)) {
                ssp = persistenceData[cacheKey];
            }
            else {
                ssp = new SeriesStorePersistenceInfo(pa);
                persistenceData[cacheKey] = ssp;
            }
            return ssp;
        }

        public SeriesStorePersistenceInfo(ProcessingArgs pa) {

            GetCxnPars(pa, out _username, out _password, out _keyspace, out _addresses, out _cacheKey);
            Builder clusterBuilder = Cluster.Builder();
            string[] clhosts = _addresses.Split(',');
            foreach (string clhost in clhosts) {
                string hName = clhost.Trim();
                if (!string.IsNullOrEmpty(hName)) {
                    clusterBuilder.AddContactPoint(clhost);
                }
            }

            clusterBuilder.WithCredentials(_username, _password);
            clusterBuilder.WithQueryTimeout(60000);
            Cluster = clusterBuilder.Build();

            //Cluster = Cluster.Builder().AddContactPoint(pa[$"{env}cassandraIP"]).WithCredentials(pa[$"{env}cassandraUsername"], pa[$"{env}cassandraPassword"]).Build();
            Debug.WriteLine("Connected to cluster: " + Cluster.Metadata.ClusterName);
            foreach (var host in Cluster.Metadata.AllHosts()) {
                Debug.WriteLine("Data Center: " + host.Datacenter + ", " +
                                "Host: " + host.Address + ", " +
                                "Rack: " + host.Rack);
            }

            Session = null;
            try {
                Session = Cluster.Connect(_keyspace);
            }
            catch (Exception exc) {
                Debug.WriteLine($"  will create due to exception on Cluster.Connect({_keyspace}) : {exc.Message} ");
                Session = Cluster.Connect();
                Session = Cluster.Connect(_keyspace);
            }

        }

        public void Discard() {

        }

        private string _keyspace;
        private string _username;
        private string _password;
        private string _addresses;
        private string _cacheKey;

    }


    /// <summary>
    /// for cassandra batch execution -- 
    /// </summary>
    public class BatchExecutor {
        public BatchExecutor() {
        }

        public RowSet Execute(List<string> qryStgs, SeriesStorePersistenceInfo cc1) {
            BatchStatement batch = new BatchStatement();
            foreach (string qry in qryStgs) {
                batch.Add(new SimpleStatement(qry));
            }
            return cc1.Session.Execute(batch);
        }


        public string ExecuteGet(ProcessingArgs pa, ContextData context, List<string> qryStgs, SeriesStorePersistenceInfo cc1) {
            var tasks = new List<Task<RowSet>>();
            string res;
            foreach (string qry in qryStgs) {
                var task = GetAsync(qry, cc1);
                tasks.Add(task);
            }
            Task<RowSet[]> tasksRows = Task.WhenAll(tasks);
            int batchTimeout = pa.GetControlVal("batchTimeout", 100);
            int batchSleepMs = pa.GetControlVal("batchSleepMs", 10);
            int waitLimit = (batchTimeout * 1000) / batchSleepMs;
            while (!tasksRows.IsCompleted && (waitLimit-- > 0)) {
                Thread.Sleep(10);
            }
            if (!tasksRows.IsCompleted && (waitLimit < 0)) { // whoops -- batch not completed
                context.ContextWrite("ExecuteGet batch timeout", $">{batchTimeout} secs for {tasks.Count}");
            }

            RowSet[] results = tasksRows.Result;
            res = $" {results.Length} results, ";
            foreach (RowSet resultRs in results) {
                res += $" {resultRs.GetAvailableWithoutFetching()}, ";
                //foreach (Row r in resultRs) {
                //}
            }
            return res;
        }



        public DataTable ExecuteGetTable(ProcessingArgs pa, ContextData context, List<string> qryStgs, SeriesStorePersistenceInfo cc1) {
            var tasks = new List<Task<RowSet>>();
            string res;
            DateTime dateTime = DateTime.Now;
            bool logCassQueries = pa.GetControlVal("logCassQueries", "0").Equals("1");
            if (logCassQueries) {
                context.ContextWriteSeg("ExecuteGetTable",true, dateTime);
            }
            foreach (string qry in qryStgs) {
                var task = GetAsync(qry, cc1);
                tasks.Add(task);
                if (logCassQueries) {
                    context.MainStream.AddLine($"cass query '{qry}' ");
                }
            }
            Task<RowSet[]> tasksRows = Task.WhenAll(tasks);
            int batchTimeout = pa.GetControlVal("batchTimeout", 100);
            int batchSleepMs = pa.GetControlVal("batchSleepMs", 10);
            int waitLimit = (batchTimeout * 1000) / batchSleepMs;
            while (!tasksRows.IsCompleted && (waitLimit-- > 0)) {
                Thread.Sleep(batchSleepMs);
            }
            context.Add2Counter("casQueries", tasks.Count);
            if (!tasksRows.IsCompleted && (waitLimit < 0)) { // whoops -- batch not completed
                context.ContextWrite("ExecuteGetTable batch timeout", $">{batchTimeout} secs for {tasks.Count}");
            }
            else {
                if (logCassQueries) {
                    context.MainStream.AddLine($"cass {qryStgs.Count} queries completed' ");
                }
            }
            if (logCassQueries) {
                context.ContextWriteSeg("ExecuteGetTable", false, dateTime);
            }

            RowSet[] results = tasksRows.Result;
            DataTable dt = Rowset2DataTable(results);
            if (logCassQueries) {
                context.ContextWriteSeg($"ExecuteGetTableConsolidated {dt.Rows.Count} ", false, dateTime);
            }

            return dt;
        }


        /// <summary>
        /// This is still a synchronous path since having thread pool deadlock issues with assembly used in IIS as well as console
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="context"></param>
        /// <param name="qryStgs"></param>
        /// <param name="cc1"></param>
        /// <returns></returns>
        public DataTable ExecuteGetTable3(ProcessingArgs pa, ContextData context, List<string> qryStgs, SeriesStorePersistenceInfo cc1) {
            //List<Task> tasks = new List<Task>();
            List<Task<Task<RowSet>>> tasks = new List<Task<Task<RowSet>>>();
            string res;
            foreach (string qry in qryStgs) {
                var t = Task.Run(async () => {
                    return GetAsync2(qry, cc1);
                });
                tasks.Add(t);
            }
            Task.WaitAll(tasks.ToArray());
            List<RowSet> results = new List<RowSet>();
            int successCount = 0;
            Task<Task<RowSet>>[] tasks2 = tasks.ToArray();
            foreach (Task<Task<RowSet>> task in tasks2) {
                if (task.IsCompleted) {
                    successCount++;
                }
            }
            if (successCount == tasks2.Length) {
                Debug.WriteLine($" {tasks.Count} of {tasks2.Length} completed");
            }
            foreach (Task<Task<RowSet>> task in tasks2) {
                if (task.IsCompleted) {
                    results.Add(task.GetAwaiter().GetResult().Result);
                }
            }

            DataTable dt = Rowset2DataTable(results);
            return dt;
        }



        /// <summary>
        /// having async thread issues (known problem) so sync implementation
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="context"></param>
        /// <param name="qryStgs"></param>
        /// <param name="cc1"></param>
        /// <returns></returns>
        public DataTable ExecuteGetTable2(ProcessingArgs pa, ContextData context, List<string> qryStgs, SeriesStorePersistenceInfo cc1) {
            List<RowSet> list = new List<RowSet>();
            foreach (string qry in qryStgs) {
                SimpleStatement ss = new SimpleStatement(qry);
                list.Add(cc1.Session.Execute(ss));
            }
            DataTable dt = Rowset2DataTable(list);
            return dt;
        }


        public static DataTable Rowset2DataTable(IEnumerable<RowSet> results) {
            DataSet ds = new DataSet("noSetName");
            DataTable dt = new DataTable("casdata");
            ds.Tables.Add(dt);
            bool initialized = false;
            foreach (RowSet resultRs in results) {
                if (!initialized) {
                    foreach (CqlColumn ccol in resultRs.Columns) {
                        dt.Columns.Add(ccol.Name, ccol.Type);
                    }
                    initialized = true;
                }
                foreach (Row r in resultRs) {
                    dt.Rows.Add(r.ToArray());
                }
            }
            return dt;
        }

        //public static DataTable Rowset2DataTable(RowSet[] results) {
        //    DataSet ds = new DataSet("noSetName");
        //    DataTable dt = new DataTable("casdata");
        //    ds.Tables.Add(dt);
        //    foreach (CqlColumn ccol in results[0].Columns) {
        //        dt.Columns.Add(ccol.Name, ccol.Type);
        //    }
        //    foreach (RowSet resultRs in results) {
        //        foreach (Row r in resultRs) {
        //            dt.Rows.Add(r.ToArray());
        //        }
        //    }
        //    return dt;
        //}

        public static DataTable Rowset2DataTable(List<RowSet> results) {
            if (results.Count == 0) {
                return null;
            }
            DataSet ds = new DataSet("noSetName");
            DataTable dt = new DataTable("casdata");
            ds.Tables.Add(dt);
            foreach (CqlColumn ccol in results[0].Columns) {
                dt.Columns.Add(ccol.Name, ccol.Type);
            }
            foreach (RowSet resultRs in results) {
                foreach (Row r in resultRs) {
                    dt.Rows.Add(r.ToArray());
                }
            }
            return dt;
        }


        public static DataTable Rowset2DataTable(RowSet results) {
            DataSet ds = new DataSet("noSetName");
            DataTable dt = new DataTable("casdata");
            ds.Tables.Add(dt);
            foreach (CqlColumn ccol in results.Columns) {
                dt.Columns.Add(ccol.Name, ccol.Type);
            }
            foreach (Row r in results) {
                dt.Rows.Add(r.ToArray());
            }
            return dt;
        }


        async Task<RowSet> GetAsync(string qry, SeriesStorePersistenceInfo cc1) {
            SimpleStatement ss = new SimpleStatement(qry);
            Task<RowSet> rsTask = cc1.Session.ExecuteAsync(ss);
            return await rsTask;
        }


        async Task<RowSet> GetAsync2(string qry, SeriesStorePersistenceInfo cc1) {
            SimpleStatement ss = new SimpleStatement(qry);
            return await cc1.Session.ExecuteAsync(ss);
        }

        ConfiguredTaskAwaitable<RowSet> GetAsync3(string qry, SeriesStorePersistenceInfo cc1) {
            SimpleStatement ss = new SimpleStatement(qry);
            ConfiguredTaskAwaitable<RowSet> rsTask = cc1.Session.ExecuteAsync(ss).ConfigureAwait(false);
            return rsTask;
        }


    }


    /// <summary>
    /// Cassandra access layer ('DAL' for cassandra operations) and data BL for in memory data shard processing
    /// Might consider breaking out the data shard operations into a separate class as they've become numerous.
    /// </summary>
    public class CassConn1 : IDisposable {


        public void Dispose() {
            try {
                ssp.Session.Cluster.Shutdown();
            }
            catch (Exception exc) {
                Debug.WriteLine($" CassConn1.Dispose exception {exc.Message}, {exc.InnerException?.Message}");
            }
        }

        SeriesStorePersistenceInfo ssp;

        public void Connect(ProcessingArgs pa) {
            ssp = SeriesStorePersistenceInfo.GetCacheCxn(pa);
        }


        /// <summary>
        /// perform a set of operationsin a batch mode.  Expectation is improved performance over singular statement executions.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="qryStgs"></param>
        /// <returns></returns>
        public RowSet PerformCasQueryBatch(ProcessingArgs pa, List<string> qryStgs) {
            BatchStatement batch = new BatchStatement();
            foreach (string qry in qryStgs) {
                batch.Add(new SimpleStatement(qry));
            }
            return ssp.Session.Execute(batch);
        }


        /// <summary>
        /// provides a standard DataTable result from cassandra results
        /// </summary>
        /// <param name="queryNum"></param>
        /// <param name="pa"></param>
        /// <param name="resultTable"></param>
        /// <returns></returns>
        public string PerformCasQuery(int queryNum, ProcessingArgs pa, out DataTable resultTable) {
            string cSel = pa["qSel"]; // assume default.
            string cRows = pa["qRows"]; // assume default.
            if (!string.IsNullOrEmpty(pa[$"qSel{queryNum}"])) {
                cSel = pa[$"qSel{queryNum}"]; // new (still restricted) Cquery spec
                if (cSel.Trim().Equals("***")) {
                    cSel = "";
                }
            }
            
            string queryLine = $"SELECT {pa["qRows"]} FROM {pa["reqVsrc"]} {cSel} limit {pa["maxrows"]} {pa["allowFilter"]};";
            if (ssp == null) {
                ssp = new SeriesStorePersistenceInfo(pa);
            }
            RowSet results = ssp.Session.Execute(queryLine);

            DataTable dt = new DataTable("casdata");
            foreach (CqlColumn ccol in results.Columns) {
                dt.Columns.Add(ccol.Name, ccol.Type);
            }
            foreach (Row result in results) {
                dt.Rows.Add(result.ToArray());
            }

            DataView dv = new DataView(dt);
            string sortDir = string.IsNullOrEmpty(pa["sortDir"]) ? "DESC" : pa["sortDir"];
            if (!string.IsNullOrEmpty(pa["sortCol"])) {
                dv.Sort = $"{pa["sortCol"]} {sortDir}";
            }
            resultTable = dv.ToTable();
            return Guid.NewGuid().ToString();
        }


        /// <summary>
        /// Major entry point for lower level cassandra queries where criteria are specified.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="qryStg"></param>
        /// <returns></returns>
        public RowSet PerformCasQuery(ProcessingArgs pa, ref string qryStg) {
            string cSel = pa["qSel"]; // assume default.
            string cRows = pa["qRows"]; // assume default.
            if (!string.IsNullOrEmpty(pa["qSel0"])) cSel = pa["qSel0"]; // new (still restricted) Cquery spec
            if (cSel.Trim().Equals("***")) {
                cSel = "";
            }

            if (pa["maxrows"].Equals("-1")) {
                pa["maxrows"] = "1000000";  // hard clamp at a million for now.
            }
            bool isPopulate = (!string.IsNullOrEmpty(pa["isInsert"])) && pa["isInsert"].Equals("1");
            bool isDelete = (!string.IsNullOrEmpty(pa["isDelete"])) && pa["isDelete"].Equals("1");
            if (isPopulate) {
                string queryLine = $"insert into {pa["reqVsrc"]} {cSel} ;";
                if (qryStg != null) qryStg = queryLine;
                return ssp.Session.Execute(queryLine);
            }
            else if (isDelete) {
                string queryLine = $"delete from {pa["reqVsrc"]} {cSel} ;";
                if (qryStg != null) qryStg = queryLine;
                return ssp.Session.Execute(queryLine);
            }
            else {
                string queryLine = $"SELECT {pa["qRows"]} FROM {pa["reqVsrc"]} {cSel} limit {pa["maxrows"]} {pa["allowFilter"]};";
                if (qryStg != null) qryStg = queryLine;
                try {
                    return ssp.Session.Execute(queryLine);
                }
                catch (Exception exc) {
                    throw new Exception($" cassandra query exception '{exc.Message}', '{exc.InnerException?.Message}'  for '{queryLine}'");
                }
            }
        }



        /// <summary>
        /// execute a set of queries as a batch asynch
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="qryStgs"></param>
        /// <returns></returns>
        public async Task<RowSet> PerformCasQueryBatchAsynch(ProcessingArgs pa, List<string> qryStgs) {
            BatchStatement batch = new BatchStatement();
            foreach (string qry in qryStgs) {
                SimpleStatement ss = new SimpleStatement(qry);
                batch.Add(ss);
            }
            return await ssp.Session.ExecuteAsync(batch);
        }

        /// <summary>
        /// asynch query
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="qryStg"></param>
        /// <returns></returns>
        public async Task<RowSet> PerformCasQueryAsynch(ProcessingArgs pa, string qryStg) {
            string cSel = pa["qSel"]; // assume default.
            string cRows = pa["qRows"]; // assume default.
            if (!string.IsNullOrEmpty(pa["qSel0"])) cSel = pa["qSel0"]; // new (still restricted) Cquery spec
            bool isPopulate = (!string.IsNullOrEmpty(pa["isInsert"])) && pa["isInsert"].Equals("1");
            bool isDelete = (!string.IsNullOrEmpty(pa["isDelete"])) && pa["isDelete"].Equals("1");
            if (isPopulate) {
                string queryLine = $"insert into {pa["reqVsrc"]} {cSel} ;";
                SimpleStatement ss = new SimpleStatement(queryLine);
                Task<RowSet> rsTask = ssp.Session.ExecuteAsync(ss);
                return await rsTask;
            }
            else if (isDelete) {
                string queryLine = $"delete from {pa["reqVsrc"]} {cSel} ;";
                SimpleStatement ss = new SimpleStatement(queryLine);
                Task<RowSet> rsTask = ssp.Session.ExecuteAsync(ss);
                return await rsTask;
            }
            else {
                string queryLine = $"SELECT {pa["qRows"]} FROM {pa["reqVsrc"]} {cSel} limit {pa["maxrows"]} {pa["allowFilter"]};";
                SimpleStatement ss = new SimpleStatement(queryLine);
                Task<RowSet> rsTask = ssp.Session.ExecuteAsync(ss);
                return await rsTask;
            }
        }


        /// <summary>
        /// simple asynch query
        /// </summary>
        /// <param name="qryStg"></param>
        /// <returns></returns>
        public async Task<RowSet> PerformCasQueryAsynch(string qryStg) {
                SimpleStatement ss = new SimpleStatement(qryStg);
                Task<RowSet> rsTask = ssp.Session.ExecuteAsync(ss);
                return await rsTask;
        }


        /// <summary>
        /// Create a cassandra query string
        /// </summary>
        /// <param name="pa"></param>
        /// <returns></returns>
        public string GetCasQuery(ProcessingArgs pa) {
            string cSel = pa["qSel"]; // assume default.
            string cRows = pa["qRows"]; // assume default.
            if (!string.IsNullOrEmpty(pa["qSel0"])) cSel = pa["qSel0"]; // new (still restricted) Cquery spec
            string queryLine = "";
            bool isPopulate = (!string.IsNullOrEmpty(pa["isInsert"])) && pa["isInsert"].Equals("1");
            bool isDelete = (!string.IsNullOrEmpty(pa["isDelete"])) && pa["isDelete"].Equals("1");
            if (isPopulate) {
                queryLine = $"insert into {pa["reqVsrc"]} {cSel} ;";
            }
            else if (isDelete) {
                queryLine = $"delete from {pa["reqVsrc"]} {cSel} ;";
            }
            else {
                queryLine = $"SELECT {pa["qRows"]} FROM {pa["reqVsrc"]} {cSel} limit {pa["maxrows"]} {pa["allowFilter"]};";
            }
            return queryLine;
        }


        /// <summary>
        /// Performs one or more data reduction/filtering operations against the accumulating memory data shards
        /// and returns the final result as a paged data table.  Good for returning results for web response but note full work has to 
        /// be done for each call here.
        /// Note that additional intermediate results can accumulate for subsequent processing
        /// </summary>
        /// <param name="queryResultTables"></param>
        /// <param name="pa"></param>
        /// <param name="ppInfo"></param>
        /// <param name="resultsFilteredFileNameRoot"></param>
        /// <param name="contextData"></param>
        /// <returns></returns>
        public static DataTable GetResultPage(List<DataTable> queryResultTables, ProcessingArgs pa, PerPageInfo ppInfo, string resultsFilteredFileNameRoot, ContextData contextData) {
            DataTable dt3 = null;

            int maxVars = 20;
            int tVal;
            if (ppInfo["maxvars"] != null && int.TryParse(ppInfo["maxvars"], out tVal)) {
                maxVars = (tVal < 100) ? tVal : 100;
            }

            // handle multiple sql filters and result pages
            SqlFilter(queryResultTables, pa, resultsFilteredFileNameRoot, contextData, maxVars);

            // and multiple levels of post processing
            for (int procIdx = 0; procIdx < maxVars; procIdx++) {
                if (string.IsNullOrEmpty(pa[$"qOper{procIdx}"])) break;
                contextData.ContextWrite("proc_step", pa[$"qOper{procIdx}"]);

                string[] ppars = pa[$"qOper{procIdx}"].Trim().Split(' ');
                try {
                    switch (ppars[0]) {
                        case "csv": {
                                contextData.ContextWrite("proc_csv", ppars.Aggregate((current, next) => current + "," + next));
                                QryCsvProcessing(resultsFilteredFileNameRoot, pa, ppars, queryResultTables, procIdx, contextData);
                            }
                            break;
                        case "seq": {
                                contextData.ContextWrite("proc_seq", ppars.Aggregate((current, next) => current + "," + next));
                                QrySeqProcessing(resultsFilteredFileNameRoot, pa, ppars, queryResultTables, procIdx, contextData);
                            }
                            break;
                        case "diff": {
                                contextData.ContextWrite("proc_dif", ppars.Aggregate((current, next) => current + "," + next));
                                QryDiffProcessing(resultsFilteredFileNameRoot, pa, ppars, queryResultTables, procIdx, contextData);
                            }
                            break;
                        case "count": {
                                contextData.ContextWrite("proc_dif", ppars.Aggregate((current, next) => current + "," + next));
                                //QryCountProcessing(resultsFilteredFileNameRoot, pa, ppars, queryResultTables, p, contextData);
                            }
                            break;
                        case "nondist": {
                                contextData.ContextWrite("nondist", ppars.Aggregate((current, next) => current + "," + next));
                                QryNondistProcessing(resultsFilteredFileNameRoot, pa, ppars, queryResultTables, procIdx, contextData);
                            }
                            break;
                        case "strip": {
                                contextData.ContextWrite("strip", ppars.Aggregate((current, next) => current + "," + next));
                                QryStripProcessing(resultsFilteredFileNameRoot, pa, ppars, queryResultTables, procIdx, contextData);
                            }
                            break;
                        case "yank": {
                                contextData.ContextWrite("strip", ppars.Aggregate((current, next) => current + "," + next));
                                QryYankProcessing(resultsFilteredFileNameRoot, pa, ppars, queryResultTables, procIdx, contextData);
                            }
                            break;
                        case "distinct": {
                                contextData.ContextWrite("strip", ppars.Aggregate((current, next) => current + "," + next));
                                QryDistinctProcessing(resultsFilteredFileNameRoot, pa, ppars, queryResultTables, procIdx, contextData);
                            }
                            break;
                    }
                }
                catch (Exception exc) {
                    string inner = (exc.InnerException != null) ? (exc.InnerException.Message) : "noinner";
                    string excStg = $" exception in p{procIdx}  qOper{procIdx}; {exc.Message}; {inner}";
                    Debug.WriteLine(excStg);
                    contextData?.ContextWrite(pa[$"qOper{procIdx}"] + "_exception", excStg);
                }
            }

            if (queryResultTables.Count > 0) {
                int filtCount = queryResultTables[queryResultTables.Count - 1].Rows.Count;
                pa["filtTotal"] = filtCount.ToString();

                if (filtCount < ppInfo.PerPage*ppInfo.Page) {
                        ppInfo.Page = filtCount / ppInfo.PerPage;
                        pa["pages"] = (filtCount/ppInfo.PerPage).ToString();
                    }
                    IEnumerable<DataRow> queryResultPage = queryResultTables[queryResultTables.Count-1].Rows.Cast<DataRow>()
                        .Skip(ppInfo.PerPage*ppInfo.Page)
                        .Take(ppInfo.PerPage);

                    dt3 = queryResultPage.CopyToDataTable();
                    pa["filtDisp"] = dt3.Rows.Count.ToString();

                    dt3.TableName = pa["reqVsrc"] ?? "logdata";
                    pa["pages"] = ((filtCount / ppInfo.PerPage) + 1).ToString();
                }
            return dt3;
        }

        /// <summary>
        /// Perform sql queries against the in pemory data shards
        /// </summary>
        /// <param name="queryResultTables"></param>
        /// <param name="pa"></param>
        /// <param name="resultsFilteredFileNameRoot"></param>
        /// <param name="contextData"></param>
        /// <param name="maxVars"></param>
        private static void SqlFilter(List<DataTable> queryResultTables, ProcessingArgs pa, string resultsFilteredFileNameRoot, ContextData contextData,
            int maxVars) {
            for (int q = 0; q < maxVars; q++) {
                if (string.IsNullOrEmpty(pa[$"qFilter{q}"])) break;
                try {
                    string qryTxt = pa[$"qFilter{q}"].Trim();
                    int idx = qryTxt.IndexOf(' ');
                    int tableNum = 0;
                    int tVal = 0;
                    if (int.TryParse(qryTxt.Substring(0, idx), out tVal)) {
                        tableNum = tVal;
                        qryTxt = qryTxt.Substring(idx).Trim();
                    }
                    if ((tableNum < 0) || (tableNum >= queryResultTables.Count)) {  // tablenums <0 are for query frags, too big == fail.
                        continue;
                    }
                    qryTxt = ValidateContexts.ReplaceParameters(pa, qryTxt);
                    contextData?.ContextWrite("qfilter_data", qryTxt);
                    DataRow[] queryResultRows = null;
                    queryResultRows = queryResultTables[tableNum].Select(qryTxt);
                    if (queryResultRows.Length > 0) {
                        DataTable dtf = queryResultRows[0].Table.Clone();
                        contextData?.ContextWrite($"qfilter_data", $" row count {queryResultRows.Length} ", qryTxt);
                        for (int i = 0; i < queryResultRows.Length; i++) {
                            dtf.ImportRow(queryResultRows[i]);
                        }

                        if (!string.IsNullOrEmpty(pa["sortCol"])) {
                            DataView dv = new DataView(dtf);
                            string sortDir = string.IsNullOrEmpty(pa["sortDir"]) ? "DESC" : pa["sortDir"];
                            dv.Sort = $"{pa["sortCol"]} {sortDir}";
                            dtf = dv.ToTable();
                        }

                        queryResultTables.Add(dtf);
                        if (!string.IsNullOrEmpty(resultsFilteredFileNameRoot)) {
                            var fileName = TableToFile(pa, resultsFilteredFileNameRoot, q, dtf);
                            if (contextData != null) {
                                contextData.ContextWrite("qfilter_file", fileName);
                                string webPath = pa["outputWeb"] + Path.GetFileName(fileName);
                                contextData.ContextWrite("qfilter_web", webPath);
                            }
                        }
                    }
                    else {
                        contextData?.ContextWrite("qfilter_res", qryTxt, "No result rows");
                    }
                }
                catch (Exception exc) {
                    string inner = (exc.InnerException != null) ? (exc.InnerException.Message) : "noinner";
                    string excStg = $" exception in q{q} {exc.Message}; {inner}";
                    Debug.WriteLine(excStg);
                    contextData?.ContextWrite(pa[$"qFilter{q}"] + "_exception", excStg);
                }
            }
        }


        /// <summary>
        /// we do a lot of determining how many of something there are for validation purposes
        /// </summary>
        /// <param name="contextData"></param>
        /// <param name="pa"></param>
        /// <param name="rs"></param>
        /// <param name="maxVars"></param>
        /// <returns></returns>
        public static int SqlCount(ContextData contextData,  ProcessingArgs pa,RowSet rs, int maxVars ) {
            int rVal = 0;
            for (int q = 0; q < maxVars; q++) {
                if (string.IsNullOrEmpty(pa[$"qFilter{q}"])) break;
                try {
                    string qryTxt = pa[$"qFilter{q}"].Trim();
                    int idx = qryTxt.IndexOf(' ');
                    int tVal = 0;
                    if (int.TryParse(qryTxt.Substring(0, idx), out tVal)) {
                        qryTxt = qryTxt.Substring(idx).Trim();
                    }
                    if (tVal>= 0) {// queries to go against frags are < 0.
                        continue;
                    }
                    qryTxt = ValidateContexts.ReplaceParameters(pa, qryTxt);
                    DataRow[] queryResultRows = null;
                    DataTable dt = BatchExecutor.Rowset2DataTable(rs);
                    queryResultRows = dt.Select(qryTxt);
                    rVal = queryResultRows.Length;
                }
                catch (Exception exc) {
                    string inner = (exc.InnerException != null) ? (exc.InnerException.Message) : "noinner";
                    string excStg = $" exception in q{q} {exc.Message}; {inner}";
                    Debug.WriteLine(excStg);
                    contextData?.ContextWrite(pa[$"qFilter{q}"] + "_exception", excStg);
                }
            }
            return rVal;
        }

        /// <summary>
        /// pick up file data into memory table shards for subsequent processing
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="contextData"></param>
        /// <param name="maxVars"></param>
        /// <param name="queryResultTables"></param>
        /// <returns></returns>
        public static DataTable IngestFiles(ProcessingArgs pa, ContextData contextData, int maxVars, List<DataTable> queryResultTables) {
            DataTable dt = null;
            for (int i = 0; i < maxVars; i++) {
                string fileName = pa[$"fiOper{i}"]?.Trim();
                if (string.IsNullOrEmpty(fileName)) continue;
                try {
                    contextData?.ContextWrite($"ingestfile_proc", "start", fileName);
                    dt = DataUtils1.TableFromFile(pa, contextData, fileName);
                    queryResultTables.Add(dt);
                    contextData?.ContextWrite($"ingestfile_proc", "end", dt.Rows.Count.ToString());
                }
                catch (Exception exc) {
                    contextData?.ContextWrite("error_ingestfile_proc", exc.Message);
                }
            }
            return dt;
        }


        /// <summary>
        /// Pick up Cassandra data into memory table(s) for subsequent processing
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="contextData"></param>
        /// <param name="maxVars"></param>
        /// <param name="queryResultTables"></param>
        /// <param name="doDbQuery"></param>
        /// <returns></returns>
        public static DataTable IngestCassandra(ProcessingArgs pa, ContextData contextData, int maxVars, List<DataTable> queryResultTables, bool doDbQuery) {
            DataTable dt = null;
            for (int qryIdx = 0; qryIdx < maxVars; qryIdx++) {
                string qryTxt = pa[$"qSel{qryIdx}"]?.Trim();
                if (string.IsNullOrEmpty(qryTxt)) continue;
                try {
                    contextData?.ContextWrite($"ingestcass_proc", "start", qryTxt);
                    if (doDbQuery) {
                        CassConn1 cc1 = new CassConn1();
                        pa["qRows"] = "*";
                        string etag = cc1.PerformCasQuery(qryIdx, pa, out dt);
                        queryResultTables.Add(dt);
                        if (!string.IsNullOrEmpty(pa["resultsrawfilepathname"])) {
                            if (qryIdx == 0) {
                                pa["X-BIO-etag"] = pa["myEtag"] = etag;
                                    contextData.SaveRawData(pa, dt);
                            }
                        }
                    }
                    else {
                        contextData?.ContextWrite($"ingestcass_proc", "just initialize");
                    }
                    contextData?.ContextWrite($"ingestcass_proc", "end", dt?.Rows.Count.ToString());
                }
                catch (Exception exc) {
                    contextData?.ContextWrite("error_ingestcass_proc", exc.Message);
                }
            }
            return dt;
        }


        /// <summary>
        /// export a memory table shard to a file in csv or xml format  
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="resultsFilteredFileNameRoot"></param>
        /// <param name="q"></param>
        /// <param name="dtf"></param>
        /// <returns></returns>
        private static string TableToFile(ProcessingArgs pa, string resultsFilteredFileNameRoot, int q, DataTable dtf) {
            string outputType = "xml";
            if (!string.IsNullOrEmpty(pa["outputType"])) {
                switch (pa["outputType"]) {
                    case "csv":
                        outputType = "csv";
                        break;
                    case "xml":
                        outputType = "xml";
                        break;
                }
            }
            string fileName = $"{resultsFilteredFileNameRoot}_Q{q}.{outputType}";
            pa[$"q{q}filename"] = fileName;
            using (StreamWriter sw = new StreamWriter(fileName)) {
                switch (outputType) {
                    case "csv":
                        string colNames = "";
                        foreach (DataColumn col in dtf.Columns) {
                            colNames += col.ColumnName + ",";
                        }
                        sw.WriteLine(colNames.Trim(','));
                        foreach (DataRow row in dtf.Rows) {
                            sw.WriteLine(string.Join(",", row.ItemArray));
                        }
                        break;
                    case "xml":
                        dtf.WriteXml(sw, true);
                        break;
                }
                sw.Close();
            }
            return fileName;
        }


        #region data_analysis_filtering 

        /// <summary>
        /// determine which records are not distinct in their data set
        /// </summary>
        /// <param name="resultsFilteredFileNameRoot"></param>
        /// <param name="pa"></param>
        /// <param name="ppars"></param>
        /// <param name="queryResultTables"></param>
        /// <param name="p"></param>
        /// <param name="contextData"></param>
        private static void QryNondistProcessing(string resultsFilteredFileNameRoot, ProcessingArgs pa, string[] ppars, List<DataTable> queryResultTables, int p, ContextData contextData = null) {
            int tryInt = 0;
            contextData?.ContextWrite($"nondistinct_proc", "start", string.Join(";", ppars));
            try {
                if (!int.TryParse(ppars[1], out tryInt)) {
                    contextData?.ContextWrite("NonDistP", "bad table selector", ppars[1]);
                }
                DataTable dt_ = queryResultTables[tryInt];

                int grpCnt = 1;
                if (int.TryParse(pa["nondistcnt"], out tryInt)) {
                    grpCnt = tryInt;
                }
                string[] cols = ppars[2].Trim().Split(',');
                string tableName = dt_.TableName;

                StringDictionary sd = new StringDictionary();

                List<DataRow> nondistRows = new List<DataRow>();

                switch (cols.Length) {
                    case 3: {
                            foreach (DataRow _dr in dt_.AsEnumerable()
                                .GroupBy(r => new {
                                    c1 = r.Field<string>($"{cols[0]}"),
                                    c2 = r.Field<string>($"{cols[1]}"),
                                    c3 = r.Field<string>($"{cols[2]}")
                                }).Where(grp => grp.Count() > grpCnt).SelectMany(itm => itm)) {
                                string key = string.Join(",", _dr.ItemArray);
                                if (!sd.ContainsKey(key)) {
                                    sd[key] = key;
                                    nondistRows.Add(_dr);
                                }
                            }
                        }
                        break;
                    case 4: {
                            foreach (DataRow _dr in dt_.AsEnumerable()
                                .GroupBy(r => new {
                                    c0 = r.Field<string>($"{cols[0]}"),
                                    c1 = r.Field<string>($"{cols[1]}"),
                                    c2 = r.Field<string>($"{cols[2]}"),
                                    c3 = r.Field<string>($"{cols[3]}")
                                }).Where(grp => grp.Count() > grpCnt).SelectMany(itm => itm)) {
                                string key = string.Join(",", _dr.ItemArray);
                                if (!sd.ContainsKey(key)) {
                                    sd[key] = key;
                                    nondistRows.Add(_dr);
                                }
                            }
                        }
                        break;
                    case 5: {
                            foreach (DataRow _dr in dt_.AsEnumerable()
                                .GroupBy(r => new {
                                    c0 = r.Field<string>($"{cols[0]}"),
                                    c1 = r.Field<string>($"{cols[1]}"),
                                    c2 = r.Field<string>($"{cols[2]}"),
                                    c3 = r.Field<string>($"{cols[3]}"),
                                    c4 = r.Field<string>($"{cols[4]}")
                                }).Where(grp => grp.Count() > grpCnt).SelectMany(itm => itm)) {
                                string key = string.Join(",", _dr.ItemArray);
                                if (!sd.ContainsKey(key)) {
                                    sd[key] = key;
                                    nondistRows.Add(_dr);
                                }
                            }
                        }
                        break;
                    default: {
                            contextData?.ContextWrite("NonDistP", "bad selector", ppars[2]);
                        }
                        break;
                }
                string fileName = $"{resultsFilteredFileNameRoot}_P{p}_{ppars[0]}.csv";
                pa[$"p{p}filename"] = fileName;
                contextData?.ContextWrite("NonDistP", "count", nondistRows.Count.ToString());
                using (StreamWriter sw = new StreamWriter(fileName)) {
                    sw.WriteLine(ppars[2].Trim());
                    foreach (var nondistRow in nondistRows) {
                        sw.WriteLine(string.Join(",", nondistRow.ItemArray));
                    }
                    sw.Close();
                }
            }
            catch (Exception exc) {
                contextData?.ContextWrite("error_nondistinctproc", string.Join(";", ppars), exc.Message);
            }
            contextData?.ContextWrite($"nondistinct_proc", "end", string.Join(";", ppars));
        }

        /// <summary>
        /// extract a set of distinct records from a data set
        /// </summary>
        /// <param name="resultsFilteredFileNameRoot"></param>
        /// <param name="pa"></param>
        /// <param name="ppars"></param>
        /// <param name="queryResultTables"></param>
        /// <param name="p"></param>
        /// <param name="contextData"></param>
        private static void QryDistinctProcessing(string resultsFilteredFileNameRoot, ProcessingArgs pa, string[] ppars, List<DataTable> queryResultTables, int p, ContextData contextData = null) {
            int tableNum = 0;
            contextData?.ContextWrite($"distinct_proc", "start", string.Join(";", ppars));
            try {
                int.TryParse(ppars[1], out tableNum);
                if ((tableNum >= 0) && (tableNum < queryResultTables.Count) && (queryResultTables[tableNum] != null)) {
                    contextData?.ContextWrite("strip", "startColumns", queryResultTables[tableNum].Columns.Count.ToString());
                    string qryTxt = ppars[2].Trim();
                    qryTxt = ValidateContexts.ReplaceParameters(pa, qryTxt);
                    string[] cols = qryTxt.Split(',');
                    if (cols.Length == 1) {
                        DataTable dtf = new DataTable("distincts");
                        dtf.Columns.Add(cols[0], queryResultTables[tableNum].Columns[qryTxt].DataType);
                        switch (queryResultTables[tableNum].Columns[qryTxt].DataType.Name) {  // TODO:  currently only deals with Int32 and string
                            case "Int32": {
                                //dtf.Columns.Add(cols[0]);
                                IEnumerable<int> distinctVals = (from row in queryResultTables[tableNum].AsEnumerable() select row.Field<int>(qryTxt)).Distinct();
                                foreach (int distinctVal in distinctVals) {
                                    int[] vals = { distinctVal };
                                    dtf.Rows.Add(vals[0].ToString()); // should only be 1
                                }
                                contextData?.ContextWrite($"distinct", $" row count {dtf.Rows.Count} ", qryTxt);
                                queryResultTables.Add(dtf);
                            }
                                break;

                            case "DateTimeOffset": {
                                IEnumerable<DateTimeOffset> distinctVals = (from row in queryResultTables[tableNum].AsEnumerable() select row.Field<DateTimeOffset>(qryTxt)).Distinct();
                                foreach (DateTimeOffset distinctVal in distinctVals) {
                                    DateTimeOffset[] vals = { distinctVal };
                                    dtf.Rows.Add(vals[0]);
                                    //dtf.Rows.Add(vals[0].ToString(ContextData.StandardTimeFormat));
                                }
                                contextData?.ContextWrite($"distinct", $" row count {dtf.Rows.Count} ", qryTxt);
                                queryResultTables.Add(dtf);
                            }
                                break;
                            default: {
                                IEnumerable<string> distinctVals = (from row in queryResultTables[tableNum].AsEnumerable() select row.Field<string>(qryTxt)).Distinct();
                                foreach (string distinctVal in distinctVals) {
                                    string[] vals = { distinctVal };
                                    dtf.Rows.Add(vals[0]);
                                }
                                contextData?.ContextWrite($"distinct", $" row count {dtf.Rows.Count} ", qryTxt);
                                queryResultTables.Add(dtf);
                            }
                                break;
                        }
                    }
                    else {
                        DataTable dtf = null;
                        bool tryQuery = true;
                        if (tryQuery) {
                            //string colsString = "";
                            //for (int i = 0; i < cols.Length; i++) {
                            //    colsString += $"{cols[i]},";
                            //}
                            //colsString = colsString.Trim(',');
                            //DataRow [] resRows = queryResultTables[tableNum].Select($" distinct {colsString} from {queryResultTables[tableNum].TableName}");
                            //dtf = new DataTable();
                            //foreach (DataRow dataRow in resRows) {
                            //    dtf.ImportRow(dataRow);
                            //}

                            RowEqualityComparer rcomp = new RowEqualityComparer(cols);
                            List<DataRow> lrows = queryResultTables[tableNum].AsEnumerable().Distinct(rcomp).ToList();

                            dtf = new DataTable();
                            foreach (string colName in cols) {
                                dtf.Columns.Add(new DataColumn(colName, typeof(string)));
                            }
                            foreach (DataRow dataRow in lrows) {
                                dtf.ImportRow(dataRow);
                            }
                            queryResultTables.Add(dtf);
                        }
                        else {
                            dtf = queryResultTables[tableNum].DefaultView.ToTable(true, cols);
                            queryResultTables.Add(dtf);
                        }
                        contextData?.ContextWrite($"distinct", $" row count {dtf.Rows.Count} ", qryTxt);
                    }
                }
            }
            catch (Exception exc) {
                contextData?.ContextWrite("error_distinctproc", string.Join(";", ppars), exc.Message);
            }

            contextData?.ContextWrite($"distinct_proc", "stop");
        }


        private static void QryStripProcessing(string resultsFilteredFileNameRoot, ProcessingArgs pa, string[] ppars, List<DataTable> queryResultTables, int p, ContextData contextData = null) {
            int tableNum = 0;
            contextData?.ContextWrite($"strip_proc", "start", string.Join(";", ppars));
            try {
                int.TryParse(ppars[1], out tableNum);
                if ((tableNum >= 0) && (tableNum < queryResultTables.Count) && (queryResultTables[tableNum] != null)) {
                    contextData?.ContextWrite("strip", "startColumns", queryResultTables[tableNum].Columns.Count.ToString());
                    string[] selectedColumns = ppars[2].Trim().Split(',');
                    foreach (string selectedColumn in selectedColumns) {
                        string removeColumn = selectedColumn.Trim();
                        if (!string.IsNullOrEmpty(removeColumn)) {
                            queryResultTables[tableNum].Columns.Remove(removeColumn);
                        }
                    }
                    contextData?.ContextWrite("strip", "endColumns", queryResultTables[tableNum].Columns.Count.ToString());
                }
            }
            catch (Exception exc) {
                contextData?.ContextWrite("error_stripproc", string.Join(";", ppars), exc.Message);
            }
            contextData?.ContextWrite($"strip_proc", "end");
        }

        /// <summary>
        /// yank rows from one table selected by a common column in a second table to form a third table
        /// yank tab0 col0 tab1 col1
        /// </summary>
        /// <param name="resultsFilteredFileNameRoot"></param>
        /// <param name="pa"></param>
        /// <param name="ppars"></param>
        /// <param name="queryResultTables"></param>
        /// <param name="p"></param>
        /// <param name="contextData"></param>
        private static void QryYankProcessing(string resultsFilteredFileNameRoot, ProcessingArgs pa, string[] ppars, List<DataTable> queryResultTables, int p, ContextData contextData = null) {
            int tableNum0 = int.Parse(ppars[1]);
            int tableNum1 = int.Parse(ppars[3]);
            string colTab0 = ppars[2];
            string colTab1 = ppars[4];
            bool invert = (ppars.Length > 5) && ppars[5].Equals("invert");
            contextData?.ContextWrite($"yank_proc", "start", string.Join(";", ppars));
            DataTable newTab = new DataTable("yankdes");
            if (invert) {
                foreach (DataColumn col in queryResultTables[tableNum1].Columns) {
                    newTab.Columns.Add(col.ColumnName, col.DataType);
                }
            }
            else {
                foreach (DataColumn col in queryResultTables[tableNum0].Columns) {
                    newTab.Columns.Add(col.ColumnName, col.DataType);
                }
            }
            try {
                bool forceDtConvert = false;
                foreach (DataRow row1 in queryResultTables[tableNum1].Rows) {
                    DataRow[] resRows = null;
                    string qqal = $"'{row1[colTab1]}'";
                    if (!forceDtConvert) {
                        try {
                            resRows = queryResultTables[tableNum0].Select($"{colTab0}={qqal}");
                        }
                        catch (Exception exc) {
                            forceDtConvert = true;  // 
                        }
                    }
                    if (forceDtConvert) {  // crude handle timedateoffsett for now.
                        qqal = ((DateTimeOffset)row1[colTab1]).ToString(ContextData.StandardTimeFormat);
                        qqal = $"#{qqal}#";
                        resRows = queryResultTables[tableNum0].Select($"convert({colTab0},System.String)={qqal}");

                    }
                    if (invert) {
                        if (resRows.Length == 0) {
                            newTab.Rows.Add(row1.ItemArray);
                        }
                    }
                    else {
                        foreach (DataRow dr in resRows) {
                            newTab.Rows.Add(dr.ItemArray);
                        }
                    }
                }
                queryResultTables.Add(newTab);
                    contextData?.ContextWrite("yank_proc", $"yankrows={newTab.Rows.Count}");
            }
            catch (Exception exc) {
                contextData?.ContextWrite("error_yank_proc", string.Join(";", ppars), $"{exc.Message},{exc.InnerException?.Message}");
            }
            contextData?.ContextWrite($"yank_proc", "end");
        }


        private static void QryDiffProcessing(string resultsFilteredFileNameRoot, ProcessingArgs pa, string[] ppars, List<DataTable> queryResultTables, int p, ContextData contextData = null) {
            contextData?.ContextWrite($"diff_proc", "start", string.Join(";", ppars));
            try {
                int table0 = 0;
                if (!int.TryParse(ppars[1], out table0)) {
                    contextData?.ContextWrite("diff_result", "bad table 0", string.Join(";", ppars));
                    return;
                }
                int table1 = 1;
                if (!int.TryParse(ppars[2], out table1)) {
                    contextData?.ContextWrite("diff_result", "bad table 1", string.Join(";", ppars));
                    return;
                }
                if ((table0 >= queryResultTables.Count) || (table1 >= queryResultTables.Count)) {
                    contextData?.ContextWrite("diff_result", $"bad index -- only {queryResultTables.Count}");
                    return;
                }

                string[] selectedColumns = ppars[3].Split(',');

                DataTable dt1s = new DataView(queryResultTables[table0]).ToTable(false, selectedColumns);
                DataTable dt2s = new DataView(queryResultTables[table1]).ToTable(false, selectedColumns);
                dt2s.TableName += "2";

                DataTable dt = DataUtils1.GetDifferentRecords(dt1s, dt2s);

                if (dt.Rows.Count > 0) {
                    int colCount = dt.Columns.Count;
                    if (!string.IsNullOrEmpty(resultsFilteredFileNameRoot)) {
                        string fileName = $"{resultsFilteredFileNameRoot}_P{p}_{ppars[0]}.xml";
                        pa[$"p{p}filename"] = fileName;
                        if (contextData != null) {
                            contextData.ContextWrite("diff_data", ppars.Aggregate((current, next) => current + "," + next));
                            contextData.ContextWrite("diff_file", fileName);
                            string webPath = pa["outputWeb"] + Path.GetFileName(fileName);
                            contextData.ContextWrite("diff_web", webPath);

                            if (pa["contextRecs"] != null) {
                                int tryLines;
                                int.TryParse(pa["contextRecs"], out tryLines);
                                for (int i = 0; i < tryLines && i < dt.Rows.Count; i++) {
                                    StringBuilder sb = new StringBuilder();
                                    for (int c = 0; c < colCount; c++) {
                                        sb.Append(dt.Rows[i][c]);
                                        if (c < colCount - 1) sb.Append(",");
                                    }
                                    contextData.ContextWrite(sb.ToString());
                                }
                            }
                        }
                        using (StreamWriter sw = new StreamWriter(fileName)) {
                            dt.WriteXml(sw, true);
                            sw.Close();
                        }
                    }
                }
                else {
                    contextData?.ContextWrite("diff_result", "no differences");
                }
            }
            catch (Exception exc) {
                contextData?.ContextWrite("error_stripproc", string.Join(";", ppars), exc.Message);
            }
            contextData?.ContextWrite("diff_proc", "end", string.Join(";", ppars));
        }


        /// <summary>
        /// Expect to find a series of sequentially numbered items in a data source
        /// </summary>
        /// <param name="resultsFilteredFileNameRoot"></param>
        /// <param name="pa"></param>
        /// <param name="ppars"></param>
        /// <param name="queryResultTables"></param>
        /// <param name="p"></param>
        /// <param name="contextData"></param>
        private static void QrySeqProcessing(string resultsFilteredFileNameRoot, ProcessingArgs pa, string[] ppars, List<DataTable> queryResultTables, int p, ContextData contextData = null) {
            contextData?.ContextWrite($"seq_proc", "start", string.Join(";", ppars));
            try {
                int table2Use = 0;
                int.TryParse(ppars[1], out table2Use);
                if (table2Use >= queryResultTables.Count) return;
                DataTable dt = new DataView(queryResultTables[table2Use]).ToTable(false, ppars[2]);
                int start = 0;
                int end = 100;
                int count = 1;
                int.TryParse(ppars[4], out start);
                int.TryParse(ppars[5], out end);
                if (ppars.Length > 6) {
                    int.TryParse(ppars[6], out count);
                }
                string expectMatch = "min";
                if (ppars.Length > 7) {
                    expectMatch = ppars[7];
                }

                if (start > end) return;
                if (string.IsNullOrEmpty(resultsFilteredFileNameRoot)) return;
                string fileName = $"{resultsFilteredFileNameRoot}_P{p}_{ppars[0]}.txt";
                pa[$"p{p}filename"] = fileName;
                int ctxLines = 0;
                if (contextData != null) {
                    contextData.ContextWrite("seq_data", ppars.Aggregate((current, next) => current + "," + next));
                    contextData.ContextWrite("seq_file", fileName);
                    string webPath = pa["outputWeb"] + Path.GetFileName(fileName);
                    contextData.ContextWrite("seq_web", webPath);
                    int tryLines;
                    if (pa["contextRecs"] != null) {
                        if (int.TryParse(pa["contextRecs"], out tryLines)) {
                            ctxLines = tryLines;
                        }
                    }
                }

                using (StreamWriter sw = new StreamWriter(fileName)) {
                    int matchMiss = 0;
                    int matchFound = 0;
                    for (int cval = start; cval < end; cval++) {
                        string qVal = $"{ppars[3]}{cval}";
                        string qry = ppars[2] + " = '" + qVal + "'";
                        DataRow[] queryResultRows = queryResultTables[table2Use].Select(qry);
                        bool success = true; // assume
                        switch (expectMatch) {
                            case "min": {
                                if (queryResultRows.Length < count) {
                                    matchMiss++;
                                    success = false;
                                }
                                else {
                                    matchFound++;
                                }
                            }
                                break;
                            case "max": {
                                if (queryResultRows.Length > count) {
                                    matchMiss++;
                                    success = false;
                                }
                                else {
                                    matchFound++;
                                }
                            }
                                break;
                            case "exact": {
                                if (queryResultRows.Length != count) {
                                    matchMiss++;
                                    success = false;
                                }
                                else {
                                    matchFound++;
                                }
                            }
                                break;
                        }
                        if (!success) {
                            string resLine =
                                $"{qVal}, found {queryResultRows.Length} doesn't match expected {expectMatch} of {count} hits, {qry}";
                            sw.WriteLine(resLine);
                            if (contextData != null && (ctxLines-- >= 0)) {
                                contextData.ContextWrite("seq_res", resLine);
                            }
                        }
                        else {
                            //contextData?.ContextWrite("seq_res", "no issues");
                        }
                    }

                    string missData = $"{matchMiss} total missing wrt {expectMatch} of {count} ";
                    string hitData = $"{matchFound}, total found wrt {expectMatch} of {count} ";
                    sw.WriteLine(missData);
                    sw.WriteLine(hitData);
                    if (contextData != null) {
                        contextData.ContextWrite("miss", missData);
                        contextData.ContextWrite("hits", hitData);
                    }
                }
            }
            catch (Exception exc) {
                contextData?.ContextWrite("error_seqproc", string.Join(";", ppars), exc.Message);
            }
            contextData?.ContextWrite("seq_proc", "end", string.Join(";", ppars));
        }

        private static void QryCsvProcessing(string resultsFilteredFileNameRoot, ProcessingArgs pa, string[] ppars, List<DataTable> queryResultTables, int p, ContextData contextData = null) {
            contextData?.ContextWrite($"csv_proc", "start", string.Join(";", ppars));
            try {
                int table2Use = 0;
                int.TryParse(ppars[1], out table2Use);
                if (table2Use >= queryResultTables.Count) return;
                string colNames = ValidateContexts.ReplaceParameters(pa, ppars[2]);
                string[] selectedColumns = colNames.Trim().Split(',');
                if (selectedColumns.Length == 1 && selectedColumns[0] == "*") {
                    colNames = "";
                    foreach (DataColumn dataColumn in queryResultTables[table2Use].Columns) {
                        colNames += dataColumn.ColumnName + ",";
                    }
                    selectedColumns = colNames.Trim(',').Split(',');
                }
                string addlNameKey = "";
                string parseFmt = "";
                string transFmt = "";
                string transType = "DT";  // datetime transform.
                int transIdx = -1;

                // TODO: clean up the following (messy due to not wanting to break old scripts)
                // [3] might be format (old scripts) or might be alt-tag.
                // if alt tag [4] might be format
                if (ppars.Length > 3) {
                    string[] transSets = ppars[3].Split(';');  // 1 or more sets of translation vars
                    string[] transPars = transSets[0].Split(',');  // start of with only a single variable to translate
                    if (int.TryParse(transPars[0], out transIdx)) {
                        transType = transPars[1]; // OK, real dump here for starters.  assuming 
                        parseFmt = transPars[2]; // OK, real dump here for starters.  assuming 
                        transFmt = transPars[3];
                    }
                    else { // crowbar in alternate tag
                        transIdx = -1;
                        addlNameKey = "_" + ppars[3];
                    }
                }
                if (ppars.Length > 4) {
                    string[] transSets = ppars[4].Split(';');  // 1 or more sets of translation vars
                    string[] transPars = transSets[0].Split(',');  // start of with only a single variable to translate
                    if (int.TryParse(transPars[0], out transIdx)) {
                        transType = transPars[1]; // OK, real dump here for starters.  assuming 
                        parseFmt = transPars[2]; // OK, real dump here for starters.  assuming 
                        transFmt = transPars[3];
                    }
                    else { // crowbar in alternate tag
                        transIdx = -1;
                        addlNameKey = "_" + ppars[3];
                    }
                }

                DataTable dt = new DataView(queryResultTables[table2Use]).ToTable(false, selectedColumns);
                if (dt.Rows.Count > 0) {
                    int colCount = dt.Columns.Count;
                    if (!string.IsNullOrEmpty(resultsFilteredFileNameRoot)) {
                        string fileName = $"{resultsFilteredFileNameRoot}_P{p}_{ppars[0]}{addlNameKey}.csv";
                        pa[$"p{p}filename"] = fileName;
                        if (contextData != null) {
                            contextData.ContextWrite("csv_file", fileName);
                            contextData.ContextWrite("csv_web", pa["outputWeb"] + Path.GetFileName(fileName));
                        }
                        using (StreamWriter sw = new StreamWriter(fileName)) {
                            int tryLines = 0;
                            if (pa["contextRecs"] != null) {
                                if (int.TryParse(pa["contextRecs"], out tryLines)) {
                                }
                            }
                            sw.WriteLine(colNames.Trim(','));
                            contextData?.ContextWrite("csv_data", colNames.Trim(','));
                            for (int i = 0; i < dt.Rows.Count; i++) {
                                StringBuilder sb = new StringBuilder();
                                for (int c = 0; c < colCount; c++) {
                                    if (transIdx < 0) {
                                        // no translation
                                        sb.Append(dt.Rows[i][c]);
                                    }
                                    else {
                                        if (transIdx == c) {
                                            switch (dt.Rows[i][c].GetType().FullName) {
                                                case "System.DateTimeOffset": {
                                                    DateTime dts = ((DateTimeOffset) dt.Rows[i][c]).DateTime;
                                                    sb.Append(dts.ToString(transFmt));
                                                }
                                                    break;
                                                default: {
                                                    DateTime dts = DateTime.Parse((string) dt.Rows[i][c]);
                                                    sb.Append(dts.ToString(transFmt));
                                                }
                                                    break;
                                            }
                                        }
                                        else {
                                            sb.Append(dt.Rows[i][c]);
                                        }
                                    }
                                    if (c < colCount - 1) sb.Append(",");
                                }
                                string row = sb.ToString();
                                sw.WriteLine(row);
                                if (contextData != null && (tryLines-- >= 0)) {
                                    contextData.ContextWrite(row);
                                }
                            }
                            sw.Close();
                        }
                    }
                }
            }
            catch (Exception exc) {
                contextData?.ContextWrite("error_csvproc", string.Join(";", ppars), exc.Message);
            }
            contextData?.ContextWrite("csv_proc", "end", string.Join(";", ppars));
        }

#endregion

    }

    class RowEqualityComparer : IEqualityComparer<DataRow> {


        public bool Equals(DataRow b1, DataRow b2) {
            if (b2 == null && b1 == null)
                return true;
            if (b1 == null | b2 == null)
                return false;
            if (CheckEquals(b1,b2))
                return true;
            return false;
        }

        bool CheckEquals(DataRow r1, DataRow r2) {
            foreach (string useCol in useCols) {
                if (r1[useCol] == null && r2[useCol] == null)
                    return true;
                if (r1[useCol] == null | r2[useCol] == null)
                    return false;
                if (!r1[useCol].Equals(r2[useCol])) return false;
            }
            return true;
        }

        private string[] useCols = {"ClientId", "MeterId"};
        public RowEqualityComparer(string[] cols) {
            useCols = cols;
        }

        public int GetHashCode(DataRow bx) {
            string cstg = "";
            foreach (string useCol in useCols) {
                cstg += bx[useCol].ToString();
            }
            return cstg.GetHashCode();
        }


        public bool EqualsOld(DataRow b1, DataRow b2) {
            if (b2 == null && b1 == null)
                return true;
            else if (b1 == null | b2 == null)
                return false;
            else if (b1["ClientId"].Equals(b2["ClientId"]) && b1["MeterId"].Equals(b2["MeterId"]))
                return true;
            else
                return false;
        }

        public int GetHashCodeOld(DataRow bx) {
            string cstg = (string)bx["ClientId"] + (string)bx["MeterId"];
            return cstg.GetHashCode();
        }
    }
}