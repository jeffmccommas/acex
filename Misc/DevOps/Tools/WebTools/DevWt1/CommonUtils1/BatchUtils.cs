﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace AO.TimeSeriesProcessing {


    public class BatchUtils {



        /// <summary>
        /// do validation analysis (clean this up -- extracted method, so put vals in pa)
        /// </summary>
        /// <param name="contextData"></param>
        /// <param name="pa"></param>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public static void AnalysisBatch(ContextData contextData, ProcessingArgs pa, HttpRequestMessage request, HttpResponseMessage response) {
            if (request == null) request = new HttpRequestMessage();
            if (response == null) response = new HttpResponseMessage();

            contextData.ContextWrite("AnalysisBatch", "begin");
            List<DataTable> queryResultTables = new List<DataTable> { };
            contextData.ContextWrite("AnalysisBatch", "1");

            List<string> batchQueries = new List<string>();

            List<string> autoFixes = new List<string>();
            List<string> noFixes = new List<string>();

            contextData.ContextWrite("AnalysisBatch", "2");
            CassConn1 cc1 = new CassConn1();
            cc1.Connect(pa);
            contextData.ContextWrite("AnalysisBatch", "3");

            string _myEtag = (!string.IsNullOrEmpty(pa["myEtag"])) ? pa["myEtag"] : "";


            if (!string.IsNullOrEmpty(pa["procResp"]) && (pa["procResp"].ToLower() != "0")) { // process data and write
                contextData.ContextWrite("webPath", pa["outputWeb"]);
                contextData.ContextWrite("etag", _myEtag);
                contextData.ContextWrite("RequestUri", request?.RequestUri?.ToString());
                contextData.ContextWrite("RequestHeaders", request?.Headers);
                contextData.ContextWriteBulk("RequestContent", pa["reqContent"]);
            }
            int expectRows = 1;
            if (pa["expectRows"] != null) int.TryParse(pa["expectRows"], out expectRows);
            string referenceSourceFile = @"c:\_Test\AclValidateSrc.csv";
            string importContent = pa.GetControlVal("dataContent", "");
            try {
                if (string.IsNullOrEmpty(importContent)) {
                    if (!String.IsNullOrEmpty(pa[$"fiOper0"]?.Trim())) {
                        contextData.ContextWrite("AnalysisBatch", "4");
                        referenceSourceFile = pa["fiOper0"].Trim();
                        FileAttributes attr = File.GetAttributes(referenceSourceFile);
                        if (attr.HasFlag(FileAttributes.Directory)) {
                            // directory == get list of CSV files
                            if (ConsolidateFiles(contextData, ref referenceSourceFile, pa)) return;
                        }
                    }
                }
            }
            catch (Exception exc) {
                contextData.ContextWrite("AnalysisBatch", $"Exception i {exc.Message}, {exc.InnerException?.Message}");
            }
            string matchType = "exact";
            if (!String.IsNullOrEmpty(pa["matchType"]?.Trim())) {
                matchType = pa["matchType"].Trim();
            }
            contextData.ContextWrite("AnalysisBatch", "5");
            DateTime dtStart = DateTime.Now;
            contextData?.ContextWrite("analSource", $"using {referenceSourceFile} rows ");
            contextData?.ContextWrite("analStart", $"expecting {expectRows} rows ", dtStart.ToString());
            int matchMiss = 0;
            int matchFound = 0;
            int readRows = 0;
            int missOutputLimit = 50;
            int missAbortLimit = 50000;

            int ctxLines = 25;
            int ctxLinesMax = ctxLines;
            int tryLines;
            if (pa["contextRecs"] != null) {
                if (int.TryParse(pa["contextRecs"], out tryLines)) {
                    ctxLines = tryLines;
                    ctxLinesMax = ctxLines;
                }
            }

            int batchTimeout = GetConfigInt(pa, "batchTimeout", 100);
            int batchSleepMs = GetConfigInt(pa, "batchSleepMs", 10);
            int procLimit = GetConfigInt(pa, "procLimit", 200);
            int batchLimit = GetConfigInt(pa, "batchLimit", 200);
            int batchDataLimit = GetConfigInt(pa, "batchDataLimit", 4600);
            int skipRefRows = GetConfigInt(pa, "skipRefRows", 0);
            int taskListMax = GetConfigInt(pa, "taskListMax", 30);
            bool doAsyncBatch = GetConfigInt(pa, "doAsyncBatch", 1) == 1;

            contextData.NoFixes.AddLine(pa.AsJson());



            if (pa["sqlMeterMgtCxnStg"] == null) {
                pa["sqlMeterMgtCxnStg"] = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
            }

            StreamReader referenceStream = null;
            try {
                //contextData.ContextWrite("AnalysisBatch", pa.AsJson());

                if ((pa["qSel0"] != null) && (pa["qSel0"].Trim() != "*")) {
                    string csvNames = "nada";
                    var tasks = new List<Task>();
                    if (!string.IsNullOrEmpty(importContent)) {
                        contextData.ContextWrite("AnalysisBatch", "importContent");
                        referenceStream = new StreamReader(StreamUtils.ToStream(importContent));
                    }
                    else {
                        contextData.ContextWrite("AnalysisBatch", referenceSourceFile);
                        referenceStream = new StreamReader(referenceSourceFile);
                    }
                    {
                        bool hasHeaders = pa.GetControlVal("hasheaders", 1) == 1;

                        // used to be using
                        if (hasHeaders) {
                            csvNames = referenceStream.ReadLine();
                        }
                        int maxRows = int.Parse(pa["maxrows"]);
                        string qryFmt = pa["qSel0"];
                        if (string.IsNullOrEmpty(qryFmt)) {
                            qryFmt = pa["cassquery"];
                        }
                        if (!string.IsNullOrEmpty(qryFmt)) {

                            if (contextData != null) {
                                contextData.ContextWrite("val_file", contextData.Validation.FileName);
                                contextData.ContextWrite("val_web", pa["outputWeb"] + Path.GetFileName(contextData.Validation.FileName));
                            }
                            int batchDataLength = 0;
                            while (!referenceStream.EndOfStream && (procLimit-- > 0)) {
                                // maybe skipping some lines (e.g., alternating in some of the 224 subscription files)
                                if (skipRefRows > 0) {
                                    for (int skip = 0; skip < skipRefRows && !referenceStream.EndOfStream; skip++) {
                                        var discard = referenceStream.ReadLine();
                                    }
                                }
                                if (referenceStream.EndOfStream) break;
                                var csvVals = referenceStream.ReadLine();
                                readRows++;
                                contextData.IncrementCounter("RefReadRows");
                                if (string.IsNullOrEmpty(csvVals)) {
                                    continue;
                                }
                                ValidateContexts ctxs = ValidateContexts.GetQuerySet(pa, qryFmt, csvVals);
                                foreach (ValidateContext ctx in ctxs) {
                                    pa["qSel0"] = ctx.QryString;
                                    pa["qRows"] = "*"; // at some point be selective
                                    string cassandraQuery = cc1.GetCasQuery(pa);
                                    batchQueries.Add(cassandraQuery);
                                    contextData.Validation.AddLine(cassandraQuery);
                                    batchDataLength += cassandraQuery.Length;
                                }
                                if ((batchQueries.Count > batchLimit) || (batchDataLength >= batchDataLimit)) {
                                    if (doAsyncBatch) {
                                        tasks.Add(cc1.PerformCasQueryBatchAsynch(pa, batchQueries));
                                        if (tasks.Count > taskListMax) {
                                            int waitLimit = (batchTimeout*1000)/batchSleepMs;
                                            Task tAll = Task.WhenAll(tasks);
                                            while (!tAll.IsCompleted && (waitLimit-- > 0)) {
                                                Thread.Sleep(10);
                                            }
                                            if (!tAll.IsCompleted && (waitLimit < 0)) {
                                                // whoops -- batch not completed
                                                contextData.ContextWrite(" batch timeout", $">{batchTimeout}secs for {tasks.Count}");
                                            }
                                            tasks.Clear();
                                        }
                                    }
                                    else {
                                        cc1.PerformCasQueryBatch(pa, batchQueries);
                                    }
                                    contextData.IncrementCounter("dobatch");
                                    batchQueries.Clear();
                                    batchDataLength = 0;
                                }
                            }
                            if (doAsyncBatch) {
                                if (tasks.Count > 0) {
                                    int waitLimit = (batchTimeout*1000)/batchSleepMs;
                                    Task tAll = Task.WhenAll(tasks);
                                    while (!tAll.IsCompleted && (waitLimit-- > 0)) {
                                        Thread.Sleep(10);
                                    }
                                    if (!tAll.IsCompleted && (waitLimit < 0)) {
                                        // whoops -- batch not completed
                                        contextData.ContextWrite("finish batch timeout", $">{batchTimeout}secs for {tasks.Count}");
                                    }
                                    tasks.Clear();
                                }
                            }

                            if (batchQueries.Count > 0) {
                                cc1.PerformCasQueryBatch(pa, batchQueries);
                                batchQueries.Clear();
                                batchDataLength = 0;
                                contextData.IncrementCounter("dobatch");
                            }

                            pa["X-BIO-etag"] = _myEtag;
                            DateTime dtNow = DateTime.Now;
                            contextData?.ContextWrite("analComplete",
                                $" at {dtNow}, delta = {dtNow.Subtract(dtStart).TotalMilliseconds} ms.");
                            if (contextData != null) {
                                contextData.ContextWrite("miss", $"{matchMiss} total not having {pa["matchType"]} count of {expectRows} ");
                                contextData.ContextWrite("hits", $"{matchFound} total     having {pa["matchType"]} count of {expectRows} ");
                                contextData.ContextWrite("read", $"{readRows} rows read");
                                if (autoFixes.Count > 0) {
                                    DateTime dateTime = DateTime.Now;
                                    contextData.ContextWriteSeg("AutoFix", true, dateTime);
                                    string fixFileName = pa["fixFileName"];
                                    string metaString = "";
                                    if (!String.IsNullOrEmpty(pa["useFixMeta"])) {
                                        string[] metaStrings = pa["useFixMeta"].Trim().Split(',');
                                        foreach (string stg in metaStrings) {
                                            metaString += stg + ",";
                                        }
                                    }
                                    //using (StreamWriter fixOutput = new StreamWriter(fixFileName)) {
                                    ctxLines = ctxLinesMax;
                                    // fixOutput.WriteLine(metaString + csvNames);
                                    contextData.AutoFixes.AddLine(metaString + csvNames);
                                    foreach (string autoFix in autoFixes.Distinct()) {
                                        if (ctxLines-- > 0) contextData.ContextWrite(autoFix);
                                        contextData.AutoFixes.AddLine(autoFix);
                                        //fixOutput.WriteLine(autoFix);
                                        contextData.IncrementCounter("autoFixDistinct");
                                    }
                                    //}
                                    contextData.ContextWriteSeg("AutoFix", false, dateTime);
                                    contextData.ContextWrite("fix_file", fixFileName);
                                    contextData.ContextWrite("fix_web", pa["outputWeb"] + Path.GetFileName(fixFileName));
                                }
                                if (noFixes.Count > 0) {
                                    DateTime dateTime = DateTime.Now;
                                    contextData.ContextWriteSeg("NoFix", true, dateTime);
                                    string noFixFileName = pa["noFixFileName"];
                                    string metaString = "";
                                    if (!string.IsNullOrEmpty(pa["useFixMeta"])) {
                                        string[] metaStrings = pa["useFixMeta"].Trim().Split(',');
                                        foreach (string stg in metaStrings) {
                                            metaString += stg + ",";
                                        }
                                    }
                                    //using (StreamWriter fixOutput = new StreamWriter(noFixFileName)) {
                                    ctxLines = ctxLinesMax;
                                    //fixOutput.WriteLine(metaString + csvNames);
                                    contextData.AutoFixes.AddLine(metaString + csvNames);
                                    foreach (string autoFix in noFixes.Distinct()) {
                                        if (ctxLines-- > 0) contextData.ContextWrite(autoFix);
                                        contextData.AutoFixes.AddLine(autoFix);
                                        //fixOutput.WriteLine(autoFix);
                                        contextData.IncrementCounter("noFixDistinct");
                                    }
                                    //}
                                    contextData.ContextWriteSeg("NoFix", false, dateTime);
                                    //contextData.ContextWrite("nofix_file", noFixFileName);
                                    //contextData.ContextWrite("nofix_web", pa["outputWeb"] + Path.GetFileName(noFixFileName));
                                }

                            }
                        }
                    }
                }
                else {
                    contextData.ContextWrite("AnalysisBatch", "nothing to do");
                }

                if (!String.IsNullOrEmpty(pa["procResp"]) && (pa["procResp"].ToLower() != "0")) {
                    // process data and write
                    DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, pa["resultsRawFileName"] + "_", contextData);
                    contextData.ContextWrite("ResponseHeaders", response?.Headers);
                    string contextReturn = pa["contextRet"] ?? "0";
                    if (contextReturn.Equals("1") && response != null) {
                        response.Content = new StringContent(contextData.Snag());
                    }
                }
                else {
                    // process data no write (maybe merge with above, but this is simpler)
                    DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, null, null);
                }
            }
            catch (Exception exc) {
                contextData.ContextWrite("AnalysisBatch", $"Exception o {exc.Message}, {exc.InnerException?.Message}");
                NameValueCollection nv = new NameValueCollection();
                nv.Add("n", "v");
                XmlDocument doc = XmlDocUtils.GetXMLExceptionDocument("putAnalysis", nv, exc);
                DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, null, null);
                contextData.ContextWrite("AnalysisBatch", doc.OuterXml);
                if (response != null) {
                    response.Content = new StringContent(doc.OuterXml);
                }
            }
            finally {
                contextData.ContextWrite("AnalysisBatch", "complete");
                if (referenceStream != null) {
                    referenceStream.Close();
                }
            }
            contextData.ContextWrite("AnalysisBatch", "end");
        }

        /// <summary>
        /// get config val.  default, overriden by config, overriden by pa
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="parName"></param>
        /// <param name="defVal"></param>
        /// <returns></returns>
        private static int GetConfigInt(ProcessingArgs pa, string parName, int defVal) {
            int rVal = defVal;
            int tryLines;
            string tempStg = ConfigurationManager.AppSettings.Get(parName);
            if (!string.IsNullOrEmpty(tempStg)) { }
            if (int.TryParse(tempStg, out tryLines)) {
                rVal = tryLines;
            }
            if (pa[parName] != null) {
                if (int.TryParse(pa[parName], out tryLines)) {
                    rVal = tryLines;
                }
            }
            return rVal;
        }


        public static bool ConsolidateFiles(ContextData contextData, ref string referenceSourceFile, ProcessingArgs pa) {
            IEnumerable<string> files = Directory.EnumerateFiles(referenceSourceFile, "*.csv");
            string[] fileNames = files.ToArray();
            if (fileNames.Length <= 0) return true;
            string teeDestFile = Path.Combine(referenceSourceFile, "CombinedFiles.merged_csv.tee.csv");
            referenceSourceFile = Path.Combine(referenceSourceFile, "CombinedFiles.merged_csv");
            int[] teeCols = null;
            StreamWriter tw = null;  // to tee of csv if asked to do so
            int totalRowsCount = 0;
            int totalFilesCount = 0;
            try {
                if (pa != null) {
                    string teeColsStg = pa.GetControlVal("teeCols", "");
                    if (!string.IsNullOrEmpty(teeColsStg)) {
                        string[] teeColStgs = teeColsStg.Split(',');
                        teeCols = new int[teeColStgs.Length];
                        for (int i = 0; i < teeColStgs.Length; i++) {
                            teeCols[i] = int.Parse(teeColStgs[i]);
                        }
                        tw = new StreamWriter(teeDestFile);
                    }
                }
                try {
                    using (StreamWriter sw = new StreamWriter(referenceSourceFile)) {
                        using (StreamReader sr = new StreamReader(fileNames[0])) {
                            // get header line from first file.
                            string headerData = sr.ReadLine();
                            sw.WriteLine(headerData);
                            if (tw != null) {
                                string teeHdrsStg = pa.GetControlVal("teeHdrs", "");
                                string[] inCols = (!string.IsNullOrEmpty(teeHdrsStg))? teeHdrsStg.Split(','):headerData.Split(',');
                                string teeStg = "";
                                for (int j = 0; j < teeCols.Length; j++) {
                                    teeStg += inCols[j] + ",";
                                }
                                teeStg = teeStg.Trim(',');
                                tw.WriteLine(teeStg);

                            }
                            contextData.ContextWrite("consolidate write", $" file '{fileNames[0]}',header='{headerData}'");
                            sr.Close();
                        }

                        for (int i = 0; i < fileNames.Length; i++) {
                            using (StreamReader sr = new StreamReader(fileNames[i])) {
                                string headerData = sr.ReadLine(); // eat names
                                int rowsCount = 0;
                                while (!sr.EndOfStream) {
                                    string input = sr.ReadLine();
                                    if (!String.IsNullOrEmpty(input)) {
                                        if (input.Equals(headerData)) {
                                            contextData.ContextWrite("consolidate discard", $" file '{fileNames[i]}', '{input}' ");
                                        }
                                        else {
                                            sw.WriteLine(input);
                                            if (teeCols != null) {
                                                string[] inCols = input.Split(',');
                                                string teeStg = "";
                                                for (int j = 0; j < teeCols.Length; j++) {
                                                    teeStg += inCols[teeCols[j]] + ",";
                                                }
                                                teeStg = teeStg.Trim(',');
                                                tw.WriteLine(teeStg);
                                            }
                                            if (rowsCount == 0)
                                                contextData.ContextWrite("consolidate write",
                                                    $" file '{fileNames[i]}', total {totalRowsCount} rows {input} ");
                                            rowsCount++;
                                            contextData.IncrementCounter($"{fileNames[i]}.rows");
                                        }
                                    }
                                    else {
                                        contextData.IncrementCounter($"{fileNames[i]}.ignores");
                                    }
                                }
                                totalRowsCount += rowsCount;
                                contextData.ContextWrite("consolidate", $" file '{fileNames[i]}', {rowsCount} rows ");
                                sr.Close();
                            }
                            totalFilesCount++;
                        }
                    }
                }
                catch (Exception exc) {
                    contextData.ContextWrite("error ", $" ConsolidateFiles processing {exc.Message} , {exc.InnerException?.Message} ");
                }
                finally {
                    tw?.Close();
                }
            }
            catch (Exception exc) {
                contextData.ContextWrite("error ", $" ConsolidateFiles setup {exc.Message} , {exc.InnerException?.Message} ");
            }
            contextData.ContextWrite("consolidated", $" {totalFilesCount} files, {totalRowsCount} rows ");
            return false;
        }

    }
}
