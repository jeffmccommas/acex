﻿using System.Data;
using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace AO.TimeSeriesProcessing {
    public interface IDataSpecification {
        object Starter(ProcessingArgs pa, object source);
        void Generate(ProcessingArgs pa);
        void GenerateData(ProcessingArgs pa);
        void AddMultiple(ProcessingArgs pa,DataTable dt, SqlConnection con, int quantity);
        IDataSpecification AddSingle(ProcessingArgs pa, IDataSpecification obj, long key, SqlCommand cmd);
        DataTable GetSome(ProcessingArgs pa, SqlConnection cxn);
        string Show();
        long Id { get; set; }
        string Specs { get; set; }
        string Statistics(ProcessingArgs pa);


    }


    public interface ISeriesCollectionSpecification : IDataSpecification {
        string CollectionId { get; set; }
    }


    public interface ISeriespecification : IDataSpecification {
        string DidSpec { get; set; }
        string TsSpec { get; set; }
    }




}
