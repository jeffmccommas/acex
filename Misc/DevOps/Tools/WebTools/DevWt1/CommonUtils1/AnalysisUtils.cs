﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Xml;
using Cassandra;
using Newtonsoft.Json;

// ReSharper disable once CheckNamespace
namespace AO.TimeSeriesProcessing {


    public class AnalysisUtils {
        /// <summary>
        /// do validation analysis 
        /// </summary>
        /// <param name="contextData"></param>
        /// <param name="pa"></param>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public static void AnalysisValidate(ContextData contextData, ProcessingArgs pa, HttpRequestMessage request, HttpResponseMessage response) {
            //string qryTemplate = "where client_id={0} and type in ('A','S') and customer_id = '{1}' and account_id = '{2}' ";
            List<DataTable> queryResultTables = new List<DataTable> { };
            DataUtils1.LoadQueryData(contextData, pa, queryResultTables, false);
            CassConn1 cc1 = new CassConn1();
            cc1.Connect(pa);

            string _myEtag = (!string.IsNullOrEmpty(pa["myEtag"])) ? pa["myEtag"] : "";

            if (!string.IsNullOrEmpty(pa["procResp"]) && (pa["procResp"].ToLower() != "0")) { // process data and write
                contextData.ContextWrite("webPath", pa["outputWeb"]);
                contextData.ContextWrite("etag", _myEtag);
                contextData.ContextWrite("RequestUri", request.RequestUri.ToString());
                contextData.ContextWrite("RequestHeaders", request.Headers);
                contextData.ContextWriteBulk("RequestContent", pa["reqContent"]);
            }
            int expectRows = 1;
            if (pa["expectRows"] != null) int.TryParse(pa["expectRows"], out expectRows);
            string referenceSourceFile = @"c:\_Test\AclValidateSrc.csv";
            if (!string.IsNullOrEmpty(pa[$"fiOper0"]?.Trim())) {
                referenceSourceFile = pa["fiOper0"].Trim();
                FileAttributes attr = File.GetAttributes(referenceSourceFile);
                if (attr.HasFlag(FileAttributes.Directory)) {  // directory == get list of CSV files
                    if (BatchUtils.ConsolidateFiles(contextData, ref referenceSourceFile, pa)) return;
                }
            }
            string matchType = "exact";
            if (!string.IsNullOrEmpty(pa["matchType"]?.Trim())) {
                matchType = pa["matchType"].Trim();
            }

            DateTime dtStart = DateTime.Now;
            contextData?.ContextWrite("analSource", $"using {referenceSourceFile} rows ");
            contextData?.ContextWrite("analStart", $"expecting {expectRows} rows ", dtStart.ToString());
            int matchMiss = 0;
            int matchFound = 0;
            int readRows = 0;
            int missOutputLimit = 50;
            int missAbortLimit = 50000;

            int ctxLines = 25;
            int ctxLinesMax = ctxLines;
            int tryLines;
            if (pa["contextRecs"] != null) {
                if (int.TryParse(pa["contextRecs"], out tryLines)) {
                    ctxLines = tryLines;
                    ctxLinesMax = ctxLines;
                }
            }
            int procLimit = 100;
            if (pa["procLimit"] != null) {
                if (int.TryParse(pa["procLimit"], out tryLines)) {
                    procLimit = tryLines;
                }
            }


            int inputLimit = procLimit;
            if (pa["inputLimit"] != null) {
                if (int.TryParse(pa["inputLimit"], out tryLines)) {
                    inputLimit = tryLines;
                }
            }



            int skipRefRows = 0;
            if (pa["skipRows"] != null) {
                if (int.TryParse(pa["skipRows"], out tryLines)) {
                    skipRefRows = tryLines;
                }
            }

            if (pa["sqlMeterMgtCxnStg"] == null) {
                pa["sqlMeterMgtCxnStg"] = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
            }

            if (pa["dodbtest"] != null) {

                string sqlMeterMgt = ConfigurationManager.AppSettings.Get("sqlMeterMgt");

                using (SqlConnection con = new SqlConnection(sqlMeterMgt)) {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("SELECT TOP 2 * FROM ZTestTable1", con)) {
                        using (SqlDataReader reader = command.ExecuteReader()) {
                            while (reader.Read()) {
                                Console.WriteLine("{0} {1} {2}",
                                    reader.GetInt32(0), reader.GetString(1), reader.GetString(2));
                            }
                        }
                    }
                }

                NameValueCollection nv = new NameValueCollection();
                nv.Add("noData", "expected");
                XmlDocument doc = XmlDocUtils.GetXMLExceptionDocument("DataTest", nv);
                DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, null, contextData);
                response.Content = new StringContent(doc.OuterXml);
            }


            try {
                bool startedOutput = false;
                bool storingData = false;
                bool storedOriginalTable = false;
                if (File.Exists(referenceSourceFile)) using (StreamReader referenceStream = new StreamReader(referenceSourceFile)) {
                        string csvNames = referenceStream.ReadLine();
                        contextData.NoFixes.AddLine(csvNames);
                        contextData.AutoFixes.AddLine(csvNames);
                        int maxRows = int.Parse(pa["maxrows"]);
                        string qryFmtSave = pa["qSel0"];  // we need at least one but this is just a minimal proposition.  A different query might be selected to replace it
                        if (!string.IsNullOrEmpty(qryFmtSave)) {

                            if (contextData != null) {
                                contextData.ContextWrite("val_file", contextData.Validation.FileName);
                                contextData.ContextWrite("val_web", pa["outputWeb"] + Path.GetFileName(contextData.Validation.FileName));
                            }
                            DataTable accumulatorTable = new DataTable("getCassdata");

                            if ((pa["qSel0"] != null) && (pa["qSel0"].Trim() != "*")) {
                                while (!referenceStream.EndOfStream && (inputLimit-- > 0)) {
                                    // maybe skipping some lines (e.g., alternating in some of the 224 subscription files)
                                    if (skipRefRows > 0) {
                                        for (int skip = 0; skip < skipRefRows && !referenceStream.EndOfStream; skip++) {
                                            var discard = referenceStream.ReadLine();
                                        }
                                    }
                                    if (referenceStream.EndOfStream) break;
                                    string csvStg = referenceStream.ReadLine();
                                    contextData.IncrementCounter("RefReadRows");
                                    try {
                                        string[] csvVals = csvStg.Split(',');
                                        string qryFmt = qryFmtSave; // reset
                                        if (!string.IsNullOrEmpty(pa["inOper0"])) {
                                            // figure out if we need to replace the base query.
                                            string[] indirectPars = pa["inOper0"].Trim().Split(' ');
                                            if (indirectPars.Length < 3) break; // src is form "1 has col1=aa"  or "1 not col1=,"
                                            string[] compVars = indirectPars[2].Split('=');
                                            string rValue = ""; // assume blank for rValue

                                            string lValue = csvVals[0]; // assume blank for rValue

                                            if (compVars.Length > 1) {
                                                rValue = compVars[1];
                                            }
                                            int tVal = 0;
                                            if (int.TryParse(compVars[0], out tVal)) {
                                                lValue = csvVals[tVal];
                                            }
                                            if (lValue.Equals(rValue)) {
                                                // add additional operators here
                                                if (int.TryParse(indirectPars[0], out tVal)) {
                                                    string qSelector = $"qSel{tVal}";
                                                    if (!string.IsNullOrEmpty(pa[qSelector])) {
                                                        // selected a different query than the default
                                                        qryFmt = pa[qSelector];
                                                    }
                                                }
                                            }
                                        }

                                        ValidateContexts ctxs = ValidateContexts.GetQuerySet(pa, qryFmt, csvStg);
                                        foreach (ValidateContext ctx in ctxs) {
                                            pa["qSel0"] = ctx.QryString;
                                            string getRows = pa.GetControlVal("qRows", "*");
                                            if (getRows.Equals("*"))  pa["qRows"] = "*"; // at some point be selective
                                            string cassandraQuery = "";
                                            RowSet rs = cc1.PerformCasQuery(pa, ref cassandraQuery);

                                            int retRowsCnt = rs.GetAvailableWithoutFetching();
                                            string sqlResults="";
                                            if (pa.GetControlVal("qryFrag", 0) == 1) {
                                                int sqlCount = CassConn1.SqlCount(contextData, pa, rs, 1);
                                                if (sqlCount != retRowsCnt) {
                                                    sqlResults = $"[sql={retRowsCnt}->{sqlCount}]";
                                                }
                                                retRowsCnt = sqlCount;
                                            }

                                            bool fail = false;
                                            switch (matchType) {
                                                case "get": // results being stored.
                                                    storingData = true;
                                                    break;
                                                case "min":
                                                    if (retRowsCnt < expectRows) fail = true;
                                                    break;
                                                case "max":
                                                    if (retRowsCnt > expectRows) fail = true;
                                                    break;
                                                case "exact":
                                                    if (retRowsCnt != expectRows) fail = true;
                                                    break;
                                                default:
                                                    if (retRowsCnt != expectRows) fail = true;
                                                    break;
                                            }
                                            if (!storingData) {
                                                contextData.Validation.AddLine($"{retRowsCnt} rows for : {cassandraQuery}");
                                                if (fail) {
                                                    Row row = null;
                                                    string rowData = "";
                                                    if (retRowsCnt > 0) {
                                                        //rs.FetchMoreResults();
                                                        foreach (Row r in rs) {
                                                            foreach (object val in r) {
                                                                rowData += val?.ToString().Replace(",", "\\,") + ",";
                                                            }
                                                        }
                                                    }
                                                    matchMiss++;
                                                    string missText = $"analMiss {retRowsCnt} rows ({matchType}={expectRows} {sqlResults}) {cassandraQuery}";
                                                    contextData.Validation.AddLine(missText);
                                                    contextData.IncrementCounter("autoFix");
                                                    if (row != null) contextData.Validation.AddLine($"analMisData {rowData}");
                                                    if (!string.IsNullOrEmpty(ctx.AutoFix)) {
                                                        contextData.Validation.AddLine($"autoFix {ctx.AutoFix}");
                                                        contextData.AutoFixes.AddLine(ctx.AutoFix);
                                                    }
                                                    if (matchMiss < missOutputLimit) {
                                                        contextData?.ContextWrite(missText);
                                                        if (!string.IsNullOrEmpty(ctx.AutoFix))
                                                            contextData?.ContextWrite("autoFix", ctx.AutoFix);
                                                        if (row != null) contextData.ContextWrite("analMisData", rowData);
                                                    }
                                                    else if (matchMiss == missOutputLimit) {
                                                        contextData?.ContextWrite("analMiss", $" {matchMiss} = to many misses. Tuncating output");
                                                    }
                                                    else if (matchMiss >= missAbortLimit) {
                                                        contextData?.ContextWrite("analMiss", $" {matchMiss} = to many misses.  Aborting");
                                                        break;
                                                    }
                                                }
                                                else {
                                                    matchFound++;
                                                    contextData.IncrementCounter("noFix");
                                                    contextData.NoFixes.AddLine(ctx.AutoFix);
                                                    if (ctxLines-- > 0) {
                                                        contextData?.ContextWrite("analHit",
                                                            $" {retRowsCnt} rows ({matchType}={expectRows}  {sqlResults}) for : {cassandraQuery}");
                                                    }
                                                }
                                            }
                                            else {
                                                if (retRowsCnt > 0) {
                                                    foreach (Row row in rs) {
                                                        if (!startedOutput) {
                                                            CqlColumn[] cols = rs.Columns;
                                                            string colNames = "";
                                                            foreach (CqlColumn ccol in rs.Columns) {
                                                                accumulatorTable.Columns.Add(ccol.Name, ccol.Type);
                                                                colNames += ccol.Name + ",";
                                                            }
                                                            contextData.Validation.AddLine(colNames.Trim(','));
                                                            startedOutput = true;
                                                        }
                                                        accumulatorTable.Rows.Add(row.ToArray());
                                                        string rowData = "";
                                                        foreach (object val in row) {
                                                            rowData += val?.ToString().Replace(",", "\\,") + ",";
                                                        }
                                                        contextData.Validation.AddLine(rowData.Trim(','));
                                                    }
                                                    if (!storedOriginalTable) {
                                                        queryResultTables.Add(accumulatorTable);
                                                        storedOriginalTable = true;
                                                    }
                                                }
                                            }
                                        }
                                        readRows++;
                                    }
                                    catch (Exception exc) {
                                        contextData.ContextWrite("Exception", $" {exc.Message}, {exc.InnerException?.Message}, with '{csvStg}'");
                                    }
                                }
                            }

                            pa["X-BIO-etag"] = _myEtag;
                            DateTime dtNow = DateTime.Now;
                            contextData?.ContextWrite("analComplete", $" at {dtNow}, delta = {dtNow.Subtract(dtStart).TotalMilliseconds} ms.");
                            if (contextData != null) {
                                if (matchMiss > 0) contextData.ContextWrite("missed", $"{matchMiss} total not having {pa["matchType"]} count of {expectRows} ");
                                contextData.ContextWrite("miss", $"{matchMiss} total not having {pa["matchType"]} count of {expectRows} ");
                                contextData.ContextWrite("hits", $"{matchFound} total     having {pa["matchType"]} count of {expectRows} ");
                                contextData.ContextWrite("read", $"{readRows} rows read");
                            }
                        }
                        else {
                            contextData.ContextWrite("val_noCQuery", " doing nothing");
                        }
                    }

                DataTable _lastResultTable = null;// from older 
                if (!string.IsNullOrEmpty(pa["procResp"]) && (pa["procResp"].ToLower() != "0")) { // process data and write
                    if (_lastResultTable != null) {
                        contextData.ContextWrite("rawData", $" row count {_lastResultTable.Rows.Count}");
                    }
                    DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, pa["resultsRawFileName"] + "_", contextData);
                    contextData.ContextWrite("ResponseHeaders", response.Headers);
                    //string contexFileNamePath = contextData?.End();
                    string contextReturn = pa["contextRet"] ?? "0";
                    if (contextReturn.Equals("1") && response != null) {
                        response.Content = new StringContent(contextData.Snag());
                    }
                }
                else { // process data no write (maybe merge with above, but this is simpler)
                    DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, null, contextData);
                }
            }
            catch (Exception exc) {
                contextData?.ContextWrite("AO.TimeSeriesProcessing.AnalysisUtils.AnalysisValidate exception", $"{exc.Message}, {exc.InnerException?.Message}");
                NameValueCollection nv = new NameValueCollection();
                nv.Add("n", "v");
                XmlDocument doc = XmlDocUtils.GetXMLExceptionDocument("putAnalysis", nv, exc);
                DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, null, contextData);
                response.Content = new StringContent(doc.OuterXml);
            }
        }


        /// <summary>
        /// do basic analysis (clean this up -- extracted method, so put vals in pa)
        /// </summary>
        /// <param name="context"></param>
        /// <param name="pa"></param>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public static void AnalysisBasic(ContextData context, ProcessingArgs pa, HttpRequestMessage request, HttpResponseMessage response) {
            switch (pa.GetControlVal("oper", "nada")) {
                case "recallseries": {
                        try {
                            if (pa["sqlMeterMgtCxnStg"] == null) {
                                pa["sqlMeterMgtCxnStg"] = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
                            }
                            SeriesCollectionSpecification collSpec = new SeriesCollectionSpecification(pa);
                            collSpec.InitializeCache(pa, false, context);
                            SeriesSpecification seriesSpec = new SeriesSpecification(pa, null as DataRow);
                            seriesSpec.InitializeCache(pa);
                            DataTable dt = collSpec.GetSeriesData2(pa, context, pa["collid"], seriesSpec);
                            DataUtils1.PrepareResponseInfo(dt, pa, request, response, null, context);
                            context.ContextWrite("ResponseHeaders", response.Headers);
                            string contextReturn = pa["contextRet"] ?? "0";
                            if (contextReturn.Equals("1") && response != null) {
                                response.Content = new StringContent(context.Snag());
                                Newtonsoft.Json.Formatting fmt = pa.GetControlVal("jsonfmt", "1").Equals("1")
                                    ? Newtonsoft.Json.Formatting.Indented
                                    : Newtonsoft.Json.Formatting.None;
                                string dataOut = JsonConvert.SerializeObject(dt, fmt);
                                context.NoFixes.AddLine(dataOut);
                            }
                            int retRows = dt.Rows.Count;
                            if (retRows > 0) {
                                context.ContextWrite("tseries returned data", $" rows={retRows}");
                                context.Add2Counter("retrows", retRows);
                            }
                            else {
                                context.ContextWrite("warn_tseries", " no returned data");
                            }
                        }
                        catch (Exception exc) {
                            context.ContextWrite("error_tseries", $" exception {exc.Message},{exc.InnerException?.Message} ");
                        }
                    }
                    break;

                case "recallmeters": {
                        try {
                            if (pa["sqlMeterMgtCxnStg"] == null) {
                                pa["sqlMeterMgtCxnStg"] = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
                            }
                            DataTable dt = new SeriesCollectionSpecification(pa).GetSome(pa);
                            DataUtils1.PrepareResponseInfo(dt, pa, request, response, null, context);
                            context.ContextWrite("ResponseHeaders", response.Headers);
                            string contextReturn = pa["contextRet"] ?? "0";
                            if (contextReturn.Equals("1") && response != null) {
                                response.Content = new StringContent(context.Snag());
                                Newtonsoft.Json.Formatting fmt = pa.GetControlVal("jsonfmt", "1").Equals("1")
                                    ? Newtonsoft.Json.Formatting.Indented
                                    : Newtonsoft.Json.Formatting.None;
                                string dataOut = JsonConvert.SerializeObject(dt, fmt);
                                context.NoFixes.AddLine(dataOut);
                            }
                            int retRows = dt.Rows.Count;
                            if (retRows > 0) {
                                context.ContextWrite("tseries returned data", $" rows={retRows}");
                            }
                            else {
                                context.ContextWrite("warn_tseries", " no returned data");
                            }
                        }
                        catch (Exception exc) {
                            context.ContextWrite("error_tseries", $" exception {exc.Message},{exc.InnerException?.Message} ");
                        }
                    }
                    break;

                default: {
                        List<DataTable> queryResultTables = new List<DataTable> { };
                        DataUtils1.LoadQueryData(context, pa, queryResultTables, true);

                        string _myEtag = (!string.IsNullOrEmpty(pa["myEtag"])) ? pa["myEtag"] : "";
                        if (!string.IsNullOrEmpty(pa["procResp"]) && (pa["procResp"].ToLower() != "0")) { // process data and write
                            context.ContextWrite("webPath", pa["outputWeb"]);
                            context.ContextWrite("etag", _myEtag);
                            context.ContextWrite("RequestUri", request.RequestUri.ToString());
                            context.ContextWrite("RequestHeaders", request.Headers);
                            context.ContextWriteBulk("RequestContent", pa["reqContent"]);
                            DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, pa["resultsRawFileName"] + "_", context);
                            for (int i = 0; i < queryResultTables.Count; i++) {
                                if (queryResultTables[i] != null) context.ContextWrite("rawData", $" table {i} row count {queryResultTables[i].Rows.Count}");
                            }
                            context.ContextWrite("ResponseHeaders", response.Headers);
                            string contextReturn = pa["contextRet"] ?? "0";
                            if (contextReturn.Equals("1") && response != null) {
                                response.Content = new StringContent(context.Snag());
                            }
                        }
                        else { // process data no write (maybe merge with above, but this is simpler)
                            DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, null, context);
                        }
                    }
                    break;
            }

        }
    }
}
