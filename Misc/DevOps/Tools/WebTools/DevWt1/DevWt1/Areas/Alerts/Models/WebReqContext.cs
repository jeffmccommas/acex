﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using AO.TimeSeriesProcessing;
using Newtonsoft.Json;
// ReSharper disable StringIndexOfIsCultureSpecific.1

namespace DevWt1.Areas.Alerts.Models {
    public class WebReqContext {
        public string ID { get; set; }

        [Display(Name = "Request Name")]
        public string Name { get; set; }
        [Display(Name = "URL info")]
        public string Url { get; set; }
        public string Method { get; set; }
        public string Description { get; set; }
        public string Headers { get; set; }
        public string Data { get; set; }
        public bool Process { get; set; }
        public string UserName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LastResult => lastResult;

        /// <summary>
        /// 
        /// </summary>
        //public List<string> HeaderList () {
        //    return KvpToList(HeadersKVPListGen()); 
        //}


        /// <summary>
        /// 
        /// </summary>
        public List<string> QueryparsList () {
            return KvpToList(QueryparsKVPList);
        }


        public static List<WebReqContext> RequestSet() => GetList();

        private string lastResult = "";  // the last result from the request
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetLastResult() => lastResult;


        public WebReqContext DoExecute(WebReqContext ctx) {
            ctx.lastResult = ctx.ReallyExecute(ctx);
            return ctx;
        }


        public string ReallyExecute(WebReqContext ctx) {

            string retResult = "nothingDone";
            using (HttpClient cl = new HttpClient()) {
                HttpContent content = new StringContent(Data);
                foreach (KeyValuePair<string, string> pair in HeadersKVPList) {
                    try {
                        cl.DefaultRequestHeaders.Add(pair.Key,
                            pair.Value.StartsWith(postmanVariableLeaderTag) ? CleanupStringVars(pair.Value) : pair.Value);
                    }
                    catch (Exception exc) {
                        Trace.WriteLine($"Exception on set header for '{pair.Key}'='{pair.Value}' {exc.Message}, {exc.InnerException?.Message}");
                    }
                }
                bool useHTTPS = true;
                if (useHTTPS) {
                    ServicePointManager.CertificatePolicy = new HttpUtils.AcceptAllCertificatePolicy();
                }

                string furl = CleanupStringVars(Url);

                string doExecute = ConfigurationManager.AppSettings.Get("ExecuteWebRequests");
                if (doExecute == "1") {
                    try {
                        Task<HttpResponseMessage> tsk = cl.PutAsync(furl, content);
                        int delay = 2000;
                        while (!tsk.IsCompleted & delay-- > 0) { // cheap. don't care if we wait a bit.
                            Thread.Sleep(500);
                        }
                        if (delay == 0) {
                            return "timeout";
                        }
                        if (tsk.Result == null) {
                            string rVal = $"Null result, task status = {tsk.Status} ";
                            string tVal = "";
                            if (tsk.Exception != null) {
                                tVal = $" exception:{tsk.Exception.Message}";
                            }
                            return rVal + tVal;
                        }
                        Task<string> results = tsk.Result.Content.ReadAsStringAsync();
                        delay = 20;
                        while (!results.IsCompleted & delay-- > 0) {
                            Thread.Sleep(1000);
                        }
                        if (delay == 0) {
                            return "timeout";
                        }
                        retResult = results.Result;
                    }
                    catch (Exception exc) {
                        NameValueCollection pars = new NameValueCollection();
                        pars.Add("no", "pars");
                        pars.Add("furl", furl);
                        retResult = XmlDocUtils.GetXMLExceptionDocument("reqExecute", pars, exc).OuterXml;
                    }
                }
            }
            return retResult;
        }

        public List<string> urlParts;

        public List<string> UrlParts() {
            List<string> parts = new List<string>();
            Url = CleanupStringVars(Url);
            Uri uri = new Uri(Url);
            parts.Add(uri.Port.ToString());
            parts.Add(uri.Host);
            foreach (var s in uri.AbsolutePath.Split('/')) {
                if (!string.IsNullOrEmpty(s)) parts.Add(s);
            }
            //parts.Add(uri.AbsolutePath);
            return parts;
        }

        public List<string> LastResultLines() {
            List<string> list = new List<string>();
            list.Add("--nada--");
            if (!string.IsNullOrEmpty(LastResult)) {
                list = LastResult.Split('\n').ToList();
            }
            return list;
        }


        public List<string> DataLines() {
            List<string> list = null;
            if (!string.IsNullOrEmpty(Data)) {
                list = Data.Split('\n').ToList();
            }
            return list;
        }

        public List<KeyValuePair<string, string>> HeadersKVPList = new List<KeyValuePair<string, string>>();


        public static List<KeyValuePair<string, string>> HeadersKVPListGen(WebReqContext req) {
            req.HeadersKVPList = new List<KeyValuePair<string, string>>();
            if (!string.IsNullOrEmpty(req.Headers)) {
                string[] headerStgs = req.Headers.Split('\n');
                foreach (string headerStg in headerStgs) {
                    if (!string.IsNullOrEmpty(headerStg)) {
                        try {
                            int idx = headerStg.IndexOf(":");
                            string name = headerStg.Substring(0, idx);
                            string value = (headerStg.Length >= idx + 2) ? headerStg.Substring(idx + 2) : "";
                            value = req.CleanupStringVars(value);
                            req.HeadersKVPList.Add(new KeyValuePair<string, string>(name, value));
                        }
                        catch (Exception exc) {
                            req.HeadersKVPList.Add(new KeyValuePair<string, string>("BadHeader", headerStg));
                        }
                    }
                }
            }
            return req.HeadersKVPList;
        }

        public List<KeyValuePair<string, string>> QueryparsKVPList = new List<KeyValuePair<string, string>>();

        public static List<KeyValuePair<string, string>> QueryparsKVPListGen(WebReqContext req) {
            req.QueryparsKVPList = new List<KeyValuePair<string, string>>();

            req.Url = req.CleanupStringVars(req.Url);
            Uri uri = new Uri(req.Url);
            string[] pars = uri.Query.Trim('?').Split('&');
            foreach (string par in pars) {
                string[] parnv = par.Split('=');
                string val = (par.Length >= 2) ? req.CleanupStringVars(parnv[1]) : "";
                req.QueryparsKVPList.Add(new KeyValuePair<string, string>(parnv[0], val));
            }
            return req.QueryparsKVPList;
        }





        private static List<string> KvpToList(List<KeyValuePair<string, string>> headers) {
            List<string> list = new List<string>();
            foreach (var header in headers) {
                list.Add($"{header.Key}={header.Value}");
            }
            return list;
        }


        public static WebReqContext GetItem(string id) {
            var doc = RequestsDocument();
            XmlNode request = doc.SelectSingleNode($"//requests[id=\"{id}\"]");
            if (request != null) {
                WebReqContext req = CreateContext(request);
                req.Url = req.CleanupStringVars(req.Url);
                QueryparsKVPListGen(req);
                HeadersKVPListGen(req);
                req.urlParts = req.UrlParts();
                return req;
            }
            return null;
        }


        private static List<WebReqContext> GetList() {
            var doc = RequestsDocument();

            List<WebReqContext> nodes = new List<WebReqContext>();
            string value = doc.SelectSingleNode("root/name").InnerText;

            foreach (XmlNode request in doc.FirstChild.SelectNodes("requests")) {
                var req = CreateContext(request);
                nodes.Add(req);
            }
            return nodes;
        }

        private static WebReqContext CreateContext(XmlNode request) {
            WebReqContext req = new WebReqContext() {
                Name = request.SelectSingleNode("name").InnerText,
                ID = request.SelectSingleNode("id").InnerText,
                Url = request.SelectSingleNode("url").InnerText,
                Method = request.SelectSingleNode("method").InnerText,
                Description = request.SelectSingleNode("description").InnerText,
                Headers = request.SelectSingleNode("headers").InnerText
            };
            if (req.Method.Equals("PUT") || req.Method.Equals("POST")) {
                req.Data = request.SelectSingleNode("rawModeData").InnerText;

            }
            return req;
        }


        private static XmlDocument RequestsDocument() {
            string fName = ConfigurationManager.AppSettings.Get("PostmanDiagFile");
            fName = System.Web.HttpContext.Current.Server.MapPath(fName);
            string jsonStg;
            using (StreamReader sr = new StreamReader(fName)) {
                jsonStg = sr.ReadToEnd();
            }
            XmlDocument doc = JsonConvert.DeserializeXmlNode(jsonStg, "root");
            return doc;
        }






        const string postmanVariableLeaderTag = "{{";  // marks a postman replacement variable
        const string postmanVariableTrailererTag = "}}";  // marks a postman replacement variable

        private List<KeyValuePair<string, string>> paramReplacements = null;
        private static string paramReplacementsPadlock = "paramReplacements";
        /// <summary>
        /// something to clean up parameterized xxx values in strings.  postman "{{xxx}}"
        /// src is config value of "{{xxx}}=newxxx;{{yyy}}=newyyy"
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public string CleanupStringVars(string src) {
            if (paramReplacements == null) {
                lock (paramReplacementsPadlock) {
                    if (paramReplacements == null) {
                        paramReplacements = new List<KeyValuePair<string, string>>();
                        string replacmentStg = ConfigurationManager.AppSettings.Get("PostmanReplacements");
                        foreach (string s in replacmentStg.Split(';')) {
                            if (!string.IsNullOrEmpty(s)) {
                                string[] ss = s.Split('=');
                                if (ss.Length >= 2) {
                                    paramReplacements.Add(new KeyValuePair<string, string>(ss[0], ss[1]));
                                }
                                else {
                                    paramReplacements.Add(new KeyValuePair<string, string>(ss[0], "nada"));
                                }
                            }
                        }
                    }
                }
            }
            if (src.IndexOf(postmanVariableLeaderTag) >= 0) {
                foreach (KeyValuePair<string, string> pair in paramReplacements) {
                    src = src.Replace(pair.Key, pair.Value);
                }
                if (src.IndexOf(postmanVariableLeaderTag) >= 0) {  // destroy any trailers
                    src = src.Replace(postmanVariableLeaderTag, "");
                }
                if (src.IndexOf(postmanVariableTrailererTag) >= 0) {  // destroy any trailers
                    src = src.Replace(postmanVariableTrailererTag, "");
                }
            }
            return src;
        }

    }




}