﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.ValueProviders.Providers;
using System.Web.Mvc;
using System.Xml;
using Cassandra;
using DevWt1.Areas.Alerts.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DevWt1.Areas.Alerts.Controllers
{
    public class AlertsController : Controller
    {
        // GET: Alerts/Alerts
        public ActionResult Index()
        {
            return View(WebReqContext.RequestSet());
        }


        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(WebReqContext req) {
            if (req.Description.StartsWith("ex")) {
                WebReqContext result = req.DoExecute(req);
                return View(result);
            }
            if (!ModelState.IsValid) {
                return RedirectToAction("Index");
            }
            return View(req);
        }


        //[ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Details(WebReqContext req) {
            string pwotd = ConfigurationManager.AppSettings.Get("pwotd");
            if (req.Process && (!string.IsNullOrEmpty(req.UserName)) && req.UserName.Equals(pwotd)) {
                req = WebReqContext.GetItem(req.ID);  // BUGBUG -- FAILING TO RETURN REQ FROM PAGE SO REINITIALIZE
                WebReqContext result = req.DoExecute(req);
                return View(result);
            }
            WebReqContext newReq = WebReqContext.GetItem(req.ID);
            if (!ModelState.IsValid) {
                return RedirectToAction("Index");
            }
            return View(newReq);
        }

        public ActionResult Details(string id, string val) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WebReqContext ctx = WebReqContext.GetItem(id);
            if (ctx == null) {
                return HttpNotFound();
            }
            return View(ctx);
        }

        public ActionResult Edit(string id, string val) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WebReqContext ctx = WebReqContext.GetItem(id);
            if (ctx == null) {
                return HttpNotFound();
            }
            if (val.Equals("execute")) {
                WebReqContext result = ctx.DoExecute(ctx);
                return View(result);
            }
            return View(ctx);
        }







    }
}
