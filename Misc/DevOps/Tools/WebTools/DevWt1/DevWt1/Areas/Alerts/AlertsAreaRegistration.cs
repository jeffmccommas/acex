﻿using System.Security.Policy;
using System.Web.Mvc;

namespace DevWt1.Areas.Alerts
{
    public class AlertsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Alerts";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Alerts_default",
                "Alerts/{controller}/{action}/{id}",
                new { action = "Index", controller = "Alerts", id = UrlParameter.Optional, val = "nogo" },
                new[] { "DevWt1.Areas.Alerts.Controllers" }
            );

            //context.MapRoute(
            //    "Alerts_Execute",
            //    "Alerts/{controller}/{action}/{id}",
            //    new { action = "DoExecute", controller = "Alerts", id = UrlParameter.Optional, val = "dogo" },
            //    new[] { "DevWt1.Areas.Alerts.Controllers" }
            //);
        }
    }
}