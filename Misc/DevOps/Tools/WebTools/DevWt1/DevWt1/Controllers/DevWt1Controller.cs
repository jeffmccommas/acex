﻿using System.Web.Mvc;

namespace DevWt1.Controllers
{
    public class DevWt1Controller : Controller
    {
        // GET: DevWt1
        public ActionResult Index()
        {
            return View();
        }

        // GET: DevWt1/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: DevWt1/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DevWt1/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DevWt1/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: DevWt1/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DevWt1/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: DevWt1/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }




    }
}
