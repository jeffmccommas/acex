﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Xml;
using System.Xml.Serialization;
using AO.TimeSeriesProcessing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Formatting = Newtonsoft.Json.Formatting;

namespace DevWt1.Controllers {



    //[RoutePrefix("Status")]
    /// <summary>
    /// 
    /// </summary>
    public class StatusController : ApiController {


        //public string GetDefault(int? id)
        //{
        //    return $"StatusController get at Default(), id={id}";
        //}

        //string dbCxnStg = "Server=tcp:aceqsql1c0.database.windows.net,1433;Database=InsightsMetaData;User ID=Aclweb@aceqsql1c0;Password=393;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;Min Pool Size=5;";
        string dbCxnStg = "";
       //private string dbCxnStg = "Server=acl10507l\\sqlexpress;Database=InsightsMetaData;uid=sa;pwd=Tee#1.off;Max Pool Size=1000; Min Pool Size=100;Connection Lifetime=120;";
        private bool dbOverride = false;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="env"></param>
        /// <param name="client"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public HttpResponseMessage GetConfigInfo(string env, string client, string key) {
            ProcessingArgs pa = new ProcessingArgs();
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            ContextData.InitContextStrings(env, client, key, pa,Request);
            ProcessingArgs.AccumulateRequestPars(Request, client, env, key, pa);
            ContextData contextData = new ContextData(pa);

            StringWriter sw = new StringWriter();
            string commandRoot = "select * from sys.";
            IEnumerable<KeyValuePair<string, string>> inQParams = Request.GetQueryNameValuePairs();
            bool doNothing = false;
            List<FoundItem> foundItems = new List<FoundItem>();

            string tCxn = ConfigurationManager.AppSettings.Get("sqlCxn0" + env);
            if (!string.IsNullOrEmpty(tCxn)) {
                dbCxnStg = tCxn;
            }
            bool doJsonConfig = false;

            try {
                string filterText = "";
                string followText = "";
                foreach (var keyValuePair in inQParams) {
                    switch (keyValuePair.Key) {
                        case "jstest": {
                            string respStg = "{\"status_code\":\"MPCE4001\",\"request_id\":\"O1612240818265f477bcb\"}";
                            JObject jo = new JObject(JObject.Parse(respStg));
                            string jp = jo["status_code"].ToString();
                            switch (jp) {
                                case "nt":
                                    Debug.WriteLine("is nt");
                                    break;
                                default:
                                    Debug.WriteLine($" status={jp}");
                                    break;
                            }
                        }
                            break;

                        case "db2use":
                            string tVal = keyValuePair.Value ?? "0.00";
                            tCxn = ConfigurationManager.AppSettings.Get("sqlCxn0" + tVal);
                            if (!string.IsNullOrEmpty(tCxn)) {
                                dbCxnStg = tCxn;
                                dbOverride = true;
                            }

                            break;
                        case "textfilter":
                            filterText = keyValuePair.Value;
                            break;
                        case "textfollow":
                            followText = keyValuePair.Value;
                            break;
                        case "config":
                            doJsonConfig = true;
                            break;
                        default:
                            doNothing = true;
                            break;
                    }
                }

                if (doJsonConfig) {
                    return ProcessJsonData(contextData, pa, "hidden", env, client, key, false, null);
                }



                if (doNothing) {
                    response.Content = new StringContent("else disabled for now");
                }
                else {
                    PrepSqlConnection();
                    string sqlCmdText = PrepSqlCmd(ref env, commandRoot);
                    var dataError = RunSqlCmd(dbCxnStg, sqlCmdText, env, sw);

                    if (!dataError && !string.IsNullOrEmpty(key)) {
                        switch (key) {
                            default: {
                                int hits = 0;
                                XmlDocument xd = new XmlDocument();
                                xd.LoadXml(sw.ToString());
                                XmlNodeList nl = xd.SelectNodes("//" + key);
                                XmlDocument td = new XmlDocument();
                                XmlNode rootNode = td.AppendChild(td.CreateElement("tdRoot"));
                                XmlNode elems = rootNode.AppendChild(td.CreateElement("elems"));
                                foreach (XmlNode n in nl) {
                                    string srcName = "";
                                    if (n.ParentNode != null) {
                                        if (env.Equals("function")) {
                                            XmlNode pn = n.ParentNode;
                                            if (pn.SelectSingleNode("SPECIFIC_SCHEMA") != null)
                                                srcName += pn.SelectSingleNode("SPECIFIC_SCHEMA").InnerText + ".";
                                            if (pn.SelectSingleNode("SPECIFIC_NAME") != null)
                                                srcName += pn.SelectSingleNode("SPECIFIC_NAME").InnerText;
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(filterText)) {
                                        try {
                                            if (n.InnerText.Contains(filterText)) {
                                                if (!string.IsNullOrEmpty(followText)) {
                                                    int idx = n.InnerText.IndexOf(followText);
                                                    string frag = n.InnerText.Substring(idx);
                                                    frag = frag.Substring(frag.IndexOf(' ')).Trim();
                                                    frag = frag.Substring(0, frag.IndexOf(' '));
                                                    hits++;
                                                    XmlNode n1 = elems.AppendChild(td.CreateElement("elem"));
                                                    n1.AppendChild(td.ImportNode(n, true));
                                                    frag = frag.Replace("[", string.Empty).Replace("]", string.Empty);
                                                    n1.AppendChild(td.CreateElement("found")).InnerText = frag;
                                                    foundItems.Add(new FoundItem() {
                                                        Item = srcName,
                                                        Target = frag
                                                    });
                                                    n1.AppendChild(td.CreateElement("filt")).InnerText = filterText;
                                                    n1.AppendChild(td.CreateElement("foll")).InnerText = followText;
                                                }
                                                else {
                                                    hits++;
                                                    XmlNode n1 = elems.AppendChild(td.CreateElement("elem"));
                                                    n1.AppendChild(td.ImportNode(n, true));
                                                    n1.AppendChild(td.CreateElement("filt")).InnerText = filterText;
                                                }
                                            }
                                        }
                                        catch (Exception exc) {
                                            contextData.ContextWrite("GetConfigInfo Exception",
                                                $"{exc.Message}, {exc.InnerException?.Message}");
                                        }
                                    }
                                    else {
                                        hits++;
                                        XmlNode n1 = elems.AppendChild(td.CreateElement("elem"));
                                        n1.AppendChild(td.ImportNode(n, true));

                                    }
                                }
                                if (foundItems.Count > 0) {
                                    var distinctTypeIDs = foundItems.Select(x => x.Target).Distinct();
                                    XmlNode foundsDistinct = rootNode.AppendChild(td.CreateElement("distincts"));
                                    int distincts = 0;
                                    foreach (string itm in distinctTypeIDs) {
                                        XmlNode foundsDistinctItem = foundsDistinct.AppendChild(td.CreateElement("distinct"));
                                        foundsDistinctItem.AppendChild(td.CreateElement("target")).InnerText = itm;
                                        foundsDistinctItem.AppendChild(td.CreateElement("count")).InnerText = RunRowCount(dbCxnStg, itm);
                                        distincts++;
                                    }
                                    foundsDistinct.AppendChild(td.CreateElement("hits")).InnerText = distincts.ToString();
                                    XmlNode founds = rootNode.AppendChild(td.CreateElement("founds"));
                                    founds.AppendChild(td.CreateElement("hits")).InnerText = foundItems.Count.ToString();
                                    foreach (FoundItem fi in foundItems) {
                                        XmlNode fin = founds.AppendChild(td.CreateElement("items"));
                                        fin.AppendChild(td.CreateElement("item")).InnerText = fi.Item;
                                        fin.AppendChild(td.CreateElement("target")).InnerText = fi.Target;
                                    }
                                }
                                elems.AppendChild(td.CreateElement("hits")).InnerText = hits.ToString();
                                sw = new StringWriter();
                                XmlWriter xw = new XmlTextWriter(sw);
                                td.WriteTo(xw);
                            }
                                break;
                        }
                    }
                    response.Content = new StringContent(sw.ToString());
                }
            }
            catch (Exception exc) {
                contextData.ContextWrite("GetConfigInfo outer Exception", $"{exc.Message}, {exc.InnerException?.Message}");
                string excStg = "{\"exception\":\"" + exc.Message + "\"}";
                //XmlDocument doc = JsonConvert.DeserializeXmlNode(excStg);
                response.Content = new StringContent(excStg);
            }
            finally {
                contextData.End();
            }
            FinalizeResponseHeaders(pa, response);
            return response;
        }



        //[HttpPut]
        public HttpResponseMessage PutConfigInfo(string env, string client, string key) {
            HttpResponseMessage resp = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            ProcessingArgs pa = new ProcessingArgs();
            ContextData.InitContextStrings(env, client, key, pa, Request);
            ProcessingArgs.AccumulateRequestPars(Request, client, env, key, pa);
            ContextData contextData = new ContextData(pa);
            try {
                string dataIn = Request.Content.ReadAsStringAsync().Result;
                IEnumerable<KeyValuePair<string, string>> inQParams = Request.GetQueryNameValuePairs();
                resp = ProcessJsonDataHack(contextData, pa, env, client, key, inQParams, true, dataIn);
            }
            catch (Exception exc) {
                contextData.ContextWrite("PutConfigInfo exception",$"{exc.Message},{exc.InnerException?.Message}");
            }
            finally {
                contextData.End();
            }
            return resp;
        }



        public HttpResponseMessage ProcessJsonDataHack(ContextData contextData, ProcessingArgs pa, string env, string client, string key, IEnumerable<KeyValuePair<string, string>> inQParams, bool allowChanges, string dataIn) {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            string commandRoot = "select * from sys.";
            ReformatTest.DoFormat();
            string what2Do = pa.GetControlVal("config", "nothing");
            bool doModify = false;
            int writeOp = 0;
            if (allowChanges) {
                writeOp = pa.GetControlVal("writeoveride", writeOp);
            }
            string tCxn = ConfigurationManager.AppSettings.Get("sqlCxn0" + env);
            if (!string.IsNullOrEmpty(tCxn)) {
                dbCxnStg = tCxn;
            }

            string parName = pa.GetControlVal("name", "default");
            string parValue = pa.GetControlVal("value", "0.00");
            string dbSelect = null;
            string db2Use = dbCxnStg;
            string dbDiff = "";
            string tenv = pa.GetControlVal("dbdiff", "");
            if (!string.IsNullOrEmpty(tenv)) {
                tenv = ConfigurationManager.AppSettings.Get("sqlCxn0" + tenv);
                if (!string.IsNullOrEmpty(tenv)) {
                    dbDiff = tenv;
                }

            }
            int doDiffOp = pa.GetControlVal("dodiff", 0);
            bool clientReset = false;
            int diffClient = pa.GetControlVal("diffclient", 0);
            string diffEnv = pa.GetControlVal("diffenv", "");
            string rootItem = pa.GetControlVal("rootitem", "ExtendedAttributes");


            bool nameValueList = false;
            string jsonString = "";
            bool doingCompare = false;

            string configKey = "aclaraone.clientsettings";
            switch (key) {
                case "settings":
                    configKey = "aclaraone.clientsettings";
                    break;
                case "export":
                    configKey = "aclaraone.clientexportsettings";
                    break;
                case "veesettings":
                    configKey = "aclaraone.clientveesettings";
                    break;
                case "bulkload":
                    configKey = "bulkload";
                    break;
            }


            if (!string.IsNullOrEmpty(pa.GetControlVal("EnvironmentKey",""))) {
                env = pa.GetControlVal("EnvironmentKey", "");
            }

            string contentType = "application/json";
            try {
                PrepSqlConnection();
                string sqlCmdText = $"select JSONConfiguration from cm.ClientCOnfigurationBulk where ClientID='{client}' and EnvironmentKey='{env}' AND ConfigurationBulkKey = '{configKey}'";
                using (SqlConnection cxn = new SqlConnection(db2Use)) {
                    cxn.Open();
                    switch (what2Do) { // prep the actions for later
                        case "nvl":
                            nameValueList = true;
                            what2Do = "update";
                            break;
                        default:
                            rootItem = parName;
                            break;
                        case "compare":
                            doingCompare = true;
                            break;
                    }

                    switch (what2Do) { // get the starting payload
                        case "reload":
                            string fname = ConfigurationManager.AppSettings.Get("dfile1");
                            using (StreamReader sr = new StreamReader(fname)) {
                                jsonString = sr.ReadToEnd();
                            }
                            doModify = true;
                            break;
                        case "clreset":
                            clientReset = true;
                            jsonString = dataIn;
                            doModify = true;
                            break;
                        default:
                            if (string.IsNullOrEmpty(jsonString)) {
                                SqlCommand cmd = new SqlCommand(sqlCmdText, cxn);
                                using (SqlDataReader dr = cmd.ExecuteReader()) {
                                    dr.Read();
                                    jsonString = dr.GetString(0);
                                    contextData.NoFixes.AddLine($"config query {sqlCmdText}");
                                    contextData.NoFixes.AddLine(jsonString);
                                }
                            }
                            break;
                    }

                    JToken json = JObject.Parse(jsonString);
                    var jsonObjectInital = JObject.Parse(jsonString);
                    string startTraceSave = TraceConfigJson(json.ToString(), "pre", client, env, configKey, db2Use, "n/a");

                    if (doModify) {
                        json["AlterInformation"] = "apply changes indicated";
                        JObject jsonObjectFinal = JObject.Parse(json.ToString());  // force through string for sanity check

                        response.Content = new StringContent(jsonObjectFinal.ToString());

                        SqlCommand cmd = new SqlCommand("[cm].[usp_UpdateJsonConfig]", cxn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@ID", client);
                        cmd.Parameters.AddWithValue("@Env", env);
                        string trashString = pa.GetControlVal("corruptStg", "");
                        if (!string.IsNullOrEmpty(trashString)) {
                            string targetStg = json.ToString();
                            targetStg = targetStg.Replace($"\"{trashString}\"", trashString);
                            cmd.Parameters.AddWithValue("@Json", targetStg);
                        }
                        else {
                            cmd.Parameters.AddWithValue("@Json", json.ToString());
                        }

                        StringBuilder dw1 = CompareObjects(jsonObjectInital, jsonObjectFinal);
                        jsonObjectFinal["DeltaInitial2Final"] = dw1.ToString();
                        StringBuilder dw2 = CompareObjects(jsonObjectFinal, jsonObjectInital);
                        jsonObjectFinal["DeltaFinal2Initial"] = dw2.ToString();
                        if (startTraceSave != null) {
                            jsonObjectFinal["ErrorTracingCHanges"] = startTraceSave;
                        }

                        string deltas = $"initial->final:\n{dw1}\nfinal->initial\n{dw2}";
                        if (doDiffOp == 10) {
                            response.Content = new StringContent(deltas);
                            contentType = "text/plain";
                        }
                        else {
                            jsonObjectFinal["DeltaBoth"] = deltas;
                            response.Content = new StringContent(jsonObjectFinal.ToString());
                        }
                        TraceConfigJson(json.ToString(), "post", client, env, configKey, db2Use, deltas);
                        if ((writeOp > 0) && allowChanges) {

                            int rval = 0;
                            contextData.NoFixes.AddLine($"write config, {client} {env}");
                            contextData.NoFixes.AddLine(jsonString);
                            rval = cmd.ExecuteNonQuery();
                            if (rval > 0) {
                            }
                        }
                    }
                    else {
                        string jsonString2;
                        if (diffClient > 0) {
                            if (string.IsNullOrEmpty(dbDiff)) {
                                dbDiff = db2Use;
                            }
                            using (SqlConnection cxn2 = new SqlConnection(dbDiff)) {
                                cxn2.Open();
                                string sqlCmdText2 =
                                    $"select JSONConfiguration from cm.ClientCOnfigurationBulk where ClientID='{diffClient}' and EnvironmentKey='{env}' AND ConfigurationBulkKey = '{configKey}'";
                                SqlCommand cmd = new SqlCommand(sqlCmdText2, cxn2);
                                using (SqlDataReader dr = cmd.ExecuteReader()) {
                                    dr.Read();
                                    jsonString2 = dr.GetString(0);
                                }
                                JObject json2 = JObject.Parse(jsonString2);
                                StringBuilder dw1 = CompareObjects(jsonObjectInital, json2);
                                StringBuilder dw2 = CompareObjects(json2, jsonObjectInital);
                                string deltas = $"left->right:\n{dw1}\nright->left\n{dw2}";
                                response.Content = new StringContent(deltas);
                                contentType = "text/plain";
                            }
                        }
                        else if (!string.IsNullOrEmpty(diffEnv)) {
                            if (string.IsNullOrEmpty(dbDiff)) {
                                dbDiff = db2Use;
                            }
                            using (SqlConnection cxn2 = new SqlConnection(dbDiff)) {
                                cxn2.Open();
                                string sqlCmdText3 =
                                    $"select JSONConfiguration from cm.ClientCOnfigurationBulk where ClientID='{client}' and EnvironmentKey='{diffEnv}' AND ConfigurationBulkKey = '{configKey}'";
                                SqlCommand cmd = new SqlCommand(sqlCmdText3, cxn2);
                                using (SqlDataReader dr = cmd.ExecuteReader()) {
                                    dr.Read();
                                    jsonString2 = dr.GetString(0);
                                }
                                JObject json2 = JObject.Parse(jsonString2);
                                StringBuilder dw1 = CompareObjects(jsonObjectInital, json2);
                                StringBuilder dw2 = CompareObjects(json2, jsonObjectInital);
                                string deltas = $"initial->final:\n{dw1}\nfinal->initial\n{dw2}";
                                response.Content = new StringContent(deltas);
                                contentType = "text/plain";
                            }
                        }
                        else {
                            json["AlterInformation"] = "no changes";
                            response.Content = new StringContent(json.ToString());
                        }
                    }

                }
                FinalizeResponseHeaders(pa, response, contentType);
            }
            catch (Exception exc) {
                contextData.ContextWrite("ProcessJsonDataHack Exception", $"{exc.Message}, {exc.InnerException?.Message}");
                string excStg = "{\"exception\":\"" + exc.Message + "\"}";
                XmlDocument doc = JsonConvert.DeserializeXmlNode(excStg);
                response.Content = new StringContent(doc.OuterXml);
                FinalizeResponseHeaders(pa, response, "application/xml");
            }
            return response;
        }


        public HttpResponseMessage ProcessJsonData(ContextData contextData, ProcessingArgs pa, string hide, string env,string client, string key, bool allowChanges, string dataIn) {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);

            string commandRoot = "select * from sys.";
            ReformatTest.DoFormat();
            string what2Do = pa.GetControlVal("config", "nothing");
            bool doModify = false;
            int writeOp = 0;
            if (allowChanges) {
                writeOp = pa.GetControlVal("writeoveride", writeOp);
            }
            string tCxn = ConfigurationManager.AppSettings.Get("sqlCxn0" + env);
            if (!string.IsNullOrEmpty(tCxn)) {
                dbCxnStg = tCxn;
            }

            string parName = pa.GetControlVal("name", "default");
            string parValue = pa.GetControlVal("value", "0.00");
            string dbSelect = null;
            string db2Use = dbCxnStg;
            string dbDiff = "";
            string tenv = pa.GetControlVal("dbdiff", "");
            if (! string.IsNullOrEmpty(tenv)){
                tenv = ConfigurationManager.AppSettings.Get("sqlCxn0" + tenv);
                if (!string.IsNullOrEmpty(tenv)) {
                    dbDiff = tenv;
                }

            }
            int doDiffOp = pa.GetControlVal("dodiff",0);
            bool clientReset = false;
            int diffClient = pa.GetControlVal("diffclient", 0);
            string diffEnv = pa.GetControlVal("diffenv", "");
            string rootItem = pa.GetControlVal("rootitem", "ExtendedAttributes"); 

            bool nameValueList = false;
            string jsonString = "";
            bool doingCompare = false;

            string configKey = "aclaraone.clientsettings";
            switch (key) {
                case "settings":
                    configKey = "aclaraone.clientsettings";
                    break;
                case "export":
                    configKey = "aclaraone.clientexportsettings";
                    break;
                case "veesettings":
                    configKey = "aclaraone.clientveesettings";
                    break;
                case "bulkload":
                    configKey = "bulkload";
                    break;
            }


            string contentType = "application/json";

            try {
                PrepSqlConnection();
                string sqlCmdText = $"select JSONConfiguration from cm.ClientCOnfigurationBulk where ClientID='{client}' and EnvironmentKey='{env}' AND ConfigurationBulkKey = '{configKey}'";
                //string[] columnTypes;
                using (SqlConnection cxn = new SqlConnection(db2Use)) {
                    cxn.Open();
                    switch (what2Do) { // prep the actions for later
                        case "nvl":
                            nameValueList = true;
                            what2Do = "update";
                            break;
                        default:
                            rootItem = parName;
                            break;
                        case "compare":
                            doingCompare = true;
                            break;
                    }


                    switch (what2Do) { // get the starting payload
                        case "reload":
                            string fname = ConfigurationManager.AppSettings.Get("dfile1");
                            using (StreamReader sr = new StreamReader(fname)) {
                                jsonString = sr.ReadToEnd();
                            }
                            doModify = true;
                            break;
                        case "clreset":
                            clientReset = true;
                            jsonString = dataIn;
                            break;
                        default:
                            if (string.IsNullOrEmpty(jsonString)) {
                                SqlCommand cmd = new SqlCommand(sqlCmdText, cxn);
                                using (SqlDataReader dr = cmd.ExecuteReader()) {
                                    dr.Read();
                                    jsonString = dr.GetString(0);
                                }
                            }
                            break;
                    }


                    JToken json = JObject.Parse(jsonString);
                    var jsonObjectInital = JObject.Parse(jsonString);
                    string startTraceSave = TraceConfigJson(json.ToString(), "pre", client, env, configKey, db2Use, "n/a");

                    //JToken jt1 = json.First;
                    List<KeyValuePair<string, string>> kvps = new List<KeyValuePair<string, string>>();
                    if (!doingCompare) {
                        //cmd = new SqlCommand(sqlCmdText, cxn);
                        if (nameValueList) {
                            if (json[rootItem] == null) {
                                string setVal = $"{parName}={parValue}";
                                //setVal = "normalDecimalFormat=0.00";
                                json[rootItem] = setVal;
                                doModify = true;
                            }
                        }
                        else {
                            json[rootItem] = parValue;
                            doModify = true;
                        }
                        if (nameValueList) {
                            string nvPairsStg = json[rootItem].Value<string>();
                            string[] nvPairs = nvPairsStg.Split(',');
                            foreach (string stg in nvPairs) {
                                string[] nvpair = stg.Split('=');
                                kvps.Add(new KeyValuePair<string, string>(nvpair[0], nvpair[1]));
                            }
                        }
                    }

                    switch (what2Do) {
                        case "reset":
                            kvps = new List<KeyValuePair<string, string>> {
                                    new KeyValuePair<string, string>("noDecimalFormat", "0")
                                };
                            doModify = true;
                            break;
                        case "testappend":
                            kvps.Add(new KeyValuePair<string, string>("newFormat", "00000.000000"));
                            doModify = true;
                            break;
                        case "update":
                            bool altered = false;
                            if (nameValueList) {
                                if (!string.IsNullOrEmpty(parName) && !string.IsNullOrEmpty(parValue)) {
                                    foreach (var kvp in kvps) {
                                        if (kvp.Key == parName) {
                                            kvps.Remove(kvp);
                                            kvps.Add(new KeyValuePair<string, string>(parName, parValue));
                                            altered = true;
                                            break;
                                        }
                                    }
                                    if (!altered) {
                                        kvps.Add(new KeyValuePair<string, string>(parName, parValue));
                                    }
                                }
                            }
                            doModify = true;
                            break;
                    }

                    if (doModify) {
                        if (nameValueList) {
                            string newJsonValue = "";
                            foreach (var kvp in kvps) {
                                newJsonValue += $"{kvp.Key}={kvp.Value},";
                            }
                            json[rootItem] = newJsonValue.TrimEnd(',');
                        }
                        else {

                        }
                        json["AlterInformation"] = "apply changes indicated";
                        //if (clientReset) {
                        //    InitJsonInfo2(json);
                        //}
                        JObject jsonObjectFinal = JObject.Parse(json.ToString());  // force through string for sanity check

                        response.Content = new StringContent(jsonObjectFinal.ToString());

                        SqlCommand cmd = new SqlCommand("[cm].[usp_UpdateJsonConfig]", cxn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@ID", client);
                        cmd.Parameters.AddWithValue("@Env", env);
                        cmd.Parameters.AddWithValue("@Json", json.ToString());

                        //if (doDiffOp > 1) {  // force some changes as a sanity check of the delta process
                        //    jsonObjectFinal["makediff"] = "forcing add difference";
                        //    if (jsonObjectFinal["TimeZone"] != null) {
                        //        jsonObjectFinal["TimeZone"].Parent.Remove();
                        //    }
                        //}

                        StringBuilder dw1 = CompareObjects(jsonObjectInital, jsonObjectFinal);
                        jsonObjectFinal["DeltaInitial2Final"] = dw1.ToString();
                        StringBuilder dw2 = CompareObjects(jsonObjectFinal, jsonObjectInital);
                        jsonObjectFinal["DeltaFinal2Initial"] = dw2.ToString();
                        if (startTraceSave != null) {
                            jsonObjectFinal["ErrorTracingCHanges"] = startTraceSave;
                        }

                        string deltas = $"initial->final:\n{dw1}\nfinal->initial\n{dw2}";
                        if (doDiffOp == 10) {
                            response.Content = new StringContent(deltas);
                            contentType = "text/plain";
                        }
                        else {
                            jsonObjectFinal["DeltaBoth"] = deltas;
                            response.Content = new StringContent(jsonObjectFinal.ToString());
                        }
                        TraceConfigJson(json.ToString(), "post", client, env, configKey, db2Use, deltas);
                        if ((writeOp > 0) && allowChanges) {
                            int rval = cmd.ExecuteNonQuery();
                        }
                    }
                    else {
                        string jsonString2;
                        if (diffClient > 0) {
                            if (string.IsNullOrEmpty(dbDiff)) {
                                dbDiff = db2Use;
                            }
                            using (SqlConnection cxn2 = new SqlConnection(dbDiff)) {
                                cxn2.Open();
                                string sqlCmdText2 =
                                    $"select JSONConfiguration from cm.ClientCOnfigurationBulk where ClientID='{diffClient}' and EnvironmentKey='{env}' AND ConfigurationBulkKey = '{configKey}'";
                                SqlCommand cmd = new SqlCommand(sqlCmdText2, cxn2);
                                using (SqlDataReader dr = cmd.ExecuteReader()) {
                                    dr.Read();
                                    jsonString2 = dr.GetString(0);
                                }
                                JObject json2 = JObject.Parse(jsonString2);
                                StringBuilder dw1 = CompareObjects(jsonObjectInital, json2);
                                StringBuilder dw2 = CompareObjects(json2, jsonObjectInital);
                                string deltas = $"left->right:\n{dw1}\nright->left\n{dw2}";
                                response.Content = new StringContent(deltas);
                                contentType = "text/plain";
                            }
                        }
                        else if (!string.IsNullOrEmpty(diffEnv)) {
                            if (string.IsNullOrEmpty(dbDiff)) {
                                dbDiff = db2Use;
                            }
                            using (SqlConnection cxn2 = new SqlConnection(dbDiff)) {
                                cxn2.Open();
                                string sqlCmdText3 =
                                    $"select JSONConfiguration from cm.ClientCOnfigurationBulk where ClientID='{client}' and EnvironmentKey='{diffEnv}' AND ConfigurationBulkKey = '{configKey}'";
                                SqlCommand cmd = new SqlCommand(sqlCmdText3, cxn2);
                                using (SqlDataReader dr = cmd.ExecuteReader()) {
                                    dr.Read();
                                    jsonString2 = dr.GetString(0);
                                }
                                JObject json2 = JObject.Parse(jsonString2);
                                StringBuilder dw1 = CompareObjects(jsonObjectInital, json2);
                                StringBuilder dw2 = CompareObjects(json2, jsonObjectInital);
                                string deltas = $"initial->final:\n{dw1}\nfinal->initial\n{dw2}";
                                response.Content = new StringContent(deltas);
                                contentType = "text/plain";
                            }
                        }
                        else {
                            json["AlterInformation"] = "no changes";
                            response.Content = new StringContent(json.ToString());
                        }
                    }

                }
                FinalizeResponseHeaders(pa, response, contentType);
            }
            catch (Exception exc) {
                contextData.ContextWrite("ProcessJsonData outer Exception", $"{exc.Message}, {exc.InnerException?.Message}");
                string excStg = "{\"exception\":\"" + exc.Message + "\"}";
                XmlDocument doc = JsonConvert.DeserializeXmlNode(excStg);
                response.Content = new StringContent(doc.OuterXml);
                FinalizeResponseHeaders(pa, response, "application/xml");
            }
            return response;
        }

        /// <summary>
        /// Deep compare two NewtonSoft JObjects. If they don't match, returns text diffs
        /// </summary>
        /// <param name="source">The expected results</param>
        /// <param name="target">The actual results</param>
        /// <returns>Text string</returns>

        private static StringBuilder CompareObjects(JObject source, JObject target) {
            StringBuilder returnString = new StringBuilder();
            foreach (KeyValuePair<string, JToken> sourcePair in source) {
                if (sourcePair.Value.Type == JTokenType.Object) {
                    if (target.GetValue(sourcePair.Key) == null) {
                        returnString.Append($"Key {sourcePair.Key}:'{sourcePair.Value}' not found {Environment.NewLine}");
                    }
                    else {
                        returnString.Append(CompareObjects(sourcePair.Value.ToObject<JObject>(),
                            target.GetValue(sourcePair.Key).ToObject<JObject>()));
                    }
                }
                else if (sourcePair.Value.Type == JTokenType.Array) {
                    if (target.GetValue(sourcePair.Key) == null) {
                        returnString.Append($"Key {sourcePair.Key}:'{sourcePair.Value}' not found {Environment.NewLine}");
                    }
                    else {
                        returnString.Append(CompareArrays(sourcePair.Value.ToObject<JArray>(),
                            target.GetValue(sourcePair.Key).ToObject<JArray>(), sourcePair.Key));
                    }
                }
                else {
                    JToken expected = sourcePair.Value;
                    var actual = target.SelectToken(sourcePair.Key);
                    if (actual == null) {
                        returnString.Append($"Key {sourcePair.Key}:'{sourcePair.Value}' not found {Environment.NewLine}");
                    }
                    else {
                        if (!JToken.DeepEquals(expected, actual)) {
                            returnString.Append($"Key {sourcePair.Key}:'{sourcePair.Value}' !=  '{target.Property(sourcePair.Key).Value}'{Environment.NewLine}");
                        }
                    }
                }
            }
            return returnString;
        }


        /// <summary>
        /// Deep compare two NewtonSoft JArrays. If they don't match, returns text diffs
        /// </summary>
        /// <param name="source">The expected results</param>
        /// <param name="target">The actual results</param>
        /// <param name="arrayName">The name of the array to use in the text diff</param>
        /// <returns>Text string</returns>

        private static StringBuilder CompareArrays(JArray source, JArray target, string arrayName = "") {
            var returnString = new StringBuilder();
            for (var index = 0; index < source.Count; index++) {

                var expected = source[index];
                if (expected.Type == JTokenType.Object) {
                    var actual = (index >= target.Count) ? new JObject() : target[index];
                    returnString.Append(CompareObjects(expected.ToObject<JObject>(),
                        actual.ToObject<JObject>()));
                }
                else {

                    var actual = (index >= target.Count) ? "" : target[index];
                    if (!JToken.DeepEquals(expected, actual)) {
                        if (String.IsNullOrEmpty(arrayName)) {
                            returnString.Append("Index " + index + ": " + expected
                                                + " != " + actual + Environment.NewLine);
                        }
                        else {
                            returnString.Append("Key " + arrayName
                                                + "[" + index + "]: " + expected
                                                + " != " + actual + Environment.NewLine);
                        }
                    }
                }
            }
            return returnString;
        }


        /// <summary>
        /// Save json to a file along with some donfiguration data about what was being worked on
        /// </summary>
        /// <param name="json"></param>
        /// <param name="activityCode"></param>
        /// <param name="id"></param>
        /// <param name="env"></param>
        /// <param name="key"></param>
        /// <param name="cxnStg"></param>
        /// <param name="deltas"></param>
        private string TraceConfigJson(string json, string activityCode, string id, string env, string key, string cxnStg, string deltas) {
            string rVal = null;
            try {
                JObject jObject = JObject.Parse(json); // force through string for sanity check
                string fileRoot = DateTime.Now.Ticks.ToString();
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(cxnStg);
                string encrStg = SimpleEncryption.Crypt(builder.Password);
                //string pwd = builder.Password;
                builder.Password = encrStg;
                jObject["SaveActivityData"] =
                    $" activity={activityCode}, id={id}, env={env} key={key} cxn={builder.ConnectionString}";
                jObject["SaveActivityDeltas"] = deltas;

                string saveDir = "/temp/traceInfo";
                if (!Directory.Exists(saveDir)) {
                    Directory.CreateDirectory(saveDir);
                }
                string saveFile = $"{fileRoot}_{activityCode}_{id}_{env}.json";
                string filePath = Path.Combine(saveDir, saveFile);
                using (StreamWriter sw = new StreamWriter(filePath)) {
                    sw.Write(jObject.ToString());
                }
            }
            catch (Exception exc) {
                rVal =
                    $" TraceConfigJson fail {exc.Message}; {((exc.InnerException != null) ? exc.InnerException.Message : "")} ";
            }
            //if (rVal == null) rVal = "test trace fail";
            return rVal;
        }


        /*  some captured bodies for resetting things
        void InitJsonInfo(JToken json)
        {
            json["TimeZone"] = "EST";
            json["IsAmiIntervalStart"] = false;
            json["IsDstHandled"] = false;
            json["CalculationScheduleTime"] = "04:00";
            json["NotificationReportScheduleTime"] = "05:00";
            json["InsightReportScheduleTime"] = "06:00";
            json["ForgotPasswordEmailTemplateId"] = "af5c0251-486f-4100-a4cf-2d91a905f108";
            json["RegistrationConfirmationEmailTemplateId"] = "7c0657cd-1020-42a4-a281-87db82a7f6c4";
            json["OptInEmailConfirmationTemplateId"] = "777ca9f8-bb03-490b-946b-b1e6991b2b15";
            json["OptInSmsConfirmationTemplateId"] = "87_12";
            json["OptInEmailUserName"] = "aclaradev";
            json["OptInEmailPassword"] = "Xaclaradev2311X";
            json["OptInEmailFrom"] = "support@aclarax.com";
            json["TrumpiaShortCode"] = "99000";
            json["TrumpiaApiKey"] = "b4c1971153b3509b6ec0d8a24a33454c";
            json["TrumpiaUserName"] = "aclaradev";
            json["TrumpiaContactList"] = "AclaraDemoDev";
            json["UnmaskedAccountIdEndingDigit"] = "4";
            json["IsVeeSubscribed"] = true;
        }

        void InitJsonInfo2(JToken json)
        {
            json["TimeZone"] = "EST";
            json["IsAmiIntervalStart"] = true;
            json["IsDstHandled"] = true;
            json["CalculationScheduleTime"] = "04:00";
            json["NotificationReportScheduleTime"] = "05:00";
            json["InsightReportScheduleTime"] = "06:00";
            json["ForgotPasswordEmailTemplateId"] = "af5c0251-486f-4100-a4cf-2d91a905f108";
            json["RegistrationConfirmationEmailTemplateId"] = "7c0657cd-1020-42a4-a281-87db82a7f6c4";
            json["OptInEmailConfirmationTemplateId"] = "777ca9f8-bb03-490b-946b-b1e6991b2b15";
            json["OptInSmsConfirmationTemplateId"] = "290_12";
            json["OptInEmailUserName"] = "aclaradev";
            json["OptInEmailPassword"] = "Xaclaradev2311X";
            json["OptInEmailFrom"] = "support@aclarax.com";
            json["TrumpiaShortCode"] = "33221";
            json["TrumpiaApiKey"] = "b4c1971153b3509b6ec0d8a24a33454c";
            json["TrumpiaUserName"] = "aclaradev";
            json["TrumpiaContactList"] = "LUSDev";
            json["UnmaskedAccountIdEndingDigit"] = "6";
        }
        */


        /// <summary>
        /// Perform some form of operation on DB(s).  Maybe copy data from one to another
        /// </summary>
        /// <param name="dtype"></param>
        /// <returns></returns>
        public HttpResponseMessage GetSqlConnect(string dtype) {
            ProcessingArgs pa = new ProcessingArgs();
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            StringWriter sw = new StringWriter();
            string commandRoot = "select * from sys.";
            bool dataError;
            ReformatTest.DoFormat();
            try {
                PrepSqlConnection();
                string refType = dtype;
                string sqlCmdText = PrepSqlCmd(ref refType, commandRoot);
                if (dtype.IndexOf("gcfg") == 0) {
                    if (dtype.Equals("gcfg00")) {
                        refType = "gcfg0";
                        sqlCmdText = PrepSqlCmd(ref refType, "");
                        GetColumnsInfo(dbCxnStg, sqlCmdText, ref insightMetaDataColumnTypes);
                        DataTable dt = RunSqlCmd(dbCxnStg, sqlCmdText);
                        refType = "gcfg02";
                        sqlCmdText = PrepSqlCmd(ref refType, "");
                        foreach (DataRow dataRow in dt.Rows) {
                            String cmdTxt = string.Format(sqlCmdText, dataRow.ItemArray);
                            int res = RunSqlCmd(dbCxnStg, sqlCmdText, dataRow.ItemArray);
                        }

                        dt.WriteXml(sw, true);
                    }
                    else {
                        dataError = RunSqlCmd(dbCxnStg, sqlCmdText, dtype, sw);
                    }
                }
                else {
                    dataError = RunSqlCmd(dbCxnStg, sqlCmdText, dtype, sw);
                }
            }
            catch (Exception exc) {
                string excStg = "{\"exception\":\"" + exc.Message + "\"}";
                XmlDocument doc = JsonConvert.DeserializeXmlNode(excStg);
                response.Content = new StringContent(doc.OuterXml);
            }
            response.Content = new StringContent(sw.ToString());
            FinalizeResponseHeaders(pa, response);
            return response;
        }

        private void PrepSqlConnection(string keyName = null) {
            if (dbOverride) return;
            string cfgTempStg;
            bool dataError = false;
            if (keyName == null) {
                if ((cfgTempStg = ConfigurationManager.AppSettings.Get("sqlCxnCfg")) != null) {
                    string cfgIdxVal = cfgTempStg;
                    if ((cfgTempStg = ConfigurationManager.AppSettings.Get("sqlCxn" + cfgIdxVal)) != null) {
                        dbCxnStg = cfgTempStg;
                    }
                }
            }
            else {
                dbCxnStg = ConfigurationManager.AppSettings.Get(keyName);
            }
        }
        private string PrepSqlCmd(ref string dtype, string commandRoot) {
            string desiredObjectType = "indexes";
            switch (dtype) {
                case "index":
                    desiredObjectType = "indexes";
                    break;
                case "procedure":
                    commandRoot = "";
                    desiredObjectType = "select * from information_schema.routines where routine_type = 'PROCEDURE'";
                    break;
                case "function":
                    commandRoot = "";
                    desiredObjectType = "select * from information_schema.routines where routine_type = 'FUNCTION'";
                    break;
                case "table":
                    desiredObjectType = "tables";
                    break;
                case "gcfg0":
                    commandRoot = "";
                    dbCxnStg = ConfigurationManager.AppSettings.Get("sqlCxn0d");
                    desiredObjectType = ConfigurationManager.AppSettings.Get("gcfg0");
                    break;
                case "gcfg2":
                    commandRoot = "";
                    dbCxnStg = ConfigurationManager.AppSettings.Get("sqlCxn2");
                    desiredObjectType = ConfigurationManager.AppSettings.Get("gcfg2");
                    break;
                case "gcfg02":
                    commandRoot = "";
                    dbCxnStg = ConfigurationManager.AppSettings.Get("sqlCxn2");
                    desiredObjectType = ConfigurationManager.AppSettings.Get("gcfg02");
                    break;
                default:
                    dtype = "index";
                    desiredObjectType = "indexes";
                    break;
            }
            string sqlCmdText = commandRoot + desiredObjectType;
            return sqlCmdText;
        }

        private static bool RunSqlCmd(string dbCxnStg, string sqlCmdText, string dtype, StringWriter sw) {
            bool dataError = false;
            try {
                using (SqlConnection cxn = new SqlConnection(dbCxnStg)) {
                    cxn.Open();
                    SqlCommand cmd = new SqlCommand(dbCxnStg);
                    cmd.CommandText = sqlCmdText;
                    cmd.Connection = cxn;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataTable dt = new DataTable(dtype);
                    da.Fill(dt);
                    dt.WriteXml(sw, true);
                }
            }
            catch (Exception exc) {
                dataError = true;
                string excStg = "{\"exception\":\"" + exc.Message + "\"}";
                XmlDocument doc = JsonConvert.DeserializeXmlNode(excStg);
                sw.Write(doc.OuterXml);
            }
            return dataError;
        }
        private DataTable RunSqlCmd(string cxnStg, string sqlCmdText) {
            bool dataError = false;
            try {
                using (SqlConnection cxn = new SqlConnection(cxnStg)) {
                    cxn.Open();
                    SqlCommand cmd = new SqlCommand(cxnStg);
                    cmd.CommandText = sqlCmdText;
                    cmd.Connection = cxn;
                    using (SqlDataAdapter da = new SqlDataAdapter()) {
                        da.SelectCommand = cmd;
                        DataTable dt = new DataTable("responseTable");
                        da.Fill(dt);
                        return dt;
                    }
                }
            }
            catch (Exception exc) {
                dataError = true;
                string excStg = "{\"exception\":\"" + exc.Message + "\"}";
                Debug.WriteLine(excStg);
            }
            return null;
        }


        private int GetColumnsInfo(string cxnStg, string sqlCmdText, ref SqlDbType[] items) {
            bool dataError = false;
            int rval = -1;
            try {
                string[] columnTypes;
                using (SqlConnection cxn = new SqlConnection(cxnStg)) {
                    cxn.Open();
                    SqlCommand cmd = new SqlCommand(sqlCmdText, cxn);
                    using (SqlDataReader dr = cmd.ExecuteReader()) {
                        dr.Read();
                        columnTypes = new string[dr.FieldCount];
                        for (int i = 0; i < dr.FieldCount; i++) {
                            columnTypes[i] = dr.GetDataTypeName(i);
                        }
                    }
                }
                MapDataTypes(columnTypes, ref items);
            }
            catch (Exception exc) {
                dataError = true;
                string excStg = "{\"exception\":\"" + exc.Message + "\"}";
                Debug.WriteLine(excStg);
                rval = -2;
            }
            return rval;
        }


        SqlDbType[] insightMetaDataColumnTypes;
        //= {
        //                SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
        //                SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.NVarChar, SqlDbType.DateTime
        //            };

        private void MapDataTypes(string[] dataTypes, ref SqlDbType[] dbTypes) {
            dbTypes = new SqlDbType[dataTypes.Length];
            for (int i = 0; i < dataTypes.Length; i++) {
                switch (dataTypes[i]) {
                    case "int":
                        dbTypes[i] = SqlDbType.Int;
                        break;
                    case "varchar":
                        dbTypes[i] = SqlDbType.VarChar;
                        break;
                    case "nvarchar":
                        dbTypes[i] = SqlDbType.NVarChar;
                        break;
                    case "datetime":
                        dbTypes[i] = SqlDbType.DateTime;
                        break;
                }
            }
        }

        private int RunSqlCmd(string cxnStg, string sqlCmdText, object[] items) {
            bool dataError = false;
            int rval = -1;
            try {
                using (SqlConnection cxn = new SqlConnection(cxnStg)) {
                    cxn.Open();
                    SqlCommand cmd = new SqlCommand(sqlCmdText, cxn);
                    using (SqlDataAdapter da = new SqlDataAdapter()) {
                        int idx = 0;
                        foreach (object obj in items) {
                            if (idx != 0) {  // skip auto unique key
                                string pname = $"@p{idx}";
                                cmd.Parameters.Add(pname, insightMetaDataColumnTypes[idx]);
                                cmd.Parameters[pname].Value = obj;
                            }
                            idx++;
                        }
                        rval = cmd.ExecuteNonQuery();

                    }
                }
            }
            catch (Exception exc) {
                dataError = true;
                string excStg = "{\"exception\":\"" + exc.Message + "\"}";
                Debug.WriteLine(excStg);
                rval = -2;
            }
            return rval;
        }

        private static string RunRowCount(string dbCxnStg, string tableName) {
            string result = "";
            try {
                using (SqlConnection cxn = new SqlConnection(dbCxnStg)) {
                    cxn.Open();
                    SqlCommand cmd = new SqlCommand(dbCxnStg);
                    cmd.CommandText = $"select count(*) from {tableName}";
                    cmd.Connection = cxn;
                    int rval = (int)cmd.ExecuteScalar();
                    result = rval.ToString();
                }
            }
            catch (Exception exc) {
                result = $"exception : {exc.Message}";
            }
            return result;
        }

        #region dsTools
        public static string DatasetToXml(DataSet ds) {
            using (var memoryStream = new MemoryStream()) {
                using (TextWriter streamWriter = new StreamWriter(memoryStream)) {
                    var xmlSerializer = new XmlSerializer(typeof(DataSet));
                    xmlSerializer.Serialize(streamWriter, ds);
                    return Encoding.UTF8.GetString(memoryStream.ToArray());
                }
            }
        }

        public void FinalizeResponseHeaders(ProcessingArgs pa, HttpResponseMessage response, string contentType = null) {
            ManageOrigin(pa, Request, response);
            IEnumerable<string> values;
            if (!string.IsNullOrEmpty(pa["cassbuiltqry"])) {
                response.Headers.Add("cassbuiltqry", pa["cassbuiltqry"]);
            }
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType ?? "application/xml");
            if (Request.Headers.TryGetValues("X-BIO-timeStamp", out values)) {
                response.Headers.Add("X-BIO-timeStamp", values.FirstOrDefault());
                response.Headers.Add("X-BIO-timeStampStop", DateTime.Now.ToString());
            }
        }

        public static void FinalizeResponseHeaders(ProcessingArgs pa, HttpResponseMessage response, HttpRequestMessage request, string contentType = null) {
            ManageOrigin(pa, request, response);
            IEnumerable<string> values;
            if (!string.IsNullOrEmpty(pa["cassbuiltqry"])) {
                response.Headers.Add("cassbuiltqry", pa["cassbuiltqry"]);
            }
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType ?? "application/xml");
            if (request.Headers.TryGetValues("X-BIO-timeStamp", out values)) {
                response.Headers.Add("X-BIO-timeStamp", values.FirstOrDefault());
                response.Headers.Add("X-BIO-timeStampStop", DateTime.Now.ToString());
            }
        }
        #endregion

        [HttpPost]
        public HttpResponseMessage AclActionsTable(int id) {
            string csvIn = Request.Content.ReadAsStringAsync().Result;
            ProcessingArgs pa = new ProcessingArgs();

            IEnumerable<KeyValuePair<string, string>> qparams = Request.GetQueryNameValuePairs();
            pa["queryselect"] = CreateQueryString(qparams, pa);
            pa["queryJsonParams"] = CreateJsonString(qparams, pa);

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            ManageOrigin(pa, Request, response);

            const char elemSepChar = '|';
            StringBuilder sb = new StringBuilder();
            StringWriter logData = new StringWriter(sb);
            try {
                using (StreamReader sr = new StreamReader(new FileStream(csvIn, FileMode.Open))) {
                    string headerLine = sr.ReadLine();
                    string[] colNames = headerLine.Split(elemSepChar);
                    int numCols = colNames.Length;
                    DataTable dt = new DataTable("ActionTable");
                    foreach (string colName in colNames) {
                        dt.Columns.Add(new DataColumn(colName, typeof(string)));
                    }
                    string rowStg;
                    while ((rowStg = sr.ReadLine()) != null) {
                        object[] rowCols = rowStg.Split(elemSepChar);
                        dt.Rows.Add(rowCols);
                    }
                    logData.WriteLine($" table '{dt.TableName}' has {dt.Columns.Count} columns  ");

                    foreach (string colName in colNames) {
                        IEnumerable<string> uniqueIds = (from row in dt.AsEnumerable()
                                                         select row.Field<string>(colName)).Distinct();
                        logData.WriteLine($" col '{colName}' has {uniqueIds.Count()} unique entries out of a total of {dt.Rows.Count} rows ");
                    }


                    string cfgTempStg;
                    if ((cfgTempStg = ConfigurationManager.AppSettings.Get("actionQueryText1")) != null) {
                        string queryTxt = cfgTempStg;
                        if ((cfgTempStg = ConfigurationManager.AppSettings.Get("actionQueryCols1")) != null) {
                            object[] cols = cfgTempStg.Split(',');
                            queryTxt = string.Format(queryTxt, cols);
                        }
                        DataRow[] rows = dt.Select(queryTxt);
                        logData.WriteLine($" query '{queryTxt}' has {rows.Count()} rows ");

                    }

                    GetCustomerInfo(dt, logData);
                    GetPremiseInfo(dt, logData);


                    logData.Flush();
                    response.Content = new StringContent(sb.ToString());
                }
            }
            catch (Exception exc) {
                string excStg = "{\"exception\":\"" + exc.Message + "\"}";
                response.Content = new StringContent(excStg);
            }
            FinalizeResponseHeaders(pa, response, "text/plain");
            return response;
        }

        #region customerInfo
        private static void GetCustomerInfo(DataTable dt, StringWriter logData) {
            int totalCount = 0;
            int g1Count = 0;
            IEnumerable<string> ids = (from row in dt.AsEnumerable()
                                       select row.Field<string>("customerid")).Distinct();

            foreach (string id in ids) {
                var res = from row in dt.AsEnumerable()
                          where row.Field<string>("customerid") == id
                          select row;
                totalCount += res.Count();
                if (res.Count() > 1) {
                    string ownerIdList = "";
                    foreach (DataRow g1row in res) {
                        string ownerId = (string)g1row["accountid"];
                        if (string.IsNullOrEmpty(ownerIdList)) {
                            ownerIdList = ownerId;
                        }
                        else {
                            if (!ownerIdList.Contains(ownerId)) {
                                ownerIdList += "," + ownerId;
                            }
                        }
                    }
                    logData.WriteLine($" for customer {id} account '{ownerIdList}' has {res.Count()} accounts ");
                    g1Count += res.Count();
                }
            }
            logData.WriteLine($" customer count is {totalCount}  ");
            logData.WriteLine($" customer g1 count is {g1Count}  ");
        }

        private static void GetPremiseInfo(DataTable dt, StringWriter logData) {
            int totalCount = 0;
            int g1Count = 0;
            IEnumerable<string> premiseIds = (from row in dt.AsEnumerable()
                                              select row.Field<string>("premiseid")).Distinct();

            foreach (string pid in premiseIds) {
                var res = from row in dt.AsEnumerable()
                          where row.Field<string>("premiseid") == pid
                          select row;
                totalCount += res.Count();
                if (res.Count() > 1) {
                    string custidList = "";
                    foreach (DataRow g1row in res) {
                        string custid = (string)g1row["customerid"];
                        if (string.IsNullOrEmpty(custidList)) {
                            custidList = custid;
                        }
                        else {
                            if (!custidList.Contains(custid)) {
                                custidList += "," + custid;
                            }
                        }
                    }
                    logData.WriteLine($" for premise {pid} customer '{custidList}' has {res.Count()} premises ");
                    g1Count += res.Count();
                }
            }
            logData.WriteLine($" premise count is {totalCount}  ");
            logData.WriteLine($" premise g1 count is {g1Count}  ");
        }
        #endregion

        [HttpPost]
        public HttpResponseMessage Data2Xml() {
            string jsonIn = Request.Content.ReadAsStringAsync().Result;
            ProcessingArgs pa = new ProcessingArgs();
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            ManageOrigin(pa, Request, response);
            IEnumerable<string> values;
            if (Request.Headers.TryGetValues("X-BIO-timeStamp", out values)) {
                response.Headers.Add("X-BIO-timeStampStart", DateTime.Now.ToString());
            }

            IEnumerable<KeyValuePair<string, string>> qparams = Request.GetQueryNameValuePairs();
            //string qstg = "";
            //qstg = CreateQueryString(qparams, pa);
            pa["queryselect"] = CreateQueryString(qparams, pa);
            pa["queryJsonParams"] = CreateJsonString(qparams, pa);


            try {
                XmlDocument doc = JsonConvert.DeserializeXmlNode(jsonIn);
                response.Content = new StringContent(doc.OuterXml);
            }
            catch (Exception exc) {
                string excStg = "{\"exception\":\"" + exc.Message + "\"}";
                XmlDocument doc = JsonConvert.DeserializeXmlNode(excStg);
                response.Content = new StringContent(doc.OuterXml);
            }

            FinalizeResponseHeaders(pa, response);
            return response;
        }

        #region webProcessing
        private static void ManageOrigin(ProcessingArgs pa, HttpRequestMessage request, HttpResponseMessage response) {
            string reqHost = request.Headers.Host;
            IEnumerable<string> values;
            if (pa["OriginHosts"] != null) {
                // we're dealing with multiple sites as well as nginx altered routes so indicate allowed sites 
                string[] allowedHosts = pa["OriginHosts"].Split(',');
                string allowHost = "http://localhost"; // default if nothing implicit or explicit
                if (allowedHosts.Contains(reqHost)) {
                    string useScheme = request.RequestUri.Scheme;
                    if (request.Headers.TryGetValues("X-http-scheme", out values)) {
                        useScheme = values.FirstOrDefault();
                    }
                    allowHost = $"{useScheme}://{reqHost}";
                    if (request.Headers.Referrer != null) {
                        // nginx forwarded or some other override
                        allowHost = $"{request.Headers.Referrer.Scheme}://{request.Headers.Referrer.Authority}";
                    }
                }
                response.Headers.Add("Access-Control-Allow-Origin", allowHost);
                response.Headers.Add("X-ACAO", allowHost);
                // so we can see what it did since ACAO doesnt make it through to the chrome debugger
            }
        }
        #endregion


        //[HttpGet]
        //[Route("DataDetails/{id}")]
        public HttpResponseMessage GetDataDetails(int? id) {
            ProcessingArgs pa = new ProcessingArgs();
            pa["select"] = "DistinctEID";  // some initial defaults so if nothing else is said something useful should happen.
            pa["usecassandra"] = "1";
            pa["dowrite"] = "1";


            IEnumerable<KeyValuePair<string, string>> qparams = Request.GetQueryNameValuePairs();
            pa["queryselect"] = CreateQueryString(qparams, pa);
            pa["queryJsonParams"] = CreateJsonString(qparams, pa);

            StringBuilder sb = new StringBuilder();
            JToken json = JObject.Parse("{\"no\":\"data\"}");
            try {
                using (StringWriter tw = new StringWriter(sb)) {
                    //mp.Process(jsonArgs, tw);
                    tw.Write(JsonConvert.SerializeObject(json));  // hack in since process gone
                    tw.Flush();
                    json = JObject.Parse(sb.ToString());
                }
            }
            catch (Exception exc) {
                string excStg = "{\"exception\":\"" + exc.Message + "\"}";
                json = JObject.Parse(excStg);
            }

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(json.ToString());
            FinalizeResponseHeaders(pa, response, "application/json");
            return response;
        }

        #region stringManipulation

        // TODO this stuff goes in config
        // hack for "key>='val'" ops ( other than basic = which trnaslates cleanly) in qparam
        readonly List<string> opsInQParams = new List<string> { "<", ">", "=", ">=", "<=" };
        readonly List<string> xferParams = new List<string> { "goal", "cols", "orderby", "qlimit" };  // transfer these directly
        readonly List<string> eatItInQParams = new List<string> { "bustCache" };  // throw these away (e.g., chrome anti-cache qparam)


        private string CreateQueryString(IEnumerable<KeyValuePair<string, string>> qparams, ProcessingArgs pa) {
            string qstg = "";
            foreach (var kvp in qparams) {
                if (eatItInQParams.Contains(kvp.Key)) {
                    // eat it
                }
                else if (xferParams.Contains(kvp.Key)) {
                    pa[kvp.Key] = kvp.Value;
                }
                else {
                    if (qstg.Length != 0) qstg += " and ";
                    bool done = false;
                    foreach (string op in opsInQParams) {
                        if (kvp.Key.Contains(op)) {
                            qstg += $"{kvp.Key}";
                            done = true;
                            break;
                        }
                    }
                    if (!done) {
                        qstg += $"{kvp.Key}='{kvp.Value}'";
                    }
                }
            }
            return qstg;
        }
        private string CreateJsonString(IEnumerable<KeyValuePair<string, string>> qparams, ProcessingArgs pa) {
            try {
                JArray ja = new JArray();
                foreach (var kvp in qparams) {
                    string key = kvp.Key;
                    string val = kvp.Value;
                    JObject j = new JObject {
                        {key, new JValue(val)}
                    };
                    ja.Add(j);
                }
                JObject jo = new JObject {
                    {"qparams", ja}
                };
                return jo.ToString(Formatting.None);
            }
            catch (Exception exc) {
                JObject jo = new JObject {
                    {"qparams", exc.Message}
                };
                return jo.ToString(Formatting.None);
            }
        }
        #endregion



    }
}
