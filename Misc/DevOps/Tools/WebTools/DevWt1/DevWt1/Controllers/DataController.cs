﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using AO.TimeSeriesProcessing;
using Newtonsoft.Json;

namespace DevWt1.Controllers
{
    /// <summary>
    /// Data access API
    /// </summary>
    public class DataController : ApiController
    {
        // GET: api/Data
        /// <summary>
        /// default get
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> Get()
        {
            return new[] { "value1", "value2" };
        }

        // GET: api/Data/5
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string Get(int id)
        {
            return "value";
        }


        private string _myEtag = "";  // temporary for single user

        private static string UniqueNameFrag() {
            return DateTime.Now.ToString("MMddyyyy-HHmmss.ffff");
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="vsrc"></param>
        /// <param name="venv"></param>
        /// <param name="vval"></param>
        /// <param name="vanal"></param>
        /// <returns></returns>
        public HttpResponseMessage PutStg(string vsrc, string venv, string vval, string vanal) {
            ProcessingArgs pa = new ProcessingArgs();
            pa["bodyOverride"] = "yes";
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            ContextData.InitContextStrings(vsrc, venv, vval, pa,Request);
            ProcessingArgs.AccumulateRequestPars(Request, vsrc, venv, vval, pa);
            ContextData contextData = new ContextData(pa);
            pa["webEnv"] = venv;
            try {
                //pa["reqContent"] = Request.Content.ReadAsStringAsync().Result;
                pa["reqVanal"] = vanal;
                switch (vanal) {
                    case "batch":
                        BatchUtils.AnalysisBatch(contextData, pa, Request, response);
                        break;
                    case "validate":
                        pa["myEtag"] = _myEtag;
                        AnalysisUtils.AnalysisValidate(contextData, pa, Request, response);
                        break;
                    default:
                        AnalysisUtils.AnalysisBasic(contextData, pa, Request, response);
                        break;
                }
            }
            catch (Exception exc) {
                contextData.ContextWrite("DataController.PutStg exception", $"{exc.Message}, {exc.InnerException?.Message}");
                response.Content = DataUtils1.GetExceptionResponse("PutStg", pa, new[] {exc.Message, venv, vval}, exc);
            }
            finally {
                contextData.End();
            }
            return response;
        }








        /// <summary>
        /// updated GET
        /// </summary>
        /// <param name="vsrc"></param>
        /// <param name="venv">optional env to use [PERF (default), UAT, ...]</param>
        /// <param name="vval">optional level value [ERROR, INFO, WARN, etc.]</param>
        /// <param name="vanal"></param>
        /// <returns></returns>
        public HttpResponseMessage GetStg(string vsrc, string venv, string vval, string vanal) {
            ProcessingArgs pa = new ProcessingArgs();
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            ContextData.InitContextStrings(vsrc, venv, vval, pa,Request);
            ProcessingArgs.AccumulateRequestPars(Request, vsrc, venv, vval, pa);
            ContextData contextData = new ContextData(pa);
            try {
                switch (vsrc) {
                    case "proccache": {
                        XmlDocument cacheXml = contextData.ResultsCache.AsXml();
                        XmlNode retXml = cacheXml;
                        if (!string.IsNullOrEmpty(venv)) {
                            string xpq = $"//item[tag='{venv}']";
                            XmlNode xe = retXml.SelectSingleNode(xpq);
                            if (xe != null) {
                                retXml = xe;
                            }
                        }
                        bool isJson = !string.IsNullOrEmpty(pa["contentType"]) && (pa["contentType"].ToLower() == "application/json");
                        if (isJson) {
                            response.Content = new StringContent(JsonConvert.SerializeObject(retXml));
                        }
                        else {
                            response.Content = new StringContent(retXml.OuterXml);
                        }
                        HttpUtils.FinalizeResponseHeaders(pa, response, Request, contextData, pa["contentType"]);
                    }
                        break;
                    default:
                        ProcessDataGet(contextData, vsrc, venv, vval, pa, response);
                        break;
                }
            }
            catch (Exception exc) {
                contextData.ContextWrite("DataController.ProcessDataGet exception", $"{exc.Message}, {exc.InnerException?.Message}");
                response.Content = DataUtils1.GetExceptionResponse("GetStg", pa, new[] { exc.Message, venv, vval }, exc);
            }
            finally {
                contextData.End();
            }
            return response;
        }



        private void ProcessDataGet(ContextData contextData, string vsrc, string venv, string vval, ProcessingArgs pa, HttpResponseMessage response) {
            try {
                pa["reqContent"] = "";
                pa["X-BIO-etag"] = _myEtag;
                List<DataTable> queryResultTables = new List<DataTable>();
                DataUtils1.LoadQueryData(contextData, pa, queryResultTables, true);
                contextData.ContextWrite("etag", _myEtag);
                contextData.ContextWrite("RequestUri", Request.RequestUri.ToString());
                contextData.ContextWrite("RequestHeaders", Request.Headers);
                contextData.ContextWriteBulk("RequestContent", pa["reqContent"]);
                contextData.ContextWrite("ResponseHeaders", response.Headers);
                string contextReturn = pa["contextRet"] ?? "0";
                if (contextReturn.Equals("1") && response != null) {
                    response.Content = new StringContent(contextData.Snag());
                }
            }
            catch (Exception exc) {
                contextData?.ContextWrite("DataController.ProcessDataGet exception", $"{exc.Message}, {exc.InnerException?.Message}");
                response.Content = DataUtils1.GetExceptionResponse("GetStg", pa, new[] { exc.Message, vsrc, venv, vval }, exc);
            }
        }






        // POST: api/Data
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Data/5
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Data/5
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
        }
    }



}
