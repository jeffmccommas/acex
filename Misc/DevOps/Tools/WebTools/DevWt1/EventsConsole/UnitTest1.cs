﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using AO.TimeSeriesProcessing;
using Cassandra;
using Xunit;

//using Xunit;

namespace AO.DiagnosticsConsole {
    public class UnitTest1 {

        /// <summary>
        /// run 'n' processing tests wich involve extracting series data.
        /// Validates the processing paths through the series management code and database.
        /// </summary>
        [Fact]
        public void TestSeriesExtract() {
            EventsConsole.EventsConsole evc = new EventsConsole.EventsConsole();
            foreach (string stg in testList) {
                string[] rVals = ServicesBroker.TestProcess(stg.Split(' '));
                BasicAsserts(rVals);
            }
        }

        // NOTE: in the below, change the numeric collid to one that exists in the system being tested.
        private string[] testList = {
            "oper=tseries procLimit=50  doAsyncBatch=1 aclenv=work collid=collectionStarterId dtStt=1/1/2017 dtstp=1/4/2017",
            "oper=tseries procLimit=50  doAsyncBatch=1 aclenv=work collid=011006280 dtStt=1/23/2017 dtstp=2/25/2017",
            "oper=tseries procLimit=50  doAsyncBatch=1 aclenv=work collid=011006280 dtStt=1/23/2017 dtstp=3/25/2017",
            "oper=recallseries aclenv=work dtStt=2017-01-01T00:00:00Z dtStp=2017-03-01T00:25:00Z collid=011051563 gasync=0 Accept=text/csv"
        };

        [Fact]
        public void TestStasticsExtract() {
            EventsConsole.EventsConsole evc = new EventsConsole.EventsConsole();
            ProcessingArgs pa = new ProcessingArgs(testList[0].Split(' '));
            if (pa["sqlMeterMgtCxnStg"] == null) {
                pa["sqlMeterMgtCxnStg"] = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
            }
            ContextData.InitContextStrings("evc", pa["aclenv"], "none", pa, null);
            ProcessingArgs.AccumulateRequestPars("evc", pa["aclenv"], "none", pa);
            ContextData context = new ContextData(pa);

            SeriesCollectionSpecification collSpec = new SeriesCollectionSpecification(pa);
            collSpec.InitializeCache(pa);
            SeriesSpecification seriesSpec = new SeriesSpecification(pa, null as DataRow);
            string res = collSpec.Statistics(pa);
            bool isOk = res.IndexOf("count=") >= 0;
            Assert.Equal(isOk, true);

            res = seriesSpec.Statistics(pa);
            isOk = res.IndexOf("count=") >= 0;
            Assert.Equal(isOk, true);


        }

        /// <summary>
        /// Test importing and retrieving series data into cassandra.
        /// This validates the series management sql and cassandra access paths.
        /// ingests a record with estimate indication and verifies.
        /// Updates the record with no estimate indication and verifies.
        /// </summary>
        [Fact]
        public void TestCassImport() {
            EventsConsole.EventsConsole evc = new EventsConsole.EventsConsole();

            CassConn1 cc1 = new CassConn1();
            ProcessingArgs pa = new ProcessingArgs(testList[0].Split(' '));
            DateTime dtStt = DateTime.Now;
            Console.WriteLine($"Starting at {dtStt}");
            if (pa["sqlMeterMgtCxnStg"] == null) {
                pa["sqlMeterMgtCxnStg"] = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
            }
            ContextData.InitContextStrings("evc", pa["aclenv"], "none", pa, null);
            ProcessingArgs.AccumulateRequestPars("evc", pa["aclenv"], "none", pa);
            ContextData context = new ContextData(pa);

            cc1.Connect(pa);

            InsertCassandraDataTest(cassImp1);
            var rs = RecallCassandraDataTest(pa, cc1);

            // now validate content
            foreach (Row row in rs) {
                double val = (double)row["val"];
                Assert.Equal(val,0);
                IDictionary<string, string> map = (IDictionary<string, string>) row["meta"];
                Assert.Equal(map["estInd"],"Yes");
            }

            InsertCassandraDataTest(cassImp2);
            rs = RecallCassandraDataTest(pa, cc1);

            // now validate content
            foreach (Row row in rs) {
                double val = (double)row["val"];
                Assert.Equal(val, 50);
                IDictionary<string, string> map = (IDictionary<string, string>)row["meta"];
                Assert.Equal(map["estInd"], "No");
            }

        }

        private string cassImp1 = "c,s,v\n10000000,201701,2017-01-01 00:00:00Z,0,0,(*( 'estDesc':'D2NO' (*)'estInd':'Yes' )*)";
        private string cassImp2 = "c,s,v\n10000000,201701,2017-01-01 00:00:00Z,50,0,(*( 'estDesc':'D2NO' (*)'estInd':'No' )*)";
        private string cassQrySer = "";

        private static RowSet RecallCassandraDataTest(ProcessingArgs pa, CassConn1 cc1) {
            pa["qRows"] = "*";
            pa["reqVsrc"] = "value_series_meta";
            pa["maxrows"] = "2";
            pa["allowFilter"] = "";
            string selStg = "where id = 10000000 and did = 201701";
            pa["qSel"] = selStg;
            RowSet rs = cc1.PerformCasQuery(pa, ref selStg);
            Assert.Equal(rs.GetAvailableWithoutFetching(), 1);
            return rs;
        }

        // helper to ingest a file and validate the ingestion succeeded (doesn't validate data).
        private void InsertCassandraDataTest(string dat) {
            string tmpFile = ".\\EvcTestFile.csv";
            StreamWriter sw = new StreamWriter(tmpFile);
            sw.Write(dat);
            sw.Close();
            string stg = $"oper=cassimport procLimit=5000000 importfile={tmpFile} doAsyncBatch=1 aclenv=work";
            string[] rVals = ServicesBroker.TestProcess(stg.Split(' '));
            BasicAsserts(rVals);
        }



        /// <summary>
        /// Basic processing test checks against processing log.
        /// </summary>
        /// <param name="rVals"></param>
        void BasicAsserts(string[] rVals) {
            Assert.Equal(rVals[4].IndexOf("**error"), -1);  // look for any errors.
            Assert.Equal(rVals[4].IndexOf("ctxFile") > 0, true);  // sanity check we actually have expected output
        }


    }
}
