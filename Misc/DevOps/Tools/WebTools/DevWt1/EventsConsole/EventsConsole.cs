﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AO.TimeSeriesProcessing;

//using AWSHostProvider;

namespace EventsConsole {
    public class EventsConsole {


        public static void Main(string[] args) {
            MainProcess(args);
        }

        private static void MainProcess(string[] args) {
            ProcessingArgs pa = new ProcessingArgs(args);
            pa["bodyOverride"] = "yes";
            Console.WriteLine($"Starting at {DateTime.Now}");
            if (pa["sqlMeterMgtCxnStg"] == null) {
                pa["sqlMeterMgtCxnStg"] = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
            }
            ContextData.InitContextStrings("evc", pa["aclenv"], "none", pa, null);
            ProcessingArgs.AccumulateRequestPars("evc", pa["aclenv"], "none", pa);
            ContextData context = new ContextData(pa);
            DateTime dtStt = DateTime.Now;
            string todo = pa["oper"] ?? "nada";
            switch (todo) {
                case "dumpconfig": {
                    Console.WriteLine(pa.AsJson());
                }
                    break;
                case "hostservice": {
                        SimpleWebServerLocal ws = new SimpleWebServerLocal(pa);
                        for (;;) {
                            Console.WriteLine(" in hostservice and waiting ....");
                            Thread.Sleep(5000);
                        }
                    }
                    break;
                case "splitcsv": {
                        string srcFile = pa.GetControlVal("srcfile", "nada");
                        int maxLines = pa.GetControlVal("maxlines", 10000);
                        using (StreamReader sr = new StreamReader(srcFile)) {
                            string csvLine = sr.ReadLine();
                            int destIdx = 0;
                            int lineCnt = 0;
                            string destFile = $"{srcFile}.{destIdx:D3}.input.csv";
                            StreamWriter sw = new StreamWriter(destFile);
                            sw.WriteLine(csvLine);
                            while (!sr.EndOfStream) {
                                string line = sr.ReadLine();
                                sw.WriteLine(line);
                                if (++lineCnt >= maxLines) {
                                    sw.Flush();
                                    sw.Close();
                                    lineCnt = 0;
                                    destIdx++;
                                    destFile = $"{srcFile}.{destIdx:D3}.input.csv";
                                    sw = new StreamWriter(destFile);
                                    sw.WriteLine(csvLine);
                                }
                            }
                            sw.Close();

                        }
                    }
                    break;

                case "multicsv": {
                        ToolingRequest tr = new ToolingRequest();
                        tr.SubmitBulkDataRequests(pa, context);
                    }
                    break;
                default: {
                        try {
                            context.ContextWrite("MainProcess call ProcessOptions", "Begin");
                            ServicesBroker.ProcessOptions(pa, context, dtStt);
                            context.ContextWrite("MainProcess call ProcessOptions", "End", dtStt);
                        }
                        finally {
                            Console.WriteLine(context.Snag());
                            context.End();
                        }
                    }
                    break;
            }
            DateTime dtNow = DateTime.Now;
            Console.WriteLine($"mainprocess done at {dtNow}, delta = {dtNow.Subtract(dtStt).TotalMilliseconds} ms.");
        }

    }

    class ToolingRequest {
        //private string defToolRoot = "https://localhost/DevWt1/api/Data/consumption_by_meter/prod/4/validate?allowFilter=1&maxRows=300&procLimit=100&sortCol=ami_time_stamp&procResp=1&outputTag=test1&userPath=anon2&contextRecs=25&contextRet=1&expectRows=1&matchtype=get";  // localhost with standalone service.
        private string defToolRoot = "https://localhost/DevWt1/api/Data/consumption_by_meter/prod/4/validate?allowFilter=1&sortCol=ami_time_stamp&procResp=1&outputTag=test1&userPath=anon2&contextRecs=25&contextRet=1&expectRows=1&matchtype=get";  // localhost with standalone service.
        public const string StandardTimeFormat = "yyyy-MM-ddTHH:mm:ss.fffzz00";



        private string _amiReq =
                "/api/current/ami/{0}?dtStt={1}&dtStp={2}&bucketSizeHours={3}&wantMetadata=0&wantCassinfo=0&zmetaQualName=estInd&metaQualVal=yes&requireLogForce=no&seriesSort=desc&seriesValFmt=F3&logCassQueries=1&retdatapa=yes&zspecCassandraKeyspace=not-there&zallowcachecas=no&zresetcache=yes"
            ;

        //private const string CfgReq = "/api/current/config?custom=nyes&oversecj=2216";

        private void InitWebCall(WebClient client) {
            client.Headers["X-BIO-DiagUser"] = "AclaraDiag";
            client.Headers["Accept"] = "application/json";
            client.Headers["Content-Type"] = "text/plain";
        }

        public async Task SubmitBulkDataRequests(ProcessingArgs paIn, ContextData context) {

            string requestBody =

                "#C where client_id={0} and meter_id = '{1}' and ami_time_stamp>'2015-01-01 00:00:00' and ami_time_stamp<'2017-07-01 00:00:00'\n" +
                @"#F C:\_Test\uil_prod\files\uil_distinct_meters_20170529.csv.000.input.csv" + "\n" +
                "#P csv 1 meter_id,ami_time_stamp,consumption 1,DT,,yyyy-MM-ddTHH:mm:ss.fffzz00;\n";


            string bodyOverrideFile = paIn.GetControlVal("bulkReqBody", @".\data\BulkCassExtractBody.txt");
            if (!string.IsNullOrEmpty(bodyOverrideFile) && File.Exists(bodyOverrideFile)) {
                try {
                    using (StreamReader sr = new StreamReader(bodyOverrideFile)) {
                        requestBody = sr.ReadToEnd();
                        context.ContextWrite($"using query body file {bodyOverrideFile} is \n{requestBody}\n");
                    }
                }
                catch (Exception exc) {
                    context.ContextWrite(
                        $"exception getting body file {bodyOverrideFile} is {exc.Message}, inner={exc.InnerException?.Message}");
                }
            }


            //reqMsg.Headers.Add("Content-Type","text/plain");
            //string srcFileName = @"C:\_Test\uil_prod\files\uil_distinct_meters_20170529.csv.000.input.csv";
            string srcFileName = paIn.GetControlVal("srcfile", @"C:\_Test\uil_prod\files\uil_distinct_meters_20170529.csv.000.input.csv");
            System.Collections.Concurrent.ConcurrentBag<string> srcFiles = new System.Collections.Concurrent.ConcurrentBag<string>(new[] { srcFileName });  // start off assuming a file.

            FileAttributes attr = File.GetAttributes(srcFileName);
            //detect whether its a directory or file
            if ((attr & FileAttributes.Directory) == FileAttributes.Directory) {
                IEnumerable<string> fnames = Directory.EnumerateFiles(srcFileName, "*.input.csv");
                srcFiles = new System.Collections.Concurrent.ConcurrentBag<string>();
                foreach (string fname in fnames) {
                    srcFiles.Add(fname);
                }

            }

            bool doParallel = false;
            if (doParallel) {
                Parallel.ForEach(srcFiles, srcFil => {
                    ProcessingArgs pa = new ProcessingArgs(paIn);
                    Thread.Sleep(1000); // let things stabilize -- seems to be a little race somewhere.
                    var result = PerformBulkRequestSegment(pa, srcFil, requestBody);
                });
            }
            else {
                foreach (string srcFil in srcFiles) {
                    ProcessingArgs pa = new ProcessingArgs(paIn);
                    //Thread.Sleep(1000); // let things stabilize -- seems to be a little race somewhere.
                    var result = PerformBulkRequestSegment(pa, srcFil, requestBody);
                }
            }

        }

        private static string _consolePadlock = "ProcDataCachePadlock";


        private async Task<string> PerformBulkRequestSegment(ProcessingArgs pa, string srcFile, string requestBodyStg) {


            HttpRequestMessage reqMsg = new HttpRequestMessage();
            reqMsg.Headers.Add("X-BIO-DiagUser", "AclaraDiag");
            reqMsg.Headers.Add("Accept", "*/*");
            string[] requestBody = requestBodyStg.Split('\n');
            for (int i = 0; i < requestBody.Length; i++) {
                requestBody[i] = requestBody[i].Trim();
            }

            requestBody[1] = $"#F {srcFile}";

            string srcDir = Path.GetDirectoryName(srcFile);

            string destFilename = srcFile + ".results.csv";
            if (File.Exists(destFilename)) {
                bool overwriteFiles = pa.GetControlVal("overwriteFiles", 0) > 0;
                if (overwriteFiles) {
                    Console.WriteLine($" SubmitBulkDataRequests '{destFilename}' exists, overwriteFiles={overwriteFiles}");
                    File.Delete(destFilename);
                }
                else {
                    string cTxt = $" ***** SubmitBulkDataRequests '{destFilename}' exists, overwriteFiles={overwriteFiles} so cancelling. ";
                    Console.WriteLine(cTxt);
                    return cTxt;
                }
            }

            string reqBody = requestBody.Aggregate((current, next) => current + Environment.NewLine + next); ;
            reqMsg.Content = new StringContent(reqBody);
            reqMsg.RequestUri = new Uri(defToolRoot);

            HttpResponseMessage respMsg = new HttpResponseMessage();

            ContextData.InitContextStrings("consumption_by_meter", "prod", "4", pa, reqMsg);
            pa["alreadyExtractedVars"] = "no";
            ProcessingArgs.AccumulateRequestPars(reqMsg, "consumption_by_meter", "prod", "4", pa);
            pa["bodyOverride"] = "yes";

            DateTime dtStt = DateTime.Now;
            ContextData contextData = new ContextData(pa);
            string result = "";
            try {

                if (pa.GetControlVal("showdetail", 0) > 0) {
                    contextData.ContextWriteSeg("Tooling request", true, dtStt);
                    contextData.ContextWriteBulk("config", pa.AsJson());
                }
                AnalysisUtils.AnalysisValidate(contextData, pa, reqMsg, respMsg);
                Byte[] byteArray = await respMsg.Content.ReadAsByteArrayAsync();
                result = Encoding.UTF8.GetString(byteArray);
                string ctxOutputName = contextData.MainStream.FileName;
                string csvP0Name = ctxOutputName + "_raw___P0_csv.csv";
                csvP0Name = pa.GetControlVal("p0filename", csvP0Name);
                bool overwriteFiles = pa.GetControlVal("overwriteFiles", 0) > 0;
                if (File.Exists(destFilename)) {
                    Console.WriteLine(
                        $" SubmitBulkDataRequests '{destFilename}' exists, overwriteFiles={overwriteFiles}");
                    if (overwriteFiles) {
                        File.Delete(destFilename);
                    }
                }
                if (pa.GetControlVal("showdetail", 0) > 0) {
                    contextData.ContextWriteSeg("Tooling request", false, dtStt);
                }
                File.Copy(csvP0Name, destFilename);

                string srcDirName = Path.GetDirectoryName(ctxOutputName);
                string logDirName = Path.GetFileName(ctxOutputName);
                string searchPat = $"{logDirName}*_pproc.csv";
                IEnumerable<string> fnames = Directory.EnumerateFiles(srcDirName, searchPat, SearchOption.AllDirectories);
                List<String> srcFiles = new List<string>();
                foreach (string fname in fnames) {
                    srcFiles.Add(fname);
                    contextData.ContextWrite("found file name", fname);
                    string tName = Path.GetFileName(fname);
                    string tFrag = tName.Substring(logDirName.Length);
                    string newFileName = srcFile + tFrag;
                    contextData.ContextWrite("will use", newFileName);
                    if (overwriteFiles) {
                        File.Delete(newFileName);
                    }
                    File.Copy(fname, newFileName);

                }



            }
            catch (Exception exc) {
                contextData.ContextWrite($" exception in Tooling request  {exc.Message}; inner={exc.InnerException?.Message}");

            }
            contextData.End();

            lock (_consolePadlock) {
                Console.WriteLine($" SubmitBulkDataRequests response\n{result}\n");
            }
            return result;
        }
    }



    public class SimpleWebServerLocal : SimpleWebServer {
        public static void LocalLog(string msg) {
            Console.WriteLine(msg, null);
        }

        public SimpleWebServerLocal(ProcessingArgs pa) : base(pa) {
            SetLogFn(LocalLog);
            CheckEndpoints(pa);
        }


        public void CheckEndpoints(ProcessingArgs pa) {
            var epName = "AdvAmiEP";
            string hostAddr = pa.GetControlVal("hostIp", "127.0.0.1");
            string hostPort = pa.GetControlVal("hostPort", "443");
            string tmpStg = ConfigurationManager.AppSettings["AdvAmiHostAddress"];
            if (!string.IsNullOrEmpty(tmpStg)) {
                hostAddr = tmpStg;
            }
            tmpStg = ConfigurationManager.AppSettings["AdvAmiHostPort"];
            if (!string.IsNullOrEmpty(tmpStg)) {
                hostPort = tmpStg;
            }
            bool cloudConfigSuccess = false;
            try {
                pa["hostIp"] = hostAddr;
                pa["port"] = hostPort;
                InitializeServer(pa);
            }
            catch (Exception e) {
                Trace.TraceError(
                    $"Caught exception in CheckEndpoint SimpleWebServer. {e.Message}, {e.InnerException?.Message} Details: {0}", e);
                Logit($"Event Process Worker  Caught exception in CheckEndpoint SimpleWebServer. {e.Message}, {e.InnerException?.Message}");
            }
        }


    }










}

