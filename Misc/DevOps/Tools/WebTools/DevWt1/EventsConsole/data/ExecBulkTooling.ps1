$localList = Test-Path SrcDirs.txt
$exeDir = "C:\work3\ACEx\17.06\Misc\DevOps\Tools\WebTools\DevWt1\EventsConsole\bin\Debug"
$exe = "EventsConsole.exe"
$bulkReqBodyPath = "data\BulkCassExtractBody.txt"
$cmdLine = "oper=multicsv srcfile=C:\_Test\uil_prod\filesSome\1 maxRows=3000000 procLimit=1000 overwriteFiles=1 showdetail=1"

$sttdir = Get-Location
if (!$localList) {
    $dirs = Get-ChildItem -Directory
}
else {
    $dirs = Get-Content SrcDirs.txt
}
$dirs
$sttdir
$exeLocation = join-path $exeDir $exe
foreach ($dir in $dirs) {
    $outFilPath= join-path $sttdir $dir
    $outFileName = $dir.Name +"_cmdout.txt"
    $outFile= join-path $outFilPath $outFileName
	$bodyFile = join-path $exeDir $bulkReqBodyPath
    $cmdStt=" oper=multicsv srcfile="
    $cmdDataDir = Join-Path $sttdir $dir
    #$cmdEnd=" maxRows=3000000 procLimit=1000000 overwriteFiles=1 showdetail=0 bulkReqBody=" + $bodyFile
    $cmdEnd=" maxRows=3000000 procLimit=10 overwriteFiles=1 showdetail=0 bulkReqBody=" + $bodyFile
    $cmdPars = $cmdStt + $cmdDataDir + $cmdEnd
    $cmdPars
    $outFile
    Set-Location -Path $exeDir
    Get-Location
    start-process $exeLocation -ArgumentList $cmdPars -RedirectStandardOutput $outFile
    Set-Location -Path $sttdir
    Get-Location
    Start-Sleep -s 1
}
