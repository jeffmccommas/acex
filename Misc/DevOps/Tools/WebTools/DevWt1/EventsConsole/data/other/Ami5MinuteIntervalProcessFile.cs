﻿using CE.AO.Models.RawFileModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using CE.AO.Logging;
using CE.AO.Models;
using AO.TimeSeriesProcessing;
using Microsoft.Azure;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage.Blob;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Files
{
    public class Ami5MinuteIntervalProcessFile : ProcessFile
    {
        //public Ami5MinuteIntervalProcessFile(LogModel logModel) : base(logModel) {
        //}

        private LogModel logModel;
        private string fileName;
        private string[] msgParts;
        private string containerName;
        private CloudBlobClient cloudBlobClient;
        private CloudBlockBlob blockBlob;
        bool archiveBlob = true;
        private static SimpleWebServer ws;  // really just for testing restart as we debug this capability for advancedami


        public string LastConvertRoot { get; set; } = "";
        public string LastImportRoot { get; set; } = "";


        /// <summary>
        /// little different than the 'classic' process file stuff.  need more info and don't want to refactor all the old stuff.
        /// </summary>
        /// <param name="_logModel"></param>
        /// <param name="_fileName"></param>
        /// <param name="_msgParts"></param>
        /// <param name="_containerName"></param>
        /// <param name="_cloudBlobClient"></param>
        public Ami5MinuteIntervalProcessFile(LogModel _logModel, string _fileName, string[] _msgParts, string _containerName, CloudBlobClient _cloudBlobClient) : base(_logModel) {
            logModel = _logModel;
            fileName = _fileName;
            msgParts = _msgParts;
            containerName = _containerName;
            cloudBlobClient = _cloudBlobClient;
        }


        /// <summary>
        /// little different than the 'classic' process file stuff.  need more info and don't want to refactor all the old stuff.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="containerName"></param>
        /// <param name="clientId"></param>
        public override void Initialize(string fileName, string containerName, string clientId) {
            var container = cloudBlobClient.GetContainerReference(containerName);
            blockBlob = container.GetBlockBlobReference(fileName);
            if (msgParts.Length > 3) {
                Logger.Info($"Ami5Min additional control info {msgParts[3]}", logModel);
                if (!string.IsNullOrEmpty(msgParts[3]) && msgParts[3].ToLower().Equals("noarchive")) {
                    archiveBlob = false;
                    Logger.Info($"Ami5Min additional control info for archive", logModel);
                }
                if ((msgParts.Length > 4) && !string.IsNullOrEmpty(msgParts[4]) && msgParts[4].ToLower().Equals("restartcomms")) {
                    try {
                        ProcessingArgs pa = new ProcessingArgs();
                        Logger.Info($"Ami5Min restartcomms {msgParts[4]}", logModel);
                        string hostAddr = "127.0.0.1";
                        string hostPort = "4443";
                        string overrideAmi = "";
                        string tmpStg = ConfigurationManager.AppSettings["AdvAmiHostAddress"];
                        if (!string.IsNullOrEmpty(tmpStg)) {
                            hostAddr = tmpStg;
                            Logger.Info($"Ami5Min restartcomms config  AdvAmiHostAddress={hostAddr}", logModel);
                        }
                        tmpStg = ConfigurationManager.AppSettings["AdvAmiHostPort"];
                        if (!string.IsNullOrEmpty(tmpStg)) {
                            hostPort = tmpStg;
                            Logger.Info($"Ami5Min restartcomms config  AdvAmiHostPort={hostPort}", logModel);
                        }
                        tmpStg = ConfigurationManager.AppSettings["overrideAMIPrefix"];
                        if (!string.IsNullOrEmpty(tmpStg)) {
                            overrideAmi = tmpStg;
                            pa["overrideAMIPrefix"] = overrideAmi;
                            Logger.Info($"Ami5Min restartcomms config  overrideAMIPrefix={overrideAmi}", logModel);
                        }
                        if ((msgParts.Length > 5) && !string.IsNullOrEmpty(msgParts[5])) {
                            overrideAmi = msgParts[5];
                            overrideAmi = overrideAmi.Replace(';', ':');
                            pa["overrideAMIPrefix"] = overrideAmi;
                            Logger.Info($"Ami5Min restartcomms message  overrideAMIPrefix={overrideAmi}", logModel);
                        }


                        pa["hostIp"] = hostAddr;
                        pa["port"] = hostPort;
                        Logger.Info($"Ami5Min restartcomms {hostAddr}:{hostPort} override={overrideAmi}", logModel);
                        ws = new SimpleWebServerRole(pa);
                        Logger.Info(
                            $"Event Process Worker (Instance: {RoleEnvironment.CurrentRoleInstance.Id}) CheckEndpoint SimpleWebServer running at 127",
                            null);
                    }
                    catch (Exception e2) {
                        Trace.TraceError($"restartcomms exception Ami5MinuteIntervalProcessFile.Initialize . {e2.Message}, {e2.InnerException?.Message} Details: {0}", e2);
                        Logger.Error($"restartcomms exception Ami5MinuteIntervalProcessFile.Initialize { e2.Message}, { e2.InnerException?.Message}", null);
                    }
                }
            }
            else {
                Logger.Info($"Ami5Min no additional control info", logModel);
            }
            Process05MinAmiFileObject();
        }

        public void ArchiveLogFiles() {
            try {
                string[] fileNames = { LastConvertRoot, LastImportRoot };
                Logger.Info($"Ami5Min archiving logs {LastConvertRoot},{LastImportRoot}", logModel);
                foreach (string name in fileNames) {
                    try {
                        Logger.Info($"Ami5Min try archiving log {name}", logModel);
                        if (string.IsNullOrEmpty(name)) continue;
                        string rootPath = Path.GetDirectoryName(name);
                        string fName = Path.GetFileName(name) + "*";
                        Logger.Info($"Ami5Min archiving log  check {rootPath} for {fName}", logModel);
                        if (Directory.Exists(rootPath)) {
                            IEnumerable<string> fnames = Directory.EnumerateFiles(rootPath, fName);
                            foreach (string fname in fnames) {
                                Logger.Info($"Ami5Min archiving log  file {fname}", logModel);
                                CloudBlobContainer container = CloudBlobClient.GetContainerReference(containerName);
                                CloudBlockBlob logBlockBlob = container.GetBlockBlobReference($"log/advami/{Path.GetFileName(fname)}");
                                using (var fileStream = System.IO.File.OpenRead(fname)) {
                                    logBlockBlob.UploadFromStream(fileStream);
                                }
                            }
                        }
                        else {
                            Logger.Info($"Ami5Min archiving log nodirectory {rootPath}", logModel);
                        }
                    }
                    catch (Exception exc) {
                        string msg = $" Process05MinAmiFileObject ArchiveLogFile '{name}', exception {exc.Message}, {exc.InnerException?.Message}";
                        Logger.Error(msg, exc, logModel);
                        Debug.WriteLine(msg);
                    }
                }
            }
            catch (Exception exc) {
                string msg = $" Process05MinAmiFileObject ArchiveLogFiles, exception {exc.Message}, {exc.InnerException?.Message}";
                Logger.Error(msg, exc, logModel);
                Debug.WriteLine(msg);
            }
        }

        /// <summary>
        /// perform post processing on the import based on what was returned from the advanced ami connector import
        /// </summary>
        /// <param name="impResult"></param>
        private void PostProcessCmds(string impResult) {
            string[] cmdStgs = impResult.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (cmdStgs.Length > 0) {
                int maxCmds = 10;
                foreach (string cmdStg in cmdStgs) {
                    if (maxCmds-- <= 0) break;
                    switch (cmdStg) {
                        // deleted old commands for safety reasons
                        //
                        case "nada": {
                                //SuppressErrors until this is filled back in.
                            }
                            break;
                    }
                }
            }
        }




        /// <summary>
        /// Process the file using the saved parameters
        /// </summary>
        private void Process05MinAmiFileObject() {
            string containerN = (containerName == null) ? "nocontainer" : containerName;
            string info = $"05min  {fileName} container={containerN} ";
            Logger.Info(info, logModel);
            Debug.WriteLine(info);
            string importControl = "enabled";
            string destLocation = CloudConfigurationManager.GetSetting("05MinImportDirectory");
            if (!string.IsNullOrEmpty(destLocation)) {
                Debug.WriteLine($"{info} cfglocation={destLocation}");
                importControl = destLocation;
            }
            bool success = false;
            info = $"05min is {importControl}, {info}";
            if (importControl.Equals("disabled")) {
                Logger.Warn(info, logModel);
                return ;
            }
            Logger.Info(info, logModel);

            try {
                string fnameAdjust = DateTime.Now.ToString("MMddyyyy_HHmmssFFFFFF") + "_";
                string filePath = Path.Combine(importControl, fnameAdjust + fileName);
                info += $" destfile={filePath}";
                StringWriter sw = new StringWriter(); // watch this for huge files -- maybe keep under 500MB?
                List<string> firstFewLines = new List<string>();
                {
                    int maxFirstLines = 10;
                    using (StreamReader reader = new StreamReader(blockBlob.OpenRead())) {
                        while (!reader.EndOfStream) {
                            string row = reader.ReadLine();
                            if (!string.IsNullOrEmpty(row)) {
                                sw.WriteLine(row);
                                if (maxFirstLines-- > 0) {
                                    firstFewLines.Add(row);
                                }
                            }
                        }
                    }
                    sw.Flush();
                    Debug.WriteLine($" 5min finished blob extract {info}");
                    Logger.Info($" 5min finished blob extract {info}", logModel);
                }
                AmiAdvancedConnector advAmiC = new AmiAdvancedConnector();
                string impResult = advAmiC.ImportFile(filePath, 5, logModel, firstFewLines, sw.ToString());
                LastConvertRoot = advAmiC.LastConvertRoot;
                LastImportRoot = advAmiC.LastImportRoot;
                Logger.Info($" 5min finished import {info}", logModel);
                success = true;
                PostProcessCmds(impResult);
            }
            catch (Exception exc) {
                string msg = $" Process05MinAmiFileObject {info}, exception {exc.Message}, {exc.InnerException?.Message}";
                Logger.Fatal(msg, exc, logModel);
                Debug.WriteLine(msg);
            }
            finally {
                if (success && archiveBlob) {
                    ArchieveBlob(blockBlob, fileName, containerName, Enums.EventType.Ami);
                }
                else {
                    Logger.Info($"Ami5Min not archiving blob success={success} archive flag={archiveBlob}", logModel);
                }
                Logger.Info($"Ami5Min go archive logs", logModel);
                ArchiveLogFiles();
            }
        }



    }
}
