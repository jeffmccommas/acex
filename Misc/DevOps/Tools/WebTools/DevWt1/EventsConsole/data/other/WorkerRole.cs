using Microsoft.Azure;
using Microsoft.Azure.WebJobs;
using Microsoft.WindowsAzure.ServiceRuntime;
using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Net;
using System.Threading;
using CE.AO.Logging;
using System.Configuration;
using System.IO;
using System.Text;
using AO.TimeSeriesProcessing;

namespace CE.AO.DataImport.BatchProcessWorker
{

    public class SimpleWebServerRole : SimpleWebServer {
        public new string WhoamiStg() {
            return $"Event Process Worker (Instance: {RoleEnvironment.CurrentRoleInstance.Id})";
        }

        public new void Logit(string msg) {
            Logger.Info(msg, null);
        }

        public void CheckEndpoints(ProcessingArgs pa) {
            var epName = "AdvAmiEP";
            string hostAddr = pa.GetControlVal("hostIp","127.0.0.1");
            string hostPort = pa.GetControlVal("hostPort", "443");
            string tmpStg = ConfigurationManager.AppSettings["AdvAmiHostAddress"];
            if (!string.IsNullOrEmpty(tmpStg)) {
                hostAddr = tmpStg;
            }
            tmpStg = ConfigurationManager.AppSettings["AdvAmiHostPort"];
            if (!string.IsNullOrEmpty(tmpStg)) {
                hostPort = tmpStg;
            }
            bool cloudConfigSuccess = false;

            try {
                var roleInstance = RoleEnvironment.CurrentRoleInstance;
                if ((roleInstance.InstanceEndpoints[epName] != null) && (roleInstance.InstanceEndpoints[epName].PublicIPEndpoint != null)) {
                    var myPublicEp = roleInstance.InstanceEndpoints[epName].PublicIPEndpoint;
                    Trace.TraceInformation("CheckEndpoint public IP:{0}, Port:{1}", myPublicEp.Address, myPublicEp.Port);
                    Logger.Info(
                        $"Event Process Worker CheckEndpoint SimpleWebServer running at public IP:{myPublicEp.Address}, Port:{myPublicEp.Port}",
                        null);
                    hostAddr = myPublicEp.Address.ToString();
                    hostPort = myPublicEp.Port.ToString();
                    Logger.Info($"Event Process Worker CheckEndpoint SimpleWebServer external address info IP:{hostAddr}, Port:{hostPort}", null);

                    var myInternalEp = roleInstance.InstanceEndpoints[epName].IPEndpoint;
                    Trace.TraceInformation("CheckEndpoint internal IP:{0}, Port:{1}", myInternalEp.Address, myInternalEp.Port);
                    hostAddr = myInternalEp.Address.ToString();
                    hostPort = myInternalEp.Port.ToString();

                    Logger.Info($"Event Process Worker CheckEndpoint SimpleWebServer running at internal IP:{myInternalEp.Address}, Port:{myInternalEp.Port}", null);

                    cloudConfigSuccess = true;
                }
                else {
                    Logger.Info($" CheckEndpoint SimpleWebServer no RoleEnvironment ip data.  using {hostAddr}, Port:{hostPort}", null);
                }
            }
            catch (Exception e) {
                Trace.TraceError($"Caught exception in CheckEndpoint. {e.Message}, {e.InnerException?.Message} Details: {0}", e);
                Logger.Error($"Event Process Worker (Instance: {RoleEnvironment.CurrentRoleInstance.Id}) Caught exception in CheckEndpoint. { e.Message}, { e.InnerException?.Message}", null);
            }
            string useUrl = "nada";
            try {
                pa["hostIp"] = hostAddr;
                pa["port"] = hostPort;
                useUrl = InitializeServer(pa);
                Logger.Info($"Event Process Worker (Instance: {RoleEnvironment.CurrentRoleInstance.Id}) CheckEndpoint SimpleWebServer running at '{useUrl}'", null);
            }
            catch (Exception e) {
                Trace.TraceError($"Caught exception in CheckEndpoint SimpleWebServer. {e.Message}, {e.InnerException?.Message} Details: {0}", e);
                Logger.Error($"Event Process Worker (Instance: {RoleEnvironment.CurrentRoleInstance.Id}) Caught exception in CheckEndpoint SimpleWebServer. { e.Message}, { e.InnerException?.Message}", null);
                if (cloudConfigSuccess) {  //maybe the cloud config stuff is intefering, try just the app config
                    tmpStg = ConfigurationManager.AppSettings["AdvAmiHostAddress"];
                    if (!string.IsNullOrEmpty(tmpStg)) {
                        hostAddr = tmpStg;
                    }
                    tmpStg = ConfigurationManager.AppSettings["AdvAmiHostPort"];
                    if (!string.IsNullOrEmpty(tmpStg)) {
                        hostPort = tmpStg;
                    }
                    try {
                        pa["hostIp"] = hostAddr;
                        pa["port"] = hostPort;
                        useUrl = InitializeServer(pa);
                        Logger.Info($"Event Process Worker (Instance: {RoleEnvironment.CurrentRoleInstance.Id}) CheckEndpoint SimpleWebServer running at '{useUrl}'", null);
                    }
                    catch (Exception e2) {
                        Trace.TraceError($"Try2 Caught exception in CheckEndpoint SimpleWebServer. {e2.Message}, {e2.InnerException?.Message} Details: {0}", e2);
                        Logger.Error($"Try2 Event Process Worker (Instance: {RoleEnvironment.CurrentRoleInstance.Id}) '{useUrl}' Caught exception in CheckEndpoint SimpleWebServer. { e2.Message}, { e2.InnerException?.Message}", null);
                    }
                }
            }

        }

        public static void LocalLog(string msg) {
            Logger.Info(msg, null);
        }

        public SimpleWebServerRole(ProcessingArgs pa) : base(pa) {
            SetLogFn(LocalLog);
            CheckEndpoints(pa);
        }



    }


    public class WorkerRole : RoleEntryPoint, IDisposable
    {
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private ManualResetEvent _runCompleteEvent = new ManualResetEvent(false);

        public override void Run()
        {
            Trace.TraceInformation("CE.AO.DataImport.BatchProcessWorker is running");
            ProcessingArgs pa = new ProcessingArgs();
            SimpleWebServer sws = new SimpleWebServerRole(pa);

            try
            {
                if (!_cancellationTokenSource.Token.IsCancellationRequested)
                {
                    AutoMapperConfig.AutoMapperMappings();
                    var storageConn = CloudConfigurationManager.GetSetting(StaticConfig.AclaraOneStorageConnectionString);
                    var config = new JobHostConfiguration { NameResolver = new QueueNameResolver() };

                    config.Queues.BatchSize = 8; //(default is 16).
                    config.Queues.MaxDequeueCount = 5;  //(default is 5).
                    config.Queues.MaxPollingInterval = TimeSpan.FromSeconds(15); //(default is 1 minute).
                    config.StorageConnectionString = storageConn;
                    config.DashboardConnectionString = storageConn;

                    var host = new JobHost(config);
                    host.RunAndBlock();
                }

                //Wait for shutdown to be called, else the role will recycle
                _runCompleteEvent.WaitOne();
            }
            finally
            {
                _runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.
            string useAmiLog = ConfigurationManager.AppSettings["useAmiLocalResource"];
            if (!string.IsNullOrEmpty(useAmiLog) && useAmiLog.Equals("yes")) {
                string customTempLocalResourcePath = RoleEnvironment.GetLocalResource("TempAMILogStore").RootPath;
                Environment.SetEnvironmentVariable("TMP", customTempLocalResourcePath);
                Environment.SetEnvironmentVariable("TEMP", customTempLocalResourcePath);
            }

            var result = base.OnStart();

            Trace.TraceInformation("CE.AO.DataImport.BatchProcessWorker has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("CE.AO.DataImport.BatchProcessWorker is stopping");

            _cancellationTokenSource.Cancel();
            _runCompleteEvent.WaitOne();

            Logger.Fatal($"Event Process Worker (Instance: {RoleEnvironment.CurrentRoleInstance.Id}) stopped", null);

            base.OnStop();

            Trace.TraceInformation("CE.AO.DataImport.BatchProcessWorker has stopped");
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_cancellationTokenSource != null)
                {
                    _cancellationTokenSource.Cancel();
                    _cancellationTokenSource.Dispose();
                    _cancellationTokenSource = null;
                }
                if (_runCompleteEvent != null)
                {
                    _runCompleteEvent.Dispose();
                    _runCompleteEvent = null;
                }
            }
        }
    }
}
