﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using CsvHelper;
using Microsoft.Azure;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Files
{
    /// <summary>
    /// ProcessFile abstract class which is inherited by all other classes which processes specific files
    /// </summary>
    public abstract class ProcessFile
    {
        /// <summary>
        /// Parameterized contructor
        /// </summary>
        /// <param name="logModel">Used for logging details in case of any error</param>
        protected ProcessFile(LogModel logModel)
        {
            LogModel = logModel;
        }

        /// <summary>
        /// Initialize processing of file abstract method
        /// </summary>
        /// <param name="fileName">filename of file to process</param>
        /// <param name="containerName">blob container name</param>
        /// <param name="clientId">client id</param>
        public abstract void Initialize(string fileName, string containerName, string clientId);

        /// <summary>
        /// Used for logging details in case of any error
        /// </summary>
        public LogModel LogModel { get; set; }
        
        /// <summary>
        /// CloudBlobClient private variable
        /// </summary>
        private CloudBlobClient _cloudBlobClient;
        public CloudBlobClient CloudBlobClient
        {
            get
            {
                if (_cloudBlobClient != null) return _cloudBlobClient;
                var storageAccount =
                    CloudStorageAccount.Parse(
                        CloudConfigurationManager.GetSetting(StaticConfig.AclaraOneStorageConnectionString));
                _cloudBlobClient = storageAccount.CreateCloudBlobClient();
                return _cloudBlobClient;
            }
        }

        /// <summary>
        /// Get Event Hub client
        /// </summary>
        /// <param name="connectionString">Event Hub connection string</param>
        /// <param name="path">The path to the Event Hub</param>
        /// <returns>EventHubClient</returns>
        private static EventHubClient GetEventHubClient(string connectionString, string path)
        {
            var messagingFactory = MessagingFactory.CreateFromConnectionString(connectionString);
            messagingFactory.GetSettings().OperationTimeout = TimeSpan.FromHours(1);
            var client = messagingFactory.CreateEventHubClient(path);
            return client;
        }

        /// <summary>
        /// Add custom properties to the event
        /// </summary>
        /// <param name="data">Event data</param>
        /// <param name="customProperties">Custom properties that needs to be added</param>
        /// <param name="logModel">Used for logging details in case of any error</param>
        public void AddCustomProperties(EventData data, IDictionary<string, object> customProperties, LogModel logModel)
        {
            if (customProperties.Count > 0)
            {
                foreach (var property in customProperties)
                {
                    data.Properties.Add(property);
                }
            }
            data.Properties.Add("Source", logModel.Source);
            data.Properties.Add("ProcessingType", Convert.ToString(logModel.ProcessingType));
            data.Properties.Add("RowIndex", Convert.ToString(logModel.RowIndex));
        }
        
        /// <summary>
        /// Gets instance of event hub client
        /// </summary>
        /// <returns>Instance of Event Hub Client</returns>
        public virtual EventHubClient GetEventHubClient()
        {
            var connection = CloudConfigurationManager.GetSetting(StaticConfig.ImportDataEventHubSendConnectionString);
            var eventHubPath = CloudConfigurationManager.GetSetting(StaticConfig.ImportDataEventHubPath);
            return GetEventHubClient(connection, eventHubPath);
        }

        /// <summary>
        /// Close the connection for instance of event hub client
        /// </summary>
        /// <param name="client">Instance of Event Hub Client</param>
        public virtual void CloseEventHubClient(EventHubClient client)
        {
            client.Close();
        }

        public virtual void PreProcessRow(Enums.EventType eventType, IRawFileModel record)
        {
            
        }

        /// <summary>
        /// Process event
        /// </summary>
        /// <typeparam name="T">Type of event</typeparam>
        /// <param name="blockBlob">Blob</param>
        /// <param name="customProperties">Custom properties that needs to be added to the event</param>
        /// <param name="eventType">EventType enum which specifies the type of event</param>
        /// <param name="client">Instance of Event Hub client</param>
        public virtual void Process<T>(ICloudBlob blockBlob, IDictionary<string, object> customProperties, Enums.EventType eventType, EventHubClient client) where T : IRawFileModel
        {
            var index = 1;
            var eventsList = new List<EventData>();

            var stream = blockBlob.OpenRead();
            var sr = new StreamReader(stream);
            
            using (var reader = new CsvReader(sr))
            {
                reader.Configuration.IsHeaderCaseSensitive = false;
                reader.Configuration.TrimHeaders = true;
				reader.Configuration.TrimFields = true; //If there is any space in blank row then reader.IsRecordEmpty() will be true and that row will not get processed.
				while (reader.Read())
                {
                    try
                    {
						if (!reader.IsRecordEmpty())
                        {
                            LogModel.Metadata = JsonConvert.SerializeObject(reader.CurrentRecord);
                            LogModel.RowIndex = Convert.ToString(index);
                            var record = reader.GetRecord<T>();
                                    
                            PreProcessRow(eventType, record);

                            var serializedEventData = JsonConvert.SerializeObject(record);
                            var data = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

                            AddCustomProperties(data, customProperties, LogModel);
                            eventsList.Add(data);

                            if (index == 1)
                            {
                                var firstEventData = new EventData();
                                AddCustomProperties(firstEventData, customProperties, LogModel);
                                firstEventData.Properties.Add("FirstEventOfFile", 1);
                                client.Send(firstEventData);
                            }

                            if (index % 100 == 0)
                            {
                                SendEventsInBatch(client, eventsList);
                                eventsList = new List<EventData>();
                                if (index % 1000 == 0)
                                {
                                    LogModel.ProcessingEventCount = index;
                                    Logger.Info($"{index} records ingested into Batch Process Event Hub.", LogModel);
                                }
                            }
                            index++;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, LogModel);
                    }
                }
            }

            if (eventsList.Count > 0)
                SendEventsInBatch(client, eventsList);

            var lastEventData = new EventData();
            AddCustomProperties(lastEventData, customProperties, LogModel);
            lastEventData.Properties.Add("LastEventOfFile", index - 1);
            client.Send(lastEventData);

            LogModel.ProcessingEventCount = index - 1;
            LogModel.Metadata = null;
            Logger.Info($"{index - 1} records ingested into Batch Process Event Hub.", LogModel);
        }

        /// <summary>
        /// Sends events in batch
        /// </summary>
        /// <param name="client">Event Hub client</param>
        /// <param name="eventsList">List of events</param>
        public virtual void SendEventsInBatch(EventHubClient client, List<EventData> eventsList)
        {
            var eventData = eventsList.First();
            var totalSizeOfEvents = eventData.SerializedSizeInBytes * eventsList.Count;
            if (totalSizeOfEvents > 250000) //Events size is greater >  250000 bytes
            {
                var maxEventsCanBeSent = Convert.ToInt32(eventsList.Count * 250000 / totalSizeOfEvents);
                var loopCount = Convert.ToInt32(eventsList.Count / maxEventsCanBeSent);
                var totalEventSent = 0;
                for (var i = 0; i <= loopCount - 1; i++)
                {
                    var events = eventsList.Skip(i * maxEventsCanBeSent).Take(maxEventsCanBeSent).ToList();
                    if (events.Count > 0) client.SendBatch(events);
                    totalEventSent = (i + 1) * maxEventsCanBeSent;
                }
                if (totalEventSent != eventsList.Count)
                {
                    var events = eventsList.Skip(totalEventSent).ToList();
                    if (events.Count > 0) client.SendBatch(events);
                }
            }
            else
            {
                if (eventsList.Count > 0) client.SendBatch(eventsList);
            }
        }

        /// <summary>
        /// Archieves Blob 
        /// </summary>
        /// <param name="blockBlob">Blob</param>
        /// <param name="fileName">Name of file</param>
        /// <param name="containerName">Container Name</param>
        /// <param name="eventType">EventType enum which specifies the event type of file</param>
        public virtual void ArchieveBlob(ICloudBlob blockBlob, string fileName, string containerName, Enums.EventType eventType)
        {
            using (var ms = new MemoryStream())
            {
                ms.Position = 0;
                blockBlob.DownloadToStream(ms);
                var container = CloudBlobClient.GetContainerReference(containerName);
                var archieveBlockBlob = container.GetBlockBlobReference($"archive/{fileName}");
                ms.Position = 0;
                archieveBlockBlob.UploadFromStream(ms);
                if (eventType == Enums.EventType.NccAmiReading)
                {
                    var nccAmiConsumptionBlockBlob = container.GetBlockBlobReference($"NccCalcConsumption{fileName}");
                    ms.Position = 0;
                    nccAmiConsumptionBlockBlob.UploadFromStream(ms);
                }
            }
            blockBlob.Delete();
        }

        /// <summary>
        /// Gets the CloudBlockBlob using container name and file name
        /// </summary>
        /// <param name="fileName">Name of the blob</param>
        /// <param name="containerName">Name of the container in which the blob resides</param>
        /// <returns></returns>
        public virtual ICloudBlob GetBlockBlob(string fileName, string containerName)
        {
            var container = CloudBlobClient.GetContainerReference(containerName);
            return container.GetBlockBlobReference(fileName);
        }
    }
}