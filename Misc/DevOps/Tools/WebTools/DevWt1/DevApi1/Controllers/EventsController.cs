﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using CommonUtils1;
using DevApi1.Utils;
using EventsConsole.utils;
using Microsoft.ServiceBus.Messaging;

namespace DevApi1.Controllers
{
    public class EventsController : ApiController
    {


        public HttpResponseMessage GetConfigInfo(string dtype, string infoType) {
            ProcessingArgs pa = new ProcessingArgs();
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            StringWriter sw = new StringWriter();
            HubConnect(pa);

            IEnumerable<KeyValuePair<string, string>> inQParams = Request.GetQueryNameValuePairs();
            response.Content = new StringContent(pa.AsJson());
            StatusController.FinalizeResponseHeaders(pa, response, Request, "application/json");
            return response;
        }


        void HubConnect(ProcessingArgs pa) {
            string env = pa["env"] ?? "dev";

            //string eventHubConnectionString = "Endpoint=sb://aclaoaclaraonesbnsdev.servicebus.windows.net/;SharedAccessKeyName=Manage;SharedAccessKey=gwp1cTMyUcJ2t3tP21AgbKdvw+efteMwecwjafl7oBI=";
            //string eventHubName = "aclaoimportdataehdev";
            //string storageAccountName = "aclaoaclaraonestgdev";
            //string storageAccountKey = "5Kv12Tw2/O9+jFaHqfy9yrWf9b0uQM5xa0K9559guWhsb6ugUmS/u5FN86qCS8LtZfAYsDqtOvi8VisNaaRnXQ==";
            //string storageConnectionString = string.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}", storageAccountName, storageAccountKey);
            string eventHubConnectionString = pa[env + ".hubCxn"];
            string eventHubName = "aclaoimportdataeh" + env;
            string storageAccountName = "aclaoaclaraonestg" + env;
            string storageAccountKey = pa[env + ".storeAcctKey"];
            string storageConnectionString = $"DefaultEndpointsProtocol=https;AccountName={storageAccountName};AccountKey={storageAccountKey}";

            string eventProcessorHostName = Guid.NewGuid().ToString();
            EventProcessorHost eventProcessorHost = new EventProcessorHost(eventProcessorHostName, eventHubName, EventHubConsumerGroup.DefaultGroupName, eventHubConnectionString, storageConnectionString);
            Debug.WriteLine("Registering EventProcessor...");
            var options = new EventProcessorOptions();
            options.ExceptionReceived += (sender, e) => { Debug.WriteLine(e.Exception); };
            eventProcessorHost.RegisterEventProcessorAsync<SimpleEventProcessor>(options).Wait();

            Debug.WriteLine("Receiving. Press enter key to stop worker.");
            Thread.Sleep(2000);
            //Console.ReadLine();
            eventProcessorHost.UnregisterEventProcessorAsync().Wait();
        }




        // GET: api/Events
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Events/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Events
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Events/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Events/5
        public void Delete(int id)
        {
        }
    }
}
