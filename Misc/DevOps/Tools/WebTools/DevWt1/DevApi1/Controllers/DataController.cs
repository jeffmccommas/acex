﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Xml.Serialization;
using Cassandra;
using CommonUtils1;
using DevApi1.Utils;

namespace DevApi1.Controllers
{
    public class DataController : ApiController
    {
        // GET: api/Data
        public IEnumerable<string> Get()
        {
            return new[] { "value1", "value2" };
        }

        // GET: api/Data/5
        public string Get(int id)
        {
            return "value";
        }



        private static string _myEtag = "";
        private static DataTable _lastLogResultTable;  // this is temporary for single user.




        public HttpResponseMessage PutStg(string vtype, string vval) {
            ProcessingArgs pa = new ProcessingArgs();
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);

            string content = Request.Content.ReadAsStringAsync().Result;
            string[] plines = content.Split('\n');
            pa["qSel"] = plines[0];
            AccumulateRequestPars(pa);
            if ((plines.Length > 1) && !string.IsNullOrEmpty(plines[1])) {  // maybe supplying complex filtering
                pa["qFilter"] = plines[1];
            }
            else { // otherwise check for simple filtering 
                pa["qFilter"] = "";
                if (!string.IsNullOrEmpty(pa["qLevel"])) {
                    pa["qFilter"] = $"level = '{pa["qLevel"]}'";
                }
            }
            if (string.IsNullOrEmpty(pa["myEtag"]) || !_myEtag.Equals(pa["myEtag"])) {
                PerformLogQuery(pa, out _lastLogResultTable);
            }
            pa["X-BIO-etag"] = _myEtag;
            PrepareResponseInfo(_lastLogResultTable, pa, response);
            return response;
        }

        public HttpResponseMessage GetStg(string vtype, string vval) {
            ProcessingArgs pa = new ProcessingArgs();
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            //string qSel = "where month_year=122016 and client_id in ('290','224')";
            string qSel = "";
            pa["qSel"] = qSel;

            AccumulateRequestPars(pa);
            pa["qFilter"] = "";
            if (!string.IsNullOrEmpty(pa["qLevel"])) {
                pa["qFilter"] = $"level = '{pa["qLevel"]}'";
            }
            if (string.IsNullOrEmpty(pa["myEtag"]) || !_myEtag.Equals(pa["myEtag"])) {
                PerformLogQuery(pa, out _lastLogResultTable);
            }
            pa["X-BIO-etag"] = _myEtag;
            PrepareResponseInfo(_lastLogResultTable, pa, response);
            return response;
        }



        private static void PerformLogQuery(ProcessingArgs pa, out DataTable resultTable) {
            CassConn1 cc1 = new CassConn1();
            cc1.Connect(pa);
            string queryLine = $"SELECT * FROM log {pa["qSel"]} limit {pa["maxrows"]} {pa["allowFilter"]};";
            RowSet results = cc1.Session.Execute(queryLine);
            DataTable dt = new DataTable("logdata");

            foreach (CqlColumn ccol in results.Columns) {
                dt.Columns.Add(ccol.Name, ccol.Type);
            }

            foreach (Row result in results) {
                //sb.AppendLine(result[0]);
                Debug.WriteLine(result[0]);
                dt.Rows.Add(result.ToArray());
            }
            DataView dv = new DataView(dt) { Sort = "timestamp_utc DESC" };
            resultTable = dv.ToTable();
            _myEtag = Guid.NewGuid().ToString();
        }

        private void AccumulateRequestPars(ProcessingArgs pa) {
            IEnumerable<string> headerValues;
            if (Request.Headers.TryGetValues("X-BIO-etag", out headerValues)) {
                pa["myEtag"] = headerValues.FirstOrDefault();
            }
            pa["aclenv"] = "perf";
            IEnumerable<KeyValuePair<string, string>> inQParams = Request.GetQueryNameValuePairs();
            foreach (var keyValuePair in inQParams) {
                switch (keyValuePair.Key) {
                    case "aclenv":
                        pa["aclenv"] = keyValuePair.Value;
                        break;
                    case "qreset":
                        if (keyValuePair.Value.Equals("1")) {
                            pa["myEtag"] = "";
                        }
                        break;
                    case "allowFilter":
                        if (keyValuePair.Value.Equals("1")) {
                            pa["allowFilter"] = "allow filtering";
                        }
                        break;
                    case "maxRows":
                        int maxRows;
                        int tval;
                        if (int.TryParse(keyValuePair.Value, out tval)) {
                            maxRows = tval;
                        }
                        else {
                            maxRows = 5000;
                        }
                        if (maxRows > 100000) {
                            maxRows = 100000;
                        }
                        pa["maxrows"] = maxRows.ToString();
                        break;
                    default:
                        if (!string.IsNullOrEmpty(keyValuePair.Value)) {
                            pa[keyValuePair.Key] = keyValuePair.Value;
                        }
                        break;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="resultTable"> result table for content</param>
        /// <param name="pa"> processing aguements</param>
        /// <param name="response">response to be filled in</param>
        private void PrepareResponseInfo(DataTable resultTable, ProcessingArgs pa, HttpResponseMessage response) {
            PerPageInfo ppInfo = new PerPageInfo(HttpContext.Current.Request.QueryString);
            string contentType = "application/xml";
            IEnumerable<string> headerValues;
            if (Request.Headers.TryGetValues("Accept", out headerValues)) {
                contentType = headerValues.FirstOrDefault();
            }
            bool isJson = !string.IsNullOrEmpty(contentType) && contentType.ToLower() == "application/json";
            try {
                if (resultTable.Rows.Count > 0) {
                    if (resultTable.Rows.Count < ppInfo.PerPage*ppInfo.Page) {
                        ppInfo.Page = resultTable.Rows.Count/ppInfo.PerPage;
                    }
                    else {
                        pa["next"] = (ppInfo.Page + 2).ToString();
                    }

                    IEnumerable<DataRow> queryResultPage1 = resultTable.Select(pa["qFilter"]);
                    int filtCount = queryResultPage1.Count();

                    if (filtCount < ppInfo.PerPage * ppInfo.Page) {
                        ppInfo.Page = resultTable.Rows.Count / ppInfo.PerPage;
                        pa["pages"] = (filtCount/ppInfo.PerPage).ToString();
                    }
                    IEnumerable<DataRow> queryResultPage = queryResultPage1
                        .Skip(ppInfo.PerPage * ppInfo.Page)
                        .Take(ppInfo.PerPage);


                    //IEnumerable<DataRow> queryResultPage = resultTable.Select(pa["qFilter"])
                    //    .Skip(ppInfo.PerPage * ppInfo.Page)
                    //    .Take(ppInfo.PerPage);




                    DataTable dt3 = queryResultPage.CopyToDataTable();
                    pa["filtDisp"] = dt3.Rows.Count.ToString();

                    dt3.TableName = "logdata";
                    if (isJson) {
                        response.Content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(dt3));
                    }
                    else {
                        StringWriter sw = new StringWriter();
                        dt3.WriteXml(sw);
                        response.Content = new StringContent(sw.ToString());
                    }
                    pa["pages"] = ((resultTable.Rows.Count / ppInfo.PerPage) + 1).ToString();
                }
                else {
                    pa["pages"] = "0";
                    response.Content = isJson ? new StringContent("{\"results\":\"none\"}") : new StringContent("<results>none</results>");
                }
            }
            catch (Exception exc) {
                response.Content = isJson ? new StringContent("{\"results\":\"exc\"}") : new StringContent($"<results>{exc.Message}</results>");
            }

            pa["recCount"] = resultTable.Rows.Count.ToString();
            pa["start"] = (ppInfo.Page+1).ToString();
            pa["per_page"] = (ppInfo.PerPage).ToString();

            FinalizeResponseHeaders(pa, response, contentType);
        }

        // POST: api/Data
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Data/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Data/5
        public void Delete(int id)
        {
        }


        public static string DatasetToXml(DataSet ds)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (TextWriter streamWriter = new StreamWriter(memoryStream))
                {
                    var xmlSerializer = new XmlSerializer(typeof(DataSet));
                    xmlSerializer.Serialize(streamWriter, ds);
                    return Encoding.UTF8.GetString(memoryStream.ToArray());
                }
            }
        }


        #region webProcessing

        public void FinalizeResponseHeaders(ProcessingArgs pa, HttpResponseMessage response, string contentType = null)
        {
            ManageOrigin(pa, Request, response);
            IEnumerable<string> values;
            if (!string.IsNullOrEmpty(pa["cassbuiltqry"]))
            {
                response.Headers.Add("cassbuiltqry", pa["cassbuiltqry"]);
            }
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType ?? "application/xml");
            if (Request.Headers.TryGetValues("X-BIO-timeStamp", out values))
            {
                response.Headers.Add("X-BIO-timeStamp", values.FirstOrDefault());
                response.Headers.Add("X-BIO-timeStampStop", DateTime.Now.ToString());
            }
            if (!string.IsNullOrEmpty(pa["start"])) response.Headers.Add("X-BIO-start_page", pa["start"]);
            if (!string.IsNullOrEmpty(pa["next"])) response.Headers.Add("X-BIO-next_page", pa["next"]);
            if (!string.IsNullOrEmpty(pa["per_page"])) response.Headers.Add("X-BIO-per_page", pa["per_page"]);
            if (!string.IsNullOrEmpty(pa["pages"])) response.Headers.Add("X-BIO-pages", pa["pages"]);
            if (!string.IsNullOrEmpty(pa["recCount"])) response.Headers.Add("X-BIO-records", pa["recCount"]);
            if (!string.IsNullOrEmpty(pa["X-BIO-etag"])) response.Headers.Add("X-BIO-etag", pa["X-BIO-etag"]); 
            if (!string.IsNullOrEmpty(pa["filtDisp"])) response.Headers.Add("X-BIO-filtDisp", pa["filtDisp"]); 

        }

        private static void ManageOrigin(ProcessingArgs pa, HttpRequestMessage request, HttpResponseMessage response)
        {
            string reqHost = request.Headers.Host;
            IEnumerable<string> values;
            if (pa["OriginHosts"] != null)
            {
                // we're dealing with multiple sites as well as nginx altered routes so indicate allowed sites 
                string[] allowedHosts = pa["OriginHosts"].Split(',');
                string allowHost = "http://localhost"; // default if nothing implicit or explicit
                if (allowedHosts.Contains(reqHost))
                {
                    string useScheme = request.RequestUri.Scheme;
                    if (request.Headers.TryGetValues("X-http-scheme", out values))
                    {
                        useScheme = values.FirstOrDefault();
                    }
                    allowHost = $"{useScheme}://{reqHost}";
                    if (request.Headers.Referrer != null)
                    {
                        // nginx forwarded or some other override
                        allowHost = $"{request.Headers.Referrer.Scheme}://{request.Headers.Referrer.Authority}";
                    }
                }
                response.Headers.Add("Access-Control-Allow-Origin", allowHost);
                response.Headers.Add("X-ACAO", allowHost);
                // so we can see what it did since ACAO doesnt make it through to the chrome debugger
            }
        }

#endregion

    }
}
