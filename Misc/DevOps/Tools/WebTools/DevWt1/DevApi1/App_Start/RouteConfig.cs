﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace DevApi1
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");



            routes.MapHttpRoute(
                name: "Sql"
                , routeTemplate: "api/{controller}/SqlConnect/{dtype}"
                , defaults: new { controller = "Status", dtype = "index" }
            );

            //routes.MapHttpRoute(
            //    name: "ConfigDetails"
            //    , routeTemplate: "api/{controller}/Config/{dtype}/{infoType}"
            //    , defaults: new { controller = "Status", dtype = "index", action = "PUT" }
            //);

            routes.MapHttpRoute(
                name: "ConfigDetails"
                , routeTemplate: "api/{controller}/Config/{dtype}/{infoType}"
                , defaults: new { controller = "Status", dtype = "index" }
            );

            //routes.MapHttpRoute(
            //    name: "SqlDetails"
            //    , routeTemplate: "api/{controller}/SqlConnect/{dtype}/{infoType}"
            //    , defaults: new { controller = "Status", dtype = "index" }
            //);

            // for the api related stuff.  apparently MapHttpRoute needs to com before MapRoute routes.
            routes.MapHttpRoute(
                name: "Status"
                , routeTemplate: "api/{controller}/{action}/{id}"
                , defaults: new { controller = "Status", action = "GetDefault", id = UrlParameter.Optional }
            );


        }
    }
}
