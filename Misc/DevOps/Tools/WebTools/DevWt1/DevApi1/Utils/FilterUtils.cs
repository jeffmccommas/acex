﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DevApi1.Utils
{
    public class FilterUtils
    {

        private const string ActionBillHighlight = "billhighlight";
        private const string ActionAmiHighlight = "amihighlight";

        private void FilterByHighlight(IList<MeasureRow> data)
        {
            // iterate each measure row, and if it is not part of the inclusive filter list, remove it!
            for (var i = data.Count - 1; i >= 0; i--)
            {
                if (ActionBillHighlight.Equals(data[i].ActionTypeKey, StringComparison.InvariantCultureIgnoreCase) || ActionAmiHighlight.Equals(data[i].ActionTypeKey, StringComparison.InvariantCultureIgnoreCase))
                {
                    data.RemoveAt(i);
                }
            }
        }

    }



    /// <summary>
    /// The Measure Row class
    /// </summary>
    class MeasureRow
    {
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public int ClientId { get; set; }

        public string CustomerId { get; set; }

        public string AccountId { get; set; }

        public string PremiseId { get; set; }

        public string ActionKey { get; set; }

        public string ActionTypeKey { get; set; }

        public decimal AnnualCost { get; set; }

        public decimal AnnualCostVariancePercent { get; set; }

        public decimal AnnualSavingsEstimate { get; set; }

        public string AnnualSavingsEstimateCurrencyKey { get; set; }

        public decimal Roi { get; set; }

        public decimal Payback { get; set; }

        public decimal UpfrontCost { get; set; }

        public string CommodityKey { get; set; }

        public decimal ElecSavEst { get; set; }

        public string ElecSavEstCurrencyKey { get; set; }

        public decimal ElecUsgSavEst { get; set; }

        public string ElecUsgSavEstUomKey { get; set; }
        public decimal GasSavEst { get; set; }
        public string GasSavEstCurrencyKey { get; set; }

        public decimal GasUsgSavEst { get; set; }

        public string GasUsgSavEstUomKey { get; set; }

        public decimal WaterSavEst { get; set; }

        public string WaterSavEstCurrencyKey { get; set; }

        public decimal WaterUsgSavEst { get; set; }

        public string WaterUsgSavEstUomKey { get; set; }

        public int Priority { get; set; }

        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public DateTime NewDate { get; set; }

        public decimal RebateAmount { get; set; }
    }

}