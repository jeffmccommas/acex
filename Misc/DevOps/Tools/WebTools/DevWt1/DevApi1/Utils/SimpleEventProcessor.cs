﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using DevApi1.Utils;
using Microsoft.ServiceBus.Messaging;
using CommonUtils1;

namespace EventsConsole.utils {
    internal class SimpleEventProcessor : IEventProcessor {

        public static string Mode { get; set; }
        private static string _partitionOperationsLog = "";
        private static int _partitionsOpenedCount;
        private static int _partitionsClosedCount;
        private static StreamWriter _logStream;

        private static int _consFull = 100000;
        private static int _consInfo = 10000;
        private static bool _doCheckpoint;

        // watch it --= assumes 32
        private static readonly int[] PartMessages = {
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0
        };

        private static string _environ = "na";
        private static ProcessingArgs _procArgs = null;
        /// <summary>
        /// DO THIS BEFORE USE -- static initialization of logging, etc.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="env"></param>
        public static void Init(ProcessingArgs pa, string env) {
            _procArgs = pa;
            _environ = env;
            if (pa["consFull"] != null) int.TryParse(pa["consFull"], out _consFull);
            if (pa["consInfo"] != null) int.TryParse(pa["consInfo"], out _consInfo);
            if (pa["doCheckpoint"] != null) _doCheckpoint = pa["doCheckpoint"].Equals("1");

            string fName = $"evConsLog_{DateTime.Now.Month}_{DateTime.Now.Day}_{DateTime.Now.Hour}_{DateTime.Now.Minute}_{DateTime.Now.Second}_{_environ}.txt";
            _logStream = new StreamWriter(fName);
            LogStg2($"SimpleEventProcessor, now={DateTime.Now}, name={fName}");
        }

        private static void LogStg(string stg) {
            lock (_logStream) {
                _logStream.WriteLine(stg);
            }
        }

        public static void LogStg2(string stg) {
            LogStg(stg);
            Console.WriteLine(stg);
        }

        public static void FlushData() {
            lock (_logStream) {
                _logStream.Flush();
            }
        }

        public static void ShowInfo(string info) {
            LogStg2($"at '{info}', total messages ={_totalMsgsCounter}  total rx Size = {_totalMsgsSize}");

        }


        public static void ShowLog() {
            LogStg2($"\npartitions opened={_partitionsOpenedCount} closed={_partitionsClosedCount},  checkpoint={_doCheckpoint} now={DateTime.Now}");
            LogStg2("\n partitionsLog: \n" + _partitionOperationsLog);
            for (int i = 0; i < PartMessages.Length; i++) {
                LogStg2($"partitions[{i}]={PartMessages[i]}");
            }
            LogStg2($"total messages ={_totalMsgsCounter}");
            FlushData();
        }

        Stopwatch _checkpointStopWatch;

        async Task IEventProcessor.CloseAsync(PartitionContext context, CloseReason reason) {
            string stg = $"Processor Shutting Down. Partition: '{context.Lease.PartitionId}', Reason: '{reason}'', time={DateTime.Now}  checkpoint={_doCheckpoint}";
            _partitionOperationsLog += stg + "\n";
            LogStg2(stg);
            _partitionsClosedCount++;
            if (reason == CloseReason.Shutdown) {
                if (_doCheckpoint) {
                    await context.CheckpointAsync();
                }
            }
        }

        Task IEventProcessor.OpenAsync(PartitionContext context) {
            string stg = $"SimpleEventProcessor initialized.  Partition: '{context.Lease.PartitionId}', Offset: '{context.Lease.Offset}',  checkpoint={_doCheckpoint} time={DateTime.Now}";
            _partitionOperationsLog += stg + "\n";
            LogStg2(stg);
            _partitionsOpenedCount++;
            _checkpointStopWatch = new Stopwatch();
            _checkpointStopWatch.Start();
            return Task.FromResult<object>(null);
        }
        static int _totalMsgsCounter;
        static long _totalMsgsSize;

        async Task IEventProcessor.ProcessEventsAsync(PartitionContext context, IEnumerable<EventData> messages) {
            foreach (EventData eventData in messages) {
                PartMessages[int.Parse(context.Lease.PartitionId)] += 1;
                _totalMsgsSize += eventData.SerializedSizeInBytes;
                LogStg($"Message received. Count {_totalMsgsCounter} totalsize {_totalMsgsSize} Partition: '{context.Lease.PartitionId}', Data: '{Encoding.UTF8.GetString(eventData.GetBytes())}'");
                if (_totalMsgsCounter % _consFull == 0) {
                    string data = Encoding.UTF8.GetString(eventData.GetBytes());
                    Console.WriteLine($"Message received. Count {_totalMsgsCounter}  Partition: '{context.Lease.PartitionId}', Data: '{data}'");
                }
                else {
                    if (_totalMsgsCounter % _consInfo == 0) {
                        Console.WriteLine($"[{_totalMsgsCounter},{context.Lease.PartitionId},{eventData.SerializedSizeInBytes}]");
                    }
                }
                _totalMsgsCounter++;
            }

            //Call checkpoint every 5 minutes, so that worker can resume processing from 5 minutes back if it restarts.
            if (_checkpointStopWatch.Elapsed > TimeSpan.FromMinutes(5)) {
                string stg = $"SimpleEventProcessor CheckpointAsync.  Partition: '{context.Lease.PartitionId}', time={DateTime.Now} checkpoint={_doCheckpoint}";
                _partitionOperationsLog += stg + "\n";
                LogStg2(stg);
                if (_doCheckpoint) {
                    await context.CheckpointAsync();
                }
                _checkpointStopWatch.Restart();
            }
        }
    }

}