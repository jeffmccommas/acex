<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text"/>

  <xsl:variable name="newline">
    <xsl:text>
</xsl:text>
  </xsl:variable>

  <xsl:template match="/">
    <xsl:for-each select="//body">
      <xsl:call-template name="procBody"/>
    </xsl:for-each>
  </xsl:template>


  <xsl:template name="procBody">
    <xsl:variable name="bodyRoot">
      <xsl:value-of select="StartTime"/>|<xsl:value-of select="Serial"/>|<xsl:value-of select="Host"/>|<xsl:value-of select="Suite"/>
    </xsl:variable>
    <xsl:for-each select="Log">
      <xsl:call-template name="log">
        <xsl:with-param name="bodyRoot" select="$bodyRoot"/>
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>


  <xsl:template name="log">
    <xsl:param name="bodyRoot"/>
    <xsl:for-each select="xdoctor_data/collection">
      <xsl:variable name="cName">
        <xsl:value-of select="@name"/>
      </xsl:variable>
      <xsl:for-each select="node">
        <xsl:call-template name="node">
          <xsl:with-param name="bodyRoot" select="$bodyRoot"/>
          <xsl:with-param name="cName" select="$cName"/>
        </xsl:call-template>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="node">
    <xsl:param name="bodyRoot"/>
    <xsl:param name="cName"/>

    <xsl:variable name="nName">
      <xsl:value-of select="@name"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$cName='all.mds_latency'">

        <xsl:for-each select="*">
          <xsl:variable name="eName">
            <xsl:value-of select="name()"/>
          </xsl:variable>
          <xsl:variable name="rootstring">
            <xsl:value-of select="$bodyRoot"/>|<xsl:value-of select="$cName"/>|<xsl:value-of select="$nName"/>|<xsl:value-of select="$eName"/>
          </xsl:variable>

          <xsl:choose>
            <xsl:when test="$eName='seda_stats_clear_status'">
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$rootstring"/>|<xsl:value-of select="@port"/>|<xsl:value-of select="@avg_time"/><xsl:value-of select="$newline"/>
            </xsl:otherwise>
          </xsl:choose>

        </xsl:for-each>

      </xsl:when>
      <xsl:otherwise>

        <xsl:for-each select="*">
          <xsl:variable name="eName">
            <xsl:value-of select="name()"/>
          </xsl:variable>
          <xsl:variable name="rootstring">
            <xsl:value-of select="$bodyRoot"/>|<xsl:value-of select="$cName"/>|<xsl:value-of select="$nName"/>|<xsl:value-of select="$eName"/>
          </xsl:variable>
          <xsl:choose>
            <xsl:when test="$eName='cpu_util'">
              <xsl:call-template name="cpu_util">
                <xsl:with-param name="rootstring" select="$rootstring"/>
              </xsl:call-template>
            </xsl:when>
            <xsl:when test="$eName='disk_util'">
              <xsl:call-template name="disk_util">
                <xsl:with-param name="rootstring" select="$rootstring"/>
              </xsl:call-template>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>


  <xsl:template name="cpu_util">
    <xsl:param name="rootstring"/>
    <xsl:value-of select="$rootstring"/>|iowait|<xsl:value-of select="@iowait"/><xsl:value-of select="$newline"/>
    <xsl:value-of select="$rootstring"/>|idle|<xsl:value-of select="@idle"/><xsl:value-of select="$newline"/>
  </xsl:template>

  <xsl:template name="disk_util">
    <xsl:param name="rootstring"/>
    <xsl:value-of select="$rootstring"/>|<xsl:value-of select="@dev"/>|<xsl:value-of select="@util"/><xsl:value-of select="$newline"/>
  </xsl:template>


</xsl:stylesheet>




<!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\Performance\t001\dhcoutput2.xml" htmlbaseurl="" outputurl="" processortype="internal" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->