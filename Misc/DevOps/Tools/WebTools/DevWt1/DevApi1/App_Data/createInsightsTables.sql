USE [InsightsMetaData]
GO


if not exists (select * from master.dbo.syslogins where loginname = N'asfop')
BEGIN
	declare @logindb nvarchar(132), @loginlang nvarchar(132) 
    select @logindb = N'master', @loginlang = N'us_english'
	if @logindb is null or not exists (select * from master.dbo.sysdatabases where name = @logindb)
		select @logindb = N'master'
	if @loginlang is null or (not exists (select * from master.dbo.syslanguages where name = @loginlang) and @loginlang <> N'us_english')
		select @loginlang = @@language
	exec sp_addlogin N'asfop', 'asf#1pw2', @logindb, @loginlang
END
GO


if not exists (select * from dbo.sysusers where name = N'asfop' and uid < 16382)
	EXEC sp_grantdbaccess N'asfop', N'asfop'
GO



/****** Object:  Table [dbo].[ClientConfigurationBulk]    Script Date: 10/6/2016 8:04:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[cm].[ClientConfigurationBulk]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [cm].[ClientConfigurationBulk]
GO

CREATE TABLE [cm].[ClientConfigurationBulk](
	[ClientConfigurationBulkID] [int] IDENTITY(1,1) NOT NULL,
	[ClientID] [int] NOT NULL,
	[EnvironmentKey] [varchar](256) NOT NULL,
	[ConfigurationBulkKey] [varchar](256) NOT NULL,
	[Name] [varchar](256) NOT NULL,
	[CategoryKey] [varchar](256) NOT NULL,
	[Description] [varchar](4000) NOT NULL,
	[JSONConfiguration] [nvarchar](max) NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_ClientConfigurationBulk] PRIMARY KEY CLUSTERED 
(
	[ClientConfigurationBulkID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [cm].[ClientConfigurationBulk] ADD  CONSTRAINT [DF_ClientConfigurationBulk_DateUpdated]  DEFAULT (getdate()) FOR [DateUpdated]
GO



GRANT SELECT, INSERT, UPDATE, DELETE ON cm.ClientConfigurationBulk  to asfop
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[cm].[usp_UpdateJsonConfig]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [cm].[usp_UpdateJsonConfig]
GO

--DATABASE STORED PROCEDURE
CREATE Procedure [cm].[usp_UpdateJsonConfig]
    @ID int,
    @Env varchar(256),
    @Json nvarchar (max)    
AS
    SET NOCOUNT ON

    UPDATE [cm].[ClientConfigurationBulk] 
    SET JSONConfiguration = @Json 
    WHERE ClientID = @ID and EnvironmentKey=@Env  AND ConfigurationBulkKey = 'aclaraone.clientsettings'

    RETURN
	GO

	GRANT EXECUTE ON [cm].[usp_UpdateJsonConfig] TO asfop
GO

	GRANT EXECUTE ON [cm].[usp_UpdateJsonConfig] TO asfop