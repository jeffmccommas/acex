﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CodeGenUtils
{
    class CodeGenUtils
    {
        static void Main(string[] args) {
            //TestFmt1();
            ProcDhcProc1Results();
        }

        class UriHit {
            private string uri;
            private double duration;
            private double blocked;
            private double wait;
            private UriHit[] hits;
        }

        Dictionary<string,UriHit>  hitRepo = new Dictionary<string, UriHit>();



        private static void ProcDhcProc1Results() {

            string fname = "c:/work/Performance/t001/dhcoutput_proessed_proc1.xml";
            string fData;
            //using (StreamReader sr = new StreamReader(fname)) {
            //    fData = sr.ReadToEnd();
            //    Console.WriteLine(fData);
            //}
            XmlDocument doc = new XmlDocument();
            doc.Load(fname);
            XmlNodeList nodes = doc.SelectNodes("//entry/request[method='GET']");
            foreach (XmlNode node in nodes) {
                string lUri = node.SelectSingleNode("url").InnerText;
                string sUri = "";
                int idx = 0;
                int delimCnt = 0;
                while (idx < lUri.Length) {
                    if (lUri[idx] == '/') {
                        if (++delimCnt >= 3) break;
                    }
                    sUri += lUri[idx];
                    idx++;
                }

                Console.WriteLine($"{sUri}, {lUri}");

            }


        }


        private static void TestFmt1() {
            int miIdx = 5;
            int hrs = 24;
            int mins = 60;
            int count = 0;
            string csvHeader = "";
            for (int hr = 0; hr < hrs; hr++) {
                for (int mi = 0; mi < mins; mi += miIdx) {
                    count++;
                    string parValu = $"IntValue{hr:00}{mi:00}";
                    csvHeader += parValu + ",";
                    //Console.WriteLine(string.Format("public double? IntValue{1:00}{2:00} {{ get; set; }}", hr, mi));
                    Console.WriteLine($"public double? {parValu} {{ get; set; }}");
                    //public double? IntValue0000 { get; set; }
                }
            }
            Console.WriteLine(csvHeader.TrimEnd(','));
            string csvValues = "";
            for (int i = 0; i < count; i++) {
                csvValues += "1,";
            }
            Console.WriteLine(csvValues.TrimEnd(','));
        }
    }
}
