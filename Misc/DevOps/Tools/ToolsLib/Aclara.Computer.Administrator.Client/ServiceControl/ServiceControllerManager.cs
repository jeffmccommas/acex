﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace Aclara.Computer.Administrator.Client
{
    /// <summary>
    /// Service controller manager.
    /// </summary>
    public class ServiceControllerManager
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private string _serviceName;

        #endregion

        #region Public Propertiers

        /// <summary>
        /// Property: Service name.
        /// </summary>
        public string ServiceName
        {
            get { return _serviceName; }
            set { _serviceName = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Perferred constructor.
        /// </summary>
        /// <param name="serviceName"></param>
        public ServiceControllerManager(string serviceName)
        {
            _serviceName = serviceName;
        }

        #endregion

        #region Protected Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <remarks>
        /// Prevent instantiating via default constructor.
        /// </remarks>
        protected ServiceControllerManager()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve service status.
        /// </summary>
        /// <returns></returns>
        public ServiceControllerStatus GetStatus()
        {
            ServiceControllerStatus result;
            ServiceController serviceController = null;

            try
            {
                //Deterine if service is installed.
                if (this.IsServiceInstalled() != true)
                {
                    result = ServiceControllerStatus.Stopped;
                }

                serviceController = new ServiceController(this.ServiceName);
                serviceController.Refresh();
                result = serviceController.Status;
                serviceController = null;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Start service.
        /// </summary>
        public void Start()
        {
            ServiceControllerStatus serviceControllerStatus;
            ServiceController serviceController = null;

            try
            {
                //Deterine if service is installed.
                if (this.IsServiceInstalled() != true)
                {
                    return;
                }
                
                serviceController = new ServiceController(this.ServiceName);
                serviceController.Refresh();
                serviceControllerStatus = serviceController.Status;
                if (serviceControllerStatus != ServiceControllerStatus.Running)
                {
                    serviceController.Start();
                }
                serviceController = null;

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Stop service.
        /// </summary>
        public void Stop()
        {
            ServiceControllerStatus serviceControllerStatus;
            ServiceController serviceController = null;

            try
            {
                //Deterine if service is installed.
                if (this.IsServiceInstalled() != true)
                {
                    return;
                }
                serviceController = new ServiceController(this.ServiceName);
                serviceController.Refresh();
                serviceControllerStatus = serviceController.Status;
                if (serviceControllerStatus != ServiceControllerStatus.Stopped)
                {
                    serviceController.Stop();
                }
                serviceController = null;

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Determine whether service is installed.
        /// </summary>
        /// <returns></returns>
        public bool IsServiceInstalled()
        {
            bool result = false;
            ServiceController serviceController
                    = null;
            try
            {
                serviceController = ServiceController.GetServices().Where(s=>s.ServiceName == this.ServiceName).FirstOrDefault();

                if (serviceController == null)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion
    }

}
