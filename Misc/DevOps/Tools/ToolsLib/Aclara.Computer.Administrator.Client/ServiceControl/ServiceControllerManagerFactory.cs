﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.Computer.Administrator.Client
{
    /// <summary>
    /// Service controller manager factory.
    /// </summary>
    public static class ServiceControllerManagerFactory
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Propertiers

        #endregion

        #region Public Methods

        /// <summary>
        /// Create Service controller manager.
        /// </summary>
        /// <returns></returns>
        public static ServiceControllerManager CreateServiceControllerManager(string serviceName)
        {
            ServiceControllerManager result = null;

            try
            {

                result = new ServiceControllerManager(serviceName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion
    }
}
