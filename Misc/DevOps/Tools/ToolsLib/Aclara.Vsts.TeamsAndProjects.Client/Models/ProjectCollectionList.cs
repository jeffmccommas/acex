﻿using System;
using System.Collections.Generic;

namespace Aclara.Vsts.TeamsAndProjects.Client.Models
{
    /// <summary>
    /// Team project reference list.
    /// </summary>
    public class ProjectCollectionList : List<Microsoft.TeamFoundation.Core.WebApi.TeamProjectCollection>
    {
        #region Private Constants

        #endregion

        #region Private Data Memembers

        private Uri _teamProjectCollectionUri;
        private string _basicAuthRestApiUserProfileName;
        private string _basicAuthRestApiPassword;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Team project collection uri.
        /// </summary>
        public Uri TeamProjectCollectionUri
        {
            get { return _teamProjectCollectionUri; }
            set
            {
                string teamProjectCollectionUriAsText = string.Empty;

                _teamProjectCollectionUri = value;

                teamProjectCollectionUriAsText = value.ToString();
                if (teamProjectCollectionUriAsText.EndsWith("/") == false)
                {
                    teamProjectCollectionUriAsText = value + @"/";
                    _teamProjectCollectionUri = new Uri(teamProjectCollectionUriAsText);
                }
            }
        }

        /// <summary>
        /// Property: Basic authorization REST API user profile name.
        /// </summary>
        public string BasicAuthRestApiUserProfileName
        {
            get { return _basicAuthRestApiUserProfileName; }
            set { _basicAuthRestApiUserProfileName = value; }
        }

        /// <summary>
        /// Property: Basic authorization REST API password.
        /// </summary>
        public string BasicAuthRestApiPassword
        {
            get { return _basicAuthRestApiPassword; }
            set { _basicAuthRestApiPassword = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ProjectCollectionList()
        {

        }

        #endregion

        #region Public Methods


        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
