﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vsts.TeamsAndProjects.Client.Models
{
    /// <summary>
    /// Team project reference list container.
    /// </summary>
    public class TeamProjectListContainer
    {

        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties

        public List<Microsoft.TeamFoundation.Core.WebApi.TeamProject> Value;

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public TeamProjectListContainer()
        {

        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
