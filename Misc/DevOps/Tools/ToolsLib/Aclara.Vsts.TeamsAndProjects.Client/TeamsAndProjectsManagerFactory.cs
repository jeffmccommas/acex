﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vsts.TeamsAndProjects.Client
{
    /// <summary>
    /// Projects and teams manager factory.
    /// </summary>
    public static class TeamsAndProjectsManagerFactory
    {
        #region Private Constants
        #endregion

        #region Public Methods

        /// <summary>
        /// Create projects and teams manager.
        /// </summary>
        /// <param name="vstsAccountUri"></param>
        /// <param name="basicAuthRestAPIUserProfileName"></param>
        /// <param name="basicAuthRestAPIPassword"></param>
        /// <returns></returns>
        static public TeamsAndProjectsManager CreateProjectsAndTeamsManager(Uri vstsAccountUri,
                                                                            string basicAuthRestAPIUserProfileName,
                                                                            string basicAuthRestAPIPassword)
        {
            TeamsAndProjectsManager result = null;
            TeamsAndProjectsManager projectsAndTeamsManager = null;

            try
            {

                projectsAndTeamsManager = new TeamsAndProjectsManager(vstsAccountUri,
                                                                      basicAuthRestAPIUserProfileName,
                                                                      basicAuthRestAPIPassword);

                result = projectsAndTeamsManager;
                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
