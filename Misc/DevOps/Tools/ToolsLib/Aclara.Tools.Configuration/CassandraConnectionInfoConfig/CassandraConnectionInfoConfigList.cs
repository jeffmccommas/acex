﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Tools.Configuration.CassandraConnectionInfoConfig
{

    /// <summary>
    /// Cassandra connection info config list.
    /// </summary>
    public class CassandraConnectionInfoConfigList : List<CassandraConnectionInfoConfig>
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Propertiers

        #endregion

        #region Public Constructors

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

    }
}
