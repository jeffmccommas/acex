﻿namespace Aclara.Tools.Configuration.CassandraConnectionInfoConfig
{

    /// <summary>
    /// Azure storage account config.
    /// </summary>
    public class CassandraConnectionInfoConfig
    {

        #region Private Data Members

        private string _name;
        private string _accessKey;
        private string _keyspaceName;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Property: Access key.
        /// </summary>
        public string AccessKey
        {
            get { return _accessKey; }
            set { _accessKey = value; }
        }

        /// <summary>
        /// Property: keyspace name.
        /// </summary>
        public string KeyspaceName
        {
            get { return _keyspaceName; }
            set { _keyspaceName = value; }
        }

        #endregion

    }

}
