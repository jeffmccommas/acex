﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Tools.Configuration.ScriptHostConfig
{
    /// <summary>
    /// Script host PowerShell configuration.
    /// </summary>
    public class ScriptHostPowerShellConfig : ScriptHostConfig
    {
        #region Private Constants
        
        #endregion

        #region Private Data Members

        #endregion

        #region Public Properties

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ScriptHostPowerShellConfig()
        {

        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

    }
}
