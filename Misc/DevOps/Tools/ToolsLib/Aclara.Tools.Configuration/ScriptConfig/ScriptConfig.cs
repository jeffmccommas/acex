﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Tools.Configuration.ScriptConfig
{
    /// <summary>
    /// Script  configuration.
    /// </summary>
    public  class ScriptConfig
    {
        #region Private Constants
        
        #endregion

        #region Private Data Members

        private string _name;
        private string _fileName;
        private string _workingFolder;

        #endregion
        
        #region Public Properties
        
        /// <summary>
        /// Property: Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        
        /// <summary>
        /// Property: File name.
        /// </summary>
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        /// <summary>
        /// Property: Working folder.
        /// </summary>
        public string WorkingFolder
        {
            get { return _workingFolder; }
            set { _workingFolder = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ScriptConfig()
        {

        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

    }
}
