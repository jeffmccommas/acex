﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.Tools.Configuration
{

    /// <summary>
    /// Application setting configuration manager factory.
    /// </summary>
    public static class AppSettingsManageConfigFactory
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion
        
        #region Public Propertiers

        #endregion

        #region Public Methods

        /// <summary>
        /// Create application setttings manager.
        /// </summary>
        /// <returns></returns>
        public static AppSettingConfigManager CreateAppSettingsManager()
        {
            AppSettingConfigManager result = null;

            try
            {

                result = new AppSettingConfigManager();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

    }
}
