﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.Tools.Configuration
{

    /// <summary>
    /// App setting configuration list.
    /// </summary>
    public class AppSettingConfigList : List<AppSettingConfig>
    {
    
        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Propertiers

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AppSettingConfigList()
        {

        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

    }
}
