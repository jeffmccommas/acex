﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.Tools.Configuration
{

    /// <summary>
    /// App setting configuration.
    /// </summary>
    public class AppSettingConfig
    {
        #region Private Data Members

        private string _fileName;
        private string _xPath;
        private string _value;
        
        #endregion

        #region Public Properties

        /// <summary>
        /// Property: File name.
        /// </summary>
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        /// <summary>
        /// Property: XPath.
        /// </summary>
        public string XPath
        {
            get { return _xPath; }
            set { _xPath = value; }
        }

        /// <summary>
        /// Property: Value.
        /// </summary>
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
        
        #endregion
    }

}
