﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace Aclara.Tools.Configuration
{
    /// <summary>
    /// Application setting configuration manager.
    /// </summary>
    public class AppSettingConfigManager
    {

        #region Private Constants

        private const string MicrosoftNetFramework64ConfigFolder = @"C:\WINDOWS\Microsoft.NET\Framework64\v4.0.30319\CONFIG\";
        private const string MicrosoftNetFrameworkConfigFolder = @"C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319\CONFIG\";

        #endregion

        #region Private Data Members

        #endregion

        #region Public Propertiers

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AppSettingConfigManager()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update app setting list.
        /// </summary>
        /// <param name="bitness"></param>
        /// <param name="appSettingList"></param>
        public void UpdateAppSettingList(MachineConfigTypes.Bitness bitness,
                                         AppSettingConfigList appSettingList)
        {
            try
            {
                foreach (AppSettingConfig appSetting in appSettingList)
                {
                    this.UpdateAppSetting(bitness, appSetting);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update app settting.
        /// </summary>
        /// <param name="bitness"></param>
        /// <param name="appSetting"></param>
        public void UpdateAppSetting(MachineConfigTypes.Bitness bitness,
                                     AppSettingConfig appSetting)
        {

            string microsoftNetFrameworkConfigFolderPath = string.Empty;
            XmlDocument configurationXmlDocument = null;
            string configurationFileName = string.Empty;
            XmlNode appSettingXmlNode = null;

            try
            {

                configurationXmlDocument = new XmlDocument();

                microsoftNetFrameworkConfigFolderPath = GetMicrosoftFrameworkConfigFolderPath(bitness);

                configurationFileName = Path.Combine(microsoftNetFrameworkConfigFolderPath, appSetting.FileName);

                configurationXmlDocument.Load(configurationFileName);

                appSettingXmlNode = configurationXmlDocument.SelectSingleNode(appSetting.XPath);

                if (appSettingXmlNode == null)
                {
                    throw new ArgumentNullException(string.Format("Unable to find appSetting. (Configuration file name: {0}, AppSetting: {1}",
                                                                  configurationFileName, 
                                                                  appSetting.XPath));
                }
                appSettingXmlNode.Attributes["value"].Value = appSetting.Value;

                configurationXmlDocument.Save(configurationFileName);

            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Replace token in text with specified value.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="token"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string ReplaceTokenInTextWithValue(string text, string token, string value)
        {
            string result = string.Empty;

            result = Regex.Replace(text, token, value);

            return result;
        }

        /// <summary>
        /// Retrieve Microsoft .Net Framwork config folder path.
        /// </summary>
        /// <param name="bitness"></param>
        /// <returns></returns>
        protected string GetMicrosoftFrameworkConfigFolderPath(MachineConfigTypes.Bitness bitness)
        {


            string result = string.Empty;

            try
            {

                switch (bitness)
                {
                    case MachineConfigTypes.Bitness.x64:
                        result = MicrosoftNetFramework64ConfigFolder;
                        break;

                    case MachineConfigTypes.Bitness.x86:
                        result = MicrosoftNetFrameworkConfigFolder;
                        break;

                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected system type. (Bitness: {0})",
                                                                            bitness.ToString()));
                }


            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


        #endregion

        #region Private Methods

        #endregion

    }
}
