﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.Tools.Configuration
{

    /// <summary>
    /// Machine config types.
    /// </summary>
    public class MachineConfigTypes
    {

        /// <summary>
        /// Application settings name.
        /// </summary>
        public enum AppSettingName
        {
            Unspecified = 0,
            ReferrerInfoFolderPath = 1
        }

        /// <summary>
        /// Bitness.
        /// </summary>
        public enum Bitness
        {
            Unspecified = 0,
            x86 = 1,
            x64 = 2
        }
    }
}
