﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Tools.Configuration.WorkspaceOwnerConfig
{
    /// <summary>
    /// Workspace owner configuration list.
    /// </summary>
    public class WorkspaceOwnerConfigList : List<WorkspaceOwnerConfig>
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Propertiers

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public WorkspaceOwnerConfigList()
        {

        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion
    }

}