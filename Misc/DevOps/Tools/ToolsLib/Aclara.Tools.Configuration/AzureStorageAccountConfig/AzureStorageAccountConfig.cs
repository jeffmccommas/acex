﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Tools.Configuration.AzureStorageAccountConfig
{

    /// <summary>
    /// Azure storage account config.
    /// </summary>
    public class AzureStorageAccountConfig
    {

        #region Private Data Members

        private string _name;
        private string _accessKey;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Property: Access key.
        /// </summary>
        public string AccessKey
        {
            get { return _accessKey; }
            set { _accessKey = value; }
        }

        #endregion

    }

}
