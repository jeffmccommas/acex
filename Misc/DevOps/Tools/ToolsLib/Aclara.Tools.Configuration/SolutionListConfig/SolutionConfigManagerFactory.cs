﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.Tools.Configuration.SolutionListConfig
{
    /// <summary>
    /// Solution configuration manager factory.
    /// </summary>
    public class SolutionConfigManagerFactory
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Propertiers

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SolutionConfigManagerFactory()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create solution configuration manager.
        /// </summary>
        /// <returns></returns>
        public SolutionConfigManager CreateSolutionConfigManager()
        {
            SolutionConfigManager result = null;

            try
            {
                result = new SolutionConfigManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion
    }
}
