﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.Tools.Configuration
{
    /// <summary>
    /// Solution list configuration.
    /// </summary>
    public class SolutionConfig
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private string _solutionName = string.Empty;
        private string _solutionPath;

        #endregion
        private Dictionary<string, string> _msbuildProperties;

        #region Public Properties

        /// <summary>
        /// Property: Solution name.
        /// </summary>
        public string SolutionName
        {
            get { return _solutionName; }
            set { _solutionName = value; }
        }

        /// <summary>
        /// Property: Solution path.
        /// </summary>
        public string SolutionPath
        {
            get { return _solutionPath; }
            set { _solutionPath = value; }
        }

        /// <summary>
        /// Property: MSBuild properties.
        /// </summary>
        public Dictionary<string, string> MSBuildProperties
        {
            get { return _msbuildProperties; }
            set { _msbuildProperties = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SolutionConfig()
        {

        }
        #endregion

        #region Protected Constructors

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

    }
}
