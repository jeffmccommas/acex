﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml;

namespace Aclara.Tools.Configuration.SolutionListConfig
{
    /// <summary>
    /// Solution configuration manager.
    /// </summary>
    public class SolutionConfigManager
    {

        #region Private Constants

        private const string WAC_SOLUTIONLISTS = "SolutionLists";
        private const string WAC_SOLUTIONLIST = "SolutionList";
        private const string WAC_SOLUTION = "Solution";
        private const string WAC_NAME = "Name";
        private const string WAC_PATH = "Path";
        private const string WAC_VALUE = "Value";
        private const string WAC_MSBuild = "MSBuild";
        private const string WAC_Properties = "Properties";
        private const string WAC_Property = "Property";

        #endregion

        #region Private Data Members

        #endregion

        #region Public Propertiers

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SolutionConfigManager()
        {

        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve solution group list.
        /// </summary>
        /// <param name="configurationFileName"></param>
        /// <returns></returns>
        public SolutionLists GetSolutionGroupList(string configurationFileName)
        {
            SolutionLists result = null;
            SolutionList solutionList = null;
            string xpath = string.Empty;
            XmlDocument configurationXmlDocument = null;
            XmlNodeList solutionListsXmlNodeList = null;

            try
            {

                //Validate input parameter: Configuration file name.
                if (string.IsNullOrEmpty(configurationFileName) == true)
                {
                    throw new ArgumentException("Solution configuration file name is required.");
                }

                configurationXmlDocument = new XmlDocument();

                configurationXmlDocument.Load(configurationFileName);

                result = new SolutionLists();

                //Path to solution lists.
                xpath = "//" + WAC_SOLUTIONLISTS + "/" +
                        WAC_SOLUTIONLIST;

                solutionListsXmlNodeList = configurationXmlDocument.SelectNodes(xpath);

                //Solution lists cannot be found.
                if (solutionListsXmlNodeList == null)
                {
                    throw new ConfigurationErrorsException(string.Format("Solution configuration not found in solution lists configuration file {0}",
                                                                         configurationFileName));
                }

                //Iterate through web application config list.
                foreach (XmlNode solutionListXmlNode in solutionListsXmlNodeList)
                {

                    //Create web application configuration from xml node.
                    solutionList = this.CreateSolutionListFromXmlNode(solutionListXmlNode, 
                                                                      configurationFileName);
                    result.Add(solutionList);

                }

            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create solution list from xml node.
        /// </summary>
        /// <param name="solutionListXmlNode"></param>
        /// <param name="configurationFileName"></param>
        /// <returns></returns>
        protected SolutionList CreateSolutionListFromXmlNode(XmlNode solutionListXmlNode, string configurationFileName)
        {
            SolutionList result = null;
            SolutionConfig solutionConfig = null;
            string xpath = string.Empty;
            XmlNodeList solutionXmlNodeList = null;

            //Name.
            XmlNode nameXmlNode = null;
            string name = string.Empty;

            try
            {
                result = new SolutionList();

                //Retrieve name.
                xpath = WAC_NAME;
                nameXmlNode = solutionListXmlNode.SelectSingleNode(xpath);
                if (nameXmlNode != null)
                {
                    name = nameXmlNode.InnerText;
                }

                //Retrieve solution list.
                xpath = WAC_SOLUTION;
                solutionXmlNodeList = solutionListXmlNode.SelectNodes(xpath);

                //Solutions cannot be found.
                if (solutionXmlNodeList == null)
                {
                    throw new ConfigurationErrorsException(string.Format("Solution configuration not found in solution lists configuration file {0}",
                                                                         configurationFileName));
                }


                //Populate solution list properties with values.
                result.Name = name;

                //Iterate through solution config list.
                foreach (XmlNode solutionXmlNode in solutionXmlNodeList)
                {

                    //Create web application configuration from xml node.
                    solutionConfig = this.CreateSolutionConfigFromXmlNode(solutionXmlNode);
                    result.Add(solutionConfig);

                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create solution config from xml node.
        /// </summary>
        /// <param name="solutionConfigXmlNode"></param>
        /// <returns></returns>
        protected SolutionConfig CreateSolutionConfigFromXmlNode(XmlNode solutionConfigXmlNode)
        {
            SolutionConfig result = null;
            string xpath = string.Empty;

            //Name.
            XmlNode nameXmlNode = null;
            string name = string.Empty;

            //Path.
            XmlNode pathXmlNode = null;
            string path = string.Empty;

            try
            {
                result = new SolutionConfig();

                //Retrieve name.
                xpath = WAC_NAME;
                nameXmlNode = solutionConfigXmlNode.SelectSingleNode(xpath);
                if (nameXmlNode != null)
                {
                    name = nameXmlNode.InnerText;
                }

                //Retrieve path.
                xpath = WAC_PATH;
                pathXmlNode = solutionConfigXmlNode.SelectSingleNode(xpath);
                if (pathXmlNode != null)
                {
                    path = pathXmlNode.InnerText;
                }

                //Populate solution list properties with values.
                result.SolutionName = name;
                result.SolutionPath = path;

                //Populate MSBuild properites.
                result.MSBuildProperties = this.CreateMSBuildPropertiesConfigFromXmlNode(solutionConfigXmlNode);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create solution config from xml node.
        /// </summary>
        /// <param name="solutionConfigXmlNode"></param>
        /// <returns></returns>
        protected Dictionary<string, string> CreateMSBuildPropertiesConfigFromXmlNode(XmlNode solutionConfigXmlNode)
        {
            Dictionary<string, string> result = null;
            string xpath = string.Empty;

            //MSBuild.
            XmlNode msbuildXmlNode = null;

            //Properties.
            XmlNodeList propertiesXmlNodeList = null;

            //Property.
            string name = string.Empty;
            string value = string.Empty;

            try
            {
                result = new Dictionary<string, string>();

                //Retrieve msbuild.
                xpath = WAC_MSBuild;
                msbuildXmlNode = solutionConfigXmlNode.SelectSingleNode(xpath);
                if (msbuildXmlNode != null)
                {
                    //Retrieve property list.
                    xpath = WAC_Properties + "/" + WAC_Property;
                    propertiesXmlNodeList = msbuildXmlNode.SelectNodes(xpath);

                    //Properties found.
                    if (propertiesXmlNodeList != null)
                    {
                        //Iterate through property list.
                        foreach (XmlNode propertyXmlNode in propertiesXmlNodeList)
                        {

                            name = propertyXmlNode.Attributes[WAC_NAME].InnerXml;
                            value = propertyXmlNode.Attributes[WAC_VALUE].InnerXml;
                            result.Add(name, value);
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
