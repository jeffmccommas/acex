﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.Tools.Configuration.SolutionListConfig
{
    /// <summary>
    /// Solution list.
    /// </summary>
    public class SolutionList : List<SolutionConfig>
    {
        #region Private Constants

        #endregion

        #region Private Data Members

        private string _name = string.Empty;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SolutionList()
        {

        }

        #endregion

        #region Protected Constructors

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

    }
}
