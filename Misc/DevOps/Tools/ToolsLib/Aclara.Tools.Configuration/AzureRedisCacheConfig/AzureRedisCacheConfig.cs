﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Tools.Configuration.AzureRedisCacheConfig
{
    /// <summary>
    /// Azure Redisc cache configuration.
    /// </summary>
    public class AzureRedisCacheConfig
    {

        #region Private Data Members

        private string _name;
        private string _endpoint;
        private string _password;
        private bool _ssl;
        private int _connectRetry;
        private int _connectTimeout;
        private int _syncTimeout;


        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Property: Endpoint.
        /// </summary>
        public string Endpoint
        {
            get { return _endpoint; }
            set { _endpoint = value; }
        }

        /// <summary>
        /// Property: Password.
        /// </summary>
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        /// <summary>
        /// Property: Ssl.
        /// </summary>
        public bool Ssl
        {
            get { return _ssl; }
            set { _ssl = value; }
        }

        /// <summary>
        /// Property: Connect retry.
        /// </summary>
        public int ConnectRetry
        {
            get { return _connectRetry; }
            set { _connectRetry = value; }
        }

        /// <summary>
        /// Property: Connect timeout.
        /// </summary>
        public int ConnectTimeout
        {
            get { return _connectTimeout; }
            set { _connectTimeout = value; }
        }

        /// <summary>
        /// Property: Sync timout.
        /// </summary>
        public int SyncTimeout
        {
            get { return _syncTimeout; }
            set { _syncTimeout = value; }
        }

        #endregion
    }
}

