﻿using Aclara.Build.Engine.Client;
using Aclara.Tools.Configuration;
using Aclara.Tools.Configuration.SolutionListConfig;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;

namespace UnitTest
{
    /// <summary>
    ///This is a test class for WebAppManagerTest and is intended
    ///to contain all WebAppManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BuildManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for RunBuild
        ///</summary>
        [TestMethod()]
        public void SolutionConfigManagerTest()
        {
            const string ConfigurationFileName = @"C:\acltfs\EnergyPrism\14.06\Misc\Tools\ToolsLib\Configuration\SolutionConfig.xml";

            SolutionConfigManager target = null;
            SolutionConfigManagerFactory webAppConfigManagerFactory = null;
            SolutionLists solutionLists = null;

            webAppConfigManagerFactory = new SolutionConfigManagerFactory();
            target = webAppConfigManagerFactory.CreateSolutionConfigManager();
            if (File.Exists(ConfigurationFileName) == false)
            {
                throw new FileNotFoundException(string.Format("Web Application Configuration file not found. (ConfigurationFileName: {0})",
                                                              ConfigurationFileName));
            }

            solutionLists = target.GetSolutionGroupList(ConfigurationFileName);

        }

    }
}
