﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Aclara.Tools.Configuration
{
    /// <summary>
    /// Web application configuration manager factory.
    /// </summary>
    public class WebAppConfigManagerFactory
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Propertiers

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public WebAppConfigManagerFactory()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create web application configuration manager.
        /// </summary>
        /// <returns></returns>
        public WebAppConfigManager CreateWebAppConfigManager()
        {
            WebAppConfigManager result = null;

            try
            {
                result = new WebAppConfigManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

    }
}
