﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml;

namespace Aclara.Tools.Configuration
{

    /// <summary>
    /// Web application configuration manager.
    /// </summary>
    public class WebAppConfigManager
    {

        #region Private Constants

        private const string WAC_SITES = "Sites";
        private const string WAC_SITE = "Site";
        private const string WAC_NAME = "Name";
        private const string WAC_WEBAPPCONFIGLIST = "ApplicationList";
        private const string WAC_WEBAPPCONFIG = "Application";
        private const string WAC_APPID = "AppID";
        private const string WAC_ALIAS = "Alias";
        private const string WAC_APPLICATIONPOOLNAME = "ApplicationPoolName";
        private const string WAC_PATH = "Path";
        private const string WAC_PHYSICALPATH = "PhysicalPath";
        private const string WAC_VIRTUALDIRECTORYLIST = "VirtualDirectoryList";
        private const string WAC_VIRTUALDIRECTORY = "VirtualDirectory";

        #endregion

        #region Private Data Members

        #endregion

        #region Public Propertiers

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public WebAppConfigManager()
        {

        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve web application configuration list.
        /// </summary>
        /// <param name="configurationFileName"></param>
        /// <param name="siteName"></param>
        /// <returns></returns>
        public WebAppConfigList GetWebAppConfigList(string configurationFileName,
                                                    string siteName)
        {
            WebAppConfigList result = null;
            XmlNodeList webAppConfigListXmlNodeList = null;
            string xpath = string.Empty;
            WebAppConfig webAppConfig = null;
            string environmentNameFromConfig = string.Empty;
            XmlDocument configurationXmlDocument = null;

            try
            {

                //Validate input parameter: Configuration file name.
                if (string.IsNullOrEmpty(configurationFileName) == true)
                {
                    throw new ArgumentException("Web application configuration file name is required.");
                }

                //Validate input parameter: Site name.
                if (string.IsNullOrEmpty(siteName) == true)
                {
                    throw new ArgumentException("Site name is required.");
                }

                configurationXmlDocument = new XmlDocument();

                configurationXmlDocument.Load(configurationFileName);

                result = new WebAppConfigList();

                //Path to web application configuration.
                xpath = "//" + WAC_SITES + "/" +
                        WAC_SITE + "[" + WAC_NAME + "=\"" + siteName + "\"]/" +
                        WAC_WEBAPPCONFIGLIST + "/" +
                        WAC_WEBAPPCONFIG;

                webAppConfigListXmlNodeList = configurationXmlDocument.SelectNodes(xpath);

                //Web application configuration cannot be found.
                if (webAppConfigListXmlNodeList == null)
                {
                    throw new ConfigurationErrorsException(string.Format("Web application configuration not found in web application configuration file {0}",
                                                                         configurationFileName));
                }

                //Iterate through web application config list.
                foreach (XmlNode webAppConfigXmlNode in webAppConfigListXmlNodeList)
                {

                    //Create web application configuration from xml node.
                    webAppConfig = this.CreateWebAppConfigFromXmlNode(webAppConfigXmlNode);
                    result.Add(webAppConfig);

                }

            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create web application configuration from xml node.
        /// </summary>
        /// <param name="webAppConfigXmlNode"></param>
        /// <returns></returns>
        protected WebAppConfig CreateWebAppConfigFromXmlNode(XmlNode webAppConfigXmlNode)
        {
            WebAppConfig result = null;
            string xpath = string.Empty;

            //AppID.
            XmlNode appIDXmlNode = null;
            string appID = string.Empty;
            Guid appIDAsGuid;

            //Alias.
            XmlNode aliasXmlNode = null;
            string alias = string.Empty;

            //Application pool.
            XmlNode applicationPoolXmlNode = null;
            string applicationPool = string.Empty;

            //Path.
            XmlNode pathXmlNode = null;
            string path = string.Empty;

            XmlNodeList virtualDirectoryListXmlNodeList = null;

            VirtualDirectoryConfigList virtualDirectoryConfigList = null;
            VirtualDirectoryConfig virtualDirectoryConfig = null;

            try
            {
                result = new WebAppConfig();

                //Retrieve application identifier.
                xpath = WAC_APPID;
                appIDXmlNode = webAppConfigXmlNode.SelectSingleNode(xpath);
                appIDAsGuid = new Guid();
                if (appIDXmlNode != null)
                {
                    appID = appIDXmlNode.InnerText;
                    appIDAsGuid = new Guid(appID);
                }
                else
                {
                }

                //Retrieve alias.
                xpath = WAC_ALIAS;
                aliasXmlNode = webAppConfigXmlNode.SelectSingleNode(xpath);
                if (aliasXmlNode != null)
                {
                    alias = aliasXmlNode.InnerText;
                }

                //Retrieve application pool.
                xpath = WAC_APPLICATIONPOOLNAME;
                applicationPoolXmlNode = webAppConfigXmlNode.SelectSingleNode(xpath);
                if (applicationPoolXmlNode != null)
                {
                    applicationPool = applicationPoolXmlNode.InnerText;
                }

                //Retrieve path.
                xpath = WAC_PATH;
                pathXmlNode = webAppConfigXmlNode.SelectSingleNode(xpath);
                if (pathXmlNode != null)
                {
                    path = pathXmlNode.InnerText;
                }

                //Path to web application configuration.
                xpath = "./" + WAC_VIRTUALDIRECTORYLIST + "/" +
                        WAC_VIRTUALDIRECTORY;

                virtualDirectoryListXmlNodeList = webAppConfigXmlNode.SelectNodes(xpath);

                virtualDirectoryConfigList = new VirtualDirectoryConfigList();
                if (virtualDirectoryListXmlNodeList != null)
                {
                    //Iterate through web application config list.
                    foreach (XmlNode virtualDirectoryXmlNode in virtualDirectoryListXmlNodeList)
                    {

                        //Create web application configuration from xml node.
                        virtualDirectoryConfig = this.CreateVirtualDirectoryConfigFromXmlNode(virtualDirectoryXmlNode);
                        virtualDirectoryConfigList.Add(virtualDirectoryConfig);

                    }
                }

                //Populate web application configuration properties with values.

                result.AppID = appIDAsGuid;
                result.Alias = alias;
                result.ApplicationPoolName = applicationPool;
                result.Path = path;
                result.VirtualDirectoryConfigList = virtualDirectoryConfigList;

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create virtual directory configuration from xml node.
        /// </summary>
        /// <param name="virtualDirectoryXmlNode"></param>
        /// <returns></returns>
        protected VirtualDirectoryConfig CreateVirtualDirectoryConfigFromXmlNode(XmlNode virtualDirectoryXmlNode)
        {
            VirtualDirectoryConfig result = null;
            string xpath = string.Empty;

            //Path.
            XmlNode pathXmlNode = null;
            string path = string.Empty;

            //Physical path.
            XmlNode physicalPathXmlNode = null;
            string physicalPath = string.Empty;

            try
            {

                //Retrieve path.
                xpath = WAC_PATH;
                pathXmlNode = virtualDirectoryXmlNode.SelectSingleNode(xpath);
                path = pathXmlNode.InnerText;

                //Retrieve physical path.
                xpath = WAC_PHYSICALPATH;
                physicalPathXmlNode = virtualDirectoryXmlNode.SelectSingleNode(xpath);
                physicalPath = physicalPathXmlNode.InnerText;

                //Populate virtual directory properties with values.
                result = new VirtualDirectoryConfig();

                result.Path = path;
                result.PhysicalPath = physicalPath;

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
