﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.Tools.Configuration
{

    /// <summary>
    /// Web application configuration list.
    /// </summary>
    public class WebAppConfigList : List<WebAppConfig>
    {
        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Propertiers

        #endregion

        #region Public Constructors

        #endregion

        #region Protected Constructors

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion
    }

}
