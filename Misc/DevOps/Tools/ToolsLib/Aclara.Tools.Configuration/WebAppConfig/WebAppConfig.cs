﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.Tools.Configuration
{
    /// <summary>
    /// Web application configuration.
    /// </summary>
    public class WebAppConfig
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private Guid _appID;
        private string _alias = string.Empty;
        private string _applicationPoolName = string.Empty;
        private string _path = string.Empty;
        private VirtualDirectoryConfigList _virturalDirectoryConfigList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Application identifier.
        /// </summary>
        public Guid AppID
        {
            get { return _appID; }
            set { _appID = value; }
        }

        /// <summary>
        /// Property: Alias.
        /// </summary>
        public string Alias
        {
            get { return _alias; }
            set { _alias = value; }
        }

        /// <summary>
        /// Property: Application pool name.
        /// </summary>
        public string ApplicationPoolName
        {
            get { return _applicationPoolName; }
            set { _applicationPoolName = value; }
        }

        /// <summary>
        /// Property: Path.
        /// </summary>
        public string Path
        {
            get { return _path; }
            set { _path = value; }
        }

        /// <summary>
        /// Property: Virtual directory configuration list.
        /// </summary>
        public VirtualDirectoryConfigList VirtualDirectoryConfigList
        {
            get { return _virturalDirectoryConfigList; }
            set { _virturalDirectoryConfigList = value; }
        }
        
        #endregion

        #region Public Constructors

        #endregion

        #region Protected Constructors

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

    }
}
