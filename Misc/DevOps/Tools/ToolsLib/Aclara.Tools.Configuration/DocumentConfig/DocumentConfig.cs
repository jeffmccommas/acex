﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Tools.Configuration.DocumentConfig
{
    /// <summary>
    /// Document configuration.
    /// </summary>
    public class DocumentConfig
    {
        #region Private Data Members

        private string _name;
        private string _fileName;
        private string _path;
        
        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Property: File name.
        /// </summary>
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        /// <summary>
        /// Property: Path.
        /// </summary>
        public string Path
        {
            get { return _path; }
            set { _path = value; }
        }
        
        #endregion
    }
}
