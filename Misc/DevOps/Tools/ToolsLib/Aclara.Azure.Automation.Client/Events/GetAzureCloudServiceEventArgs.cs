﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.Azure.Automation.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Client.Events
{
    public class GetAzureCloudServiceEventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private string _azureCloudServiceName;
        private StatusList _statusList;
        private string _message;
        private AzureCloudServiceShallow _azureCloudSeriveShallow;
        private int _processedCount;
        private int _totalCount;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Azure cloud service name.
        /// </summary>
        public string AzureCloudServiceName
        {
            get { return _azureCloudServiceName; }
            set { _azureCloudServiceName = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        /// <summary>
        /// Property: Message.
        /// </summary>
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        /// <summary>
        /// Property: Azure cloud service shallow.
        /// </summary>
        public AzureCloudServiceShallow AzureCloudServiceShallow
        {
            get { return _azureCloudSeriveShallow; }
            set { _azureCloudSeriveShallow = value; }
        }

        /// <summary>
        /// Property: Processed count.
        /// </summary>
        public int ProcessedCount
        {
            get { return _processedCount; }
            set { _processedCount = value; }
        }

        /// <summary>
        /// Property: Total count.
        /// </summary>
        public int TotalCount
        {
            get { return _totalCount; }
            set { _totalCount = value; }
        }
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GetAzureCloudServiceEventArgs()
        {
            _statusList = new StatusList();
        }

        #endregion
    }
}
