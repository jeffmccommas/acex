﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Client.Models
{
    public class AzureCloudServiceShallow
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private string _serviceName;
        private string _slot;
        private int _roleInstanceCount;
        private string _vmSize;
        private string _roleName;
        private int _roleInstanceCountScaleUp;
        private int _roleInstanceCountScaleDown;
        private int _roleInstanceCountScaleCustom;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Service name.
        /// </summary>
        public string ServiceName
        {
            get { return _serviceName; }
            set { _serviceName = value; }
        }

        /// <summary>
        /// Property: Slot.
        /// </summary>
        public string Slot
        {
            get { return _slot; }
            set { _slot = value; }
        }

        /// <summary>
        /// Property: Role instance count.
        /// </summary>
        public int RoleInstanceCount
        {
            get { return _roleInstanceCount; }
            set { _roleInstanceCount = value; }
        }

        /// <summary>
        /// Property: Virtual machine size.
        /// </summary>
        public string VMSize
        {
            get { return _vmSize; }
            set { _vmSize = value; }
        }

        /// <summary>
        /// Property: Role name.
        /// </summary>
        public string RoleName
        {
            get { return _roleName; }
            set { _roleName = value; }
        }

        /// <summary>
        /// Property: Role instance count scale up.
        /// </summary>
        public int RoleInstanceCountScaleUp
        {
            get { return _roleInstanceCountScaleUp; }
            set { _roleInstanceCountScaleUp = value; }
        }

        /// <summary>
        /// Property: Role instance count scale down.
        /// </summary>
        public int RoleInstanceCountScaleDown
        {
            get { return _roleInstanceCountScaleDown; }
            set { _roleInstanceCountScaleDown = value; }
        }

        /// <summary>
        /// Property: Role instance count scale custom.
        /// </summary>
        public int RoleInstanceCountScaleCustom
        {
            get { return _roleInstanceCountScaleCustom; }
            set { _roleInstanceCountScaleCustom = value; }
        }
        #endregion

        #region Public Constructors
        #endregion
    }

}
