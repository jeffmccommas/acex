﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Client.Models
{
    /// <summary>
    /// Azure cloud service (shallow).
    /// </summary>
    public class AzureCloudServiceShallowList : List<AzureCloudServiceShallow>
    {

        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AzureCloudServiceShallowList()
        {

        }

        /// <summary>
        /// Constructor override.
        /// </summary>
        /// <param name="azureCloudServiceList"></param>
        public AzureCloudServiceShallowList(IEnumerable<AzureCloudServiceShallow> azureCloudServiceList)
        {

            foreach (AzureCloudServiceShallow azureCloudService in azureCloudServiceList)
            {
                this.Add(azureCloudService);
            }
        }

        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }

    static class IEnumerableAzureCloudServiceListExtensions
    {
        public static AzureCloudServiceShallowList ToAzureCloudServiceList(this IEnumerable<AzureCloudServiceShallow> AzureCloudServiceShallowList)
        {
            return new AzureCloudServiceShallowList(AzureCloudServiceShallowList);
        }
    }

}
