﻿using Aclara.Azure.Automation.Client.Events;
using Aclara.Azure.Automation.Client.Models;
using Aclara.PowerShellHost;
using Aclara.Tools.Common.StatusManagement;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Client
{
    /// <summary>
    /// Scale direction.
    /// </summary>
    public enum ScaleDirection
    {
        ScaleUp = 1,
        ScaleDown = 2,
        ScaleCustom = 3
    }

    public class AzureAutomationManager
    {
        #region Private Constants

        private const string ExceptionMessagePattern_BadRequestNoChangeInSettings = "BadRequest: No change in settings specified";

        #endregion

        #region Private Data Members

        private PowerShellHostManager _powerShellHostManager = null;

        #endregion

        #region Public Delegates

        public event EventHandler<ScaleAzureCloudServiceEventArgs> ScaleAzureCloudServiceBegan;
        public event EventHandler<ScaleAzureCloudServiceEventArgs> ScaleAzureCloudServiceCompleted;
        public event EventHandler<GetAzureCloudServiceEventArgs> GetAzureCloudServiceBegan;
        public event EventHandler<GetAzureCloudServiceEventArgs> GetAzureCloudServiceCompleted;

        #endregion



        #region Public Properties

        /// <summary>
        /// Property: PowerShellHost manager.
        /// </summary>
        public PowerShellHostManager PowerShellHostManager
        {
            get
            {
                if (_powerShellHostManager == null)
                {
                    _powerShellHostManager = this.CreatePowerShellHostManager();
                }
                return _powerShellHostManager;
            }
            set { _powerShellHostManager = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AzureAutomationManager()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get Azure cloud service.
        /// </summary>
        /// <param name="subscriptionName"></param>
        /// <param name="azureCloudServiceShallowList"></param>
        /// <param name="cancellationToken"></param>
        public void GetAzureCloudService(string subscriptionName,
                                         AzureCloudServiceShallowList azureCloudServiceShallowList,
                                         CancellationToken cancellationToken)
        {
            GetAzureCloudServiceEventArgs getAzureCloudServiceEventArgs = null;
            Status status = null;
            int processedCount = 0;

            try
            {
                getAzureCloudServiceEventArgs = new GetAzureCloudServiceEventArgs();

                foreach (AzureCloudServiceShallow azureCloudServiceShallow in azureCloudServiceShallowList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    processedCount++;

                    getAzureCloudServiceEventArgs.Message = string.Format("Retrieving Azure cloud service: {0}...",
                                                                          azureCloudServiceShallow.ServiceName);
                    getAzureCloudServiceEventArgs.AzureCloudServiceShallow = azureCloudServiceShallow;
                    getAzureCloudServiceEventArgs.ProcessedCount = 0;
                    getAzureCloudServiceEventArgs.TotalCount = 0;

                    if (GetAzureCloudServiceBegan != null)
                    {
                        GetAzureCloudServiceBegan(this, getAzureCloudServiceEventArgs);
                    }

                    this.GetAzureCloudService(subscriptionName, azureCloudServiceShallow);

                    getAzureCloudServiceEventArgs.Message = string.Format("Retrieved Azure cloud service: {0}.",
                                                                          azureCloudServiceShallow.ServiceName);
                    getAzureCloudServiceEventArgs.ProcessedCount = processedCount;
                    getAzureCloudServiceEventArgs.TotalCount = azureCloudServiceShallowList.Count;
                    if (GetAzureCloudServiceCompleted != null)
                    {
                        GetAzureCloudServiceCompleted(this, getAzureCloudServiceEventArgs);
                    }
                }

            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                getAzureCloudServiceEventArgs = new GetAzureCloudServiceEventArgs();
                getAzureCloudServiceEventArgs.StatusList.Add(status);

                if (GetAzureCloudServiceCompleted != null)
                {
                    GetAzureCloudServiceCompleted(this, getAzureCloudServiceEventArgs);
                }
            }

        }

        /// <summary>
        /// Get Azure cloud service.
        /// </summary>
        /// <param name="subscriptionName"></param>
        /// <param name="azureCloudServiceShallow"></param>
        public void GetAzureCloudService(string subscriptionName,
                                         AzureCloudServiceShallow azureCloudServiceShallow)
        {
            string powerShellScript = string.Empty;
            StatusList statusList = null;
            List<object> dataList = null;
            string dataAsText = String.Empty;
            List<AzureRoleInstance> azureRoleInstanceList = null;
            AzureRoleInstance azureRoleInstance = null;

            try
            {
                this.SelectAzureSubscription(subscriptionName);

                powerShellScript = string.Format("Get-AzureRole -ServiceName {0} -Slot {1} -InstanceDetails | ConvertTo-Json", azureCloudServiceShallow.ServiceName, azureCloudServiceShallow.Slot);
                statusList = new StatusList();

                dataList = this.InvokePowerShellScriptSync(powerShellScript, ref statusList);

                foreach (object data in dataList)
                {
                    if (data is string)
                    {
                        dataAsText = (string)data;

                        try
                        {
                            azureRoleInstanceList = JsonConvert.DeserializeObject<List<AzureRoleInstance>>(dataAsText);
                        }
                        catch (Newtonsoft.Json.JsonSerializationException)
                        {
                            azureRoleInstanceList = new List<AzureRoleInstance>();
                            azureRoleInstance = JsonConvert.DeserializeObject<AzureRoleInstance>(dataAsText);
                            azureRoleInstanceList.Add(azureRoleInstance);
                        }
                        catch (Exception)
                        {
                            throw;
                        }

                        if (azureRoleInstanceList != null)
                        {
                            azureCloudServiceShallow.RoleInstanceCount = azureRoleInstanceList.Count();
                            if (azureRoleInstanceList.Count() > 0)
                            {
                                azureCloudServiceShallow.VMSize = azureRoleInstanceList[0].InstanceSize;
                                azureCloudServiceShallow.RoleName = azureRoleInstanceList[0].RoleName;
                            }
                        }
                    }
                }

                foreach (Status status in statusList)
                {
                    if (status.Exception != null)
                    {
                        throw status.Exception;
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }

        }


        /// <summary>
        /// Scale Azure cloud service.
        /// </summary>
        /// <param name="subscriptionName"></param>
        /// <param name="azureCloudServiceShallowList"></param>
        /// <param name="scaleDirection"></param>
        /// <param name="cancellationToken"></param>
        public void ScaleAzureCloudService(string subscriptionName,
                                           AzureCloudServiceShallowList azureCloudServiceShallowList,
                                           ScaleDirection scaleDirection,
                                           CancellationToken cancellationToken)
        {
            ScaleAzureCloudServiceEventArgs scaleAzureCloudServiceEventArgs = null;
            GetAzureCloudServiceEventArgs getAzureCloudServiceEventArgs = null;
            Status status = null;
            StatusList statusList = null;
            int processedCount = 0;

            try
            {
                scaleAzureCloudServiceEventArgs = new ScaleAzureCloudServiceEventArgs();
                getAzureCloudServiceEventArgs = new GetAzureCloudServiceEventArgs();

                foreach (AzureCloudServiceShallow azureCloudServiceShallow in azureCloudServiceShallowList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    processedCount++;

                    scaleAzureCloudServiceEventArgs.Message = string.Format("Scaling Azure cloud service: {0}...",
                                                                            azureCloudServiceShallow.ServiceName);
                    scaleAzureCloudServiceEventArgs.AzureCloudServiceShallow = azureCloudServiceShallow;

                    if (ScaleAzureCloudServiceBegan != null)
                    {
                        ScaleAzureCloudServiceBegan(this, scaleAzureCloudServiceEventArgs);
                    }

                    statusList = this.ScaleAzureCloudService(subscriptionName, azureCloudServiceShallow, scaleDirection);

                    scaleAzureCloudServiceEventArgs.Message = string.Format("Scaled Azure cloud service: {0}.",
                                                                            azureCloudServiceShallow.ServiceName);
                    scaleAzureCloudServiceEventArgs.ProcessedCount = 0;
                    scaleAzureCloudServiceEventArgs.TotalCount = 0;
                    scaleAzureCloudServiceEventArgs.StatusList = statusList;
                    if (ScaleAzureCloudServiceCompleted != null)
                    {
                        ScaleAzureCloudServiceCompleted(this, scaleAzureCloudServiceEventArgs);
                    }

                    getAzureCloudServiceEventArgs.Message = string.Format("Retrieving Azure cloud service: {0}...",
                                                                          azureCloudServiceShallow.ServiceName);
                    getAzureCloudServiceEventArgs.AzureCloudServiceShallow = azureCloudServiceShallow;
                    getAzureCloudServiceEventArgs.ProcessedCount = processedCount;
                    getAzureCloudServiceEventArgs.TotalCount = azureCloudServiceShallowList.Count;

                    if (GetAzureCloudServiceBegan != null)
                    {
                        GetAzureCloudServiceBegan(this, getAzureCloudServiceEventArgs);
                    }

                    this.GetAzureCloudService(subscriptionName, azureCloudServiceShallow);

                    getAzureCloudServiceEventArgs.Message = string.Format("Retrieved Azure cloud service: {0}.",
                                                                          azureCloudServiceShallow.ServiceName);
                    if (GetAzureCloudServiceCompleted != null)
                    {
                        GetAzureCloudServiceCompleted(this, getAzureCloudServiceEventArgs);
                    }

                }

            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                scaleAzureCloudServiceEventArgs = new ScaleAzureCloudServiceEventArgs();
                scaleAzureCloudServiceEventArgs.StatusList.Add(status);

                if (ScaleAzureCloudServiceCompleted != null)
                {
                    ScaleAzureCloudServiceCompleted(this, scaleAzureCloudServiceEventArgs);
                }
            }

        }

        /// <summary>
        /// Get Azure cloud service.
        /// </summary>
        /// <param name="subscriptionName"></param>
        /// <param name="azureCloudServiceShallow"></param>
        public StatusList ScaleAzureCloudService(string subscriptionName,
                                           AzureCloudServiceShallow azureCloudServiceShallow,
                                           ScaleDirection scaleDirection)
        {
            string powerShellScript = string.Empty;
            StatusList statusList = null;
            List<object> dataList = null;
            string dataAsText = String.Empty;

            try
            {
                statusList = new StatusList();

                this.SelectAzureSubscription(subscriptionName);

                switch (scaleDirection)
                {

                    case ScaleDirection.ScaleUp:
                        powerShellScript = string.Format("Set-AzureRole -ServiceName {0} -Slot {1} -RoleName {2} -Count {3}",
                                                         azureCloudServiceShallow.ServiceName,
                                                         azureCloudServiceShallow.Slot,
                                                         azureCloudServiceShallow.RoleName,
                                                         azureCloudServiceShallow.RoleInstanceCountScaleUp);
                        break;

                    case ScaleDirection.ScaleDown:
                        powerShellScript = string.Format("Set-AzureRole -ServiceName {0} -Slot {1} -RoleName {2} -Count {3}",
                                                         azureCloudServiceShallow.ServiceName,
                                                         azureCloudServiceShallow.Slot,
                                                         azureCloudServiceShallow.RoleName,
                                                         azureCloudServiceShallow.RoleInstanceCountScaleDown);
                        break;

                    case ScaleDirection.ScaleCustom:
                        powerShellScript = string.Format("Set-AzureRole -ServiceName {0} -Slot {1} -RoleName {2} -Count {3}",
                                                         azureCloudServiceShallow.ServiceName,
                                                         azureCloudServiceShallow.Slot,
                                                         azureCloudServiceShallow.RoleName,
                                                         azureCloudServiceShallow.RoleInstanceCountScaleCustom);
                        break;
                    default:
                        break;
                }

                statusList = new StatusList();

                dataList = this.InvokePowerShellScriptSync(powerShellScript, ref statusList);
                foreach (Status status in statusList)
                {

                    if (status.Exception != null)
                    {
                        if (status.Exception.Message.Contains(ExceptionMessagePattern_BadRequestNoChangeInSettings) == false)
                        {
                            throw status.Exception;
                        }
                        
                    }
                }
                return statusList;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Select Azure subscription.
        /// </summary>
        public void SelectAzureSubscription(string subscriptionName)
        {
            string powerShellScript = string.Empty;
            StatusList statusList = null;
            List<object> dataList = null;

            try
            {
                powerShellScript = @"Select-AzureRmSubscription ";
                powerShellScript += string.Format(" -SubscriptionName \"{0}\"", subscriptionName);
                statusList = new StatusList();

                dataList = this.InvokePowerShellScriptSync(powerShellScript, ref statusList);

                foreach (Status status in statusList)
                {
                    if (status.Exception != null)
                    {
                        throw status.Exception;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods

        /// <summary>
        /// Create PowerShellHost manager.
        /// </summary>
        /// <returns></returns>
        protected PowerShellHostManager CreatePowerShellHostManager()
        {
            PowerShellHostManager result = null;

            try
            {
                result = PowerShellHostManagerFactory.CreatePowerShellHostManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        /// <summary>
        /// Invoke PowerShell script synchronously.
        /// </summary>
        /// <param name="powerShellScript"></param>
        /// <returns></returns>
        protected List<object> InvokePowerShellScriptSync(string powerShellScript, ref StatusList statusList)
        {

            List<object> result = null;

            try
            {
                result = this.PowerShellHostManager.InvokePowerShellScriptSync(powerShellScript, ref statusList);
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region Private Methods
        #endregion
    }
}
