﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.VersionControl.Client.Results;
using Aclara.TeamFoundation.VersionControl.Client.Interfaces;

namespace Aclara.TeamFoundation.VersionControl.Client.Events
{
    /// <summary>
    /// Delete workspace event arguments.
    /// </summary>
    public class DeleteWorkspaceEventArgs : EventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private IDeleteWorkspaceResult _deleteWorkspaceResult;

        #endregion
        
        #region Public Properties

        /// <summary>
        /// Property: Delete workspace result.
        /// </summary>
        public IDeleteWorkspaceResult DeleteWorkspaceResult
        {
            get { return _deleteWorkspaceResult; }
            set { _deleteWorkspaceResult = value; }
        }

        #endregion

        #region Public Constructors
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public DeleteWorkspaceEventArgs()
        {
            _deleteWorkspaceResult = new DeleteWorkspaceResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="requestBuildResult"></param>
        public DeleteWorkspaceEventArgs(IDeleteWorkspaceResult deleteWorkspaceResult)
        {
            _deleteWorkspaceResult = deleteWorkspaceResult;
        }

        #endregion

    }
}
