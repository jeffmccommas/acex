﻿using Aclara.TeamFoundation.VersionControl.Client.Events;
using Aclara.TeamFoundation.VersionControl.Client.Interfaces;
using Aclara.TeamFoundation.VersionControl.Client.Results;
using Aclara.TeamFoundation.VersionControl.Client.StatusManagement;
using Microsoft.TeamFoundation.VersionControl.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Aclara.TeamFoundation.VersionControl.Client
{
    /// <summary>
    /// Project manager.
    /// </summary>
    public class ProjectManager
    {

        #region Private Constants

        private const string ProjectPath_TFSRoot = "$/";
        private const int DeleteWorkspacePauseTimeMilliseconds = 10000;

        #endregion

        #region Private Data Members

        private VersionControlServer _versionControlServer = null;
        private string _teamProjectName = string.Empty;

        #endregion

        #region Public Delegates

        public event EventHandler<DeleteWorkspaceEventArgs> WorkspaceDeleted;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Version control server.
        /// </summary>
        public VersionControlServer VersionControlServer
        {
            get { return _versionControlServer; }
            set { _versionControlServer = value; }
        }

        /// <summary>
        /// Property: Team project name.
        /// </summary>
        public string TeamProjectName
        {
            get { return _teamProjectName; }
            set { _teamProjectName = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="versionControlServer"></param>
        /// <param name="teamProjectName"></param>
        public ProjectManager(VersionControlServer versionControlServer, string teamProjectName)
        {
            _versionControlServer = versionControlServer;
            _teamProjectName = teamProjectName;
        }

        #endregion

        #region Protected Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <remarks>
        /// Prevent instantiation via default constructor.
        /// </remarks>
        protected ProjectManager()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// <summary>
        /// Retrieve all workspaces.
        /// </summary>
        /// <remarks>Work in Progress</remarks>
        /// </summary>
        /// <param name="workspaceName"></param>
        /// <param name="workspaceOwner"></param>
        /// <param name="computer"></param>
        /// <returns></returns>
        public Workspace[] GetWorkspaces(string workspaceName,
                                         string workspaceOwner,
                                         string computer)
        {
            Workspace[] result = null;

            try
            {
                if (string.IsNullOrEmpty(workspaceName) == true)
                {
                    workspaceName = null;
                }

                if (string.IsNullOrEmpty(workspaceOwner) == true)
                {
                    workspaceOwner = null;
                }

                if (string.IsNullOrEmpty(computer) == true)
                {
                    computer = null;
                }

                result = this.VersionControlServer.QueryWorkspaces(workspaceName,
                                                                   workspaceOwner,
                                                                   computer);

            }
            catch
            {

                throw;

            }
            return result;
        }

        /// <summary>
        /// Retrieve workspaces.
        /// </summary>
        /// <param name="workspaceOwner"></param>
        /// <param name="computerList"></param>
        /// <returns></returns>
        public List<Workspace> GetWorkspaces(string workspaceOwner,
                                             List<string> computerList)
        {
            List<Workspace> result = null;
            Workspace[] workspaceList = null;
            string workspaceName = null;

            try
            {
                result = new List<Workspace>();

                foreach (string computer in computerList)
                {

                    workspaceName = null;

                    if (string.IsNullOrEmpty(workspaceOwner) == true)
                    {
                        workspaceOwner = null;
                    }

                    workspaceList = this.VersionControlServer.QueryWorkspaces(workspaceName,
                                                                              workspaceOwner,
                                                                              computer);
                    result.AddRange(workspaceList);
                }

            }
            catch
            {

                throw;

            }
            return result;
        }

        /// <summary>
        /// Delete workspace.
        /// </summary>
        /// <param name="workspace"></param>
        /// <returns></returns>
        public IDeleteWorkspaceResult DeleteWorkspace(Workspace workspace)
        {
            IDeleteWorkspaceResult result = null;
            bool deleteResult = false;
            Status status = null;

            try
            {
                result = DeleteWorkspaceResultFactory.CreateDeleteWorkspaceResult();

                result.Workspace = workspace;

                Thread.Sleep(DeleteWorkspacePauseTimeMilliseconds);

                deleteResult = true;
                deleteResult = workspace.Delete();

                if (deleteResult == false)
                {
                    status = new Status();
                    status.Exception = new DeleteWorkspaceFailedException(string.Format("Workspace deletion failed. (Workspace name: {0}, Computer: {1}, Owner none: {2})",
                                                                                        workspace.Name,
                                                                                        workspace.Computer,
                                                                                        workspace.OwnerName));
                    status.StatusServerity = StatusTypes.StatusSeverity.Error;
                    result.StatusList.Add(status);
                }

            }
            catch (Exception ex)
            {

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;
        }

        /// <summary>
        /// Delete workspaces in workspace list.
        /// </summary>
        /// <param name="workspaceList"></param>
        public void DeleteWorkspace(WorkspaceList workspaceList)
        {

            IDeleteWorkspaceResult deleteWorkspaceResult = null;
            DeleteWorkspaceEventArgs deleteWorkspaceEventArgs = null;

            try
            {

                foreach (Workspace workspace in workspaceList)
                {
                    deleteWorkspaceEventArgs = new DeleteWorkspaceEventArgs();

                    deleteWorkspaceResult = this.DeleteWorkspace(workspace);

                    deleteWorkspaceEventArgs.DeleteWorkspaceResult = deleteWorkspaceResult;

                    if (WorkspaceDeleted != null)
                    {
                        WorkspaceDeleted(this, deleteWorkspaceEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Delete workspaces in workspace list.
        /// </summary>
        /// <param name="workspaceList"></param>
        /// <param name="cancellationToken"></param>
        public void DeleteWorkspace(WorkspaceList workspaceList,
                                    CancellationToken cancellationToken)
        {
            IDeleteWorkspaceResult deleteWorkspaceResult = null;
            DeleteWorkspaceEventArgs deleteWorkspaceEventArgs = null;

            try
            {

                foreach (Workspace workspace in workspaceList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    deleteWorkspaceEventArgs = new DeleteWorkspaceEventArgs();

                    deleteWorkspaceResult = this.DeleteWorkspace(workspace);

                    deleteWorkspaceEventArgs.DeleteWorkspaceResult = deleteWorkspaceResult;

                    if (WorkspaceDeleted != null)
                    {
                        WorkspaceDeleted(this, deleteWorkspaceEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Retrieve team project year month branch list.
        /// </summary>
        public List<string> GetTeamProjectYearMonthBranchNames()
        {
            List<string> result = null;
            string path = string.Empty;
            ItemSet folderListItemSet = null;
            string matchedYearMonthBranchName = string.Empty;
            Match match = null;
            string pattern = string.Empty;

            try
            {

                //Verify property: Version control server.
                if (this.VersionControlServer == null)
                {
                    throw new ArgumentException(string.Format("Version control server is invalid."));
                }

                //Verify property: Team project name.
                if (string.IsNullOrEmpty(this.TeamProjectName) == true)
                {
                    throw new ArgumentException(string.Format("Team project name is required."));
                }

                result = new List<string>();

                path = ProjectPath_TFSRoot + this.TeamProjectName;

                folderListItemSet = this.VersionControlServer.GetItems(path, RecursionType.OneLevel);

                pattern = @"^\" + ProjectPath_TFSRoot + this.TeamProjectName + @"/(\d+).(\d+[I]?)$";

                foreach (Item folderItem in folderListItemSet.Items)
                {


                    match = Regex.Match(folderItem.ServerItem,
                                              pattern,
                                              RegexOptions.IgnoreCase);

                    // Match found.
                    if (match.Success)
                    {
                        if (match.Groups.Count == 3)
                        {
                            matchedYearMonthBranchName = match.Groups[1].Value + "." + match.Groups[2].Value;
                        }

                        result.Add(matchedYearMonthBranchName);
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Determine whether specified team project name is mapped to any local folder.
        /// </summary>
        /// <param name="teamProjectName"></param>
        /// <param name="workspaceName"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        /// 
        [Obsolete("Remove as method is referenced by not used.")]
        public bool IsTeamProjectMappedToLocalFolder(string teamProjectName,
                                                     string workspaceName,
                                                     string userName)
        {
            bool result = false;
            string teamProjectWorkingFolder = string.Empty;

            try
            {
                //Retrieve team project working folder.
                teamProjectWorkingFolder = this.GetTeamProjectWorkingFolder(teamProjectName, workspaceName, "", userName);
                if (string.IsNullOrEmpty(teamProjectWorkingFolder) == true)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve team project working folder path.
        /// </summary>
        /// <param name="teamProjectName"></param>
        /// <param name="workspaceName"></param>
        /// <param name="branchName"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public string GetTeamProjectWorkingFolder(string teamProjectName,
                                                  string workspaceName,
                                                  string branchName,
                                                  string userName)
        {
            string result = string.Empty;
            Workspace workspace = null;
            string teamProjectWorkingFolderPath = string.Empty;
            string[] teamProjectWorkingFolders = null;

            try
            {

                //Verify input parameter: Team project name.
                if (string.IsNullOrEmpty(teamProjectName) == true)
                {
                    throw new ArgumentNullException("Team project name is required.");
                }

                //Retrieve workspace for current machine/user.
                workspace = this.VersionControlServer.GetWorkspace(workspaceName, userName);

                //Workspace could not be found.
                if (workspace == null)
                {
                    throw new ProgramManagerException(string.Format("Workspace not found."));
                }

                //Retrieve 
                teamProjectWorkingFolderPath = workspace.TryGetLocalItemForServerItem("$/" + teamProjectName + "/" + branchName);

                //Could not retrieve team project working forlder.
                if (string.IsNullOrEmpty(teamProjectWorkingFolderPath) == true)
                {
                    result = string.Empty;
                    return result;
                }

                teamProjectWorkingFolders = teamProjectWorkingFolderPath.Split(Path.DirectorySeparatorChar);

                result = string.Empty;

                //Reassembly team project working folder path excluding team project name.
                foreach (string teamProjectWorkingFolder in teamProjectWorkingFolders)
                {
                    if (teamProjectWorkingFolder.ToUpper() != teamProjectName.ToUpper())
                    {
                        if (string.IsNullOrEmpty(result) == true)
                        {
                            result = teamProjectWorkingFolder;
                        }
                        else
                        {
                            result += Path.DirectorySeparatorChar + teamProjectWorkingFolder;
                        }
                    }
                    else
                    {
                        break;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve team project branch working folder path.
        /// </summary>
        /// <param name="teamProjectName"></param>
        /// <param name="workspaceName"></param>
        /// <param name="branchName"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public string GetTeamProjectBranchWorkingFolder(string teamProjectName,
                                                        string workspaceName,
                                                        string branchName,
                                                        string userName)
        {
            string result = string.Empty;
            Workspace workspace = null;
            string teamProjectWorkingFolderPath = string.Empty;

            try
            {

                //Verify input parameter: Team project name.
                if (string.IsNullOrEmpty(teamProjectName) == true)
                {
                    throw new ArgumentNullException("Team project name is required.");
                }

                //Retrieve workspace for current machine/user.
                workspace = this.VersionControlServer.GetWorkspace(workspaceName, userName);

                //Workspace could not be found.
                if (workspace == null)
                {
                    throw new ProgramManagerException(string.Format("Workspace not found."));
                }

                //Retrieve 
                result = workspace.TryGetLocalItemForServerItem("$/" + teamProjectName + "/" + branchName);

                //Could not retrieve team project working forlder.
                if (string.IsNullOrEmpty(result) == true)
                {
                    result = string.Empty;
                    return result;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        #endregion

    }
}
