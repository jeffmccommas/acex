﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.TeamFoundation.Common;
using Microsoft.TeamFoundation.VersionControl.Client;
using Aclara.TeamFoundation.VersionControl.Client.StatusManagement;
using Aclara.TeamFoundation.VersionControl.Client.Interfaces;

namespace Aclara.TeamFoundation.VersionControl.Client.Results
{
    /// <summary>
    /// Delete workspace result.
    /// </summary>
    public class DeleteWorkspaceResult : IDeleteWorkspaceResult
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private Workspace _workspace;
        private StatusList _statusList;
        
        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Workspace.
        /// </summary>
        public Workspace Workspace
        {
            get
            {
                return _workspace;
            }
            set
            {
                _workspace = value;
            }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DeleteWorkspaceResult()
        {
            _statusList = new StatusList();
        }

        #endregion
    }
}
