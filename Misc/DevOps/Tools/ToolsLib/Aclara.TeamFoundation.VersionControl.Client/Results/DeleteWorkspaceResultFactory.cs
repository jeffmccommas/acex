﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.VersionControl.Client.Interfaces;
using Aclara.TeamFoundation.VersionControl.Client.StatusManagement;

namespace Aclara.TeamFoundation.VersionControl.Client.Results
{

    /// <summary>
    /// Delete workspace definition result factory.
    /// </summary>
    static public class DeleteWorkspaceResultFactory
    {

        /// <summary>
        /// Create delete workspace result.
        /// </summary>
        /// <returns></returns>
        static public IDeleteWorkspaceResult CreateDeleteWorkspaceResult()
        {
            IDeleteWorkspaceResult result = null;
            DeleteWorkspaceResult deleteWorkspaceResult = null;

            try
            {
                deleteWorkspaceResult = new DeleteWorkspaceResult();

                result = deleteWorkspaceResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
