﻿using Aclara.TeamFoundation.VersionControl.Client.StatusManagement;
using Microsoft.TeamFoundation.VersionControl.Client;

namespace Aclara.TeamFoundation.VersionControl.Client.Interfaces
{

    /// <summary>
    /// Delete workspace result interface.
    /// </summary>
    public interface IDeleteWorkspaceResult
    {
        Workspace Workspace { get; set; }
        StatusList StatusList { get; }
    }
}
