﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.TeamFoundation.VersionControl.Client;

namespace Aclara.TeamFoundation.VersionControl.Client
{
    /// <summary>
    /// Workspace list.
    /// </summary>
    public class WorkspaceList : List<Workspace>
    {
        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public WorkspaceList()
        {

        }

        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

    }
}
