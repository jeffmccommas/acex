﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.VersionControl.Client;

namespace Aclara.TeamFoundation.VersionControl.Client
{
    public class ProjectManagerFactory
    {

        #region Private Constants
        
        #endregion

        #region Public Constructors
        
        #endregion

        #region Public Mehtods
        /// <summary>
        /// Retrieve version control server.
        /// </summary>
        /// <param name="tfsTeamProjectCollection"></param>
        /// <returns></returns>
        static public VersionControlServer GetVersionControlServer(Uri tfsServerUri)
        {
            VersionControlServer result = null;
            TfsTeamProjectCollection tfsTeamProjectCollection = null;
            VersionControlServer versionControlServer = null;

            try
            {
                tfsTeamProjectCollection = new TfsTeamProjectCollection(tfsServerUri);

                tfsTeamProjectCollection.EnsureAuthenticated();

                versionControlServer = tfsTeamProjectCollection.GetService<VersionControlServer>();

                result = versionControlServer;

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Create project manager.
        /// </summary>
        /// <param name="tfsServerUri"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        static public ProjectManager CreateProjectManager(Uri tfsServerUri,
                                                          string teamProjectName)
        {
            ProjectManager result = null;
            ProjectManager projectManager = null;
            VersionControlServer versionControlServer = null;

            try
            {
                //Verify input parameter: TFS server uri.
                if (tfsServerUri == null)
                {
                    throw new ArgumentException(string.Format("TFS service uri is required."));
                }

                //Verify input parameter: Team project name.
                if (string.IsNullOrEmpty(teamProjectName) == true)
                {
                    throw new ArgumentException(string.Format("Team project name is required."));
                }

                versionControlServer = GetVersionControlServer(tfsServerUri);

                projectManager = new ProjectManager(versionControlServer, teamProjectName);

                result = projectManager;
                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion
    }
}
