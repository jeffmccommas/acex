﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aclara.TeamFoundation.VersionControl.Client
{
    [Serializable]
    public class ProgramManagerException : Exception
    {

        #region Public Constructors

        public ProgramManagerException()
            : base()
        {
        }

        public ProgramManagerException(string message)
            : base(message)
        {
        }

        public ProgramManagerException(string message, 
                                       Exception innerException)
            : base(message, innerException)
        {
        }

        #endregion

        #region Protected Constructors

        protected ProgramManagerException(SerializationInfo serializationInfo, 
                                          StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }

        #endregion

    }
}
