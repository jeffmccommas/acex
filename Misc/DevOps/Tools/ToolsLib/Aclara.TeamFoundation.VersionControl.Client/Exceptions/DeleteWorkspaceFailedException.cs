﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aclara.TeamFoundation.VersionControl.Client
{
    [Serializable]
    public class DeleteWorkspaceFailedException : Exception
    {

        #region Public Constructors

        public DeleteWorkspaceFailedException()
            : base()
        {
        }

        public DeleteWorkspaceFailedException(string message)
            : base(message)
        {
        }

        public DeleteWorkspaceFailedException(string message, 
                                       Exception innerException)
            : base(message, innerException)
        {
        }

        #endregion

        #region Protected Constructors

        protected DeleteWorkspaceFailedException(SerializationInfo serializationInfo, 
                                                 StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }

        #endregion

    }
}

