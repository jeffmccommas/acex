﻿using System;
using System.Runtime.Serialization;

namespace Aclara.Vsts.Notification.Client.Exceptions
{
    [Serializable]
    public class TeamProjectCollectionInfoNotFoundException : Exception
    {

            #region Public Constructors

            public TeamProjectCollectionInfoNotFoundException()
                : base()
            {
            }

            public TeamProjectCollectionInfoNotFoundException(string message)
                : base(message)
            {
            }

            public TeamProjectCollectionInfoNotFoundException(string message,
                                           Exception innerException)
                : base(message, innerException)
            {
            }

            #endregion

            #region Protected Constructors

            protected TeamProjectCollectionInfoNotFoundException(SerializationInfo serializationInfo,
                                                     StreamingContext streamingContext)
                : base(serializationInfo, streamingContext)
            {
            }

            #endregion

    }
}
