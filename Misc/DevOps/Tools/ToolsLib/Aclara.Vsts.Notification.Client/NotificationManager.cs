﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.Vsts.Notification.Client.Events;
using Aclara.Vsts.Notification.Client.Exceptions;
using Aclara.Vsts.Notification.Client.Models;
using Aclara.Vsts.Notification.Client.Types;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.Core.WebApi;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using Microsoft.VisualStudio.Services.Common;
using Microsoft.VisualStudio.Services.Notifications.WebApi;
using Microsoft.VisualStudio.Services.Notifications.WebApi.Clients;
using Microsoft.VisualStudio.Services.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.Vsts.Notification.Client
{
    public class NotificationManager
    {
        #region Private Constants

        private const string EventText_UnknownNotificationSubscription = "Unknown notification subscription. (Id: {0})";
        private const string EventText_NotificationSubscriptionDoesNotExist = "Notification subscription does not exist. Delete operation skipped.";

        private const string ExceptionText_TeamProjectCollectionInfoNotFound = "Team project collection info not found. (Team project collection uri: {0})";
        private const string ExceptionText_TeamProjectInfoNotFound = "Team project info not found. (Team project collection uri: {0}, Team project name: {1})";

        #endregion

        #region Private Data Members

        private Uri _teamProjectCollectionUri;
        private Uri _teamProjectCollectionVsrmUri;
        private Guid _teamProjectCollectionInstanceId;
        private string _teamProjectName;
        private Guid _teamProjectId;
        private string _basicAuthRestApiUserProfileName;
        private string _basicAuthRestApiPassword;

        #endregion

        #region Public Delegates

        public event EventHandler<CreateNotificationSubscriptionEventArgs> CreateNotificationSubscriptionCompleted;
        public event EventHandler<ChangeNotificationSubscriptionEventArgs> ChangeNotificationSubscriptionCompleted;
        public event EventHandler<DeleteNotificationSubscriptionEventArgs> DeleteNotificationSubscriptionCompleted;
        public event EventHandler<GetNotificationSubscriptionListEventArgs> GetNotificationSubscriptionListRetrieved;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Team project collection uri.
        /// </summary>
        public Uri TeamProjectCollectionUri
        {
            get { return _teamProjectCollectionUri; }
            set
            {
                string teamProjectCollectionUriAsText = string.Empty;

                _teamProjectCollectionUri = value;

                teamProjectCollectionUriAsText = value.ToString();
                if (teamProjectCollectionUriAsText.EndsWith("/") == false)
                {
                    teamProjectCollectionUriAsText = value + @"/";
                    _teamProjectCollectionUri = new Uri(teamProjectCollectionUriAsText);
                }
            }
        }

        /// <summary>
        /// Property: Team project collection vsrm uri.
        /// </summary>
        public Uri TeamProjectCollectionVsrmUri
        {
            get { return _teamProjectCollectionVsrmUri; }
            set
            {
                string teamProjectCollectionVsrmUriAsText = string.Empty;

                _teamProjectCollectionVsrmUri = value;

                teamProjectCollectionVsrmUriAsText = value.ToString();
                if (teamProjectCollectionVsrmUriAsText.EndsWith("/") == false)
                {
                    teamProjectCollectionVsrmUriAsText = value + @"/";
                    _teamProjectCollectionVsrmUri = new Uri(teamProjectCollectionVsrmUriAsText);
                }
            }
        }

        /// <summary>
        /// Property: Team project collection - instance id.
        /// </summary>
        public Guid TeamProjectCollectionInstanceId
        {
            get { return _teamProjectCollectionInstanceId; }
            set { _teamProjectCollectionInstanceId = value; }
        }

        /// <summary>
        /// Property: Team project name.
        /// </summary>
        public string TeamProjectName
        {
            get { return _teamProjectName; }
            set { _teamProjectName = value; }
        }

        /// <summary>
        /// Property: Team project id.
        /// </summary>
        public Guid TeamProjectId
        {
            get { return _teamProjectId; }
            set { _teamProjectId = value; }
        }

        /// <summary>
        /// Property: Basic authorization REST API user profile name.
        /// </summary>
        public string BasicAuthRestApiUserProfileName
        {
            get { return _basicAuthRestApiUserProfileName; }
            set { _basicAuthRestApiUserProfileName = value; }
        }

        /// <summary>
        /// Property: Basic authorization REST API password.
        /// </summary>
        public string BasicAuthRestApiPassword
        {
            get { return _basicAuthRestApiPassword; }
            set { _basicAuthRestApiPassword = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamProjectCollectionUri"></param>
        /// <param name="teamProjectCollectionVsrmUri"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="basicAuthRestAPIUserProfileName"></param>
        /// <param name="basicAuthRestAPIPassword"></param>
        public NotificationManager(Uri teamProjectCollectionUri,
                                   Uri teamProjectCollectionVsrmUri,
                                   string teamProjectName,
                                   string basicAuthRestAPIUserProfileName,
                                   string basicAuthRestAPIPassword)
        {
            _teamProjectCollectionUri = teamProjectCollectionUri;
            _teamProjectCollectionVsrmUri = teamProjectCollectionVsrmUri;
            _teamProjectName = teamProjectName;
            _basicAuthRestApiUserProfileName = basicAuthRestAPIUserProfileName;
            _basicAuthRestApiPassword = basicAuthRestAPIPassword;
        }

        #endregion

        #region Protected Constructors
        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve notification subscription list.
        /// </summary>
        /// <param name="buildDefinitionNames"></param>
        /// <param name="filterOnTag"></param>
        /// <param name="tag"></param>
        /// <param name="cancellationToken"></param>
        public void GetNotificationSubscriptionList(List<string> buildDefinitionNames,
                                                    bool filterOnTag,
                                                    string tag,
                                                    CancellationToken cancellationToken)
        {
            GetNotificationSubscriptionListEventArgs getNotificationSubscriptionListEventArgs = null;
            NotificationSubscriptionList notificationSubscriptionList = null;
            int notificationSubscriptionListTotalCount = 0;
            int notificationSubscriptionListRetrievedCount = 0;
            List<NotificationSubscription> notificationSubscriptions = null;
            List<NotificationSubscription> filteredNotificationSubscriptions = null;
            string requestUrl = String.Empty;
            string parameter = string.Empty;

            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUrl = string.Format("{0}DefaultCollection/_apis/notification/subscriptions?api-version=3.2-preview",
                                               this.TeamProjectCollectionUri);

                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUrl).Result;

                    notificationSubscriptionList = new NotificationSubscriptionList();

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {

                        notificationSubscriptions = httpResponseMessage.Content.ReadAsAsync<NotificationSubscriptionListContainer>().Result.value;
                        if (notificationSubscriptions != null)
                        {
                            notificationSubscriptionListRetrievedCount = notificationSubscriptions.Count;
                            notificationSubscriptionListTotalCount = notificationSubscriptions.Count;
                        }

                        if (filterOnTag == true)
                        {
                            filteredNotificationSubscriptions = notificationSubscriptions.Where(ns => buildDefinitionNames.Any(bdn => !string.IsNullOrEmpty(ns.Description) &&
                                                                                                                                      ns.Description.Contains(bdn) &&
                                                                                                                                      ns.Description.Contains(tag))).ToList();

                        }
                        else
                        {
                            filteredNotificationSubscriptions = notificationSubscriptions.Where(ns => buildDefinitionNames.Any(bdn => !string.IsNullOrEmpty(ns.Description) &&
                                                                                                                                      ns.Description.Contains(bdn))).ToList();
                        }

                        foreach (NotificationSubscription notificationSubscription in filteredNotificationSubscriptions)
                        {

                            //Check for cancellation.
                            cancellationToken.ThrowIfCancellationRequested();

                            notificationSubscriptionList.Add(notificationSubscription);

                        }

                        getNotificationSubscriptionListEventArgs = new GetNotificationSubscriptionListEventArgs();
                        getNotificationSubscriptionListEventArgs.NotificationSubscriptionTotalCount = notificationSubscriptionListRetrievedCount;
                        getNotificationSubscriptionListEventArgs.NotificationSubscriptionRetrievedCount = notificationSubscriptionListTotalCount;
                        getNotificationSubscriptionListEventArgs.NotificationSubscriptionList = notificationSubscriptionList;

                        if (GetNotificationSubscriptionListRetrieved != null)
                        {
                            GetNotificationSubscriptionListRetrieved(this, getNotificationSubscriptionListEventArgs);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Status status = null;

                getNotificationSubscriptionListEventArgs = new GetNotificationSubscriptionListEventArgs();
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                getNotificationSubscriptionListEventArgs.StatusList.Add(status);
            }
        }

        /// <summary>
        /// Create notification subscription list.
        /// </summary>
        /// <param name="buildDefinitionNames"></param>
        /// <param name="teamName"></param>
        /// <param name="emailAddresses"></param>
        /// <param name="tag"></param>
        /// <param name="cancellationToken"></param>
        public void CreateNotificationSubscriptionList(List<string> buildDefinitionNames,
                                                       string teamName,
                                                       string emailAddresses,
                                                       string tag,
                                                       CancellationToken cancellationToken)
        {
            CreateNotificationSubscriptionEventArgs createNotificationSubscriptionEventArgs = null;
            NotificationSubscription notificationSubscription = null;
            int notificationSubscriptionCreatedCount = 0;
            try
            {
                this.PopulateTeamProjectInfo();

                foreach (string buildDefinitionName in buildDefinitionNames)
                {
                    notificationSubscriptionCreatedCount++;

                    notificationSubscription = this.CreateSubscriptionForTeam(buildDefinitionName, teamName, emailAddresses, tag);

                    createNotificationSubscriptionEventArgs = new CreateNotificationSubscriptionEventArgs();
                    createNotificationSubscriptionEventArgs.Description = buildDefinitionName;
                    createNotificationSubscriptionEventArgs.NotificationSubscriptionTotalCount = buildDefinitionNames.Count();
                    createNotificationSubscriptionEventArgs.NotificationSubscriptionCreatedCount = notificationSubscriptionCreatedCount;
                    if (CreateNotificationSubscriptionCompleted != null)
                    {
                        CreateNotificationSubscriptionCompleted(this, createNotificationSubscriptionEventArgs);
                    }
                }

            }
            catch (Exception ex)
            {
                Status status = null;

                createNotificationSubscriptionEventArgs = new CreateNotificationSubscriptionEventArgs();
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                createNotificationSubscriptionEventArgs.StatusList.Add(status);
            }
        }

        /// <summary>
        /// Change notification subscription list.
        /// </summary>
        /// <param name="notificationSubscriptionIdList"></param>
        /// <param name="notificationSubscriptionChangeProperties"></param>
        /// <param name="cancellationToken"></param>
        public void ChangeNotificationSubscriptionList(List<string> notificationSubscriptionIdList,
                                                       NotificationSubscriptionChangeProperties notificationSubscriptionChangeProperties,
                                                       CancellationToken cancellationToken)
        {
            ChangeNotificationSubscriptionEventArgs changeNotificationSubscriptionEventArgs = null;
            int notificationSubscriptionChangedCount = 0;

            try
            {
                this.PopulateTeamProjectInfo();

                foreach (string notificationSubscriptionId in notificationSubscriptionIdList)
                {
                    notificationSubscriptionChangedCount++;

                    this.ChangeNotificationSubscription(notificationSubscriptionId, notificationSubscriptionChangeProperties);

                    changeNotificationSubscriptionEventArgs = new ChangeNotificationSubscriptionEventArgs();
                    changeNotificationSubscriptionEventArgs.Description = notificationSubscriptionId;
                    changeNotificationSubscriptionEventArgs.NotificationSubscriptionTotalCount = notificationSubscriptionIdList.Count();
                    changeNotificationSubscriptionEventArgs.NotificationSubscriptionChangedCount = notificationSubscriptionChangedCount;
                    changeNotificationSubscriptionEventArgs.NotificationSubscriptionChangeProperties = notificationSubscriptionChangeProperties;
                    if (ChangeNotificationSubscriptionCompleted != null)
                    {
                        ChangeNotificationSubscriptionCompleted(this, changeNotificationSubscriptionEventArgs);
                    }
                }

            }
            catch (Exception ex)
            {
                Status status = null;

                changeNotificationSubscriptionEventArgs = new ChangeNotificationSubscriptionEventArgs();
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                changeNotificationSubscriptionEventArgs.StatusList.Add(status);
            }
        }

        /// <summary>
        /// Delete notification subscription list.
        /// </summary>
        /// <param name="buildDefinitionNames"></param>
        /// <param name="cancellationToken"></param>
        public void DeleteNotificationSubscriptionList(List<string> notificationsubscriptionIdList,
                                                       CancellationToken cancellationToken)
        {
            DeleteNotificationSubscriptionEventArgs deleteNotificationSubscriptionEventArgs = null;
            NotificationSubscription notificationSubscription = null;
            int notificationSubscriptionDeletedCount = 0;
            Status status = null;

            try
            {
                foreach (string id in notificationsubscriptionIdList)
                {
                    notificationSubscriptionDeletedCount++;

                    deleteNotificationSubscriptionEventArgs = new DeleteNotificationSubscriptionEventArgs();

                    notificationSubscription = this.GetNotificationSubscription(id);

                    if (notificationSubscription != null)
                    {
                        this.DeleteNotificationSubscription(id);

                        deleteNotificationSubscriptionEventArgs.Description = notificationSubscription.Description;
                        deleteNotificationSubscriptionEventArgs.NotificationSubscriptionTotalCount = notificationsubscriptionIdList.Count();
                        deleteNotificationSubscriptionEventArgs.NotificationSubscriptionDeletedCount += notificationSubscriptionDeletedCount;
                        if (DeleteNotificationSubscriptionCompleted != null)
                        {
                            DeleteNotificationSubscriptionCompleted(this, deleteNotificationSubscriptionEventArgs);
                        }
                    }
                    else
                    {
                        this.DeleteNotificationSubscription(id);

                        deleteNotificationSubscriptionEventArgs.Description = string.Format(EventText_UnknownNotificationSubscription,
                                                                                            id);
                        deleteNotificationSubscriptionEventArgs.NotificationSubscriptionTotalCount = notificationsubscriptionIdList.Count();
                        deleteNotificationSubscriptionEventArgs.NotificationSubscriptionDeletedCount = notificationSubscriptionDeletedCount;

                        status = new Status(new Exception(string.Format(EventText_NotificationSubscriptionDoesNotExist)), StatusTypes.StatusSeverity.Error);

                        deleteNotificationSubscriptionEventArgs.StatusList.Add(status);
                        if (DeleteNotificationSubscriptionCompleted != null)
                        {
                            DeleteNotificationSubscriptionCompleted(this, deleteNotificationSubscriptionEventArgs);
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                deleteNotificationSubscriptionEventArgs = new DeleteNotificationSubscriptionEventArgs();
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                deleteNotificationSubscriptionEventArgs.StatusList.Add(status);
            }
        }

        /// <summary>
        /// Retrieve team from project by name.
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="teamName"></param>
        /// <returns></returns>
        public List<string> GetTeamNameList(Guid? projectId)
        {
            List<string> result = null;
            TeamHttpClient teamHttpClient = null;
            VssCredentials vssCredentials = null;
            string userState = string.Empty;
            Task<List<WebApiTeam>> getTeamsTask = null;

            try
            {
                vssCredentials = new VssCredentials();
                vssCredentials = new VssCredentials(new Microsoft.VisualStudio.Services.Common.WindowsCredential(new NetworkCredential(this.BasicAuthRestApiUserProfileName, this.BasicAuthRestApiPassword)));

                teamHttpClient = new TeamHttpClient(this.TeamProjectCollectionUri, vssCredentials);

                getTeamsTask = teamHttpClient.GetTeamsAsync(projectId.ToString(), null, null, userState, default(CancellationToken));

                getTeamsTask.Wait();

                result = getTeamsTask.Result.Select(t => t.Name).ToList();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve team project id.
        /// </summary>
        /// <param name="teamProjectCollectionUri"></param>
        /// <returns></returns>
        public Guid GetProjectId(Uri teamProjectCollectionUri)
        {
            Guid result;
            WorkItemStore workItemStore = null;
            Project project = null;
            TfsTeamProjectCollection tpc = null;

            try
            {
                using (tpc = new TfsTeamProjectCollection(teamProjectCollectionUri))
                {
                    if (tpc == null)
                    {
                        throw new TeamProjectCollectionInfoNotFoundException(string.Format(ExceptionText_TeamProjectCollectionInfoNotFound,
                                                                                           teamProjectCollectionUri.ToString()));
                    }
                    workItemStore = tpc.GetService<WorkItemStore>();
                    if (workItemStore == null)
                    {
                        throw new TeamProjectInfoNotFoundException(string.Format(ExceptionText_TeamProjectInfoNotFound,
                                                                                  teamProjectCollectionUri.ToString(),
                                                                                  this.TeamProjectName));
                    }

                    project = workItemStore.Projects[this.TeamProjectName];
                    if (project == null)
                    {
                        throw new TeamProjectInfoNotFoundException(string.Format(ExceptionText_TeamProjectInfoNotFound,
                                                                                  teamProjectCollectionUri.ToString(),
                                                                                  this.TeamProjectName));
                    }

                    result = project.Guid;
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create a notification subscription for a team.
        /// </summary>
        /// <param name="buildDefinitionName"></param>
        /// <param name="teamName"></param>
        /// <param name="emailAddresses"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        protected NotificationSubscription CreateSubscriptionForTeam(string buildDefinitionName, 
                                                                     string teamName, 
                                                                     string emailAddresses, 
                                                                     string tag)
        {

            NotificationSubscription result = null;
            NotificationHttpClient notificationHttpClient = null;
            VssCredentials vssCredentials = null;
            NotificationSubscriptionCreateParameters createParameters = null;
            string userState = string.Empty;
            Task<NotificationSubscription> createDefinitionTask = null;
            try
            {

                vssCredentials = new VssCredentials();
                vssCredentials = new VssCredentials(new Microsoft.VisualStudio.Services.Common.WindowsCredential(new NetworkCredential(this.BasicAuthRestApiUserProfileName, this.BasicAuthRestApiPassword)));

                notificationHttpClient = new NotificationHttpClient(this.TeamProjectCollectionUri, vssCredentials);

                createParameters = CreateNotificationSubscriptionCreateParameters_BuildFailed(buildDefinitionName, teamName, emailAddresses, tag);

                createDefinitionTask = notificationHttpClient.CreateSubscriptionAsync(createParameters, userState, default(CancellationToken));

                createDefinitionTask.Wait();

                result = createDefinitionTask.Result;

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Change a notification subscription for a team.
        /// </summary>
        /// <param name="notificationSubscriptionId"></param>
        /// <param name="teamName"></param>
        /// <param name="emailAddresses"></param>
        /// <returns></returns>
        protected void ChangeNotificationSubscription(string notificationSubscriptionId, NotificationSubscriptionChangeProperties notificationSubscriptionChangeProperties)
        {
            NotificationHttpClient notificaitonHttpClient = null;
            VssCredentials vssCredentials = null;
            string userState = string.Empty;
            NotificationSubscriptionUpdateParameters notificationSubscriptionUpdateParameters = null;
            Task<NotificationSubscription> createDefinitionTask = null;

            try
            {

                vssCredentials = new VssCredentials();
                vssCredentials = new VssCredentials(new Microsoft.VisualStudio.Services.Common.WindowsCredential(new NetworkCredential(this.BasicAuthRestApiUserProfileName, this.BasicAuthRestApiPassword)));

                notificaitonHttpClient = new NotificationHttpClient(this.TeamProjectCollectionUri, vssCredentials);

                switch (notificationSubscriptionChangeProperties.ChangeNotificationSubscriptionType)
                {
                    case Enumerations.ChangeNotificationSubscriptionType.Unspecified:
                        break;
                    case Enumerations.ChangeNotificationSubscriptionType.ChangeStatus:
                        notificationSubscriptionUpdateParameters = new NotificationSubscriptionUpdateParameters()
                        {
                            Status = notificationSubscriptionChangeProperties.ConvertToSubscriptionStatus()
                        };
                        break;
                    default:
                        break;
                }

                createDefinitionTask = notificaitonHttpClient.UpdateSubscriptionAsync(notificationSubscriptionUpdateParameters, notificationSubscriptionId, userState, default(CancellationToken));

                createDefinitionTask.Wait();

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Delete a notification subscription.
        /// </summary>
        /// <returns></returns>
        protected void DeleteNotificationSubscription(string id)
        {

            NotificationHttpClient buildHttpClient = null;
            VssCredentials vssCredentials = null;
            string userState = string.Empty;
            Task deleteDefinitionTask = null;

            try
            {

                vssCredentials = new VssCredentials();
                vssCredentials = new VssCredentials(new Microsoft.VisualStudio.Services.Common.WindowsCredential(new NetworkCredential(this.BasicAuthRestApiUserProfileName, this.BasicAuthRestApiPassword)));

                buildHttpClient = new NotificationHttpClient(this.TeamProjectCollectionUri, vssCredentials);

                deleteDefinitionTask = buildHttpClient.DeleteSubscriptionAsync(id, userState, default(CancellationToken));
                deleteDefinitionTask.Wait();

            }
            catch (Exception)
            {
                throw;
            }

            return;

        }

        /// <summary>
        /// Create notification subscription create parameters.
        /// </summary>
        /// <param name="buildDefinitionName"></param>
        /// <param name="teamName"></param>
        /// <param name="emailAddresses"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        protected NotificationSubscriptionCreateParameters CreateNotificationSubscriptionCreateParameters_BuildFailed(string buildDefinitionName, 
                                                                                                                      string teamName, 
                                                                                                                      string emailAddresses,
                                                                                                                      string tag)
        {

            NotificationSubscriptionCreateParameters result = null;
            WebApiTeamRef team = null;
            EmailHtmlSubscriptionChannel emailHtmlSubscriptionChannel = null;
            string description = string.Empty;

            try
            {
                team = this.GetTeamByName(this.TeamProjectId, teamName);

                emailHtmlSubscriptionChannel = new EmailHtmlSubscriptionChannel();
                emailHtmlSubscriptionChannel.Address = emailAddresses;
                emailHtmlSubscriptionChannel.UseCustomAddress = true;

                description = string.Format("{0} {1}",
                                            tag,
                                            buildDefinitionName);

                var expressionModel = new ExpressionFilterModel()
                {
                    Clauses = new ExpressionFilterClause[]
                                    {
                                        new ExpressionFilterClause()
                                        {
                                            FieldName = "Status",
                                            Operator = "=",
                                            Value = "Failed"
                                        },
                                        new ExpressionFilterClause()
                                        {
                                            LogicalOperator = "And",
                                            FieldName = "Definition name",
                                            Operator = "=",
                                            Value = buildDefinitionName
                                        }
                                    }
                };
                result = new NotificationSubscriptionCreateParameters()
                {
                    Description = description,
                    Filter = new ExpressionFilter("ms.vss-build.build-completed-event", expressionModel)
                    {
                    },
                    Channel = emailHtmlSubscriptionChannel,
                    Subscriber = new IdentityRef()
                    {
                        Id = team.Id.ToString()
                    }
                };

                return result;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve notification subscription.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected NotificationSubscription GetNotificationSubscription(string id)
        {
            NotificationSubscription result = null;
            NotificationHttpClient buildHttpClient = null;
            VssCredentials vssCredentials = null;
            Task<NotificationSubscription> getNotificationSubscriptionTask = null;

            try
            {

                vssCredentials = new VssCredentials();
                vssCredentials = new VssCredentials(new Microsoft.VisualStudio.Services.Common.WindowsCredential(new NetworkCredential(this.BasicAuthRestApiUserProfileName, this.BasicAuthRestApiPassword)));

                buildHttpClient = new NotificationHttpClient(this.TeamProjectCollectionUri, vssCredentials);

                getNotificationSubscriptionTask = buildHttpClient.GetSubscriptionAsync(id, null, default(CancellationToken));
                getNotificationSubscriptionTask.Wait();
                result = getNotificationSubscriptionTask.Result;

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        //TODO: Move PopulateTeamProjectInfo() to a common class to be used by ReleaseDefinitionManager and NotificationManager.

        /// <summary>
        /// Populate team project information.
        /// </summary>
        /// <param name="projectName"></param>
        protected void PopulateTeamProjectInfo()
        {
            Uri teamProjectCollectionUri = null;
            WorkItemStore workItemStore = null;
            Project project = null;
            TfsTeamProjectCollection tpc = null;

            try
            {
                teamProjectCollectionUri = this.TeamProjectCollectionUri;
                using (tpc = new TfsTeamProjectCollection(teamProjectCollectionUri))
                {
                    if (tpc == null)
                    {
                        throw new TeamProjectCollectionInfoNotFoundException(string.Format(ExceptionText_TeamProjectCollectionInfoNotFound,
                                                                                           teamProjectCollectionUri.ToString()));
                    }
                    workItemStore = tpc.GetService<WorkItemStore>();
                    if (workItemStore == null)
                    {
                        throw new TeamProjectInfoNotFoundException(string.Format(ExceptionText_TeamProjectInfoNotFound,
                                                                                  teamProjectCollectionUri.ToString(),
                                                                                  this.TeamProjectName));
                    }

                    project = workItemStore.Projects[this.TeamProjectName];
                    if (project == null)
                    {
                        throw new TeamProjectInfoNotFoundException(string.Format(ExceptionText_TeamProjectInfoNotFound,
                                                                                  teamProjectCollectionUri.ToString(),
                                                                                  this.TeamProjectName));
                    }

                    this.TeamProjectCollectionInstanceId = workItemStore.TeamProjectCollection.InstanceId;

                    this.TeamProjectId = project.Guid;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Retrieve team from project by name.
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="teamName"></param>
        /// <returns></returns>
        protected WebApiTeamRef GetTeamByName(Guid? projectId, string teamName)
        {
            WebApiTeamRef result = null;
            TeamHttpClient teamHttpClient = null;
            VssCredentials vssCredentials = null;
            string userState = string.Empty;
            Task<List<WebApiTeam>> getTeamsTask = null;

            try
            {
                vssCredentials = new VssCredentials();
                vssCredentials = new VssCredentials(new Microsoft.VisualStudio.Services.Common.WindowsCredential(new NetworkCredential(this.BasicAuthRestApiUserProfileName, this.BasicAuthRestApiPassword)));

                teamHttpClient = new TeamHttpClient(this.TeamProjectCollectionUri, vssCredentials);

                getTeamsTask = teamHttpClient.GetTeamsAsync(projectId.ToString(), null, null, userState, default(CancellationToken));

                getTeamsTask.Wait();

                result = getTeamsTask.Result.Where(t => t.Name == teamName).SingleOrDefault();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Private Methods
        #endregion
    }
}
