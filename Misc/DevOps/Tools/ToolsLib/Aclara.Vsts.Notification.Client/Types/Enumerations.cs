﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vsts.Notification.Client.Types
{
    public class Enumerations
    {

        #region Public Types
        
        /// <summary>
        /// Change notification subscription type.
        /// </summary>
        public enum ChangeNotificationSubscriptionType
        {
            Unspecified = 0,
            ChangeStatus = 1
        }

        /// <summary>
        /// Notification subscription status.
        /// </summary>
        public enum NotificationSubscriptionStatus
        {
            Unspecified = 0,
            Enabled = 1,
            Disabled = 2
        }

        #endregion
    }
}
