﻿using Microsoft.VisualStudio.Services.Notifications.WebApi;
using System;
using static Aclara.Vsts.Notification.Client.Types.Enumerations;

namespace Aclara.Vsts.Notification.Client.Types
{
    public class NotificationSubscriptionChangeProperties
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private ChangeNotificationSubscriptionType _changeNotificationSubscriptionType;
        private NotificationSubscriptionStatus _notificationSubscriptionStatus;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Change notification subscription type.
        /// </summary>
        public ChangeNotificationSubscriptionType ChangeNotificationSubscriptionType
        {
            get { return _changeNotificationSubscriptionType; }
            set { _changeNotificationSubscriptionType = value; }
        }

        #endregion

        /// <summary>
        /// Property: Notification subscription status.
        /// </summary>
        public NotificationSubscriptionStatus NotificationSubscriptionStatus
        {
            get { return _notificationSubscriptionStatus; }
            set { _notificationSubscriptionStatus = value; }
        }

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public NotificationSubscriptionChangeProperties()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Convert notification subscription status to subscription status.
        /// </summary>
        /// <param name="notificationSubscriptionStatus"></param>
        /// <returns></returns>
        public SubscriptionStatus ConvertToSubscriptionStatus()
        {
            SubscriptionStatus result;

            try
            {
                switch (this.NotificationSubscriptionStatus)
                {
                    case NotificationSubscriptionStatus.Unspecified:
                        result = SubscriptionStatus.Disabled;
                        break;
                    case NotificationSubscriptionStatus.Enabled:
                        result = SubscriptionStatus.Enabled;
                        break;
                    case NotificationSubscriptionStatus.Disabled:
                        result = SubscriptionStatus.Disabled;
                        break;
                    default:
                        result = SubscriptionStatus.Disabled;
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
