﻿using System;

namespace Aclara.Vsts.Notification.Client
{
    public static class NotificationManagerFactory
    {
        #region Public Methods

        /// <summary>
        /// Create notification manger.
        /// </summary>
        /// <param name="teamProjectCollectionUri"></param>
        /// <param name="teamProjectCollectionVsrmUri"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="basicAuthRestAPIUserProfileName"></param>
        /// <param name="basicAuthRestAPIPassword"></param>
        /// <returns></returns>
        static public NotificationManager CreateNotificationManager(Uri teamProjectCollectionUri,
                                                                    Uri teamProjectCollectionVsrmUri,
                                                                    string teamProjectName,
                                                                    string basicAuthRestAPIUserProfileName,
                                                                    string basicAuthRestAPIPassword)
        {
            NotificationManager result = null;
            NotificationManager notificationManager = null;

            try
            {
                notificationManager = new NotificationManager(teamProjectCollectionUri,
                                                              teamProjectCollectionVsrmUri,
                                                              teamProjectName,
                                                              basicAuthRestAPIUserProfileName,
                                                              basicAuthRestAPIPassword);

                result = notificationManager;
                return result;
            }
            catch (Exception)
            {
                throw;
            }

            #endregion
        }
    }
}
