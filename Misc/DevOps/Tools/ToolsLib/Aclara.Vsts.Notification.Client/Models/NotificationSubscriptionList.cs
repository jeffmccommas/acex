﻿using Microsoft.VisualStudio.Services.Notifications.WebApi;
using System.Collections.Generic;

namespace Aclara.Vsts.Notification.Client.Models
{
    public class NotificationSubscriptionList : List<NotificationSubscription>
    {

        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public NotificationSubscriptionList()
        {

        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
