﻿using Microsoft.VisualStudio.Services.Notifications.WebApi;
using System.Collections.Generic;

namespace Aclara.Vsts.Notification.Client.Models
{
    public class NotificationSubscriptionListContainer
    {
        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public  Data Members

        public List<NotificationSubscription> value;

        #endregion

        #region Public Properties

        #endregion

        #region Public Constructors
        public NotificationSubscriptionListContainer()
        {
        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

    }
    }
