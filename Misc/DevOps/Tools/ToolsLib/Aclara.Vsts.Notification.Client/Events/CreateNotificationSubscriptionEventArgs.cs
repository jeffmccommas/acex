﻿using Aclara.Tools.Common.StatusManagement;

namespace Aclara.Vsts.Notification.Client.Events
{
    public class CreateNotificationSubscriptionEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private string _description;
        private int _notificationSubscriptionTotalCount;
        private int _notificationSubscriptionCreatedCount;
        StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Description.
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// Property: Notification subscription total count.
        /// </summary>
        public int NotificationSubscriptionTotalCount
        {
            get { return _notificationSubscriptionTotalCount; }
            set { _notificationSubscriptionTotalCount = value; }
        }

        /// <summary>
        /// Property: Notification subscription created count.
        /// </summary>
        public int NotificationSubscriptionCreatedCount
        {
            get { return _notificationSubscriptionCreatedCount; }
            set { _notificationSubscriptionCreatedCount = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CreateNotificationSubscriptionEventArgs()
        {
             _statusList = new StatusList();
       }

        #endregion
    }
}
