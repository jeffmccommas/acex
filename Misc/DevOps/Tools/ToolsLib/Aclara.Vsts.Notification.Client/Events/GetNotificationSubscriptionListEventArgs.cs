﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.Vsts.Notification.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.Services.Notifications.WebApi;

namespace Aclara.Vsts.Notification.Client.Events
{
    public class GetNotificationSubscriptionListEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        NotificationSubscriptionList _notificationSubscriptionList = null;
        private int _notificationSubscriptionTotalCount;
        private int _notificationSubscriptionRetrievedCount;
        StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Notification subscription list.
        /// </summary>
        public NotificationSubscriptionList NotificationSubscriptionList
        {
            get { return _notificationSubscriptionList; }
            set { _notificationSubscriptionList = value; }
        }

        /// <summary>
        /// Property: Notification subscription total count.
        /// </summary>
        public int NotificationSubscriptionTotalCount
        {
            get { return _notificationSubscriptionTotalCount; }
            set { _notificationSubscriptionTotalCount = value; }
        }

        /// <summary>
        /// Property: Notification subscription retrieved count.
        /// </summary>
        public int NotificationSubscriptionRetrievedCount
        {
            get { return _notificationSubscriptionRetrievedCount; }
            set { _notificationSubscriptionRetrievedCount = value; }
        }

        /// <summary>
        /// Property : Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }
        
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GetNotificationSubscriptionListEventArgs()
        {
            _statusList = new StatusList();
        }
        #endregion

    }
}
