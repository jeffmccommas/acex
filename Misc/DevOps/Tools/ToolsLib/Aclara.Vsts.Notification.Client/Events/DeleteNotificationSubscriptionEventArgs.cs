﻿using Aclara.Tools.Common.StatusManagement;

namespace Aclara.Vsts.Notification.Client.Events
{
    public class DeleteNotificationSubscriptionEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private string _description;
        private int _notificationSubscriptionTotalCount;
        private int _notificationSubscriptionDeletedCount;
        StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Description.
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// Property: Notification subscription total count.
        /// </summary>
        public int NotificationSubscriptionTotalCount
        {
            get { return _notificationSubscriptionTotalCount; }
            set { _notificationSubscriptionTotalCount = value; }
        }

        /// <summary>
        /// Property: Notification subscription deleted count.
        /// </summary>
        public int NotificationSubscriptionDeletedCount
        {
            get { return _notificationSubscriptionDeletedCount; }
            set { _notificationSubscriptionDeletedCount = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DeleteNotificationSubscriptionEventArgs()
        {
            _statusList = new StatusList();
        }

        #endregion
    }
}
