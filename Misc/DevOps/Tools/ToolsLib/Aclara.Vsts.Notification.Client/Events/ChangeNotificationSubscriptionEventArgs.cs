﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.Vsts.Notification.Client.Types;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vsts.Notification.Client.Events
{
    public class ChangeNotificationSubscriptionEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private string _description;
        private int _notificationSubscriptionTotalCount;
        private int _notificationSubscriptionChangedCount;
        private NotificationSubscriptionChangeProperties _notificationSubscriptionChangeProperties;
        StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Description.
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// Property: Notification subscription total count.
        /// </summary>
        public int NotificationSubscriptionTotalCount
        {
            get { return _notificationSubscriptionTotalCount; }
            set { _notificationSubscriptionTotalCount = value; }
        }

        /// <summary>
        /// Property: Notification subscription changed count.
        /// </summary>
        public int NotificationSubscriptionChangedCount
        {
            get { return _notificationSubscriptionChangedCount; }
            set { _notificationSubscriptionChangedCount = value; }
        }

        /// <summary>
        /// Property: Notification subscription change properties.
        /// </summary>
        public NotificationSubscriptionChangeProperties NotificationSubscriptionChangeProperties
        {
            get { return _notificationSubscriptionChangeProperties; }
            set { _notificationSubscriptionChangeProperties = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ChangeNotificationSubscriptionEventArgs()
        {
            _notificationSubscriptionChangeProperties = new NotificationSubscriptionChangeProperties();
            _statusList = new StatusList();
        }

        #endregion
    }
}
