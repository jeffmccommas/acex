﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Aclara.Sidekicks.AutoUpdater
{
    /// <summary>
    /// Sidekicks auto-updater manager.
    /// </summary>
    public class SidekicksAutoUpdaterManager
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Properties

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekicksAutoUpdaterManager()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve sidekicks auto-updater.
        /// </summary>
        /// <param name="bootstrapperExeRootPath"></param>
        /// <param name="bootstrapperExeName"></param>
        /// <returns></returns>
        public SidekicksAutoUpdater GetSidekicksAutoUpdater(string bootstrapperExeRootPath,
                                                            string bootstrapperExeName)
        {
            SidekicksAutoUpdater result = null;

            try
            {
                //Retrieve bootstrapper auto-updater - newest version.
                result = this.GetSidekicksAutoUpdaterNewestVersion(bootstrapperExeRootPath,
                                                                   bootstrapperExeName);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Retrieve sidekicks auto-updater - newest version.
        /// </summary>
        /// <param name="bootstrapperExeRootPath"></param>
        /// <param name="bootstrapperExeName"></param>
        /// <returns></returns>
        protected SidekicksAutoUpdater GetSidekicksAutoUpdaterNewestVersion(string bootstrapperExeRootPath,
                                                                            string bootstrapperExeName)
        {
            SidekicksAutoUpdater result = null;
            string bootstrapperExeFullPath = string.Empty;
            string bootstrapperExeFileVersion = string.Empty;
            string changeLogFilePath = string.Empty;
            string changeLogFileName = string.Empty;

            try
            {

                result = new SidekicksAutoUpdater();

                //Get bootstrapper executable information.
                result.GetBootstrapperExeInformation(bootstrapperExeRootPath,
                                                     bootstrapperExeName,
                                                     ref bootstrapperExeFullPath,
                                                     ref bootstrapperExeFileVersion,
                                                     ref changeLogFilePath,
                                                     ref changeLogFileName);

            }
            catch (Exception ex)
            {
                if (result != null)
                {
                    result.Status.RunState = RunState.Failed;
                    result.Status.Exception = ex;
                }
            }

            return result;
        }

        #endregion

        #region Private Methods

        #endregion

    }

}
