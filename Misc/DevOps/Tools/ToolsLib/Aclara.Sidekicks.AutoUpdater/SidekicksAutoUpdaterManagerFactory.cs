﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Sidekicks.AutoUpdater
{
    /// <summary>
    /// Sidekicks auto-updater manager factory.
    /// </summary>
    public class SidekicksAutoUpdaterManagerFactory
    {

        #region Private Constants

        #endregion

        #region Public Methods

        /// <summary>
        /// Create sidekicks update manager.
        /// </summary>
        /// <returns></returns>
        public static SidekicksAutoUpdaterManager CreateSidekicksUpdateManager()
        {
            SidekicksAutoUpdaterManager result = null;

            try
            {

                result = new SidekicksAutoUpdaterManager();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

    }
}
