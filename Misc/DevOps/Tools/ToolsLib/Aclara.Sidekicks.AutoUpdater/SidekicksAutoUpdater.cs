﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Aclara.Sidekicks.AutoUpdater
{
    /// <summary>
    /// Sidekicks auto updater.
    /// </summary>
    public class SidekicksAutoUpdater
    {

        #region Private Constants

        private const string PropertyUnavailable = "Unavailable";

        private const string ChangeLogFileName_FileNameOnly = "ChangeLog.txt";

        #endregion

        #region Private Data Members

        private string _bootstrapperExePath;
        private string _bootstrapperExeName;
        private string _bootstrapperExeFileVersion;
        private string _changeLogFilePath;
        private string _changeLogFileName;
        private string _commandLine = string.Empty;
        private int _connectionTimeoutInSeconds = 0;
        private Status _status = null;
       
        #endregion

        #region Public Properties
        
        /// <summary>
        /// Property: Bootstrapper executable path.
        /// </summary>
        public string BootstrapperExePath
        {
            get { return _bootstrapperExePath; }
            set { _bootstrapperExePath = value; }
        }
        
        /// <summary>
        /// Property: Bootstrapper executable name.
        /// </summary>
        public string BootstrapperExeName
        {
            get { return _bootstrapperExeName; }
            set { _bootstrapperExeName = value; }
        }

        /// <summary>
        /// Property: Bootstrapper executable file version.
        /// </summary>
        public string BootstrapperExeFileVersion
        {
            get { return _bootstrapperExeFileVersion; }
            set { _bootstrapperExeFileVersion = value; }
        }

        /// <summary>
        /// Property: Change log file path.
        /// </summary>
        public string ChangeLogFilePath
        {
            get { return _changeLogFilePath; }
            set { _changeLogFilePath = value; }
        }

        /// <summary>
        /// Property: Change log file name.
        /// </summary>
        public string ChangeLogFileName
        {
            get { return _changeLogFileName; }
            set { _changeLogFileName = value; }
        }

        /// <summary>
        /// Property: Command line.
        /// </summary>
        public string CommandLine
        {
            get
            {
                return _commandLine;
            }
        }

        /// <summary>
        /// Property: Connection timeout in seconds.
        /// </summary>
        public int ConnectionTimeoutInSeconds
        {
            get
            {
                return _connectionTimeoutInSeconds;
            }
            set
            {
                _connectionTimeoutInSeconds = value;
            }
        }

        /// <summary>
        /// Property: Status.
        /// </summary>
        public Status Status
        {
            get { return _status; }
            set { _status = value; }
        }

        /// <summary>
        /// Property: Auto-updater information (context).
        /// </summary>
        public string AutoUpdaterInfo
        {
            get { return string.Format("SidekicksAutoUpdater: Location({0}), Updater Executable Name({1}), Version({2})",
                                       this.BootstrapperExePath,
                                       this.BootstrapperExeName,
                                       this.BootstrapperExeFileVersion);
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekicksAutoUpdater()
        {
            this.Status = new Status();
            this.Status.RunState = RunState.OK;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve bootstrapper executable full path with latest bootstrapper version.
        /// </summary>
        /// <param name="bootstrapperExeRootPath"></param>
        /// <param name="bootstrapperExeName"></param>
        /// <param name="bootstrapperExeFullPath"></param>
        /// <param name="bootstrapperExeFileVersion"></param>
        /// <param name="changeLogFilePath"></param>
        /// <param name="changeLogFileName"></param>
        public void GetBootstrapperExeInformation(string bootstrapperExeRootPath,
                                                  string bootstrapperExeName,
                                                  ref string bootstrapperExeFullPath,
                                                  ref string bootstrapperExeFileVersion,
                                                  ref string changeLogFilePath,
                                                  ref string changeLogFileName)
        {

            List<string> folderNameFromFileSystemList = null;
            List<string> folderNameMatchingVersionPattern = null;
            List<string> folderNameWithBootstrapperExeList = null;
            string versionPattern = @"[Vv]\d+\.\d+\.\d+\.[0-9F]+";
            string versionNumberPattern = @"\d+(?:\.\d+)+";
            Regex versionNumberWithPrefixRegex = null;
            Regex versionNumberRegex = null;
            Match versionNumberMatch = null;
            string versionNumberAsText = string.Empty;
            FileVersionInfo bootstrapperExeFileVersionInfo;
            Version latestVersion = null;
            Version comparisonVersion = null;
            string folderNameWithBootstrapperExe = string.Empty;

            try
            {
                //Create regular expression.
                versionNumberWithPrefixRegex = new Regex(versionPattern, RegexOptions.IgnoreCase);
                versionNumberRegex = new Regex(versionNumberPattern, RegexOptions.IgnoreCase);

                this.BootstrapperExeName = bootstrapperExeName;
                this.BootstrapperExePath = bootstrapperExeRootPath;
                this.BootstrapperExeFileVersion = PropertyUnavailable;
                this.ChangeLogFilePath = changeLogFilePath;
                this.ChangeLogFileName = changeLogFileName;

                //Retrieve sorted list of folders from bootstrapper location.
                folderNameFromFileSystemList = new List<string>(Directory.EnumerateDirectories(bootstrapperExeRootPath).OrderBy(folderName => folderName));

                //Filter out folder names that do NOT match version pattern.
                folderNameMatchingVersionPattern = folderNameFromFileSystemList.Where(folderName => versionNumberWithPrefixRegex.IsMatch(folderName)).ToList();

                //Filter out folders that are NOT containing bootstrapper executable.
                folderNameWithBootstrapperExeList = new List<string>();
                foreach (string folderName in folderNameMatchingVersionPattern)
                {
                    //Format bootstrapper full path name.
                    bootstrapperExeFullPath = Path.Combine(bootstrapperExeRootPath, folderName, bootstrapperExeName);

                    if (File.Exists(bootstrapperExeFullPath) == true)
                    {
                        folderNameWithBootstrapperExeList.Add(folderName);
                    }
                }

                //Iterate folder names with bootstrapper exe list.
                //Find latest version.
                foreach (string folderName in folderNameWithBootstrapperExeList)
                {
                    versionNumberMatch = versionNumberRegex.Match(folderName);
                    versionNumberAsText = versionNumberMatch.Groups[0].Value;

                    if (latestVersion == null)
                    {
                        latestVersion = new Version(versionNumberAsText);
                        folderNameWithBootstrapperExe = folderName;
                    }
                    comparisonVersion = new Version(versionNumberAsText);
                    if (comparisonVersion > latestVersion)
                    {
                        latestVersion = comparisonVersion;
                        folderNameWithBootstrapperExe = folderName;
                    }
                }

                //Format bootstrapper full path.
                bootstrapperExeFullPath = Path.Combine(bootstrapperExeRootPath, folderNameWithBootstrapperExe);

                //Retreive bootstrapper executable file version.
                bootstrapperExeFileVersionInfo = FileVersionInfo.GetVersionInfo(Path.Combine(bootstrapperExeFullPath, bootstrapperExeName));
                bootstrapperExeFileVersion = bootstrapperExeFileVersionInfo.FileVersion;

                //Retrieve change log file path.
                changeLogFilePath = bootstrapperExeFullPath;

                //Retrieve change log file name.
                changeLogFileName = ChangeLogFileName_FileNameOnly;

                this.BootstrapperExeName = bootstrapperExeName;
                this.BootstrapperExePath = bootstrapperExeFullPath;
                this.BootstrapperExeFileVersion = bootstrapperExeFileVersion;
                this.ChangeLogFilePath = changeLogFilePath;
                this.ChangeLogFileName = changeLogFileName;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Determine whether newer version of bootstapper is available when compare to specified version.
        /// </summary>
        /// <param name="currentVersion"></param>
        /// <returns></returns>
        public bool IsNewVersionAvailable(string currentVersion)
        {
            bool result = false;
            int comparisonResult = 0;

            try
            {

                comparisonResult = CompareVersionString(currentVersion, this.BootstrapperExeFileVersion);

                //Current version is a "version" before comparison verison.
                if(comparisonResult < 0)
                {
                    result = true;
                }
                //Current version is the same "version" as comparison version.
                else if (comparisonResult == 0)
                {
                    result = false;
                }
                //Current version is the a "version" subsequent to comparison version.
                else if (comparisonResult > 0)
                {
                    result = false;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(string.Format("Unexpected comparison result. (comparisonResult: {0})",
                                                                        comparisonResult.ToString()));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Compare two version strings.
        /// </summary>
        /// <remarks>
        /// Return value:
        /// Less then zero = The current version is a version before the comparison version.
        /// Zero = The current version is the same as the comparison version.
        /// Greater than zero = The current version is a version subsequent to version.
        /// </remarks>
        /// <param name="currentVersionAsText"></param>
        /// <param name="comparisonVersionAsText"></param>
        /// <returns></returns>
        public int CompareVersionString(String currentVersionAsText, String comparisonVersionAsText)
        {
            int result;
            Version currentVersion = null;
            Version comparisonVersion = null;

            try
            {
                currentVersion = new Version(currentVersionAsText.Replace(",", "."));
                comparisonVersion = new Version(comparisonVersionAsText.Replace(",", "."));

            }
            catch (Exception)
            {
                throw;
            }

            result = currentVersion.CompareTo(comparisonVersion);

            return result;
        }

        /// <summary>
        /// Run bootstrapper executable.
        /// </summary>
        /// <param name="stdout"></param>
        /// <param name="stderr"></param>
        /// <returns></returns>
        public int Run(out string stdout, out string stderr)
        {

            int result = 0;

            stdout = string.Empty;
            stderr = string.Empty;

            ProcessStartInfo processStartInfo = null;
            string bootstrapperExeFullPathName = string.Empty;

            bootstrapperExeFullPathName = Path.Combine(this.BootstrapperExePath, this.BootstrapperExeName);
            this._commandLine = FormatCommandLine();

            using (Process process = new Process())
            {

                try
                {
                    processStartInfo = new ProcessStartInfo(bootstrapperExeFullPathName);
                    processStartInfo.UseShellExecute = false;
                    processStartInfo.RedirectStandardInput = true;
                    processStartInfo.RedirectStandardOutput = true;
                    processStartInfo.RedirectStandardError = true;
                    processStartInfo.CreateNoWindow = false;
                    processStartInfo.Arguments = this.CommandLine;

                    process.StartInfo = processStartInfo;
                    process.EnableRaisingEvents = false;

                    process.Start();

                    result = 0;

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    processStartInfo = null;
                }
            }

            return result;
            
        }

        #endregion
        
        #region Protected Methods

        /// <summary>
        /// Format command line.
        /// </summary>
        /// <returns></returns>
        protected string FormatCommandLine()
        {
            string result = string.Empty;
            try
            {

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Private Methods
        #endregion

    }
}
