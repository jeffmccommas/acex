﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Aclara.Sidekicks.AutoUpdater;

namespace UnitTest
{
    [TestClass]
    public class SidekicksAutoUpdaterManagerTest
    {

        [TestMethod]
        public void GetAutoUpdaterForLatestVersionTest()
        {
            const string BootstrapperLocation = @"\\discovery\software\AclaraSW\Aclara LocalDev Sidekicks\";
            const string BootstrapperExeName = "Aclara.LocalDev.Sidekicks.Bootstrapper.exe";
            
            SidekicksAutoUpdaterManager sidekicksAutoUpdaterManager = null;
            SidekicksAutoUpdater actual = null;
            string bootstrapperExeRootPath = string.Empty;
            string bootstrapperExeName = string.Empty;

            sidekicksAutoUpdaterManager = SidekicksAutoUpdaterManagerFactory.CreateSidekicksUpdateManager();

            //Retrieve sidekicks auto-updater.
            bootstrapperExeRootPath = BootstrapperLocation;
            bootstrapperExeName = BootstrapperExeName;
            actual = sidekicksAutoUpdaterManager.GetSidekicksAutoUpdater(bootstrapperExeRootPath, 
                                                                        bootstrapperExeName);

            Assert.IsNotNull(actual,
                             string.Format("GetAutoUpdaterForLatestVersion: Expected non-null sidekicks auto-updater."));
        }

        [TestMethod]
        public void IsNewVersionAvailableTest()
        {
            const string BootstrapperLocation = @"\\discovery\software\AclaraSW\Aclara LocalDev Sidekicks\";
            const string BootstrapperExeName = "Aclara.LocalDev.Sidekicks.Bootstrapper.exe";
            const string ComparisonVersion = "1.0.1.0";

            SidekicksAutoUpdaterManager sidekicksAutoUpdateManager = null;
            SidekicksAutoUpdater sidekicksAutoUpdater = null;
            string bootstrapperExeRootPath = string.Empty;
            string bootstrapperExeName = string.Empty;
            bool actual = false;

            sidekicksAutoUpdateManager = SidekicksAutoUpdaterManagerFactory.CreateSidekicksUpdateManager();

            //Retrieve sidekicks auto-updater.
            bootstrapperExeRootPath = BootstrapperLocation;
            bootstrapperExeName = BootstrapperExeName;
            sidekicksAutoUpdater = sidekicksAutoUpdateManager.GetSidekicksAutoUpdater(bootstrapperExeRootPath,
                                                                                      bootstrapperExeName);
            Assert.IsNotNull(sidekicksAutoUpdater,
                             string.Format("GetAutoUpdaterForLatestVersion: Expected non-null sidekicks auto-updater."));

            actual = sidekicksAutoUpdater.IsNewVersionAvailable(ComparisonVersion);
            Assert.IsTrue(actual, string.Format("IsNewVersionAvailable: Expected true."));
        }

        [TestMethod]
        public void RunTest()
        {
            const string BootstrapperLocation = @"\\discovery\software\AclaraSW\Aclara LocalDev Sidekicks\";
            const string BootstrapperExeName = "Aclara.LocalDev.Sidekicks.Bootstrapper.exe";
            const string ComparisonVersion = "1.0.1.0";

            SidekicksAutoUpdaterManager sidekicksAutoUpdateManager = null;
            SidekicksAutoUpdater sidekicksAutoUpdater = null;
            string bootstrapperExeRootPath = string.Empty;
            string bootstrapperExeName = string.Empty;
            string stdout = string.Empty;
            string stderr = string.Empty;

            sidekicksAutoUpdateManager = SidekicksAutoUpdaterManagerFactory.CreateSidekicksUpdateManager();

            //Retrieve sidekicks auto-updater.
            bootstrapperExeRootPath = BootstrapperLocation;
            bootstrapperExeName = BootstrapperExeName;
            sidekicksAutoUpdater = sidekicksAutoUpdateManager.GetSidekicksAutoUpdater(bootstrapperExeRootPath,
                                                                                      bootstrapperExeName);
            Assert.IsNotNull(sidekicksAutoUpdater,
                             string.Format("GetAutoUpdaterForLatestVersion: Expected non-null sidekicks auto-updater."));

            if (sidekicksAutoUpdater.IsNewVersionAvailable(ComparisonVersion) == true)
            {
                sidekicksAutoUpdater.Run(out stdout, out stderr);
            }

        }

    }
}
