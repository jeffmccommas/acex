﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Sidekicks.AutoUpdater
{
    public class Status
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private RunState _runState;
        private Exception _exception;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Run state.
        /// </summary>
        public RunState RunState
        {
            get { return _runState; }
            set { _runState = value; }
        }

        /// <summary>
        /// Property: Exception.
        /// </summary>
        public Exception Exception
        {
            get { return _exception; }
            set { _exception = value; }
        }

        #endregion
    }

    /// <summary>
    /// Enumeration: Run state.
    /// </summary>
    public enum RunState
    {
        OK = 0,
        Failed = 1,
        Warning = 2
    }
}
