﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Build.Engine.Client.Interfaces;
using Aclara.Build.Engine.Client.StatusManagement;

namespace Aclara.Build.Engine.Client.Results
{
    
    /// <summary>
    /// Build request completed result factory.
    /// </summary>
    static public class BuildRequestCompletedResultFactory
    {

        static public IBuildRequestCompletedResult CreateBuildCompletedResult()
        {

            IBuildRequestCompletedResult result = null;
            BuildRequestCompletedResult buildCompletedResult = null;

            try
            {

                buildCompletedResult = new BuildRequestCompletedResult();

                result = buildCompletedResult;

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }
    }
}
