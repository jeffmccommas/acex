﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Build.Engine.Client.Results
{
    /// <summary>
    /// Result types.
    /// </summary>
    public class ResultTypes
    {

        /// <summary>
        /// Build request status.
        /// </summary>
        public enum BuildRequestStatus
        {
            Unspecified = 0,
            Success = 1,
            Failure = 2
        }
    }
}
