﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Build.Engine.Client.StatusManagement;
using Aclara.Build.Engine.Client.Interfaces;

namespace Aclara.Build.Engine.Client.Results
{
    /// <summary>
    /// Build request started result.
    /// </summary>
    public class BuildRequestStartedResult : IBuildRequestStartedResult
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private string _solutionListName;
        private string _solutionName;
        private BuildStatus _buildStatus;
        private DateTime _dateStarted;
        private StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Solution list name.
        /// </summary>
        public string SolutionListName
        {
            get { return _solutionListName; }
            set { _solutionListName = value; }
        }

        /// <summary>
        /// Property: Solution name.
        /// </summary>
        public string SolutionName
        {
            get { return _solutionName; }
            set { _solutionName = value; }
        }

        /// <summary>
        /// Property: Build status.
        /// </summary>
        public BuildStatus BuildStatus
        {
            get { return _buildStatus; }
            set { _buildStatus = value; }
        }

        /// <summary>
        /// Property: Date started.
        /// </summary>
        public DateTime DateStarted
        {
            get { return _dateStarted; }
            set { _dateStarted = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildRequestStartedResult()
        {
            _statusList = new StatusList();
        }

        #endregion
    }
}
