﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Build.Engine.Client.Interfaces;
using Aclara.Build.Engine.Client.StatusManagement;

namespace Aclara.Build.Engine.Client.Results
{
    
    /// <summary>
    /// Build request started result factory.
    /// </summary>
    static public class BuildRequestStartedResultFactory
    {

        static public IBuildRequestStartedResult CreateBuildStartedResult()
        {

            IBuildRequestStartedResult result = null;
            BuildRequestStartedResult buildStartedResult = null;

            try
            {

                buildStartedResult = new BuildRequestStartedResult();

                result = buildStartedResult;

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }
    }
}
