﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Build.Engine.Client.StatusManagement;
using Aclara.Build.Engine.Client.Interfaces;

namespace Aclara.Build.Engine.Client.Results
{
    /// <summary>
    /// Build request completed result.
    /// </summary>
    public class BuildRequestCompletedResult : IBuildRequestCompletedResult
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private string _solutionListName;
        private string _solutionName;
        private BuildStatus _buildStatus;
        private DateTime _dateStarted;
        private DateTime _dateCompleted;
        private string buildOutput;
        private List<string> _buildWarningList;
        private List<string> _buildErrorList;
        private StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Solution list name.
        /// </summary>
        public string SolutionListName
        {
            get { return _solutionListName; }
            set { _solutionListName = value; }
        }

        /// <summary>
        /// Property: Solution name.
        /// </summary>
        public string SolutionName
        {
            get { return _solutionName; }
            set { _solutionName = value; }
        }

        /// <summary>
        /// Property: Build status.
        /// </summary>
        public BuildStatus BuildStatus
        {
            get { return _buildStatus; }
            set { _buildStatus = value; }
        }

        /// <summary>
        /// Property: Date started.
        /// </summary>
        public DateTime DateStarted
        {
            get { return _dateStarted; }
            set { _dateStarted = value; }
        }

        /// <summary>
        /// Property: Date completed.
        /// </summary>
        public DateTime DateCompleted
        {
            get { return _dateCompleted; }
            set { _dateCompleted = value; }
        }

        /// <summary>
        /// Property: Build duration.
        /// </summary>
        public TimeSpan BuildDuration
        {
            get 
            { 
                TimeSpan buildDuration = this._dateCompleted - this._dateStarted;
                return buildDuration;
            }
        }

        /// <summary>
        /// Property: Build output.
        /// </summary>
        public string BuildOutput
        {
            get { return buildOutput; }
            set { buildOutput = value; }
        }

        /// <summary>
        /// Property: Build warning list.
        /// </summary>
        public List<string> BuildWarningList
        {
            get { return _buildWarningList; }
            set { _buildWarningList = value; }
        }

        /// <summary>
        /// Property: Build error list.
        /// </summary>
        public List<string> BuildErrorList
        {
            get { return _buildErrorList; }
            set { _buildErrorList = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildRequestCompletedResult()
        {
            _statusList = new StatusList();
        }

        #endregion
    }
}
