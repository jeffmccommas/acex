﻿using Aclara.Build.Engine.Client.StatusManagement;
using Aclara.Build.Engine.Client.Results;
using System;
using System.Collections.Generic;

namespace Aclara.Build.Engine.Client.Interfaces
{

    /// <summary>
    /// Build completed result.
    /// </summary>
    public interface IBuildRequestCompletedResult
    {
        string SolutionListName { get; set; }
        string SolutionName { get; set; }
        BuildStatus BuildStatus { get; set; }
        DateTime DateStarted { get; set; }
        DateTime DateCompleted { get; set; }
        TimeSpan BuildDuration { get; }
        string BuildOutput { get; set; }
        List<string> BuildWarningList { get; set; }
        List<string> BuildErrorList { get; set; }
        StatusList StatusList { get; }
    }
}
