﻿using Aclara.Build.Engine.Client.StatusManagement;
using System;

namespace Aclara.Build.Engine.Client.Interfaces
{

    /// <summary>
    /// Build started result.
    /// </summary>
    public interface IBuildRequestStartedResult
    {
        string SolutionListName { get; set; }
        string SolutionName { get; set; }
        BuildStatus BuildStatus { get; set; }
        DateTime DateStarted { get; set; }
        StatusList StatusList { get; }
    }
}
