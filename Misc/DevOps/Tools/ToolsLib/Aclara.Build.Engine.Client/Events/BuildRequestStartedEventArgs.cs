﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Build.Engine.Client.Interfaces;
using Aclara.Build.Engine.Client.Results;

namespace Aclara.Build.Engine.Client.Events
{
    /// <summary>
    /// Build request started event arguments.
    /// </summary>
    public class BuildRequestStartedEventArgs : EventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IBuildRequestStartedResult _buildStartedResult;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Build started result.
        /// </summary>
        public IBuildRequestStartedResult BuildStartedResult
        {
            get { return _buildStartedResult; }
            set { _buildStartedResult = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildRequestStartedEventArgs()
        {
            _buildStartedResult = new BuildRequestStartedResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="buildStartedResult"></param>
        public BuildRequestStartedEventArgs(IBuildRequestStartedResult buildStartedResult)
        {
            _buildStartedResult = buildStartedResult;
        }

        #endregion

    }
}
