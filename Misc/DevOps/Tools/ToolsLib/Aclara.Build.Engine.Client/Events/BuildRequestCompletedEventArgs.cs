﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Build.Engine.Client.Interfaces;
using Aclara.Build.Engine.Client.Results;

namespace Aclara.Build.Engine.Client.Events
{
    /// <summary>
    /// Build request completed event arguments.
    /// </summary>
    public class BuildRequestCompletedEventArgs : EventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IBuildRequestCompletedResult _buildCompletedResult;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Build completed result.
        /// </summary>
        public IBuildRequestCompletedResult BuildCompletedResult
        {
            get { return _buildCompletedResult; }
            set { _buildCompletedResult = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildRequestCompletedEventArgs()
        {
            _buildCompletedResult = new BuildRequestCompletedResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="buildCompletedResult"></param>
        public BuildRequestCompletedEventArgs(IBuildRequestCompletedResult buildCompletedResult)
        {
            _buildCompletedResult = buildCompletedResult;
        }

        #endregion

    }
}
