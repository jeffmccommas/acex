﻿using Aclara.Build.Engine.Client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace UnitTest
{


    /// <summary>
    ///This is a test class for WebAppManagerTest and is intended
    ///to contain all WebAppManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BuildManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for RunBuild
        ///</summary>
        [TestMethod()]
        public void RunBuildTest()
        {
            const string LogFileName = @"c:\mjones\LocDev_BuildLog.txt";

            BuildManager target = null;
            BuildManagerFactory buildManagerFactory = null;
            BuildSolutionRequestData buildSolutionRequestData = null;
            buildManagerFactory = new BuildManagerFactory();

            buildSolutionRequestData = new BuildSolutionRequestData();
            buildSolutionRequestData.SolutionPath = @"C:\acltfs\EnergyPrism\13.12";
            buildSolutionRequestData.SolutionName = @"Nexus.TestBillData.Website.sln";

            target = buildManagerFactory.CreateBuildManager();

            target.RunBuild(buildSolutionRequestData);
        }

        /// <summary>
        ///A test for RunBuild
        ///</summary>
        [TestMethod()]
        public void RunBuildTest1()
        {
            //const string LogFileName = @"c:\mjones\LocDev_BuildLog.txt";

            //BuildManager target = null;
            //BuildManagerFactory buildManagerFactory = null;
            //BuildSolutionRequestDataList buildSolutionRequestDataList = null;
            //BuildSolutionRequestData buildSolutionRequestData = null;

            //buildManagerFactory = new BuildManagerFactory();

            //buildSolutionRequestDataList = new BuildSolutionRequestDataList();

            //buildSolutionRequestData = new BuildSolutionRequestData();
            //buildSolutionRequestData.SolutionPath = @"C:\acltfs\EnergyPrism\13.12";
            //buildSolutionRequestData.SolutionName = @"Nexus.TestBillData.Website.sln";

            //buildSolutionRequestDataList.Add(buildSolutionRequestData);

            //target = buildManagerFactory.CreateBuildManager();

            //target.RunBuild(buildSolutionRequestDataList);

        }
    
    }
}
