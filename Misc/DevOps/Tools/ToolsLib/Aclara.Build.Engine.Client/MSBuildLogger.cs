﻿using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Build.Engine.Client
{

    /// <summary>
    /// MSBuild logger.
    /// </summary>
    /// <remarks>
    /// Source: https://msdn.microsoft.com/en-us/library/ms171471.aspx
    /// </remarks>
    public class MSBuildLogger : Logger
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private MemoryStream _buildOutputMemoryStream;
        private StreamWriter _buildOutputStreamWriter;
        private List<string> _buildWarningList;
        private List<string> _buildErrorList;
        private string _buildOutput;
        private string _buildWarnings;
        private string _buildErrors;
        private int _indent;

        #endregion

        #region Private Properties

        /// <summary>
        /// Property: Build output tream writer.
        /// </summary>
        private StreamWriter BuildOutputStreamWriter
        {
            get { return _buildOutputStreamWriter; }
            set { _buildOutputStreamWriter = value; }
        }

        /// <summary>
        /// Property: Build output memory stream.
        /// </summary>
        private MemoryStream BuildOutputMemoryStream
        {
            get { return _buildOutputMemoryStream; }
            set { _buildOutputMemoryStream = value; }
        }

        /// <summary>
        /// Property: Build warning list.
        /// </summary>
        public List<string> BuildWarningList
        {
            get { return _buildWarningList; }
            set { _buildWarningList = value; }
        }

        /// <summary>
        /// Property: Build error list.
        /// </summary>
        public List<string> BuildErrorList
        {
            get { return _buildErrorList; }
            set { _buildErrorList = value; }
        }

        /// <summary>
        /// Property: Indent.
        /// </summary>
        private int Indent
        {
            get { return _indent; }
            set { _indent = value; }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Build output.
        /// </summary>
        public string BuildOutput
        {
            get { return _buildOutput; }
            set { _buildOutput = value; }
        }

        /// <summary>
        /// Property: Build warnings.
        /// </summary>
        public string BuildWarnings
        {
            get { return _buildWarnings; }
            set { _buildWarnings = value; }
        }

        /// <summary>
        /// Property: Build errors.
        /// </summary>
        public string BuildErrors
        {
            get { return _buildErrors; }
            set { _buildErrors = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public MSBuildLogger()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize msbuild logger.
        /// </summary>
        /// <remarks>
        /// Initialize is guaranteed to be called by MSBuild at the start of the build
        /// before any events are raised.
        /// </remarks>
        /// <param name="eventSource"></param>
        public override void Initialize(IEventSource eventSource)
        {

            try
            {

                this.BuildOutputMemoryStream = new MemoryStream();
                this.BuildOutputStreamWriter = new StreamWriter(this.BuildOutputMemoryStream, Encoding.UTF8);

                this.BuildWarningList = new List<string>();
                this.BuildErrorList = new List<string>();

            }
            catch (Exception ex)
            {
                if
                (
                    ex is UnauthorizedAccessException ||
                    ex is ArgumentNullException ||
                    ex is PathTooLongException ||
                    ex is DirectoryNotFoundException ||
                    ex is NotSupportedException ||
                    ex is ArgumentException ||
                    ex is SecurityException ||
                    ex is IOException
                )
                {
                    throw new LoggerException("Failed to initialize msbuild logger: " + ex.Message);
                }
                else
                {
                    // Unexpected failure
                    throw;
                }
            }

            // Register events.
            eventSource.ProjectStarted += new ProjectStartedEventHandler(ProjectStarted);
            eventSource.TaskStarted += new TaskStartedEventHandler(TaskStarted);
            eventSource.MessageRaised += new BuildMessageEventHandler(MessageRaised);
            eventSource.WarningRaised += new BuildWarningEventHandler(WarningRaised);
            eventSource.ErrorRaised += new BuildErrorEventHandler(ErrorRaised);
            eventSource.ProjectFinished += new ProjectFinishedEventHandler(ProjectFinished);
        }

        /// <summary>
        /// Shutdown msbuild logger.
        /// </summary>
        /// <remarks>
        /// Shutdown is guaranteed to be called by MSBuild at the end of the build, after all 
        /// events have been raised.
        /// </remarks>
        public override void Shutdown()
        {
            StreamReader buildOutputStreamReader = new StreamReader(this.BuildOutputMemoryStream);

            try
            {
                buildOutputStreamReader = new StreamReader(this.BuildOutputMemoryStream);
                this.BuildOutputMemoryStream.Position = 0;
                this.BuildOutput = buildOutputStreamReader.ReadToEnd();

                BuildOutputStreamWriter.Close();

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Error raised.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ErrorRaised(object sender, BuildErrorEventArgs e)
        {
            string line = string.Empty;

            try
            {
                line = String.Format(": ERROR {0}({1},{2}): ", e.File, e.LineNumber, e.ColumnNumber);

                AddFormattedBuildError(line, e);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Warning raised.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WarningRaised(object sender, BuildWarningEventArgs e)
        {
            string line = string.Empty;

            try
            {
                line = String.Format(": Warning {0}({1},{2}): ", e.File, e.LineNumber, e.ColumnNumber);
                AddFormattedBuildWarning(line, e);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Message raised.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MessageRaised(object sender, BuildMessageEventArgs e)
        {
            string line = string.Empty;

            try
            {

                if ((e.Importance == MessageImportance.High && IsVerbosityAtLeast(LoggerVerbosity.Minimal)) ||
                    (e.Importance == MessageImportance.Normal && IsVerbosityAtLeast(LoggerVerbosity.Normal)) ||
                    (e.Importance == MessageImportance.Low && IsVerbosityAtLeast(LoggerVerbosity.Detailed)))
                {
                    line = string.Empty;
                    WriteLineToBuildOutput(line, e);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Task started.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TaskStarted(object sender, TaskStartedEventArgs e)
        {
        }

        /// <summary>
        /// Event Handler: Project started.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProjectStarted(object sender, ProjectStartedEventArgs e)
        {
            WriteLineToBuildOutput(String.Empty, e);
            _indent++;
        }

        /// <summary>
        /// Event Handler: Project finished.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProjectFinished(object sender, ProjectFinishedEventArgs e)
        {
            _indent--;
            WriteLineToBuildOutput(String.Empty, e);
        }

        /// <summary>
        /// Write line to build output.
        /// </summary>
        private void WriteLineToBuildOutput(string line, BuildEventArgs e)
        {
            for (int i = this.Indent; i > 0; i--)
            {
                BuildOutputStreamWriter.Write("\t");
            }

            if (0 == String.Compare(e.SenderName, "MSBuild", true /*ignore case*/))
            {
                BuildOutputStreamWriter.WriteLine(line + e.Message);
            }
            else
            {
                BuildOutputStreamWriter.WriteLine(e.SenderName + ": " + line + e.Message);
            }

            BuildOutputStreamWriter.Flush();

        }

        /// <summary>
        /// Add build warning.
        /// </summary>
        private void AddFormattedBuildWarning(string line, BuildEventArgs e)
        {

            if (0 == String.Compare(e.SenderName, "MSBuild", true /*ignore case*/))
            {
                this.BuildWarningList.Add(string.Format("{0} {1}", 
                                          line, 
                                          e.Message));
            }
            else
            {
                this.BuildWarningList.Add(string.Format("{0} : {1} {2}",
                                          e.SenderName,
                                          line,
                                          e.Message));
            }

        }

        /// <summary>
        /// Add build error.
        /// </summary>
        private void AddFormattedBuildError(string line, BuildEventArgs e)
        {

            if (0 == String.Compare(e.SenderName, "MSBuild", true /*ignore case*/))
            {
                this.BuildErrorList.Add(string.Format("{0} {1}",
                                        line,
                                        e.Message));
            }
            else
            {
                this.BuildErrorList.Add(string.Format("{0} : {1} {2}",
                                        e.SenderName,
                                        line,
                                        e.Message));
            }


        }

        #endregion


    }
}
