﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.Build.Engine.Client.StatusManagement
{
    public class StatusList : List<Status>
    {

        /// <summary>
        /// Quick determiniation to see if status list has any errors.
        /// </summary>
        /// <returns></returns>
        public bool HasNoErrors()
        {
            return (!HasStatusSeverity(StatusTypes.StatusSeverity.Error));
        }


        /// <summary>
        /// Determine if status list containts specified status severity.
        /// </summary>
        /// <returns></returns>
        public bool HasStatusSeverity(StatusTypes.StatusSeverity statusSeverity)
        {
            bool result = false;
            Status status = null;

            try
            {
                //Find status with matching status severity.
                status = this.Find(statusToFind => statusToFind.StatusServerity == statusSeverity);

                //Status with matching status severtiy found.
                if (status != null)
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Determine if status list containts specified type.
        /// </summary>
        /// <returns></returns>
        public bool HasException(System.Type comparisonType)
        {
            bool result = false;
            Status status = null;

            try
            {

                //Find status with matching exception subtype.
                status = this.Find(statusToFind => statusToFind.Exception.GetType() == comparisonType);

                //Status with matching exception subtype found.
                if (status != null)
                {
                    result = true;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Return status list with all status objects that match specified status severity.
        /// If no statuses match then empty status list is returned.
        /// </summary>
        /// <param name="statusSeverity"></param>
        /// <returns></returns>
        public StatusList GetStatusListByStatusSeverity(StatusTypes.StatusSeverity statusSeverity)
        {
            StatusList result = null;
            List<Status> matchingStatuslist = null;

            matchingStatuslist = this.FindAll(statusToFind => statusToFind.StatusServerity == statusSeverity);

            result = new StatusList();

            result.AddRange(matchingStatuslist);

            return result;
        }

        /// <summary>
        /// Format status list into text.
        /// </summary>
        /// <param name="formatOption"></param>
        /// <returns></returns>
        public string Format(StatusTypes.FormatOption formatOption)
        {
            string result = string.Empty;
            StringBuilder statusListStringBuilder = null;

            try
            {

                statusListStringBuilder = new StringBuilder();

                result = "No status available.";

                foreach (Status status in this)
                {
                    switch (formatOption)
                    {
                        case StatusTypes.FormatOption.Unspecified:
                            break;
                        case StatusTypes.FormatOption.Full:
                            statusListStringBuilder.Append(string.Format("Severity: {0}; Exception Data: {1}; Exception InnerException: {2}; Exception Message: {3}; Exception Source: {4}; Exception Stack Trace: {5}; Exception TargetSite: {6}",
                                                                         status.StatusServerity,
                                                                         status.Exception.Data, 
                                                                         status.Exception.InnerException,
                                                                         status.Exception.Message,
                                                                         status.Exception.Source,
                                                                         status.Exception.StackTrace,
                                                                         status.Exception.TargetSite));
                            break;
                        case StatusTypes.FormatOption.Minimum:
                            statusListStringBuilder.Append(string.Format("Severity: {0}; Exception Message: {1}",
                                                                         status.StatusServerity,
                                                                         status.Exception.Message));
                            break;
                        default:
                            statusListStringBuilder.Append(string.Format("Unexpected format option: {0}",
                                                                         formatOption));
                            break;
                    }
                }
                result = statusListStringBuilder.ToString();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
     }
}
