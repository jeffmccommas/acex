﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.Build.Engine.Client.StatusManagement
{

    /// <summary>
    /// Status management types.
    /// </summary>
    public class StatusTypes
    {

        /// <summary>
        /// Enumeration: Status level.
        /// </summary>
        public enum StatusSeverity
        {
            Unspecified = 0,
            Error = 1,
            Warning = 2,
            Informational = 3
        }

        public enum FormatOption
        {
            Unspecified = 0,
            Full = 1,
            Minimum = 2
        }
    }
}
