﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Build.Engine.Client
{
    /// <summary>
    /// Build solution request data.
    /// </summary>
    public class BuildSolutionRequestData
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private string _solutionPath;
        private string _solutionListName;
        private string _solutionName;
        private Dictionary<string, string> _msbuildProperties;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Solution path.
        /// </summary>
        public string SolutionPath
        {
            get { return _solutionPath; }
            set { _solutionPath = value; }
        }

        /// <summary>
        /// Property: Solution list name.
        /// </summary>
        public string SolutionListName
        {
            get { return _solutionListName; }
            set { _solutionListName = value; }
        }

        /// <summary>
        /// Property: Solution name.
        /// </summary>
        public string SolutionName
        {
            get { return _solutionName; }
            set { _solutionName = value; }
        }

        /// <summary>
        /// Property: MSBuild properties.
        /// </summary>
        public Dictionary<string, string> MSBuildProperties
        {
            get { return _msbuildProperties; }
            set { _msbuildProperties = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildSolutionRequestData()
        {

        }

        #endregion

        #region Protected Constructors

        #endregion

        #region Private Constructors

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

    }
}
