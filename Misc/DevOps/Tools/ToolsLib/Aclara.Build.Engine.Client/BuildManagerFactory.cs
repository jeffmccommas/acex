﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Build.Engine.Client
{
    /// <summary>
    /// Build manager factory.
    /// </summary>
    public class BuildManagerFactory
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Properties

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildManagerFactory()
        {

        }

        #endregion

        #region Protected Constructors

        #endregion

        #region Private Constructors

        #endregion

        #region Public Methods

        /// <summary>
        /// Create build manager.
        /// </summary>
        /// <returns></returns>
        public BuildManager CreateBuildManager()
        {
            BuildManager result = null;

            try
            {
                result = new BuildManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        #endregion

    }
}
