﻿using Aclara.Build.Engine.Client.Events;
using Aclara.Build.Engine.Client.Interfaces;
using Aclara.Build.Engine.Client.Results;
using Aclara.Build.Engine.Client.StatusManagement;
using Microsoft.Build.BuildEngine;
using Microsoft.Build.Construction;
using Microsoft.Build.Evaluation;
using Microsoft.Build.Execution;
using Microsoft.Build.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.Build.Engine.Client
{
    /// <summary>
    /// Enumeration: Build status.
    /// </summary>
    public enum BuildStatus
    {
        Unspecified = 0,
        Started = 1,
        Failed = 2,
        Succeeded = 3
    }

    /// <summary>
    /// Build manager.
    /// </summary>
    public class BuildManager
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Properties

        #endregion

        #region Public Delegates

        public event EventHandler<BuildRequestStartedEventArgs> BuildRequestStarted;
        public event EventHandler<BuildRequestCompletedEventArgs> BuildRequestCompleted;

        #endregion

        #region Public Constructors

        #endregion

        #region Protected Constructors

        #endregion

        #region Private Constructors

        #endregion

        #region Public Methods

        /// <summary>
        /// Run build.
        /// </summary>
        /// <param name="buildSolutionRequestData"></param>
        /// <param name="buildTarget"></param>
        /// <returns></returns>
        public void RunBuild(BuildSolutionRequestData buildSolutionRequestData, string buildTarget)
        {

            string projectFullPath = string.Empty;
            BuildResult buildResult = null;
            Dictionary<string, string> globalPropertiesDictionary = null;
            BuildRequestData buildRequestData = null;
            BuildParameters buildParameters = null;
            MSBuildLogger msbuildLogger = null;
            string[] targetsToBuild = null;

            DateTime dateStarted = DateTime.Now;
            DateTime dateCompleted;

            IBuildRequestStartedResult buildRequestStartedResult = null;
            BuildRequestStartedEventArgs buildRequestStartedEventArgs = null;
            IBuildRequestCompletedResult buildRequestCompletedResult = null;
            BuildRequestCompletedEventArgs buildRequestCompletedEventArgs = null;

            try
            {

                //Format project file full path.
                projectFullPath = Path.Combine(buildSolutionRequestData.SolutionPath,
                                               buildSolutionRequestData.SolutionName);

                ExecuteNuGetRestore(projectFullPath);

                //Format global properties.
                globalPropertiesDictionary = new Dictionary<string, string>();

                foreach (KeyValuePair<string, string> keyValuePair in buildSolutionRequestData.MSBuildProperties)
                {
                    globalPropertiesDictionary.Add(keyValuePair.Key, keyValuePair.Value);
                }

                //Format target names.
                targetsToBuild = new string[] { buildTarget };

                //Create build request data.
                buildRequestData = new BuildRequestData(projectFullPath,
                                                        globalPropertiesDictionary,
                                                        null,
                                                        targetsToBuild,
                                                        null);
                msbuildLogger = new MSBuildLogger();

                using (var pc = new ProjectCollection())
                {
                    //Create build parameters.
                    buildParameters = new BuildParameters(pc)
                    {
                        DetailedSummary = true,
                        Loggers = new List<ILogger>() { msbuildLogger }
                    };

                    //DEBUG: BEGIN.
                    ///tv:15.0 /p:VisualStudioVersion=15.0
                    globalPropertiesDictionary.Add("tv", "15.0");
                    globalPropertiesDictionary.Add("VisualStudioVersion", "15.0");
                    Environment.SetEnvironmentVariable("VSINSTALLDIR", @"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise");
                    Environment.SetEnvironmentVariable("VisualStudioVersion", @"15.0");
                    Environment.SetEnvironmentVariable("MSBUILD_EXE_PATH", @"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\MSBuild.exe");
                    //DeBUG: END.

                    //Create build request started event arguments/result.
                    buildRequestStartedEventArgs = new BuildRequestStartedEventArgs();
                    buildRequestStartedResult = BuildRequestStartedResultFactory.CreateBuildStartedResult();

                    //Set build request started result properties.
                    buildRequestStartedResult.SolutionListName = buildSolutionRequestData.SolutionListName;
                    buildRequestStartedResult.SolutionName = buildSolutionRequestData.SolutionName;
                    dateStarted = DateTime.Now;
                    buildRequestStartedResult.DateStarted = dateStarted;
                    buildRequestStartedResult.BuildStatus = BuildStatus.Started;
                    buildRequestStartedEventArgs.BuildStartedResult = buildRequestStartedResult;

                    if (this.BuildRequestStarted != null)
                    {
                        this.BuildRequestStarted(this, buildRequestStartedEventArgs);
                    }

                    buildResult = Microsoft.Build.Execution.BuildManager.DefaultBuildManager.Build(buildParameters, buildRequestData);

                    //Create buid request completed envent arguments/result.
                    buildRequestCompletedEventArgs = new BuildRequestCompletedEventArgs();
                    buildRequestCompletedResult = BuildRequestCompletedResultFactory.CreateBuildCompletedResult();

                    //Set build request completed result properties.
                    buildRequestCompletedResult.SolutionListName = buildSolutionRequestData.SolutionListName;
                    buildRequestCompletedResult.SolutionName = buildSolutionRequestData.SolutionName;
                    dateCompleted = DateTime.Now;
                    buildRequestCompletedResult.DateStarted = dateStarted;
                    buildRequestCompletedResult.DateCompleted = dateCompleted;
                    if (buildResult.OverallResult == BuildResultCode.Success)
                    {
                        buildRequestCompletedResult.BuildStatus = BuildStatus.Succeeded;
                    }
                    else
                    {
                        buildRequestCompletedResult.BuildStatus = BuildStatus.Failed;
                    }
                    buildRequestCompletedEventArgs.BuildCompletedResult = buildRequestCompletedResult;
                    buildRequestCompletedResult.BuildOutput = msbuildLogger.BuildOutput;
                    buildRequestCompletedResult.BuildWarningList = msbuildLogger.BuildWarningList;
                    buildRequestCompletedResult.BuildErrorList = msbuildLogger.BuildErrorList;

                    if (this.BuildRequestCompleted != null)
                    {
                        this.BuildRequestCompleted(this, buildRequestCompletedEventArgs);
                    }
                }

            }
            catch (Exception ex)
            {
                Status status = null;

                //Create buid request completed envent arguments/result.
                buildRequestCompletedEventArgs = new BuildRequestCompletedEventArgs();
                buildRequestCompletedResult = BuildRequestCompletedResultFactory.CreateBuildCompletedResult();

                //Set build request completed result properties.
                dateCompleted = DateTime.Now;
                buildRequestCompletedResult.DateStarted = dateStarted;
                buildRequestCompletedResult.DateCompleted = dateCompleted;

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                buildRequestCompletedResult.StatusList.Add(status);
                if (this.BuildRequestCompleted != null)
                {
                    this.BuildRequestCompleted(this, buildRequestCompletedEventArgs);
                }
            }
            finally
            {
            }
        }

        /// <summary>
        /// Run build.
        /// </summary>
        /// <param name="buildSolutionRequestDataList"
        /// <param name="buildTarget"></param>
        /// <param name="buildSolutionRequestDataList"></param>
        /// <returns></returns>
        public void RunBuild(BuildSolutionRequestDataList buildSolutionRequestDataList,
                             string buildTarget,
                             CancellationToken cancellationToken)
        {

            try
            {

                foreach (BuildSolutionRequestData buildSolutionRequestData in buildSolutionRequestDataList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    this.RunBuild(buildSolutionRequestData, buildTarget);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Execute NuGet Restore.
        /// </summary>
        /// <param name="solutionNamePath"></param>
        /// <returns></returns>
        protected int ExecuteNuGetRestore(string solutionNamePath)
        {
            const int DefaultProcessTimeoutInSeconds = 60;
            const string NuGetPath = "nuget.exe";
            const string NuGetCommand_Restore = "restore";
            int result;
            ProcessStartInfo processStartInfo = null;
            string psExecFileNamePath = string.Empty;
            int processTimeoutInMilliseconds = 0;
            TimeSpan processTimeoutTimeSpan;
            string stdout;
            string stderr;

            psExecFileNamePath = NuGetPath;

            using (Process process = new Process())
            {

                try
                {
                    processStartInfo = new ProcessStartInfo(psExecFileNamePath);
                    processStartInfo.UseShellExecute = false;
                    processStartInfo.RedirectStandardInput = true;
                    processStartInfo.RedirectStandardOutput = true;
                    processStartInfo.RedirectStandardError = true;
                    processStartInfo.CreateNoWindow = true;
                    processStartInfo.UseShellExecute = false;
                    processStartInfo.Arguments = string.Format("{0} {1}",
                        NuGetCommand_Restore,
                        solutionNamePath);

                    process.StartInfo = processStartInfo;
                    process.EnableRaisingEvents = false;

                    process.Start();

                    //Convert default connection timeout (in seconds) to process timeout time span.
                    processTimeoutTimeSpan = TimeSpan.FromSeconds(DefaultProcessTimeoutInSeconds);

                    //Calculate process timeout (in milliseconds).
                    processTimeoutInMilliseconds = processTimeoutTimeSpan.Milliseconds;

                    process.WaitForExit(processTimeoutInMilliseconds);

                    stdout = process.StandardOutput.ReadToEnd();
                    stderr = process.StandardError.ReadToEnd();

                    result = process.ExitCode;

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    processStartInfo = null;
                }
            }
            return result;
        }

        #endregion
    }
}
