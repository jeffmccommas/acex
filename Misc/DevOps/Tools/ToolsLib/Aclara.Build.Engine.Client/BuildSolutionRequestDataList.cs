﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Build.Engine.Client
{
    /// <summary>
    /// Build solution request data list.
    /// </summary>
    public class BuildSolutionRequestDataList : List<BuildSolutionRequestData>
    {
        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Properties

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildSolutionRequestDataList()
        {

        }

        #endregion

        #region Protected Constructors

        #endregion

        #region Private Constructors

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion
    }
}
