﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Script.Host
{
    /// <summary>
    /// Script host factory.
    /// </summary>
    public class ScriptHostFactory
    {
        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Properties

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ScriptHostFactory()
        {

        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Create script host - command prompt.
        /// </summary>
        /// <returns></returns>
        public ScriptHostCMD CreateScriptHostCMD()
        {
            ScriptHostCMD result = null;

            try
            {
                result = new ScriptHostCMD();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private methods

        #endregion
    }
}
