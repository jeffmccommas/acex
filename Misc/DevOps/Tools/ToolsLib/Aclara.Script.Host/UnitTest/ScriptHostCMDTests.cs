﻿using Aclara.Script.Host;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace UnitTest
{
    /// <summary>
    /// Summary description for ScriptHostManagerTests
    /// </summary>
    [TestClass]
    public class ScriptHostCMDTests
    {
        public ScriptHostCMDTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        [DeploymentItem("TestData\\ScriptHostExecuteBasic.bat")]
        public void ExecuteBasicTest()
        {
            const string ScriptName = "ScriptHostExecuteBasic.bat";
            const string OutputFileName = "testresult.txt";

            string stdout = string.Empty;
            string stderr = string.Empty;
            int result = 0;
            ScriptHostCMD target = null;

            target = new ScriptHostCMD();

            target.ScriptName = ScriptName;
            target.WorkingDirectory = TestContext.DeploymentDirectory;

            target.Arguments = string.Empty;

            result = target.Execute(out stdout, out stderr);

            Assert.IsTrue(result == 0, string.Format("Expected Execute() method to return 0 (zero)."));
            Assert.IsTrue(File.Exists(Path.Combine(target.WorkingDirectory, OutputFileName)) == true, string.Format("Expected ScriptHostCMD.Execute() to generate output file."));
           
        }

        [TestMethod]
        [DeploymentItem("TestData\\ScriptHostExecutePrompt.bat")]
        public void ExecutePromptTest()
        {
            const string ScriptName = "ScriptHostExecutePrompt.bat";
            const string OutputFileName = "testresult.txt";

            string stdout = string.Empty;
            string stderr = string.Empty;
            int result = 0;
            ScriptHostCMD target = null;

            target = new ScriptHostCMD();

            target.ScriptName = ScriptName;
            target.WorkingDirectory = TestContext.DeploymentDirectory;
            target.Arguments = "silent";

            result = target.Execute(out stdout, out stderr);

            Assert.IsTrue(result == 0, string.Format("Expected Execute() method to return 0 (zero)."));
            Assert.IsTrue(File.Exists(Path.Combine(target.WorkingDirectory, OutputFileName)) == true, string.Format("Expected ScriptHostCMD.Execute() to generate output file."));

        }
    }
}
