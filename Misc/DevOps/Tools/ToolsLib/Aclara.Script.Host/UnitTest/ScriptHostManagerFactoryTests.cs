﻿using Aclara.Script.Host;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTest
{
    [TestClass]
    public class ScriptHostManagerFactoryTests
    {
        [TestMethod]
        public void CreateScriptHostManager()
        {

            ScriptHostManagerFactory target = null;
            ScriptHostManager scriptHostManager = null;

            target = new ScriptHostManagerFactory();

            scriptHostManager = target.CreateScriptHostManager();

            Assert.IsNotNull(target, string.Format("Expected ScriptHostManagerFactory.CreateScriptHostManager to return non-null ScriptHostManager."));
        }
    }
}
