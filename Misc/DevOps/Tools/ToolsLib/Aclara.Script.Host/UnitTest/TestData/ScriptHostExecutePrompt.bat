@ECHO OFF
if "%1"=="silent" goto skip_prompt
echo:
ECHO Make sure that above assemblies are in the current folder before continuing.
set /p continueregistration=Do you want to continue (y/n): 
echo:
if %continueregistration% == y goto unregisterdll
goto cancel

:skip_prompt

:unregisterdll

set CURRENT_DATE=%DATE%
set CURRENT_TIME=%TIME%
echo test completed successfully %CURRENT_DATE% %CURRENT_TIME% >> testresult.txt
goto end


:cancel
ECHO Process terminated.
echo test failed >> testresult.txt
goto end

:end
if "%1"=="silent" goto skip_pause
Pause
:skip_pause