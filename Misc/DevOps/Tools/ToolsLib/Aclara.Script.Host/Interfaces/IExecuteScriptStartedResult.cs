﻿using Aclara.Script.Host.StatusManagement;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Script.Host.Interfaces
{
    /// <summary>
    /// Interface: execute script started result.
    /// </summary>
    public interface IExecuteScriptStartedResult
    {
        DateTime DateStarted { get; set; }
        ScriptHost ScriptHost { get; set; }
        string ScriptName { get; }
        string WorkingDirectory { get; }
        string Arguments { get; }
        StatusList StatusList { get; }
    }
}
