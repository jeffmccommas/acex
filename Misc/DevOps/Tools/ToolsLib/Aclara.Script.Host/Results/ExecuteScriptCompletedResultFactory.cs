﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aclara.Script.Host.Interfaces;

namespace Aclara.Script.Host.Results
{
    /// <summary>
    /// Execute script completed result factory.
    /// </summary>
    public class ExecuteScriptCompletedResultFactory
    {

        /// <summary>
        /// Create execute script completed result.
        /// </summary>
        /// <returns></returns>
        static public IExecuteScriptCompletedResult CreateExecuteScriptCompletedResult()
        {
            IExecuteScriptCompletedResult result = null;
            ExecuteScriptCompletedResult executeScriptCompletedResult = null;

            try
            {
                executeScriptCompletedResult = new ExecuteScriptCompletedResult();

                result = executeScriptCompletedResult;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
