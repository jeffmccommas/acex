﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aclara.Script.Host.Interfaces;

namespace Aclara.Script.Host.Results
{
    /// <summary>
    /// Execute script started result factory.
    /// </summary>
    public class ExecuteScriptStartedResultFactory
    {

        /// <summary>
        /// Create execute script started result.
        /// </summary>
        /// <returns></returns>
        static public IExecuteScriptStartedResult CreateExecuteScriptStartedResult()
        {
            IExecuteScriptStartedResult result = null;
            ExecuteScriptStartedResult executeScriptStartedResult = null;

            try
            {
                executeScriptStartedResult = new ExecuteScriptStartedResult();

                result = executeScriptStartedResult;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
