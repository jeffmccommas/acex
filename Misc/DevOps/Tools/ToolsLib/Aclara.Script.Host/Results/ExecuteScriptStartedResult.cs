﻿using Aclara.Script.Host.Interfaces;
using Aclara.Script.Host.StatusManagement;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Script.Host.Results
{
    /// <summary>
    /// Execute script started result.
    /// </summary>
    public class ExecuteScriptStartedResult : IExecuteScriptStartedResult
    {

        #region Private Constants

        private const string PropertyNotAvailable = "N/A";

        #endregion

        #region Private Data Members

        private DateTime _dateStarted;
        private ScriptHost _scriptHost;
        private StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Script name.
        /// </summary>
        public string ScriptName
        {
            get { return this.GetScripName(); }
        }

        /// <summary>
        /// Property: Working directory.
        /// </summary>
        public string WorkingDirectory
        {
            get { return this.GetWorkingDirectory(); }
        }

        /// <summary>
        /// Property: Arguments.
        /// </summary>
        public string Arguments
        {
            get { return this.GetArguments(); }
        }

        /// <summary>
        /// Property: Date started.
        /// </summary>
        public DateTime DateStarted
        {
            get { return _dateStarted; }
            set { _dateStarted = value; }
        }

        /// <summary>
        /// Property: Script host - command.
        /// </summary>
        public ScriptHost ScriptHost
        {
            get { return _scriptHost; }
            set { _scriptHost = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ExecuteScriptStartedResult()
        {
            _statusList = new StatusList();
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Retrieve arguments.
        /// </summary>
        /// <returns></returns>
        protected string GetArguments()
        {
            string result = string.Empty;
            ScriptHostCMD scriptHostCMD = null;

            try
            {
                //Script host not available.
                if (this.ScriptHost == null)
                {
                    result = PropertyNotAvailable;
                    return result;
                }

                if (this.ScriptHost is ScriptHostCMD == true)
                {
                    scriptHostCMD = (ScriptHostCMD)this.ScriptHost;
                    result = scriptHostCMD.Arguments;
                }

                if (this.ScriptHost is ScriptHostPowerShell == true)
                {
                    throw new NotImplementedException(string.Format("Script host not implemented. (Type: {0})",
                                                      this.ScriptHost.GetType()));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve script name.
        /// </summary>
        /// <returns></returns>
        protected string GetScripName()
        {
            string result = string.Empty;
            ScriptHostCMD scriptHostCMD = null;

            try
            {
                //Script host not available.
                if (this.ScriptHost == null)
                {
                    result = PropertyNotAvailable;
                    return result;
                }

                if (this.ScriptHost is ScriptHostCMD == true)
                {
                    scriptHostCMD = (ScriptHostCMD)this.ScriptHost;
                    result = scriptHostCMD.ScriptName;
                }

                if (this.ScriptHost is ScriptHostPowerShell == true)
                {
                    throw new NotImplementedException(string.Format("Script host not implemented. (Type: {0})",
                                                      this.ScriptHost.GetType()));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve working directory.
        /// </summary>
        /// <returns></returns>
        protected string GetWorkingDirectory()
        {
            string result = string.Empty;
            ScriptHostCMD scriptHostCMD = null;

            try
            {
                //Script host not available.
                if (this.ScriptHost == null)
                {
                    result = PropertyNotAvailable;
                    return result;
                }

                if (this.ScriptHost is ScriptHostCMD == true)
                {
                    scriptHostCMD = (ScriptHostCMD)this.ScriptHost;
                    result = scriptHostCMD.WorkingDirectory;
                }

                if (this.ScriptHost is ScriptHostPowerShell == true)
                {
                    throw new NotImplementedException(string.Format("Script host not implemented. (Type: {0})",
                                                      this.ScriptHost.GetType()));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

    }
}
