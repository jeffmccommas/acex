﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Script.Host
{
    /// <summary>
    /// Script host manager factory.
    /// </summary>
    public class ScriptHostManagerFactory
    {
        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Properties

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ScriptHostManagerFactory()
        {

        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Create script host manager.
        /// </summary>
        /// <returns></returns>
        public ScriptHostManager CreateScriptHostManager()
        {
            ScriptHostManager result = null;

            try
            {
                result = new ScriptHostManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private methods

        #endregion

    }
}
