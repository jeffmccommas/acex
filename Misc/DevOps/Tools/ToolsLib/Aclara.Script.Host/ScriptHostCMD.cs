﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.Script.Host
{
    /// <summary>
    /// Script host - CMD.
    /// </summary>
    public class ScriptHostCMD : ScriptHost
    {
        #region Private Constants

        private string Cmd = "cmd.exe";

        #endregion

        #region Private Data Members

        private string _scriptName;
        private string _workingDirectory;
        private string _arguments;
        private StringDictionary _environmentVariables;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Script name.
        /// </summary>
        public string ScriptName
        {
            get { return _scriptName; }
            set { _scriptName = value; }
        }
        
        /// <summary>
        /// Property: Working directory.
        /// </summary>
        public string WorkingDirectory
        {
            get { return _workingDirectory; }
            set { _workingDirectory = value; }
        }

        /// <summary>
        /// Property: Arguments.
        /// </summary>
        public string Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        /// <summary>
        /// Property: Environment variables.
        /// </summary>
        public StringDictionary EnvironmentVariables
        {
            get { return _environmentVariables; }
            set { _environmentVariables = value; }
        }
        
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ScriptHostCMD()
            : base()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Execute script.
        /// </summary>
        /// <param name="stdout"></param>
        /// <param name="stderr"></param>
        /// <returns></returns>
        public int Execute(out string stdout, out string stderr)
        {
            const int Sleep_TwoSeconds = 2000;
            const int ScriptExecutionTimeout = 10;

            int result;
            ProcessStartInfo processStartInfo;
            stdout = string.Empty;
            stderr = string.Empty;
            DateTime processStartDateTime;
            TimeSpan duration;

            using (Process process = new Process())
            {

                try
                {

                    processStartInfo = new ProcessStartInfo();

                    processStartInfo.UseShellExecute = false;
                    processStartInfo.RedirectStandardInput = true;
                    processStartInfo.RedirectStandardOutput = true;
                    processStartInfo.RedirectStandardError = true;
                    processStartInfo.CreateNoWindow = true;

                    processStartInfo.FileName = Cmd;
                    processStartInfo.WorkingDirectory = this.WorkingDirectory;
                    processStartInfo.Arguments = string.Format("/C \"{0}\" {1}",
                                                               this.ScriptName,
                                                               this.Arguments);

                    process.StartInfo = processStartInfo;
                    process.EnableRaisingEvents = true;
                    processStartDateTime = DateTime.Now;

                    process.Start();

                    while (process.HasExited == false)
                    {
                        Thread.Sleep(Sleep_TwoSeconds);

                        //Retrieve standard output to ensure process does not hang.
                        stdout += process.StandardOutput.ReadToEnd();
                        stderr += process.StandardError.ReadToEnd();

                        //Calculate execution time.
                        duration = DateTime.Now - processStartDateTime;

                        //Maximum execution time exceeded.
                        if (duration.TotalMinutes > ScriptExecutionTimeout)
                        {
                            stderr = string.Format("Script exceeded maximum execution time. (Maximum execution time - minutes: {0})",
                                                   duration.TotalMinutes);
                            result = 1;
                            return result;
                        }
                    }

                    stdout = process.StandardOutput.ReadToEnd();
                    stderr = process.StandardError.ReadToEnd();

                    result = process.ExitCode;

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    processStartInfo = null;
                }
            }
            return result;
        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private methods

        #endregion

    }
}
