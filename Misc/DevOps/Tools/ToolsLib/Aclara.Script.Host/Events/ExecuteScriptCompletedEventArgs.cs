﻿using Aclara.Script.Host.Events;
using Aclara.Script.Host.Interfaces;
using Aclara.Script.Host.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Script.Host.Events
{
    /// <summary>
    /// Execute script completed event arguments.
    /// </summary>
    public class ExecuteScriptCompletedEventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IExecuteScriptCompletedResult _executeScriptCompletedResult;

        #endregion
        
        #region Public Properties

        /// <summary>
        /// Property: Execute script completed result.
        /// </summary>
        public IExecuteScriptCompletedResult ExecuteScriptCompletedResult
        {
            get { return _executeScriptCompletedResult; }
            set { _executeScriptCompletedResult = value; }
        }

        #endregion

        #region Public Constructors
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ExecuteScriptCompletedEventArgs()
        {
            _executeScriptCompletedResult = new ExecuteScriptCompletedResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="executeScriptCMDResult"></param>
        public ExecuteScriptCompletedEventArgs(IExecuteScriptCompletedResult executeScriptCMDResult)
        {
            _executeScriptCompletedResult = executeScriptCMDResult;
        }

        #endregion

    }
}
