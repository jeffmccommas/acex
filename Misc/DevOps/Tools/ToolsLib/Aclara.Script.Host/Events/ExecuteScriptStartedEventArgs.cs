﻿using Aclara.Script.Host.Events;
using Aclara.Script.Host.Interfaces;
using Aclara.Script.Host.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Script.Host.Events
{
    /// <summary>
    /// Execute script started event arguments.
    /// </summary>
    public class ExecuteScriptStartedEventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IExecuteScriptStartedResult _executeScriptStartedResult;

        #endregion
        
        #region Public Properties

        /// <summary>
        /// Property: Execute script started result.
        /// </summary>
        public IExecuteScriptStartedResult ExecuteScriptStartedResult
        {
            get { return _executeScriptStartedResult; }
            set { _executeScriptStartedResult = value; }
        }

        #endregion

        #region Public Constructors
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ExecuteScriptStartedEventArgs()
        {
            _executeScriptStartedResult = new ExecuteScriptStartedResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="executeScriptStartedResult"></param>
        public ExecuteScriptStartedEventArgs(IExecuteScriptStartedResult executeScriptStartedResult)
        {
            _executeScriptStartedResult = executeScriptStartedResult;
        }

        #endregion

    }
}
