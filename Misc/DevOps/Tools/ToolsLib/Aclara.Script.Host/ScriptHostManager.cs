﻿using Aclara.Script.Host.Events;
using Aclara.Script.Host.Interfaces;
using Aclara.Script.Host.Results;
using Aclara.Script.Host.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.Script.Host
{
    /// <summary>
    /// Script host manager.
    /// </summary>
    public class ScriptHostManager
    {
        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Properties

        #endregion

        #region Delegates

        public event EventHandler<ExecuteScriptStartedEventArgs> ExecuteScriptStarted;
        public event EventHandler<ExecuteScriptCompletedEventArgs> ExecuteScriptCompleted;

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ScriptHostManager()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Execute script.
        /// </summary>
        /// <remarks>
        /// Execute a single script.
        /// </remarks>
        /// <param name="scriptHost"></param>
        /// <returns></returns>
        public void ExecuteScript(ScriptHost scriptHost)
        {
            ScriptHostCMD scriptHostCMD = null;
            string stdout = string.Empty;
            string stderr = string.Empty;
            int exitCode = 0;
            Status status = null;

            DateTime dateStarted = DateTime.Now;
            DateTime dateCompleted;

            IExecuteScriptStartedResult executeScriptStartedResult = null;
            ExecuteScriptStartedEventArgs executeScriptStartedEventArgs = null;
            IExecuteScriptCompletedResult executeScriptCompletedResult = null;
            ExecuteScriptCompletedEventArgs executeScriptCompletedEventArgs = null;

            try
            {

                //Validate input parameter: Script host.
                if (scriptHost == null)
                {
                    throw new ArgumentException(string.Format("Script host is required."));
                }

                //Validate input parameter: Script host type.
                if ((scriptHost is ScriptHostCMD) == false &&
                    (scriptHost is ScriptHostPowerShell) == false)
                {
                    throw new NotImplementedException(string.Format("Script host not implemented. (Type: {0})",
                                                      scriptHost.GetType()));
                }

                //Create execute script started event arguments/result.
                executeScriptStartedEventArgs = new ExecuteScriptStartedEventArgs();
                executeScriptStartedResult = ExecuteScriptStartedResultFactory.CreateExecuteScriptStartedResult();

                //Create execute script completed event arguments/result.
                executeScriptCompletedEventArgs = new ExecuteScriptCompletedEventArgs();
                executeScriptCompletedResult = ExecuteScriptCompletedResultFactory.CreateExecuteScriptCompletedResult();

                //Set execute script started result properties.
                executeScriptStartedResult.ScriptHost = scriptHost;
                dateStarted = DateTime.Now;
                executeScriptStartedResult.DateStarted = dateStarted;
                executeScriptStartedEventArgs.ExecuteScriptStartedResult = executeScriptStartedResult;

                if (this.ExecuteScriptStarted != null)
                {
                    this.ExecuteScriptStarted(this, executeScriptStartedEventArgs);
                }

                //Execute script - via command prompt.
                if ((scriptHost is ScriptHostCMD) == true)
                {
                    scriptHostCMD = (ScriptHostCMD)scriptHost;

                    exitCode = scriptHostCMD.Execute(out stdout, out stderr);

                    if (exitCode > 0)
                    {
                        status = new Status();
                        status.Exception = new ApplicationException(string.Format("Execute script command failed with exit code {0}. Error: {1}",
                                                                     exitCode, stderr));
                        status.StatusServerity = StatusTypes.StatusSeverity.Error;
                        executeScriptCompletedResult.StatusList.Add(status);
                    }

                }

                //Execute script - via PowerShell.
                //NOTE: Currently not supported.
                if ((scriptHost is ScriptHostPowerShell) == true)
                {
                    throw new NotImplementedException(string.Format("PowerShell script host not implemented."));
                }

                //Set execute script completed result properties.
                executeScriptCompletedResult.ScriptHost = scriptHost;
                dateCompleted = DateTime.Now;
                executeScriptCompletedResult.DateStarted = dateStarted;
                executeScriptCompletedResult.DateCompleted = dateCompleted;
                executeScriptCompletedEventArgs.ExecuteScriptCompletedResult = executeScriptCompletedResult;

                if (this.ExecuteScriptCompleted != null)
                {
                    this.ExecuteScriptCompleted(this, executeScriptCompletedEventArgs);
                }

            }
            catch (Exception ex)
            {
                //Create execute script completed envent arguments/result.
                executeScriptCompletedEventArgs = new ExecuteScriptCompletedEventArgs();
                executeScriptCompletedResult = ExecuteScriptCompletedResultFactory.CreateExecuteScriptCompletedResult();

                //Set execute script completed result properties.
                executeScriptStartedResult.ScriptHost = scriptHost;
                dateCompleted = DateTime.Now;
                executeScriptCompletedResult.DateStarted = dateStarted;
                executeScriptCompletedResult.DateCompleted = dateCompleted;
                executeScriptCompletedEventArgs.ExecuteScriptCompletedResult = executeScriptCompletedResult;

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                executeScriptCompletedResult.StatusList.Add(status);

                if (this.ExecuteScriptCompleted != null)
                {
                    this.ExecuteScriptCompleted(this, executeScriptCompletedEventArgs);
                }
            }

            return;
        }

        /// <summary>
        /// Execute script.
        /// </summary>
        /// <remarks>
        /// Execute a list of scripts.
        /// </remarks>
        /// <param name="ScriptHostList"></param>
        public void ExecuteScript(ScriptHostList scriptHostList)
        {

            try
            {
                foreach (ScriptHost scriptHost in scriptHostList)
                {
                    this.ExecuteScript(scriptHost);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Execute script.
        /// </summary>
        /// <remarks>
        /// Execute a list of scripts on a thread with the ability to cancel.
        /// </remarks>
        /// <param name="scriptHost"></param>
        /// <param name="cancellationToken"></param>
        public void ExecuteScript(ScriptHostList scriptHostList,
                                  CancellationToken cancellationToken)
        {

            try
            {
                foreach (ScriptHost scriptHost in scriptHostList)
                {

                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    this.ExecuteScript(scriptHost);

                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private methods

        #endregion

    }
}
