﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.NuGet.Core.Client.Models
{
    public class NuGetPackageInfo
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private string _packageId;
        private NuGetPackageVersionList _nugetPackageVersionList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Package id.
        /// </summary>
        public string PackageId
        {
            get { return _packageId; }
            set { _packageId = value; }
        }

        /// <summary>
        /// Property: NuGet package version list.
        /// </summary>
        public NuGetPackageVersionList NuGetPackageVersionList
        {
            get { return _nugetPackageVersionList; }
            set { _nugetPackageVersionList = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public NuGetPackageInfo()
        {
            _nugetPackageVersionList = new NuGetPackageVersionList();
        }

        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }
}
