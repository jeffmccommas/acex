﻿using Aclara.NuGet.Core.Client.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aclara.NuGet.Core.Client.Models
{

    /// <summary>
    /// NuGet package information list.
    /// </summary>
    public class NuGetPackageInfoList : List<NuGetPackageInfo>
    {
        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors
        #endregion

        #region Public Methods

        /// <summary>
        /// Add or replace NuGet package info (if already exist).
        /// </summary>
        /// <param name="nuGetPackageInfo"></param>
        public void AddOrReplace(NuGetPackageInfo nuGetPackageInfo)
        {
            try
            {
                var teampNuGetPackageInfo = this.Where(i => i.PackageId == nuGetPackageInfo.PackageId).FirstOrDefault();
                var index = this.IndexOf(teampNuGetPackageInfo);

                if (index != -1)
                {
                    this.RemoveAt(index);
                    this.Add(nuGetPackageInfo);
                }
                else
                {
                    this.Add(nuGetPackageInfo);
                }

            }
            catch (System.Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Retrieve NuGet package info list filtered by package id pattern.
        /// </summary>
        /// <param name="packageIdPattern"></param>
        /// <param name="exactMatch"></param>
        /// <returns></returns>
        public NuGetPackageInfoList GetNuGetPackageInfoListFilterdByPackageIdPattern(string packageIdPattern,
                                                                                     bool exactMatch)
        {
            NuGetPackageInfoList result = null;

            try
            {
                result = new NuGetPackageInfoList();
                if (string.IsNullOrEmpty(packageIdPattern) == true)
                {
                    result = this;
                }
                else
                {
                    var filteredBuildDefinitionList = Wildcard.FilterListByNamePropertyAndPattern(this, packageIdPattern, exactMatch);

                    foreach (NuGetPackageInfo buildDefinitionSelector in filteredBuildDefinitionList)
                    {
                        result.Add(buildDefinitionSelector);
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }
}
