﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.NuGet.Core.Client.Models
{
    /// <summary>
    /// NeGut package version.
    /// </summary>
    public class NuGetPackageVersion
    {


        #region Private Constants
        #endregion

        #region Private Data Members

        private string _version;
        private List<string> _referencedPathList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Verison.
        /// </summary>
        public string Version
        {
            get { return _version; }
            set { _version = value; }
        }

        /// <summary>
        /// Property: Reference path list.
        /// </summary>
        public List<string> ReferencedPathList
        {
            get { return _referencedPathList; }
            set { _referencedPathList = value; }
        }

        #endregion

        #region Public Constructors
        public NuGetPackageVersion()
        {
            _referencedPathList = new List<string>();
        }

        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
