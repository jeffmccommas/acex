﻿using Aclara.NuGet.Core.Client.Events;
using Aclara.NuGet.Core.Client.Models;
using Aclara.Tools.Common.StatusManagement;
using NuGet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Aclara.NuGet.Core.Client
{

    /// <summary>
    /// NuGet core manager.
    /// </summary>
    public class NuGetCoreManager
    {
        #region Private Constants

        private const string NuGetPackageFileName = "packages.config";

        #endregion

        #region Private Data Members

        private string _rootPathFolderName;

        #endregion

        #region Public Delegates

        public event EventHandler<GetNuGetPackageInformationEventArgs> GetNuGetPackageInformationCompleted;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Root path folder name.
        /// </summary>
        public string RootPathFolderName
        {
            get { return _rootPathFolderName; }
            set { _rootPathFolderName = value; }
        }

        #endregion

        #region Public Constructors
        #endregion

        #region Protected Constructors
        #endregion

        #region Public Methods

        public void GetNuGetPackageInformation(string rootPathFolerName,
                                               CancellationToken cancellationToken)
        {
            GetNuGetPackageInformationEventArgs getNuGetPackageInformationEventArgs = null;
            int nuGetPackageInfoTotalCount = 0;
            int nuGetPackageInfoRetrievedCount = 0;
            List<string> fileNameList = null;
            NuGetPackageInfoList nugetPackageInfoList = null;
            NuGetPackageInfo nugetPackageInfo = null;
            NuGetPackageVersion nugetPackageVersion = null;
            PackageReferenceFile packageReferenceFile = null;

            try
            {
                fileNameList = Directory.GetFiles(rootPathFolerName, NuGetPackageFileName, SearchOption.AllDirectories).ToList();

                nuGetPackageInfoRetrievedCount = fileNameList.Count();
                nugetPackageInfoList = new NuGetPackageInfoList();

                foreach (string fileName in fileNameList)
                {
                    nuGetPackageInfoTotalCount++;

                    packageReferenceFile = new PackageReferenceFile(fileName);

                    foreach (PackageReference packageReference in packageReferenceFile.GetPackageReferences())
                    {
                        nugetPackageInfo = nugetPackageInfoList.FirstOrDefault(x => x.PackageId == packageReference.Id);

                        if (nugetPackageInfo == null)
                        {
                            nugetPackageInfo = new NuGetPackageInfo
                            {
                                PackageId = packageReference.Id
                            };
                            nugetPackageInfoList.Add(nugetPackageInfo);
                        }

                        nugetPackageVersion = nugetPackageInfo.NuGetPackageVersionList.FirstOrDefault(x => x.Version == packageReference.Version.ToString());
                        if (nugetPackageVersion == null)
                        {
                            nugetPackageVersion = new NuGetPackageVersion
                            {
                                Version = packageReference.Version.ToString()
                            };
                            nugetPackageInfo.NuGetPackageVersionList.Add(nugetPackageVersion);
                        }

                        nugetPackageVersion.ReferencedPathList.Add(fileName);

                        getNuGetPackageInformationEventArgs = new GetNuGetPackageInformationEventArgs();
                        getNuGetPackageInformationEventArgs.NuGetPackageInfo = nugetPackageInfo;
                        getNuGetPackageInformationEventArgs.NuGetPackageInfoTotalCount = nuGetPackageInfoRetrievedCount;
                        getNuGetPackageInformationEventArgs.NuGetPackageInfoRetrievedCount = nuGetPackageInfoTotalCount;

                        if (GetNuGetPackageInformationCompleted != null)
                        {
                            GetNuGetPackageInformationCompleted(this, getNuGetPackageInformationEventArgs);
                        }
                    }


                }


            }
            catch (Exception ex)
            {
                Status status = null;

                getNuGetPackageInformationEventArgs = new GetNuGetPackageInformationEventArgs();
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                getNuGetPackageInformationEventArgs.StatusList.Add(status);
            }
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion


        static void GetNuGetPackageInformationWIP(string[] args)
        {
            string path = @"C:\Work\Repository\TFS\Research";
            string[] files = Directory.GetFiles(path, "packages.config", SearchOption.AllDirectories);

            const string format = "{0},{1},{2}";
            StringBuilder builder = new StringBuilder();
            List<SearchResults> results = new List<SearchResults>();

            foreach (var fileName in files)
            {
                var file = new PackageReferenceFile(fileName);
                foreach (PackageReference packageReference in file.GetPackageReferences())
                {
                    SearchResults currentResult = results.FirstOrDefault(x => x.PackageId == packageReference.Id);
                    if (currentResult == null)
                    {
                        currentResult = new SearchResults
                        {
                            PackageId = packageReference.Id
                        };
                        results.Add(currentResult);
                    }

                    SearchResults.SearchVersion currentVersion = currentResult.Versions.FirstOrDefault(x => x.PackageVersion == packageReference.Version.ToString());
                    if (currentVersion == null)
                    {
                        currentVersion = new SearchResults.SearchVersion
                        {
                            PackageVersion = packageReference.Version.ToString()
                        };
                        currentResult.Versions.Add(currentVersion);
                    }

                    currentVersion.Path.Add(fileName);
                }
            }

            results.ForEach(result =>
            {
                if (result.Versions.Count > 1) // multiple versions of same package
                {
                    result.Versions.ForEach(version =>
                    {
                        version.Path.ForEach(packagePath =>
                        {
                            builder.AppendFormat(format, result.PackageId, packagePath, version.PackageVersion);
                            builder.AppendLine();
                        });
                    });
                }
            });
            File.WriteAllText(Path.Combine(path, "packages-duplicates.csv"), builder.ToString());

        }
    }

    public class SearchResults
    {
        public string PackageId { get; set; }
        public List<SearchVersion> Versions { get; set; }
        public SearchResults()
        {
            Versions = new List<SearchResults.SearchVersion>();
        }

        public class SearchVersion
        {
            public string PackageVersion { get; set; }
            public List<string> Path { get; set; }
            public SearchVersion()
            {
                Path = new List<string>();
            }
        }
    }
}
