﻿using Aclara.Tools.Common.StatusManagement;
using NuGet.Runtime;
using NuGet.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aclara.NuGet.Core.Client.Models;

namespace Aclara.NuGet.Core.Client.Events
{
    public class GetNuGetPackageInformationEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private NuGetPackageInfo _nuGetPackageInfo;
        private int _nuGetPackageInfoTotalCount;
        private int _nuGetPackageInfoRetrievedCount;
        StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: NuGet package information.
        /// </summary>
        public NuGetPackageInfo NuGetPackageInfo
        {
            get { return _nuGetPackageInfo; }
            set { _nuGetPackageInfo = value; }
        }

        /// <summary>
        /// Property: NuGet package info total count.
        /// </summary>
        public int NuGetPackageInfoTotalCount
        {
            get { return _nuGetPackageInfoTotalCount; }
            set { _nuGetPackageInfoTotalCount = value; }
        }

        /// <summary>
        /// Property: NuGet package info retrieved count.
        /// </summary>
        public int NuGetPackageInfoRetrievedCount
        {
            get { return _nuGetPackageInfoRetrievedCount; }
            set { _nuGetPackageInfoRetrievedCount = value; }
        }

        /// <summary>
        /// Property : Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GetNuGetPackageInformationEventArgs()
        {
            _statusList = new StatusList();
        }
        #endregion
    }
}
