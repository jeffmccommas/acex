﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.NuGet.Core.Client
{
    public static class NuGetCoreManagerFactory
    {
        #region Public Methods

        /// <summary>
        /// Create NuGet core manager.
        /// </summary>
        /// <returns></returns>
        public static NuGetCoreManager CreateNuGetCoreManager()
        {
            NuGetCoreManager result = null;
            try
            {
                result = new NuGetCoreManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


        #endregion
    }
}
