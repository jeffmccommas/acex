﻿using Aclara.AzureRedisCache.Client.Interfaces;
using Aclara.AzureRedisCache.Client.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzureRedisCache.Client.Events
{
    /// <summary>
    /// Azure Redis cache administration status event arguments.
    /// </summary>
    public class AzureRedisCacheAdminStatusEventArgs : EventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private IAzureRedisCacheAdminStatusResult _azureRedisCacheAdminStatusResult;

        #endregion


        #region Public Propertiers

        /// <summary>
        /// Property: Azure Redis cache administration result.
        /// </summary>
        public IAzureRedisCacheAdminStatusResult AzureRedisCacheAdminStatusResult
        {
            get { return _azureRedisCacheAdminStatusResult; }
            set { _azureRedisCacheAdminStatusResult = value; }
        }
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AzureRedisCacheAdminStatusEventArgs()
        {
            _azureRedisCacheAdminStatusResult = new AzureRedisCacheAdminStatusResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        public AzureRedisCacheAdminStatusEventArgs(IAzureRedisCacheAdminStatusResult azureRedisCacheAdminStatusResult)
        {
            _azureRedisCacheAdminStatusResult = azureRedisCacheAdminStatusResult;
        }

        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
