﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzureRedisCache.Client.Types
{
    public class Enumerations
    {
       public enum AzureRedisCacheAdminOperation
        {
            Unspecified = 0,
            ClearAzureRedisCache = 1
        };
    }
}
