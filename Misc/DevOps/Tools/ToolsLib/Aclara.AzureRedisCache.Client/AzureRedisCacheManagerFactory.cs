﻿using Aclara.Tools.Configuration.AzureRedisCacheConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzureRedisCache.Client
{
    public static class AzureRedisCacheManagerFactory
    {

        public static AzureRedisCacheManager CreateRedisCacheManager(AzureRedisCacheConfig azureRedisCacheConfig)
        {
            AzureRedisCacheManager result = null;
            ContentCacheProviderRedisAzure constentCacheProvider = null;

            try
            {
                constentCacheProvider = CreateContentCacheProvider(azureRedisCacheConfig);

                result = new AzureRedisCacheManager(constentCacheProvider);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        private static ContentCacheProviderRedisAzure CreateContentCacheProvider(AzureRedisCacheConfig azureRedisCacheConfig)
        {
            ContentCacheProviderRedisAzure result;



            result = new ContentCacheProviderRedisAzure(azureRedisCacheConfig);
            
            return result;
        }
    }
}
