﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aclara.AzureRedisCache.Client.Interfaces;

namespace Aclara.AzureRedisCache.Client.Results
{

    /// <summary>
    /// Azure Redis cache administration status result factory.
    /// </summary>
    public static class AzureRedisCacheAdminStatusResultFactory
    {

        /// <summary>
        /// Create Azure Redis cache administration status result.
        /// </summary>
        /// <returns></returns>
        static public IAzureRedisCacheAdminStatusResult CreateAzureRedisCacheAdminStatusResult()
        {
            IAzureRedisCacheAdminStatusResult result = null;
            AzureRedisCacheAdminStatusResult azureRedisCacheAdminStatusResult = null;

            try
            {
                azureRedisCacheAdminStatusResult = new AzureRedisCacheAdminStatusResult();

                result = azureRedisCacheAdminStatusResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
