﻿using Aclara.AzureRedisCache.Client.Interfaces;
using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzureRedisCache.Client.Results
{
    /// <summary>
    /// Azure Redis cache administration status result.
    /// </summary>
    public class AzureRedisCacheAdminStatusResult : IAzureRedisCacheAdminStatusResult
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private string _statusMessage;
        private StatusList _statusList;

        #endregion

        #region Public Properties

        public string StatusMessage
        {
            get { return _statusMessage; }
            set { _statusMessage = value; }
        }

        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AzureRedisCacheAdminStatusResult()
        {
            _statusList = new StatusList();
        }

        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Privatre Methods
        #endregion


    }
}
