﻿using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzureRedisCache.Client.Interfaces
{
    /// <summary>
    /// Azure Redis cache administration status result interface.
    /// </summary>
    public interface IAzureRedisCacheAdminStatusResult
    {
        String StatusMessage { get; set; }
        StatusList StatusList { get; }
    }
}
