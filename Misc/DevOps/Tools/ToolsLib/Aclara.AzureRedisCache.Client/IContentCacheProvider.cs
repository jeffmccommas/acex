﻿using System;

namespace Aclara.AzureRedisCache.Client
{
    public interface IContentCacheProvider
    {

        /// <summary>
        /// Store content in cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="absoluteExpiration"></param>
        /// <param name="regionName"></param>
        void Set(string key, object value, DateTimeOffset absoluteExpiration, string regionName = null);

        /// <summary>
        /// Retrieve content from cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="regionName"></param>
        /// <returns></returns>
        object Get(string key, string regionName = null);

        /// <summary>
        /// Remove content from cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="regionName"></param>
        /// <returns></returns>
        object Remove(string key, string regionName = null);

        /// <summary>
        /// Flush all databases.
        /// </summary>
        void FlushAllDatabases();
    }
}
