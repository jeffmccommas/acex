﻿using Aclara.AzureRedisCache.Client.Events;
using Aclara.AzureRedisCache.Client.Interfaces;
using Aclara.AzureRedisCache.Client.Results;
using Aclara.Tools.Common.StatusManagement;
using Aclara.Tools.Configuration.AzureRedisCacheConfig;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Aclara.AzureRedisCache.Client.Types.Enumerations;

namespace Aclara.AzureRedisCache.Client
{
    /// <summary>
    /// Azure redis cache manager.
    /// </summary>
    public class AzureRedisCacheManager
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        ContentCacheProviderRedisAzure _contentCacheProvider;
        #endregion

        #region Public Delegates

        public event EventHandler<AzureRedisCacheAdminStatusEventArgs> AzureRedisCacheAdminStatusUpdated;

        #endregion

        #region Public Properties
        #endregion

        #region Private Properties

        /// <summary>
        /// Property: Content cache provider.
        /// </summary>
        private ContentCacheProviderRedisAzure ContentCacheProvider
        {
            get { return _contentCacheProvider; }
            set { _contentCacheProvider = value; }
        }

        #endregion

        #region Public Constructors
        public AzureRedisCacheManager(ContentCacheProviderRedisAzure contentCacheProvider)
        {
            _contentCacheProvider = contentCacheProvider;
        }

        #endregion

        #region Private Constructors

        private AzureRedisCacheManager()
        {

        }

        #endregion

        #region Public Methods



        /// <summary>
        /// Perform Azure Redis cache administration operation.
        /// </summary>
        public void PerformAzureRedisCacheAdminOperation(AzureRedisCacheAdminOperation azureRedisCacheAdminOption, CancellationToken cancellationToken)
        {
            IAzureRedisCacheAdminStatusResult azureRedisCacheAdminStatusResult = null;
            AzureRedisCacheAdminStatusEventArgs requestBuildEventArgs = null;

            try
            {
                //Check for cancellation.
                cancellationToken.ThrowIfCancellationRequested();

                requestBuildEventArgs = new AzureRedisCacheAdminStatusEventArgs();

                switch (azureRedisCacheAdminOption)
                {
                    case AzureRedisCacheAdminOperation.Unspecified:
                        break;

                    case AzureRedisCacheAdminOperation.ClearAzureRedisCache:

                        //Peform Azure Redis cache administration task.
                        azureRedisCacheAdminStatusResult = ClearCache();
                        break;

                    default:
                        break;
                }


                requestBuildEventArgs.AzureRedisCacheAdminStatusResult = azureRedisCacheAdminStatusResult;

                if (AzureRedisCacheAdminStatusUpdated != null)
                {
                    AzureRedisCacheAdminStatusUpdated(this, requestBuildEventArgs);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Clear Azure Redis cache. 
        /// </summary>
        /// <returns></returns>
        public IAzureRedisCacheAdminStatusResult ClearCache()
        {
            IAzureRedisCacheAdminStatusResult result = null;

            try
            {
                result = AzureRedisCacheAdminStatusResultFactory.CreateAzureRedisCacheAdminStatusResult();

                ContentCacheProvider.FlushAllDatabases();
                result.StatusMessage = string.Format("Azure Redis cache cleared. (Endpoint: {0})",
                                                     this.ContentCacheProvider.ConfigurationOptions.EndPoints.First());

            }
            catch (Exception ex)
            {
                Status status = null;

                result = AzureRedisCacheAdminStatusResultFactory.CreateAzureRedisCacheAdminStatusResult();
                result.StatusMessage = string.Format("Error occurred while clearing Azure Redis cache.");
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }


    /// <summary>
    /// Content cache provider - implemented via Redis Cache Azure.
    /// </summary>
    public class ContentCacheProviderRedisAzure
    {

        #region Private Constants

        private const string ConfigurationSectionAzureRedisCacheSection = "azureRedisCache";

        #endregion

        #region Private Data Members

        private AzureRedisCacheConfig _azureRedisCacheConfig;
        private ConnectionMultiplexer _connectionMultiplexer;
        private ConfigurationOptions _configurationOptions;

        #endregion

        #region Public Properties.

        /// <summary>
        /// Property: Redis cache connection.
        /// </summary>
        public ConnectionMultiplexer Connection
        {
            get { return _connectionMultiplexer; }
        }

        /// <summary>
        /// Property: Redis cache Configuration options.
        /// </summary>
        public ConfigurationOptions ConfigurationOptions
        {
            get { return _configurationOptions; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="azureRedisCacheConfig"></param>
        public ContentCacheProviderRedisAzure(AzureRedisCacheConfig azureRedisCacheConfig)
        {
            _azureRedisCacheConfig = azureRedisCacheConfig;

            _configurationOptions = ConvertAzureRedisCacheConfigToConfigurationOptions(azureRedisCacheConfig);
            _configurationOptions.AllowAdmin = true;

            _connectionMultiplexer = ConnectionMultiplexer.Connect(ConfigurationOptions);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Flush all databases.
        /// </summary>
        public void FlushAllDatabases()
        {
            EndPoint[] endPoints = Connection.GetEndPoints(true);

            IServer server = Connection.GetServer(endPoints.First());

            server.FlushAllDatabases(CommandFlags.None);
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        /// <summary>
        /// Convert Azure Redis cache configuration to connection multiplexer configuration options.
        /// </summary>
        /// <param name="azureRedisCacheConfig"></param>
        /// <returns></returns>
        private ConfigurationOptions ConvertAzureRedisCacheConfigToConfigurationOptions(AzureRedisCacheConfig azureRedisCacheConfig)
        {
            var configurationOptions = new ConfigurationOptions();

            //Required: values.

            configurationOptions.EndPoints.Add(azureRedisCacheConfig.Endpoint);

            configurationOptions.Password = azureRedisCacheConfig.Password;

            //Optional: values:

            configurationOptions.Ssl = azureRedisCacheConfig.Ssl;
            configurationOptions.ConnectRetry = azureRedisCacheConfig.ConnectRetry;
            configurationOptions.ConnectTimeout = azureRedisCacheConfig.ConnectTimeout;
            configurationOptions.SyncTimeout = azureRedisCacheConfig.SyncTimeout;

            //Silently retry in background rather than throw exception, if connection is unsuccessfull.
            configurationOptions.AbortOnConnectFail = false;

            return configurationOptions;

        }

        #endregion

    }

}
