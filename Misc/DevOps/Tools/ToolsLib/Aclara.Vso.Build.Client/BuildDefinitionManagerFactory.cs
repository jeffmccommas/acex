﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vso.Build.Client
{
    /// <summary>
    /// Build definition manager factory
    /// </summary>
    public static class BuildDefinitionManagerFactory
    {
        #region Private Constants

        #endregion

        #region Public Methods

        /// <summary>
        /// Create build definition manager.
        /// </summary>
        /// <param name="teamProjectCollectionUri"></param>
        /// <param name="teamProjectCollectionVsrmUri"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="basicAuthRestAPIUserProfileName"></param>
        /// <param name="basicAuthRestAPIPassword"></param>
        /// <returns></returns>
        static public BuildDefinitionManager CreateBuildDefinitionManager(Uri teamProjectCollectionUri,
                                                                          Uri teamProjectCollectionVsrmUri,
                                                                          string teamProjectName,
                                                                          string basicAuthRestAPIUserProfileName,
                                                                          string basicAuthRestAPIPassword)
        {
            BuildDefinitionManager result = null;
            BuildDefinitionManager buildDefinitionManager = null;

            try
            {

                buildDefinitionManager = new BuildDefinitionManager(teamProjectCollectionUri, 
                                                                    teamProjectCollectionVsrmUri,
                                                                    teamProjectName, 
                                                                    basicAuthRestAPIUserProfileName, 
                                                                    basicAuthRestAPIPassword);

                result = buildDefinitionManager;
                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion
    }
}
