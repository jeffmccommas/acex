﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.Vso.Build.Client.Models;
using Microsoft.TeamFoundation.Build.WebApi;

namespace Aclara.Vso.Build.Client.Events
{
    /// <summary>
    /// Get build list event arguments.
    /// </summary>
    public class GetBuildListEventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private BuildDefinition _buildDefinition;
        private bool _queuedBuildDefinitions;
        private BuildList _buildList;
        StatusList _statusList;
        private int _buildListTotalCount;
        private int _buildListRetrievedCount;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Build definition.
        /// </summary>
        public BuildDefinition BuildDefinition
        {
            get { return _buildDefinition; }
            set { _buildDefinition = value; }
        }

        /// <summary>
        /// Property: Queued build definitions.
        /// </summary>
        public bool QueuedBuildDefinitions
        {
            get { return _queuedBuildDefinitions; }
            set { _queuedBuildDefinitions = value; }
        }

        /// <summary>
        /// Property: Build list.
        /// </summary>
        public BuildList BuildList
        {
            get { return _buildList; }
            set { _buildList = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        /// <summary>
        /// Property: Build list total count.
        /// </summary>
        public int BuildListTotalCount
        {
            get { return _buildListTotalCount; }
            set { _buildListTotalCount = value; }
        }

        /// <summary>
        /// Property: Build list retrieved count.
        /// </summary>
        public int BuildListRetrievedCount
        {
            get { return _buildListRetrievedCount; }
            set { _buildListRetrievedCount = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GetBuildListEventArgs()
        {
            _statusList = new StatusList();
        }

        #endregion

    }
}
