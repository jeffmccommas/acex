﻿using Aclara.Tools.Common.StatusManagement;
using Microsoft.TeamFoundation.Build.WebApi;

namespace Aclara.Vso.Build.Client.Events
{
    /// <summary>
    /// Change build definition queue status event arguments.
    /// </summary>
    public class ChangeBuildDefinitionQueueStatusEventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private BuildDefinition _buildDefinition;
        private StatusList _statusList;
        private int _buildDefinitionTotalCount;
        private int _buildDefinitionChangedCount;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Build definition.
        /// </summary>
        public BuildDefinition BuildDefinition
        {
            get { return _buildDefinition; }
            set { _buildDefinition = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        /// <summary>
        /// Property: Build definition total count.
        /// </summary>
        public int BuildDefinitionTotalCount
        {
            get { return _buildDefinitionTotalCount; }
            set { _buildDefinitionTotalCount = value; }
        }

        /// <summary>
        /// Property: build definition changed count.
        /// </summary>
        public int BuildDefinitionChangedCount
        {
            get { return _buildDefinitionChangedCount; }
            set { _buildDefinitionChangedCount = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ChangeBuildDefinitionQueueStatusEventArgs()
        {
            _statusList = new StatusList();
        }

        #endregion

    }
}
