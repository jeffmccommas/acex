﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.Vso.Build.Client.Models;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.Vso.Build.Client.Events
{
    public class GetReleaseDefinitionListEventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data  Members

        private ReleaseDefinitionList _releaseDefinitionList;
        private ReleaseDefinitionRetrievalMode _releaseDefinitionRetrievalMode;
        StatusList _statusList;
        private int _releaseDefinitionTotalCount;
        private int _releaseDefinitionRetrievedCount;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Release definition list.
        /// </summary>
        public ReleaseDefinitionList ReleaseDefinitionList
        {
            get { return _releaseDefinitionList; }
            set { _releaseDefinitionList = value; }
        }

        /// <summary>
        /// Property: Release definition retrieval mode.
        /// </summary>
        public ReleaseDefinitionRetrievalMode ReleaseDefinitionRetrievalMode
        {
            get { return _releaseDefinitionRetrievalMode; }
            set { _releaseDefinitionRetrievalMode = value; }
        }

        /// <summary>
        /// Property : Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        /// <summary>
        /// Property: Release definition total count.
        /// </summary>
        public int ReleaseDefinitionTotalCount
        {
            get { return _releaseDefinitionTotalCount; }
            set { _releaseDefinitionTotalCount = value; }
        }

        /// <summary>
        /// Property: Release definition retrieved count.
        /// </summary>
        public int ReleaseDefinitionRetrievedCount
        {
            get { return _releaseDefinitionRetrievedCount; }
            set { _releaseDefinitionRetrievedCount = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GetReleaseDefinitionListEventArgs()
        {
            _statusList = new StatusList();
        }

        #endregion
    }
}
