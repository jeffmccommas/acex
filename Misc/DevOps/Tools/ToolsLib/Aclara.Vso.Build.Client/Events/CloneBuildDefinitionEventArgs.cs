﻿using Aclara.Tools.Common.StatusManagement;
using Microsoft.TeamFoundation.Build.WebApi;
using Microsoft.VisualStudio.Services.ReleaseManagement.WebApi;
using System;

namespace Aclara.Vso.Build.Client.Events
{
    /// <summary>
    /// Clone build definition event arguments.
    /// </summary>
    public class CloneBuildDefinitionEventArgs : EventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private BuildDefinition _sourceBuildDefinition;
        private BuildDefinition _targetBuildDefinition;
        private ReleaseDefinition _sourceReleaseDefinition;
        private ReleaseDefinition _targetReleaseDefinition;
        private int _buildDefinitionTotalCount;
        private int _buildDefinitionClonedCount;
        StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Source build definition.
        /// </summary>
        public BuildDefinition SourceBuildDefinition
        {
            get
            {
                return _sourceBuildDefinition;
            }
            set
            {
                _sourceBuildDefinition = value;
            }
        }

        /// <summary>
        /// Property: Target build definition.
        /// </summary>
        public BuildDefinition TargetBuildDefinition
        {
            get
            {
                return _targetBuildDefinition;
            }
            set
            {
                _targetBuildDefinition = value;
            }
        }

        /// <summary>
        /// Property: Source release definition.
        /// </summary>
        public ReleaseDefinition SourceReleaseDefinition
        {
            get
            {
                return _sourceReleaseDefinition;
            }
            set
            {
                _sourceReleaseDefinition = value;
            }
        }

        /// <summary>
        /// Property: Target release definition.
        /// </summary>
        public ReleaseDefinition TargetReleaseDefinition
        {
            get
            {
                return _targetReleaseDefinition;
            }
            set
            {
                _targetReleaseDefinition = value;
            }
        }
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }
 
        /// <summary>
        /// Property: Build definition total count.
        /// </summary>
        public int BuildDefinitionTotalCount
        {
            get { return _buildDefinitionTotalCount; }
            set { _buildDefinitionTotalCount = value; }
        }

        /// <summary>
        /// Property: Build definition cloned count.
        /// </summary>
        public int BuildDefinitionClonedCount
        {
            get { return _buildDefinitionClonedCount; }
            set { _buildDefinitionClonedCount = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CloneBuildDefinitionEventArgs()
        {
            _statusList = new StatusList();
        }

        #endregion

    }

}
