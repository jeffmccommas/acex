﻿using Aclara.Tools.Common.StatusManagement;
using Microsoft.TeamFoundation.Build.WebApi;
using System;
using System.IO;

namespace Aclara.Vso.Build.Client.Events
{
    /// <summary>
    /// Export build definition to zip archive.
    /// </summary>
    public class ExportBuildDefinitionEventArgs : EventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private BuildDefinition _buildDefinition;
        private StatusList _statusList;
        private int _buildDefinitionTotalCount;
        private int _buildDefinitionAddedToZipArchiveCount;
        private MemoryStream _zipArchiveMemoryStream;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Build definition.
        /// </summary>
        public BuildDefinition BuildDefinition
        {
            get { return _buildDefinition; }
            set { _buildDefinition = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        /// <summary>
        /// Property: Build definition total count.
        /// </summary>
        public int BuildDefinitionTotalCount
        {
            get { return _buildDefinitionTotalCount; }
            set { _buildDefinitionTotalCount = value; }
        }

        /// <summary>
        /// Property: Build definition added to zip archive count.
        /// </summary>
        public int BuildDefinitionAddedToZipArchiveCount
        {
            get { return _buildDefinitionAddedToZipArchiveCount; }
            set { _buildDefinitionAddedToZipArchiveCount = value; }
        }

        /// <summary>
        /// Property: Zip archive memory stream.
        /// </summary>
        public MemoryStream ZipArchiveMemoryStream
        {
            get { return _zipArchiveMemoryStream; }
            set { _zipArchiveMemoryStream = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ExportBuildDefinitionEventArgs()
        {
            _statusList = new StatusList();
        }

        #endregion

    }

}
