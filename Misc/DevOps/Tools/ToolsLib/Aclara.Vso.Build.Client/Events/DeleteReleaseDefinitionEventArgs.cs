﻿using Microsoft.VisualStudio.Services.ReleaseManagement.WebApi;
using Aclara.Tools.Common.StatusManagement;

namespace Aclara.Vso.Build.Client.Events
{
    /// <summary>
    /// Delete release definition event arguments.
    /// </summary>
    public class DeleteReleaseDefinitionEventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private ReleaseDefinition _releaseDefinition;
        private StatusList _statusList;
        private int _releaseDefinitionTotalCount;
        private int _releaseDefinitionDeletedCount;

        #endregion


        #region Public Properties

        /// <summary>
        /// Property: Release definition.
        /// </summary>
        public ReleaseDefinition ReleaseDefinition
        {
            get { return _releaseDefinition; }
            set { _releaseDefinition = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        /// <summary>
        /// Property: Release definition total count.
        /// </summary>
        public int ReleaseDefinitionTotalCount
        {
            get { return _releaseDefinitionTotalCount; }
            set { _releaseDefinitionTotalCount = value; }
        }

        /// <summary>
        /// Property: Release definition deleted count.
        /// </summary>
        public int ReleaseDefinitionDeletedCount
        {
            get { return _releaseDefinitionDeletedCount; }
            set { _releaseDefinitionDeletedCount = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DeleteReleaseDefinitionEventArgs()
        {
            _statusList = new StatusList();
        }

        #endregion
    }
}
