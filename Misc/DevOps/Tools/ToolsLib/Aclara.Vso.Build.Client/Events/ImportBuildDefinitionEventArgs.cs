﻿using Aclara.Tools.Common.StatusManagement;
using Microsoft.TeamFoundation.Build.WebApi;
using System;
using System.IO;

namespace Aclara.Vso.Build.Client.Events
{
    /// <summary>
    /// Export build definition to zip archive.
    /// </summary>
    public class ImportBuildDefinitionEventArgs : EventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private BuildDefinition _buildDefinition;
        private string _name;
        private string _originalName;
        private StatusList _statusList;
        private int _buildDefinitionTotalCount;
        private int _buildDefinitionImportedCount;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Build definition.
        /// </summary>
        public BuildDefinition BuildDefinition
        {
            get { return _buildDefinition; }
            set { _buildDefinition = value; }
        }

        /// <summary>
        /// Property: Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Property: Orignal name.
        /// </summary>
        public string OriginalName
        {
            get { return _originalName; }
            set { _originalName = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        /// <summary>
        /// Property: Build definition total count.
        /// </summary>
        public int BuildDefinitionTotalCount
        {
            get { return _buildDefinitionTotalCount; }
            set { _buildDefinitionTotalCount = value; }
        }

        /// <summary>
        /// Property: Build definition added to zip archive count.
        /// </summary>
        public int BuildDefinitionImportedCount
        {
            get { return _buildDefinitionImportedCount; }
            set { _buildDefinitionImportedCount = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ImportBuildDefinitionEventArgs()
        {
            _statusList = new StatusList();
        }

        #endregion

    }

}
