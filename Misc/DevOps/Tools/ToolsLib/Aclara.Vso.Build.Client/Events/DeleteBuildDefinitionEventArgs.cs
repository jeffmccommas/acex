﻿using Aclara.Tools.Common.StatusManagement;
using Microsoft.TeamFoundation.Build.WebApi;

namespace Aclara.Vso.Build.Client.Events
{
    /// <summary>
    /// Delete build definition event arguments.
    /// </summary>
    public class DeleteBuildDefinitionEventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private BuildDefinition _buildDefinition;
        private StatusList _statusList;
        private int _buildDefinitionTotalCount;
        private int _buildDefinitionDeletedCount;

        #endregion

        #region Public Properties


        /// <summary>
        /// Property: Build definition.
        /// </summary>
        public BuildDefinition BuildDefinition
        {
            get { return _buildDefinition; }
            set { _buildDefinition = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        /// <summary>
        /// Property: Build definition total count.
        /// </summary>
        public int BuildDefinitionTotalCount
        {
            get { return _buildDefinitionTotalCount; }
            set { _buildDefinitionTotalCount = value; }
        }

        /// <summary>
        /// Property: build definition deleted count.
        /// </summary>
        public int BuildDefinitionDeletedCount
        {
            get { return _buildDefinitionDeletedCount; }
            set { _buildDefinitionDeletedCount = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DeleteBuildDefinitionEventArgs()
        {
            _statusList = new StatusList();
        }



        #endregion
    }
}
