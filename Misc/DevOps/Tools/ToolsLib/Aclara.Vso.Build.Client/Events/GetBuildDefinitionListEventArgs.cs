﻿using Aclara.Tools.Common.StatusManagement;
using Aclara.Vso.Build.Client.Models;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.Vso.Build.Client.Events
{

    /// <summary>
    /// Get build definition list event arguments.
    /// </summary>
    public class GetBuildDefinitionListEventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private BuildDefinitionList _buildDefinitionList;
        private BuildDefinitionRetrievalMode _buildDefinitionRetrievalMode;
        StatusList _statusList;
        private int _buildDefinitionTotalCount;
        private int _buildDefinitionRetrievedCount;

       #endregion

        #region Public Properties

        /// <summary>
        /// Property: Build definition list.
        /// </summary>
        public BuildDefinitionList BuildDefinitionList
        {
            get { return _buildDefinitionList; }
            set { _buildDefinitionList = value; }
        }

        /// <summary>
        /// Property: Build definition retrieval mode.
        /// </summary>
        public BuildDefinitionRetrievalMode BuildDefinitionRetrievalMode
        {
            get { return _buildDefinitionRetrievalMode; }
            set { _buildDefinitionRetrievalMode = value; }
        }

        /// <summary>
        /// Property : Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        /// <summary>
        /// Property: Build definition total count.
        /// </summary>
        public int BuildDefinitionTotalCount
        {
            get { return _buildDefinitionTotalCount; }
            set { _buildDefinitionTotalCount = value; }
        }

        /// <summary>
        /// Property: Build definition retrieved count.
        /// </summary>
        public int BuildDefinitionRetrievedCount
        {
            get { return _buildDefinitionRetrievedCount; }
            set { _buildDefinitionRetrievedCount = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GetBuildDefinitionListEventArgs()
        {
            _statusList = new StatusList();
        }

        #endregion


    }
}
