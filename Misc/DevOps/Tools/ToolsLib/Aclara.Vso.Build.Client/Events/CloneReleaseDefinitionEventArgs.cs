﻿using Aclara.Tools.Common.StatusManagement;
using Microsoft.VisualStudio.Services.ReleaseManagement.WebApi;

namespace Aclara.Vso.Build.Client.Events
{
    public class CloneReleaseDefinitionEventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data  Members

        private ReleaseDefinition _sourceReleaseDefinition;
        private ReleaseDefinition _targetReleaseDefinition;
        StatusList _statusList;
        private int _releaseDefinitionTotalCount;
        private int _releaseDefinitionClonedCount;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Source release definition list.
        /// </summary>
        public ReleaseDefinition SourceReleaseDefinition
        {
            get { return _sourceReleaseDefinition; }
            set { _sourceReleaseDefinition = value; }
        }

        /// <summary>
        /// Property: Target release definition list.
        /// </summary>
        public ReleaseDefinition TargetReleaseDefinition
        {
            get { return _targetReleaseDefinition; }
            set { _targetReleaseDefinition = value; }
        }

        /// <summary>
        /// Property : Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        /// <summary>
        /// Property: Release definition total count.
        /// </summary>
        public int ReleaseDefinitionTotalCount
        {
            get { return _releaseDefinitionTotalCount; }
            set { _releaseDefinitionTotalCount = value; }
        }

        /// <summary>
        /// Property: Release definition cloned count.
        /// </summary>
        public int ReleaseDefinitionClonedCount
        {
            get { return _releaseDefinitionClonedCount; }
            set { _releaseDefinitionClonedCount = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CloneReleaseDefinitionEventArgs()
        {
            _statusList = new StatusList();
        }

        #endregion
    }
}
