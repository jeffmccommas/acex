﻿using Microsoft.TeamFoundation.Build.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vso.Build.Client
{
    /// <summary>
    /// Build lsit.
    /// </summary>
    public class AgentPoolQueueListContainer
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        #endregion

        #region Public Properties

        public List<Microsoft.TeamFoundation.Build.WebApi.AgentPoolQueue> Value;

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AgentPoolQueueListContainer()
        {
        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods
        #endregion

        #region private Methods
        #endregion
    }
}