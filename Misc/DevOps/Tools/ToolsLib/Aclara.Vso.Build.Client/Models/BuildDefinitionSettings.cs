﻿using Aclara.Vso.Build.Client.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vso.Build.Client.Models
{
    /// <summary>
    /// Build definition settings.
    /// </summary>
    public class BuildDefinitionSettings
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private Enumerations.BuildDefinitionSettingsGroup _buildDefinitionSettingsGroup;
        private DefaultAgentPoolQueueSettings _defaultAgentPoolQueue;
        private TriggerSettingsSchedule _triggerSettingsSchedule;
        private TriggerSettingsContinuousIntegration _triggerContinousIntegrationSettings;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Build definition settings group.
        /// </summary>
        public Enumerations.BuildDefinitionSettingsGroup BuildDefinitionSettingsGroup
        {
            get { return _buildDefinitionSettingsGroup; }
            set { _buildDefinitionSettingsGroup = value; }
        }

        /// <summary>
        /// Property: Default agent pool queue.
        /// </summary>
        public DefaultAgentPoolQueueSettings DefaultAgentPoolQueue
        {
            get { return _defaultAgentPoolQueue; }
            set { _defaultAgentPoolQueue = value; }
        }

        /// <summary>
        /// Property: Trigger settings - schedule.
        /// </summary>
        public TriggerSettingsSchedule TriggerSettingsSchedule
        {
            get { return _triggerSettingsSchedule; }
            set { _triggerSettingsSchedule = value; }
        }

        /// <summary>
        /// Property: Trigger settings - continuous integration.
        /// </summary>
        public TriggerSettingsSchedule ScheduleTriggerSettings
        {
            get { return _triggerSettingsSchedule; }
            set { _triggerSettingsSchedule = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="defaultAgentPoolQueue"></param>
        public BuildDefinitionSettings(DefaultAgentPoolQueueSettings defaultAgentPoolQueue)
        {
            this._defaultAgentPoolQueue = defaultAgentPoolQueue;
            this._triggerSettingsSchedule = new TriggerSettingsSchedule();
            this._buildDefinitionSettingsGroup = Enumerations.BuildDefinitionSettingsGroup.DefaultAgentPoolQueue;
        }

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="triggerSettingsSchedule"></param>
        public BuildDefinitionSettings(TriggerSettingsSchedule triggerSettingsSchedule)
        {
            this._defaultAgentPoolQueue = new DefaultAgentPoolQueueSettings();
            this._triggerSettingsSchedule = triggerSettingsSchedule;
            this._buildDefinitionSettingsGroup = Enumerations.BuildDefinitionSettingsGroup.TriggersSchedule;
        }

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="triggerSettingsContinuousIntegration"></param>
        public BuildDefinitionSettings(TriggerSettingsContinuousIntegration triggerSettingsContinuousIntegration)
        {
            this._triggerContinousIntegrationSettings = triggerSettingsContinuousIntegration;
            this._buildDefinitionSettingsGroup = Enumerations.BuildDefinitionSettingsGroup.TriggersContinousIntegration;

        }

        #endregion

        #region Protected Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <remarks>
        /// Hidden.
        /// </remarks>
        protected BuildDefinitionSettings()
        {
            this._defaultAgentPoolQueue = new DefaultAgentPoolQueueSettings();
            this._triggerSettingsSchedule = new TriggerSettingsSchedule();
            this._buildDefinitionSettingsGroup = Enumerations.BuildDefinitionSettingsGroup.Unspecified;
        }

        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }

    /// <summary>
    /// Default agent pool queue settings.
    /// </summary>
    public class DefaultAgentPoolQueueSettings
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private int _agentPoolQueueId;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Agent pool queue id.
        /// </summary>
        public int AgentPoolQueueId
        {
            get { return _agentPoolQueueId; }
            set { _agentPoolQueueId = value; }
        }

        #endregion

        #region Public Constructors
        #endregion

        #region Protected Constructors
        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }

    /// <summary>
    /// Trigger: Schedule settings.
    /// </summary>
    public class TriggerSettingsSchedule
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private int _hours;
        private int _minute;
        private string _timeZoneId;
        private DaysToBuildSchedule _daysToBuildSchedule;
        private bool _allowAddSchedule;
        private string _teamProjectName;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Hours.
        /// </summary>
        public int Hours
        {
            get { return _hours; }
            set { _hours = value; }
        }

        /// <summary>
        /// Property: Minutes.
        /// </summary>
        public int Minutes
        {
            get { return _minute; }
            set { _minute = value; }
        }

        /// <summary>
        /// Property: Time zone identifier.
        /// </summary>
        public string TimeZoneId
        {
            get { return _timeZoneId; }
            set { _timeZoneId = value; }
        }

        /// <summary>
        /// Property: Days to build schedule.
        /// </summary>
        public DaysToBuildSchedule DaystoBuildSchedule
        {
            get { return _daysToBuildSchedule; }
            set { _daysToBuildSchedule = value; }
        }

        /// <summary>
        /// Property: Team project name.
        /// </summary>
        public string TeamProjectName
        {
            get { return _teamProjectName; }
            set { _teamProjectName = value; }
        }

        /// <summary>
        /// Property: Branch filter.
        /// </summary>
        public string BranchFilter
        {
            get
            {
                return string.Format("+$/{0}", this.TeamProjectName);
            }
        }

        /// <summary>
        /// Property: Allow add schedule (to build definition that has none).
        /// </summary>
        public bool AllowAddSchedule
        {
            get { return _allowAddSchedule; }
            set { _allowAddSchedule = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public TriggerSettingsSchedule()
        {
            _daysToBuildSchedule = new DaysToBuildSchedule();
        }

        #endregion

        #region Protected Constructors
        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }

    /// <summary>
    /// Trigger: Continuous integration settings.
    /// </summary>
    public class TriggerSettingsContinuousIntegration
    {
    }

    /// <summary>
    /// Days to build schedule.
    /// </summary>
    public class DaysToBuildSchedule
    {
        #region Private Data Members

        private bool _monday;
        private bool _tuesday;
        private bool _wednesday;
        private bool _thursday;
        private bool _friday;
        private bool _saturday;
        private bool _sunday;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Monday.
        /// </summary>
        public bool Monday
        {
            get { return _monday; }
            set { _monday = value;}
        }

        /// <summary>
        /// Property: Tuesday.
        /// </summary>
        public bool Tuesday
        {
            get { return _tuesday; }
            set { _tuesday = value; }
        }

        /// <summary>
        /// Property: Wednesday.
        /// </summary>
        public bool Wednesday
        {
            get { return _wednesday; }
            set{ _wednesday = value; }
        }

        /// <summary>
        /// Property: Thursday.
        /// </summary>
        public bool Thursday
        {
            get { return _thursday; }
            set { _thursday = value; }
        }

        /// <summary>
        /// Property: Friday.
        /// </summary>
        public bool Friday
        {
            get { return _friday; }
            set {_friday = value; }
        }

        /// <summary>
        /// Property: Saturday.
        /// </summary>
        public bool Saturday
        {
            get { return _saturday; }
            set { _saturday = value; }
        }

        /// <summary>
        /// Property: Sunday.
        /// </summary>
        public bool Sunday
        {
            get { return _sunday; }
            set { _sunday = value; }
        }

        #endregion

        #region Protected Methods
        #endregion
    }
}
