﻿using Aclara.Vso.Build.Client.Models;
using System;

namespace Aclara.Vso.Build.Client.Models
{
    public static class BuildDefinitionSettingsFactory
    {
        /// <summary>
        /// Create build definition settings from default agent pool queue settings.
        /// </summary>
        /// <param name="defaultAgentPoolQueueSettings"></param>
        /// <returns></returns>
        public static BuildDefinitionSettings CreateBuildDefinitionSettings(DefaultAgentPoolQueueSettings defaultAgentPoolQueueSettings)
        {
            BuildDefinitionSettings result = null;

            try
            {

                result = new BuildDefinitionSettings(defaultAgentPoolQueueSettings);


            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        /// <summary>
        /// Create build definition settings from schedule trigger settings.
        /// </summary>
        /// <param name="scheduleTriggerSettings"></param>
        /// <returns></returns>
        public static BuildDefinitionSettings CreateBuildDefinitionSettings(TriggerSettingsSchedule scheduleTriggerSettings)
        {
            BuildDefinitionSettings result = null;

            try
            {
                result = new BuildDefinitionSettings(scheduleTriggerSettings);

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }
    }
}
