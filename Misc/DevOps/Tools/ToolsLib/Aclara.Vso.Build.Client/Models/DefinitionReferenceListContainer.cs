﻿using Microsoft.TeamFoundation.Build.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vso.Build.Client.Models
{
    public class DefinitionReferenceListContainer
    {
        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public  Data Members

        public List<DefinitionReference> value;

        #endregion

        #region Public Properties

        #endregion

        #region Public Constructors
        public DefinitionReferenceListContainer()
        {
        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion




    }
}
