﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vso.Build.Client.Models
{
    /// <summary>
    /// Build option list.
    /// </summary>
    public class BuildOptionList : List<Microsoft.TeamFoundation.Build.WebApi.BuildOptionDefinition>
    {

        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildOptionList()
        {

        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
