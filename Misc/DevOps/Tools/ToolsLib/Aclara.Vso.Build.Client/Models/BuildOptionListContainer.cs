﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vso.Build.Client.Models
{
    /// <summary>
    /// Build option list container.
    /// </summary>
    public class BuildOptionListContainer
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        #endregion

        #region Public Properties

        public List<Microsoft.TeamFoundation.Build.WebApi.BuildOptionDefinition> Value;

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildOptionListContainer()
        {
        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods
        #endregion

        #region private Methods
        #endregion
    }
}
