﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vso.Build.Client.Models
{
    /// <summary>
    /// Git repository container.
    /// </summary>
    public class GitRepositoryContainer
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        #endregion

        #region Public Properties

        public List<Microsoft.TeamFoundation.SourceControl.WebApi.GitRepository> Value;

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GitRepositoryContainer()
        {
        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods
        #endregion

        #region private Methods
        #endregion

    }
}
