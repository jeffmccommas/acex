﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vso.Build.Client.Models
{
    /// <summary>
    /// Definition (Build/Release) import.
    /// </summary>
    public class DefinitionImport
    {

        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        private string _name;
        private string _originalName;
        private string _definitionAsJson;

        #region Public Properties

        /// <summary>
        /// Property: Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Property: Original name.
        /// </summary>
        public string OriginalName
        {
            get { return _originalName; }
            set { _originalName = value; }
        }

        /// <summary>
        /// Property: Definition as json.
        /// </summary>
        public string DefinitionAsJson
        {
            get { return _definitionAsJson; }
            set { _definitionAsJson = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        public DefinitionImport()
        {

        }   

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }
}
