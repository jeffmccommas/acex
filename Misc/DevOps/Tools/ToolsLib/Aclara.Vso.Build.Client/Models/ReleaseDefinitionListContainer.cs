﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.Services.ReleaseManagement.WebApi;

namespace Aclara.Vso.Build.Client.Models
{
    public class ReleaseDefinitionListContainer
    {
        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public  Data Members

        public List<ReleaseDefinition> value;

        #endregion

        #region Public Properties

        #endregion

        #region Public Constructors
        public ReleaseDefinitionListContainer()
        {
        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion
    }
}
