﻿using Microsoft.TeamFoundation.Build.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vso.Build.Client.Models
{
    /// <summary>
    /// Build list container.
    /// </summary>
    public class BuildListContainer
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        #endregion

        #region Public Properties

        public List<Microsoft.TeamFoundation.Build.WebApi.Build> Value;

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildListContainer()
        {
        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods
        #endregion

        #region private Methods
        #endregion
    }
}
