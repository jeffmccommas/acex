﻿using System.Web;
using Aclara.Tools.Common.StatusManagement;
using Aclara.Vso.Build.Client.Events;
using Aclara.Vso.Build.Client.Exceptions;
using Aclara.Vso.Build.Client.Models;
using Aclara.Vso.Build.Client.Types;
using Aclara.Vso.Build.Client.Utilities;
using Microsoft.TeamFoundation.Build.WebApi;
using Microsoft.TeamFoundation.Core.WebApi;
using Microsoft.TeamFoundation.SourceControl.WebApi;
using Microsoft.VisualStudio.Services.Common;
using Microsoft.VisualStudio.Services.ReleaseManagement.WebApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using static Aclara.Vso.Build.Client.Types.Enumerations;
using BWRetentionPolicy = Microsoft.TeamFoundation.Build.WebApi.RetentionPolicy;
using BWScheduleDays = Microsoft.TeamFoundation.Build.WebApi.ScheduleDays;

namespace Aclara.Vso.Build.Client
{
    /// <summary>
    /// Build definition manager.
    /// </summary>
    public class BuildDefinitionManager
    {

        #region Private Constants

        private const string PropertyKey_TFVCMapping = "tfvcMapping";
        private const string RepositoryTypeName_TFSGit = "TfsGit";
        private const string RepositoryTypeName_TFSVersionControl = "TfsVersionControl";

        private const string BuildDefinition_PathPrefex = @"\\";

        private const string BuildRepositoryProperty_DataMemberName_CleanOptions = "cleanOptions";
        private const string BuildRepositoryProperty_DataMemberName_LabelSources = "labelSources";
        private const string BuildRepositoryProperty_DataMemberName_LabelSourcesFormat = "labelSourcesFormat";
        private const string BuildRepositoryProperty_DataMemberName_ReportBuildStatus = "reportBuildStatus";
        private const string BuildRepositoryProperty_DataMemberName_GitLfsSupport = "gitLfsSupport";
        private const string BuildRepositoryProperty_DataMemberName_SkipSyncSource = "skipSyncSource";
        private const string BuildRepositoryProperty_DataMemberName_CheckoutNestedSubmodules = "checkoutNestedSubmodules";
        private const string BuildRepositoryProperty_DataMemberName_FetchDepth = "fetchDepth";

        #endregion

        #region Private Data Members

        private Uri _teamProjectCollectionUri;
        private Uri _teamProjectCollectionVsrmUri;
        private string _teamProjectName;
        private string _basicAuthRestApiUserProfileName;
        private string _basicAuthRestApiPassword;

        #endregion

        #region Public Delegates

        public event EventHandler<ChangeBuildDefinitionQueueStatusEventArgs> BuildDefinitionQueueStatusChanged;
        public event EventHandler<ChangeBuildDefinitionSettingsEventArgs> BuildDefinitionSettingsChanged;
        public event EventHandler<CloneBuildDefinitionEventArgs> BuildDefinitionCloned;
        public event EventHandler<DeleteBuildDefinitionEventArgs> BuildDefinitionDeleted;
        public event EventHandler<RequestBuildEventArgs> BuildRequested;
        public event EventHandler<GetBuildDefinitionListEventArgs> BuildDefinintionListRetrieved;
        public event EventHandler<GetBuildListEventArgs> BuildListRetrieved;
        public event EventHandler<ExportBuildDefinitionEventArgs> BuildDefinitionExported;
        public event EventHandler<ExportBuildDefinitionEventArgs> BuildDefinitionExportCompleted;
        public event EventHandler<ImportBuildDefinitionEventArgs> BuildDefinitionImported;
        public event EventHandler<ImportBuildDefinitionEventArgs> BuildDefinitionImportCompleted;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Team project collection uri.
        /// </summary>
        public Uri TeamProjectCollectionUri
        {
            get { return _teamProjectCollectionUri; }
            set
            {
                string teamProjectCollectionUriAsText = string.Empty;

                _teamProjectCollectionUri = value;

                teamProjectCollectionUriAsText = value.ToString();
                if (teamProjectCollectionUriAsText.EndsWith("/") == false)
                {
                    teamProjectCollectionUriAsText = value + @"/";
                    _teamProjectCollectionUri = new Uri(teamProjectCollectionUriAsText);
                }
            }
        }

        /// <summary>
        /// Property: Team project collection vsrm uri.
        /// </summary>
        public Uri TeamProjectCollectionVsrmUri
        {
            get { return _teamProjectCollectionVsrmUri; }
            set
            {
                string teamProjectCollectionVsrmUriAsText = string.Empty;

                _teamProjectCollectionVsrmUri = value;

                teamProjectCollectionVsrmUriAsText = value.ToString();
                if (teamProjectCollectionVsrmUriAsText.EndsWith("/") == false)
                {
                    teamProjectCollectionVsrmUriAsText = value + @"/";
                    _teamProjectCollectionVsrmUri = new Uri(teamProjectCollectionVsrmUriAsText);
                }
            }
        }

        /// <summary>
        /// Property: Team project name.
        /// </summary>
        public string TeamProjectName
        {
            get { return _teamProjectName; }
            set { _teamProjectName = value; }
        }

        /// <summary>
        /// Property: Basic authorization REST API user profile name.
        /// </summary>
        public string BasicAuthRestApiUserProfileName
        {
            get { return _basicAuthRestApiUserProfileName; }
            set { _basicAuthRestApiUserProfileName = value; }
        }

        /// <summary>
        /// Property: Basic authorization REST API password.
        /// </summary>
        public string BasicAuthRestApiPassword
        {
            get { return _basicAuthRestApiPassword; }
            set { _basicAuthRestApiPassword = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamProjectCollectionUri"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="basicAuthRestAPIUserProfileName"></param>
        /// <param name="basicAuthRestAPIPassword"></param>
        public BuildDefinitionManager(Uri teamProjectCollectionUri,
                                      Uri teamProjectCollectionVsrmUri,
                                      string teamProjectName,
                                      string basicAuthRestAPIUserProfileName,
                                      string basicAuthRestAPIPassword)
        {
            this.TeamProjectCollectionUri = teamProjectCollectionUri;
            this.TeamProjectCollectionVsrmUri = teamProjectCollectionVsrmUri;
            this.TeamProjectName = teamProjectName;
            this.BasicAuthRestApiUserProfileName = basicAuthRestAPIUserProfileName;
            this.BasicAuthRestApiPassword = basicAuthRestAPIPassword;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor is disabled.
        /// </summary>
        private BuildDefinitionManager()
        {

        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve build definitions asynchronously via web api.
        /// </summary>
        public BuildDefinitionList GetDefinitionsRestApi(List<int> buildDefinitionIDList)
        {

            BuildDefinitionList result = null;
            BuildDefinitionList buildDefinitionList = null;

            BuildDefinition buildDefinition = null;
            List<DefinitionReference> definitionReferences = null;
            string requestUrl = String.Empty;
            string parameter = string.Empty;

            try
            {

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUrl = string.Format("{0}DefaultCollection/{1}/_apis/build/definitions?api-version=2.0",
                                               this.TeamProjectCollectionUri,
                                               this.TeamProjectName);
                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUrl).Result;

                    buildDefinitionList = new BuildDefinitionList();


                    if (httpResponseMessage.IsSuccessStatusCode)
                    {

                        definitionReferences = httpResponseMessage.Content.ReadAsAsync<DefinitionReferenceListContainer>().Result.value;

                        var filteredDefinitionList = from definitionReference in definitionReferences
                                                     where buildDefinitionIDList.Any(s => s == definitionReference.Id)
                                                     select definitionReference;

                        //Retrieve deep definition references using shallow filtered definition references.
                        foreach (DefinitionReference definitionReference in filteredDefinitionList)
                        {
                            if (definitionReference.Type == DefinitionType.Build)
                            {
                                buildDefinition = GetDefinitionRestApi(definitionReference.Id);
                                buildDefinitionList.Add(buildDefinition);
                            }
                        }
                    }
                }

                result = buildDefinitionList;
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        /// <summary>
        /// Get build definition list.
        /// </summary>
        /// <param name="buildDefinitionNamePattern"></param>
        /// <param name="alreadyRetrievedBuildDefinitionList"></param>
        /// <param name="buildDefinitionRetrievalMode"></param>
        /// <param name="cancellationToken"></param>
        public void GetBuildDefinitionList(string buildDefinitionNamePattern,
                                           BuildDefinitionList alreadyRetrievedBuildDefinitionList,
                                           BuildDefinitionRetrievalMode buildDefinitionRetrievalMode,
                                           CancellationToken cancellationToken)
        {
            GetBuildDefinitionListEventArgs getBuildDefinitionListEventArgs = null;
            BuildDefinitionList buildDefinitionList = null;
            IEnumerable<DefinitionReference> filteredDefinitionReferences = null;
            BuildDefinition buildDefinition = null;
            List<DefinitionReference> definitionReferences = null;
            int buildDefinitionListTotalCount = 0;
            int buildDefinitionListRetrievedCount = 0;
            string requestUrl = String.Empty;
            string parameter = string.Empty;

            try
            {

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUrl = string.Format("{0}DefaultCollection/{1}/_apis/build/definitions?api-version=2.0",
                                               this.TeamProjectCollectionUri,
                                               this.TeamProjectName);
                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUrl).Result;

                    buildDefinitionList = new BuildDefinitionList();

                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {

                        definitionReferences = httpResponseMessage.Content.ReadAsAsync<DefinitionReferenceListContainer>().Result.value;

                        if (buildDefinitionRetrievalMode == BuildDefinitionRetrievalMode.Reset)
                        {
                            filteredDefinitionReferences = Wildcard.FilterListByNamePropertyAndPattern(definitionReferences, buildDefinitionNamePattern, false);
                        }
                        else if (buildDefinitionRetrievalMode == BuildDefinitionRetrievalMode.RetrieveRemaining)
                        {
                            filteredDefinitionReferences = GetDefinitionReferencesNotAlreadyRetrieved(definitionReferences,
                                                                                                      alreadyRetrievedBuildDefinitionList);
                        }

                        buildDefinitionListTotalCount = filteredDefinitionReferences.Where(dr => dr.Type == DefinitionType.Build).Count();

                        //Retrieve deep definition references using shallow definition references.
                        foreach (DefinitionReference definitionReference in filteredDefinitionReferences.Where(dr => dr.Type == DefinitionType.Build))
                        {
                            //Check for cancellation.
                            cancellationToken.ThrowIfCancellationRequested();

                            //buildDefinition = GetDefinitionRestApi(definitionReference.Id);
                            buildDefinition = (BuildDefinition)definitionReference;
                            if (buildDefinition == null)
                            {
                                continue;
                            }

                            buildDefinitionList.Add(buildDefinition);
                            buildDefinitionListRetrievedCount++;

                            getBuildDefinitionListEventArgs = new GetBuildDefinitionListEventArgs();
                            getBuildDefinitionListEventArgs.BuildDefinitionTotalCount = buildDefinitionListTotalCount;
                            getBuildDefinitionListEventArgs.BuildDefinitionRetrievedCount = buildDefinitionListRetrievedCount;
                            getBuildDefinitionListEventArgs.BuildDefinitionList = buildDefinitionList;
                            getBuildDefinitionListEventArgs.BuildDefinitionRetrievalMode = buildDefinitionRetrievalMode;

                            if (BuildDefinintionListRetrieved != null)
                            {
                                BuildDefinintionListRetrieved(this, getBuildDefinitionListEventArgs);
                            }

                        }
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Get build definition list.
        /// </summary>
        /// <param name="buildDefinitionNamePattern"></param>
        public BuildDefinitionList GetBuildDefinitionList(string buildDefinitionNamePattern)
        {
            BuildDefinitionList result = null;
            IEnumerable<DefinitionReference> filteredDefinitionReferences = null;
            BuildDefinition buildDefinition = null;
            List<DefinitionReference> definitionReferences = null;
            string requestUrl = String.Empty;
            string parameter = string.Empty;

            try
            {

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUrl = string.Format("{0}DefaultCollection/{1}/_apis/build/definitions?api-version=2.0",
                                               this.TeamProjectCollectionUri,
                                               this.TeamProjectName);
                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUrl).Result;

                    result = new BuildDefinitionList();


                    if (httpResponseMessage.IsSuccessStatusCode)
                    {

                        definitionReferences = httpResponseMessage.Content.ReadAsAsync<DefinitionReferenceListContainer>().Result.value;

                        filteredDefinitionReferences = Wildcard.FilterListByNamePropertyAndPattern(definitionReferences, buildDefinitionNamePattern, false);

                        //Retrieve deep definition references using shallow definition references.
                        foreach (DefinitionReference definitionReference in filteredDefinitionReferences.Where(dr => dr.Type == DefinitionType.Build))
                        {

                            buildDefinition = GetDefinitionRestApi(definitionReference.Id);
                            if (buildDefinition == null)
                            {
                                continue;
                            }
                            result.Add(buildDefinition);

                        }
                    }
                }

                return result;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Retrieve build definitions asynchronously via web api.
        /// </summary>
        /// <param name="definitionId"></param>
        public BuildDefinition GetDefinitionRestApi(int definitionId)
        {

            BuildDefinition result = null;

            BuildDefinition buildDefinition = null;
            string requestUrl = String.Empty;
            string parameter = string.Empty;

            BuildHttpClient buildHttpClient = null;
            VssCredentials vssCredentials = null;
            string userState = string.Empty;
            Task<BuildDefinition> getDefinitionTask = null;

            try
            {

                vssCredentials = new VssCredentials();
                vssCredentials = new VssCredentials(new WindowsCredential(new NetworkCredential(this.BasicAuthRestApiUserProfileName, this.BasicAuthRestApiPassword)));

                buildHttpClient = new BuildHttpClient(this.TeamProjectCollectionUri, vssCredentials);

                getDefinitionTask = buildHttpClient.GetDefinitionAsync(this.TeamProjectName, definitionId, null, null, userState, default(CancellationToken));
                getDefinitionTask.Wait();
                buildDefinition = getDefinitionTask.Result;

                result = buildDefinition;

            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        /// <summary>
        /// Retrieve build definitions asynchronously via web api.
        /// </summary>
        /// <param name="definitionId"></param>
        public async Task<BuildDefinition> GetDefinitionRestApiAsync(int definitionId)
        {

            BuildDefinition result = null;

            DefinitionReference definitionReference = null;
            BuildDefinition buildDefinition = null;
            string requestUrl = String.Empty;
            string parameter = string.Empty;

            try
            {

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUrl = string.Format("{0}DefaultCollection/{1}/_apis/build/definitions/{2}?api-version=2.0",
                                               this.TeamProjectCollectionUri,
                                               this.TeamProjectName,
                                               definitionId);
                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUrl).Result;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        definitionReference = await httpResponseMessage.Content.ReadAsAsync<DefinitionReference>();
                        buildDefinition = (BuildDefinition)definitionReference;
                    }

                }
                result = buildDefinition;
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve list of all Git repositories.
        /// </summary>
        /// <returns></returns>
        public List<GitRepository> GetGitRepositoriesRestApi()
        {
            List<GitRepository> result = null;
            string requestUrl = String.Empty;
            string parameter = string.Empty;

            try
            {

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUrl = string.Format("{0}DefaultCollection/{1}/_apis/git/repositories?api-version=1.0",
                                               this.TeamProjectCollectionUri,
                                               this.TeamProjectName);


                    //{0}DefaultCollection/{1}
                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUrl).Result;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {

                        result = httpResponseMessage.Content.ReadAsAsync<GitRepositoryContainer>().Result.Value;

                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Retrieve agent pool queues via web api.
        /// </summary>
        public List<AgentPoolQueue> GetAgentPoolQueuesRestApi()
        {

            List<AgentPoolQueue> result = null;
            string requestUrl = String.Empty;
            string parameter = string.Empty;

            try
            {

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUrl = string.Format("{0}DefaultCollection/{1}/_apis/distributedtask/queues",
                                               this.TeamProjectCollectionUri,
                                               this.TeamProjectName);

                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUrl).Result;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {

                        result = httpResponseMessage.Content.ReadAsAsync<AgentPoolQueueListContainer>().Result.Value;

                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build definition options asynchronously via web api.
        /// </summary>
        /// <param name="buildId"></param>
        public List<Microsoft.TeamFoundation.Build.WebApi.BuildOptionDefinition> GetBuildOptionDefinition()
        {
            List<Microsoft.TeamFoundation.Build.WebApi.BuildOptionDefinition> result = null;
            List<Microsoft.TeamFoundation.Build.WebApi.BuildOptionDefinition> options = null;
            string requestUri = String.Empty;
            string parameter = string.Empty;

            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUri = string.Format("{0}/_apis/build/options?api-version=2.0",
                                               this.TeamProjectCollectionUri);
                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUri).Result;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {

                        options = httpResponseMessage.Content.ReadAsAsync<BuildOptionListContainer>().Result.Value;

                    }
                }
                result = options;
            }
            catch (Exception)
            {

                result = null;
            }
            return result;
        }

        /// <summary>
        /// Get build list.
        /// </summary>
        /// <param name="buildDefinitionList"></param>
        /// <param name="dateFilter"></param>
        /// <param name="top"></param>
        /// <param name="recentlyCompletedInHours"
        /// <param name="queuedBuildDefinitions"></param>
        /// <param name="cancellationToken"></param>
        public void GetBuildList(List<int> buildDefinitionIdList,
                                 DateFilter dateFilter,
                                 int top,
                                 int recentlyCompletedInHours,
                                 bool queuedBuildDefinitions,
                                 CancellationToken cancellationToken)
        {
            BuildDefinitionList buildDefinitionList = null;
            GetBuildListEventArgs getBuildListEventArgs = null;
            BuildList buildList = null;
            int buildDefinitionCount = 0;

            try
            {

                if (queuedBuildDefinitions == true)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    buildList = GetBuildsRestApi(dateFilter, recentlyCompletedInHours);

                    buildDefinitionCount++;

                    getBuildListEventArgs = new GetBuildListEventArgs();
                    getBuildListEventArgs.BuildListTotalCount = buildDefinitionCount;
                    getBuildListEventArgs.BuildListRetrievedCount = buildDefinitionCount;
                    getBuildListEventArgs.BuildDefinition = null;
                    getBuildListEventArgs.QueuedBuildDefinitions = true;
                    getBuildListEventArgs.BuildList = buildList;

                    if (BuildListRetrieved != null)
                    {
                        BuildListRetrieved(this, getBuildListEventArgs);
                    }
                }
                else
                {
                    buildDefinitionList = this.GetDefinitionsRestApi(buildDefinitionIdList);

                    foreach (BuildDefinition buildDefinition in buildDefinitionList)
                    {
                        //Check for cancellation.
                        cancellationToken.ThrowIfCancellationRequested();

                        buildList = GetBuildsRestApi(buildDefinition.Id, dateFilter, top, recentlyCompletedInHours);

                        buildDefinitionCount++;

                        getBuildListEventArgs = new GetBuildListEventArgs();
                        getBuildListEventArgs.BuildListTotalCount = buildDefinitionList.Count();
                        getBuildListEventArgs.BuildListRetrievedCount = buildDefinitionCount;
                        getBuildListEventArgs.BuildDefinition = buildDefinition;
                        getBuildListEventArgs.QueuedBuildDefinitions = false;
                        getBuildListEventArgs.BuildList = buildList;

                        if (BuildListRetrieved != null)
                        {
                            BuildListRetrieved(this, getBuildListEventArgs);
                        }

                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Retrieve builds asynchronously via web api.
        /// </summary>
        /// <param name="definitionId"></param>
        /// <param name="dateFilter"></param>
        /// <param name="top"></param>
        /// <param name="recentlyCompletedInHours"
        /// <returns></returns>
        public BuildList GetBuildsRestApi(int definitionId,
                                          DateFilter dateFilter,
                                          int top,
                                          int recentlyCompletedInHours)
        {

            BuildList result = null;
            List<Microsoft.TeamFoundation.Build.WebApi.Build> builds = null;
            string parameter = string.Empty;
            string requestUri = string.Empty;
            DateTime minFinishTime;

            try
            {
                result = new BuildList();

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    if (dateFilter == DateFilter.No_Filter ||
                        dateFilter == DateFilter.Recent)
                    {
                        requestUri = string.Format("{0}DefaultCollection/{1}/_apis/build/builds?api-version=2.0&definitions={2}&$top={3}",
                                                   this.TeamProjectCollectionUri,
                                                   this.TeamProjectName,
                                                   definitionId,
                                                   top);
                    }
                    else
                    {
                        switch (dateFilter)
                        {
                            case DateFilter.Last_6_hours:
                                minFinishTime = DateTime.Now.AddHours(-6).ToUniversalTime();
                                break;
                            case DateFilter.Last_12_hours:
                                minFinishTime = DateTime.Now.AddHours(-12).ToUniversalTime();
                                break;
                            case DateFilter.Last_24_hours:
                                minFinishTime = DateTime.Now.AddHours(-24).ToUniversalTime();
                                break;
                            case DateFilter.Last_3_days:
                                minFinishTime = DateTime.Now.AddDays(-3).ToUniversalTime();
                                break;
                            case DateFilter.Last_7_days:
                                minFinishTime = DateTime.Now.AddDays(-7).ToUniversalTime();
                                break;
                            default:
                                minFinishTime = DateTime.Now.AddHours(-24).ToUniversalTime();
                                break;
                        }

                        requestUri = string.Format("{0}DefaultCollection/{1}/_apis/build/builds?api-version=2.0&definitions={2}&$top={3}&minFinishTime={4}",
                                                   this.TeamProjectCollectionUri,
                                                   this.TeamProjectName,
                                                   definitionId,
                                                   top,
                                                   minFinishTime.ToString("s") + "Z");

                    }

                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUri).Result;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {

                        builds = httpResponseMessage.Content.ReadAsAsync<BuildListContainer>().Result.Value;

                        foreach (Microsoft.TeamFoundation.Build.WebApi.Build build in builds)
                        {
                            if (dateFilter == DateFilter.Recent)
                            {
                                if (build.Status == BuildStatus.Completed &&
                                    build.FinishTime <= DateTime.Now.ToUniversalTime() &&
                                    build.FinishTime >= DateTime.Now.AddHours(recentlyCompletedInHours * -1).ToUniversalTime())
                                {
                                    result.Add(build);
                                }
                                else if (build.Status != BuildStatus.Completed)
                                {
                                    result.Add(build);
                                }
                            }
                            else
                            {
                                result.Add(build);
                            }

                        }

                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        /// <summary>
        /// Retrieve builds asynchronously via web api.
        /// </summary>
        /// <param name="dateFilter"></param>
        /// <param name="top"></param>
        /// <param name="recentlyCompletedInHours"
        /// <returns></returns>
        public BuildList GetBuildsRestApi(DateFilter dateFilter,
                                          int recentlyCompletedInHours)
        {
            const string StatusFilter = "all";

            BuildList result = null;
            List<Microsoft.TeamFoundation.Build.WebApi.Build> builds = null;
            string parameter = string.Empty;
            string requestUri = string.Empty;

            try
            {
                result = new BuildList();

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUri = string.Format("{0}DefaultCollection/{1}/_apis/build/builds?api-version=2.0&statusFilter={2}",
                                                this.TeamProjectCollectionUri,
                                                this.TeamProjectName,
                                                StatusFilter);

                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUri).Result;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {

                        builds = httpResponseMessage.Content.ReadAsAsync<BuildListContainer>().Result.Value;

                        foreach (Microsoft.TeamFoundation.Build.WebApi.Build build in builds)
                        {
                            if (dateFilter == DateFilter.Recent)
                            {
                                if (build.Status == BuildStatus.Completed &&
                                    build.FinishTime <= DateTime.Now.ToUniversalTime() &&
                                    build.FinishTime >= DateTime.Now.AddHours(recentlyCompletedInHours * -1).ToUniversalTime())
                                {
                                    result.Add(build);
                                }
                                else if (build.Status != BuildStatus.Completed)
                                {
                                    result.Add(build);
                                }
                            }
                            else
                            {
                                result.Add(build);
                            }
                        }

                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        /// <summary>
        /// Delete build definition asynchronously via web api.
        /// </summary>
        /// <param name="definitionId"></param>
        public void DeleteDefinitionRestApi(int definitionId)
        {

            string parameter = string.Empty;

            try
            {

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    HttpResponseMessage httpResponseMessage = httpClient.DeleteAsync(string.Format("{0}DefaultCollection/{1}/_apis/build/definitions/{2}?api-version=2.0",
                                                                                                   this.TeamProjectCollectionUri,
                                                                                                   this.TeamProjectName,
                                                                                                   definitionId)).Result;

                    //Throw on error.
                    httpResponseMessage.EnsureSuccessStatusCode();

                }

            }
            catch (Exception)
            {

                throw;
            }
            return;
        }

        /// <summary>
        /// Clone build definitions.
        /// </summary>
        /// <param name="buildDefinitionIdList"></param>
        /// <param name="sourceBuildDefinitionNamePrefix"></param>
        /// <param name="targetBuildDefinitionNamePrefix"></param>
        /// <param name="sourceBranchName"></param>
        /// <param name="targetBranchName"></param>
        /// <param name="buildDefinitionCloneProperties"></param>
        /// <param name="cancellationToken"></param>
        public void CloneBuildDefinitions(List<int> buildDefinitionIdList,
                                          string sourceBuildDefinitionNamePrefix,
                                          string targetBuildDefinitionNamePrefix,
                                          string sourceBranchName,
                                          string targetBranchName,
                                          BuildDefinitionCloneProperties buildDefinitionCloneProperties,
                                          CancellationToken cancellationToken)
        {
            BuildDefinitionList buildDefinitionList = null;
            CloneBuildDefinitionEventArgs cloneBuildDefinitionEventArgs = null;
            BuildDefinition sourceBuildDefinition = null;
            int buildDefinitionClonedCount = 0;

            try
            {
                buildDefinitionList = this.GetDefinitionsRestApi(buildDefinitionIdList);

                foreach (BuildDefinition buildDefinition in buildDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    cloneBuildDefinitionEventArgs = new CloneBuildDefinitionEventArgs();

                    //Retieve "deep" build definition.
                    sourceBuildDefinition = this.GetDefinitionRestApi(buildDefinition.Id);

                    //Clone build definition.
                    cloneBuildDefinitionEventArgs = CloneBuildDefinition(buildDefinition,
                                                                         sourceBuildDefinitionNamePrefix,
                                                                         targetBuildDefinitionNamePrefix,
                                                                         sourceBranchName,
                                                                         targetBranchName,
                                                                         buildDefinitionCloneProperties);

                    cloneBuildDefinitionEventArgs.BuildDefinitionTotalCount = buildDefinitionList.Count;
                    cloneBuildDefinitionEventArgs.BuildDefinitionClonedCount = ++buildDefinitionClonedCount;

                    if (BuildDefinitionCloned != null)
                    {
                        BuildDefinitionCloned(this, cloneBuildDefinitionEventArgs);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Clone build definition.
        /// </summary>
        /// <param name="sourceBuildDefinition"></param>
        /// <param name="sourceBuildDefinitionNamePrefix"></param>
        /// <param name="targetBuildDefinitionNamePrefix"></param>
        /// <param name="sourceBranchName"></param>
        /// <param name="targetBranchName"></param>
        /// <param name="buildDefinitionCloneProperties"></param>
        /// <returns></returns>
        public CloneBuildDefinitionEventArgs CloneBuildDefinition(BuildDefinition sourceBuildDefinition,
                                                                  string sourceBuildDefinitionNamePrefix,
                                                                  string targetBuildDefinitionNamePrefix,
                                                                  string sourceBranchName,
                                                                  string targetBranchName,
                                                                  BuildDefinitionCloneProperties buildDefinitionCloneProperties)
        {
            CloneBuildDefinitionEventArgs result = null;

            BuildDefinition targetBuildDefinition = null;

            try
            {
                result = new CloneBuildDefinitionEventArgs();
                result.SourceBuildDefinition = sourceBuildDefinition;

                if (buildDefinitionCloneProperties.DoNotCloneBuildDefinitions == false)
                {
                    // Create new build definition from another (returns new build definition).
                    targetBuildDefinition = CreateBuildDefinitionFromAnother(sourceBuildDefinition,
                                                                             sourceBuildDefinitionNamePrefix,
                                                                             targetBuildDefinitionNamePrefix,
                                                                             sourceBranchName,
                                                                             targetBranchName,
                                                                             buildDefinitionCloneProperties);
                    //Save target build definition.
                    UpdateDefinition(targetBuildDefinition);

                    result.TargetBuildDefinition = targetBuildDefinition;
                }

                if (buildDefinitionCloneProperties.CloneReleaseDefinitions == true)
                {

                    this.CloneReleaseDefinition(sourceBuildDefinition,
                                                sourceBuildDefinitionNamePrefix,
                                                targetBuildDefinitionNamePrefix,
                                                sourceBranchName,
                                                targetBranchName,
                                                buildDefinitionCloneProperties,
                                                result);
                }

            }
            catch (Exception ex)
            {
                Status status = null;

                result = new CloneBuildDefinitionEventArgs();
                result.SourceBuildDefinition = sourceBuildDefinition;
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        /// <summary>
        /// Clone release definition.
        /// </summary>
        /// <param name="targetBuildDefinition"></param>
        /// <param name="sourceBuildDefinitionNamePrefix"></param>
        /// <param name="targetBuildDefinitionNamePrefix"></param>
        /// <param name="sourceBranchName"></param>
        /// <param name="targetBranchName"></param>
        /// <param name="buildDefinitionCloneProperties"></param>
        /// <returns></returns>
        public ReleaseDefinition CloneReleaseDefinition(BuildDefinition targetBuildDefinition,
                                                        string sourceBuildDefinitionNamePrefix,
                                                        string targetBuildDefinitionNamePrefix,
                                                        string sourceBranchName,
                                                        string targetBranchName,
                                                        BuildDefinitionCloneProperties buildDefinitionCloneProperties,
                                                        CloneBuildDefinitionEventArgs cloneBuildDefinitionEventArgs)
        {
            ReleaseDefinition result = null;
            ReleaseDefinition sourceReleaseDefinition = null;
            ReleaseDefinition targetReleaseDefinition = null;
            ReleaseDefinitionManager releaseDefinitionManager = null;
            StatusList statusList = null;
            CloneReleaseDefinitionEventArgs cloneReleaseDefinitionEventArgs = null;

            try
            {
                statusList = new StatusList();

                releaseDefinitionManager = ReleaseDefinitionManagerFactory.CreateReleaseDefinitionManager(this.TeamProjectCollectionUri,
                                                                                                          this.TeamProjectCollectionVsrmUri,
                                                                                                          this.TeamProjectName,
                                                                                                          this.BasicAuthRestApiUserProfileName,
                                                                                                          this.BasicAuthRestApiPassword);

                sourceReleaseDefinition = releaseDefinitionManager.GetReleaseDefinition(targetBuildDefinition.Name, "all");

                if (sourceReleaseDefinition == null)
                {
                    if (buildDefinitionCloneProperties.CreateMissingReleaseDefinitions == true)
                    {
                        result = releaseDefinitionManager.CreateReleaseDefinitionFromBuildDefinition(targetBuildDefinition,
                                                                                                     sourceBuildDefinitionNamePrefix,
                                                                                                     targetBuildDefinitionNamePrefix,
                                                                                                     sourceBranchName,
                                                                                                     targetBranchName,
                                                                                                     buildDefinitionCloneProperties.MigrateBuildSteps,
                                                                                                     statusList);
                    }
                    else
                    {
                        //TODO: Return error: source release definition missing.
                    }
                }
                else
                {
                    cloneReleaseDefinitionEventArgs = releaseDefinitionManager.CloneReleaseDefinition(sourceReleaseDefinition,
                                                                                                      sourceBuildDefinitionNamePrefix,
                                                                                                      targetBuildDefinitionNamePrefix,
                                                                                                      sourceBuildDefinitionNamePrefix,
                                                                                                      targetBranchName,
                                                                                                      ref targetReleaseDefinition);
                    //TODO: Throw if cloneReleaseDefinitionEventArgs.StatusList contains exception.
                    result = targetReleaseDefinition;
                }

                cloneBuildDefinitionEventArgs.TargetReleaseDefinition = result;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Delete build definitions.
        /// </summary>
        /// <param name="buildDefinitionIdList"></param>
        /// <param name="cancellationToken"></param>
        public void DeleteBuildDefinitions(List<int> buildDefinitionIdList,
                                           CancellationToken cancellationToken)
        {
            BuildDefinitionList buildDefinitionList = null;
            DeleteBuildDefinitionEventArgs deleteBuildDefinitionEventArgs = null;
            BuildDefinition deepBuildDefinition = null;
            int buildDefinitionDeletedCount = 0;

            try
            {
                buildDefinitionList = this.GetDefinitionsRestApi(buildDefinitionIdList);

                foreach (BuildDefinition buildDefinition in buildDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();


                    //Retrieve "deep" build definition.
                    deepBuildDefinition = this.GetDefinitionRestApi(buildDefinition.Id);

                    //Delete build definition.
                    deleteBuildDefinitionEventArgs = this.DeleteBuildDefinition(deepBuildDefinition);
                    deleteBuildDefinitionEventArgs.BuildDefinitionTotalCount = buildDefinitionList.Count;
                    deleteBuildDefinitionEventArgs.BuildDefinitionDeletedCount = ++buildDefinitionDeletedCount;

                    if (BuildDefinitionDeleted != null)
                    {
                        BuildDefinitionDeleted(this, deleteBuildDefinitionEventArgs);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Delete build definition.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <returns></returns>
        public DeleteBuildDefinitionEventArgs DeleteBuildDefinition(BuildDefinition buildDefinition)
        {
            DeleteBuildDefinitionEventArgs result = null;

            try
            {
                result = new DeleteBuildDefinitionEventArgs();

                DeleteDefinitionRestApi(buildDefinition.Id);

                result.BuildDefinition = buildDefinition;

            }
            catch (Exception ex)
            {
                Status status = null;

                result = new DeleteBuildDefinitionEventArgs();
                result.BuildDefinition = buildDefinition;
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        /// <summary>
        /// Change build definitions queue status.
        /// </summary>
        /// <param name="buildDefinitionIdList"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="definitionQueueStatus"></param>
        /// <param name="cancellationToken"></param>
        public void ChangeBuildDefinitionsQueueStatus(List<int> buildDefinitionIdList,
                                                      BuildDefinitionQueueStatus buildDefinitionQueueStatus,
                                                      CancellationToken cancellationToken)
        {
            BuildDefinitionList buildDefinitionList = null;
            ChangeBuildDefinitionQueueStatusEventArgs changeBuildDefinitionQueueStatusEventArgs = null;
            BuildDefinition deepBuildDefinition = null;
            DefinitionQueueStatus definitionQueueStatus;
            int buildDefinitionChangedCount = 0;

            try
            {
                buildDefinitionList = this.GetDefinitionsRestApi(buildDefinitionIdList);

                foreach (BuildDefinition buildDefinition in buildDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();


                    //Retieve "deep" build definition.
                    deepBuildDefinition = this.GetDefinitionRestApi(buildDefinition.Id);

                    //Convert build definition queue status to definition queue status.
                    switch (buildDefinitionQueueStatus)
                    {
                        case BuildDefinitionQueueStatus.Enabled:
                            definitionQueueStatus = DefinitionQueueStatus.Enabled;
                            break;
                        case BuildDefinitionQueueStatus.Paused:
                            definitionQueueStatus = DefinitionQueueStatus.Paused;
                            break;
                        case BuildDefinitionQueueStatus.Disabled:
                            definitionQueueStatus = DefinitionQueueStatus.Disabled;
                            break;
                        default:
                            definitionQueueStatus = DefinitionQueueStatus.Disabled;
                            break;
                    }

                    //Change build definition queue status.
                    changeBuildDefinitionQueueStatusEventArgs = ChangeBuildDefinitionQueueStatus(buildDefinition,
                                                                                                 definitionQueueStatus);
                    changeBuildDefinitionQueueStatusEventArgs.BuildDefinitionTotalCount = buildDefinitionList.Count;
                    changeBuildDefinitionQueueStatusEventArgs.BuildDefinitionChangedCount = ++buildDefinitionChangedCount;
                    if (BuildDefinitionQueueStatusChanged != null)
                    {
                        BuildDefinitionQueueStatusChanged(this, changeBuildDefinitionQueueStatusEventArgs);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Change build definitions settings.
        /// </summary>
        /// <param name="buildDefinitionIdList"></param>
        /// <param name="buildDefinitionSettings"></param>
        /// <param name="cancellationToken"></param>
        public void ChangeBuildDefinitionsSettings(List<int> buildDefinitionIdList,
                                                   BuildDefinitionSettings buildDefinitionSettings,
                                                   CancellationToken cancellationToken)
        {
            BuildDefinitionList buildDefinitionList = null;
            ChangeBuildDefinitionSettingsEventArgs changeBuildDefinitionSettingsEventArgs = null;
            BuildDefinition deepBuildDefinition = null;
            int buildDefinitionChangedCount = 0;

            try
            {
                buildDefinitionList = this.GetDefinitionsRestApi(buildDefinitionIdList);

                foreach (BuildDefinition buildDefinition in buildDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();


                    //Retieve "deep" build definition.
                    deepBuildDefinition = this.GetDefinitionRestApi(buildDefinition.Id);

                    switch (buildDefinitionSettings.BuildDefinitionSettingsGroup)
                    {
                        case BuildDefinitionSettingsGroup.DefaultAgentPoolQueue:

                            //Change build definition default agent pool queue.
                            changeBuildDefinitionSettingsEventArgs = ChangeBuildDefinitionDefaultAgentPoolQueue(buildDefinition,
                                                                                                             buildDefinitionSettings.DefaultAgentPoolQueue.AgentPoolQueueId);

                            break;

                        case BuildDefinitionSettingsGroup.TriggersSchedule:

                            //Change build definiton schedule trigger.
                            changeBuildDefinitionSettingsEventArgs = ChangeBuildDefinitionScheduleTrigger(buildDefinition,
                                                                                                          buildDefinitionSettings.ScheduleTriggerSettings);

                            break;

                        case BuildDefinitionSettingsGroup.TriggersContinousIntegration:

                            //Change build definiton continuous integration trigger.
                            changeBuildDefinitionSettingsEventArgs = this.UpdateBuildDefinitionPathFilters(buildDefinition,
                                                                                                           buildDefinitionSettings);

                            break;

                        default:
                            throw new ArgumentOutOfRangeException(string.Format("Unexpected build definition settings. (BuildDefinitionSettingsGroup: {0})",
                                                                                buildDefinitionSettings.BuildDefinitionSettingsGroup));
                    }


                    changeBuildDefinitionSettingsEventArgs.BuildDefinitionTotalCount = buildDefinitionList.Count;
                    changeBuildDefinitionSettingsEventArgs.BuildDefinitionChangedCount = ++buildDefinitionChangedCount;

                    if (BuildDefinitionSettingsChanged != null)
                    {
                        BuildDefinitionSettingsChanged(this, changeBuildDefinitionSettingsEventArgs);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Change build definition queue status.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="definitionQueueStatus"></param>
        /// <returns></returns>
        public ChangeBuildDefinitionQueueStatusEventArgs ChangeBuildDefinitionQueueStatus(BuildDefinition buildDefinition,
                                                                                        DefinitionQueueStatus definitionQueueStatus)
        {
            ChangeBuildDefinitionQueueStatusEventArgs result = null;

            try
            {
                result = new ChangeBuildDefinitionQueueStatusEventArgs();
                result.BuildDefinition = buildDefinition;

                //Change queue status.
                buildDefinition.QueueStatus = definitionQueueStatus;

                //Save build definition.
                UpdateDefinition(buildDefinition);

                result.BuildDefinition = buildDefinition;

            }
            catch (Exception ex)
            {
                Status status = null;

                result = new ChangeBuildDefinitionQueueStatusEventArgs();
                result.BuildDefinition = buildDefinition;
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        /// <summary>
        /// Change build definition default agent pool queue.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="definitionQueueStatus"></param>
        /// <returns></returns>
        public ChangeBuildDefinitionSettingsEventArgs ChangeBuildDefinitionDefaultAgentPoolQueue(BuildDefinition buildDefinition,
                                                                                               int agentPoolQueueId)
        {
            ChangeBuildDefinitionSettingsEventArgs result = null;

            try
            {
                result = new ChangeBuildDefinitionSettingsEventArgs();
                result.BuildDefinition = buildDefinition;

                //Change agent pool queue.
                buildDefinition.Queue = GetAgentPoolQueueById(agentPoolQueueId);

                //Save build definition.
                UpdateDefinition(buildDefinition);

                result.BuildDefinition = buildDefinition;

            }
            catch (Exception ex)
            {
                Status status = null;

                result = new ChangeBuildDefinitionSettingsEventArgs();
                result.BuildDefinition = buildDefinition;
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        /// <summary>
        /// Change build definition schedule trigger.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="scheduleTriggerSettings"></param>
        /// <returns></returns>
        public ChangeBuildDefinitionSettingsEventArgs ChangeBuildDefinitionScheduleTrigger(BuildDefinition buildDefinition,
                                                                                         TriggerSettingsSchedule scheduleTriggerSettings)
        {
            ChangeBuildDefinitionSettingsEventArgs result = null;
            ScheduleTrigger scheduleTrigger = null;
            bool scheduleTriggerFound = false;
            bool scheduleTriggerWithScheduleFound = false;

            try
            {
                result = new ChangeBuildDefinitionSettingsEventArgs();
                result.BuildDefinition = buildDefinition;

                //Triggers found.
                if (buildDefinition.Triggers != null && buildDefinition.Triggers.Count > 0)
                {
                    foreach (BuildTrigger buildTrigger in buildDefinition.Triggers)
                    {
                        //Build trigger is a schedule trigger.
                        if (buildTrigger.GetType() == typeof(ScheduleTrigger))
                        {
                            scheduleTriggerFound = true;
                            scheduleTrigger = (ScheduleTrigger)buildTrigger;
                            foreach (Schedule schedule in scheduleTrigger.Schedules)
                            {
                                UpdateSchedule(scheduleTriggerSettings, schedule);
                                scheduleTriggerWithScheduleFound = true;
                                break;
                            }

                            //Add schedule allowed and schedule trigger has no schedules.
                            if (scheduleTriggerSettings.AllowAddSchedule == true &&
                                scheduleTriggerWithScheduleFound == false)
                            {
                                Schedule schedule = null;

                                schedule = CreateSchedule(scheduleTriggerSettings);

                                scheduleTrigger.Schedules.Add(schedule);
                            }
                        }
                    }
                }

                //Add schedule allowed and no schedule triggers found.
                if (scheduleTriggerSettings.AllowAddSchedule == true &&
                   scheduleTriggerFound == false)
                {
                    Schedule schedule = null;

                    schedule = CreateSchedule(scheduleTriggerSettings);

                    scheduleTrigger = new ScheduleTrigger();
                    scheduleTrigger.Schedules.Add(schedule);

                    buildDefinition.Triggers.Add(scheduleTrigger);

                }

                //Save build definition.
                UpdateDefinition(buildDefinition);

                result.BuildDefinition = buildDefinition;

            }
            catch (Exception ex)
            {
                Status status = null;

                result = new ChangeBuildDefinitionSettingsEventArgs();
                result.BuildDefinition = buildDefinition;
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        /// <summary>
        /// Request builds.
        /// </summary>
        /// <param name="buildDefinitionIdList"></param>
        /// <param name="parameters"></param>
        /// <param name="overrideQueueStatus"></param>
        /// <param name="queuePriority"></param>
        /// <param name="cancellationToken"></param>
        public void RequestBuilds(List<int> buildDefinitionIdList,
                                  string parameters,
                                  bool overrideQueueStatus,
                                  Aclara.Vso.Build.Client.Types.Enumerations.QueuePriority queuePriority,
                                  CancellationToken cancellationToken)
        {
            BuildDefinitionList buildDefinitionList = null;
            RequestBuildEventArgs requestBuildEventArgs = null;
            BuildDefinition deepBuildDefinition = null;
            int buildDefinitionBuildRequestedCount = 0;

            try
            {
                buildDefinitionList = this.GetDefinitionsRestApi(buildDefinitionIdList);

                foreach (BuildDefinition buildDefinition in buildDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();


                    //Retieve "deep" build definition.
                    deepBuildDefinition = this.GetDefinitionRestApi(buildDefinition.Id);

                    //Request build.
                    requestBuildEventArgs = RequestBuild(buildDefinition,
                                                      parameters,
                                                      overrideQueueStatus,
                                                      queuePriority);

                    requestBuildEventArgs.BuildDefinitionTotalCount = buildDefinitionList.Count;
                    requestBuildEventArgs.BuildDefinitionBuildRequestedCount = ++buildDefinitionBuildRequestedCount;
                    if (BuildRequested != null)
                    {
                        BuildRequested(this, requestBuildEventArgs);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Request build.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="parameters"></param>
        /// <param name="overrideQueueStatus"></param>
        /// <param name="queuePriority"></param>
        /// <returns></returns>
        public RequestBuildEventArgs RequestBuild(BuildDefinition buildDefinition,
                                                string parameters,
                                                bool overrideQueueStatus,
                                                Aclara.Vso.Build.Client.Types.Enumerations.QueuePriority queuePriority)
        {
            RequestBuildEventArgs result = null;
            DefinitionQueueStatus originalDefintionQueueStatus = DefinitionQueueStatus.Disabled;
            BuildDefinition freshBuildDefinition = null;

            try
            {
                result = new RequestBuildEventArgs();

                result.BuildDefinition = buildDefinition;

                originalDefintionQueueStatus = buildDefinition.QueueStatus;

                if (overrideQueueStatus == true &&
                    buildDefinition.QueueStatus == DefinitionQueueStatus.Disabled)
                {
                    //Change build definition queue status.
                    ChangeBuildDefinitionQueueStatus(buildDefinition, DefinitionQueueStatus.Enabled);
                    buildDefinition = this.GetDefinitionRestApi(buildDefinition.Id);
                }

                //Request build.
                QueueBuild(buildDefinition, parameters, queuePriority);

                //Reset queue status (if changed).
                if (overrideQueueStatus == true &&
                    originalDefintionQueueStatus == DefinitionQueueStatus.Disabled)
                {
                    freshBuildDefinition = this.GetDefinitionRestApi(buildDefinition.Id);
                    //Change build definition queue status.
                    ChangeBuildDefinitionQueueStatus(freshBuildDefinition, originalDefintionQueueStatus);
                }

            }
            catch (Exception ex)
            {
                Status status = null;

                result = new RequestBuildEventArgs();
                result.BuildDefinition = buildDefinition;
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);

                //Reset queue status (if changed).
                if (overrideQueueStatus == true &&
                    originalDefintionQueueStatus == DefinitionQueueStatus.Disabled)
                {
                    freshBuildDefinition = this.GetDefinitionRestApi(buildDefinition.Id);
                    //Change build definition queue status.
                    ChangeBuildDefinitionQueueStatus(freshBuildDefinition, originalDefintionQueueStatus);
                }
            }

            return result;

        }

        /// <summary>
        /// Export build definitions.
        /// </summary>
        /// <param name="buildDefinitionIdList"></param>
        /// <param name="cancellationToken"></param>
        public void ExportBuildDefinitions(List<int> buildDefinitionIdList,
                                           CancellationToken cancellationToken)
        {
            BuildDefinitionList buildDefinitionList = null;
            ExportBuildDefinitionEventArgs exportBuildDefinitionEventArgs = null;
            int buildDefinitionAddedToZipArchiveCount = 0;
            MemoryStream zipArchiveMemoryStream = null;
            string buildDefinitionAsJson = string.Empty;
            BuildDefinition validateBuildDefinition = null;
            Status status = null;

            try
            {
                zipArchiveMemoryStream = new MemoryStream();

                buildDefinitionList = this.GetDefinitionsRestApi(buildDefinitionIdList);

                using (ZipArchive zip = new ZipArchive(zipArchiveMemoryStream, ZipArchiveMode.Create, true))
                {
                    foreach (BuildDefinition buildDefinition in buildDefinitionList)
                    {
                        //Check for cancellation.
                        cancellationToken.ThrowIfCancellationRequested();

                        //Retreive build definition as json.
                        buildDefinitionAsJson = this.GetDefinitionRestApiAsyncAsJson(buildDefinition.Id);

                        //Export build definition.
                        exportBuildDefinitionEventArgs = new ExportBuildDefinitionEventArgs();

                        var entry = zip.CreateEntry(string.Format("{0}.json", buildDefinition.Name));
                        using (StreamWriter streamWriter = new StreamWriter(entry.Open()))
                        {
                            streamWriter.WriteLine(buildDefinitionAsJson);
                        }

                        //Validate build definiton property: steps.
                        validateBuildDefinition = JsonConvert.DeserializeObject<BuildDefinition>(buildDefinitionAsJson);
                        if (validateBuildDefinition.Steps == null ||
                            validateBuildDefinition.Steps.Count == 0)
                        {
                            status = new Status();
                            status.Exception = new UpdateBuildDefinitionRequiredPropertiesException("Required build definition property \"Steps\" missing.");
                            status.StatusServerity = StatusTypes.StatusSeverity.Error;
                            exportBuildDefinitionEventArgs.StatusList.Add(status);
                        }

                        exportBuildDefinitionEventArgs.BuildDefinition = buildDefinition;
                        exportBuildDefinitionEventArgs.BuildDefinitionTotalCount = buildDefinitionList.Count;
                        exportBuildDefinitionEventArgs.BuildDefinitionAddedToZipArchiveCount = ++buildDefinitionAddedToZipArchiveCount;
                        if (BuildDefinitionExported != null)
                        {
                            BuildDefinitionExported(this, exportBuildDefinitionEventArgs);
                        }
                    }
                }

                if (BuildDefinitionExportCompleted != null)
                {
                    exportBuildDefinitionEventArgs.BuildDefinition = null;
                    exportBuildDefinitionEventArgs.ZipArchiveMemoryStream = zipArchiveMemoryStream;
                    BuildDefinitionExportCompleted(this, exportBuildDefinitionEventArgs);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Import build definitions.
        /// </summary>
        /// <param name="importDefinitionList"></param>
        /// <param name="buildDefinitionCloneProperties"></param>
        /// <param name="cancellationToken"></param>
        public void ImportBuildDefinitions(DefinitionImportList importDefinitionList,
                                           BuildDefinitionCloneProperties buildDefinitionCloneProperties,
                                           CancellationToken cancellationToken)
        {
            ImportBuildDefinitionEventArgs importBuildDefinitionEventArgs = null;
            int buildDefinitionImportedCount = 0;
            BuildDefinition sourceBuildDefinition = null;
            BuildDefinition targetBuildDefinition = null;

            try
            {

                foreach (DefinitionImport definitionImport in importDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    //Import build definition.
                    importBuildDefinitionEventArgs = new ImportBuildDefinitionEventArgs();

                    //Deserialize json into build definition.
                    sourceBuildDefinition = JsonConvert.DeserializeObject<BuildDefinition>(definitionImport.DefinitionAsJson);

                    //Create build definition from imported build definition.
                    targetBuildDefinition = CreateBuildDefinitionFromAnother(sourceBuildDefinition,
                                                                             sourceBuildDefinition.Name,
                                                                             definitionImport.Name,
                                                                             string.Empty,
                                                                             string.Empty,
                                                                             buildDefinitionCloneProperties);
                    //Save target build definition.
                    UpdateDefinition(targetBuildDefinition);

                    importBuildDefinitionEventArgs.BuildDefinition = sourceBuildDefinition;
                    importBuildDefinitionEventArgs.BuildDefinitionTotalCount = importDefinitionList.Count;
                    importBuildDefinitionEventArgs.BuildDefinitionImportedCount = ++buildDefinitionImportedCount;
                    if (BuildDefinitionImported != null)
                    {
                        BuildDefinitionImported(this, importBuildDefinitionEventArgs);
                    }
                }

                if (BuildDefinitionImportCompleted != null)
                {
                    importBuildDefinitionEventArgs.BuildDefinition = null;
                    importBuildDefinitionEventArgs.Name = string.Empty;
                    importBuildDefinitionEventArgs.OriginalName = string.Empty;
                    BuildDefinitionImportCompleted(this, importBuildDefinitionEventArgs);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        ///// <summary>
        ///// Retrieve team project reference list via web api.
        ///// </summary>
        //public List<TeamProjectReference> GetTeamProjectReferenceListRestApi()
        //{

        //    List<TeamProjectReference> result = null;
        //    string requestUrl = String.Empty;
        //    string parameter = string.Empty;

        //    try
        //    {

        //        using (var httpClient = new HttpClient())
        //        {
        //            httpClient.BaseAddress = this.TeamProjectCollectionUri;
        //            httpClient.DefaultRequestHeaders.Accept.Clear();
        //            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //            parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
        //                                                                                                      this.BasicAuthRestApiUserProfileName,
        //                                                                                                      this.BasicAuthRestApiPassword)));

        //            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

        //            requestUrl = string.Format("{0}DefaultCollection/{1}/_apis/distributedtask/queues",
        //                                       this.TeamProjectCollectionUri,
        //                                       this.TeamProjectName);

        //            HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUrl).Result;

        //            if (httpResponseMessage.IsSuccessStatusCode)
        //            {

        //                result = httpResponseMessage.Content.ReadAsAsync<AgentPoolQueueListContainer>().Result.Value;

        //            }
        //        }

        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }

        //    return result;
        //}

        #endregion

        #region Protected methods

        /// <summary>
        /// Update build definition path filters.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="buildDefinitionSettings"></param>
        /// <returns></returns>
        protected ChangeBuildDefinitionSettingsEventArgs UpdateBuildDefinitionPathFilters(BuildDefinition buildDefinition, BuildDefinitionSettings buildDefinitionSettings)
        {
            //TODO: Remove after coding retrieval of solution path from build defintion build step.
            const string TFVC_Path = @"$/ACEx/17.09/AppLib/CE.Insights/CE.Insights.sln";

            ChangeBuildDefinitionSettingsEventArgs result = null;
            string solutionFileAsText = string.Empty;
            string content = string.Empty;
            List<string> referencedProjectList = null;
            List<string> tfvcReferencedProjectList = null;

            try
            {
                result = new ChangeBuildDefinitionSettingsEventArgs();
                result.BuildDefinition = buildDefinition;

                //TODO: retrieval of solution path from build defintion build step.

                solutionFileAsText = this.TFVC_GetSingleFile(TFVC_Path);
                referencedProjectList = this.GetReferencedProjectList(solutionFileAsText);

                tfvcReferencedProjectList = this.ConvertWindowsPathToTFVCPath(TFVC_Path, referencedProjectList);

                //TODO: Replace or add continuous integration path filters.

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Convert Windows path (list) TFVC path (list).
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="pathList"></param>
        /// <returns></returns>
        protected List<string> ConvertWindowsPathToTFVCPath(string sourcePath, List<string> pathList)
        {
            List<string> result = null;
            string[] sourcePathFolders = null;
            string[] workingPathFolders = null;
            string[] targetPathFolders = null;
            string targetPath = string.Empty;
            List<string> targetPathFolderList = null;
            int matchUpFolderCount = 0;
            int totalSourcePathFolderCount = 0;

            try
            {
                result = new List<string>();

                sourcePathFolders = sourcePath.Split('/');

                foreach (string workingPath in pathList)
                {

                    workingPathFolders = workingPath.Split('\\');
                    matchUpFolderCount = Regex.Matches(Regex.Escape(workingPath), @"[^\\/]+[\\/]..[\\/]").Count;
                    totalSourcePathFolderCount = sourcePathFolders.Count() - 1 - matchUpFolderCount;

                    targetPathFolderList = new List<string>();

                    for (int index = 0; index < totalSourcePathFolderCount; index++)
                    {
                        targetPathFolderList.Add(sourcePathFolders[index]);
                    }

                    foreach (string tempWorkingPathFolder in workingPathFolders)
                    {
                        if (tempWorkingPathFolder != "..")
                        {
                            targetPathFolderList.Add(tempWorkingPathFolder);
                        }
                    }

                    targetPathFolders = targetPathFolderList.ToArray();

                    targetPath = string.Join("/", targetPathFolders);

                    result.Add(targetPath);
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve referenced project list from solution file (as text).
        /// </summary>
        /// <param name="solutionFileAsText"></param>
        /// <returns></returns>
        protected List<string> GetReferencedProjectList(string solutionFileAsText)
        {
            const string RegEx_Pattern = "Project\\(\"\\{[\\w-]*\\}\"\\) = \"([\\w _]*.*)\", \"(.*\\.(cs|vcx|vb)proj)\"";
            const int MatchGroupCount = 2;

            List<string> result = null;
            IEnumerable<Match> matches = null;
            Regex regex = null;

            try
            {
                regex = new Regex(RegEx_Pattern, RegexOptions.Compiled);

                matches = regex.Matches(solutionFileAsText).Cast<Match>();

                result = matches.Select(x => x.Groups[MatchGroupCount].Value).ToList();

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// TFVC: Retrieve a single file from (Team Foundation version control).
        /// </summary>
        /// <returns></returns>
        protected string TFVC_GetSingleFile(string path)
        {
            string result = string.Empty;
            string requestUrl = String.Empty;
            string parameter = string.Empty;
            Stream receiveStream = null;
            StreamReader readStream = null;
            try
            {

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/octet-stream"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUrl = string.Format("{0}DefaultCollection/{1}/_apis/tfvc/items?path={2}&api-version=1.0",
                                               this.TeamProjectCollectionUri,
                                               this.TeamProjectName,
                                               path);

                    //var request = (HttpWebRequest)WebRequest.Create(requestUrl);

                    //request.UserAgent = "VSTS-Get";
                    //request.Headers.Set(HttpRequestHeader.Authorization, GetAuthenticationHeaderValue(
                    //                    authentication).ToString());
                    //request.ContentType = "application/json";
                    //request.Method = httpMethod;

                    //response = (HttpWebResponse)request.GetResponse();

                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUrl).Result;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        //var content = httpResponseMessage.Content.ReadAsStreamAsync();
                        receiveStream = httpResponseMessage.Content.ReadAsStreamAsync().Result;
                        readStream = new StreamReader(receiveStream, Encoding.UTF8);
                        result = readStream.ReadToEnd();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create schedule.
        /// </summary>
        /// <param name="scheduleTriggerSettings"></param>
        /// <returns></returns>
        protected Schedule CreateSchedule(TriggerSettingsSchedule scheduleTriggerSettings)
        {

            Schedule result = null;
            BWScheduleDays scheduleDays = BWScheduleDays.None;

            try
            {
                scheduleDays = ConvertDaysToBuildSchedule(scheduleTriggerSettings.DaystoBuildSchedule);

                result = new Schedule();

                result.StartHours = scheduleTriggerSettings.Hours;
                result.StartMinutes = scheduleTriggerSettings.Minutes;
                result.TimeZoneId = scheduleTriggerSettings.TimeZoneId;
                result.DaysToBuild = scheduleDays;
                result.BranchFilters.Add(scheduleTriggerSettings.TeamProjectName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Update schedule.
        /// </summary>
        /// <param name="scheduleTriggerSettings"></param>
        /// <param name="schedule"></param>
        protected void UpdateSchedule(TriggerSettingsSchedule scheduleTriggerSettings, Schedule schedule)
        {
            BWScheduleDays scheduleDays = BWScheduleDays.None;

            try
            {
                scheduleDays = ConvertDaysToBuildSchedule(scheduleTriggerSettings.DaystoBuildSchedule);

                schedule.StartHours = scheduleTriggerSettings.Hours;
                schedule.StartMinutes = scheduleTriggerSettings.Minutes;
                schedule.TimeZoneId = scheduleTriggerSettings.TimeZoneId;
                schedule.DaysToBuild = scheduleDays;

                schedule.DaysToBuild = scheduleDays;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Convert days to build schedule to schedule days.
        /// </summary>
        /// <param name="daysToBuildSchedule"></param>
        /// <returns></returns>
        protected BWScheduleDays ConvertDaysToBuildSchedule(DaysToBuildSchedule daysToBuildSchedule)
        {
            BWScheduleDays result = BWScheduleDays.None;

            try
            {
                if (daysToBuildSchedule.Monday == true)
                {
                    result |= BWScheduleDays.Monday;
                }
                if (daysToBuildSchedule.Tuesday == true)
                {
                    result |= BWScheduleDays.Tuesday;
                }
                if (daysToBuildSchedule.Wednesday == true)
                {
                    result |= BWScheduleDays.Wednesday;
                }
                if (daysToBuildSchedule.Thursday == true)
                {
                    result |= BWScheduleDays.Thursday;
                }
                if (daysToBuildSchedule.Friday == true)
                {
                    result |= BWScheduleDays.Friday;
                }
                if (daysToBuildSchedule.Saturday == true)
                {
                    result |= BWScheduleDays.Saturday;
                }
                if (daysToBuildSchedule.Sunday == true)
                {
                    result |= BWScheduleDays.Sunday;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Retrieve agent pool queue by identifier.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected AgentPoolQueue GetAgentPoolQueueById(int id)
        {
            AgentPoolQueue result = null;
            List<AgentPoolQueue> agentPoolQueueList = null;
            AgentPoolQueue agentPoolQueue = null;

            try
            {

                agentPoolQueueList = this.GetAgentPoolQueuesRestApi();

                agentPoolQueue = agentPoolQueueList.FirstOrDefault(i => i.Id == id);

                result = agentPoolQueue;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create build definition from another.
        /// </summary>
        /// <param name="sourceBuildDefinition"></param>
        /// <param name="sourceBuildDefinitionNamePrefix"></param>
        /// <param name="targetBuildDefinitionNamePrefix"></param>
        /// <param name="sourceBranchName"></param>
        /// <param name="targetBranchName"></param>
        /// <param name="buildDefinitionCloneProperties"></param>
        /// <returns></returns>
        protected BuildDefinition CreateBuildDefinitionFromAnother(BuildDefinition sourceBuildDefinition,
                                                                   string sourceBuildDefinitionNamePrefix,
                                                                   string targetBuildDefinitionNamePrefix,
                                                                   string sourceBranchName,
                                                                   string targetBranchName,
                                                                   BuildDefinitionCloneProperties buildDefinitionCloneProperties)
        {
            BuildDefinition result = null;
            BuildDefinition targetBuildDefinition = null;
            List<AgentPoolQueue> agentPoolQueueList = null;
            //Fix:Backlog Item: 23021 - DevOps (All): POC GIT Based VSTS Builds [BEGIN]
            bool migrateToGitPOC = false;
            //Fix:Backlog Item: 23021 - DevOps (All): POC GIT Based VSTS Builds [END]

            try
            {

                targetBuildDefinition = new BuildDefinition();

                //Fix:Backlog Item: 23021 - DevOps (All): POC GIT Based VSTS Builds [BEGIN]
                if ((sourceBuildDefinition.Project.Name != this.TeamProjectName) &&
                    (buildDefinitionCloneProperties.MigrateToGit == true))
                {
                    migrateToGitPOC = true;
                }
                //Fix:Backlog Item: 23021 - DevOps (All): POC GIT Based VSTS Builds [END]

                //Uri:
                targetBuildDefinition.Uri = sourceBuildDefinition.Uri;

                //Project:
                if (sourceBuildDefinition.Project.Name == this.TeamProjectName)
                {
                    targetBuildDefinition.Project = sourceBuildDefinition.Project;
                }
                else
                {
                    targetBuildDefinition.Project = null;
                }

                //Name:
                targetBuildDefinition.Name = ReplaceTokenInTextWithValue(sourceBuildDefinition.Name,
                                                                         sourceBuildDefinitionNamePrefix,
                                                                         targetBuildDefinitionNamePrefix);
                //Path:
                if (string.IsNullOrEmpty(buildDefinitionCloneProperties.BuildDefinitionFolderName) != true)
                {
                    targetBuildDefinition.Path = BuildDefinition_PathPrefex + buildDefinitionCloneProperties.BuildDefinitionFolderName;
                }

                //Build steps:
                foreach (BuildDefinitionStep buildDefinitionStep in sourceBuildDefinition.Steps)
                {
                    targetBuildDefinition.Steps.Add(buildDefinitionStep);
                }

                if (buildDefinitionCloneProperties.MigrateToGit == true)
                {
                    foreach (BuildDefinitionStep targetDefinitionStep in targetBuildDefinition.Steps)
                    {
                        targetDefinitionStep.DisplayName = FixSolutionFilePathForGit(targetDefinitionStep.DisplayName);

                        List<string> keys = new List<string>(targetDefinitionStep.Inputs.Keys);
                        foreach (string key in keys)
                        {
                            //Fix:Backlog Item: 23021 - DevOps (All): POC GIT Based VSTS Builds [BEGIN]
                            if (migrateToGitPOC == true)
                            {
                                //Replace Resource Manager based Connection Endpoint to Azure.
                                //FROM    VSTS Project: ACEx
                                //     Connection Name: Aclara-Azure ACE DEV RM
                                //
                                //TO      VSTS Project: PocDevOps
                                //     Connection Name:PocDevOps Aclara-Azure ACE DEV RM
                                if (targetDefinitionStep.Inputs[key] == "dcfb1d96-bd77-49d3-9e2a-1657e8701ca8")
                                {
                                    targetDefinitionStep.Inputs[key] = "5ed334e5-e653-4816-a3ae-b605d4ef75a0";
                                    break;
                                }
                                //Replace Azure Classic based Connection Endpoint to Azure.
                                //FROM    VSTS Project: ACEx
                                //     Connection Name: Aclara-Azure(Converted to EA)
                                //
                                //TO      VSTS Project: PocDevOps
                                //     Connection Name:PocDevOps Aclara-Azure ACE DEV
                                if (targetDefinitionStep.Inputs[key] == "d8f26de2-5a35-4992-adbc-65dbe39a4661")
                                {
                                    targetDefinitionStep.Inputs[key] = "b44f8409-705b-45a9-bd91-79109cb535eb";
                                    break;
                                }
                            }
                            //Fix:Backlog Item: 23021 - DevOps (All): POC GIT Based VSTS Builds [END]

                            //Remove $/ACEX/<branch> from any input parameters.
                            targetDefinitionStep.Inputs[key] = FixSolutionFilePathForGit(targetDefinitionStep.Inputs[key]);
                        }
                    }

                }
                //Repository:
                switch (buildDefinitionCloneProperties.RepositoryType)
                {
                    case RepositoryType.SourceBuildDefinitionRepositoryType:

                        targetBuildDefinition.Repository = sourceBuildDefinition.Repository;
                        break;
                    case RepositoryType.Git:
                        targetBuildDefinition.Repository = sourceBuildDefinition.Repository;
                        if (buildDefinitionCloneProperties.MigrateToGit == true)
                        {
                            targetBuildDefinition.Repository = this.CreateBuildRepository(sourceBuildDefinition.Repository,
                                                                                          buildDefinitionCloneProperties.GitRepository);
                        }
                        else
                        {
                            targetBuildDefinition.Repository = sourceBuildDefinition.Repository;
                            targetBuildDefinition.Repository.Type = RepositoryTypeName_TFSGit;
                        }
                        break;
                    case RepositoryType.TeamFoundationVersionControl:
                        targetBuildDefinition.Repository = sourceBuildDefinition.Repository;
                        targetBuildDefinition.Repository.Type = RepositoryTypeName_TFSVersionControl;
                        break;
                    case RepositoryType.GitHub:
                        throw new NotImplementedException(string.Format("Support for GitHub not implemented."));
                    case RepositoryType.ExternalGit:
                        throw new NotImplementedException(string.Format("Support for External Git not implemented."));
                    case RepositoryType.Subversion:
                        throw new NotImplementedException(string.Format("Support for Subversion not implemented."));
                    default:
                        throw new NotImplementedException(string.Format("Support for Subversion not implemented."));
                }

                if (buildDefinitionCloneProperties.MigrateToGit == true)
                {
                    //Override default branch.
                    targetBuildDefinition.Repository.DefaultBranch = string.Format("refs/heads/{0}",
                                                                                   targetBranchName);
                }

                //Variables:
                foreach (KeyValuePair<string, BuildDefinitionVariable> variable in sourceBuildDefinition.Variables)
                {
                    targetBuildDefinition.Variables.Add(variable.Key, variable.Value);
                }

                //Triggers:
                this.CopyAndAdjustTriggers(sourceBuildDefinition.Triggers, 
                                           targetBuildDefinition.Triggers, 
                                           buildDefinitionCloneProperties, 
                                           targetBranchName);


                targetBuildDefinition.Queue = sourceBuildDefinition.Queue;

                //Fix:Backlog Item: 23021 - DevOps (All): POC GIT Based VSTS Builds [BEGIN]
                if (migrateToGitPOC == true)
                {
                    agentPoolQueueList = GetAgentPoolQueuesRestApi();
                    if (agentPoolQueueList != null)
                    {
                        //Queue:
                        targetBuildDefinition.Queue = agentPoolQueueList.Where(x => x.Name == sourceBuildDefinition.Queue.Name).Single();
                    }
                }
                //Fix:Backlog Item: 23021 - DevOps (All): POC GIT Based VSTS Builds [END]

                //Description:
                targetBuildDefinition.Description = sourceBuildDefinition.Description;

                //Fix:Backlog Item: 23021 - DevOps (All): POC GIT Based VSTS Builds [BEGIN]
                if (migrateToGitPOC == true)
                {
                    //Description:
                    targetBuildDefinition.Description = sourceBuildDefinition.Description +
                                                        Environment.NewLine +
                                                        "Exported from VSTS project: ACEx." +
                                                        Environment.NewLine +
                                                        "Migrated repository FROM: TfsVersionControl TO: TfsGit";
                }

                //Build number format:
                targetBuildDefinition.BuildNumberFormat = sourceBuildDefinition.BuildNumberFormat;

                //Build job timeout in minutes:
                targetBuildDefinition.JobTimeoutInMinutes = sourceBuildDefinition.JobTimeoutInMinutes;

                //Retention:
                foreach (BWRetentionPolicy retentionPolicy in sourceBuildDefinition.RetentionRules)
                {
                    targetBuildDefinition.RetentionRules.Add(retentionPolicy);
                }

                //Properties:
                foreach (KeyValuePair<string, object> property in sourceBuildDefinition.Properties)
                {
                    targetBuildDefinition.Properties.Add(property.Key, property.Value);
                }

                //Create target build definition.
                targetBuildDefinition = CreateDefinition(sourceBuildDefinition, targetBuildDefinition);

                //Disable target build defintion.
                targetBuildDefinition.QueueStatus = DefinitionQueueStatus.Disabled;

                //Replace text in target build definition.
                ReplaceTextInBuildDefinition(ref targetBuildDefinition,
                                             sourceBuildDefinitionNamePrefix,
                                             targetBuildDefinitionNamePrefix,
                                             sourceBranchName,
                                             targetBranchName);

                result = targetBuildDefinition;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create build definition asynchronously.
        /// </summary>
        /// <param name="sourceBuildDefinition"></param>
        /// <param name="targetBuildDefinition"></param>
        protected BuildDefinition CreateDefinition(BuildDefinition sourceBuildDefinition,
                                                   BuildDefinition targetBuildDefinition)
        {

            BuildDefinition result = null;
            BuildHttpClient buildHttpClient = null;
            VssCredentials vssCredentials = null;
            string userState = string.Empty;
            Task<BuildDefinition> createDefinitionTask = null;

            try
            {

                vssCredentials = new VssCredentials();
                vssCredentials = new VssCredentials(new WindowsCredential(new NetworkCredential(this.BasicAuthRestApiUserProfileName, this.BasicAuthRestApiPassword)));

                buildHttpClient = new BuildHttpClient(this.TeamProjectCollectionUri, vssCredentials);

                //Create build definition.
                createDefinitionTask = buildHttpClient.CreateDefinitionAsync(targetBuildDefinition, this.TeamProjectName, null, null, userState, default(CancellationToken));

                createDefinitionTask.Wait();
                result = createDefinitionTask.Result;

                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Update build definition asynchronously.
        /// </summary>
        /// <param name="buildDefinition"></param>
        protected void UpdateDefinition(BuildDefinition buildDefinition)
        {

            BuildHttpClient buildHttpClient = null;
            VssCredentials vssCredentials = null;
            string userState = string.Empty;
            Task<BuildDefinition> updateDefinitionTask = null;

            try
            {
                //Validate build definiton property: steps.
                if (buildDefinition.Steps == null ||
                    buildDefinition.Steps.Count == 0)
                {
                    throw new UpdateBuildDefinitionRequiredPropertiesException("Required build definition property \"Steps\" missing.");
                }

                vssCredentials = new VssCredentials();
                vssCredentials = new VssCredentials(new WindowsCredential(new NetworkCredential(this.BasicAuthRestApiUserProfileName, this.BasicAuthRestApiPassword)));

                buildHttpClient = new BuildHttpClient(this.TeamProjectCollectionUri, vssCredentials);

                //Create build definition.
                updateDefinitionTask = buildHttpClient.UpdateDefinitionAsync(buildDefinition, userState, default(CancellationToken));
                updateDefinitionTask.Wait();

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Queue build asynchronously.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="parameters"></param>
        protected void QueueBuild(BuildDefinition buildDefinition,
                                  string parameters,
                                  Aclara.Vso.Build.Client.Types.Enumerations.QueuePriority queuePriority)
        {

            BuildHttpClient buildHttpClient = null;
            VssCredentials vssCredentials = null;
            string userState = string.Empty;
            Task<Microsoft.TeamFoundation.Build.WebApi.Build> queueBuildTask = null;
            Microsoft.TeamFoundation.Build.WebApi.Build build = null;
            bool? ignoreWarnings = false;

            try
            {

                vssCredentials = new VssCredentials();
                vssCredentials = new VssCredentials(new WindowsCredential(new NetworkCredential(this.BasicAuthRestApiUserProfileName, this.BasicAuthRestApiPassword)));

                buildHttpClient = new BuildHttpClient(this.TeamProjectCollectionUri, vssCredentials);

                build = new Microsoft.TeamFoundation.Build.WebApi.Build();

                build.Definition = buildDefinition;

                build.Priority = ConvertQueuePriority(queuePriority);

                if (string.IsNullOrEmpty(parameters) == false)
                {
                    build.Parameters = string.Format("{{{0}}}",
                                                     parameters);
                }

                ignoreWarnings = false;

                //Queue build.
                queueBuildTask = buildHttpClient.QueueBuildAsync(build, this.TeamProjectName, ignoreWarnings, userState, default(CancellationToken));
                queueBuildTask.Wait();

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Replace text in build definition.
        /// </summary>
        /// <param name="buildDefinition"></param>
        protected void ReplaceTextInBuildDefinition(ref BuildDefinition buildDefinition,
                                                    string sourceBuildDefinitionNamePrefix,
                                                    string targetBuildDefinitionNamePrefix,
                                                    string sourceBranchName,
                                                    string targetBranchName)
        {
            ContinuousIntegrationTrigger continuousIntegrationTrigger = null;

            try
            {

                buildDefinition.Description = ReplaceTokenInTextWithValue(buildDefinition.Description,
                                                                          sourceBuildDefinitionNamePrefix,
                                                                          targetBuildDefinitionNamePrefix);

                buildDefinition.Description = ReplaceTokenInTextWithValue(buildDefinition.Description,
                                                                          sourceBranchName,
                                                                          targetBranchName);


                buildDefinition.Repository.DefaultBranch = ReplaceTokenInTextWithValue(buildDefinition.Repository.DefaultBranch,
                                                                                       sourceBranchName,
                                                                                       targetBranchName);

                if (buildDefinition.Repository.Properties.ContainsKey(PropertyKey_TFVCMapping) == true)
                {
                    buildDefinition.Repository.Properties[PropertyKey_TFVCMapping] = ReplaceTokenInTextWithValue(buildDefinition.Repository.Properties[PropertyKey_TFVCMapping].ToString(),
                                                                                                                 sourceBranchName,
                                                                                                                 targetBranchName);
                }

                foreach (BuildDefinitionStep buildDefinitionStep in buildDefinition.Steps)
                {

                    buildDefinitionStep.DisplayName = ReplaceTokenInTextWithValue(buildDefinitionStep.DisplayName,
                                                                                      sourceBranchName,
                                                                                      targetBranchName);


                    List<string> keys = new List<string>(buildDefinitionStep.Inputs.Keys);
                    foreach (string key in keys)
                    {

                        buildDefinitionStep.Inputs[key] = ReplaceTokenInTextWithValue(buildDefinitionStep.Inputs[key].ToString(),
                                                                                      sourceBranchName,
                                                                                      targetBranchName);
                    }
                }

                //Triggers:
                foreach (BuildTrigger buildTrigger in buildDefinition.Triggers)
                {
                    //Continuous itegration triggers.
                    if (buildTrigger.GetType() == typeof(ContinuousIntegrationTrigger))
                    {
                        continuousIntegrationTrigger = (ContinuousIntegrationTrigger)buildTrigger;

                        //Branch filters.
                        for (int branchFilterIndex = 0; branchFilterIndex < continuousIntegrationTrigger.BranchFilters.Count; branchFilterIndex++)
                        {
                            continuousIntegrationTrigger.BranchFilters[branchFilterIndex] = ReplaceTokenInTextWithValue(continuousIntegrationTrigger.BranchFilters[branchFilterIndex],
                                                                                                                        sourceBranchName,
                                                                                                                        targetBranchName);
                        }

                        //Path filters.
                        for (int pathFilterIndex = 0; pathFilterIndex < continuousIntegrationTrigger.PathFilters.Count; pathFilterIndex++)
                        {
                            continuousIntegrationTrigger.PathFilters[pathFilterIndex] = ReplaceTokenInTextWithValue(continuousIntegrationTrigger.PathFilters[pathFilterIndex],
                                                                                                                    sourceBranchName,
                                                                                                                    targetBranchName);
                        }
                    }
                }



            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Replace token in text with specified value.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="token"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string ReplaceTokenInTextWithValue(string text, string token, string value)
        {
            string result = string.Empty;

            try
            {

                if (string.IsNullOrEmpty(text) == true)
                {
                    result = text;
                    return result;
                }

                result = Regex.Replace(text, token, value, RegexOptions.IgnoreCase);

                return result;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Copy and convert source triggers to target triggers.
        /// </summary>
        /// <param name="sourceTriggers"></param>
        /// <param name="targetTriggers"></param>
        /// <param name="buildDefinitionCloneProperties"></param>
        /// <param name="gitBranchName"></param>
        protected void CopyAndAdjustTriggers(List<BuildTrigger> sourceTriggers, 
                                            List<BuildTrigger> targetTriggers,
                                            BuildDefinitionCloneProperties buildDefinitionCloneProperties,
                                            string gitBranchName)
        {
            ContinuousIntegrationTrigger continuousIntegrationTrigger = null;

            foreach (BuildTrigger buildTrigger in sourceTriggers)
            {

                //Transfer Branch Filters to Path Filters.
                if (buildTrigger.TriggerType == DefinitionTriggerType.ContinuousIntegration)
                {

                    if ((buildTrigger).GetType() == typeof(ContinuousIntegrationTrigger))
                    {

                        continuousIntegrationTrigger = (ContinuousIntegrationTrigger)buildTrigger;
                        if (continuousIntegrationTrigger.PathFilters.Count == 0)
                        {
                            foreach (string branchFilter in continuousIntegrationTrigger.BranchFilters)
                            {
                                continuousIntegrationTrigger.PathFilters.Add(branchFilter);
                            }

                            continuousIntegrationTrigger.BranchFilters.Clear();
                        }
                    }
                }

                if (buildDefinitionCloneProperties.MigrateToGit == true)
                {
                    ContinuousIntegrationTrigger gitBuildTrigger = new ContinuousIntegrationTrigger();
                    gitBuildTrigger.BatchChanges = false;
                    gitBuildTrigger.MaxConcurrentBuildsPerBranch = 1;
                    gitBuildTrigger.PollingInterval = 0;
                    gitBuildTrigger.BranchFilters.Add(string.Format("+refs/heads/{0}", gitBranchName));

                    targetTriggers.Add(gitBuildTrigger);
                }
                else
                {
                    targetTriggers.Add(buildTrigger);
                }

            }
        }

        /// <summary>
        /// Adjust triggers.
        /// </summary>
        /// <param name="triggers"></param>
        /// <param name="targetTriggers"></param>
        protected void AdjustTriggers(List<BuildTrigger> triggers)
        {
            ContinuousIntegrationTrigger continuousIntegrationTrigger = null;

            foreach (BuildTrigger buildTrigger in triggers)
            {

                //Transfer Branch Filters to Path Filters.
                if (buildTrigger.TriggerType == DefinitionTriggerType.ContinuousIntegration)
                {

                    if ((buildTrigger).GetType() == typeof(ContinuousIntegrationTrigger))
                    {

                        continuousIntegrationTrigger = (ContinuousIntegrationTrigger)buildTrigger;
                        if (continuousIntegrationTrigger.PathFilters.Count == 0)
                        {
                            foreach (string branchFilter in continuousIntegrationTrigger.BranchFilters)
                            {
                                continuousIntegrationTrigger.PathFilters.Add(branchFilter);
                            }

                            continuousIntegrationTrigger.BranchFilters.Clear();
                        }
                    }
                }

            }
        }

        /// <summary>
        /// Convert queue priority.
        /// </summary>
        /// <param name="queuePriority"></param>
        /// <returns></returns>
        protected Microsoft.TeamFoundation.Build.WebApi.QueuePriority ConvertQueuePriority(Aclara.Vso.Build.Client.Types.Enumerations.QueuePriority queuePriority)
        {
            Microsoft.TeamFoundation.Build.WebApi.QueuePriority result = Microsoft.TeamFoundation.Build.WebApi.QueuePriority.Normal;

            switch (queuePriority)
            {
                case Aclara.Vso.Build.Client.Types.Enumerations.QueuePriority.High:

                    result = Microsoft.TeamFoundation.Build.WebApi.QueuePriority.High;
                    break;
                case Aclara.Vso.Build.Client.Types.Enumerations.QueuePriority.AboveNormal:
                    result = Microsoft.TeamFoundation.Build.WebApi.QueuePriority.AboveNormal;
                    break;
                case Aclara.Vso.Build.Client.Types.Enumerations.QueuePriority.Normal:
                    result = Microsoft.TeamFoundation.Build.WebApi.QueuePriority.Normal;
                    break;
                case Aclara.Vso.Build.Client.Types.Enumerations.QueuePriority.BelowNormal:
                    result = Microsoft.TeamFoundation.Build.WebApi.QueuePriority.BelowNormal;
                    break;
                case Aclara.Vso.Build.Client.Types.Enumerations.QueuePriority.Low:
                    result = Microsoft.TeamFoundation.Build.WebApi.QueuePriority.Low;
                    break;
                default:
                    result = Microsoft.TeamFoundation.Build.WebApi.QueuePriority.Normal;
                    break;
            }

            return result;
        }

        /// <summary>
        /// Retrieve definition references not already retrieved.
        /// </summary>
        /// <param name="definitionReferences"></param>
        /// <param name="alreadyRetrievedBuildDefinitionList"></param>
        /// <returns></returns>
        protected List<DefinitionReference> GetDefinitionReferencesNotAlreadyRetrieved(List<DefinitionReference> definitionReferences,
                                                                                       BuildDefinitionList alreadyRetrievedBuildDefinitionList)
        {
            List<DefinitionReference> result = null;

            try
            {
                result = new List<DefinitionReference>();

                var missingDefinitionReferences = from definitionReference in definitionReferences
                                                  where !alreadyRetrievedBuildDefinitionList.Any(b => b.Name == definitionReference.Name)
                                                  select definitionReference;

                foreach (DefinitionReference definitionReference in missingDefinitionReferences)
                {
                    result.Add(definitionReference);
                }

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Add build definition to zip archive.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="parameters"></param>
        /// <param name="overrideQueueStatus"></param>
        /// <param name="queuePriority"></param>
        /// <returns></returns>
        protected ExportBuildDefinitionEventArgs AddBuildDefinitionToZipArchive(BuildDefinition buildDefinition,
                                                                                            byte[] fileContent,
                                                                                            ZipArchive zipArchive)
        {
            ExportBuildDefinitionEventArgs result = null;

            try
            {
                result = new ExportBuildDefinitionEventArgs();

                result.BuildDefinition = buildDefinition;

                var entry = zipArchive.CreateEntry(string.Format("{0}.txt", buildDefinition.Name));
                //using (var stream = entry.Open())
                //{
                //    stream.Write(fileContent, 0, fileContent.Length);
                //}
                using (StreamWriter sw = new StreamWriter(entry.Open()))
                {
                    sw.WriteLine(
                        "Etiam eros nunc, hendrerit nec malesuada vitae, pretium at ligula.");
                }
            }
            catch (Exception ex)
            {
                Status status = null;

                result = new ExportBuildDefinitionEventArgs();
                result.BuildDefinition = buildDefinition;
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        /// <summary>
        /// Add build definition to zip archive.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="parameters"></param>
        /// <param name="overrideQueueStatus"></param>
        /// <param name="queuePriority"></param>
        /// <returns></returns>
        protected ExportBuildDefinitionEventArgs AddBuildDefinitionToZipArchive1(BuildDefinition buildDefinition,
                                                                                            byte[] fileContent,
                                                                                            ZipArchive zipArchive)
        {
            ExportBuildDefinitionEventArgs result = null;

            try
            {
                result = new ExportBuildDefinitionEventArgs();

                result.BuildDefinition = buildDefinition;

                var entry = zipArchive.CreateEntry("test.txt");
                using (StreamWriter sw = new StreamWriter(entry.Open()))
                {
                    sw.WriteLine(
                        "Etiam eros nunc, hendrerit nec malesuada vitae, pretium at ligula.");
                }
            }
            catch (Exception ex)
            {
                Status status = null;

                result = new ExportBuildDefinitionEventArgs();
                result.BuildDefinition = buildDefinition;
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }
        /// <summary>
        /// Retrieve build definition asynchronously via web api as json.
        /// </summary>
        /// <param name="definitionId"></param>
        protected string GetDefinitionRestApiAsyncAsJson(int definitionId)
        {

            string result = null;

            string requestUrl = String.Empty;
            string parameter = string.Empty;

            try
            {

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUrl = string.Format("{0}DefaultCollection/{1}/_apis/build/definitions/{2}?api-version=2.0",
                                               this.TeamProjectCollectionUri,
                                               this.TeamProjectName,
                                               definitionId);
                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUrl).Result;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        result = httpResponseMessage.Content.ReadAsStringAsync().Result;
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        /// <summary>
        /// Create build repository.
        /// </summary>
        /// <param name="sourceBuildRepository"></param>
        /// <param name="gitRepository"></param>
        /// <returns></returns>
        protected BuildRepository CreateBuildRepository(BuildRepository sourceBuildRepository,
                                                        GitRepository gitRepository)
        {
            BuildRepository result = null;
            KeyValuePair<string, string> keyValuePair;

            try
            {
                result = new BuildRepository();

                result.Id = gitRepository.Id.ToString();
                result.Type = "TfsGit";
                result.Name = gitRepository.Name;
                result.Url = new System.Uri(gitRepository.Url);
                result.DefaultBranch = gitRepository.DefaultBranch;
                result.Clean = sourceBuildRepository.Clean;
                result.CheckoutSubmodules = sourceBuildRepository.CheckoutSubmodules;

                result.Properties.Add(BuildRepositoryProperty_DataMemberName_CheckoutNestedSubmodules, "false");

                result.Properties.Add(BuildRepositoryProperty_DataMemberName_CleanOptions,
                                      GetValueFromBuildRepositoryProperty(sourceBuildRepository.Properties,
                                                                          BuildRepositoryProperty_DataMemberName_CleanOptions));

                result.Properties.Add(BuildRepositoryProperty_DataMemberName_FetchDepth, "0");

                result.Properties.Add(BuildRepositoryProperty_DataMemberName_GitLfsSupport, "false");

                result.Properties.Add(BuildRepositoryProperty_DataMemberName_LabelSources,
                                      GetValueFromBuildRepositoryProperty(sourceBuildRepository.Properties,
                                                                          BuildRepositoryProperty_DataMemberName_LabelSources));

                result.Properties.Add(BuildRepositoryProperty_DataMemberName_LabelSourcesFormat, "$(build.buildNumber)");

                result.Properties.Add(BuildRepositoryProperty_DataMemberName_ReportBuildStatus, "true");

                result.Properties.Add(BuildRepositoryProperty_DataMemberName_SkipSyncSource, "false");

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Retrieve value from build repository property.
        /// </summary>
        /// <param name="properties"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        protected string GetValueFromBuildRepositoryProperty(IDictionary<string, string> properties, string key)
        {
            string result = string.Empty;

            if (properties != null)
            {

                if (properties.ContainsKey(key) == true)
                {
                    result = properties[key];
                }
            }

            return result;


        }

        #endregion

        #region Private methods

        /// <summary>
        /// Fix solution path for git.
        /// </summary>
        /// <param name="solutionFilePath"></param>
        /// <returns></returns>
        private string FixSolutionFilePathForGit(string solutionFilePath)
        {
            string result = string.Empty;

            try
            {
                result = solutionFilePath;

                result = result.Replace("$/ACEx/17.06/", string.Empty);
                result = result.Replace("$/ACEx/17.07/", string.Empty);
                result = result.Replace("$/ACEx/17.08/", string.Empty);
                result = result.Replace("$/ACEx/17.09/", string.Empty);
                result = result.Replace("$/ACEx/17.10/", string.Empty);
                result = result.Replace("$/ACEx/17.10A/", string.Empty);
                result = result.Replace("$/ACEx/17.12/", string.Empty);
                result = result.Replace("$/ACEx/18.01/", string.Empty);
                result = result.Replace("$/ACEx/18.02/", string.Empty);
                result = result.Replace("$/ACEx/18.03/", string.Empty);
                result = result.Replace("$/ACEx/18.04/", string.Empty);
                result = result.Replace("$/ACEx/18.05/", string.Empty);
                result = result.Replace("$/ACEx/18.06/", string.Empty);
                result = result.Replace("$/ACEx/18.07/", string.Empty);
                result = result.Replace("$/ACEx/18.08/", string.Empty);
                result = result.Replace("$/ACEx/18.09/", string.Empty);
                result = result.Replace("$/ACEx/18.10/", string.Empty);
                result = result.Replace("$/ACEx/18.12/", string.Empty);

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }


        #endregion
    }
}
