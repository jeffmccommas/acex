﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vso.Build.Client.Types
{
    public class Enumerations
    {
        public enum BuildDefinitionQueueStatus
        {
            Enabled = 0,
            Paused = 1,
            Disabled = 2
        }

        public enum Verbosity
        {
            Quiet = 0,
            Minimal = 1,
            Normal = 2,
            Detailed = 3,
            Diagnostic = 4
        }

        public enum QueuePriority
        {
            High = 0,
            AboveNormal = 1,
            Normal = 2,
            BelowNormal = 3,
            Low = 4
        }

        public enum DateFilter
        {
            Recent = 0,
            Last_6_hours = 1,
            Last_12_hours = 2,
            Last_24_hours = 3,
            Last_3_days = 4,
            Last_7_days = 5,
            No_Filter = 6,
        }

        public enum BuildDefinitionRetrievalMode
        {
            Unspecified = 0,
            Reset = 1,
            RetrieveRemaining = 2
        }

        public enum ReleaseDefinitionRetrievalMode
        {
            Unspecified = 0,
            Reset = 1,
            RetrieveRemaining = 2
        }

        public enum BuildDefinitionSettingsGroup
        {
            Unspecified = 0,
            DefaultAgentPoolQueue = 1,
            TriggersSchedule = 2,
            TriggersContinousIntegration = 3
        }

        public enum RepositoryType
        {
            [Description("Source build definition")]
            SourceBuildDefinitionRepositoryType = 0,
            [Description("Git")]
            Git,
            [Description("Team Foundation Version Control")]
            TeamFoundationVersionControl,
            [Description("GitHub")]
            GitHub,
            [Description("ExternalGit")]
            ExternalGit,
            [Description("Subversion")]
            Subversion

        }
    }
}
