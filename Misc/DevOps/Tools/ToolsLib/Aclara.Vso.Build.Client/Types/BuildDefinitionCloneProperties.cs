﻿using System;
using Microsoft.TeamFoundation.SourceControl.WebApi;
using static Aclara.Vso.Build.Client.Types.Enumerations;

namespace Aclara.Vso.Build.Client.Types
{
    public class BuildDefinitionCloneProperties
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private bool _donotCloneBuildDefinitions;
        private bool _cloneReleaseDefinitions;
        private bool _createMissingReleaseDefinitions;
        private bool _migrateBuildSteps;
        private RepositoryType _repositoryType;
        private GitRepository _gitRepository;
        private string buildDefinitionFolderName;
        private bool _migrateToGit;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Do not clone build definitions.
        /// </summary>
        public bool DoNotCloneBuildDefinitions
        {
            get { return _donotCloneBuildDefinitions; }
            set { _donotCloneBuildDefinitions = value; }
        }

        /// <summary>
        /// Property: Clone release definitions.
        /// </summary>
        public bool CloneReleaseDefinitions
        {
            get { return _cloneReleaseDefinitions; }
            set { _cloneReleaseDefinitions = value; }
        }

        /// <summary>
        /// Property: Create missing release definitions.
        /// </summary>
        public bool CreateMissingReleaseDefinitions
        {
            get { return _createMissingReleaseDefinitions; }
            set { _createMissingReleaseDefinitions = value; }
        }

        /// <summary>
        /// Property: Migrate deploy steps.
        /// </summary>
        public bool MigrateBuildSteps
        {
            get { return _migrateBuildSteps; }
            set { _migrateBuildSteps = value; }
        }

        /// <summary>
        /// Property: Repository type.
        /// </summary>
        public RepositoryType RepositoryType
        {
            get { return _repositoryType; }
            set { _repositoryType = value; }

        }

        /// <summary>
        /// Property: Repository id.
        /// </summary>
        public GitRepository GitRepository
        {
            get { return _gitRepository; }
            set { _gitRepository = value; }
        }

        /// <summary>
        /// Property: Build definition folder name.
        /// </summary>
        public string BuildDefinitionFolderName
        {
            get { return buildDefinitionFolderName; }
            set { buildDefinitionFolderName = value; }
        }

        /// <summary>
        /// Property: Migrate to git (from TFVC).
        /// </summary>
        public bool MigrateToGit
        {
            get { return _migrateToGit; }
            set { _migrateToGit = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildDefinitionCloneProperties()
        {

        }

        #endregion
        
        #region Public Methods
        #endregion
        
        #region Protected Methods
        #endregion
        
        #region Private Methods
        #endregion
    }
}
