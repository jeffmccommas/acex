﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vso.Build.Client
{
    /// <summary>
    /// Release definition manager factory.
    /// </summary>
    public static class ReleaseDefinitionManagerFactory
    {

        #region Private Constants

        #endregion

        #region Public Methods

        /// <summary>
        /// Create release definition manager.
        /// </summary>
        /// <param name="teamProjectCollectionUri"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="basicAuthRestAPIPassword"></param>
        /// <param name="basicAuthRestAPIUserProfileName"></param>
        /// <returns></returns>
        static public ReleaseDefinitionManager CreateReleaseDefinitionManager(Uri teamProjectCollectionUri,
                                                                              Uri teamProjectCollectionVsrmUri,
                                                                              string teamProjectName,
                                                                              string basicAuthRestAPIUserProfileName,
                                                                              string basicAuthRestAPIPassword)
        {
            ReleaseDefinitionManager result = null;
            ReleaseDefinitionManager releaseDefinitionManager = null;

            try
            {

                releaseDefinitionManager = new ReleaseDefinitionManager(teamProjectCollectionUri,
                                                                        teamProjectCollectionVsrmUri,
                                                                        teamProjectName,
                                                                        basicAuthRestAPIUserProfileName,
                                                                        basicAuthRestAPIPassword);

                result = releaseDefinitionManager;
                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion
    }
}
