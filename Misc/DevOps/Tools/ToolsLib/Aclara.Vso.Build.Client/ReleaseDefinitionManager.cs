﻿
using Aclara.Tools.Common.StatusManagement;
using Aclara.Vso.Build.Client.Events;
using Aclara.Vso.Build.Client.Exceptions;
using Aclara.Vso.Build.Client.Models;
using Aclara.Vso.Build.Client.Utilities;
using Microsoft.TeamFoundation.Build.WebApi;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using Microsoft.VisualStudio.Services.Common;
using Microsoft.VisualStudio.Services.ReleaseManagement.WebApi;
using Microsoft.VisualStudio.Services.ReleaseManagement.WebApi.Clients;
using Microsoft.VisualStudio.Services.ReleaseManagement.WebApi.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using static Aclara.Vso.Build.Client.Types.Enumerations;

using RMArtifact = Microsoft.VisualStudio.Services.ReleaseManagement.WebApi.Contracts.Artifact;

namespace Aclara.Vso.Build.Client
{

    /// <summary>
    /// Release definition manager.
    /// </summary>
    public class ReleaseDefinitionManager
    {

        #region Private Constants

        private const string Expand_All = "environments,artificats";
        private const string Expand_Artificats = "artificats";
        private const string Expand_Environments = "environments";
        private const string Expand_None = "none";

        private const string ArtifactSourceReference_ArtifactSourceDefinitionUrl = "artifactSourceDefinitionUrl";
        private const string ArtifactSourceReference_Definition = "definition";
        private const string ArtifactSourceReference_DefaultVersionType = "defaultVersionType";
        private const string ArtifactSourceReference_Project = "project";

        private const string Artifact_Type_Build = "Build";

        private const string Environment_Default = "default";
        private const string Environment_DEV = "dev";
        private const string Environment_QA = "qa";
        private const string Environment_UAT = "uat";
        private const string Environment_PERF = "perf";
        private const string Environment_PROD = "prod";
        private const string Environment_DEVDR = "devdr";
        private const string Environment_UATDR = "uatdr";
        private const string Environment_PRODDR = "proddr";

        private const string DeployPhase_Name_RunOnAgent = "Run on agent";

        #endregion

        #region Private Data Members

        private Uri _teamProjectCollectionUri;
        private Uri _teamProjectCollectionVsrmUri;
        private Guid _teamProjectCollectionInstanceId;
        private string _teamProjectName;
        private Guid _teamProjectId;

        private string _basicAuthRestApiUserProfileName;
        private string _basicAuthRestApiPassword;

        #endregion

        #region Public Delegates

        public event EventHandler<GetReleaseDefinitionListEventArgs> ReleaseDefinintionListRetrieved;
        public event EventHandler<CloneReleaseDefinitionEventArgs> ReleaseDefinitionCloned;
        public event EventHandler<DeleteReleaseDefinitionEventArgs> ReleaseDefinitionDeleted;

        #endregion

        #region Public properties

        /// <summary>
        /// Property: Team project collection uri.
        /// </summary>
        public Uri TeamProjectCollectionUri
        {
            get { return _teamProjectCollectionUri; }
            set
            {
                string teamProjectCollectionUriAsText = string.Empty;

                _teamProjectCollectionUri = value;

                teamProjectCollectionUriAsText = value.ToString();
                if (teamProjectCollectionUriAsText.EndsWith("/") == false)
                {
                    teamProjectCollectionUriAsText = value + @"/";
                    _teamProjectCollectionUri = new Uri(teamProjectCollectionUriAsText);
                }
            }
        }

        /// <summary>
        /// Property: Team project collection Vsrm uri.
        /// </summary>
        public Uri TeamProjectCollectionVsrmUri
        {
            get { return _teamProjectCollectionVsrmUri; }
            set
            {
                string teamProjectCollectionVsrmUriAsText = string.Empty;

                _teamProjectCollectionVsrmUri = value;

                teamProjectCollectionVsrmUriAsText = value.ToString();
                if (teamProjectCollectionVsrmUriAsText.EndsWith("/") == false)
                {
                    teamProjectCollectionVsrmUriAsText = value + @"/";
                    _teamProjectCollectionVsrmUri = new Uri(teamProjectCollectionVsrmUriAsText);
                }
            }
        }

        /// <summary>
        /// Property: Team project collection - instance id.
        /// </summary>
        public Guid TeamProjectCollectionInstanceId
        {
            get { return _teamProjectCollectionInstanceId; }
            set { _teamProjectCollectionInstanceId = value; }
        }

        /// <summary>
        /// Property: Team project name.
        /// </summary>
        public string TeamProjectName
        {
            get { return _teamProjectName; }
            set { _teamProjectName = value; }
        }

        /// <summary>
        /// Property: Team project id.
        /// </summary>
        public Guid TeamProjectId
        {
            get { return _teamProjectId; }
            set { _teamProjectId = value; }
        }

        /// <summary>
        /// Property: Basic authorization REST API user profile name.
        /// </summary>
        public string BasicAuthRestApiUserProfileName
        {
            get { return _basicAuthRestApiUserProfileName; }
            set { _basicAuthRestApiUserProfileName = value; }
        }

        /// <summary>
        /// Property: Basic authorization REST API password.
        /// </summary>
        public string BasicAuthRestApiPassword
        {
            get { return _basicAuthRestApiPassword; }
            set { _basicAuthRestApiPassword = value; }
        }


        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamProjectCollectionUri"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="basicAuthRestAPIUserProfileName"></param>
        /// <param name="basicAuthRestAPIPassword"></param>
        public ReleaseDefinitionManager(Uri teamProjectCollectionUri,
                                        Uri teamProjectCollectionVsrmUri,
                                        string teamProjectName,
                                        string basicAuthRestAPIUserProfileName,
                                        string basicAuthRestAPIPassword)
        {
            _teamProjectCollectionUri = teamProjectCollectionUri;
            _teamProjectCollectionVsrmUri = teamProjectCollectionVsrmUri;
            _teamProjectName = teamProjectName;
            _basicAuthRestApiUserProfileName = basicAuthRestAPIUserProfileName;
            _basicAuthRestApiPassword = basicAuthRestAPIPassword;
        }

        #endregion

        #region Private Constructors

        private ReleaseDefinitionManager()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve release definitions asynchronously via web api.
        /// </summary>
        public ReleaseDefinitionList GetDefinitionsRestApi(List<int> releaseDefinitionIDList)
        {

            ReleaseDefinitionList result = null;
            ReleaseDefinitionList releaseDefinitionList = null;
            ReleaseDefinition releaseDefinition = null;
            List<ReleaseDefinition> definitionReferences = null;
            string requestUrl = String.Empty;
            string parameter = string.Empty;

            try
            {

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUrl = string.Format("{0}DefaultCollection/{1}/_apis/release/definitions?api-version=3.0-preview.1",
                                               this.TeamProjectCollectionVsrmUri,
                                               this.TeamProjectName);
                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUrl).Result;

                    releaseDefinitionList = new ReleaseDefinitionList();


                    if (httpResponseMessage.IsSuccessStatusCode)
                    {

                        definitionReferences = httpResponseMessage.Content.ReadAsAsync<ReleaseDefinitionListContainer>().Result.value;

                        var filteredDefinitionList = from definitionReference in definitionReferences
                                                     where releaseDefinitionIDList.Any(s => s == definitionReference.Id)
                                                     select definitionReference;

                        //Retrieve deep definition references using shallow filtered definition references.
                        foreach (ReleaseDefinition releaseDefinition1 in filteredDefinitionList)
                        {
                            releaseDefinition = GetDefinitionRestApi(releaseDefinition1.Id);
                            releaseDefinitionList.Add(releaseDefinition);
                        }
                    }
                }

                result = releaseDefinitionList;
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        /// <summary>
        /// Get release definition list.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="alreadyRetrievedReleaseDefinitionList"></param>
        /// <param name="releaseDefinitionRetrievalMode"></param>
        /// <param name="cancellationToken"></param>
        public void GetReleaseDefinitionList(string releaseDefinitionNamePattern,
                                             ReleaseDefinitionList alreadyRetrievedReleaseDefinitionList,
                                             ReleaseDefinitionRetrievalMode releaseDefinitionRetrievalMode,
                                             CancellationToken cancellationToken)
        {
            GetReleaseDefinitionListEventArgs getReleaseDefinitionListEventArgs = null;
            ReleaseDefinitionList releaseDefinitionList = null;
            List<ReleaseDefinition> releaseDefinitions = null;
            IEnumerable<ReleaseDefinition> filteredReleaseDefinitions = null;
            int releaseDefinitionListTotalCount = 0;
            int releaseDefinitionListRetrievedCount = 0;
            string requestUrl = String.Empty;
            string parameter = string.Empty;

            try
            {

                using (var httpClient = new HttpClient())
                {

                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    releaseDefinitionList = new ReleaseDefinitionList();

                    httpClient.BaseAddress = this.TeamProjectCollectionVsrmUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUrl = string.Format("{0}DefaultCollection/{1}/_apis/release/definitions?api-version=3.0-preview.1",
                                               this.TeamProjectCollectionVsrmUri,
                                               this.TeamProjectName);

                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUrl).Result;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {

                        releaseDefinitions = httpResponseMessage.Content.ReadAsAsync<ReleaseDefinitionListContainer>().Result.value;

                        filteredReleaseDefinitions = Wildcard.FilterListByNamePropertyAndPattern(releaseDefinitions, releaseDefinitionNamePattern, false);

                        releaseDefinitionListTotalCount = filteredReleaseDefinitions.Count();

                        foreach (ReleaseDefinition releaseDefinition in filteredReleaseDefinitions)
                        {
                            //Check for cancellation.
                            cancellationToken.ThrowIfCancellationRequested();

                            releaseDefinitionList.Add(releaseDefinition);
                            releaseDefinitionListRetrievedCount++;

                            getReleaseDefinitionListEventArgs = new GetReleaseDefinitionListEventArgs();

                            getReleaseDefinitionListEventArgs.ReleaseDefinitionTotalCount = releaseDefinitionListTotalCount;
                            getReleaseDefinitionListEventArgs.ReleaseDefinitionRetrievedCount = releaseDefinitionListRetrievedCount;
                            getReleaseDefinitionListEventArgs.ReleaseDefinitionList = releaseDefinitionList;
                            getReleaseDefinitionListEventArgs.ReleaseDefinitionRetrievalMode = releaseDefinitionRetrievalMode;

                            if (ReleaseDefinintionListRetrieved != null)
                            {
                                ReleaseDefinintionListRetrieved(this, getReleaseDefinitionListEventArgs);
                            }

                        }
                    }

                }

            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Get release definition.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="expand"></param>
        /// <returns></returns>
        public ReleaseDefinition GetReleaseDefinition(string name, string expand = "none")
        {
            ReleaseDefinition result = null;

            ReleaseDefinition releaseDefinition = null;

            GetReleaseDefinitionListEventArgs getReleaseDefinitionListEventArgs = null;
            ReleaseDefinitionList releaseDefinitionList = null;
            List<ReleaseDefinition> releaseDefinitions = null;
            IEnumerable<ReleaseDefinition> filteredReleaseDefinitions = null;
            int releaseDefinitionListTotalCount = 0;
            int releaseDefinitionListRetrievedCount = 0;
            string requestUrl = String.Empty;
            string parameter = string.Empty;

            try
            {

                using (var httpClient = new HttpClient())
                {

                    releaseDefinitionList = new ReleaseDefinitionList();

                    httpClient.BaseAddress = this.TeamProjectCollectionVsrmUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUrl = string.Format("{0}DefaultCollection/{1}/_apis/release/definitions?api-version=3.0-preview.1",
                                               this.TeamProjectCollectionVsrmUri,
                                               this.TeamProjectName);

                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUrl).Result;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {

                        releaseDefinitions = httpResponseMessage.Content.ReadAsAsync<ReleaseDefinitionListContainer>().Result.value;

                        filteredReleaseDefinitions = Wildcard.FilterListByNamePropertyAndPattern(releaseDefinitions, name, false);

                        releaseDefinitionListTotalCount = filteredReleaseDefinitions.Count();

                        releaseDefinition = filteredReleaseDefinitions.SingleOrDefault();

                        if (releaseDefinition != null)
                        {
                            result = this.GetDefinitionRestApi(releaseDefinition.Id, expand);
                        }
                    }

                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve release definitions asynchronously via web api.
        /// </summary>
        /// <param name="releaseDefinitionId"></param>
        /// <param name="expand"></param>
        /// <returns></returns>
        public ReleaseDefinition GetDefinitionRestApi(int releaseDefinitionId, string expand = "none")
        {

            ReleaseDefinition result = null;

            ReleaseDefinition releaseDefinition = null;
            string requestUrl = String.Empty;
            string parameter = string.Empty;

            try
            {

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUrl = string.Format("{0}DefaultCollection/{1}/_apis/release/definitions/{2}?$expand={3}&api-version=3.0-preview.1",
                                               this.TeamProjectCollectionVsrmUri,
                                               this.TeamProjectName,
                                               releaseDefinitionId,
                                               expand);
                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUrl).Result;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {

                        releaseDefinition = httpResponseMessage.Content.ReadAsAsync<ReleaseDefinition>().Result;
                    }
                }
                result = releaseDefinition;

            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        /// <summary>
        /// Clone release definitions.
        /// </summary>
        /// <param name="releaseDefinitionList"></param>
        /// <param name="sourceReleaseDefinitionNamePrefix"></param>
        /// <param name="targetReleaseDefinitionNamePrefix"></param>
        /// <param name="sourceBranchName"></param>
        /// <param name="targetBranchName"></param>
        /// <param name="cancellationToken"></param>
        public void CloneReleaseDefinitions(ReleaseDefinitionList releaseDefinitionList,
                                            string sourceReleaseDefinitionNamePrefix,
                                            string targetReleaseDefinitionNamePrefix,
                                            string sourceBranchName,
                                            string targetBranchName,
                                            CancellationToken cancellationToken)
        {
            List<int> filteredReleaseDefinitionIdList = null;
            CloneReleaseDefinitionEventArgs cloneReleaseDefinitionEventArgs = null;
            ReleaseDefinitionList refreshedReleaseDefinitionList = null;
            ReleaseDefinition targetReleaseDefinition = null;
            string expand = string.Empty;
            int releaseDefinitionClonedCount = 0;

            try
            {

                this.PopulateTeamProjectInfo();

                //Reterieve fresh release definition list. (In case release definitions have changed since last retrieval).
                filteredReleaseDefinitionIdList = new List<int>();
                foreach (ReleaseDefinition releaseDefinition in releaseDefinitionList)
                {
                    filteredReleaseDefinitionIdList.Add(releaseDefinition.Id);
                }
                refreshedReleaseDefinitionList = this.GetDefinitionsRestApi(filteredReleaseDefinitionIdList);

                foreach (ReleaseDefinition releaseDefinition in refreshedReleaseDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    //Retieve "deep" source release definition.
                    var sourceReleaseDefinition = this.GetDefinitionRestApi(releaseDefinition.Id, Expand_All);

                    //Clone release definition.
                    cloneReleaseDefinitionEventArgs  = this.CloneReleaseDefinition(sourceReleaseDefinition,
                                                                                   sourceReleaseDefinitionNamePrefix,
                                                                                   targetReleaseDefinitionNamePrefix,
                                                                                   sourceBranchName,
                                                                                   targetBranchName,
                                                                                   ref targetReleaseDefinition);

                    cloneReleaseDefinitionEventArgs.ReleaseDefinitionTotalCount = releaseDefinitionList.Count;
                    cloneReleaseDefinitionEventArgs.ReleaseDefinitionClonedCount = ++releaseDefinitionClonedCount;

                    if (ReleaseDefinitionCloned != null)
                    {
                        ReleaseDefinitionCloned(this, cloneReleaseDefinitionEventArgs);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Clone release definition.
        /// </summary>
        /// <param name="sourceReleaseDefinition"></param>
        /// <param name="sourceReleaseDefinitionNamePrefix"></param>
        /// <param name="targetReleaseDefinitionNamePrefix"></param>
        /// <param name="sourceBranchName"></param>
        /// <param name="targetBranchName"></param>
        /// <param name="targetBuildDefinition"></param>
        /// <param name="targetReleaseDefinition"></param>
        /// <param name="statusList"></param>
        public CloneReleaseDefinitionEventArgs CloneReleaseDefinition(ReleaseDefinition sourceReleaseDefinition,
                                           string sourceReleaseDefinitionNamePrefix,
                                           string targetReleaseDefinitionNamePrefix,
                                           string sourceBranchName,
                                           string targetBranchName,
                                           ref ReleaseDefinition targetReleaseDefinition)
        {
            CloneReleaseDefinitionEventArgs result = null;

            try
            {
                result = new CloneReleaseDefinitionEventArgs();

                //TODO:
                targetReleaseDefinition = null;

                // Create new release definition from another (returns new release definition).
                targetReleaseDefinition = CreateReleaseDefinitionFromAnother(sourceReleaseDefinition,
                                                                             sourceReleaseDefinitionNamePrefix,
                                                                             targetReleaseDefinitionNamePrefix,
                                                                             sourceBranchName,
                                                                             targetBranchName);
                //Save target release definition.
                UpdateDefinition(targetReleaseDefinition);

                result.SourceReleaseDefinition = sourceReleaseDefinition;
                result.TargetReleaseDefinition = targetReleaseDefinition;

            }
            catch (Exception ex)
            {
                result = new CloneReleaseDefinitionEventArgs();

                result.SourceReleaseDefinition = sourceReleaseDefinition;
                result.TargetReleaseDefinition = targetReleaseDefinition;

                Status status = null;

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        /// <summary>
        /// Create release definition from build definition.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="sourceReleaseDefinition"></param>
        /// <param name="sourceReleaseDefinitionNamePrefix"></param>
        /// <param name="targetReleaseDefinitionNamePrefix"></param>
        /// <param name="sourceBranchName"></param>
        /// <param name="targetBranchName"></param>
        /// <param name="migrateBuildSteps"></param>
        /// <param name="statusList"></param>
        public ReleaseDefinition CreateReleaseDefinitionFromBuildDefinition(BuildDefinition buildDefinition,
                                                               string sourceReleaseDefinitionNamePrefix,
                                                               string targetReleaseDefinitionNamePrefix,
                                                               string sourceBranchName,
                                                               string targetBranchName,
                                                               bool migrateBuildSteps,
                                                               StatusList statusList)
        {
            ReleaseDefinition result = null;
            try
            {
                result = null;

                this.PopulateTeamProjectInfo();

                // Create new release definition from another (returns new release definition).
                result = CreateReleaseDefinitionFromBuildDefinition(buildDefinition,
                                                                    sourceReleaseDefinitionNamePrefix,
                                                                    targetReleaseDefinitionNamePrefix,
                                                                    sourceBranchName,
                                                                    targetBranchName,
                                                                    migrateBuildSteps);
                //Save target release definition.
                UpdateDefinition(result);

            }
            catch (Exception ex)
            {
                Status status = null;

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                statusList.Add(status);
            }

            return result;
        }

        /// <summary>
        /// Delete release definition asynchronously via web api.
        /// </summary>
        /// <param name="definitionId"></param>
        public void DeleteReleaseDefinitionRestApi(int definitionId)
        {

            string parameter = string.Empty;

            try
            {

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    HttpResponseMessage httpResponseMessage = httpClient.DeleteAsync(string.Format("{0}DefaultCollection/{1}/_apis/release/definitions/{2}?api-version=3.0-preview.1",
                                                                                                   this.TeamProjectCollectionVsrmUri,
                                                                                                   this.TeamProjectName,
                                                                                                   definitionId)).Result;

                    //Throw on error.
                    httpResponseMessage.EnsureSuccessStatusCode();

                }

            }
            catch (Exception)
            {

                throw;
            }
            return;
        }

        /// <summary>
        /// Delete release definitions.
        /// </summary>
        /// <param name="releaseDefinitionList"></param>
        /// <param name="cancellationToken"></param>
        public void DeleteReleaseDefinitions(ReleaseDefinitionList releaseDefinitionList,
                                             CancellationToken cancellationToken)
        {
            DeleteReleaseDefinitionEventArgs deleteReleaseDefinitionEventArgs = null;
            int releaseDefinitionDeletedCount = 0;

            try
            {
                foreach (ReleaseDefinition releaseDefinition in releaseDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    //Delete release definition.
                    deleteReleaseDefinitionEventArgs = DeleteReleaseDefinition(releaseDefinition);

                    deleteReleaseDefinitionEventArgs.ReleaseDefinitionTotalCount = releaseDefinitionList.Count;
                    deleteReleaseDefinitionEventArgs.ReleaseDefinitionDeletedCount = ++releaseDefinitionDeletedCount;        

                    if (ReleaseDefinitionDeleted != null)
                    {
                        ReleaseDefinitionDeleted(this, deleteReleaseDefinitionEventArgs);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Delete release definition.
        /// </summary>
        /// <param name="releaseDefinition"></param>
        /// <returns></returns>
        public DeleteReleaseDefinitionEventArgs DeleteReleaseDefinition(ReleaseDefinition releaseDefinition)
        {
            DeleteReleaseDefinitionEventArgs result = null;

            try
            {
                result = new DeleteReleaseDefinitionEventArgs();

                result.ReleaseDefinition = releaseDefinition;

                DeleteReleaseDefinitionRestApi(releaseDefinition.Id);

            }
            catch (Exception ex)
            {
                Status status = null;

                result = new DeleteReleaseDefinitionEventArgs();
                result.ReleaseDefinition = releaseDefinition;
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create release definition asynchronously.
        /// </summary>
        /// <param name="targetReleaseDefinition"></param>
        protected ReleaseDefinition CreateDefinition(ReleaseDefinition targetReleaseDefinition)
        {

            ReleaseDefinition result = null;
            ReleaseHttpClient releaseHttpClient = null;
            VssCredentials vssCredentials = null;
            string userState = string.Empty;
            Task<ReleaseDefinition> createDefinitionTask = null;

            try
            {

                vssCredentials = new VssCredentials();
                vssCredentials = new VssCredentials(new Microsoft.VisualStudio.Services.Common.WindowsCredential(new NetworkCredential(this.BasicAuthRestApiUserProfileName, this.BasicAuthRestApiPassword)));

                releaseHttpClient = new ReleaseHttpClient(this.TeamProjectCollectionVsrmUri, vssCredentials);

                //Create release definition.
                createDefinitionTask = releaseHttpClient.CreateReleaseDefinitionAsync(targetReleaseDefinition, this.TeamProjectName, userState, default(CancellationToken));

                createDefinitionTask.Wait();
                result = createDefinitionTask.Result;

                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Create release definition asynchronously.
        /// </summary>
        /// <param name="releaseDefinition"></param>
        protected ReleaseDefinition UpdateDefinition(ReleaseDefinition releaseDefinition)
        {

            ReleaseDefinition result = null;
            ReleaseHttpClient releaseHttpClient = null;
            VssCredentials vssCredentials = null;
            string userState = string.Empty;
            Task<ReleaseDefinition> createDefinitionTask = null;

            try
            {

                vssCredentials = new VssCredentials();
                vssCredentials = new VssCredentials(new Microsoft.VisualStudio.Services.Common.WindowsCredential(new NetworkCredential(this.BasicAuthRestApiUserProfileName, this.BasicAuthRestApiPassword)));

                releaseHttpClient = new ReleaseHttpClient(this.TeamProjectCollectionVsrmUri, vssCredentials);

                //Create release definition.
                createDefinitionTask = releaseHttpClient.UpdateReleaseDefinitionAsync(releaseDefinition, this.TeamProjectName, userState, default(CancellationToken));

                createDefinitionTask.Wait();
                result = createDefinitionTask.Result;

                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Create release definition from another.
        /// </summary>
        /// <param name="sourceReleaseDefinition"></param>
        /// <param name="sourceReleaseDefinitionNamePrefix"></param>
        /// <param name="targetReleaseDefinitionNamePrefix"></param>
        /// <param name="sourceBranchName"></param>
        /// <param name="targetBranchName"></param>
        /// <param name="repositoryType"></param>
        /// <returns></returns>
        protected ReleaseDefinition CreateReleaseDefinitionFromAnother(ReleaseDefinition sourceReleaseDefinition,
                                                                       string sourceReleaseDefinitionNamePrefix,
                                                                       string targetReleaseDefinitionNamePrefix,
                                                                       string sourceBranchName,
                                                                       string targetBranchName)
        {
            ReleaseDefinition result = null;
            ReleaseDefinition targetReleaseDefinition = null;

            try
            {

                targetReleaseDefinition = new ReleaseDefinition();

                targetReleaseDefinition.Name = ReplaceTokenInTextWithValue(sourceReleaseDefinition.Name,
                                                                           sourceReleaseDefinitionNamePrefix,
                                                                           targetReleaseDefinitionNamePrefix);

                targetReleaseDefinition.Comment = sourceReleaseDefinition.Comment;
                targetReleaseDefinition.Links = sourceReleaseDefinition.Links;
                targetReleaseDefinition.ReleaseNameFormat = sourceReleaseDefinition.ReleaseNameFormat;
                //DO NOT SET: targetReleaseDefinition.Url = sourceReleaseDefinition.Url;

                targetReleaseDefinition.Artifacts = new List<Microsoft.VisualStudio.Services.ReleaseManagement.WebApi.Contracts.Artifact>();
                foreach (Microsoft.VisualStudio.Services.ReleaseManagement.WebApi.Contracts.Artifact artifact in sourceReleaseDefinition.Artifacts)
                {
                    targetReleaseDefinition.Artifacts.Add(artifact);
                }
                this.UpdateArtifactDefinitionReferences(targetReleaseDefinition.Name, targetReleaseDefinition.Artifacts);

                targetReleaseDefinition.Environments = new List<ReleaseDefinitionEnvironment>();
                foreach (ReleaseDefinitionEnvironment environment in sourceReleaseDefinition.Environments)
                {
                    targetReleaseDefinition.Environments.Add(environment);
                }
                targetReleaseDefinition.VariableGroups = new List<int>();
                foreach (int variableGroup in sourceReleaseDefinition.VariableGroups)
                {
                    targetReleaseDefinition.VariableGroups.Add(variableGroup);
                }
                targetReleaseDefinition.Tags = new List<string>();
                foreach (string tag in sourceReleaseDefinition.Tags)
                {
                    targetReleaseDefinition.Tags.Add(tag);
                }
                targetReleaseDefinition.Variables = new Dictionary<string, ConfigurationVariableValue>();
                foreach (KeyValuePair<string, ConfigurationVariableValue> variable in sourceReleaseDefinition.Variables)
                {
                    targetReleaseDefinition.Variables.Add(variable.Key, variable.Value);
                }
                ////DO NOT SET: targetReleaseDefinition.LastRelease = sourceReleaseDefinition.LastRelease;
                ////DO NOT SET: targetReleaseDefinition.ModifiedOn = sourceReleaseDefinition.ModifiedOn;
                ////DO NOT SET: targetReleaseDefinition.ModifiedBy = sourceReleaseDefinition.ModifiedBy;
                ////DO NOT SET: targetReleaseDefinition.CreatedOn = sourceReleaseDefinition.CreatedOn;
                ////DO NOT SET: targetReleaseDefinition.CreatedBy = sourceReleaseDefinition.CreatedBy;
                targetReleaseDefinition.Revision = 1;
                ////DO NOT SET: targetReleaseDefinition.Id = sourceReleaseDefinition.Id;
                targetReleaseDefinition.Source = ReleaseDefinitionSource.RestApi;
                ////DO NOT SET: targetReleaseDefinition.Path = sourceReleaseDefinition.Path;

                targetReleaseDefinition.Triggers = new List<ReleaseTriggerBase>();
                foreach (ReleaseTriggerBase releaseTriggerBase in sourceReleaseDefinition.Triggers)
                {
                    targetReleaseDefinition.Triggers.Add(releaseTriggerBase);
                }
                //Properties:
                foreach (KeyValuePair<string, object> property in sourceReleaseDefinition.Properties)
                {
                    targetReleaseDefinition.Properties.Add(property.Key, property.Value);
                }

                //Create target build definition.
                targetReleaseDefinition = CreateDefinition(targetReleaseDefinition);

                //Replace text in target release definition.
                this.ReplaceTextInReleaseDefinition(ref targetReleaseDefinition,
                                                    sourceReleaseDefinitionNamePrefix,
                                                    targetReleaseDefinitionNamePrefix,
                                                    sourceBranchName,
                                                    targetBranchName);

                result = targetReleaseDefinition;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create release definition from build definition.
        /// </summary>
        /// <param name="targetReleaseDefinition"></param>
        /// <param name="sourceReleaseDefinitionNamePrefix"></param>
        /// <param name="targetReleaseDefinitionNamePrefix"></param>
        /// <param name="sourceBranchName"></param>
        /// <param name="targetBranchName"></param>
        /// <param name="repositoryType"></param>
        /// <returns></returns>
        protected ReleaseDefinition CreateReleaseDefinitionFromBuildDefinition(BuildDefinition buildDefinition,
                                                                               string sourceReleaseDefinitionNamePrefix,
                                                                               string targetReleaseDefinitionNamePrefix,
                                                                               string sourceBranchName,
                                                                               string targetBranchName,
                                                                               bool migrateBuildSteps)
        {
            const int Default_DaysToKeep = 30;
            const int Default_ReleasesToKeep = 3;
            const int Default_Rank = 1;
            const int Default_Revision = 1;

            ReleaseDefinition result = null;
            ReleaseDefinition targetReleaseDefinition = null;
            string environmentName = string.Empty;
            ReleaseDefinitionEnvironment releaseDefinitionEnvironment = null;
            ReleaseDefinitionApprovalStep releaseDefinitionApprovalStep = null;
            WorkflowTask workflowTask = null;
            AgentBasedDeployPhase deployPhase = null;
            ConfigurationVariableValue configurationVariable = null;

            try
            {

                targetReleaseDefinition = new ReleaseDefinition();

                targetReleaseDefinition.Name = ReplaceTokenInTextWithValue(buildDefinition.Name,
                                                                           sourceReleaseDefinitionNamePrefix,
                                                                           targetReleaseDefinitionNamePrefix);

                targetReleaseDefinition.Artifacts = this.CreateArtifactDefinitionReferences(buildDefinition);

                targetReleaseDefinition.Environments = new List<ReleaseDefinitionEnvironment>();
                releaseDefinitionEnvironment = new ReleaseDefinitionEnvironment();
                releaseDefinitionEnvironment.Name = this.GetEnvironmentFromName(buildDefinition.Name);

                releaseDefinitionEnvironment.RetentionPolicy = new EnvironmentRetentionPolicy();
                releaseDefinitionEnvironment.RetentionPolicy.DaysToKeep = Default_DaysToKeep;
                releaseDefinitionEnvironment.RetentionPolicy.ReleasesToKeep = Default_ReleasesToKeep;
                releaseDefinitionEnvironment.RetentionPolicy.RetainBuild = true;

                releaseDefinitionEnvironment.PreDeployApprovals = new ReleaseDefinitionApprovals();
                releaseDefinitionApprovalStep = new ReleaseDefinitionApprovalStep();
                releaseDefinitionApprovalStep.IsAutomated = true;
                releaseDefinitionApprovalStep.Rank = Default_Rank;
                releaseDefinitionApprovalStep.IsNotificationOn = false;
                releaseDefinitionEnvironment.PreDeployApprovals.Approvals.Add(releaseDefinitionApprovalStep);

                releaseDefinitionEnvironment.PostDeployApprovals = new ReleaseDefinitionApprovals();
                releaseDefinitionApprovalStep = new ReleaseDefinitionApprovalStep();
                releaseDefinitionApprovalStep.IsAutomated = true;
                releaseDefinitionApprovalStep.Rank = Default_Rank;
                releaseDefinitionApprovalStep.IsNotificationOn = false;
                releaseDefinitionEnvironment.PostDeployApprovals.Approvals.Add(releaseDefinitionApprovalStep);

                releaseDefinitionEnvironment.DeployStep = new ReleaseDefinitionDeployStep();

                releaseDefinitionEnvironment.DeployPhases = new List<DeployPhase>();

                deployPhase = new AgentBasedDeployPhase();
                deployPhase.Name = DeployPhase_Name_RunOnAgent;
                deployPhase.Rank = Default_Rank;
                deployPhase.DeploymentInput.QueueId = buildDefinition.Queue.Id;
                deployPhase.WorkflowTasks = new List<WorkflowTask>();

                if (migrateBuildSteps == true)
                {
                    foreach (BuildDefinitionStep buildDefinitionStep in buildDefinition.Steps)
                    {
                        workflowTask = new WorkflowTask();

                        workflowTask.TaskId = buildDefinitionStep.TaskDefinition.Id;
                        workflowTask.Version = buildDefinitionStep.TaskDefinition.VersionSpec;
                        workflowTask.Name = buildDefinitionStep.DisplayName;
                        foreach (KeyValuePair<string, string> keyValuePair in buildDefinitionStep.Inputs)
                        {
                            workflowTask.Inputs.Add(keyValuePair.Key, keyValuePair.Value);
                        }
                        workflowTask.TimeoutInMinutes = buildDefinitionStep.TimeoutInMinutes;
                        workflowTask.Enabled = false;

                        deployPhase.WorkflowTasks.Add(workflowTask);
                    }

                }                releaseDefinitionEnvironment.DeployPhases.Add(deployPhase);
                targetReleaseDefinition.Environments.Add(releaseDefinitionEnvironment);

                //Create target build definition.
                targetReleaseDefinition = CreateDefinition(targetReleaseDefinition);



                //DO NOT SET: targetReleaseDefinition.Comment = sourceReleaseDefinition.Comment;
                //DO NOT SET: targetReleaseDefinition.Links = sourceReleaseDefinition.Links;
                //DO NOT SET: targetReleaseDefinition.ReleaseNameFormat = sourceReleaseDefinition.ReleaseNameFormat;
                //DO NOT SET: targetReleaseDefinition.Url = sourceReleaseDefinition.Url;

                //DO NOT SET: targetReleaseDefinition.Artifacts = new List<Microsoft.VisualStudio.Services.ReleaseManagement.WebApi.Contracts.Artifact>();
                //DO NOT SET: foreach (Microsoft.VisualStudio.Services.ReleaseManagement.WebApi.Contracts.Artifact artifact in sourceReleaseDefinition.Artifacts)
                //DO NOT SET: {
                //DO NOT SET:   targetReleaseDefinition.Artifacts.Add(artifact);
                //DO NOT SET: }
                //DO NOT SET: this.UpdateArtifactDefinitionReferences(targetReleaseDefinition.Name, buildDefinition, targetReleaseDefinition.Artifacts);

                //DO NOT SET: targetReleaseDefinition.VariableGroups = new List<int>();
                //DO NOT SET: foreach (int variableGroup in sourceReleaseDefinition.VariableGroups)
                //DO NOT SET: {
                //DO NOT SET:   targetReleaseDefinition.VariableGroups.Add(variableGroup);
                //DO NOT SET: }
                //DO NOT SET: targetReleaseDefinition.Tags = new List<string>();
                //DO NOT SET: foreach (string tag in sourceReleaseDefinition.Tags)
                //DO NOT SET: {
                //DO NOT SET:   targetReleaseDefinition.Tags.Add(tag);
                //DO NOT SET: }
                targetReleaseDefinition.Variables = new Dictionary<string, ConfigurationVariableValue>();
                foreach (KeyValuePair<string, BuildDefinitionVariable> buildDefinitionVariable in buildDefinition.Variables)
                {

                    configurationVariable = new ConfigurationVariableValue();
                    configurationVariable.Value = buildDefinitionVariable.Value.Value;

                    targetReleaseDefinition.Variables.Add(buildDefinitionVariable.Key, configurationVariable);
                }
                ////DO NOT SET: targetReleaseDefinition.LastRelease = sourceReleaseDefinition.LastRelease;
                ////DO NOT SET: targetReleaseDefinition.ModifiedOn = sourceReleaseDefinition.ModifiedOn;
                ////DO NOT SET: targetReleaseDefinition.ModifiedBy = sourceReleaseDefinition.ModifiedBy;
                ////DO NOT SET: targetReleaseDefinition.CreatedOn = sourceReleaseDefinition.CreatedOn;
                ////DO NOT SET: targetReleaseDefinition.CreatedBy = sourceReleaseDefinition.CreatedBy;
                targetReleaseDefinition.Revision = Default_Revision;
                ////DO NOT SET: targetReleaseDefinition.Id = sourceReleaseDefinition.Id;
                targetReleaseDefinition.Source = ReleaseDefinitionSource.RestApi;
                ////DO NOT SET: targetReleaseDefinition.Path = sourceReleaseDefinition.Path;

                targetReleaseDefinition.Triggers = new List<ReleaseTriggerBase>();



                //DO NOT SET: foreach (ReleaseTriggerBase releaseTriggerBase in sourceReleaseDefinition.Triggers)
                //DO NOT SET: {
                //DO NOT SET:   targetReleaseDefinition.Triggers.Add(releaseTriggerBase);
                //DO NOT SET: }
                //Properties:
                //DO NOT SET: foreach (KeyValuePair<string, object> property in sourceReleaseDefinition.Properties)
                //DO NOT SET: {
                //DO NOT SET: targetReleaseDefinition.Properties.Add(property.Key, property.Value);
                //DO NOT SET: }


                //DO NOT SET: //Replace text in target release definition.
                //DO NOT SET: this.ReplaceTextInReleaseDefinition(ref targetReleaseDefinition,
                //DO NOT SET:                                     sourceReleaseDefinitionNamePrefix,
                //DO NOT SET:                                     targetReleaseDefinitionNamePrefix,
                //DO NOT SET:                                     sourceBranchName,
                //DO NOT SET:                                     targetBranchName);

                result = targetReleaseDefinition;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        ///// <summary>
        ///// Create release definition from build definition.
        ///// </summary>
        ///// <param name="sourceBuildDefinitionDefinition"></param>
        ///// <param name="sourceReleaseDefinitionNamePrefix"></param>
        ///// <param name="targetReleaseDefinitionNamePrefix"></param>
        ///// <param name="sourceBranchName"></param>
        ///// <param name="targetBranchName"></param>
        ///// <param name="repositoryType"></param>
        ///// <returns></returns>
        //protected ReleaseDefinition CreateReleaseDefinitionFromBuildDefinition(BuildDefinition sourceBuildDefinitionDefinition,
        //                                                                       string sourceReleaseDefinitionNamePrefix,
        //                                                                       string targetReleaseDefinitionNamePrefix,
        //                                                                       string sourceBranchName,
        //                                                                       string targetBranchName)
        //{
        //    ReleaseDefinition result = null;
        //    ReleaseDefinition targetReleaseDefinition = null;

        //    try
        //    {

        //        targetReleaseDefinition = new ReleaseDefinition();

        //        targetReleaseDefinition.Name = ReplaceTokenInTextWithValue(sourceBuildDefinitionDefinition.Name,
        //                                                                   sourceReleaseDefinitionNamePrefix,
        //                                                                   targetReleaseDefinitionNamePrefix);

        //        targetReleaseDefinition.Comment = sourceBuildDefinitionDefinition.Comment;
        //        targetReleaseDefinition.Links = sourceBuildDefinitionDefinition.Links;
        //        //targetReleaseDefinition.ReleaseNameFormat = sourceBuildDefinitionDefinition.ReleaseNameFormat;
        //        //DO NOT SET: targetReleaseDefinition.Url = sourceReleaseDefinition.Url;

        //        targetReleaseDefinition.Artifacts = new List<Microsoft.VisualStudio.Services.ReleaseManagement.WebApi.Contracts.Artifact>();
        //        foreach (Microsoft.VisualStudio.Services.ReleaseManagement.WebApi.Contracts.Artifact artifact in sourceBuildDefinitionDefinition.Artifacts)
        //        {
        //            targetReleaseDefinition.Artifacts.Add(artifact);
        //        }
        //        this.UpdateArtifactDefinitionReferences(targetReleaseDefinition.Name, sourceBuildDefinitionDefinition, targetReleaseDefinition.Artifacts);

        //        targetReleaseDefinition.Environments = new List<ReleaseDefinitionEnvironment>();
        //        //foreach (ReleaseDefinitionEnvironment environment in sourceBuildDefinitionDefinition.Environments)
        //        //{
        //        //    targetReleaseDefinition.Environments.Add(environment);
        //        //}
        //        targetReleaseDefinition.VariableGroups = new List<int>();
        //        //foreach (int variableGroup in sourceBuildDefinitionDefinition.VariableGroups)
        //        //{
        //        //    targetReleaseDefinition.VariableGroups.Add(variableGroup);
        //        //}
        //        targetReleaseDefinition.Tags = new List<string>();
        //        foreach (string tag in sourceBuildDefinitionDefinition.Tags)
        //        {
        //            targetReleaseDefinition.Tags.Add(tag);
        //        }
        //        targetReleaseDefinition.Variables = new Dictionary<string, ConfigurationVariableValue>();
        //        //foreach (KeyValuePair<string, ConfigurationVariableValue> variable in sourceBuildDefinitionDefinition.Variables)
        //        //{
        //        //    targetReleaseDefinition.Variables.Add(variable.Key, variable.Value);
        //        //}
        //        ////DO NOT SET: targetReleaseDefinition.LastRelease = sourceReleaseDefinition.LastRelease;
        //        ////DO NOT SET: targetReleaseDefinition.ModifiedOn = sourceReleaseDefinition.ModifiedOn;
        //        ////DO NOT SET: targetReleaseDefinition.ModifiedBy = sourceReleaseDefinition.ModifiedBy;
        //        ////DO NOT SET: targetReleaseDefinition.CreatedOn = sourceReleaseDefinition.CreatedOn;
        //        ////DO NOT SET: targetReleaseDefinition.CreatedBy = sourceReleaseDefinition.CreatedBy;
        //        targetReleaseDefinition.Revision = 1;
        //        ////DO NOT SET: targetReleaseDefinition.Id = sourceReleaseDefinition.Id;
        //        targetReleaseDefinition.Source = ReleaseDefinitionSource.RestApi;
        //        ////DO NOT SET: targetReleaseDefinition.Path = sourceReleaseDefinition.Path;

        //        targetReleaseDefinition.Triggers = new List<ReleaseTriggerBase>();
        //        //foreach (ReleaseTriggerBase releaseTriggerBase in sourceBuildDefinitionDefinition.Triggers)
        //        //{
        //        //    targetReleaseDefinition.Triggers.Add(releaseTriggerBase);
        //        //}
        //        //Properties:
        //        foreach (KeyValuePair<string, object> property in sourceBuildDefinitionDefinition.Properties)
        //        {
        //            targetReleaseDefinition.Properties.Add(property.Key, property.Value);
        //        }

        //        //Create target build definition.
        //        targetReleaseDefinition = CreateDefinition(targetReleaseDefinition);

        //        //Replace text in target release definition.
        //        this.ReplaceTextInReleaseDefinition(ref targetReleaseDefinition,
        //                                            sourceReleaseDefinitionNamePrefix,
        //                                            targetReleaseDefinitionNamePrefix,
        //                                            sourceBranchName,
        //                                            targetBranchName);

        //        result = targetReleaseDefinition;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }

        //    return result;
        //}

        /// <summary>
        /// Create artifact definition references.
        /// </summary>
        /// <remarks>
        /// It is assumed that build definitions and releases definition have the same exact names. When an artifact contains a
        /// reference to a build definition then the build definition will have the exact same name as the release definition.
        /// </remarks>
        /// <param name="buildDefinition"></param>
        /// <returns></returns>
        protected List<RMArtifact> CreateArtifactDefinitionReferences(BuildDefinition buildDefinition)
        {
            List<RMArtifact> result = null;
            RMArtifact rmArtifact = null;
            ArtifactSourceReference artifactSourceReference = null;
            List<string> artifactSourceReferenceTypeList = null;

            try
            {
                result = new List<RMArtifact>();

                artifactSourceReferenceTypeList = new List<string>(){ ArtifactSourceReference_ArtifactSourceDefinitionUrl,
                                                                      ArtifactSourceReference_Definition,
                                                                      ArtifactSourceReference_DefaultVersionType,
                                                                      ArtifactSourceReference_Project};

                rmArtifact = new RMArtifact();
                rmArtifact.Type = Artifact_Type_Build;
                //rmArtifact.SourceId = string.Format("{0}:{1}",
                //                                    this.TeamProjectId, 
                //                                    buildDefinition.Id);
                rmArtifact.Alias = buildDefinition.Name;
                rmArtifact.DefinitionReference = new Dictionary<string, ArtifactSourceReference>();
                rmArtifact.IsPrimary = true;
                foreach (string artifactSourceReferenceType in artifactSourceReferenceTypeList)
                {
                    artifactSourceReference = new ArtifactSourceReference();

                    switch (artifactSourceReferenceType)
                    {
                        case ArtifactSourceReference_ArtifactSourceDefinitionUrl:
                            artifactSourceReference.Id = string.Format("{0}_permalink/_build/index?collectionId={1}&projectId={2}&definitionId={3}",
                                                                       this.TeamProjectCollectionUri.ToString(),
                                                                       this._teamProjectCollectionInstanceId,
                                                                       this.TeamProjectId,
                                                                       buildDefinition.Id);
                            artifactSourceReference.Name = "";
                            break;

                        case ArtifactSourceReference_Definition:
                            artifactSourceReference.Id = string.Format("{0}",
                                                                       buildDefinition.Id);
                            artifactSourceReference.Name = string.Format("{0}",
                                                                         buildDefinition.Name);
                            break;

                        case ArtifactSourceReference_DefaultVersionType:
                            artifactSourceReference.Id = string.Format("{0}",
                                                                       "latestType");
                            artifactSourceReference.Name = string.Format("{0}",
                                                                         "Latest");
                            break;

                        case ArtifactSourceReference_Project:
                            artifactSourceReference.Id = string.Format("{0}",
                                                                       this.TeamProjectId);
                            artifactSourceReference.Name = string.Format("{0}",
                                                                         this.TeamProjectName);
                            break;

                        default:
                            break;
                    }

                    rmArtifact.DefinitionReference.Add(artifactSourceReferenceType, artifactSourceReference);

                }

                result.Add(rmArtifact);

            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        /// <summary>
        /// Update artifact definition references.
        /// </summary>
        /// <remarks>
        /// It is assumed that build definitions and releases definition have the same exact names. When an artifact contains a
        /// reference to a build definition then the build definition will have the exact same name as the release definition.
        /// </remarks>
        /// <param name="targetReleaseDefinitionName"></param>
        /// <param name="artifacts"></param>
        protected void UpdateArtifactDefinitionReferences(string targetReleaseDefinitionName,
                                                          IList<RMArtifact> artifacts)
        {
            BuildDefinitionManager buildDefinitionManager = null;
            BuildDefinitionList buildDefinitionList = null;
            string buildDefinitionNamePattern = string.Empty;
            ArtifactSourceReference artifactSourceReference = null;
            BuildDefinition targetBuildDefinition = null;
            try
            {

                buildDefinitionManager = BuildDefinitionManagerFactory.CreateBuildDefinitionManager(this.TeamProjectCollectionUri,
                                                                                                    this.TeamProjectCollectionVsrmUri,
                                                                                                    this.TeamProjectName,
                                                                                                    this.BasicAuthRestApiUserProfileName,
                                                                                                    this.BasicAuthRestApiPassword);

                foreach (RMArtifact artifact in artifacts)
                {

                    var definitionReference = artifact.DefinitionReference;

                    if (artifact.Type.ToLower() == Artifact_Type_Build.ToLower())
                    {

                        if (definitionReference.ContainsKey("artifcatSourceDefinitionUrl") ||
                        definitionReference.ContainsKey("definition"))
                        {
                            if (targetBuildDefinition == null)
                            {
                                buildDefinitionNamePattern = targetReleaseDefinitionName;
                                buildDefinitionList = buildDefinitionManager.GetBuildDefinitionList(buildDefinitionNamePattern);
                                targetBuildDefinition = buildDefinitionList.SingleOrDefault();
                            }

                        }

                        if (targetBuildDefinition == null)
                        {
                            throw new ReleaseDefinitionArtifactCouldNotBeUpdatedException(string.Format("Artifact could not be updated. Build definition not found. (Build definition name: {0}",
                                                                                                         buildDefinitionNamePattern));
                        }

                        artifact.Alias = targetBuildDefinition.Name;


                        foreach (KeyValuePair<string, ArtifactSourceReference> keyValuePair in definitionReference)
                        {

                            if (keyValuePair.Value.GetType() == typeof(ArtifactSourceReference))
                            {
                                artifactSourceReference = (ArtifactSourceReference)keyValuePair.Value;
                            }

                            switch (keyValuePair.Key)
                            {
                                case ArtifactSourceReference_ArtifactSourceDefinitionUrl:
                                    if (targetBuildDefinition == null)
                                    {
                                        throw new ReleaseDefinitionArtifactSourceDefinitionUrlCouldNotBeUpdatedException(string.Format("Artifact source definition Url could not be updated. Build definition not found. (Build definition name: {0}",
                                                                                                                      buildDefinitionNamePattern));
                                    }
                                    artifactSourceReference.Id = string.Format("{0}_permalink/_build/index?collectionId={1}&projectId={2}&definitionId={3}",
                                                       this.TeamProjectCollectionUri.ToString(),
                                                       this._teamProjectCollectionInstanceId,
                                                       this.TeamProjectId,
                                                       targetBuildDefinition.Id);
                                    break;
                                case ArtifactSourceReference_Definition:
                                    if (targetBuildDefinition == null)
                                    {
                                        throw new ReleaseDefinitionArtifactDefinitionCouldNotBeUpdatedException(string.Format("Artifact definition could not be updated. Build definition not found. (Build definition name: {0}",
                                                                                                                              buildDefinitionNamePattern));
                                    }
                                    artifactSourceReference.Id = targetBuildDefinition.Id.ToString();
                                    artifactSourceReference.Name = targetBuildDefinition.Name;
                                    break;
                                case Artifact_Type_Build:
                                    break;
                                default:
                                    break;
                            }

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Replace token in text with specified value.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="token"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string ReplaceTokenInTextWithValue(string text, string token, string value)
        {
            string result = string.Empty;

            try
            {

                if (string.IsNullOrEmpty(text) == true)
                {
                    result = text;
                    return result;
                }

                result = Regex.Replace(text, token, value, RegexOptions.IgnoreCase);

                return result;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Replace text in release definition.
        /// </summary>
        /// <param name="releaseDefinition"></param>
        protected void ReplaceTextInReleaseDefinition(ref ReleaseDefinition releaseDefinition,
                                                     string sourceReleaseDefinitionNamePrefix,
                                                     string targetReleaseDefinitionNamePrefix,
                                                     string sourceBranchName,
                                                     string targetBranchName)
        {

            try
            {

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Populate team project information.
        /// </summary>
        /// <param name="projectName"></param>
        protected void PopulateTeamProjectInfo()
        {
            Uri teamProjectCollectionUri = null;
            WorkItemStore workItemStore = null;
            Project project = null;
            TfsTeamProjectCollection tpc = null;

            try
            {
                teamProjectCollectionUri = this.TeamProjectCollectionUri;
                using (tpc = new TfsTeamProjectCollection(teamProjectCollectionUri))
                {
                    if (tpc == null)
                    {
                        throw new TeamProjectCollectionInfoNotFoundException(string.Format("Team project collection info not found. (Team project collection uri: {0})",
                                                                                           teamProjectCollectionUri.ToString()));
                    }
                    workItemStore = tpc.GetService<WorkItemStore>();
                    if (workItemStore == null)
                    {
                        throw new TeamProjectInfoNotFoundException(string.Format("Team project info not found. (Team project collection uri: {0}, Team project name: {1})",
                                                                                  teamProjectCollectionUri.ToString(),
                                                                                  this.TeamProjectName));
                    }

                    project = workItemStore.Projects[this.TeamProjectName];
                    if (project == null)
                    {
                        throw new TeamProjectInfoNotFoundException(string.Format("Team project info not found. (Team project collection uri: {0}, Team project name: {1})",
                                                                                  teamProjectCollectionUri.ToString(),
                                                                                  this.TeamProjectName));
                    }

                    this.TeamProjectCollectionInstanceId = workItemStore.TeamProjectCollection.InstanceId;

                    this.TeamProjectId = project.Guid;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Retrieve environment abbreviation from full build definition name or release definition name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected string GetEnvironmentFromName(string name)
        {

            string result = string.Empty;
            string environmentAbbreviation = string.Empty;
            List<string> environmentAbbreviations = new List<string>() { Environment_DEV,
                                                                         Environment_QA,
                                                                         Environment_UAT,
                                                                         Environment_PERF,
                                                                         Environment_PROD,
                                                                         Environment_DEVDR,
                                                                         Environment_UATDR,
                                                                         Environment_PRODDR };
            try
            {
                environmentAbbreviation = environmentAbbreviations.SingleOrDefault(env => name.ToLower().EndsWith(env.ToLower()));

                if (string.IsNullOrEmpty(environmentAbbreviation) == true)
                {
                    result = Environment_Default;
                }
                else
                {
                    result = environmentAbbreviation.ToUpper();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Private Methods

        #endregion


    }
}
