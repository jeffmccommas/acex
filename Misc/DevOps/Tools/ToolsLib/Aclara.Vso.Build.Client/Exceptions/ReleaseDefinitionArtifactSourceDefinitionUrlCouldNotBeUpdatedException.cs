﻿using System;
using System.Runtime.Serialization;

namespace Aclara.Vso.Build.Client.Exceptions
{
    [Serializable]
    public class ReleaseDefinitionArtifactSourceDefinitionUrlCouldNotBeUpdatedException : Exception
    {

        #region Public Constructors

        public ReleaseDefinitionArtifactSourceDefinitionUrlCouldNotBeUpdatedException()
            : base()
        {
        }

        public ReleaseDefinitionArtifactSourceDefinitionUrlCouldNotBeUpdatedException(string message)
            : base(message)
        {
        }

        public ReleaseDefinitionArtifactSourceDefinitionUrlCouldNotBeUpdatedException(string message,
                                       Exception innerException)
            : base(message, innerException)
        {
        }

        #endregion

        #region Protected Constructors

        protected ReleaseDefinitionArtifactSourceDefinitionUrlCouldNotBeUpdatedException(SerializationInfo serializationInfo,
                                                 StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }

        #endregion

    }
}
