﻿using System;
using System.Runtime.Serialization;

namespace Aclara.Vso.Build.Client.Exceptions
{
    [Serializable]
    public class TeamProjectInfoNotFoundException : Exception
    {

        #region Public Constructors

        public TeamProjectInfoNotFoundException()
            : base()
        {
        }

        public TeamProjectInfoNotFoundException(string message)
            : base(message)
        {
        }

        public TeamProjectInfoNotFoundException(string message,
                                       Exception innerException)
            : base(message, innerException)
        {
        }

        #endregion

        #region Protected Constructors

        protected TeamProjectInfoNotFoundException(SerializationInfo serializationInfo,
                                                 StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }

        #endregion

    }
}
