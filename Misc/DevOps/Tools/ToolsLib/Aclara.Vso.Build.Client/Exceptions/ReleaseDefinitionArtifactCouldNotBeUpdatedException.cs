﻿using System;
using System.Runtime.Serialization;

namespace Aclara.Vso.Build.Client.Exceptions
{
    [Serializable]
    public class ReleaseDefinitionArtifactCouldNotBeUpdatedException : Exception
    {

        #region Public Constructors

        public ReleaseDefinitionArtifactCouldNotBeUpdatedException()
            : base()
        {
        }

        public ReleaseDefinitionArtifactCouldNotBeUpdatedException(string message)
            : base(message)
        {
        }

        public ReleaseDefinitionArtifactCouldNotBeUpdatedException(string message,
                                       Exception innerException)
            : base(message, innerException)
        {
        }

        #endregion

        #region Protected Constructors

        protected ReleaseDefinitionArtifactCouldNotBeUpdatedException(SerializationInfo serializationInfo,
                                                 StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }

        #endregion

    }
}