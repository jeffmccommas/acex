﻿using System;
using System.Runtime.Serialization;

namespace Aclara.Vso.Build.Client.Exceptions
{
    [Serializable]
    public class ReleaseDefinitionArtifactDefinitionCouldNotBeUpdatedException : Exception
    {

        #region Public Constructors

        public ReleaseDefinitionArtifactDefinitionCouldNotBeUpdatedException()
            : base()
        {
        }

        public ReleaseDefinitionArtifactDefinitionCouldNotBeUpdatedException(string message)
            : base(message)
        {
        }

        public ReleaseDefinitionArtifactDefinitionCouldNotBeUpdatedException(string message,
                                       Exception innerException)
            : base(message, innerException)
        {
        }

        #endregion

        #region Protected Constructors

        protected ReleaseDefinitionArtifactDefinitionCouldNotBeUpdatedException(SerializationInfo serializationInfo,
                                                 StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }

        #endregion

    }
}