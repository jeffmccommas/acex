﻿using System;
using System.Runtime.Serialization;

namespace Aclara.Vso.Build.Client.Exceptions
{
    [Serializable]
    public class UpdateBuildDefinitionRequiredPropertiesException : Exception
    {


        #region Public Constructors

        public UpdateBuildDefinitionRequiredPropertiesException()
                : base()
            {
        }

        public UpdateBuildDefinitionRequiredPropertiesException(string message)
                : base(message)
            {
        }

        public UpdateBuildDefinitionRequiredPropertiesException(string message,
                                                                Exception innerException)
                : base(message, innerException)
            {
        }

        #endregion

        #region Protected Constructors

        protected UpdateBuildDefinitionRequiredPropertiesException(SerializationInfo serializationInfo,
                                                                   StreamingContext streamingContext)
                : base(serializationInfo, streamingContext)
            {
        }

        #endregion


    }
}
