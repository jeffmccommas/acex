﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Models
{
    public class PowerBIWorkspace
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private string _workspaceCollectionName;
        private string _id;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Workspace collection name.
        /// </summary>
        public string WorkspaceCollectionName
        {
            get { return _workspaceCollectionName; }
            set { _workspaceCollectionName = value; }
        }

        /// <summary>
        /// Property: Identifier.
        /// </summary>
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        #endregion

        #region Public Contructors
        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
