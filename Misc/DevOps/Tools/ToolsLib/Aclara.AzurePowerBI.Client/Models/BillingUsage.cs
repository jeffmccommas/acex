﻿using Newtonsoft.Json;

namespace Aclara.AzurePowerBI.Client.Models
{
    public class BillingUsage
    {
        [JsonProperty(PropertyName = "renders")]
        public int Renders { get; set; }
    }
}
