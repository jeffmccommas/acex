﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Models
{
    public class PowerBIDataset
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private string _name;
        private string _id;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Property: Identifier.
        /// </summary>
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        #endregion

        #region Public Contructors
        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
