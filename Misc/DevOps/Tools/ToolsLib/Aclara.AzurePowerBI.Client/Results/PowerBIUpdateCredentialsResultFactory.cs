﻿using Aclara.AzurePowerBI.Client.Interfaces;
using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Results
{
    public class PowerBIUpdateCredentialsResultFactory
    {
        /// <summary>
        /// Create PowerBI delete dataset result.
        /// </summary>
        /// <returns></returns>
        static public IPowerBIUpdateCredentialsResult CreatePowerBIUpdateCredentialsResult()
        {
            IPowerBIUpdateCredentialsResult result = null;
            PowerBIUpdateCredentialsResult powerBIUpdateCredentialsResult = null;

            try
            {
                powerBIUpdateCredentialsResult = new PowerBIUpdateCredentialsResult();

                result = powerBIUpdateCredentialsResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
