﻿using Aclara.AzurePowerBI.Client.Interfaces;
using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Results
{
    public class PowerBIDeleteDatasetResultFactory
    {
        /// <summary>
        /// Create PowerBI delete dataset result.
        /// </summary>
        /// <returns></returns>
        static public IPowerBIDeleteDatasetResult CreatePowerBIDeleteDatasetResult()
        {
            IPowerBIDeleteDatasetResult result = null;
            PowerBIDeleteDatasetResult powerBIDeleteDatasetResult = null;

            try
            {
                powerBIDeleteDatasetResult = new PowerBIDeleteDatasetResult();

                result = powerBIDeleteDatasetResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
