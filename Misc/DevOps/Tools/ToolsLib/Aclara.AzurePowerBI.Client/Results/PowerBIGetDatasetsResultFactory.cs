﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aclara.AzurePowerBI.Client.Interfaces;

namespace Aclara.AzurePowerBI.Client.Results
{
    public class PowerBIGetDatasetsResultFactory
    {
        /// <summary>
        /// Create PowerBI get datasets result.
        /// </summary>
        /// <returns></returns>
        static public IPowerBIGetDatasetsResult CreatePowerBIGetDatasetsResult()
        {
            IPowerBIGetDatasetsResult result = null;
            PowerBIGetDatasetsResult powerBIGetDatasetsResult = null;

            try
            {
                powerBIGetDatasetsResult = new PowerBIGetDatasetsResult();

                result = powerBIGetDatasetsResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
