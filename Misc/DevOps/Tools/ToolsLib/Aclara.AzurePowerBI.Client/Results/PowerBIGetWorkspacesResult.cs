﻿using Aclara.AzurePowerBI.Client.Interfaces;
using Aclara.AzurePowerBI.Client.Models;
using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Results
{
    public class PowerBIGetWorkspacesResult : IPowerBIGetWorkspacesResult
    {
        #region Private Constants
        #endregion

        #region Private Data  Members

        private List<string> _workspacesIdList;
        StatusList _statusList;
        private List<PowerBIWorkspace> _workspaceList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        /// <summary>
        /// Property: Workspace id list.
        /// </summary>
        public List<string> WorkspaceIdList
        {
            get { return _workspacesIdList; }
            set { _workspacesIdList = value; }
        }

        /// <summary>
        /// Property: Workspace list.
        /// </summary>
        public List<PowerBIWorkspace> WorkspaceList
        {
            get { return _workspaceList; }
            set { _workspaceList = value; }
        }

        #endregion

        #region Public Constructors 

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerBIGetWorkspacesResult()
        {
            _workspacesIdList = new List<string>();
            _workspaceList = new List<PowerBIWorkspace>();
            _statusList = new StatusList();
        }

        #endregion
    }
}
