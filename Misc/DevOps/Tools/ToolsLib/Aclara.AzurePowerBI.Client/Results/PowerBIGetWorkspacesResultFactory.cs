﻿using Aclara.AzurePowerBI.Client.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Results
{
    public class PowerBIGetWorkspacesResultFactory
    {
        /// <summary>
        /// Create PowerBI get workspaces result.
        /// </summary>
        /// <returns></returns>
        static public IPowerBIGetWorkspacesResult CreatePowerBIGetWorkspacesResult()
        {
            IPowerBIGetWorkspacesResult result = null;
            PowerBIGetWorkspacesResult powerBIGetWorkspacesResult = null;

            try
            {
                powerBIGetWorkspacesResult = new PowerBIGetWorkspacesResult();

                result = powerBIGetWorkspacesResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
