﻿using Aclara.AzurePowerBI.Client.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Results
{
    public class PowerBIImportPbixResultFactory
    {
        /// <summary>
        /// Create PowerBI get workspaces result.
        /// </summary>
        /// <returns></returns>
        static public IPowerBIImportPbixResult CreatePowerBIImportPbixResult()
        {
            IPowerBIImportPbixResult result = null;
            PowerBIImportPbixResult powerBIImportPbixResult = null;

            try
            {
                powerBIImportPbixResult = new PowerBIImportPbixResult();

                result = powerBIImportPbixResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
