﻿using Aclara.AzurePowerBI.Client.Interfaces;
using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Results
{
    public class PowerBIImportPbixResult : IPowerBIImportPbixResult
    {
        #region Private Constants
        #endregion

        #region Private Data  Members

        private string _importId;
        StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        /// <summary>
        /// Property: Import id.
        /// </summary>
        public string ImportId
        {
            get { return _importId; }
            set { _importId = value; }
        }
        #endregion

        #region Public Constructors 

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerBIImportPbixResult()
        {
            _importId = string.Empty;
            _statusList = new StatusList();
        }

        #endregion
    }
}
