﻿using Aclara.AzurePowerBI.Client.Interfaces;
using Aclara.AzurePowerBI.Client.Models;
using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Results
{
    public class PowerBIGetDatasetsResult: IPowerBIGetDatasetsResult
    {
        #region Private Constants
        #endregion

        #region Private Data  Members

        private List<PowerBIDataset> _datasetList;
        StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        /// <summary>
        /// Property: Dataset list.
        /// </summary>
        public List<PowerBIDataset> DatasetList
        {
            get { return _datasetList; }
            set { _datasetList = value; }
        }
        #endregion

        #region Public Constructors 

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerBIGetDatasetsResult()
        {
            _datasetList = new List<PowerBIDataset>();
            _statusList = new StatusList();
        }

        #endregion
    }
}
