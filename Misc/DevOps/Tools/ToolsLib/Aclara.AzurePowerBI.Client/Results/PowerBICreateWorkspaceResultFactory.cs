﻿using Aclara.AzurePowerBI.Client.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Results
{
    public class PowerBICreateWorkspaceResultFactory
    {
        /// <summary>
        /// Create PowerBI get workspaces result.
        /// </summary>
        /// <returns></returns>
        static public IPowerBICreateWorkspaceResult CreatePowerBICreateWorkspaceResult()
        {
            IPowerBICreateWorkspaceResult result = null;
            PowerBICreateWorkspaceResult powerBICreateWorkspaceResult = null;

            try
            {
                powerBICreateWorkspaceResult = new PowerBICreateWorkspaceResult();

                result = powerBICreateWorkspaceResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
