﻿using Aclara.AzurePowerBI.Client.Interfaces;
using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Results
{
    public class PowerBICreateWorkspaceResult : IPowerBICreateWorkspaceResult
    {
        #region Private Constants
        #endregion

        #region Private Data  Members

        private string _createdWorkspaceId;
        StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        /// <summary>
        /// Property: Workspace id.
        /// </summary>
        public string CreatedWorkspaceId
        {
            get { return _createdWorkspaceId; }
            set { _createdWorkspaceId = value; }
        }
        #endregion

        #region Public Constructors 

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerBICreateWorkspaceResult()
        {
            _createdWorkspaceId = string.Empty;
            _statusList = new StatusList();
        }

        #endregion
    }
}
