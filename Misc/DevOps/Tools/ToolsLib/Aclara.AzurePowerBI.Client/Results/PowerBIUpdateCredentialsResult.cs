﻿using Aclara.AzurePowerBI.Client.Interfaces;
using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Results
{
    public class PowerBIUpdateCredentialsResult : IPowerBIUpdateCredentialsResult
    {
        #region Private Constants
        #endregion

        #region Private Data  Members

        StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        #endregion

        #region Public Constructors 

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerBIUpdateCredentialsResult()
        {
            _statusList = new StatusList();
        }

        #endregion
    }
}
