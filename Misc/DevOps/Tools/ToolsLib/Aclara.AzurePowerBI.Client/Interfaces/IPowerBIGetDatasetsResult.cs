﻿using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aclara.AzurePowerBI.Client.Models;

namespace Aclara.AzurePowerBI.Client.Interfaces
{
    public interface IPowerBIGetDatasetsResult
    {
            List<PowerBIDataset> DatasetList { get; }

            StatusList StatusList { get; }
    }
}
