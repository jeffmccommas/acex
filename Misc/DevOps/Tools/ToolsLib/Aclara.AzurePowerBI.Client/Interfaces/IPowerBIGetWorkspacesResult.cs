﻿using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Interfaces
{
    public interface IPowerBIGetWorkspacesResult
    {
        List<string> WorkspaceIdList { get; }
        StatusList StatusList { get; }
    }
}
