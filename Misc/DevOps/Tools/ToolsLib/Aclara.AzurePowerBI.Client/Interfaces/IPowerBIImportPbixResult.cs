﻿using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Interfaces
{
    public interface IPowerBIImportPbixResult
    {
        string ImportId { get; set; }

        StatusList StatusList { get; }
    }
}
