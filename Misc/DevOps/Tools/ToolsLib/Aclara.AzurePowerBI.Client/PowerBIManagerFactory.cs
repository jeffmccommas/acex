﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client
{
    public static class PowerBIManagerFactory
    {
        #region Public Methods
        /// <summary>
        /// Create PowerBI manager.
        /// </summary>
        /// <returns></returns>
        static public PowerBIManager CreatePowerBIManager()
        {
            PowerBIManager result = null;
            PowerBIManager PowerBIManager = null;

            try
            {
                PowerBIManager = new PowerBIManager();

                result = PowerBIManager;
                return result;
            }
            catch (Exception)
            {
                throw;
            }

            #endregion
        }
    }
}
