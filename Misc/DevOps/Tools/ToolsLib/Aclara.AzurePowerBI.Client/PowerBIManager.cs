﻿using Aclara.AzurePowerBI.Client.Events;
using Aclara.AzurePowerBI.Client.Interfaces;
using Aclara.AzurePowerBI.Client.Models;
using Aclara.AzurePowerBI.Client.Results;
using Aclara.Tools.Common.StatusManagement;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.PowerBI.Api.V1;
using Microsoft.PowerBI.Api.V1.Models;
using Microsoft.Rest;
using Microsoft.Rest.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client
{
    public class PowerBIManager
    {
        #region Private Constants

        const string version = "?api-version=2016-01-29";
        const string armResource = "https://management.core.windows.net/";

        #endregion

        #region Private Data Members

        #endregion

        #region Public Delegates

        public event EventHandler<PowerBIConfigUpdateEventArgs> PowerBIConfigUpdateCompleted;
        public event EventHandler<PowerBIGetWorkspacesEventArgs> PowerBIGetWorkspacesCompleted;
        public event EventHandler<PowerBIGetDatasetsEventArgs> PowerBIGetDatasetsCompleted;
        public event EventHandler<PowerBICreateWorkspaceEventArgs> PowerBICreateWorkspaceCompleted;
        public event EventHandler<PowerBIImportPbixEventArgs> PowerBIImportPbixCompleted;
        public event EventHandler<PowerBIUpdateCredentialsEventArgs> PowerBIUpdateCredentialsCompleted;
        public event EventHandler<PowerBIDeleteDatasetEventArgs> PowerBIDeleteDatasetCompleted;

        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        public PowerBIManager()
        {

        }

        #endregion

        #region Protected Constructors
        #endregion

        #region Public Methods

        /// <summary>
        /// Get PowerBI workspaces from workspace collection.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="cancellationToken"></param>
        public void GetWorkspaces(string powerBIApiEndpoint,
                                  string azureApiEndpoint,
                                  string workspaceCollectionName,
                                  string workspaceCollectionAccessKey,
                                  CancellationToken cancellationToken)
        {
            IPowerBIGetWorkspacesResult powerBIGetWorkspacesResult = null;
            PowerBIGetWorkspacesEventArgs powerBIGetWorkspacesEventArgs = null;

            try
            {
                powerBIGetWorkspacesEventArgs = new PowerBIGetWorkspacesEventArgs();

                var workspaces = GetWorkspacesAsync(powerBIApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey).Result;

                if (PowerBIGetWorkspacesCompleted != null)
                {
                    powerBIGetWorkspacesResult = PowerBIGetWorkspacesResultFactory.CreatePowerBIGetWorkspacesResult();
                    foreach (Workspace workspace in workspaces)
                    {
                        powerBIGetWorkspacesResult.WorkspaceIdList.Add(workspace.WorkspaceId);
                    }
                    powerBIGetWorkspacesEventArgs.PowerBIGetWorkspacesResult = powerBIGetWorkspacesResult;
                    PowerBIGetWorkspacesCompleted(this, powerBIGetWorkspacesEventArgs);
                }

            }
            catch (Exception ex)
            {
                Status status = null;

                powerBIGetWorkspacesResult = PowerBIGetWorkspacesResultFactory.CreatePowerBIGetWorkspacesResult();
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                powerBIGetWorkspacesResult.StatusList.Add(status);
                if (PowerBIGetWorkspacesCompleted != null)
                {
                    PowerBIGetWorkspacesCompleted(this, powerBIGetWorkspacesEventArgs);
                }
            }
        }

        /// <summary>
        /// Get PowerBI workspaces from workspace collection.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="cancellationToken"></param>
        public List<PowerBIWorkspace> GetWorkspaces(string powerBIApiEndpoint,
                                                    string azureApiEndpoint,
                                                    string workspaceCollectionName,
                                                    string workspaceCollectionAccessKey)
        {
            List<PowerBIWorkspace> result = null;
            PowerBIWorkspace powerBIWorkspace = null;
            List<Workspace> workspaces = null;

            try
            {
                result = new List<PowerBIWorkspace>();


                workspaces = GetWorkspacesSync(powerBIApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey);

                foreach (Workspace workspace in workspaces)
                {
                    powerBIWorkspace = new Models.PowerBIWorkspace();
                    powerBIWorkspace.Id = workspace.WorkspaceId;
                    powerBIWorkspace.WorkspaceCollectionName = workspace.WorkspaceCollectionName;
                    result.Add(powerBIWorkspace);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get PowerBI dataset from workspace collection for specified workspace.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="workspaceId"></param>
        /// <param name="cancellationToken"></param>
        public void GetDatasets(string powerBIApiEndpoint,
                                  string azureApiEndpoint,
                                  string workspaceCollectionName,
                                  string workspaceCollectionAccessKey,
                                  string workspaceId,
                                  CancellationToken cancellationToken)
        {
            IPowerBIGetDatasetsResult powerBIGetDatasetsResult = null;
            PowerBIGetDatasetsEventArgs powerBIGetDatasetsEventArgs = null;

            try
            {
                powerBIGetDatasetsEventArgs = new PowerBIGetDatasetsEventArgs();

                var datasets = GetDatasetsAsync(powerBIApiEndpoint,
                                           workspaceCollectionName,
                                           workspaceCollectionAccessKey,
                                           workspaceId).Result;

                if (PowerBIGetDatasetsCompleted != null)
                {
                    powerBIGetDatasetsResult = PowerBIGetDatasetsResultFactory.CreatePowerBIGetDatasetsResult();
                    foreach (PowerBIDataset dataset in datasets)
                    {
                        powerBIGetDatasetsResult.DatasetList.Add(dataset);
                    }
                    powerBIGetDatasetsEventArgs.PowerBIGetDatasetsResult = powerBIGetDatasetsResult;
                    PowerBIGetDatasetsCompleted(this, powerBIGetDatasetsEventArgs);
                }

            }
            catch (Exception ex)
            {
                Status status = null;

                powerBIGetDatasetsResult = PowerBIGetDatasetsResultFactory.CreatePowerBIGetDatasetsResult();
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                powerBIGetDatasetsResult.StatusList.Add(status);
                if (PowerBIGetDatasetsCompleted != null)
                {
                    PowerBIGetDatasetsCompleted(this, powerBIGetDatasetsEventArgs);
                }
            }
        }

        /// <summary>
        /// Get PowerBI datasets from workspace collection.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="workspaceId"></param>
        /// <returns></returns>
        public List<PowerBIDataset> GeDatasets(string powerBIApiEndpoint,
                                               string azureApiEndpoint,
                                               string workspaceCollectionName,
                                               string workspaceCollectionAccessKey,
                                               string workspaceId)
        {
            List<PowerBIDataset> result = null;
            PowerBIDataset powerBIDataset = null;
            List<PowerBIDataset> datasets = null;

            try
            {
                result = new List<PowerBIDataset>();


                datasets = GetDatasetsSync(powerBIApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey, workspaceId);

                foreach (PowerBIDataset dataset in datasets)
                {
                    powerBIDataset = new Models.PowerBIDataset();
                    powerBIDataset.Id = dataset.Id;
                    powerBIDataset.Name = dataset.Name;
                    result.Add(powerBIDataset);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Create PowerBI workspace in workspace collection.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="cancellationToken"></param>
        public void CreateWorkspace(string powerBIApiEndpoint,
                                    string azureApiEndpoint,
                                    string workspaceCollectionName,
                                    string workspaceCollectionAccessKey,
                                    CancellationToken cancellationToken)
        {
            IPowerBICreateWorkspaceResult powerBICreateWorkspaceResult = null;
            PowerBICreateWorkspaceEventArgs powerBICreateWorkspaceEventArgs = null;

            try
            {
                powerBICreateWorkspaceEventArgs = new PowerBICreateWorkspaceEventArgs();

                var workspace = CreateWorkspace(powerBIApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey).Result;

                if (PowerBICreateWorkspaceCompleted != null)
                {
                    powerBICreateWorkspaceResult = PowerBICreateWorkspaceResultFactory.CreatePowerBICreateWorkspaceResult();


                    powerBICreateWorkspaceResult.CreatedWorkspaceId = workspace.WorkspaceId;

                    powerBICreateWorkspaceEventArgs.PowerBICreateWorkspaceResult = powerBICreateWorkspaceResult;
                    PowerBICreateWorkspaceCompleted(this, powerBICreateWorkspaceEventArgs);
                }

            }
            catch (Exception ex)
            {
                Status status = null;

                powerBICreateWorkspaceResult = PowerBICreateWorkspaceResultFactory.CreatePowerBICreateWorkspaceResult();
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                powerBICreateWorkspaceResult.StatusList.Add(status);
                if (PowerBICreateWorkspaceCompleted != null)
                {
                    PowerBICreateWorkspaceCompleted(this, powerBICreateWorkspaceEventArgs);
                }
            }
        }

        /// <summary>
        /// Update PowerBI credential.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="workspaceId"></param>
        /// <param name="password"></param>
        /// <param name="username"></param>
        /// <param name="cancellationToken"></param>
        public void UpdateCredentials(string powerBIApiEndpoint,
                                      string azureApiEndpoint,
                                      string workspaceCollectionName,
                                      string workspaceCollectionAccessKey,
                                      string workspaceId,
                                      string username,
                                      string password,
                                      CancellationToken cancellationToken)
        {
            IPowerBIUpdateCredentialsResult powerBIUpdateCredentialsResult = null;
            PowerBIUpdateCredentialsEventArgs powerBIUpdateCredentialsEventArgs = null;

            try
            {
                powerBIUpdateCredentialsEventArgs = new PowerBIUpdateCredentialsEventArgs();

                UpdateCredentials(powerBIApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey, workspaceId, username, password);

                if (PowerBIUpdateCredentialsCompleted != null)
                {
                    powerBIUpdateCredentialsResult = PowerBIUpdateCredentialsResultFactory.CreatePowerBIUpdateCredentialsResult();

                    powerBIUpdateCredentialsEventArgs.PowerBIUpdateCredentialsResult = powerBIUpdateCredentialsResult;
                    PowerBIUpdateCredentialsCompleted(this, powerBIUpdateCredentialsEventArgs);
                }

            }
            catch (Exception ex)
            {
                Status status = null;

                powerBIUpdateCredentialsResult = PowerBIUpdateCredentialsResultFactory.CreatePowerBIUpdateCredentialsResult();
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                powerBIUpdateCredentialsResult.StatusList.Add(status);
                if (PowerBIUpdateCredentialsCompleted != null)
                {
                    PowerBIUpdateCredentialsCompleted(this, powerBIUpdateCredentialsEventArgs);
                }
            }
        }

        /// <summary>
        /// Delete dataset.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="workspaceId"></param>
        /// <param name="datasetId"></param>
        /// <param name="cancellationToken"></param>
        public void DeleteDataset(string powerBIApiEndpoint,
                                  string azureApiEndpoint,
                                  string workspaceCollectionName,
                                  string workspaceCollectionAccessKey,
                                  string workspaceId,
                                  string datasetId,
                                  CancellationToken cancellationToken)
        {
            IPowerBIDeleteDatasetResult powerBIDeleteDatasetResult = null;
            PowerBIDeleteDatasetEventArgs powerBIDeleteDatasetEventArgs = null;

            try
            {
                powerBIDeleteDatasetEventArgs = new PowerBIDeleteDatasetEventArgs();

                DeleteDataset(powerBIApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey, workspaceId, datasetId);

                if (PowerBIDeleteDatasetCompleted != null)
                {
                    powerBIDeleteDatasetResult = PowerBIDeleteDatasetResultFactory.CreatePowerBIDeleteDatasetResult();

                    powerBIDeleteDatasetEventArgs.PowerBIDeleteDatasetResult = powerBIDeleteDatasetResult;
                    PowerBIDeleteDatasetCompleted(this, powerBIDeleteDatasetEventArgs);
                }

            }
            catch (Exception ex)
            {
                Status status = null;

                powerBIDeleteDatasetResult = PowerBIDeleteDatasetResultFactory.CreatePowerBIDeleteDatasetResult();
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                powerBIDeleteDatasetResult.StatusList.Add(status);
                if (PowerBIDeleteDatasetCompleted != null)
                {
                    PowerBIDeleteDatasetCompleted(this, powerBIDeleteDatasetEventArgs);
                }
            }
        }

        /// <summary>
        /// Import pbix file.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="cancellationToken"></param>
        public void ImportPbixFile(string powerBIApiEndpoint,
                                   string azureApiEndpoint,
                                   string workspaceCollectionName,
                                   string workspaceCollectionAccessKey,
                                   string workspaceId,
                                   string datasetName,
                                   string filePath,
                                   CancellationToken cancellationToken)
        {
            IPowerBIImportPbixResult powerBIImportPbixResult = null;
            PowerBIImportPbixEventArgs powerBIImportPbixEventArgs = null;

            try
            {
                powerBIImportPbixEventArgs = new PowerBIImportPbixEventArgs();

                var import = ImportPbixAsync(powerBIApiEndpoint,
                                             workspaceCollectionName,
                                             workspaceCollectionAccessKey,
                                             workspaceId,
                                             datasetName,
                                             filePath).Result;

                if (PowerBIImportPbixCompleted != null)
                {
                    powerBIImportPbixResult = PowerBIImportPbixResultFactory.CreatePowerBIImportPbixResult();


                    powerBIImportPbixResult.ImportId = import.Id;

                    powerBIImportPbixEventArgs.PowerBIImportPbixResult = powerBIImportPbixResult;
                    PowerBIImportPbixCompleted(this, powerBIImportPbixEventArgs);
                }

            }
            catch (Exception ex)
            {
                Status status = null;

                powerBIImportPbixResult = PowerBIImportPbixResultFactory.CreatePowerBIImportPbixResult();
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                powerBIImportPbixResult.StatusList.Add(status);
                if (PowerBIImportPbixCompleted != null)
                {
                    PowerBIImportPbixCompleted(this, powerBIImportPbixEventArgs);
                }
            }
        }

        #endregion

        #region Protected Methods




        #endregion

        #region Private Methods

        /// <summary>
        /// Retrieve PowerBI workspaces in workspace collection.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <returns></returns>
        private static async Task<IEnumerable<Workspace>> GetWorkspacesAsync(string powerBIApiEndpoint, string workspaceCollectionName, string workspaceCollectionAccessKey)
        {
            try
            {
                using (var client = CreateClient(powerBIApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey))
                {
                    var response = await client.Workspaces.GetWorkspacesByCollectionNameAsync(workspaceCollectionName);
                    return response.Value;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve PowerBI workspaces in workspace collection.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <returns></returns>
        private static List<Workspace> GetWorkspacesSync(string powerBIApiEndpoint, string workspaceCollectionName, string workspaceCollectionAccessKey)
        {
            List<Workspace> result = null;
            try
            {

                result = new List<Workspace>();

                using (var client = CreateClient(powerBIApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey))
                {
                    ODataResponseListWorkspace response = client.Workspaces.GetWorkspacesByCollectionNameAsync(workspaceCollectionName).Result;

                    foreach (Workspace workspace in response.Value)
                    {
                        result.Add(workspace);
                    }

                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve PowerBI datasets in workspace collection for specified workspace.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="workspaceId"></param>
        /// <returns></returns>
        static async Task<IEnumerable<PowerBIDataset>> GetDatasetsAsync(string powerBIApiEndpoint,
                                                                        string workspaceCollectionName,
                                                                        string workspaceCollectionAccessKey,
                                                                        string workspaceId)
        {

            List<PowerBIDataset> result = null;
            PowerBIDataset powerBIDataset = null;

            try
            {
                result = new List<PowerBIDataset>();

                using (var client = CreateClient(powerBIApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey))
                {
                    ODataResponseListDataset response = await client.Datasets.GetDatasetsAsync(workspaceCollectionName, workspaceId);
                    foreach (Dataset dataset in response.Value)
                    {
                        powerBIDataset = new PowerBIDataset();
                        powerBIDataset.Name = dataset.Name;
                        powerBIDataset.Id = dataset.Id;
                        result.Add(powerBIDataset);
                    }
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve PowerBI datasets in workspace collection for specified workspace.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="workspaceId"></param>
        /// <returns></returns>
        private static List<PowerBIDataset> GetDatasetsSync(string powerBIApiEndpoint, 
                                                            string workspaceCollectionName, 
                                                            string workspaceCollectionAccessKey,
                                                            string workspaceId)
        {
            List<PowerBIDataset> result = null;
            PowerBIDataset powerBIDataset = null;

            try
            {

                result = new List<PowerBIDataset>();

                using (var client = CreateClient(powerBIApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey))
                {
                    ODataResponseListDataset response = client.Datasets.GetDatasetsAsync(workspaceCollectionName, workspaceId).Result;

                    foreach (Dataset dataset in response.Value)
                    {
                        powerBIDataset = new PowerBIDataset();
                        powerBIDataset.Name = dataset.Name;
                        powerBIDataset.Id = dataset.Id;

                    result.Add(powerBIDataset);
                    }

                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Creates a new Power BI Embedded workspace within the specified collection.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <returns></returns>
        static async Task<Workspace> CreateWorkspace(string powerBIApiEndpoint, string workspaceCollectionName, string workspaceCollectionAccessKey)
        {

            using (var client = CreateClient(powerBIApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey))
            {
                // Create a new workspace witin the specified collection
                return await client.Workspaces.PostWorkspaceAsync(workspaceCollectionName);
            }
        }

        /// <summary>
        /// Update PowerBI credentials.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="workspaceId"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        static async void UpdateCredentials(string powerBIApiEndpoint, 
                                            string workspaceCollectionName, 
                                            string workspaceCollectionAccessKey, 
                                            string workspaceId,
                                            string username,
                                            string password)
        {

            using (var client = CreateClient(powerBIApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey))
            {
                // Get the newly created dataset from the previous import process
                var datasets = await client.Datasets.GetDatasetsAsync(workspaceCollectionName, workspaceId);

                //// Optionally udpate the connectionstring details if preent
                //if (!string.IsNullOrWhiteSpace(connectionString))
                //{
                //    var connectionParameters = new Dictionary<string, object>
                //    {
                //        { "connectionString", connectionString }
                //    };
                //    await client.Datasets.SetAllConnectionsAsync(workspaceCollectionName, workspaceId, datasets.Value[datasets.Value.Count - 1].Id, connectionParameters);
                //}

                // Get the datasources from the dataset
                var datasources = await client.Datasets.GetGatewayDatasourcesAsync(workspaceCollectionName, workspaceId, datasets.Value[datasets.Value.Count - 1].Id);

                // Reset your connection credentials
                var delta = new GatewayDatasource
                {
                    CredentialType = "Basic",
                    BasicCredentials = new BasicCredentials
                    {
                        Username = username,
                        Password = password
                    }
                };

                // Update the datasource with the specified credentials
                await client.Gateways.PatchDatasourceAsync(workspaceCollectionName, workspaceId, datasources.Value[datasources.Value.Count - 1].GatewayId, datasources.Value[datasources.Value.Count - 1].Id, delta);
            }
        }

        /// <summary>
        /// Delete dataset.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="workspaceId"></param>
        /// <param name="datasetId"></param>
        static async void  DeleteDataset(string powerBIApiEndpoint, 
                                         string workspaceCollectionName, 
                                         string workspaceCollectionAccessKey, 
                                         string workspaceId, 
                                         string datasetId)
        {

            using (var client = CreateClient(powerBIApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey))
            {
                await client.Datasets.DeleteDatasetByIdAsync(workspaceCollectionName, workspaceId, datasetId);
            }
        }

        /// <summary>
        /// Imports a Power BI Desktop file (pbix) into the Power BI Embedded service
        /// </summary>
        /// <param name="workspaceCollectionName">The Power BI workspace collection name</param>
        /// <param name="workspaceId">The target Power BI workspace id</param>
        /// <param name="datasetName">The dataset name to apply to the uploaded dataset</param>
        /// <param name="filePath">A local file path on your computer</param>
        /// <returns></returns>
        static async Task<Import> ImportPbixAsync(string powerBIApiEndpoint,
                                             string workspaceCollectionName,
                                             string workspaceCollectionAccessKey,
                                             string workspaceId,
                                             string datasetName,
                                             string filePath)
        {
            using (var fileStream = File.OpenRead(filePath))
            {
                using (var client = CreateClient(powerBIApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey))
                {
                    // Import PBIX file from the file stream
                    var import = await client.Imports.PostImportWithFileAsync(workspaceCollectionName, workspaceId, fileStream, datasetName);

                    // Example of polling the import to check when the import has succeeded.
                    while (import.ImportState != "Succeeded" && import.ImportState != "Failed")
                    {
                        import = await client.Imports.GetImportByIdAsync(workspaceCollectionName, workspaceId, import.Id);
                        Thread.Sleep(1000);
                    }

                    return import;
                }
            }
        }

        /// <summary>
        /// Creates a new instance of the PowerBIClient with the specified token
        /// </summary>
        /// <param name="token">A Power BI app token</param>
        /// <returns></returns>
        private static IPowerBIClient CreateClient(string powerBIApiEndpoint, string workspaceCollectionName, string workspaceCollectionAccessKey)
        {


            // Create a token credentials with "AppToken" type
            var credentials = new TokenCredentials(workspaceCollectionAccessKey, "AppKey");

            // Instantiate your Power BI client passing in the required credentials
            var client = new PowerBIClient(credentials);

            // Override the api endpoint base URL.  Default value is https://api.powerbi.com
            client.BaseUri = new Uri(powerBIApiEndpoint);

            return client;
        }
        #endregion
    }
}
