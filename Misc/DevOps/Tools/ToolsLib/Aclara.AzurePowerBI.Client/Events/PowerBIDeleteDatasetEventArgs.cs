﻿using Aclara.AzurePowerBI.Client.Interfaces;
using Aclara.AzurePowerBI.Client.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Events
{
    public class PowerBIDeleteDatasetEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private IPowerBIDeleteDatasetResult _powerBIDeleteDatasetResult;

        #endregion

        #region Public Properties

        public IPowerBIDeleteDatasetResult PowerBIDeleteDatasetResult
        {
            get { return _powerBIDeleteDatasetResult; }
            set { _powerBIDeleteDatasetResult = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerBIDeleteDatasetEventArgs()
        {
            _powerBIDeleteDatasetResult = new PowerBIDeleteDatasetResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="powerBIDeleteDatasetResult"></param>
        public PowerBIDeleteDatasetEventArgs(IPowerBIDeleteDatasetResult powerBIDeleteDatasetResult)
        {
            _powerBIDeleteDatasetResult = powerBIDeleteDatasetResult;
        }

        #endregion
    }
}
