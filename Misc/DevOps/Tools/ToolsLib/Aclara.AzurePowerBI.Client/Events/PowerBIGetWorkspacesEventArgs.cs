﻿using Aclara.AzurePowerBI.Client.Interfaces;
using Aclara.AzurePowerBI.Client.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Events
{
    public class PowerBIGetWorkspacesEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private IPowerBIGetWorkspacesResult _powerBIGetWorkspacesResult;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Power BI get workspaces result.
        /// </summary>
        public IPowerBIGetWorkspacesResult PowerBIGetWorkspacesResult
        {
            get { return _powerBIGetWorkspacesResult; }
            set { _powerBIGetWorkspacesResult = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerBIGetWorkspacesEventArgs()
        {
            _powerBIGetWorkspacesResult = new PowerBIGetWorkspacesResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="powerBIGetWorkspacesResult"></param>
        public PowerBIGetWorkspacesEventArgs(IPowerBIGetWorkspacesResult powerBIGetWorkspacesResult)
        {
            _powerBIGetWorkspacesResult = powerBIGetWorkspacesResult;
        }

        #endregion
    }
}