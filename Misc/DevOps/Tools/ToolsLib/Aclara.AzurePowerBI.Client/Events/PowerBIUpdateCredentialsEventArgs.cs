﻿using Aclara.AzurePowerBI.Client.Interfaces;
using Aclara.AzurePowerBI.Client.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Events
{
    public class PowerBIUpdateCredentialsEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private IPowerBIUpdateCredentialsResult _powerBIUpdateCredentialsResult;

        #endregion

        #region Public Properties

        public IPowerBIUpdateCredentialsResult PowerBIUpdateCredentialsResult
        {
            get { return _powerBIUpdateCredentialsResult; }
            set { _powerBIUpdateCredentialsResult = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerBIUpdateCredentialsEventArgs()
        {
            _powerBIUpdateCredentialsResult = new PowerBIUpdateCredentialsResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="powerBIUpdateCredentialsResult"></param>
        public PowerBIUpdateCredentialsEventArgs(IPowerBIUpdateCredentialsResult powerBIUpdateCredentialsResult)
        {
            _powerBIUpdateCredentialsResult = powerBIUpdateCredentialsResult;
        }

        #endregion
    }
}
