﻿using Aclara.AzurePowerBI.Client.Interfaces;
using Aclara.AzurePowerBI.Client.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Events
{
    public class PowerBIConfigUpdateEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private IPowerBIConfigUpdateResult _powerBIConfigUpdateResult;

        #endregion

        #region Public Properties

        public IPowerBIConfigUpdateResult PowerBIConfigUpdateResult
        {
            get { return _powerBIConfigUpdateResult; }
            set { _powerBIConfigUpdateResult = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerBIConfigUpdateEventArgs()
        {
            _powerBIConfigUpdateResult = new PowerBIConfigUpdateResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="powerBIConfigUpdateResult"></param>
        public PowerBIConfigUpdateEventArgs(IPowerBIConfigUpdateResult powerBIConfigUpdateResult)
        {
            _powerBIConfigUpdateResult = powerBIConfigUpdateResult;
        }

        #endregion
    }
}
