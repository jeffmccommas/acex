﻿using Aclara.AzurePowerBI.Client.Interfaces;
using Aclara.AzurePowerBI.Client.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Events
{
    public class PowerBIImportPbixEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private IPowerBIImportPbixResult _powerBIGetWorkspacesResult;

        #endregion

        #region Public Properties

        public IPowerBIImportPbixResult PowerBIImportPbixResult
        {
            get { return _powerBIGetWorkspacesResult; }
            set { _powerBIGetWorkspacesResult = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerBIImportPbixEventArgs()
        {
            _powerBIGetWorkspacesResult = new PowerBIImportPbixResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="powerBIImportPbixResult"></param>
        public PowerBIImportPbixEventArgs(IPowerBIImportPbixResult powerBIImportPbixResult)
        {
            _powerBIGetWorkspacesResult = powerBIImportPbixResult;
        }

        #endregion
    }
}
