﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aclara.AzurePowerBI.Client.Interfaces;
using Aclara.AzurePowerBI.Client.Results;

namespace Aclara.AzurePowerBI.Client.Events
{
    public class PowerBIGetDatasetsEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private IPowerBIGetDatasetsResult _powerBIGetDatasetsResult;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Power BI get datasets result.
        /// </summary>
        public IPowerBIGetDatasetsResult PowerBIGetDatasetsResult
        {
            get { return _powerBIGetDatasetsResult; }
            set { _powerBIGetDatasetsResult = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerBIGetDatasetsEventArgs()
        {
            _powerBIGetDatasetsResult = new PowerBIGetDatasetsResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="powerBIGetDatasetsResult"></param>
        public PowerBIGetDatasetsEventArgs(IPowerBIGetDatasetsResult powerBIGetDatasetsResult)
        {
            _powerBIGetDatasetsResult = powerBIGetDatasetsResult;
        }

        #endregion
    }
}
