﻿using Aclara.AzurePowerBI.Client.Interfaces;
using Aclara.AzurePowerBI.Client.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AzurePowerBI.Client.Events
{
    public class PowerBICreateWorkspaceEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private IPowerBICreateWorkspaceResult _powerBIGetWorkspacesResult;

        #endregion

        #region Public Properties

        public IPowerBICreateWorkspaceResult PowerBICreateWorkspaceResult
        {
            get { return _powerBIGetWorkspacesResult; }
            set { _powerBIGetWorkspacesResult = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerBICreateWorkspaceEventArgs()
        {
            _powerBIGetWorkspacesResult = new PowerBICreateWorkspaceResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="powerBICreateWorkspaceResult"></param>
        public PowerBICreateWorkspaceEventArgs(IPowerBICreateWorkspaceResult powerBICreateWorkspaceResult)
        {
            _powerBIGetWorkspacesResult = powerBICreateWorkspaceResult;
        }

        #endregion
    }
}