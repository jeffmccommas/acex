﻿using Aclara.AOAlertsDiag.Client.Events;
using Aclara.AOAlertsDiag.Client.Interfaces;
using Aclara.AOAlertsDiag.Client.Results;
using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Aclara.AOAlertsDiag.Client
{
    public class AOAlertsDiagManager
    {
        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Delegates

        public event EventHandler<AAAEventAAAEventArgs> AAAEventAAACompleted;

        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        public AOAlertsDiagManager()
        {

        }

        #endregion

        #region Protected Constructors
        #endregion

        #region Public Methods

        /// <summary>
        /// Example method (rename this method).
        /// </summary>
        /// <param name="operationCount"></param>
        /// <param name="timePerOperationCancellationToken"></param>
        /// <param name=""></param>
        public void ExampleRenameThisMethod(int operationCount, 
                                            int timePerOperationCancellationToken, 
                                            CancellationToken cancellationToken)
        {
            IAAAEventAAAResult aaaEventAAAResult = null;
            AAAEventAAAEventArgs aaaEventAAAEventArgs = null;
            int count = operationCount;

            try
            {
                for (int i = 0; i < count; i++)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    aaaEventAAAEventArgs = new AAAEventAAAEventArgs();

                    //Simulate time consuming operation.
                    System.Threading.Thread.Sleep(timePerOperationCancellationToken);

                    if (AAAEventAAACompleted != null)
                    {
                        AAAEventAAACompleted(this, aaaEventAAAEventArgs);
                    } 
                }
            }
            catch (Exception ex)
            {
                Status status = null;

                aaaEventAAAResult = AAAEventAAAResultFactory.CreateAAAEventAAAnResult();
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                aaaEventAAAResult.StatusList.Add(status);
                if (AAAEventAAACompleted != null)
                {
                    AAAEventAAACompleted(this, aaaEventAAAEventArgs);
                }
            }
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
