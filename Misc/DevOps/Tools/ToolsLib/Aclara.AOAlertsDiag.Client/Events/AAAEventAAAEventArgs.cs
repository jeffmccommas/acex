﻿using Aclara.AOAlertsDiag.Client.Interfaces;
using Aclara.AOAlertsDiag.Client.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AOAlertsDiag.Client.Events
{
    public class AAAEventAAAEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private IAAAEventAAAResult _aaaEventAAAResult;

        #endregion

        #region Public Properties

        public IAAAEventAAAResult AAAEventAAAResult
        {
            get { return _aaaEventAAAResult; }
            set { _aaaEventAAAResult = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AAAEventAAAEventArgs()
        {
            _aaaEventAAAResult = new AAAEventAAAResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="aaaEventAAAResult"></param>
        public AAAEventAAAEventArgs(IAAAEventAAAResult aaaEventAAAResult)
        {
            _aaaEventAAAResult = aaaEventAAAResult;
        }

        #endregion
    }
}
