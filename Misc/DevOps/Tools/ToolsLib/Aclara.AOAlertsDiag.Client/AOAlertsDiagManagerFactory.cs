﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AOAlertsDiag.Client
{
    public static class AOAlertsDiagManagerFactory
    {
        #region Public Methods

        /// <summary>
        /// Create AO Alerts Diag manager.
        /// </summary>
        /// <returns></returns>
        static public AOAlertsDiagManager CreateAOAlertsDiagManager()
        {
            AOAlertsDiagManager result = null;
            AOAlertsDiagManager aoAlertsDiagManagerManager = null;

            try
            {
                aoAlertsDiagManagerManager = new AOAlertsDiagManager();

                result = aoAlertsDiagManagerManager;
                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion
    }
}
