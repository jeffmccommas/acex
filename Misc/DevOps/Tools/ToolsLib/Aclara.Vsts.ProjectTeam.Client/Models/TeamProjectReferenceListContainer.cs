﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Vsts.ProjectTeam.Client.Models
{
    /// <summary>
    /// Team project reference list container.
    /// </summary>
    public class TeamProjectReferenceListContainer
    {



        #region Private Constants
        #endregion

        #region Private Data Members

         public List<Microsoft.TeamFoundation.Core.WebApi.TeamProjectReference> Value;

       #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public TeamProjectReferenceListContainer()
        {

        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
