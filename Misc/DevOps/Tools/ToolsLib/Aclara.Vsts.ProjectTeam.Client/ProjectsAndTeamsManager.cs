﻿using Aclara.Vsts.ProjectTeam.Client.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Aclara.Vsts.ProjectTeam.Client
{

    /// <summary>
    /// Visual Studio Team Services projects and teams manager.
    /// </summary>
    public class ProjectsAndTeamsManager
    {
        #region Private Constants

        #endregion

        #region Private Data Members

        private Uri _teamProjectCollectionUri;
        private string _basicAuthRestApiUserProfileName;
        private string _basicAuthRestApiPassword;

        #endregion

        #region Public Delegates
        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Team project collection uri.
        /// </summary>
        public Uri TeamProjectCollectionUri
        {
            get { return _teamProjectCollectionUri; }
            set
            {
                string teamProjectCollectionUriAsText = string.Empty;

                _teamProjectCollectionUri = value;

                teamProjectCollectionUriAsText = value.ToString();
                if (teamProjectCollectionUriAsText.EndsWith("/") == false)
                {
                    teamProjectCollectionUriAsText = value + @"/";
                    _teamProjectCollectionUri = new Uri(teamProjectCollectionUriAsText);
                }
            }
        }

        /// <summary>
        /// Property: Basic authorization REST API user profile name.
        /// </summary>
        public string BasicAuthRestApiUserProfileName
        {
            get { return _basicAuthRestApiUserProfileName; }
            set { _basicAuthRestApiUserProfileName = value; }
        }

        /// <summary>
        /// Property: Basic authorization REST API password.
        /// </summary>
        public string BasicAuthRestApiPassword
        {
            get { return _basicAuthRestApiPassword; }
            set { _basicAuthRestApiPassword = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="teamProjectCollectionUri"></param>
        /// <param name="basicAuthRestAPIUserProfileName"></param>
        /// <param name="basicAuthRestAPIPassword"></param>
        public ProjectsAndTeamsManager(Uri teamProjectCollectionUri,
                                   string basicAuthRestAPIUserProfileName,
                                   string basicAuthRestAPIPassword)
        {
            _teamProjectCollectionUri = teamProjectCollectionUri;
            _basicAuthRestApiUserProfileName = basicAuthRestAPIUserProfileName;
            _basicAuthRestApiPassword = basicAuthRestAPIPassword;
        }

        #endregion

        #region Protected Constructors
        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve build definition teamProjectReferences asynchronously via web api.
        /// </summary>
        /// <param name="buildId"></param>
        public TeamProjectReferenceList GetTeamProjectReferences()
        {
            TeamProjectReferenceList result = null;
            List<Microsoft.TeamFoundation.Core.WebApi.TeamProjectReference> teamProjectReferences = null;
            string requestUri = String.Empty;
            string parameter = string.Empty;

            try
            {
                result = new TeamProjectReferenceList();

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = this.TeamProjectCollectionUri;
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    parameter = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}",
                                                                                                              this.BasicAuthRestApiUserProfileName,
                                                                                                              this.BasicAuthRestApiPassword)));

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", parameter);

                    requestUri = string.Format("{0}/_apis/build/teamProjectReferences?api-version=2.0",
                                               this.TeamProjectCollectionUri);
                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(requestUri).Result;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {

                        teamProjectReferences = httpResponseMessage.Content.ReadAsAsync<TeamProjectReferenceListContainer>().Result.Value;

                        foreach (Microsoft.TeamFoundation.Core.WebApi.TeamProjectReference teamProjectReference in teamProjectReferences)
                        {
                            result.Add(teamProjectReference);

                        }

                    }
                }
            }
            catch (Exception)
            {

                result = null;
            }
            return result;
        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods
        #endregion

    }
}
