﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.PowerShellHost
{
    public static class PowerShellHostManagerFactory
    {
        #region Public Methods

        /// <summary>
        /// Create PowerShell host manager.
        /// </summary>
        /// <returns></returns>
        static public PowerShellHostManager CreatePowerShellHostManager()
        {
            PowerShellHostManager result = null;
            PowerShellHostManager powerShellHostManager = null;

            try
            {
                powerShellHostManager = new PowerShellHostManager();

                result = powerShellHostManager;
                return result;
            }
            catch (Exception)
            {
                throw;
            }


        }

        #endregion
    }
}
