﻿using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.PowerShellHost.Events
{
    public class PowerShellInvocationCompletedEventArgs : EventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private string _message;
        private StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Message.
        /// </summary>
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerShellInvocationCompletedEventArgs()
        {
            _statusList = new StatusList();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="powerShellInvocationResult"></param>
        public PowerShellInvocationCompletedEventArgs(StatusList statusList)
        {
            _statusList = statusList;
        }

        #endregion
    }
}
