﻿using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.PowerShellHost.Events
{
    public sealed class PowerShellPSCollectionDataAddedEventArgs : EventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private object _data;
        private StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Data.
        /// </summary>
        public object Data
        {
            get { return _data; }
            set { _data = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerShellPSCollectionDataAddedEventArgs()
        {
            _statusList = new StatusList();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="statusList"></param>
        public PowerShellPSCollectionDataAddedEventArgs(StatusList statusList)
        {
            _statusList = statusList;
        }

        #endregion
    }
}
