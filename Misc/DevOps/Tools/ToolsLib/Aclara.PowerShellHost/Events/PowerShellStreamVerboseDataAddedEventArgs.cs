﻿using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.PowerShellHost.Events
{
    public sealed class PowerShellStreamVerboseDataAddedEventArgs : EventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private StatusList _statusList;
        private string _message;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        /// <summary>
        /// Property: Message.
        /// </summary>
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerShellStreamVerboseDataAddedEventArgs()
        {
            _statusList = new StatusList();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="powerShellInvocationResult"></param>
        public PowerShellStreamVerboseDataAddedEventArgs(StatusList statusList)
        {
            _statusList = statusList;
        }

        #endregion
    }
    }
