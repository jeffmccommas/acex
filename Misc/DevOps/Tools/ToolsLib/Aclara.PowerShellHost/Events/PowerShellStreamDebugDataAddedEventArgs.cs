﻿using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.PowerShellHost.Events
{
    public sealed class PowerShellStreamDebugDataAddedEventArgs : EventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private StatusList _statusList;
        private string _message;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        /// <summary>
        /// Property: Message.
        /// </summary>
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerShellStreamDebugDataAddedEventArgs()
        {
            _statusList = new StatusList();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="powerShellStreamDebugDataAddedResult"></param>
        public PowerShellStreamDebugDataAddedEventArgs(StatusList statusList)
        {
            _statusList = statusList;
        }

        #endregion
    }
    }
