﻿using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.PowerShellHost.Events
{
    public sealed class PowerShellStreamProgressDataAddedEventArgs : EventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private StatusList _statusList;
        private string _statusDescription;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        /// <summary>
        /// Property: Status description.
        /// </summary>
        public string StatusDescription
        {
            get { return _statusDescription; }
            set { _statusDescription = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PowerShellStreamProgressDataAddedEventArgs()
        {
            _statusList = new StatusList();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="powerShellStreamProgressDataAddedResult"></param>
        public PowerShellStreamProgressDataAddedEventArgs(StatusList statusList)
        {
            _statusList = statusList;
        }

        #endregion
    }
}
