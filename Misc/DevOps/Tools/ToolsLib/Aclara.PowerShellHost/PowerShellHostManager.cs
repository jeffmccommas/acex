﻿using Aclara.PowerShellHost.Events;
using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Management.Automation;
using System.Text;
using System.Threading;

namespace Aclara.PowerShellHost
{
    public class PowerShellHostManager
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private PowerShell _powerShell = null;
        private PSDataCollection<PSObject>  _psDataCollection;

        #endregion

        #region Public Delegates

        public event EventHandler<PowerShellInvocationBeganEventArgs> PowerShellInvocationBegan;
        public event EventHandler<PowerShellInvocationCompletedEventArgs> PowerShellInvocationCompleted;

        public event EventHandler<PowerShellPSCollectionDataAddedEventArgs> PowerShellPSCollectionDataAdded;

        public event EventHandler<PowerShellStreamErrorDataAddedEventArgs> PowerShellStreamErrorDataAdded;
        public event EventHandler<PowerShellStreamWarningDataAddedEventArgs> PowerShellStreamWarningDataAdded;
        public event EventHandler<PowerShellStreamDebugDataAddedEventArgs> PowerShellStreamDebugDataAdded;
        public event EventHandler<PowerShellStreamVerboseDataAddedEventArgs> PowerShellStreamVerboseDataAdded;
        public event EventHandler<PowerShellStreamProgressDataAddedEventArgs> PowerShellStreamProgressDataAdded;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: PowerShell (instance).
        /// </summary>
        public PowerShell PowerShell
        {
            get { return _powerShell; }
            set { _powerShell = value; }
        }

        /// <summary>
        /// Property: PowerShell data collection
        /// </summary>
        public PSDataCollection<PSObject> PSDataCollection
        {
            get { return _psDataCollection; }
            set { _psDataCollection = value; }
        }

        #endregion

        #region Public Constructors

        public PowerShellHostManager()
        {
            this.SetExecutionPolicy();
        }

        #endregion

        #region Protected Constructors
        #endregion

        #region Public Methods

        /// <summary>
        /// Invoke PowerShell script.
        /// </summary>
        /// <param name="operationCount"></param>
        /// <param name="timePerOperationCancellationToken"></param>
        /// <param name=""></param>
        public void InvokePowerShellScript(string powerShellScript,
                                           CancellationToken cancellationToken)
        {
            Status status = null;
            PowerShellInvocationBeganEventArgs powerShellInvocationBeganEventArgs = null;
            PowerShellInvocationCompletedEventArgs powerShellInvocationCompletedEventArgs = null;

            try
            {

                //Check for cancellation.
                cancellationToken.ThrowIfCancellationRequested();

                if (PowerShellInvocationBegan != null)
                {
                    powerShellInvocationBeganEventArgs = new PowerShellInvocationBeganEventArgs();
                    powerShellInvocationBeganEventArgs.Message = "Invoke PowerShell script (BEGAN).";
                    PowerShellInvocationBegan(this, powerShellInvocationBeganEventArgs);
                }

                using (this.PowerShell = PowerShell.Create())
                {
                    this.PowerShell.AddScript(powerShellScript);

                    this.PSDataCollection = new PSDataCollection<PSObject>();
                    this.PSDataCollection.DataAdded += OnDataAdded_OutputCollection;
                    this.PowerShell.Streams.Error.DataAdded += OnDataAdded_StreamError;
                    this.PowerShell.Streams.Warning.DataAdded += OnDataAdded_StreamWarning;
                    this.PowerShell.Streams.Debug.DataAdded += OnDataAdded_StreamDebug;
                    this.PowerShell.Streams.Verbose.DataAdded += OnDataAdded_StreamVerbose;

                    IAsyncResult result = this.PowerShell.BeginInvoke<PSObject, PSObject>(null, this.PSDataCollection);

                    while (result.IsCompleted == false)
                    {
                        Console.WriteLine("Waiting for pipeline to finish...");
                        Thread.Sleep(1000);
                    }

                    Console.WriteLine("Execution has stopped. The pipeline state: " + this.PowerShell.InvocationStateInfo.State);

                    foreach (PSObject outputItem in this.PSDataCollection)
                    {
                        Console.WriteLine(outputItem.BaseObject.ToString());
                    }
                }

                if (PowerShellInvocationCompleted != null)
                {
                    powerShellInvocationCompletedEventArgs = new PowerShellInvocationCompletedEventArgs();
                    powerShellInvocationCompletedEventArgs.Message = "Invoke PowerShell script (COMPLETED).";
                    PowerShellInvocationCompleted(this, powerShellInvocationCompletedEventArgs);
                }

            }
            catch (Exception ex)
            {

                if (PowerShellInvocationCompleted != null)
                {
                    status = new Status();
                    status.Exception = ex;
                    status.StatusServerity = StatusTypes.StatusSeverity.Error;

                    powerShellInvocationCompletedEventArgs = new PowerShellInvocationCompletedEventArgs();
                    powerShellInvocationCompletedEventArgs.StatusList.Add(status);

                    PowerShellInvocationCompleted(this, powerShellInvocationCompletedEventArgs);
                }
            }
        }

        /// <summary>
        /// Invoke PowerShell script.
        /// </summary>
        /// <param name="operationCount"></param>
        /// <param name="timePerOperationCancellationToken"></param>
        /// <param name=""></param>
        public List<object> InvokePowerShellScriptSync(string powerShellScript, ref StatusList statusList)
        {
            List<object> result = null;
            Status status = null;

            try
            {

                result = new List<object>();


                using (this.PowerShell = PowerShell.Create())
                {
                    this.PowerShell.AddScript(powerShellScript);

                    this.PSDataCollection = new PSDataCollection<PSObject>();


                    Collection<PSObject> psOutput = this.PowerShell.Invoke();


                    foreach (PSObject outputItem in psOutput)
                    {
                        result.Add(outputItem.BaseObject);
                    }

                    foreach (ErrorRecord errorRecord in this.PowerShell.Streams.Error)
                    {
                        status = new Status();
                        status.Exception = errorRecord.Exception;
                        status.StatusServerity = StatusTypes.StatusSeverity.Error;
                        statusList.Add(status);
                    }

                }

            }
            catch (Exception ex)
            {
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                statusList.Add(status);
            }

            return result;

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Event handler for when data is added to the output stream.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnDataAdded_OutputCollection(object sender, DataAddedEventArgs e)
        {

            PowerShellPSCollectionDataAddedEventArgs powerShellPSCollectionDataAddedEventArgs = null;

            try
            {

                PSObject psObject = this.PSDataCollection[e.Index];

                powerShellPSCollectionDataAddedEventArgs = new PowerShellPSCollectionDataAddedEventArgs();
                powerShellPSCollectionDataAddedEventArgs.Data = psObject.BaseObject;
                PowerShellPSCollectionDataAdded?.Invoke(this, powerShellPSCollectionDataAddedEventArgs);

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Event handler for when Data is added to the error stream.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnDataAdded_StreamError(object sender, DataAddedEventArgs e)
        {
            PowerShellStreamErrorDataAddedEventArgs powerShellStreamErrorDataAddedEventArgs = null;
            Status status = null;

            try
            {

                ErrorRecord errorRecord = this.PowerShell.Streams.Error[e.Index];

                status = new Status();
                status.Exception = errorRecord.Exception;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;

                powerShellStreamErrorDataAddedEventArgs = new PowerShellStreamErrorDataAddedEventArgs();
                powerShellStreamErrorDataAddedEventArgs.StatusList.Add(status);
                PowerShellStreamErrorDataAdded?.Invoke(this, powerShellStreamErrorDataAddedEventArgs);

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Event handler for when Data is added to the warning stream.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnDataAdded_StreamWarning(object sender, DataAddedEventArgs e)
        {
            PowerShellStreamWarningDataAddedEventArgs powerShellStreamWarningDataAddedEventArgs = null;

            try
            {

                WarningRecord warningRecord = this.PowerShell.Streams.Warning[e.Index];

                powerShellStreamWarningDataAddedEventArgs = new PowerShellStreamWarningDataAddedEventArgs();
                powerShellStreamWarningDataAddedEventArgs.Message = warningRecord.Message;
                PowerShellStreamWarningDataAdded?.Invoke(this, powerShellStreamWarningDataAddedEventArgs);

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Event handler for when Data is added to the degbug stream.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnDataAdded_StreamDebug(object sender, DataAddedEventArgs e)
        {
            PowerShellStreamDebugDataAddedEventArgs powerShellStreamDebugDataAddedEventArgs = null;

            try
            {

                DebugRecord debugRecord = this.PowerShell.Streams.Debug[e.Index];

                powerShellStreamDebugDataAddedEventArgs = new PowerShellStreamDebugDataAddedEventArgs();
                powerShellStreamDebugDataAddedEventArgs.Message = debugRecord.Message;
                PowerShellStreamDebugDataAdded?.Invoke(this, powerShellStreamDebugDataAddedEventArgs);

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Event handler for when Data is added to the degbug stream.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnDataAdded_StreamProgress(object sender, DataAddedEventArgs e)
        {
            PowerShellStreamProgressDataAddedEventArgs powerShellStreamProgressDataAddedEventArgs = null;

            try
            {

                ProgressRecord progressRecord = this.PowerShell.Streams.Progress[e.Index];

                powerShellStreamProgressDataAddedEventArgs = new PowerShellStreamProgressDataAddedEventArgs();
                powerShellStreamProgressDataAddedEventArgs.StatusDescription = progressRecord.StatusDescription;
                if (PowerShellStreamProgressDataAdded != null)
                {
                    PowerShellStreamProgressDataAdded(this, powerShellStreamProgressDataAddedEventArgs);
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Event handler for when Data is added to the verbose stream.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnDataAdded_StreamVerbose(object sender, DataAddedEventArgs e)
        {
            PowerShellStreamVerboseDataAddedEventArgs powerShellStreamErrorDataAddedEventArgs = null;

            try
            {

                VerboseRecord verboseRecord = this.PowerShell.Streams.Verbose[e.Index];

                powerShellStreamErrorDataAddedEventArgs = new PowerShellStreamVerboseDataAddedEventArgs();
                powerShellStreamErrorDataAddedEventArgs.Message = verboseRecord.Message;
                if (PowerShellStreamVerboseDataAdded != null)
                {
                    PowerShellStreamVerboseDataAdded(this, powerShellStreamErrorDataAddedEventArgs);
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Set execution policy to remote signed.
        /// </summary>
        protected bool SetExecutionPolicy()
        {
            bool result = false;
            string powerShellScript = string.Empty;
            StatusList statusList = null;
            List<object> dataList = null;

            try
            {
                powerShellScript = @"Set-ExecutionPolicy RemoteSigned -Force -Scope process";

                powerShellScript = string.Format(powerShellScript);

                statusList = new StatusList();

                dataList = this.InvokePowerShellScriptSync(powerShellScript, ref statusList);
                foreach (object data in dataList)
                {
                }

                foreach (Status status in statusList)
                {
                    if (status.Exception != null || status.StatusServerity == StatusTypes.StatusSeverity.Error)
                    {
                        throw status.Exception;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        #endregion

        #region Private Methods
        #endregion
    }
}
