﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Interfaces;
using Aclara.TeamFoundation.Build.Client.StatusManagement;

namespace Aclara.TeamFoundation.Build.Client.Results
{

    /// <summary>
    /// Change build definition retention policy result factory.
    /// </summary>
    static public class ChangeBuildDefinitionRetentionPolicyResultFactory
    {

        /// <summary>
        /// Create Change build definition retention policy result.
        /// </summary>
        /// <returns></returns>
        static public IChangeBuildDefinitionRetentionPolicyResult CreateChangeBuildDefinitionRetentionPolicyResult()
        {
            IChangeBuildDefinitionRetentionPolicyResult result = null;
            ChangeBuildDefinitionRetentionPolicyResult changeBuildDefinitionRetentionPolicyResult = null;

            try
            {
                changeBuildDefinitionRetentionPolicyResult = new ChangeBuildDefinitionRetentionPolicyResult();

                result = changeBuildDefinitionRetentionPolicyResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
