﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Interfaces;
using Aclara.TeamFoundation.Build.Client.StatusManagement;

namespace Aclara.TeamFoundation.Build.Client.Results
{

    /// <summary>
    /// Change build definition schedule result factory.
    /// </summary>
    static public class ChangeBuildDefinitionSchedulesResultFactory
    {

        /// <summary>
        /// Create change build definition schedule result.
        /// </summary>
        /// <returns></returns>
        static public IChangeBuildDefinitionSchedulesResult CreateChangeBuildDefinitionScheduleResult()
        {
            IChangeBuildDefinitionSchedulesResult result = null;
            ChangeBuildDefinitionSchedulesResult changeBuildDefinitionScheduleResult = null;

            try
            {
                changeBuildDefinitionScheduleResult = new ChangeBuildDefinitionSchedulesResult();

                result = changeBuildDefinitionScheduleResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
