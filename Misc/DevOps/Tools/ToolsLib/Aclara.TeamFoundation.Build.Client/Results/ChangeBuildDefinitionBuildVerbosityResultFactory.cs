﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Interfaces;
using Aclara.TeamFoundation.Build.Client.StatusManagement;

namespace Aclara.TeamFoundation.Build.Client.Results
{

    /// <summary>
    /// Change build definition build verbosity result factory.
    /// </summary>
    static public class ChangeBuildDefinitionBuildVerbosityResultFactory
    {

        /// <summary>
        /// Create change build definition build verbosity result.
        /// </summary>
        /// <returns></returns>
        static public IChangeBuildDefinitionBuildVerbosityResult CreateChangeBuildDefinitionBuildVerbosityResult()
        {
            IChangeBuildDefinitionBuildVerbosityResult result = null;
            ChangeBuildDefinitionBuildVerbosityResult changeBuildDefinitionBuildVerbosityResult = null;

            try
            {
                changeBuildDefinitionBuildVerbosityResult = new ChangeBuildDefinitionBuildVerbosityResult();

                result = changeBuildDefinitionBuildVerbosityResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
