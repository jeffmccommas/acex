﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Interfaces;
using Aclara.TeamFoundation.Build.Client.StatusManagement;

namespace Aclara.TeamFoundation.Build.Client.Results
{

    /// <summary>
    /// Change build definition build controller result factory.
    /// </summary>
    static public class ChangeBuildDefinitionBuildControllerResultFactory
    {

        /// <summary>
        /// Create Change build definition build controller result.
        /// </summary>
        /// <returns></returns>
        static public IChangeBuildDefinitionBuildControllerResult CreateChangeBuildDefinitionBuildControllerResult()
        {
            IChangeBuildDefinitionBuildControllerResult result = null;
            ChangeBuildDefinitionBuildControllerResult changeBuildDefinitionBuildControllerResult = null;

            try
            {
                changeBuildDefinitionBuildControllerResult = new ChangeBuildDefinitionBuildControllerResult();

                result = changeBuildDefinitionBuildControllerResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
