﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Interfaces;
using Aclara.TeamFoundation.Build.Client.StatusManagement;

namespace Aclara.TeamFoundation.Build.Client.Results
{

    /// <summary>
    /// Enable build definition result factory.
    /// </summary>
    static public class EnableBuildDefinitionResultFactory
    {

        /// <summary>
        /// Create enble build definition result.
        /// </summary>
        /// <returns></returns>
        static public IEnableBuildDefinitionResult CreateEnableBuildDefinitionResult()
        {
            IEnableBuildDefinitionResult result = null;
            EnableBuildDefinitionResult enableBuildDefinitionResult = null;

            try
            {
                enableBuildDefinitionResult = new EnableBuildDefinitionResult();

                result = enableBuildDefinitionResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
