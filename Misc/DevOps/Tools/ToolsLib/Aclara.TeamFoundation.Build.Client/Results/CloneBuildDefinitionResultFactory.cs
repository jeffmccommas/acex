﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Interfaces;
using Aclara.TeamFoundation.Build.Client.StatusManagement;

namespace Aclara.TeamFoundation.Build.Client.Results
{

    /// <summary>
    /// Clone build definition result factory.
    /// </summary>
    static public class CloneBuildDefinitionResultFactory
    {

        /// <summary>
        /// Create clone build definition result.
        /// </summary>
        /// <returns></returns>
        static public ICloneBuildDefinitionResult CreateCloneBuildDefinitionResult()
        {
            ICloneBuildDefinitionResult result = null;
            CloneBuildDefinitionResult cloneBuildDefinitionResult = null;

            try
            {
                cloneBuildDefinitionResult = new CloneBuildDefinitionResult();

                result = cloneBuildDefinitionResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
