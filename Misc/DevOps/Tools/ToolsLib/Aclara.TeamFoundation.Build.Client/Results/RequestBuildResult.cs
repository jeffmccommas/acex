﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.TeamFoundation.Build.Client;
using Microsoft.TeamFoundation.Build.Workflow;
using Microsoft.TeamFoundation.Common;
using Aclara.TeamFoundation.Build.Client.StatusManagement;
using Aclara.TeamFoundation.Build.Client.Interfaces;

namespace Aclara.TeamFoundation.Build.Client.Results
{
    /// <summary>
    /// Queue build definition result.
    /// </summary>
    public class RequestBuildResult : IRequestBuildResult
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IBuildDefinition _buildDefinition;
        private IQueuedBuild _queuedBuild;
        private StatusList _statusList;
        private string _msbuildArguments;
        
        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Build defintion.
        /// </summary>
        public IBuildDefinition BuildDefinition
        {
            get { return _buildDefinition; }
            set { _buildDefinition = value; }
        }

        /// <summary>
        /// Property: Queued build.
        /// </summary>
        public IQueuedBuild QueuedBuild
        {
            get { return _queuedBuild; }
            set { _queuedBuild = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        /// <summary>
        /// Property: MSBuild arguments.
        /// </summary>
        public string MSBuildArguments
        {
            get { return _msbuildArguments; }
            set { _msbuildArguments = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public RequestBuildResult()
        {
            _statusList = new StatusList();
        }

        #endregion
    }
}
