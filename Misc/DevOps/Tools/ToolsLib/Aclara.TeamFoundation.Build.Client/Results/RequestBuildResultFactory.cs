﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Interfaces;
using Aclara.TeamFoundation.Build.Client.StatusManagement;

namespace Aclara.TeamFoundation.Build.Client.Results
{

    /// <summary>
    /// Queue build definition result factory.
    /// </summary>
    static public class RequestBuildResultFactory
    {

        /// <summary>
        /// Create queue build definition result.
        /// </summary>
        /// <returns></returns>
        static public IRequestBuildResult CreateQueueBuildDefinitionResult()
        {
            IRequestBuildResult result = null;
            RequestBuildResult queueBuildDefinitionResult = null;

            try
            {
                queueBuildDefinitionResult = new RequestBuildResult();

                result = queueBuildDefinitionResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
