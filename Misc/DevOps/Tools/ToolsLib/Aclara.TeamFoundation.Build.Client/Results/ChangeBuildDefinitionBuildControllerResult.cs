﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.TeamFoundation.Build.Client;
using Microsoft.TeamFoundation.Build.Workflow;
using Microsoft.TeamFoundation.Common;
using Aclara.TeamFoundation.Build.Client.StatusManagement;
using Aclara.TeamFoundation.Build.Client.Interfaces;

namespace Aclara.TeamFoundation.Build.Client.Results
{
    /// <summary>
    /// Change build definition build controller result.
    /// </summary>
    public class ChangeBuildDefinitionBuildControllerResult : IChangeBuildDefinitionBuildControllerResult
    {

        #region Private Constants
        #endregion

        #region Private Data  Members

        private IBuildDefinition _buildDefinition;
        StatusList _statusList;

        #endregion

        #region Public Properties

        public IBuildDefinition BuildDefinition
        {
            get { return _buildDefinition; }
            set { _buildDefinition = value; }
        }

        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ChangeBuildDefinitionBuildControllerResult()
        {
            _statusList = new StatusList();
        }

        #endregion
    }
}
