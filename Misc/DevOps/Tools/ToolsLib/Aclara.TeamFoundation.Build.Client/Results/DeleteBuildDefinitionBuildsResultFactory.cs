﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Interfaces;
using Aclara.TeamFoundation.Build.Client.StatusManagement;

namespace Aclara.TeamFoundation.Build.Client.Results
{

    /// <summary>
    /// delete build definition builds result factory.
    /// </summary>
    static public class DeleteBuildDefinitionBuildsResultFactory
    {

        /// <summary>
        /// Create delete build definition builds result.
        /// </summary>
        /// <returns></returns>
        static public IDeleteBuildDefinitionBuildsResult CreateDeleteBuildDefinitionBuildsResult()
        {
            IDeleteBuildDefinitionBuildsResult result = null;
            DeleteBuildDefinitionBuildsResult deleteBuildDefinitionBuildsResult = null;

            try
            {
                deleteBuildDefinitionBuildsResult = new DeleteBuildDefinitionBuildsResult();

                result = deleteBuildDefinitionBuildsResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
