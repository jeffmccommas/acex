﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.TeamFoundation.Build.Client.StatusManagement
{
    public class Status
    {


        #region "Private Constants"

        #endregion

        #region "Private Data Members"

        Exception _exception;
        StatusTypes.StatusSeverity _statusServerity;

        #endregion

        #region "Public Properties"

        /// <summary>
        /// Property: Exception
        /// </summary>
        public Exception Exception
        {
            get
            {
                return _exception;
            }
            set
            {
                _exception = value;
            }
        }

        /// <summary>
        /// Property: Status serverity.
        /// </summary>
        public StatusTypes.StatusSeverity StatusServerity
        {
            get
            {
                return _statusServerity;
            }
            set
            {
                _statusServerity = value;
            }
        }

        #endregion

        #region "Protected Properties"

        #endregion

        #region "Public Constructors"

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Status()
        {
            _exception = null;
            _statusServerity = StatusTypes.StatusSeverity.Unspecified;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public Status(Exception exception, StatusTypes.StatusSeverity statusServerity)
        {
            _exception = exception;
            _statusServerity = statusServerity;
        }

        #endregion

        #region "Protected Constructors"

        #endregion

        #region "Public Methods"

        #endregion

        #region "Protected Methods"

        #endregion

    }
}
