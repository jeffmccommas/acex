﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.TeamFoundation.Build.Client;
using Microsoft.TeamFoundation.Build.Workflow;
using Microsoft.TeamFoundation.Build.Workflow.Activities;
using Microsoft.TeamFoundation.Client;

namespace Aclara.TeamFoundation.Build.Client
{

    /// <summary>
    /// Build definition manager factory.
    /// </summary>
    public static class BuildDefinitionManagerFactory
    {

        #region Private Constants

        #endregion

        #region Public Methods

        /// <summary>
        /// Create build definition manager.
        /// </summary>
        /// <param name="tfsServerUri"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        static public BuildDefinitionManager CreateBuildDefinitionManager(Uri tfsServerUri,
                                                                          string teamProjectName)
        {
            BuildDefinitionManager result = null;
            BuildDefinitionManager buildDefinitionManager = null;
            IBuildServer buildServer = null;

            try
            {

                buildServer = GetBuildServer(tfsServerUri);

                buildDefinitionManager = new BuildDefinitionManager(buildServer);

                result = buildDefinitionManager;
                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Retrieve build server.
        /// </summary>
        /// <param name="tfsTeamProjectCollection"></param>
        /// <returns></returns>
        static public IBuildServer GetBuildServer(Uri tfsServerUri)
        {
            IBuildServer result = null;
            IBuildServer buildServer = null;
            TfsTeamProjectCollection tfsTeamProjectCollection = null;
            Object service = null;

            try
            {
                tfsTeamProjectCollection = new TfsTeamProjectCollection(tfsServerUri);

                tfsTeamProjectCollection.EnsureAuthenticated();

                service = tfsTeamProjectCollection.GetService(typeof(IBuildServer));
                if (service is IBuildServer)
                {
                    buildServer = (IBuildServer)service;
                }

                result = buildServer;

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region Protected Methods

        #endregion

    }
}
