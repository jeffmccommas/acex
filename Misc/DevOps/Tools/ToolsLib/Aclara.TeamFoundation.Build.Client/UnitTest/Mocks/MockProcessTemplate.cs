﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.TeamFoundation.Build.Client;
using Microsoft.TeamFoundation.Client;

namespace UnitTest
{

    /// <summary>
    /// Mock process template.
    /// </summary>
    public class MockProcessTemplate : IProcessTemplate
    {
        #region IProcessTemplate Members

        #region Private Data Members

        private string _description = string.Empty;
        private BuildReason _supportedReasons;
        private string _parameters = string.Empty;
        private string _serverPath = string.Empty;
        private string _teamProjectName = string.Empty;
        private ProcessTemplateType _processTemplateType;

        #endregion

        #region Public Properties

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public BuildReason SupportedReasons
        {
            get
            {
                return _supportedReasons;
            }
            set
            {
                _supportedReasons = value;
            }
        }

        public string Parameters
        {
            get
            { 
                return _parameters; 
            }
        }


        public string ServerPath
        {
            get 
            {
                return _serverPath;
            }
        }

        public string TeamProject
        {
            get 
            {
                return _teamProjectName;
            }
        }

        public ProcessTemplateType TemplateType
        {
            get
            {
                return _processTemplateType;
            }
            set
            {
                _processTemplateType = value;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="serverPath"></param>
        /// <param name="teamProjectName"></param>
        public MockProcessTemplate(string parameters,
                                   string serverPath,
                                   string teamProjectName)
        {
            this._parameters = parameters;
            this._serverPath = serverPath;
            this._teamProjectName = teamProjectName;
        }

        #endregion

        #region Public Methods

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public string Download(string versionSpec)
        {
            throw new NotImplementedException();
        }

        public string Download()
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        #endregion

        #endregion

        public void CopyFrom(IProcessTemplate source)
        {
            throw new NotImplementedException();
        }

        public int Id
        {
            get { throw new NotImplementedException(); }
        }

        public Version Version
        {
            get { throw new NotImplementedException(); }
        }
    }
}
