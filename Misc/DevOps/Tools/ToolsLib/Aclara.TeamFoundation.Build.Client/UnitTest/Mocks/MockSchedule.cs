﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.TeamFoundation.Build.Client;
using Microsoft.TeamFoundation.Client;

namespace UnitTest.Mocks
{
    public class MockSchedule : ISchedule
    {

        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion



        IBuildDefinition _buildDefinition = null;
        ScheduleDays _daysToBuild;
        int _startTime;
        TimeZoneInfo _timeZoneInfo = null;
        ScheduleType _type;

        #region Public Properties

        public IBuildDefinition BuildDefinition
        {
            get
            {
                return _buildDefinition;
            }
        }

        public ScheduleDays DaysToBuild
        {
            get
            {
                return _daysToBuild;
            }
            set
            {
                _daysToBuild = value;
            }
        }

        public int StartTime
        {
            get
            {
                return _startTime;
            }
            set
            {
                _startTime = value;
            }
        }

        public TimeZoneInfo TimeZone
        {
            get
            {
                return _timeZoneInfo;
            }
            set
            {
                _timeZoneInfo = value;
            }
        }

        public ScheduleType Type
        {
            get 
            {
                return _type;
            }
        }
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public MockSchedule()
        {

        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="daysToBuild"></param>
        /// <param name="_startTime"></param>
        /// <param name="_timeZoneInfo"></param>
        /// <param name="_type"></param>
        public MockSchedule(IBuildDefinition buildDefinition, 
                            ScheduleDays daysToBuild, 
                            int startTime,
                            TimeZoneInfo timeZoneInfo,
                            ScheduleType type)
        {
            _buildDefinition = buildDefinition;
            _daysToBuild = daysToBuild;
            _startTime = startTime;
            _timeZoneInfo = timeZoneInfo;
            _type = type;
        }

        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

    }
}
