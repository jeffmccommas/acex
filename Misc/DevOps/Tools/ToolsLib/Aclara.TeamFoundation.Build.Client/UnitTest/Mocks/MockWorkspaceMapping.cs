﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.TeamFoundation.Build.Client;
using Microsoft.TeamFoundation.Client;

namespace UnitTest
{
    public class MockWorkspaceMapping: IWorkspaceMapping
    {

        #region Private Data Members

        private WorkspaceMappingDepth _workspaceMappingDepth;
        private string _localItem = string.Empty;
        private WorkspaceMappingType _workspaceMappingType;
        private string _serverItem = string.Empty;

        #endregion

        #region IWorkspaceMapping Members

        public WorkspaceMappingDepth Depth
        {
            get
            {
                return _workspaceMappingDepth;
            }
            set
            {
                _workspaceMappingDepth = value;
            }
        }

        public string LocalItem
        {
            get
            {
                return _localItem;
            }
            set
            {
                _localItem = value;
            }
        }

        public WorkspaceMappingType MappingType
        {
            get
            {
                return _workspaceMappingType;
            }
            set
            {
                _workspaceMappingType = value;
            }
        }
        public string ServerItem
        {
            get
            {
                return _serverItem;
            }
            set
            {
                _serverItem = value;
            }
        }

        #endregion
    }
}
