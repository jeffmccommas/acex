﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.TeamFoundation.Build.Client;
using Microsoft.TeamFoundation.Client;

namespace UnitTest.Mocks
{
    public class MockRetentionPolicy : IRetentionPolicy
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IBuildDefinition _buildDefinition = null;
        private BuildReason _buildReason;
        private BuildStatus _buildStatus;
        private DeleteOptions _deleteOptions;
        private int _numberToKeep;

        #endregion

        #region Public Properties

        public IBuildDefinition BuildDefinition
        {
            get { throw new NotImplementedException(); }
        }

        public BuildReason BuildReason
        {
            get
            {
                return _buildReason;
            }
            set
            {
                _buildReason = value;
            }
        }

        public BuildStatus BuildStatus
        {
            get
            {
                return _buildStatus;
            }
            set
            {
                _buildStatus = value;
            }
        }

        public DeleteOptions DeleteOptions
        {
            get
            {
                return _deleteOptions;
            }
            set
            {
                _deleteOptions = value;
            }
        }

        public int NumberToKeep
        {
            get
            {
                return _numberToKeep;
            }
            set
            {
                _numberToKeep = value;
            }
        }
        #endregion

        #region Public Constructors
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MockRetentionPolicy()
        {

        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="buildReason"></param>
        /// <param name="buildStatus"></param>
        /// <param name="deleteOptions"></param>
        /// <param name="numberToKeep"></param>
        public MockRetentionPolicy(IBuildDefinition buildDefinition,
                                   BuildReason buildReason,
                                   BuildStatus buildStatus,
                                   DeleteOptions deleteOptions,
                                   int numberToKeep)
        {

            _buildDefinition = buildDefinition;
            _buildReason = buildReason;
            _buildStatus = buildStatus;
            _deleteOptions = deleteOptions;
            _numberToKeep = numberToKeep;

        }
        
        #endregion
    }
}
