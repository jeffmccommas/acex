﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Aclara.TeamFoundation.Build.Client;
using Aclara.TeamFoundation.Build.Client.Events;
using Aclara.TeamFoundation.Build.Client.Interfaces;
using Microsoft.TeamFoundation.Build.Client;
using Microsoft.TeamFoundation.Build.Workflow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTest.TestFactories;
using UnitTest.TestSetup;

namespace UnitTest
{


    /// <summary>
    ///This is a test class for BuildDefinitionManagerTest and is intended
    ///to contain all BuildDefinitionManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BuildDefinitionManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for BuildDefinitionTestSetup.
        ///</summary>
        [TestMethod()]
        public void BuildDefinitionTestSetupTest()
        {

            IBuildDefinition buildDefinition = null;

            buildDefinition = BuildDefinitionTestSetup.CreateBuildDefinition(TestFactoryTypes.TestScenario.CreateBuildDefinition01);
            Assert.IsNotNull(buildDefinition, string.Format("Expected non-null build definition from BuildDefinitionTestSetup.CreateBuildDefinition() method."));

            buildDefinition.Save();

        }

        /// <summary>
        ///A test for ChangeBuildDefinitionBuildController
        ///</summary>
        [TestMethod()]
        public void ChangeBuildDefinitionBuildControllerTest()
        {
            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            string teamProjectName = null;
            string buildDefinitionNamePrefix = string.Empty;
            BuildDefinitionList buildDefinitionList = null;
            bool buildControllerSpecified = false;
            IBuildController buildController = null;
            string buildControllerName = string.Empty;
            bool agentSettingsTagSpecified = false;
            string tag = string.Empty;
            bool dropFolderSpecified = false;
            string dropFolderSource = string.Empty;
            string dropFolderTarget = string.Empty;
            IChangeBuildDefinitionBuildControllerResult actual = null;

            target = new BuildDefinitionManager(BuildDefinitionManagerTestFactory.GetBuildServer(BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.DeleteBuildDefinitionBuilds01)));
            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01);
            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01);
            buildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01);
            buildControllerName = BuildDefinitionManagerTestFactory.GetBuildControllerName(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01);
            buildController = BuildDefinitionManagerTestFactory.GetBuildController(tfsServerUri, buildControllerName);

            buildDefinitionList = target.GetBuildDefinitionList(teamProjectName);
            var subsetBuildDefinitionList = buildDefinitionList.Where(bd => bd.Name.ToLower().StartsWith(buildDefinitionNamePrefix.ToLower()));

            foreach (IBuildDefinition buildDefinition in subsetBuildDefinitionList)
            {
                buildControllerSpecified = true;
                actual = target.ChangeBuildDefinitionBuildController(buildDefinition,
                                                                     buildControllerSpecified,
                                                                     buildController,
                                                                     agentSettingsTagSpecified,
                                                                     tag,
                                                                     dropFolderSpecified,
                                                                     dropFolderSource,
                                                                     dropFolderTarget);
                Assert.IsNotNull(actual, string.Format("Expected non-null IChangeBuildDefinitionBuildControllerResult from ChangeBuildDefinitionBuildController() method."));

            }

        }

        /// <summary>
        ///A test for ChangeBuildDefinitionBuildController
        ///</summary>
        [TestMethod()]
        public void ChangeBuildDefinitionBuildControllerTest1()
        {
            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            string teamProjectName = null;
            string buildDefinitionNamePrefix = string.Empty;
            BuildDefinitionList buildDefinitionList = null;
            BuildDefinitionList subsetBuildDefinitionList = null;
            bool buildControllerSpecified = false;
            IBuildController buildController = null;
            string buildControllerName = string.Empty;
            bool agentSettingsTagSpecified = false;
            string tag = string.Empty;
            bool dropFolderSpecified = false;
            string dropFolderSource = string.Empty;
            string dropFolderTarget = string.Empty;
            IChangeBuildDefinitionBuildControllerResult actual = null;



            target = new BuildDefinitionManager(BuildDefinitionManagerTestFactory.GetBuildServer(BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01)));
            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01);
            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01);
            buildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01);
            buildControllerName = BuildDefinitionManagerTestFactory.GetBuildControllerName(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01);
            buildController = BuildDefinitionManagerTestFactory.GetBuildController(tfsServerUri, buildControllerName);

            buildDefinitionList = target.GetBuildDefinitionList(teamProjectName);
            subsetBuildDefinitionList = buildDefinitionList.FindByNamePrefix(buildDefinitionNamePrefix);

            target.BuildDefinitionBuildControllerChanged += (Object sender, ChangeBuildDefinitionBuildControllerEventArgs changeBuildDefinitionBuildControllerEventArgs) =>
                {
                    System.Diagnostics.Debug.WriteLine(string.Format("OnBuildDefinitionBuildControllerChanged: Build definition name = {0}",
                                                                     changeBuildDefinitionBuildControllerEventArgs.ChangeBuildDefinitionBuildControllerResult.BuildDefinition.Name));
                };

            target.ChangeBuildDefinitionBuildController(subsetBuildDefinitionList,
                                                        buildControllerSpecified,
                                                        buildController,
                                                        agentSettingsTagSpecified,
                                                        tag,
                                                        dropFolderSpecified,
                                                        dropFolderSource,
                                                        dropFolderTarget);
        }

        /// <summary>
        ///A test for ChangeBuildDefinitionBuildVerbosity
        ///</summary>
        [TestMethod()]
        public void ChangeBuildDefinitionBuildVerbosityTest()
        {

            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            string teamProjectName = null;
            string buildDefinitionNamePrefix = string.Empty;
            BuildDefinitionList buildDefinitionList = null;
            BuildVerbosity buildVerbosity = BuildVerbosity.Minimal;
            IChangeBuildDefinitionBuildVerbosityResult actual;

            target = new BuildDefinitionManager(BuildDefinitionManagerTestFactory.GetBuildServer(BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.DeleteBuildDefinitionBuilds01)));
            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01);
            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01);
            buildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01);
            buildVerbosity = BuildDefinitionManagerTestFactory.GetBuildVerbosity(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildVerbostiy01);


            buildDefinitionList = target.GetBuildDefinitionList(teamProjectName);
            var subsetBuildDefinitionList = buildDefinitionList.Where(bd => bd.Name.ToLower().StartsWith(buildDefinitionNamePrefix.ToLower()));

            foreach (IBuildDefinition buildDefinition in subsetBuildDefinitionList)
            {
                actual = target.ChangeBuildDefinitionBuildVerbosity(buildDefinition, buildVerbosity);
                Assert.IsNotNull(actual, string.Format("Expected non-null IChangeBuildDefinitionBuildVerbosityResult from ChangeBuildDefinitionBuildVerbosity() method."));
            }

        }

        /// <summary>
        ///A test for ChangeBuildDefinitionBuildVerbosity
        ///</summary>
        [TestMethod()]
        public void ChangeBuildDefinitionBuildVerbosityTest1()
        {

            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            string teamProjectName = null;
            string buildDefinitionNamePrefix = string.Empty;
            BuildDefinitionList buildDefinitionList = null;
            BuildDefinitionList subsetBuildDefinitionList = null;
            BuildVerbosity buildVerbosity = BuildVerbosity.Minimal;

            target = new BuildDefinitionManager(BuildDefinitionManagerTestFactory.GetBuildServer(BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.DeleteBuildDefinitionBuilds01)));
            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01);
            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01);
            buildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01);
            buildVerbosity = BuildDefinitionManagerTestFactory.GetBuildVerbosity(TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildVerbostiy01);

            buildDefinitionList = target.GetBuildDefinitionList(teamProjectName);
            subsetBuildDefinitionList = buildDefinitionList.FindByNamePrefix(buildDefinitionNamePrefix);

            target.BuildDefinitionBuildVerbosityChanged += (Object sender, ChangeBuildDefinitionBuildVerbosityEventArgs changeBuildDefinitionBuildVerbosityEventArgs) =>
            {
                System.Diagnostics.Debug.WriteLine(string.Format("OnBuildDefinitionBuildVerbosityChanged: Build definition name = {0}",
                                                                 changeBuildDefinitionBuildVerbosityEventArgs.ChangeBuildDefinitionBuildVerbosityResult.BuildDefinition.Name));
            };

            target.ChangeBuildDefinitionBuildVerbosity(subsetBuildDefinitionList, buildVerbosity);

        }

        /// <summary>
        ///A test for ChangeBuildDefinitionRetentionPolicy
        ///</summary>
        [TestMethod()]
        public void ChangeBuildDefinitionRetentionPolicyTest()
        {
            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            string teamProjectName = null;
            string buildDefinitionNamePrefix = string.Empty;
            BuildDefinitionList buildDefinitionList = null;
            IBuildServer buildServer = null;
            int numberToKeep;
            IChangeBuildDefinitionRetentionPolicyResult actual = null;

            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.ChangeBuildDefinitionRetentionPolicy01);
            buildServer = BuildDefinitionManagerTestFactory.GetBuildServer(tfsServerUri);
            target = new BuildDefinitionManager(buildServer);
            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.ChangeBuildDefinitionRetentionPolicy01);
            buildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.ChangeBuildDefinitionRetentionPolicy01);

            numberToKeep = BuildDefinitionManagerTestFactory.GetRetentionPolicyNumberToKeep(TestFactoryTypes.TestScenario.ChangeBuildDefinitionRetentionPolicy01);

            buildDefinitionList = target.GetBuildDefinitionList(teamProjectName);
            var subsetBuildDefinitionList = buildDefinitionList.Where(bd => bd.Name.ToLower().StartsWith(buildDefinitionNamePrefix.ToLower()));

            foreach (IBuildDefinition buildDefinition in subsetBuildDefinitionList)
            {
                actual = target.ChangeBuildDefinitionRetentionPolicy(buildDefinition, numberToKeep);
                Assert.IsNotNull(actual, string.Format("Expected non-null IChangeBuildDefinitionRetentionPolicyResult from ChangeBuildDefinitionRetentionPolicy() method."));
            }
        }

        /// <summary>
        ///A test for ChangeBuildDefinitionRetentionPolicy
        ///</summary>
        [TestMethod()]
        public void ChangeBuildDefinitionRetentionPolicyTest1()
        {
            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            string teamProjectName = null;
            string buildDefinitionNamePrefix = string.Empty;
            BuildDefinitionList buildDefinitionList = null;
            BuildDefinitionList subsetBuildDefinitionList = null;
            IBuildServer buildServer = null;
            int numberToKeep;
            IChangeBuildDefinitionRetentionPolicyResult actual = null;

            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.ChangeBuildDefinitionRetentionPolicy01);
            buildServer = BuildDefinitionManagerTestFactory.GetBuildServer(tfsServerUri);
            target = new BuildDefinitionManager(buildServer);
            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.ChangeBuildDefinitionRetentionPolicy01);
            buildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.ChangeBuildDefinitionRetentionPolicy01);

            numberToKeep = BuildDefinitionManagerTestFactory.GetRetentionPolicyNumberToKeep(TestFactoryTypes.TestScenario.ChangeBuildDefinitionRetentionPolicy01);

            buildDefinitionList = target.GetBuildDefinitionList(teamProjectName);
            subsetBuildDefinitionList = buildDefinitionList.FindByNamePrefix(buildDefinitionNamePrefix);

            target.BuildDefinitionRetentionPolicyChanged += (Object sender, ChangeBuildDefinitionRetentionPolicyEventArgs changeBuildDefinitionRetentionPolicyEventArgs) =>
            {
                System.Diagnostics.Debug.WriteLine(string.Format("OnBuildDefinitionRetentionPolicyChanged: Build definition name = {0}",
                                                                 changeBuildDefinitionRetentionPolicyEventArgs.ChangeBuildDefinitionRetentionPolicyResult.BuildDefinition.Name));
            };

            target.ChangeBuildDefinitionRetentionPolicy(subsetBuildDefinitionList, numberToKeep);
        }

        /// <summary>
        ///A test for ChangeBuildDefinitionSchedules
        ///</summary>
        [TestMethod()]
        public void ChangeBuildDefinitionSchedulesTest()
        {
            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            string teamProjectName = null;
            string buildDefinitionNamePrefix = string.Empty;
            BuildDefinitionList buildDefinitionList = null;
            ScheduleDays daysToBuild;
            IBuildServer buildServer = null;
            IChangeBuildDefinitionSchedulesResult actual = null;

            int startTime;

            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.ChangeBuildDefinitionSchedule01);
            buildServer = BuildDefinitionManagerTestFactory.GetBuildServer(tfsServerUri);
            target = new BuildDefinitionManager(buildServer);
            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.ChangeBuildDefinitionSchedule01);
            buildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.ChangeBuildDefinitionSchedule01);

            daysToBuild = BuildDefinitionManagerTestFactory.GetScheduleDays(TestFactoryTypes.TestScenario.ChangeBuildDefinitionSchedule01);
            startTime = BuildDefinitionManagerTestFactory.GetScheduleStartTime(TestFactoryTypes.TestScenario.ChangeBuildDefinitionSchedule01);

            buildDefinitionList = target.GetBuildDefinitionList(teamProjectName);
            var subsetBuildDefinitionList = buildDefinitionList.Where(bd => bd.Name.ToLower().StartsWith(buildDefinitionNamePrefix.ToLower()));

            foreach (IBuildDefinition buildDefinition in subsetBuildDefinitionList)
            {
                actual = target.ChangeBuildDefinitionSchedules(buildDefinition, daysToBuild, startTime);
                Assert.IsNotNull(actual, string.Format("Expected non-null IChangeBuildDefinitionSchedulesResult from ChangeBuildDefinitionSchedules() method."));
            }
        }

        /// <summary>
        ///A test for ChangeBuildDefinitionSchedules
        ///</summary>
        [TestMethod()]
        public void ChangeBuildDefinitionSchedulesTest1()
        {
            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            IBuildServer buildServer = null;
            string teamProjectName = null;
            string buildDefinitionNamePrefix = string.Empty;
            BuildDefinitionList buildDefinitionList = null;
            BuildDefinitionList subsetBuildDefinitionList = null;
            ScheduleDays daysToBuild;
            IChangeBuildDefinitionSchedulesResult actual = null;

            int startTime;

            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.ChangeBuildDefinitionSchedule01);
            buildServer = BuildDefinitionManagerTestFactory.GetBuildServer(tfsServerUri);
            target = new BuildDefinitionManager(buildServer);
            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.ChangeBuildDefinitionSchedule01);
            buildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.ChangeBuildDefinitionSchedule01);

            daysToBuild = BuildDefinitionManagerTestFactory.GetScheduleDays(TestFactoryTypes.TestScenario.ChangeBuildDefinitionSchedule01);
            startTime = BuildDefinitionManagerTestFactory.GetScheduleStartTime(TestFactoryTypes.TestScenario.ChangeBuildDefinitionSchedule01);

            buildDefinitionList = target.GetBuildDefinitionList(teamProjectName);
            subsetBuildDefinitionList = buildDefinitionList.FindByNamePrefix(buildDefinitionNamePrefix);

            target.BuildDefinitionSchedulesChanged += (Object sender, ChangeBuildDefinitionSchedulesEventArgs changeBuildDefinitionSchedulesEventArgs) =>
            {
                System.Diagnostics.Debug.WriteLine(string.Format("OnBuildDefinitionSchedulesChanged: Build definition name = {0}",
                                                                 changeBuildDefinitionSchedulesEventArgs.ChangeBuildDefinitionSchedulesResult.BuildDefinition.Name));
            };

            target.ChangeBuildDefinitionSchedules(subsetBuildDefinitionList, daysToBuild, startTime);
        }

        /// <summary>
        ///A test for CloneBuildDefinition
        ///</summary>
        [TestMethod()]
        public void CloneBuildDefinitionTest()
        {
            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            IBuildServer buildServer = null;
            string teamProjectName = null;
            string sourceBuildDefinitionNamePrefix = string.Empty;
            string targetBuildDefinitionNamePrefix = string.Empty;
            string sourceBranchName = string.Empty;
            string targetBranchName = string.Empty;
            BuildDefinitionList buildDefinitionList = null;
            ICloneBuildDefinitionResult actual = null;

            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.CloneBuildDefinition01);
            buildServer = BuildDefinitionManagerTestFactory.GetBuildServer(tfsServerUri);

            target = new BuildDefinitionManager(buildServer);

            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.CloneBuildDefinition01);
            sourceBuildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetSourceBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.CloneBuildDefinition01);
            targetBuildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetTargetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.CloneBuildDefinition01);
            sourceBranchName = BuildDefinitionManagerTestFactory.GetSourceBranchName(TestFactoryTypes.TestScenario.CloneBuildDefinition01);
            targetBranchName = BuildDefinitionManagerTestFactory.GetTargetBranchName(TestFactoryTypes.TestScenario.CloneBuildDefinition01);

            buildDefinitionList = target.GetBuildDefinitionList(BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.CloneBuildDefinition01));
            var subsetBuildDefinitionList = buildDefinitionList.Where(bd => bd.Name.ToLower().Contains(sourceBuildDefinitionNamePrefix.ToLower()));

            foreach (IBuildDefinition sourceBuildDefinition in subsetBuildDefinitionList)
            {
                actual = target.CloneBuildDefinition(teamProjectName,
                                                     sourceBuildDefinition,
                                                     sourceBuildDefinitionNamePrefix,
                                                     targetBuildDefinitionNamePrefix,
                                                     sourceBranchName,
                                                     targetBranchName);
                Assert.IsNotNull(actual, string.Format("Expected non-null ICloneBuildDefinitionResult from CloneBuildDefinition() method."));
            }

        }

        /// <summary>
        ///A test for CloneBuildDefinition
        ///</summary>
        [TestMethod()]
        public void CloneBuildDefinitionTest1()
        {
            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            IBuildServer buildServer = null;
            string teamProjectName = null;
            BuildDefinitionList buildDefinitionList = null;
            BuildDefinitionList subsetBuildDefinitionList = null;
            string sourceBuildDefinitionNamePrefix = string.Empty;
            string targetBuildDefinitionNamePrefix = string.Empty;
            string sourceBranchName = string.Empty;
            string targetBranchName = string.Empty;

            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.CloneBuildDefinition01);
            buildServer = BuildDefinitionManagerTestFactory.GetBuildServer(tfsServerUri);
            target = new BuildDefinitionManager(buildServer);
            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.CloneBuildDefinition01);
            sourceBuildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetSourceBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.CloneBuildDefinition01);
            targetBuildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetTargetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.CloneBuildDefinition01);
            sourceBranchName = BuildDefinitionManagerTestFactory.GetSourceBranchName(TestFactoryTypes.TestScenario.CloneBuildDefinition01);
            targetBranchName = BuildDefinitionManagerTestFactory.GetTargetBranchName(TestFactoryTypes.TestScenario.CloneBuildDefinition01);

            buildDefinitionList = target.GetBuildDefinitionList(BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.CloneBuildDefinition01));
            subsetBuildDefinitionList = buildDefinitionList.FindByNamePrefix(sourceBuildDefinitionNamePrefix);

            target.BuildDefinitionCloned += (Object sender, CloneBuildDefinitionEventArgs cloneBuildDefinitionEventArgs) =>
            {
                System.Diagnostics.Debug.WriteLine(string.Format("OnBuildDefinitionCloned: Build definition name = {0}",
                                                                 cloneBuildDefinitionEventArgs.CloneBuildDefinitionResult.BuildDefinition.Name));
            };

            target.CloneBuildDefinition(subsetBuildDefinitionList,
                                        teamProjectName,
                                        sourceBuildDefinitionNamePrefix,
                                        targetBuildDefinitionNamePrefix,
                                        sourceBranchName,
                                        targetBranchName);
        }

        /// <summary>
        ///A test for DeleteBuildDefinitionBuilds
        ///</summary>
        [TestMethod()]
        public void DeleteBuildDefinitionBuildsTest()
        {
            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            string teamProjectName = null;
            string buildDefinitionNamePrefix = string.Empty;
            BuildDefinitionList buildDefinitionList = null;
            IBuildServer buildServer = null;
            IDeleteBuildDefinitionBuildsResult actual = null;

            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.DeleteBuildDefinitionBuilds01);
            buildServer = BuildDefinitionManagerTestFactory.GetBuildServer(tfsServerUri);

            target = new BuildDefinitionManager(buildServer);

            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.DeleteBuildDefinitionBuilds01);
            buildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.DeleteBuildDefinitionBuilds01);

            buildDefinitionList = target.GetBuildDefinitionList(teamProjectName);
            var subsetBuildDefinitionList = buildDefinitionList.Where(bd => bd.Name.ToLower().StartsWith(buildDefinitionNamePrefix.ToLower()));

            foreach (IBuildDefinition buildDefinition in subsetBuildDefinitionList)
            {
                actual = target.DeleteBuildDefinitionBuilds(buildDefinition);
                Assert.IsNotNull(actual, string.Format("Expected non-null IDeleteBuildDefinitionBuildsResult from DeleteBuildDefinitionBuilds() method."));
            }
        }

        /// <summary>
        ///A test for DeleteBuildDefinitionBuilds
        ///</summary>
        [TestMethod()]
        public void DeleteBuildDefinitionBuildsTest1()
        {
            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            string teamProjectName = null;
            string buildDefinitionNamePrefix = string.Empty;
            BuildDefinitionList buildDefinitionList = null;
            BuildDefinitionList subsetBuildDefinitionList = null;
            IBuildServer buildServer = null;
            IDeleteBuildDefinitionBuildsResult actual = null;

            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.DeleteBuildDefinitionBuilds01);
            buildServer = BuildDefinitionManagerTestFactory.GetBuildServer(tfsServerUri);

            target = new BuildDefinitionManager(buildServer);

            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.DeleteBuildDefinitionBuilds01);
            buildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.DeleteBuildDefinitionBuilds01);

            buildDefinitionList = target.GetBuildDefinitionList(teamProjectName);
            subsetBuildDefinitionList = buildDefinitionList.FindByNamePrefix(buildDefinitionNamePrefix);

            target.BuildDefinitionBuildsDeleted += (Object sender, DeleteBuildDefinitionBuildsEventArgs deleteBuildDefinitionBuildsEventArgs) =>
            {
                System.Diagnostics.Debug.WriteLine(string.Format("OnBuildDefinitionBuildsDeleted: Build definition name = {0}",
                                                                 deleteBuildDefinitionBuildsEventArgs.DeleteBuildDefinitionBuildsResult.BuildDefinition.Name));
            };

            target.DeleteBuildDefinitionBuilds(subsetBuildDefinitionList);

        }

        /// <summary>
        ///A test for EnableBuildDefinition
        ///</summary>
        [TestMethod()]
        public void EnableBuildDefinitionTest()
        {
            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            string teamProjectName = null;
            string buildDefinitionNamePrefix = string.Empty;
            BuildDefinitionList buildDefinitionList = null;
            IBuildServer buildServer = null;
            bool enableValue = false;
            IEnableBuildDefinitionResult actual = null;

            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.EnableBuildDefinition01);
            buildServer = BuildDefinitionManagerTestFactory.GetBuildServer(tfsServerUri);

            target = new BuildDefinitionManager(buildServer);

            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.EnableBuildDefinition01);
            buildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.EnableBuildDefinition01);

            buildDefinitionList = target.GetBuildDefinitionList(teamProjectName);
            var subsetBuildDefinitionList = buildDefinitionList.Where(bd => bd.Name.ToLower().StartsWith(buildDefinitionNamePrefix.ToLower()));

            enableValue = true;
            foreach (IBuildDefinition buildDefinition in subsetBuildDefinitionList)
            {
                actual = target.EnableBuildDefinition(buildDefinition, enableValue);
                Assert.IsNotNull(actual, string.Format("Expected non-null IEnableBuildDefinitionResult from EnableBuildDefinition() method."));
            }

        }

        /// <summary>
        ///A test for EnableBuildDefinition
        ///</summary>
        [TestMethod()]
        public void EnableBuildDefinitionTest1()
        {
            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            string teamProjectName = null;
            string buildDefinitionNamePrefix = string.Empty;
            BuildDefinitionList buildDefinitionList = null;
            BuildDefinitionList subsetBuildDefinitionList = null;
            IBuildServer buildServer = null;
            bool enableValue = false;
            IEnableBuildDefinitionResult actual = null;

            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.EnableBuildDefinition01);
            buildServer = BuildDefinitionManagerTestFactory.GetBuildServer(tfsServerUri);

            target = new BuildDefinitionManager(buildServer);

            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.EnableBuildDefinition01);
            buildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.EnableBuildDefinition01);

            buildDefinitionList = target.GetBuildDefinitionList(teamProjectName);
            subsetBuildDefinitionList = buildDefinitionList.FindByNamePrefix(buildDefinitionNamePrefix);

            target.BuildDefinitionEnabled += (Object sender, EnableBuildDefinitionEventArgs enableBuildDefinitionEventArgs) =>
            {
                System.Diagnostics.Debug.WriteLine(string.Format("OnBuildDefinitionBuildsDeleted: Build definition name = {0}",
                                                                 enableBuildDefinitionEventArgs.EnableBuildDefinitionResult.BuildDefinition.Name));
            };

            enableValue = true;
            target.EnableBuildDefinition(subsetBuildDefinitionList, enableValue);

        }

        /// <summary>
        ///A test for RequestBuild
        ///</summary>
        [TestMethod()]
        public void RequestBuildTest()
        {

            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            string teamProjectName = null;
            string buildDefinitionNamePrefix = string.Empty;
            string msbuildArguments = string.Empty;
            BuildDefinitionList buildDefinitionList = null;
            bool overrideQueueStatus = false;
            QueuePriority queuePriority = QueuePriority.Normal;
            bool disableBuildDefinitionAfterBuildRequest = false;
            IBuildServer buildServer = null;
            IRequestBuildResult actual = null;

            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.RequestBuild01);
            buildServer = BuildDefinitionManagerTestFactory.GetBuildServer(tfsServerUri);

            target = new BuildDefinitionManager(buildServer);

            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.RequestBuild01);
            buildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.RequestBuild01);
            msbuildArguments = BuildDefinitionManagerTestFactory.GetMSBuildArguments(TestFactoryTypes.TestScenario.RequestBuild01);
            buildDefinitionList = target.GetBuildDefinitionList(teamProjectName);
            var subsetBuildDefinitionList = buildDefinitionList.Where(bd => bd.Name.ToLower().StartsWith(buildDefinitionNamePrefix.ToLower()));

            foreach (IBuildDefinition buildDefinition in subsetBuildDefinitionList)
            {
                actual = target.RequestBuild(buildDefinition,
                                             msbuildArguments,
                                             overrideQueueStatus,
                                             queuePriority,
                                             disableBuildDefinitionAfterBuildRequest);
                Assert.IsNotNull(actual, string.Format("Expected non-null IRequestBuildResult from RequestBuild() method."));
            }

        }

        /// <summary>
        ///A test for RequestBuild
        ///</summary>
        [TestMethod()]
        public void RequestBuildTest1()
        {

            BuildDefinitionManager target = null;
            Uri tfsServerUri = null;
            string teamProjectName = null;
            string buildDefinitionNamePrefix = string.Empty;
            string msbuildArguments = string.Empty;
            BuildDefinitionList buildDefinitionList = null;
            BuildDefinitionList subsetBuildDefinitionList = null;
            IBuildServer buildServer = null;
            bool overrideQueueStatus = false;
            QueuePriority queuePriority = QueuePriority.Normal;
            bool disableBuildDefinitionAfterBuildRequest = false; IRequestBuildResult actual = null;

            tfsServerUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(TestFactoryTypes.TestScenario.RequestBuild01);
            buildServer = BuildDefinitionManagerTestFactory.GetBuildServer(tfsServerUri);

            target = new BuildDefinitionManager(buildServer);

            teamProjectName = BuildDefinitionManagerTestFactory.GetTeamProjectName(TestFactoryTypes.TestScenario.RequestBuild01);
            buildDefinitionNamePrefix = BuildDefinitionManagerTestFactory.GetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario.RequestBuild01);
            msbuildArguments = BuildDefinitionManagerTestFactory.GetMSBuildArguments(TestFactoryTypes.TestScenario.RequestBuild01);

            buildDefinitionList = target.GetBuildDefinitionList(teamProjectName);
            subsetBuildDefinitionList = buildDefinitionList.FindByNamePrefix(buildDefinitionNamePrefix);

            target.BuildRequested += (Object sender, RequestBuildEventArgs requestBuildEventArgs) =>
            {
                System.Diagnostics.Debug.WriteLine(string.Format("OnBuildRequested: Build definition name = {0}",
                                                                 requestBuildEventArgs.RequestBuildResult.BuildDefinition.Name));
            };

            target.RequestBuild(subsetBuildDefinitionList,
                                disableBuildDefinitionAfterBuildRequest,
                                msbuildArguments,
                                overrideQueueStatus,
                                queuePriority);
        }
    }
}
