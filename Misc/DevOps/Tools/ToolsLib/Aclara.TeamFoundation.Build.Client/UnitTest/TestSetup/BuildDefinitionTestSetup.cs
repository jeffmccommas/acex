﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.TeamFoundation.Build.Client;
using Microsoft.TeamFoundation.Build.Workflow;
using Microsoft.TeamFoundation.Build.Workflow.Activities;
using Microsoft.TeamFoundation.Client;
using UnitTest.TestFactories;

namespace UnitTest.TestSetup
{
    /// <summary>
    /// Build definition test setup.
    /// </summary>
    public static class BuildDefinitionTestSetup
    {

        #region Public Methods

        /// <summary>
        /// Create build definition.
        /// </summary>
        /// <param name="testScenario"></param>
        /// <returns></returns>
        public static IBuildDefinition CreateBuildDefinition(TestFactoryTypes.TestScenario testScenario)
        {
            IBuildDefinition result = null;

            try
            {

                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CreateBuildDefinition01:


                        result = BuildDefinitionManagerTestFactory.GetBuildDefinition(testScenario);

                        break;

                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario {0}.", testScenario));
                }

                return result;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve build server.
        /// </summary>
        /// <param name="tfsTeamProjectCollection"></param>
        /// <returns></returns>
        public static IBuildServer GetBuildServer(Uri tfsServerUri)
        {
            IBuildServer result = null;
            IBuildServer buildServer = null;
            TfsTeamProjectCollection tfsTeamProjectCollection = null;
            Object service = null;

            try
            {
                tfsTeamProjectCollection = new TfsTeamProjectCollection(tfsServerUri);

                tfsTeamProjectCollection.EnsureAuthenticated();

                service = tfsTeamProjectCollection.GetService(typeof(IBuildServer));
                if (service is IBuildServer)
                {
                    buildServer = (IBuildServer)service;
                }

                result = buildServer;

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion  
    }
}
