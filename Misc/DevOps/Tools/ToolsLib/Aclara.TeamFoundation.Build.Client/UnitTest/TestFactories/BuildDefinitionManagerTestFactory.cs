﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.TeamFoundation.Build.Client;
using Microsoft.TeamFoundation.Build.Workflow;
using Microsoft.TeamFoundation.Build.Workflow.Activities;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.Common;
using UnitTest.Mocks;

namespace UnitTest.TestFactories
{
    /// <summary>
    /// Build definition manager test factory.
    /// </summary>
    public static class BuildDefinitionManagerTestFactory
    {

        #region Private Constants

        //Common to all test scenarios:

        private const string TestData_TfsServerUriAsText = "http://swtfs.energyguide.com:8080/tfs/defaultcollection";
        private const string TestData_TeamProjectName = "EnergyPrism";

        //CreateBuildDefinition01:
        private const string TestData_CRTBLDDEF_01_Name = "EP_11.09_TeamBuildSidekick";
        private const string TestData_CRTBLDDEF_01_Description = "Unit test generated.";
        private const string TestData_CRTBLDDEF_01_BuildDefinitionNamePrefix = "EP_11.09";
        private const string TestData_CRTBLDDEF_01_BranchName = @"11.09";
        private const string TestData_CRTBLDDEF_01_BuildControllerName = "Controller acltfsbuild03 EP";
        private const string TestData_CRTBLDDEF_01_BuildAgentServerName = "acltfsbuild03";
        //CreateBuildDefinition - process template.
        private const string TestData_CRTBLDDEF_ProcTemp_01_description = "Unit test process template";
        private const BuildReason TestData_CRTBLDDEF_ProcTemp_01_SupportedReasons = BuildReason.All;
        private const string TestData_CRTBLDDEF_ProcTemp_01_Parameters = "TBD";
        private const string TestData_CRTBLDDEF_ProcTemp_01_ServerPath = "TBD";
        private const string TestData_CRTBLDDEF_ProcTemp_01_TeamProjectName = "EnergyPrism";
        private const string TestData_CRTBLDDEF_01_TeamBuildTypeName = "TeamBuildSidekick";
        private const ProcessTemplateType TestData_CRTBLDDEF_ProcTemp_01_ProcessTemplateType = ProcessTemplateType.Upgrade;

        //ChangeBuildDefinitionBuildController01
        private const string TestData_CBDBC_01_BuildDefinitionNamePrefix = "EP_11.09_TeamBuildSidekick";
        private const string TestData_CBDBC_01_BuildControllerName = "Controller acltfsbuild03 EP";

        //ChangeBuildDefinitionBuildVerbostiy01

        //ChangeBuildDefinitionRetentionPolicy01
        private const string TestData_CBDRP_01_BuildDefinitionNamePrefix = "EP_11.09_TeamBuildSidekick";
        private const int TestData_CBDRP_01_NumberToKeep = 10;

        //ChangeBuildDefinitionSchedule01
        private const string TestData_CBDS_01_BuildDefinitionNamePrefix = "EP_11.09_TeamBuildSidekick";
        private const string TestData_CBDS_01_ScheduleStartTime = "00:05:00";

        //CloneBuildDefinition01
        private const string TestData_ClnBldDef_01_SourceBuildDefinitionNamePrefix = "EP_11.09_TeamBuildSidekick";
        private const string TestData_ClnBldDef_01_TargetBuildDefinitionNamePrefix = "UT_11.09_TeamBuildSidekick";
        private const string TestData_ClnBldDef_01_SourceBranchName = @"11.09";
        private const string TestData_ClnBldDef_01_TargetBranchName = @"11.09";

        //DeleteBuildDefinitionBuilds01
        private const string TestData_DBDB_01_BuildDefinitionNamePrefix = "EP_11.09_TeamBuildSidekick";

        //EnableBuildDefinition01
        private const string TestData_EBD_01_BuildDefinitionNamePrefix = "EP_11.09_TeamBuildSidekick";

        //RequestBuild01
        private const string TestData_RB_01_BuildDefinitionNamePrefix = "EP_11.09_TeamBuildSidekick";
        private const string TestData_RB_01_MSBuildArguments = @"/p:SkipDeploy=""true""";


        #endregion

        #region Public Methods - Helper Methods

        /// <summary>
        /// Retrieve build server.
        /// </summary>
        /// <param name="tfsTeamProjectCollection"></param>
        /// <returns></returns>
        static public IBuildServer GetBuildServer(Uri tfsServerUri)
        {
            IBuildServer result = null;
            IBuildServer buildServer = null;
            TfsTeamProjectCollection tfsTeamProjectCollection = null;
            Object service = null;

            try
            {
                tfsTeamProjectCollection = new TfsTeamProjectCollection(tfsServerUri);

                tfsTeamProjectCollection.EnsureAuthenticated();

                service = tfsTeamProjectCollection.GetService(typeof(IBuildServer));
                if (service is IBuildServer)
                {
                    buildServer = (IBuildServer)service;
                }

                result = buildServer;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build controller.
        /// </summary>
        /// <param name="tfsServerUri"></param>
        /// <param name="buildControllerName"></param>
        /// <returns></returns>
        static public IBuildController GetBuildController(Uri tfsServerUri,
                                                          string buildControllerName)
        {
            IBuildController result = null;
            IBuildServer buildServer = null;
            TfsTeamProjectCollection tfsTeamProjectCollection = null;
            Object service = null;

            try
            {
                tfsTeamProjectCollection = new TfsTeamProjectCollection(tfsServerUri);

                tfsTeamProjectCollection.EnsureAuthenticated();

                service = tfsTeamProjectCollection.GetService(typeof(IBuildServer));
                if (service is IBuildServer)
                {
                    buildServer = (IBuildServer)service;
                }

                result = buildServer.GetBuildController(buildControllerName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve default process template.
        /// </summary>
        /// <param name="tfsServerUri"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        static public IProcessTemplate GetDefaultProcessTemplate(Uri tfsServerUri,
                                                                 string teamProjectName)
        {
            IProcessTemplate result = null;
            IBuildServer buildServer = null;

            try
            {
                buildServer = GetBuildServer(tfsServerUri);

                result = buildServer.QueryProcessTemplates(teamProjectName).Where(p => p.TemplateType == ProcessTemplateType.Default).First();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve upgrade process template.
        /// </summary>
        /// <param name="tfsServerUri"></param>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        static public IProcessTemplate GetUpgradeProcessTemplate(Uri tfsServerUri,
                                                                 string teamProjectName)
        {
            IProcessTemplate result = null;
            IBuildServer buildServer = null;

            try
            {
                buildServer = GetBuildServer(tfsServerUri);

                result = buildServer.QueryProcessTemplates(teamProjectName).Where(p => p.TemplateType == ProcessTemplateType.Upgrade).First();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve build controller name.
        /// </summary>
        /// <param name="testScenario"></param>
        /// <returns></returns>
        public static string GetBuildControllerName(TestFactoryTypes.TestScenario testScenario)
        {
            string result = string.Empty;

            try
            {

                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01:
                        result = TestData_CBDBC_01_BuildControllerName;
                        break;
                    case TestFactoryTypes.TestScenario.CreateBuildDefinition01:
                        result = TestData_CRTBLDDEF_01_BuildControllerName;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build verbosity.
        /// </summary>
        /// <param name="testScenario"></param>
        /// <returns></returns>
       public static BuildVerbosity GetBuildVerbosity(TestFactoryTypes.TestScenario testScenario)
        {
            BuildVerbosity result = BuildVerbosity.Minimal;

            try
            {

                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildVerbostiy01:
                        result = BuildVerbosity.Normal;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }

            }
            catch (Exception)
            {
                throw;
            }

           return result;
        }

        /// <summary>
        /// Retrieve Team Foundation Server Uri.
        /// </summary>
        /// <param name="TestFactoryTypes.TestScenario"></param>
        /// <returns></returns>
        public static Uri GetTfsServerUri(TestFactoryTypes.TestScenario testScenario)
        {
            Uri result = null;

            switch (testScenario)
            {
                default:
                    result = new Uri(TestData_TfsServerUriAsText);
                    break;
            }

            return result;
        }

        /// <summary>
        /// Retrieve team project name.
        /// </summary>
        /// <param name="TestFactoryTypes.TestScenario"></param>
        /// <returns></returns>
        public static string GetTeamProjectName(TestFactoryTypes.TestScenario testScenario)
        {
            string result = string.Empty;

            try
            {
                switch (testScenario)
                {
                    default:
                        result = TestData_TeamProjectName;
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build definition name.
        /// </summary>
        /// <param name="testScenario"></param>
        /// <returns></returns>
        public static string GetBuildDefinitionName(TestFactoryTypes.TestScenario testScenario)
        {
            string result = string.Empty;

            try
            {
                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CreateBuildDefinition01:
                        result = TestData_CRTBLDDEF_01_Name;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build definition description.
        /// </summary>
        /// <param name="testScenario"></param>
        /// <returns></returns>
        public static string GetBuildDefinitionDescription(TestFactoryTypes.TestScenario testScenario)
        {
            string result = string.Empty;

            try
            {
                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CreateBuildDefinition01:
                        result = TestData_CRTBLDDEF_01_Description;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build definition continuous integration type.
        /// </summary>
        /// <param name="testScenario"></param>
        /// <returns></returns>
        public static ContinuousIntegrationType GetBuildDefinitionContinuousIntegrationType(TestFactoryTypes.TestScenario testScenario)
        {
            ContinuousIntegrationType result = ContinuousIntegrationType.All;

            try
            {
                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CreateBuildDefinition01:
                        result = ContinuousIntegrationType.Schedule;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve retention policy number to keep.
        /// </summary>
        /// <param name="testScenario"></param>
        /// <returns></returns>
        public static int GetRetentionPolicyNumberToKeep(TestFactoryTypes.TestScenario testScenario)
        {
            int result;

            try
            {
                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.ChangeBuildDefinitionRetentionPolicy01:
                        result = TestData_CBDRP_01_NumberToKeep;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build definition workspace mappings.
        /// </summary>
        /// <param name="testScenario"></param>
        /// <returns></returns>
        public static List<IWorkspaceMapping> GetBuildDefinitionWorkspaceMappings(TestFactoryTypes.TestScenario testScenario)
        {
            List<IWorkspaceMapping> result = null;
            IWorkspaceMapping workspaceMapping = null;
            string teamProjectName = string.Empty;
            string branchName = string.Empty;

            try
            {
                result = new List<IWorkspaceMapping>();

                teamProjectName = GetTeamProjectName(testScenario);
                branchName = GetBranchName(testScenario);

                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CreateBuildDefinition01:

                        workspaceMapping = CreateMockWorkspaceMapping(WorkspaceMappingType.Map, string.Format("$/{0}/{1}", teamProjectName, branchName), string.Format("$(SourceDir)"), WorkspaceMappingDepth.OneLevel);
                        result.Add(workspaceMapping);

                        workspaceMapping = CreateMockWorkspaceMapping(WorkspaceMappingType.Cloak, string.Format("$/{0}/{1}/AppLib", teamProjectName, branchName), string.Empty, WorkspaceMappingDepth.OneLevel);
                        result.Add(workspaceMapping);

                        workspaceMapping = CreateMockWorkspaceMapping(WorkspaceMappingType.Cloak, string.Format("$/{0}/{1}/EntLib", teamProjectName, branchName), string.Empty, WorkspaceMappingDepth.OneLevel);
                        result.Add(workspaceMapping);

                        workspaceMapping = CreateMockWorkspaceMapping(WorkspaceMappingType.Cloak, string.Format("$/{0}/{1}/Misc", teamProjectName, branchName), string.Empty, WorkspaceMappingDepth.OneLevel);
                        result.Add(workspaceMapping);

                        workspaceMapping = CreateMockWorkspaceMapping(WorkspaceMappingType.Map, string.Format("$/{0}/{1}/Misc/Build", teamProjectName, branchName), string.Format(@"$(SourceDir)\Misc\Build"), WorkspaceMappingDepth.OneLevel);
                        result.Add(workspaceMapping);

                        workspaceMapping = CreateMockWorkspaceMapping(WorkspaceMappingType.Cloak, string.Format("$/{0}/{1}/UFxAppLib", teamProjectName, branchName), string.Empty, WorkspaceMappingDepth.OneLevel);
                        result.Add(workspaceMapping);

                        workspaceMapping = CreateMockWorkspaceMapping(WorkspaceMappingType.Cloak, string.Format("$/{0}/{1}/UFxEntLib", teamProjectName, branchName), string.Empty, WorkspaceMappingDepth.OneLevel);
                        result.Add(workspaceMapping);

                        workspaceMapping = CreateMockWorkspaceMapping(WorkspaceMappingType.Cloak, string.Format("$/{0}/{1}/UFxLib", teamProjectName, branchName), string.Empty, WorkspaceMappingDepth.OneLevel);
                        result.Add(workspaceMapping);

                        workspaceMapping = CreateMockWorkspaceMapping(WorkspaceMappingType.Cloak, string.Format("$/{0}/{1}/UFxMisc", teamProjectName, branchName), string.Empty, WorkspaceMappingDepth.OneLevel);
                        result.Add(workspaceMapping);

                        workspaceMapping = CreateMockWorkspaceMapping(WorkspaceMappingType.Cloak, string.Format("$/{0}/{1}/UFxTP", teamProjectName, branchName), string.Empty, WorkspaceMappingDepth.OneLevel);
                        result.Add(workspaceMapping);

                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build definition build controller.
        /// </summary>
        /// <param name="testScenario"></param>
        /// <returns></returns>
        public static IBuildController GetBuildDefinitionBuildController(TestFactoryTypes.TestScenario testScenario)
        {
            IBuildController result = null;

            try
            {

                switch (testScenario)
                {
                    default:
                        //Retrieve build controller.
                        result = GetBuildController(GetTfsServerUri(testScenario),
                                                    TestData_CRTBLDDEF_01_BuildControllerName);
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build definition default drop location.
        /// </summary>
        /// <param name="testScenario"></param>
        /// <returns></returns>
        public static string GetBuildDefinitionDefaultDropLocation(TestFactoryTypes.TestScenario testScenario)
        {
            string result = string.Empty;
            string buildAgentServerName = string.Empty;
            string teamProjectName = string.Empty;
            string teamBuildTypeName = string.Empty;
            string buildDefinitionNamePrefix = string.Empty;

            try
            {

                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CreateBuildDefinition01:
                        buildAgentServerName = TestData_CRTBLDDEF_01_BuildAgentServerName;
                        buildDefinitionNamePrefix = TestData_CRTBLDDEF_01_BuildDefinitionNamePrefix;
                        teamProjectName = TestData_TeamProjectName;
                        teamBuildTypeName = TestData_CRTBLDDEF_01_TeamBuildTypeName;

                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }

                result = string.Format(@"\\{0}\BuildDrops\{1}\{2}_{3}",
                                       buildAgentServerName,
                                       teamProjectName,
                                       buildDefinitionNamePrefix,
                                       teamBuildTypeName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build definition process.
        /// </summary>
        /// <param name="testScenario"></param>
        /// <returns></returns>
        public static IProcessTemplate GetBuildDefinitionProcess(TestFactoryTypes.TestScenario testScenario)
        {
            IProcessTemplate result = null;

            try
            {

                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CreateBuildDefinition01:

                        //Create mock process template.
                        result = CreateMockProcessTemplate(TestData_CRTBLDDEF_ProcTemp_01_description,
                                                           TestData_CRTBLDDEF_ProcTemp_01_SupportedReasons,
                                                           TestData_CRTBLDDEF_ProcTemp_01_Parameters,
                                                           TestData_CRTBLDDEF_ProcTemp_01_ServerPath,
                                                           TestData_CRTBLDDEF_ProcTemp_01_TeamProjectName,
                                                           TestData_CRTBLDDEF_ProcTemp_01_ProcessTemplateType);


                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build definition process parameters.
        /// </summary>
        /// <param name="testScenario"></param>
        /// <returns></returns>
        public static string GetBuildDefinitionProcessParameters(TestFactoryTypes.TestScenario testScenario,
                                                                 string processParameters)
        {
            string result = string.Empty;
            string configurationFolderPath = string.Empty;
            IDictionary<string, object> processParametersDictionary = null;


            try
            {

                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CreateBuildDefinition01:

                        processParametersDictionary = WorkflowHelpers.DeserializeProcessParameters(processParameters);

                        configurationFolderPath = GetBuildDefinitionConfigurationFolderPath(testScenario);
                        processParametersDictionary[ProcessParameterMetadata.StandardParameterNames.ConfigurationFolderPath] = configurationFolderPath;

                        result = WorkflowHelpers.SerializeProcessParameters(processParametersDictionary);

                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build definition retention policy list.
        /// </summary>
        /// <param name="testScenario"></param>
        /// <param name="buildDefinition"></param>
        /// <returns></returns>
        public static List<IRetentionPolicy> GetBuildDefinitionRetentionPolicyList(TestFactoryTypes.TestScenario testScenario,
                                                                                   IBuildDefinition buildDefinition)
        {
            List<IRetentionPolicy> result = null;
            IRetentionPolicy retentionPolicy = null;
            BuildReason buildReason;
            BuildStatus buildStatus;
            DeleteOptions deleteOptions;
            int numberToKeep;

            try
            {
                result = new List<IRetentionPolicy>();

                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CreateBuildDefinition01:

                        buildReason = BuildReason.Triggered;
                        buildStatus = BuildStatus.Stopped;
                        deleteOptions = DeleteOptions.All;
                        numberToKeep = 10;
                        retentionPolicy = CreateMockRetentionPolicy(buildDefinition, buildReason, buildStatus, deleteOptions, numberToKeep);
                        result.Add(retentionPolicy);

                        buildReason = BuildReason.Triggered;
                        buildStatus = BuildStatus.Failed;
                        deleteOptions = DeleteOptions.All;
                        numberToKeep = 10;
                        retentionPolicy = CreateMockRetentionPolicy(buildDefinition, buildReason, buildStatus, deleteOptions, numberToKeep);
                        result.Add(retentionPolicy);

                        buildReason = BuildReason.Triggered;
                        buildStatus = BuildStatus.PartiallySucceeded;
                        deleteOptions = DeleteOptions.All;
                        numberToKeep = 10;
                        retentionPolicy = CreateMockRetentionPolicy(buildDefinition, buildReason, buildStatus, deleteOptions, numberToKeep);
                        result.Add(retentionPolicy);

                        buildReason = BuildReason.Triggered;
                        buildStatus = BuildStatus.Succeeded;
                        deleteOptions = DeleteOptions.All;
                        numberToKeep = 10;
                        retentionPolicy = CreateMockRetentionPolicy(buildDefinition, buildReason, buildStatus, deleteOptions, numberToKeep);
                        result.Add(retentionPolicy);

                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build definition.
        /// </summary>
        /// <param name="testScenario"></param>
        /// <returns></returns>
        public static IBuildDefinition GetBuildDefinition(TestFactoryTypes.TestScenario testScenario)
        {
            IBuildDefinition result = null;
            IBuildServer buildServer = null;
            string teamProjectName = string.Empty;
            Uri teamProjectUri = null;
            string buildContollerName = string.Empty;
            ISchedule mockSchedule = null;
            ISchedule schedule = null;
            ScheduleDays daysToBuild;
            int startTime;
            TimeZoneInfo timeZoneInfo = null;
            ScheduleType scheduleType;
            List<IWorkspaceMapping> workspaceMappings = null;
            string process = string.Empty;
            string procesParameters = string.Empty;

            try
            {
                teamProjectName = GetTeamProjectName(testScenario);
                teamProjectUri = BuildDefinitionManagerTestFactory.GetTfsServerUri(testScenario);
                buildContollerName = BuildDefinitionManagerTestFactory.GetBuildControllerName(testScenario);

                buildServer = GetBuildServer(teamProjectUri);

                //Create build definition.
                result = buildServer.CreateBuildDefinition(teamProjectName);

                result.Name = GetBuildDefinitionName(testScenario);
                result.Description = GetBuildDefinitionDescription(testScenario);

                result.ContinuousIntegrationType = GetBuildDefinitionContinuousIntegrationType(testScenario);

                //Create schedule.
                daysToBuild = BuildDefinitionManagerTestFactory.GetScheduleDays(testScenario);
                startTime = BuildDefinitionManagerTestFactory.GetScheduleStartTime(testScenario);
                scheduleType = ScheduleType.Weekly;
                timeZoneInfo = TimeZoneInfo.Local;
                mockSchedule = CreateMockSchedule(result, daysToBuild, startTime, timeZoneInfo, scheduleType);

                result.Schedules.Clear();
                schedule = result.AddSchedule();
                schedule.DaysToBuild = mockSchedule.DaysToBuild;
                schedule.StartTime = mockSchedule.StartTime;
                schedule.TimeZone = mockSchedule.TimeZone;

                //Create workspace mappings.
                workspaceMappings = GetBuildDefinitionWorkspaceMappings(testScenario);

                foreach (IWorkspaceMapping map in workspaceMappings)
                {
                    result.Workspace.AddMapping(map.ServerItem, map.LocalItem, map.MappingType);
                }

                result.BuildController = GetBuildController(teamProjectUri, buildContollerName);

                result.DefaultDropLocation = GetBuildDefinitionDefaultDropLocation(testScenario);

                result.Process = GetUpgradeProcessTemplate(teamProjectUri,
                                                           teamProjectName);
                result.ProcessParameters = GetBuildDefinitionProcessParameters(testScenario, result.ProcessParameters);

                List<IRetentionPolicy> retentionPolicyList = null;
                retentionPolicyList = GetBuildDefinitionRetentionPolicyList(testScenario, result);
                result.RetentionPolicyList.Clear();
                foreach (IRetentionPolicy retentionPolicy in retentionPolicyList)
                {
                    result.AddRetentionPolicy(retentionPolicy.BuildReason,
                                              retentionPolicy.BuildStatus,
                                              retentionPolicy.NumberToKeep,
                                              retentionPolicy.DeleteOptions);
                }

                result.Enabled = true;

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build definition prefix name.
        /// </summary>
        /// <param name="TestFactoryTypes.TestScenario"></param>
        /// <returns></returns>
        public static string GetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario testScenario)
        {
            string result = string.Empty;

            switch (testScenario)
            {
                case TestFactoryTypes.TestScenario.ChangeBuildDefinitionBuildController01:
                    result = TestData_CBDBC_01_BuildDefinitionNamePrefix;
                    break;
                case TestFactoryTypes.TestScenario.ChangeBuildDefinitionRetentionPolicy01:
                    result = TestData_CBDRP_01_BuildDefinitionNamePrefix;
                    break;
                case TestFactoryTypes.TestScenario.ChangeBuildDefinitionSchedule01:
                    result = TestData_CBDS_01_BuildDefinitionNamePrefix;
                    break;
                case TestFactoryTypes.TestScenario.DeleteBuildDefinitionBuilds01:
                    result = TestData_DBDB_01_BuildDefinitionNamePrefix;
                    break;
                case TestFactoryTypes.TestScenario.EnableBuildDefinition01:
                    result = TestData_EBD_01_BuildDefinitionNamePrefix;
                    break;
                case TestFactoryTypes.TestScenario.RequestBuild01:
                    result = TestData_RB_01_BuildDefinitionNamePrefix;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
            }

            return result;
        }


        /// <summary>
        /// Retrieve MSBuild arguments.
        /// </summary>
        /// <param name="TestFactoryTypes.TestScenario"></param>
        /// <returns></returns>
        public static string GetMSBuildArguments(TestFactoryTypes.TestScenario testScenario)
        {
            string result = string.Empty;

            switch (testScenario)
            {
                case TestFactoryTypes.TestScenario.RequestBuild01:
                    result = TestData_RB_01_MSBuildArguments;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
            }

            return result;
        }

        /// <summary>
        /// Retrieve source build definition name prefix.
        /// </summary>
        /// <param name="TestFactoryTypes.TestScenario"></param>
        /// <returns></returns>
        public static string GetSourceBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario testScenario)
        {
            string result = string.Empty;

            try
            {
                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CloneBuildDefinition01:
                        result = TestData_ClnBldDef_01_SourceBuildDefinitionNamePrefix;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve target build definition name prefix.
        /// </summary>
        /// <param name="TestFactoryTypes.TestScenario"></param>
        /// <returns></returns>
        public static string GetTargetBuildDefinitionNamePrefix(TestFactoryTypes.TestScenario testScenario)
        {
            string result = string.Empty;

            try
            {
                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CloneBuildDefinition01:
                        result = TestData_ClnBldDef_01_TargetBuildDefinitionNamePrefix;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve branch name.
        /// </summary>
        /// <param name="TestFactoryTypes.TestScenario"></param>
        /// <returns></returns>
        public static string GetBranchName(TestFactoryTypes.TestScenario testScenario)
        {
            string result = string.Empty;

            try
            {
                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CreateBuildDefinition01:
                        result = TestData_CRTBLDDEF_01_BranchName;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve source branch name.
        /// </summary>
        /// <param name="TestFactoryTypes.TestScenario"></param>
        /// <returns></returns>
        public static string GetSourceBranchName(TestFactoryTypes.TestScenario testScenario)
        {
            string result = string.Empty;

            try
            {
                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CloneBuildDefinition01:
                        result = TestData_ClnBldDef_01_SourceBranchName;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve target branch name.
        /// </summary>
        /// <param name="TestFactoryTypes.TestScenario"></param>
        /// <returns></returns>
        public static string GetTargetBranchName(TestFactoryTypes.TestScenario testScenario)
        {
            string result = string.Empty;

            try
            {
                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CloneBuildDefinition01:
                        result = TestData_ClnBldDef_01_TargetBranchName;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve schedule start time.
        /// </summary>
        /// <param name="TestFactoryTypes.TestScenario"></param>
        /// <returns></returns>
        public static int GetScheduleStartTime(TestFactoryTypes.TestScenario testScenario)
        {
            int result;

            try
            {

                //Convert 24 hour time into integer.
                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CreateBuildDefinition01:
                    case TestFactoryTypes.TestScenario.ChangeBuildDefinitionSchedule01:
                        result = (int)TimeSpan.Parse(TestData_CBDS_01_ScheduleStartTime).TotalSeconds;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve schedule start time.
        /// </summary>
        /// <param name="TestFactoryTypes.TestScenario"></param>
        /// <returns></returns>
        public static ScheduleDays GetScheduleDays(TestFactoryTypes.TestScenario testScenario)
        {
            ScheduleDays result;

            try
            {
                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CreateBuildDefinition01:
                    case TestFactoryTypes.TestScenario.ChangeBuildDefinitionSchedule01:
                        result = ScheduleDays.All;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build definition configuration folder path.
        /// </summary>
        /// <param name="testScenario"></param>
        /// <returns></returns>
        public static string GetBuildDefinitionConfigurationFolderPath(TestFactoryTypes.TestScenario testScenario)
        {
            string result = string.Empty;
            string teamProjectName = string.Empty;
            string branchName = string.Empty;
            string teamBuildTypeName = string.Empty;

            try
            {
                switch (testScenario)
                {
                    case TestFactoryTypes.TestScenario.CreateBuildDefinition01:

                        teamProjectName = GetTeamProjectName(testScenario); ;
                        branchName = TestData_CRTBLDDEF_01_BranchName;
                        teamBuildTypeName = TestData_CRTBLDDEF_01_TeamBuildTypeName;

                        result = string.Format("$/{0}/{1}/Misc/Build/xTeamBuildTypes/{2}",
                                                teamProjectName,
                                                branchName,
                                                teamBuildTypeName);

                        break;

                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected test scenario: {0}.", testScenario));
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region "Protected Methods"

        /// <summary>
        /// Create mock workspace mapping
        /// </summary>
        /// <param name="workspaceMappingType"></param>
        /// <param name="serverItem"></param>
        /// <param name="localIteam"></param>
        /// <param name="workspaceMappingDepth"></param>
        /// <returns></returns>
        private static IWorkspaceMapping CreateMockWorkspaceMapping(WorkspaceMappingType workspaceMappingType,
                                                                      string serverItem,
                                                                      string localIteam,
                                                                      WorkspaceMappingDepth workspaceMappingDepth)
        {
            IWorkspaceMapping result = null;

            result = new MockWorkspaceMapping();

            result.MappingType = workspaceMappingType;
            result.ServerItem = serverItem;
            result.LocalItem = localIteam;
            result.Depth = workspaceMappingDepth;

            return result;
        }

        /// <summary>
        /// Create mock process template.
        /// </summary>
        /// <param name="description"></param>
        /// <param name="supportedReasons"></param>
        /// <param name="processTemplateType"></param>
        /// <returns></returns>
        private static IProcessTemplate CreateMockProcessTemplate(string description,
                                                                  BuildReason supportedReasons,
                                                                  string parameters,
                                                                  string serverPath,
                                                                  string teamProjectName,
                                                                  ProcessTemplateType processTemplateType)
        {

            IProcessTemplate result = null;

            try
            {
                result = new MockProcessTemplate(parameters,
                                                 serverPath,
                                                 teamProjectName);

                result.Description = description;
                result.SupportedReasons = supportedReasons;
                result.TemplateType = processTemplateType;

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create mock retention policy
        /// </summary>
        /// <returns></returns>
        private static IRetentionPolicy CreateMockRetentionPolicy(IBuildDefinition buildDefinition,
                                                                  BuildReason buildReason,
                                                                  BuildStatus buildStatus,
                                                                  DeleteOptions deleteOptions,
                                                                  int numberToKeep)
        {
            IRetentionPolicy result = null;

            try
            {

                result = new MockRetentionPolicy(buildDefinition,
                                                 buildReason,
                                                 buildStatus,
                                                 deleteOptions,
                                                 numberToKeep);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create mock schedule
        /// </summary>
        /// <returns></returns>
        private static ISchedule CreateMockSchedule(IBuildDefinition buildDefinition,
                                                    ScheduleDays daysToBuild,
                                                    int startTime,
                                                    TimeZoneInfo timeZoneInfo,
                                                    ScheduleType type)
        {
            ISchedule result = null;

            try
            {

                result = new MockSchedule(buildDefinition,
                                          daysToBuild,
                                          startTime,
                                          timeZoneInfo,
                                          type);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

    }
}
