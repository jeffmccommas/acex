﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTest.TestFactories
{

    /// <summary>
    /// Test factory types.
    /// </summary>
    public static class TestFactoryTypes
    {
        /// <summary>
        /// Test scenario enumeration.
        /// </summary>
        public enum TestScenario
        {
            CreateBuildDefinition01,
            ChangeBuildDefinitionBuildController01,
            ChangeBuildDefinitionBuildVerbostiy01,
            ChangeBuildDefinitionRetentionPolicy01,
            ChangeBuildDefinitionSchedule01,
            CloneBuildDefinition01,
            DeleteBuildDefinitionBuilds01,
            EnableBuildDefinition01,
            RequestBuild01
        }


    }
}
