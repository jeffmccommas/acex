﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Results;
using Aclara.TeamFoundation.Build.Client.Interfaces;

namespace Aclara.TeamFoundation.Build.Client.Events
{
    /// <summary>
    /// Clone build definition event arguments.
    /// </summary>
    public class CloneBuildDefinitionEventArgs : EventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private ICloneBuildDefinitionResult _cloneBuildDefinitionResult;

        #endregion

        #region Public Properties

        public ICloneBuildDefinitionResult CloneBuildDefinitionResult
        {
            get { return _cloneBuildDefinitionResult; }
            set { _cloneBuildDefinitionResult = value; }
        }

        #endregion

        #region Public Constructors
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CloneBuildDefinitionEventArgs()
        {
            _cloneBuildDefinitionResult = new CloneBuildDefinitionResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="cloneBuildDefinitionResult"></param>
        public CloneBuildDefinitionEventArgs(ICloneBuildDefinitionResult cloneBuildDefinitionResult)
        {
            _cloneBuildDefinitionResult = cloneBuildDefinitionResult;
        }

        #endregion

    }
}
