﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Results;
using Aclara.TeamFoundation.Build.Client.Interfaces;

namespace Aclara.TeamFoundation.Build.Client.Events
{

    /// <summary>
    /// Change build definition schedules event arguments.
    /// </summary>
    public class ChangeBuildDefinitionSchedulesEventArgs : EventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IChangeBuildDefinitionSchedulesResult _changeBuildDefinitionSchedulesResult;

        #endregion

        #region Public Properties

        public IChangeBuildDefinitionSchedulesResult ChangeBuildDefinitionSchedulesResult
        {
            get { return _changeBuildDefinitionSchedulesResult; }
            set { _changeBuildDefinitionSchedulesResult = value; }
        }

        #endregion

        #region Public Constructors
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ChangeBuildDefinitionSchedulesEventArgs()
        {
            _changeBuildDefinitionSchedulesResult = new ChangeBuildDefinitionSchedulesResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="changeBuildDefinitionBuildVerbosityResult"></param>
        public ChangeBuildDefinitionSchedulesEventArgs(IChangeBuildDefinitionSchedulesResult changeBuildDefinitionBuildVerbosityResult)
        {
            _changeBuildDefinitionSchedulesResult = changeBuildDefinitionBuildVerbosityResult;
        }

        #endregion

    }
}
