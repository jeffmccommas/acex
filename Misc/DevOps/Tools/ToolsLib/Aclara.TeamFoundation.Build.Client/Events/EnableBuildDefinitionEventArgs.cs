﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Results;
using Aclara.TeamFoundation.Build.Client.Interfaces;

namespace Aclara.TeamFoundation.Build.Client.Events
{

    /// <summary>
    /// Enable build definition event arguments.
    /// </summary>
    public class EnableBuildDefinitionEventArgs : EventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private IEnableBuildDefinitionResult _deleteBuildDefinitionResult;

        #endregion

        #region Public Properties

        public IEnableBuildDefinitionResult EnableBuildDefinitionResult
        {
            get { return _deleteBuildDefinitionResult; }
            set { _deleteBuildDefinitionResult = value; }
        }

        #endregion

        #region Public Constructors
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public EnableBuildDefinitionEventArgs()
        {
            _deleteBuildDefinitionResult = new EnableBuildDefinitionResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="deleteBuildDefinitionResult"></param>
        public EnableBuildDefinitionEventArgs(IEnableBuildDefinitionResult deleteBuildDefinitionResult)
        {
            _deleteBuildDefinitionResult = deleteBuildDefinitionResult;
        }

        #endregion

    }
}
