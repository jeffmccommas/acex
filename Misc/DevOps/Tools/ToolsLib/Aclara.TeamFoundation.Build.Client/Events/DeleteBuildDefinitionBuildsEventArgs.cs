﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Results;
using Aclara.TeamFoundation.Build.Client.Interfaces;

namespace Aclara.TeamFoundation.Build.Client.Events
{
    /// <summary>
    /// Delete build definition builds event arguments.
    /// </summary>
    public class DeleteBuildDefinitionBuildsEventArgs : EventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private IDeleteBuildDefinitionBuildsResult _deleteBuildDefinitionBuildsResult;

        #endregion

        #region Public Properties

        public IDeleteBuildDefinitionBuildsResult DeleteBuildDefinitionBuildsResult
        {
            get { return _deleteBuildDefinitionBuildsResult; }
            set { _deleteBuildDefinitionBuildsResult = value; }
        }

        #endregion

        #region Public Constructors
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public DeleteBuildDefinitionBuildsEventArgs()
        {
            _deleteBuildDefinitionBuildsResult = new DeleteBuildDefinitionBuildsResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="changeBuildDefinitionBuildControllerResult"></param>
        public DeleteBuildDefinitionBuildsEventArgs(IDeleteBuildDefinitionBuildsResult changeBuildDefinitionBuildControllerResult)
        {
            _deleteBuildDefinitionBuildsResult = changeBuildDefinitionBuildControllerResult;
        }

        #endregion


    }
}
