﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Results;
using Aclara.TeamFoundation.Build.Client.Interfaces;

namespace Aclara.TeamFoundation.Build.Client.Events
{
    /// <summary>
    /// Request build event arguments.
    /// </summary>
    public class RequestBuildEventArgs : EventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private IRequestBuildResult _requestBuildResult;

        #endregion
        
        #region Public Properties

        /// <summary>
        /// Property: Request build result.
        /// </summary>
        public IRequestBuildResult RequestBuildResult
        {
            get { return _requestBuildResult; }
            set { _requestBuildResult = value; }
        }

        #endregion

        #region Public Constructors
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public RequestBuildEventArgs()
        {
            _requestBuildResult = new RequestBuildResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="requestBuildResult"></param>
        public RequestBuildEventArgs(IRequestBuildResult requestBuildResult)
        {
            _requestBuildResult = requestBuildResult;
        }

        #endregion

    }
}
