﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Results;
using Aclara.TeamFoundation.Build.Client.Interfaces;

namespace Aclara.TeamFoundation.Build.Client.Events
{

    /// <summary>
    /// Change build definition build controller event arguments.
    /// </summary>
    public class ChangeBuildDefinitionBuildControllerEventArgs : EventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private IChangeBuildDefinitionBuildControllerResult _changeBuildDefinitionBuildControllerResult;

        #endregion

        #region Public Properties

        public IChangeBuildDefinitionBuildControllerResult ChangeBuildDefinitionBuildControllerResult
        {
            get { return _changeBuildDefinitionBuildControllerResult; }
            set { _changeBuildDefinitionBuildControllerResult = value; }
        }

        #endregion

        #region Public Constructors
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ChangeBuildDefinitionBuildControllerEventArgs()
        {
            _changeBuildDefinitionBuildControllerResult = new ChangeBuildDefinitionBuildControllerResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="changeBuildDefinitionBuildControllerResult"></param>
        public ChangeBuildDefinitionBuildControllerEventArgs(IChangeBuildDefinitionBuildControllerResult changeBuildDefinitionBuildControllerResult)
        {
            _changeBuildDefinitionBuildControllerResult = changeBuildDefinitionBuildControllerResult;
        }

        #endregion

    }
}
