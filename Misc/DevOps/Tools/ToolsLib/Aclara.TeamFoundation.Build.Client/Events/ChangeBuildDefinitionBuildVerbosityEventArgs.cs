﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Results;
using Aclara.TeamFoundation.Build.Client.Interfaces;

namespace Aclara.TeamFoundation.Build.Client.Events
{

    /// <summary>
    /// Change build definition build verbosity event arguments.
    /// </summary>
    public class ChangeBuildDefinitionBuildVerbosityEventArgs : EventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IChangeBuildDefinitionBuildVerbosityResult _changeBuildDefinitionBuildVerbosityResult;

        #endregion

        #region Public Properties

        public IChangeBuildDefinitionBuildVerbosityResult ChangeBuildDefinitionBuildVerbosityResult
        {
            get { return _changeBuildDefinitionBuildVerbosityResult; }
            set { _changeBuildDefinitionBuildVerbosityResult = value; }
        }

        #endregion

        #region Public Constructors
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ChangeBuildDefinitionBuildVerbosityEventArgs()
        {
            _changeBuildDefinitionBuildVerbosityResult = new ChangeBuildDefinitionBuildVerbosityResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="changeBuildDefinitionBuildVerbosityResult"></param>
        public ChangeBuildDefinitionBuildVerbosityEventArgs(IChangeBuildDefinitionBuildVerbosityResult changeBuildDefinitionBuildVerbosityResult)
        {
            _changeBuildDefinitionBuildVerbosityResult = changeBuildDefinitionBuildVerbosityResult;
        }

        #endregion

    }
}
