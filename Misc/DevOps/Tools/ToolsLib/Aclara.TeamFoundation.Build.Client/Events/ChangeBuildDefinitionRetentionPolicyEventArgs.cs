﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.TeamFoundation.Build.Client.Results;
using Aclara.TeamFoundation.Build.Client.Interfaces;

namespace Aclara.TeamFoundation.Build.Client.Events
{

    /// <summary>
    /// Change build definition retention policy envent arguments.
    /// </summary>
    public class ChangeBuildDefinitionRetentionPolicyEventArgs : EventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IChangeBuildDefinitionRetentionPolicyResult _changeBuildDefinitionRetentionPolicyResult;

        #endregion

        #region Public Properties

        public IChangeBuildDefinitionRetentionPolicyResult ChangeBuildDefinitionRetentionPolicyResult
        {
            get { return _changeBuildDefinitionRetentionPolicyResult; }
            set { _changeBuildDefinitionRetentionPolicyResult = value; }
        }

        #endregion

        #region Public Constructors
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ChangeBuildDefinitionRetentionPolicyEventArgs()
        {
            _changeBuildDefinitionRetentionPolicyResult = new ChangeBuildDefinitionRetentionPolicyResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="changeBuildDefinitionRetentionPolicyResult"></param>
        public ChangeBuildDefinitionRetentionPolicyEventArgs(IChangeBuildDefinitionRetentionPolicyResult changeBuildDefinitionRetentionPolicyResult)
        {
            _changeBuildDefinitionRetentionPolicyResult = changeBuildDefinitionRetentionPolicyResult;
        }

        #endregion

    }
}
