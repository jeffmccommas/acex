﻿using Aclara.TeamFoundation.Build.Client.StatusManagement;
using Microsoft.TeamFoundation.Build.Client;

namespace Aclara.TeamFoundation.Build.Client.Interfaces
{

    public interface IChangeBuildDefinitionSchedulesResult
    {
        IBuildDefinition BuildDefinition { get; set; }
        StatusList StatusList { get; }
    }
}
