﻿using Microsoft.TeamFoundation.Build.Client;
using Aclara.TeamFoundation.Build.Client.StatusManagement;

namespace Aclara.TeamFoundation.Build.Client.Interfaces
{
    
    public interface ICloneBuildDefinitionResult
    {

        IBuildDefinition BuildDefinition { get; set; }
        StatusList StatusList { get; }

    }

}
