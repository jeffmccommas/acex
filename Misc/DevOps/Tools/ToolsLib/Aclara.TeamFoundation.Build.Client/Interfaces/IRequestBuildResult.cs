﻿using Aclara.TeamFoundation.Build.Client.StatusManagement;
using Microsoft.TeamFoundation.Build.Client;

namespace Aclara.TeamFoundation.Build.Client.Interfaces
{
    public interface IRequestBuildResult
    {
        IBuildDefinition BuildDefinition { get; set; }
        IQueuedBuild QueuedBuild { get; set; }
        StatusList StatusList { get; }
        string MSBuildArguments { get; set; }
    }
}
