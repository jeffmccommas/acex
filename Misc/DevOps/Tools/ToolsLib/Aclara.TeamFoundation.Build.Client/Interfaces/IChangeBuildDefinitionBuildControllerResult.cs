﻿using Aclara.TeamFoundation.Build.Client.StatusManagement;
using Microsoft.TeamFoundation.Build.Client;

namespace Aclara.TeamFoundation.Build.Client.Interfaces
{

    /// <summary>
    /// Change build definition retention policy result.
    /// </summary>
    public interface IChangeBuildDefinitionRetentionPolicyResult
    {
        IBuildDefinition BuildDefinition { get; set; }
        StatusList StatusList { get; }
    }
}
