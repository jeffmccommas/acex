﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.TeamFoundation.Build.Client;
using Microsoft.TeamFoundation.Build.Workflow;
using Microsoft.TeamFoundation.Build.Workflow.Activities;
using Microsoft.TeamFoundation.Client;

namespace Aclara.TeamFoundation.Build.Client
{
    /// <summary>
    /// Build definition list.
    /// </summary>
    public class BuildDefinitionList : List<IBuildDefinition>
    {

        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildDefinitionList()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Find build definitions by name prefix.
        /// </summary>
        /// <param name="buildDefinitionNamePrefix"></param>
        /// <returns></returns>
        public BuildDefinitionList FindByNamePrefix(string buildDefinitionNamePrefix)
        {
            BuildDefinitionList result = null;

            try
            {
                result = new BuildDefinitionList();

                var subsetBuildDefinitionList = this.Where(bd => bd.Name.ToLower().StartsWith(buildDefinitionNamePrefix.ToLower()));

                foreach (IBuildDefinition buildDefinition in subsetBuildDefinitionList)
                {
                    result.Add(buildDefinition);
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }
        #endregion

        #region Protected Methods
        #endregion

    }
}
