﻿using Aclara.TeamFoundation.Build.Client.Events;
using Aclara.TeamFoundation.Build.Client.Interfaces;
using Aclara.TeamFoundation.Build.Client.Results;
using Aclara.TeamFoundation.Build.Client.StatusManagement;
using Microsoft.TeamFoundation.Build.Client;
using Microsoft.TeamFoundation.Build.Workflow;
using Microsoft.TeamFoundation.Build.Workflow.Activities;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.Common;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.TeamFoundation.Build.Client
{

    /// <summary>
    /// Build definition manager.
    /// </summary>
    public class BuildDefinitionManager
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IBuildServer _buildServer;

        #endregion

        #region Public Delegates

        public event EventHandler<ChangeBuildDefinitionBuildControllerEventArgs> BuildDefinitionBuildControllerChanged;
        public event EventHandler<ChangeBuildDefinitionBuildVerbosityEventArgs> BuildDefinitionBuildVerbosityChanged;
        public event EventHandler<ChangeBuildDefinitionRetentionPolicyEventArgs> BuildDefinitionRetentionPolicyChanged;
        public event EventHandler<ChangeBuildDefinitionSchedulesEventArgs> BuildDefinitionSchedulesChanged;
        public event EventHandler<CloneBuildDefinitionEventArgs> BuildDefinitionCloned;
        public event EventHandler<DeleteBuildDefinitionBuildsEventArgs> BuildDefinitionBuildsDeleted;
        public event EventHandler<EnableBuildDefinitionEventArgs> BuildDefinitionEnabled;
        public event EventHandler<RequestBuildEventArgs> BuildRequested;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Build server.
        /// </summary>
        public IBuildServer BuildServer
        {
            get { return _buildServer; }
            set { _buildServer = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildDefinitionManager()
        {

        }

        /// <summary>
        /// Overloaded Constructor.
        /// </summary>
        /// <param name="buildServer"></param>
        public BuildDefinitionManager(IBuildServer buildServer)
        {
            _buildServer = buildServer;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Change build definition build controller.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="buildControllerSpecified"></param>
        /// <param name="buildController"></param>
        /// <param name="agentSettingsTagSpecified"></param>
        /// <param name="tag"></param>
        /// <param name="dropFolderSpecified"></param>
        /// <param name="dropFolderSource"></param>
        /// <param name="dropFolderTarget"></param>
        /// <param name="msbuildArgumentsSpecified"></param>
        /// <param name="msbuildArguments"></param>
        /// <returns></returns>
        public IChangeBuildDefinitionBuildControllerResult ChangeBuildDefinitionBuildController(IBuildDefinition buildDefinition,
                                                                                                bool buildControllerSpecified,
                                                                                                IBuildController buildController,
                                                                                                bool agentSettingsTagSpecified,
                                                                                                string tag,
                                                                                                bool dropFolderSpecified,
                                                                                                string dropFolderSource,
                                                                                                string dropFolderTarget,
                                                                                                bool msbuildArgumentsSpecified,
                                                                                                string msbuildArguments)
        {
            IChangeBuildDefinitionBuildControllerResult result = null;
            string processParameters = string.Empty;

            try
            {

                result = ChangeBuildDefinitionBuildControllerResultFactory.CreateChangeBuildDefinitionBuildControllerResult();
                result.BuildDefinition = buildDefinition;

                //Change build controller.
                if (buildControllerSpecified == true)
                {
                    buildDefinition.BuildController = buildController;
                }

                //Change tag filters.
                if (agentSettingsTagSpecified == true)
                {
                    processParameters = ChangeProcessParametersAgentSettingsTag(buildDefinition.ProcessParameters,
                                                                                tag);
                    buildDefinition.ProcessParameters = processParameters;
                }

                //Change drop folder.
                if (dropFolderSpecified == true)
                {
                    buildDefinition.DefaultDropLocation = ReplaceTextInDefaultDropLocation(buildDefinition.DefaultDropLocation,
                                                                       dropFolderSource,
                                                                       dropFolderTarget);
                }

                //Change command line arguments.
                if (msbuildArgumentsSpecified == true)
                {
                    processParameters = ChangeProcessParametersAgentSettingsMSBuild(buildDefinition.ProcessParameters,
                                                                                    msbuildArguments);
                    buildDefinition.ProcessParameters = processParameters;
                }

                buildDefinition.Save();

                return result;

            }
            catch (Exception ex)
            {
                Status status = null;

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);

                return result;
            }

        }

        /// <summary>
        /// Change build definitions build controller.
        /// </summary>
        /// <param name="buildDefinitionList"></param>
        /// <param name="buildControllerSpecified"></param>
        /// <param name="buildController"></param>
        /// <param name="agentSettingsTagSpecified"></param>
        /// <param name="tag"></param>
        /// <param name="dropFolderSpecified"></param>
        /// <param name="dropFolderSource"></param>
        /// <param name="dropFolderTarget"></param>
        /// <param name="msbuildArgumentsSpecified"></param>
        /// <param name="msbuildArguments"></param>
        public void ChangeBuildDefinitionBuildController(BuildDefinitionList buildDefinitionList,
                                                         bool buildControllerSpecified,
                                                         IBuildController buildController,
                                                         bool agentSettingsTagSpecified,
                                                         string tag,
                                                         bool dropFolderSpecified,
                                                         string dropFolderSource,
                                                         string dropFolderTarget,
                                                         bool msbuildArgumentsSpecified,
                                                         string msbuildArguments)
        {
            IChangeBuildDefinitionBuildControllerResult changeBuildDefinitionBuildControllerResult = null;
            ChangeBuildDefinitionBuildControllerEventArgs changeBuildDefinitionBuildControllerEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    changeBuildDefinitionBuildControllerEventArgs = new ChangeBuildDefinitionBuildControllerEventArgs();

                    changeBuildDefinitionBuildControllerResult = ChangeBuildDefinitionBuildController(buildDefinition,
                                                                                                      buildControllerSpecified,
                                                                                                      buildController,
                                                                                                      agentSettingsTagSpecified,
                                                                                                      tag,
                                                                                                      dropFolderSpecified,
                                                                                                      dropFolderSource,
                                                                                                      dropFolderTarget,
                                                                                                      msbuildArgumentsSpecified,
                                                                                                      msbuildArguments);

                    changeBuildDefinitionBuildControllerEventArgs.ChangeBuildDefinitionBuildControllerResult = changeBuildDefinitionBuildControllerResult;

                    if (BuildDefinitionBuildControllerChanged != null)
                    {
                        BuildDefinitionBuildControllerChanged(this, changeBuildDefinitionBuildControllerEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Change build definitions build controller.
        /// </summary>
        /// <remarks>
        /// Overriden to take CancellationToken for use in parallel programming.
        /// </remarks>
        /// <param name="buildDefinitionList"></param>
        /// <param name="buildControllerSpecified"></param>
        /// <param name="buildController"></param>
        /// <param name="agentSettingsTagSpecified"></param>
        /// <param name="tag"></param>
        /// <param name="dropFolderSpecified"></param>
        /// <param name="dropFolderSource"></param>
        /// <param name="dropFolderTarget"></param>
        /// <param name="msbuildArgumentsSpecified"></param>
        /// <param name="msbuildArguments"></param>
        /// <param name="cancellationToken"></param>
        public void ChangeBuildDefinitionBuildController(BuildDefinitionList buildDefinitionList,
                                                         bool buildControllerSpecified,
                                                         IBuildController buildController,
                                                         bool agentSettingsTagSpecified,
                                                         string tag,
                                                         bool dropFolderSpecified,
                                                         string dropFolderSource,
                                                         string dropFolderTarget,
                                                         bool msbuildArgumentsSpecified,
                                                         string msbuildArguments,
                                                         CancellationToken cancellationToken)
        {
            IChangeBuildDefinitionBuildControllerResult changeBuildDefinitionBuildControllerResult = null;
            ChangeBuildDefinitionBuildControllerEventArgs changeBuildDefinitionBuildControllerEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    changeBuildDefinitionBuildControllerEventArgs = new ChangeBuildDefinitionBuildControllerEventArgs();

                    changeBuildDefinitionBuildControllerResult = ChangeBuildDefinitionBuildController(buildDefinition,
                                                                                                      buildControllerSpecified,
                                                                                                      buildController,
                                                                                                      agentSettingsTagSpecified,
                                                                                                      tag,
                                                                                                      dropFolderSpecified,
                                                                                                      dropFolderSource,
                                                                                                      dropFolderTarget,
                                                                                                      msbuildArgumentsSpecified,
                                                                                                      msbuildArguments);

                    changeBuildDefinitionBuildControllerEventArgs.ChangeBuildDefinitionBuildControllerResult = changeBuildDefinitionBuildControllerResult;

                    if (BuildDefinitionBuildControllerChanged != null)
                    {
                        BuildDefinitionBuildControllerChanged(this, changeBuildDefinitionBuildControllerEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Change build definition build log verbosity.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="buildVerbosity"></param>
        /// <returns></returns>
        public IChangeBuildDefinitionBuildVerbosityResult ChangeBuildDefinitionBuildVerbosity(IBuildDefinition buildDefinition,
                                                                                              BuildVerbosity buildVerbosity)
        {
            IChangeBuildDefinitionBuildVerbosityResult result = null;
            IDictionary<string, object> processParameters = null;

            try
            {

                result = ChangeBuildDefinitionBuildVerbosityResultFactory.CreateChangeBuildDefinitionBuildVerbosityResult();
                result.BuildDefinition = buildDefinition;

                processParameters = WorkflowHelpers.DeserializeProcessParameters(buildDefinition.ProcessParameters);

                processParameters[ProcessParameterMetadata.StandardParameterNames.Verbosity] = buildVerbosity;

                //Change build log verbosity.
                buildDefinition.ProcessParameters = WorkflowHelpers.SerializeProcessParameters(processParameters);
                buildDefinition.Save();

                return result;
            }
            catch (Exception ex)
            {
                Status status = null;

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);

                return result;
            }

        }

        /// <summary>
        /// Change build definitions build verbosity.
        /// </summary>
        /// <param name="buildDefinitionList"></param>
        /// <param name="buildVerbosity"></param>
        public void ChangeBuildDefinitionBuildVerbosity(BuildDefinitionList buildDefinitionList,
                                                        BuildVerbosity buildVerbosity)
        {
            IChangeBuildDefinitionBuildVerbosityResult changeBuildDefinitionBuildVerbosityResult = null;
            ChangeBuildDefinitionBuildVerbosityEventArgs changeBuildDefinitionBuildVerbosityEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    changeBuildDefinitionBuildVerbosityEventArgs = new ChangeBuildDefinitionBuildVerbosityEventArgs();

                    changeBuildDefinitionBuildVerbosityResult = ChangeBuildDefinitionBuildVerbosity(buildDefinition, buildVerbosity);

                    changeBuildDefinitionBuildVerbosityEventArgs.ChangeBuildDefinitionBuildVerbosityResult = changeBuildDefinitionBuildVerbosityResult;

                    if (BuildDefinitionBuildVerbosityChanged != null)
                    {
                        BuildDefinitionBuildVerbosityChanged(this, changeBuildDefinitionBuildVerbosityEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Change build definitions build verbosity.
        /// </summary>
        /// <remarks>
        /// Overriden to take CancellationToken for use in parallel programming.
        /// </remarks>
        /// <param name="buildDefinitionList"></param>
        /// <param name="buildVerbosity"></param>
        /// <param name="cancellationToken"></param>
        public void ChangeBuildDefinitionBuildVerbosity(BuildDefinitionList buildDefinitionList,
                                                        BuildVerbosity buildVerbosity,
                                                        CancellationToken cancellationToken)
        {
            IChangeBuildDefinitionBuildVerbosityResult changeBuildDefinitionBuildVerbosityResult = null;
            ChangeBuildDefinitionBuildVerbosityEventArgs changeBuildDefinitionBuildVerbosityEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    changeBuildDefinitionBuildVerbosityEventArgs = new ChangeBuildDefinitionBuildVerbosityEventArgs();

                    changeBuildDefinitionBuildVerbosityResult = ChangeBuildDefinitionBuildVerbosity(buildDefinition, buildVerbosity);

                    changeBuildDefinitionBuildVerbosityEventArgs.ChangeBuildDefinitionBuildVerbosityResult = changeBuildDefinitionBuildVerbosityResult;

                    if (BuildDefinitionBuildVerbosityChanged != null)
                    {
                        BuildDefinitionBuildVerbosityChanged(this, changeBuildDefinitionBuildVerbosityEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Change build definition retention policy.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="numberToKeep"></param>
        public IChangeBuildDefinitionRetentionPolicyResult ChangeBuildDefinitionRetentionPolicy(IBuildDefinition buildDefinition, int numberToKeep)
        {
            IChangeBuildDefinitionRetentionPolicyResult result = null;

            try
            {
                result = ChangeBuildDefinitionRetentionPolicyResultFactory.CreateChangeBuildDefinitionRetentionPolicyResult();
                result.BuildDefinition = buildDefinition;

                //Change retention policy.
                foreach (IRetentionPolicy retentionPolicy in buildDefinition.RetentionPolicyList)
                {
                    retentionPolicy.NumberToKeep = numberToKeep;
                }
                buildDefinition.Save();

            }
            catch (Exception ex)
            {
                Status status = null;

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);

            }

            return result;

        }

        /// <summary>
        /// Change build definitions retention policy.
        /// </summary>
        /// <param name="buildDefinitionList"></param>
        /// <param name="numberToKeep"></param>
        public void ChangeBuildDefinitionRetentionPolicy(BuildDefinitionList buildDefinitionList,
                                                         int numberToKeep)
        {
            IChangeBuildDefinitionRetentionPolicyResult changeBuildDefinitionRetentionPolicyResult = null;
            ChangeBuildDefinitionRetentionPolicyEventArgs changeBuildDefinitionRetentionPolicyEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    changeBuildDefinitionRetentionPolicyEventArgs = new ChangeBuildDefinitionRetentionPolicyEventArgs();

                    changeBuildDefinitionRetentionPolicyResult = ChangeBuildDefinitionRetentionPolicy(buildDefinition, numberToKeep);

                    changeBuildDefinitionRetentionPolicyEventArgs.ChangeBuildDefinitionRetentionPolicyResult = changeBuildDefinitionRetentionPolicyResult;

                    if (BuildDefinitionRetentionPolicyChanged != null)
                    {
                        BuildDefinitionRetentionPolicyChanged(this, changeBuildDefinitionRetentionPolicyEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Change build definitions retention policy.
        /// </summary>
        /// <param name="buildDefinitionList"></param>
        /// <param name="numberToKeep"></param>
        public void ChangeBuildDefinitionRetentionPolicy(BuildDefinitionList buildDefinitionList,
                                                         int numberToKeep,
                                                         CancellationToken cancellationToken)
        {
            IChangeBuildDefinitionRetentionPolicyResult changeBuildDefinitionRetentionPolicyResult = null;
            ChangeBuildDefinitionRetentionPolicyEventArgs changeBuildDefinitionRetentionPolicyEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    changeBuildDefinitionRetentionPolicyEventArgs = new ChangeBuildDefinitionRetentionPolicyEventArgs();

                    changeBuildDefinitionRetentionPolicyResult = ChangeBuildDefinitionRetentionPolicy(buildDefinition, numberToKeep);

                    changeBuildDefinitionRetentionPolicyEventArgs.ChangeBuildDefinitionRetentionPolicyResult = changeBuildDefinitionRetentionPolicyResult;

                    if (BuildDefinitionRetentionPolicyChanged != null)
                    {
                        BuildDefinitionRetentionPolicyChanged(this, changeBuildDefinitionRetentionPolicyEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Change build definition schedules.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="daysToBuild"></param>
        /// <param name="startTime"></param>
        public IChangeBuildDefinitionSchedulesResult ChangeBuildDefinitionSchedules(IBuildDefinition buildDefinition,
                                                                                    ScheduleDays daysToBuild,
                                                                                    int startTime)
        {
            IChangeBuildDefinitionSchedulesResult result = null;
            Status status = null;

            try
            {

                result = ChangeBuildDefinitionSchedulesResultFactory.CreateChangeBuildDefinitionScheduleResult();
                result.BuildDefinition = buildDefinition;

                // Build definition has no schedules defined.
                if (buildDefinition.Schedules == null ||
                    buildDefinition.Schedules.Count <= 0)
                {
                    //Add status and skip changing build definition schedule.
                    status = new Status();
                    status.Exception = new OperationCanceledException(string.Format("Cannot change build definition schedule. Build definition has no schedules to change."));
                    status.StatusServerity = StatusTypes.StatusSeverity.Error;
                    result.StatusList.Add(status);
                    return result;
                }

                foreach (ISchedule schedule in buildDefinition.Schedules)
                {
                    schedule.DaysToBuild = daysToBuild;
                    schedule.StartTime = startTime;
                }
                buildDefinition.Save();

            }
            catch (Exception ex)
            {

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        /// <summary>
        /// Change build definitions schedules.
        /// </summary>
        /// <param name="buildDefinitionList"></param>
        /// <param name="numberToKeep"></param>
        public void ChangeBuildDefinitionSchedules(BuildDefinitionList buildDefinitionList,
                                                   ScheduleDays daysToBuild,
                                                   int startTime)
        {
            IChangeBuildDefinitionSchedulesResult changeBuildDefinitionSchedulesResult = null;
            ChangeBuildDefinitionSchedulesEventArgs changeBuildDefinitionSchedulesEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    changeBuildDefinitionSchedulesEventArgs = new ChangeBuildDefinitionSchedulesEventArgs();

                    changeBuildDefinitionSchedulesResult = ChangeBuildDefinitionSchedules(buildDefinition, daysToBuild, startTime);

                    changeBuildDefinitionSchedulesEventArgs.ChangeBuildDefinitionSchedulesResult = changeBuildDefinitionSchedulesResult;

                    if (BuildDefinitionSchedulesChanged != null)
                    {
                        BuildDefinitionSchedulesChanged(this, changeBuildDefinitionSchedulesEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Change build definitions schedules.
        /// </summary>
        /// <remarks>
        /// Overriden to take CancellationToken for use in parallel programming.
        /// </remarks>
        /// <param name="buildDefinitionList"></param>
        /// <param name="daysToBuild"></param>
        /// <param name="startTime"></param>
        /// <param name="cancellationToken"></param>
        public void ChangeBuildDefinitionSchedules(BuildDefinitionList buildDefinitionList,
                                                   ScheduleDays daysToBuild,
                                                   int startTime,
                                                   CancellationToken cancellationToken)
        {
            IChangeBuildDefinitionSchedulesResult changeBuildDefinitionSchedulesResult = null;
            ChangeBuildDefinitionSchedulesEventArgs changeBuildDefinitionSchedulesEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    changeBuildDefinitionSchedulesEventArgs = new ChangeBuildDefinitionSchedulesEventArgs();

                    changeBuildDefinitionSchedulesResult = ChangeBuildDefinitionSchedules(buildDefinition, daysToBuild, startTime);

                    changeBuildDefinitionSchedulesEventArgs.ChangeBuildDefinitionSchedulesResult = changeBuildDefinitionSchedulesResult;

                    if (BuildDefinitionSchedulesChanged != null)
                    {
                        BuildDefinitionSchedulesChanged(this, changeBuildDefinitionSchedulesEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Clone build definition.
        /// </summary>
        /// <param name="teamProjectName"></param>
        /// <param name="buildDefinition"></param>
        /// <param name="sourceBuildDefinitionNamePrefix"></param>
        /// <param name="targetBuildDefinitionNamePrefix"></param>
        /// <param name="sourceBranchName"></param>
        /// <param name="targetBranchName"></param>
        /// <returns></returns>
        public ICloneBuildDefinitionResult CloneBuildDefinition(string teamProjectName,
                                                                IBuildDefinition buildDefinition,
                                                                string sourceBuildDefinitionNamePrefix,
                                                                string targetBuildDefinitionNamePrefix,
                                                                string sourceBranchName,
                                                                string targetBranchName)
        {
            ICloneBuildDefinitionResult result = null;

            IBuildDefinition targetBuildDefinition = null;

            try
            {
                result = CloneBuildDefinitionResultFactory.CreateCloneBuildDefinitionResult();
                result.BuildDefinition = buildDefinition;

                targetBuildDefinition = CreateNewBuildDefinitionFromAnother(teamProjectName, buildDefinition);

                //Disable build definition by default.
                targetBuildDefinition.QueueStatus = DefinitionQueueStatus.Disabled;

                ReplaceTextInBuildDefinition(ref targetBuildDefinition,
                                             sourceBuildDefinitionNamePrefix,
                                             targetBuildDefinitionNamePrefix,
                                             sourceBranchName,
                                             targetBranchName);

                targetBuildDefinition.Save();

                result.BuildDefinition = targetBuildDefinition;

            }
            catch (BuildDefinitionAlreadyExistsException buildDefinitionAlreadyExistsException)
            {
                Status status = null;

                result = CloneBuildDefinitionResultFactory.CreateCloneBuildDefinitionResult();
                status = new Status();
                status.Exception = buildDefinitionAlreadyExistsException;
                status.StatusServerity = StatusTypes.StatusSeverity.Warning;
                result.StatusList.Add(status);
            }
            catch (Exception ex)
            {
                Status status = null;

                result = CloneBuildDefinitionResultFactory.CreateCloneBuildDefinitionResult();
                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        /// <summary>
        /// Clone build definitions
        /// </summary>
        /// <param name="buildDefinitionList"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="sourceBuildDefinitionNamePrefix"></param>
        /// <param name="targetBuildDefinitionNamePrefix"></param>
        /// <param name="sourceBranchName"></param>
        /// <param name="targetBranchName"></param>
        public void CloneBuildDefinition(BuildDefinitionList buildDefinitionList,
                                         string teamProjectName,
                                         string sourceBuildDefinitionNamePrefix,
                                         string targetBuildDefinitionNamePrefix,
                                         string sourceBranchName,
                                         string targetBranchName)
        {
            ICloneBuildDefinitionResult cloneBuildDefinitionResult = null;
            CloneBuildDefinitionEventArgs cloneBuildDefinitionEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    cloneBuildDefinitionEventArgs = new CloneBuildDefinitionEventArgs();

                    cloneBuildDefinitionResult = CloneBuildDefinition(teamProjectName,
                                                                      buildDefinition,
                                                                      sourceBuildDefinitionNamePrefix,
                                                                      targetBuildDefinitionNamePrefix,
                                                                      sourceBranchName,
                                                                      targetBranchName);

                    cloneBuildDefinitionEventArgs.CloneBuildDefinitionResult = cloneBuildDefinitionResult;

                    if (BuildDefinitionCloned != null)
                    {
                        BuildDefinitionCloned(this, cloneBuildDefinitionEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Clone build definitions
        /// </summary>
        /// <remarks>
        /// Overriden to take CancellationToken for use in parallel programming.
        /// </remarks>
        /// <param name="buildDefinitionList"></param>
        /// <param name="teamProjectName"></param>
        /// <param name="sourceBuildDefinitionNamePrefix"></param>
        /// <param name="targetBuildDefinitionNamePrefix"></param>
        /// <param name="sourceBranchName"></param>
        /// <param name="targetBranchName"></param>
        /// <param name="cancellationToken"></param>
        public void CloneBuildDefinition(BuildDefinitionList buildDefinitionList,
                                         string teamProjectName,
                                         string sourceBuildDefinitionNamePrefix,
                                         string targetBuildDefinitionNamePrefix,
                                         string sourceBranchName,
                                         string targetBranchName,
                                         CancellationToken cancellationToken)
        {
            ICloneBuildDefinitionResult cloneBuildDefinitionResult = null;
            CloneBuildDefinitionEventArgs cloneBuildDefinitionEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    cloneBuildDefinitionEventArgs = new CloneBuildDefinitionEventArgs();

                    cloneBuildDefinitionResult = CloneBuildDefinition(teamProjectName,
                                                                      buildDefinition,
                                                                      sourceBuildDefinitionNamePrefix,
                                                                      targetBuildDefinitionNamePrefix,
                                                                      sourceBranchName,
                                                                      targetBranchName);

                    cloneBuildDefinitionEventArgs.CloneBuildDefinitionResult = cloneBuildDefinitionResult;

                    if (BuildDefinitionCloned != null)
                    {
                        BuildDefinitionCloned(this, cloneBuildDefinitionEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Delete build definition builds.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="deleteBuildDefinition"></param>
        /// <returns></returns>
        public IDeleteBuildDefinitionBuildsResult DeleteBuildDefinitionBuilds(IBuildDefinition buildDefinition,
                                                                              bool deleteBuildDefinition)
        {
            IDeleteBuildDefinitionBuildsResult result = null;
            IBuildDetail[] buildDetailList = null;

            try
            {

                result = DeleteBuildDefinitionBuildsResultFactory.CreateDeleteBuildDefinitionBuildsResult();
                result.BuildDefinition = buildDefinition;

                buildDetailList = buildDefinition.QueryBuilds();

                foreach (IBuildDetail buildDetail in buildDetailList)
                {
                    buildDetail.Delete();
                }

                if (deleteBuildDefinition == true)
                {
                    buildDefinition.Delete();
                }

            }
            catch (Exception ex)
            {
                Status status = null;

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        /// <summary>
        /// Delete build definitions builds.
        /// </summary>
        /// <param name="buildDefinitionList"></param>
        /// <param name="deleteBuildDefinition"></param>
        public void DeleteBuildDefinitionBuilds(BuildDefinitionList buildDefinitionList, bool deleteBuildDefinition)
        {
            IDeleteBuildDefinitionBuildsResult deleteBuildDefinitionBuildsResult = null;
            DeleteBuildDefinitionBuildsEventArgs deleteBuildDefinitionBuildsEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    deleteBuildDefinitionBuildsEventArgs = new DeleteBuildDefinitionBuildsEventArgs();

                    deleteBuildDefinitionBuildsResult = DeleteBuildDefinitionBuilds(buildDefinition, deleteBuildDefinition);
                    deleteBuildDefinitionBuildsEventArgs.DeleteBuildDefinitionBuildsResult = deleteBuildDefinitionBuildsResult;

                    if (BuildDefinitionBuildsDeleted != null)
                    {
                        BuildDefinitionBuildsDeleted(this, deleteBuildDefinitionBuildsEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Delete build definitions builds.
        /// </summary>
        /// <remarks>
        /// Overriden to take CancellationToken for use in parallel programming.
        /// </remarks>
        /// <param name="buildDefinitionList"></param>
        /// <param name="deleteBuildDefinition"></param>
        /// <param name="cancellationToken"></param>
        public void DeleteBuildDefinitionBuilds(BuildDefinitionList buildDefinitionList,
                                                bool deleteBuildDefinition,
                                                CancellationToken cancellationToken)
        {
            IDeleteBuildDefinitionBuildsResult deleteBuildDefinitionBuildsResult = null;
            DeleteBuildDefinitionBuildsEventArgs deleteBuildDefinitionBuildsEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    deleteBuildDefinitionBuildsEventArgs = new DeleteBuildDefinitionBuildsEventArgs();

                    deleteBuildDefinitionBuildsResult = DeleteBuildDefinitionBuilds(buildDefinition, deleteBuildDefinition);
                    deleteBuildDefinitionBuildsEventArgs.DeleteBuildDefinitionBuildsResult = deleteBuildDefinitionBuildsResult;

                    if (BuildDefinitionBuildsDeleted != null)
                    {
                        BuildDefinitionBuildsDeleted(this, deleteBuildDefinitionBuildsEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable build definition.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="enableValue"></param>
        public IEnableBuildDefinitionResult EnableBuildDefinition(IBuildDefinition buildDefinition, bool enableValue)
        {
            IEnableBuildDefinitionResult result = null;

            try
            {

                result = EnableBuildDefinitionResultFactory.CreateEnableBuildDefinitionResult();
                result.BuildDefinition = buildDefinition;

                //Change queue status.
                if (enableValue == true)
                {
                    buildDefinition.QueueStatus = DefinitionQueueStatus.Enabled;
                }
                else
                {
                    buildDefinition.QueueStatus = DefinitionQueueStatus.Disabled;
                }
                buildDefinition.Save();

            }
            catch (Exception ex)
            {
                Status status = null;

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        /// <summary>
        /// Enable build definitions.
        /// </summary>
        /// <param name="buildDefinitionList"></param>
        /// <param name="enableValue"></param>
        public void EnableBuildDefinition(BuildDefinitionList buildDefinitionList,
                                          bool enableValue)
        {
            IEnableBuildDefinitionResult enableBuildDefinitionResult = null;
            EnableBuildDefinitionEventArgs enableBuildDefinitionEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    enableBuildDefinitionEventArgs = new EnableBuildDefinitionEventArgs();

                    enableBuildDefinitionResult = EnableBuildDefinition(buildDefinition, enableValue);

                    enableBuildDefinitionEventArgs.EnableBuildDefinitionResult = enableBuildDefinitionResult;

                    if (BuildDefinitionEnabled != null)
                    {
                        BuildDefinitionEnabled(this, enableBuildDefinitionEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable build definitions.
        /// </summary>
        /// <remarks>
        /// Overriden to take CancellationToken for use in parallel programming.
        /// </remarks>
        /// <param name="buildDefinitionList"></param>
        /// <param name="enableValue"></param>
        /// <param name="cancellationToken"></param>
        public void EnableBuildDefinition(BuildDefinitionList buildDefinitionList,
                                          bool enableValue,
                                          CancellationToken cancellationToken)
        {
            IEnableBuildDefinitionResult enableBuildDefinitionResult = null;
            EnableBuildDefinitionEventArgs enableBuildDefinitionEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    enableBuildDefinitionEventArgs = new EnableBuildDefinitionEventArgs();

                    enableBuildDefinitionResult = EnableBuildDefinition(buildDefinition, enableValue);

                    enableBuildDefinitionEventArgs.EnableBuildDefinitionResult = enableBuildDefinitionResult;

                    if (BuildDefinitionEnabled != null)
                    {
                        BuildDefinitionEnabled(this, enableBuildDefinitionEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve build definition list.
        /// </summary>
        /// <param name="teamProjectName"></param>
        /// <returns></returns>
        public BuildDefinitionList GetBuildDefinitionList(string teamProjectName)
        {
            BuildDefinitionList result = null;
            BuildDefinitionList buildDefinitionList = null;
            IBuildDefinition[] buildDefinitionArray = null;

            try
            {

                buildDefinitionList = new BuildDefinitionList();

                buildDefinitionArray = this.BuildServer.QueryBuildDefinitions(teamProjectName);
                foreach (IBuildDefinition buildDefinition in buildDefinitionArray)
                {
                    buildDefinitionList.Add(buildDefinition);
                }

                result = buildDefinitionList;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve build controller.
        /// </summary>
        /// <param name="buildControllerName"></param>
        /// <returns></returns>
        public IBuildController GetBuildController(string buildControllerName)
        {
            IBuildController result = null;
            IBuildServer buildServer = null;

            try
            {

                buildServer = GetBuildServer();

                result = buildServer.GetBuildController(buildControllerName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build controller list.
        /// </summary>
        /// <param name="includeAgents"></param>
        /// <returns></returns>
        public List<IBuildController> GetBuildControllerList(bool includeAgents)
        {
            List<IBuildController> result = null;
            IBuildServer buildServer = null;

            IBuildController[] buildControllerList = null;

            try
            {
                result = new List<IBuildController>();

                buildServer = GetBuildServer();
                buildControllerList = buildServer.QueryBuildControllers(includeAgents);
                if (buildControllerList != null)
                {
                    result = buildControllerList.ToList<IBuildController>();
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve build server.
        /// </summary>
        /// <param name="buildControllerName"></param>
        /// <returns></returns>
        public IBuildServer GetBuildServer()
        {
            IBuildServer result = null;
            TfsTeamProjectCollection tfsTeamProjectCollection = null;
            Object service = null;

            try
            {

                tfsTeamProjectCollection = this.BuildServer.TeamProjectCollection;
                tfsTeamProjectCollection.EnsureAuthenticated();

                service = tfsTeamProjectCollection.GetService(typeof(IBuildServer));
                if (service is IBuildServer)
                {
                    result = (IBuildServer)service;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Request build.
        /// </summary>
        /// <param name="buildDefinition"></param>
        /// <param name="msbuildArguments"></param>
        /// <param name="overrideQueueStatus"></param>
        /// <param name="queuePriority"></param>
        /// <param name="disableBuildDefinitionAfterBuildRequest"></param>
        /// <returns></returns>
        public IRequestBuildResult RequestBuild(IBuildDefinition buildDefinition,
                                                string msbuildArguments,
                                                bool overrideQueueStatus,
                                                QueuePriority queuePriority,
                                                bool disableBuildDefinitionAfterBuildRequest)
        {
            IRequestBuildResult result = null;
            IBuildRequest buildRequest = null;
            string processParameters = string.Empty;
            IQueuedBuild queuedBuild = null;
            bool buildDefinitionEnabledToOverrideQueueStatus = false;

            try
            {
                result = RequestBuildResultFactory.CreateQueueBuildDefinitionResult();
                result.BuildDefinition = buildDefinition;
                result.MSBuildArguments = msbuildArguments;

                buildRequest = buildDefinition.CreateBuildRequest();

                if (overrideQueueStatus == true && buildDefinition.QueueStatus == DefinitionQueueStatus.Disabled)
                {
                    buildDefinition.QueueStatus = DefinitionQueueStatus.Enabled;
                    result.BuildDefinition.Save();

                    buildDefinitionEnabledToOverrideQueueStatus = true;
                }

                if (string.IsNullOrEmpty(msbuildArguments) != true)
                {
                    processParameters = ChangeProcessParametersMSBuildArguments(buildDefinition.ProcessParameters,
                                                                                msbuildArguments);
                    buildRequest.ProcessParameters = processParameters;
                }

                buildRequest.Priority = queuePriority;

                queuedBuild = this.BuildServer.QueueBuild(buildRequest);

                result.QueuedBuild = queuedBuild;

                if ((overrideQueueStatus == true && buildDefinitionEnabledToOverrideQueueStatus == true) ||
                    (disableBuildDefinitionAfterBuildRequest == true))
                {
                    buildDefinition.QueueStatus = DefinitionQueueStatus.Disabled;
                    result.BuildDefinition.Save();
                }

            }
            catch (BuildDefinitionDisabledException buildDefinitionDisabledException)
            {
                Status status = null;

                status = new Status();
                status.Exception = buildDefinitionDisabledException;
                status.StatusServerity = StatusTypes.StatusSeverity.Warning;
                result.StatusList.Add(status);
            }
            catch (Exception ex)
            {
                Status status = null;

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        /// <summary>
        /// Request build definitions builds.
        /// </summary>
        /// <param name="buildDefinitionList"></param>
        /// <param name="disableBuildDefinitionAfterBuildRequest"></param>
        /// <param name="msbuildArguments"></param>
        /// <param name="overrideQueueStatus"></param>
        /// <param name="queuePriority"></param>
        public void RequestBuild(BuildDefinitionList buildDefinitionList,
                                 bool disableBuildDefinitionAfterBuildRequest,
                                 string msbuildArguments,
                                 bool overrideQueueStatus,
                                 QueuePriority queuePriority)
        {
            IRequestBuildResult requestBuildResult = null;
            RequestBuildEventArgs requestBuildEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    requestBuildEventArgs = new RequestBuildEventArgs();

                    requestBuildResult = RequestBuild(buildDefinition,
                                                      msbuildArguments,
                                                      overrideQueueStatus,
                                                      queuePriority,
                                                      disableBuildDefinitionAfterBuildRequest);

                    requestBuildEventArgs.RequestBuildResult = requestBuildResult;

                    if (BuildRequested != null)
                    {
                        BuildRequested(this, requestBuildEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Request build definitions builds.
        /// </summary>
        /// <remarks>
        /// Overriden to take CancellationToken for use in parallel programming.
        /// </remarks>
        /// <param name="buildDefinitionList"></param>
        /// <param name="msbuildArguments"></param>
        /// <param name="overrideQueueStatus"></param>
        /// <param name="queuePriority"></param>
        /// <param name="disableBuildDefinitionAfterBuildRequest"></param>
        /// <param name="cancellationToken"></param>
        public void RequestBuild(BuildDefinitionList buildDefinitionList,
                                 string msbuildArguments,
                                 bool overrideQueueStatus,
                                 QueuePriority queuePriority,
                                 bool disableBuildDefinitionAfterBuildRequest,
                                 CancellationToken cancellationToken)
        {
            IRequestBuildResult requestBuildResult = null;
            RequestBuildEventArgs requestBuildEventArgs = null;

            try
            {

                foreach (IBuildDefinition buildDefinition in buildDefinitionList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    requestBuildEventArgs = new RequestBuildEventArgs();

                    requestBuildResult = RequestBuild(buildDefinition,
                                                      msbuildArguments,
                                                      overrideQueueStatus,
                                                      queuePriority,
                                                      disableBuildDefinitionAfterBuildRequest);

                    requestBuildEventArgs.RequestBuildResult = requestBuildResult;

                    if (BuildRequested != null)
                    {
                        BuildRequested(this, requestBuildEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create build definition from another.
        /// </summary>
        /// <param name="teamProjectName"></param>
        /// <param name="sourceBuildDefinition"></param>
        /// <returns></returns>
        protected IBuildDefinition CreateNewBuildDefinitionFromAnother(string teamProjectName, IBuildDefinition sourceBuildDefinition)
        {
            IBuildDefinition result = null;
            IBuildDefinition targetBuildDefinition = null;

            try
            {

                targetBuildDefinition = this.BuildServer.CreateBuildDefinition(teamProjectName);

                targetBuildDefinition.Name = sourceBuildDefinition.Name;
                targetBuildDefinition.Description = sourceBuildDefinition.Description;

                targetBuildDefinition.ContinuousIntegrationType = sourceBuildDefinition.ContinuousIntegrationType;

                targetBuildDefinition.Schedules.Clear();
                foreach (ISchedule schedule in sourceBuildDefinition.Schedules)
                {
                    targetBuildDefinition.Schedules.Add(schedule);
                }

                foreach (IWorkspaceMapping map in sourceBuildDefinition.Workspace.Mappings)
                {
                    targetBuildDefinition.Workspace.AddMapping(map.ServerItem, map.LocalItem, map.MappingType);
                }

                targetBuildDefinition.BuildController = sourceBuildDefinition.BuildController;

                targetBuildDefinition.DefaultDropLocation = sourceBuildDefinition.DefaultDropLocation;

                targetBuildDefinition.Process = sourceBuildDefinition.Process;
                targetBuildDefinition.ProcessParameters = sourceBuildDefinition.ProcessParameters;

                targetBuildDefinition.RetentionPolicyList.Clear();
                foreach (IRetentionPolicy retentionPolicy in sourceBuildDefinition.RetentionPolicyList)
                {
                    targetBuildDefinition.AddRetentionPolicy(retentionPolicy.BuildReason,
                                                             retentionPolicy.BuildStatus,
                                                             retentionPolicy.NumberToKeep,
                                                             retentionPolicy.DeleteOptions);
                }

                targetBuildDefinition.QueueStatus = sourceBuildDefinition.QueueStatus;

                result = targetBuildDefinition;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Replace text in build definition.
        /// </summary>
        /// <param name="buildDefinition"></param>
        protected void ReplaceTextInBuildDefinition(ref IBuildDefinition buildDefinition,
                                                    string sourceBuildDefinitionNamePrefix,
                                                    string targetBuildDefinitionNamePrefix,
                                                    string sourceBranchName,
                                                    string targetBranchName)
        {

            try
            {

                buildDefinition.Name = ReplaceTokenInTextWithValue(buildDefinition.Name,
                                                                   sourceBuildDefinitionNamePrefix,
                                                                   targetBuildDefinitionNamePrefix);

                buildDefinition.Description = ReplaceTokenInTextWithValue(buildDefinition.Description,
                                                                          sourceBuildDefinitionNamePrefix,
                                                                          targetBuildDefinitionNamePrefix);

                buildDefinition.Description = ReplaceTokenInTextWithValue(buildDefinition.Description,
                                                                          sourceBranchName,
                                                                          targetBranchName);

                foreach (IWorkspaceMapping map in buildDefinition.Workspace.Mappings)
                {
                    map.ServerItem = ReplaceTokenInTextWithValue(map.ServerItem,
                                                                sourceBranchName,
                                                                targetBranchName);

                }

                buildDefinition.DefaultDropLocation = ReplaceTextInDefaultDropLocation(buildDefinition.DefaultDropLocation,
                                                                                       sourceBuildDefinitionNamePrefix,
                                                                                       targetBuildDefinitionNamePrefix);

                buildDefinition.ProcessParameters = ReplaceTextInProcessParameters(buildDefinition.ProcessParameters,
                                                                                   sourceBuildDefinitionNamePrefix,
                                                                                   targetBuildDefinitionNamePrefix,
                                                                                   sourceBranchName,
                                                                                   targetBranchName);
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Replace text in build definition process parameters.
        /// </summary>
        /// <param name="processParametersAsText"></param>
        /// <param name="sourceBuildDefinitionNamePrefix"></param>
        /// <param name="targetBuildDefinitionNamePrefix"></param>
        /// <param name="sourceBranchName"></param>
        /// <param name="targetBranchName"></param>
        /// <returns></returns>
        protected string ReplaceTextInProcessParameters(string processParametersAsText,
                                                        string sourceBuildDefinitionNamePrefix,
                                                        string targetBuildDefinitionNamePrefix,
                                                        string sourceBranchName,
                                                        string targetBranchName)
        {

            string result = string.Empty;
            IDictionary<String, Object> processParameters = null;

            string configurationFolderPath = string.Empty;
            string[] projectsToBuildList = null;
            object valueAsObject = null;

            try
            {


                processParameters = WorkflowHelpers.DeserializeProcessParameters(processParametersAsText);

                //Replace text in process parameter: configuration folder path (if available).
                if (processParameters.TryGetValue(ProcessParameterMetadata.StandardParameterNames.ConfigurationFolderPath, out valueAsObject))
                {
                    configurationFolderPath = (string)valueAsObject;
                    configurationFolderPath = ReplaceTokenInTextWithValue(configurationFolderPath,
                                                                          sourceBranchName,
                                                                          targetBranchName);
                    processParameters[ProcessParameterMetadata.StandardParameterNames.ConfigurationFolderPath] = configurationFolderPath;

                }

                //Replace text in process parameter: projects to build (if available).
                if (processParameters.TryGetValue(ProcessParameterMetadata.StandardParameterNames.ProjectsToBuild, out valueAsObject))
                {
                    projectsToBuildList = (string[])valueAsObject;
                    for (int projectsToBuildIndex = 0; projectsToBuildIndex < projectsToBuildList.Length; projectsToBuildIndex++)
                    {
                        projectsToBuildList[projectsToBuildIndex] = ReplaceTokenInTextWithValue(projectsToBuildList[projectsToBuildIndex],
                                                                      sourceBranchName,
                                                                      targetBranchName);
                    }
                    processParameters[ProcessParameterMetadata.StandardParameterNames.ProjectsToBuild] = projectsToBuildList;

                }


                processParametersAsText = WorkflowHelpers.SerializeProcessParameters(processParameters);

                result = processParametersAsText;

                return result;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Replace text in build definition default drop location.
        /// </summary>
        /// <param name="defaultDropLocation"></param>
        /// <param name="sourceBuildDefinitionNamePrefix"></param>
        /// <param name="targetBuildDefinitionNamePrefix"></param>
        /// <param name="sourceBranchName"></param>
        /// <param name="targetBranchName"></param>
        /// <returns></returns>
        protected string ReplaceTextInDefaultDropLocation(string defaultDropLocation,
                                                          string sourceBuildDefinitionNamePrefix,
                                                          string targetBuildDefinitionNamePrefix)
        {

            string result = string.Empty;

            try
            {

                result = ReplaceTokenInTextWithValue(defaultDropLocation,
                                                     sourceBuildDefinitionNamePrefix,
                                                     targetBuildDefinitionNamePrefix);

                return result;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Replace token in text with specified value.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="token"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string ReplaceTokenInTextWithValue(string text, string token, string value)
        {
            string result = string.Empty;

            try
            {

                if (string.IsNullOrEmpty(text) == true)
                {
                    result = text;
                    return result;
                }

                result = Regex.Replace(text, token, value, RegexOptions.IgnoreCase);

                return result;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Change MSBuild arguments in process parameters.
        /// </summary>
        /// <param name="processParametersAsText"></param>
        /// <param name="msbuildArguments"></param>
        /// <returns></returns>
        protected string ChangeProcessParametersMSBuildArguments(string processParametersAsText,
                                                                 string msbuildArguments)
        {

            string result = string.Empty;
            IDictionary<String, Object> processParameters = null;
            string msbuildArgumentsAsText = string.Empty;

            try
            {

                processParameters = WorkflowHelpers.DeserializeProcessParameters(processParametersAsText);

                if (processParameters.ContainsKey(ProcessParameterMetadata.StandardParameterNames.MSBuildArguments) == true)
                {
                    msbuildArgumentsAsText = processParameters[ProcessParameterMetadata.StandardParameterNames.MSBuildArguments].ToString();
                    msbuildArgumentsAsText += " " + msbuildArguments;
                }
                else
                {
                    msbuildArgumentsAsText = msbuildArguments;
                }

                processParameters[ProcessParameterMetadata.StandardParameterNames.MSBuildArguments] = msbuildArgumentsAsText;

                processParametersAsText = WorkflowHelpers.SerializeProcessParameters(processParameters);

                result = processParametersAsText;

                return result;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Change agent settings tag in process parameters.
        /// </summary>
        /// <param name="processParametersAsText"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        protected string ChangeProcessParametersAgentSettingsTag(string processParametersAsText,
                                                                    string tag)
        {

            string result = string.Empty;
            IDictionary<String, Object> processParameters = null;

            object agentSettingsAsObject = null;
            Microsoft.TeamFoundation.Build.Workflow.Activities.AgentSettings agentSettings = null;
            List<string> tagList = null;
            bool agentSettingsAlreadyExists = false;


            try
            {

                processParameters = WorkflowHelpers.DeserializeProcessParameters(processParametersAsText);

                if (processParameters.TryGetValue(ProcessParameterMetadata.StandardParameterNames.AgentSettings, out agentSettingsAsObject) == true)
                {
                    if (agentSettingsAsObject is Microsoft.TeamFoundation.Build.Workflow.Activities.AgentSettings == true)
                    {
                        agentSettings = (Microsoft.TeamFoundation.Build.Workflow.Activities.AgentSettings)agentSettingsAsObject;
                        agentSettingsAlreadyExists = true;
                    }
                    else
                    {
                        agentSettings = this.CreateAgentSettingsWithDefaultValues();
                        agentSettingsAlreadyExists = false;

                    }
                }
                else
                {
                    agentSettings = this.CreateAgentSettingsWithDefaultValues();
                    agentSettingsAlreadyExists = false;
                }

                //Clear tag.
                agentSettings.Tags.Clear();

                //Tag was specified.
                if (string.IsNullOrEmpty(tag) == false)
                {
                    tagList = tag.Split(',').ToList();

                    foreach (string newTag in tagList)
                    {
                        agentSettings.Tags.Add(newTag);
                    }
                }

                if (agentSettingsAlreadyExists == true)
                {
                    processParameters[ProcessParameterMetadata.StandardParameterNames.AgentSettings] = agentSettings;
                }
                else
                {
                    processParameters.Add(ProcessParameterMetadata.StandardParameterNames.AgentSettings, agentSettings);
                }


                processParametersAsText = WorkflowHelpers.SerializeProcessParameters(processParameters);

                result = processParametersAsText;

                return result;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Create process parameter agent settings with default values.
        /// </summary>
        /// <returns></returns>
        protected AgentSettings CreateAgentSettingsWithDefaultValues()
        {
            AgentSettings result = null;

            try
            {
                result = new AgentSettings();

                //Set defaults.
                result.MaxWaitTime = new TimeSpan(4, 0, 0);
                result.TagComparison = TagComparison.MatchExactly;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Change agent settings MSBuild arguments in process parameters.
        /// </summary>
        /// <param name="processParametersAsText"></param>
        /// <param name="msbuildArguments"></param>
        /// <returns></returns>
        protected string ChangeProcessParametersAgentSettingsMSBuild(string processParametersAsText,
                                                                     string msbuildArguments)
        {

            string result = string.Empty;
            IDictionary<String, Object> processParameters = null;
            object msbuildAsObject = null;
            string msbuildArgumentsAsText = string.Empty;
            bool msbuildArgumentsAlreadyExists = false;

            try
            {

                processParameters = WorkflowHelpers.DeserializeProcessParameters(processParametersAsText);

                if (processParameters.TryGetValue(ProcessParameterMetadata.StandardParameterNames.MSBuildArguments, out msbuildAsObject) == true)
                {
                    if (msbuildAsObject is System.String == true)
                    {
                        msbuildArgumentsAsText = (System.String)msbuildAsObject;
                        msbuildArgumentsAlreadyExists = true;
                    }
                    else
                    {
                        msbuildArgumentsAsText = string.Empty;
                        msbuildArgumentsAlreadyExists = false;

                    }
                }
                else
                {
                    msbuildArgumentsAsText = string.Empty;
                    msbuildArgumentsAlreadyExists = false;
                }

                msbuildArgumentsAsText = msbuildArguments;

                if (msbuildArgumentsAlreadyExists == true)
                {
                    processParameters[ProcessParameterMetadata.StandardParameterNames.MSBuildArguments] = msbuildArgumentsAsText;
                }
                else
                {
                    processParameters.Add(ProcessParameterMetadata.StandardParameterNames.MSBuildArguments, msbuildArgumentsAsText);
                }

                processParametersAsText = WorkflowHelpers.SerializeProcessParameters(processParameters);

                result = processParametersAsText;

                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Create process parameter MSBuild arguments with default values.
        /// </summary>
        /// <returns></returns>
        protected MSBuild CreateMSBuildArgumentsWithDefaultValues()
        {
            MSBuild result = null;

            try
            {
                result = new MSBuild();

                //Set defaults.
                result.CommandLineArguments = result.CommandLineArguments;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion
    }
}
