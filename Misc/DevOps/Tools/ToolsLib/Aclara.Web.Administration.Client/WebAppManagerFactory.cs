﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.Administration;
using Microsoft.Web.Management;
using Microsoft.Web.Management.Client;

namespace Aclara.Web.Administration.Client
{

    /// <summary>
    /// Web app manager factory.
    /// </summary>
    public class WebAppManagerFactory
    {

        /// <summary>
        /// Create server manager.
        /// </summary>
        /// <returns></returns>
        public ServerManager CreateServerManager()
        {
            ServerManager result = null;

            try
            {
                result = new ServerManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Create web application manager.
        /// </summary>
        /// <returns></returns>
        public WebAppManager CreateWebAppManager()
        {
            WebAppManager result = null;
            ServerManager serverManager = null;
            try
            {

                serverManager = this.CreateServerManager();

                result = new WebAppManager(serverManager);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

    }
}
