﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Web.Administration.Client.Interfaces;
using Aclara.Web.Administration.Client.Results;

namespace Aclara.Web.Administration.Client.Events
{
    /// <summary>
    /// Change physical path event arguments.
    /// </summary>
    public class ChangePhysicalPathEventArgs : EventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IChangePhysicalPathResult _changePhysicalPathResult;

        #endregion
        
        #region Public Properties

        /// <summary>
        /// Property: Change phyiscal path result.
        /// </summary>
        public IChangePhysicalPathResult ChangePhysicalPathResult
        {
            get { return _changePhysicalPathResult; }
            set { _changePhysicalPathResult = value; }
        }

        #endregion

        #region Public Constructors
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ChangePhysicalPathEventArgs()
        {
            _changePhysicalPathResult = new ChangePhysicalPathResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="changePhysicalPathResult"></param>
        public ChangePhysicalPathEventArgs(IChangePhysicalPathResult changePhysicalPathResult)
        {
            _changePhysicalPathResult = changePhysicalPathResult;
        }

        #endregion

    }
}
