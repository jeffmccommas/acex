﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Web.Administration.Client.Interfaces;
using Aclara.Web.Administration.Client.Results;

namespace Aclara.Web.Administration.Client.Events
{
    /// <summary>
    /// Change application pool event arguments.
    /// </summary>
    public class ChangeApplicationPoolEventArgs : EventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IChangeApplicationPoolResult _changeApplicationPoolResult;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Change application pool result.
        /// </summary>
        public IChangeApplicationPoolResult ChangeApplicationPoolResult
        {
            get { return _changeApplicationPoolResult; }
            set { _changeApplicationPoolResult = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ChangeApplicationPoolEventArgs()
        {
            _changeApplicationPoolResult = new ChangeApplicationPoolResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="changePhysicalPathResult"></param>
        public ChangeApplicationPoolEventArgs(IChangeApplicationPoolResult changePhysicalPathResult)
        {
            _changeApplicationPoolResult = changePhysicalPathResult;
        }

        #endregion

    }
}
