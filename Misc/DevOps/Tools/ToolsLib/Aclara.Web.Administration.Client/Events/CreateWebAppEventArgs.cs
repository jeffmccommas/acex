﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Web.Administration.Client.Interfaces;
using Aclara.Web.Administration.Client.Results;

namespace Aclara.Web.Administration.Client.Events
{
    /// <summary>
    /// Create web application event arguments.
    /// </summary>
    public class CreateWebAppEventArgs : EventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private ICreateWebAppResult _createWebAppResult;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Create web application result.
        /// </summary>
        public ICreateWebAppResult CreateWebAppResult
        {
            get { return _createWebAppResult; }
            set { _createWebAppResult = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CreateWebAppEventArgs()
        {
            _createWebAppResult = new CreateWebAppResult();
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="changePhysicalPathResult"></param>
        public CreateWebAppEventArgs(ICreateWebAppResult changePhysicalPathResult)
        {
            _createWebAppResult = changePhysicalPathResult;
        }

        #endregion

    }
}
