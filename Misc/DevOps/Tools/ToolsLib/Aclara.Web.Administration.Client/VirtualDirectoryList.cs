﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.Web.Administration.Client
{

    /// <summary>
    /// Virtual directory list.
    /// </summary>
    public class VirtualDirectoryList : List<VirtualDirectory>
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Properties

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public VirtualDirectoryList()
        {

        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

    }

}
