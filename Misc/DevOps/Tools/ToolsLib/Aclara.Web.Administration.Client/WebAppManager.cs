﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Aclara.Web.Administration.Client.Events;
using Aclara.Web.Administration.Client.Interfaces;
using Aclara.Web.Administration.Client.Results;
using Aclara.Web.Administration.Client.StatusManagement;
using Microsoft.Web.Administration;
using Microsoft.Web.Management;
using Microsoft.Web.Management.Client;

namespace Aclara.Web.Administration.Client
{
    /// <summary>
    /// Web application manager.
    /// </summary>
    public class WebAppManager
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        #endregion

        #region Public Properties

        #endregion

        #region Public Delegates

        public event EventHandler<ChangePhysicalPathEventArgs> PhysicalPathChanged;
        public event EventHandler<CreateWebAppEventArgs> WebAppCreated;
        public event EventHandler<ChangeApplicationPoolEventArgs> WebAppChanged;

        private ServerManager _serverManager = null;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Server manager.
        /// </summary>
        public ServerManager ServerManager
        {
            get { return _serverManager; }
            set { _serverManager = value; }
        }

        #endregion

        #region Default Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public WebAppManager()
        {

        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        public WebAppManager(ServerManager serverManager)
        {
            _serverManager = serverManager;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Determine whether web application exists in IIS.
        /// </summary>
        /// <returns></returns>
        public bool ExistsInIIS(WebApp webApp)
        {
            bool result = false;
            Site site = null;
            Application application = null;

            try
            {
                //Validate input parameter: Web application.
                if (webApp == null)
                {
                    throw new ArgumentException(string.Format("Web application is required."));
                }

                site = this.ServerManager.Sites[webApp.SiteName];
                //Site not found.
                if (site == null)
                {
                    result = false;
                }

                application = null;
                application = site.Applications[webApp.Path];

                //Application not found.
                if (application == null)
                {
                    result = false;
                }
                //Application found.
                else
                {
                    result = true;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve application physical path.
        /// </summary>
        /// <returns></returns>
        public string GetAppPhysicalPath(WebApp webApp)
        {

            const string PhysicalPathNotFound = "Physical path not found.";

            string result = string.Empty;
            Site site = null;
            Application application = null;

            try
            {
                //Validate input parameter: Web application.
                if (webApp == null)
                {
                    throw new ArgumentException(string.Format("Web application is required."));
                }

                site = this.ServerManager.Sites[webApp.SiteName];
                //Site not found.
                if (site == null)
                {
                    result = PhysicalPathNotFound;
                }

                application = null;
                application = site.Applications[webApp.Path];

                //Application not found.
                if (application == null)
                {
                    result = PhysicalPathNotFound;
                }
                //Application found.
                else
                {
                    foreach (Microsoft.Web.Administration.VirtualDirectory virtualDirectory in application.VirtualDirectories)
                    {
                        result += virtualDirectory.PhysicalPath + ";";
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve list of application pool names.
        /// </summary>
        /// <returns></returns>
        public List<string> GetApplicationPoolNames()
        {
            List<string> result = null;
            ApplicationPoolCollection applicationPoolCollection = null;

            try
            {

                result = new List<string>();

                applicationPoolCollection = this.ServerManager.ApplicationPools;
                if (applicationPoolCollection == null)
                {
                    return result;
                }

                foreach (ApplicationPool applicationPool in applicationPoolCollection)
                {
                    result.Add(applicationPool.Name);
                }

                result.Sort();
            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Start site.
        /// </summary>
        /// <returns></returns>
        public void StartSite(string siteName)
        {

            string result = string.Empty;
            Site site = null;

            try
            {

                site = this.ServerManager.Sites[siteName];
                //Site not found.
                if (site == null)
                {
                    return;
                }

                site.Start();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        /// <summary>
        /// Stop site.
        /// </summary>
        /// <returns></returns>
        public void StopSite(string siteName)
        {

            string result = string.Empty;
            Site site = null;

            try
            {

                site = this.ServerManager.Sites[siteName];
                //Site not found.
                if (site == null)
                {
                    return;
                }

                site.Stop();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        /// <summary>
        /// Restart site.
        /// </summary>
        /// <returns></returns>
        public void RestartSite(string siteName)
        {

            string result = string.Empty;
            Site site = null;

            try
            {

                site = this.ServerManager.Sites[siteName];
                //Site not found.
                if (site == null)
                {
                    return;
                }

                site.Stop();
                site.Start();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        /// <summary>
        /// Change physical path.
        /// </summary>
        /// <param name="webApp"></param>
        /// <returns></returns>
        public IChangePhysicalPathResult ChangePhysicalPath(WebApp webApp)
        {

            IChangePhysicalPathResult result = null;
            Site site = null;
            Application application = null;
            Microsoft.Web.Administration.VirtualDirectory iisVirtualDirectory = null;

            try
            {
                //Validate input parameter: Web application.
                if (webApp == null)
                {
                    throw new ArgumentException(string.Format("Web application is required."));
                }

                result = ChangePhysicalPathResultFactory.CreateChangePhysicalPathResult();
                result.WebApp = webApp;

                site = this.ServerManager.Sites[webApp.SiteName];
                if (site == null)
                {
                    throw new ArgumentNullException(string.Format("Site name specified in web application not found. (Site name: {0})",
                                                                  webApp.SiteName));
                }

                application = site.Applications[webApp.Path];
                if (application == null)
                {
                    throw new ArgumentNullException(string.Format("Application path specified in web application not found in site. (Site name: {0}, Application path: {1})",
                                                                  webApp.SiteName, webApp.Path));
                }

                //Iterate through list web app virtual directories.
                foreach (Aclara.Web.Administration.Client.VirtualDirectory virtualDirectory in webApp.VirtualDirectoryList)
                {

                    iisVirtualDirectory = application.VirtualDirectories[virtualDirectory.Path];

                    //Virtual directory not found.
                    if (iisVirtualDirectory == null)
                    {
                        //Change physical path.
                        application.VirtualDirectories.Add(virtualDirectory.Path, virtualDirectory.PhysicalPath);
                    }
                    //Virtual directory found.
                    else
                    {
                        //Create virtual directory.
                        iisVirtualDirectory.PhysicalPath = virtualDirectory.PhysicalPath;
                    }

                    this.ServerManager.CommitChanges();

                }

            }
            catch (Exception ex)
            {
                Status status = null;

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        /// <summary>
        /// Change physical path.
        /// </summary>
        /// <param name="webAppList"></param>
        public void ChangePhysicalPath(WebAppList webAppList)
        {
            IChangePhysicalPathResult changePhysicalPathResult = null;
            ChangePhysicalPathEventArgs changePhysicalPathEventArgs = null;

            try
            {

                foreach (WebApp webApp in webAppList)
                {
                    changePhysicalPathEventArgs = new ChangePhysicalPathEventArgs();

                    changePhysicalPathResult = ChangePhysicalPath(webApp);


                    if (PhysicalPathChanged != null)
                    {
                        PhysicalPathChanged(this, changePhysicalPathEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Change physical path.
        /// </summary>
        /// <remarks>
        /// Overriden to take CancellationToken for use in parallel programming.
        /// </remarks>
        /// <param name="webAppList"></param>
        /// <param name="cancellationToken"></param>
        public void ChangePhysicalPath(WebAppList webAppList,
                                       CancellationToken cancellationToken)
        {
            IChangePhysicalPathResult changePhysicalPathResult = null;
            ChangePhysicalPathEventArgs changePhysicalPathEventArgs = null;

            try
            {

                foreach (WebApp webApp in webAppList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    changePhysicalPathEventArgs = new ChangePhysicalPathEventArgs();
                    changePhysicalPathResult = ChangePhysicalPath(webApp);

                    changePhysicalPathEventArgs.ChangePhysicalPathResult = changePhysicalPathResult;

                    if (PhysicalPathChanged != null)
                    {
                        PhysicalPathChanged(this, changePhysicalPathEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Create web app.
        /// </summary>
        /// <param name="webApp"></param>
        /// <param name="applicationPoolName"></param>
        /// <returns></returns>
        public ICreateWebAppResult CreateWebApp(WebApp webApp,
                                                string applicationPoolName)
        {

            ICreateWebAppResult result = null;
            Site site = null;
            Application application = null;

            try
            {
                //Validate input parameter: Web application.
                if (webApp == null)
                {
                    throw new ArgumentException(string.Format("Web application is required."));
                }

                result = CreateWebAppResultFactory.CreateCreateWebAppResult();
                result.WebApp = webApp;

                site = this.ServerManager.Sites[webApp.SiteName];
                if (site == null)
                {
                    throw new ArgumentNullException(string.Format("Site name specified in web application not found. (Site name: {0})",
                                                                  webApp.SiteName));
                }

                //Iterate through list of virtual directories.
                foreach (Aclara.Web.Administration.Client.VirtualDirectory virtualDirectory in webApp.VirtualDirectoryList)
                {
                    //Create web application.
                    application = site.Applications.Add(webApp.Path, virtualDirectory.PhysicalPath);

                    //Application pool not overridden.
                    if (string.IsNullOrEmpty(applicationPoolName))
                    {
                        //Assign application pool name.
                        if (string.IsNullOrEmpty(webApp.ApplicationPoolName) == false)
                        {
                            application.ApplicationPoolName = webApp.ApplicationPoolName;
                        }
                    }
                    else
                    {
                        //Assign application pool.
                        application.ApplicationPoolName = applicationPoolName;
                    }

                    break;
                }

                this.ServerManager.CommitChanges();

            }
            catch (Exception ex)
            {
                Status status = null;

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        /// <summary>
        /// Create web application.
        /// </summary>
        /// <param name="webAppList"></param>
        /// <param name="applicationPoolName"></param>
        public void CreateWebApp(WebAppList webAppList,
                                 string applicationPoolName)
        {
            ICreateWebAppResult createWebAppResult = null;
            CreateWebAppEventArgs createWebAppEventArgs = null;

            try
            {

                foreach (WebApp webApp in webAppList)
                {
                    createWebAppEventArgs = new CreateWebAppEventArgs();

                    createWebAppResult = CreateWebApp(webApp, applicationPoolName);


                    if (WebAppCreated != null)
                    {
                        WebAppCreated(this, createWebAppEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Create web application.
        /// </summary>
        /// <remarks>
        /// Overriden to take CancellationToken for use in parallel programming.
        /// </remarks>
        /// <param name="webAppList"></param>
        /// <param name="applicationPoolName"></param>
        /// <param name="cancellationToken"></param>
        public void CreateWebApp(WebAppList webAppList,
                                 string applicationPoolName,
                                 CancellationToken cancellationToken)
        {
            ICreateWebAppResult createWebAppResult = null;
            CreateWebAppEventArgs createWebAppEventArgs = null;

            try
            {

                foreach (WebApp webApp in webAppList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    createWebAppEventArgs = new CreateWebAppEventArgs();
                    createWebAppResult = CreateWebApp(webApp, applicationPoolName);

                    createWebAppEventArgs.CreateWebAppResult = createWebAppResult;

                    if (WebAppCreated != null)
                    {
                        WebAppCreated(this, createWebAppEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Change application pool.
        /// </summary>
        /// <param name="webApp"></param>
        /// <returns></returns>
        public IChangeApplicationPoolResult ChangeApplicationPool(WebApp webApp)
        {

            IChangeApplicationPoolResult result = null;
            Site site = null;
            Application application = null;

            try
            {
                //Validate input parameter: Web application.
                if (webApp == null)
                {
                    throw new ArgumentException(string.Format("Web application is required."));
                }

                result = ChangeApplicationPoolResultFactory.CreateChangeApplicationPoolResult();
                result.WebApp = webApp;

                //Retrieve site.
                site = this.ServerManager.Sites[webApp.SiteName];
                if (site == null)
                {
                    throw new ArgumentNullException(string.Format("Site name specified in web application not found. (Site name: {0})",
                                                                  webApp.SiteName));
                }

                //Retrieve application.
                application = site.Applications[webApp.Path];
                if (application == null)
                {
                    throw new ArgumentNullException(string.Format("Path specified in web application not found. (Site name: {0}, Path: {1})",
                                                                  webApp.SiteName, webApp.Path));
                }

                //Change application pool name.
                if (string.IsNullOrEmpty(webApp.ApplicationPoolName) == false)
                {
                    application.ApplicationPoolName = webApp.ApplicationPoolName;
                }

                this.ServerManager.CommitChanges();

            }
            catch (Exception ex)
            {
                Status status = null;

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                result.StatusList.Add(status);
            }

            return result;

        }

        /// <summary>
        /// Change application pool.
        /// </summary>
        /// <param name="webAppList"></param>
        public void ChangeApplicationPool(WebAppList webAppList)
        {
            IChangeApplicationPoolResult changeApplicationPoolResult = null;
            ChangeApplicationPoolEventArgs changeApplicationPoolEventArgs = null;

            try
            {

                foreach (WebApp webApp in webAppList)
                {
                    changeApplicationPoolEventArgs = new ChangeApplicationPoolEventArgs();

                    changeApplicationPoolResult = ChangeApplicationPool(webApp);


                    if (WebAppChanged != null)
                    {
                        WebAppChanged(this, changeApplicationPoolEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Change application pool.
        /// </summary>
        /// <remarks>
        /// Overriden to take CancellationToken for use in parallel programming.
        /// </remarks>
        /// <param name="webAppList"></param>
        /// <param name="cancellationToken"></param>
        public void ChangeApplicationPool(WebAppList webAppList,
                                          CancellationToken cancellationToken)
        {
            IChangeApplicationPoolResult changeApplicationPoolResult = null;
            ChangeApplicationPoolEventArgs changeApplicationPoolEventArgs = null;

            try
            {

                foreach (WebApp webApp in webAppList)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    changeApplicationPoolEventArgs = new ChangeApplicationPoolEventArgs();
                    changeApplicationPoolResult = ChangeApplicationPool(webApp);

                    changeApplicationPoolEventArgs.ChangeApplicationPoolResult = changeApplicationPoolResult;

                    if (WebAppChanged != null)
                    {
                        WebAppChanged(this, changeApplicationPoolEventArgs);
                    }
                }

                return;

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Protected Methods

        #endregion

    }
}
