﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.Web.Administration.Client
{
    /// <summary>
    /// Web application.
    /// </summary>
    public class WebApp
    {

        #region Private Constants
        
        #endregion

        #region Private Data Members

        private string _siteName = string.Empty;
        private Guid _appID;
        private string _alias = string.Empty;
        private string _applicationPoolName = string.Empty;
        private string _path = string.Empty;
        private VirtualDirectoryList _virtualDirectoryList;

        #endregion
        
        #region Public Properties

        /// <summary>
        /// Property: Application identifier.
        /// </summary>
        public Guid AppID
        {
            get { return _appID; }
            set { _appID = value; }
        }

        /// <summary>
        /// Property: Alias.
        /// </summary>
        public string Alias
        {
            get { return _alias; }
            set { _alias = value; }
        }

        /// <summary>
        /// Property: Application pool name.
        /// </summary>
        public string ApplicationPoolName
        {
            get { return _applicationPoolName; }
            set { _applicationPoolName = value; }
        }

        /// <summary>
        /// Property: Site name.
        /// </summary>
        public string SiteName
        {
            get { return _siteName; }
            set { _siteName = value; }
        }

        /// <summary>
        /// Property: Path.
        /// </summary>
        public string Path
        {
            get { return _path; }
            set { _path = value; }
        }

        /// <summary>
        /// Property: Virtual directory list.
        /// </summary>
        public VirtualDirectoryList VirtualDirectoryList
        {
            get { return _virtualDirectoryList; }
            set { _virtualDirectoryList = value; }
        }


        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public WebApp()
        {

        }

        #endregion

        #region Protected Constructors

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

    }
}
