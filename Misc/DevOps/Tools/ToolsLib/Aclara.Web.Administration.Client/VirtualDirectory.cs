﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.Web.Administration.Client
{

    /// <summary>
    /// Virtual directory.
    /// </summary>
    public class VirtualDirectory
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private string _path;
        private string _physicalPath;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Path.
        /// </summary>
        public string Path
        {
            get { return _path; }
            set { _path = value; }
        }

        /// <summary>
        /// Property: Physical path.
        /// </summary>
        public string PhysicalPath
        {
            get { return _physicalPath; }
            set { _physicalPath = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public VirtualDirectory()
        {

        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

    }
}
