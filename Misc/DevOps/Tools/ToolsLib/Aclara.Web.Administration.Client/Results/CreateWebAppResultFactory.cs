﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Web.Administration.Client.Interfaces;
using Aclara.Web.Administration.Client.StatusManagement;

namespace Aclara.Web.Administration.Client.Results
{

    /// <summary>
    /// Create web application result factory.
    /// </summary>
    static public class CreateWebAppResultFactory
    {

        static public ICreateWebAppResult CreateCreateWebAppResult()
        {

            ICreateWebAppResult result = null;
            CreateWebAppResult createWebAppResult = null;

            try
            {

                createWebAppResult = new CreateWebAppResult();

                result = createWebAppResult;

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }
    }
}
