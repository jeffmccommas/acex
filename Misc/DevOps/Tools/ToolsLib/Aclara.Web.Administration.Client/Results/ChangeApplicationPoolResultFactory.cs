﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Web.Administration.Client.Interfaces;
using Aclara.Web.Administration.Client.StatusManagement;

namespace Aclara.Web.Administration.Client.Results
{

    /// <summary>
    /// Change application pool result factory.
    /// </summary>
    static public class ChangeApplicationPoolResultFactory
    {

        static public IChangeApplicationPoolResult CreateChangeApplicationPoolResult()
        {

            IChangeApplicationPoolResult result = null;
            ChangeApplicationPoolResult changeApplicationPoolResult = null;

            try
            {

                changeApplicationPoolResult = new ChangeApplicationPoolResult();

                result = changeApplicationPoolResult;

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }
    }
}
