﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Web.Administration.Client.StatusManagement;
using Aclara.Web.Administration.Client.Interfaces;

namespace Aclara.Web.Administration.Client.Results
{
    /// <summary>
    /// Create web application result.
    /// </summary>
    public class CreateWebAppResult : ICreateWebAppResult
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private WebApp _webApp;
        private StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Web application.
        /// </summary>
        public WebApp WebApp
        {
            get { return _webApp; }
            set { _webApp = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CreateWebAppResult()
        {
            _statusList = new StatusList();
        }

        #endregion
    }
}
