﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Web.Administration.Client.Interfaces;
using Aclara.Web.Administration.Client.StatusManagement;

namespace Aclara.Web.Administration.Client.Results
{
    
    /// <summary>
    /// Change physical path result factory.
    /// </summary>
    static public class ChangePhysicalPathResultFactory
    {

        static public IChangePhysicalPathResult CreateChangePhysicalPathResult()
        {

            IChangePhysicalPathResult result = null;
            ChangePhysicalPathResult changePhysicalPathResult = null;

            try
            {

                changePhysicalPathResult = new ChangePhysicalPathResult();

                result = changePhysicalPathResult;

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }
    }
}
