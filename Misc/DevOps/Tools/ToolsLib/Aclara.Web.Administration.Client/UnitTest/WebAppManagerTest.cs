﻿using Aclara.Web.Administration.Client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Aclara.Web.Administration.Client.Interfaces;
using System.Collections.Generic;

namespace UnitTest
{
    
    
    /// <summary>
    ///This is a test class for WebAppManagerTest and is intended
    ///to contain all WebAppManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class WebAppManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ChangePhysicalPath
        ///</summary>
        [TestMethod()]
        public void ChangePhysicalPathTest()
        {
            WebAppManager target = null;
            WebAppManagerFactory webAppManagerFactory = null;
            WebApp webApp = null;
            Guid appID;
            IChangePhysicalPathResult expected = null;
            IChangePhysicalPathResult actual = null;
            VirtualDirectory virtualDirectory = null;
            VirtualDirectoryList virtualDirectoryList = null;

            webAppManagerFactory = new WebAppManagerFactory();

            target = webAppManagerFactory.CreateWebAppManager();

            webApp = new WebApp();
            appID = Guid.NewGuid();

            virtualDirectoryList = new VirtualDirectoryList();
            virtualDirectory = new VirtualDirectory();

            virtualDirectory.Path = "/";
            virtualDirectory.PhysicalPath = @"C:\acltfs\EnergyPrism\12.06\AppLib\TestBillData\Website\Nexus.TestBillData.Website";
            virtualDirectoryList.Add(virtualDirectory);

            webApp.SiteName = "Default Web Site";
            webApp.AppID = appID;
            webApp.Path = "/Nexus.TestBillData.Website";
            webApp.VirtualDirectoryList = virtualDirectoryList;
            
            actual = target.ChangePhysicalPath(webApp);
        }

        /// <summary>
        ///A test for ExistsInIIS
        ///</summary>
        [TestMethod()]
        public void ExistsInIISTest()
        {
            WebAppManager target = null;
            WebAppManagerFactory webAppManagerFactory = null;
            WebApp webApp = null;
            Guid appID;
            VirtualDirectory virtualDirectory = null;
            VirtualDirectoryList virtualDirectoryList = null;
            bool actual = false;

            webAppManagerFactory = new WebAppManagerFactory();

            target = webAppManagerFactory.CreateWebAppManager();

            webApp = new WebApp();
            appID = Guid.NewGuid();

            virtualDirectoryList = new VirtualDirectoryList();
            virtualDirectory = new VirtualDirectory();

            virtualDirectory.Path = "/";
            virtualDirectory.PhysicalPath = @"C:\acltfs\EnergyPrism\12.06\AppLib\TestBillData\Website\Nexus.TestBillData.Website";
            virtualDirectoryList.Add(virtualDirectory);

            webApp.SiteName = "Default Web Site";
            webApp.AppID = appID;
            webApp.Path = "/Nexus.TestBillData.Website";
            webApp.VirtualDirectoryList = virtualDirectoryList;

            actual = target.ExistsInIIS(webApp);

            Assert.IsTrue(actual, string.Format("Expected web application to exist in IIS."));

        
        }

        /// <summary>
        ///A test for CreateWebApp
        ///</summary>
        [TestMethod()]
        public void CreateWebAppTest()
        {
            WebAppManager target = null;
            WebAppManagerFactory webAppManagerFactory = null;
            WebApp webApp = null;
            Guid appID;
            ICreateWebAppResult expected = null;
            ICreateWebAppResult actual = null;
            VirtualDirectory virtualDirectory = null;
            VirtualDirectoryList virtualDirectoryList = null;
            string applicationPoolName = string.Empty;

            webAppManagerFactory = new WebAppManagerFactory();

            target = webAppManagerFactory.CreateWebAppManager();

            webApp = new WebApp();
            appID = Guid.NewGuid();

            virtualDirectoryList = new VirtualDirectoryList();
            virtualDirectory = new VirtualDirectory();

            virtualDirectory.Path = "/UnitTestWebApp";
            virtualDirectory.PhysicalPath = @"C:\acltfs\EnergyPrism\12.06\AppLib\UnitTestWebApp\Website\UnitTestWebApp";
            virtualDirectoryList.Add(virtualDirectory);

            webApp.SiteName = "Default Web Site";
            webApp.AppID = appID;
            webApp.ApplicationPoolName = "ASP.NET v4.0";
            webApp.Alias = "UnitTestWebApp";
            webApp.Path = "/";
            webApp.VirtualDirectoryList = virtualDirectoryList;

            actual = target.CreateWebApp(webApp, applicationPoolName);
        }

        /// <summary>
        ///A test for GetApplicationPoolNames
        ///</summary>
        [TestMethod()]
        public void GetApplicationPoolNamesTest()
        {
            WebAppManager target = null;
            WebAppManagerFactory webAppManagerFactory = null;
            List<string> actual = null;

            webAppManagerFactory = new WebAppManagerFactory();

            target = webAppManagerFactory.CreateWebAppManager();

            actual = target.GetApplicationPoolNames();

            Assert.IsNotNull(actual, string.Format("Expected non-null application pool names."));
            Assert.IsTrue(actual.Count > 0, string.Format("Expected application pool names count greater than zero."));
        }

        /// <summary>
        ///A test for ChangeApplicationPool
        ///</summary>
        [TestMethod()]
        public void ChangeApplicationPoolTest()
        {
            WebAppManager target = null;
            WebAppManagerFactory webAppManagerFactory = null;
            WebApp webApp = null;
            Guid appID;
            ICreateWebAppResult createWebAppResult = null;
            IChangeApplicationPoolResult expected = null;
            IChangeApplicationPoolResult actual = null;
            VirtualDirectory virtualDirectory = null;
            VirtualDirectoryList virtualDirectoryList = null;
            string applicationPoolName = string.Empty;

            webAppManagerFactory = new WebAppManagerFactory();

            target = webAppManagerFactory.CreateWebAppManager();

            webApp = new WebApp();
            appID = Guid.NewGuid();

            virtualDirectoryList = new VirtualDirectoryList();
            virtualDirectory = new VirtualDirectory();

            virtualDirectory.Path = "/UnitTestWebApp";
            virtualDirectory.PhysicalPath = @"C:\acltfs\EnergyPrism\12.06\AppLib\UnitTestWebApp\Website\UnitTestWebApp";
            virtualDirectoryList.Add(virtualDirectory);

            webApp.SiteName = "Default Web Site";
            webApp.AppID = appID;
            webApp.ApplicationPoolName = "ASP.NET v4.0";
            webApp.Alias = "UnitTestWebApp";
            webApp.Path = "/UnitTestWebApp";
            webApp.VirtualDirectoryList = virtualDirectoryList;

            //Create web app.
            createWebAppResult = target.CreateWebApp(webApp, applicationPoolName);

            //Change application pool.
            webApp.ApplicationPoolName = "Classic ASP.NET v4.0";
            actual = target.ChangeApplicationPool(webApp);
        }
    }
}
