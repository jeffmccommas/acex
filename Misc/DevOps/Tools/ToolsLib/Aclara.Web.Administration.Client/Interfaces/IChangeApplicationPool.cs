﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Web.Administration.Client.StatusManagement;

namespace Aclara.Web.Administration.Client.Interfaces
{
    public interface IChangeApplicationPoolResult
    {
        WebApp WebApp { get; set; }
        StatusList StatusList { get; }
    }
}
