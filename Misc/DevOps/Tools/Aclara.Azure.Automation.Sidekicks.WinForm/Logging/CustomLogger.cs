﻿using NLog;
using NLog.Fluent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Logging
{
    public class CustomLogger
    {

        #region Private Constants

        public const string LogPropertyName_SidekickName = "SidekickName";
        public const string LogPropertyName_SidekickDescription = "SidekickDescription";

        #endregion

        #region Private Data Members

        private Logger _logger;
        private string _sidekickName;
        private string _sidekickDescription;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public Logger Logger
        {
            get { return _logger; }
            set { _logger = value; }
        }

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get { return _sidekickName; }
            set { _sidekickName = value; }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get { return _sidekickDescription; }
            set { _sidekickDescription = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="logger"></param>
        public CustomLogger(Logger logger, string sidekickName, string sidekickDescription)
        {
            _logger = logger;
            _sidekickName = sidekickName;
            _sidekickDescription = sidekickDescription;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <remarks>
        /// Not recommended.
        /// </remarks>
        /// <param name="logger"></param>
        private CustomLogger()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Log entry - Level: Debug.
        /// </summary>
        /// <param name="message"></param>
        public void Debug(string message)
        {
            _logger.Debug()
                .Message(message)
                .Property(LogPropertyName_SidekickName, SidekickName)
                .Property(LogPropertyName_SidekickDescription, SidekickDescription)
                .Write();
        }

        /// <summary>
        /// Log entry - Level: Info.
        /// </summary>
        /// <param name="message"></param>
        public void Info(string message)
        {
            _logger.Info()
                .Message(message)
                .Property(LogPropertyName_SidekickName, SidekickName)
                .Property(LogPropertyName_SidekickDescription, SidekickDescription)
                .Write();
        }

        /// <summary>
        /// Log entry - Level: Warning.
        /// </summary>
        /// <param name="message"></param>
        public void Warn(string message)
        {
            _logger.Warn()
                .Message(message)
                .Property(LogPropertyName_SidekickName, SidekickName)
                .Property(LogPropertyName_SidekickDescription, SidekickDescription)
                .Write();
        }

        /// <summary>
        /// Log entry - Level: Warning.
        /// </summary>
        /// <param name="message"></param>
        public void Warn(Exception exception, string message)
        {
            _logger.Warn()
                .Exception(exception)
                .Message(message)
                .Property(LogPropertyName_SidekickName, SidekickName)
                .Property(LogPropertyName_SidekickDescription, SidekickDescription)
                .Write();
        }

        /// <summary>
        /// Log entry - Level: Error.
        /// </summary>
        /// <param name="message"></param>
        public void Error(string message)
        {
            _logger.Error()
                .Message(message)
                .Property(LogPropertyName_SidekickName, SidekickName)
                .Property(LogPropertyName_SidekickDescription, SidekickDescription)
                .Write();
        }

        /// <summary>
        /// Log entry - Level: Error.
        /// </summary>
        /// <param name="message"></param>
        public void Error(Exception exception, string message)
        {
            _logger.Error()
                .Exception(exception)
                .Message(message)
                .Property(LogPropertyName_SidekickName, SidekickName)
                .Property(LogPropertyName_SidekickDescription, SidekickDescription)
                .Write();
        }

        /// <summary>
        /// Log entry - Level: Fatal.
        /// </summary>
        /// <param name="message"></param>
        public void Fatal(string message)
        {
            _logger.Fatal()
                .Message(message)
                .Property(LogPropertyName_SidekickName, SidekickName)
                .Property(LogPropertyName_SidekickDescription, SidekickDescription)
                .Write();
        }

        #endregion
    }
}
