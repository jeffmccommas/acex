﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.TemplateSidekick.AAATemplateAAA
{
    public static class AAATemplateAAAManagerFactory
    {
        #region Public Methods
        /// <summary>
        /// Create AAATemplateAAA manager.
        /// </summary>
        /// <returns></returns>
        static public AAATemplateAAAManager CreateAAATemplateAAAManager()
        {
            AAATemplateAAAManager result = null;
            AAATemplateAAAManager aaaTemplateAAAManager = null;

            try
            {
                aaaTemplateAAAManager = new AAATemplateAAAManager();

                result = aaaTemplateAAAManager;
                return result;
            }
            catch (Exception)
            {
                throw;
            }

            #endregion
        }
    }
}
