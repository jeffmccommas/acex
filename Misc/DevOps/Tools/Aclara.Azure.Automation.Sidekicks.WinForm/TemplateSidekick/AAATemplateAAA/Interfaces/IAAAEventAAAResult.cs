﻿using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.TemplateSidekick.AAATemplateAAA.Interfaces
{
    public interface IAAAEventAAAResult
    {
        StatusList StatusList { get; }
    }
}
