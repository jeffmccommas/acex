﻿using Aclara.Azure.Automation.Sidekicks.WinForm.TemplateSidekick.AAATemplateAAA.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aclara.Tools.Common.StatusManagement;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.TemplateSidekick.AAATemplateAAA.Results
{
    public class AAAEventAAAResult : IAAAEventAAAResult
    {
        #region Private Constants
        #endregion

        #region Private Data  Members

        StatusList _statusList;

        #endregion

        #region Public Properties

        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        #endregion

        #region Public Constructors 
         
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AAAEventAAAResult()
        {
            _statusList = new StatusList();
        }

        #endregion
    }
}
