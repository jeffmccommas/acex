﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.TemplateSidekick.Views
{
    public interface IAAATemplateAAAView
    {
        string SidekickName { get; }

        string SidekickDescription { get; }

        string BackgroundTaskStatus { get; set; }

        string ApplyButtonText { get; set; }

        void ApplyButtonEnable(bool enable);

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);
    }
}
