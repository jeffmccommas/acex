﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.TemplateSidekick.Views
{
    public class AAATemplateAAAFormFactory
    {
        /// <summary>
        /// Create form.
        /// </summary>
        /// <returns></returns>
        public static AAATemplateAAAForm CreateForm(SidekickConfiguration sidekickConfiguration)
        {

            AAATemplateAAAForm result = null;
            AAATemplateAAAPresenter aaaTemplateAAAPresenter = null;

            try
            {
                result = new AAATemplateAAAForm();
                aaaTemplateAAAPresenter = new AAATemplateAAAPresenter(sidekickConfiguration, result);
                result.AAATemplateAAAPresenter = aaaTemplateAAAPresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
