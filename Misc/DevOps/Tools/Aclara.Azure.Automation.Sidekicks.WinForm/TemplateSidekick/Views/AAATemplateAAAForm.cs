﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.TemplateSidekick.Views
{
    public partial class AAATemplateAAAForm : Form, ISidekickView, IAAATemplateAAAView, ISidekickInfo
    {

        #region Private Constants

        private const string SidekickInfo_SidekickName = "AAATemplateAAA";
        private const string SidekickInfo_SidekickDescription = "AAA Template AAA";

        #endregion

        #region Private Data Members

        private AAATemplateAAAPresenter _aaaTemplateAAAPresenter;

        #endregion


        #region Public Properties

        /// <summary>
        /// Property: AAATemplateAAA presenter.
        /// </summary>
        public AAATemplateAAAPresenter AAATemplateAAAPresenter
        {
            get { return _aaaTemplateAAAPresenter; }
            set { _aaaTemplateAAAPresenter = value; }
        }

        /// <summary>
        /// Property:  Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get { return this.ApplyButton.Text; }
            set { this.ApplyButton.Text = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                if (this.ApplyButton.Text == "Apply")
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickInfo_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickInfo_SidekickDescription;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <param></param>
        public AAATemplateAAAForm()
        {
            InitializeComponent();

            this.InitializeControls();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Sidekick was activated.
        /// </summary>
        public void SidekickActivated()
        {
            try
            {
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }
        /// <summary>
        /// Apply button enable. 
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            int operationCount = 0;
            int timePerOperation = 0;

            try
            {

                operationCount = (int)this.OperationCountNumericUpDown.Value;
                timePerOperation = (int)this.TimePerOperationNumericUpDown.Value;

                this.AAATemplateAAAPresenter.AAATemplateAAA_RenameThisMethod(operationCount, timePerOperation);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

    }
}
