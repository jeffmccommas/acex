﻿namespace Aclara.Azure.Automation.Sidekicks.WinForm.TemplateSidekick.Views
{
    partial class AAATemplateAAAForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.GuidanceGroupBox = new System.Windows.Forms.GroupBox();
            this.GuidanceLabel = new System.Windows.Forms.Label();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.TimePerOperationNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.TimePerOperationLabel = new System.Windows.Forms.Label();
            this.OperationCountLabel = new System.Windows.Forms.Label();
            this.OperationCountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.GuidanceGroupBox.SuspendLayout();
            this.PropertiesPanel.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimePerOperationNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OperationCountNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(0, 1);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(719, 17);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "AAATemplateAAA";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GuidanceGroupBox
            // 
            this.GuidanceGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GuidanceGroupBox.Controls.Add(this.GuidanceLabel);
            this.GuidanceGroupBox.Location = new System.Drawing.Point(6, 28);
            this.GuidanceGroupBox.Name = "GuidanceGroupBox";
            this.GuidanceGroupBox.Size = new System.Drawing.Size(702, 68);
            this.GuidanceGroupBox.TabIndex = 0;
            this.GuidanceGroupBox.TabStop = false;
            this.GuidanceGroupBox.Text = "Guidance";
            // 
            // GuidanceLabel
            // 
            this.GuidanceLabel.AutoSize = true;
            this.GuidanceLabel.Location = new System.Drawing.Point(6, 19);
            this.GuidanceLabel.Name = "GuidanceLabel";
            this.GuidanceLabel.Size = new System.Drawing.Size(93, 13);
            this.GuidanceLabel.TabIndex = 0;
            this.GuidanceLabel.Text = "AAATemplateAAA";
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PropertiesPanel.Controls.Add(this.OperationCountNumericUpDown);
            this.PropertiesPanel.Controls.Add(this.OperationCountLabel);
            this.PropertiesPanel.Controls.Add(this.TimePerOperationLabel);
            this.PropertiesPanel.Controls.Add(this.TimePerOperationNumericUpDown);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Controls.Add(this.GuidanceGroupBox);
            this.PropertiesPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesPanel.MinimumSize = new System.Drawing.Size(560, 480);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(716, 506);
            this.PropertiesPanel.TabIndex = 3;
            // 
            // ControlPanel
            // 
            this.ControlPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Controls.Add(this.ApplyButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 466);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(716, 40);
            this.ControlPanel.TabIndex = 5;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(635, 11);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 1;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(716, 22);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // TimePerOperationNumericUpDown
            // 
            this.TimePerOperationNumericUpDown.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.TimePerOperationNumericUpDown.Location = new System.Drawing.Point(172, 128);
            this.TimePerOperationNumericUpDown.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.TimePerOperationNumericUpDown.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.TimePerOperationNumericUpDown.Name = "TimePerOperationNumericUpDown";
            this.TimePerOperationNumericUpDown.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TimePerOperationNumericUpDown.Size = new System.Drawing.Size(52, 20);
            this.TimePerOperationNumericUpDown.TabIndex = 4;
            this.TimePerOperationNumericUpDown.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // TimePerOperationLabel
            // 
            this.TimePerOperationLabel.AutoSize = true;
            this.TimePerOperationLabel.Location = new System.Drawing.Point(3, 130);
            this.TimePerOperationLabel.Name = "TimePerOperationLabel";
            this.TimePerOperationLabel.Size = new System.Drawing.Size(163, 13);
            this.TimePerOperationLabel.TabIndex = 3;
            this.TimePerOperationLabel.Text = "Time per operation (milliseconds):";
            // 
            // OperationCountLabel
            // 
            this.OperationCountLabel.AutoSize = true;
            this.OperationCountLabel.Location = new System.Drawing.Point(6, 104);
            this.OperationCountLabel.Name = "OperationCountLabel";
            this.OperationCountLabel.Size = new System.Drawing.Size(86, 13);
            this.OperationCountLabel.TabIndex = 3;
            this.OperationCountLabel.Text = "Operation count:";
            // 
            // OperationCountNumericUpDown
            // 
            this.OperationCountNumericUpDown.Location = new System.Drawing.Point(172, 102);
            this.OperationCountNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.OperationCountNumericUpDown.Name = "OperationCountNumericUpDown";
            this.OperationCountNumericUpDown.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.OperationCountNumericUpDown.Size = new System.Drawing.Size(52, 20);
            this.OperationCountNumericUpDown.TabIndex = 2;
            this.OperationCountNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // AAATemplateAAAForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 506);
            this.Controls.Add(this.PropertiesPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "AAATemplateAAAForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "AAATemplateAAAForm";
            this.GuidanceGroupBox.ResumeLayout(false);
            this.GuidanceGroupBox.PerformLayout();
            this.PropertiesPanel.ResumeLayout(false);
            this.PropertiesPanel.PerformLayout();
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TimePerOperationNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OperationCountNumericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.GroupBox GuidanceGroupBox;
        private System.Windows.Forms.Label GuidanceLabel;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Label TimePerOperationLabel;
        private System.Windows.Forms.NumericUpDown TimePerOperationNumericUpDown;
        private System.Windows.Forms.NumericUpDown OperationCountNumericUpDown;
        private System.Windows.Forms.Label OperationCountLabel;
    }
}