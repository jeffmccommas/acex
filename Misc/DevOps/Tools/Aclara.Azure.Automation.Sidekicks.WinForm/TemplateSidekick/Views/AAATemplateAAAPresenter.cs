﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Tools.Common.StatusManagement;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Aclara.Azure.Automation.Sidekicks.WinForm.TemplateSidekick.AAATemplateAAA;
using Aclara.Azure.Automation.Sidekicks.WinForm.TemplateSidekick.AAATemplateAAA.Results;
using Aclara.Azure.Automation.Sidekicks.WinForm.TemplateSidekick.AAATemplateAAA.Events;
using Aclara.Azure.Automation.Sidekicks.WinForm.Logging;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.TemplateSidekick.Views
{
    public class AAATemplateAAAPresenter
    {

        #region Private Constants

        private const string BackgroundTaskStatus_CancellingRequest = "Cancelling AAATemplateAAA request...";
        private const string BackgroundTaskStatus_Requested = "AAATemplateAAA requested...";
        private const string BackgroundTaskStatus_Completed = "AAATemplateAAA request canceled.";
        private const string BackgroundTaskStatus_Canceled = "AAATemplateAAA request canceled.";

        private const string BackgroundTaskTotalDuration = "[*] AAATemplateAAA request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})";

        private const string ApplyButtonText_Cancel = "Cancel";

        private const string Event_AAAEventAAA_CompletedWithStatus = "AAAEventAAA completed. (Status --> {1})";
        private const string Event_AAAEventAAA_Completed = "AAAEventAAA completed.";

        private const string BackgroundTaskException = "Operation cancelled.";
        private const string BackgroundTaskExceptionUnobserved = "Operation cancelled. (Exception unobserved)";

        private const string RequestFailed = "Request failed.";

        #endregion

        #region Private Data Members

        private CustomLogger _logger;
        private SidekickConfiguration _sidekickConfiguration = null;
        private IAAATemplateAAAView _aaaTemplateAAAView = null;
        private AAATemplateAAAManager _aaaTemplateAAAManager = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.AAATemplateAAAView.SidekickName,
                                               this.AAATemplateAAAView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: AAATemplateAAA view.
        /// </summary>
        public IAAATemplateAAAView AAATemplateAAAView
        {
            get { return _aaaTemplateAAAView; }
            set { _aaaTemplateAAAView = value; }
        }

        /// <summary>
        /// Property: AAATemplateAAA manager.
        /// </summary>
        public AAATemplateAAAManager AAATemplateAAAManager
        {
            get { return _aaaTemplateAAAManager; }
            set { _aaaTemplateAAAManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="aaaTemplateAAAView"></param>
        public AAATemplateAAAPresenter(SidekickConfiguration sidekickConfiguration,
                                       IAAATemplateAAAView aaaTemplateAAAView)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.AAATemplateAAAView = aaaTemplateAAAView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private AAATemplateAAAPresenter()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// AAATemplateAAA.
        /// </summary>
        /// <param name="operationCount"></param>
        /// <param name="timePerOperation"></param>
        public void AAATemplateAAA_RenameThisMethod(int operationCount, int timePerOperation)
        {

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.AAATemplateAAAView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.AAATemplateAAAView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.AAATemplateAAAView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;
                    this.AAATemplateAAAView.ApplyButtonText = ApplyButtonText_Cancel;

                    this.AAATemplateAAAManager = this.CreateAAATemplateAAAManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.AAATemplateAAAManager.AAAEventAAACompleted += OnAAAEventAAACompleted;

                    this.BackgroundTask = new Task(() => this.AAATemplateAAAManager.ExampleRenameThisMethod(operationCount, timePerOperation, cancellationToken),
                                                                                                            cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalDuration;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.AAATemplateAAAView.BackgroundTaskCompleted(BackgroundTaskStatus_Completed);
                        this.AAATemplateAAAView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format(BackgroundTaskStatus_Canceled));

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.AAATemplateAAAView.BackgroundTaskCompleted(string.Empty);
                        this.AAATemplateAAAView.ApplyButtonEnable(true);

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(BackgroundTaskException));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(BackgroundTaskException));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(RequestFailed));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(RequestFailed));
                                    }

                                }
                            }
                        }

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.AAATemplateAAAView.BackgroundTaskCompleted(Event_AAAEventAAA_Completed);
                        this.AAATemplateAAAView.ApplyButtonEnable(true);

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create AAATemplateAAA manager.
        /// </summary>
        /// <returns></returns>
        protected AAATemplateAAAManager CreateAAATemplateAAAManager()
        {
            AAATemplateAAAManager result = null;

            try
            {
                result = AAATemplateAAAManagerFactory.CreateAAATemplateAAAManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: AAAEventAAA completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="aaaEventAAAEventArgs"></param>
        protected void OnAAAEventAAACompleted(Object sender,
                                              AAAEventAAAEventArgs aaaEventAAAEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Log event completed with status.
                if (aaaEventAAAEventArgs.AAAEventAAAResult.StatusList != null &&
                    aaaEventAAAEventArgs.AAAEventAAAResult.StatusList.Count > 0)
                {
                    message = string.Format(Event_AAAEventAAA_CompletedWithStatus,
                                            aaaEventAAAEventArgs.AAAEventAAAResult.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (aaaEventAAAEventArgs.AAAEventAAAResult.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                }
                //Log event completed without status.
                else
                {
                    message = string.Format(Event_AAAEventAAA_Completed);
                    this.Logger.Info(message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                        }
                        else
                        {
                            Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                }
                else
                {
                    Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
