﻿using System;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Events
{
    public class SidekickConponentUserActionCompletedEventArgs : EventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members


        #endregion

        #region Public Properties

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekickConponentUserActionCompletedEventArgs()
        {
        }

        #endregion

    }
}
