﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Events
{
    public class SidekickComponentValidationCompletedEventArgs
    {
        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion
        
        #region Public Properties
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekickComponentValidationCompletedEventArgs()
        {
        }

        #endregion
    }
}
