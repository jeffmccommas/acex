﻿using System.ComponentModel;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Types
{

    /// <summary>
    /// Information
    /// </summary>
    public enum Information
    {
        Unspecified = 0,
        AutoUpdater = 1
    }

    public enum SidekickComponentVersionStatus
    {
        NotFound = 1,
        GreaterThanRequiredVersion = 2,
        EqualToRequiredVersion = 3,
        LessThanRequiredVersion = 4

    }

    public enum KillSwitchRunStatus
    {
        [Description("Do not run")]
        DoNotRun = 1,
        [Description("Run with warning")]
        RunWithWarning = 2
    }

}
