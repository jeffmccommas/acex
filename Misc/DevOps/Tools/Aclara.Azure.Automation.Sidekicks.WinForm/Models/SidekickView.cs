﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Models
{
    public class SidekickView : IComparable<SidekickView>
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private string _name;
        private string _description;
        private System.Windows.Forms.Form _form;
        private bool _defaultView;
        private SidekickCategory _sidekickCategory;

        #endregion

        #region Public Types

        #endregion

        #region Public Propertiers

        /// <summary>
        /// Property: Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Property: Description.
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// Property: Form.
        /// </summary>
        public System.Windows.Forms.Form Form
        {
            get { return _form; }
            set { _form = value; }
        }

        /// <summary>
        /// Property: Default view.
        /// </summary>
        public bool DefaultView
        {
            get { return _defaultView; }
            set { _defaultView = value; }
        }

        /// <summary>
        /// Property: Sidekick category.
        /// </summary>
        public SidekickCategory SidekickCategory
        {
            get { return _sidekickCategory; }
            set { _sidekickCategory = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred overloaded constructor.
        /// </summary>
        public SidekickView(string name, string description, System.Windows.Forms.Form form, bool defaultView, SidekickCategory sidekickCategory)
        {
            _name = name;
            _description = description;
            _form = form;
            _defaultView = defaultView;
            _sidekickCategory = sidekickCategory;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekickView()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Compare two SidekckickView instances.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(SidekickView other)
        {
            int result = this.Description.CompareTo(other.Description);
            return result;
        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion
    }

    /// <summary>
    /// Sidekick category.
    /// </summary>
    public class SidekickCategory : IComparable<SidekickCategory>
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public SidekickCategory(string name, string description)
        {
            this.Name = name;
            this.Description = description;
        }

        /// <summary>
        /// Compare two SidekickCategory instances.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(SidekickCategory other)
        {
            int result = this.Description.CompareTo(other.Description);
            return result;
        }
    }
}
