﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Types;
using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Models
{

    /// <summary>
    /// Sidekick component : Windows PowerShell.
    /// </summary>
    public class SidekickComponentWindowsPowerShell : SidekickComponent
    {

        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors
        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve sidekick component version status.
        /// </summary>
        public override SidekickComponentVersionStatus GetVersionStatus()
        {
            SidekickComponentVersionStatus result = SidekickComponentVersionStatus.NotFound;
            string powerShellScript = string.Empty;
            StatusList statusList = null;
            List<object> dataList = null;
            System.Version windowsPowerShellVersion;

            try
            {

                powerShellScript = @"$PSVersionTable.PSVersion";

                powerShellScript = string.Format(powerShellScript);

                statusList = new StatusList();

                dataList = this.InvokePowerShellScriptSync(powerShellScript, ref statusList);
                foreach (object data in dataList)
                {
                    if (data is System.Version)
                    {
                        windowsPowerShellVersion = (System.Version)data;

                        this.Version = windowsPowerShellVersion;
                        this.VersionAsText = this.Version.ToString();

                        result = SidekickComponent.CompareVersions(this.Version,this.MinimumVersion);
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Install sidekick component.
        /// </summary>
        /// <returns></returns>
        public override bool Install()
        {
            bool result = false;

            try
            {
                string installPathInstaller = System.IO.Path.Combine(this.InstallerPath, this.Installer);
                System.Diagnostics.Process.Start(installPathInstaller);
                result = true;
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        /// <summary>
        /// Update sidekick component.
        /// </summary>
        /// <returns></returns>

        public override bool Update()
        {
            try
            {
                return this.Install();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Protected Methods


        #endregion

        #region Private Methods
        #endregion
    }
}
