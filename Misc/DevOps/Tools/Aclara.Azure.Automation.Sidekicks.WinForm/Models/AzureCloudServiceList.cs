﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aclara.Azure.Automation.Client.Models;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Models
{
    /// <summary>
    /// Azure cloud service.
    /// </summary>
    public class AzureCloudServiceList : List<AzureCloudService>
    {

        #region Private Constants

        private const string DefaultValue_NotAvailable = "N/A";

        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AzureCloudServiceList()
        {

        }

        /// <summary>
        /// Constructor override.
        /// </summary>
        /// <param name="azureCloudServiceList"></param>
        public AzureCloudServiceList(IEnumerable<AzureCloudService> azureCloudServiceList)
        {
            foreach (AzureCloudService azureCloudService in azureCloudServiceList)
            {
                this.Add(azureCloudService);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Convert to Azure cloud service shallow list.
        /// </summary>
        /// <returns></returns>
        public AzureCloudServiceShallowList ConvertToAzureCloudServiceShallowList()
        {
            AzureCloudServiceShallowList result = null;
            AzureCloudServiceShallow azureCloudServiceShallow = null;

            try
            {
                result = new AzureCloudServiceShallowList();

                foreach (AzureCloudService azureCloudService in this)
                {
                    azureCloudServiceShallow = new AzureCloudServiceShallow();
                    azureCloudServiceShallow.ServiceName = azureCloudService.ServiceName;
                    azureCloudServiceShallow.Slot = azureCloudService.Slot;
                    azureCloudServiceShallow.RoleName = azureCloudService.RoleName;
                    azureCloudServiceShallow.RoleInstanceCount = azureCloudService.RoleInstanceCount;
                    azureCloudServiceShallow.RoleInstanceCountScaleDown = azureCloudService.RoleInstanceCountScaleDown;
                    azureCloudServiceShallow.RoleInstanceCountScaleUp = azureCloudService.RoleInstanceCountScaleUp;
                    azureCloudServiceShallow.RoleInstanceCountScaleCustom = azureCloudService.RoleInstanceCountScaleCustom;
                    azureCloudServiceShallow.VMSize = azureCloudService.VMSize;

                    result.Add(azureCloudServiceShallow);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }
    static class IEnumerableAzureCloudServiceListExtensions
    {
        public static AzureCloudServiceList ToAzureCloudServiceList(this IEnumerable<AzureCloudService> azureCloudServiceList)
        {
            return new AzureCloudServiceList(azureCloudServiceList);
        }
    }

}
