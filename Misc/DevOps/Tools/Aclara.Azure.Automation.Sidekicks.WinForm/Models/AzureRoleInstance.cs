﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Models
{
        
    /// <summary>
    /// Azure role instance.
    /// </summary>
    public class AzureRoleInstance
    {
        [JsonProperty("InstanceErrorCode")]
        public object InstanceErrorCode { get; set; }
        [JsonProperty("InstanceFaultDomain")]
        public int InstanceFaultDomain { get; set; }
        [JsonProperty("InstanceName")]
        public string InstanceName { get; set; }
        [JsonProperty("InstanceSize")]
        public string InstanceSize { get; set; }
        [JsonProperty("InstanceStateDetails")]
        public string InstanceStateDetails { get; set; }
        [JsonProperty("InstanceStatus")]
        public string InstanceStatus { get; set; }
        [JsonProperty("InstanceUpgradeDomain")]
        public int InstanceUpgradeDomain { get; set; }
        [JsonProperty("RoleName")]
        public string RoleName { get; set; }
        [JsonProperty("DeploymentID")]
        public string DeploymentID { get; set; }
        [JsonProperty("IPAddress")]
        public string IPAddress { get; set; }
        [JsonProperty("PublicIPAddress")]
        public object PublicIPAddress { get; set; }
        [JsonProperty("PublicIPName")]
        public object PublicIPName { get; set; }
        [JsonProperty("PublicIPIdleTimeoutInMinutes")]
        public object PublicIPIdleTimeoutInMinutes { get; set; }
        [JsonProperty("PublicIPDomainNameLabel")]
        public object PublicIPDomainNameLabel { get; set; }
        [JsonProperty("PublicIPFqdns")]
        public object PublicIPFqdns { get; set; }
        [JsonProperty("ServiceName")]
        public string ServiceName { get; set; }
        [JsonProperty("OperationDescription")]
        public string OperationDescription { get; set; }
        [JsonProperty("OperationId")]
        public string OperationId { get; set; }
        [JsonProperty("OperationStatus")]
        public string OperationStatus { get; set; }
        public static List<AzureRoleInstance> DeserializedJson(string responseJson)
        {
            return JsonConvert.DeserializeObject<List<AzureRoleInstance>>(responseJson);
        }
    }
}
