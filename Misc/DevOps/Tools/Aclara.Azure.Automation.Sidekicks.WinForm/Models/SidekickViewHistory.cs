﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Models
{
    public class SidekickViewHistory
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private int _sequenceNumber;
        private SidekickView _sidekickView;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sequence number.
        /// </summary>
        public int SequenceNumber
        {
            get { return _sequenceNumber; }
            set { _sequenceNumber = value; }
        }

        /// <summary>
        /// Property: Sidekick view.
        /// </summary>
        public SidekickView SidekickView
        {
            get { return _sidekickView; }
            set { _sidekickView = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekickViewHistory()
        {
        }   

        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }
}
