﻿using Aclara.PowerShellHost;
using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using Aclara.Azure.Automation.Sidekicks.WinForm.Types;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Models
{
    public class SidekickComponent
    {
        #region Private Constants

        private const string LoginStatus_Unknown = "N/A";

        private const string SidekickComponentStatus_Available = "Available";
        private const string SidekickComponentStatus_NotFound = "Not found";
        private const string SidekickComponentStatus_MimimumVersionNotFound = "Minimum version not found.";
        private const string SidekickComponentStatus_Unknown = "N/A";

        private const string LoginStatus_LoginSuccessful = "Logged in";
        private const string LoginStatus_Failed = "Login failed or cancelled";

        private const string UserAction_Install = "Install";
        private const string UserAction_Upgrade = "Upgrade";
        private const string UserAction_None = "None";

        #endregion

        #region Private Data Members

        private SidekickComponentId _sidekickComponentId;
        private bool _selected;
        private string _name;
        private string _description;
        private System.Version _minimumVersion;
        private string _minimumVersionAsText;
        private System.Version _version;
        private string _versionAsText;
        private string _helpKeyword;
        private UserAction _userAction;
        private string _moreInfo;
        private string _provider;
        private bool _requiredComponent;
        private DateTime _lastChecked;
        private string _statusSidekickComponent;
        private bool _requiredLogin;
        private string _loginStatus;
        private bool _loginSuccessful;
        private bool _available;
        private string _userActionPrompt;
        private string _installerPath;
        private string _installer;
        private string _extra;
        private PowerShellHostManager _powerShellHostManager = null;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Selected.
        /// </summary>
        [System.ComponentModel.DisplayName("-")]
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        /// <summary>
        /// Property: Sidekick component identifier.
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public SidekickComponentId SidekickComponentId
        {
            get { return _sidekickComponentId; }
            set { _sidekickComponentId = value; }
        }

        /// <summary>
        /// Property: Name.
        /// </summary>
        [System.ComponentModel.DisplayName("Name")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Property: Description.
        /// </summary>
        [System.ComponentModel.DisplayName("Description")]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// Property: Minimum version.
        /// </summary>
        public System.Version MinimumVersion
        {
            get { return _minimumVersion; }
            set { _minimumVersion = value; }
        }

        /// <summary>
        /// Property: Minimum version (text).
        /// </summary>
        [System.ComponentModel.DisplayName("Minimum Version")]
        public string MinimumVersionAsText
        {
            get { return _minimumVersionAsText; }
            set { _minimumVersionAsText = value; }
        }

        /// <summary>
        /// Property: Version.
        /// </summary>
        public System.Version Version
        {
            get { return _version; }
            set { _version = value; }
        }

        /// <summary>
        /// Property: Version (text).
        /// </summary>
        [System.ComponentModel.DisplayName("Version")]
        public string VersionAsText
        {
            get { return _versionAsText; }
            set { _versionAsText = value; }
        }

        /// <summary>
        /// Property: Help keyword.
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public string HelpKeyword
        {
            get { return _helpKeyword; }
            set { _helpKeyword = value; }
        }

        /// <summary>
        /// Property: User action.
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public UserAction UserAction
        {
            get { return _userAction; }
            set { _userAction = value; }
        }

        /// <summary>
        /// Property: User action prompt
        /// </summary>
        [System.ComponentModel.DisplayName("User Action")]
        public string UserActionPrompt
        {
            get { return _userActionPrompt; }
            set { _userActionPrompt = value; }
        }

        /// <summary>
        [System.ComponentModel.DisplayName("Help")]
        /// </summary>
        public string MoreInfo
        {
            get { return _moreInfo; }
            set { _moreInfo = value; }
        }

        /// <summary>
        /// Property: Provider.
        /// </summary>
        [System.ComponentModel.DisplayName("Provider")]
        public string Provider
        {
            get { return _provider; }
            set { _provider = value; }
        }

        /// <summary>
        /// Property: Required.
        /// </summary>
        [System.ComponentModel.DisplayName("Required Component")]
        [System.ComponentModel.ReadOnly(true)]
        public bool Required
        {
            get { return _requiredComponent; }
            set { _requiredComponent = value; }
        }

        /// <summary>
        /// Property: Component status.
        /// </summary>
        [System.ComponentModel.DisplayName("Component Status")]
        public string SidekickComponentStatus
        {
            get { return _statusSidekickComponent; }
            set { _statusSidekickComponent = value; }
        }

        /// <summary>
        /// Property: Last checked.
        /// </summary>
        [System.ComponentModel.DisplayName("Last Checked")]
        public DateTime LastChecked
        {
            get { return _lastChecked; }
            set { _lastChecked = value; }
        }

        /// <summary>
        /// Property: Available.
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public bool Available
        {
            get { return _available; }
            set { _available = value; }
        }

        /// <summary>
        /// Property: Login required.
        /// </summary>
        [System.ComponentModel.DisplayName("Required Login")]
        [System.ComponentModel.ReadOnly(true)]
        public bool LoginRequired
        {
            get { return _requiredLogin; }
            set { _requiredLogin = value; }
        }

        /// <summary>
        /// Property: Login status.
        /// </summary>
        [System.ComponentModel.DisplayName("Login Status")]
        public string LoginStatus
        {
            get { return _loginStatus; }
            set { _loginStatus = value; }
        }

        /// <summary>
        /// Property: Login successful.
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public bool LoginSuccessful
        {
            get { return _loginSuccessful; }
            set { _loginSuccessful = value; }
        }

        /// <summary>
        /// Property: Installer path.
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public string InstallerPath
        {
            get { return _installerPath; }
            set { _installerPath = value; }
        }

        /// <summary>
        /// Property: Installer.
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public string Installer
        {
            get { return _installer; }
            set { _installer = value; }
        }

        /// <summary>
        /// Property: Extra property.
        /// </summary>
        /// <remarks>
        /// This property is used to make the data grid view
        /// look more appealing. The Extra colum shows last in
        /// the data grid view and is sized as "Fill".
        /// </remarks>
        [System.ComponentModel.DisplayName("-")]
        public string Extra
        {
            get { return _extra; }
            set { _extra = value; }
        }

        /// <summary>
        /// Property: PowerShellHost manager.
        /// </summary>
        public PowerShellHostManager PowerShellHostManager
        {
            get
            {
                if (_powerShellHostManager == null)
                {
                    _powerShellHostManager = this.CreatePowerShellHostManager();
                }
                return _powerShellHostManager;
            }
            set { _powerShellHostManager = value; }
        }
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekickComponent()
        {
            _statusSidekickComponent = SidekickComponentStatus_Unknown;
            _userActionPrompt = "N/A";
            _userAction = UserAction.Unspecified;
            _loginStatus = LoginStatus_Unknown;
            _lastChecked = DateTime.MinValue;
            _moreInfo = "More Info";
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Install sidekick component.
        /// </summary>
        /// <returns></returns>
        public virtual bool Install()
        {
            return false;
        }

        /// <summary>
        /// Update  sidekick component.
        /// </summary>
        /// <returns></returns>
        public virtual bool Update()
        {
            return false;
        }

        /// <summary>
        /// Retrieve  sidekick component version status.
        /// </summary>
        /// <returns></returns>
        public virtual Types.SidekickComponentVersionStatus GetVersionStatus()
        {
            return Types.SidekickComponentVersionStatus.NotFound;
        }

        /// <summary>
        /// Check sidekick component status.
        /// </summary>
        public void CheckStatus()
        {
            bool sidekickComponentFound = false;
            bool sidekickComponentMinimumVerionFound = false;
            SidekickComponentVersionStatus sidekickComponentVersionStatus = SidekickComponentVersionStatus.NotFound;

            try
            {
                sidekickComponentVersionStatus = this.GetVersionStatus();

                if (sidekickComponentVersionStatus != Types.SidekickComponentVersionStatus.NotFound)
                {
                    sidekickComponentFound = true;
                }
                else
                {
                    sidekickComponentFound = false;
                }

                if (sidekickComponentVersionStatus == Types.SidekickComponentVersionStatus.EqualToRequiredVersion ||
                    sidekickComponentVersionStatus == Types.SidekickComponentVersionStatus.GreaterThanRequiredVersion)
                {
                    sidekickComponentMinimumVerionFound = true;
                }
                else
                {
                    sidekickComponentMinimumVerionFound = false;
                }

                this.LastChecked = DateTime.Now;

                if (sidekickComponentFound == false)
                {
                    this.Available = false;
                    this.SidekickComponentStatus = SidekickComponentStatus_NotFound;
                    this.UserActionPrompt = UserAction_Install;
                    this.UserAction = UserAction.Install;
                }
                else
                {
                    if (sidekickComponentMinimumVerionFound == false)
                    {
                        this.Available = false;
                        this.SidekickComponentStatus = SidekickComponentStatus_MimimumVersionNotFound;
                        this.UserActionPrompt = UserAction_Upgrade;
                        this.UserAction = UserAction.Upgrade;
                    }
                    else
                    {
                        this.Available = true;
                        this.SidekickComponentStatus = SidekickComponentStatus_Available;
                        this.UserActionPrompt = UserAction_None;
                        this.UserAction = UserAction.None;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Compare two versions.
        /// </summary>
        /// <param name="sourceVersion"></param>
        /// <param name="targetVersion"></param>
        /// <returns>SidekickComponentVersionStatus</returns>
        public static SidekickComponentVersionStatus CompareVersions(System.Version sourceVersion, System.Version targetVersion)
        {
            SidekickComponentVersionStatus result;
            try
            {
                switch (sourceVersion.CompareTo(targetVersion))
                {
                    case 0:
                        result = SidekickComponentVersionStatus.EqualToRequiredVersion;
                        break;
                    case 1:
                        result = SidekickComponentVersionStatus.GreaterThanRequiredVersion;
                        break;
                    case -1:
                        result = SidekickComponentVersionStatus.LessThanRequiredVersion;
                        break;
                    default:
                        result = SidekickComponentVersionStatus.NotFound;
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Convert to system version.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public System.Version ConvertToVersion(Version version)
        {
            System.Version result = null;
            string versionAsText = string.Empty;

            versionAsText = string.Format("{0}.{1}.{2}",
                                          version.Major,
                                          version.Minor,
                                          version.Build);
            result = new System.Version(versionAsText);

            return result;
        }

        /// <summary>
        /// Convert text to version.
        /// </summary>
        /// <param name="versionAsText"></param>
        /// <returns></returns>
        public System.Version ConvertToVersion(string versionAsText)
        {

            System.Version result = null;

            try
            {
                result = System.Version.Parse(versionAsText);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Invoke PowerShell script synchronously.
        /// </summary>
        /// <param name="powerShellScript"></param>
        /// <returns></returns>
        protected List<object> InvokePowerShellScriptSync(string powerShellScript, ref StatusList statusList)
        {

            List<object> result = null;

            try
            {
                result = this.PowerShellHostManager.InvokePowerShellScriptSync(powerShellScript, ref statusList);

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Create PowerShellHost manager.
        /// </summary>
        /// <returns></returns>
        protected PowerShellHostManager CreatePowerShellHostManager()
        {
            PowerShellHostManager result = null;

            try
            {
                result = PowerShellHostManagerFactory.CreatePowerShellHostManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve largest version.
        /// </summary>
        /// <param name="powerShellModuleList"></param>
        /// <returns></returns>
        protected PowerShellModule GetLargestVersion(PowerShellModule[] powerShellModuleList)
        {
            PowerShellModule result = null;
            PowerShellModule powerShellModuleLatest = null;
            System.Version powerShellModuleVersion;
            System.Version powerShellModuleLastestVersion;

            try
            {
                foreach (PowerShellModule powerShellModule in powerShellModuleList)
                {
                    if (powerShellModuleLatest == null)
                    {
                        powerShellModuleLatest = powerShellModule;
                    }
                    powerShellModuleVersion = this.ConvertToVersion(powerShellModule.Version);
                    powerShellModuleLastestVersion = this.ConvertToVersion(powerShellModuleLatest.Version);

                    switch (powerShellModuleVersion.CompareTo(powerShellModuleLastestVersion))
                    {
                        case 1:
                            powerShellModuleLatest = powerShellModule;
                            break;
                        default:
                            break;
                    }
                }

                result = powerShellModuleLatest;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve largest version.
        /// </summary>
        /// <param name="powerShellPackageProviderList"></param>
        /// <returns></returns>
        protected PowerShellPackageProvider GetLargestVersion(PowerShellPackageProvider[] powerShellPackageProviderList)
        {
            PowerShellPackageProvider result = null;
            PowerShellPackageProvider powerShellPackageProviderLatest = null;

            try
            {
                foreach (PowerShellPackageProvider powerShellPackageProvider in powerShellPackageProviderList)
                {
                    if (powerShellPackageProviderLatest == null)
                    {
                        powerShellPackageProviderLatest = powerShellPackageProvider;
                    }

                    switch (powerShellPackageProvider.Version.CompareTo(powerShellPackageProviderLatest.Version))
                    {
                        case 1:
                            powerShellPackageProviderLatest = powerShellPackageProvider;
                            break;
                        default:
                            break;
                    }
                }

                result = powerShellPackageProviderLatest;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region Private Methods
        #endregion
    }
}
