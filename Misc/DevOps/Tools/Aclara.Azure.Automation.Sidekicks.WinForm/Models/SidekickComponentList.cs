﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Models
{
    public class SidekickComponentList : List<SidekickComponent>
    {
        #region Private Constants

        private const string ComponentId_WindowsPowerShell = "WindowsPowerShell";
        private const string ComponentId_AzurePowerShell = "AzurePowerShell";
        private const string ComponentId_AzureRmPowerShell = "AzureRmPowerShell";
        private const string ComponentId_NuGetPackageManager = "NuGetPackageManager";
        private const string ComponentId_PowerShellGallery = "PowerShellGallery";

        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekickComponentList()
        {

        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="sidekickComponentList"></param>
        public SidekickComponentList(IEnumerable<SidekickComponent> sidekickComponentList)
        {
            foreach (SidekickComponent sidekickComponent in sidekickComponentList)
            {
                this.Add(sidekickComponent);
            }
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="componentList"></param>
        public SidekickComponentList(List<ComponentInfo_Component> componentList)
        {
            SidekickComponent sidekickComponent = null;
            SidekickComponentId sidekickComponentId;

            foreach (ComponentInfo_Component component in componentList)
            {
                sidekickComponentId = SidekickComponentList.ConvertToComponentId(component.Name);

                switch (sidekickComponentId)
                {
                    case SidekickComponentId.Unspecified:
                        break;
                    case SidekickComponentId.WindowsPowerShell:
                        sidekickComponent = new SidekickComponentWindowsPowerShell();
                        break;
                    case SidekickComponentId.NuGetPackageManager:
                        sidekickComponent = new SidekickComponentNuGetPackageManager();
                        break;
                    case SidekickComponentId.AzurePowerShell:
                        sidekickComponent = new SidekickComponentAzurePowerShell();
                        break;
                    case SidekickComponentId.AzureRmPowerShell:
                        sidekickComponent = new SidekickComponentAzureRmPowerShell();
                        break;
                    case SidekickComponentId.PowerShellGallery:
                        sidekickComponent = new SidekickComponentPowerShellGallery();
                        break;
                    default:
                        break;
                }

                sidekickComponent.SidekickComponentId = sidekickComponentId;
                sidekickComponent.Name = component.Name;
                sidekickComponent.Description = component.Description;
                sidekickComponent.HelpKeyword = component.HelpKeyword;
                sidekickComponent.MinimumVersionAsText = component.MinimumVersion;
                sidekickComponent.MinimumVersion = sidekickComponent.ConvertToVersion(component.MinimumVersion);
                sidekickComponent.Provider = component.Provider;
                sidekickComponent.Required = component.Required;
                sidekickComponent.LoginRequired = component.LoginRequired;
                sidekickComponent.Installer = component.Installer;
                sidekickComponent.InstallerPath = component.InstallerPath;

                sidekickComponent.LoginSuccessful = false;
                sidekickComponent.LastChecked = DateTime.MinValue;

                this.Add(sidekickComponent);
            }


        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Components available (that are required).
        /// </summary>
        /// <returns></returns>
        public bool AreComponentsAvailable(List<SidekickComponentId> sidekickComponentIdList)
        {
            bool result = false;

            try
            {
                //Zero unavailable sidekick components = Sidekick components available.
                if (GetUnavailableSidekickComponents(sidekickComponentIdList).Any() == false)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
                return result;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Components logged in (that require login).
        /// </summary>
        /// <returns></returns>
        public bool AreComponentsLoggedIn(List<SidekickComponentId> sidekickComponentIdList)
        {
            bool result = false;
            try
            {
                //Zero sidekick components not logged in = Sidekick components logged in.
                if (GetSidekickComponentsNotLoggedIn(sidekickComponentIdList).Any() == false)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve unavailable sidekick components.
        /// </summary>
        /// <param name="SidekickComponentIdList"></param>
        /// <returns></returns>
        public SidekickComponentList GetUnavailableSidekickComponents(List<SidekickComponentId> SidekickComponentIdList)
        {
            SidekickComponentList result = null;

            try
            {
                result = new SidekickComponentList(this.Where(sc => SidekickComponentIdList.Contains(sc.SidekickComponentId) && sc.Available == false));
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve sidekick components not logged in or login attempt unsuccessful.
        /// </summary>
        /// <param name="SidekickComponentIdList"></param>
        /// <returns></returns>
        public SidekickComponentList GetSidekickComponentsNotLoggedIn(List<SidekickComponentId> SidekickComponentIdList)
        {
            SidekickComponentList result = null;

            try
            {
                result = new SidekickComponentList(this.Where(sc => SidekickComponentIdList.Contains(sc.SidekickComponentId) && 
                                                                    (sc.LoginRequired == true && sc.LoginSuccessful == false)));
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Convert sidekick component identifier from text to enumeration.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static SidekickComponentId ConvertToComponentId(string name)
        {
            SidekickComponentId result = SidekickComponentId.Unspecified;

            try
            {
                switch (name)
                {
                    case ComponentId_WindowsPowerShell:
                        result = SidekickComponentId.WindowsPowerShell;
                        break;
                    case ComponentId_AzurePowerShell:
                        result = SidekickComponentId.AzurePowerShell;
                        break;
                    case ComponentId_AzureRmPowerShell:
                        result = SidekickComponentId.AzureRmPowerShell;
                        break;
                    case ComponentId_NuGetPackageManager:
                        result = SidekickComponentId.NuGetPackageManager;
                        break;
                    case ComponentId_PowerShellGallery:
                        result = SidekickComponentId.PowerShellGallery;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected sidekick component identifier."), 
                                                                            name);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }

    public enum SidekickComponentId
    {
        Unspecified = 0,
        WindowsPowerShell,
        NuGetPackageManager,
        AzurePowerShell,
        AzureRmPowerShell,
        PowerShellGallery
    }

    public enum UserAction
    {
        Unspecified = 0,
        Install,
        Upgrade,
        None
    }
}
