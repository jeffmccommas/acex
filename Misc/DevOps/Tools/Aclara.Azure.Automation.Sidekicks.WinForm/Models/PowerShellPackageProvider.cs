﻿using Newtonsoft.Json;
using System;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Models
{

    public class PowerShellPackageProvider
    {

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Version")]
        public System.Version Version { get; set; }

    }
}
