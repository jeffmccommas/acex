﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Models
{
    /// <summary>
    /// Azure cloud service.
    /// </summary>
    public class AzureCloudService
    {

        #region Private Constants

        private const string DefaultValue_ValueNotAvailable = "N/A";

        #endregion

        #region Private Data Members

        private bool _selected;
        private string _environment;
        private string _serviceName;
        private string _slot;
        private int _roleInstanceCountScaleUp;
        private int _roleInstanceCountScaleDown;
        private int _roleInstanceCountScaleCustom;
        private int _roleInstanceCount;
        private string _vmSize;
        private string _roleName;

        private bool _refreshRequired;
        private string _extra;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Selected.
        /// </summary>
        [System.ComponentModel.DisplayName("-")]
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        /// <summary>
        /// Property: Environment.
        /// </summary>
        [System.ComponentModel.DisplayName("Environment")]
        public string Environment
        {
            get { return _environment; }
            set { _environment = value; }
        }

        /// <summary>
        /// Property: Service name.
        /// </summary>
        [System.ComponentModel.DisplayName("Service Name")]
        public string ServiceName
        {
            get { return _serviceName; }
            set { _serviceName = value; }
        }

        /// <summary>
        /// Property: Slot.
        /// </summary>
        [System.ComponentModel.DisplayName("Slot")]
        public string Slot
        {
            get { return _slot; }
            set { _slot = value; }
        }

        /// <summary>
        /// Property: Role instance count - scale up.
        /// </summary>
        [System.ComponentModel.DisplayName("Scale Up")]

        public int RoleInstanceCountScaleUp
        {
            get { return _roleInstanceCountScaleUp; }
            set { _roleInstanceCountScaleUp = value; }
        }

        /// <summary>
        /// Property: Role instance count - scale down.
        /// </summary>
        [System.ComponentModel.DisplayName("Scale Down")]
        public int RoleInstanceCountScaleDown
        {
            get { return _roleInstanceCountScaleDown; }
            set { _roleInstanceCountScaleDown = value; }
        }

        /// <summary>
        /// Property: Scale custom
        /// </summary>
        [System.ComponentModel.DisplayName("Scale Custom")]
        public int RoleInstanceCountScaleCustom
        {
            get { return _roleInstanceCountScaleCustom; }
            set { _roleInstanceCountScaleCustom = value; }
        }

        /// <summary>
        /// Property: Role instance count.
        /// </summary>
        [System.ComponentModel.DisplayName("Role Instance Count")]
        public int RoleInstanceCount
        {
            get { return _roleInstanceCount; }
            set { _roleInstanceCount = value; }
        }

        /// <summary>
        /// Property: Role name.
        /// </summary>
        [System.ComponentModel.DisplayName("Role Name")]
        public string RoleName
        {
            get { return _roleName; }
            set { _roleName = value; }
        }

        /// <summary>
        /// Property: Virtual machine size.
        /// </summary>
        [System.ComponentModel.DisplayName("VM Size")]
        public string VMSize
        {
            get { return _vmSize; }
            set { _vmSize = value; }
        }

        /// <summary>
        /// Property: Extra property.
        /// </summary>
        /// <remarks>
        /// This property is used to make the data grid view
        /// look more appealing. The Extra colum shows last in
        /// the data grid view and is sized as "Fill".
        /// </remarks>
        [System.ComponentModel.DisplayName("-")]
        public string Extra
        {
            get { return _extra; }
            set { _extra = value; }
        }


        /// <summary>
        /// Property: Refresh required.
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public bool RefreshRequired
        {
            get
            {
                _refreshRequired = false;
                if (string.IsNullOrEmpty(this.RoleName) == true ||
                   this.RoleInstanceCount == 0)
                {
                    _refreshRequired = true;
                }
                return _refreshRequired;
            }
            private set { _refreshRequired = value; }
        }

        #region Public Static Properties

        /// <summary>
        /// Property: Value not available.
        /// </summary>
        public static string ValueNotAvailable
        {
            get
            {
                return DefaultValue_ValueNotAvailable;
            }
            
            private set
            {
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AzureCloudService()
        {
            _vmSize = DefaultValue_ValueNotAvailable;
            _roleInstanceCount = 0;
            _roleName = DefaultValue_ValueNotAvailable;
        }

        #endregion

        #endregion

        #region Public Methods

        /// <summary>
        /// Reset Azure cloud service properties retrieved from Azure.
        /// </summary>
        /// <returns></returns>
        public void ResetAzureCloudServiceProperties()
        {

            try
            {
                this.VMSize = DefaultValue_ValueNotAvailable;
                this.RoleInstanceCount = 0;
                this.RoleName = DefaultValue_ValueNotAvailable;
            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
