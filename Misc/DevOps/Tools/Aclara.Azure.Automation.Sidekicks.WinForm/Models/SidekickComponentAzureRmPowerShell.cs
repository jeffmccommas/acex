﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Types;
using Aclara.Tools.Common.StatusManagement;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Models
{
    public class SidekickComponentAzureRmPowerShell : SidekickComponent
    {
        #region Private Constants

        private const string PowerShellModule_AzurePowerShellFound = "Azure PowerShell installed.";
        private const string PowerShellModule_AuzrePowerShellNotFound = "Azure PowerShell not installed.";

        private const string AzureLogin_LoginRequired = "Azure login required.";
        private const string AzureLogin_LoginError = "Run Login-AzureRmAccount to login.";

        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors
        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve version status.
        /// </summary>
        /// <returns></returns>
        public override SidekickComponentVersionStatus GetVersionStatus()
        {
            SidekickComponentVersionStatus result = SidekickComponentVersionStatus.NotFound;
            string powerShellScript = string.Empty;
            StatusList statusList = null;
            List<object> dataList = null;
            string dataAsText = string.Empty;
            PowerShellModule powerShellModule = null;
            PowerShellModule[] powerShellModuleList = null;

            try
            {

                powerShellScript = @"Get-Module -ListAvailable AzureRM -Refresh | Select-Object -Property Name, Version | ConvertTo-Json";

                powerShellScript = string.Format(powerShellScript);

                statusList = new StatusList();

                dataList = this.InvokePowerShellScriptSync(powerShellScript, ref statusList);

                //Error(s) detected.
                if (statusList != null && statusList.Count > 0)
                {
                    if (statusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error) == true)
                    {
                        result = SidekickComponentVersionStatus.NotFound;
                        return result;
                    }
                }

                foreach (object data in dataList)
                {
                    if (data is string)
                    {
                        dataAsText = data.ToString();

                        if (dataAsText.Contains("[") == true)
                        {
                            powerShellModuleList = JsonConvert.DeserializeObject<PowerShellModule[]>(dataAsText);
                            powerShellModule = this.GetLargestVersion(powerShellModuleList);
                        }
                        else
                        {
                            powerShellModule = JsonConvert.DeserializeObject<PowerShellModule>(dataAsText);
                        }

                        this.Version = this.ConvertToVersion(powerShellModule.Version);
                        this.VersionAsText = this.Version.ToString();

                        result = SidekickComponent.CompareVersions(this.Version, this.MinimumVersion);
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Install sidekick component.
        /// </summary>
        /// <returns></returns>
        public override bool Install()
        {
            bool result = false;
            string powerShellScript = string.Empty;
            StatusList statusList = null;
            List<object> dataList = null;

            try
            {
                powerShellScript = string.Format(@"Install-Module AzureRM -RequiredVersion {0} -Force -AllowClobber",
                                                 this.MinimumVersionAsText);

                statusList = new StatusList();

                dataList = this.InvokePowerShellScriptSync(powerShellScript, ref statusList);

                foreach (Status status in statusList)
                {
                    if (status.Exception != null &&
                        status.Exception.Message.Contains(AzureLogin_LoginError) == true)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Update sidekick component.
        /// </summary>
        /// <returns></returns>
        public override bool Update()
        {
            try
            {
                return this.Install();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }
}
