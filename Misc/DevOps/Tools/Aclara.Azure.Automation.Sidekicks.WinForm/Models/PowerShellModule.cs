﻿using Newtonsoft.Json;
using System;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Models
{

    public class PowerShellModule
    {

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Version")]
        public Version Version { get; set; }

    }

    public class Version
    {

        [JsonProperty("Major")]
        public int Major { get; set; }

        [JsonProperty("Minor")]
        public int Minor { get; set; }

        [JsonProperty("Build")]
        public int Build { get; set; }

        [JsonProperty("Revision")]
        public int Revision { get; set; }

        [JsonProperty("MajorRevision")]
        public int MajorRevision { get; set; }

        [JsonProperty("MinorRevision")]
        public int MinorRevision { get; set; }
    }

}
