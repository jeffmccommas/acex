﻿namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    partial class ConfirmationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfirmationForm));
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.ConfirmationMessageLabel = new System.Windows.Forms.Label();
            this.TypedResponseValueTextBox = new System.Windows.Forms.TextBox();
            this.ValidationMessageLabel = new System.Windows.Forms.Label();
            this.PendingActionMessageLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // OKButton
            // 
            this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKButton.Location = new System.Drawing.Point(492, 151);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 5;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(573, 151);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 6;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ConfirmationMessageLabel
            // 
            this.ConfirmationMessageLabel.AutoSize = true;
            this.ConfirmationMessageLabel.Location = new System.Drawing.Point(12, 33);
            this.ConfirmationMessageLabel.Name = "ConfirmationMessageLabel";
            this.ConfirmationMessageLabel.Size = new System.Drawing.Size(129, 13);
            this.ConfirmationMessageLabel.TabIndex = 0;
            this.ConfirmationMessageLabel.Text = "<*confirmation message*>";
            // 
            // TypedResponseValueTextBox
            // 
            this.TypedResponseValueTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.TypedResponseValueTextBox.Location = new System.Drawing.Point(25, 60);
            this.TypedResponseValueTextBox.Name = "TypedResponseValueTextBox";
            this.TypedResponseValueTextBox.Size = new System.Drawing.Size(78, 20);
            this.TypedResponseValueTextBox.TabIndex = 1;
            // 
            // ValidationMessageLabel
            // 
            this.ValidationMessageLabel.AutoSize = true;
            this.ValidationMessageLabel.ForeColor = System.Drawing.Color.Red;
            this.ValidationMessageLabel.Location = new System.Drawing.Point(12, 92);
            this.ValidationMessageLabel.Name = "ValidationMessageLabel";
            this.ValidationMessageLabel.Size = new System.Drawing.Size(0, 13);
            this.ValidationMessageLabel.TabIndex = 4;
            // 
            // PendingActionMessageLabel
            // 
            this.PendingActionMessageLabel.AutoSize = true;
            this.PendingActionMessageLabel.Location = new System.Drawing.Point(12, 9);
            this.PendingActionMessageLabel.Name = "PendingActionMessageLabel";
            this.PendingActionMessageLabel.Size = new System.Drawing.Size(142, 13);
            this.PendingActionMessageLabel.TabIndex = 7;
            this.PendingActionMessageLabel.Text = "<*pending action message*>";
            // 
            // ConfirmationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(660, 186);
            this.Controls.Add(this.PendingActionMessageLabel);
            this.Controls.Add(this.ValidationMessageLabel);
            this.Controls.Add(this.TypedResponseValueTextBox);
            this.Controls.Add(this.ConfirmationMessageLabel);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OKButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfirmationForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Aclara Azure Automation Sidekicks - Confirmation";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Label ConfirmationMessageLabel;
        private System.Windows.Forms.TextBox TypedResponseValueTextBox;
        private System.Windows.Forms.Label ValidationMessageLabel;
        private System.Windows.Forms.Label PendingActionMessageLabel;
    }
}