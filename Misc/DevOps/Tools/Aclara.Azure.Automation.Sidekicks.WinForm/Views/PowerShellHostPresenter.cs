﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Logging;
using Aclara.PowerShellHost;
using Aclara.PowerShellHost.Events;
using Aclara.Tools.Common.StatusManagement;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public class PowerShellHostPresenter
    {

        #region Private Constants

        private const string BackgroundTaskStatus_CancellingRequest = "Cancelling PowerShellHost request...";
        private const string BackgroundTaskStatus_Requested = "PowerShellHost requested...";
        private const string BackgroundTaskStatus_Completed = "PowerShellHost request canceled.";
        private const string BackgroundTaskStatus_Canceled = "PowerShellHost request canceled.";

        private const string BackgroundTaskTotalDuration = "[*] PowerShellHost request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})";

        private const string ApplyButtonText_Cancel = "Cancel";

        private const string Event_PowerShellInvocation_BeganWithStatus = "PowerShell completed. (Status --> {0})";
        private const string Event_PowerShellInvocation_ErrorWithStatus= "(Status --> {0})";

        private const string Event_PowerShellInvocation_CompletedWithStatus = "PowerShell completed. (Status --> {0})";
        private const string Event_PowerShellInvocation_Completed = "PowerShell completed.";

        private const string BackgroundTaskException = "Operation cancelled.";
        private const string BackgroundTaskExceptionUnobserved = "Operation cancelled. (Exception unobserved)";

        private const string RequestFailed = "Request failed.";

        #endregion

        #region Private Data Members

        private CustomLogger _logger;
        private SidekickConfiguration _sidekickConfiguration = null;
        private IPowerShellHostView _PowerShellHostView = null;
        private PowerShellHostManager _powerShellHostManager = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.PowerShellHostView.SidekickName,
                                               this.PowerShellHostView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: PowerShellHost view.
        /// </summary>
        public IPowerShellHostView PowerShellHostView
        {
            get { return _PowerShellHostView; }
            set { _PowerShellHostView = value; }
        }

        /// <summary>
        /// Property: PowerShellHost manager.
        /// </summary>
        public PowerShellHostManager PowerShellHostManager
        {
            get { return _powerShellHostManager; }
            set { _powerShellHostManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="PowerShellHostView"></param>
        public PowerShellHostPresenter(SidekickConfiguration sidekickConfiguration,
                                       IPowerShellHostView PowerShellHostView)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.PowerShellHostView = PowerShellHostView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private PowerShellHostPresenter()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Invoke PowerShell script (asynchronously).
        /// </summary>
        /// <param name="powerShellScript"></param>
        public void InvokePowerShellScriptAsync(string powerShellScript)
        {

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.PowerShellHostView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.PowerShellHostView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.PowerShellHostView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;
                    this.PowerShellHostView.ApplyButtonText = ApplyButtonText_Cancel;

                    this.PowerShellHostManager = this.CreatePowerShellHostManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.PowerShellHostManager.PowerShellInvocationBegan += OnPowerShellInvocationBegan;
                    this.PowerShellHostManager.PowerShellInvocationCompleted += OnPowerShellInvocationCompleted;
                    this.PowerShellHostManager.PowerShellPSCollectionDataAdded += OnPowerShellPSCollectionDataAdded;
                    this.PowerShellHostManager.PowerShellStreamErrorDataAdded += OnPowerShellStreamErrorDataAdded;
                    this.PowerShellHostManager.PowerShellStreamWarningDataAdded += OnPowerShellStreamWarningDataAdded;
                    this.PowerShellHostManager.PowerShellStreamDebugDataAdded += OnPowerShellStreamDebugDataAdded;
                    this.PowerShellHostManager.PowerShellStreamVerboseDataAdded += OnPowerShellStreamVerboseDataAdded;

                    this.BackgroundTask = new Task(() => this.PowerShellHostManager.InvokePowerShellScript(powerShellScript, cancellationToken),
                                                                                                           cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalDuration;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.PowerShellHostView.BackgroundTaskCompleted(BackgroundTaskStatus_Completed);
                        this.PowerShellHostView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format(BackgroundTaskStatus_Canceled));

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.PowerShellHostView.BackgroundTaskCompleted(string.Empty);
                        this.PowerShellHostView.ApplyButtonEnable(true);

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(BackgroundTaskException));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(BackgroundTaskException));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(RequestFailed));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(RequestFailed));
                                    }

                                }
                            }
                        }

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.PowerShellHostView.BackgroundTaskCompleted(Event_PowerShellInvocation_Completed);
                        this.PowerShellHostView.ApplyButtonEnable(true);

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create PowerShellHost manager.
        /// </summary>
        /// <returns></returns>
        protected PowerShellHostManager CreatePowerShellHostManager()
        {
            PowerShellHostManager result = null;

            try
            {
                result = PowerShellHostManagerFactory.CreatePowerShellHostManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: PowerShell invocation completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="powerShellInvocationBeganEventArgs"></param>
        protected void OnPowerShellInvocationBegan(Object sender,
                                                   PowerShellInvocationBeganEventArgs powerShellInvocationBeganEventArgs)
        {
            try
            {

                //Log event completed with status.
                if (powerShellInvocationBeganEventArgs.StatusList != null &&
                    powerShellInvocationBeganEventArgs.StatusList.Count > 0)
                {
                    this.LogEntryFromStatusList(Event_PowerShellInvocation_BeganWithStatus, powerShellInvocationBeganEventArgs.StatusList);
                }
                //Log event completed without status.
                else
                {
                    this.Logger.Info(powerShellInvocationBeganEventArgs.Message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Event Hanlder: PowerShell invocation completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="powerShellInvocationCompletedEventArgs"></param>
        protected void OnPowerShellInvocationCompleted(Object sender,
                                                       PowerShellInvocationCompletedEventArgs powerShellInvocationCompletedEventArgs)
        {

            try
            {

                //Log event completed with status.
                if (powerShellInvocationCompletedEventArgs.StatusList != null &&
                    powerShellInvocationCompletedEventArgs.StatusList.Count > 0)
                {
                    this.LogEntryFromStatusList(Event_PowerShellInvocation_CompletedWithStatus, powerShellInvocationCompletedEventArgs.StatusList);

                }
                //Log event completed without status.
                else
                {
                    this.Logger.Info(powerShellInvocationCompletedEventArgs.Message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Hanlder: PowerShell PS collection data added.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="powerShellPSCollectionDataAddedEventArgs"></param>
        protected void OnPowerShellPSCollectionDataAdded(Object sender,
                                                         PowerShellPSCollectionDataAddedEventArgs powerShellPSCollectionDataAddedEventArgs)
        {
            object data = null;

            try
            {
                data = powerShellPSCollectionDataAddedEventArgs.Data;
                this.PowerShellHostView.PowerShellOutputDataCollection.Add(data);
                this.PowerShellHostView.PopulatePowerShellOutputDataCollection(data);

            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Event Hanlder: PowerShell stream error data added.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="powerShellStreamErrorDataAddedEventArgs"></param>
        protected void OnPowerShellStreamErrorDataAdded(Object sender,
                                                        PowerShellStreamErrorDataAddedEventArgs powerShellStreamErrorDataAddedEventArgs)
        {

            try
            {

                //Log event completed with status.
                if (powerShellStreamErrorDataAddedEventArgs.StatusList != null &&
                    powerShellStreamErrorDataAddedEventArgs.StatusList.Count > 0)
                {
                    this.LogEntryFromStatusList(Event_PowerShellInvocation_ErrorWithStatus, powerShellStreamErrorDataAddedEventArgs.StatusList);
                }
                //Log event completed without status.
                else
                {
                    this.Logger.Info(powerShellStreamErrorDataAddedEventArgs.Message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Hanlder: PowerShell stream warning data added.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="powerShellStreamDebugDataAddedEventArgs"></param>
        protected void OnPowerShellStreamWarningDataAdded(Object sender,
                                                          PowerShellStreamWarningDataAddedEventArgs powerShellStreamWarningDataAddedEventArgs)
        {

            try
            {

                //Log event completed with status.
                if (powerShellStreamWarningDataAddedEventArgs.StatusList != null &&
                    powerShellStreamWarningDataAddedEventArgs.StatusList.Count > 0)
                {
                    this.LogEntryFromStatusList(Event_PowerShellInvocation_ErrorWithStatus, powerShellStreamWarningDataAddedEventArgs.StatusList);
                }
                //Log event completed without status.
                else
                {
                    this.Logger.Warn(powerShellStreamWarningDataAddedEventArgs.Message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Hanlder: PowerShell stream debug data added.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="powerShellStreamDebugDataAddedEventArgs"></param>
        protected void OnPowerShellStreamDebugDataAdded(Object sender,
                                                        PowerShellStreamDebugDataAddedEventArgs powerShellStreamDebugDataAddedEventArgs)
        {

            try
            {

                //Log event completed with status.
                if (powerShellStreamDebugDataAddedEventArgs.StatusList != null &&
                    powerShellStreamDebugDataAddedEventArgs.StatusList.Count > 0)
                {
                    this.LogEntryFromStatusList(Event_PowerShellInvocation_ErrorWithStatus, powerShellStreamDebugDataAddedEventArgs.StatusList);
                }
                //Log event completed without status.
                else
                {
                    this.Logger.Debug(powerShellStreamDebugDataAddedEventArgs.Message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Hanlder: PowerShell stream verbose data added.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="powerShellStreamVerboseDataAddedEventArgs"></param>
        protected void OnPowerShellStreamVerboseDataAdded(Object sender,
                                                        PowerShellStreamVerboseDataAddedEventArgs powerShellStreamVerboseDataAddedEventArgs)
        {

            try
            {

                //Log event completed with status.
                if (powerShellStreamVerboseDataAddedEventArgs.StatusList != null &&
                    powerShellStreamVerboseDataAddedEventArgs.StatusList.Count > 0)
                {
                    this.LogEntryFromStatusList(Event_PowerShellInvocation_ErrorWithStatus, powerShellStreamVerboseDataAddedEventArgs.StatusList);
                }
                //Log event completed without status.
                else
                {
                    this.Logger.Info(powerShellStreamVerboseDataAddedEventArgs.Message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Log entry from status list.
        /// </summary>
        /// <param name="messageWithFormattedText"></param>
        /// <param name="statusList"></param>
        protected void LogEntryFromStatusList(string messageWithFormattedText, StatusList statusList)
        {
            string message = string.Empty;

            try
            {
                message = string.Format(messageWithFormattedText,
                        statusList.Format(StatusTypes.FormatOption.Minimum));

                switch (statusList.GetHighestStatusSeverity())
                {
                    case StatusTypes.StatusSeverity.Unspecified:
                        this.Logger.Info(message);
                        break;
                    case StatusTypes.StatusSeverity.Error:
                        statusList = statusList.GetStatusListByStatusSeverity(StatusTypes.StatusSeverity.Error);
                        if (statusList != null && statusList[0] != null)
                        {
                            this.Logger.Error(statusList[0].Exception, message);
                        }
                        else
                        {
                            this.Logger.Error(message);
                        }
                        break;
                    case StatusTypes.StatusSeverity.Warning:
                        this.Logger.Warn(message);
                        break;
                    case StatusTypes.StatusSeverity.Informational:
                        this.Logger.Info(message);
                        break;
                    default:
                        this.Logger.Info(message);
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                        }
                        else
                        {
                            Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                }
                else
                {
                    Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
