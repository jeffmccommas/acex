﻿namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    partial class SidekickComponentAssistantForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SidekickComponentAssistantForm));
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.HelpButton = new System.Windows.Forms.Button();
            this.SidekickToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.CheckComponentsButton = new System.Windows.Forms.Button();
            this.ComponentAssistantSplitContainer = new System.Windows.Forms.SplitContainer();
            this.StatusPanel = new System.Windows.Forms.Panel();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.SidekickComponentListPanel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.ComponentAssistantSplitContainer)).BeginInit();
            this.ComponentAssistantSplitContainer.Panel1.SuspendLayout();
            this.ComponentAssistantSplitContainer.Panel2.SuspendLayout();
            this.ComponentAssistantSplitContainer.SuspendLayout();
            this.StatusPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.Azure.Automation.Sidekicks.Help.chm";
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SidekickHelpProvider.SetHelpKeyword(this.HelpButton, "");
            this.SidekickHelpProvider.SetHelpString(this.HelpButton, "SidekickAzureCloudService.htm");
            this.HelpButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(1227, 3);
            this.HelpButton.Name = "HelpButton";
            this.SidekickHelpProvider.SetShowHelp(this.HelpButton, true);
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 4;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // CheckComponentsButton
            // 
            this.CheckComponentsButton.Location = new System.Drawing.Point(5, 5);
            this.CheckComponentsButton.Name = "CheckComponentsButton";
            this.CheckComponentsButton.Size = new System.Drawing.Size(121, 23);
            this.CheckComponentsButton.TabIndex = 0;
            this.CheckComponentsButton.Text = "Check Components";
            this.SidekickToolTip.SetToolTip(this.CheckComponentsButton, "Validate selected components");
            this.CheckComponentsButton.UseVisualStyleBackColor = true;
            this.CheckComponentsButton.Click += new System.EventHandler(this.CheckComponentsButton_Click);
            // 
            // ComponentAssistantSplitContainer
            // 
            this.ComponentAssistantSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComponentAssistantSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.ComponentAssistantSplitContainer.Name = "ComponentAssistantSplitContainer";
            this.ComponentAssistantSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ComponentAssistantSplitContainer.Panel1
            // 
            this.ComponentAssistantSplitContainer.Panel1.Controls.Add(this.StatusPanel);
            // 
            // ComponentAssistantSplitContainer.Panel2
            // 
            this.ComponentAssistantSplitContainer.Panel2.Controls.Add(this.SidekickComponentListPanel);
            this.ComponentAssistantSplitContainer.Size = new System.Drawing.Size(824, 450);
            this.ComponentAssistantSplitContainer.SplitterDistance = 33;
            this.ComponentAssistantSplitContainer.SplitterWidth = 6;
            this.ComponentAssistantSplitContainer.TabIndex = 5;
            // 
            // StatusPanel
            // 
            this.StatusPanel.BackColor = System.Drawing.Color.DimGray;
            this.StatusPanel.Controls.Add(this.HelpButton);
            this.StatusPanel.Controls.Add(this.StatusLabel);
            this.StatusPanel.Controls.Add(this.CheckComponentsButton);
            this.StatusPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StatusPanel.Location = new System.Drawing.Point(0, 0);
            this.StatusPanel.Name = "StatusPanel";
            this.StatusPanel.Size = new System.Drawing.Size(824, 33);
            this.StatusPanel.TabIndex = 1;
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.StatusLabel.Location = new System.Drawing.Point(132, 10);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(55, 13);
            this.StatusLabel.TabIndex = 1;
            this.StatusLabel.Text = "<status>";
            // 
            // SidekickComponentListPanel
            // 
            this.SidekickComponentListPanel.AutoScroll = true;
            this.SidekickComponentListPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.SidekickComponentListPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SidekickComponentListPanel.Location = new System.Drawing.Point(0, 0);
            this.SidekickComponentListPanel.Name = "SidekickComponentListPanel";
            this.SidekickComponentListPanel.Size = new System.Drawing.Size(824, 411);
            this.SidekickComponentListPanel.TabIndex = 5;
            // 
            // SidekickComponentAssistantForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 450);
            this.Controls.Add(this.ComponentAssistantSplitContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(583, 202);
            this.Name = "SidekickComponentAssistantForm";
            this.Text = " Component Assistant - Aclara Azure Automation Sidekicks";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SidekickComponentAssistantForm_FormClosing);
            this.Shown += new System.EventHandler(this.SidekickComponentAssistantForm_Shown);
            this.ComponentAssistantSplitContainer.Panel1.ResumeLayout(false);
            this.ComponentAssistantSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ComponentAssistantSplitContainer)).EndInit();
            this.ComponentAssistantSplitContainer.ResumeLayout(false);
            this.StatusPanel.ResumeLayout(false);
            this.StatusPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.ToolTip SidekickToolTip;
        private System.Windows.Forms.SplitContainer ComponentAssistantSplitContainer;
        private System.Windows.Forms.Panel StatusPanel;
        private System.Windows.Forms.Button HelpButton;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Button CheckComponentsButton;
        private System.Windows.Forms.Panel SidekickComponentListPanel;
    }
}