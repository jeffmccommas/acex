﻿namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    partial class AzureCloudServiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.OptionPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskProgressBar = new System.Windows.Forms.ProgressBar();
            this.CopySplitButton = new Aclara.Azure.Automation.Sidekicks.WinForm.SplitButton();
            this.CloudServiceCopyContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CloudServiceCopyAsTextToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloudServiceCopyAsHTMLToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CancelButton = new System.Windows.Forms.Button();
            this.ScaleSplitButton = new Aclara.Azure.Automation.Sidekicks.WinForm.SplitButton();
            this.CloudServiceScaleContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CloudServiceCheckScaleUpToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloudServiceCheckScaleDownToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloudServiceCheckScaleCustomToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CheckSplitButton = new Aclara.Azure.Automation.Sidekicks.WinForm.SplitButton();
            this.CloudServiceCheckContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CloudServiceCheckAllToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloudServiceCheckNoneToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.CloudServiceCheckUnderScaledToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloudServiceCheckOverScaledToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.CloudServiceCheckDevToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloudServiceCheckQAToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloudServiceCheckUATToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloudServiceCheckPerfToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloudServiceCheckProdToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AzureCloudServiceStatusLabel = new System.Windows.Forms.Label();
            this.AzureSubscriptionNameComboBox = new System.Windows.Forms.ComboBox();
            this.AzureSubscriptionNameLabel = new System.Windows.Forms.Label();
            this.DisplaySplitButton = new Aclara.Azure.Automation.Sidekicks.WinForm.SplitButton();
            this.CloudServiceDisplayContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CloudServiceDisplayAllToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.CloudServiceDisplayDevToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloudServiceDisplayQAToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloudServiceDisplayUATToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloudServiceDisplayPerfToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloudServiceDisplayProdToolStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.SidekickActivationStatusPanel = new System.Windows.Forms.Panel();
            this.AzureCloudServiceDataGridView = new System.Windows.Forms.DataGridView();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.HelpButton = new System.Windows.Forms.Button();
            this.AzureCloudServiceToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.PropertiesPanel.SuspendLayout();
            this.OptionPanel.SuspendLayout();
            this.CloudServiceCopyContextMenuStrip.SuspendLayout();
            this.CloudServiceScaleContextMenuStrip.SuspendLayout();
            this.CloudServiceCheckContextMenuStrip.SuspendLayout();
            this.CloudServiceDisplayContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AzureCloudServiceDataGridView)).BeginInit();
            this.ControlPanel.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(0, 1);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(715, 23);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "AAATemplateAAA";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PropertiesPanel.Controls.Add(this.OptionPanel);
            this.PropertiesPanel.Controls.Add(this.SidekickActivationStatusPanel);
            this.PropertiesPanel.Controls.Add(this.AzureCloudServiceDataGridView);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesPanel.MinimumSize = new System.Drawing.Size(560, 480);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(749, 506);
            this.PropertiesPanel.TabIndex = 3;
            // 
            // OptionPanel
            // 
            this.OptionPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OptionPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.OptionPanel.Controls.Add(this.BackgroundTaskProgressBar);
            this.OptionPanel.Controls.Add(this.CopySplitButton);
            this.OptionPanel.Controls.Add(this.CancelButton);
            this.OptionPanel.Controls.Add(this.ScaleSplitButton);
            this.OptionPanel.Controls.Add(this.CheckSplitButton);
            this.OptionPanel.Controls.Add(this.AzureCloudServiceStatusLabel);
            this.OptionPanel.Controls.Add(this.AzureSubscriptionNameComboBox);
            this.OptionPanel.Controls.Add(this.AzureSubscriptionNameLabel);
            this.OptionPanel.Controls.Add(this.DisplaySplitButton);
            this.OptionPanel.Controls.Add(this.RefreshButton);
            this.OptionPanel.Location = new System.Drawing.Point(1, 25);
            this.OptionPanel.Name = "OptionPanel";
            this.OptionPanel.Size = new System.Drawing.Size(748, 31);
            this.OptionPanel.TabIndex = 1;
            // 
            // BackgroundTaskProgressBar
            // 
            this.BackgroundTaskProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BackgroundTaskProgressBar.Location = new System.Drawing.Point(704, 21);
            this.BackgroundTaskProgressBar.Name = "BackgroundTaskProgressBar";
            this.BackgroundTaskProgressBar.Size = new System.Drawing.Size(38, 10);
            this.BackgroundTaskProgressBar.TabIndex = 9;
            // 
            // CopySplitButton
            // 
            this.CopySplitButton.AutoSize = true;
            this.CopySplitButton.ContextMenuStrip = this.CloudServiceCopyContextMenuStrip;
            this.CopySplitButton.Location = new System.Drawing.Point(483, 4);
            this.CopySplitButton.Name = "CopySplitButton";
            this.CopySplitButton.Size = new System.Drawing.Size(55, 23);
            this.CopySplitButton.TabIndex = 4;
            this.CopySplitButton.Text = "Copy";
            this.CopySplitButton.UseVisualStyleBackColor = true;
            // 
            // CloudServiceCopyContextMenuStrip
            // 
            this.CloudServiceCopyContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CloudServiceCopyAsTextToolStripItem,
            this.CloudServiceCopyAsHTMLToolStripItem});
            this.CloudServiceCopyContextMenuStrip.Name = "CloudServiceCopyContextMenuStrip";
            this.CloudServiceCopyContextMenuStrip.Size = new System.Drawing.Size(147, 48);
            // 
            // CloudServiceCopyAsTextToolStripItem
            // 
            this.CloudServiceCopyAsTextToolStripItem.Name = "CloudServiceCopyAsTextToolStripItem";
            this.CloudServiceCopyAsTextToolStripItem.Size = new System.Drawing.Size(146, 22);
            this.CloudServiceCopyAsTextToolStripItem.Text = "Copy as Text";
            this.CloudServiceCopyAsTextToolStripItem.Click += new System.EventHandler(this.CloudServiceCopyAsTextToolStripItem_Click);
            // 
            // CloudServiceCopyAsHTMLToolStripItem
            // 
            this.CloudServiceCopyAsHTMLToolStripItem.Name = "CloudServiceCopyAsHTMLToolStripItem";
            this.CloudServiceCopyAsHTMLToolStripItem.Size = new System.Drawing.Size(146, 22);
            this.CloudServiceCopyAsHTMLToolStripItem.Text = "Copy as Html";
            this.CloudServiceCopyAsHTMLToolStripItem.Click += new System.EventHandler(this.CloudServiceCopyAsHTMLToolStripItem_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionCancelRefresh;
            this.CancelButton.Location = new System.Drawing.Point(665, 5);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(32, 23);
            this.CancelButton.TabIndex = 7;
            this.AzureCloudServiceToolTip.SetToolTip(this.CancelButton, "Cancel");
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ScaleSplitButton
            // 
            this.ScaleSplitButton.AutoSize = true;
            this.ScaleSplitButton.ContextMenuStrip = this.CloudServiceScaleContextMenuStrip;
            this.ScaleSplitButton.Location = new System.Drawing.Point(583, 5);
            this.ScaleSplitButton.Name = "ScaleSplitButton";
            this.ScaleSplitButton.Size = new System.Drawing.Size(76, 23);
            this.ScaleSplitButton.TabIndex = 6;
            this.ScaleSplitButton.Text = "Scale";
            this.AzureCloudServiceToolTip.SetToolTip(this.ScaleSplitButton, "Scale");
            this.ScaleSplitButton.UseVisualStyleBackColor = true;
            // 
            // CloudServiceScaleContextMenuStrip
            // 
            this.CloudServiceScaleContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CloudServiceCheckScaleUpToolStripItem,
            this.CloudServiceCheckScaleDownToolStripItem,
            this.CloudServiceCheckScaleCustomToolStripItem});
            this.CloudServiceScaleContextMenuStrip.Name = "CloudServiceScaleContextMenuStrip";
            this.CloudServiceScaleContextMenuStrip.Size = new System.Drawing.Size(117, 70);
            // 
            // CloudServiceCheckScaleUpToolStripItem
            // 
            this.CloudServiceCheckScaleUpToolStripItem.Name = "CloudServiceCheckScaleUpToolStripItem";
            this.CloudServiceCheckScaleUpToolStripItem.Size = new System.Drawing.Size(116, 22);
            this.CloudServiceCheckScaleUpToolStripItem.Text = "Up";
            this.CloudServiceCheckScaleUpToolStripItem.Click += new System.EventHandler(this.CloudServiceCheckScaleUpToolStripItem_Click);
            // 
            // CloudServiceCheckScaleDownToolStripItem
            // 
            this.CloudServiceCheckScaleDownToolStripItem.Name = "CloudServiceCheckScaleDownToolStripItem";
            this.CloudServiceCheckScaleDownToolStripItem.Size = new System.Drawing.Size(116, 22);
            this.CloudServiceCheckScaleDownToolStripItem.Text = "Down";
            this.CloudServiceCheckScaleDownToolStripItem.Click += new System.EventHandler(this.CloudServiceCheckScaleDownToolStripItem_Click);
            // 
            // CloudServiceCheckScaleCustomToolStripItem
            // 
            this.CloudServiceCheckScaleCustomToolStripItem.Name = "CloudServiceCheckScaleCustomToolStripItem";
            this.CloudServiceCheckScaleCustomToolStripItem.Size = new System.Drawing.Size(116, 22);
            this.CloudServiceCheckScaleCustomToolStripItem.Text = "Custom";
            this.CloudServiceCheckScaleCustomToolStripItem.Click += new System.EventHandler(this.CloudServiceCheckScaleCustomToolStripItem_Click);
            // 
            // CheckSplitButton
            // 
            this.CheckSplitButton.AutoSize = true;
            this.CheckSplitButton.ContextMenuStrip = this.CloudServiceCheckContextMenuStrip;
            this.CheckSplitButton.Location = new System.Drawing.Point(363, 5);
            this.CheckSplitButton.Name = "CheckSplitButton";
            this.CheckSplitButton.Size = new System.Drawing.Size(55, 23);
            this.CheckSplitButton.TabIndex = 2;
            this.CheckSplitButton.Text = "Check";
            this.AzureCloudServiceToolTip.SetToolTip(this.CheckSplitButton, "Check");
            this.CheckSplitButton.UseVisualStyleBackColor = true;
            // 
            // CloudServiceCheckContextMenuStrip
            // 
            this.CloudServiceCheckContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CloudServiceCheckAllToolStripItem,
            this.CloudServiceCheckNoneToolStripItem,
            this.toolStripSeparator3,
            this.CloudServiceCheckUnderScaledToolStripItem,
            this.CloudServiceCheckOverScaledToolStripItem,
            this.toolStripSeparator2,
            this.CloudServiceCheckDevToolStripItem,
            this.CloudServiceCheckQAToolStripItem,
            this.CloudServiceCheckUATToolStripItem,
            this.CloudServiceCheckPerfToolStripItem,
            this.CloudServiceCheckProdToolStripItem});
            this.CloudServiceCheckContextMenuStrip.Name = "CloudServiceCheckContextMenuStrip";
            this.CloudServiceCheckContextMenuStrip.Size = new System.Drawing.Size(144, 214);
            // 
            // CloudServiceCheckAllToolStripItem
            // 
            this.CloudServiceCheckAllToolStripItem.Name = "CloudServiceCheckAllToolStripItem";
            this.CloudServiceCheckAllToolStripItem.Size = new System.Drawing.Size(143, 22);
            this.CloudServiceCheckAllToolStripItem.Text = "All";
            this.CloudServiceCheckAllToolStripItem.Click += new System.EventHandler(this.CloudServiceCheckAllToolStripItem_Click);
            // 
            // CloudServiceCheckNoneToolStripItem
            // 
            this.CloudServiceCheckNoneToolStripItem.Name = "CloudServiceCheckNoneToolStripItem";
            this.CloudServiceCheckNoneToolStripItem.Size = new System.Drawing.Size(143, 22);
            this.CloudServiceCheckNoneToolStripItem.Text = "None";
            this.CloudServiceCheckNoneToolStripItem.Click += new System.EventHandler(this.CloudServiceCheckNoneToolStripItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(140, 6);
            // 
            // CloudServiceCheckUnderScaledToolStripItem
            // 
            this.CloudServiceCheckUnderScaledToolStripItem.Name = "CloudServiceCheckUnderScaledToolStripItem";
            this.CloudServiceCheckUnderScaledToolStripItem.Size = new System.Drawing.Size(143, 22);
            this.CloudServiceCheckUnderScaledToolStripItem.Text = "Under Scaled";
            this.CloudServiceCheckUnderScaledToolStripItem.Click += new System.EventHandler(this.CloudServiceCheckUnderScaledToolStripItem_Click);
            // 
            // CloudServiceCheckOverScaledToolStripItem
            // 
            this.CloudServiceCheckOverScaledToolStripItem.Name = "CloudServiceCheckOverScaledToolStripItem";
            this.CloudServiceCheckOverScaledToolStripItem.Size = new System.Drawing.Size(143, 22);
            this.CloudServiceCheckOverScaledToolStripItem.Text = "Over Scaled";
            this.CloudServiceCheckOverScaledToolStripItem.Click += new System.EventHandler(this.CloudServiceCheckOverScaledToolStripItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(140, 6);
            // 
            // CloudServiceCheckDevToolStripItem
            // 
            this.CloudServiceCheckDevToolStripItem.CheckOnClick = true;
            this.CloudServiceCheckDevToolStripItem.Name = "CloudServiceCheckDevToolStripItem";
            this.CloudServiceCheckDevToolStripItem.Size = new System.Drawing.Size(143, 22);
            this.CloudServiceCheckDevToolStripItem.Text = "Dev";
            this.CloudServiceCheckDevToolStripItem.Click += new System.EventHandler(this.CloudServiceCheckDevToolStripItem_Click);
            // 
            // CloudServiceCheckQAToolStripItem
            // 
            this.CloudServiceCheckQAToolStripItem.CheckOnClick = true;
            this.CloudServiceCheckQAToolStripItem.Name = "CloudServiceCheckQAToolStripItem";
            this.CloudServiceCheckQAToolStripItem.Size = new System.Drawing.Size(143, 22);
            this.CloudServiceCheckQAToolStripItem.Text = "QA";
            this.CloudServiceCheckQAToolStripItem.Click += new System.EventHandler(this.CloudServiceCheckQAToolStripItem_Click);
            // 
            // CloudServiceCheckUATToolStripItem
            // 
            this.CloudServiceCheckUATToolStripItem.CheckOnClick = true;
            this.CloudServiceCheckUATToolStripItem.Name = "CloudServiceCheckUATToolStripItem";
            this.CloudServiceCheckUATToolStripItem.Size = new System.Drawing.Size(143, 22);
            this.CloudServiceCheckUATToolStripItem.Text = "UAT";
            this.CloudServiceCheckUATToolStripItem.Click += new System.EventHandler(this.CloudServiceCheckUATToolStripItem_Click);
            // 
            // CloudServiceCheckPerfToolStripItem
            // 
            this.CloudServiceCheckPerfToolStripItem.CheckOnClick = true;
            this.CloudServiceCheckPerfToolStripItem.Name = "CloudServiceCheckPerfToolStripItem";
            this.CloudServiceCheckPerfToolStripItem.Size = new System.Drawing.Size(143, 22);
            this.CloudServiceCheckPerfToolStripItem.Text = "Perf";
            this.CloudServiceCheckPerfToolStripItem.Click += new System.EventHandler(this.CloudServiceCheckPerfToolStripItem_Click);
            // 
            // CloudServiceCheckProdToolStripItem
            // 
            this.CloudServiceCheckProdToolStripItem.CheckOnClick = true;
            this.CloudServiceCheckProdToolStripItem.Name = "CloudServiceCheckProdToolStripItem";
            this.CloudServiceCheckProdToolStripItem.Size = new System.Drawing.Size(143, 22);
            this.CloudServiceCheckProdToolStripItem.Text = "Prod";
            this.CloudServiceCheckProdToolStripItem.Click += new System.EventHandler(this.CloudServiceCheckProdToolStripItem_Click);
            // 
            // AzureCloudServiceStatusLabel
            // 
            this.AzureCloudServiceStatusLabel.AutoSize = true;
            this.AzureCloudServiceStatusLabel.Location = new System.Drawing.Point(704, 6);
            this.AzureCloudServiceStatusLabel.Name = "AzureCloudServiceStatusLabel";
            this.AzureCloudServiceStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.AzureCloudServiceStatusLabel.TabIndex = 8;
            // 
            // AzureSubscriptionNameComboBox
            // 
            this.AzureSubscriptionNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AzureSubscriptionNameComboBox.FormattingEnabled = true;
            this.AzureSubscriptionNameComboBox.Location = new System.Drawing.Point(96, 7);
            this.AzureSubscriptionNameComboBox.Name = "AzureSubscriptionNameComboBox";
            this.AzureSubscriptionNameComboBox.Size = new System.Drawing.Size(261, 21);
            this.AzureSubscriptionNameComboBox.TabIndex = 1;
            this.AzureCloudServiceToolTip.SetToolTip(this.AzureSubscriptionNameComboBox, "Azure subscription");
            this.AzureSubscriptionNameComboBox.SelectedIndexChanged += new System.EventHandler(this.AzureSubscriptionNameComboBox_SelectedIndexChanged);
            // 
            // AzureSubscriptionNameLabel
            // 
            this.AzureSubscriptionNameLabel.AutoSize = true;
            this.AzureSubscriptionNameLabel.Location = new System.Drawing.Point(3, 10);
            this.AzureSubscriptionNameLabel.Name = "AzureSubscriptionNameLabel";
            this.AzureSubscriptionNameLabel.Size = new System.Drawing.Size(96, 13);
            this.AzureSubscriptionNameLabel.TabIndex = 0;
            this.AzureSubscriptionNameLabel.Text = "Azure subscription:";
            // 
            // DisplaySplitButton
            // 
            this.DisplaySplitButton.AutoSize = true;
            this.DisplaySplitButton.ContextMenuStrip = this.CloudServiceDisplayContextMenuStrip;
            this.DisplaySplitButton.Location = new System.Drawing.Point(424, 5);
            this.DisplaySplitButton.Name = "DisplaySplitButton";
            this.DisplaySplitButton.Size = new System.Drawing.Size(55, 23);
            this.DisplaySplitButton.TabIndex = 3;
            this.DisplaySplitButton.Text = "Display";
            this.AzureCloudServiceToolTip.SetToolTip(this.DisplaySplitButton, "Display");
            this.DisplaySplitButton.UseVisualStyleBackColor = true;
            // 
            // CloudServiceDisplayContextMenuStrip
            // 
            this.CloudServiceDisplayContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CloudServiceDisplayAllToolStripItem,
            this.toolStripSeparator1,
            this.CloudServiceDisplayDevToolStripItem,
            this.CloudServiceDisplayQAToolStripItem,
            this.CloudServiceDisplayUATToolStripItem,
            this.CloudServiceDisplayPerfToolStripItem,
            this.CloudServiceDisplayProdToolStripItem});
            this.CloudServiceDisplayContextMenuStrip.Name = "CloudServiceDisplayContextMenuStrip";
            this.CloudServiceDisplayContextMenuStrip.Size = new System.Drawing.Size(100, 142);
            // 
            // CloudServiceDisplayAllToolStripItem
            // 
            this.CloudServiceDisplayAllToolStripItem.Name = "CloudServiceDisplayAllToolStripItem";
            this.CloudServiceDisplayAllToolStripItem.Size = new System.Drawing.Size(99, 22);
            this.CloudServiceDisplayAllToolStripItem.Text = "All";
            this.CloudServiceDisplayAllToolStripItem.Click += new System.EventHandler(this.CloudServiceDisplayAllToolStripItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(96, 6);
            // 
            // CloudServiceDisplayDevToolStripItem
            // 
            this.CloudServiceDisplayDevToolStripItem.CheckOnClick = true;
            this.CloudServiceDisplayDevToolStripItem.Name = "CloudServiceDisplayDevToolStripItem";
            this.CloudServiceDisplayDevToolStripItem.Size = new System.Drawing.Size(99, 22);
            this.CloudServiceDisplayDevToolStripItem.Text = "Dev";
            this.CloudServiceDisplayDevToolStripItem.Click += new System.EventHandler(this.CloudServiceDisplayDevToolStripItem_Click);
            // 
            // CloudServiceDisplayQAToolStripItem
            // 
            this.CloudServiceDisplayQAToolStripItem.CheckOnClick = true;
            this.CloudServiceDisplayQAToolStripItem.Name = "CloudServiceDisplayQAToolStripItem";
            this.CloudServiceDisplayQAToolStripItem.Size = new System.Drawing.Size(99, 22);
            this.CloudServiceDisplayQAToolStripItem.Text = "QA";
            this.CloudServiceDisplayQAToolStripItem.Click += new System.EventHandler(this.CloudServiceDisplayQAToolStripItem_Click);
            // 
            // CloudServiceDisplayUATToolStripItem
            // 
            this.CloudServiceDisplayUATToolStripItem.CheckOnClick = true;
            this.CloudServiceDisplayUATToolStripItem.Name = "CloudServiceDisplayUATToolStripItem";
            this.CloudServiceDisplayUATToolStripItem.Size = new System.Drawing.Size(99, 22);
            this.CloudServiceDisplayUATToolStripItem.Text = "UAT";
            this.CloudServiceDisplayUATToolStripItem.Click += new System.EventHandler(this.CloudServiceDisplayUATToolStripItem_Click);
            // 
            // CloudServiceDisplayPerfToolStripItem
            // 
            this.CloudServiceDisplayPerfToolStripItem.CheckOnClick = true;
            this.CloudServiceDisplayPerfToolStripItem.Name = "CloudServiceDisplayPerfToolStripItem";
            this.CloudServiceDisplayPerfToolStripItem.Size = new System.Drawing.Size(99, 22);
            this.CloudServiceDisplayPerfToolStripItem.Text = "Perf";
            this.CloudServiceDisplayPerfToolStripItem.Click += new System.EventHandler(this.CloudServiceDisplayPerfToolStripItem_Click);
            // 
            // CloudServiceDisplayProdToolStripItem
            // 
            this.CloudServiceDisplayProdToolStripItem.CheckOnClick = true;
            this.CloudServiceDisplayProdToolStripItem.Name = "CloudServiceDisplayProdToolStripItem";
            this.CloudServiceDisplayProdToolStripItem.Size = new System.Drawing.Size(99, 22);
            this.CloudServiceDisplayProdToolStripItem.Text = "Prod";
            this.CloudServiceDisplayProdToolStripItem.Click += new System.EventHandler(this.CloudServiceDisplayProdToolStripItem_Click);
            // 
            // RefreshButton
            // 
            this.RefreshButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
            this.RefreshButton.Location = new System.Drawing.Point(545, 5);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(32, 23);
            this.RefreshButton.TabIndex = 5;
            this.AzureCloudServiceToolTip.SetToolTip(this.RefreshButton, "Refresh");
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // SidekickActivationStatusPanel
            // 
            this.SidekickActivationStatusPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SidekickActivationStatusPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.SidekickActivationStatusPanel.Location = new System.Drawing.Point(1, 25);
            this.SidekickActivationStatusPanel.Name = "SidekickActivationStatusPanel";
            this.SidekickActivationStatusPanel.Size = new System.Drawing.Size(748, 27);
            this.SidekickActivationStatusPanel.TabIndex = 4;
            // 
            // AzureCloudServiceDataGridView
            // 
            this.AzureCloudServiceDataGridView.AllowUserToAddRows = false;
            this.AzureCloudServiceDataGridView.AllowUserToDeleteRows = false;
            this.AzureCloudServiceDataGridView.AllowUserToOrderColumns = true;
            this.AzureCloudServiceDataGridView.AllowUserToResizeRows = false;
            this.AzureCloudServiceDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AzureCloudServiceDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.AzureCloudServiceDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AzureCloudServiceDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AzureCloudServiceDataGridView.EnableHeadersVisualStyles = false;
            this.AzureCloudServiceDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.AzureCloudServiceDataGridView.Location = new System.Drawing.Point(0, 62);
            this.AzureCloudServiceDataGridView.Name = "AzureCloudServiceDataGridView";
            this.AzureCloudServiceDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AzureCloudServiceDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.AzureCloudServiceDataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            this.AzureCloudServiceDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.AzureCloudServiceDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.AzureCloudServiceDataGridView.ShowEditingIcon = false;
            this.AzureCloudServiceDataGridView.Size = new System.Drawing.Size(746, 398);
            this.AzureCloudServiceDataGridView.TabIndex = 2;
            this.AzureCloudServiceDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.AzureCloudServiceDataGridView_CellClick);
            this.AzureCloudServiceDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.AzureCloudServiceDataGridView_CellContentClick);
            this.AzureCloudServiceDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.AzureCloudServiceDataGridView_CellValueChanged);
            this.AzureCloudServiceDataGridView.SelectionChanged += new System.EventHandler(this.AzureCloudServiceDataGridView_SelectionChanged);
            this.AzureCloudServiceDataGridView.Sorted += new System.EventHandler(this.AzureCloudServiceDataGridView_Sorted);
            // 
            // ControlPanel
            // 
            this.ControlPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Controls.Add(this.ApplyButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 466);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(749, 40);
            this.ControlPanel.TabIndex = 3;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(668, 11);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 1;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Visible = false;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.HelpButton);
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(749, 25);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SidekickHelpProvider.SetHelpKeyword(this.HelpButton, "");
            this.SidekickHelpProvider.SetHelpString(this.HelpButton, "SidekickAzureCloudService.htm");
            this.HelpButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(721, 1);
            this.HelpButton.Name = "HelpButton";
            this.SidekickHelpProvider.SetShowHelp(this.HelpButton, true);
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 5;
            this.AzureCloudServiceToolTip.SetToolTip(this.HelpButton, "Help");
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.Azure.Automation.Sidekicks.Help.chm";
            // 
            // AzureCloudServiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 506);
            this.Controls.Add(this.PropertiesPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "AzureCloudServiceForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "AzureCloudServiceForm";
            this.PropertiesPanel.ResumeLayout(false);
            this.OptionPanel.ResumeLayout(false);
            this.OptionPanel.PerformLayout();
            this.CloudServiceCopyContextMenuStrip.ResumeLayout(false);
            this.CloudServiceScaleContextMenuStrip.ResumeLayout(false);
            this.CloudServiceCheckContextMenuStrip.ResumeLayout(false);
            this.CloudServiceDisplayContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AzureCloudServiceDataGridView)).EndInit();
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.DataGridView AzureCloudServiceDataGridView;
        private System.Windows.Forms.Panel OptionPanel;
        private System.Windows.Forms.Button RefreshButton;
        private SplitButton DisplaySplitButton;
        private System.Windows.Forms.ContextMenuStrip CloudServiceDisplayContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceDisplayAllToolStripItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceDisplayDevToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceDisplayQAToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceDisplayUATToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceDisplayPerfToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceDisplayProdToolStripItem;
        private System.Windows.Forms.ComboBox AzureSubscriptionNameComboBox;
        private System.Windows.Forms.Label AzureSubscriptionNameLabel;
        private System.Windows.Forms.ToolTip AzureCloudServiceToolTip;
        private System.Windows.Forms.Label AzureCloudServiceStatusLabel;
        private SplitButton CheckSplitButton;
        private System.Windows.Forms.ContextMenuStrip CloudServiceCheckContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceCheckAllToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceCheckNoneToolStripItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceCheckDevToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceCheckQAToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceCheckUATToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceCheckPerfToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceCheckProdToolStripItem;
        private SplitButton ScaleSplitButton;
        private System.Windows.Forms.ContextMenuStrip CloudServiceScaleContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceCheckScaleUpToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceCheckScaleDownToolStripItem;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceCheckScaleCustomToolStripItem;
        private SplitButton CopySplitButton;
        private System.Windows.Forms.ContextMenuStrip CloudServiceCopyContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceCopyAsTextToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceCopyAsHTMLToolStripItem;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceCheckUnderScaledToolStripItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem CloudServiceCheckOverScaledToolStripItem;
        private System.Windows.Forms.ProgressBar BackgroundTaskProgressBar;
        private System.Windows.Forms.Panel SidekickActivationStatusPanel;
        private System.Windows.Forms.Button HelpButton;
    }
}