﻿using Aclara.AzurePowerBI.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public interface IReportPortalConfigView
    {
        string SidekickName { get; }

        string SidekickDescription { get; }

        string BackgroundTaskStatus { get; set; }

        string ApplyButtonText { get; set; }

        void ApplyButtonEnable(bool enable);

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);

        void PopulateGetDatasetsDatasetList();

        List<PowerBIDataset> GetDatasetsDatasetList { get; set; }
        
        void PopulateGetWorkspacesWorkspaceIdList();

        List<string> GetWorkspacesWorkspaceIdList { get; set; }

        List<PowerBIDataset> ImportPbixDatasetList { get; set; }

        void PopulatePbixImportStatus(string status);

        string CreateWorkspaceWorkspaceId { get; set; }

        void PopulateCreateWorkspaceWorkspaceId();

    }
}
