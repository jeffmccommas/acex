﻿namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    partial class LogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ClearButton = new System.Windows.Forms.Button();
            this.DisplayErrorsCheckBox = new System.Windows.Forms.CheckBox();
            this.DisplayWarningsCheckBox = new System.Windows.Forms.CheckBox();
            this.DisplayInformationCheckBox = new System.Windows.Forms.CheckBox();
            this.LogEventInfoDataGridView = new System.Windows.Forms.DataGridView();
            this.TimeStampColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SeverityIconColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.SeverityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MessageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SidekickDescriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LogContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CopyAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CopySelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.DisplayDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ClearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.DetailsButton = new System.Windows.Forms.Button();
            this.CopyButtonContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CopyButtonSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyButtonAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CopySplitButton = new Aclara.Azure.Automation.Sidekicks.WinForm.SplitButton();
            this.AutoScrollCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowLogFileLinkLabel = new System.Windows.Forms.LinkLabel();
            this.SidekickFilterComboBox = new System.Windows.Forms.ComboBox();
            this.LogToolTip = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.LogEventInfoDataGridView)).BeginInit();
            this.LogContextMenuStrip.SuspendLayout();
            this.CopyButtonContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // ClearButton
            // 
            this.ClearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ClearButton.Enabled = false;
            this.ClearButton.Location = new System.Drawing.Point(1148, 79);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 10;
            this.ClearButton.Text = "Clear";
            this.ClearButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // DisplayErrorsCheckBox
            // 
            this.DisplayErrorsCheckBox.AutoSize = true;
            this.DisplayErrorsCheckBox.Checked = true;
            this.DisplayErrorsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DisplayErrorsCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.DisplayErrorsCheckBox.Location = new System.Drawing.Point(43, 2);
            this.DisplayErrorsCheckBox.Name = "DisplayErrorsCheckBox";
            this.DisplayErrorsCheckBox.Size = new System.Drawing.Size(60, 17);
            this.DisplayErrorsCheckBox.TabIndex = 1;
            this.DisplayErrorsCheckBox.Text = "0 Errors";
            this.LogToolTip.SetToolTip(this.DisplayErrorsCheckBox, "Error log entry count.");
            this.DisplayErrorsCheckBox.UseVisualStyleBackColor = true;
            this.DisplayErrorsCheckBox.CheckedChanged += new System.EventHandler(this.DisplayErrorsCheckBox_CheckedChanged);
            // 
            // DisplayWarningsCheckBox
            // 
            this.DisplayWarningsCheckBox.AutoSize = true;
            this.DisplayWarningsCheckBox.Checked = true;
            this.DisplayWarningsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DisplayWarningsCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.DisplayWarningsCheckBox.Location = new System.Drawing.Point(116, 2);
            this.DisplayWarningsCheckBox.Name = "DisplayWarningsCheckBox";
            this.DisplayWarningsCheckBox.Size = new System.Drawing.Size(78, 17);
            this.DisplayWarningsCheckBox.TabIndex = 2;
            this.DisplayWarningsCheckBox.Text = "0 Warnings";
            this.LogToolTip.SetToolTip(this.DisplayWarningsCheckBox, "Warning log entry count.");
            this.DisplayWarningsCheckBox.UseVisualStyleBackColor = true;
            this.DisplayWarningsCheckBox.CheckedChanged += new System.EventHandler(this.DisplayWarningsCheckBox_CheckedChanged);
            // 
            // DisplayInformationCheckBox
            // 
            this.DisplayInformationCheckBox.AutoSize = true;
            this.DisplayInformationCheckBox.Checked = true;
            this.DisplayInformationCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DisplayInformationCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.DisplayInformationCheckBox.Location = new System.Drawing.Point(224, 2);
            this.DisplayInformationCheckBox.Name = "DisplayInformationCheckBox";
            this.DisplayInformationCheckBox.Size = new System.Drawing.Size(93, 17);
            this.DisplayInformationCheckBox.TabIndex = 3;
            this.DisplayInformationCheckBox.Text = "0 Informational";
            this.LogToolTip.SetToolTip(this.DisplayInformationCheckBox, "Informational log entry count.");
            this.DisplayInformationCheckBox.UseVisualStyleBackColor = true;
            this.DisplayInformationCheckBox.CheckedChanged += new System.EventHandler(this.DisplayInformationCheckBox_CheckedChanged);
            // 
            // LogEventInfoDataGridView
            // 
            this.LogEventInfoDataGridView.AllowUserToAddRows = false;
            this.LogEventInfoDataGridView.AllowUserToDeleteRows = false;
            this.LogEventInfoDataGridView.AllowUserToResizeRows = false;
            this.LogEventInfoDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LogEventInfoDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.LogEventInfoDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.LogEventInfoDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.LogEventInfoDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LogEventInfoDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TimeStampColumn,
            this.SeverityIconColumn,
            this.SeverityColumn,
            this.MessageColumn,
            this.SidekickDescriptionColumn});
            this.LogEventInfoDataGridView.ContextMenuStrip = this.LogContextMenuStrip;
            this.LogEventInfoDataGridView.EnableHeadersVisualStyles = false;
            this.LogEventInfoDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.LogEventInfoDataGridView.Location = new System.Drawing.Point(7, 29);
            this.LogEventInfoDataGridView.Name = "LogEventInfoDataGridView";
            this.LogEventInfoDataGridView.ReadOnly = true;
            this.LogEventInfoDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.LogEventInfoDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.LogEventInfoDataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            this.LogEventInfoDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.LogEventInfoDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.LogEventInfoDataGridView.ShowEditingIcon = false;
            this.LogEventInfoDataGridView.Size = new System.Drawing.Size(1135, 74);
            this.LogEventInfoDataGridView.TabIndex = 7;
            this.LogEventInfoDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.LogEventInfoDataGridView_CellFormatting);
            this.LogEventInfoDataGridView.SelectionChanged += new System.EventHandler(this.LogEventInfoDataGridView_SelectionChanged);
            // 
            // TimeStampColumn
            // 
            this.TimeStampColumn.HeaderText = "TimeStamp";
            this.TimeStampColumn.Name = "TimeStampColumn";
            this.TimeStampColumn.ReadOnly = true;
            this.TimeStampColumn.ToolTipText = "TimeStamp";
            this.TimeStampColumn.Width = 120;
            // 
            // SeverityIconColumn
            // 
            this.SeverityIconColumn.HeaderText = "";
            this.SeverityIconColumn.Name = "SeverityIconColumn";
            this.SeverityIconColumn.ReadOnly = true;
            this.SeverityIconColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SeverityIconColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SeverityIconColumn.ToolTipText = "Severity Icon";
            this.SeverityIconColumn.Width = 20;
            // 
            // SeverityColumn
            // 
            this.SeverityColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.SeverityColumn.HeaderText = "Severity";
            this.SeverityColumn.MinimumWidth = 50;
            this.SeverityColumn.Name = "SeverityColumn";
            this.SeverityColumn.ReadOnly = true;
            this.SeverityColumn.ToolTipText = "Severity";
            this.SeverityColumn.Width = 70;
            // 
            // MessageColumn
            // 
            this.MessageColumn.HeaderText = "Message";
            this.MessageColumn.MinimumWidth = 400;
            this.MessageColumn.Name = "MessageColumn";
            this.MessageColumn.ReadOnly = true;
            this.MessageColumn.ToolTipText = "Message";
            this.MessageColumn.Width = 400;
            // 
            // SidekickDescriptionColumn
            // 
            this.SidekickDescriptionColumn.HeaderText = "Sidekick";
            this.SidekickDescriptionColumn.MinimumWidth = 20;
            this.SidekickDescriptionColumn.Name = "SidekickDescriptionColumn";
            this.SidekickDescriptionColumn.ReadOnly = true;
            // 
            // LogContextMenuStrip
            // 
            this.LogContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyAllToolStripMenuItem,
            this.CopySelectionToolStripMenuItem,
            this.toolStripSeparator1,
            this.DisplayDetailsToolStripMenuItem,
            this.toolStripSeparator2,
            this.ClearToolStripMenuItem});
            this.LogContextMenuStrip.Name = "LogContextMenuStrip";
            this.LogContextMenuStrip.ShowImageMargin = false;
            this.LogContextMenuStrip.Size = new System.Drawing.Size(129, 104);
            // 
            // CopyAllToolStripMenuItem
            // 
            this.CopyAllToolStripMenuItem.Name = "CopyAllToolStripMenuItem";
            this.CopyAllToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.CopyAllToolStripMenuItem.Text = "Copy All";
            this.CopyAllToolStripMenuItem.Click += new System.EventHandler(this.CopyAllToolStripMenuItem_Click);
            // 
            // CopySelectionToolStripMenuItem
            // 
            this.CopySelectionToolStripMenuItem.Name = "CopySelectionToolStripMenuItem";
            this.CopySelectionToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.CopySelectionToolStripMenuItem.Text = "Copy Selection";
            this.CopySelectionToolStripMenuItem.Click += new System.EventHandler(this.CopySelectionToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(125, 6);
            // 
            // DisplayDetailsToolStripMenuItem
            // 
            this.DisplayDetailsToolStripMenuItem.Name = "DisplayDetailsToolStripMenuItem";
            this.DisplayDetailsToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.DisplayDetailsToolStripMenuItem.Text = "Display Details";
            this.DisplayDetailsToolStripMenuItem.Click += new System.EventHandler(this.DisplayDetailsToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(125, 6);
            // 
            // ClearToolStripMenuItem
            // 
            this.ClearToolStripMenuItem.Name = "ClearToolStripMenuItem";
            this.ClearToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.ClearToolStripMenuItem.Text = "Clear";
            this.ClearToolStripMenuItem.Click += new System.EventHandler(this.ClearToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Log";
            // 
            // DetailsButton
            // 
            this.DetailsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.DetailsButton.Enabled = false;
            this.DetailsButton.Location = new System.Drawing.Point(1148, 21);
            this.DetailsButton.Name = "DetailsButton";
            this.DetailsButton.Size = new System.Drawing.Size(75, 23);
            this.DetailsButton.TabIndex = 8;
            this.DetailsButton.Text = "Details";
            this.DetailsButton.UseVisualStyleBackColor = true;
            this.DetailsButton.Click += new System.EventHandler(this.DetailsButton_Click);
            // 
            // CopyButtonContextMenuStrip
            // 
            this.CopyButtonContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyButtonSelectionToolStripMenuItem,
            this.CopyButtonAllToolStripMenuItem});
            this.CopyButtonContextMenuStrip.Name = "CopyButtonContextMenuStrip";
            this.CopyButtonContextMenuStrip.ShowImageMargin = false;
            this.CopyButtonContextMenuStrip.Size = new System.Drawing.Size(98, 48);
            // 
            // CopyButtonSelectionToolStripMenuItem
            // 
            this.CopyButtonSelectionToolStripMenuItem.Name = "CopyButtonSelectionToolStripMenuItem";
            this.CopyButtonSelectionToolStripMenuItem.Size = new System.Drawing.Size(97, 22);
            this.CopyButtonSelectionToolStripMenuItem.Text = "Selection";
            this.CopyButtonSelectionToolStripMenuItem.Click += new System.EventHandler(this.CopySelectionToolStripMenuItem_Click);
            // 
            // CopyButtonAllToolStripMenuItem
            // 
            this.CopyButtonAllToolStripMenuItem.Name = "CopyButtonAllToolStripMenuItem";
            this.CopyButtonAllToolStripMenuItem.Size = new System.Drawing.Size(97, 22);
            this.CopyButtonAllToolStripMenuItem.Text = "All";
            this.CopyButtonAllToolStripMenuItem.Click += new System.EventHandler(this.CopyAllToolStripMenuItem_Click);
            // 
            // CopySplitButton
            // 
            this.CopySplitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CopySplitButton.AutoSize = true;
            this.CopySplitButton.ContextMenuStrip = this.CopyButtonContextMenuStrip;
            this.CopySplitButton.Enabled = false;
            this.CopySplitButton.Location = new System.Drawing.Point(1148, 50);
            this.CopySplitButton.Name = "CopySplitButton";
            this.CopySplitButton.Size = new System.Drawing.Size(75, 23);
            this.CopySplitButton.TabIndex = 9;
            this.CopySplitButton.Text = "Copy";
            this.CopySplitButton.UseVisualStyleBackColor = true;
            // 
            // AutoScrollCheckBox
            // 
            this.AutoScrollCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AutoScrollCheckBox.AutoSize = true;
            this.AutoScrollCheckBox.Checked = true;
            this.AutoScrollCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AutoScrollCheckBox.Location = new System.Drawing.Point(1065, 0);
            this.AutoScrollCheckBox.Name = "AutoScrollCheckBox";
            this.AutoScrollCheckBox.Size = new System.Drawing.Size(77, 17);
            this.AutoScrollCheckBox.TabIndex = 6;
            this.AutoScrollCheckBox.Text = "Auto Scroll";
            this.LogToolTip.SetToolTip(this.AutoScrollCheckBox, "Toggle auto-scroll to latest log entry.");
            this.AutoScrollCheckBox.UseVisualStyleBackColor = true;
            // 
            // ShowLogFileLinkLabel
            // 
            this.ShowLogFileLinkLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ShowLogFileLinkLabel.AutoSize = true;
            this.ShowLogFileLinkLabel.Location = new System.Drawing.Point(985, 0);
            this.ShowLogFileLinkLabel.Name = "ShowLogFileLinkLabel";
            this.ShowLogFileLinkLabel.Size = new System.Drawing.Size(74, 13);
            this.ShowLogFileLinkLabel.TabIndex = 5;
            this.ShowLogFileLinkLabel.TabStop = true;
            this.ShowLogFileLinkLabel.Text = "Show Log File";
            this.LogToolTip.SetToolTip(this.ShowLogFileLinkLabel, "Open log file.");
            this.ShowLogFileLinkLabel.Click += new System.EventHandler(this.ShowLogFileLinkLabel_Click);
            // 
            // SidekickFilterComboBox
            // 
            this.SidekickFilterComboBox.FormattingEnabled = true;
            this.SidekickFilterComboBox.Location = new System.Drawing.Point(324, 2);
            this.SidekickFilterComboBox.Name = "SidekickFilterComboBox";
            this.SidekickFilterComboBox.Size = new System.Drawing.Size(170, 21);
            this.SidekickFilterComboBox.TabIndex = 4;
            this.LogToolTip.SetToolTip(this.SidekickFilterComboBox, "Sidekick filter.");
            this.SidekickFilterComboBox.SelectedIndexChanged += new System.EventHandler(this.SidekickFilterComboBox_SelectedIndexChanged);
            // 
            // LogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1228, 111);
            this.ControlBox = false;
            this.Controls.Add(this.SidekickFilterComboBox);
            this.Controls.Add(this.ShowLogFileLinkLabel);
            this.Controls.Add(this.AutoScrollCheckBox);
            this.Controls.Add(this.CopySplitButton);
            this.Controls.Add(this.DetailsButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LogEventInfoDataGridView);
            this.Controls.Add(this.DisplayInformationCheckBox);
            this.Controls.Add(this.DisplayWarningsCheckBox);
            this.Controls.Add(this.DisplayErrorsCheckBox);
            this.Controls.Add(this.ClearButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(600, 90);
            this.Name = "LogForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "LogForm";
            ((System.ComponentModel.ISupportInitialize)(this.LogEventInfoDataGridView)).EndInit();
            this.LogContextMenuStrip.ResumeLayout(false);
            this.CopyButtonContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.CheckBox DisplayErrorsCheckBox;
        private System.Windows.Forms.CheckBox DisplayWarningsCheckBox;
        private System.Windows.Forms.CheckBox DisplayInformationCheckBox;
        private System.Windows.Forms.DataGridView LogEventInfoDataGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button DetailsButton;
        private System.Windows.Forms.ContextMenuStrip LogContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CopyAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CopySelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem DisplayDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem ClearToolStripMenuItem;
        private SplitButton CopySplitButton;
        private System.Windows.Forms.ContextMenuStrip CopyButtonContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CopyButtonSelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CopyButtonAllToolStripMenuItem;
        private System.Windows.Forms.CheckBox AutoScrollCheckBox;
        private System.Windows.Forms.LinkLabel ShowLogFileLinkLabel;
        private System.Windows.Forms.ComboBox SidekickFilterComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimeStampColumn;
        private System.Windows.Forms.DataGridViewImageColumn SeverityIconColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SeverityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn MessageColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SidekickDescriptionColumn;
        private System.Windows.Forms.ToolTip LogToolTip;
    }
}