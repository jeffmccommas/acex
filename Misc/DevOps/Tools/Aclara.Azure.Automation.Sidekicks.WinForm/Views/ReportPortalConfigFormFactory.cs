﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public class ReportPortalConfigFormFactory
    {
        /// <summary>
        /// Create form.
        /// </summary>
        /// <returns></returns>
        public static ReportPortalConfigForm CreateForm(SidekickConfiguration sidekickConfiguration)
        {

            ReportPortalConfigForm result = null;
            ReportPortalConfigPresenter ReportPortalConfigPresenter = null;

            try
            {
                result = new ReportPortalConfigForm(sidekickConfiguration);
                ReportPortalConfigPresenter = new ReportPortalConfigPresenter(sidekickConfiguration, result);
                result.ReportPortalConfigPresenter = ReportPortalConfigPresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
