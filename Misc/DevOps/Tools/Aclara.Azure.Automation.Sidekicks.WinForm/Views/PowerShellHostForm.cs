﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Events;
using Aclara.Azure.Automation.Sidekicks.WinForm.Models;
using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public partial class PowerShellHostForm : Form, ISidekickView, IPowerShellHostView, ISidekickInfo
    {

        #region Private Constants

        private const string SidekickInfo_SidekickName = "PowerShellHost";
        private const string SidekickInfo_SidekickDescription = "PowerShell Host";

        #endregion

        #region Private Data Members

        private PowerShellHostPresenter _powerShellHostPresenter;
        private SidekickConfiguration _sidekickConfiguration = null;
        private SidekickComponentManagerForm _sidekickComponentManagerForm;
        private SidekickComponentAssistantForm _sidekickComponentAssistantForm;
        private List<object> _powerShellOutputDataCollection;
        private bool _sidekickComponentsAvailable;
        private bool _sidekickComponentsLoggedIn;
        private DateTime _sidekickLastActivatedDateTime = DateTime.MinValue;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: PowerShellHost presenter.
        /// </summary>
        public PowerShellHostPresenter PowerShellHostPresenter
        {
            get { return _powerShellHostPresenter; }
            set { _powerShellHostPresenter = value; }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Component manager form.
        /// </summary>
        public SidekickComponentManagerForm SidekickComponentManagerForm
        {
            get { return _sidekickComponentManagerForm; }
            set { _sidekickComponentManagerForm = value; }
        }

        /// <summary>
        /// Property: Sidekick component assistant form.
        /// </summary>
        public SidekickComponentAssistantForm SidekickComponentAssistantForm
        {
            get { return _sidekickComponentAssistantForm; }
            set { _sidekickComponentAssistantForm = value; }
        }

        /// <summary>
        /// PowerShell output data collection.
        /// </summary>
        public List<object> PowerShellOutputDataCollection
        {
            get { return _powerShellOutputDataCollection; }
        }

        /// <summary>
        /// Property: Sidekick components available.
        /// </summary>
        public bool SidekickComponentsAvailable
        {
            get { return _sidekickComponentsAvailable; }
            set { _sidekickComponentsAvailable = value; }
        }

        /// <summary>
        /// Property: Sidekick components logged in.
        /// </summary>
        public bool SidekickComponentsLoggedIn
        {
            get { return _sidekickComponentsLoggedIn; }
            set { _sidekickComponentsLoggedIn = value; }
        }

        /// <summary>
        /// Property: Sidekick last activated (date, time).
        /// </summary>
        public DateTime SidekickLastInitializationDataTime
        {
            get { return _sidekickLastActivatedDateTime; }
            set { _sidekickLastActivatedDateTime = value; }
        }

        /// <summary>
        /// Property: PowerShell script.
        /// </summary>
        public string PowerShellScript
        {
            get { return this.PowerShellScriptTextBox.Text; }
            set { this.PowerShellScriptTextBox.Text = value; }
        }

        /// <summary>
        /// Property:  Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get { return this.ApplyButton.Text; }
            set { this.ApplyButton.Text = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                if (this.ApplyButton.Text == "Apply")
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickInfo_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickInfo_SidekickDescription;
            }
        }


        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <param></param>
        public PowerShellHostForm(SidekickConfiguration sidekickConfiguration)
        {

            _powerShellOutputDataCollection = new List<object>();
            _sidekickComponentsAvailable = false;

            InitializeComponent();
            this.InitializeControls();

            this.EnableControlsDependantOnValidation();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Sidekick was activated.
        /// </summary>
        public void SidekickActivated()
        {
            const int TotalTimeSinceLastSidekickInitialization = 180;

            TimeSpan timeSinceLastSidekickInitializeation;
            List<SidekickComponentId> sidekickComponentIdList;

            try
            {
                this.SidekickComponentManagerForm.SidekickComponentValidationCompleted += OnSidekickComponentValidationCompleted;

                EnableControlsDependantOnValidation();

                timeSinceLastSidekickInitializeation = (this.SidekickLastInitializationDataTime - DateTime.Now).Duration();

                if (this.SidekickLastInitializationDataTime == DateTime.MinValue ||
                    this.SidekickLastInitializationDataTime == DateTime.MaxValue ||
                    timeSinceLastSidekickInitializeation.TotalMinutes > TotalTimeSinceLastSidekickInitialization ||
                    this.SidekickComponentsAvailable == false ||
                    this.SidekickComponentsLoggedIn == false)
                {
                    SidekickActivationStatusPanel.Visible = true;
                    this.SidekickActivationStatusPanel.DockControl(this.SidekickComponentManagerForm);

                    sidekickComponentIdList = new List<SidekickComponentId>();
                    sidekickComponentIdList.Add(SidekickComponentId.AzurePowerShell);

                    //Validate required sidekick components.
                    this.SidekickComponentManagerForm.SidekickComponentIdList = sidekickComponentIdList;


                    this.SidekickLastInitializationDataTime = DateTime.Now;

                }
                this.EnableControlsDependantOnValidation();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Populate PowerShell output data collection.
        /// </summary>
        /// <param name="data"></param>
        public void PopulatePowerShellOutputDataCollection(object data)
        {

            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate { PopulatePowerShellOutputDataCollection(data); });
                }
                else
                {
                    if (string.IsNullOrEmpty(this.PowerShellOutputTextBox.Text) == false)
                    {
                        this.PowerShellOutputTextBox.Text += Environment.NewLine;
                    }
                    if (data is string)
                    {
                        this.PowerShellOutputTextBox.Text += data;
                    }
                    else
                    {
                        this.PowerShellOutputTextBox.Text += data.ToString();
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Apply button enable. 
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Enable/disable controls dependant on required components.
        /// </summary>
        protected void EnableControlsDependantOnValidation()
        {

            try
            {
                if (this.SidekickComponentsAvailable == false ||
                    this.SidekickComponentsLoggedIn == false)
                {
                    this.PowerShellScriptTextBox.Enabled = false;
                    this.PowerShellOutputTextBox.Enabled = false;
                    this.ApplyButton.Enabled = false;
                }
                else
                {
                    this.PowerShellScriptTextBox.Enabled = true;
                    this.PowerShellOutputTextBox.Enabled = true;
                    this.ApplyButton.Enabled = true;
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Hanlder: Sidekick component validation completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="sidekickComponentValidationCompletedEventArgs"></param>
        protected void OnSidekickComponentValidationCompleted(Object sender,
                                                              SidekickComponentValidationCompletedEventArgs sidekickComponentValidationCompletedEventArgs)
        {
            List<SidekickComponentId> SidekickComponentIdList;

            try
            {
                SidekickComponentIdList = new List<SidekickComponentId>();
                SidekickComponentIdList.Add(SidekickComponentId.AzurePowerShell);

                this.SidekickComponentsAvailable = this.SidekickComponentManagerForm.SidekickComponentManager.SidekickComponentList.AreComponentsAvailable(SidekickComponentIdList);

                if (this.SidekickComponentsAvailable == true)
                {
                    //Login required sidekick components.
                    this.SidekickComponentManagerForm.LoginRequiredComponents(SidekickComponentIdList);
                }
                else
                {
                    //Show sidekick component assistant.
                    this.SidekickComponentAssistantForm.Show();
                }

                this.SidekickComponentsLoggedIn = this.SidekickComponentManagerForm.SidekickComponentManager.SidekickComponentList.AreComponentsLoggedIn(SidekickComponentIdList);

                if (this.SidekickComponentsAvailable == true &&
                    this.SidekickComponentsLoggedIn == true)
                {
                    SidekickActivationStatusPanel.Visible = false;
                    this.GuidancePanel.Visible = true;
                }

                this.EnableControlsDependantOnValidation();
            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            string powerShellScript = string.Empty;

            try
            {
                this.PowerShellOutputTextBox.Text = string.Empty;

                powerShellScript = this.PowerShellScript;

                this.PowerShellHostPresenter.InvokePowerShellScriptAsync(powerShellScript);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickPowerShellHost.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion
    }
}
