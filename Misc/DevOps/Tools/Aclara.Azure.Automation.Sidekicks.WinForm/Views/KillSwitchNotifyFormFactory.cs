﻿using Aclara.Sidekicks.AutoUpdater;
using Aclara.Azure.Automation.Sidekicks.WinForm.Types;
using System;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public static class KillSwitchNotifyFormFactory
    {

        /// <summary>
        /// Create form.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="versionImpacted"></param>
        /// <param name="reason"></param>
        /// <param name="killSwitchRunStatus"></param>
        /// <param name="sidekickAutoUpdater"></param>
        /// <returns></returns>
        public static KillSwitchNotifyForm CreateForm(string title,
                                                      string versionImpacted, 
                                                      string reason, 
                                                      KillSwitchRunStatus killSwitchRunStatus,
                                                      SidekicksAutoUpdater sidekickAutoUpdater)
        {

            KillSwitchNotifyForm result = null;

            try
            {
                result = new KillSwitchNotifyForm(title,
                                                  versionImpacted, 
                                                  reason, 
                                                  killSwitchRunStatus, 
                                                  sidekickAutoUpdater);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
