﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public class CassandraConfigFormFactory
    {
        /// <summary>
        /// Create form.
        /// </summary>
        /// <returns></returns>
        public static CassandraConfigForm CreateForm(SidekickConfiguration sidekickConfiguration)
        {

            CassandraConfigForm result = null;
            CassandraConfigPresenter CassandraConfigPresenter = null;

            try
            {
                result = new CassandraConfigForm(sidekickConfiguration);
                CassandraConfigPresenter = new CassandraConfigPresenter(sidekickConfiguration, result);
                result.CassandraConfigPresenter = CassandraConfigPresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
