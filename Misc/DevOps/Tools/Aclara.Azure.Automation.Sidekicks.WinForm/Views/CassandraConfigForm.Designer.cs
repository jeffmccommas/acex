﻿namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    partial class CassandraConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.CassandraTableConfigurationGroupBox = new System.Windows.Forms.GroupBox();
            this.OpenConfigurationFileButton = new System.Windows.Forms.Button();
            this.ConfigurationFilePathNameTextBox = new System.Windows.Forms.TextBox();
            this.ConfigurationFileLabel = new System.Windows.Forms.Label();
            this.CassandraTableNameComboBox = new System.Windows.Forms.ComboBox();
            this.CassandraTableNameLabel = new System.Windows.Forms.Label();
            this.ConnectionInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.CassandraNodePasswordTextBox = new System.Windows.Forms.TextBox();
            this.CassandraNodeUsernameTextBox = new System.Windows.Forms.TextBox();
            this.CassandraKeyspaceTextBox = new System.Windows.Forms.TextBox();
            this.CassandraNodePasswordLabel = new System.Windows.Forms.Label();
            this.CassandraNodeUsernameLabel = new System.Windows.Forms.Label();
            this.CassandraKeyspaceLabel = new System.Windows.Forms.Label();
            this.ManuallyEnteredConnectionInfoGuidanceLabel = new System.Windows.Forms.Label();
            this.CassandraNodesTextBox = new System.Windows.Forms.TextBox();
            this.CassandraNodesLabel = new System.Windows.Forms.Label();
            this.EnvironmentLabel = new System.Windows.Forms.Label();
            this.EnvironmentComboBox = new System.Windows.Forms.ComboBox();
            this.ManualEntryConnectionInfoRadioButton = new System.Windows.Forms.RadioButton();
            this.PreconfiguredConnectionInfoRadioButton = new System.Windows.Forms.RadioButton();
            this.AzureSubscriptionNameComboBox = new System.Windows.Forms.ComboBox();
            this.GuidanceGroupBox = new System.Windows.Forms.GroupBox();
            this.HelpButton = new System.Windows.Forms.Button();
            this.AzureSubscriptionNameLabel = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.PropertiesPanel.SuspendLayout();
            this.CassandraTableConfigurationGroupBox.SuspendLayout();
            this.ConnectionInfoGroupBox.SuspendLayout();
            this.GuidanceGroupBox.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(0, 1);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(719, 17);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "Cassandra Configuration";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PropertiesPanel.Controls.Add(this.CassandraTableConfigurationGroupBox);
            this.PropertiesPanel.Controls.Add(this.ConnectionInfoGroupBox);
            this.PropertiesPanel.Controls.Add(this.AzureSubscriptionNameComboBox);
            this.PropertiesPanel.Controls.Add(this.GuidanceGroupBox);
            this.PropertiesPanel.Controls.Add(this.AzureSubscriptionNameLabel);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesPanel.MinimumSize = new System.Drawing.Size(560, 480);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(716, 506);
            this.PropertiesPanel.TabIndex = 0;
            // 
            // CassandraTableConfigurationGroupBox
            // 
            this.CassandraTableConfigurationGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CassandraTableConfigurationGroupBox.Controls.Add(this.OpenConfigurationFileButton);
            this.CassandraTableConfigurationGroupBox.Controls.Add(this.ConfigurationFilePathNameTextBox);
            this.CassandraTableConfigurationGroupBox.Controls.Add(this.ConfigurationFileLabel);
            this.CassandraTableConfigurationGroupBox.Controls.Add(this.CassandraTableNameComboBox);
            this.CassandraTableConfigurationGroupBox.Controls.Add(this.CassandraTableNameLabel);
            this.CassandraTableConfigurationGroupBox.Location = new System.Drawing.Point(6, 317);
            this.CassandraTableConfigurationGroupBox.Name = "CassandraTableConfigurationGroupBox";
            this.CassandraTableConfigurationGroupBox.Size = new System.Drawing.Size(707, 99);
            this.CassandraTableConfigurationGroupBox.TabIndex = 4;
            this.CassandraTableConfigurationGroupBox.TabStop = false;
            this.CassandraTableConfigurationGroupBox.Text = "Cassandra Table Configuration";
            // 
            // OpenConfigurationFileButton
            // 
            this.OpenConfigurationFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OpenConfigurationFileButton.Location = new System.Drawing.Point(676, 54);
            this.OpenConfigurationFileButton.Name = "OpenConfigurationFileButton";
            this.OpenConfigurationFileButton.Size = new System.Drawing.Size(24, 23);
            this.OpenConfigurationFileButton.TabIndex = 3;
            this.OpenConfigurationFileButton.Text = "...";
            this.OpenConfigurationFileButton.UseVisualStyleBackColor = true;
            this.OpenConfigurationFileButton.Click += new System.EventHandler(this.OpenConfigurationFileButton_Click);
            // 
            // ConfigurationFilePathNameTextBox
            // 
            this.ConfigurationFilePathNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ConfigurationFilePathNameTextBox.Location = new System.Drawing.Point(129, 56);
            this.ConfigurationFilePathNameTextBox.Name = "ConfigurationFilePathNameTextBox";
            this.ConfigurationFilePathNameTextBox.Size = new System.Drawing.Size(541, 20);
            this.ConfigurationFilePathNameTextBox.TabIndex = 2;
            // 
            // ConfigurationFileLabel
            // 
            this.ConfigurationFileLabel.AutoSize = true;
            this.ConfigurationFileLabel.Location = new System.Drawing.Point(7, 59);
            this.ConfigurationFileLabel.Name = "ConfigurationFileLabel";
            this.ConfigurationFileLabel.Size = new System.Drawing.Size(114, 13);
            this.ConfigurationFileLabel.TabIndex = 1;
            this.ConfigurationFileLabel.Text = "Configuration file (csv):";
            // 
            // CassandraTableNameComboBox
            // 
            this.CassandraTableNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CassandraTableNameComboBox.FormattingEnabled = true;
            this.CassandraTableNameComboBox.Location = new System.Drawing.Point(128, 22);
            this.CassandraTableNameComboBox.Name = "CassandraTableNameComboBox";
            this.CassandraTableNameComboBox.Size = new System.Drawing.Size(156, 21);
            this.CassandraTableNameComboBox.TabIndex = 1;
            // 
            // CassandraTableNameLabel
            // 
            this.CassandraTableNameLabel.AutoSize = true;
            this.CassandraTableNameLabel.Location = new System.Drawing.Point(7, 25);
            this.CassandraTableNameLabel.Name = "CassandraTableNameLabel";
            this.CassandraTableNameLabel.Size = new System.Drawing.Size(115, 13);
            this.CassandraTableNameLabel.TabIndex = 0;
            this.CassandraTableNameLabel.Text = "Cassandra table name:";
            // 
            // ConnectionInfoGroupBox
            // 
            this.ConnectionInfoGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ConnectionInfoGroupBox.Controls.Add(this.CassandraNodePasswordTextBox);
            this.ConnectionInfoGroupBox.Controls.Add(this.CassandraNodeUsernameTextBox);
            this.ConnectionInfoGroupBox.Controls.Add(this.CassandraKeyspaceTextBox);
            this.ConnectionInfoGroupBox.Controls.Add(this.CassandraNodePasswordLabel);
            this.ConnectionInfoGroupBox.Controls.Add(this.CassandraNodeUsernameLabel);
            this.ConnectionInfoGroupBox.Controls.Add(this.CassandraKeyspaceLabel);
            this.ConnectionInfoGroupBox.Controls.Add(this.ManuallyEnteredConnectionInfoGuidanceLabel);
            this.ConnectionInfoGroupBox.Controls.Add(this.CassandraNodesTextBox);
            this.ConnectionInfoGroupBox.Controls.Add(this.CassandraNodesLabel);
            this.ConnectionInfoGroupBox.Controls.Add(this.EnvironmentLabel);
            this.ConnectionInfoGroupBox.Controls.Add(this.EnvironmentComboBox);
            this.ConnectionInfoGroupBox.Controls.Add(this.ManualEntryConnectionInfoRadioButton);
            this.ConnectionInfoGroupBox.Controls.Add(this.PreconfiguredConnectionInfoRadioButton);
            this.ConnectionInfoGroupBox.Location = new System.Drawing.Point(3, 120);
            this.ConnectionInfoGroupBox.Name = "ConnectionInfoGroupBox";
            this.ConnectionInfoGroupBox.Size = new System.Drawing.Size(710, 191);
            this.ConnectionInfoGroupBox.TabIndex = 3;
            this.ConnectionInfoGroupBox.TabStop = false;
            this.ConnectionInfoGroupBox.Text = "Cassandra Connection Information";
            // 
            // CassandraNodePasswordTextBox
            // 
            this.CassandraNodePasswordTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CassandraNodePasswordTextBox.Location = new System.Drawing.Point(220, 127);
            this.CassandraNodePasswordTextBox.Name = "CassandraNodePasswordTextBox";
            this.CassandraNodePasswordTextBox.PasswordChar = '*';
            this.CassandraNodePasswordTextBox.Size = new System.Drawing.Size(172, 20);
            this.CassandraNodePasswordTextBox.TabIndex = 11;
            // 
            // CassandraNodeUsernameTextBox
            // 
            this.CassandraNodeUsernameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CassandraNodeUsernameTextBox.Location = new System.Drawing.Point(220, 100);
            this.CassandraNodeUsernameTextBox.Name = "CassandraNodeUsernameTextBox";
            this.CassandraNodeUsernameTextBox.Size = new System.Drawing.Size(172, 20);
            this.CassandraNodeUsernameTextBox.TabIndex = 9;
            // 
            // CassandraKeyspaceTextBox
            // 
            this.CassandraKeyspaceTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CassandraKeyspaceTextBox.Location = new System.Drawing.Point(220, 73);
            this.CassandraKeyspaceTextBox.Name = "CassandraKeyspaceTextBox";
            this.CassandraKeyspaceTextBox.Size = new System.Drawing.Size(172, 20);
            this.CassandraKeyspaceTextBox.TabIndex = 7;
            // 
            // CassandraNodePasswordLabel
            // 
            this.CassandraNodePasswordLabel.AutoSize = true;
            this.CassandraNodePasswordLabel.Location = new System.Drawing.Point(127, 130);
            this.CassandraNodePasswordLabel.Name = "CassandraNodePasswordLabel";
            this.CassandraNodePasswordLabel.Size = new System.Drawing.Size(84, 13);
            this.CassandraNodePasswordLabel.TabIndex = 10;
            this.CassandraNodePasswordLabel.Text = "Node password:";
            // 
            // CassandraNodeUsernameLabel
            // 
            this.CassandraNodeUsernameLabel.AutoSize = true;
            this.CassandraNodeUsernameLabel.Location = new System.Drawing.Point(127, 103);
            this.CassandraNodeUsernameLabel.Name = "CassandraNodeUsernameLabel";
            this.CassandraNodeUsernameLabel.Size = new System.Drawing.Size(88, 13);
            this.CassandraNodeUsernameLabel.TabIndex = 8;
            this.CassandraNodeUsernameLabel.Text = "Node user name:";
            // 
            // CassandraKeyspaceLabel
            // 
            this.CassandraKeyspaceLabel.AutoSize = true;
            this.CassandraKeyspaceLabel.Location = new System.Drawing.Point(127, 76);
            this.CassandraKeyspaceLabel.Name = "CassandraKeyspaceLabel";
            this.CassandraKeyspaceLabel.Size = new System.Drawing.Size(57, 13);
            this.CassandraKeyspaceLabel.TabIndex = 6;
            this.CassandraKeyspaceLabel.Text = "Keyspace:";
            // 
            // ManuallyEnteredConnectionInfoGuidanceLabel
            // 
            this.ManuallyEnteredConnectionInfoGuidanceLabel.AutoSize = true;
            this.ManuallyEnteredConnectionInfoGuidanceLabel.Location = new System.Drawing.Point(28, 162);
            this.ManuallyEnteredConnectionInfoGuidanceLabel.Name = "ManuallyEnteredConnectionInfoGuidanceLabel";
            this.ManuallyEnteredConnectionInfoGuidanceLabel.Size = new System.Drawing.Size(554, 13);
            this.ManuallyEnteredConnectionInfoGuidanceLabel.TabIndex = 12;
            this.ManuallyEnteredConnectionInfoGuidanceLabel.Text = "Production environment is not preconfigured. Contact Cassandra administrator for " +
    "connection information (if needed).";
            // 
            // CassandraNodesTextBox
            // 
            this.CassandraNodesTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CassandraNodesTextBox.Location = new System.Drawing.Point(220, 46);
            this.CassandraNodesTextBox.Name = "CassandraNodesTextBox";
            this.CassandraNodesTextBox.Size = new System.Drawing.Size(482, 20);
            this.CassandraNodesTextBox.TabIndex = 5;
            // 
            // CassandraNodesLabel
            // 
            this.CassandraNodesLabel.AutoSize = true;
            this.CassandraNodesLabel.Location = new System.Drawing.Point(127, 49);
            this.CassandraNodesLabel.Name = "CassandraNodesLabel";
            this.CassandraNodesLabel.Size = new System.Drawing.Size(41, 13);
            this.CassandraNodesLabel.TabIndex = 4;
            this.CassandraNodesLabel.Text = "Nodes:";
            // 
            // EnvironmentLabel
            // 
            this.EnvironmentLabel.AutoSize = true;
            this.EnvironmentLabel.Location = new System.Drawing.Point(127, 22);
            this.EnvironmentLabel.Name = "EnvironmentLabel";
            this.EnvironmentLabel.Size = new System.Drawing.Size(98, 13);
            this.EnvironmentLabel.TabIndex = 1;
            this.EnvironmentLabel.Text = "Environment name:";
            // 
            // EnvironmentComboBox
            // 
            this.EnvironmentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.EnvironmentComboBox.FormattingEnabled = true;
            this.EnvironmentComboBox.Location = new System.Drawing.Point(231, 19);
            this.EnvironmentComboBox.Name = "EnvironmentComboBox";
            this.EnvironmentComboBox.Size = new System.Drawing.Size(90, 21);
            this.EnvironmentComboBox.TabIndex = 2;
            // 
            // ManualEntryConnectionInfoRadioButton
            // 
            this.ManualEntryConnectionInfoRadioButton.AutoSize = true;
            this.ManualEntryConnectionInfoRadioButton.Location = new System.Drawing.Point(10, 47);
            this.ManualEntryConnectionInfoRadioButton.Name = "ManualEntryConnectionInfoRadioButton";
            this.ManualEntryConnectionInfoRadioButton.Size = new System.Drawing.Size(86, 17);
            this.ManualEntryConnectionInfoRadioButton.TabIndex = 3;
            this.ManualEntryConnectionInfoRadioButton.TabStop = true;
            this.ManualEntryConnectionInfoRadioButton.Text = "Manual entry";
            this.ManualEntryConnectionInfoRadioButton.UseVisualStyleBackColor = true;
            this.ManualEntryConnectionInfoRadioButton.CheckedChanged += new System.EventHandler(this.ManualEntryConnectionInfoRadioButton_CheckedChanged);
            // 
            // PreconfiguredConnectionInfoRadioButton
            // 
            this.PreconfiguredConnectionInfoRadioButton.AutoSize = true;
            this.PreconfiguredConnectionInfoRadioButton.Location = new System.Drawing.Point(10, 20);
            this.PreconfiguredConnectionInfoRadioButton.Name = "PreconfiguredConnectionInfoRadioButton";
            this.PreconfiguredConnectionInfoRadioButton.Size = new System.Drawing.Size(91, 17);
            this.PreconfiguredConnectionInfoRadioButton.TabIndex = 0;
            this.PreconfiguredConnectionInfoRadioButton.TabStop = true;
            this.PreconfiguredConnectionInfoRadioButton.Text = "Preconfigured";
            this.PreconfiguredConnectionInfoRadioButton.UseVisualStyleBackColor = true;
            this.PreconfiguredConnectionInfoRadioButton.CheckedChanged += new System.EventHandler(this.PreconfiguredConnectionInfoRadioButton_CheckedChanged);
            // 
            // AzureSubscriptionNameComboBox
            // 
            this.AzureSubscriptionNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AzureSubscriptionNameComboBox.FormattingEnabled = true;
            this.AzureSubscriptionNameComboBox.Location = new System.Drawing.Point(134, 83);
            this.AzureSubscriptionNameComboBox.Name = "AzureSubscriptionNameComboBox";
            this.AzureSubscriptionNameComboBox.Size = new System.Drawing.Size(261, 21);
            this.AzureSubscriptionNameComboBox.TabIndex = 2;
            this.AzureSubscriptionNameComboBox.SelectedIndexChanged += new System.EventHandler(this.AzureSubscriptionNameComboBox_SelectedIndexChanged);
            // 
            // GuidanceGroupBox
            // 
            this.GuidanceGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GuidanceGroupBox.Controls.Add(this.HelpButton);
            this.GuidanceGroupBox.Location = new System.Drawing.Point(3, 28);
            this.GuidanceGroupBox.Name = "GuidanceGroupBox";
            this.GuidanceGroupBox.Size = new System.Drawing.Size(710, 49);
            this.GuidanceGroupBox.TabIndex = 0;
            this.GuidanceGroupBox.TabStop = false;
            this.GuidanceGroupBox.Text = "Guidance";
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(668, 11);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 2;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // AzureSubscriptionNameLabel
            // 
            this.AzureSubscriptionNameLabel.AutoSize = true;
            this.AzureSubscriptionNameLabel.Location = new System.Drawing.Point(3, 86);
            this.AzureSubscriptionNameLabel.Name = "AzureSubscriptionNameLabel";
            this.AzureSubscriptionNameLabel.Size = new System.Drawing.Size(125, 13);
            this.AzureSubscriptionNameLabel.TabIndex = 1;
            this.AzureSubscriptionNameLabel.Text = "Azure subscription name:";
            // 
            // ControlPanel
            // 
            this.ControlPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Controls.Add(this.ApplyButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 466);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(716, 40);
            this.ControlPanel.TabIndex = 5;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(635, 11);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 1;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(716, 22);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.Azure.Automation.Sidekicks.Help.chm";
            // 
            // CassandraConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 506);
            this.Controls.Add(this.PropertiesPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "CassandraConfigForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "CassandraConfigForm";
            this.PropertiesPanel.ResumeLayout(false);
            this.PropertiesPanel.PerformLayout();
            this.CassandraTableConfigurationGroupBox.ResumeLayout(false);
            this.CassandraTableConfigurationGroupBox.PerformLayout();
            this.ConnectionInfoGroupBox.ResumeLayout(false);
            this.ConnectionInfoGroupBox.PerformLayout();
            this.GuidanceGroupBox.ResumeLayout(false);
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.GroupBox ConnectionInfoGroupBox;
        private System.Windows.Forms.Label ManuallyEnteredConnectionInfoGuidanceLabel;
        private System.Windows.Forms.TextBox CassandraNodesTextBox;
        private System.Windows.Forms.Label CassandraNodesLabel;
        private System.Windows.Forms.Label EnvironmentLabel;
        private System.Windows.Forms.ComboBox EnvironmentComboBox;
        private System.Windows.Forms.RadioButton ManualEntryConnectionInfoRadioButton;
        private System.Windows.Forms.RadioButton PreconfiguredConnectionInfoRadioButton;
        private System.Windows.Forms.ComboBox AzureSubscriptionNameComboBox;
        private System.Windows.Forms.GroupBox GuidanceGroupBox;
        private System.Windows.Forms.Label AzureSubscriptionNameLabel;
        private System.Windows.Forms.TextBox CassandraNodePasswordTextBox;
        private System.Windows.Forms.TextBox CassandraNodeUsernameTextBox;
        private System.Windows.Forms.TextBox CassandraKeyspaceTextBox;
        private System.Windows.Forms.Label CassandraNodePasswordLabel;
        private System.Windows.Forms.Label CassandraNodeUsernameLabel;
        private System.Windows.Forms.Label CassandraKeyspaceLabel;
        private System.Windows.Forms.GroupBox CassandraTableConfigurationGroupBox;
        private System.Windows.Forms.Button OpenConfigurationFileButton;
        private System.Windows.Forms.TextBox ConfigurationFilePathNameTextBox;
        private System.Windows.Forms.Label ConfigurationFileLabel;
        private System.Windows.Forms.ComboBox CassandraTableNameComboBox;
        private System.Windows.Forms.Label CassandraTableNameLabel;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.Button HelpButton;
    }
}