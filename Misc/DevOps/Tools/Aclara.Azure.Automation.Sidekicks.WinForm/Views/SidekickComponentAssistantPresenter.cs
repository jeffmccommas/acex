﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Component assistant presenter.
    /// </summary>
    public class SidekickComponentAssistantPresenter
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private IComponentAssistantView _componentAssistantView;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Component assistant view.
        /// </summary>
        public IComponentAssistantView ComponentManagerView
        {
            get { return _componentAssistantView; }
            set { _componentAssistantView = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Overriden constructor.
        /// </summary>
        public SidekickComponentAssistantPresenter(IComponentAssistantView componentAssistantView)
        {
            _componentAssistantView = componentAssistantView;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {
        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion


    }
}
