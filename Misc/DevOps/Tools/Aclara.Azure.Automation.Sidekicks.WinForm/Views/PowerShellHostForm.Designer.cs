﻿namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    partial class PowerShellHostForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.GuidanceGroupBox = new System.Windows.Forms.GroupBox();
            this.SidekickActivationStatusPanel = new System.Windows.Forms.Panel();
            this.GuidanceLabel = new System.Windows.Forms.Label();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.PowerShellGroupBox = new System.Windows.Forms.GroupBox();
            this.PowerShellOutputTextBox = new System.Windows.Forms.TextBox();
            this.PowerShellScriptLabel = new System.Windows.Forms.Label();
            this.PowerShellScriptTextBox = new System.Windows.Forms.TextBox();
            this.PowerShellOutputLabel = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.PowerShellHostToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.GuidancePanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.HelpButton = new System.Windows.Forms.Button();
            this.GuidanceGroupBox.SuspendLayout();
            this.PropertiesPanel.SuspendLayout();
            this.PowerShellGroupBox.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.GuidancePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(0, 1);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(719, 17);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "PowerShell Host";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GuidanceGroupBox
            // 
            this.GuidanceGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GuidanceGroupBox.Controls.Add(this.GuidancePanel);
            this.GuidanceGroupBox.Controls.Add(this.SidekickActivationStatusPanel);
            this.GuidanceGroupBox.Controls.Add(this.GuidanceLabel);
            this.GuidanceGroupBox.Location = new System.Drawing.Point(6, 28);
            this.GuidanceGroupBox.Name = "GuidanceGroupBox";
            this.GuidanceGroupBox.Size = new System.Drawing.Size(702, 68);
            this.GuidanceGroupBox.TabIndex = 0;
            this.GuidanceGroupBox.TabStop = false;
            this.GuidanceGroupBox.Text = "Guidance";
            // 
            // SidekickActivationStatusPanel
            // 
            this.SidekickActivationStatusPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SidekickActivationStatusPanel.BackColor = System.Drawing.Color.DarkGray;
            this.SidekickActivationStatusPanel.Location = new System.Drawing.Point(9, 35);
            this.SidekickActivationStatusPanel.Name = "SidekickActivationStatusPanel";
            this.SidekickActivationStatusPanel.Size = new System.Drawing.Size(687, 27);
            this.SidekickActivationStatusPanel.TabIndex = 4;
            // 
            // GuidanceLabel
            // 
            this.GuidanceLabel.AutoSize = true;
            this.GuidanceLabel.Location = new System.Drawing.Point(6, 19);
            this.GuidanceLabel.Name = "GuidanceLabel";
            this.GuidanceLabel.Size = new System.Drawing.Size(0, 13);
            this.GuidanceLabel.TabIndex = 0;
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PropertiesPanel.Controls.Add(this.PowerShellGroupBox);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Controls.Add(this.GuidanceGroupBox);
            this.PropertiesPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesPanel.MinimumSize = new System.Drawing.Size(560, 480);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(716, 506);
            this.PropertiesPanel.TabIndex = 3;
            // 
            // PowerShellGroupBox
            // 
            this.PowerShellGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PowerShellGroupBox.Controls.Add(this.PowerShellOutputTextBox);
            this.PowerShellGroupBox.Controls.Add(this.PowerShellScriptLabel);
            this.PowerShellGroupBox.Controls.Add(this.PowerShellScriptTextBox);
            this.PowerShellGroupBox.Controls.Add(this.PowerShellOutputLabel);
            this.PowerShellGroupBox.Location = new System.Drawing.Point(6, 102);
            this.PowerShellGroupBox.Name = "PowerShellGroupBox";
            this.PowerShellGroupBox.Size = new System.Drawing.Size(702, 358);
            this.PowerShellGroupBox.TabIndex = 1;
            this.PowerShellGroupBox.TabStop = false;
            this.PowerShellGroupBox.Text = "PowerShell";
            // 
            // PowerShellOutputTextBox
            // 
            this.PowerShellOutputTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PowerShellOutputTextBox.Location = new System.Drawing.Point(47, 133);
            this.PowerShellOutputTextBox.Multiline = true;
            this.PowerShellOutputTextBox.Name = "PowerShellOutputTextBox";
            this.PowerShellOutputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.PowerShellOutputTextBox.Size = new System.Drawing.Size(646, 219);
            this.PowerShellOutputTextBox.TabIndex = 3;
            // 
            // PowerShellScriptLabel
            // 
            this.PowerShellScriptLabel.AutoSize = true;
            this.PowerShellScriptLabel.Location = new System.Drawing.Point(6, 19);
            this.PowerShellScriptLabel.Name = "PowerShellScriptLabel";
            this.PowerShellScriptLabel.Size = new System.Drawing.Size(37, 13);
            this.PowerShellScriptLabel.TabIndex = 0;
            this.PowerShellScriptLabel.Text = "Script:";
            // 
            // PowerShellScriptTextBox
            // 
            this.PowerShellScriptTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PowerShellScriptTextBox.Location = new System.Drawing.Point(47, 16);
            this.PowerShellScriptTextBox.Multiline = true;
            this.PowerShellScriptTextBox.Name = "PowerShellScriptTextBox";
            this.PowerShellScriptTextBox.Size = new System.Drawing.Size(649, 108);
            this.PowerShellScriptTextBox.TabIndex = 1;
            this.PowerShellScriptTextBox.Text = "$s1 = \'test1\'; $s2 = \'test2\'; $s1; write-verbose \'<test> verbose\'; write-error \'<" +
    "test> error\';  write-warning \'<test> warning\'; write-debug \'<test> debug\';  star" +
    "t-sleep -s 7; $s2";
            // 
            // PowerShellOutputLabel
            // 
            this.PowerShellOutputLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PowerShellOutputLabel.AutoSize = true;
            this.PowerShellOutputLabel.Location = new System.Drawing.Point(6, 133);
            this.PowerShellOutputLabel.Name = "PowerShellOutputLabel";
            this.PowerShellOutputLabel.Size = new System.Drawing.Size(42, 13);
            this.PowerShellOutputLabel.TabIndex = 2;
            this.PowerShellOutputLabel.Text = "Output:";
            // 
            // ControlPanel
            // 
            this.ControlPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Controls.Add(this.ApplyButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 466);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(716, 40);
            this.ControlPanel.TabIndex = 2;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(635, 11);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 1;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(716, 22);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.Azure.Automation.Sidekicks.Help.chm";
            // 
            // GuidancePanel
            // 
            this.GuidancePanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GuidancePanel.Controls.Add(this.HelpButton);
            this.GuidancePanel.Controls.Add(this.label1);
            this.GuidancePanel.Location = new System.Drawing.Point(3, 14);
            this.GuidancePanel.Name = "GuidancePanel";
            this.GuidancePanel.Size = new System.Drawing.Size(693, 48);
            this.GuidancePanel.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(514, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Azure subscription. Check Azure cloud service(s) and click Refresh to retr" +
    "ieve live cloud service info.";
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(665, 0);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 2;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // PowerShellHostForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 506);
            this.Controls.Add(this.PropertiesPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "PowerShellHostForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "PowerShellHostForm";
            this.GuidanceGroupBox.ResumeLayout(false);
            this.GuidanceGroupBox.PerformLayout();
            this.PropertiesPanel.ResumeLayout(false);
            this.PowerShellGroupBox.ResumeLayout(false);
            this.PowerShellGroupBox.PerformLayout();
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.GuidancePanel.ResumeLayout(false);
            this.GuidancePanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.GroupBox GuidanceGroupBox;
        private System.Windows.Forms.Label GuidanceLabel;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.GroupBox PowerShellGroupBox;
        private System.Windows.Forms.Label PowerShellScriptLabel;
        private System.Windows.Forms.TextBox PowerShellScriptTextBox;
        private System.Windows.Forms.Label PowerShellOutputLabel;
        private System.Windows.Forms.ToolTip PowerShellHostToolTip;
        private System.Windows.Forms.Panel SidekickActivationStatusPanel;
        private System.Windows.Forms.TextBox PowerShellOutputTextBox;
        private System.Windows.Forms.Panel GuidancePanel;
        private System.Windows.Forms.Button HelpButton;
        private System.Windows.Forms.Label label1;
    }
}