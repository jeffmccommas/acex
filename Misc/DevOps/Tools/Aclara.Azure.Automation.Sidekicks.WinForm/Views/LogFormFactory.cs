﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aclara.Azure.Automation.Sidekicks.WinForm.Views;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Log form factory.
    /// </summary>
    public class LogFormFactory
    {

        #region Private Constants
        #endregion

        #region Public Methods

        /// <summary>
        /// Create form.
        /// </summary>
        /// <returns></returns>
        public static LogForm CreateForm(SidekickContainerForm sidekickContainerForm)
        {

            LogForm result = null;
            LogPresenter logPresenter = null;

            try
            {
                result = new LogForm(sidekickContainerForm);

                logPresenter = new LogPresenter(result);

                result.LogPresenter = logPresenter;
                result.TopLevel = false;

                logPresenter.Initialize();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

    }
}
