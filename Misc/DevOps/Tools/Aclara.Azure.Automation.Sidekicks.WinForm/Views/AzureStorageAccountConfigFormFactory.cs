﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public class AzureStorageAccountConfigFormFactory
    {
        /// <summary>
        /// Create form.
        /// </summary>
        /// <returns></returns>
        public static AzureStorageAccountConfigForm CreateForm(SidekickConfiguration sidekickConfiguration)
        {

            AzureStorageAccountConfigForm result = null;
            AzureStorageAccountConfigPresenter AzureStorageAccountConfigPresenter = null;

            try
            {
                result = new AzureStorageAccountConfigForm(sidekickConfiguration);
                AzureStorageAccountConfigPresenter = new AzureStorageAccountConfigPresenter(sidekickConfiguration, result);
                result.AzureStorageAccountConfigPresenter = AzureStorageAccountConfigPresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
