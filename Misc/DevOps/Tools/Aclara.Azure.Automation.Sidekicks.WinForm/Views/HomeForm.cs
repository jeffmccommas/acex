﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Models;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public partial class HomeForm : Form, ISidekickView, IHomeView, ISidekickInfo
    {

        #region Private Constants

        private const string SidekickInfo_SidekickName = "Home";
        private const string SidekickInfo_SidekickDescription = "Home";
        private const string StartPage_FileName = "startpage.html";

        #endregion

        #region Private Data Members

        private HomePresenter _HomePresenter;
        private SidekickContainerForm _sidekickContainerForm;
        private SidekickConfiguration _sidekickConfiguration = null;

        #endregion


        #region Public Properties

        /// <summary>
        /// Property: Home presenter.
        /// </summary>
        public HomePresenter HomePresenter
        {
            get { return _HomePresenter; }
            set { _HomePresenter = value; }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickInfo_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickInfo_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick container form.
        /// </summary>
        public SidekickContainerForm SidekickContainerForm
        {
            get { return _sidekickContainerForm; }
            set { _sidekickContainerForm = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <param></param>
        public HomeForm(SidekickContainerForm sidekickContainerForm, SidekickConfiguration sidekickConfiguration)
        {
            _sidekickConfiguration = sidekickConfiguration;
            _sidekickContainerForm = sidekickContainerForm;

            InitializeComponent();

            this.InitializeControls();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Sidekick was activated.
        /// </summary>
        public void SidekickActivated()
        {
            try
            {
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {

            string urlAsText = string.Empty;

            try
            {
                //Populate sidekick view list in UI components.
                PopulateUIComponentsWithSidekickViewListV2(this.SidekickContainerForm.SidekickViewList);

                urlAsText = Path.Combine(this.SidekickConfiguration.Documents.Document.Where(doc => doc.FileName == StartPage_FileName).FirstOrDefault().Path,
                                         this.SidekickConfiguration.Documents.Document.Where(doc => doc.FileName == StartPage_FileName).FirstOrDefault().FileName);
                if (string.IsNullOrEmpty(urlAsText) == false &&
                    File.Exists(urlAsText) == true)
                {
                    this.StartPageWebBrowser.Url = new Uri(urlAsText);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Populate UI components with sidekick view list.
        /// </summary>
        protected void PopulateUIComponentsWithSidekickViewListV2(SidekickViewList sidekickViewList)
        {
            Button parentButton = null;
            Button childButton = null;

            try
            {

                var categories = sidekickViewList.OrderBy(skv => skv.SidekickCategory.Description).Select(skv => skv.SidekickCategory);

                foreach (var category in categories.DistinctBy(c => c.Description))
                {
                    parentButton = new Button();
                    parentButton.Width = this.NavigationFlowLayoutPanel.Width - 50;
                    parentButton.Height = 30;
                    parentButton.FlatStyle = FlatStyle.Flat;
                    parentButton.FlatAppearance.BorderSize = 1;
                    parentButton.FlatAppearance.BorderColor = Color.LightGray;
                    parentButton.BackColor = Color.LightGray;
                    parentButton.TextAlign = ContentAlignment.BottomLeft;
                    parentButton.Dock = DockStyle.Left;
                    parentButton.Name = category.Name;
                    parentButton.Text = category.Description;
                    parentButton.TextAlign = ContentAlignment.MiddleLeft;
                    this.NavigationFlowLayoutPanel.Controls.Add(parentButton);

                    foreach (var sidekickView in sidekickViewList.Where(skv => skv.SidekickCategory.Name == category.Name).OrderBy(skv => skv.SidekickCategory))
                    {
                        childButton = new Button();
                        childButton.Width = parentButton.Width - 50;
                        childButton.Height = parentButton.Height;
                        childButton.FlatStyle = FlatStyle.Flat;
                        childButton.FlatAppearance.BorderSize = 1;
                        childButton.FlatAppearance.BorderColor = Color.LightSteelBlue;
                        childButton.TextAlign = ContentAlignment.BottomLeft;
                        childButton.Dock = DockStyle.Right;
                        childButton.Name = sidekickView.Name;
                        childButton.Text = sidekickView.Description;
                        childButton.Click += new EventHandler(ViewSidekicksSidekickViewButton_Click);
                        this.NavigationFlowLayoutPanel.Controls.Add(childButton);
                    }
                }

            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }
        #endregion

        #region Private Methods

        ///// <summary>
        ///// Event Handler: View sidekicks - sidekick view menu item clicked.
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void ViewSidekicksSidekickViewToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    ToolStripMenuItem menuItem = null;

        //    try
        //    {
        //        menuItem = (ToolStripMenuItem)sender;
        //        var sidekickView = this.SidekickContainerForm.SidekickViewList.Single(skv => skv.Description == menuItem.Text);
        //        if (sidekickView == null)
        //        {
        //            return;
        //        }
        //        this.SidekickContainerForm.ActivateSidekickView(sidekickView);

        //    }
        //    catch (Exception ex)
        //    {
        //        string context = string.Format("TBD");
        //        ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex, context);
        //        exceptionForm.ShowDialog();
        //    }
        //}

        /// <summary>
        /// Event Handler: View sidekicks - sidekick view menu item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewSidekicksSidekickViewButton_Click(object sender, EventArgs e)
        {
            Button menuItem = null;

            try
            {
                menuItem = (Button)sender;
                var sidekickView = this.SidekickContainerForm.SidekickViewList.Single(skv => skv.Description == menuItem.Text);
                if (sidekickView == null)
                {
                    return;
                }
                this.SidekickContainerForm.ActivateSidekickView(sidekickView);

            }
            catch (Exception ex)
            {
                string context = string.Format("TBD");
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex, context);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickHome.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion
    }
}
