﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Events;
using Aclara.Azure.Automation.Sidekicks.WinForm.Logging;
using Aclara.Azure.Automation.Sidekicks.WinForm.Models;
using Aclara.Azure.Automation.Sidekicks.WinForm.Utilities;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public partial class SidekickComponentAssistantForm : Form, IComponentAssistantView
    {
        #region Private Constants

        private const string SidekickComponentListDataGridViewColumn_Selected = "Selected";
        private const string SidekickComponentListDataGridViewColumn_Name = "Name";
        private const string SidekickComponentListDataGridViewColumn_Description = "Description";
        private const string SidekickComponentListDataGridViewColumn_Minimumversion = "MinimumVersion";
        private const string SidekickComponentListDataGridViewColumn_Provider = "Provider";
        private const string SidekickComponentListDataGridViewColumn_HelpTopic = "Help";
        private const string SidekickComponentListDataGridViewColumn_MoreInfo = "MoreInfo";
        private const string SidekickComponentListDataGridViewColumn_Required = "RequiredComponent";
        private const string SidekickComponentListDataGridViewColumn_LastChecked = "LastChecked";
        private const string SidekickComponentListDataGridViewColumn_LoginRequired = "RequiredLogin";
        private const string SidekickComponentListDataGridViewColumn_UserActionPrompt = "UserActionPrompt";
        private const string SidekickComponentListDataGridViewColumn_Extra = "Extra";

        private const string ValidationErrorMessage_NoComponentsSelected = "At least one component must be selected.";

        private const string Status_Default = "Click Check Components to verify minimum version of required components are already installed.";
        private const string Status_ValidateSelectedComponents = "Validating sidekicks components. Please wait...";
        private const string Status_NoUserActionRequired = "Validation completed. No user action required.";
        private const string Status_UserActionRequired_InstallorUpgrade = "One or more components require user action.\r\n1. In the component list(below), click on Install or Update to initiate component install or upgrade.\r\n2. Click Validate again once component install or upgrade is complete.";

        private const string Prompt_CheckComponents_Caption = "Sidekick Component Assistant";
        private const string Prompt_CheckComponents_Message = "Check components for minimum installed version?";

        #endregion

        #region Protected Data Types

        protected enum StatusSize
        {
            Unspecified = 0,
            Small,
            Large
        }

        #endregion

        #region Private Data Members

        private CustomLogger _logger;
        private SidekickComponentAssistantPresenter _sidekickComponentAssistantPresenter;
        private SidekickConfiguration _sidekickConfiguration;
        private SidekickComponentManager _sidekickComponentManager;
        private SidekickComponentList _sidekickComponentList;
        private ISidekickInfo _sidekickInfo;
        private bool _firstTimeComponentAssistantShown;
        private DataTableLayoutPanel _sidekickComponentListDataTableLayoutPanel;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.SidekickName,
                                               this.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Component assistant presenter.
        /// </summary>
        public SidekickComponentAssistantPresenter SidekickComponentAssistantPresenter
        {
            get { return _sidekickComponentAssistantPresenter; }
            set { _sidekickComponentAssistantPresenter = value; }
        }

        /// <summary>
        /// Properity: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: SidekickComponentManager.
        /// </summary>
        public SidekickComponentManager SidekickComponentManager
        {
            get { return _sidekickComponentManager; }
            set { _sidekickComponentManager = value; }
        }

        /// <summary>
        /// Property: Sidekick info.
        /// </summary>
        public ISidekickInfo SidekickInfo
        {
            get { return _sidekickInfo; }
            set { _sidekickInfo = value; }
        }

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get { return _sidekickInfo.SidekickName; }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get { return _sidekickInfo.SidekickDescription; }
        }

        /// <summary>
        /// Property: Sidekick component list.
        /// </summary>
        public SidekickComponentList SidekickComponentList
        {
            get { return _sidekickComponentList; }
            set { _sidekickComponentList = value; }
        }

        /// <summary>
        /// Property: First time component assistant shown.
        /// </summary>
        public bool FirstTimeComponentAssistantShown
        {
            get { return _firstTimeComponentAssistantShown; }
            set { _firstTimeComponentAssistantShown = value; }
        }

        /// <summary>
        /// Property: Sidekick component list data table layout pannel.
        /// </summary>
        public DataTableLayoutPanel SidekickComponentListDataTableLayoutPanel
        {
            get { return _sidekickComponentListDataTableLayoutPanel; }
            set { _sidekickComponentListDataTableLayoutPanel = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekickComponentAssistantForm(SidekickConfiguration sidekickConfiguration,
                                              SidekickComponentManager sidekickComponentManager)
        {
            _sidekickConfiguration = sidekickConfiguration;
            _sidekickComponentList = sidekickComponentManager.SidekickComponentList;
            _sidekickComponentManager = sidekickComponentManager;
            _firstTimeComponentAssistantShown = true;
            _sidekickComponentListDataTableLayoutPanel = new DataTableLayoutPanel();
            InitializeComponent();

            this.InitializeControls();
        }

        #endregion

        #region Protected Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private SidekickComponentAssistantForm()
        {
            InitializeComponent();

            this.InitializeControls();
        }

        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {
                this.UpdateStatus(Status_Default);

                this.SidekickComponentListDataTableLayoutPanel.SidekickConponentUserActionCompleted += OnSidekickConponentUserActionCompleted;

            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Update sidekick activation status.
        /// </summary>
        /// <param name="message"></param>
        protected void UpdateStatus(string message)
        {
            try
            {
                if (message.Length > 120)
                {
                    this.SetStatusSize(StatusSize.Large);
                }
                else
                {
                    this.SetStatusSize(StatusSize.Small);
                }
                this.StatusLabel.Text = message;
                this.StatusLabel.Invalidate();
                this.StatusLabel.Update();
                this.StatusLabel.Refresh();

                Application.DoEvents();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Set status size.
        /// </summary>
        /// <param name="statusSize"></param>
        protected void SetStatusSize(StatusSize statusSize)
        {

            try
            {
                switch (statusSize)
                {
                    case StatusSize.Unspecified:
                        break;
                    case StatusSize.Small:
                        this.ComponentAssistantSplitContainer.SplitterDistance = (int)(ComponentAssistantSplitContainer.ClientSize.Height * 0.08);
                        break;
                    case StatusSize.Large:
                        this.ComponentAssistantSplitContainer.SplitterDistance = (int)(ComponentAssistantSplitContainer.ClientSize.Height * 0.18);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Check sidekick components.
        /// </summary>
        protected void CheckSidekickComponents()
        {

            List<SidekickComponentId> sidekickComponentIdList = null;

            try
            {
                this.CheckComponentsButton.Enabled = false;

                this.Cursor = Cursors.WaitCursor;

                this.SidekickComponentListDataTableLayoutPanel.Reset();

                sidekickComponentIdList = this.SidekickComponentManager.SidekickComponentList.Select(sc => sc.SidekickComponentId).ToList();

                this.UpdateStatus(Status_ValidateSelectedComponents);

                this.SidekickComponentManager.ValidateSidekickComponents(sidekickComponentIdList);

                if (this.SidekickComponentManager.SidekickComponentList.Any(sc => sc.UserAction == UserAction.Install ||
                                                                                  sc.UserAction == UserAction.Upgrade))
                {
                    UpdateStatus(Status_UserActionRequired_InstallorUpgrade);
                }
                else
                {
                    UpdateStatus(Status_NoUserActionRequired);
                }

                this.SidekickComponentListDataTableLayoutPanel.Populate(this.SidekickComponentListPanel,
                                                                        this.SidekickComponentList,
                                                                        this.SidekickComponentManager,
                                                                        this.SidekickHelpProvider,
                                                                        this.SidekickToolTip);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.CheckComponentsButton.Enabled = true;
                this.Cursor = Cursors.Default;
            }

        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Sidekick component user action completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSidekickConponentUserActionCompleted(object sender, SidekickConponentUserActionCompletedEventArgs e)
        {

            try
            {
                this.CheckSidekickComponents();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Sidekick component assistant form - Shown.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SidekickComponentAssistantForm_Shown(object sender, EventArgs e)
        {
            SidekickComponent sidekickComponent = null;
            TimeSpan timeSinceLastComponentCheck;
            DialogResult dialogResult = DialogResult.No;

            try
            {

                sidekickComponent = this.SidekickComponentList.OrderBy(x => x.LastChecked).First();

                timeSinceLastComponentCheck = (sidekickComponent.LastChecked - DateTime.Now).Duration();

                if (timeSinceLastComponentCheck.TotalMinutes > 180)
                {
                    dialogResult = MessageBox.Show(this, Prompt_CheckComponents_Message, Prompt_CheckComponents_Caption, MessageBoxButtons.YesNoCancel);
                    if (dialogResult == DialogResult.Yes)
                    {
                        this.CheckComponentsButton.Enabled = false;

                        this.CheckSidekickComponents();
                    }
                }

                this.SidekickComponentListDataTableLayoutPanel.Populate(this.SidekickComponentListPanel,
                                                                        this.SidekickComponentList,
                                                                        this.SidekickComponentManager,
                                                                        this.SidekickHelpProvider,
                                                                        this.SidekickToolTip);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Check components button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckComponentsButton_Click(object sender, EventArgs e)
        {

            try
            {
                this.CheckSidekickComponents();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.CheckComponentsButton.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Sidekick component assistant form - Form closing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SidekickComponentAssistantForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    e.Cancel = true;
                    Hide();
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "CIG_ComponentsInstallGuide.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion
    }

    /// <summary>
    /// Data table layout panel.
    /// </summary>
    public class DataTableLayoutPanel
    {
        #region Private Constants

        private const string Tooltip_HelpButton = "Show help for sidekick component.";
        private const string Tooltip_StatusPictureBox_RequiredVersionInstalled = "Sidekick component version required is installed.";
        private const string Tooltip_StatusPictureBox_UpgradeRequired = "Sidekick component upgrade is required.";
        private const string Tooltip_StatusPictureBox_InstallationRequired = "Sidekick component installation is required.";
        private const string Tooltip_ActionButton_NoActionNeeded = "No user action needed.";
        private const string Tooltip_ActionButton_UpgradeRequired = "Upgrade sidekick component.";
        private const string Tooltip_ActionButton_InstallationRequired = "Install sidekick component.";

        private const string ValidationErrorMessage_SidekickComponentNotFound = "Sidekick component not found.";

        #endregion

        #region Public Events

        public event EventHandler<SidekickConponentUserActionCompletedEventArgs> SidekickConponentUserActionCompleted;

        #endregion

        #region Private Data Members

        private System.Windows.Forms.Panel _containerPanel;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel;
        private SidekickComponentList _sidekickComponentList;
        private HelpProvider _helpProvider;
        private ToolTip _sidekickToolTip;
        private SidekickComponentManager _sidekickComponentManager;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Container panel.
        /// </summary>
        public System.Windows.Forms.Panel ContainerPanel
        {
            get { return _containerPanel; }
            set { _containerPanel = value; }
        }

        /// <summary>
        /// Property: Table layout panel.
        /// </summary>
        public System.Windows.Forms.TableLayoutPanel TableLayoutPanel
        {
            get { return _tableLayoutPanel; }
            set { _tableLayoutPanel = value; }
        }

        /// <summary>
        /// Property: Sidekick component list.
        /// </summary>
        public SidekickComponentList SidekickComponentList
        {
            get { return _sidekickComponentList; }
            set { _sidekickComponentList = value; }
        }

        /// <summary>
        /// Property: Help provider.
        /// </summary>
        public HelpProvider HelpProvider
        {
            get { return _helpProvider; }
            set { _helpProvider = value; }
        }

        /// <summary>
        /// Property: Sidekick tooltip.
        /// </summary>
        public ToolTip SidekickToolTip
        {
            get { return _sidekickToolTip; }
            set { _sidekickToolTip = value; }
        }

        /// <summary>
        /// Property: SidekickComponentManager.
        /// </summary>
        public SidekickComponentManager SidekickComponentManager
        {
            get { return _sidekickComponentManager; }
            set { _sidekickComponentManager = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DataTableLayoutPanel()
        {

        }

        #endregion

        #region Protected Constructors

        #endregion

        #region Public Methods

        /// <summary>
        /// Remove all controls from table layout panel.
        /// </summary>
        public void Reset()
        {

            try
            {
                if (this.TableLayoutPanel != null)
                {
                    this.TableLayoutPanel.Controls.Clear();
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Populate.
        /// </summary>
        /// <param name="containerPanel"></param>
        /// <param name="sidekickComponentList"></param>
        /// <param name="helpNamespace"></param>
        public void Populate(System.Windows.Forms.Panel containerPanel,
                            SidekickComponentList sidekickComponentList,
                            SidekickComponentManager sidekickComponentManager,
                            HelpProvider helpProvider,
                            ToolTip sidekickTooltip)
        {
            int row = 0;
            System.Windows.Forms.Panel componentInfoPanel = null;
            System.Windows.Forms.Panel extraPanel = null;

            try
            {
                _containerPanel = containerPanel;
                _sidekickComponentList = sidekickComponentList;
                _sidekickComponentManager = sidekickComponentManager;
                _helpProvider = helpProvider;
                _sidekickToolTip = sidekickTooltip;

                if (this.ContainerPanel == null)
                {
                    return;
                }

                if (this.ContainerPanel.Controls.OfType<Panel>().ToList().Count == 0)
                {
                    _tableLayoutPanel = new TableLayoutPanel();
                    this.ContainerPanel.Controls.Add(this.TableLayoutPanel);
                    this.TableLayoutPanel.Size = new Size(this.ContainerPanel.Size.Width, this.ContainerPanel.Size.Height);
                    this.TableLayoutPanel.Location = new Point(1, 1);
                    this.TableLayoutPanel.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right);
                    this.TableLayoutPanel.AutoScroll = false;
                    this.TableLayoutPanel.Dock = DockStyle.Top;
                    this.TableLayoutPanel.AutoSize = true;
                    this.TableLayoutPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                }

                this.TableLayoutPanel.SuspendLayout();
                this.TableLayoutPanel.Controls.Clear();
                this.TableLayoutPanel.RowStyles.Clear();
                this.TableLayoutPanel.RowCount = 0;

                this.TableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 90F));

                foreach (SidekickComponent sidekickComponent in this.SidekickComponentList)
                {
                    this.TableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.AutoSize, 350F));
                    componentInfoPanel = this.CreateComponentInfoPanel(sidekickComponent, row);
                    this.TableLayoutPanel.Controls.Add(componentInfoPanel, 0, row);
                    componentInfoPanel.Dock = DockStyle.Fill;
                    row++;
                }

                this.TableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.AutoSize, 50F));
                extraPanel = new Panel();
                extraPanel.Location = new Point(1, 1);
                extraPanel.Size = new System.Drawing.Size(100, 50); ;
                this.TableLayoutPanel.Controls.Add(extraPanel, 0, row);

                this.TableLayoutPanel.ResumeLayout(false);
                this.TableLayoutPanel.PerformLayout();

                this.ContainerPanel.Refresh();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create component information panel.
        /// </summary>
        /// <param name="sidekickComponent"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        protected Panel CreateComponentInfoPanel(SidekickComponent sidekickComponent, int row)
        {
            System.Windows.Forms.Panel result = null;
            System.Windows.Forms.Button helpButton;
            System.Windows.Forms.PictureBox statusPictureBox;
            System.Windows.Forms.Label nameLabel;
            System.Windows.Forms.Label versionRequiredLabel;
            System.Windows.Forms.Label versionFoundLabel;
            System.Windows.Forms.Label userActionLabel;
            System.Windows.Forms.Button actionButton;
            System.Windows.Forms.Label componentRequiredLabel;
            System.Windows.Forms.Label loginRequiredLabel;

            try
            {

                result = new System.Windows.Forms.Panel();

                helpButton = new System.Windows.Forms.Button();
                statusPictureBox = new System.Windows.Forms.PictureBox();
                nameLabel = new System.Windows.Forms.Label();
                versionRequiredLabel = new System.Windows.Forms.Label();
                versionFoundLabel = new System.Windows.Forms.Label();
                userActionLabel = new System.Windows.Forms.Label();
                actionButton = new System.Windows.Forms.Button();
                componentRequiredLabel = new System.Windows.Forms.Label();
                loginRequiredLabel = new System.Windows.Forms.Label();

                // 
                // result
                // 

                //result.Dock = System.Windows.Forms.DockStyle.Fill;
                result.Location = new System.Drawing.Point(1, 1);
                result.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right);
                result.Name = "componentInfoPanel" + row;
                result.Size = new System.Drawing.Size(818, 68);
                result.TabIndex = 2;
                result.BorderStyle = BorderStyle.FixedSingle;
                result.BackColor = SystemColors.Control;

                result.Controls.Add(helpButton);
                result.Controls.Add(statusPictureBox);
                result.Controls.Add(nameLabel);
                result.Controls.Add(versionFoundLabel);
                result.Controls.Add(versionRequiredLabel);
                result.Controls.Add(actionButton);
                result.Controls.Add(userActionLabel);
                result.Controls.Add(componentRequiredLabel);
                result.Controls.Add(loginRequiredLabel);

                // 
                // StatusPictureBox
                // 
                if (sidekickComponent.UserAction == UserAction.Install)
                {
                    statusPictureBox.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.StatusActionRequired;
                    this.SidekickToolTip.SetToolTip(statusPictureBox, Tooltip_StatusPictureBox_InstallationRequired);
                }
                else if (sidekickComponent.UserAction == UserAction.Upgrade)
                {
                    statusPictureBox.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.StatusActionRequired;
                    this.SidekickToolTip.SetToolTip(statusPictureBox, Tooltip_StatusPictureBox_UpgradeRequired);
                }
                else if (sidekickComponent.UserAction == UserAction.Unspecified)
                {
                    statusPictureBox.Image = null;
                }
                else
                {
                    statusPictureBox.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.StatusCompleted;
                    this.SidekickToolTip.SetToolTip(statusPictureBox, Tooltip_StatusPictureBox_RequiredVersionInstalled);
                }
                statusPictureBox.InitialImage = null;
                statusPictureBox.Location = new System.Drawing.Point(11, 35);
                statusPictureBox.Name = "StatusPictureBox" + row;
                statusPictureBox.Size = new System.Drawing.Size(19, 18);
                statusPictureBox.TabIndex = 1;
                statusPictureBox.TabStop = false;

                // 
                // HelpButton
                // 
                helpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                helpButton.FlatAppearance.BorderColor = SystemColors.Control;
                helpButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
                helpButton.Location = new System.Drawing.Point(4, 5);
                helpButton.Name = "HelpButton" + row;
                helpButton.Size = new System.Drawing.Size(28, 23);
                helpButton.TabIndex = 0;
                helpButton.UseVisualStyleBackColor = true;
                helpButton.Click += new System.EventHandler(this.HelpButton_Click);
                helpButton.Tag = sidekickComponent;
                this.SidekickToolTip.SetToolTip(helpButton, Tooltip_HelpButton);

                //
                // NameLabel
                // 
                nameLabel.AutoSize = true;
                nameLabel.Location = new System.Drawing.Point(38, 10);
                nameLabel.Name = "NameLabel" + row;
                nameLabel.Size = new System.Drawing.Size(104, 13);
                nameLabel.TabIndex = 0;
                nameLabel.Text = sidekickComponent.Name;

                // 
                // VersionRequiredLabel
                // 
                versionRequiredLabel.AutoSize = true;
                versionRequiredLabel.Location = new System.Drawing.Point(345, 10);
                versionRequiredLabel.Name = "versionRequiredLabel" + row;
                versionRequiredLabel.Size = new System.Drawing.Size(140, 13);
                versionRequiredLabel.TabIndex = 2;
                versionRequiredLabel.Text = string.Format("Version required: {0}",
                                                          sidekickComponent.MinimumVersionAsText);
                // 
                // VersionFoundLabel
                // 
                versionFoundLabel.AutoSize = true;
                versionFoundLabel.Location = new System.Drawing.Point(345, 35);
                versionFoundLabel.Name = "versionFoundLabel" + row;
                versionFoundLabel.Size = new System.Drawing.Size(129, 13);
                versionFoundLabel.TabIndex = 4;
                versionFoundLabel.Text = string.Format("Version found: {0}",
                                                       sidekickComponent.VersionAsText);

                // 
                // UserActionLabel
                // 
                userActionLabel.AutoSize = true;
                userActionLabel.Location = new System.Drawing.Point(38, 38);
                userActionLabel.Name = "UserActionLabel";
                userActionLabel.Size = new System.Drawing.Size(35, 13);
                userActionLabel.TabIndex = 5;
                userActionLabel.Text = string.Format("User action:");

                // 
                // ActionButton
                // 
                actionButton.Location = new System.Drawing.Point(108, 33);
                actionButton.Name = "ActionButton";
                actionButton.Size = new System.Drawing.Size(75, 23);
                actionButton.TabIndex = 6;
                actionButton.Text = string.Format("{0}",
                                                  sidekickComponent.UserActionPrompt);
                actionButton.UseVisualStyleBackColor = true;
                actionButton.Click += new System.EventHandler(this.ActionButton_Click);
                actionButton.Tag = sidekickComponent;
                if (sidekickComponent.UserAction == UserAction.Install)
                {
                    actionButton.Enabled = true;
                    this.SidekickToolTip.SetToolTip(actionButton, Tooltip_ActionButton_InstallationRequired);
                }
                else if (sidekickComponent.UserAction == UserAction.Upgrade)
                {
                    actionButton.Enabled = true;
                    this.SidekickToolTip.SetToolTip(actionButton, Tooltip_ActionButton_UpgradeRequired);
                }
                else
                {
                    actionButton.Enabled = false;
                    this.SidekickToolTip.SetToolTip(actionButton, Tooltip_ActionButton_NoActionNeeded);
                }

                // 
                // ComponentRequiredLabel
                // 
                componentRequiredLabel.AutoSize = true;
                componentRequiredLabel.Location = new System.Drawing.Point(212, 11);
                componentRequiredLabel.Name = "ComponentRequiredLabel" + row;
                componentRequiredLabel.Size = new System.Drawing.Size(35, 13);
                componentRequiredLabel.TabIndex = 7;
                if (sidekickComponent.Required == true)
                {
                    componentRequiredLabel.Text = "Component required: Yes";
                }
                else
                {
                    componentRequiredLabel.Text = "Component required: No";
                }

                // 
                // LoginRequiredLabel
                // 
                loginRequiredLabel.AutoSize = true;
                loginRequiredLabel.Location = new System.Drawing.Point(212, 35);
                loginRequiredLabel.Name = "LoginRequiredLabel" + row;
                loginRequiredLabel.Size = new System.Drawing.Size(35, 13);
                loginRequiredLabel.TabIndex = 8;
                if (sidekickComponent.LoginRequired == true)
                {
                    loginRequiredLabel.Text = "Login required: Yes";
                }
                else
                {
                    loginRequiredLabel.Text = "Login required: No";
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            SidekickComponent sidekickComponent = null;
            System.Windows.Forms.Button button = null;

            try
            {
                if (sender.GetType() == typeof(Button))
                {
                    button = (Button)sender;
                    if (button.Tag.GetType().BaseType == typeof(SidekickComponent))
                    {
                        sidekickComponent = (SidekickComponent)button.Tag;
                    }
                }

                Help.ShowHelp(button, this.HelpProvider.HelpNamespace, sidekickComponent.HelpKeyword);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ActionButton_Click(object sender, EventArgs e)
        {
            SidekickComponent sidekickComponent = null;
            System.Windows.Forms.Button button = null;
            SidekickConponentUserActionCompletedEventArgs sidekickConponentUserActionCompletedEventArgs = null;
            try
            {

                if (sender.GetType() == typeof(Button))
                {
                    ContainerPanel.Cursor = Cursors.WaitCursor;

                    button = (Button)sender;
                    if (button.Tag.GetType().BaseType == typeof(SidekickComponent))
                    {
                        sidekickComponent = (SidekickComponent)button.Tag;

                        if (sidekickComponent != null)
                        {
                            this.SidekickComponentManager.PerformUserAction(sidekickComponent);
                        }
                        else
                        {
                            MessageBox.Show(string.Format(ValidationErrorMessage_SidekickComponentNotFound));
                        }

                        if (SidekickConponentUserActionCompleted != null)
                        {
                            sidekickConponentUserActionCompletedEventArgs = new SidekickConponentUserActionCompletedEventArgs();
                            this.SidekickConponentUserActionCompleted(this, sidekickConponentUserActionCompletedEventArgs);
                        }

                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                ContainerPanel.Cursor = Cursors.Default;
            }
        }

        #endregion

    }
}
