﻿namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    partial class SidekickContainerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SidekickContainerForm));
            this.SidekickContainerMenuStrip = new System.Windows.Forms.MenuStrip();
            this.FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewSidekicksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewShowLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewOpenLogFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewSidekickComponentAssistantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpShowHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpOpenChangeLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpCheckForNewVersionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpDocumentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpAboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainToolStrip = new System.Windows.Forms.ToolStrip();
            this.HomeToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.BackToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ForwardToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.MainSidekickSelectorToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.MainSidekickSelectorContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MainShowLogToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.MainDocumentsToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.MainMicrosoftAzureToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.MainMicrosoftAzurePortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMicrosoftClassicAzurePortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainShowSidekickComponentAssistantToolStripSplitButton = new System.Windows.Forms.ToolStripButton();
            this.MainHelpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.MainAutoUpdaterToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.MainAutoUpdaterShowChangeLogToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.MainAutoUpdatertoolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.SidekickContainerStatusStrip = new System.Windows.Forms.StatusStrip();
            this.LogPanel = new System.Windows.Forms.Panel();
            this.HorizontalSplitter = new System.Windows.Forms.Splitter();
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.SidekickContainerPanel = new System.Windows.Forms.Panel();
            this.SidekickTimer = new System.Windows.Forms.Timer(this.components);
            this.SidekickToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SidekickViewHistoryContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.HelpSidekicksAutoUpdaterInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SidekickContainerMenuStrip.SuspendLayout();
            this.MainToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // SidekickContainerMenuStrip
            // 
            this.SidekickContainerMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileToolStripMenuItem,
            this.ViewToolStripMenuItem,
            this.HelpToolStripMenuItem});

            this.SidekickContainerMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.SidekickContainerMenuStrip.Name = "SidekickContainerMenuStrip";
            this.SidekickContainerMenuStrip.Size = new System.Drawing.Size(1228, 24);
            this.SidekickContainerMenuStrip.TabIndex = 0;
            this.SidekickContainerMenuStrip.Text = "menuStrip1";
            // 
            // FileToolStripMenuItem
            // 
            this.FileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExitToolStripMenuItem});
            this.FileToolStripMenuItem.Name = "FileToolStripMenuItem";
            this.FileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.FileToolStripMenuItem.Text = "File";
            // 
            // ExitToolStripMenuItem
            // 
            this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
            this.ExitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.ExitToolStripMenuItem.Text = "Exit";
            this.ExitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // ViewToolStripMenuItem
            // 
            this.ViewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ViewSidekicksToolStripMenuItem,
            this.ViewShowLogToolStripMenuItem,
            this.ViewOpenLogFileToolStripMenuItem,
            this.ViewSidekickComponentAssistantToolStripMenuItem});
            this.ViewToolStripMenuItem.Name = "ViewToolStripMenuItem";
            this.ViewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.ViewToolStripMenuItem.Text = "View";
            // 
            // ViewSidekicksToolStripMenuItem
            // 
            this.ViewSidekicksToolStripMenuItem.Name = "ViewSidekicksToolStripMenuItem";
            this.ViewSidekicksToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.ViewSidekicksToolStripMenuItem.Text = "Sidekicks";
            // 
            // ViewShowLogToolStripMenuItem
            // 
            this.ViewShowLogToolStripMenuItem.Checked = true;
            this.ViewShowLogToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ViewShowLogToolStripMenuItem.Name = "ViewShowLogToolStripMenuItem";
            this.ViewShowLogToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.ViewShowLogToolStripMenuItem.Text = "Show Log";
            this.ViewShowLogToolStripMenuItem.Click += new System.EventHandler(this.ViewShowLogToolStripMenuItem_Click);
            // 
            // ViewOpenLogFileToolStripMenuItem
            // 
            this.ViewOpenLogFileToolStripMenuItem.Name = "ViewOpenLogFileToolStripMenuItem";
            this.ViewOpenLogFileToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.ViewOpenLogFileToolStripMenuItem.Text = "Open Log File";
            this.ViewOpenLogFileToolStripMenuItem.Click += new System.EventHandler(this.ViewOpenLogFileToolStripMenuItem_Click);
            // 
            // ViewSidekickComponentAssistantToolStripMenuItem
            // 
            this.ViewSidekickComponentAssistantToolStripMenuItem.Name = "ViewSidekickComponentAssistantToolStripMenuItem";
            this.ViewSidekickComponentAssistantToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.ViewSidekickComponentAssistantToolStripMenuItem.Text = "Show Component Assistant...";
            this.ViewSidekickComponentAssistantToolStripMenuItem.Click += new System.EventHandler(this.ViewSidekickComponentAssistantToolStripMenuItem_Click);
            // 
            // HelpToolStripMenuItem
            // 
            this.HelpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HelpShowHelpToolStripMenuItem,
            this.HelpOpenChangeLogToolStripMenuItem,
            this.HelpCheckForNewVersionToolStripMenuItem,
            this.HelpSidekicksAutoUpdaterInfoToolStripMenuItem,
            this.HelpDocumentsToolStripMenuItem,
            this.HelpAboutToolStripMenuItem});

            this.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem";
            this.HelpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.HelpToolStripMenuItem.Text = "Help";
            // 
            // HelpShowHelpToolStripMenuItem
            // 
            this.HelpShowHelpToolStripMenuItem.Name = "HelpShowHelpToolStripMenuItem";
            this.HelpShowHelpToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.HelpShowHelpToolStripMenuItem.Text = "Help";
            this.HelpShowHelpToolStripMenuItem.Click += new System.EventHandler(this.HelpShowHelpToolStripMenuItem_Click);
            // 
            // HelpOpenChangeLogToolStripMenuItem
            // 
            this.HelpOpenChangeLogToolStripMenuItem.Name = "HelpOpenChangeLogToolStripMenuItem";
            this.HelpOpenChangeLogToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.HelpOpenChangeLogToolStripMenuItem.Text = "Open Change Log...";
            this.HelpOpenChangeLogToolStripMenuItem.Click += new System.EventHandler(this.HelpOpenChangeLogToolStripMenuItem_Click);
            // 
            // HelpCheckForNewVersionToolStripMenuItem
            // 
            this.HelpCheckForNewVersionToolStripMenuItem.Name = "HelpCheckForNewVersionToolStripMenuItem";
            this.HelpCheckForNewVersionToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.HelpCheckForNewVersionToolStripMenuItem.Text = "Check for New Version";
            this.HelpCheckForNewVersionToolStripMenuItem.Click += new System.EventHandler(this.HelpCheckForNewVersionToolStripMenuItem_Click);
            // 
            // HelpDocumentsToolStripMenuItem
            // 
            this.HelpDocumentsToolStripMenuItem.Name = "HelpDocumentsToolStripMenuItem";
            this.HelpDocumentsToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.HelpDocumentsToolStripMenuItem.Text = "Documents";
            // 
            // HelpAboutToolStripMenuItem
            // 
            this.HelpAboutToolStripMenuItem.Name = "HelpAboutToolStripMenuItem";
            this.HelpAboutToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.HelpAboutToolStripMenuItem.Text = "About Aclara Azure Automation Sidekicks";
            this.HelpAboutToolStripMenuItem.Click += new System.EventHandler(this.HelpAboutToolStripMenuItem_Click);
            // 
            // MainToolStrip
            // 
            this.MainToolStrip.BackColor = System.Drawing.Color.LightSteelBlue;
            this.MainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HomeToolStripButton,
            this.BackToolStripButton,
            this.ForwardToolStripButton,
            this.MainSidekickSelectorToolStripSplitButton,
            this.MainShowLogToolStripButton,
            this.MainDocumentsToolStripSplitButton,
            this.MainMicrosoftAzureToolStripSplitButton,
            this.MainShowSidekickComponentAssistantToolStripSplitButton,
            this.MainHelpToolStripButton,
            this.MainAutoUpdaterToolStripButton,
            this.MainAutoUpdaterShowChangeLogToolStripButton,
            this.MainAutoUpdatertoolStripLabel});

            this.MainToolStrip.Location = new System.Drawing.Point(0, 24);
            this.MainToolStrip.Name = "MainToolStrip";
            this.MainToolStrip.Size = new System.Drawing.Size(1228, 25);
            this.MainToolStrip.TabIndex = 1;
            this.MainToolStrip.Text = "toolStrip1";
            // 
            // HomeToolStripButton
            // 
            this.HomeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.HomeToolStripButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionHome;
            this.HomeToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.HomeToolStripButton.Name = "HomeToolStripButton";
            this.HomeToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.HomeToolStripButton.Text = "Home";
            this.HomeToolStripButton.Click += new System.EventHandler(this.HomeToolStripButton_Click);
            // 
            // BackToolStripButton
            // 
            this.BackToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BackToolStripButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionBack;
            this.BackToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BackToolStripButton.Name = "BackToolStripButton";
            this.BackToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.BackToolStripButton.Text = "Back";
            this.BackToolStripButton.ToolTipText = "Click to go back";
            this.BackToolStripButton.Click += new System.EventHandler(this.BackToolStripButton_Click);
            this.BackToolStripButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BackToolStripButton_MouseDown);
            // 
            // ForwardToolStripButton
            // 
            this.ForwardToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ForwardToolStripButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionForward;
            this.ForwardToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ForwardToolStripButton.Name = "ForwardToolStripButton";
            this.ForwardToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.ForwardToolStripButton.Text = "Forward";
            this.ForwardToolStripButton.ToolTipText = "Click to go forward";
            this.ForwardToolStripButton.Click += new System.EventHandler(this.ForwardToolStripButton_Click);
            this.ForwardToolStripButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ForwardToolStripButton_MouseDown);
            // 
            // MainSidekickSelectorToolStripSplitButton
            // 
            this.MainSidekickSelectorToolStripSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.MainSidekickSelectorToolStripSplitButton.DropDown = this.MainSidekickSelectorContextMenuStrip;
            this.MainSidekickSelectorToolStripSplitButton.Image = ((System.Drawing.Image)(resources.GetObject("MainSidekickSelectorToolStripSplitButton.Image")));
            this.MainSidekickSelectorToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MainSidekickSelectorToolStripSplitButton.Name = "MainSidekickSelectorToolStripSplitButton";
            this.MainSidekickSelectorToolStripSplitButton.Size = new System.Drawing.Size(66, 22);
            this.MainSidekickSelectorToolStripSplitButton.Text = "Sidekick";
            this.MainSidekickSelectorToolStripSplitButton.ToolTipText = "Activate Sidekick";
            this.MainSidekickSelectorToolStripSplitButton.ButtonClick += new System.EventHandler(this.MainSidekickSelectorToolStripSplitButton_ButtonClick);
            // 
            // MainSidekickSelectorContextMenuStrip
            // 
            this.MainSidekickSelectorContextMenuStrip.Name = "MainSidekickSelectorContextMenuStrip";
            this.MainSidekickSelectorContextMenuStrip.OwnerItem = this.MainSidekickSelectorToolStripSplitButton;
            this.MainSidekickSelectorContextMenuStrip.Size = new System.Drawing.Size(61, 4);
            // 
            // MainShowLogToolStripButton
            // 
            this.MainShowLogToolStripButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.MainShowLogToolStripButton.Checked = true;
            this.MainShowLogToolStripButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MainShowLogToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.MainShowLogToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("MainShowLogToolStripButton.Image")));
            this.MainShowLogToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MainShowLogToolStripButton.Name = "MainShowLogToolStripButton";
            this.MainShowLogToolStripButton.Size = new System.Drawing.Size(59, 22);
            this.MainShowLogToolStripButton.Text = "Hide Log";
            this.MainShowLogToolStripButton.ToolTipText = "Show/Hide Log";
            this.MainShowLogToolStripButton.Click += new System.EventHandler(this.MainShowLogToolStripButton_Click);
            // 
            // MainDocumentsToolStripSplitButton
            // 
            this.MainDocumentsToolStripSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.MainDocumentsToolStripSplitButton.Image = ((System.Drawing.Image)(resources.GetObject("MainDocumentsToolStripSplitButton.Image")));
            this.MainDocumentsToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MainDocumentsToolStripSplitButton.Name = "MainDocumentsToolStripSplitButton";
            this.MainDocumentsToolStripSplitButton.Size = new System.Drawing.Size(84, 22);
            this.MainDocumentsToolStripSplitButton.Text = "Documents";
            this.MainDocumentsToolStripSplitButton.ToolTipText = "Display Documents";
            // 
            // MainMicrosoftAzureToolStripSplitButton
            // 
            this.MainMicrosoftAzureToolStripSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.MainMicrosoftAzureToolStripSplitButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MainMicrosoftAzurePortalToolStripMenuItem,
            this.MainMicrosoftClassicAzurePortalToolStripMenuItem});
            this.MainMicrosoftAzureToolStripSplitButton.Image = ((System.Drawing.Image)(resources.GetObject("MainMicrosoftAzureToolStripSplitButton.Image")));
            this.MainMicrosoftAzureToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MainMicrosoftAzureToolStripSplitButton.Name = "MainMicrosoftAzureToolStripSplitButton";
            this.MainMicrosoftAzureToolStripSplitButton.Size = new System.Drawing.Size(32, 22);
            this.MainMicrosoftAzureToolStripSplitButton.Text = "Microsoft Azure";
            this.MainMicrosoftAzureToolStripSplitButton.ToolTipText = "Navigate to Microsoft Azure";
            // 
            // MainMicrosoftAzurePortalToolStripMenuItem
            // 
            this.MainMicrosoftAzurePortalToolStripMenuItem.Name = "MainMicrosoftAzurePortalToolStripMenuItem";
            this.MainMicrosoftAzurePortalToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.MainMicrosoftAzurePortalToolStripMenuItem.Text = "Microsoft Azure Portal...";
            this.MainMicrosoftAzurePortalToolStripMenuItem.Click += new System.EventHandler(this.MainMicrosoftAzurePortalToolStripMenuItem_Click);
            // 
            // MainMicrosoftClassicAzurePortalToolStripMenuItem
            // 
            this.MainMicrosoftClassicAzurePortalToolStripMenuItem.Name = "MainMicrosoftClassicAzurePortalToolStripMenuItem";
            this.MainMicrosoftClassicAzurePortalToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.MainMicrosoftClassicAzurePortalToolStripMenuItem.Text = "Microsoft Classic Azure Portal...";
            this.MainMicrosoftClassicAzurePortalToolStripMenuItem.Click += new System.EventHandler(this.MainMicrosoftClassicAzurePortalToolStripMenuItem_Click);
            // 
            // MainShowSidekickComponentAssistantToolStripSplitButton
            // 
            this.MainShowSidekickComponentAssistantToolStripSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.MainShowSidekickComponentAssistantToolStripSplitButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionShowComponentAssistant;
            this.MainShowSidekickComponentAssistantToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MainShowSidekickComponentAssistantToolStripSplitButton.Name = "MainShowSidekickComponentAssistantToolStripSplitButton";
            this.MainShowSidekickComponentAssistantToolStripSplitButton.Size = new System.Drawing.Size(23, 22);
            this.MainShowSidekickComponentAssistantToolStripSplitButton.Text = "Show Component Assistant...";
            this.MainShowSidekickComponentAssistantToolStripSplitButton.Click += new System.EventHandler(this.MainShowSidekickComponentAssistantToolStripSplitButton_Click);
            // 
            // MainHelpToolStripButton
            // 
            this.MainHelpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.MainHelpToolStripButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.MainHelpToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MainHelpToolStripButton.Name = "MainHelpToolStripButton";
            this.MainHelpToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.MainHelpToolStripButton.ToolTipText = "Display Help";
            this.MainHelpToolStripButton.Click += new System.EventHandler(this.MainHelpToolStripButton_Click);
            // 
            // MainAutoUpdaterToolStripButton
            // 
            this.MainAutoUpdaterToolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.MainAutoUpdaterToolStripButton.BackColor = System.Drawing.Color.Gold;
            this.MainAutoUpdaterToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.MainAutoUpdaterToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("MainAutoUpdaterToolStripButton.Image")));
            this.MainAutoUpdaterToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MainAutoUpdaterToolStripButton.Name = "MainAutoUpdaterToolStripButton";
            this.MainAutoUpdaterToolStripButton.Size = new System.Drawing.Size(77, 22);
            this.MainAutoUpdaterToolStripButton.Text = "Update Now";
            this.MainAutoUpdaterToolStripButton.Click += new System.EventHandler(this.MainAutoUpdaterToolStripButton_Click);
            // 
            // MainAutoUpdaterShowChangeLogToolStripButton
            // 
            this.MainAutoUpdaterShowChangeLogToolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.MainAutoUpdaterShowChangeLogToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.MainAutoUpdaterShowChangeLogToolStripButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionShowChangeLog;
            this.MainAutoUpdaterShowChangeLogToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MainAutoUpdaterShowChangeLogToolStripButton.Name = "MainAutoUpdaterShowChangeLogToolStripButton";
            this.MainAutoUpdaterShowChangeLogToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.MainAutoUpdaterShowChangeLogToolStripButton.Text = "toolStripButton1";
            this.MainAutoUpdaterShowChangeLogToolStripButton.ToolTipText = "Show change log";
            this.MainAutoUpdaterShowChangeLogToolStripButton.Click += new System.EventHandler(this.MainAutoUpdaterShowChangeLogToolStripButton_Click);
            // 
            // MainAutoUpdatertoolStripLabel
            // 
            this.MainAutoUpdatertoolStripLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.MainAutoUpdatertoolStripLabel.BackColor = System.Drawing.Color.LightSteelBlue;
            this.MainAutoUpdatertoolStripLabel.Name = "MainAutoUpdatertoolStripLabel";
            this.MainAutoUpdatertoolStripLabel.Size = new System.Drawing.Size(144, 22);
            this.MainAutoUpdatertoolStripLabel.Text = "A new version is available.";
            // 
            // SidekickContainerStatusStrip
            // 
            this.SidekickContainerStatusStrip.Location = new System.Drawing.Point(0, 813);
            this.SidekickContainerStatusStrip.Name = "SidekickContainerStatusStrip";
            this.SidekickContainerStatusStrip.Size = new System.Drawing.Size(1228, 22);
            this.SidekickContainerStatusStrip.TabIndex = 2;
            // 
            // LogPanel
            // 
            this.LogPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LogPanel.Location = new System.Drawing.Point(0, 713);
            this.LogPanel.Name = "LogPanel";
            this.LogPanel.Size = new System.Drawing.Size(1228, 100);
            this.LogPanel.TabIndex = 5;
            // 
            // HorizontalSplitter
            // 
            this.HorizontalSplitter.BackColor = System.Drawing.SystemColors.ControlDark;
            this.HorizontalSplitter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.HorizontalSplitter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.HorizontalSplitter.Location = new System.Drawing.Point(0, 703);
            this.HorizontalSplitter.Name = "HorizontalSplitter";
            this.HorizontalSplitter.Size = new System.Drawing.Size(1228, 10);
            this.HorizontalSplitter.TabIndex = 6;
            this.HorizontalSplitter.TabStop = false;
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.Azure.Automation.Sidekicks.Help.chm";
            // 
            // SidekickContainerPanel
            // 
            this.SidekickContainerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SidekickContainerPanel.Location = new System.Drawing.Point(0, 49);
            this.SidekickContainerPanel.Name = "SidekickContainerPanel";
            this.SidekickContainerPanel.Size = new System.Drawing.Size(1228, 654);
            this.SidekickContainerPanel.TabIndex = 7;
            // 
            // SidekickTimer
            // 
            this.SidekickTimer.Enabled = true;
            this.SidekickTimer.Tick += new System.EventHandler(this.SidekickTimer_Tick);
            // 
            // SidekickViewHistoryContextMenuStrip
            // 
            this.SidekickViewHistoryContextMenuStrip.Name = "SidekickViewHistoryContextMenuStrip";
            this.SidekickViewHistoryContextMenuStrip.Size = new System.Drawing.Size(61, 4);
            // 
            // HelpSidekicksAutoUpdaterInfoToolStripMenuItem
            // 
            this.HelpSidekicksAutoUpdaterInfoToolStripMenuItem.Name = "HelpSidekicksAutoUpdaterInfoToolStripMenuItem";
            this.HelpSidekicksAutoUpdaterInfoToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.HelpSidekicksAutoUpdaterInfoToolStripMenuItem.Text = "Sidekicks Auto-updater Info...";
            this.HelpSidekicksAutoUpdaterInfoToolStripMenuItem.Click += new System.EventHandler(this.HelpSidekicksAutoUpdaterInfoToolStripMenuItem_Click);
            // 
            // SidekickContainerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1228, 835);
            this.Controls.Add(this.SidekickContainerPanel);
            this.Controls.Add(this.HorizontalSplitter);
            this.Controls.Add(this.LogPanel);
            this.Controls.Add(this.SidekickContainerStatusStrip);
            this.Controls.Add(this.MainToolStrip);
            this.Controls.Add(this.SidekickContainerMenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.SidekickContainerMenuStrip;
            this.MinimumSize = new System.Drawing.Size(16, 750);
            this.Name = "SidekickContainerForm";
            this.Text = "* - Aclara Azure Automation Sidekicks";
            this.SidekickContainerMenuStrip.ResumeLayout(false);
            this.SidekickContainerMenuStrip.PerformLayout();
            this.MainToolStrip.ResumeLayout(false);
            this.MainToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip SidekickContainerMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ViewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ViewSidekicksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ViewShowLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ViewOpenLogFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HelpShowHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HelpOpenChangeLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HelpCheckForNewVersionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HelpDocumentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HelpAboutToolStripMenuItem;
        private System.Windows.Forms.ToolStrip MainToolStrip;
        private System.Windows.Forms.ToolStripButton MainShowLogToolStripButton;
        private System.Windows.Forms.ToolStripSplitButton MainDocumentsToolStripSplitButton;
        private System.Windows.Forms.ToolStripButton MainHelpToolStripButton;
        private System.Windows.Forms.ToolStripButton MainAutoUpdaterToolStripButton;
        private System.Windows.Forms.ToolStripLabel MainAutoUpdatertoolStripLabel;
        private System.Windows.Forms.ToolStripSplitButton MainMicrosoftAzureToolStripSplitButton;
        private System.Windows.Forms.ToolStripMenuItem MainMicrosoftAzurePortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MainMicrosoftClassicAzurePortalToolStripMenuItem;
        private System.Windows.Forms.StatusStrip SidekickContainerStatusStrip;
        private System.Windows.Forms.Panel LogPanel;
        private System.Windows.Forms.Splitter HorizontalSplitter;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.Panel SidekickContainerPanel;
        private System.Windows.Forms.ToolStripSplitButton MainSidekickSelectorToolStripSplitButton;
        private System.Windows.Forms.ContextMenuStrip MainSidekickSelectorContextMenuStrip;
        private System.Windows.Forms.ToolStripButton HomeToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem ViewSidekickComponentAssistantToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton MainShowSidekickComponentAssistantToolStripSplitButton;
        private System.Windows.Forms.Timer SidekickTimer;
        private System.Windows.Forms.ToolTip SidekickToolTip;
        private System.Windows.Forms.ToolStripButton MainAutoUpdaterShowChangeLogToolStripButton;
        private System.Windows.Forms.ContextMenuStrip SidekickViewHistoryContextMenuStrip;
        private System.Windows.Forms.ToolStripButton BackToolStripButton;
        private System.Windows.Forms.ToolStripButton ForwardToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem HelpSidekicksAutoUpdaterInfoToolStripMenuItem;
    }
}

