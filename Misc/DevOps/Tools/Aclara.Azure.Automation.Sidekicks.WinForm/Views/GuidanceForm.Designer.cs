﻿namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    partial class GuidanceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GuidanceForm));
            this.GuidanceTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // GuidanceTextBox
            // 
            this.GuidanceTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GuidanceTextBox.Location = new System.Drawing.Point(12, 12);
            this.GuidanceTextBox.Multiline = true;
            this.GuidanceTextBox.Name = "GuidanceTextBox";
            this.GuidanceTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.GuidanceTextBox.Size = new System.Drawing.Size(698, 270);
            this.GuidanceTextBox.TabIndex = 0;
            // 
            // GuidanceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 294);
            this.Controls.Add(this.GuidanceTextBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GuidanceForm";
            this.Text = "Guidance for <sidekick> - Aclara Azure Automation Sidekicks";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox GuidanceTextBox;
    }
}