﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    /// <summary>
    /// Component assistant form factory.
    /// </summary>
    public class SidekickComponentAssistantFormFactory
    {

        #region Private Constants
        #endregion

        #region Public Methods

        /// <summary>
        /// Create form.
        /// </summary>
        /// <returns></returns>
        public static SidekickComponentAssistantForm CreateForm(SidekickConfiguration sidekickConfiguration, SidekickComponentManager sidekickComponentManager)
        {

            SidekickComponentAssistantForm result = null;
            SidekickComponentAssistantPresenter componentAssistantPresenter = null;

            try
            {
                result = new SidekickComponentAssistantForm(sidekickConfiguration, sidekickComponentManager);

                componentAssistantPresenter = new SidekickComponentAssistantPresenter(result);

                result.SidekickComponentAssistantPresenter = componentAssistantPresenter;

                componentAssistantPresenter.Initialize();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion
    }
}
