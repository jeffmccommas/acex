﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Utilities;
using Aclara.Azure.Automation.Sidekicks.WinForm.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public class AzureCloudServiceFormFactory
    {
        /// <summary>
        /// Create form.
        /// </summary>
        /// <returns></returns>
        public static AzureCloudServiceForm CreateForm(SidekickConfiguration sidekickConfiguration, SidekickComponentManager sidekickComponentManager)
        {

            AzureCloudServiceForm result = null;
            AzureCloudServicePresenter azureCloudServicePresenter = null;
            SidekickComponentManagerForm sidekickComponentManagerForm = null;
            SidekickComponentAssistantForm sidekickComponentAssistantForm = null;

            try
            {
                result = new AzureCloudServiceForm(sidekickConfiguration);

                azureCloudServicePresenter = new AzureCloudServicePresenter(sidekickConfiguration, result);
                sidekickComponentManagerForm = SidekickComponentManagerFormFactory.CreateForm(sidekickConfiguration, sidekickComponentManager);
                sidekickComponentAssistantForm = SidekickComponentAssistantFormFactory.CreateForm(sidekickConfiguration, sidekickComponentManager);

                result.AzureCloudServicePresenter = azureCloudServicePresenter;
                result.SidekickComponentManagerForm = sidekickComponentManagerForm;
                result.SidekickComponentAssistantForm = sidekickComponentAssistantForm;

                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
