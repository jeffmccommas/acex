﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Events;
using Aclara.Azure.Automation.Sidekicks.WinForm.Logging;
using Aclara.Azure.Automation.Sidekicks.WinForm.Models;
using Aclara.Azure.Automation.Sidekicks.WinForm.Types;
using Aclara.Azure.Automation.Sidekicks.WinForm.Utilities;
using Aclara.Sidekicks.AutoUpdater;
using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public partial class SidekickContainerForm : Form
    {

        #region Private Constants

        private const string SidekickContainerFormTitleSuffix = "Aclara Azure Automation Sidekicks";

        private const string SidekicksAutoUpdater_NewVersionAvailableShort = "A new version is available.";
        private const string SidekicksAutoUpdater_UpdaterFailed = "Auto-updater failed.";
        private const string SidekicksAutoUpdater_UpToDate = "Sidekicks are up to date.";
        private const string SidekicksAutoUpdater_MoreInfo = "More info...";
        private const string SidekicksAutoUpdater_NewVersionAvailablePrompt = "A new version of Aclara.Azure.Automation.Sidekicks is available. Update now?";
        private const string SidekicksAutoUpdater_CurrentVersionAlreadyInstalledPrompt = "Current version of Aclara.Azure.Automation.Sidekicks is already installed.";

        private const int NextAutoUpdaterCheckIsDueInMinutes = 15;

        private const string SidekickCategory_Automation = "Automation";
        private const string SidekickCategory_Configuration = "Configuration";
        private const string SidekickCategory_Home = "Home";
        private const string SidekickCategory_Unspecified = "Unspecified";
        private const string SidekickCategory_Utility = "Utility";

        private const string ShowLogToolStripButton_ShowLog = "Show Log";
        private const string ShowLogToolStripButton_HideLog = "Hide Log";

        private const string ShowLogFileErrorMessage_CouldNotLocateLogFile = "Could not locate log file.";
        private const string ShowChangeLogFileErrorMessage_CouldNotLocateLogFile = "Could not locate change log file.";

        private const string ChangeLogFileName = "ChangeLog.txt";

        private const string MicrosoftAzurePortalBaseUri = "https://portal.azure.com/";
        private const string MicrosoftAzurePortalClassicBaseUri = "https://manage.windowsazure.com/";

        private const string Information_SidekicksAutoUpdater = "Sidekicks Auto-Updater - Information";

        private const string KillSwitch_ConfigurationFileName = "killswitch.json";

        #endregion

        #region Private Data Members

        private LogForm _logForm = null;
        private SidekickConfiguration _sidekickConfiguration;
        private SidekickComponentManager _sidekickComponentManager;
        private SidekickComponentAssistantForm _sidekickComponentAssistantForm;
        private SidekickViewList _sidekickViewList;
        private SidekicksAutoUpdater _sidekicksAutoUpdater;
        private string _sidekicksBootstrapperExeRootPath;
        private string _sidekicksBootstrapperExeName;
        private string _sidekicksAutoUpdaterVersion;
        private SidekickView _homeSidekickView;
        private DateTime _autoUpdaterLastDateTime;
        private SidekickViewHistoryList _sidekickViewHistoryList;

        #endregion

        #region Public Delegates

        public event EventHandler<SidekickSelectionChangedEventArgs> SidekickSelectionChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Log form.
        /// </summary>
        public LogForm LogForm
        {
            get { return _logForm; }
            set { _logForm = value; }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick component mangager.
        /// </summary>
        public SidekickComponentManager SidekickComponentManager
        {
            get { return _sidekickComponentManager; }
            set { _sidekickComponentManager = value; }
        }

        /// <summary>
        /// Property: Sidekick component assistant form.
        /// </summary>
        public SidekickComponentAssistantForm SidekickComponentAssistantForm
        {
            get { return _sidekickComponentAssistantForm; }
            set { _sidekickComponentAssistantForm = value; }
        }

        /// <summary>
        /// Property: Sidekick view list.
        /// </summary>
        public SidekickViewList SidekickViewList
        {
            get { return _sidekickViewList; }
            set { _sidekickViewList = value; }
        }

        /// <summary>
        /// Property: Home sidekick view.
        /// </summary>
        public SidekickView HomeSidekickView
        {
            get { return _homeSidekickView; }
            set { _homeSidekickView = value; }
        }

        /// <summary>
        /// Property: Auto updater last check.
        /// </summary>
        public DateTime AutoUpdaterLastCheckDateTime
        {
            get { return _autoUpdaterLastDateTime; }
            set { _autoUpdaterLastDateTime = value; }
        }

        /// <summary>
        /// Property: Sidekickview history list.
        /// </summary>
        public SidekickViewHistoryList SidekickViewHistoryList
        {
            get { return _sidekickViewHistoryList; }
            set { _sidekickViewHistoryList = value; }
        }

        #endregion

        #region Protected Properties

        /// <summary>
        /// Property: Sidekicks auto-updater.
        /// </summary>
        protected SidekicksAutoUpdater SidekicksAutoUpdater
        {
            get
            {
                if (_sidekicksAutoUpdater == null)
                {
                    _sidekicksAutoUpdater = new SidekicksAutoUpdater();
                }
                return _sidekicksAutoUpdater;
            }
            set { _sidekicksAutoUpdater = value; }
        }

        /// <summary>
        /// Property: Sidekicks bootstrapper executable root path.
        /// </summary>
        protected string SidekicksBootstrapperExeRootPath
        {
            get
            {
                try
                {
                    _sidekicksBootstrapperExeRootPath = Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Settings.Default.SidekicksBootstrapperExeRootPath;

                }
                catch (Exception)
                {
                    throw;
                }
                return _sidekicksBootstrapperExeRootPath;
            }
        }

        /// <summary>
        /// Property: Sidekicks bootstrapper executable name.
        /// </summary>
        protected string SidekicksBootstrapperExeName
        {
            get
            {
                try
                {
                    _sidekicksBootstrapperExeName = Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Settings.Default.SidekicksBootstrapperExeName;

                }
                catch (Exception)
                {
                    throw;
                }
                return _sidekicksBootstrapperExeName;
            }
        }

        /// <summary>
        /// Property: Sidekicks auto updater version.
        /// </summary>
        protected string SidekicksAutoUpdaterVersion
        {
            get
            {
                try
                {
                    _sidekicksAutoUpdaterVersion = Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Settings.Default.SidekicksAutoUpdaterVersion;

                }
                catch (Exception)
                {
                    throw;
                }
                return _sidekicksAutoUpdaterVersion;
            }
        }

        #endregion 

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekickContainerForm()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            SidekickConfigurationManager sidekickConfigurationManager = null;

            try
            {
                currentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionHandler);

                _sidekickViewList = new SidekickViewList();
                _sidekickViewHistoryList = new SidekickViewHistoryList();

                InitializeComponent();

                sidekickConfigurationManager = SidekickConfigurationFactory.CreateSidekickConfiguration("AppSettings.json", AppDomain.CurrentDomain.BaseDirectory);
                sidekickConfigurationManager.InitializeConfiguration();
                _sidekickConfiguration = sidekickConfigurationManager.SidekickConfiguration;

                _sidekickComponentManager = new SidekickComponentManager(this.SidekickConfiguration);

                _sidekickComponentAssistantForm = SidekickComponentAssistantFormFactory.CreateForm(_sidekickConfiguration, _sidekickComponentManager);

                InitializeViews();
                InitializeControls();

                this.PerformKillSwitchCheck();

            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Activate sidekick view.
        /// </summary>
        /// <param name="sidekickViewList"></param>
        public void ActivateSidekickView(SidekickView sidekickView)
        {
            SidekickSelectionChangedEventArgs sidekickSelectionChangedEventArgs = null;

            try
            {

                ChangeSidekickContainerFormTitle(sidekickView);

                UpdateSplitButtonSidekicksToolStripMenuItemChecked(sidekickView);

                UpdateViewSidekicksToolStripMenuItemChecked(sidekickView);

                this.SidekickContainerPanel.DockControl(sidekickView.Form);

                if (SidekickSelectionChanged != null)
                {
                    sidekickSelectionChangedEventArgs = new SidekickSelectionChangedEventArgs(sidekickView.Name, sidekickView.Description);
                    this.SidekickSelectionChanged(this, sidekickSelectionChangedEventArgs);
                }

                if (sidekickView.Form is ISidekickView)
                {
                    (sidekickView.Form as ISidekickView).SidekickActivated();
                }

                this.SidekickViewHistoryList.Add(sidekickView);
                this.UpdateBackAndForwardButton();
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Update back and forward buttons.
        /// </summary>
        protected void UpdateBackAndForwardButton()
        {

            try
            {
                if (this.SidekickViewHistoryList.IsBackAvailable() == true)
                {
                    this.BackToolStripButton.Enabled = true;
                }
                else
                {
                    this.BackToolStripButton.Enabled = false;
                }

                if (this.SidekickViewHistoryList.IsForwardAvailable() == true)
                {
                    this.ForwardToolStripButton.Enabled = true;
                }
                else
                {
                    this.ForwardToolStripButton.Enabled = false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize views.
        /// </summary>
        protected void InitializeViews()
        {
            bool defaultView = false;
            Form sidekickForm = null;
            ISidekickInfo sidekickInfo = null;

            try
            {
                this.LogForm = LogFormFactory.CreateForm(this);
                this.LogForm.TopLevel = false;

                this.LogPanel.DockControl(this.LogForm);

                InitializeLogWriter();

                ////Add Aclara One Alerts Diagnostics sidekick view.
                //defaultView = false;
                //sidekickForm = AOAlertsDiagFormFactory.CreateForm(this.SidekickConfiguration);
                //sidekickInfo = (ISidekickInfo)sidekickForm;
                //this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                //                                          sidekickInfo.SidekickDescription,
                //                                          sidekickForm,
                //                                          defaultView,
                //                                          new SidekickCategory(SidekickCategory_Utility, SidekickCategory_Utility)));

                //Add Azure storage account configuration sidekick view.
                defaultView = false;
                sidekickForm = AzureStorageAccountConfigFormFactory.CreateForm(this.SidekickConfiguration);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                          sidekickInfo.SidekickDescription,
                                                          sidekickForm,
                                                          defaultView,
                                                          new SidekickCategory(SidekickCategory_Configuration, SidekickCategory_Configuration)));

                //Add Azure Redis cache administration sidekick view.
                defaultView = false;
                sidekickForm = AzureRedisCacheAdminFormFactory.CreateForm(this.SidekickConfiguration);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_Utility, SidekickCategory_Utility)));


                //Add Cassandra configuration sidekick view.
                defaultView = false;
                sidekickForm = CassandraConfigFormFactory.CreateForm(this.SidekickConfiguration);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                           sidekickInfo.SidekickDescription,
                                                           sidekickForm,
                                                           defaultView,
                                                           new SidekickCategory(SidekickCategory_Configuration, SidekickCategory_Configuration)));

                ////Add PowerShell Host sidekick view.
                //defaultView = false;
                //sidekickForm = PowerShellHostFormFactory.CreateForm(this.SidekickConfiguration, this.SidekickComponentManager);
                //sidekickInfo = (ISidekickInfo)sidekickForm;
                //this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                //                                           sidekickInfo.SidekickDescription,
                //                                           sidekickForm,
                //                                           defaultView,
                //                                           new SidekickCategory(SidekickCategory_Automation, SidekickCategory_Automation)));

                //Add report portal configuration sidekick view.
                defaultView = false;
                sidekickForm = ReportPortalConfigFormFactory.CreateForm(this.SidekickConfiguration);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                          sidekickInfo.SidekickDescription,
                                                          sidekickForm,
                                                          defaultView,
                                                          new SidekickCategory(SidekickCategory_Configuration, SidekickCategory_Configuration)));

                //Add Azure cloud service sidekick view.
                defaultView = false;
                sidekickForm = AzureCloudServiceFormFactory.CreateForm(this.SidekickConfiguration, this.SidekickComponentManager);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.SidekickViewList.Add(new SidekickView(sidekickInfo.SidekickName,
                                                          sidekickInfo.SidekickDescription,
                                                          sidekickForm,
                                                          defaultView,
                                                          new SidekickCategory(SidekickCategory_Automation, SidekickCategory_Automation)));

                //--------------------------------------------------------------------------------------------------------------------------------------------------
                //NOTE: Home sidekick view is added after all other sidekick views inorder to render sidekick view navigation.
                //--------------------------------------------------------------------------------------------------------------------------------------------------

                //Add home sidekick view.
                defaultView = true;
                sidekickForm = HomeFormFactory.CreateForm(this, this.SidekickConfiguration);
                sidekickInfo = (ISidekickInfo)sidekickForm;
                this.HomeSidekickView = new SidekickView(sidekickInfo.SidekickName,
                                                         sidekickInfo.SidekickDescription,
                                                         sidekickForm,
                                                         defaultView,
                                                         new SidekickCategory(SidekickCategory_Home, SidekickCategory_Home));
                this.SidekickViewList.Add(this.HomeSidekickView);
                this.SidekickContainerPanel.DockControl(this.SidekickViewList.First().Form);
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {

            try
            {
                //Populate auto-updater state in UI components.
                PopulateUIComponentsWithAutoUpdaterState();

                //Populate document list in UI components.
                PopulateUIComponentsWithDocumentList();

                //Populate sidekick view list in UI components.
                PopulateUIComponentsWithSidekickViewList(this.SidekickViewList);

            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Populate UI components with auto-updater state.
        /// </summary>
        protected void PopulateUIComponentsWithAutoUpdaterState()
        {
            try
            {
                this.MainAutoUpdatertoolStripLabel.Visible = false;
                this.MainAutoUpdaterShowChangeLogToolStripButton.Visible = false;
                this.MainAutoUpdaterToolStripButton.Visible = false;

                //Retrieve sidekicks auto-updater.
                this.SidekicksAutoUpdater = this.GetSidekicksAutoUpdater();

                //A new sidekicks auto-updater version is available.
                if (this.SidekicksAutoUpdater.Status.RunState == RunState.OK &&
                    this.SidekicksAutoUpdater.IsNewVersionAvailable(this.SidekicksAutoUpdaterVersion) == true)
                {
                    this.MainAutoUpdatertoolStripLabel.Visible = true;
                    this.MainAutoUpdaterShowChangeLogToolStripButton.Visible = true;
                    this.MainAutoUpdaterToolStripButton.Visible = true;
                }
                else if (this.SidekicksAutoUpdater.Status.RunState == RunState.OK &&
                         this.SidekicksAutoUpdater.IsNewVersionAvailable(this.SidekicksAutoUpdaterVersion) == false)
                {
                    this.MainAutoUpdatertoolStripLabel.Visible = true;
                    this.MainAutoUpdaterShowChangeLogToolStripButton.Visible = false;
                    this.MainAutoUpdaterToolStripButton.Visible = false;
                    this.MainAutoUpdatertoolStripLabel.Text = SidekicksAutoUpdater_UpToDate;
                }

                //Sidekicks auto-updater failed.
                if (this.SidekicksAutoUpdater.Status.RunState == RunState.Failed)
                {
                    this.MainAutoUpdatertoolStripLabel.Visible = true;
                    this.MainAutoUpdaterShowChangeLogToolStripButton.Visible = true;
                    this.MainAutoUpdaterToolStripButton.Visible = true;
                    this.MainAutoUpdaterToolStripButton.Text = SidekicksAutoUpdater_MoreInfo;
                    this.MainAutoUpdatertoolStripLabel.Text = SidekicksAutoUpdater_UpdaterFailed;
                }

            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Populate UI components with document list.
        /// </summary>
        protected void PopulateUIComponentsWithDocumentList()
        {
            const string DocumentNamePrefix = "Document";
            int documentNameIndex = 0;
            ToolStripMenuItem helpMenuToolStripMenuItem = null;
            ToolStripMenuItem toolStripSplitButtonMenuToolStripMenuItem = null;

            try
            {
                //Iterate through document list.
                foreach (var documentConfig in this.SidekickConfiguration.Documents.Document.Where(doc => doc.Visible == "true"))
                {
                    documentNameIndex++;

                    //Create and populate menu item - help.documents menu.
                    helpMenuToolStripMenuItem = new ToolStripMenuItem();
                    helpMenuToolStripMenuItem.Name = string.Format("{0}{1}",
                                                  DocumentNamePrefix, documentNameIndex);
                    helpMenuToolStripMenuItem.Text = documentConfig.Name;
                    helpMenuToolStripMenuItem.ToolTipText = documentConfig.FileName;

                    helpMenuToolStripMenuItem.Click += new EventHandler(DocumentToolStripMenuItem_Click);

                    this.HelpDocumentsToolStripMenuItem.DropDownItems.Add(helpMenuToolStripMenuItem);

                    //Create and populate menu item - tool strip split button documents.
                    toolStripSplitButtonMenuToolStripMenuItem = new ToolStripMenuItem();
                    toolStripSplitButtonMenuToolStripMenuItem.Name = string.Format("{0}{1}",
                                                                          DocumentNamePrefix, documentNameIndex);
                    toolStripSplitButtonMenuToolStripMenuItem.Text = documentConfig.Name;
                    toolStripSplitButtonMenuToolStripMenuItem.ToolTipText = documentConfig.FileName;

                    toolStripSplitButtonMenuToolStripMenuItem.Click += new EventHandler(DocumentToolStripMenuItem_Click);

                    this.MainDocumentsToolStripSplitButton.DropDownItems.Add(toolStripSplitButtonMenuToolStripMenuItem);

                }

            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Populate UI components with sidekick view list.
        /// </summary>
        protected void PopulateUIComponentsWithSidekickViewList(SidekickViewList sidekickViewList)
        {
            SidekickView defaultSidekickView = null;
            ToolStripMenuItem splitButtonParentToolStripMenuItem = null;
            ToolStripMenuItem splitButtonChildToolStripMenuItem = null;
            ToolStripMenuItem viewMenuParentToolStripMenuItem = null;
            ToolStripMenuItem viewMenuChildToolStripMenuItem = null;

            try
            {

                //Populate view menu items and sidekick selector split button.
                var categories = sidekickViewList.OrderBy(skv => skv.SidekickCategory.Description).Select(skv => skv.SidekickCategory);
                foreach (var category in categories.DistinctBy(c => c.Description))
                {
                    splitButtonParentToolStripMenuItem = new ToolStripMenuItem();
                    splitButtonParentToolStripMenuItem.Name = category.Name;
                    splitButtonParentToolStripMenuItem.Text = category.Description;
                    splitButtonParentToolStripMenuItem.ToolTipText = category.Description;
                    this.MainSidekickSelectorContextMenuStrip.Items.Add(splitButtonParentToolStripMenuItem);

                    viewMenuParentToolStripMenuItem = new ToolStripMenuItem();
                    viewMenuParentToolStripMenuItem.Name = category.Name;
                    viewMenuParentToolStripMenuItem.Text = category.Description;
                    viewMenuParentToolStripMenuItem.ToolTipText = category.Description;
                    this.ViewSidekicksToolStripMenuItem.DropDownItems.Add(viewMenuParentToolStripMenuItem);

                    foreach (var sidekickView in sidekickViewList.Where(skv => skv.SidekickCategory.Name == category.Name).OrderBy(skv => skv.SidekickCategory))
                    {
                        splitButtonChildToolStripMenuItem = new ToolStripMenuItem();
                        splitButtonChildToolStripMenuItem.Name = sidekickView.Name;
                        splitButtonChildToolStripMenuItem.Text = sidekickView.Description;
                        splitButtonChildToolStripMenuItem.ToolTipText = sidekickView.Description;
                        splitButtonChildToolStripMenuItem.Click += new EventHandler(ViewSidekicksSidekickViewToolStripMenuItem_Click);
                        splitButtonParentToolStripMenuItem.DropDownItems.Add(splitButtonChildToolStripMenuItem);

                        viewMenuChildToolStripMenuItem = new ToolStripMenuItem();
                        viewMenuChildToolStripMenuItem.Name = sidekickView.Name;
                        viewMenuChildToolStripMenuItem.Text = sidekickView.Description;
                        viewMenuChildToolStripMenuItem.ToolTipText = sidekickView.Description;
                        viewMenuChildToolStripMenuItem.Click += new EventHandler(ViewSidekicksSidekickViewToolStripMenuItem_Click);
                        viewMenuParentToolStripMenuItem.DropDownItems.Add(viewMenuChildToolStripMenuItem);

                    }
                }

                //Set default sidekick view.
                defaultSidekickView = sidekickViewList.Single(skv => skv.DefaultView == true);
                ActivateSidekickView(defaultSidekickView);

            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Update split button - sidekicks tool strip menu item checked.
        /// </summary>
        /// <param name="sidekickView"></param>
        protected void UpdateSplitButtonSidekicksToolStripMenuItemChecked(SidekickView sidekickView)
        {
            try
            {
                foreach (ToolStripMenuItem parentToolStripMenuItem in this.MainSidekickSelectorToolStripSplitButton.DropDownItems)
                {
                    foreach (ToolStripMenuItem childToolStripMenitem in parentToolStripMenuItem.DropDownItems)
                        if (sidekickView.Name == childToolStripMenitem.Name)
                        {
                            childToolStripMenitem.Checked = true;
                        }
                        else
                        {
                            childToolStripMenitem.Checked = false;
                        }

                }
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }
        /// <summary>
        /// Update view - sidekicks tool strip menu item checked.
        /// </summary>
        /// <param name="sidekickView"></param>
        protected void UpdateViewSidekicksToolStripMenuItemChecked(SidekickView sidekickView)
        {
            try
            {
                foreach (ToolStripMenuItem parentToolStripMenuItem in this.ViewSidekicksToolStripMenuItem.DropDownItems)
                {
                    foreach (ToolStripMenuItem childToolStripMenitem in parentToolStripMenuItem.DropDownItems)
                        if (sidekickView.Name == childToolStripMenitem.Name)
                        {
                            childToolStripMenitem.Checked = true;
                        }
                        else
                        {
                            childToolStripMenitem.Checked = false;
                        }

                }
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Change sidekick container form title.
        /// </summary>
        /// <param name="sidekickName"></param>
        protected void ChangeSidekickContainerFormTitle(SidekickView sidekickView)
        {
            try
            {
                this.Text = string.Format("{0} - {1}",
                                  sidekickView.Description,
                                  SidekickContainerFormTitleSuffix);
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Initialize log writer.
        /// </summary>
        protected void InitializeLogWriter()
        {
            //Create log event target.
            ConfigurationItemFactory.Default.Targets
                                    .RegisterDefinition("LogEventTarget", typeof(Aclara.Azure.Automation.Sidekicks.WinForm.Logging.LogEventTarget));
            var target = (LogEventTarget)LogManager.Configuration.FindTargetByName("logevent");
            target.LogEntryCreated += this.LogForm.LogEntryCreate;
        }

        /// <summary>
        /// Show sidekick view history list context menu.
        /// </summary>
        protected void ShowSidekickViewHistoryListContextMenu()
        {
            List<SidekickViewHistory> sidekickViewHistories = null;
            ToolStripMenuItem toolStripMenuItem = null;

            try
            {

                sidekickViewHistories = this.SidekickViewHistoryList.GetSidekickViewHistoryList();

                var sortedSidekickViewHistoryList = sidekickViewHistories.OrderBy(skv => skv.SequenceNumber);

                this.SidekickViewHistoryContextMenuStrip.Items.Clear();
                foreach (SidekickViewHistory category in sortedSidekickViewHistoryList)
                {
                    toolStripMenuItem = new ToolStripMenuItem();
                    toolStripMenuItem.Name = category.SidekickView.Name;
                    toolStripMenuItem.Text = category.SidekickView.Description;
                    toolStripMenuItem.ToolTipText = category.SidekickView.Description;
                    toolStripMenuItem.Click += new EventHandler(ViewSidekicksSidekickViewToolStripMenuItem_Click);

                    this.SidekickViewHistoryContextMenuStrip.Items.Add(toolStripMenuItem);

                }

                this.SidekickViewHistoryContextMenuStrip.Show(Cursor.Position);
            }
            catch (Exception e)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Retrieve sidekicks auto-updater.
        /// </summary>
        protected SidekicksAutoUpdater GetSidekicksAutoUpdater()
        {

            SidekicksAutoUpdater result = null;
            SidekicksAutoUpdaterManager sidekicksAutoUpdaterManager = null;
            string bootstrapperExeRootPath = string.Empty;
            string bootstrapperExeName = string.Empty;

            try
            {
                sidekicksAutoUpdaterManager = SidekicksAutoUpdaterManagerFactory.CreateSidekicksUpdateManager();

                //Retrieve sidekicks auto-updater.
                bootstrapperExeRootPath = this.SidekicksBootstrapperExeRootPath;
                bootstrapperExeName = this.SidekicksBootstrapperExeName;
                result = sidekicksAutoUpdaterManager.GetSidekicksAutoUpdater(this.SidekicksBootstrapperExeRootPath,
                                                                            bootstrapperExeName);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Perform kill switch check.
        /// </summary>
        protected void PerformKillSwitchCheck()
        {
            KillSwitchConfiguration killSwitchConfiguration = null;
            KillSwitchConfigurationManager killSwitchConfigurationManager = null;
            Document document = null;

            KillSwitchNotifyForm killSwitchNotifyForm = null;
            KillSwitch killSwitch = null;
            KillSwitchRunStatus killSwitchRunStatus;

            try
            {
                document = this.SidekickConfiguration.Documents.Document.Where(doc => doc.FileName == KillSwitch_ConfigurationFileName).SingleOrDefault();
                if (document == null)
                {
                    return;
                }

                killSwitchConfigurationManager = KillSwitchConfigurationFactory.CreateSidekickConfiguration(document.FileName, document.Path);
                killSwitchConfigurationManager.InitializeConfiguration();
                killSwitchConfiguration = killSwitchConfigurationManager.KillSwitchConfiguration;
                if (killSwitchConfiguration == null)
                {
                    return;
                }
                if (killSwitchConfiguration.KillSwitches == null)
                {
                    return;
                }

                killSwitch = killSwitchConfiguration.KillSwitches.KillSwitch.Where(ks => ks.VersionImpacted == this.SidekicksAutoUpdaterVersion).SingleOrDefault();
                if (killSwitch == null)
                {
                    return;
                }

                //Kill switch found.
                Enum.TryParse(killSwitch.RunStatus, out killSwitchRunStatus);
                this.SidekicksAutoUpdater = this.GetSidekicksAutoUpdater();
                killSwitchNotifyForm = KillSwitchNotifyFormFactory.CreateForm(SidekickContainerFormTitleSuffix,
                                                                              this.SidekicksAutoUpdaterVersion,
                                                                              killSwitch.Reason,
                                                                              killSwitchRunStatus,
                                                                              this.SidekicksAutoUpdater);
                killSwitchNotifyForm.ShowDialog();

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Unhandled exception - handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        static private void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(e);
            exceptionForm.ShowDialog();
        }

        /// <summary>
        /// Event Handler: About <app> click.</app>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpAboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBoxForm aboutBox = null;

            try
            {
                aboutBox = new AboutBoxForm();

                aboutBox.Show(this);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Exit tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Event Handler: Auto-updater tool strip button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainAutoUpdaterToolStripButton_Click(object sender, EventArgs e)
        {
            string stdout = string.Empty;
            string stderr = string.Empty;

            try
            {
                if (this.SidekicksAutoUpdater.Status.RunState == RunState.OK ||
                   this.SidekicksAutoUpdater.Status.RunState == RunState.Warning)
                {
                    this.SidekicksAutoUpdater.Run(out stdout, out stderr);

                    Application.Exit();
                }
                if (this.SidekicksAutoUpdater.Status.RunState == RunState.Failed)
                {
                    ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(this.SidekicksAutoUpdater.Status.Exception, this.SidekicksAutoUpdater.AutoUpdaterInfo);
                    exceptionForm.ShowDialog();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Show log tool strip menu item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewShowLogToolStripMenuItem_Click(object sender, EventArgs e)
        {

            ToolStripMenuItem toolStripMenuItem = null;

            try
            {
                if (sender is ToolStripMenuItem)
                {
                    toolStripMenuItem = (ToolStripMenuItem)sender;
                    if (toolStripMenuItem.Checked == true)
                    {
                        this.LogPanel.Hide();
                        toolStripMenuItem.Checked = false;
                        this.MainShowLogToolStripButton.Text = ShowLogToolStripButton_ShowLog;

                    }
                    else
                    {
                        this.LogPanel.Show();
                        toolStripMenuItem.Checked = true;
                        this.MainShowLogToolStripButton.Text = ShowLogToolStripButton_HideLog;
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Main Show log tool strip button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainShowLogToolStripButton_Click(object sender, EventArgs e)
        {

            ToolStripButton toolStripButton = null;

            try
            {
                if (sender is ToolStripButton)
                {
                    toolStripButton = (ToolStripButton)sender;
                    if (toolStripButton.Checked == true)
                    {
                        this.LogPanel.Hide();
                        toolStripButton.Checked = false;
                        this.ViewShowLogToolStripMenuItem.Checked = false;
                        toolStripButton.Text = ShowLogToolStripButton_ShowLog;
                    }
                    else
                    {
                        this.LogPanel.Show();
                        toolStripButton.Checked = true;
                        this.ViewShowLogToolStripMenuItem.Checked = true;
                        toolStripButton.Text = ShowLogToolStripButton_HideLog;
                    }
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Vew open log file tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewOpenLogFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileTarget fileTarget = null;
            string logFileName = string.Empty;
            string logFilePath = string.Empty;
            string executablePath = string.Empty;
            LogEventInfo logEventInfo = null;

            try
            {
                //Retrive NLog FileTarget from configuration.
                fileTarget = (FileTarget)LogManager.Configuration.FindTargetByName("LogFile");

                if (fileTarget == null)
                {
                    MessageBox.Show(string.Format(ShowLogFileErrorMessage_CouldNotLocateLogFile));
                    return;
                }

                logEventInfo = new LogEventInfo();
                logFileName = fileTarget.FileName.Render(logEventInfo);

                if (logFileName == null)
                {
                    MessageBox.Show(string.Format(ShowLogFileErrorMessage_CouldNotLocateLogFile));
                    return;
                }

                executablePath = Path.GetDirectoryName(Application.ExecutablePath);
                logFilePath = Path.Combine(executablePath, logFileName);

                if (File.Exists(logFilePath) == false)
                {
                    MessageBox.Show(string.Format(ShowLogFileErrorMessage_CouldNotLocateLogFile));
                    return;
                }

                System.Diagnostics.Process.Start(logFilePath);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: View sidekick component assistant tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewSidekickComponentAssistantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.SidekickComponentAssistantForm.Show();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Help open change log tool strip menu item - Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpOpenChangeLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string changeLogFilePath = string.Empty;
            string executablePath = string.Empty;

            try
            {
                executablePath = Path.GetDirectoryName(Application.ExecutablePath);
                changeLogFilePath = Path.Combine(executablePath, ChangeLogFileName);

                System.Diagnostics.Process.Start(changeLogFilePath);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Help check for new version tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpCheckForNewVersionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string stdout = string.Empty;
            string stderr = string.Empty;

            try
            {
                //Retrieve sidekicks auto-updater.
                this.SidekicksAutoUpdater = this.GetSidekicksAutoUpdater();

                //A new sidekicks auto-updater version is available.
                if (this.SidekicksAutoUpdater.Status.RunState == RunState.OK &&
                    this.SidekicksAutoUpdater.IsNewVersionAvailable(this.SidekicksAutoUpdaterVersion) == true)
                {
                    if (MessageBox.Show(SidekicksAutoUpdater_NewVersionAvailablePrompt,
                                        SidekickContainerFormTitleSuffix,
                                        MessageBoxButtons.YesNoCancel,
                                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        this.SidekicksAutoUpdater.Run(out stdout, out stderr);
                        Application.Exit();
                    }
                }
                else if (this.SidekicksAutoUpdater.Status.RunState == RunState.Failed)
                {
                    ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(this.SidekicksAutoUpdater.Status.Exception, this.SidekicksAutoUpdater.AutoUpdaterInfo);
                    exceptionForm.ShowDialog();
                }
                else
                {
                    MessageBox.Show(SidekicksAutoUpdater_CurrentVersionAlreadyInstalledPrompt,
                                    SidekickContainerFormTitleSuffix,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Document menu item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string stdout = string.Empty;
            string stderr = string.Empty;
            string documentFilePath = string.Empty;

            try
            {

                ToolStripMenuItem menuItem = (ToolStripMenuItem)sender;

                foreach (var documentConfig in this.SidekickConfiguration.Documents.Document.Where(doc => doc.Visible == "true"))
                {
                    if (documentConfig.Name == menuItem.Text)
                    {
                        documentFilePath = Path.Combine(documentConfig.Path, documentConfig.FileName);

                        System.Diagnostics.Process.Start(documentFilePath);
                        break;
                    }

                }

            }
            catch (Exception ex)
            {
                string context = string.Format("Document path: {0}",
                                               documentFilePath);
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex, context);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: View sidekicks - sidekick view menu item clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewSidekicksSidekickViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = null;

            try
            {
                menuItem = (ToolStripMenuItem)sender;
                var sidekickView = this.SidekickViewList.Single(skv => skv.Description == menuItem.Text);
                if (sidekickView == null)
                {
                    return;
                }
                this.ActivateSidekickView(sidekickView);

            }
            catch (Exception ex)
            {
                string context = string.Format("TBD");
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex, context);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Main Microsoft Azure portal tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainMicrosoftAzurePortalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string link = string.Empty;

            try
            {
                link = string.Format("{0}", MicrosoftAzurePortalBaseUri);
                System.Diagnostics.Process.Start(link);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Main Microsoft Azure portal classic tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainMicrosoftClassicAzurePortalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string link = string.Empty;

            try
            {
                link = string.Format("{0}", MicrosoftAzurePortalClassicBaseUri);
                System.Diagnostics.Process.Start(link);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Main help tool strip button Clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainHelpToolStripButton_Click(object sender, EventArgs e)
        {

            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, this.SidekickHelpProvider.GetHelpKeyword(this));
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Help show help tool strip menut item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpShowHelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, this.SidekickHelpProvider.GetHelpKeyword(this));

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Home tool strip button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HomeToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.ActivateSidekickView(this.HomeSidekickView);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Main sidekick selector tool strip split button - Button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainSidekickSelectorToolStripSplitButton_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                this.MainSidekickSelectorToolStripSplitButton.ShowDropDown();
            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Main show sidekick component assistant tool strip split button - Button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainShowSidekickComponentAssistantToolStripSplitButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.SidekickComponentAssistantForm.Show(this);
            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Sidekick Timer - Tick.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SidekickTimer_Tick(object sender, EventArgs e)
        {
            TimeSpan timeSinceLastCheckTimeSpan;
            try
            {
                timeSinceLastCheckTimeSpan = (this.AutoUpdaterLastCheckDateTime - DateTime.Now).Duration();

                if (timeSinceLastCheckTimeSpan.TotalMinutes >= NextAutoUpdaterCheckIsDueInMinutes)
                {
                    this.PopulateUIComponentsWithAutoUpdaterState();
                    this.AutoUpdaterLastCheckDateTime = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Show change log tool strip button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainAutoUpdaterShowChangeLogToolStripButton_Click(object sender, EventArgs e)
        {
            string logFileName = string.Empty;
            string logFilePath = string.Empty;

            try
            {
                logFilePath = Path.Combine(this.SidekicksAutoUpdater.ChangeLogFilePath, this.SidekicksAutoUpdater.ChangeLogFileName);

                if (File.Exists(logFilePath) == false)
                {
                    MessageBox.Show(string.Format(ShowChangeLogFileErrorMessage_CouldNotLocateLogFile));
                    return;
                }

                System.Diagnostics.Process.Start(logFilePath);

            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Back tool strip button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackToolStripButton_Click(object sender, EventArgs e)
        {
            SidekickViewHistory sidekickViewHistory = null;

            try
            {
                if (this.SidekickViewHistoryList.IsBackAvailable() == true)
                {
                    sidekickViewHistory = this.SidekickViewHistoryList.Back();
                    this.ActivateSidekickView(sidekickViewHistory.SidekickView);
                }

            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Back tool strip button - Mouse down.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackToolStripButton_MouseDown(object sender, MouseEventArgs e)
        {

            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    this.ShowSidekickViewHistoryListContextMenu();
                }
            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Forward tool strip button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ForwardToolStripButton_Click(object sender, EventArgs e)
        {
            SidekickViewHistory sidekickViewHistory = null;

            try
            {
                if (this.SidekickViewHistoryList.IsForwardAvailable() == true)
                {
                    sidekickViewHistory = this.SidekickViewHistoryList.Forward();
                    this.ActivateSidekickView(sidekickViewHistory.SidekickView);
                }
            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Forward tool strip button - Mouse down.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ForwardToolStripButton_MouseDown(object sender, MouseEventArgs e)
        {

            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    this.ShowSidekickViewHistoryListContextMenu();
                }
            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Help sidekicks auto-updater info tool strip menu item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpSidekicksAutoUpdaterInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InfoForm infoForm = null;

            try
            {
                infoForm = new InfoForm();
                infoForm.Information = Types.Information.AutoUpdater;
                infoForm.SidekicksAutoUpdater = this.SidekicksAutoUpdater;
                infoForm.Text = Information_SidekicksAutoUpdater;
                infoForm.ShowDialog();
            }
            catch (Exception ex)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(ex);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

    }

    /// <summary>
    /// Panel control extensions.
    /// </summary>
    public static class PanelExtensions
    {
        /// <summary>
        /// Dock control.
        /// </summary>
        /// <param name="thisControl"></param>
        /// <param name="controlToDock"></param>
        public static void DockControl(this Panel thisControl,
                                       Control controlToDock)
        {
            thisControl.Controls.Clear();
            thisControl.Controls.Add(controlToDock);
            controlToDock.Dock = DockStyle.Fill;
            controlToDock.Show();
        }
    }
}
