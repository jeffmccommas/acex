﻿using Aclara.Sidekicks.AutoUpdater;
using Aclara.Azure.Automation.Sidekicks.WinForm.Types;
using System;
using System.Windows.Forms;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public partial class KillSwitchNotifyForm : Form
    {

        #region Private Constants

        private const string FormTitle_Prefix_DoNotRun = "Application Critical Issue";
        private const string FormTitle_Prefix_Warning = "Application Warning";

        private const string UserPrompt_DoNotRun_UpdateAvailable = "An issue has been identified. Click Update Now to install a new version or OK to exit application.";
        private const string UserPrompt_DoNotRun_UpdateNotAvailable = "An issue has been identified. No update is currently available. Click OK to exit application.";
        private const string UserPrompt_RunWithWarning_UpdateAvailable = "An issue has been identified.  Click Update Now to install a new version or OK to continue.";
        private const string UserPrompt_RunWithWarning_UpdateNotAvailable = "An issue has been identified. No update is currently available. Click OK to continue.";

        #endregion

        #region Private Data Members
        private string _title;

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private string _versionImpacted;
        private string _reason;
        private KillSwitchRunStatus _killSwitchRunStatus;
        private SidekicksAutoUpdater _sidekicksAutoUpdater;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Version impacted.
        /// </summary>
        public string VersionImpacted
        {
            get { return _versionImpacted; }
            set { _versionImpacted = value; }
        }

        /// <summary>
        /// Property: Reason.
        /// </summary>
        public string Reason
        {
            get { return _reason; }
            set { _reason = value; }
        }

        /// <summary>
        /// Property: Kill switch run status.
        /// </summary>
        public KillSwitchRunStatus KillSwitchRunStatus
        {
            get { return _killSwitchRunStatus; }
            set { _killSwitchRunStatus = value; }
        }

        /// <summary>
        /// Property: Sidekick auto-updater.
        /// </summary>
        public SidekicksAutoUpdater SidekicksAutoUpdater
        {
            get { return _sidekicksAutoUpdater; }
            set { _sidekicksAutoUpdater = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="versionImpacted"></param>
        /// <param name="reason"></param>
        /// <param name="killSwitchRunStatus"></param>
        public KillSwitchNotifyForm(string title,
                                    string versionImpacted,
                                    string reason,
                                    KillSwitchRunStatus killSwitchRunStatus, 
                                    SidekicksAutoUpdater sidekickAutoUpdater)
        {
            _title = title;
            _versionImpacted = versionImpacted;
            _reason = reason;
            _killSwitchRunStatus = killSwitchRunStatus;
            _sidekicksAutoUpdater = sidekickAutoUpdater;

            InitializeComponent();
            this.InitializeControls();
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor is disabled.
        /// </summary>
        private KillSwitchNotifyForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods
        protected void InitializeControls()
        {
            try
            {
                this.VersionImpactedValueLabel.Text = this.VersionImpacted;
                this.ReasonTextBox.Text = this.Reason;

                switch (KillSwitchRunStatus)
                {
                    case KillSwitchRunStatus.DoNotRun:
                        this.Text = string.Format("{0} - {1}",
                                                  FormTitle_Prefix_DoNotRun,
                                                  this.Title);
                        if (this.SidekicksAutoUpdater.IsNewVersionAvailable(this.VersionImpacted) == true)
                        {
                            this.UserPromptValueLabel.Text = UserPrompt_DoNotRun_UpdateAvailable;
                            this.UpdateButton.Visible = true;
                        }
                        else
                        {
                            this.UserPromptValueLabel.Text = UserPrompt_DoNotRun_UpdateNotAvailable;
                            this.UpdateButton.Visible = false;
                        }
                        break;
                    case KillSwitchRunStatus.RunWithWarning:
                        this.Text = string.Format("{0} - {1}",
                                                  FormTitle_Prefix_Warning,
                                                  this.Title);
                        if (this.SidekicksAutoUpdater.IsNewVersionAvailable(this.VersionImpacted) == true)
                        {
                            this.UserPromptValueLabel.Text = UserPrompt_RunWithWarning_UpdateAvailable;
                            this.UpdateButton.Visible = true;
                        }
                        else
                        {
                            this.UserPromptValueLabel.Text = UserPrompt_RunWithWarning_UpdateNotAvailable;
                            this.UpdateButton.Visible = false;
                        }
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: OK button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OKButton_Click(object sender, EventArgs e)
        {

            try
            {
                switch (this.KillSwitchRunStatus)
                {
                    case KillSwitchRunStatus.DoNotRun:
                        Application.Exit();
                        break;
                    case KillSwitchRunStatus.RunWithWarning:
                        this.Close();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Update button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
       private void UpdateButton_Click(object sender, EventArgs e)
        {
            string stdout = string.Empty;
            string stderr = string.Empty;

            try
            {
                this.SidekicksAutoUpdater.Run(out stdout, out stderr);

                Application.Exit();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Kill switch notify form - Form closing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KillSwitchNotifyForm_FormClosing(object sender, FormClosingEventArgs e)
        {

            try
            {
                switch (this.KillSwitchRunStatus)
                {
                    case KillSwitchRunStatus.DoNotRun:
                        Application.Exit();
                        break;
                    case KillSwitchRunStatus.RunWithWarning:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Reason text box - Selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReasonTextBox_SelectionChanged(object sender, EventArgs e)
        {
            this.ReasonTextBox.SelectionLength = 0;
        }

        #endregion

    }
}
