﻿using Aclara.Azure.Automation.Client;
using Aclara.Azure.Automation.Client.Models;
using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Events;
using Aclara.Azure.Automation.Sidekicks.WinForm.Models;
using Aclara.Azure.Automation.Sidekicks.WinForm.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public partial class AzureCloudServiceForm : Form, ISidekickView, IAzureCloudServiceView, ISidekickInfo
    {

        #region Private Constants

        private const string SidekickInfo_SidekickName = "AzureCloudService";
        private const string SidekickInfo_SidekickDescription = "Azure Cloud Service";

        private const string AzureCloudServiceDataGridViewColumn_Selected = "Selected";
        private const string AzureCloudServiceDataGridViewColumn_Environment = "Environment";
        private const string AzureCloudServiceDataGridViewColumn_ServiceName = "ServiceName";
        private const string AzureCloudServiceDataGridViewColumn_Slot = "Slot";
        private const string AzureCloudServiceDataGridViewColumn_RoleInstanceCountScaleUp = "RoleInstanceCountScaleUp";
        private const string AzureCloudServiceDataGridViewColumn_RoleInstanceCountScaleDown = "RoleInstanceCountScaleDown";
        private const string AzureCloudServiceDataGridViewColumn_RoleInstanceCountScaleCustom = "RoleInstanceCountScaleCustom";
        private const string AzureCloudServiceDataGridViewColumn_RoleInstanceCount = "RoleInstanceCount";
        private const string AzureCloudServiceDataGridViewColumn_RoleName = "RoleName";
        private const string AzureCloudServiceDataGridViewColumn_VMSize = "VMSize";
        private const string AzureCloudServiceDataGridViewColumn_Extra = "Extra";

        private const string SidekickConfiguration_AzureCloudServiceInfo_SubscriptionName = "SubscriptionName";
        private const string SidekickConfiguration_AzureCloudServiceInfoInfo_EnvironmentName = "Name";
        private const string SidekickConfiguration_AzureCloudServiceInfoInfo_CloudService = "CloudService";

        private const string RefreshStatus_RetrievingAzureRoleInstanceInfo = "Retrieving Azure role instance data. Please wait...";
        private const string RefreshStatus_ScaleAzureCloudService = "Scaling Azure cloud service. Please wait...";

        private const string CloudServiceTag_Dev = "dev";
        private const string CloudServiceTag_Qa = "qa";
        private const string CloudServiceTag_Uat = "uat";
        private const string CloudServiceTag_Perf = "perf";
        private const string CloudServiceTag_Production = "prod";

        private const string ValidationErrorMessage_NoAzureCloudServicesSelected = "At least one Azure cloud service must be selected.";
        private const string ValidationErrorMessage_RefreshRequired = "Azure cloud service(s) require refresh. Click Refresh button.";
        private const string ValidationErrorMessage_ScaleCustomRequired = "Scale custom value(s) are required.";

        private const string ConfirmationMessage_Caption = "Scale Azure Cloud Services";
        private const string ConfirmationMessage_ScaleUp = "Are you sure you want to scale up selected cloud service(s)?";
        private const string ConfirmationMessage_ScaleDown = "Are you sure you want to scale down selected cloud service(s)?";
        private const string ConfirmationMessage_ScaleCustom = "Are you sure you want to custom scale selected cloud service(s)?";

        #endregion

        #region Private Data Members

        private AzureCloudServicePresenter _AzureCloudServicePresenter;
        private SidekickConfiguration _sidekickConfiguration = null;
        private SidekickComponentManagerForm _sidekickComponentManagerForm;
        private SidekickComponentAssistantForm _sidekickComponentAssistantForm;
        private bool _sidekickComponentsAvailable;
        private bool _componentsLoggedIn;
        private DateTime _sidekickLastActivatedDateTime = DateTime.MinValue;
        private AzureCloudServiceList _azureCloudServiceList;
        private System.Windows.Forms.SortOrder _azureCloudServiceDataGridViewSortOrder;
        private DataGridViewColumn _azureCloudServiceDataGridViewSortedColumn;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Azure Cloud Service presenter.
        /// </summary>
        public AzureCloudServicePresenter AzureCloudServicePresenter
        {
            get { return _AzureCloudServicePresenter; }
            set { _AzureCloudServicePresenter = value; }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Component manager form.
        /// </summary>
        public SidekickComponentManagerForm SidekickComponentManagerForm
        {
            get { return _sidekickComponentManagerForm; }
            set { _sidekickComponentManagerForm = value; }
        }

        /// <summary>
        /// Property: Sidekick component assistant form.
        /// </summary>
        public SidekickComponentAssistantForm SidekickComponentAssistantForm
        {
            get { return _sidekickComponentAssistantForm; }
            set { _sidekickComponentAssistantForm = value; }
        }

        /// <summary>
        /// Property: Sidekick components available.
        /// </summary>
        public bool SidekickComponentsAvailable
        {
            get { return _sidekickComponentsAvailable; }
            set { _sidekickComponentsAvailable = value; }
        }

        /// <summary>
        /// Property: Sidekick components logged in.
        /// </summary>
        public bool SidekickComponentsLoggedIn
        {
            get { return _componentsLoggedIn; }
            set { _componentsLoggedIn = value; }
        }

        /// <summary>
        /// Property: Sidekick last activated (date, time).
        /// </summary>
        public DateTime SidekickLastInitializationDataTime
        {
            get { return _sidekickLastActivatedDateTime; }
            set { _sidekickLastActivatedDateTime = value; }
        }

        /// <summary>
        /// Property:  Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get { return this.ApplyButton.Text; }
            set { this.ApplyButton.Text = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                if (this.ApplyButton.Text == "Apply")
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickInfo_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickInfo_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Azure cloud service list.
        /// </summary>
        public AzureCloudServiceList AzureCloudServiceList
        {
            get { return _azureCloudServiceList; }
            set { _azureCloudServiceList = value; }
        }

        /// <summary>
        /// Property: Azure cloud service data grid view sort order.
        /// </summary>
        public System.Windows.Forms.SortOrder AzureCloudServiceDataGridViewSortOrder
        {
            get { return _azureCloudServiceDataGridViewSortOrder; }
            set { _azureCloudServiceDataGridViewSortOrder = value; }
        }

        /// <summary>
        /// Property: Azure cloud service data grid view sorted column.
        /// </summary>
        public DataGridViewColumn AzureCloudServiceDataGridViewSortedColumn
        {
            get { return _azureCloudServiceDataGridViewSortedColumn; }
            set { _azureCloudServiceDataGridViewSortedColumn = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <param></param>
        public AzureCloudServiceForm(SidekickConfiguration sidekickConfiguration)
        {
            _sidekickConfiguration = sidekickConfiguration;
            _azureCloudServiceList = new AzureCloudServiceList();

            InitializeComponent();

            this.InitializeControls();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Sidekick was activated.
        /// </summary>
        public void SidekickActivated()
        {
            const int TotalTimeSinceLastSidekickInitialization = 180;

            TimeSpan timeSinceLastSidekickInitializeation;
            List<SidekickComponentId> sidekickComponentIdList;

            try
            {
                this.SidekickComponentManagerForm.SidekickComponentValidationCompleted += OnSidekickComponentValidationCompleted;

                EnableControlsDependantOnValidation();

                timeSinceLastSidekickInitializeation = (this.SidekickLastInitializationDataTime - DateTime.Now).Duration();

                if (this.SidekickLastInitializationDataTime == DateTime.MinValue ||
                    this.SidekickLastInitializationDataTime == DateTime.MaxValue ||
                    timeSinceLastSidekickInitializeation.TotalMinutes > TotalTimeSinceLastSidekickInitialization ||
                    this.SidekickComponentsAvailable == false ||
                    this.SidekickComponentsLoggedIn == false)
                {

                    this.SidekickActivationStatusPanel.Visible = true;
                    this.SidekickActivationStatusPanel.DockControl(this.SidekickComponentManagerForm);

                    this.OptionPanel.Visible = false;
                    this.AzureCloudServiceDataGridView.Visible = false;
                    
                    sidekickComponentIdList = new List<SidekickComponentId>();
                    sidekickComponentIdList.Add(SidekickComponentId.AzurePowerShell);

                    //Validate required sidekick components.
                    this.SidekickComponentManagerForm.SidekickComponentIdList = sidekickComponentIdList;

                    this.SidekickLastInitializationDataTime = DateTime.Now;
                }

                this.ColorizeAllAzureCloudService();

                this.EnableControlsDependantOnBackgroundTask();
                this.EnableControlsDependantOnValidation();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Apply button enable. 
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
                this.BackgroundTaskProgressBar.Visible = false;

            }
        }

        /// <summary>
        /// Populate Azure cloud service (data grid view).
        /// </summary>
        public void PopulateAzureCloudService()
        {
            SortableBindingList<AzureCloudService> azureCloudServiceListBindingSource = null;
            ListSortDirection sortDirection;
            List<string> cloudServiceTagMatchList = null;

            try
            {

                this.AzureCloudServiceDataGridViewSortOrder = this.AzureCloudServiceDataGridView.SortOrder;
                this.AzureCloudServiceDataGridViewSortedColumn = this.AzureCloudServiceDataGridView.SortedColumn;

                this.AzureCloudServiceDataGridView.DataSource = null;
                azureCloudServiceListBindingSource = new SortableBindingList<AzureCloudService>(this.AzureCloudServiceList);
                this.AzureCloudServiceDataGridView.DataSource = azureCloudServiceListBindingSource;

                foreach (DataGridViewColumn column in this.AzureCloudServiceDataGridView.Columns)
                {
                    if (column.Name == AzureCloudServiceDataGridViewColumn_Selected)
                    {
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        column.HeaderText = string.Empty;
                    }
                    else if (column.Name == AzureCloudServiceDataGridViewColumn_Environment)
                    {
                        column.ReadOnly = true;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    }
                    else if (column.Name == AzureCloudServiceDataGridViewColumn_ServiceName)
                    {
                        column.ReadOnly = true;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    }
                    else if (column.Name == AzureCloudServiceDataGridViewColumn_Slot)
                    {
                        column.ReadOnly = true;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    }
                    else if (column.Name == AzureCloudServiceDataGridViewColumn_RoleInstanceCountScaleUp)
                    {
                        column.ReadOnly = true;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    }
                    else if (column.Name == AzureCloudServiceDataGridViewColumn_RoleInstanceCountScaleDown)
                    {
                        column.ReadOnly = true;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    }
                    else if (column.Name == AzureCloudServiceDataGridViewColumn_RoleInstanceCountScaleCustom)
                    {
                        column.ReadOnly = false;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    }
                    else if (column.Name == AzureCloudServiceDataGridViewColumn_RoleInstanceCount)
                    {
                        column.ReadOnly = true;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    }
                    else if (column.Name == AzureCloudServiceDataGridViewColumn_RoleName)
                    {
                        column.ReadOnly = true;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    }
                    else if (column.Name == AzureCloudServiceDataGridViewColumn_VMSize)
                    {
                        column.ReadOnly = true;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    }
                    else if (column.Name == AzureCloudServiceDataGridViewColumn_Extra)
                    {
                        column.ReadOnly = true;
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        column.HeaderText = string.Empty;
                    }
                }

                //Restore sort order.
                if (this.AzureCloudServiceDataGridViewSortOrder == SortOrder.Ascending)
                {
                    sortDirection = ListSortDirection.Ascending;
                }
                else
                {
                    sortDirection = ListSortDirection.Descending;
                }

                if (this.AzureCloudServiceDataGridViewSortedColumn != null)
                {
                    this.AzureCloudServiceDataGridView.Sort(this.AzureCloudServiceDataGridView.Columns[this.AzureCloudServiceDataGridViewSortedColumn.Name], sortDirection); ;
                }

                //Display selected environments.
                cloudServiceTagMatchList = this.GetCloudServiceDisplayTagMatchList();
                if (cloudServiceTagMatchList == null ||
                    cloudServiceTagMatchList.Any() == false)
                {
                    this.CloudServiceDisplayAll();
                }
                else
                {
                    this.ToggleDisplayCloudServiceWithMatchingTag(cloudServiceTagMatchList);
                }

                this.ColorizeAllAzureCloudService();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Update Azure cloud service status.
        /// </summary>
        /// <param name="message"></param>
        public void UpdateAzureCloudServiceStatus(string message, int processedCount, int totalCount)
        {
            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke(new Action<string, int, int>(this.UpdateAzureCloudServiceStatus), message, processedCount, totalCount);
                }
                else
                {
                    this.AzureCloudServiceStatusLabel.Text = message;
                    this.AzureCloudServiceStatusLabel.Invalidate();
                    this.AzureCloudServiceStatusLabel.Update();
                    this.AzureCloudServiceStatusLabel.Refresh();
                    if (processedCount > 0 && 
                        totalCount > 0 &&
                        processedCount <= totalCount)
                    {
                        this.BackgroundTaskProgressBar.Visible = true;
                        this.BackgroundTaskProgressBar.Maximum = totalCount;
                        this.BackgroundTaskProgressBar.Value = processedCount;
                    }
                    Application.DoEvents();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update Azure cloud service list.
        /// </summary>
        /// <param name="azureCloudServiceShallow"></param>
        public void UpdateAzureCloudServiceList(AzureCloudServiceShallow azureCloudServiceShallow)
        {
            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke(new Action<AzureCloudServiceShallow>(this.UpdateAzureCloudServiceList), azureCloudServiceShallow);
                }
                else
                {
                    var azureCloudService = this.AzureCloudServiceList.Where(acs => acs.ServiceName == azureCloudServiceShallow.ServiceName).Single();

                    azureCloudService.RoleName = azureCloudServiceShallow.RoleName;
                    azureCloudService.RoleInstanceCount = azureCloudServiceShallow.RoleInstanceCount;
                    azureCloudService.VMSize = azureCloudServiceShallow.VMSize;
                    this.PopulateAzureCloudService();

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Enable/disable controls dependant on background task.
        /// </summary>
        public void EnableControlsDependantOnBackgroundTask()
        {

            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        EnableControlsDependantOnBackgroundTask();
                    });
                }
                else
                {

                    if (this.AzureCloudServicePresenter.IsBackgroundTaskRunning() == true)
                    {
                        this.ScaleSplitButton.Enabled = false;
                        this.RefreshButton.Enabled = false;
                        this.CancelButton.Enabled = true;
                    }
                    else
                    {
                        this.ScaleSplitButton.Enabled = true;
                        this.RefreshButton.Enabled = true;
                        this.CancelButton.Enabled = false;
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            BindingSource azureCloudServiceInfoBindingSource = null;

            try
            {

                this.SidekickActivationStatusPanel.Visible = false;
                this.OptionPanel.Visible = false;
                this.AzureCloudServiceDataGridView.Visible = false;

                this.PropertiesHeaderLabel.Text = this.SidekickDescription;
                this.AzureSubscriptionNameComboBox.FormattingEnabled = true;
                azureCloudServiceInfoBindingSource = new BindingSource();
                azureCloudServiceInfoBindingSource.DataSource = this.SidekickConfiguration.AzureCloudServiceInfo.AzureSubscription.ToList();

                this.AzureSubscriptionNameComboBox.DataSource = azureCloudServiceInfoBindingSource;
                this.AzureSubscriptionNameComboBox.DisplayMember = SidekickConfiguration_AzureCloudServiceInfo_SubscriptionName;
                this.AzureSubscriptionNameComboBox.ValueMember = SidekickConfiguration_AzureCloudServiceInfo_SubscriptionName;

                this.BackgroundTaskProgressBar.Visible = false;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Enable/disable controls dependant on validation.
        /// </summary>
        protected void EnableControlsDependantOnValidation()
        {

            try
            {
                if (this.SidekickComponentsAvailable == false ||
                    this.SidekickComponentsLoggedIn == false)
                {
                    this.AzureSubscriptionNameComboBox.Enabled = false;
                    this.AzureCloudServiceDataGridView.Enabled = false;
                    this.CheckSplitButton.Enabled = false;
                    this.DisplaySplitButton.Enabled = false;
                    this.RefreshButton.Enabled = false;
                    this.ScaleSplitButton.Enabled = false;
                    this.CancelButton.Enabled = false;
                    this.ApplyButton.Enabled = false;
                }
                else
                {
                    this.AzureSubscriptionNameComboBox.Enabled = true;
                    this.AzureCloudServiceDataGridView.Enabled = true;
                    this.CheckSplitButton.Enabled = true;
                    this.DisplaySplitButton.Enabled = true;
                    this.RefreshButton.Enabled = true;
                    this.AzureCloudServiceDataGridView.Enabled = true;
                    this.ScaleSplitButton.Enabled = true;
                    this.EnableControlsDependantOnBackgroundTask();
                    //CancelButton only enabled when background task is running.
                    //this.CancelButton.Enabled = true;
                    this.ApplyButton.Enabled = true;
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Reset build definition check tool strip items - checked state.
        /// </summary>
        protected void ResetCloudServiceCheckToolStripItemCheckedState()
        {


            if (this.CloudServiceCheckDevToolStripItem.Checked == true)
            {
                this.CloudServiceCheckDevToolStripItem.Checked = false;
            }

            if (this.CloudServiceCheckQAToolStripItem.Checked == true)
            {
                this.CloudServiceCheckQAToolStripItem.Checked = false;
            }

            if (this.CloudServiceCheckUATToolStripItem.Checked == true)
            {
                this.CloudServiceCheckUATToolStripItem.Checked = false;
            }

            if (this.CloudServiceCheckPerfToolStripItem.Checked == true)
            {
                this.CloudServiceCheckPerfToolStripItem.Checked = false;
            }

            if (this.CloudServiceCheckProdToolStripItem.Checked == true)
            {
                this.CloudServiceCheckProdToolStripItem.Checked = false;
            }

        }

        /// <summary>
        /// Cloud service check all.
        /// </summary>
        protected void CloudServiceCheckAll()
        {
            AzureCloudService azureCloudService = null;
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ResetCloudServiceCheckToolStripItemCheckedState();

                foreach (DataGridViewRow dataGridViewRow in this.AzureCloudServiceDataGridView.Rows)
                {
                    azureCloudService = (AzureCloudService)dataGridViewRow.DataBoundItem;
                    if (dataGridViewRow.Visible == true)
                    {
                        dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_Selected].Value = true;
                    }
                }
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Cloud service check none.
        /// </summary>
        protected void CloudServiceCheckNone()
        {
            AzureCloudService azureCloudService = null;
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ResetCloudServiceCheckToolStripItemCheckedState();

                foreach (DataGridViewRow dataGridViewRow in this.AzureCloudServiceDataGridView.Rows)
                {
                    azureCloudService = (AzureCloudService)dataGridViewRow.DataBoundItem;
                    if (dataGridViewRow.Visible == true)
                    {
                        dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_Selected].Value = false;
                    }
                }
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Cloud service check under scaled.
        /// </summary>
        protected void CloudServiceCheckUnderScaled()
        {
            AzureCloudService azureCloudService = null;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                foreach (DataGridViewRow dataGridViewRow in this.AzureCloudServiceDataGridView.Rows)
                {
                    azureCloudService = (AzureCloudService)dataGridViewRow.DataBoundItem;
                    if (dataGridViewRow.Visible == true)
                    {
                        if (azureCloudService.RoleInstanceCount <= azureCloudService.RoleInstanceCountScaleDown)
                        {
                            dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_Selected].Value = true;
                        }
                        else
                        {
                            dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_Selected].Value = false;
                        }
                    }

                }
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Cloud service check over scaled.
        /// </summary>
        protected void CloudServiceCheckOverScaled()
        {
            AzureCloudService azureCloudService = null;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                foreach (DataGridViewRow dataGridViewRow in this.AzureCloudServiceDataGridView.Rows)
                {
                    azureCloudService = (AzureCloudService)dataGridViewRow.DataBoundItem;
                    if (dataGridViewRow.Visible == true)
                    {
                        if (azureCloudService.RoleInstanceCount > azureCloudService.RoleInstanceCountScaleDown)
                        {
                            dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_Selected].Value = true;
                        }
                        else
                        {
                            dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_Selected].Value = false;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Retrieve cloud service 'check' tag match list.
        /// </summary>
        /// <returns></returns>
        protected List<string> GetCloudServiceCheckTagMatchList()
        {
            List<string> result = null;

            try
            {
                result = new List<string>();

                //Non-production environments:
                if (this.CloudServiceCheckDevToolStripItem.Checked == true)
                {
                    result.Add(CloudServiceTag_Dev);
                }

                if (this.CloudServiceCheckQAToolStripItem.Checked == true)
                {
                    result.Add(CloudServiceTag_Qa);
                }

                if (this.CloudServiceCheckUATToolStripItem.Checked == true)
                {
                    result.Add(CloudServiceTag_Uat);
                }

                if (this.CloudServiceCheckPerfToolStripItem.Checked == true)
                {
                    result.Add(CloudServiceTag_Perf);
                }

                //Production environmnet:
                if (this.CloudServiceCheckProdToolStripItem.Checked == true)
                {
                    result.Add(CloudServiceTag_Production);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Toggle check cloud service with matching tag.
        /// </summary>
        /// <param name="matchTag"></param>
        protected void ToggleCheckCloudServiceWithMatchingTag(List<string> cloudServiceTagMatchList)
        {
            AzureCloudService azureCloudService = null;
            DataGridViewCell dataGridViewCell = null;
            string environment = string.Empty;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                foreach (DataGridViewRow dataGridViewRow in this.AzureCloudServiceDataGridView.Rows)
                {
                    dataGridViewCell = dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_Environment];

                    if (dataGridViewCell != null)
                    {
                        if (dataGridViewCell.Value != null)
                        {
                            environment = dataGridViewCell.Value.ToString();
                            azureCloudService =
                                (AzureCloudService)
                                    AzureCloudServiceDataGridView.Rows[dataGridViewRow.Index].DataBoundItem;

                            if (string.IsNullOrEmpty(environment) == false &&
                                cloudServiceTagMatchList.Any(environment.ToLower().Contains) == true)
                            {
                                dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_Selected].Value = true;
                            }
                            else
                            {
                                dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_Selected].Value = false;
                            }

                        }
                    }

                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Cloud service display all.
        /// </summary>
        protected void CloudServiceDisplayAll()
        {
            AzureCloudService azureCloudService = null;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                foreach (DataGridViewRow dataGridViewRow in this.AzureCloudServiceDataGridView.Rows)
                {
                    azureCloudService = (AzureCloudService)dataGridViewRow.DataBoundItem;
                    if (dataGridViewRow.Visible == false)
                    {
                        dataGridViewRow.Visible = true;
                    }
                }

                this.ColorizeAllAzureCloudService();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Reset build definition display tool strip items - checked state.
        /// </summary>
        protected void ResetCloudServiceDisplayToolStripItemCheckedState()
        {


            if (this.CloudServiceDisplayDevToolStripItem.Checked == true)
            {
                this.CloudServiceDisplayDevToolStripItem.Checked = false;
            }

            if (this.CloudServiceDisplayQAToolStripItem.Checked == true)
            {
                this.CloudServiceDisplayQAToolStripItem.Checked = false;
            }

            if (this.CloudServiceDisplayUATToolStripItem.Checked == true)
            {
                this.CloudServiceDisplayUATToolStripItem.Checked = false;
            }

            if (this.CloudServiceDisplayPerfToolStripItem.Checked == true)
            {
                this.CloudServiceDisplayPerfToolStripItem.Checked = false;
            }

            if (this.CloudServiceDisplayProdToolStripItem.Checked == true)
            {
                this.CloudServiceDisplayProdToolStripItem.Checked = false;
            }

        }

        /// <summary>
        /// Retrieve cloud service 'display' tag match list.
        /// </summary>
        /// <returns></returns>
        protected List<string> GetCloudServiceDisplayTagMatchList()
        {
            List<string> result = null;

            try
            {
                result = new List<string>();

                //Non-production environments:
                if (this.CloudServiceDisplayDevToolStripItem.Checked == true)
                {
                    result.Add(CloudServiceTag_Dev);
                }

                if (this.CloudServiceDisplayQAToolStripItem.Checked == true)
                {
                    result.Add(CloudServiceTag_Qa);
                }

                if (this.CloudServiceDisplayUATToolStripItem.Checked == true)
                {
                    result.Add(CloudServiceTag_Uat);
                }

                if (this.CloudServiceDisplayPerfToolStripItem.Checked == true)
                {
                    result.Add(CloudServiceTag_Perf);
                }

                //Production environmnet:
                if (this.CloudServiceDisplayProdToolStripItem.Checked == true)
                {
                    result.Add(CloudServiceTag_Production);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Toggle display cloud service with matching tag.
        /// </summary>
        /// <param name="matchTag"></param>
        protected void ToggleDisplayCloudServiceWithMatchingTag(List<string> cloudServiceTagMatchList)
        {
            AzureCloudService azureCloudService = null;
            DataGridViewCell dataGridViewCell = null;
            string environment = string.Empty;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                foreach (DataGridViewRow dataGridViewRow in this.AzureCloudServiceDataGridView.Rows)
                {
                    dataGridViewCell = dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_Environment];

                    if (dataGridViewCell != null)
                    {
                        if (dataGridViewCell.Value != null)
                        {
                            environment = dataGridViewCell.Value.ToString();
                        }
                    }
                    else
                    {
                        environment = string.Empty;
                    }

                    if (string.IsNullOrEmpty(environment) == false &&
                        cloudServiceTagMatchList.Any(environment.ToLower().Contains) == true)
                    {
                        dataGridViewRow.Visible = true;
                    }
                    else
                    {
                        CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[AzureCloudServiceDataGridView.DataSource];
                        currencyManager1.SuspendBinding();
                        dataGridViewRow.Visible = false;
                        currencyManager1.ResumeBinding();
                    }
                }

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Highlight all Azure cloud service(s) that require refresh.
        /// </summary>
        protected void ColorizeAllAzureCloudService()
        {
            try
            {
                foreach (DataGridViewRow dataGridViewRow in this.AzureCloudServiceDataGridView.Rows)
                {
                    this.ColorizeSingleAzureCloudService(dataGridViewRow);
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Highlight Azure cloud service(s) that require refresh.
        /// </summary>
        protected void ColorizeSingleAzureCloudService(DataGridViewRow dataGridViewRow)
        {
            AzureCloudService azureCloudService = null;

            try
            {
                DataGridViewCellStyle roleNameCellStyle = new DataGridViewCellStyle();

                DataGridViewCellStyle roleInstanceCountCellStyle = new DataGridViewCellStyle();

                DataGridViewCellStyle vmSizeCellStyle = new DataGridViewCellStyle();


                azureCloudService = (AzureCloudService)dataGridViewRow.DataBoundItem;

                //Role name:
                if (azureCloudService.RoleName == AzureCloudService.ValueNotAvailable)
                {
                    roleNameCellStyle.BackColor = Color.Gainsboro;
                    dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_RoleName].Style = roleNameCellStyle;
                }
                else
                {
                    roleNameCellStyle.BackColor = SystemColors.ControlLightLight;
                    dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_RoleName].Style = roleNameCellStyle;
                }

                //Role instance count:
                if (azureCloudService.RoleInstanceCount == 0)
                {
                    roleInstanceCountCellStyle.BackColor = Color.Gainsboro;
                    dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_RoleInstanceCount].Style = roleInstanceCountCellStyle;
                }
                else if (azureCloudService.RoleInstanceCount <= azureCloudService.RoleInstanceCountScaleDown)
                {
                    roleInstanceCountCellStyle.BackColor = Color.LightGreen;
                    dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_RoleInstanceCount].Style = roleInstanceCountCellStyle;

                }
                else if (azureCloudService.RoleInstanceCount > azureCloudService.RoleInstanceCountScaleDown)
                {
                    roleInstanceCountCellStyle.BackColor = Color.LightGoldenrodYellow;
                    dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_RoleInstanceCount].Style = roleInstanceCountCellStyle;
                    if (azureCloudService.RoleInstanceCount > azureCloudService.RoleInstanceCountScaleUp)
                    {
                        roleInstanceCountCellStyle.BackColor = Color.Pink;
                        dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_RoleInstanceCount].Style = roleInstanceCountCellStyle;
                    }
                }
                else
                {
                    vmSizeCellStyle.BackColor = SystemColors.ControlLightLight;
                    dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_RoleInstanceCount].Style = vmSizeCellStyle;
                }

                //VM size:
                if (azureCloudService.VMSize == AzureCloudService.ValueNotAvailable)
                {
                    vmSizeCellStyle.BackColor = Color.Gainsboro;
                    dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_VMSize].Style = vmSizeCellStyle;
                }
                else
                {
                    vmSizeCellStyle.BackColor = SystemColors.ControlLightLight;
                    dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_VMSize].Style = vmSizeCellStyle;
                }


            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Event Hanlder: Sidekick component validation completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="sidekickComponentValidationCompletedEventArgs"></param>
        protected void OnSidekickComponentValidationCompleted(Object sender,
                                                              SidekickComponentValidationCompletedEventArgs sidekickComponentValidationCompletedEventArgs)
        {
            List<SidekickComponentId> SidekickComponentIdList;

            try
            {
                SidekickComponentIdList = new List<SidekickComponentId>();
                SidekickComponentIdList.Add(SidekickComponentId.AzurePowerShell);

                this.SidekickComponentsAvailable = this.SidekickComponentManagerForm.SidekickComponentManager.SidekickComponentList.AreComponentsAvailable(SidekickComponentIdList);

                if (this.SidekickComponentsAvailable == true)
                {
                    //Login required sidekick components.
                    this.SidekickComponentManagerForm.LoginRequiredComponents(SidekickComponentIdList);
                }
                else
                {
                    //Show sidekick component assistant.
                    this.SidekickComponentAssistantForm.Show();
                }

                this.SidekickComponentsLoggedIn = this.SidekickComponentManagerForm.SidekickComponentManager.SidekickComponentList.AreComponentsLoggedIn(SidekickComponentIdList);

                if (this.SidekickComponentsAvailable == true &&
                    this.SidekickComponentsLoggedIn == true)
                {
                    SidekickActivationStatusPanel.Visible = false;
                    this.OptionPanel.Visible = true;
                    this.AzureCloudServiceDataGridView.Visible = true;
                }

                this.EnableControlsDependantOnValidation();
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Format Build status data grid view as text.
        /// </summary>
        /// <returns></returns>
        protected string FormatAzureCloudServiceDataGridViewAsText()
        {

            const string EnvironmentHeading = "Environment";
            const string ServiceNameHeading = "Service Name";
            const string SlotHeading = "Slot";
            const string RoleInstanceCountScaleUpHeading = "Scale Up";
            const string RoleInstanceCountScaleDownHeading = "Scale Down";
            const string RoleInstanceCountScaleCustomHeading = "Scale Custom";
            const string RoleInstanceCountHeading = "Current";
            const string RoleNameHeading = "Role Name";
            const string VMSizeHeading = "VM Size";

            string result = string.Empty;
            StringBuilder formattedTextStringBuilder = null;
            AzureCloudService azureCloudService = null;
            bool selected = false;

            try
            {
                formattedTextStringBuilder = new StringBuilder();

                //Add header.
                formattedTextStringBuilder.AppendLine(string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}",
                                                                    EnvironmentHeading,
                                                                    ServiceNameHeading,
                                                                    SlotHeading,
                                                                    RoleInstanceCountScaleUpHeading,
                                                                    RoleInstanceCountScaleDownHeading,
                                                                    RoleInstanceCountScaleCustomHeading,
                                                                    RoleInstanceCountHeading,
                                                                    RoleNameHeading,
                                                                    VMSizeHeading));

                foreach (DataGridViewRow dataGridViewRow in this.AzureCloudServiceDataGridView.Rows)
                {

                    azureCloudService = (AzureCloudService)dataGridViewRow.DataBoundItem;

                    selected = (bool)dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_Selected].Value;

                    if (dataGridViewRow.Visible == false)
                    { continue; }
                    if (selected == false)
                    { continue; }

                    formattedTextStringBuilder.AppendLine(string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}",
                                                                        azureCloudService.Environment,
                                                                        azureCloudService.ServiceName,
                                                                        azureCloudService.Slot,
                                                                        azureCloudService.RoleInstanceCountScaleUp,
                                                                        azureCloudService.RoleInstanceCountScaleDown,
                                                                        azureCloudService.RoleInstanceCountScaleCustom,
                                                                        azureCloudService.RoleInstanceCount,
                                                                        azureCloudService.RoleName,
                                                                        azureCloudService.VMSize));

                }

                result = formattedTextStringBuilder.ToString();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

            return result;

        }

        /// <summary>
        /// Format Build status data grid view as html.
        /// </summary>
        /// <returns></returns>
        protected string FormatBuildStatusDataGridViewAsHtml()
        {

            const string EnvironmentHeading = "Environment";
            const string ServiceNameHeading = "Service Name";
            const string SlotHeading = "Slot";
            const string RoleInstanceCountScaleUpHeading = "Scale Up";
            const string RoleInstanceCountScaleDownHeading = "Scale Down";
            const string RoleInstanceCountScaleCustomHeading = "Scale Custom";
            const string RoleInstanceCountHeading = "Current";
            const string RoleNameHeading = "Role Name";
            const string VMSizeHeading = "VM Size";
            const string ColorWhite = "#FFFFFF";
            const string ColorGreen = "#A9DFBF";
            const string ColorBlue = "#D6EAF8";
            const string ColorYellow = "#FCF3CF";
            const string ColorGray = "";

            string result = string.Empty;
            StringBuilder formattedTextStringBuilder = null;
            AzureCloudService azureCloudService = null;
            string azureCloudServiceClass = string.Empty;
            bool selected = false;

            try
            {
                formattedTextStringBuilder = new StringBuilder();

                formattedTextStringBuilder.AppendLine("<html>");

                formattedTextStringBuilder.AppendLine("<style type='text//css'>");
                formattedTextStringBuilder.AppendLine(".tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}");
                formattedTextStringBuilder.AppendLine(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}");
                formattedTextStringBuilder.AppendLine(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}");
                formattedTextStringBuilder.AppendLine(".tg .tg-7fle_gray{font-weight:bold;background-color:#efefef;text-align:center;vertical-align:top}");
                formattedTextStringBuilder.AppendLine(".tg .tg-q9qv_yellow{ background-color:#FFFFE0;vertical-align:top}");
                formattedTextStringBuilder.AppendLine(".tg .tg-a080_green{background-color:#9aff99;vertical-align:top}");
                formattedTextStringBuilder.AppendLine(".tg .tg-c57o_blue{background-color:#ecf4ff;vertical-align:top}");
                formattedTextStringBuilder.AppendLine(".tg .tg-3we0_white{background-color:#ffffff;vertical-align:top}");
                formattedTextStringBuilder.AppendLine(".tg .tg-7kg1_pink{background-color:#FFC0CB;vertical-align:top}");
                formattedTextStringBuilder.AppendLine("</style> ");


                formattedTextStringBuilder.AppendLine("<table class='tg'>");
                formattedTextStringBuilder.AppendLine("<tr>");


                //Add header.
                formattedTextStringBuilder.AppendLine(string.Format("<th class='tg-7fle_gray'>{0}<//th><th class='tg-7fle_gray'>{1}<//th><th class='tg-7fle_gray'>{2}<//th><th class='tg-7fle_gray'>{3}<//th><th class='tg-7fle_gray'>{4}<//th><th class='tg-7fle_gray'>{5}<//th><th class='tg-7fle_gray'>{6}<//th><th class='tg-7fle_gray'>{7}<//th><th class='tg-7fle_gray'>{8}<//th>",
                                                                    EnvironmentHeading,
                                                                    ServiceNameHeading,
                                                                    SlotHeading,
                                                                    RoleInstanceCountScaleUpHeading,
                                                                    RoleInstanceCountScaleDownHeading,
                                                                    RoleInstanceCountScaleCustomHeading,
                                                                    RoleInstanceCountHeading,
                                                                    RoleNameHeading,
                                                                    VMSizeHeading));
                formattedTextStringBuilder.AppendLine("</tr>");

                foreach (DataGridViewRow dataGridViewRow in this.AzureCloudServiceDataGridView.Rows)
                {

                    selected = (bool)dataGridViewRow.Cells[AzureCloudServiceDataGridViewColumn_Selected].Value;

                    if (dataGridViewRow.Visible == false)
                    { continue; }
                    if (selected == false)
                    { continue; }

                    azureCloudService = (AzureCloudService)dataGridViewRow.DataBoundItem;
                    formattedTextStringBuilder.AppendLine("<tr>");
                    if (azureCloudService.RoleInstanceCount <= azureCloudService.RoleInstanceCountScaleDown)
                    {
                        azureCloudServiceClass = "tg-a080_green";
                    }
                    else if (azureCloudService.RoleInstanceCount > azureCloudService.RoleInstanceCountScaleDown)
                    {
                        azureCloudServiceClass = "tg-q9qv_yellow";
                        if (azureCloudService.RoleInstanceCount > azureCloudService.RoleInstanceCountScaleUp)
                        {
                            azureCloudServiceClass = "tg-7kg1_pink";
                        }

                    }
                    else if (azureCloudService.RoleInstanceCount == 0)
                    {
                        azureCloudServiceClass = "tg-7fle_gray";
                    }
                    else
                    {
                        azureCloudServiceClass = "tg-3we0_white";
                    }
                    formattedTextStringBuilder.AppendLine(string.Format("<td class='tg-3we0_white'>{0}<//td><td class='tg-3we0_white'>{1}<//td><td class='tg-3we0_white'>{2}<//td><td class='tg-3we0_white'>{3}<//td><td class='tg-3we0_white'>{4}<//td><td class='tg-3we0_white'>{5}<//td><td class='{9}'>{6}<//td><td class='tg-3we0_white'>{7}<//td><td class='tg-3we0_white'>{8}<//td>",
                                                                        azureCloudService.Environment,
                                                                        azureCloudService.ServiceName,
                                                                        azureCloudService.Slot,
                                                                        azureCloudService.RoleInstanceCountScaleUp,
                                                                        azureCloudService.RoleInstanceCountScaleDown,
                                                                        azureCloudService.RoleInstanceCountScaleCustom,
                                                                        azureCloudService.RoleInstanceCount,
                                                                        azureCloudService.RoleName,
                                                                        azureCloudService.VMSize,
                                                                        azureCloudServiceClass));
                    formattedTextStringBuilder.AppendLine("</tr>");
                }

                formattedTextStringBuilder.AppendLine("</table>");
                formattedTextStringBuilder.AppendLine("</html>");

                result = formattedTextStringBuilder.ToString();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

            return result;

        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Obsolete("Apply button is not needed.")]
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            string powerShellScript = string.Empty;

            try
            {
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Refresh cloud service button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshButton_Click(object sender, EventArgs e)
        {
            AzureCloudServiceInfo_AzureSubscription azureSubscription = null;
            AzureCloudServiceList selectedAzureCloudServiceList = null;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                azureSubscription = (AzureCloudServiceInfo_AzureSubscription)this.AzureSubscriptionNameComboBox.SelectedItem;

                selectedAzureCloudServiceList = this.AzureCloudServiceList.Where(acs => acs.Selected == true).ToAzureCloudServiceList();

                selectedAzureCloudServiceList.ForEach(acs => acs.ResetAzureCloudServiceProperties());
                this.ColorizeAllAzureCloudService();

                //Validation:
                if (selectedAzureCloudServiceList.Count == 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_NoAzureCloudServicesSelected));
                    return;
                }

                this.AzureCloudServicePresenter.GetAzureCloudService(azureSubscription.SubscriptionName, selectedAzureCloudServiceList.ConvertToAzureCloudServiceShallowList());

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Event  Handler: Azure subscription name combo box - selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AzureSubscriptionNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            AzureCloudServiceInfo_AzureSubscription azureSubscription = null;
            AzureCloudService azureCloudService = null;
            List<AzureCloudServiceInfo_Environment> environmentList = null;
            List<AzureCloudServiceInfo_CloudService> cloudServiceList = null;

            try
            {

                azureSubscription = (AzureCloudServiceInfo_AzureSubscription)this.AzureSubscriptionNameComboBox.SelectedItem;
                environmentList = this.SidekickConfiguration.AzureCloudServiceInfo.AzureSubscription.Where(asub => asub.SubscriptionName == azureSubscription.SubscriptionName).Single().Environment.ToList();
                cloudServiceList = this.SidekickConfiguration.AzureCloudServiceInfo.AzureSubscription.Where(asub => asub.SubscriptionName == azureSubscription.SubscriptionName).Single().Environment.SelectMany(env => env.CloudService).ToList();

                this.AzureCloudServiceList.Clear();

                foreach (AzureCloudServiceInfo_Environment environment in environmentList)
                {

                    foreach (AzureCloudServiceInfo_CloudService cloudService in environment.CloudService)
                    {
                        azureCloudService = new AzureCloudService();
                        azureCloudService.ServiceName = cloudService.ServiceName;
                        azureCloudService.Slot = cloudService.Slot;
                        azureCloudService.Environment = environment.Name;
                        azureCloudService.RoleInstanceCountScaleUp = cloudService.InstanceCountScaleUp;
                        azureCloudService.RoleInstanceCountScaleDown = cloudService.InstanceCountScaleDown;
                        this.AzureCloudServiceList.Add(azureCloudService);
                    }
                }
                this.PopulateAzureCloudService();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Azure cloud service data grid view - Cell value changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AzureCloudServiceDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            AzureCloudService azureCloudService = null;
            DataGridViewCheckBoxCell dataGridViewCheckBoxCell = null;

            try
            {
                if (AzureCloudServiceDataGridView.Columns[e.ColumnIndex].Name == AzureCloudServiceDataGridViewColumn_Selected && e.RowIndex >= 0 && e.RowIndex < AzureCloudServiceDataGridView.Rows.Count)
                {
                    azureCloudService = (AzureCloudService)AzureCloudServiceDataGridView.Rows[e.RowIndex].DataBoundItem;

                    dataGridViewCheckBoxCell = (DataGridViewCheckBoxCell)AzureCloudServiceDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];

                    if ((bool)dataGridViewCheckBoxCell.Value == true)
                    {
                        azureCloudService.Selected = true;
                    }
                    else
                    {
                        azureCloudService.Selected = false;
                    }

                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Azure cloud service data grid view - cell content click changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AzureCloudServiceDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                AzureCloudServiceDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Azure cloud service data grid view - cell click changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AzureCloudServiceDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                AzureCloudServiceDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Azure cloud service data grid view - selection changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AzureCloudServiceDataGridView_SelectionChanged(object sender, EventArgs e)
        {

            try
            {

                this.AzureCloudServiceDataGridView.ClearSelection();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Cloud service check all tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloudServiceCheckAllToolStripItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.CloudServiceCheckAll();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Cloud service check none tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloudServiceCheckNoneToolStripItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.CloudServiceCheckNone();

            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Cloud service check under scaled tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloudServiceCheckUnderScaledToolStripItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.CloudServiceCheckNone();
                this.CloudServiceCheckUnderScaled();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Cloud service check over scaled tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloudServiceCheckOverScaledToolStripItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.CloudServiceCheckNone();
                this.CloudServiceCheckOverScaled();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Cloud service check dev tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloudServiceCheckDevToolStripItem_Click(object sender, EventArgs e)
        {
            List<string> cloudServiceTagMatchList = null;
            try
            {
                cloudServiceTagMatchList = this.GetCloudServiceCheckTagMatchList();
                if (cloudServiceTagMatchList == null ||
                    cloudServiceTagMatchList.Any() == false)
                {
                    CloudServiceCheckNone();
                    return;
                }

                this.ToggleCheckCloudServiceWithMatchingTag(cloudServiceTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        /// <summary>
        /// Event Handler: Cloud service check qa tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloudServiceCheckQAToolStripItem_Click(object sender, EventArgs e)
        {
            List<string> cloudServiceTagMatchList = null;
            try
            {
                cloudServiceTagMatchList = this.GetCloudServiceCheckTagMatchList();
                if (cloudServiceTagMatchList == null ||
                    cloudServiceTagMatchList.Any() == false)
                {
                    CloudServiceCheckNone();
                    return;
                }

                this.ToggleCheckCloudServiceWithMatchingTag(cloudServiceTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        /// <summary>
        /// Event Handler: Cloud service check uat tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloudServiceCheckUATToolStripItem_Click(object sender, EventArgs e)
        {
            List<string> cloudServiceTagMatchList = null;
            try
            {
                cloudServiceTagMatchList = this.GetCloudServiceCheckTagMatchList();
                if (cloudServiceTagMatchList == null ||
                    cloudServiceTagMatchList.Any() == false)
                {
                    CloudServiceCheckNone();
                    return;
                }

                this.ToggleCheckCloudServiceWithMatchingTag(cloudServiceTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        /// <summary>
        /// Event Handler: Cloud service check perf tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloudServiceCheckPerfToolStripItem_Click(object sender, EventArgs e)
        {
            List<string> cloudServiceTagMatchList = null;
            try
            {
                cloudServiceTagMatchList = this.GetCloudServiceCheckTagMatchList();
                if (cloudServiceTagMatchList == null ||
                    cloudServiceTagMatchList.Any() == false)
                {
                    CloudServiceCheckNone();
                    return;
                }

                this.ToggleCheckCloudServiceWithMatchingTag(cloudServiceTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        /// <summary>
        /// Event Handler: Cloud service check perf tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloudServiceCheckProdToolStripItem_Click(object sender, EventArgs e)
        {
            List<string> cloudServiceTagMatchList = null;
            try
            {
                cloudServiceTagMatchList = this.GetCloudServiceCheckTagMatchList();
                if (cloudServiceTagMatchList == null ||
                    cloudServiceTagMatchList.Any() == false)
                {
                    CloudServiceCheckNone();
                    return;
                }

                this.ToggleCheckCloudServiceWithMatchingTag(cloudServiceTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void CloudServiceDisplayAllToolStripItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.ResetCloudServiceDisplayToolStripItemCheckedState();

                this.CloudServiceDisplayAll();
            }
            catch (Exception exception)
            {
                this.Cursor = Cursors.Default;
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Event Handler: Cloud service display dev tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloudServiceDisplayDevToolStripItem_Click(object sender, EventArgs e)
        {
            List<string> cloudServiceTagMatchList = null;
            try
            {
                cloudServiceTagMatchList = this.GetCloudServiceDisplayTagMatchList();
                if (cloudServiceTagMatchList == null ||
                    cloudServiceTagMatchList.Any() == false)
                {
                    CloudServiceDisplayAll();
                    return;
                }

                this.ToggleDisplayCloudServiceWithMatchingTag(cloudServiceTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Cloud service display QA tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        private void CloudServiceDisplayQAToolStripItem_Click(object sender, EventArgs e)
        {
            List<string> cloudServiceTagMatchList = null;
            try
            {
                cloudServiceTagMatchList = this.GetCloudServiceDisplayTagMatchList();
                if (cloudServiceTagMatchList == null ||
                    cloudServiceTagMatchList.Any() == false)
                {
                    CloudServiceDisplayAll();
                    return;
                }

                this.ToggleDisplayCloudServiceWithMatchingTag(cloudServiceTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Cloud service display UAT tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        private void CloudServiceDisplayUATToolStripItem_Click(object sender, EventArgs e)
        {
            List<string> cloudServiceTagMatchList = null;
            try
            {
                cloudServiceTagMatchList = this.GetCloudServiceDisplayTagMatchList();
                if (cloudServiceTagMatchList == null ||
                    cloudServiceTagMatchList.Any() == false)
                {
                    CloudServiceDisplayAll();
                    return;
                }

                this.ToggleDisplayCloudServiceWithMatchingTag(cloudServiceTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Cloud service display Perf tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        private void CloudServiceDisplayPerfToolStripItem_Click(object sender, EventArgs e)
        {
            List<string> cloudServiceTagMatchList = null;
            try
            {
                cloudServiceTagMatchList = this.GetCloudServiceDisplayTagMatchList();
                if (cloudServiceTagMatchList == null ||
                    cloudServiceTagMatchList.Any() == false)
                {
                    CloudServiceDisplayAll();
                    return;
                }

                this.ToggleDisplayCloudServiceWithMatchingTag(cloudServiceTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Cloud service display Prod tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        private void CloudServiceDisplayProdToolStripItem_Click(object sender, EventArgs e)
        {
            List<string> cloudServiceTagMatchList = null;
            try
            {
                cloudServiceTagMatchList = this.GetCloudServiceDisplayTagMatchList();
                if (cloudServiceTagMatchList == null ||
                    cloudServiceTagMatchList.Any() == false)
                {
                    CloudServiceDisplayAll();
                    return;
                }

                this.ToggleDisplayCloudServiceWithMatchingTag(cloudServiceTagMatchList);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Cloud service scale up tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloudServiceCheckScaleUpToolStripItem_Click(object sender, EventArgs e)
        {
            AzureCloudServiceInfo_AzureSubscription azureSubscription = null;
            AzureCloudServiceList selectedAzureCloudServiceList = null;
            ScaleDirection scaleDirection;
            DialogResult confirmationDialogResult = DialogResult.No;
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                azureSubscription = (AzureCloudServiceInfo_AzureSubscription)this.AzureSubscriptionNameComboBox.SelectedItem;

                selectedAzureCloudServiceList = this.AzureCloudServiceList.Where(acs => acs.Selected == true).ToAzureCloudServiceList();

                //Validation:
                if (selectedAzureCloudServiceList.Count == 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_NoAzureCloudServicesSelected));
                    return;
                }

                if (selectedAzureCloudServiceList.Any(acs => acs.RefreshRequired == true))
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_RefreshRequired));
                    return;
                }

                confirmationDialogResult = MessageBox.Show(ConfirmationMessage_ScaleUp, ConfirmationMessage_Caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (confirmationDialogResult != DialogResult.Yes)
                {
                    return;
                }

                UpdateAzureCloudServiceStatus(RefreshStatus_ScaleAzureCloudService, 0, 0);

                scaleDirection = ScaleDirection.ScaleUp;
                this.AzureCloudServicePresenter.ScaleAzureCloudService(azureSubscription.SubscriptionName, selectedAzureCloudServiceList.ConvertToAzureCloudServiceShallowList(), scaleDirection);

                UpdateAzureCloudServiceStatus(string.Empty, 0, 0);

                selectedAzureCloudServiceList.ForEach(acs => acs.ResetAzureCloudServiceProperties());
                this.PopulateAzureCloudService();

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Cloud service scale down tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloudServiceCheckScaleDownToolStripItem_Click(object sender, EventArgs e)
        {
            AzureCloudServiceInfo_AzureSubscription azureSubscription = null;
            AzureCloudServiceList selectedAzureCloudServiceList = null;
            ScaleDirection scaleDirection;
            DialogResult confirmationDialogResult = DialogResult.No;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                azureSubscription = (AzureCloudServiceInfo_AzureSubscription)this.AzureSubscriptionNameComboBox.SelectedItem;

                selectedAzureCloudServiceList = this.AzureCloudServiceList.Where(acs => acs.Selected == true).ToAzureCloudServiceList();

                //Validation:
                if (selectedAzureCloudServiceList.Count == 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_NoAzureCloudServicesSelected));
                    return;
                }

                if (selectedAzureCloudServiceList.Any(acs => acs.RefreshRequired == true))
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_RefreshRequired));
                    return;
                }

                confirmationDialogResult = MessageBox.Show(ConfirmationMessage_ScaleDown, ConfirmationMessage_Caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (confirmationDialogResult != DialogResult.Yes)
                {
                    return;
                }


                UpdateAzureCloudServiceStatus(RefreshStatus_ScaleAzureCloudService, 0, 0);

                scaleDirection = ScaleDirection.ScaleDown;
                this.AzureCloudServicePresenter.ScaleAzureCloudService(azureSubscription.SubscriptionName,
                                                                       selectedAzureCloudServiceList.ConvertToAzureCloudServiceShallowList(),
                                                                       scaleDirection);

                UpdateAzureCloudServiceStatus(string.Empty, 0, 0);

                selectedAzureCloudServiceList.ForEach(acs => acs.ResetAzureCloudServiceProperties());
                this.PopulateAzureCloudService();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Cloud service scale custom tool strip item - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloudServiceCheckScaleCustomToolStripItem_Click(object sender, EventArgs e)
        {
            AzureCloudServiceInfo_AzureSubscription azureSubscription = null;
            AzureCloudServiceList selectedAzureCloudServiceList = null;
            ScaleDirection scaleDirection;
            DialogResult confirmationDialogResult = DialogResult.No;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                azureSubscription = (AzureCloudServiceInfo_AzureSubscription)this.AzureSubscriptionNameComboBox.SelectedItem;

                selectedAzureCloudServiceList = this.AzureCloudServiceList.Where(acs => acs.Selected == true).ToAzureCloudServiceList();

                //Validation:
                if (selectedAzureCloudServiceList.Count == 0)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_NoAzureCloudServicesSelected));
                    return;
                }

                if (selectedAzureCloudServiceList.Any(acs => acs.RefreshRequired == true))
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_RefreshRequired));
                    return;
                }

                if (selectedAzureCloudServiceList.Any(acs => acs.RoleInstanceCountScaleCustom == 0))
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_ScaleCustomRequired));
                    return;
                }

                confirmationDialogResult = MessageBox.Show(ConfirmationMessage_ScaleCustom, ConfirmationMessage_Caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (confirmationDialogResult != DialogResult.Yes)
                {
                    return;
                }

                UpdateAzureCloudServiceStatus(RefreshStatus_ScaleAzureCloudService, 0, 0);

                scaleDirection = ScaleDirection.ScaleCustom;
                this.AzureCloudServicePresenter.ScaleAzureCloudService(azureSubscription.SubscriptionName,
                                                                       selectedAzureCloudServiceList.ConvertToAzureCloudServiceShallowList(),
                                                                       scaleDirection);

                UpdateAzureCloudServiceStatus(string.Empty, 0, 0);

                selectedAzureCloudServiceList.ForEach(acs => acs.ResetAzureCloudServiceProperties());
                this.PopulateAzureCloudService();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event Handler: Cancel button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.AzureCloudServicePresenter.CancelBackgroundTask();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Azure cloud service data grid view - sorted.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AzureCloudServiceDataGridView_Sorted(object sender, EventArgs e)
        {
            List<string> cloudServiceTagMatchList = null;

            try
            {
                //Display selected environments.
                cloudServiceTagMatchList = this.GetCloudServiceDisplayTagMatchList();
                if (cloudServiceTagMatchList == null ||
                    cloudServiceTagMatchList.Any() == false)
                {
                    this.CloudServiceDisplayAll();
                }
                else
                {
                    this.ToggleDisplayCloudServiceWithMatchingTag(cloudServiceTagMatchList);
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickAzureCloudService.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Cloud service copy as text tool strip item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloudServiceCopyAsTextToolStripItem_Click(object sender, EventArgs e)
        {
            string azureCloudServiceFormattedAsText = string.Empty;

            try
            {
                azureCloudServiceFormattedAsText = FormatAzureCloudServiceDataGridViewAsText();

                Clipboard.SetDataObject(azureCloudServiceFormattedAsText);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Cloud service copy as html tool strip item - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloudServiceCopyAsHTMLToolStripItem_Click(object sender, EventArgs e)
        {
            string azureCloudServiceFormattedAsText = string.Empty;
            string azureCloudServiceFormattedAsHtml = string.Empty;

            try
            {
                azureCloudServiceFormattedAsText = FormatAzureCloudServiceDataGridViewAsText();
                azureCloudServiceFormattedAsHtml = FormatBuildStatusDataGridViewAsHtml();

                ClipboardHelper.CopyToClipboard(azureCloudServiceFormattedAsHtml, azureCloudServiceFormattedAsText);
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion

    }
}
