﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Models;
using Aclara.Azure.Automation.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public interface IAzureCloudServiceView
    {
        string SidekickName { get; }

        string SidekickDescription { get; }
        
        string BackgroundTaskStatus { get; set; }

        string ApplyButtonText { get; set; }

        void EnableControlsDependantOnBackgroundTask();

        void UpdateAzureCloudServiceStatus(string message, int processedCount, int totalCount);

        void UpdateAzureCloudServiceList ( AzureCloudServiceShallow azureCloudServiceShallow);

        void ApplyButtonEnable(bool enable);

        void BackgroundTaskCancelled(string backgroundTaskStatus);

        void BackgroundTaskCompleted(string backgroundTaskStatus);
    }
}
