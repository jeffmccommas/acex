﻿namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    partial class ReportPortalConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.DeleteDatasetPanel = new System.Windows.Forms.Panel();
            this.DeleteDatasetGroupBox = new System.Windows.Forms.GroupBox();
            this.DeleteDatasetRefreshDatasetsButton = new System.Windows.Forms.Button();
            this.DeleteDatasetWorkspaceIdComboBox = new System.Windows.Forms.ComboBox();
            this.DeleteDatasetDatasetListLabel = new System.Windows.Forms.Label();
            this.DeleteDatasetDatasetsListView = new System.Windows.Forms.ListView();
            this.DeleteDatasetRefreshWorkspacesButton = new System.Windows.Forms.Button();
            this.DeleteDatasetWorkspaceIdLabel = new System.Windows.Forms.Label();
            this.ImportPbixPanel = new System.Windows.Forms.Panel();
            this.ImportPbixGroupBox = new System.Windows.Forms.GroupBox();
            this.ImportPbixDatasetNameTextBox = new System.Windows.Forms.TextBox();
            this.ImportPbixDatasetNameLabel = new System.Windows.Forms.Label();
            this.ImportPbixRefreshDatasetsButton = new System.Windows.Forms.Button();
            this.ImportPbixWorkspaceIdComboBox = new System.Windows.Forms.ComboBox();
            this.ImportPbixDatasetsListView = new System.Windows.Forms.ListView();
            this.ImportPbixRefreshWorkspacesButton = new System.Windows.Forms.Button();
            this.PbixImportStatusLabel = new System.Windows.Forms.Label();
            this.OpenPbixFileButton = new System.Windows.Forms.Button();
            this.ImportPbixFilePathTextBox = new System.Windows.Forms.TextBox();
            this.ImportPbixFilePathLabel = new System.Windows.Forms.Label();
            this.ImportPbixDatasetsLabel = new System.Windows.Forms.Label();
            this.WorkspaceIdLabel = new System.Windows.Forms.Label();
            this.GetWorkspacesPanel = new System.Windows.Forms.Panel();
            this.GetWorkspacesGroupBox = new System.Windows.Forms.GroupBox();
            this.GetWorkspacesRefreshWorkspacesButton = new System.Windows.Forms.Button();
            this.GetWorkspacesClearButton = new System.Windows.Forms.Button();
            this.GetWorkspacesStatusLabel = new System.Windows.Forms.Label();
            this.GetWorkspacesCopyButton = new System.Windows.Forms.Button();
            this.GetWorkspacesWorkspaceIdListBox = new System.Windows.Forms.ListBox();
            this.GetWorkspacesWorkspacesLabel = new System.Windows.Forms.Label();
            this.GetDatasetsPanel = new System.Windows.Forms.Panel();
            this.GetDatasetsGroupBox = new System.Windows.Forms.GroupBox();
            this.GetDatasetsRefreshDatasetsButton = new System.Windows.Forms.Button();
            this.GetDatasetsWorkspaceIdComboBox = new System.Windows.Forms.ComboBox();
            this.GetDatasetsRefreshWorkspaceListButton = new System.Windows.Forms.Button();
            this.GetDatasetsDatasetsListView = new System.Windows.Forms.ListView();
            this.GetDatasetsStatusLabel = new System.Windows.Forms.Label();
            this.GetDatasetsClearButton = new System.Windows.Forms.Button();
            this.GetDatasetsCopyButton = new System.Windows.Forms.Button();
            this.GetDatasetsDatasetsLabel = new System.Windows.Forms.Label();
            this.GetDatasetsWorkspaceIdLabel = new System.Windows.Forms.Label();
            this.UpdateCredentialsPanel = new System.Windows.Forms.Panel();
            this.UpdateCredentialsGroupBox = new System.Windows.Forms.GroupBox();
            this.UpdateCredentialsWorkspaceIdComboBox = new System.Windows.Forms.ComboBox();
            this.UpdateCredentialsRefreshWorkspaceIdsButton = new System.Windows.Forms.Button();
            this.UpdateCredentialsPasswordTextBox = new System.Windows.Forms.TextBox();
            this.UpdateCredentialsPasswordLabel = new System.Windows.Forms.Label();
            this.UpdateCredentialsUsernameTextBox = new System.Windows.Forms.TextBox();
            this.UpdateCredentialsUsernameLabel = new System.Windows.Forms.Label();
            this.UpdateCredentialsWorkspaceIdLabel = new System.Windows.Forms.Label();
            this.CreateWorkspacePanel = new System.Windows.Forms.Panel();
            this.CreateWorkspaceGroupBox = new System.Windows.Forms.GroupBox();
            this.CreateWorkspaceWorkspaceIdTextBox = new System.Windows.Forms.TextBox();
            this.CreateWorkspaceWorkspaceLabel = new System.Windows.Forms.Label();
            this.SelectActionLabel = new System.Windows.Forms.Label();
            this.SelectActionComboBox = new System.Windows.Forms.ComboBox();
            this.ConnectionInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.WorkspaceCollectionComboBox = new System.Windows.Forms.ComboBox();
            this.WorkspaceCollectionLabel = new System.Windows.Forms.Label();
            this.EnvironmentLabel = new System.Windows.Forms.Label();
            this.EnvironmentComboBox = new System.Windows.Forms.ComboBox();
            this.AzureSubscriptionNameComboBox = new System.Windows.Forms.ComboBox();
            this.AzureSubscriptionNameLabel = new System.Windows.Forms.Label();
            this.GuidanceGroupBox = new System.Windows.Forms.GroupBox();
            this.HelpButton = new System.Windows.Forms.Button();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.SidekickToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.PropertiesPanel.SuspendLayout();
            this.DeleteDatasetPanel.SuspendLayout();
            this.DeleteDatasetGroupBox.SuspendLayout();
            this.ImportPbixPanel.SuspendLayout();
            this.ImportPbixGroupBox.SuspendLayout();
            this.GetWorkspacesPanel.SuspendLayout();
            this.GetWorkspacesGroupBox.SuspendLayout();
            this.GetDatasetsPanel.SuspendLayout();
            this.GetDatasetsGroupBox.SuspendLayout();
            this.UpdateCredentialsPanel.SuspendLayout();
            this.UpdateCredentialsGroupBox.SuspendLayout();
            this.CreateWorkspacePanel.SuspendLayout();
            this.CreateWorkspaceGroupBox.SuspendLayout();
            this.ConnectionInfoGroupBox.SuspendLayout();
            this.GuidanceGroupBox.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(0, 1);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(719, 17);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "Report Portal Configuration";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PropertiesPanel.Controls.Add(this.DeleteDatasetPanel);
            this.PropertiesPanel.Controls.Add(this.ImportPbixPanel);
            this.PropertiesPanel.Controls.Add(this.GetWorkspacesPanel);
            this.PropertiesPanel.Controls.Add(this.GetDatasetsPanel);
            this.PropertiesPanel.Controls.Add(this.UpdateCredentialsPanel);
            this.PropertiesPanel.Controls.Add(this.CreateWorkspacePanel);
            this.PropertiesPanel.Controls.Add(this.SelectActionLabel);
            this.PropertiesPanel.Controls.Add(this.SelectActionComboBox);
            this.PropertiesPanel.Controls.Add(this.ConnectionInfoGroupBox);
            this.PropertiesPanel.Controls.Add(this.AzureSubscriptionNameComboBox);
            this.PropertiesPanel.Controls.Add(this.AzureSubscriptionNameLabel);
            this.PropertiesPanel.Controls.Add(this.GuidanceGroupBox);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesPanel.MinimumSize = new System.Drawing.Size(560, 480);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(716, 506);
            this.PropertiesPanel.TabIndex = 3;
            // 
            // DeleteDatasetPanel
            // 
            this.DeleteDatasetPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DeleteDatasetPanel.Controls.Add(this.DeleteDatasetGroupBox);
            this.DeleteDatasetPanel.Location = new System.Drawing.Point(0, 251);
            this.DeleteDatasetPanel.Name = "DeleteDatasetPanel";
            this.DeleteDatasetPanel.Size = new System.Drawing.Size(716, 215);
            this.DeleteDatasetPanel.TabIndex = 12;
            // 
            // DeleteDatasetGroupBox
            // 
            this.DeleteDatasetGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DeleteDatasetGroupBox.Controls.Add(this.DeleteDatasetRefreshDatasetsButton);
            this.DeleteDatasetGroupBox.Controls.Add(this.DeleteDatasetWorkspaceIdComboBox);
            this.DeleteDatasetGroupBox.Controls.Add(this.DeleteDatasetDatasetListLabel);
            this.DeleteDatasetGroupBox.Controls.Add(this.DeleteDatasetDatasetsListView);
            this.DeleteDatasetGroupBox.Controls.Add(this.DeleteDatasetRefreshWorkspacesButton);
            this.DeleteDatasetGroupBox.Controls.Add(this.DeleteDatasetWorkspaceIdLabel);
            this.DeleteDatasetGroupBox.Location = new System.Drawing.Point(9, 13);
            this.DeleteDatasetGroupBox.Name = "DeleteDatasetGroupBox";
            this.DeleteDatasetGroupBox.Size = new System.Drawing.Size(704, 180);
            this.DeleteDatasetGroupBox.TabIndex = 0;
            this.DeleteDatasetGroupBox.TabStop = false;
            this.DeleteDatasetGroupBox.Text = "Delete Dataset";
            // 
            // DeleteDatasetRefreshDatasetsButton
            // 
            this.DeleteDatasetRefreshDatasetsButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
            this.DeleteDatasetRefreshDatasetsButton.Location = new System.Drawing.Point(555, 59);
            this.DeleteDatasetRefreshDatasetsButton.Name = "DeleteDatasetRefreshDatasetsButton";
            this.DeleteDatasetRefreshDatasetsButton.Size = new System.Drawing.Size(32, 23);
            this.DeleteDatasetRefreshDatasetsButton.TabIndex = 10;
            this.SidekickToolTip.SetToolTip(this.DeleteDatasetRefreshDatasetsButton, "Refresh");
            this.DeleteDatasetRefreshDatasetsButton.UseVisualStyleBackColor = true;
            this.DeleteDatasetRefreshDatasetsButton.Click += new System.EventHandler(this.DeleteDatasetRefreshDatasetsButton_Click);
            // 
            // DeleteDatasetWorkspaceIdComboBox
            // 
            this.DeleteDatasetWorkspaceIdComboBox.FormattingEnabled = true;
            this.DeleteDatasetWorkspaceIdComboBox.Location = new System.Drawing.Point(122, 24);
            this.DeleteDatasetWorkspaceIdComboBox.Name = "DeleteDatasetWorkspaceIdComboBox";
            this.DeleteDatasetWorkspaceIdComboBox.Size = new System.Drawing.Size(427, 21);
            this.DeleteDatasetWorkspaceIdComboBox.TabIndex = 9;
            // 
            // DeleteDatasetDatasetListLabel
            // 
            this.DeleteDatasetDatasetListLabel.AutoSize = true;
            this.DeleteDatasetDatasetListLabel.Location = new System.Drawing.Point(7, 55);
            this.DeleteDatasetDatasetListLabel.Name = "DeleteDatasetDatasetListLabel";
            this.DeleteDatasetDatasetListLabel.Size = new System.Drawing.Size(52, 13);
            this.DeleteDatasetDatasetListLabel.TabIndex = 8;
            this.DeleteDatasetDatasetListLabel.Text = "Datasets:";
            // 
            // DeleteDatasetDatasetsListView
            // 
            this.DeleteDatasetDatasetsListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.DeleteDatasetDatasetsListView.HideSelection = false;
            this.DeleteDatasetDatasetsListView.Location = new System.Drawing.Point(122, 57);
            this.DeleteDatasetDatasetsListView.MultiSelect = false;
            this.DeleteDatasetDatasetsListView.Name = "DeleteDatasetDatasetsListView";
            this.DeleteDatasetDatasetsListView.Size = new System.Drawing.Size(427, 117);
            this.DeleteDatasetDatasetsListView.TabIndex = 7;
            this.DeleteDatasetDatasetsListView.UseCompatibleStateImageBehavior = false;
            this.DeleteDatasetDatasetsListView.View = System.Windows.Forms.View.Details;
            this.DeleteDatasetDatasetsListView.SelectedIndexChanged += new System.EventHandler(this.DeleteDatasetDatasetsListView_SelectedIndexChanged);
            // 
            // DeleteDatasetRefreshWorkspacesButton
            // 
            this.DeleteDatasetRefreshWorkspacesButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
            this.DeleteDatasetRefreshWorkspacesButton.Location = new System.Drawing.Point(555, 22);
            this.DeleteDatasetRefreshWorkspacesButton.Name = "DeleteDatasetRefreshWorkspacesButton";
            this.DeleteDatasetRefreshWorkspacesButton.Size = new System.Drawing.Size(32, 23);
            this.DeleteDatasetRefreshWorkspacesButton.TabIndex = 0;
            this.SidekickToolTip.SetToolTip(this.DeleteDatasetRefreshWorkspacesButton, "Refresh");
            this.DeleteDatasetRefreshWorkspacesButton.UseVisualStyleBackColor = true;
            this.DeleteDatasetRefreshWorkspacesButton.Click += new System.EventHandler(this.DeleteDatasetRefreshWorkspacesButton_Click);
            // 
            // DeleteDatasetWorkspaceIdLabel
            // 
            this.DeleteDatasetWorkspaceIdLabel.AutoSize = true;
            this.DeleteDatasetWorkspaceIdLabel.Location = new System.Drawing.Point(7, 27);
            this.DeleteDatasetWorkspaceIdLabel.Name = "DeleteDatasetWorkspaceIdLabel";
            this.DeleteDatasetWorkspaceIdLabel.Size = new System.Drawing.Size(76, 13);
            this.DeleteDatasetWorkspaceIdLabel.TabIndex = 2;
            this.DeleteDatasetWorkspaceIdLabel.Text = "Workspace id:";
            // 
            // ImportPbixPanel
            // 
            this.ImportPbixPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ImportPbixPanel.Controls.Add(this.ImportPbixGroupBox);
            this.ImportPbixPanel.Location = new System.Drawing.Point(0, 251);
            this.ImportPbixPanel.Name = "ImportPbixPanel";
            this.ImportPbixPanel.Size = new System.Drawing.Size(716, 215);
            this.ImportPbixPanel.TabIndex = 6;
            // 
            // ImportPbixGroupBox
            // 
            this.ImportPbixGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ImportPbixGroupBox.Controls.Add(this.ImportPbixDatasetNameTextBox);
            this.ImportPbixGroupBox.Controls.Add(this.ImportPbixDatasetNameLabel);
            this.ImportPbixGroupBox.Controls.Add(this.ImportPbixRefreshDatasetsButton);
            this.ImportPbixGroupBox.Controls.Add(this.ImportPbixWorkspaceIdComboBox);
            this.ImportPbixGroupBox.Controls.Add(this.ImportPbixDatasetsListView);
            this.ImportPbixGroupBox.Controls.Add(this.ImportPbixRefreshWorkspacesButton);
            this.ImportPbixGroupBox.Controls.Add(this.PbixImportStatusLabel);
            this.ImportPbixGroupBox.Controls.Add(this.OpenPbixFileButton);
            this.ImportPbixGroupBox.Controls.Add(this.ImportPbixFilePathTextBox);
            this.ImportPbixGroupBox.Controls.Add(this.ImportPbixFilePathLabel);
            this.ImportPbixGroupBox.Controls.Add(this.ImportPbixDatasetsLabel);
            this.ImportPbixGroupBox.Controls.Add(this.WorkspaceIdLabel);
            this.ImportPbixGroupBox.Location = new System.Drawing.Point(6, 3);
            this.ImportPbixGroupBox.Name = "ImportPbixGroupBox";
            this.ImportPbixGroupBox.Size = new System.Drawing.Size(707, 209);
            this.ImportPbixGroupBox.TabIndex = 0;
            this.ImportPbixGroupBox.TabStop = false;
            this.ImportPbixGroupBox.Text = "Import PowerBI Desktop File";
            // 
            // ImportPbixDatasetNameTextBox
            // 
            this.ImportPbixDatasetNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ImportPbixDatasetNameTextBox.Location = new System.Drawing.Point(125, 151);
            this.ImportPbixDatasetNameTextBox.Name = "ImportPbixDatasetNameTextBox";
            this.ImportPbixDatasetNameTextBox.Size = new System.Drawing.Size(427, 20);
            this.ImportPbixDatasetNameTextBox.TabIndex = 4;
            // 
            // ImportPbixDatasetNameLabel
            // 
            this.ImportPbixDatasetNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ImportPbixDatasetNameLabel.AutoSize = true;
            this.ImportPbixDatasetNameLabel.Location = new System.Drawing.Point(6, 154);
            this.ImportPbixDatasetNameLabel.Name = "ImportPbixDatasetNameLabel";
            this.ImportPbixDatasetNameLabel.Size = new System.Drawing.Size(76, 13);
            this.ImportPbixDatasetNameLabel.TabIndex = 12;
            this.ImportPbixDatasetNameLabel.Text = "Dataset name:";
            // 
            // ImportPbixRefreshDatasetsButton
            // 
            this.ImportPbixRefreshDatasetsButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
            this.ImportPbixRefreshDatasetsButton.Location = new System.Drawing.Point(558, 54);
            this.ImportPbixRefreshDatasetsButton.Name = "ImportPbixRefreshDatasetsButton";
            this.ImportPbixRefreshDatasetsButton.Size = new System.Drawing.Size(32, 23);
            this.ImportPbixRefreshDatasetsButton.TabIndex = 2;
            this.SidekickToolTip.SetToolTip(this.ImportPbixRefreshDatasetsButton, "Refresh");
            this.ImportPbixRefreshDatasetsButton.UseVisualStyleBackColor = true;
            this.ImportPbixRefreshDatasetsButton.Click += new System.EventHandler(this.ImportPbixRefreshDatasetsButton_Click);
            // 
            // ImportPbixWorkspaceIdComboBox
            // 
            this.ImportPbixWorkspaceIdComboBox.FormattingEnabled = true;
            this.ImportPbixWorkspaceIdComboBox.Location = new System.Drawing.Point(125, 23);
            this.ImportPbixWorkspaceIdComboBox.Name = "ImportPbixWorkspaceIdComboBox";
            this.ImportPbixWorkspaceIdComboBox.Size = new System.Drawing.Size(427, 21);
            this.ImportPbixWorkspaceIdComboBox.TabIndex = 1;
            // 
            // ImportPbixDatasetsListView
            // 
            this.ImportPbixDatasetsListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ImportPbixDatasetsListView.HideSelection = false;
            this.ImportPbixDatasetsListView.Location = new System.Drawing.Point(125, 54);
            this.ImportPbixDatasetsListView.MultiSelect = false;
            this.ImportPbixDatasetsListView.Name = "ImportPbixDatasetsListView";
            this.ImportPbixDatasetsListView.Size = new System.Drawing.Size(427, 76);
            this.ImportPbixDatasetsListView.TabIndex = 2;
            this.ImportPbixDatasetsListView.UseCompatibleStateImageBehavior = false;
            this.ImportPbixDatasetsListView.View = System.Windows.Forms.View.Details;
            // 
            // ImportPbixRefreshWorkspacesButton
            // 
            this.ImportPbixRefreshWorkspacesButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
            this.ImportPbixRefreshWorkspacesButton.Location = new System.Drawing.Point(558, 22);
            this.ImportPbixRefreshWorkspacesButton.Name = "ImportPbixRefreshWorkspacesButton";
            this.ImportPbixRefreshWorkspacesButton.Size = new System.Drawing.Size(32, 23);
            this.ImportPbixRefreshWorkspacesButton.TabIndex = 11;
            this.SidekickToolTip.SetToolTip(this.ImportPbixRefreshWorkspacesButton, "Refresh");
            this.ImportPbixRefreshWorkspacesButton.UseVisualStyleBackColor = true;
            this.ImportPbixRefreshWorkspacesButton.Click += new System.EventHandler(this.ImportPbixRefreshWorkspacesButton_Click);
            // 
            // PbixImportStatusLabel
            // 
            this.PbixImportStatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PbixImportStatusLabel.AutoSize = true;
            this.PbixImportStatusLabel.Location = new System.Drawing.Point(128, 134);
            this.PbixImportStatusLabel.Name = "PbixImportStatusLabel";
            this.PbixImportStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.PbixImportStatusLabel.TabIndex = 3;
            // 
            // OpenPbixFileButton
            // 
            this.OpenPbixFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OpenPbixFileButton.Location = new System.Drawing.Point(675, 178);
            this.OpenPbixFileButton.Name = "OpenPbixFileButton";
            this.OpenPbixFileButton.Size = new System.Drawing.Size(24, 23);
            this.OpenPbixFileButton.TabIndex = 6;
            this.OpenPbixFileButton.Text = "...";
            this.OpenPbixFileButton.UseVisualStyleBackColor = true;
            this.OpenPbixFileButton.Click += new System.EventHandler(this.OpenPbixFileButton_Click);
            // 
            // ImportPbixFilePathTextBox
            // 
            this.ImportPbixFilePathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ImportPbixFilePathTextBox.Location = new System.Drawing.Point(125, 180);
            this.ImportPbixFilePathTextBox.Name = "ImportPbixFilePathTextBox";
            this.ImportPbixFilePathTextBox.Size = new System.Drawing.Size(544, 20);
            this.ImportPbixFilePathTextBox.TabIndex = 5;
            // 
            // ImportPbixFilePathLabel
            // 
            this.ImportPbixFilePathLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ImportPbixFilePathLabel.AutoSize = true;
            this.ImportPbixFilePathLabel.Location = new System.Drawing.Point(6, 183);
            this.ImportPbixFilePathLabel.Name = "ImportPbixFilePathLabel";
            this.ImportPbixFilePathLabel.Size = new System.Drawing.Size(70, 13);
            this.ImportPbixFilePathLabel.TabIndex = 1;
            this.ImportPbixFilePathLabel.Text = "Pbix file path:";
            // 
            // ImportPbixDatasetsLabel
            // 
            this.ImportPbixDatasetsLabel.AutoSize = true;
            this.ImportPbixDatasetsLabel.Location = new System.Drawing.Point(6, 54);
            this.ImportPbixDatasetsLabel.Name = "ImportPbixDatasetsLabel";
            this.ImportPbixDatasetsLabel.Size = new System.Drawing.Size(52, 13);
            this.ImportPbixDatasetsLabel.TabIndex = 0;
            this.ImportPbixDatasetsLabel.Text = "Datasets:";
            // 
            // WorkspaceIdLabel
            // 
            this.WorkspaceIdLabel.AutoSize = true;
            this.WorkspaceIdLabel.Location = new System.Drawing.Point(6, 26);
            this.WorkspaceIdLabel.Name = "WorkspaceIdLabel";
            this.WorkspaceIdLabel.Size = new System.Drawing.Size(76, 13);
            this.WorkspaceIdLabel.TabIndex = 0;
            this.WorkspaceIdLabel.Text = "Workspace id:";
            // 
            // GetWorkspacesPanel
            // 
            this.GetWorkspacesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GetWorkspacesPanel.Controls.Add(this.GetWorkspacesGroupBox);
            this.GetWorkspacesPanel.Location = new System.Drawing.Point(0, 251);
            this.GetWorkspacesPanel.Name = "GetWorkspacesPanel";
            this.GetWorkspacesPanel.Size = new System.Drawing.Size(716, 215);
            this.GetWorkspacesPanel.TabIndex = 6;
            // 
            // GetWorkspacesGroupBox
            // 
            this.GetWorkspacesGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GetWorkspacesGroupBox.Controls.Add(this.GetWorkspacesRefreshWorkspacesButton);
            this.GetWorkspacesGroupBox.Controls.Add(this.GetWorkspacesClearButton);
            this.GetWorkspacesGroupBox.Controls.Add(this.GetWorkspacesStatusLabel);
            this.GetWorkspacesGroupBox.Controls.Add(this.GetWorkspacesCopyButton);
            this.GetWorkspacesGroupBox.Controls.Add(this.GetWorkspacesWorkspaceIdListBox);
            this.GetWorkspacesGroupBox.Controls.Add(this.GetWorkspacesWorkspacesLabel);
            this.GetWorkspacesGroupBox.Location = new System.Drawing.Point(6, 3);
            this.GetWorkspacesGroupBox.Name = "GetWorkspacesGroupBox";
            this.GetWorkspacesGroupBox.Size = new System.Drawing.Size(707, 196);
            this.GetWorkspacesGroupBox.TabIndex = 0;
            this.GetWorkspacesGroupBox.TabStop = false;
            this.GetWorkspacesGroupBox.Text = "Get Workspaces";
            // 
            // GetWorkspacesRefreshWorkspacesButton
            // 
            this.GetWorkspacesRefreshWorkspacesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GetWorkspacesRefreshWorkspacesButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
            this.GetWorkspacesRefreshWorkspacesButton.Location = new System.Drawing.Point(589, 21);
            this.GetWorkspacesRefreshWorkspacesButton.Name = "GetWorkspacesRefreshWorkspacesButton";
            this.GetWorkspacesRefreshWorkspacesButton.Size = new System.Drawing.Size(32, 23);
            this.GetWorkspacesRefreshWorkspacesButton.TabIndex = 5;
            this.SidekickToolTip.SetToolTip(this.GetWorkspacesRefreshWorkspacesButton, "Refresh");
            this.GetWorkspacesRefreshWorkspacesButton.UseVisualStyleBackColor = true;
            this.GetWorkspacesRefreshWorkspacesButton.Click += new System.EventHandler(this.GetWorkspacesRefreshWorkspacesButton_Click);
            // 
            // GetWorkspacesClearButton
            // 
            this.GetWorkspacesClearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GetWorkspacesClearButton.Location = new System.Drawing.Point(627, 48);
            this.GetWorkspacesClearButton.Name = "GetWorkspacesClearButton";
            this.GetWorkspacesClearButton.Size = new System.Drawing.Size(75, 23);
            this.GetWorkspacesClearButton.TabIndex = 4;
            this.GetWorkspacesClearButton.Text = "Clear";
            this.GetWorkspacesClearButton.UseVisualStyleBackColor = true;
            this.GetWorkspacesClearButton.Click += new System.EventHandler(this.GetWorkspacesClearButton_Click);
            // 
            // GetWorkspacesStatusLabel
            // 
            this.GetWorkspacesStatusLabel.AutoSize = true;
            this.GetWorkspacesStatusLabel.Location = new System.Drawing.Point(125, 23);
            this.GetWorkspacesStatusLabel.Name = "GetWorkspacesStatusLabel";
            this.GetWorkspacesStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.GetWorkspacesStatusLabel.TabIndex = 1;
            // 
            // GetWorkspacesCopyButton
            // 
            this.GetWorkspacesCopyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GetWorkspacesCopyButton.Location = new System.Drawing.Point(627, 21);
            this.GetWorkspacesCopyButton.Name = "GetWorkspacesCopyButton";
            this.GetWorkspacesCopyButton.Size = new System.Drawing.Size(75, 23);
            this.GetWorkspacesCopyButton.TabIndex = 3;
            this.GetWorkspacesCopyButton.Text = "Copy";
            this.GetWorkspacesCopyButton.UseVisualStyleBackColor = true;
            this.GetWorkspacesCopyButton.Click += new System.EventHandler(this.GetWorkspacesCopyButton_Click);
            // 
            // GetWorkspacesWorkspaceIdListBox
            // 
            this.GetWorkspacesWorkspaceIdListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GetWorkspacesWorkspaceIdListBox.FormattingEnabled = true;
            this.GetWorkspacesWorkspaceIdListBox.Location = new System.Drawing.Point(125, 24);
            this.GetWorkspacesWorkspaceIdListBox.Name = "GetWorkspacesWorkspaceIdListBox";
            this.GetWorkspacesWorkspaceIdListBox.Size = new System.Drawing.Size(458, 147);
            this.GetWorkspacesWorkspaceIdListBox.TabIndex = 2;
            // 
            // GetWorkspacesWorkspacesLabel
            // 
            this.GetWorkspacesWorkspacesLabel.AutoSize = true;
            this.GetWorkspacesWorkspacesLabel.Location = new System.Drawing.Point(6, 23);
            this.GetWorkspacesWorkspacesLabel.Name = "GetWorkspacesWorkspacesLabel";
            this.GetWorkspacesWorkspacesLabel.Size = new System.Drawing.Size(70, 13);
            this.GetWorkspacesWorkspacesLabel.TabIndex = 0;
            this.GetWorkspacesWorkspacesLabel.Text = "Workspaces:";
            // 
            // GetDatasetsPanel
            // 
            this.GetDatasetsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GetDatasetsPanel.Controls.Add(this.GetDatasetsGroupBox);
            this.GetDatasetsPanel.Location = new System.Drawing.Point(0, 251);
            this.GetDatasetsPanel.Name = "GetDatasetsPanel";
            this.GetDatasetsPanel.Size = new System.Drawing.Size(716, 215);
            this.GetDatasetsPanel.TabIndex = 11;
            // 
            // GetDatasetsGroupBox
            // 
            this.GetDatasetsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GetDatasetsGroupBox.Controls.Add(this.GetDatasetsRefreshDatasetsButton);
            this.GetDatasetsGroupBox.Controls.Add(this.GetDatasetsWorkspaceIdComboBox);
            this.GetDatasetsGroupBox.Controls.Add(this.GetDatasetsRefreshWorkspaceListButton);
            this.GetDatasetsGroupBox.Controls.Add(this.GetDatasetsDatasetsListView);
            this.GetDatasetsGroupBox.Controls.Add(this.GetDatasetsStatusLabel);
            this.GetDatasetsGroupBox.Controls.Add(this.GetDatasetsClearButton);
            this.GetDatasetsGroupBox.Controls.Add(this.GetDatasetsCopyButton);
            this.GetDatasetsGroupBox.Controls.Add(this.GetDatasetsDatasetsLabel);
            this.GetDatasetsGroupBox.Controls.Add(this.GetDatasetsWorkspaceIdLabel);
            this.GetDatasetsGroupBox.Location = new System.Drawing.Point(3, 4);
            this.GetDatasetsGroupBox.Name = "GetDatasetsGroupBox";
            this.GetDatasetsGroupBox.Size = new System.Drawing.Size(710, 205);
            this.GetDatasetsGroupBox.TabIndex = 0;
            this.GetDatasetsGroupBox.TabStop = false;
            this.GetDatasetsGroupBox.Text = "Get Datasets";
            // 
            // GetDatasetsRefreshDatasetsButton
            // 
            this.GetDatasetsRefreshDatasetsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GetDatasetsRefreshDatasetsButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
            this.GetDatasetsRefreshDatasetsButton.Location = new System.Drawing.Point(586, 49);
            this.GetDatasetsRefreshDatasetsButton.Name = "GetDatasetsRefreshDatasetsButton";
            this.GetDatasetsRefreshDatasetsButton.Size = new System.Drawing.Size(32, 23);
            this.GetDatasetsRefreshDatasetsButton.TabIndex = 11;
            this.SidekickToolTip.SetToolTip(this.GetDatasetsRefreshDatasetsButton, "Refresh");
            this.GetDatasetsRefreshDatasetsButton.UseVisualStyleBackColor = true;
            this.GetDatasetsRefreshDatasetsButton.Click += new System.EventHandler(this.GetDatasetsRefreshDatasetsButton_Click);
            // 
            // GetDatasetsWorkspaceIdComboBox
            // 
            this.GetDatasetsWorkspaceIdComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GetDatasetsWorkspaceIdComboBox.FormattingEnabled = true;
            this.GetDatasetsWorkspaceIdComboBox.Location = new System.Drawing.Point(128, 21);
            this.GetDatasetsWorkspaceIdComboBox.Name = "GetDatasetsWorkspaceIdComboBox";
            this.GetDatasetsWorkspaceIdComboBox.Size = new System.Drawing.Size(452, 21);
            this.GetDatasetsWorkspaceIdComboBox.TabIndex = 1;
            // 
            // GetDatasetsRefreshWorkspaceListButton
            // 
            this.GetDatasetsRefreshWorkspaceListButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GetDatasetsRefreshWorkspaceListButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
            this.GetDatasetsRefreshWorkspaceListButton.Location = new System.Drawing.Point(586, 17);
            this.GetDatasetsRefreshWorkspaceListButton.Name = "GetDatasetsRefreshWorkspaceListButton";
            this.GetDatasetsRefreshWorkspaceListButton.Size = new System.Drawing.Size(32, 23);
            this.GetDatasetsRefreshWorkspaceListButton.TabIndex = 2;
            this.SidekickToolTip.SetToolTip(this.GetDatasetsRefreshWorkspaceListButton, "Refresh");
            this.GetDatasetsRefreshWorkspaceListButton.UseVisualStyleBackColor = true;
            this.GetDatasetsRefreshWorkspaceListButton.Click += new System.EventHandler(this.GetDatasetsRefreshWorkspaceListButton_Click);
            // 
            // GetDatasetsDatasetsListView
            // 
            this.GetDatasetsDatasetsListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GetDatasetsDatasetsListView.Location = new System.Drawing.Point(128, 48);
            this.GetDatasetsDatasetsListView.Name = "GetDatasetsDatasetsListView";
            this.GetDatasetsDatasetsListView.Size = new System.Drawing.Size(452, 147);
            this.GetDatasetsDatasetsListView.TabIndex = 3;
            this.GetDatasetsDatasetsListView.UseCompatibleStateImageBehavior = false;
            this.GetDatasetsDatasetsListView.View = System.Windows.Forms.View.Details;
            // 
            // GetDatasetsStatusLabel
            // 
            this.GetDatasetsStatusLabel.AutoSize = true;
            this.GetDatasetsStatusLabel.Location = new System.Drawing.Point(128, 66);
            this.GetDatasetsStatusLabel.Name = "GetDatasetsStatusLabel";
            this.GetDatasetsStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.GetDatasetsStatusLabel.TabIndex = 7;
            // 
            // GetDatasetsClearButton
            // 
            this.GetDatasetsClearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GetDatasetsClearButton.Location = new System.Drawing.Point(632, 77);
            this.GetDatasetsClearButton.Name = "GetDatasetsClearButton";
            this.GetDatasetsClearButton.Size = new System.Drawing.Size(75, 23);
            this.GetDatasetsClearButton.TabIndex = 5;
            this.GetDatasetsClearButton.Text = "Clear";
            this.GetDatasetsClearButton.UseVisualStyleBackColor = true;
            this.GetDatasetsClearButton.Click += new System.EventHandler(this.GetDatasetsClearButton_Click);
            // 
            // GetDatasetsCopyButton
            // 
            this.GetDatasetsCopyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GetDatasetsCopyButton.Location = new System.Drawing.Point(632, 50);
            this.GetDatasetsCopyButton.Name = "GetDatasetsCopyButton";
            this.GetDatasetsCopyButton.Size = new System.Drawing.Size(75, 23);
            this.GetDatasetsCopyButton.TabIndex = 4;
            this.GetDatasetsCopyButton.Text = "Copy";
            this.GetDatasetsCopyButton.UseVisualStyleBackColor = true;
            this.GetDatasetsCopyButton.Click += new System.EventHandler(this.GetDatasetsCopyButton_Click);
            // 
            // GetDatasetsDatasetsLabel
            // 
            this.GetDatasetsDatasetsLabel.AutoSize = true;
            this.GetDatasetsDatasetsLabel.Location = new System.Drawing.Point(13, 64);
            this.GetDatasetsDatasetsLabel.Name = "GetDatasetsDatasetsLabel";
            this.GetDatasetsDatasetsLabel.Size = new System.Drawing.Size(52, 13);
            this.GetDatasetsDatasetsLabel.TabIndex = 2;
            this.GetDatasetsDatasetsLabel.Text = "Datasets:";
            // 
            // GetDatasetsWorkspaceIdLabel
            // 
            this.GetDatasetsWorkspaceIdLabel.AutoSize = true;
            this.GetDatasetsWorkspaceIdLabel.Location = new System.Drawing.Point(9, 25);
            this.GetDatasetsWorkspaceIdLabel.Name = "GetDatasetsWorkspaceIdLabel";
            this.GetDatasetsWorkspaceIdLabel.Size = new System.Drawing.Size(76, 13);
            this.GetDatasetsWorkspaceIdLabel.TabIndex = 0;
            this.GetDatasetsWorkspaceIdLabel.Text = "Workspace id:";
            // 
            // UpdateCredentialsPanel
            // 
            this.UpdateCredentialsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UpdateCredentialsPanel.Controls.Add(this.UpdateCredentialsGroupBox);
            this.UpdateCredentialsPanel.Location = new System.Drawing.Point(0, 251);
            this.UpdateCredentialsPanel.Name = "UpdateCredentialsPanel";
            this.UpdateCredentialsPanel.Size = new System.Drawing.Size(716, 215);
            this.UpdateCredentialsPanel.TabIndex = 6;
            // 
            // UpdateCredentialsGroupBox
            // 
            this.UpdateCredentialsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UpdateCredentialsGroupBox.Controls.Add(this.UpdateCredentialsWorkspaceIdComboBox);
            this.UpdateCredentialsGroupBox.Controls.Add(this.UpdateCredentialsRefreshWorkspaceIdsButton);
            this.UpdateCredentialsGroupBox.Controls.Add(this.UpdateCredentialsPasswordTextBox);
            this.UpdateCredentialsGroupBox.Controls.Add(this.UpdateCredentialsPasswordLabel);
            this.UpdateCredentialsGroupBox.Controls.Add(this.UpdateCredentialsUsernameTextBox);
            this.UpdateCredentialsGroupBox.Controls.Add(this.UpdateCredentialsUsernameLabel);
            this.UpdateCredentialsGroupBox.Controls.Add(this.UpdateCredentialsWorkspaceIdLabel);
            this.UpdateCredentialsGroupBox.Location = new System.Drawing.Point(3, 13);
            this.UpdateCredentialsGroupBox.Name = "UpdateCredentialsGroupBox";
            this.UpdateCredentialsGroupBox.Size = new System.Drawing.Size(710, 130);
            this.UpdateCredentialsGroupBox.TabIndex = 0;
            this.UpdateCredentialsGroupBox.TabStop = false;
            this.UpdateCredentialsGroupBox.Text = "Update PowerBI Dataset Credentials";
            // 
            // UpdateCredentialsWorkspaceIdComboBox
            // 
            this.UpdateCredentialsWorkspaceIdComboBox.FormattingEnabled = true;
            this.UpdateCredentialsWorkspaceIdComboBox.Location = new System.Drawing.Point(116, 30);
            this.UpdateCredentialsWorkspaceIdComboBox.Name = "UpdateCredentialsWorkspaceIdComboBox";
            this.UpdateCredentialsWorkspaceIdComboBox.Size = new System.Drawing.Size(261, 21);
            this.UpdateCredentialsWorkspaceIdComboBox.TabIndex = 1;
            // 
            // UpdateCredentialsRefreshWorkspaceIdsButton
            // 
            this.UpdateCredentialsRefreshWorkspaceIdsButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionRefresh;
            this.UpdateCredentialsRefreshWorkspaceIdsButton.Location = new System.Drawing.Point(383, 27);
            this.UpdateCredentialsRefreshWorkspaceIdsButton.Name = "UpdateCredentialsRefreshWorkspaceIdsButton";
            this.UpdateCredentialsRefreshWorkspaceIdsButton.Size = new System.Drawing.Size(32, 23);
            this.UpdateCredentialsRefreshWorkspaceIdsButton.TabIndex = 2;
            this.SidekickToolTip.SetToolTip(this.UpdateCredentialsRefreshWorkspaceIdsButton, "Refresh");
            this.UpdateCredentialsRefreshWorkspaceIdsButton.UseVisualStyleBackColor = true;
            this.UpdateCredentialsRefreshWorkspaceIdsButton.Click += new System.EventHandler(this.UpdateCredentialsRefreshWorkspaceIdsButton_Click);
            // 
            // UpdateCredentialsPasswordTextBox
            // 
            this.UpdateCredentialsPasswordTextBox.Location = new System.Drawing.Point(116, 92);
            this.UpdateCredentialsPasswordTextBox.Name = "UpdateCredentialsPasswordTextBox";
            this.UpdateCredentialsPasswordTextBox.PasswordChar = '*';
            this.UpdateCredentialsPasswordTextBox.Size = new System.Drawing.Size(261, 20);
            this.UpdateCredentialsPasswordTextBox.TabIndex = 4;
            // 
            // UpdateCredentialsPasswordLabel
            // 
            this.UpdateCredentialsPasswordLabel.AutoSize = true;
            this.UpdateCredentialsPasswordLabel.Location = new System.Drawing.Point(9, 92);
            this.UpdateCredentialsPasswordLabel.Name = "UpdateCredentialsPasswordLabel";
            this.UpdateCredentialsPasswordLabel.Size = new System.Drawing.Size(56, 13);
            this.UpdateCredentialsPasswordLabel.TabIndex = 10;
            this.UpdateCredentialsPasswordLabel.Text = "Password:";
            // 
            // UpdateCredentialsUsernameTextBox
            // 
            this.UpdateCredentialsUsernameTextBox.Location = new System.Drawing.Point(116, 61);
            this.UpdateCredentialsUsernameTextBox.Name = "UpdateCredentialsUsernameTextBox";
            this.UpdateCredentialsUsernameTextBox.Size = new System.Drawing.Size(261, 20);
            this.UpdateCredentialsUsernameTextBox.TabIndex = 3;
            // 
            // UpdateCredentialsUsernameLabel
            // 
            this.UpdateCredentialsUsernameLabel.AutoSize = true;
            this.UpdateCredentialsUsernameLabel.Location = new System.Drawing.Point(8, 59);
            this.UpdateCredentialsUsernameLabel.Name = "UpdateCredentialsUsernameLabel";
            this.UpdateCredentialsUsernameLabel.Size = new System.Drawing.Size(61, 13);
            this.UpdateCredentialsUsernameLabel.TabIndex = 8;
            this.UpdateCredentialsUsernameLabel.Text = "User name:";
            // 
            // UpdateCredentialsWorkspaceIdLabel
            // 
            this.UpdateCredentialsWorkspaceIdLabel.AutoSize = true;
            this.UpdateCredentialsWorkspaceIdLabel.Location = new System.Drawing.Point(6, 31);
            this.UpdateCredentialsWorkspaceIdLabel.Name = "UpdateCredentialsWorkspaceIdLabel";
            this.UpdateCredentialsWorkspaceIdLabel.Size = new System.Drawing.Size(76, 13);
            this.UpdateCredentialsWorkspaceIdLabel.TabIndex = 0;
            this.UpdateCredentialsWorkspaceIdLabel.Text = "Workspace id:";
            // 
            // CreateWorkspacePanel
            // 
            this.CreateWorkspacePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateWorkspacePanel.Controls.Add(this.CreateWorkspaceGroupBox);
            this.CreateWorkspacePanel.Location = new System.Drawing.Point(0, 251);
            this.CreateWorkspacePanel.Name = "CreateWorkspacePanel";
            this.CreateWorkspacePanel.Size = new System.Drawing.Size(716, 215);
            this.CreateWorkspacePanel.TabIndex = 10;
            // 
            // CreateWorkspaceGroupBox
            // 
            this.CreateWorkspaceGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateWorkspaceGroupBox.Controls.Add(this.CreateWorkspaceWorkspaceIdTextBox);
            this.CreateWorkspaceGroupBox.Controls.Add(this.CreateWorkspaceWorkspaceLabel);
            this.CreateWorkspaceGroupBox.Location = new System.Drawing.Point(6, 3);
            this.CreateWorkspaceGroupBox.Name = "CreateWorkspaceGroupBox";
            this.CreateWorkspaceGroupBox.Size = new System.Drawing.Size(707, 99);
            this.CreateWorkspaceGroupBox.TabIndex = 0;
            this.CreateWorkspaceGroupBox.TabStop = false;
            this.CreateWorkspaceGroupBox.Text = "Create Workspace";
            // 
            // CreateWorkspaceWorkspaceIdTextBox
            // 
            this.CreateWorkspaceWorkspaceIdTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateWorkspaceWorkspaceIdTextBox.Location = new System.Drawing.Point(128, 23);
            this.CreateWorkspaceWorkspaceIdTextBox.Name = "CreateWorkspaceWorkspaceIdTextBox";
            this.CreateWorkspaceWorkspaceIdTextBox.ReadOnly = true;
            this.CreateWorkspaceWorkspaceIdTextBox.Size = new System.Drawing.Size(571, 20);
            this.CreateWorkspaceWorkspaceIdTextBox.TabIndex = 1;
            // 
            // CreateWorkspaceWorkspaceLabel
            // 
            this.CreateWorkspaceWorkspaceLabel.AutoSize = true;
            this.CreateWorkspaceWorkspaceLabel.Location = new System.Drawing.Point(12, 26);
            this.CreateWorkspaceWorkspaceLabel.Name = "CreateWorkspaceWorkspaceLabel";
            this.CreateWorkspaceWorkspaceLabel.Size = new System.Drawing.Size(102, 13);
            this.CreateWorkspaceWorkspaceLabel.TabIndex = 0;
            this.CreateWorkspaceWorkspaceLabel.Text = "Created workspace:";
            // 
            // SelectActionLabel
            // 
            this.SelectActionLabel.AutoSize = true;
            this.SelectActionLabel.Location = new System.Drawing.Point(12, 216);
            this.SelectActionLabel.Name = "SelectActionLabel";
            this.SelectActionLabel.Size = new System.Drawing.Size(40, 13);
            this.SelectActionLabel.TabIndex = 4;
            this.SelectActionLabel.Text = "Action:";
            // 
            // SelectActionComboBox
            // 
            this.SelectActionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SelectActionComboBox.FormattingEnabled = true;
            this.SelectActionComboBox.Location = new System.Drawing.Point(131, 216);
            this.SelectActionComboBox.Name = "SelectActionComboBox";
            this.SelectActionComboBox.Size = new System.Drawing.Size(258, 21);
            this.SelectActionComboBox.Sorted = true;
            this.SelectActionComboBox.TabIndex = 5;
            this.SelectActionComboBox.SelectedIndexChanged += new System.EventHandler(this.SelectActionComboBox_SelectedIndexChanged);
            // 
            // ConnectionInfoGroupBox
            // 
            this.ConnectionInfoGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ConnectionInfoGroupBox.Controls.Add(this.WorkspaceCollectionComboBox);
            this.ConnectionInfoGroupBox.Controls.Add(this.WorkspaceCollectionLabel);
            this.ConnectionInfoGroupBox.Controls.Add(this.EnvironmentLabel);
            this.ConnectionInfoGroupBox.Controls.Add(this.EnvironmentComboBox);
            this.ConnectionInfoGroupBox.Location = new System.Drawing.Point(3, 120);
            this.ConnectionInfoGroupBox.Name = "ConnectionInfoGroupBox";
            this.ConnectionInfoGroupBox.Size = new System.Drawing.Size(710, 76);
            this.ConnectionInfoGroupBox.TabIndex = 3;
            this.ConnectionInfoGroupBox.TabStop = false;
            this.ConnectionInfoGroupBox.Text = "Azure PowerBI Connection Information";
            // 
            // WorkspaceCollectionComboBox
            // 
            this.WorkspaceCollectionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.WorkspaceCollectionComboBox.FormattingEnabled = true;
            this.WorkspaceCollectionComboBox.Location = new System.Drawing.Point(128, 48);
            this.WorkspaceCollectionComboBox.Name = "WorkspaceCollectionComboBox";
            this.WorkspaceCollectionComboBox.Size = new System.Drawing.Size(258, 21);
            this.WorkspaceCollectionComboBox.TabIndex = 3;
            // 
            // WorkspaceCollectionLabel
            // 
            this.WorkspaceCollectionLabel.AutoSize = true;
            this.WorkspaceCollectionLabel.Location = new System.Drawing.Point(9, 48);
            this.WorkspaceCollectionLabel.Name = "WorkspaceCollectionLabel";
            this.WorkspaceCollectionLabel.Size = new System.Drawing.Size(113, 13);
            this.WorkspaceCollectionLabel.TabIndex = 2;
            this.WorkspaceCollectionLabel.Text = "Workspace collection:";
            // 
            // EnvironmentLabel
            // 
            this.EnvironmentLabel.AutoSize = true;
            this.EnvironmentLabel.Location = new System.Drawing.Point(9, 22);
            this.EnvironmentLabel.Name = "EnvironmentLabel";
            this.EnvironmentLabel.Size = new System.Drawing.Size(98, 13);
            this.EnvironmentLabel.TabIndex = 0;
            this.EnvironmentLabel.Text = "Environment name:";
            // 
            // EnvironmentComboBox
            // 
            this.EnvironmentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.EnvironmentComboBox.FormattingEnabled = true;
            this.EnvironmentComboBox.Location = new System.Drawing.Point(128, 19);
            this.EnvironmentComboBox.Name = "EnvironmentComboBox";
            this.EnvironmentComboBox.Size = new System.Drawing.Size(90, 21);
            this.EnvironmentComboBox.TabIndex = 1;
            this.EnvironmentComboBox.SelectedIndexChanged += new System.EventHandler(this.EnvironmentComboBox_SelectedIndexChanged);
            // 
            // AzureSubscriptionNameComboBox
            // 
            this.AzureSubscriptionNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AzureSubscriptionNameComboBox.FormattingEnabled = true;
            this.AzureSubscriptionNameComboBox.Location = new System.Drawing.Point(128, 83);
            this.AzureSubscriptionNameComboBox.Name = "AzureSubscriptionNameComboBox";
            this.AzureSubscriptionNameComboBox.Size = new System.Drawing.Size(261, 21);
            this.AzureSubscriptionNameComboBox.TabIndex = 2;
            this.AzureSubscriptionNameComboBox.SelectedIndexChanged += new System.EventHandler(this.AzureSubscriptionNameComboBox_SelectedIndexChanged);
            // 
            // AzureSubscriptionNameLabel
            // 
            this.AzureSubscriptionNameLabel.AutoSize = true;
            this.AzureSubscriptionNameLabel.Location = new System.Drawing.Point(3, 86);
            this.AzureSubscriptionNameLabel.Name = "AzureSubscriptionNameLabel";
            this.AzureSubscriptionNameLabel.Size = new System.Drawing.Size(96, 13);
            this.AzureSubscriptionNameLabel.TabIndex = 1;
            this.AzureSubscriptionNameLabel.Text = "Azure subscription:";
            // 
            // GuidanceGroupBox
            // 
            this.GuidanceGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GuidanceGroupBox.Controls.Add(this.HelpButton);
            this.GuidanceGroupBox.Location = new System.Drawing.Point(3, 28);
            this.GuidanceGroupBox.Name = "GuidanceGroupBox";
            this.GuidanceGroupBox.Size = new System.Drawing.Size(710, 49);
            this.GuidanceGroupBox.TabIndex = 0;
            this.GuidanceGroupBox.TabStop = false;
            this.GuidanceGroupBox.Text = "Guidance";
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(668, 11);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 0;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // ControlPanel
            // 
            this.ControlPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Controls.Add(this.ApplyButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 466);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(716, 40);
            this.ControlPanel.TabIndex = 7;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(635, 11);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 1;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(716, 22);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.Azure.Automation.Sidekicks.Help.chm";
            // 
            // ReportPortalConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 506);
            this.Controls.Add(this.PropertiesPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "ReportPortalConfigForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "ReportPortalConfigForm";
            this.PropertiesPanel.ResumeLayout(false);
            this.PropertiesPanel.PerformLayout();
            this.DeleteDatasetPanel.ResumeLayout(false);
            this.DeleteDatasetGroupBox.ResumeLayout(false);
            this.DeleteDatasetGroupBox.PerformLayout();
            this.ImportPbixPanel.ResumeLayout(false);
            this.ImportPbixGroupBox.ResumeLayout(false);
            this.ImportPbixGroupBox.PerformLayout();
            this.GetWorkspacesPanel.ResumeLayout(false);
            this.GetWorkspacesGroupBox.ResumeLayout(false);
            this.GetWorkspacesGroupBox.PerformLayout();
            this.GetDatasetsPanel.ResumeLayout(false);
            this.GetDatasetsGroupBox.ResumeLayout(false);
            this.GetDatasetsGroupBox.PerformLayout();
            this.UpdateCredentialsPanel.ResumeLayout(false);
            this.UpdateCredentialsGroupBox.ResumeLayout(false);
            this.UpdateCredentialsGroupBox.PerformLayout();
            this.CreateWorkspacePanel.ResumeLayout(false);
            this.CreateWorkspaceGroupBox.ResumeLayout(false);
            this.CreateWorkspaceGroupBox.PerformLayout();
            this.ConnectionInfoGroupBox.ResumeLayout(false);
            this.ConnectionInfoGroupBox.PerformLayout();
            this.GuidanceGroupBox.ResumeLayout(false);
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.GroupBox GuidanceGroupBox;
        private System.Windows.Forms.ComboBox AzureSubscriptionNameComboBox;
        private System.Windows.Forms.Label AzureSubscriptionNameLabel;
        private System.Windows.Forms.GroupBox ConnectionInfoGroupBox;
        private System.Windows.Forms.Label EnvironmentLabel;
        private System.Windows.Forms.ComboBox EnvironmentComboBox;
        private System.Windows.Forms.Label SelectActionLabel;
        private System.Windows.Forms.ComboBox SelectActionComboBox;
        private System.Windows.Forms.Panel GetWorkspacesPanel;
        private System.Windows.Forms.GroupBox GetWorkspacesGroupBox;
        private System.Windows.Forms.ComboBox WorkspaceCollectionComboBox;
        private System.Windows.Forms.Label WorkspaceCollectionLabel;
        private System.Windows.Forms.Label GetWorkspacesWorkspacesLabel;
        private System.Windows.Forms.ListBox GetWorkspacesWorkspaceIdListBox;
        private System.Windows.Forms.Button GetWorkspacesCopyButton;
        private System.Windows.Forms.Label GetWorkspacesStatusLabel;
        private System.Windows.Forms.Panel ImportPbixPanel;
        private System.Windows.Forms.GroupBox ImportPbixGroupBox;
        private System.Windows.Forms.Label ImportPbixFilePathLabel;
        private System.Windows.Forms.Label ImportPbixDatasetsLabel;
        private System.Windows.Forms.Label WorkspaceIdLabel;
        private System.Windows.Forms.Button OpenPbixFileButton;
        private System.Windows.Forms.TextBox ImportPbixFilePathTextBox;
        private System.Windows.Forms.Label PbixImportStatusLabel;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.Button HelpButton;
        private System.Windows.Forms.Button GetWorkspacesClearButton;
        private System.Windows.Forms.Panel UpdateCredentialsPanel;
        private System.Windows.Forms.Panel CreateWorkspacePanel;
        private System.Windows.Forms.GroupBox CreateWorkspaceGroupBox;
        private System.Windows.Forms.TextBox CreateWorkspaceWorkspaceIdTextBox;
        private System.Windows.Forms.Label CreateWorkspaceWorkspaceLabel;
        private System.Windows.Forms.GroupBox UpdateCredentialsGroupBox;
        private System.Windows.Forms.TextBox UpdateCredentialsPasswordTextBox;
        private System.Windows.Forms.Label UpdateCredentialsPasswordLabel;
        private System.Windows.Forms.TextBox UpdateCredentialsUsernameTextBox;
        private System.Windows.Forms.Label UpdateCredentialsUsernameLabel;
        private System.Windows.Forms.Label UpdateCredentialsWorkspaceIdLabel;
        private System.Windows.Forms.Panel GetDatasetsPanel;
        private System.Windows.Forms.GroupBox GetDatasetsGroupBox;
        private System.Windows.Forms.Label GetDatasetsWorkspaceIdLabel;
        private System.Windows.Forms.Label GetDatasetsDatasetsLabel;
        private System.Windows.Forms.Button GetDatasetsClearButton;
        private System.Windows.Forms.Button GetDatasetsCopyButton;
        private System.Windows.Forms.Label GetDatasetsStatusLabel;
        private System.Windows.Forms.Panel DeleteDatasetPanel;
        private System.Windows.Forms.GroupBox DeleteDatasetGroupBox;
        private System.Windows.Forms.Label DeleteDatasetWorkspaceIdLabel;
        private System.Windows.Forms.ListView GetDatasetsDatasetsListView;
        private System.Windows.Forms.Button GetDatasetsRefreshWorkspaceListButton;
        private System.Windows.Forms.ComboBox GetDatasetsWorkspaceIdComboBox;
        private System.Windows.Forms.ToolTip SidekickToolTip;
        private System.Windows.Forms.Button UpdateCredentialsRefreshWorkspaceIdsButton;
        private System.Windows.Forms.ComboBox UpdateCredentialsWorkspaceIdComboBox;
        private System.Windows.Forms.Button DeleteDatasetRefreshWorkspacesButton;
        private System.Windows.Forms.Label DeleteDatasetDatasetListLabel;
        private System.Windows.Forms.ListView DeleteDatasetDatasetsListView;
        private System.Windows.Forms.ComboBox DeleteDatasetWorkspaceIdComboBox;
        private System.Windows.Forms.Button DeleteDatasetRefreshDatasetsButton;
        private System.Windows.Forms.Button ImportPbixRefreshDatasetsButton;
        private System.Windows.Forms.ComboBox ImportPbixWorkspaceIdComboBox;
        private System.Windows.Forms.ListView ImportPbixDatasetsListView;
        private System.Windows.Forms.Button ImportPbixRefreshWorkspacesButton;
        private System.Windows.Forms.TextBox ImportPbixDatasetNameTextBox;
        private System.Windows.Forms.Label ImportPbixDatasetNameLabel;
        private System.Windows.Forms.Button GetDatasetsRefreshDatasetsButton;
        private System.Windows.Forms.Button GetWorkspacesRefreshWorkspacesButton;
    }
}