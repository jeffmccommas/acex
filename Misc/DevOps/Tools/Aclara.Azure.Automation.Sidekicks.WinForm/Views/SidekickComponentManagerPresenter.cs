﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public class SidekickComponentManagerPresenter
    {

        #region Private Constants

        #endregion

        #region Private Data Members

        private IComponentManagerView _componentManagerView;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Component manager view.
        /// </summary>
        public IComponentManagerView ComponentManagerView
        {
            get { return _componentManagerView; }
            set { _componentManagerView = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Overriden constructor.
        /// </summary>
        public SidekickComponentManagerPresenter(IComponentManagerView logView)
        {
            _componentManagerView = logView;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {
        }

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        #endregion

    }
}
