﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Utilities;
using Aclara.Azure.Automation.Sidekicks.WinForm.Models;
using Aclara.Azure.Automation.Sidekicks.WinForm.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public partial class SidekickComponentManagerForm : Form, IComponentManagerView
    {

        #region Private Constants

        private const string SidekickComponent_AzurePowerShell_Name = "Azure PowerShell";
        private const string SidekickComponent_AzurePowerShell_Description = "Azure PowerShell";
        private const string SidekickComponent_AzurePowerShell_HelpKeyword = "RequiredComponentsInstallGuide.htm";

        private const string RequiredComponentNotFound_AzurePowerShell = "Required component not found: Azure PowerShell.";

        private const string SidekickActivationStatus_ClickContinueButtonPrompt = "Click Continue to begin component validation.";
        private const string SidekickActivationStatus_ValidateRequiredComponents = "Validating required components. Please wait...";
        private const string SidekickActivationStatus_RequiredComponentsNotFound = "Some required components could not be found.";
        private const string SidekickAtivationStatus_LoginToAzurePowerShellRequired = "Login to Azure PowerShell required.";
        private const string SidekickActivationStatus_LoginAzurePowerShellFailed = "Login to Azure PowerShell failed or was canceled.";

        #endregion

        #region Private Data Members

        private SidekickComponentManagerPresenter _componentManagerPresenter;
        private SidekickConfiguration _sidekickConfiguration;
        private SidekickComponentManager _sidekickComponentManager;
        private List<SidekickComponentId> _sidekickComponentIdList;

        #endregion

        #region Public Delegates

        public event EventHandler<SidekickComponentValidationCompletedEventArgs> SidekickComponentValidationCompleted;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Component manager presenter.
        /// </summary>
        public SidekickComponentManagerPresenter ComponentManagerPresenter
        {
            get { return _componentManagerPresenter; }
            set { _componentManagerPresenter = value; }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick component manager.
        /// </summary>
        public SidekickComponentManager SidekickComponentManager
        {
            get { return _sidekickComponentManager; }
            set { _sidekickComponentManager = value; }
        }

        /// <summary>
        /// Property: Sidekick component identifier list.
        /// </summary>
        public List<SidekickComponentId> SidekickComponentIdList
        {
            get { return _sidekickComponentIdList; }
            set { _sidekickComponentIdList = value; }
        }

        #endregion

        #region Publc Contructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="componentManagerPresenter"></param>
        /// <param name="sidekickComponentManager"></param>
        public SidekickComponentManagerForm(SidekickConfiguration sidekickConfiguration,
                                            SidekickComponentManager sidekickComponentManager)
        {
            _sidekickConfiguration = sidekickConfiguration;
            _sidekickComponentManager = sidekickComponentManager;
            InitializeComponent();

            this.InitializeControls();
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="componentManagerPresenter"></param>
        private SidekickComponentManagerForm()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Login required sidekick components.
        /// </summary>
        /// <param name="SidekickComponentIdList"></param>
        public void LoginRequiredComponents(List<SidekickComponentId> SidekickComponentIdList)
        {

            try
            {
                this.UpdateSidekickActivationStatus(SidekickAtivationStatus_LoginToAzurePowerShellRequired);

                this.SidekickComponentManager.LoginSidekickComponents(SidekickComponentIdList);

                if (this.SidekickComponentManager.GetSidekickComponentsNotLoggedIn(SidekickComponentIdList).Count < 0)
                {
                    this.UpdateSidekickActivationStatus(SidekickActivationStatus_LoginAzurePowerShellFailed);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {

            try
            {
                this.UpdateSidekickActivationStatus(SidekickActivationStatus_ClickContinueButtonPrompt);

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Update sidekick activation status.
        /// </summary>
        /// <param name="message"></param>
        protected void UpdateSidekickActivationStatus(string message)
        {
            try
            {
                this.SidekickActivationStatusLabel.Text = message;
                this.SidekickActivationStatusLabel.Invalidate();
                this.SidekickActivationStatusLabel.Update();
                this.SidekickActivationStatusLabel.Refresh();
                Application.DoEvents();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Validate required components.
        /// </summary>
        protected void ValidateRequiredComponents()
        {

            try
            {
                this.UpdateSidekickActivationStatus(SidekickActivationStatus_ValidateRequiredComponents);

                this.SidekickComponentManager.ValidateSidekickComponents(this.SidekickComponentIdList);

                if (this.SidekickComponentManager.GetUnavailableSidekickComponents(this.SidekickComponentIdList).Count < 0)
                {
                    this.UpdateSidekickActivationStatus(SidekickActivationStatus_RequiredComponentsNotFound);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler - Continue button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContinueButton_Click(object sender, EventArgs e)
        {
            SidekickComponentValidationCompletedEventArgs sidekickComponentValidationCompletedEventArgs;
            try
            {
                this.Cursor = Cursors.WaitCursor;

                this.ContinueButton.Enabled = false;

                this.ValidateRequiredComponents();

                if (this.SidekickComponentValidationCompleted != null)
                {
                    sidekickComponentValidationCompletedEventArgs = new SidekickComponentValidationCompletedEventArgs();
                    this.SidekickComponentValidationCompleted(this, sidekickComponentValidationCompletedEventArgs);
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
            finally
            {
                this.ContinueButton.Enabled = true;
                this.Cursor = Cursors.Default;

            }
        }


        #endregion
    }
}
