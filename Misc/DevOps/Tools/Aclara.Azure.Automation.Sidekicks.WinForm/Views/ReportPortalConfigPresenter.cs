﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.AzurePowerBI.Client.Models;
using Aclara.Tools.Common.StatusManagement;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Aclara.AzurePowerBI.Client;
using Aclara.AzurePowerBI.Client.Results;
using Aclara.AzurePowerBI.Client.Events;
using Aclara.Azure.Automation.Sidekicks.WinForm.Logging;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public class ReportPortalConfigPresenter
    {

        #region Private Constants

        private const string BackgroundTaskStatus_CancellingRequest = "Cancelling Report Portal configuration request...";
        private const string BackgroundTaskStatus_Requested = "Report Portal configuration operation requested...";
        private const string BackgroundTaskStatus_Completed = "Report Portal configuration request canceled.";
        private const string BackgroundTaskStatus_Canceled = "Report Portal configuration request canceled.";

        private const string BackgroundTaskTotalDuration = "[*] Report Portal configuration request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})";

        private const string ApplyButtonText_Cancel = "Cancel";

        private const string Event_PowerBI_Operation_CompletedWithStatus = "PowerBI operation completed. (Status --> {0})";
        private const string Event_PowerBI_Operation_Completed = "PowerBI operation completed.";

        private const string Event_PowerBIGetDatasets_CompletedWithStatus = "PowerBI get datasets completed. (Status --> {0})";
        private const string Event_PowerBIGetDatasets_Completed = "PowerBI get datasets completed.";

        private const string Event_PowerBIDeleteDataset_CompletedWithStatus = "PowerBI delete dataset completed. (Status --> {0})";
        private const string Event_PowerBIDeleteDataset_Completed = "PowerBI delete dataset completed.";

        private const string Event_PowerBIGetWorkspaces_CompletedWithStatus = "PowerBI get workspaces completed. (Status --> {0})";
        private const string Event_PowerBIGetWorkspaces_Completed = "PowerBI get workspaces completed.";

        private const string Event_PowerBICreateWorkspace_CompletedWithStatus = "PowerBI create workspace completed. (Status --> {0})";
        private const string Event_PowerBICreateWorkspace_Completed = "PowerBI create workspace completed.";

        private const string Event_PowerBIUpdateCredentials_CompletedWithStatus = "PowerBI update credentials completed. (Status --> {0})";
        private const string Event_PowerBIUpdateCredentials_Completed = "PowerBI update credentials completedcompleted.";

        private const string Event_PowerBIImportPbix_CompletedWithStatus = "PowerBI import pbix file completed. (Status --> {0})";
        private const string Event_PowerBIImportPbix_Completed = "PowerBI import pbix file completed.";

        private const string BackgroundTaskException = "Operation cancelled.";
        private const string BackgroundTaskExceptionUnobserved = "Operation cancelled. (Exception unobserved)";

        private const string PbixImportStatus_CompletedSuccessfully = "Pbix import completed successfully (Pbix import Id: {0})";

        private const string RequestFailed = "Request failed.";

        #endregion

        #region Private Data Members

        private CustomLogger _logger;
        private SidekickConfiguration _sidekickConfiguration = null;
        private IReportPortalConfigView _ReportPortalConfigView = null;
        private PowerBIManager _powerBIManager = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.ReportPortalConfigView.SidekickName,
                                               this.ReportPortalConfigView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: ReportPortalConfig view.
        /// </summary>
        public IReportPortalConfigView ReportPortalConfigView
        {
            get { return _ReportPortalConfigView; }
            set { _ReportPortalConfigView = value; }
        }

        /// <summary>
        /// Property: Power BI manager.
        /// </summary>
        public PowerBIManager PowerBIManager
        {
            get { return _powerBIManager; }
            set { _powerBIManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="ReportPortalConfigView"></param>
        public ReportPortalConfigPresenter(SidekickConfiguration sidekickConfiguration,
                                       IReportPortalConfigView ReportPortalConfigView)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.ReportPortalConfigView = ReportPortalConfigView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private ReportPortalConfigPresenter()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// PowerBI get datasets.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="workspaceId"></param>
        public void GetDatasets(string powerBIApiEndpoint,
                                string azureApiEndpoint,
                                string workspaceCollectionName,
                                string workspaceCollectionAccessKey,
                                string workspaceId)
        {

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.ReportPortalConfigView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.ReportPortalConfigView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.ReportPortalConfigView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;
                    this.ReportPortalConfigView.ApplyButtonText = ApplyButtonText_Cancel;

                    this.PowerBIManager = this.CreateReportPortalConfigManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.PowerBIManager.PowerBIGetDatasetsCompleted += OnPowerBIGetDatasetsCompleted;

                    this.BackgroundTask = new Task(() => this.PowerBIManager.GetDatasets(powerBIApiEndpoint,
                                                                                           azureApiEndpoint,
                                                                                           workspaceCollectionName,
                                                                                           workspaceCollectionAccessKey,
                                                                                           workspaceId,
                                                                                           cancellationToken),
                                                                                                            cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// PowerBI get datasets.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="workspaceId"></param>
        /// <returns></returns>
        public List<PowerBIDataset> GetDatasetsSync(string powerBIApiEndpoint,
                                                         string azureApiEndpoint,
                                                         string workspaceCollectionName,
                                                         string workspaceCollectionAccessKey,
                                                         string workspaceId)
        {
            List<PowerBIDataset> result = null;

            try
            {
                this.PowerBIManager = this.CreateReportPortalConfigManager();

                result = this.PowerBIManager.GeDatasets(powerBIApiEndpoint,
                                                        azureApiEndpoint,
                                                        workspaceCollectionName,
                                                        workspaceCollectionAccessKey,
                                                        workspaceId);

                return result;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// PowerBI get workspaces.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        public void GetWorkspaces(string powerBIApiEndpoint,
                                  string azureApiEndpoint,
                                  string workspaceCollectionName,
                                  string workspaceCollectionAccessKey)
        {

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.ReportPortalConfigView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.ReportPortalConfigView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.ReportPortalConfigView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;
                    this.ReportPortalConfigView.ApplyButtonText = ApplyButtonText_Cancel;

                    this.PowerBIManager = this.CreateReportPortalConfigManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.PowerBIManager.PowerBIGetWorkspacesCompleted += OnPowerBIGetWorkspacesCompleted;

                    this.BackgroundTask = new Task(() => this.PowerBIManager.GetWorkspaces(powerBIApiEndpoint,
                                                                                           azureApiEndpoint,
                                                                                           workspaceCollectionName,
                                                                                           workspaceCollectionAccessKey,
                                                                                           cancellationToken),
                                                                                                            cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// PowerBI get workspaces.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        public List<PowerBIWorkspace> GetWorkspacesSync(string powerBIApiEndpoint,
                                                        string azureApiEndpoint,
                                                        string workspaceCollectionName,
                                                        string workspaceCollectionAccessKey)
        {
            List<PowerBIWorkspace> result = null;

            try
            {
                this.PowerBIManager = this.CreateReportPortalConfigManager();

                result = this.PowerBIManager.GetWorkspaces(powerBIApiEndpoint,
                                                           azureApiEndpoint,
                                                           workspaceCollectionName,
                                                           workspaceCollectionAccessKey);

                return result;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// PowerBI create workspace.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        public void CreateWorkspace(string powerBIApiEndpoint,
                                    string azureApiEndpoint,
                                    string workspaceCollectionName,
                                    string workspaceCollectionAccessKey)
        {

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.ReportPortalConfigView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.ReportPortalConfigView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.ReportPortalConfigView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;
                    this.ReportPortalConfigView.ApplyButtonText = ApplyButtonText_Cancel;

                    this.PowerBIManager = this.CreateReportPortalConfigManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.PowerBIManager.PowerBICreateWorkspaceCompleted += OnPowerBICreateWorkspaceCompleted;

                    this.BackgroundTask = new Task(() => this.PowerBIManager.CreateWorkspace(powerBIApiEndpoint,
                                                                                             azureApiEndpoint,
                                                                                             workspaceCollectionName,
                                                                                             workspaceCollectionAccessKey,
                                                                                             cancellationToken),
                                                                                                            cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// PowerBI delete dataset.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="workspaceId"></param>
        /// <param name="datasetId"></param>
        public void DeleteDataset(string powerBIApiEndpoint,
                                  string azureApiEndpoint,
                                  string workspaceCollectionName,
                                  string workspaceCollectionAccessKey,
                                  string workspaceId,
                                  string datasetId)
        {

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.ReportPortalConfigView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.ReportPortalConfigView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.ReportPortalConfigView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;
                    this.ReportPortalConfigView.ApplyButtonText = ApplyButtonText_Cancel;

                    this.PowerBIManager = this.CreateReportPortalConfigManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.PowerBIManager.PowerBIDeleteDatasetCompleted += OnPowerBIDeleteDatasetCompleted;

                    this.BackgroundTask = new Task(() => this.PowerBIManager.DeleteDataset(powerBIApiEndpoint,
                                                                                             azureApiEndpoint,
                                                                                             workspaceCollectionName,
                                                                                             workspaceCollectionAccessKey,
                                                                                             workspaceId,
                                                                                             datasetId,
                                                                                             cancellationToken),
                                                                                                            cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// PowerBI update credentials.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="workspaceId"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public void UpdateCredentials(string powerBIApiEndpoint,
                                      string azureApiEndpoint,
                                      string workspaceCollectionName,
                                      string workspaceCollectionAccessKey,
                                      string workspaceId,
                                      string username,
                                      string password)
        {

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.ReportPortalConfigView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.ReportPortalConfigView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.ReportPortalConfigView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;
                    this.ReportPortalConfigView.ApplyButtonText = ApplyButtonText_Cancel;

                    this.PowerBIManager = this.CreateReportPortalConfigManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.PowerBIManager.PowerBIUpdateCredentialsCompleted += OnPowerBIUpdateCredentialsCompleted;

                    this.BackgroundTask = new Task(() => this.PowerBIManager.UpdateCredentials(powerBIApiEndpoint,
                                                                                               azureApiEndpoint,
                                                                                               workspaceCollectionName,
                                                                                               workspaceCollectionAccessKey,
                                                                                               workspaceId,
                                                                                               username, 
                                                                                               password,
                                                                                               cancellationToken),
                                                                                                            cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// PowerBI import pbix file.
        /// </summary>
        /// <param name="powerBIApiEndpoint"></param>
        /// <param name="azureApiEndpoint"></param>
        /// <param name="workspaceCollectionName"></param>
        /// <param name="workspaceCollectionAccessKey"></param>
        /// <param name="pbixWorkspaceId"></param>
        /// <param name="pbixDatasetName"></param>
        /// <param name="pbixFilePath"></param>
        public void ImportPbixFile(string powerBIApiEndpoint,
                                   string azureApiEndpoint,
                                   string workspaceCollectionName,
                                   string workspaceCollectionAccessKey,
                                   string pbixWorkspaceId,
                                   string pbixDatasetName,
                                   string pbixFilePath)
        {

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.ReportPortalConfigView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.ReportPortalConfigView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.ReportPortalConfigView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;
                    this.ReportPortalConfigView.ApplyButtonText = ApplyButtonText_Cancel;

                    this.PowerBIManager = this.CreateReportPortalConfigManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.PowerBIManager.PowerBIImportPbixCompleted += OnPowerBIImportPbixCompleted;

                    this.BackgroundTask = new Task(() => this.PowerBIManager.ImportPbixFile(powerBIApiEndpoint,
                                                                                             azureApiEndpoint,
                                                                                             workspaceCollectionName,
                                                                                             workspaceCollectionAccessKey,
                                                                                             pbixWorkspaceId,
                                                                                             pbixDatasetName,
                                                                                             pbixFilePath,
                                                                                             cancellationToken),
                                                                                                            cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalDuration;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.ReportPortalConfigView.BackgroundTaskCompleted(BackgroundTaskStatus_Completed);
                        this.ReportPortalConfigView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format(BackgroundTaskStatus_Canceled));

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.ReportPortalConfigView.BackgroundTaskCompleted(string.Empty);
                        this.ReportPortalConfigView.ApplyButtonEnable(true);

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(BackgroundTaskException));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(BackgroundTaskException));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(RequestFailed));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(RequestFailed));
                                    }

                                }
                            }
                        }

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.ReportPortalConfigView.BackgroundTaskCompleted(Event_PowerBI_Operation_Completed);
                        this.ReportPortalConfigView.ApplyButtonEnable(true);

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        // <summary>
        // Create ReportPortalConfig manager.
        // </summary>
        // <returns></returns>
        protected PowerBIManager CreateReportPortalConfigManager()
        {
            PowerBIManager result = null;

            try
            {
                result = PowerBIManagerFactory.CreatePowerBIManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        // <summary>
        // Event Hanlder: PowerBI configuration update completed.
        // </summary>
        // <param name = "sender" ></ param >
        // < param name= "powerBIConfigUpdateEventArgs" ></ param >
        protected void OnPowerBIConfigUpdateCompleted(Object sender,
                                                      PowerBIConfigUpdateEventArgs powerBIConfigUpdateEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Log event completed with status.
                if (powerBIConfigUpdateEventArgs.PowerBIConfigUpdateResult.StatusList != null &&
                    powerBIConfigUpdateEventArgs.PowerBIConfigUpdateResult.StatusList.Count > 0)
                {
                    message = string.Format(Event_PowerBI_Operation_CompletedWithStatus,
                                            powerBIConfigUpdateEventArgs.PowerBIConfigUpdateResult.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (powerBIConfigUpdateEventArgs.PowerBIConfigUpdateResult.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                }
                //Log event completed without status.
                else
                {
                    message = string.Format(Event_PowerBI_Operation_Completed);
                    this.Logger.Info(message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        // <summary>
        // Event Hanlder: PowerBI get datasets completed.
        // </summary>
        // <param name = "sender" ></ param >
        // < param name= "powerBIConfigUpdateEventArgs" ></ param >
        protected void OnPowerBIGetDatasetsCompleted(Object sender,
                                                     PowerBIGetDatasetsEventArgs powerBIGetDatasetsEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Log event completed with status.
                if (powerBIGetDatasetsEventArgs.PowerBIGetDatasetsResult.StatusList != null &&
                    powerBIGetDatasetsEventArgs.PowerBIGetDatasetsResult.StatusList.Count > 0)
                {
                    message = string.Format(Event_PowerBIGetDatasets_CompletedWithStatus,
                                            powerBIGetDatasetsEventArgs.PowerBIGetDatasetsResult.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (powerBIGetDatasetsEventArgs.PowerBIGetDatasetsResult.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                }
                //Log event completed without status.
                else
                {
                    message = string.Format(Event_PowerBIGetDatasets_Completed);
                    this.Logger.Info(message);
                }

                this.ReportPortalConfigView.GetDatasetsDatasetList = powerBIGetDatasetsEventArgs.PowerBIGetDatasetsResult.DatasetList;
                this.ReportPortalConfigView.PopulateGetDatasetsDatasetList();

            }
            catch (Exception)
            {
                throw;
            }

        }
        // <summary>
        // Event Hanlder: PowerBI get workspaces completed.
        // </summary>
        // <param name = "sender" ></ param >
        // < param name= "powerBIConfigUpdateEventArgs" ></ param >
        protected void OnPowerBIGetWorkspacesCompleted(Object sender,
                                                      PowerBIGetWorkspacesEventArgs powerBIGetWorkspacesEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Log event completed with status.
                if (powerBIGetWorkspacesEventArgs.PowerBIGetWorkspacesResult.StatusList != null &&
                    powerBIGetWorkspacesEventArgs.PowerBIGetWorkspacesResult.StatusList.Count > 0)
                {
                    message = string.Format(Event_PowerBIGetWorkspaces_CompletedWithStatus,
                                            powerBIGetWorkspacesEventArgs.PowerBIGetWorkspacesResult.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (powerBIGetWorkspacesEventArgs.PowerBIGetWorkspacesResult.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                }
                //Log event completed without status.
                else
                {
                    message = string.Format(Event_PowerBIGetWorkspaces_Completed);
                    this.Logger.Info(message);
                }

                this.ReportPortalConfigView.GetWorkspacesWorkspaceIdList = powerBIGetWorkspacesEventArgs.PowerBIGetWorkspacesResult.WorkspaceIdList;
                this.ReportPortalConfigView.PopulateGetWorkspacesWorkspaceIdList();

            }
            catch (Exception)
            {
                throw;
            }

        }

        // <summary>
        // Event Hanlder: PowerBI create workspace completed.
        // </summary>
        // <param name = "sender" ></ param >
        // < param name= "powerBIConfigUpdateEventArgs" ></ param >
        protected void OnPowerBICreateWorkspaceCompleted(Object sender,
                                                      PowerBICreateWorkspaceEventArgs powerBICreateWorkspaceEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Log event completed with status.
                if (powerBICreateWorkspaceEventArgs.PowerBICreateWorkspaceResult.StatusList != null &&
                    powerBICreateWorkspaceEventArgs.PowerBICreateWorkspaceResult.StatusList.Count > 0)
                {
                    message = string.Format(Event_PowerBICreateWorkspace_CompletedWithStatus,
                                            powerBICreateWorkspaceEventArgs.PowerBICreateWorkspaceResult.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (powerBICreateWorkspaceEventArgs.PowerBICreateWorkspaceResult.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                }
                //Log event completed without status.
                else
                {
                    message = string.Format(Event_PowerBICreateWorkspace_Completed);
                    this.Logger.Info(message);
                }

                this.ReportPortalConfigView.CreateWorkspaceWorkspaceId = powerBICreateWorkspaceEventArgs.PowerBICreateWorkspaceResult.CreatedWorkspaceId;
                this.ReportPortalConfigView.PopulateCreateWorkspaceWorkspaceId();

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        // Event Hanlder: PowerBI delete dataset completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="powerBIDeleteDatasetEventArgs"></param>
        protected void OnPowerBIDeleteDatasetCompleted(Object sender,
                                                      PowerBIDeleteDatasetEventArgs powerBIDeleteDatasetEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Log event completed with status.
                if (powerBIDeleteDatasetEventArgs.PowerBIDeleteDatasetResult.StatusList != null &&
                    powerBIDeleteDatasetEventArgs.PowerBIDeleteDatasetResult.StatusList.Count > 0)
                {
                    message = string.Format(Event_PowerBIDeleteDataset_CompletedWithStatus,
                                            powerBIDeleteDatasetEventArgs.PowerBIDeleteDatasetResult.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (powerBIDeleteDatasetEventArgs.PowerBIDeleteDatasetResult.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                }
                //Log event completed without status.
                else
                {
                    message = string.Format(Event_PowerBIDeleteDataset_Completed);
                    this.Logger.Info(message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }


        /// <summary>
        // Event Hanlder: PowerBI delete dataset completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="powerBIDeleteDatasetEventArgs"></param>
        protected void OnPowerBIUpdateCredentialsCompleted(Object sender,
                                                           PowerBIUpdateCredentialsEventArgs powerBIUpdateCredentialsEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Log event completed with status.
                if (powerBIUpdateCredentialsEventArgs.PowerBIUpdateCredentialsResult.StatusList != null &&
                    powerBIUpdateCredentialsEventArgs.PowerBIUpdateCredentialsResult.StatusList.Count > 0)
                {
                    message = string.Format(Event_PowerBIUpdateCredentials_CompletedWithStatus,
                                            powerBIUpdateCredentialsEventArgs.PowerBIUpdateCredentialsResult.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (powerBIUpdateCredentialsEventArgs.PowerBIUpdateCredentialsResult.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                }
                //Log event completed without status.
                else
                {
                    message = string.Format(Event_PowerBIUpdateCredentials_Completed);
                    this.Logger.Info(message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        // <summary>
        // Event Hanlder: PowerBI import pbix file completed.
        // </summary>
        // <param name = "sender" ></ param >
        // < param name= "powerBIConfigUpdateEventArgs" ></ param >
        protected void OnPowerBIImportPbixCompleted(Object sender,
                                                      PowerBIImportPbixEventArgs powerBIImportPbixEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Log event completed with status.
                if (powerBIImportPbixEventArgs.PowerBIImportPbixResult.StatusList != null &&
                    powerBIImportPbixEventArgs.PowerBIImportPbixResult.StatusList.Count > 0)
                {
                    message = string.Format(Event_PowerBIImportPbix_CompletedWithStatus,
                                            powerBIImportPbixEventArgs.PowerBIImportPbixResult.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (powerBIImportPbixEventArgs.PowerBIImportPbixResult.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                }
                //Log event completed without status.
                else
                {
                    message = string.Format(Event_PowerBIImportPbix_Completed);
                    this.Logger.Info(message);
                }

                this.ReportPortalConfigView.PopulatePbixImportStatus(string.Format(PbixImportStatus_CompletedSuccessfully,
                                                                                   powerBIImportPbixEventArgs.PowerBIImportPbixResult.ImportId));

            }
            catch (Exception)
            {
                throw;
            }

        }


        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                        }
                        else
                        {
                            Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                }
                else
                {
                    Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
