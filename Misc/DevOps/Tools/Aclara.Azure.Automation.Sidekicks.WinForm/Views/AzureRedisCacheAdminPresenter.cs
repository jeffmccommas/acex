﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Logging;
using Aclara.AzureRedisCache.Client;
using Aclara.AzureRedisCache.Client.Events;
using Aclara.AzureRedisCache.Client.Results;
using Aclara.Tools.Common.StatusManagement;
using Aclara.Tools.Configuration.AzureRedisCacheConfig;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Aclara.AzureRedisCache.Client.Types.Enumerations;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public class AzureRedisCacheAdminPresenter
    {

        #region Private Constants

        private const string BackgroundTaskStatus_CancellingRequest = "Cancelling Azure Redis cache operation request...";
        private const string BackgroundTaskStatus_Requested = "Azure Redis cache operation requested...";
        private const string BackgroundTaskStatus_Completed = "Azure Redis cache operation request canceled.";
        private const string BackgroundTaskStatus_Canceled = "Azure Redis cache operation request canceled.";

        private const string BackgroundTaskTotalDuration = "[*] Azure Redis cache operation request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})";

        private const string ApplyButtonText_Cancel = "Cancel";

        private const string Event_AAAEventAAA_CompletedWithStatus = "Azure Redis cache operation completed. (Status --> {1})";
        private const string Event_AAAEventAAA_Completed = "Azure Redis cache operation completed.";

        private const string BackgroundTaskException = "Operation cancelled.";
        private const string BackgroundTaskExceptionUnobserved = "Operation cancelled. (Exception unobserved)";

        private const string RequestFailed = "Request failed.";

        #endregion

        #region Private Data Members

        private CustomLogger _logger;
        private SidekickConfiguration _sidekickConfiguration = null;
        private IAzureRedisCacheAdminView _azureRedisCacheAdminView = null;
        private AzureRedisCacheManager _azureRedisCacheManager = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.AzureRedisCacheAdminView.SidekickName,
                                               this.AzureRedisCacheAdminView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: AzureRedisCacheAdmin view.
        /// </summary>
        public IAzureRedisCacheAdminView AzureRedisCacheAdminView
        {
            get { return _azureRedisCacheAdminView; }
            set { _azureRedisCacheAdminView = value; }
        }

        /// <summary>
        /// Property: Azure redis cache admin manager.
        /// </summary>
        public AzureRedisCacheManager AzureRedisCacheManager
        {
            get { return _azureRedisCacheManager; }
            set { _azureRedisCacheManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="AzureRedisCacheAdminView"></param>
        public AzureRedisCacheAdminPresenter(SidekickConfiguration sidekickConfiguration,
                                       IAzureRedisCacheAdminView AzureRedisCacheAdminView)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.AzureRedisCacheAdminView = AzureRedisCacheAdminView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private AzureRedisCacheAdminPresenter()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// AzureRedisCacheAdmin.
        /// </summary>
        /// <param name="operationCount"></param>
        /// <param name="timePerOperation"></param>
        public void PerformAzureRedisCacheAdminOperation(AzureRedisCacheConfig azureRedisCacheConfig,
                                                         AzureRedisCacheAdminOperation azureRedisCacheAdminOption)
        {

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.AzureRedisCacheAdminView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.AzureRedisCacheAdminView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.AzureRedisCacheAdminView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;
                    this.AzureRedisCacheAdminView.ApplyButtonText = ApplyButtonText_Cancel;

                    this.AzureRedisCacheManager = this.CreateAzureRedisCacheManager(azureRedisCacheConfig);

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.AzureRedisCacheManager.AzureRedisCacheAdminStatusUpdated += OnAzureRedisCacheAdminStatusUpdated;

                    this.BackgroundTask = new Task(() => this.AzureRedisCacheManager.PerformAzureRedisCacheAdminOperation(azureRedisCacheAdminOption, cancellationToken), cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalDuration;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.AzureRedisCacheAdminView.BackgroundTaskCompleted(BackgroundTaskStatus_Completed);
                        this.AzureRedisCacheAdminView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format(BackgroundTaskStatus_Canceled));

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.AzureRedisCacheAdminView.BackgroundTaskCompleted(string.Empty);
                        this.AzureRedisCacheAdminView.ApplyButtonEnable(true);

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(BackgroundTaskException));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(BackgroundTaskException));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(RequestFailed));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(RequestFailed));
                                    }

                                }
                            }
                        }

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.AzureRedisCacheAdminView.BackgroundTaskCompleted(Event_AAAEventAAA_Completed);
                        this.AzureRedisCacheAdminView.ApplyButtonEnable(true);

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create Azure Redis cache manager.
        /// </summary>
        /// <param name="azureRedisCacheConfig"></param>
        /// <returns></returns>
        protected AzureRedisCacheManager CreateAzureRedisCacheManager(AzureRedisCacheConfig azureRedisCacheConfig)
        {
            AzureRedisCacheManager result = null;

            try
            {

                result = AzureRedisCacheManagerFactory.CreateRedisCacheManager(azureRedisCacheConfig);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: AAAEventAAA completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="azureRedisCacheAdminStatusEventArgs"></param>
        protected void OnAzureRedisCacheAdminStatusUpdated(Object sender,
                                                           AzureRedisCacheAdminStatusEventArgs azureRedisCacheAdminStatusEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Log event completed with status.
                if (azureRedisCacheAdminStatusEventArgs.AzureRedisCacheAdminStatusResult.StatusList != null &&
                    azureRedisCacheAdminStatusEventArgs.AzureRedisCacheAdminStatusResult.StatusList.Count > 0)
                {
                    message = string.Format(Event_AAAEventAAA_CompletedWithStatus,
                                            azureRedisCacheAdminStatusEventArgs.AzureRedisCacheAdminStatusResult.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (azureRedisCacheAdminStatusEventArgs.AzureRedisCacheAdminStatusResult.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                }
                //Log event completed without status.
                else
                {
                    message = string.Format(Event_AAAEventAAA_Completed);
                    this.Logger.Info(message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                        }
                        else
                        {
                            Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                }
                else
                {
                    Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
