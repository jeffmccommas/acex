﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Views;
using Aclara.Tools.Configuration.AzureRedisCacheConfig;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Aclara.AzureRedisCache.Client.Types.Enumerations;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public partial class AzureRedisCacheAdminForm : Form, ISidekickView, IAzureRedisCacheAdminView, ISidekickInfo
    {

        #region Private Constants

        private const string SidekickInfo_SidekickName = "AzureRedisCacheAdmin";
        private const string SidekickInfo_SidekickDescription = "Azure Redis Cache Administration";

        private const string SidekickConfiguration_AzureRedisCacheConnectionInfo_SubscriptionName = "SubscriptionName";
        private const string SidekickConfiguration_AzureRedisCacheConnectionInfo_EnvironmentName = "Name";
        private const string SidekickConfiguration_AzureRedisCacheConnectionInfo_RedisCacheName = "RedisCacheName";

        private const string AzureRedisCacheAdminOperation_Unspecified = "<Select Operation>";
        private const string AzureRedisCacheAdminOperation_ClearAzureRedisCache = "Clear Azure Redis Cache";

        private const string ValidationErrorMessage_OperationIsRequired = "Operation is required.";

        #endregion

        #region Private Data Members

        private AzureRedisCacheAdminPresenter _AzureRedisCacheAdminPresenter;
        private SidekickConfiguration _sidekickConfiguration = null;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: AzureRedisCacheAdmin presenter.
        /// </summary>
        public AzureRedisCacheAdminPresenter AzureRedisCacheAdminPresenter
        {
            get { return _AzureRedisCacheAdminPresenter; }
            set { _AzureRedisCacheAdminPresenter = value; }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property:  Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get { return this.ApplyButton.Text; }
            set { this.ApplyButton.Text = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                if (this.ApplyButton.Text == "Apply")
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickInfo_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickInfo_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Environment.
        /// </summary>
        public AzureStorageAccount_Environment Environment
        {
            get { return (AzureStorageAccount_Environment)this.EnvironmentComboBox.SelectedItem; }
            set { this.EnvironmentComboBox.SelectedItem = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <param></param>
        public AzureRedisCacheAdminForm(SidekickConfiguration sidekickConfiguration)
        {
            _sidekickConfiguration = sidekickConfiguration;

            InitializeComponent();

            this.InitializeControls();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Sidekick was activated.
        /// </summary>
        public void SidekickActivated()
        {
            try
            {
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Apply button enable. 
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            BindingSource azureStorageAccountBindingSource = null;

            try
            {

                this.AzureSubscriptionNameComboBox.FormattingEnabled = true;
                azureStorageAccountBindingSource = new BindingSource();
                azureStorageAccountBindingSource.DataSource = this.SidekickConfiguration.RedisCacheConnectionInfo.AzureSubscription;

                this.AzureSubscriptionNameComboBox.DataSource = azureStorageAccountBindingSource;
                this.AzureSubscriptionNameComboBox.DisplayMember = SidekickConfiguration_AzureRedisCacheConnectionInfo_SubscriptionName;
                this.AzureSubscriptionNameComboBox.ValueMember = SidekickConfiguration_AzureRedisCacheConnectionInfo_SubscriptionName;

                this.OperationComboBox.FormattingEnabled = true;
                this.OperationComboBox.Format += delegate (object sender, ListControlConvertEventArgs e)
                {
                    e.Value = ConvertAzureRedisCacheAdminOperationsToText((AzureRedisCacheAdminOperation)e.Value);
                };
                this.OperationComboBox.DataSource = Enum.GetValues(typeof(AzureRedisCacheAdminOperation));

                this.OperationComboBox.SelectedItem = AzureRedisCacheAdminOperation.ClearAzureRedisCache;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        // Convert Azure Redis cache admin operations enum value to text.
        /// </summary>
        /// <param name="azureRedisCacheAdminOperation"></param>
        /// <returns></returns>
        protected string ConvertAzureRedisCacheAdminOperationsToText(AzureRedisCacheAdminOperation azureRedisCacheAdminOperation)
        {
            string result = string.Empty;

            try
            {
                switch (azureRedisCacheAdminOperation)
                {
                    case AzureRedisCacheAdminOperation.Unspecified:
                        result = AzureRedisCacheAdminOperation_Unspecified;
                        break;
                    case AzureRedisCacheAdminOperation.ClearAzureRedisCache:
                        result = AzureRedisCacheAdminOperation_ClearAzureRedisCache;
                        break;
                    default:
                        result = AzureRedisCacheAdminOperation_Unspecified;
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {

            AzureRedisCacheConnectionInfo_RedisCache redisCache = null;
            AzureRedisCacheAdminOperation azureRedisCacheAdminOption = AzureRedisCacheAdminOperation.Unspecified;
            AzureRedisCacheConfig azureRedisCacheConfig = null;

            try
            {

                redisCache = (AzureRedisCacheConnectionInfo_RedisCache)this.AzureRedisCacheNameComboBox.SelectedItem;

                azureRedisCacheConfig = new AzureRedisCacheConfig();

                if (redisCache != null)
                {
                    azureRedisCacheConfig.Name = redisCache.RedisCacheName;
                    azureRedisCacheConfig.Endpoint = redisCache.Endpoint;
                    azureRedisCacheConfig.Password = redisCache.Password;
                    azureRedisCacheConfig.Ssl = redisCache.SSL;
                    azureRedisCacheConfig.ConnectRetry = redisCache.ConnectRetry;
                    azureRedisCacheConfig.ConnectTimeout = redisCache.ConnectTimeout;
                    azureRedisCacheConfig.SyncTimeout = redisCache.SyncTimeout;

                    if (this.OperationComboBox.SelectedItem is AzureRedisCacheAdminOperation)
                    {
                        if ((AzureRedisCacheAdminOperation)this.OperationComboBox.SelectedItem == AzureRedisCacheAdminOperation.Unspecified)
                        {
                            MessageBox.Show(string.Format(ValidationErrorMessage_OperationIsRequired));
                            return;
                        }
                        else
                        {
                            azureRedisCacheAdminOption = (AzureRedisCacheAdminOperation)this.OperationComboBox.SelectedItem;
                        }
                    }

                }

                this.AzureRedisCacheAdminPresenter.PerformAzureRedisCacheAdminOperation(azureRedisCacheConfig, azureRedisCacheAdminOption);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Azure subscription name combo box - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AzureSubscriptionNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingSource environmentBindingSource = null;

            try
            {
                AzureRedisCacheConnectionInfo_AzureSubscription azureSubscription = (AzureRedisCacheConnectionInfo_AzureSubscription)this.AzureSubscriptionNameComboBox.SelectedItem;

                environmentBindingSource = new BindingSource();
                environmentBindingSource.DataSource = this.SidekickConfiguration.RedisCacheConnectionInfo.AzureSubscription.Where(asub => asub.SubscriptionName == azureSubscription.SubscriptionName).Single().Environment;

                this.EnvironmentComboBox.DataSource = environmentBindingSource;
                this.EnvironmentComboBox.FormattingEnabled = true;
                this.EnvironmentComboBox.DisplayMember = SidekickConfiguration_AzureRedisCacheConnectionInfo_EnvironmentName;
                this.EnvironmentComboBox.ValueMember = SidekickConfiguration_AzureRedisCacheConnectionInfo_EnvironmentName;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Azure subscription name combo box - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnvironmentComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingSource redisCacheBindingSource = null;

            try
            {
                AzureRedisCacheConnectionInfo_AzureSubscription azureSubscription = (AzureRedisCacheConnectionInfo_AzureSubscription)this.AzureSubscriptionNameComboBox.SelectedItem;
                AzureRedisCacheConnectionInfo_Environment environment = (AzureRedisCacheConnectionInfo_Environment)this.EnvironmentComboBox.SelectedItem;

                redisCacheBindingSource = new BindingSource();
                redisCacheBindingSource.DataSource = this.SidekickConfiguration.RedisCacheConnectionInfo.AzureSubscription.Where(asub => asub.SubscriptionName == azureSubscription.SubscriptionName).Single()
                                                                                                                  .Environment.Where(env => env.Name == environment.Name).Single().RedisCache;

                this.AzureRedisCacheNameComboBox.DataSource = redisCacheBindingSource;
                this.AzureRedisCacheNameComboBox.FormattingEnabled = true;
                this.AzureRedisCacheNameComboBox.DisplayMember = SidekickConfiguration_AzureRedisCacheConnectionInfo_RedisCacheName;
                this.AzureRedisCacheNameComboBox.ValueMember = SidekickConfiguration_AzureRedisCacheConnectionInfo_RedisCacheName;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickAzureRedisCacheAdmin.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion


    }
}
