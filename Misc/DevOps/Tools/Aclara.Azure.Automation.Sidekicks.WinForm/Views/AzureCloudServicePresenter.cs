﻿using Aclara.Azure.Automation.Client;
using Aclara.Azure.Automation.Client.Events;
using Aclara.Azure.Automation.Client.Models;
using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Logging;
using Aclara.Azure.Automation.Sidekicks.WinForm.Utilities;
using Aclara.Tools.Common.StatusManagement;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public class AzureCloudServicePresenter
    {

        #region Private Constants

        private const string BackgroundTaskStatus_CancellingRequest = "Cancelling Azure cloud service operation request...";
        private const string BackgroundTaskStatus_Requested = "Azure cloud service operation requested...";
        private const string BackgroundTaskStatus_Completed = "Azure cloud service operationrequest canceled.";
        private const string BackgroundTaskStatus_Canceled = "Azure cloud service operation request canceled.";

        private const string BackgroundTaskTotalDuration = "[*] Azure cloud service request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})";

        private const string ApplyButtonText_Cancel = "Cancel";

        private const string Event_AzureCloudServiceRequest_BeganWithStatus = "Azure cloud service request completed. (Status --> {0})";
        private const string Event_AzureCloudService_ErrorWithStatus = "(Status --> {0})";

        private const string Event_AzureCloudService_CompletedWithStatus = "Azure cloud service request completed. (Status --> {0})";
        private const string Event_AzureCloudService_Completed = "Azure cloud service request completed.";

        private const string BackgroundTaskException = "Operation cancelled.";
        private const string BackgroundTaskExceptionUnobserved = "Operation cancelled. (Exception unobserved)";

        private const string RequestFailed = "Request failed.";

        #endregion

        #region Private Data Members

        private CustomLogger _logger;
        private SidekickConfiguration _sidekickConfiguration = null;
        private IAzureCloudServiceView _azureCloudServiceView = null;
        private AzureAutomationManager _azureAutomationManager = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.AzureCloudServiceView.SidekickName,
                                               this.AzureCloudServiceView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Azure cloud service view.
        /// </summary>
        public IAzureCloudServiceView AzureCloudServiceView
        {
            get { return _azureCloudServiceView; }
            set { _azureCloudServiceView = value; }
        }

        /// <summary>
        /// Property: Azure automation manager.
        /// </summary>
        public AzureAutomationManager AzureAutomationManager
        {
            get { return _azureAutomationManager; }
            set { _azureAutomationManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="AzureCloudServiceView"></param>
        public AzureCloudServicePresenter(SidekickConfiguration sidekickConfiguration,
                                          IAzureCloudServiceView AzureCloudServiceView)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.AzureCloudServiceView = AzureCloudServiceView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private AzureCloudServicePresenter()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Cancel background task.
        /// </summary>
        /// <param name="subscriptionName"></param>
        public void CancelBackgroundTask()
        {
            try
            {
                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {
                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.AzureCloudServiceView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.AzureCloudServiceView.ApplyButtonEnable(false);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Determine whether background task is running.
        /// </summary>
        /// <returns></returns>
        public bool IsBackgroundTaskRunning()
        {
            bool result = false;
            try
            {
                if (this.BackgroundTask == null)
                {
                    return result;
                }

                if (this.BackgroundTask.Status == TaskStatus.Running ||
                    this.BackgroundTask.Status == TaskStatus.WaitingToRun ||
                    this.BackgroundTask.Status == TaskStatus.WaitingForActivation ||
                    this.BackgroundTask.Status == TaskStatus.WaitingForChildrenToComplete)
                {
                    result = true;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        /// <summary>
        /// Select azure subscription.
        /// </summary>
        /// <param name="subscriptionName"></param>
        public void SelectAzureSubscription(string subscriptionName)
        {

            AzureRmPowerShellManager azurePowerShellManager = null;

            try
            {
                azurePowerShellManager = new AzureRmPowerShellManager();

                azurePowerShellManager.SelectAzureSubscription(subscriptionName);

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Azure cloud service(s).
        /// </summary>
        /// <param name="subscriptionName"></param>
        /// <param name="azureCloudServiceShallowList"></param>
        public void GetAzureCloudService(string subscriptionName, AzureCloudServiceShallowList azureCloudServiceShallowList)
        {

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.AzureCloudServiceView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.AzureCloudServiceView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                    this.BackgroundTask.IsCanceled == true ||
                    this.BackgroundTask.IsCompleted == true ||
                    this.BackgroundTask.IsFaulted == true)
                {

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.AzureCloudServiceView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;
                    this.AzureCloudServiceView.ApplyButtonText = ApplyButtonText_Cancel;

                    this.SelectAzureSubscription(subscriptionName);

                    this.AzureAutomationManager = this.CreateAzureAutomationManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.AzureAutomationManager.GetAzureCloudServiceBegan += OnGetAzureCloudServiceBegan;
                    this.AzureAutomationManager.GetAzureCloudServiceCompleted += OnGetAzureCloudServiceCompleted;

                    this.BackgroundTask = new Task(() => this.AzureAutomationManager.GetAzureCloudService(subscriptionName,
                                                                                                          azureCloudServiceShallowList,
                                                                                                          cancellationToken),
                                                                                                           cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    this.AzureCloudServiceView.EnableControlsDependantOnBackgroundTask();

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Scale Azure cloud service(s).
        /// </summary>
        /// <param name="subscriptionName"></param>
        public void ScaleAzureCloudService(string subscriptionName, 
                                           AzureCloudServiceShallowList azureCloudServiceShallowList,
                                           ScaleDirection scaleDirection)
        {

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.AzureCloudServiceView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.AzureCloudServiceView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                    this.BackgroundTask.IsCanceled == true ||
                    this.BackgroundTask.IsCompleted == true ||
                    this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.AzureCloudServiceView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;
                    this.AzureCloudServiceView.ApplyButtonText = ApplyButtonText_Cancel;

                    this.SelectAzureSubscription(subscriptionName);

                    this.AzureAutomationManager = this.CreateAzureAutomationManager();

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.AzureAutomationManager.GetAzureCloudServiceBegan += OnGetAzureCloudServiceBegan;
                    this.AzureAutomationManager.GetAzureCloudServiceCompleted += OnGetAzureCloudServiceCompleted;

                    this.AzureAutomationManager.ScaleAzureCloudServiceBegan += OnScaleAzureCloudServiceBegan;
                    this.AzureAutomationManager.ScaleAzureCloudServiceCompleted += OnScaleAzureCloudServiceCompleted;

                    this.BackgroundTask = new Task(() => this.AzureAutomationManager.ScaleAzureCloudService(subscriptionName,
                                                                                                            azureCloudServiceShallowList,
                                                                                                            scaleDirection,
                                                                                                            cancellationToken),
                                                                                                           cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    this.AzureCloudServiceView.EnableControlsDependantOnBackgroundTask();

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalDuration;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.AzureCloudServiceView.BackgroundTaskCompleted(BackgroundTaskStatus_Completed);
                        this.AzureCloudServiceView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format(BackgroundTaskStatus_Canceled));

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.AzureCloudServiceView.BackgroundTaskCompleted(string.Empty);
                        this.AzureCloudServiceView.ApplyButtonEnable(true);

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(BackgroundTaskException));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(BackgroundTaskException));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(RequestFailed));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(RequestFailed));
                                    }

                                }
                            }
                        }

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.AzureCloudServiceView.BackgroundTaskCompleted(Event_AzureCloudService_Completed);
                        this.AzureCloudServiceView.ApplyButtonEnable(true);

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

                this.AzureCloudServiceView.UpdateAzureCloudServiceStatus(string.Empty, 0, 0);
                this.AzureCloudServiceView.EnableControlsDependantOnBackgroundTask();

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create Azure automation manager.
        /// </summary>
        /// <returns></returns>
        protected AzureAutomationManager CreateAzureAutomationManager()
        {
            AzureAutomationManager result = null;

            try
            {
                result = AzureAutomationManagerFactory.CreateAzureAutomationManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: Get Azure cloud service began began.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="getAzureCloudServiceEventArgs"></param>
        protected void OnGetAzureCloudServiceBegan(Object sender,
                                                   GetAzureCloudServiceEventArgs getAzureCloudServiceEventArgs)
        {
            try
            {

                //Log event completed with status.
                if (getAzureCloudServiceEventArgs.StatusList != null &&
                    getAzureCloudServiceEventArgs.StatusList.Count > 0)
                {
                    this.LogEntryFromStatusList(Event_AzureCloudServiceRequest_BeganWithStatus, getAzureCloudServiceEventArgs.StatusList);
                }
                //Log event completed without status.
                else
                {
                    this.Logger.Info(getAzureCloudServiceEventArgs.Message);
                }

                this.AzureCloudServiceView.UpdateAzureCloudServiceStatus(getAzureCloudServiceEventArgs.Message, 
                                                                         getAzureCloudServiceEventArgs.ProcessedCount, 
                                                                         getAzureCloudServiceEventArgs.TotalCount);
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Event Hanlder: Get Azure cloud service completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="getAzureCloudServiceEventArgs"></param>
        protected void OnGetAzureCloudServiceCompleted(Object sender,
                                                       GetAzureCloudServiceEventArgs getAzureCloudServiceEventArgs)
        {

            try
            {

                //Log event completed with status.
                if (getAzureCloudServiceEventArgs.StatusList != null &&
                    getAzureCloudServiceEventArgs.StatusList.Count > 0)
                {
                    this.LogEntryFromStatusList(Event_AzureCloudService_CompletedWithStatus, getAzureCloudServiceEventArgs.StatusList);

                }
                //Log event completed without status.
                else
                {
                    this.Logger.Info(getAzureCloudServiceEventArgs.Message);
                }

                if (getAzureCloudServiceEventArgs.AzureCloudServiceShallow != null)
                {
                    this.AzureCloudServiceView.UpdateAzureCloudServiceList(getAzureCloudServiceEventArgs.AzureCloudServiceShallow);
                }

                this.AzureCloudServiceView.UpdateAzureCloudServiceStatus(getAzureCloudServiceEventArgs.Message,
                                                                         getAzureCloudServiceEventArgs.ProcessedCount,
                                                                         getAzureCloudServiceEventArgs.TotalCount);


            }
            catch (Exception)
            {
                throw;
            }

        }


        /// <summary>
        /// Event Hanlder: Scale Azure cloud service began began.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="scaleAzureCloudServiceEventArgs"></param>
        protected void OnScaleAzureCloudServiceBegan(Object sender,
                                                     ScaleAzureCloudServiceEventArgs scaleAzureCloudServiceEventArgs)
        {
            try
            {

                //Log event completed with status.
                if (scaleAzureCloudServiceEventArgs.StatusList != null &&
                    scaleAzureCloudServiceEventArgs.StatusList.Count > 0)
                {
                    this.LogEntryFromStatusList(Event_AzureCloudServiceRequest_BeganWithStatus, scaleAzureCloudServiceEventArgs.StatusList);
                }
                //Log event completed without status.
                else
                {
                    this.Logger.Info(scaleAzureCloudServiceEventArgs.Message);
                }

                this.AzureCloudServiceView.UpdateAzureCloudServiceStatus(scaleAzureCloudServiceEventArgs.Message, 
                                                                         scaleAzureCloudServiceEventArgs.ProcessedCount,
                                                                         scaleAzureCloudServiceEventArgs.TotalCount);
            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Event Hanlder: Scale Azure cloud service completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="scaleAzureCloudServiceEventArgs"></param>
        protected void OnScaleAzureCloudServiceCompleted(Object sender,
                                                         ScaleAzureCloudServiceEventArgs scaleAzureCloudServiceEventArgs)
        {

            try
            {

                //Log event completed with status.
                if (scaleAzureCloudServiceEventArgs.StatusList != null &&
                    scaleAzureCloudServiceEventArgs.StatusList.Count > 0)
                {
                    this.LogEntryFromStatusList(Event_AzureCloudService_CompletedWithStatus, scaleAzureCloudServiceEventArgs.StatusList);

                }
                //Log event completed without status.
                else
                {
                    this.Logger.Info(scaleAzureCloudServiceEventArgs.Message);
                }

                if (scaleAzureCloudServiceEventArgs.AzureCloudServiceShallow != null)
                {
                    this.AzureCloudServiceView.UpdateAzureCloudServiceList(scaleAzureCloudServiceEventArgs.AzureCloudServiceShallow);
                }

                this.AzureCloudServiceView.UpdateAzureCloudServiceStatus(scaleAzureCloudServiceEventArgs.Message,
                                                                         scaleAzureCloudServiceEventArgs.ProcessedCount,
                                                                         scaleAzureCloudServiceEventArgs.TotalCount);


            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Log entry from status list.
        /// </summary>
        /// <param name="messageWithFormattedText"></param>
        /// <param name="statusList"></param>
        protected void LogEntryFromStatusList(string messageWithFormattedText, StatusList statusList)
        {
            string message = string.Empty;

            try
            {
                message = string.Format(messageWithFormattedText,
                        statusList.Format(StatusTypes.FormatOption.Minimum));

                switch (statusList.GetHighestStatusSeverity())
                {
                    case StatusTypes.StatusSeverity.Unspecified:
                        this.Logger.Info(message);
                        break;
                    case StatusTypes.StatusSeverity.Error:
                        statusList = statusList.GetStatusListByStatusSeverity(StatusTypes.StatusSeverity.Error);
                        if (statusList != null && statusList[0] != null)
                        {
                            this.Logger.Error(statusList[0].Exception, message);
                        }
                        else
                        {
                            this.Logger.Error(message);
                        }
                        break;
                    case StatusTypes.StatusSeverity.Warning:
                        this.Logger.Warn(message);
                        break;
                    case StatusTypes.StatusSeverity.Informational:
                        this.Logger.Info(message);
                        break;
                    default:
                        this.Logger.Info(message);
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                        }
                        else
                        {
                            Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                }
                else
                {
                    Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
