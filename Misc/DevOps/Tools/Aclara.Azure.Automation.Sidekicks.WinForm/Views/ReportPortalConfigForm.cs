﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Views;
using Aclara.AzurePowerBI.Client.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public partial class ReportPortalConfigForm : Form, ISidekickView, IReportPortalConfigView, ISidekickInfo
    {

        #region Private Constants

        private const string SidekickInfo_SidekickName = "ReportPortalConfig";
        private const string SidekickInfo_SidekickDescription = "Report Portal Config";

        private const string SidekickConfiguration_AzureStorageAccountConnectionInfo_SubscriptionName = "SubscriptionName";
        private const string SidekickConfiguration_PowerBIConnectionInfo_EnvironmentName = "Name";
        private const string SidekickConfiguration_PowerBIConnectionInfo_WorkspaceCollectionName = "WorkspaceCollectionName";

        private const string Action_GetWorkspaces = "Get Workspaces";
        private const string Action_CreateWorkspace = "Create Workspace";
        private const string Action_ImportPbix = "Import PowerBI Desktop File (pbix)";
        private const string Action_UpdateCredentials = "Update Credentials";
        private const string Action_GetDatasets = "Get Datasets";
        private const string Action_DeleteDataset = "Delete Dataset";

        private const string CopyToClipboard_WorkspaceIdList_Header1 = "Workspace Collection Name: {0}";
        private const string CopyToClipboard_WorkspaceIdList_Header2 = "Workspace Ids";

        private const string GetWorkspacesWorkspaceListView_ColumnHeaderId = "Id";
        private const string GetWorkspacesWorkspaceListView_ColumnHeaderName = "Name";

        private const string GetDatasetsDatasetsListView_ColumnHeaderId = "Id";
        private const string GetDatasetsDatasetsListView_ColumnHeaderName = "Name";

        private const string DeleteDatasetDatasetsListView_ColumnHeaderId = "Id";
        private const string DeleteDatasetDatasetsListView_ColumnHeaderName = "Name";

        private const string ImportPbixDatasetsListView_ColumnHeaderId = "Id";
        private const string ImportPbixDatasetsListView_ColumnHeaderName = "Name";

        private const string DeleteDatasetConfirmation_PendingAction = "Are you sure you want to delete selected dataset (Id: {0}) ?";
        private const string DeleteDatasetConfirmation_ConfirmationMessage = @"Type ""YES"" to confirm dataset deletion.";
        private const string DeleteDatasetConfirmation_ExpectedTypeResponse = "YES";

        private const string CopyToClipboard_DatasetNameList_Header1 = "Workspace Collection Name: {0}";
        private const string CopyToClipboard_DatasetNameList_Header2 = "Workspace Id: {0}";
        private const string CopyToClipboard_DatasetNameList_Header3 = "Datasets (Id | Name):";

        private const string GetWorkspaceList_Status = "{0} workspaces retrieved.";
        private const string GetDatasetList_Status = "{0} Datasets retrieved.";

        private const string ValidationErrorMessage_WorkspaceIdIsRequired = "Workspace id is required.";
        private const string ValidationErrorMessage_DatasetNameIsRequired = "Dataset name is required.";
        private const string ValidationErrorMessage_PbixFilePathIsRequired = "PowerBI pbix file path is required.";
        private const string ValidationErrorMessage_UsernameIsRequired = "User name is required.";
        private const string ValidationErrorMessage_PasswordIsRequired = "Password is required.";
        private const string ValidationErrorMessage_DatasetNotSelected = "Dataset is not selected.";

        #endregion

        #region Public Types

        public enum ActionTypes
        {
            GetWorkspaces = 1,
            CreateWorkspace = 2,
            ImportPbix = 3,
            UpdateDatasetCredentials = 4,
            GetDatasets = 5,
            DeleteDataset = 6
        }

        #endregion

        #region Private Data Members

        private ReportPortalConfigPresenter _ReportPortalConfigPresenter;
        private SidekickConfiguration _sidekickConfiguration = null;
        private List<string> _getWorkspacesWorkspaceIdList;
        private List<string> _importPbixWorkspaceIdList;
        private List<PowerBIDataset> _getDatasetsDatasetList;
        private List<PowerBIDataset> _deleteDatasetsDatasetList;
        private List<PowerBIDataset> _importPbixDatasetList;
        private string _createWorkspaceWorkspaceId;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: ReportPortalConfig presenter.
        /// </summary>
        public ReportPortalConfigPresenter ReportPortalConfigPresenter
        {
            get { return _ReportPortalConfigPresenter; }
            set { _ReportPortalConfigPresenter = value; }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: "get workspaces" workspace id list.
        /// </summary>
        public List<string> GetWorkspacesWorkspaceIdList
        {
            get { return _getWorkspacesWorkspaceIdList; }
            set { _getWorkspacesWorkspaceIdList = value; }
        }

        /// <summary>
        /// Property: "import pbix" workspace id list.
        /// </summary>
        public List<string> ImportPbixWorkspaceIdList
        {
            get { return _getWorkspacesWorkspaceIdList; }
            set { _getWorkspacesWorkspaceIdList = value; }
        }

        /// <summary>
        /// Property: "get datasets" Dataset list.
        /// </summary>
        public List<PowerBIDataset> GetDatasetsDatasetList
        {
            get { return _getDatasetsDatasetList; }
            set { _getDatasetsDatasetList = value; }
        }

        /// <summary>
        /// Property: "delete dataset" Dataset list.
        /// </summary>
        public List<PowerBIDataset> DeleteDatasetDatasetList
        {
            get { return _deleteDatasetsDatasetList; }
            set { _deleteDatasetsDatasetList = value; }
        }

        /// <summary>
        /// Property: "import pbix" Dataset list.
        /// </summary>
        public List<PowerBIDataset> ImportPbixDatasetList
        {
            get { return _importPbixDatasetList; }
            set { _importPbixDatasetList = value; }
        }
        /// <summary>
        /// Property: Get datasets workspace id.
        /// </summary>
        public string GetDatasetsWorkspaceId
        {
            get { return this.GetDatasetsWorkspaceIdComboBox.Text; }
            set { this.GetDatasetsWorkspaceIdComboBox.Text = value; }
        }

        /// <summary>
        /// Property: "delete datasets" workspace id.
        /// </summary>
        public string DeleteDatasetWorkspaceId
        {
            get { return this.DeleteDatasetWorkspaceIdComboBox.Text; }
            set { this.DeleteDatasetWorkspaceIdComboBox.Text = value; }
        }

        /// <summary>
        /// Property: Create workspace workspace id.
        /// </summary>
        public string CreateWorkspaceWorkspaceId
        {
            get { return _createWorkspaceWorkspaceId; }
            set { _createWorkspaceWorkspaceId = value; }
        }

        /// <summary>
        /// Property: Update credentials workspace id.
        /// </summary>
        public string UpdateCredentialsWorkspaceId
        {
            get { return this.UpdateCredentialsWorkspaceIdComboBox.Text; }
            set { this.UpdateCredentialsWorkspaceIdComboBox.Text = value; }
        }

        /// <summary>
        /// Property: Update credentials user name.
        /// </summary>
        public string UpdateCredentialsUsername
        {
            get { return this.UpdateCredentialsUsernameTextBox.Text; }
            set { this.UpdateCredentialsUsernameTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Update credentials password.
        /// </summary>
        public string UpdateCredentialsPassword
        {
            get { return this.UpdateCredentialsPasswordTextBox.Text; }
            set { this.UpdateCredentialsPasswordTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Action.
        /// </summary>
        public ActionTypes Action
        {
            get
            {
                return (ActionTypes)this.SelectActionComboBox.SelectedItem;
            }
        }

        /// <summary>
        /// Property: Import pbix workspace id.
        /// </summary>
        public string ImportPbixWorkspaceId
        {
            get { return this.ImportPbixWorkspaceIdComboBox.Text; }
            set { this.ImportPbixWorkspaceIdComboBox.Text = value; }
        }

        /// <summary>
        /// Property: Import pbix dataset name.
        /// </summary>
        public string ImportPbixDatasetName
        {
            get { return this.ImportPbixDatasetNameTextBox.Text; }
            set { this.ImportPbixDatasetNameTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Import pbix file path.
        /// </summary>
        public string ImportPbixFilePath
        {
            get { return this.ImportPbixFilePathTextBox.Text; }
            set { this.ImportPbixFilePathTextBox.Text = value; }
        }

        /// <summary>
        /// Property:  Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get { return this.ApplyButton.Text; }
            set { this.ApplyButton.Text = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                if (this.ApplyButton.Text == "Apply")
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickInfo_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickInfo_SidekickDescription;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <param></param>
        public ReportPortalConfigForm(SidekickConfiguration sidekickConfiguration)
        {
            _sidekickConfiguration = sidekickConfiguration;

            _getWorkspacesWorkspaceIdList = new List<string>();

            InitializeComponent();

            this.InitializeControls();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Sidekick was activated.
        /// </summary>
        public void SidekickActivated()
        {
            try
            {
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Populate "get datasets" dataset list.
        /// </summary>
        public void PopulateGetDatasetsDatasetList()
        {

            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        PopulateGetDatasetsDatasetList();
                    });

                }
                else
                {

                    ColumnHeader IdColumnHeader;
                    ColumnHeader NameColumnHeader;

                    this.GetDatasetsDatasetsListView.Clear();
                    this.GetDatasetsDatasetsListView.View = View.Details;

                    IdColumnHeader = new ColumnHeader();
                    IdColumnHeader.Text = GetDatasetsDatasetsListView_ColumnHeaderId;
                    IdColumnHeader.Width = 250;

                    NameColumnHeader = new ColumnHeader();
                    NameColumnHeader.Text = GetDatasetsDatasetsListView_ColumnHeaderName;
                    NameColumnHeader.Width = 250;

                    this.GetDatasetsDatasetsListView.Columns.Add(IdColumnHeader);
                    this.GetDatasetsDatasetsListView.Columns.Add(NameColumnHeader);

                    foreach (PowerBIDataset dataset in this.GetDatasetsDatasetList)
                    {
                        string[] row = { dataset.Id, dataset.Name };
                        ListViewItem listViewItem = new ListViewItem(row);
                        this.GetDatasetsDatasetsListView.Items.Add(listViewItem);
                    }

                    this.GetDatasetsStatusLabel.Text = string.Format(GetDatasetList_Status,
                                                                     this.GetDatasetsDatasetsListView.Items.Count);
                    if (this.GetDatasetsDatasetsListView.Items.Count > 0)
                    {
                        this.GetDatasetsCopyButton.Enabled = true;
                        this.GetDatasetsClearButton.Enabled = true;
                    }
                    else
                    {
                        this.GetDatasetsCopyButton.Enabled = false;
                        this.GetDatasetsClearButton.Enabled = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Populate "delete dataset" dataset list.
        /// </summary>
        public void PopulateDeleteDatasetDatasetList()
        {

            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        PopulateDeleteDatasetDatasetList();
                    });

                }
                else
                {

                    ColumnHeader IdColumnHeader;
                    ColumnHeader NameColumnHeader;

                    this.DeleteDatasetDatasetsListView.Clear();
                    this.DeleteDatasetDatasetsListView.View = View.Details;

                    IdColumnHeader = new ColumnHeader();
                    IdColumnHeader.Text = DeleteDatasetDatasetsListView_ColumnHeaderId;
                    IdColumnHeader.Width = 250;

                    NameColumnHeader = new ColumnHeader();
                    NameColumnHeader.Text = DeleteDatasetDatasetsListView_ColumnHeaderName;
                    NameColumnHeader.Width = 250;

                    this.DeleteDatasetDatasetsListView.Columns.Add(IdColumnHeader);
                    this.DeleteDatasetDatasetsListView.Columns.Add(NameColumnHeader);

                    foreach (PowerBIDataset dataset in this.DeleteDatasetDatasetList)
                    {
                        string[] row = { dataset.Id, dataset.Name };
                        ListViewItem listViewItem = new ListViewItem(row);
                        this.DeleteDatasetDatasetsListView.Items.Add(listViewItem);
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Populate "import pbix" dataset list.
        /// </summary>
        public void PopulateImportPbixDatasetList()
        {

            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        PopulateImportPbixDatasetList();
                    });

                }
                else
                {

                    ColumnHeader IdColumnHeader;
                    ColumnHeader NameColumnHeader;

                    this.ImportPbixDatasetsListView.Clear();
                    this.ImportPbixDatasetsListView.View = View.Details;

                    IdColumnHeader = new ColumnHeader();
                    IdColumnHeader.Text = ImportPbixDatasetsListView_ColumnHeaderId;
                    IdColumnHeader.Width = 250;

                    NameColumnHeader = new ColumnHeader();
                    NameColumnHeader.Text = ImportPbixDatasetsListView_ColumnHeaderName;
                    NameColumnHeader.Width = 250;

                    this.ImportPbixDatasetsListView.Columns.Add(IdColumnHeader);
                    this.ImportPbixDatasetsListView.Columns.Add(NameColumnHeader);

                    foreach (PowerBIDataset dataset in this.ImportPbixDatasetList)
                    {
                        string[] row = { dataset.Id, dataset.Name };
                        ListViewItem listViewItem = new ListViewItem(row);
                        this.ImportPbixDatasetsListView.Items.Add(listViewItem);
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Populate "get workspaces" workspace id list.
        /// </summary>
        public void PopulateGetWorkspacesWorkspaceIdList()
        {

            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        PopulateGetWorkspacesWorkspaceIdList();
                    });

                }
                else
                {
                    this.GetWorkspacesWorkspaceIdListBox.Items.Clear();
                    foreach (string workspaceId in this.GetWorkspacesWorkspaceIdList)
                    {
                        this.GetWorkspacesWorkspaceIdListBox.Items.Add(workspaceId);
                    }
                    this.GetWorkspacesStatusLabel.Text = string.Format(GetWorkspaceList_Status,
                                                                       this.GetWorkspacesWorkspaceIdListBox.Items.Count);
                    if (this.GetWorkspacesWorkspaceIdListBox.Items.Count > 0)
                    {
                        this.GetWorkspacesCopyButton.Enabled = true;
                        this.GetWorkspacesClearButton.Enabled = true;
                    }
                    else
                    {
                        this.GetWorkspacesCopyButton.Enabled = false;
                        this.GetWorkspacesClearButton.Enabled = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Populate "import pbix" workspace id list.
        /// </summary>
        public void PopulateImportPbixWorkspaceIdList()
        {

            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        PopulateImportPbixWorkspaceIdList();
                    });

                }
                else
                {
                    this.ImportPbixWorkspaceIdComboBox.Items.Clear();
                    foreach (string workspaceId in this.ImportPbixWorkspaceIdList)
                    {
                        this.ImportPbixWorkspaceIdComboBox.Items.Add(workspaceId);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Populate "create workspace" workspace id.
        /// </summary>
        public void PopulateCreateWorkspaceWorkspaceId()
        {

            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        PopulateCreateWorkspaceWorkspaceId();
                    });

                }
                else
                {
                    this.CreateWorkspaceWorkspaceIdTextBox.Text = this.CreateWorkspaceWorkspaceId;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Populate Pbix import status.
        /// </summary>
        /// <param name="status"></param>
        public void PopulatePbixImportStatus(string status)
        {

            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        PopulatePbixImportStatus(status);
                    });

                }
                else
                {
                    this.PbixImportStatusLabel.Text = status;
                    ImportPbixRefreshDatasetsButton.PerformClick();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Apply button enable. 
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            BindingSource AzurePowerBIConnectionInfoBindingSource = null;

            try
            {

                this.AzureSubscriptionNameComboBox.FormattingEnabled = true;
                AzurePowerBIConnectionInfoBindingSource = new BindingSource();
                AzurePowerBIConnectionInfoBindingSource.DataSource = this.SidekickConfiguration.AzurePowerBIConnectionInfo.AzureSubscription.ToList();

                this.AzureSubscriptionNameComboBox.DataSource = AzurePowerBIConnectionInfoBindingSource;
                this.AzureSubscriptionNameComboBox.DisplayMember = SidekickConfiguration_AzureStorageAccountConnectionInfo_SubscriptionName;
                this.AzureSubscriptionNameComboBox.ValueMember = SidekickConfiguration_AzureStorageAccountConnectionInfo_SubscriptionName;

                this.SelectActionComboBox.FormattingEnabled = true;
                this.SelectActionComboBox.Format += delegate (object sender, ListControlConvertEventArgs e)
                {
                    e.Value = ConvertActionTypeToText((ActionTypes)e.Value);
                };
                this.SelectActionComboBox.DataSource = Enum.GetValues(typeof(ActionTypes));
                this.SelectActionComboBox.SelectedItem = ActionTypes.GetWorkspaces;

                this.GetWorkspacesCopyButton.Enabled = false;
                this.GetWorkspacesClearButton.Enabled = false;

                this.GetDatasetsCopyButton.Enabled = false;
                this.GetDatasetsClearButton.Enabled = false;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        // Convert action type enum value to text.
        /// </summary>
        /// <returns></returns>
        protected string ConvertActionTypeToText(ActionTypes actionType)
        {
            string result = string.Empty;

            try
            {
                switch (actionType)
                {
                    case ActionTypes.GetWorkspaces:
                        result = Action_GetWorkspaces;
                        break;
                    case ActionTypes.CreateWorkspace:
                        result = Action_CreateWorkspace;
                        break;
                    case ActionTypes.ImportPbix:
                        result = Action_ImportPbix;
                        break;
                    case ActionTypes.UpdateDatasetCredentials:
                        result = Action_UpdateCredentials;
                        break;
                    case ActionTypes.GetDatasets:
                        result = Action_GetDatasets;
                        break;
                    case ActionTypes.DeleteDataset:
                        result = Action_DeleteDataset;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected action type. (Action type: {0})",
                                                                            actionType));
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {

            string powerBIApiEndpoint = string.Empty;
            string azureApiEndpoint = string.Empty;
            string workspaceCollectionName = string.Empty;
            string workspaceCollectionAccessKey = string.Empty;
            string getDatasetsWorkspaceId = string.Empty;
            string deleteDatasetWorkspaceId = string.Empty;
            string deleteDatasetDatasetId = string.Empty;
            string updateCredentialsWorkspaceId = string.Empty;
            string updateCredentialsUsername = string.Empty;
            string updateCredentialsPassword = string.Empty;
            DialogResult dialogResult;
            ConfirmationForm confirmationForm = null;

            try
            {
                AzurePowerBIConnectionInfo_PowerBI powerBI = (AzurePowerBIConnectionInfo_PowerBI)this.WorkspaceCollectionComboBox.SelectedItem;

                powerBIApiEndpoint = powerBI.PowerBIApiEndpoint;
                azureApiEndpoint = powerBI.AzureApiEndpoint;
                workspaceCollectionName = powerBI.WorkspaceCollectionName;
                workspaceCollectionAccessKey = powerBI.WorkspaceCollectionAccessKey;

                if (this.Action == ActionTypes.CreateWorkspace)
                {
                    this.CreateWorkspaceWorkspaceIdTextBox.Text = string.Empty;
                    this.ReportPortalConfigPresenter.CreateWorkspace(powerBIApiEndpoint, azureApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey);
                }
                else if (this.Action == ActionTypes.DeleteDataset)
                {
                    deleteDatasetWorkspaceId = this.DeleteDatasetWorkspaceId;

                    //Validate required parameters.
                    if (string.IsNullOrEmpty(deleteDatasetWorkspaceId) == true)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_WorkspaceIdIsRequired));
                        return;
                    }
                    if (DeleteDatasetDatasetsListView.SelectedItems.Count == 0)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_DatasetNotSelected));
                        return;
                    }

                    deleteDatasetDatasetId = DeleteDatasetDatasetsListView.SelectedItems[0].Text;

                    //Request confirmation.
                    confirmationForm = ConfirmationFormFactory.CreateForm(string.Format(DeleteDatasetConfirmation_PendingAction, 
                                                                                        deleteDatasetDatasetId),
                                                                          DeleteDatasetConfirmation_ConfirmationMessage,
                                                                          DeleteDatasetConfirmation_ExpectedTypeResponse,
                                                                          true);
                    confirmationForm.StartPosition = FormStartPosition.CenterParent;
                    dialogResult = confirmationForm.ShowDialog(this);
                    if (dialogResult == DialogResult.OK)
                    {
                        this.ReportPortalConfigPresenter.DeleteDataset(powerBIApiEndpoint,
                                                                       azureApiEndpoint,
                                                                       workspaceCollectionName,
                                                                       workspaceCollectionAccessKey,
                                                                       deleteDatasetWorkspaceId,
                                                                       deleteDatasetDatasetId);
                    }

                }
                else if (this.Action == ActionTypes.GetDatasets)
                {
                    getDatasetsWorkspaceId = this.GetDatasetsWorkspaceId;

                    //Validate required parameters.
                    if (string.IsNullOrEmpty(getDatasetsWorkspaceId) == true)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_WorkspaceIdIsRequired));
                        return;
                    }

                    this.GetDatasetsDatasetsListView.Clear();
                    this.ReportPortalConfigPresenter.GetDatasets(powerBIApiEndpoint,
                                                                 azureApiEndpoint,
                                                                 workspaceCollectionName,
                                                                 workspaceCollectionAccessKey,
                                                                 getDatasetsWorkspaceId);
                }
                else if (this.Action == ActionTypes.GetWorkspaces)
                {
                    this.GetWorkspacesWorkspaceIdListBox.Items.Clear();
                    this.ReportPortalConfigPresenter.GetWorkspaces(powerBIApiEndpoint, azureApiEndpoint, workspaceCollectionName, workspaceCollectionAccessKey);
                }
                else if (this.Action == ActionTypes.ImportPbix)
                {
                    //Validate required parameters.
                    if (string.IsNullOrEmpty(this.ImportPbixWorkspaceId) == true)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_WorkspaceIdIsRequired));
                        return;
                    }
                    if (string.IsNullOrEmpty(this.ImportPbixDatasetName) == true)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_DatasetNameIsRequired));
                        return;
                    }
                    if (string.IsNullOrEmpty(this.ImportPbixFilePath) == true)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_PbixFilePathIsRequired));
                        return;
                    }

                    this.ReportPortalConfigPresenter.ImportPbixFile(powerBIApiEndpoint,
                                                                    azureApiEndpoint,
                                                                    workspaceCollectionName,
                                                                    workspaceCollectionAccessKey,
                                                                    this.ImportPbixWorkspaceId,
                                                                    this.ImportPbixDatasetName,
                                                                    this.ImportPbixFilePath);
                }
                else if (this.Action == ActionTypes.UpdateDatasetCredentials)
                {
                    updateCredentialsWorkspaceId = this.UpdateCredentialsWorkspaceId;

                    //Validate required parameters.
                    if (string.IsNullOrEmpty(updateCredentialsWorkspaceId) == true)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_WorkspaceIdIsRequired));
                        return;
                    }
                    if (string.IsNullOrEmpty(this.UpdateCredentialsUsername) == true)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_UsernameIsRequired));
                        return;
                    }
                    if (string.IsNullOrEmpty(this.UpdateCredentialsPassword) == true)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_PasswordIsRequired));
                        return;
                    }

                    updateCredentialsUsername = this.UpdateCredentialsUsername;
                    updateCredentialsPassword = this.UpdateCredentialsPassword;

                    this.ReportPortalConfigPresenter.UpdateCredentials(powerBIApiEndpoint,
                                               azureApiEndpoint,
                                               workspaceCollectionName,
                                               workspaceCollectionAccessKey,
                                               updateCredentialsWorkspaceId,
                                               updateCredentialsUsername,
                                               updateCredentialsPassword);
                }


            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Azure subscription name combo box - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AzureSubscriptionNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingSource environmentBindingSource = null;

            try
            {
                AzurePowerBIConnectionInfo_AzureSubscription azureSubscription = (AzurePowerBIConnectionInfo_AzureSubscription)this.AzureSubscriptionNameComboBox.SelectedItem;

                environmentBindingSource = new BindingSource();
                environmentBindingSource.DataSource = this.SidekickConfiguration.AzurePowerBIConnectionInfo.AzureSubscription.Where(asub => asub.SubscriptionName == azureSubscription.SubscriptionName).Single().Environment;

                this.EnvironmentComboBox.DataSource = environmentBindingSource;
                this.EnvironmentComboBox.FormattingEnabled = true;
                this.EnvironmentComboBox.DisplayMember = SidekickConfiguration_PowerBIConnectionInfo_EnvironmentName;
                this.EnvironmentComboBox.ValueMember = SidekickConfiguration_PowerBIConnectionInfo_EnvironmentName;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Environment combo box - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnvironmentComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingSource workspaceCollectionBindingSource = null;

            try
            {
                AzurePowerBIConnectionInfo_AzureSubscription azureSubscription = (AzurePowerBIConnectionInfo_AzureSubscription)this.AzureSubscriptionNameComboBox.SelectedItem;
                AzurePowerBIConnectionInfo_Environment environment = (AzurePowerBIConnectionInfo_Environment)this.EnvironmentComboBox.SelectedItem;

                workspaceCollectionBindingSource = new BindingSource();
                workspaceCollectionBindingSource.DataSource = this.SidekickConfiguration.AzurePowerBIConnectionInfo.AzureSubscription.Where(asub => asub.SubscriptionName == azureSubscription.SubscriptionName).Single().
                                                                                         Environment.Where(env => env.Name == environment.Name).Single().
                                                                                         PowerBI;

                this.WorkspaceCollectionComboBox.DataSource = workspaceCollectionBindingSource;
                this.WorkspaceCollectionComboBox.FormattingEnabled = true;
                this.WorkspaceCollectionComboBox.DisplayMember = SidekickConfiguration_PowerBIConnectionInfo_WorkspaceCollectionName;
                this.WorkspaceCollectionComboBox.ValueMember = SidekickConfiguration_PowerBIConnectionInfo_WorkspaceCollectionName;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Action type combo box - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectActionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            ActionTypes action = ActionTypes.CreateWorkspace;

            try
            {
                if (this.SelectActionComboBox.SelectedItem is ActionTypes)
                {
                    action = (ActionTypes)this.SelectActionComboBox.SelectedItem;

                    this.GetWorkspacesPanel.Visible = false;
                    this.CreateWorkspacePanel.Visible = false;
                    this.ImportPbixPanel.Visible = false;
                    this.UpdateCredentialsPanel.Visible = false;
                    this.GetDatasetsPanel.Visible = false;
                    this.DeleteDatasetPanel.Visible = false;
                    this.ApplyButton.Enabled = true;

                    switch (action)
                    {
                        case ActionTypes.GetWorkspaces:
                            this.ApplyButton.Enabled = false;
                            this.GetWorkspacesPanel.Visible = true;
                            break;
                        case ActionTypes.CreateWorkspace:
                            this.CreateWorkspacePanel.Visible = true;
                            break;
                        case ActionTypes.ImportPbix:
                            this.ImportPbixPanel.Visible = true;
                            break;
                        case ActionTypes.UpdateDatasetCredentials:
                            this.UpdateCredentialsPanel.Visible = true;
                            break;
                        case ActionTypes.GetDatasets:
                            this.ApplyButton.Enabled = false;
                            this.GetDatasetsPanel.Visible = true;
                            break;
                        case ActionTypes.DeleteDataset:
                            this.ApplyButton.Enabled = false;
                            this.DeleteDatasetPanel.Visible = true;
                            break;
                        default:
                            this.ApplyButton.Enabled = false;
                            this.GetWorkspacesPanel.Visible = true;
                            break;
                    }

                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }


        /// <summary>
        /// Event Handler: Open pbix file button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenPbixFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = null;
            DialogResult dialogResult = DialogResult.None;

            try
            {
                openFileDialog = new OpenFileDialog();

                openFileDialog.Filter = "PowerBI Desktop Files (.pbix)|*.pbix|All Files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.Multiselect = false;

                dialogResult = openFileDialog.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {
                    this.ImportPbixFilePathTextBox.Text = openFileDialog.FileName;
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickReportPortalConfig.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Get workspaces refersh workspaces button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetWorkspacesRefreshWorkspacesButton_Click(object sender, EventArgs e)
        {
            string powerBIApiEndpoint = string.Empty;
            string azureApiEndpoint = string.Empty;
            string workspaceCollectionName = string.Empty;
            string workspaceCollectionAccessKey = string.Empty;
            List<PowerBIWorkspace> powerBIWorkspaceList = null;
            try
            {
                AzurePowerBIConnectionInfo_PowerBI powerBI = (AzurePowerBIConnectionInfo_PowerBI)this.WorkspaceCollectionComboBox.SelectedItem;

                powerBIApiEndpoint = powerBI.PowerBIApiEndpoint;
                azureApiEndpoint = powerBI.AzureApiEndpoint;
                workspaceCollectionName = powerBI.WorkspaceCollectionName;
                workspaceCollectionAccessKey = powerBI.WorkspaceCollectionAccessKey;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                powerBIWorkspaceList = this.ReportPortalConfigPresenter.GetWorkspacesSync(powerBIApiEndpoint,
                                                                                          azureApiEndpoint,
                                                                                          workspaceCollectionName,
                                                                                          workspaceCollectionAccessKey);

                this.GetWorkspacesWorkspaceIdListBox.Items.Clear();
                foreach (PowerBIWorkspace powerBIWorkspace in powerBIWorkspaceList)
                {
                    this.GetWorkspacesWorkspaceIdListBox.Items.Add(powerBIWorkspace.Id);
                }

                if (this.GetWorkspacesWorkspaceIdListBox.Items.Count > 0)
                {
                    this.GetWorkspacesCopyButton.Enabled = true;
                    this.GetWorkspacesClearButton.Enabled = true;
                }
                else
                {
                    this.GetWorkspacesCopyButton.Enabled = false;
                    this.GetWorkspacesClearButton.Enabled = false;
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Copy workspace id list to clipboard button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetWorkspacesCopyButton_Click(object sender, EventArgs e)
        {
            StringBuilder workspaceIdsStringBuilder = null;
            string workspaceCollectionName = string.Empty;

            try
            {
                workspaceIdsStringBuilder = new StringBuilder();

                AzurePowerBIConnectionInfo_PowerBI powerBI = (AzurePowerBIConnectionInfo_PowerBI)this.WorkspaceCollectionComboBox.SelectedItem;

                workspaceCollectionName = powerBI.WorkspaceCollectionName;

                workspaceIdsStringBuilder.AppendLine(string.Format(CopyToClipboard_WorkspaceIdList_Header1, workspaceCollectionName));
                workspaceIdsStringBuilder.AppendLine(string.Format(CopyToClipboard_WorkspaceIdList_Header2));

                foreach (string workspaceId in this.GetWorkspacesWorkspaceIdListBox.Items)
                {
                    workspaceIdsStringBuilder.AppendLine(workspaceId);
                }

                Clipboard.SetDataObject(workspaceIdsStringBuilder.ToString());
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Clear workspace id list button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetWorkspacesClearButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.GetWorkspacesWorkspaceIdList.Clear();
                this.PopulateGetWorkspacesWorkspaceIdList();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Copy dataset name list to clipboard button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetDatasetsCopyButton_Click(object sender, EventArgs e)
        {
            StringBuilder datasetsStringBuilder = null;
            string workspaceCollectionName = string.Empty;

            try
            {
                datasetsStringBuilder = new StringBuilder();

                AzurePowerBIConnectionInfo_PowerBI powerBI = (AzurePowerBIConnectionInfo_PowerBI)this.WorkspaceCollectionComboBox.SelectedItem;

                workspaceCollectionName = powerBI.WorkspaceCollectionName;

                datasetsStringBuilder.AppendLine(string.Format(CopyToClipboard_DatasetNameList_Header1, workspaceCollectionName));
                datasetsStringBuilder.AppendLine(string.Format(CopyToClipboard_DatasetNameList_Header2, this.GetDatasetsWorkspaceId));
                datasetsStringBuilder.AppendLine(string.Format(CopyToClipboard_DatasetNameList_Header3));

                foreach (PowerBIDataset dataset in this.GetDatasetsDatasetList)
                {

                    datasetsStringBuilder.AppendLine(string.Format("{0} | {1}",
                                                                       dataset.Id,
                                                                       dataset.Name));
                }

                Clipboard.SetDataObject(datasetsStringBuilder.ToString());
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Clear dataset name list button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetDatasetsClearButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.GetDatasetsDatasetList.Clear();
                this.PopulateGetDatasetsDatasetList();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Get datasets refresh workspace list button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetDatasetsRefreshWorkspaceListButton_Click(object sender, EventArgs e)
        {
            string powerBIApiEndpoint = string.Empty;
            string azureApiEndpoint = string.Empty;
            string workspaceCollectionName = string.Empty;
            string workspaceCollectionAccessKey = string.Empty;
            List<PowerBIWorkspace> powerBIWorkspaceList = null;

            try
            {
                AzurePowerBIConnectionInfo_PowerBI powerBI = (AzurePowerBIConnectionInfo_PowerBI)this.WorkspaceCollectionComboBox.SelectedItem;

                powerBIApiEndpoint = powerBI.PowerBIApiEndpoint;
                azureApiEndpoint = powerBI.AzureApiEndpoint;
                workspaceCollectionName = powerBI.WorkspaceCollectionName;
                workspaceCollectionAccessKey = powerBI.WorkspaceCollectionAccessKey;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                powerBIWorkspaceList = this.ReportPortalConfigPresenter.GetWorkspacesSync(powerBIApiEndpoint,
                                                                                               azureApiEndpoint,
                                                                                               workspaceCollectionName,
                                                                                               workspaceCollectionAccessKey);

                this.GetDatasetsWorkspaceIdComboBox.Items.Clear();
                foreach (PowerBIWorkspace powerBIWorkspace in powerBIWorkspaceList)
                {
                    this.GetDatasetsWorkspaceIdComboBox.Items.Add(powerBIWorkspace.Id);
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Get datasets refresh datasets button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetDatasetsRefreshDatasetsButton_Click(object sender, EventArgs e)
        {
            string powerBIApiEndpoint = string.Empty;
            string azureApiEndpoint = string.Empty;
            string workspaceCollectionName = string.Empty;
            string workspaceCollectionAccessKey = string.Empty;
            string workspaceId = string.Empty;
            List<PowerBIDataset> powerBIDatasetList = null;
            try
            {
                AzurePowerBIConnectionInfo_PowerBI powerBI = (AzurePowerBIConnectionInfo_PowerBI)this.WorkspaceCollectionComboBox.SelectedItem;

                powerBIApiEndpoint = powerBI.PowerBIApiEndpoint;
                azureApiEndpoint = powerBI.AzureApiEndpoint;
                workspaceCollectionName = powerBI.WorkspaceCollectionName;
                workspaceCollectionAccessKey = powerBI.WorkspaceCollectionAccessKey;
                workspaceId = this.GetDatasetsWorkspaceId;

                //Validate required parameters.
                if (string.IsNullOrEmpty(workspaceId) == true)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_WorkspaceIdIsRequired));
                    return;
                }
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                powerBIDatasetList = this.ReportPortalConfigPresenter.GetDatasetsSync(powerBIApiEndpoint,
                                                                                               azureApiEndpoint,
                                                                                               workspaceCollectionName,
                                                                                               workspaceCollectionAccessKey,
                                                                                               workspaceId);
                this.GetDatasetsDatasetList = powerBIDatasetList;
                this.PopulateGetDatasetsDatasetList();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }
        /// <summary>
        /// Event Handler: Update credentials refresh workspace list button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateCredentialsRefreshWorkspaceIdsButton_Click(object sender, EventArgs e)
        {
            string powerBIApiEndpoint = string.Empty;
            string azureApiEndpoint = string.Empty;
            string workspaceCollectionName = string.Empty;
            string workspaceCollectionAccessKey = string.Empty;
            List<PowerBIWorkspace> powerBIWorkspaceList = null;

            try
            {
                AzurePowerBIConnectionInfo_PowerBI powerBI = (AzurePowerBIConnectionInfo_PowerBI)this.WorkspaceCollectionComboBox.SelectedItem;

                powerBIApiEndpoint = powerBI.PowerBIApiEndpoint;
                azureApiEndpoint = powerBI.AzureApiEndpoint;
                workspaceCollectionName = powerBI.WorkspaceCollectionName;
                workspaceCollectionAccessKey = powerBI.WorkspaceCollectionAccessKey;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                powerBIWorkspaceList = this.ReportPortalConfigPresenter.GetWorkspacesSync(powerBIApiEndpoint,
                                                                                               azureApiEndpoint,
                                                                                               workspaceCollectionName,
                                                                                               workspaceCollectionAccessKey);

                this.UpdateCredentialsWorkspaceIdComboBox.Items.Clear();
                foreach (PowerBIWorkspace powerBIWorkspace in powerBIWorkspaceList)
                {
                    this.UpdateCredentialsWorkspaceIdComboBox.Items.Add(powerBIWorkspace.Id);
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Delete dataset refresh workspace list button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteDatasetRefreshWorkspacesButton_Click(object sender, EventArgs e)
        {
            string powerBIApiEndpoint = string.Empty;
            string azureApiEndpoint = string.Empty;
            string workspaceCollectionName = string.Empty;
            string workspaceCollectionAccessKey = string.Empty;
            List<PowerBIWorkspace> powerBIWorkspaceList = null;
            try
            {
                AzurePowerBIConnectionInfo_PowerBI powerBI = (AzurePowerBIConnectionInfo_PowerBI)this.WorkspaceCollectionComboBox.SelectedItem;

                powerBIApiEndpoint = powerBI.PowerBIApiEndpoint;
                azureApiEndpoint = powerBI.AzureApiEndpoint;
                workspaceCollectionName = powerBI.WorkspaceCollectionName;
                workspaceCollectionAccessKey = powerBI.WorkspaceCollectionAccessKey;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                powerBIWorkspaceList = this.ReportPortalConfigPresenter.GetWorkspacesSync(powerBIApiEndpoint,
                                                                                               azureApiEndpoint,
                                                                                               workspaceCollectionName,
                                                                                               workspaceCollectionAccessKey);

                this.GetDatasetsWorkspaceIdComboBox.Items.Clear();
                foreach (PowerBIWorkspace powerBIWorkspace in powerBIWorkspaceList)
                {
                    this.DeleteDatasetWorkspaceIdComboBox.Items.Add(powerBIWorkspace.Id);
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Delete dataset refresh datasets button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteDatasetRefreshDatasetsButton_Click(object sender, EventArgs e)
        {
            string powerBIApiEndpoint = string.Empty;
            string azureApiEndpoint = string.Empty;
            string workspaceCollectionName = string.Empty;
            string workspaceCollectionAccessKey = string.Empty;
            string workspaceId = string.Empty;
            List<PowerBIDataset> powerBIDatasetList = null;
            try
            {
                AzurePowerBIConnectionInfo_PowerBI powerBI = (AzurePowerBIConnectionInfo_PowerBI)this.WorkspaceCollectionComboBox.SelectedItem;

                powerBIApiEndpoint = powerBI.PowerBIApiEndpoint;
                azureApiEndpoint = powerBI.AzureApiEndpoint;
                workspaceCollectionName = powerBI.WorkspaceCollectionName;
                workspaceCollectionAccessKey = powerBI.WorkspaceCollectionAccessKey;
                workspaceId = this.DeleteDatasetWorkspaceId;

                //Validate required parameters.
                if (string.IsNullOrEmpty(workspaceId) == true)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_WorkspaceIdIsRequired));
                    return;
                }
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                powerBIDatasetList = this.ReportPortalConfigPresenter.GetDatasetsSync(powerBIApiEndpoint,
                                                                                               azureApiEndpoint,
                                                                                               workspaceCollectionName,
                                                                                               workspaceCollectionAccessKey,
                                                                                               workspaceId);
                this.DeleteDatasetDatasetList = powerBIDatasetList;
                this.PopulateDeleteDatasetDatasetList();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Import pbix refresh workspace list button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportPbixRefreshWorkspacesButton_Click(object sender, EventArgs e)
        {
            string powerBIApiEndpoint = string.Empty;
            string azureApiEndpoint = string.Empty;
            string workspaceCollectionName = string.Empty;
            string workspaceCollectionAccessKey = string.Empty;
            List<PowerBIWorkspace> powerBIWorkspaceList = null;
            try
            {
                AzurePowerBIConnectionInfo_PowerBI powerBI = (AzurePowerBIConnectionInfo_PowerBI)this.WorkspaceCollectionComboBox.SelectedItem;

                powerBIApiEndpoint = powerBI.PowerBIApiEndpoint;
                azureApiEndpoint = powerBI.AzureApiEndpoint;
                workspaceCollectionName = powerBI.WorkspaceCollectionName;
                workspaceCollectionAccessKey = powerBI.WorkspaceCollectionAccessKey;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                powerBIWorkspaceList = this.ReportPortalConfigPresenter.GetWorkspacesSync(powerBIApiEndpoint,
                                                                                               azureApiEndpoint,
                                                                                               workspaceCollectionName,
                                                                                               workspaceCollectionAccessKey);

                this.ImportPbixWorkspaceIdComboBox.Items.Clear();
                foreach (PowerBIWorkspace powerBIWorkspace in powerBIWorkspaceList)
                {
                    this.ImportPbixWorkspaceIdComboBox.Items.Add(powerBIWorkspace.Id);
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Import pbix refresh datasets button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportPbixRefreshDatasetsButton_Click(object sender, EventArgs e)
        {
            string powerBIApiEndpoint = string.Empty;
            string azureApiEndpoint = string.Empty;
            string workspaceCollectionName = string.Empty;
            string workspaceCollectionAccessKey = string.Empty;
            string workspaceId = string.Empty;
            List<PowerBIDataset> powerBIDatasetList = null;

            try
            {
                AzurePowerBIConnectionInfo_PowerBI powerBI = (AzurePowerBIConnectionInfo_PowerBI)this.WorkspaceCollectionComboBox.SelectedItem;

                powerBIApiEndpoint = powerBI.PowerBIApiEndpoint;
                azureApiEndpoint = powerBI.AzureApiEndpoint;
                workspaceCollectionName = powerBI.WorkspaceCollectionName;
                workspaceCollectionAccessKey = powerBI.WorkspaceCollectionAccessKey;
                workspaceId = this.ImportPbixWorkspaceId;

                //Validate required parameters.
                if (string.IsNullOrEmpty(workspaceId) == true)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_WorkspaceIdIsRequired));
                    return;
                }
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                powerBIDatasetList = this.ReportPortalConfigPresenter.GetDatasetsSync(powerBIApiEndpoint,
                                                                                      azureApiEndpoint,
                                                                                      workspaceCollectionName,
                                                                                      workspaceCollectionAccessKey,
                                                                                      workspaceId);
                this.ImportPbixDatasetList = powerBIDatasetList;
                this.PopulateImportPbixDatasetList();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Delete dataset datasets list view - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteDatasetDatasetsListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (DeleteDatasetDatasetsListView.SelectedItems.Count == 0)
                {
                    this.ApplyButton.Enabled = false;
                }
                else
                {
                    this.ApplyButton.Enabled = true;
                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }


        #endregion


    }
}
