﻿namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    partial class SidekickComponentManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SidekickActivationStatusPanel = new System.Windows.Forms.Panel();
            this.ContinueButton = new System.Windows.Forms.Button();
            this.SidekickActivationStatusLabel = new System.Windows.Forms.Label();
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.SidekickToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SidekickActivationStatusPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // SidekickActivationStatusPanel
            // 
            this.SidekickActivationStatusPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SidekickActivationStatusPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.SidekickActivationStatusPanel.Controls.Add(this.ContinueButton);
            this.SidekickActivationStatusPanel.Controls.Add(this.SidekickActivationStatusLabel);
            this.SidekickActivationStatusPanel.Location = new System.Drawing.Point(0, 0);
            this.SidekickActivationStatusPanel.Name = "SidekickActivationStatusPanel";
            this.SidekickActivationStatusPanel.Size = new System.Drawing.Size(687, 27);
            this.SidekickActivationStatusPanel.TabIndex = 5;
            // 
            // ContinueButton
            // 
            this.ContinueButton.Location = new System.Drawing.Point(5, 2);
            this.ContinueButton.Name = "ContinueButton";
            this.ContinueButton.Size = new System.Drawing.Size(75, 23);
            this.ContinueButton.TabIndex = 7;
            this.ContinueButton.Text = "Continue...";
            this.ContinueButton.UseVisualStyleBackColor = true;
            this.ContinueButton.Click += new System.EventHandler(this.ContinueButton_Click);
            // 
            // SidekickActivationStatusLabel
            // 
            this.SidekickActivationStatusLabel.AutoSize = true;
            this.SidekickActivationStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SidekickActivationStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.SidekickActivationStatusLabel.Location = new System.Drawing.Point(86, 7);
            this.SidekickActivationStatusLabel.Name = "SidekickActivationStatusLabel";
            this.SidekickActivationStatusLabel.Size = new System.Drawing.Size(55, 13);
            this.SidekickActivationStatusLabel.TabIndex = 6;
            this.SidekickActivationStatusLabel.Text = "<status>";
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.Azure.Automation.Sidekicks.Help.chm";
            // 
            // SidekickComponentManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 27);
            this.ControlBox = false;
            this.Controls.Add(this.SidekickActivationStatusPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SidekickComponentManagerForm";
            this.Text = "ComponentManagerForm";
            this.SidekickActivationStatusPanel.ResumeLayout(false);
            this.SidekickActivationStatusPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel SidekickActivationStatusPanel;
        private System.Windows.Forms.Label SidekickActivationStatusLabel;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.Button ContinueButton;
        private System.Windows.Forms.ToolTip SidekickToolTip;
    }
}