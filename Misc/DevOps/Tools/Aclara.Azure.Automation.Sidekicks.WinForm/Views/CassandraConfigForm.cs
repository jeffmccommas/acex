﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static CE.AO.ConfigurationManagement.Types.Enumerations;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public partial class CassandraConfigForm : Form, ISidekickView, ICassandraConfigView, ISidekickInfo
    {

        #region Private Constants

        private const string SidekickInfo_SidekickName = "CassandraConfig";
        private const string SidekickInfo_SidekickDescription = "Cassandra Configuration";

        private const string SidekickConfiguration_CassandraConnectionInfo_SubscriptionName = "SubscriptionName";
        private const string SidekickConfiguration_CassandraConnectionInfo_EnvironmentName = "Name";

        private const string CassandraTableName_SelectAzureTableName = "<Select Azure Table Name>";
        private const string CassandraTableName_BillCycleSchedule = "Bill Cycle Schedule";
        private const string CassandraTableName_SmsTemplate = "SMS Template";
        private const string CassandraTableName_TrumpiaKeywordConfiguration = "Trumpia Keyward Configuration";

        private const string ValidationErrorMessage_EnvironmentIsRequired = "Environment is required when selecting preconfigured connection information.";
        private const string ValidationErrorMessage_CassandraNodesIsRequired = "Cassandra nodes is required when selecting preconfigured connection information.";
        private const string ValidationErrorMessage_CassandraKeyspaceIsRequired = "Cassandra keyspace is required when selecting preconfigured connection information.";
        private const string ValidationErrorMessage_CassandraNodeUsernameIsRequired = "Cassandra node username is required when selecting preconfigured connection information.";
        private const string ValidationErrorMessage_CassandraNodePasswordIsRequired = "Cassandra node password is required when selecting preconfigured connection information.";

        #endregion

        #region Private Data Members

        private CassandraConfigPresenter _CassandraConfigPresenter;
        private SidekickConfiguration _sidekickConfiguration = null;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: CassandraConfig presenter.
        /// </summary>
        public CassandraConfigPresenter CassandraConfigPresenter
        {
            get { return _CassandraConfigPresenter; }
            set { _CassandraConfigPresenter = value; }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property:  Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get { return this.ApplyButton.Text; }
            set { this.ApplyButton.Text = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                if (this.ApplyButton.Text == "Apply")
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickInfo_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickInfo_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Environment.
        /// </summary>
        public CassandraConnectionInfo_Environment Environment
        {
            get { return (CassandraConnectionInfo_Environment)this.EnvironmentComboBox.SelectedItem; }
            set { this.EnvironmentComboBox.SelectedItem = value; }
        }

        /// <summary>
        /// Property: Cassandra nodes.
        /// </summary>
        public string CassandraNodes
        {
            get
            {
                return this.CassandraNodesTextBox.Text;
            }

        }

        /// <summary>
        /// Property: Cassandra keyspace.
        /// </summary>
        public string CassandraKeyspace
        {
            get
            {
                return this.CassandraKeyspaceTextBox.Text;
            }

        }

        /// <summary>
        /// Property: Cassandra node user name.
        /// </summary>
        public string CassandraNodeUsername
        {
            get
            {
                return this.CassandraNodeUsernameTextBox.Text;
            }

        }

        /// <summary>
        /// Property: Cassandra node password.
        /// </summary>
        public string CassandraNodePassword
        {
            get
            {
                return this.CassandraNodePasswordTextBox.Text;
            }

        }

        /// <summary>
        /// Property: Cassandra table.
        /// </summary>
        public CassandraTable CassandraTable
        {
            get { return (CassandraTable)this.CassandraTableNameComboBox.SelectedItem; }
            set { this.CassandraTableNameComboBox.SelectedItem = value; }
        }

        /// <summary>
        /// Property: Configuration file path name.
        /// </summary>
        public string ConfigurationFilePathName
        {
            get { return this.ConfigurationFilePathNameTextBox.Text; }
            set { this.ConfigurationFilePathNameTextBox.Text = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <param></param>
        public CassandraConfigForm(SidekickConfiguration sidekickConfiguration)
        {
            _sidekickConfiguration = sidekickConfiguration;

            InitializeComponent();

            this.InitializeControls();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Sidekick was activated.
        /// </summary>
        public void SidekickActivated()
        {
            try
            {
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Apply button enable. 
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            BindingSource CassandraBindingSource = null;

            try
            {

                this.AzureSubscriptionNameComboBox.FormattingEnabled = true;
                CassandraBindingSource = new BindingSource();
                CassandraBindingSource.DataSource = this.SidekickConfiguration.CassandraConnectionInfo.AzureSubscription.ToList();

                this.AzureSubscriptionNameComboBox.DataSource = CassandraBindingSource;
                this.AzureSubscriptionNameComboBox.DisplayMember = SidekickConfiguration_CassandraConnectionInfo_SubscriptionName;
                this.AzureSubscriptionNameComboBox.ValueMember = SidekickConfiguration_CassandraConnectionInfo_SubscriptionName;

                this.PreconfiguredConnectionInfoRadioButton.Checked = true;
                this.ManualEntryConnectionInfoRadioButton.Checked = false;

                // Initialize Cassandra table combobox.
                this.CassandraTableNameComboBox.FormattingEnabled = true;
                this.CassandraTableNameComboBox.Format += delegate (object sender, ListControlConvertEventArgs e)
                {
                    e.Value = ConvertCassandraTableToText((CassandraTable)e.Value);
                };
                this.CassandraTableNameComboBox.DataSource = Enum.GetValues(typeof(CassandraTable));

                this.CassandraTableNameComboBox.SelectedItem = CassandraTable.Unspecified;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Toggle configuration information UI controls.
        /// </summary>
        protected void ToggleConfigurationInfoUIControls()
        {
            if (PreconfiguredConnectionInfoRadioButton.Checked == true)
            {
                EnvironmentComboBox.Enabled = true;
                CassandraNodesTextBox.Enabled = false;
                CassandraKeyspaceTextBox.Enabled = false;
                CassandraNodeUsernameTextBox.Enabled = false;
                CassandraNodePasswordTextBox.Enabled = false;
            }
            else if (ManualEntryConnectionInfoRadioButton.Checked == true)
            {
                EnvironmentComboBox.Enabled = false;
                CassandraNodesTextBox.Enabled = true;
                CassandraKeyspaceTextBox.Enabled = true;
                CassandraNodeUsernameTextBox.Enabled = true;
                CassandraNodePasswordTextBox.Enabled = true;
            }
        }

        /// <summary>
        // Convert Cassandra table enum value to text.
        /// </summary>
        /// <param name="dateFilter"></param>
        /// <returns></returns>
        protected string ConvertCassandraTableToText(CassandraTable cassandraTable)
        {
            string result = string.Empty;

            try
            {
                switch (cassandraTable)
                {
                    case CassandraTable.Unspecified:
                        result = CassandraTableName_SelectAzureTableName;
                        break;
                    case CassandraTable.BillCycleSchedule:
                        result = CassandraTableName_BillCycleSchedule;
                        break;
                    case CassandraTable.SmsTemplate:
                        result = CassandraTableName_SmsTemplate;
                        break;
                    case CassandraTable.TrumpiaKeywordConfiguration:
                        result = CassandraTableName_TrumpiaKeywordConfiguration;
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {

            string cassandraNodes = string.Empty;
            string cassandraKeyspace = string.Empty;
            string cassandraNodeUsername = string.Empty;
            string cassandraNodePassword = string.Empty;
            CassandraTable cassandraTable = CassandraTable.Unspecified;
            string configurationPathFileName = string.Empty;

            try
            {

                // Validate: Preconfigured connection information.
                if (this.PreconfiguredConnectionInfoRadioButton.Checked == true && this.Environment == null)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_EnvironmentIsRequired));
                    return;
                }

                // Validate: Manually entered connection information.
                if (this.ManualEntryConnectionInfoRadioButton.Checked == true && string.IsNullOrEmpty(this.CassandraNodesTextBox.Text) == true)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_CassandraNodesIsRequired));
                    return;
                }
                if (this.ManualEntryConnectionInfoRadioButton.Checked == true && string.IsNullOrEmpty(this.CassandraKeyspaceTextBox.Text) == true)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_CassandraKeyspaceIsRequired));
                    return;
                }
                if (this.ManualEntryConnectionInfoRadioButton.Checked == true && string.IsNullOrEmpty(this.CassandraNodeUsernameTextBox.Text) == true)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_CassandraNodeUsernameIsRequired));
                    return;
                }
                if (this.ManualEntryConnectionInfoRadioButton.Checked == true && string.IsNullOrEmpty(this.CassandraNodePasswordTextBox.Text) == true)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_CassandraNodePasswordIsRequired));
                    return;
                }

                // Retrieve Cassand connection information.
                if (this.PreconfiguredConnectionInfoRadioButton.Checked == true)
                {
                    cassandraNodes = this.Environment.CassandraNodes;
                    cassandraKeyspace = this.Environment.CassandraKeyspace;
                    cassandraNodeUsername = this.Environment.CassandraNodeUsername;
                    cassandraNodePassword = this.Environment.CassandraNodePassword;
                }
                else if (this.ManualEntryConnectionInfoRadioButton.Checked == true)
                {
                    cassandraNodes = this.CassandraNodes;
                    cassandraKeyspace = this.CassandraKeyspace;
                    cassandraNodeUsername = this.CassandraNodeUsername;
                    cassandraNodePassword = this.CassandraNodePassword;
                }

                //Retrieve configuration information.
                cassandraTable = this.CassandraTable;
                configurationPathFileName = this.ConfigurationFilePathName;

                this.CassandraConfigPresenter.UpdateCassandraTableWithConfigurationFile(cassandraNodes,
                                                                                        cassandraKeyspace,
                                                                                        cassandraNodeUsername,
                                                                                        cassandraNodePassword,
                                                                                        cassandraTable,
                                                                                        configurationPathFileName);

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Preconfigured connection information Radio Button - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PreconfiguredConnectionInfoRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ToggleConfigurationInfoUIControls();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Manual entry connection information Radio Button - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ManualEntryConnectionInfoRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ToggleConfigurationInfoUIControls();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Azure subscription name combo box - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AzureSubscriptionNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingSource environmentBindingSource = null;

            try
            {
                CassandraConnectionInfo_AzureSubscription azureSubscription = (CassandraConnectionInfo_AzureSubscription)this.AzureSubscriptionNameComboBox.SelectedItem;

                environmentBindingSource = new BindingSource();
                environmentBindingSource.DataSource = this.SidekickConfiguration.CassandraConnectionInfo.AzureSubscription.Where(asub => asub.SubscriptionName == azureSubscription.SubscriptionName).Single().Environment;

                this.EnvironmentComboBox.DataSource = environmentBindingSource;
                this.EnvironmentComboBox.FormattingEnabled = true;
                this.EnvironmentComboBox.DisplayMember = SidekickConfiguration_CassandraConnectionInfo_EnvironmentName;
                this.EnvironmentComboBox.ValueMember = SidekickConfiguration_CassandraConnectionInfo_EnvironmentName;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Open configuration file button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenConfigurationFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = null;
            DialogResult dialogResult = DialogResult.None;

            try
            {
                openFileDialog = new OpenFileDialog();

                openFileDialog.Filter = "Csv Files (.csv)|*.csv|All Files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.Multiselect = false;

                dialogResult = openFileDialog.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {
                    this.ConfigurationFilePathNameTextBox.Text = openFileDialog.FileName;
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickCassandraConfig.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion
    }
}