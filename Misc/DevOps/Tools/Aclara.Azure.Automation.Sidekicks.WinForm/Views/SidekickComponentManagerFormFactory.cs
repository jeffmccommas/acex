﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Utilities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public class SidekickComponentManagerFormFactory
    {

        #region Private Constants
        #endregion

        #region Public Methods

        /// <summary>
        /// Create form.
        /// </summary>
        /// <returns></returns>
        public static SidekickComponentManagerForm CreateForm(SidekickConfiguration sidekickConfiguration, 
                                                              SidekickComponentManager sidekickComponentManager)
        {

            SidekickComponentManagerForm result = null;
            SidekickComponentManagerPresenter componentManagerPresenter = null;

            try
            {
                result = new SidekickComponentManagerForm(sidekickConfiguration, sidekickComponentManager);

                componentManagerPresenter = new SidekickComponentManagerPresenter(result);

                result.ComponentManagerPresenter = componentManagerPresenter;
                result.TopLevel = false;

                componentManagerPresenter.Initialize();

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion
    }
}
