﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{

    /// <summary>
    /// Confirmation view - interface.
    /// </summary>
    public interface IConfirmationView
    {
        string PendingActionMessage { get; set; }
        string ConfirmationMessage { get; set; }
        string ExpectedTypedResponseValue { get; set; }
        string TypedResponseValue { get; set; }
        bool RequireTypedResponse { get; set; }
    }
}
