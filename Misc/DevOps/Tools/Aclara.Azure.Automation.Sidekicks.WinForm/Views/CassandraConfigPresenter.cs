﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Tools.Common.StatusManagement;
using CE.AO.ConfigurationManagement;
using CE.AO.ConfigurationManagement.Events;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Aclara.Azure.Automation.Sidekicks.WinForm.Logging;
using static CE.AO.ConfigurationManagement.Types.Enumerations;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public class CassandraConfigPresenter
    {

        #region Private Constants

        private const string BackgroundTaskStatus_CancellingRequest = "Cancelling Cassandra Config request...";
        private const string BackgroundTaskStatus_Requested = "Cassandra Config requested...";
        private const string BackgroundTaskStatus_Completed = "Cassandra Config request canceled.";
        private const string BackgroundTaskStatus_Canceled = "Cassandra Config request canceled.";

        private const string BackgroundTaskTotalDuration = "[*] Cassandra Config request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})";

        private const string ApplyButtonText_Cancel = "Cancel";

        private const string Event_CassandraConfigurationUpdateStatus_CompletedWithStatus = "Cassandra Configuration update completed. (Status --> {0})";
        private const string Event_CassandraConfigurationUpdateStatus_Completed = "Cassandra completed.";

        private const string Event_StatusUpdate_CompletedWithoutDetails = "Configuration update completed. Details unavailable.";

        private const string BackgroundTaskException = "Operation cancelled.";
        private const string BackgroundTaskExceptionUnobserved = "Operation cancelled. (Exception unobserved)";

        private const string RequestFailed = "Request failed.";

        #endregion

        #region Private Data Members

        private CustomLogger _logger;
        private SidekickConfiguration _sidekickConfiguration = null;
        private ICassandraConfigView _CassandraConfigView = null;
        private CassandraConfigurationManager _configurationManager = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.CassandraConfigView.SidekickName,
                                               this.CassandraConfigView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: CassandraConfig view.
        /// </summary>
        public ICassandraConfigView CassandraConfigView
        {
            get { return _CassandraConfigView; }
            set { _CassandraConfigView = value; }
        }

        /// <summary>
        /// Property: Azure configuration manager.
        /// </summary>
        public CassandraConfigurationManager ConfigurationManager
        {
            get { return _configurationManager; }
            set { _configurationManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="CassandraConfigView"></param>
        public CassandraConfigPresenter(SidekickConfiguration sidekickConfiguration,
                                       ICassandraConfigView CassandraConfigView)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.CassandraConfigView = CassandraConfigView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private CassandraConfigPresenter()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update Cassandra table with configuration file.
        /// </summary>
        /// <param name="cassandraNodes"></param>
        /// <param name="cassandraKeyspace"></param>
        /// <param name="cassandraNodeUsername"></param>
        /// <param name="cassandraNodePassword"></param>
        /// <param name="cassandraTable"></param>
        /// <param name="configurationPathFileName"></param>
        public void UpdateCassandraTableWithConfigurationFile(string cassandraNodes,
                                                              string cassandraKeyspace,
                                                              string cassandraNodeUsername,
                                                              string cassandraNodePassword,
                                                              CassandraTable cassandraTable,
                                                              string configurationPathFileName)
        {
            const string CassandraNodeUsernamePrefix = "UserName=";
            const string CassandraNodePasswordPrefix = "Password=";

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.CassandraConfigView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.CassandraConfigView.ApplyButtonEnable(false);

                    return;
                }
                // Validate: Cassandra connection info access key.
                if (string.IsNullOrEmpty(cassandraNodes))
                {
                    throw new ArgumentNullException($"Cassandra connection info access key is required.");
                }

                // Validate: Cassandra connection info keyspace name.
                if (string.IsNullOrEmpty(cassandraKeyspace))
                {
                    throw new ArgumentNullException($"Cassandra connection info keyspace name is required.");
                }

                // Validate: Cassandra table.
                if (cassandraTable == CassandraTable.Unspecified)
                {
                    throw new ArgumentNullException($"Cassandra table is required.");
                }

                // Validate: Configuration csv path file name.
                if (string.IsNullOrEmpty(configurationPathFileName))
                {
                    throw new ArgumentNullException($"Configuration csv path file name is required.");
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.CassandraConfigView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;
                    this.CassandraConfigView.ApplyButtonText = ApplyButtonText_Cancel;

                    this.ConfigurationManager = this.CreateConfigurationManager(cassandraNodes,
                                                                                cassandraKeyspace,
                                                                                cassandraNodeUsername,
                                                                                cassandraNodePassword);

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.ConfigurationManager.ConfigurationUpdateStatus += OnConfigurationUpdateStatusUpdated;
                    this.ConfigurationManager.ConfigurationUpdated += OnConfigurationUpdated;

                    cassandraNodes = string.Format("{0};{1};{2}",
                                                   cassandraNodes,
                                                   cassandraNodeUsername,
                                                   cassandraNodePassword);

                    this.BackgroundTask = new Task(() => this.ConfigurationManager.UpdateCassandraTableConfiguration(cassandraTable.ToString(),
                                                                                                                     cassandraNodes,
                                                                                                                     cassandraKeyspace,
                                                                                                                     cassandraTable,
                                                                                                                     configurationPathFileName,
                                                                                                                     cancellationToken),
                                                                                                                     cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalDuration;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.CassandraConfigView.BackgroundTaskCompleted(BackgroundTaskStatus_Completed);
                        this.CassandraConfigView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format(BackgroundTaskStatus_Canceled));

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.CassandraConfigView.BackgroundTaskCompleted(string.Empty);
                        this.CassandraConfigView.ApplyButtonEnable(true);

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(BackgroundTaskException));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(BackgroundTaskException));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(RequestFailed));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(RequestFailed));
                                    }

                                }
                            }
                        }

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.CassandraConfigView.BackgroundTaskCompleted(Event_CassandraConfigurationUpdateStatus_Completed);
                        this.CassandraConfigView.ApplyButtonEnable(true);

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create configuration manager.
        /// </summary>
        /// <param name="cassandraNodes"></param>
        /// <param name="cassandraKeyspace"></param>
        /// <param name="cassandraNodeUsername"></param>
        /// <param name="cassandraNodePassword"></param>
        /// <returns></returns>
        protected CassandraConfigurationManager CreateConfigurationManager(string cassandraNodes,
                                                                           string cassandraKeyspace,
                                                                           string cassandraNodeUsername,
                                                                           string cassandraNodePassword)
        {
            CassandraConfigurationManager result;

            try
            {
                result = new CassandraConfigurationManager(cassandraNodes, cassandraNodeUsername, cassandraNodePassword, cassandraKeyspace);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: Configuration update - status updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="configurationUpdateStatusEventArgs"></param>
        protected void OnConfigurationUpdateStatusUpdated(Object sender,
                                                          ConfigurationUpdateStatusEventArgs configurationUpdateStatusEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Log event completed with status.
                if (configurationUpdateStatusEventArgs.ConfigurationUpdateStatus.StatusList != null &&
                    configurationUpdateStatusEventArgs.ConfigurationUpdateStatus.StatusList.Count > 0)
                {
                    message = string.Format(Event_CassandraConfigurationUpdateStatus_CompletedWithStatus,
                                            configurationUpdateStatusEventArgs.ConfigurationUpdateStatus.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (configurationUpdateStatusEventArgs.ConfigurationUpdateStatus.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                } //Log event completed with status message.
                else if (string.IsNullOrEmpty(configurationUpdateStatusEventArgs.ConfigurationUpdateStatus.StatusMessage) == false)
                {
                    this.Logger.Info(configurationUpdateStatusEventArgs.ConfigurationUpdateStatus.StatusMessage);
                } //Log event completed without status.
                else
                {
                    message = string.Format(Event_StatusUpdate_CompletedWithoutDetails);
                    this.Logger.Info(message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Event Hanlder: AAAEventAAA completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="configurationUpdateResultEventArgs"></param>
        protected void OnConfigurationUpdated(Object sender,
                                              ConfigurationUpdateResultEventArgs configurationUpdateResultEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Log event completed with status.
                if (configurationUpdateResultEventArgs.ConfigurationUpdateResult.StatusList != null &&
                    configurationUpdateResultEventArgs.ConfigurationUpdateResult.StatusList.Count > 0)
                {
                    message = string.Format(Event_CassandraConfigurationUpdateStatus_CompletedWithStatus,
                                            configurationUpdateResultEventArgs.ConfigurationUpdateResult.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (configurationUpdateResultEventArgs.ConfigurationUpdateResult.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                } //Log event completed with status message.
                else if (string.IsNullOrEmpty(configurationUpdateResultEventArgs.ConfigurationUpdateResult.ResultMessage) == false)
                {
                    this.Logger.Info(configurationUpdateResultEventArgs.ConfigurationUpdateResult.ResultMessage);
                } //Log event completed without status.
                else
                {
                    message = string.Format(Event_StatusUpdate_CompletedWithoutDetails);
                    this.Logger.Info(message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                        }
                        else
                        {
                            Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                }
                else
                {
                    Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
