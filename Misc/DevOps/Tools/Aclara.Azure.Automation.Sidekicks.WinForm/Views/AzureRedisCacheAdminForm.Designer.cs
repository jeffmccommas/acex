﻿namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    partial class AzureRedisCacheAdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GuidanceGroupBox = new System.Windows.Forms.GroupBox();
            this.GuidanceLabel = new System.Windows.Forms.Label();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.OperationComboBox = new System.Windows.Forms.ComboBox();
            this.OperationLabel = new System.Windows.Forms.Label();
            this.AzureRedisCacheNameComboBox = new System.Windows.Forms.ComboBox();
            this.AzureSubscriptionNameComboBox = new System.Windows.Forms.ComboBox();
            this.AzureSubscriptionNameLabel = new System.Windows.Forms.Label();
            this.EnvironmentLabel = new System.Windows.Forms.Label();
            this.EnvironmentComboBox = new System.Windows.Forms.ComboBox();
            this.AzureRedisCacheNameLabel = new System.Windows.Forms.Label();
            this.HelpButton = new System.Windows.Forms.Button();
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.GuidanceGroupBox.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.PropertiesPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // GuidanceGroupBox
            // 
            this.GuidanceGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GuidanceGroupBox.Controls.Add(this.HelpButton);
            this.GuidanceGroupBox.Controls.Add(this.GuidanceLabel);
            this.GuidanceGroupBox.Location = new System.Drawing.Point(6, 28);
            this.GuidanceGroupBox.Name = "GuidanceGroupBox";
            this.GuidanceGroupBox.Size = new System.Drawing.Size(702, 68);
            this.GuidanceGroupBox.TabIndex = 0;
            this.GuidanceGroupBox.TabStop = false;
            this.GuidanceGroupBox.Text = "Guidance";
            // 
            // GuidanceLabel
            // 
            this.GuidanceLabel.AutoSize = true;
            this.GuidanceLabel.Location = new System.Drawing.Point(6, 19);
            this.GuidanceLabel.Name = "GuidanceLabel";
            this.GuidanceLabel.Size = new System.Drawing.Size(414, 13);
            this.GuidanceLabel.TabIndex = 0;
            this.GuidanceLabel.Text = "Select Azure subscription, environment, Azure Redis cache, operation and click Ap" +
    "ply.";
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(716, 22);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(0, 1);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(719, 17);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "Azure Redis Cache Administration";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ControlPanel
            // 
            this.ControlPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Controls.Add(this.ApplyButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 466);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(716, 40);
            this.ControlPanel.TabIndex = 11;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(635, 11);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 1;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PropertiesPanel.Controls.Add(this.OperationComboBox);
            this.PropertiesPanel.Controls.Add(this.OperationLabel);
            this.PropertiesPanel.Controls.Add(this.AzureRedisCacheNameComboBox);
            this.PropertiesPanel.Controls.Add(this.AzureSubscriptionNameComboBox);
            this.PropertiesPanel.Controls.Add(this.AzureSubscriptionNameLabel);
            this.PropertiesPanel.Controls.Add(this.EnvironmentLabel);
            this.PropertiesPanel.Controls.Add(this.EnvironmentComboBox);
            this.PropertiesPanel.Controls.Add(this.AzureRedisCacheNameLabel);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Controls.Add(this.GuidanceGroupBox);
            this.PropertiesPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesPanel.MinimumSize = new System.Drawing.Size(560, 480);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(716, 506);
            this.PropertiesPanel.TabIndex = 3;
            // 
            // OperationComboBox
            // 
            this.OperationComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OperationComboBox.FormattingEnabled = true;
            this.OperationComboBox.Location = new System.Drawing.Point(147, 201);
            this.OperationComboBox.Name = "OperationComboBox";
            this.OperationComboBox.Size = new System.Drawing.Size(261, 21);
            this.OperationComboBox.TabIndex = 8;
            // 
            // OperationLabel
            // 
            this.OperationLabel.AutoSize = true;
            this.OperationLabel.Location = new System.Drawing.Point(12, 204);
            this.OperationLabel.Name = "OperationLabel";
            this.OperationLabel.Size = new System.Drawing.Size(56, 13);
            this.OperationLabel.TabIndex = 7;
            this.OperationLabel.Text = "Operation:";
            // 
            // AzureRedisCacheNameComboBox
            // 
            this.AzureRedisCacheNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AzureRedisCacheNameComboBox.FormattingEnabled = true;
            this.AzureRedisCacheNameComboBox.Location = new System.Drawing.Point(147, 168);
            this.AzureRedisCacheNameComboBox.Name = "AzureRedisCacheNameComboBox";
            this.AzureRedisCacheNameComboBox.Size = new System.Drawing.Size(173, 21);
            this.AzureRedisCacheNameComboBox.TabIndex = 6;
            // 
            // AzureSubscriptionNameComboBox
            // 
            this.AzureSubscriptionNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AzureSubscriptionNameComboBox.FormattingEnabled = true;
            this.AzureSubscriptionNameComboBox.Location = new System.Drawing.Point(147, 102);
            this.AzureSubscriptionNameComboBox.Name = "AzureSubscriptionNameComboBox";
            this.AzureSubscriptionNameComboBox.Size = new System.Drawing.Size(261, 21);
            this.AzureSubscriptionNameComboBox.TabIndex = 2;
            this.AzureSubscriptionNameComboBox.SelectedIndexChanged += new System.EventHandler(this.AzureSubscriptionNameComboBox_SelectedIndexChanged);
            // 
            // AzureSubscriptionNameLabel
            // 
            this.AzureSubscriptionNameLabel.AutoSize = true;
            this.AzureSubscriptionNameLabel.Location = new System.Drawing.Point(12, 105);
            this.AzureSubscriptionNameLabel.Name = "AzureSubscriptionNameLabel";
            this.AzureSubscriptionNameLabel.Size = new System.Drawing.Size(125, 13);
            this.AzureSubscriptionNameLabel.TabIndex = 1;
            this.AzureSubscriptionNameLabel.Text = "Azure subscription name:";
            // 
            // EnvironmentLabel
            // 
            this.EnvironmentLabel.AutoSize = true;
            this.EnvironmentLabel.Location = new System.Drawing.Point(12, 138);
            this.EnvironmentLabel.Name = "EnvironmentLabel";
            this.EnvironmentLabel.Size = new System.Drawing.Size(98, 13);
            this.EnvironmentLabel.TabIndex = 3;
            this.EnvironmentLabel.Text = "Environment name:";
            // 
            // EnvironmentComboBox
            // 
            this.EnvironmentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.EnvironmentComboBox.FormattingEnabled = true;
            this.EnvironmentComboBox.Location = new System.Drawing.Point(147, 135);
            this.EnvironmentComboBox.Name = "EnvironmentComboBox";
            this.EnvironmentComboBox.Size = new System.Drawing.Size(173, 21);
            this.EnvironmentComboBox.TabIndex = 4;
            this.EnvironmentComboBox.SelectedIndexChanged += new System.EventHandler(this.EnvironmentComboBox_SelectedIndexChanged);
            // 
            // AzureRedisCacheNameLabel
            // 
            this.AzureRedisCacheNameLabel.AutoSize = true;
            this.AzureRedisCacheNameLabel.Location = new System.Drawing.Point(12, 171);
            this.AzureRedisCacheNameLabel.Name = "AzureRedisCacheNameLabel";
            this.AzureRedisCacheNameLabel.Size = new System.Drawing.Size(129, 13);
            this.AzureRedisCacheNameLabel.TabIndex = 5;
            this.AzureRedisCacheNameLabel.Text = "Azure Redis cache name:";
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(668, 11);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 1;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.Azure.Automation.Sidekicks.Help.chm";
            // 
            // AzureRedisCacheAdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 506);
            this.Controls.Add(this.PropertiesPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "AzureRedisCacheAdminForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "AzureRedisCacheAdminForm";
            this.GuidanceGroupBox.ResumeLayout(false);
            this.GuidanceGroupBox.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.PropertiesPanel.ResumeLayout(false);
            this.PropertiesPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GuidanceGroupBox;
        private System.Windows.Forms.Label GuidanceLabel;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Label AzureRedisCacheNameLabel;
        private System.Windows.Forms.ComboBox AzureSubscriptionNameComboBox;
        private System.Windows.Forms.Label AzureSubscriptionNameLabel;
        private System.Windows.Forms.Label EnvironmentLabel;
        private System.Windows.Forms.ComboBox EnvironmentComboBox;
        private System.Windows.Forms.ComboBox AzureRedisCacheNameComboBox;
        private System.Windows.Forms.ComboBox OperationComboBox;
        private System.Windows.Forms.Label OperationLabel;
        private System.Windows.Forms.Button HelpButton;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
    }
}