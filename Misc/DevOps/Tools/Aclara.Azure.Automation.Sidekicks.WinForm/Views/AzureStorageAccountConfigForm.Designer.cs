﻿namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    partial class AzureStorageAccountConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PropertiesHeaderLabel = new System.Windows.Forms.Label();
            this.PropertiesPanel = new System.Windows.Forms.Panel();
            this.SelectActionLabel = new System.Windows.Forms.Label();
            this.SelectActionComboBox = new System.Windows.Forms.ComboBox();
            this.ConnectionInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.ManuallyEnteredConnectionInfoGuidanceLabel = new System.Windows.Forms.Label();
            this.AccessKeyTextBox = new System.Windows.Forms.TextBox();
            this.AccessKeyLabel = new System.Windows.Forms.Label();
            this.EnvironmentLabel = new System.Windows.Forms.Label();
            this.EnvironmentComboBox = new System.Windows.Forms.ComboBox();
            this.ManualEntryConnectionInfoRadioButton = new System.Windows.Forms.RadioButton();
            this.PreconfiguredConnectionInfoRadioButton = new System.Windows.Forms.RadioButton();
            this.AzureSubscriptionNameComboBox = new System.Windows.Forms.ComboBox();
            this.GuidanceGroupBox = new System.Windows.Forms.GroupBox();
            this.HelpButton = new System.Windows.Forms.Button();
            this.AzureSubscriptionNameLabel = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.BackgroundTaskStatusLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.PropertiesHeaderPanel = new System.Windows.Forms.Panel();
            this.AzureBlobContainerPanel = new System.Windows.Forms.Panel();
            this.AzureBlobContainerGroupBox = new System.Windows.Forms.GroupBox();
            this.BlobContainerNameTextBox = new System.Windows.Forms.TextBox();
            this.BlobContainerNameLabel = new System.Windows.Forms.Label();
            this.AzureTablePanel = new System.Windows.Forms.Panel();
            this.AzureTableConfigurationGroupBox = new System.Windows.Forms.GroupBox();
            this.OpenConfigurationFileButton = new System.Windows.Forms.Button();
            this.ConfigurationFilePathNameTextBox = new System.Windows.Forms.TextBox();
            this.ConfigurationFileLabel = new System.Windows.Forms.Label();
            this.AzureTableNameComboBox = new System.Windows.Forms.ComboBox();
            this.AzureTableNameLabel = new System.Windows.Forms.Label();
            this.SidekickHelpProvider = new System.Windows.Forms.HelpProvider();
            this.PropertiesPanel.SuspendLayout();
            this.ConnectionInfoGroupBox.SuspendLayout();
            this.GuidanceGroupBox.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.PropertiesHeaderPanel.SuspendLayout();
            this.AzureBlobContainerPanel.SuspendLayout();
            this.AzureBlobContainerGroupBox.SuspendLayout();
            this.AzureTablePanel.SuspendLayout();
            this.AzureTableConfigurationGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // PropertiesHeaderLabel
            // 
            this.PropertiesHeaderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PropertiesHeaderLabel.Location = new System.Drawing.Point(0, 1);
            this.PropertiesHeaderLabel.Name = "PropertiesHeaderLabel";
            this.PropertiesHeaderLabel.Size = new System.Drawing.Size(719, 17);
            this.PropertiesHeaderLabel.TabIndex = 0;
            this.PropertiesHeaderLabel.Text = "Azure Storage Account Configuration";
            this.PropertiesHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PropertiesPanel.Controls.Add(this.SelectActionLabel);
            this.PropertiesPanel.Controls.Add(this.SelectActionComboBox);
            this.PropertiesPanel.Controls.Add(this.ConnectionInfoGroupBox);
            this.PropertiesPanel.Controls.Add(this.AzureSubscriptionNameComboBox);
            this.PropertiesPanel.Controls.Add(this.GuidanceGroupBox);
            this.PropertiesPanel.Controls.Add(this.AzureSubscriptionNameLabel);
            this.PropertiesPanel.Controls.Add(this.ControlPanel);
            this.PropertiesPanel.Controls.Add(this.PropertiesHeaderPanel);
            this.PropertiesPanel.Controls.Add(this.AzureBlobContainerPanel);
            this.PropertiesPanel.Controls.Add(this.AzureTablePanel);
            this.PropertiesPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesPanel.MinimumSize = new System.Drawing.Size(560, 480);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.Size = new System.Drawing.Size(716, 506);
            this.PropertiesPanel.TabIndex = 0;
            // 
            // SelectActionLabel
            // 
            this.SelectActionLabel.AutoSize = true;
            this.SelectActionLabel.Location = new System.Drawing.Point(12, 233);
            this.SelectActionLabel.Name = "SelectActionLabel";
            this.SelectActionLabel.Size = new System.Drawing.Size(40, 13);
            this.SelectActionLabel.TabIndex = 4;
            this.SelectActionLabel.Text = "Action:";
            // 
            // SelectActionComboBox
            // 
            this.SelectActionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SelectActionComboBox.FormattingEnabled = true;
            this.SelectActionComboBox.Location = new System.Drawing.Point(117, 230);
            this.SelectActionComboBox.Name = "SelectActionComboBox";
            this.SelectActionComboBox.Size = new System.Drawing.Size(207, 21);
            this.SelectActionComboBox.TabIndex = 5;
            this.SelectActionComboBox.SelectedIndexChanged += new System.EventHandler(this.SelectActionComboBox_SelectedIndexChanged);
            // 
            // ConnectionInfoGroupBox
            // 
            this.ConnectionInfoGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ConnectionInfoGroupBox.Controls.Add(this.ManuallyEnteredConnectionInfoGuidanceLabel);
            this.ConnectionInfoGroupBox.Controls.Add(this.AccessKeyTextBox);
            this.ConnectionInfoGroupBox.Controls.Add(this.AccessKeyLabel);
            this.ConnectionInfoGroupBox.Controls.Add(this.EnvironmentLabel);
            this.ConnectionInfoGroupBox.Controls.Add(this.EnvironmentComboBox);
            this.ConnectionInfoGroupBox.Controls.Add(this.ManualEntryConnectionInfoRadioButton);
            this.ConnectionInfoGroupBox.Controls.Add(this.PreconfiguredConnectionInfoRadioButton);
            this.ConnectionInfoGroupBox.Location = new System.Drawing.Point(3, 120);
            this.ConnectionInfoGroupBox.Name = "ConnectionInfoGroupBox";
            this.ConnectionInfoGroupBox.Size = new System.Drawing.Size(710, 100);
            this.ConnectionInfoGroupBox.TabIndex = 3;
            this.ConnectionInfoGroupBox.TabStop = false;
            this.ConnectionInfoGroupBox.Text = "Azure Storage Account Connection Information";
            // 
            // ManuallyEnteredConnectionInfoGuidanceLabel
            // 
            this.ManuallyEnteredConnectionInfoGuidanceLabel.AutoSize = true;
            this.ManuallyEnteredConnectionInfoGuidanceLabel.Location = new System.Drawing.Point(28, 74);
            this.ManuallyEnteredConnectionInfoGuidanceLabel.Name = "ManuallyEnteredConnectionInfoGuidanceLabel";
            this.ManuallyEnteredConnectionInfoGuidanceLabel.Size = new System.Drawing.Size(481, 13);
            this.ManuallyEnteredConnectionInfoGuidanceLabel.TabIndex = 5;
            this.ManuallyEnteredConnectionInfoGuidanceLabel.Text = "Production environment is not preconfigured. Contact DevOps for connection inform" +
    "ation (if needed).";
            // 
            // AccessKeyTextBox
            // 
            this.AccessKeyTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AccessKeyTextBox.Location = new System.Drawing.Point(231, 47);
            this.AccessKeyTextBox.Name = "AccessKeyTextBox";
            this.AccessKeyTextBox.Size = new System.Drawing.Size(471, 20);
            this.AccessKeyTextBox.TabIndex = 4;
            // 
            // AccessKeyLabel
            // 
            this.AccessKeyLabel.AutoSize = true;
            this.AccessKeyLabel.Location = new System.Drawing.Point(127, 50);
            this.AccessKeyLabel.Name = "AccessKeyLabel";
            this.AccessKeyLabel.Size = new System.Drawing.Size(65, 13);
            this.AccessKeyLabel.TabIndex = 3;
            this.AccessKeyLabel.Text = "Access key:";
            // 
            // EnvironmentLabel
            // 
            this.EnvironmentLabel.AutoSize = true;
            this.EnvironmentLabel.Location = new System.Drawing.Point(127, 22);
            this.EnvironmentLabel.Name = "EnvironmentLabel";
            this.EnvironmentLabel.Size = new System.Drawing.Size(98, 13);
            this.EnvironmentLabel.TabIndex = 0;
            this.EnvironmentLabel.Text = "Environment name:";
            // 
            // EnvironmentComboBox
            // 
            this.EnvironmentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.EnvironmentComboBox.FormattingEnabled = true;
            this.EnvironmentComboBox.Location = new System.Drawing.Point(231, 19);
            this.EnvironmentComboBox.Name = "EnvironmentComboBox";
            this.EnvironmentComboBox.Size = new System.Drawing.Size(90, 21);
            this.EnvironmentComboBox.TabIndex = 1;
            // 
            // ManualEntryConnectionInfoRadioButton
            // 
            this.ManualEntryConnectionInfoRadioButton.AutoSize = true;
            this.ManualEntryConnectionInfoRadioButton.Location = new System.Drawing.Point(9, 48);
            this.ManualEntryConnectionInfoRadioButton.Name = "ManualEntryConnectionInfoRadioButton";
            this.ManualEntryConnectionInfoRadioButton.Size = new System.Drawing.Size(86, 17);
            this.ManualEntryConnectionInfoRadioButton.TabIndex = 2;
            this.ManualEntryConnectionInfoRadioButton.TabStop = true;
            this.ManualEntryConnectionInfoRadioButton.Text = "Manual entry";
            this.ManualEntryConnectionInfoRadioButton.UseVisualStyleBackColor = true;
            this.ManualEntryConnectionInfoRadioButton.CheckedChanged += new System.EventHandler(this.ManualEntryConnectionInfoRadioButton_CheckedChanged);
            // 
            // PreconfiguredConnectionInfoRadioButton
            // 
            this.PreconfiguredConnectionInfoRadioButton.AutoSize = true;
            this.PreconfiguredConnectionInfoRadioButton.Location = new System.Drawing.Point(10, 20);
            this.PreconfiguredConnectionInfoRadioButton.Name = "PreconfiguredConnectionInfoRadioButton";
            this.PreconfiguredConnectionInfoRadioButton.Size = new System.Drawing.Size(91, 17);
            this.PreconfiguredConnectionInfoRadioButton.TabIndex = 0;
            this.PreconfiguredConnectionInfoRadioButton.TabStop = true;
            this.PreconfiguredConnectionInfoRadioButton.Text = "Preconfigured";
            this.PreconfiguredConnectionInfoRadioButton.UseVisualStyleBackColor = true;
            this.PreconfiguredConnectionInfoRadioButton.CheckedChanged += new System.EventHandler(this.PreconfiguredConnectionInfoRadioButton_CheckedChanged);
            // 
            // AzureSubscriptionNameComboBox
            // 
            this.AzureSubscriptionNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AzureSubscriptionNameComboBox.FormattingEnabled = true;
            this.AzureSubscriptionNameComboBox.Location = new System.Drawing.Point(134, 83);
            this.AzureSubscriptionNameComboBox.Name = "AzureSubscriptionNameComboBox";
            this.AzureSubscriptionNameComboBox.Size = new System.Drawing.Size(261, 21);
            this.AzureSubscriptionNameComboBox.TabIndex = 1;
            this.AzureSubscriptionNameComboBox.SelectedIndexChanged += new System.EventHandler(this.AzureSubscriptionNameComboBox_SelectedIndexChanged);
            // 
            // GuidanceGroupBox
            // 
            this.GuidanceGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GuidanceGroupBox.Controls.Add(this.HelpButton);
            this.GuidanceGroupBox.Location = new System.Drawing.Point(3, 28);
            this.GuidanceGroupBox.Name = "GuidanceGroupBox";
            this.GuidanceGroupBox.Size = new System.Drawing.Size(710, 49);
            this.GuidanceGroupBox.TabIndex = 0;
            this.GuidanceGroupBox.TabStop = false;
            this.GuidanceGroupBox.Text = "Guidance";
            // 
            // HelpButton
            // 
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.FlatAppearance.BorderSize = 0;
            this.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelpButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionDisplayHelp;
            this.HelpButton.Location = new System.Drawing.Point(668, 11);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(28, 23);
            this.HelpButton.TabIndex = 2;
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // AzureSubscriptionNameLabel
            // 
            this.AzureSubscriptionNameLabel.AutoSize = true;
            this.AzureSubscriptionNameLabel.Location = new System.Drawing.Point(3, 86);
            this.AzureSubscriptionNameLabel.Name = "AzureSubscriptionNameLabel";
            this.AzureSubscriptionNameLabel.Size = new System.Drawing.Size(125, 13);
            this.AzureSubscriptionNameLabel.TabIndex = 1;
            this.AzureSubscriptionNameLabel.Text = "Azure subscription name:";
            // 
            // ControlPanel
            // 
            this.ControlPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ControlPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ControlPanel.Controls.Add(this.BackgroundTaskStatusLabel);
            this.ControlPanel.Controls.Add(this.ApplyButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ControlPanel.Location = new System.Drawing.Point(0, 466);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(716, 40);
            this.ControlPanel.TabIndex = 6;
            // 
            // BackgroundTaskStatusLabel
            // 
            this.BackgroundTaskStatusLabel.AutoSize = true;
            this.BackgroundTaskStatusLabel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundTaskStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundTaskStatusLabel.ForeColor = System.Drawing.Color.Gold;
            this.BackgroundTaskStatusLabel.Location = new System.Drawing.Point(6, 16);
            this.BackgroundTaskStatusLabel.Name = "BackgroundTaskStatusLabel";
            this.BackgroundTaskStatusLabel.Size = new System.Drawing.Size(0, 13);
            this.BackgroundTaskStatusLabel.TabIndex = 0;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(635, 11);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 1;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // PropertiesHeaderPanel
            // 
            this.PropertiesHeaderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertiesHeaderPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PropertiesHeaderPanel.Controls.Add(this.PropertiesHeaderLabel);
            this.PropertiesHeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.PropertiesHeaderPanel.MinimumSize = new System.Drawing.Size(275, 22);
            this.PropertiesHeaderPanel.Name = "PropertiesHeaderPanel";
            this.PropertiesHeaderPanel.Size = new System.Drawing.Size(716, 22);
            this.PropertiesHeaderPanel.TabIndex = 0;
            // 
            // AzureBlobContainerPanel
            // 
            this.AzureBlobContainerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AzureBlobContainerPanel.Controls.Add(this.AzureBlobContainerGroupBox);
            this.AzureBlobContainerPanel.Location = new System.Drawing.Point(0, 261);
            this.AzureBlobContainerPanel.Name = "AzureBlobContainerPanel";
            this.AzureBlobContainerPanel.Size = new System.Drawing.Size(716, 199);
            this.AzureBlobContainerPanel.TabIndex = 6;
            // 
            // AzureBlobContainerGroupBox
            // 
            this.AzureBlobContainerGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AzureBlobContainerGroupBox.Controls.Add(this.BlobContainerNameTextBox);
            this.AzureBlobContainerGroupBox.Controls.Add(this.BlobContainerNameLabel);
            this.AzureBlobContainerGroupBox.Location = new System.Drawing.Point(6, 3);
            this.AzureBlobContainerGroupBox.Name = "AzureBlobContainerGroupBox";
            this.AzureBlobContainerGroupBox.Size = new System.Drawing.Size(707, 80);
            this.AzureBlobContainerGroupBox.TabIndex = 2;
            this.AzureBlobContainerGroupBox.TabStop = false;
            this.AzureBlobContainerGroupBox.Text = "Azure Blob Container";
            // 
            // BlobContainerNameTextBox
            // 
            this.BlobContainerNameTextBox.Location = new System.Drawing.Point(129, 26);
            this.BlobContainerNameTextBox.Name = "BlobContainerNameTextBox";
            this.BlobContainerNameTextBox.Size = new System.Drawing.Size(100, 20);
            this.BlobContainerNameTextBox.TabIndex = 3;
            // 
            // BlobContainerNameLabel
            // 
            this.BlobContainerNameLabel.AutoSize = true;
            this.BlobContainerNameLabel.Location = new System.Drawing.Point(6, 29);
            this.BlobContainerNameLabel.Name = "BlobContainerNameLabel";
            this.BlobContainerNameLabel.Size = new System.Drawing.Size(107, 13);
            this.BlobContainerNameLabel.TabIndex = 2;
            this.BlobContainerNameLabel.Text = "Blob container name:";
            // 
            // AzureTablePanel
            // 
            this.AzureTablePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AzureTablePanel.Controls.Add(this.AzureTableConfigurationGroupBox);
            this.AzureTablePanel.Location = new System.Drawing.Point(0, 261);
            this.AzureTablePanel.Name = "AzureTablePanel";
            this.AzureTablePanel.Size = new System.Drawing.Size(716, 199);
            this.AzureTablePanel.TabIndex = 8;
            // 
            // AzureTableConfigurationGroupBox
            // 
            this.AzureTableConfigurationGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AzureTableConfigurationGroupBox.Controls.Add(this.OpenConfigurationFileButton);
            this.AzureTableConfigurationGroupBox.Controls.Add(this.ConfigurationFilePathNameTextBox);
            this.AzureTableConfigurationGroupBox.Controls.Add(this.ConfigurationFileLabel);
            this.AzureTableConfigurationGroupBox.Controls.Add(this.AzureTableNameComboBox);
            this.AzureTableConfigurationGroupBox.Controls.Add(this.AzureTableNameLabel);
            this.AzureTableConfigurationGroupBox.Location = new System.Drawing.Point(6, 3);
            this.AzureTableConfigurationGroupBox.Name = "AzureTableConfigurationGroupBox";
            this.AzureTableConfigurationGroupBox.Size = new System.Drawing.Size(707, 99);
            this.AzureTableConfigurationGroupBox.TabIndex = 0;
            this.AzureTableConfigurationGroupBox.TabStop = false;
            this.AzureTableConfigurationGroupBox.Text = "Azure Table Configuration";
            // 
            // OpenConfigurationFileButton
            // 
            this.OpenConfigurationFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OpenConfigurationFileButton.Location = new System.Drawing.Point(676, 54);
            this.OpenConfigurationFileButton.Name = "OpenConfigurationFileButton";
            this.OpenConfigurationFileButton.Size = new System.Drawing.Size(24, 23);
            this.OpenConfigurationFileButton.TabIndex = 4;
            this.OpenConfigurationFileButton.Text = "...";
            this.OpenConfigurationFileButton.UseVisualStyleBackColor = true;
            this.OpenConfigurationFileButton.Click += new System.EventHandler(this.OpenConfigurationFileButton_Click);
            // 
            // ConfigurationFilePathNameTextBox
            // 
            this.ConfigurationFilePathNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ConfigurationFilePathNameTextBox.Location = new System.Drawing.Point(129, 56);
            this.ConfigurationFilePathNameTextBox.Name = "ConfigurationFilePathNameTextBox";
            this.ConfigurationFilePathNameTextBox.Size = new System.Drawing.Size(541, 20);
            this.ConfigurationFilePathNameTextBox.TabIndex = 3;
            // 
            // ConfigurationFileLabel
            // 
            this.ConfigurationFileLabel.AutoSize = true;
            this.ConfigurationFileLabel.Location = new System.Drawing.Point(7, 59);
            this.ConfigurationFileLabel.Name = "ConfigurationFileLabel";
            this.ConfigurationFileLabel.Size = new System.Drawing.Size(114, 13);
            this.ConfigurationFileLabel.TabIndex = 2;
            this.ConfigurationFileLabel.Text = "Configuration file (csv):";
            // 
            // AzureTableNameComboBox
            // 
            this.AzureTableNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AzureTableNameComboBox.FormattingEnabled = true;
            this.AzureTableNameComboBox.Location = new System.Drawing.Point(128, 22);
            this.AzureTableNameComboBox.Name = "AzureTableNameComboBox";
            this.AzureTableNameComboBox.Size = new System.Drawing.Size(156, 21);
            this.AzureTableNameComboBox.TabIndex = 1;
            // 
            // AzureTableNameLabel
            // 
            this.AzureTableNameLabel.AutoSize = true;
            this.AzureTableNameLabel.Location = new System.Drawing.Point(7, 25);
            this.AzureTableNameLabel.Name = "AzureTableNameLabel";
            this.AzureTableNameLabel.Size = new System.Drawing.Size(92, 13);
            this.AzureTableNameLabel.TabIndex = 0;
            this.AzureTableNameLabel.Text = "Azure table name:";
            // 
            // SidekickHelpProvider
            // 
            this.SidekickHelpProvider.HelpNamespace = "Aclara.Azure.Automation.Sidekicks.Help.chm";
            // 
            // AzureStorageAccountConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 506);
            this.Controls.Add(this.PropertiesPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "AzureStorageAccountConfigForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "AzureStorageAccountConfigForm";
            this.PropertiesPanel.ResumeLayout(false);
            this.PropertiesPanel.PerformLayout();
            this.ConnectionInfoGroupBox.ResumeLayout(false);
            this.ConnectionInfoGroupBox.PerformLayout();
            this.GuidanceGroupBox.ResumeLayout(false);
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.PropertiesHeaderPanel.ResumeLayout(false);
            this.AzureBlobContainerPanel.ResumeLayout(false);
            this.AzureBlobContainerGroupBox.ResumeLayout(false);
            this.AzureBlobContainerGroupBox.PerformLayout();
            this.AzureTablePanel.ResumeLayout(false);
            this.AzureTableConfigurationGroupBox.ResumeLayout(false);
            this.AzureTableConfigurationGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label PropertiesHeaderLabel;
        private System.Windows.Forms.Panel PropertiesPanel;
        private System.Windows.Forms.Panel PropertiesHeaderPanel;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Label BackgroundTaskStatusLabel;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.GroupBox ConnectionInfoGroupBox;
        private System.Windows.Forms.Label ManuallyEnteredConnectionInfoGuidanceLabel;
        private System.Windows.Forms.TextBox AccessKeyTextBox;
        private System.Windows.Forms.Label AccessKeyLabel;
        private System.Windows.Forms.Label EnvironmentLabel;
        private System.Windows.Forms.ComboBox EnvironmentComboBox;
        private System.Windows.Forms.RadioButton ManualEntryConnectionInfoRadioButton;
        private System.Windows.Forms.RadioButton PreconfiguredConnectionInfoRadioButton;
        private System.Windows.Forms.ComboBox AzureSubscriptionNameComboBox;
        private System.Windows.Forms.GroupBox GuidanceGroupBox;
        private System.Windows.Forms.Label AzureSubscriptionNameLabel;
        private System.Windows.Forms.Panel AzureBlobContainerPanel;
        private System.Windows.Forms.Label SelectActionLabel;
        private System.Windows.Forms.ComboBox SelectActionComboBox;
        private System.Windows.Forms.Panel AzureTablePanel;
        private System.Windows.Forms.GroupBox AzureBlobContainerGroupBox;
        private System.Windows.Forms.TextBox BlobContainerNameTextBox;
        private System.Windows.Forms.Label BlobContainerNameLabel;
        private System.Windows.Forms.GroupBox AzureTableConfigurationGroupBox;
        private System.Windows.Forms.Button OpenConfigurationFileButton;
        private System.Windows.Forms.TextBox ConfigurationFilePathNameTextBox;
        private System.Windows.Forms.Label ConfigurationFileLabel;
        private System.Windows.Forms.ComboBox AzureTableNameComboBox;
        private System.Windows.Forms.Label AzureTableNameLabel;
        private System.Windows.Forms.HelpProvider SidekickHelpProvider;
        private System.Windows.Forms.Button HelpButton;
    }
}