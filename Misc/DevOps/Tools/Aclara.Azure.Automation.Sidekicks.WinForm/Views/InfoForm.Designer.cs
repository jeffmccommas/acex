﻿namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    partial class InfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InfoForm));
            this.SidekicksAutoUpdaterInfoPanel = new System.Windows.Forms.Panel();
            this.SidekicksAutoUpdaterInfoLabel = new System.Windows.Forms.Label();
            this.SidekicksAutoUpdaterWarningPictureBox = new System.Windows.Forms.PictureBox();
            this.SidekicksAutoUpdaterStatusLabel = new System.Windows.Forms.Label();
            this.SidekicksAutoUpdaterOpenFolderButton = new System.Windows.Forms.Button();
            this.SidekicksAutoUpdaterCopyToClipboardButton = new System.Windows.Forms.Button();
            this.SidekicksAutoUpdaterLocationLabel = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.InfoToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SidekicksAutoUpdaterInfoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SidekicksAutoUpdaterWarningPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // SidekicksAutoUpdaterInfoPanel
            // 
            this.SidekicksAutoUpdaterInfoPanel.Controls.Add(this.SidekicksAutoUpdaterInfoLabel);
            this.SidekicksAutoUpdaterInfoPanel.Controls.Add(this.SidekicksAutoUpdaterWarningPictureBox);
            this.SidekicksAutoUpdaterInfoPanel.Controls.Add(this.SidekicksAutoUpdaterStatusLabel);
            this.SidekicksAutoUpdaterInfoPanel.Controls.Add(this.SidekicksAutoUpdaterOpenFolderButton);
            this.SidekicksAutoUpdaterInfoPanel.Controls.Add(this.SidekicksAutoUpdaterCopyToClipboardButton);
            this.SidekicksAutoUpdaterInfoPanel.Controls.Add(this.SidekicksAutoUpdaterLocationLabel);
            this.SidekicksAutoUpdaterInfoPanel.Location = new System.Drawing.Point(0, -1);
            this.SidekicksAutoUpdaterInfoPanel.Name = "SidekicksAutoUpdaterInfoPanel";
            this.SidekicksAutoUpdaterInfoPanel.Size = new System.Drawing.Size(908, 334);
            this.SidekicksAutoUpdaterInfoPanel.TabIndex = 0;
            // 
            // SidekicksAutoUpdaterInfoLabel
            // 
            this.SidekicksAutoUpdaterInfoLabel.AutoSize = true;
            this.SidekicksAutoUpdaterInfoLabel.Location = new System.Drawing.Point(98, 38);
            this.SidekicksAutoUpdaterInfoLabel.Name = "SidekicksAutoUpdaterInfoLabel";
            this.SidekicksAutoUpdaterInfoLabel.Size = new System.Drawing.Size(36, 13);
            this.SidekicksAutoUpdaterInfoLabel.TabIndex = 6;
            this.SidekicksAutoUpdaterInfoLabel.Text = "<info>";
            // 
            // SidekicksAutoUpdaterWarningPictureBox
            // 
            this.SidekicksAutoUpdaterWarningPictureBox.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.Severity_Icon_Warning;
            this.SidekicksAutoUpdaterWarningPictureBox.InitialImage = null;
            this.SidekicksAutoUpdaterWarningPictureBox.Location = new System.Drawing.Point(101, 63);
            this.SidekicksAutoUpdaterWarningPictureBox.Name = "SidekicksAutoUpdaterWarningPictureBox";
            this.SidekicksAutoUpdaterWarningPictureBox.Size = new System.Drawing.Size(16, 16);
            this.SidekicksAutoUpdaterWarningPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.SidekicksAutoUpdaterWarningPictureBox.TabIndex = 5;
            this.SidekicksAutoUpdaterWarningPictureBox.TabStop = false;
            // 
            // SidekicksAutoUpdaterStatusLabel
            // 
            this.SidekicksAutoUpdaterStatusLabel.AutoSize = true;
            this.SidekicksAutoUpdaterStatusLabel.ForeColor = System.Drawing.Color.Red;
            this.SidekicksAutoUpdaterStatusLabel.Location = new System.Drawing.Point(131, 63);
            this.SidekicksAutoUpdaterStatusLabel.Name = "SidekicksAutoUpdaterStatusLabel";
            this.SidekicksAutoUpdaterStatusLabel.Size = new System.Drawing.Size(47, 13);
            this.SidekicksAutoUpdaterStatusLabel.TabIndex = 4;
            this.SidekicksAutoUpdaterStatusLabel.Text = "<status>";
            // 
            // SidekicksAutoUpdaterOpenFolderButton
            // 
            this.SidekicksAutoUpdaterOpenFolderButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionOpenFolder;
            this.SidekicksAutoUpdaterOpenFolderButton.Location = new System.Drawing.Point(55, 13);
            this.SidekicksAutoUpdaterOpenFolderButton.Name = "SidekicksAutoUpdaterOpenFolderButton";
            this.SidekicksAutoUpdaterOpenFolderButton.Size = new System.Drawing.Size(37, 37);
            this.SidekicksAutoUpdaterOpenFolderButton.TabIndex = 1;
            this.InfoToolTip.SetToolTip(this.SidekicksAutoUpdaterOpenFolderButton, "Open folder");
            this.SidekicksAutoUpdaterOpenFolderButton.UseVisualStyleBackColor = true;
            this.SidekicksAutoUpdaterOpenFolderButton.Click += new System.EventHandler(this.SidekicksAutoUpdaterOpenFolderButton_Click);
            // 
            // SidekicksAutoUpdaterCopyToClipboardButton
            // 
            this.SidekicksAutoUpdaterCopyToClipboardButton.Image = global::Aclara.Azure.Automation.Sidekicks.WinForm.Properties.Resources.ActionCopyToClipboard;
            this.SidekicksAutoUpdaterCopyToClipboardButton.Location = new System.Drawing.Point(12, 13);
            this.SidekicksAutoUpdaterCopyToClipboardButton.Name = "SidekicksAutoUpdaterCopyToClipboardButton";
            this.SidekicksAutoUpdaterCopyToClipboardButton.Size = new System.Drawing.Size(37, 37);
            this.SidekicksAutoUpdaterCopyToClipboardButton.TabIndex = 0;
            this.SidekicksAutoUpdaterCopyToClipboardButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.InfoToolTip.SetToolTip(this.SidekicksAutoUpdaterCopyToClipboardButton, "Copy to clipboard");
            this.SidekicksAutoUpdaterCopyToClipboardButton.UseVisualStyleBackColor = true;
            this.SidekicksAutoUpdaterCopyToClipboardButton.Click += new System.EventHandler(this.SidekicksAutoUpdaterCopyToClipboardButton_Click);
            // 
            // SidekicksAutoUpdaterLocationLabel
            // 
            this.SidekicksAutoUpdaterLocationLabel.AutoSize = true;
            this.SidekicksAutoUpdaterLocationLabel.Location = new System.Drawing.Point(98, 13);
            this.SidekicksAutoUpdaterLocationLabel.Name = "SidekicksAutoUpdaterLocationLabel";
            this.SidekicksAutoUpdaterLocationLabel.Size = new System.Drawing.Size(56, 13);
            this.SidekicksAutoUpdaterLocationLabel.TabIndex = 3;
            this.SidekicksAutoUpdaterLocationLabel.Text = "<location>";
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(819, 339);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 1;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // InfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 374);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.SidekicksAutoUpdaterInfoPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InfoForm";
            this.Text = "InfoForm";
            this.Shown += new System.EventHandler(this.InfoForm_Shown);
            this.SidekicksAutoUpdaterInfoPanel.ResumeLayout(false);
            this.SidekicksAutoUpdaterInfoPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SidekicksAutoUpdaterWarningPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel SidekicksAutoUpdaterInfoPanel;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button SidekicksAutoUpdaterCopyToClipboardButton;
        private System.Windows.Forms.Label SidekicksAutoUpdaterLocationLabel;
        private System.Windows.Forms.Button SidekicksAutoUpdaterOpenFolderButton;
        private System.Windows.Forms.ToolTip InfoToolTip;
        private System.Windows.Forms.Label SidekicksAutoUpdaterStatusLabel;
        private System.Windows.Forms.PictureBox SidekicksAutoUpdaterWarningPictureBox;
        private System.Windows.Forms.Label SidekicksAutoUpdaterInfoLabel;
    }
}