﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public class AzureRedisCacheAdminFormFactory
    {
        /// <summary>
        /// Create form.
        /// </summary>
        /// <returns></returns>
        public static AzureRedisCacheAdminForm CreateForm(SidekickConfiguration sidekickConfiguration)
        {

            AzureRedisCacheAdminForm result = null;
            AzureRedisCacheAdminPresenter AzureRedisCacheAdminPresenter = null;

            try
            {
                result = new AzureRedisCacheAdminForm(sidekickConfiguration);
                AzureRedisCacheAdminPresenter = new AzureRedisCacheAdminPresenter(sidekickConfiguration, result);
                result.AzureRedisCacheAdminPresenter = AzureRedisCacheAdminPresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
