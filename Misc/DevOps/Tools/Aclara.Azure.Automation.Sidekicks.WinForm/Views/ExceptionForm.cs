﻿using System;
using System.Windows.Forms;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public partial class ExceptionForm : Form, IExceptionView
    {

        #region Private Constants

        private const int FormHeight_Collapsed = 200;
        private const int FormHeight_Expanded = 386;

        private const string MoreButtonText_MoreInformation = "More information";
        private const string MoreButtonText_LessInformation = "Less information";

        #endregion

        #region Private Data Members

        private ExceptionPresenter _exceptionPresenter;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Exception presenter.
        /// </summary>
        public ExceptionPresenter ExceptionPresenter
        {
            get { return _exceptionPresenter; }
            set { _exceptionPresenter = value; }
        }

        /// <summary>
        /// Property: Exception message.
        /// </summary>
        public string ExceptionMessage
        {
            get
            {
                return this.MessageTextbox.Text;
            }
            set
            {
                this.MessageTextbox.Text = value;
            }
        }

        /// <summary>
        /// Property: Excetion text.
        /// </summary>
        public string ExceptionText
        {
            get
            {
                return this.ExceptionTextbox.Text;
            }
            set
            {
                this.ExceptionTextbox.Text = value;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ExceptionForm()
        {
            InitializeComponent();
            this.Height = FormHeight_Collapsed;

            this.ExceptionLabel.Visible = false;
            this.ExceptionTextbox.Visible = false;

            this.MoreButton.Text = MoreButtonText_MoreInformation;
            this.MessageTextbox.SelectionLength = 0;
        }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Copy button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyButton_Click(object sender, EventArgs e)
        {
            string clipboardText = string.Empty;

            clipboardText = this.MessageTextbox.Text;
            clipboardText += Environment.NewLine;
            clipboardText += Environment.NewLine;
            clipboardText += this.ExceptionTextbox.Text;

            Clipboard.SetDataObject(clipboardText);

        }

        /// <summary>
        /// Event Handler: Close button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Event Handler: Exception textbox key pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExceptionTextbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 1)
            {
                ((TextBox)sender).SelectAll();
                e.Handled = true;
            }

        }


        /// <summary>
        /// Event Handler: More button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoreButton_Click(object sender, EventArgs e)
        {

            if (this.Height == FormHeight_Expanded)
            {
                //Collapse form.
                this.Height = FormHeight_Collapsed;

                this.ExceptionLabel.Visible = false;
                this.ExceptionTextbox.Visible = false;

                this.MoreButton.Text = MoreButtonText_MoreInformation;

            }
            else
            {
                //Expand form.
                this.Height = FormHeight_Expanded;

                this.ExceptionLabel.Visible = true;
                this.ExceptionTextbox.Visible = true;

                this.MoreButton.Text = MoreButtonText_LessInformation;
            }
        }

        #endregion

    }
}
