﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public partial class GuidanceForm : Form
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Caption.
        /// </summary>
        public string Caption
        {
            get { return this.Text; }
            set { this.Text = value; }
        }

        /// <summary>
        /// Property: Guidance text.
        /// </summary>
        public string GuidanceText
        {
            get { return this.GuidanceTextBox.Text; }
            set { this.GuidanceTextBox.Text = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="caption"></param>
        /// <param name="guidanceText"></param>
        public GuidanceForm(string caption, string guidanceText)
        {
            InitializeComponent();
            this.Caption = caption;
            this.GuidanceText = guidanceText;
        }

        #endregion

        #region Protected Constructors
        protected GuidanceForm()
        {
            InitializeComponent();
        }
        #endregion

        #region Public Methods
        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
        }
        
        #endregion

        #region Private Methods
        #endregion
    }
}
