﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public class AOAlertsDiagFormFactory
    {
        /// <summary>
        /// Create form.
        /// </summary>
        /// <returns></returns>
        public static AOAlertsDiagForm CreateForm(SidekickConfiguration sidekickConfiguration)
        {

            AOAlertsDiagForm result = null;
            AOAlertsDiagPresenter AOAlertsDiagPresenter = null;

            try
            {
                result = new AOAlertsDiagForm();
                AOAlertsDiagPresenter = new AOAlertsDiagPresenter(sidekickConfiguration, result);
                result.AOAlertsDiagPresenter = AOAlertsDiagPresenter;
                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
