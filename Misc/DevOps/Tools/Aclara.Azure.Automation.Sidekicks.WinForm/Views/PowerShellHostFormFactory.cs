﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public class PowerShellHostFormFactory
    {
        /// <summary>
        /// Create form.
        /// </summary>
        /// <returns></returns>
        public static PowerShellHostForm CreateForm(SidekickConfiguration sidekickConfiguration, SidekickComponentManager sidekickComponentManager)
        {

            PowerShellHostForm result = null;
            SidekickComponentManagerForm sidekickComponentManagerForm = null;
            SidekickComponentAssistantForm sidekickComponentAssistantForm = null;
            PowerShellHostPresenter PowerShellHostPresenter = null;

            try
            {
                result = new PowerShellHostForm(sidekickConfiguration);

                PowerShellHostPresenter = new PowerShellHostPresenter(sidekickConfiguration, result);
                sidekickComponentManagerForm = SidekickComponentManagerFormFactory.CreateForm(sidekickConfiguration, sidekickComponentManager);
                sidekickComponentAssistantForm = SidekickComponentAssistantFormFactory.CreateForm(sidekickConfiguration, sidekickComponentManager);

                result.PowerShellHostPresenter = PowerShellHostPresenter;
                result.SidekickComponentManagerForm = sidekickComponentManagerForm;
                result.SidekickComponentAssistantForm = sidekickComponentAssistantForm;

                result.TopLevel = false;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
