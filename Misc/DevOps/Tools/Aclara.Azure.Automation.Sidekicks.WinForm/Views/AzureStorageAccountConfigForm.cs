﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static CE.AO.ConfigurationManagement.Types.Enumerations;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public partial class AzureStorageAccountConfigForm : Form, ISidekickView, IAzureStorageAccountConfigView, ISidekickInfo
    {

        #region Private Constants

        private const string SidekickInfo_SidekickName = "AzureStorageAccountConfig";
        private const string SidekickInfo_SidekickDescription = "Azure Storage Account Configuration";

        private const string SidekickConfiguration_AzureStorageAccountConnectionInfo_SubscriptionName = "SubscriptionName";
        private const string SidekickConfiguration_AzureStorageAccountConnectionInfo_EnvironmentName = "Name";

        private const string ActionType_AzureBlobContainer = "Create Azure Blob Container";
        private const string ActionType_AzureTable = "Update Azure Table";

        private const string AzureTableName_SelectAzureTableName = "<Select Azure Table Name>";
        private const string AzureTableName_BillCycleSchedule = "Bill Cycle Schedule";
        private const string AzureTableName_ClientDomain = "Client Domain";
        private const string AzureTableName_ClientWidgetConfiguration = "Client Widget Configuration";
        private const string AzureTableName_SmsTemplate = "SMS Template";
        private const string AzureTableName_TrumpiaKeywordConfiguration = "Trumpia Keyward Configuration";

        private const string ValidationErrorMessage_AzureStorageAccountIsRequired = "Environment is required when selecting preconfigured connection information.";
        private const string ValidationErrorMessage_AzureStorageAccountAccessKeyIsRequired = "Azure storage account access key is required when entering connection information manually.";
        private const string ValidationErrorMessage_AzureTableIsRequired = "Azure table name is required.";
        private const string ValidationErrorMessage_ConfigurationFileCouldNotBeFound = "Configuration file could not be found. (File name: {0})";
        private const string ValidationErrorMessage_BlobContainerIsNameRequired = "Blob container name is required.";

        #endregion

        #region Private Data Members

        private AzureStorageAccountConfigPresenter _AzureStorageAccountConfigPresenter;
        private SidekickConfiguration _sidekickConfiguration = null;

        #endregion

        #region Public Types

        public enum ActionTypes
        {
            AzureBlobContainer = 1,
            AzureTable = 2
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: AzureStorageAccountConfig presenter.
        /// </summary>
        public AzureStorageAccountConfigPresenter AzureStorageAccountConfigPresenter
        {
            get { return _AzureStorageAccountConfigPresenter; }
            set { _AzureStorageAccountConfigPresenter = value; }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property:  Apply button text.
        /// </summary>
        public string ApplyButtonText
        {
            get { return this.ApplyButton.Text; }
            set { this.ApplyButton.Text = value; }
        }

        /// <summary>
        /// Property: Background task status.
        /// </summary>
        public string BackgroundTaskStatus
        {
            get { return this.BackgroundTaskStatusLabel.Text; }
            set { this.BackgroundTaskStatusLabel.Text = value; }
        }

        /// <summary>
        /// Property: Is sidekick busy. 
        /// </summary>
        public bool IsSidekickBusy
        {
            get
            {
                bool result = false;

                if (this.ApplyButton.Text == "Apply")
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
        }

        /// <summary>
        /// Property: Sidekick name.
        /// </summary>
        public string SidekickName
        {
            get
            {
                return SidekickInfo_SidekickName;
            }
        }

        /// <summary>
        /// Property: Sidekick description.
        /// </summary>
        public string SidekickDescription
        {
            get
            {
                return SidekickInfo_SidekickDescription;
            }
        }

        /// <summary>
        /// Property: Environment.
        /// </summary>
        public AzureStorageAccount_Environment Environment
        {
            get { return (AzureStorageAccount_Environment)this.EnvironmentComboBox.SelectedItem; }
            set { this.EnvironmentComboBox.SelectedItem = value; }
        }

        /// <summary>
        /// Property: Access key.
        /// </summary>
        public string AccessKey
        {
            get
            {
                return this.AccessKeyTextBox.Text;
            }

        }

        /// <summary>
        /// Property: Configuration type.
        /// </summary>
        public ActionTypes ConfigurationType
        {

            get
            {
                return (ActionTypes)this.SelectActionComboBox.SelectedItem;
            }
        }

        /// <summary>
        /// Property: Azure table.
        /// </summary>
        public AzureTable AzureTable
        {
            get { return (AzureTable)this.AzureTableNameComboBox.SelectedItem; }
            set { this.AzureTableNameComboBox.SelectedItem = value; }
        }

        /// <summary>
        /// Property: Configuration file path name.
        /// </summary>
        public string ConfigurationFilePathName
        {
            get { return this.ConfigurationFilePathNameTextBox.Text; }
            set { this.ConfigurationFilePathNameTextBox.Text = value; }
        }

        /// <summary>
        /// Property: Blob container name.
        /// </summary>
        public string BlobContainerName
        {
            get
            {
                return this.BlobContainerNameTextBox.Text;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="buildDefinitionSearchForm"></param>
        /// <param></param>
        public AzureStorageAccountConfigForm(SidekickConfiguration sidekickConfiguration)
        {
            _sidekickConfiguration = sidekickConfiguration;

            InitializeComponent();

            this.InitializeControls();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Sidekick was activated.
        /// </summary>
        public void SidekickActivated()
        {
            try
            {
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Apply button enable. 
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void ApplyButtonEnable(bool enable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<bool>(this.ApplyButtonEnable), enable);
            }
            else
            {
                this.ApplyButton.Enabled = enable;
            }
        }

        /// <summary>
        /// Background task cancelled.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCancelled(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCancelled), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        /// <param name="backgroundTaskStatus"></param>
        public void BackgroundTaskCompleted(string backgroundTaskStatus)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new Action<string>(this.BackgroundTaskCompleted), backgroundTaskStatus);
            }
            else
            {
                this.BackgroundTaskStatus = backgroundTaskStatus;
                this.ApplyButton.Text = "Apply";
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize controls.
        /// </summary>
        protected void InitializeControls()
        {
            BindingSource azureStorageAccountBindingSource = null;

            try
            {

                this.AzureSubscriptionNameComboBox.FormattingEnabled = true;
                azureStorageAccountBindingSource = new BindingSource();
                azureStorageAccountBindingSource.DataSource = this.SidekickConfiguration.AzureStorageAccount.AzureSubscription.ToList();

                this.AzureSubscriptionNameComboBox.DataSource = azureStorageAccountBindingSource;
                this.AzureSubscriptionNameComboBox.DisplayMember = SidekickConfiguration_AzureStorageAccountConnectionInfo_SubscriptionName;
                this.AzureSubscriptionNameComboBox.ValueMember = SidekickConfiguration_AzureStorageAccountConnectionInfo_SubscriptionName;

                this.PreconfiguredConnectionInfoRadioButton.Checked = true;
                this.ManualEntryConnectionInfoRadioButton.Checked = false;

                this.SelectActionComboBox.FormattingEnabled = true;
                this.SelectActionComboBox.Format += delegate (object sender, ListControlConvertEventArgs e)
                {
                    e.Value = ConvertActionTypeToText((ActionTypes)e.Value);
                };
                this.SelectActionComboBox.DataSource = Enum.GetValues(typeof(ActionTypes));
                this.SelectActionComboBox.SelectedItem = ActionTypes.AzureTable;

                // Initialize Azure table combobox.
                this.AzureTableNameComboBox.FormattingEnabled = true;
                this.AzureTableNameComboBox.Format += delegate (object sender, ListControlConvertEventArgs e)
                {
                    e.Value = ConvertAzureTableToText((AzureTable)e.Value);
                };
                this.AzureTableNameComboBox.DataSource = Enum.GetValues(typeof(AzureTable));

                this.AzureTableNameComboBox.SelectedItem = AzureTable.Unspecified;

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        // Convert configuration type enum value to text.
        /// </summary>
        /// <returns></returns>
        protected string ConvertActionTypeToText(ActionTypes configurationType)
        {
            string result = string.Empty;

            try
            {
                switch (configurationType)
                {
                    case ActionTypes.AzureBlobContainer:
                        result = ActionType_AzureBlobContainer;
                        break;
                    case ActionTypes.AzureTable:
                        result = ActionType_AzureTable;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(string.Format("Unexpected configuration type. (Coniguration type: {0})",
                                                                            configurationType));
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        // Convert Azure table enum value to text.
        /// </summary>
        /// <param name="dateFilter"></param>
        /// <returns></returns>
        protected string ConvertAzureTableToText(AzureTable azureTable)
        {
            string result = string.Empty;

            try
            {
                switch (azureTable)
                {
                    case AzureTable.Unspecified:
                        result = AzureTableName_SelectAzureTableName;
                        break;
                    case AzureTable.BillCycleSchedule:
                        result = AzureTableName_BillCycleSchedule;
                        break;
                    case AzureTable.ClientDomain:
                        result = AzureTableName_ClientDomain;
                        break;
                    case AzureTable.ClientWidgetConfiguration:
                        result = AzureTableName_ClientWidgetConfiguration;
                        break;
                    case AzureTable.SmsTemplate:
                        result = AzureTableName_SmsTemplate;
                        break;
                    case AzureTable.TrumpiaKeywordConfiguration:
                        result = AzureTableName_TrumpiaKeywordConfiguration;
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Toggle configuration information UI controls.
        /// </summary>
        protected void ToggleConfigurationInfoUIControls()
        {
            if (PreconfiguredConnectionInfoRadioButton.Checked == true)
            {
                EnvironmentComboBox.Enabled = true;
                AccessKeyTextBox.Enabled = false;
            }
            else if (ManualEntryConnectionInfoRadioButton.Checked == true)
            {
                EnvironmentComboBox.Enabled = false;
                AccessKeyTextBox.Enabled = true;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Event Handler: Apply button - click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyButton_Click(object sender, EventArgs e)
        {

            string accessKey = string.Empty;
            string blobContainerName = string.Empty;
            AzureTable azureTable = AzureTable.Unspecified;
            string configurationPathFileName = string.Empty;
            bool validateConfigurationCsvFile = false;

            try
            {

                // Validate: Preconfigured or manually entered connection information.
                if (this.PreconfiguredConnectionInfoRadioButton.Checked == true && this.Environment == null)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_AzureStorageAccountIsRequired));
                    return;
                }
                else if (this.ManualEntryConnectionInfoRadioButton.Checked == true && string.IsNullOrEmpty(this.AccessKey) == true)
                {
                    MessageBox.Show(string.Format(ValidationErrorMessage_AzureStorageAccountAccessKeyIsRequired));
                    return;
                }

                // Retrieve Azure storage account access key.
                if (this.PreconfiguredConnectionInfoRadioButton.Checked == true)
                {
                    accessKey = this.Environment.AccessKey;
                }
                else if (this.ManualEntryConnectionInfoRadioButton.Checked == true)
                {
                    accessKey = this.AccessKey;
                }

                if (this.ConfigurationType == ActionTypes.AzureTable)
                {
                    // Validate: Azure table.
                    if (this.AzureTable == AzureTable.Unspecified)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_AzureTableIsRequired));
                        return;
                    }

                    // Validate: Configuration file.
                    if (File.Exists(this.ConfigurationFilePathName) == false)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_ConfigurationFileCouldNotBeFound,
                                                      ConfigurationFilePathName));
                        return;
                    }

                    azureTable = this.AzureTable;
                    configurationPathFileName = this.ConfigurationFilePathName;
                    validateConfigurationCsvFile = true;

                    this.AzureStorageAccountConfigPresenter.UpdateAzureTableWithConfigurationFile(accessKey, azureTable, configurationPathFileName, validateConfigurationCsvFile);


                }
                else if (this.ConfigurationType == ActionTypes.AzureBlobContainer)
                {
                    //Validate: Blob container name.
                    if (string.IsNullOrEmpty(this.BlobContainerName) == true)
                    {
                        MessageBox.Show(string.Format(ValidationErrorMessage_BlobContainerIsNameRequired));
                        return;
                    }

                    blobContainerName = this.BlobContainerName;

                    this.AzureStorageAccountConfigPresenter.CreateAzureBlobContainer(accessKey, blobContainerName);
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Configuration type combo box - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectConfigTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            ActionTypes buildDefinitionSettings = ActionTypes.AzureTable;

            try
            {
                if (this.SelectActionComboBox.SelectedItem is ActionTypes)
                {
                    buildDefinitionSettings = (ActionTypes)this.SelectActionComboBox.SelectedItem;

                    switch (buildDefinitionSettings)
                    {
                        case ActionTypes.AzureBlobContainer:
                            this.AzureBlobContainerPanel.Visible = true;
                            this.AzureTablePanel.Visible = false;
                            break;
                        case ActionTypes.AzureTable:
                            this.AzureBlobContainerPanel.Visible = false;
                            this.AzureTablePanel.Visible = true;
                            break;
                        default:
                            this.AzureBlobContainerPanel.Visible = true;
                            this.AzureTablePanel.Visible = false;
                            break;
                    }

                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Preconfigured connection information Radio Button - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PreconfiguredConnectionInfoRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ToggleConfigurationInfoUIControls();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Manual entry connection information Radio Button - Checked changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ManualEntryConnectionInfoRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ToggleConfigurationInfoUIControls();
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }

        }

        /// <summary>
        /// Event Handler: Azure subscription name combo box - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AzureSubscriptionNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingSource environmentBindingSource = null;

            try
            {
                AzureStorageAccount_AzureSubscription azureSubscription = (AzureStorageAccount_AzureSubscription)this.AzureSubscriptionNameComboBox.SelectedItem;

                environmentBindingSource = new BindingSource();
                environmentBindingSource.DataSource = this.SidekickConfiguration.AzureStorageAccount.AzureSubscription.Where(asub => asub.SubscriptionName == azureSubscription.SubscriptionName).Single().Environment;

                this.EnvironmentComboBox.DataSource = environmentBindingSource;
                this.EnvironmentComboBox.FormattingEnabled = true;
                this.EnvironmentComboBox.DisplayMember = SidekickConfiguration_AzureStorageAccountConnectionInfo_EnvironmentName;
                this.EnvironmentComboBox.ValueMember = SidekickConfiguration_AzureStorageAccountConnectionInfo_EnvironmentName;
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        /// <summary>
        /// Event Handler: Select action combo box - Selected index changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectActionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            ActionTypes action = ActionTypes.AzureTable;

            try
            {
                if (this.SelectActionComboBox.SelectedItem is ActionTypes)
                {
                    action = (ActionTypes)this.SelectActionComboBox.SelectedItem;

                    switch (action)
                    {
                        case ActionTypes.AzureBlobContainer:
                            this.AzureBlobContainerPanel.Visible = true;
                            this.AzureTablePanel.Visible = false;
                            break;
                        case ActionTypes.AzureTable:
                            this.AzureBlobContainerPanel.Visible = false;
                            this.AzureTablePanel.Visible = true;
                            break;

                        default:
                            this.AzureBlobContainerPanel.Visible = true;
                            this.AzureTablePanel.Visible = false;
                            break;
                    }

                }
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }
        /// <summary>
        /// Event Handler: Open configuration file button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenConfigurationFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = null;
            DialogResult dialogResult = DialogResult.None;

            try
            {
                openFileDialog = new OpenFileDialog();

                openFileDialog.Filter = "Csv Files (.csv)|*.csv|All Files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.Multiselect = false;

                dialogResult = openFileDialog.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {
                    this.ConfigurationFilePathNameTextBox.Text = openFileDialog.FileName;
                }

            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }


        /// <summary>
        /// Event Handler: Help button - Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                Help.ShowHelp(this, this.SidekickHelpProvider.HelpNamespace, "SidekickAzureStorageAccountConfig.htm");
            }
            catch (Exception exception)
            {
                ExceptionForm exceptionForm = ExceptionFormFactory.CreateForm(exception);
                exceptionForm.ShowDialog();
            }
        }

        #endregion
    }
}