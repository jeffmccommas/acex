﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Tools.Common.StatusManagement;
using CE.AO.ConfigurationManagement;
using CE.AO.ConfigurationManagement.Events;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Aclara.Azure.Automation.Sidekicks.WinForm.Logging;
using static CE.AO.ConfigurationManagement.Types.Enumerations;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Views
{
    public class AzureStorageAccountConfigPresenter
    {

        #region Private Constants

        private const string BackgroundTaskStatus_CancellingRequest = "Cancelling AzureStorageAccountConfig request...";
        private const string BackgroundTaskStatus_Requested = "AzureStorageAccountConfig requested...";
        private const string BackgroundTaskStatus_Completed = "AzureStorageAccountConfig request canceled.";
        private const string BackgroundTaskStatus_Canceled = "AzureStorageAccountConfig request canceled.";

        private const string BackgroundTaskTotalDuration = "[*] Azure Storage Account Config request completed. (Total duration: {0:dd\\.hh\\:mm\\:ss})";

        private const string ApplyButtonText_Cancel = "Cancel";

        private const string Event_AzureStorageAccountUpdate_CompletedWithStatus = "Azure storage account configuration update completed. (Status --> {0})";
        private const string Event_AzureStorageAccountUpdate_Completed = "Azure storage account configuration update completed.";

        private const string Event_StatusUpdate_CompletedWithoutDetails = "Azure storage account configuration update completed. Details unavailable.";

        private const string BackgroundTaskException = "Operation cancelled.";
        private const string BackgroundTaskExceptionUnobserved = "Operation cancelled. (Exception unobserved)";

        private const string RequestFailed = "Request failed.";

        #endregion

        #region Private Data Members

        private CustomLogger _logger;
        private SidekickConfiguration _sidekickConfiguration = null;
        private IAzureStorageAccountConfigView _AzureStorageAccountConfigView = null;
        private ConfigurationManager _configurationManager = null;
        private System.Threading.Tasks.Task _backgroundTask;
        private System.Threading.CancellationTokenSource _backgroundTaskCancellationToken;
        private DateTime _backgroundTaskStartedDateTime;
        private DateTime _backgroundTaskCompletedDateTime;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               this.AzureStorageAccountConfigView.SidekickName,
                                               this.AzureStorageAccountConfigView.SidekickDescription);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: AzureStorageAccountConfig view.
        /// </summary>
        public IAzureStorageAccountConfigView AzureStorageAccountConfigView
        {
            get { return _AzureStorageAccountConfigView; }
            set { _AzureStorageAccountConfigView = value; }
        }

        /// <summary>
        /// Property: Azure configuration manager.
        /// </summary>
        public ConfigurationManager ConfigurationManager
        {
            get
            {
                if (_configurationManager == null)
                {
                    // if null, use the dev one by default
                    //var storageAccountConfigs = SidekickConfiguration.GetAzureStorageAccountConfigList();
                    //_configurationManager = this.CreateConfigurationManager(storageAccountConfigs[0].AccessKey);
                }
                return _configurationManager;
            }
            set { _configurationManager = value; }
        }

        /// <summary>
        /// Property: Background task.
        /// </summary>
        public System.Threading.Tasks.Task BackgroundTask
        {
            get { return _backgroundTask; }
            set { _backgroundTask = value; }
        }

        /// <summary>
        /// Property: Background task cancellation token.
        /// </summary>
        public System.Threading.CancellationTokenSource BackgroundTaskCancellationTokenSource
        {
            get { return _backgroundTaskCancellationToken; }
            set { _backgroundTaskCancellationToken = value; }
        }

        /// <summary>
        /// Property: Background task started date/time.
        /// </summary>
        public DateTime BackgroundTaskStartedDateTime
        {
            get { return _backgroundTaskStartedDateTime; }
            set { _backgroundTaskStartedDateTime = value; }
        }

        /// <summary>
        /// Property: Background task completed date/time.
        /// </summary>
        public DateTime BackgroundTaskCompletedDateTime
        {
            get { return _backgroundTaskCompletedDateTime; }
            set { _backgroundTaskCompletedDateTime = value; }
        }

        /// <summary>
        /// Property: Is background task busy.
        /// </summary>
        public bool IsBackgroundTaskBusy
        {
            get
            {
                bool result = false;

                if (this.BackgroundTask == null)
                {
                    return result;
                }

                switch (this.BackgroundTask.Status)
                {

                    case TaskStatus.Canceled:
                        result = false;
                        break;
                    case TaskStatus.Created:
                        result = false;
                        break;
                    case TaskStatus.Faulted:
                        result = false;
                        break;
                    case TaskStatus.RanToCompletion:
                        result = false;
                        break;
                    case TaskStatus.Running:
                        result = true;
                        break;
                    case TaskStatus.WaitingForActivation:
                        result = false;
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        result = true;
                        break;
                    case TaskStatus.WaitingToRun:
                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        /// <param name="AzureStorageAccountConfigView"></param>
        public AzureStorageAccountConfigPresenter(SidekickConfiguration sidekickConfiguration,
                                       IAzureStorageAccountConfigView AzureStorageAccountConfigView)
        {
            this.SidekickConfiguration = sidekickConfiguration;
            this.AzureStorageAccountConfigView = AzureStorageAccountConfigView;
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private AzureStorageAccountConfigPresenter()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create azure blob container.
        /// </summary>
        /// <param name="operationCount"></param>
        /// <param name="timePerOperation"></param>
        public void CreateAzureBlobContainer(string accessKey, string blobContainerName)
        {

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.AzureStorageAccountConfigView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.AzureStorageAccountConfigView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                   this.BackgroundTask.IsCanceled == true ||
                   this.BackgroundTask.IsCompleted == true ||
                   this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.AzureStorageAccountConfigView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;
                    this.AzureStorageAccountConfigView.ApplyButtonText = ApplyButtonText_Cancel;

                    this.ConfigurationManager = this.CreateConfigurationManager(accessKey);

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.ConfigurationManager.ConfigurationUpdateStatus += OnConfigurationUpdateStatusUpdated;
                    this.ConfigurationManager.ConfigurationUpdated += OnConfigurationUpdated;

                    this.BackgroundTask = new Task(() => this.ConfigurationManager.CreateAzureBlobContainer(accessKey,
                                                                                                            blobContainerName,
                                                                                                            cancellationToken),
                                                                                                       cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Update azure table with configuration file.
        /// </summary>
        /// <param name="operationCount"></param>
        /// <param name="timePerOperation"></param>
        public void UpdateAzureTableWithConfigurationFile(string accessKey,
                                                          AzureTable azureTable,
                                                          string configurationCsvPathFileName,
                                                          bool validateConfigurationCsvFile)
        {

            CancellationToken cancellationToken;

            try
            {

                if (this.BackgroundTask != null && this.BackgroundTask.Status == TaskStatus.Running)
                {

                    this.BackgroundTaskCancellationTokenSource.Cancel();
                    this.AzureStorageAccountConfigView.BackgroundTaskStatus = BackgroundTaskStatus_CancellingRequest;
                    this.AzureStorageAccountConfigView.ApplyButtonEnable(false);

                    return;
                }

                if (this.BackgroundTask == null ||
                    this.BackgroundTask.IsCanceled == true ||
                    this.BackgroundTask.IsCompleted == true ||
                    this.BackgroundTask.IsFaulted == true)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    this.AzureStorageAccountConfigView.BackgroundTaskStatus = BackgroundTaskStatus_Requested;
                    this.AzureStorageAccountConfigView.ApplyButtonText = ApplyButtonText_Cancel;

                    this.ConfigurationManager = this.CreateConfigurationManager(accessKey);

                    this.BackgroundTaskCancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = this.BackgroundTaskCancellationTokenSource.Token;
                    cancellationToken.Register(() => BackgroundTaskCompleted());

                    this.ConfigurationManager.ConfigurationUpdateStatus += OnConfigurationUpdateStatusUpdated;
                    this.ConfigurationManager.ConfigurationUpdated += OnConfigurationUpdated;

                    this.BackgroundTask = new Task(() => this.ConfigurationManager.UpdateAzureTableConfiguration(azureTable.ToString(),
                                                                                                                 accessKey,
                                                                                                                 azureTable,
                                                                                                                 configurationCsvPathFileName,
                                                                                                                 validateConfigurationCsvFile,
                                                                                                                 cancellationToken),
                                                                                                       cancellationToken);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), cancellationToken, TaskContinuationOptions.None, TaskScheduler.Current);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnCanceled);
                    this.BackgroundTask.ContinueWith(t => this.BackgroundTaskCompleted(), TaskContinuationOptions.OnlyOnFaulted);

                    TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                    this.BackgroundTask.Start();

                    this.BackgroundTaskStartedDateTime = DateTime.Now;

                    return;

                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Background task completed.
        /// </summary>
        public void BackgroundTaskCompleted()
        {
            TimeSpan totalDuration;
            Exception innerException = null;

            try
            {

                this.BackgroundTaskCompletedDateTime = DateTime.Now;
                totalDuration = this.BackgroundTaskCompletedDateTime - this.BackgroundTaskStartedDateTime;

                switch (this.BackgroundTask.Status)
                {
                    case TaskStatus.Canceled:
                        this.AzureStorageAccountConfigView.BackgroundTaskCompleted(BackgroundTaskStatus_Completed);
                        this.AzureStorageAccountConfigView.ApplyButtonEnable(true);

                        this.Logger.Info(string.Format(BackgroundTaskStatus_Canceled));

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));
                        break;

                    case TaskStatus.Created:
                        break;

                    case TaskStatus.Faulted:
                        this.AzureStorageAccountConfigView.BackgroundTaskCompleted(string.Empty);
                        this.AzureStorageAccountConfigView.ApplyButtonEnable(true);

                        if (this.BackgroundTask.IsFaulted == true)
                        {
                            if (this.BackgroundTask.Exception != null)
                            {
                                if (this.BackgroundTask.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                                {
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(BackgroundTaskException));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(BackgroundTaskException));
                                    }

                                }
                                else
                                {
                                    //Handle unexpected exception.
                                    if (this.BackgroundTask.Exception != null)
                                    {
                                        innerException = this.BackgroundTask.Exception.InnerException;
                                        Logger.Error(innerException, string.Format(RequestFailed));
                                    }
                                    else
                                    {
                                        Logger.Error(string.Format(RequestFailed));
                                    }

                                }
                            }
                        }

                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.RanToCompletion:
                        this.AzureStorageAccountConfigView.BackgroundTaskCompleted(Event_AzureStorageAccountUpdate_Completed);
                        this.AzureStorageAccountConfigView.ApplyButtonEnable(true);

                        //Log background task completed log entry.
                        this.Logger.Info(string.Format(BackgroundTaskTotalDuration,
                                                       totalDuration));

                        break;

                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Create configuration manager.
        /// </summary>
        /// <returns></returns>
        protected ConfigurationManager CreateConfigurationManager(string azureStorageAccountConnectionString)
        {
            ConfigurationManager result = null;

            try
            {
                result = new ConfigurationManager(azureStorageAccountConnectionString);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Event Hanlder: Configuration update - status updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="configurationUpdateStatusEventArgs"></param>
        protected void OnConfigurationUpdateStatusUpdated(Object sender,
                                                          ConfigurationUpdateStatusEventArgs configurationUpdateStatusEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Log event completed with status.
                if (configurationUpdateStatusEventArgs.ConfigurationUpdateStatus.StatusList != null &&
                    configurationUpdateStatusEventArgs.ConfigurationUpdateStatus.StatusList.Count > 0)
                {
                    message = string.Format(Event_AzureStorageAccountUpdate_CompletedWithStatus,
                                            configurationUpdateStatusEventArgs.ConfigurationUpdateStatus.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (configurationUpdateStatusEventArgs.ConfigurationUpdateStatus.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                } //Log event completed with status message.
                else if (string.IsNullOrEmpty(configurationUpdateStatusEventArgs.ConfigurationUpdateStatus.StatusMessage) == false)
                {
                    this.Logger.Info(configurationUpdateStatusEventArgs.ConfigurationUpdateStatus.StatusMessage);
                } //Log event completed without status.
                else
                {
                    message = string.Format(Event_StatusUpdate_CompletedWithoutDetails);
                    this.Logger.Info(message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Event Hanlder: AAAEventAAA completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="configurationUpdateResultEventArgs"></param>
        protected void OnConfigurationUpdated(Object sender,
                                              ConfigurationUpdateResultEventArgs configurationUpdateResultEventArgs)
        {
            string message = string.Empty;

            try
            {

                //Log event completed with status.
                if (configurationUpdateResultEventArgs.ConfigurationUpdateResult.StatusList != null &&
                    configurationUpdateResultEventArgs.ConfigurationUpdateResult.StatusList.Count > 0)
                {
                    message = string.Format(Event_AzureStorageAccountUpdate_CompletedWithStatus,
                                            configurationUpdateResultEventArgs.ConfigurationUpdateResult.StatusList.Format(StatusTypes.FormatOption.Minimum));

                    switch (configurationUpdateResultEventArgs.ConfigurationUpdateResult.StatusList.GetHighestStatusSeverity())
                    {
                        case StatusTypes.StatusSeverity.Unspecified:
                            this.Logger.Info(message);
                            break;
                        case StatusTypes.StatusSeverity.Error:
                            this.Logger.Error(message);
                            break;
                        case StatusTypes.StatusSeverity.Warning:
                            this.Logger.Warn(message);
                            break;
                        case StatusTypes.StatusSeverity.Informational:
                            this.Logger.Info(message);
                            break;
                        default:
                            this.Logger.Info(message);
                            break;
                    }
                } //Log event completed with status message.
                else if (string.IsNullOrEmpty(configurationUpdateResultEventArgs.ConfigurationUpdateResult.ResultMessage) == false)
                {
                    this.Logger.Info(configurationUpdateResultEventArgs.ConfigurationUpdateResult.ResultMessage);
                } //Log event completed without status.
                else
                {
                    message = string.Format(Event_StatusUpdate_CompletedWithoutDetails);
                    this.Logger.Info(message);
                }

            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Event Handler: Unobserved task exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="UnobservedTaskExceptionEventArgs"></param>
        protected void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
        {
            Exception innerException = null;

            try
            {

                if (eventArgs.Exception != null)
                {
                    if (eventArgs.Exception.InnerException.GetType() == typeof(OperationCanceledException))
                    {
                        //Handle operation canceled exception.
                        if (eventArgs.Exception != null)
                        {
                            innerException = eventArgs.Exception;
                            Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                        }
                        else
                        {
                            Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                        }

                        eventArgs.SetObserved();

                    }

                    return;
                }

                if (eventArgs.Exception != null)
                {
                    innerException = eventArgs.Exception;
                    Logger.Error(innerException, string.Format(BackgroundTaskExceptionUnobserved));

                }
                else
                {
                    Logger.Error(string.Format(BackgroundTaskExceptionUnobserved));
                }

                eventArgs.SetObserved();

            }
            catch (Exception)
            {
                throw;
            }

            return;
        }

        #endregion

        #region Private Methods

        #endregion

    }
}
