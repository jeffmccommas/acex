﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Configuration;
using Aclara.Azure.Automation.Sidekicks.WinForm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Utilities
{

    /// <summary>
    /// Sidekick component manager.
    /// </summary>
    public class SidekickComponentManager
    {
        #region Private Constants

        private const string SidekickComponentStatus_Available = "Available";
        private const string SidekickComponentStatus_NotFound = "Not found";
        private const string SidekickComponentStatus_MimimumVersionNotFound = "Minimum version not found.";

        private const string LoginStatus_LoginSuccessful = "Logged in";
        private const string LoginStatus_Failed = "Login failed or cancelled";

        private const string UserAction_Install = "Install";
        private const string UserAction_Upgrade = "Upgrade";
        private const string UserAction_None = "None";

        #endregion

        #region Private Data Members

        private SidekickConfiguration _sidekickConfiguration;
        private SidekickComponentList _sidekickComponentList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        /// <summary>
        /// Property: Sidekick component list.
        /// </summary>
        public SidekickComponentList SidekickComponentList
        {
            get { return _sidekickComponentList; }
            set { _sidekickComponentList = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="sidekickConfiguration"></param>
        public SidekickComponentManager(SidekickConfiguration sidekickConfiguration)
        {
            _sidekickConfiguration = sidekickConfiguration;
            _sidekickComponentList = new SidekickComponentList();
            InitializeSidekickComponentList();
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        private SidekickComponentManager()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Validate required components.
        /// </summary>
        /// <param name="SidekickComponentIdList"></param>
        public SidekickComponentList ValidateSidekickComponents(List<SidekickComponentId> SidekickComponentIdList)
        {

            SidekickComponentList result = null;
            SidekickComponent sidekickComponent = null;

            try
            {
                result = new SidekickComponentList();

                foreach (SidekickComponentId sidekickComponentId in SidekickComponentIdList)
                {

                    sidekickComponent = this.SidekickComponentList.Where(sk => sk.SidekickComponentId == sidekickComponentId).Single();

                    result.Add(sidekickComponent);

                    sidekickComponent.CheckStatus();

                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Login required components.
        /// </summary>
        /// <param name="SidekickComponentIdList"></param>
        public SidekickComponentList LoginSidekickComponents(List<SidekickComponentId> SidekickComponentIdList)
        {

            SidekickComponentList result = null;
            SidekickComponent sidekickComponent = null;

            try
            {
                result = new SidekickComponentList();

                foreach (SidekickComponentId sidekickComponentId in SidekickComponentIdList)
                {
                    sidekickComponent = this.SidekickComponentList.Where(sk => sk.SidekickComponentId == sidekickComponentId).Single();
                    result.Add(sidekickComponent);

                    switch (sidekickComponentId)
                    {
                        case SidekickComponentId.WindowsPowerShell:
                            break;
                        case SidekickComponentId.AzurePowerShell:

                            if (sidekickComponent.Available == true)
                            {
                                this.LoginToAzurePowerShell(sidekickComponent);
                            }
                            break;
                        default:
                            break;
                    }

                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve unavailable sidekick components.
        /// </summary>
        /// <param name="SidekickComponentIdList"></param>
        /// <returns></returns>
        public SidekickComponentList GetUnavailableSidekickComponents(List<SidekickComponentId> SidekickComponentIdList)
        {
            SidekickComponentList result = null;

            try
            {
                result = this.SidekickComponentList.GetUnavailableSidekickComponents(SidekickComponentIdList);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Retrieve sidekick components not logged in or login attempt unsuccessful.
        /// </summary>
        /// <param name="SidekickComponentIdList"></param>
        /// <returns></returns>
        public SidekickComponentList GetSidekickComponentsNotLoggedIn(List<SidekickComponentId> SidekickComponentIdList)
        {
            SidekickComponentList result = null;

            try
            {
                result = result = this.SidekickComponentList.GetSidekickComponentsNotLoggedIn(SidekickComponentIdList);

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Perform user action.
        /// </summary>
        /// <param name="sidekickComponent"></param>
        public void PerformUserAction(SidekickComponent sidekickComponent)
        {

            try
            {

                switch (sidekickComponent.UserAction)
                {
                    case UserAction.Install:

                        sidekickComponent.Install();

                        break;

                    case UserAction.Upgrade:

                        sidekickComponent.Update();

                        break;

                    default:
                        break;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize sidekick component list.
        /// </summary>
        protected void InitializeSidekickComponentList()
        {
            System.Version clientOSVersion;
            ComponentInfo_ClientOS clientOS;

            try
            {
                clientOSVersion = this.GetClientOSVersion();

                clientOS = this.SidekickConfiguration.ComponentInfo.ClientOSList.ClientOS.Where(cos => cos.VersionMajor == clientOSVersion.Major &&
                                                                                                       cos.VersionMinor == clientOSVersion.Minor).SingleOrDefault();
                if (clientOS == null)
                {
                    throw new NotSupportedException(string.Format("Client operating system not supported. (Major version: {0}, Minor version: {1})",
                                                                  clientOSVersion.Major,
                                                                  clientOSVersion.Minor));
                }

                this.SidekickComponentList = new SidekickComponentList(clientOS.ComponentList.Component);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Login to Azure PowerShell if required.
        /// </summary>
        protected void LoginToAzurePowerShell(SidekickComponent sidekickComponent)
        {
            AzureRmPowerShellManager azurePowerShellManager = null;

            try
            {
                azurePowerShellManager = new AzureRmPowerShellManager();
                azurePowerShellManager.LoginAzureAccount();

                sidekickComponent.LoginSuccessful = true;
                sidekickComponent.LoginStatus = LoginStatus_LoginSuccessful;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve client OS version.
        /// </summary>
        /// <returns></returns>
        protected System.Version GetClientOSVersion()
        {
            System.Version result;

            try
            {
                result = Environment.OSVersion.Version;
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        #endregion

        #region Private Methods
        #endregion
    }
}
