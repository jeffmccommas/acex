﻿using Aclara.Azure.Automation.Sidekicks.WinForm.Logging;
using Aclara.Azure.Automation.Sidekicks.WinForm.Models;
using Aclara.PowerShellHost;
using Aclara.Tools.Common.StatusManagement;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Utilities
{
    /// <summary>
    /// PowerShell manager.
    /// </summary>
    public class AzureRmPowerShellManager
    {

        #region Private Contants

        private const string PowerShellModule_AzurePowerShellFound = "Azure PowerShell installed.";
        private const string PowerShellModule_AuzrePowerShellNotFound = "Azure PowerShell not installed.";

        private const string PowerShellPackageProvider_NuGetFound= "NuGet installed.";
        private const string PowerShellPackageProvider_NuGetNotFound = "NuGet not installed.";

        private const string AzureLogin_LoginRequired = "Azure login required.";
        private const string AzureLogin_LoginError = "Run Login-AzureRmAccount to login.";

        private const string ExecutionPolicy_RemoteSigned = "RemoteSigned";
        private const string ExecutionPolicy_Unrestricted = "Unrestricted";
        private const string ExecutionPolicy_Bypass = "Bypass";

        private const string LogEntry_SetExecutionPolicyRemoteSigned = "Set Execution Policy to Remote Signed.";

        #endregion

        #region Private Data Members

        private CustomLogger _logger;
        private PowerShellHostManager _powerShellHostManager = null;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Logger.
        /// </summary>
        public CustomLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new CustomLogger(LogManager.GetCurrentClassLogger(),
                                               string.Empty,
                                               string.Empty);
                }
                return _logger;
            }
        }

        /// <summary>
        /// Property: PowerShellHost manager.
        /// </summary>
        public PowerShellHostManager PowerShellHostManager
        {
            get
            {
                if (_powerShellHostManager == null)
                {
                    _powerShellHostManager = this.CreatePowerShellHostManager();
                }
                return _powerShellHostManager;
            }
            set { _powerShellHostManager = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AzureRmPowerShellManager()
        {
            this.SetExecutionPolicy();
        }

        #endregion

        #region Public Methods
        
        /// <summary>
        /// Select Azure subscription.
        /// </summary>
        public void SelectAzureSubscription(string subscriptionName)
        {
            string powerShellScript = string.Empty;
            StatusList statusList = null;
            List<object> dataList = null;

            try
            {

                powerShellScript = @"Select-AzureRmSubscription";
                powerShellScript += string.Format(" -SubscriptionName \"{0}\"", subscriptionName);
                statusList = new StatusList();

                dataList = this.InvokePowerShellScriptSync(powerShellScript, ref statusList);

                foreach (Status status in statusList)
                {

                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Login to Azure account.
        /// </summary>
        public void LoginAzureAccount()
        {
            string powerShellScript = string.Empty;
            StatusList statusList = null;
            List<object> dataList = null;

            try
            {
                //powerShellScript = @"Add-AzureAccount";
                powerShellScript = @"Login-AzureRmAccount";

                statusList = new StatusList();

                dataList = this.InvokePowerShellScriptSync(powerShellScript, ref statusList);

                foreach (Status status in statusList)
                {
                    if (status.Exception != null)
                    {
                        throw status.Exception;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Invoke PowerShell script synchronously.
        /// </summary>
        /// <param name="powerShellScript"></param>
        /// <returns></returns>
        protected List<object> InvokePowerShellScriptSync(string powerShellScript, ref StatusList statusList)
        {

            List<object> result = null;

            try
            {
                result = this.PowerShellHostManager.InvokePowerShellScriptSync(powerShellScript, ref statusList);

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Create PowerShellHost manager.
        /// </summary>
        /// <returns></returns>
        protected PowerShellHostManager CreatePowerShellHostManager()
        {
            PowerShellHostManager result = null;

            try
            {
                result = PowerShellHostManagerFactory.CreatePowerShellHostManager();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Set execution policy to remote signed.
        /// </summary>
        protected bool SetExecutionPolicy()
        {
            bool result = false;
            string powerShellScript = string.Empty;
            StatusList statusList = null;
            List<object> dataList = null;

            try
            {
                powerShellScript = @"Set-ExecutionPolicy RemoteSigned -Force -Scope process";

                powerShellScript = string.Format(powerShellScript);

                statusList = new StatusList();

                dataList = this.InvokePowerShellScriptSync(powerShellScript, ref statusList);
                foreach (object data in dataList)
                {
                }

                foreach (Status status in statusList)
                {
                    if (status.Exception != null || status.StatusServerity == StatusTypes.StatusSeverity.Error)
                    {
                        throw status.Exception;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        #endregion

    }
}
