Aclara Azure Automation Sidekicks - Change Log

--- V0.60.0.0 --- (17.12) ------------------------------------------------------

2017-10-20 Mark Jones

	* Added kill switch.
	  - We can not update a json file and force a specific version of the application to shutdown and give
	    the user the option of upgrading or shutdown the app.

--- V0.59.0.0 --- (17.12) ------------------------------------------------------

2017-10-19 Mark Jones

	* Rebuilt from new branch 17.12.

--- V0.58.0.0 --- (17.06) ------------------------------------------------------

2017-08-20 Mark Jones

	* Refactored Sidekick Components to support Windows 10.

--- V0.57.0.0 --- (17.06) ------------------------------------------------------

2017-06-14 Mark Jones

	* Added Azure subscription: Aclara-Azure ACE PROD to AzurePowerBIConnectionInfo configuration.
	  - Production subscription accessible from Report Portal Configuration Sidekick.

--- V0.56.0.0 --- (17.06) ------------------------------------------------------

2017-06-05 Mark Jones

	* Updated help - SidekickReportPortalConfig.htm.

--- V0.55.0.0 --- (17.06) ------------------------------------------------------

2017-05-29 Mark Jones

	* Enhanced Sidekicks Components Assistant and Manager.
	  - Enhanced configuration to support Windows 7 and Windows 10.
	* Added Sidekicks Auto-updater Info.

--- V0.54.0.0 --- (17.05) ------------------------------------------------------

2017-05-23 Mark Jones

	* Enhanced sidekick container.
	  - Added "back" and "forward" buttons.
	* Updated Report Portal Sidekick help page.
	  - Added Mike Coote notes.

--- V0.53.0.0 --- (17.05) ------------------------------------------------------

2017-05-16 Mark Jones

	* Incremented version number to demonstrate auto-updater.

--- V0.52.0.0 --- (17.05) ------------------------------------------------------

2017-05-16 Mark Jones

	* Removed Aclara One Alerts Diagnostics sidekick.

--- V0.51.0.0 --- (17.05) ------------------------------------------------------

2017-05-10 Mark Jones

	* Fix bugs: Sidekick Components Assistant.
	  - Prevent horizontal scroll bar unless needed.
	  - Moved User Action button to right.

--- V0.50.0.0 --- (17.05) ------------------------------------------------------

2017-05-10 Mark Jones

	* Enhanced Sidekick Components Assistant.
	  - Enhanced user interface.

--- V0.49.0.0 --- (17.05) ------------------------------------------------------

2017-05-08 Mark Jones

	* Enhanced Cloud Service Sidekick.
      - Refresh cloud service automatically after scale cloud service.
	  - Added Check Under Scaled and Check Over Scaled menu options.
      - Prompt user to confirm cloud scale up/down/custom.
      - Renamed Validation button to Continue...
      - Show progress bar when getting/scaling Azure cloud service(s).
      - Numerous UI improvements.

--- V0.48.0.0 --- (17.05) ------------------------------------------------------

2017-04-21 Mark Jones

	* Enhanced Cloud Service Sidekick.
	  - Added color highligh depending on current role instance count.
--- V0.48.0.0 --- (17.05) ------------------------------------------------------

2017-04-21 Mark Jones

	* Enhanced Cloud Service Sidekick.
	  - Added color highligh depending on current role instance count.
		[Current role instance count <= Scale down] = Green
		[Current role instance count > Scale down] = Yellow
			[Current role instance count > Scale Up] = Red

--- V0.48.0.0 --- (17.05) ------------------------------------------------------

2017-04-19 Mark Jones

	* Enhanced Cloud Service Sidekick.
	  - Added ability to copy Azure cloud services list to clipboard as text and html.

--- V0.46.0.0 --- (17.05) ------------------------------------------------------

2017-04-18 Mark Jones

	* Fixed bug: Cloud Service Sidekick not retrieving cloud service instance count.

--- V0.45.0.0 --- (17.05) ------------------------------------------------------

2017-04-18 Mark Jones

	* Fixed bug: AzureRm will not load because execution policy is restricted.
	  - Added code to call: Set-ExecutionPolicy RemoteSigned -Force -Scope process.

--- V0.44.0.0 --- (17.05) ------------------------------------------------------

2017-04-18 Mark Jones

	* Fixed bug: 17843 - Azure Automation Sidekicks fails to create Azure Blob Container
	  - Refactored Azure Storage Account Config UI validation.

--- V0.43.0.0 --- (17.05) ------------------------------------------------------

2017-04-17 Mark Jones

	* Fixed bug: Login-AzureRmAccount may not work for all users.
	  - Replaced call to Login-AzureRmAccount with Add-AzureRmAccount.

--- V0.42.0.0 --- (17.05) ------------------------------------------------------

2017-04-17 Mark Jones

	* Fixed bug: Default Azure Subscription was not set before Azure API request.
	  - Replaced call to Select-AzureSubscription with Select-AzureRmSubscription.

--- V0.41.0.0 --- (17.05) ------------------------------------------------------

2017-04-15 Mark Jones

	* Fixed bug: Default Azure Subscription was not set before Azure API request.
	* Enhanced Sidekick Container to allow user to view new version change log (when available). 

--- V0.40.0.0 --- (17.03) ------------------------------------------------------

2017-03-13 Mark Jones

	* Enhanced component assistant to resize status panel depending on length of text displayed.
	* Enhanced help to display link to Microsoft .Net Framework downloads.

--- V0.39.0.0 --- (17.03) ------------------------------------------------------

2017-03-13 Mark Jones

	* Fixed bug timer that controls check Auto-updater was not enabled.

--- V0.38.0.0 --- (17.03) ------------------------------------------------------

2017-03-13 Mark Jones

	* Updated help: Component installation guide.

--- V0.37.0.0 --- (17.03) ------------------------------------------------------

2017-03-12 Mark Jones

	* Enhanced sidekick component assistant.
	  - Improved user experience.

	* Enhanced sidekick component manager.
	  - Display Validate button so that user can decide to proceed.

	* Enhanced Azure Cloud Service sidekick.
	  - Reset Azure cloud service properties when retrieving from Azure.

	* Enhanced Sidekick Container to check auto-updater every 60 minutes.

--- V0.36.0.0 --- (17.03) ------------------------------------------------------

2017-03-10 Mark Jones

	* Enhanced sidekick component assistant.
	  - User can install, upgrade Windows PowerShell via the assistant.

--- V0.35.0.0 --- (17.03) ------------------------------------------------------

2017-03-09 Mark Jones

	* Enhanced sidekick component assistant .
	  - User can install PowerShell Gallery (Package Manager) and Azure PowerShell module via the assistant.

--- V0.34.0.0 --- (17.03) ------------------------------------------------------

2017-03-07 Mark Jones

	* Enhanced sidekick component manager .
	  - Check PowerShell module: Azure for minimum version.

--- V0.33.0.0 --- (17.03) ------------------------------------------------------

2017-03-06 Mark Jones

	* Added Sidekick Component Assistant.
	  - View and verify Sidekick components (Ex: Azure PowerShell).

--- V0.32.0.0 --- (17.03) ------------------------------------------------------

2017-03-02 Mark Jones

	* Enhanced Azure Cloud Service Sidekick.
	  - Log errors when performing Azure cloud service retrieval and scale operations.

--- V0.31.0.0 --- (17.03) ------------------------------------------------------

2017-03-02 Mark Jones

	* Enhanced Azure Cloud Service Sidekick.
	  - Add "custom" scale functionality.
	  - Show which Azure cloud service require refresh before scale change can
	    be requested.

--- V0.30.0.0 --- (17.03) ------------------------------------------------------

2017-03-01 Mark Jones

	* Added Azure Cloud Service Sidekick.
	  - Retrieve Azure cloud service information (Role Instance Count, VM Size).
	  - Scale (up/down) Azure cloud service.

--- V0.29.0.0 --- (17.03) ------------------------------------------------------

2017-02-19 Mark Jones

	* Added component manager.
	  - Verify required components.
	  - Login into components.

--- V0.28.0.0 --- (17.03) ------------------------------------------------------

2017-02-13 Mark Jones

	* Partial implementation of PowerShell Host Sidekick.

--- V0.27.0.0 --- (17.03) ------------------------------------------------------

2017-02-09 Mark Jones

	* Enhanced Report Portal Configuration Sidekick.
	  - Bugs fixed.

--- V0.26.0.0 --- (17.03) ------------------------------------------------------

2017-02-07 Mark Jones

	* Enhanced Report Portal Configuration Sidekick.
	  - Added actions: 
	    o Get Datasets, 
		o Delete Dataset, 
		o Update Dataset Credentials.

--- V0.25.0.0 --- (17.03) ------------------------------------------------------

2017-02-04 Mark Jones

	* Added AO Alerts Diag Sidekick (Partial implementation).

--- V0.24.0.0 --- (17.03) ------------------------------------------------------

2016-01-28 Mark Jones

	* Minor UI improvements.

--- V0.23.0.0 --- (17.03) ------------------------------------------------------

2016-01-26 Mark Jones

	* Added missing assembly to setup.

--- V0.22.0.0 --- (17.03) ------------------------------------------------------

2016-01-26 Mark Jones

	* Enhanced Home Sidekick.
	  - Redesigned navigation menu.

--- V0.21.0.0 --- (17.03) ------------------------------------------------------

2016-01-24 Mark Jones

	* Added missing assembly to setup.

--- V0.20.0.0 --- (17.03) ------------------------------------------------------

2016-01-24 Mark Jones

	* Fixed bugs in Azure Storage Account Config sidekick.
	  - Detailed log entries missing.
    * Fixed bugs in Cassandra Config sidekick.
	  - Detailed log entries missing.

--- V0.19.0.0 --- (17.03) ------------------------------------------------------

2016-01-24 Mark Jones

	* Added help button to each sidekick.

--- V0.18.0.0 --- (17.03) ------------------------------------------------------

2016-01-23 Mark Jones

	* Enhanced Report Portal Config sidekick.
	  - Added ability to import PowerBI desktop file (pbix).

--- V0.17.0.0 --- (17.03) ------------------------------------------------------

2016-01-22 Mark Jones

	* Enhanced Report Portal Config sidekick.
	  - Added ability to create workspace in specified workspace collection.

--- V0.16.0.0 --- (17.03) ------------------------------------------------------

2016-01-22 Mark Jones

	* Added missing assembly reference in setup.

--- V0.15.0.0 --- (17.03) ------------------------------------------------------

2016-01-22 Mark Jones

	* Added Report Portal Sidekick.

--- V0.14.0.0 --- (17.03) ------------------------------------------------------

2016-01-14 Mark Jones

	* Added Redis Cache sidekick.
	* Added Azure Storage Account Config Sidekick.

--- V0.13.0.0 --- (17.03) ------------------------------------------------------

2016-01-12 Mark Jones

	* Added Redis Cache connection information to AppSettings.json.

--- V0.12.0.0 --- (17.03) ------------------------------------------------------

2016-01-12 Mark Jones

	* Replaced .NET app.config with JSON AppSetttings.json.

--- V0.11.0.0 --- (17.03) ------------------------------------------------------

2016-01-08 Mark Jones

	* Updated copyright date.
	* Updated logging user interface.

--- V0.10.0.0 --- (17.03) ------------------------------------------------------

2016-01-07 Mark Jones

	* Enhanced logging user interface.

--- V0.9.0.0 --- (17.03) ------------------------------------------------------

2016-01-05 Mark Jones

	* Enhanced logging to include sidekick name.

--- V0.8.0.0 --- (17.03) ------------------------------------------------------

2016-01-04 Mark Jones

	* Added Data Explorer Sidekick (Partial implementation)

--- V0.7.0.0 --- (17.03) ------------------------------------------------------

2016-12-29 Mark Jones

	* Added PowerShell Host Sidekick (Partial implementation)

--- V0.6.0.0 --- (17.03) ------------------------------------------------------

2016-12-29 Mark Jones

	* Updated logo.

--- V0.5.0.0 --- (17.03) ------------------------------------------------------

2016-12-29 Mark Jones

	* Added assembly to setup program.
	  - Aclara.Tools.Common.dll

--- V0.4.0.0 --- (17.03) ------------------------------------------------------

2016-12-29 Mark Jones

	* Fully functional template sidekick.

--- V0.3.0.0 --- (17.03) ------------------------------------------------------

2016-12-25 Mark Jones

	* Changed icon and about box dialog to show current Aclara branding.

--- V0.2.0.0 --- (17.03) ------------------------------------------------------

2016-12-25 Mark Jones

	* Sidekick Container Form implementation:
	  - Menu strip menu items.
	  - Main tool strip items.
    * Bug fix: Corrected Bootstrapper "Output name" typo. 

--- V0.1.0.0 --- (17.03) ------------------------------------------------------

2016-12-25 Mark Jones

	* First version
