﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.Azure.Automation.Sidekicks.WinForm.Configuration
{
    public class SidekickConfigurationManager
    {
        #region Private Constants
        #endregion

        #region Private Data Members

        private string _configurationFileName;
        private string _configurationFilePath;
        private SidekickConfiguration _sidekickConfiguration;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Configuration file name.
        /// </summary>
        public string ConfigurationFileName
        {
            get { return _configurationFileName; }
            set { _configurationFileName = value; }
        }

        /// <summary>
        /// Property: Configuration file path.
        /// </summary>
        public string ConfigurationFilePath
        {
            get { return _configurationFilePath; }
            set { _configurationFilePath = value; }
        }

        /// <summary>
        /// Property: Sidekick configuration.
        /// </summary>
        public SidekickConfiguration SidekickConfiguration
        {
            get { return _sidekickConfiguration; }
            set { _sidekickConfiguration = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="configurationFileName"></param>
        /// <param name="configurationFilePath"></param>
        public SidekickConfigurationManager(string configurationFileName, string configurationFilePath)
        {
            _configurationFileName = configurationFileName;
            _configurationFilePath = configurationFilePath;
            _sidekickConfiguration = new SidekickConfiguration();
        }

        #endregion

        #region Private Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SidekickConfigurationManager()
        {

        }

        #endregion

        #region Public Methods

        public void InitializeConfiguration()
        {
            string configurationPathFileName = string.Empty;

            try
            {
                configurationPathFileName = Path.Combine(this.ConfigurationFilePath, this.ConfigurationFileName);

                using (StreamReader file = File.OpenText(configurationPathFileName))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    this.SidekickConfiguration = (SidekickConfiguration)serializer.Deserialize(file, typeof(SidekickConfiguration));
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }

    public class SidekickConfiguration
    {
        [JsonProperty("Documents")]
        public Documents Documents { get; set; }

        [JsonProperty("AzureStorageAccount")]
        public AzureStorageAccountConnectionInfo AzureStorageAccount { get; set; }

        [JsonProperty("CassandraConnectionInfo")]
        public CassandraConnectionInfo CassandraConnectionInfo { get; set; }

        [JsonProperty("AzureRedisCacheConnectionInfo")]
        public AzureRedisCacheConnectionInfo RedisCacheConnectionInfo { get; set; }

        [JsonProperty("AzurePowerBIConnectionInfo")]
        public AzurePowerBIConnectionInfo AzurePowerBIConnectionInfo { get; set; }

        [JsonProperty("AzureCloudServiceInfo")]
        public AzureCloudServiceInfo AzureCloudServiceInfo { get; set; }

        [JsonProperty("ComponentInfo")]
        public ComponentInfo ComponentInfo { get; set; }
        
    }

    public class Documents
    {

        [JsonProperty("Document")]
        public List<Document> Document { get; set; }

    }

    /// <summary>
    /// Document configuration information.
    /// </summary>
    public class Document
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("FileName")]
        public string FileName { get; set; }

        [JsonProperty("Path")]
        public string Path { get; set; }

        [JsonProperty("Visible")]
        public string Visible { get; set; }
    }

    /// <summary>
    /// Azure storage account connection info.
    /// </summary>
    public class AzureStorageAccountConnectionInfo
    {
        [JsonProperty("AzureSubscription")]
        public List<AzureStorageAccount_AzureSubscription> AzureSubscription { get; set; }

    }

    /// <summary>
    /// Azure storage account connection info.
    /// </summary>
    public class AzureStorageAccount_AzureSubscription
    {

        [JsonProperty("SubscriptionName")]
        public string SubscriptionName { get; set; }

        [JsonProperty("Environment")]
        public List<AzureStorageAccount_Environment> Environment { get; set; }

    }

    /// <summary>
    /// Azure storage connection info: Environment.
    /// </summary>
    public class AzureStorageAccount_Environment
    {

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("AccessKey")]
        public string AccessKey { get; set; }
    }

    /// <summary>
    /// Cassandra connection info.
    /// </summary>
    public class CassandraConnectionInfo
    {

        [JsonProperty("AzureSubscription")]
        public List<CassandraConnectionInfo_AzureSubscription> AzureSubscription { get; set; }

    }

    /// <summary>
    /// Cassandra connection info.
    /// </summary>
    public class CassandraConnectionInfo_AzureSubscription
    {

        [JsonProperty("SubscriptionName")]
        public string SubscriptionName { get; set; }

        [JsonProperty("Environment")]
        public List<CassandraConnectionInfo_Environment> Environment { get; set; }

    }
    /// <summary>
    /// Cassandra connection info: Environment.
    /// </summary>
    public class CassandraConnectionInfo_Environment
    {

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("CassandraNodes")]
        public string CassandraNodes { get; set; }

        [JsonProperty("CassandraKeyspace")]
        public string CassandraKeyspace { get; set; }

        [JsonProperty("CassandraNodeUsername")]
        public string CassandraNodeUsername { get; set; }

        [JsonProperty("CassandraNodePassword")]
        public string CassandraNodePassword { get; set; }

    }

    /// <summary>
    /// Azure Redis cache connection info.
    /// </summary>
    public class AzureRedisCacheConnectionInfo
    {

        [JsonProperty("AzureSubscription")]
        public List<AzureRedisCacheConnectionInfo_AzureSubscription> AzureSubscription { get; set; }

    }

    /// <summary>
    /// Azure Redis cache connection info.
    /// </summary>
    public class AzureRedisCacheConnectionInfo_AzureSubscription
    {

        [JsonProperty("SubscriptionName")]
        public string SubscriptionName { get; set; }

        [JsonProperty("Environment")]
        public List<AzureRedisCacheConnectionInfo_Environment> Environment { get; set; }

    }

    /// <summary>
    /// Azure Redis cache connection info: Environment.
    /// </summary>
    public class AzureRedisCacheConnectionInfo_Environment
    {

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("RedisCache")]
        public List<AzureRedisCacheConnectionInfo_RedisCache> RedisCache { get; set; }

    }

    /// <summary>
    /// Azure Redis cache connection info: Environment.
    /// </summary>
    public class AzureRedisCacheConnectionInfo_RedisCache
    {

        [JsonProperty("RedisCacheName")]
        public string RedisCacheName { get; set; }

        [JsonProperty("Endpoint")]
        public string Endpoint { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("SSL")]
        public bool SSL { get; set; }

        [JsonProperty("ConnectRetry")]
        public int ConnectRetry { get; set; }

        [JsonProperty("ConnectTimeout")]
        public int ConnectTimeout { get; set; }

        [JsonProperty("SyncTimeout")]
        public int SyncTimeout { get; set; }
    }

    /// <summary>
    /// Azure PowerBI connection info.
    /// </summary>
    public class AzurePowerBIConnectionInfo
    {

        [JsonProperty("AzureSubscription")]
        public List<AzurePowerBIConnectionInfo_AzureSubscription> AzureSubscription { get; set; }

    }

    /// <summary>
    /// Azure PowerBI connection info.
    /// </summary>
    public class AzurePowerBIConnectionInfo_AzureSubscription
    {

        [JsonProperty("SubscriptionName")]
        public string SubscriptionName { get; set; }

        [JsonProperty("Environment")]
        public List<AzurePowerBIConnectionInfo_Environment> Environment { get; set; }

    }
    /// <summary>
    /// Azure PowerBI connection info: Environment.
    /// </summary>
    public class AzurePowerBIConnectionInfo_Environment
    {

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("PowerBI")]
        public List<AzurePowerBIConnectionInfo_PowerBI> PowerBI { get; set; }

    }

    /// <summary>
    /// Azure PowewrBI connection info: Environment.
    /// </summary>
    public class AzurePowerBIConnectionInfo_PowerBI
    {

        [JsonProperty("PowerBIApiEndpoint")]
        public string PowerBIApiEndpoint { get; set; }

        [JsonProperty("AzureApiEndpoint")]
        public string AzureApiEndpoint { get; set; }

        [JsonProperty("WorkspaceCollectionName")]
        public string WorkspaceCollectionName { get; set; }

        [JsonProperty("WorkspaceCollectionAccessKey")]
        public string WorkspaceCollectionAccessKey { get; set; }

    }

    /// <summary>
    /// Azure cloud service info.
    /// </summary>
    public class AzureCloudServiceInfo
    {

        [JsonProperty("AzureSubscription")]
        public List<AzureCloudServiceInfo_AzureSubscription> AzureSubscription { get; set; }

    }

    /// <summary>
    /// Azure cloud service info: Azure subscription.
    /// </summary>
    public class AzureCloudServiceInfo_AzureSubscription
    {

        [JsonProperty("SubscriptionName")]
        public string SubscriptionName { get; set; }

        [JsonProperty("Environment")]
        public List<AzureCloudServiceInfo_Environment> Environment { get; set; }

    }
    /// <summary>
    /// Azure cloud service info: Environment.
    /// </summary>
    public class AzureCloudServiceInfo_Environment
    {

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("CloudService")]
        public List<AzureCloudServiceInfo_CloudService> CloudService { get; set; }

    }

    /// <summary>
    /// Azure cloud service info: Cloud service.
    /// </summary>
    public class AzureCloudServiceInfo_CloudService
    {

        [JsonProperty("ServiceName")]
        public string ServiceName { get; set; }

        [JsonProperty("Slot")]
        public string Slot { get; set; }

        [JsonProperty("InstanceCountScaleUp")]
        public int InstanceCountScaleUp { get; set; }

        [JsonProperty("InstanceCountScaleDown")]
        public int InstanceCountScaleDown { get; set; }

    }

    /// <summary>
    /// Component information.
    /// </summary>
    public class ComponentInfo
    {
        [JsonProperty("ClientOSList")]
        public ComponentInfo_ClientOSList ClientOSList { get; set; }
    }

    /// <summary>
    /// Client OS.
    /// </summary>
    public class ComponentInfo_ClientOSList
    {
        [JsonProperty("ClientOS")]
        public List<ComponentInfo_ClientOS> ClientOS { get; set; }
    }

    /// <summary>
    /// Client OS.
    /// </summary>
    public class ComponentInfo_ClientOS
    {

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("VersionMajor")]
        public int VersionMajor { get; set; }

        [JsonProperty("VersionMinor")]
        public int VersionMinor { get; set; }

        [JsonProperty("ComponentList")]
        public ComponentInfo_ComponentList ComponentList { get; set; }
    }

    /// <summary>
    /// Component list.
    /// </summary>
    public class ComponentInfo_ComponentList
    {
        [JsonProperty("Component")]
        public List<ComponentInfo_Component> Component { get; set; }
    }

    /// <summary>
    /// Component.
    /// </summary>
    public class ComponentInfo_Component
    {
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("Description")]
        public string Description { get; set; }
        [JsonProperty("MinimumVersion")]
        public string MinimumVersion { get; set; }
        [JsonProperty("Provider")]
        public string Provider { get; set; }
        [JsonProperty("HelpKeyword")]
        public string HelpKeyword { get; set; }
        [JsonProperty("Required")]
        public bool Required { get; set; }
        [JsonProperty("LoginRequired")]
        public bool LoginRequired { get; set; }
        [JsonProperty("InstallerPath")]
        public string InstallerPath { get; set; }
        [JsonProperty("Installer")]
        public string Installer { get; set; }
    }
}
