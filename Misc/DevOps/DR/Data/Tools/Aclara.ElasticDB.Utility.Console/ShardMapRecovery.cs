﻿using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement;
using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement.Recovery;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Aclara.ElasticDB.Utility.Console
{
    public class ShardMapRecovery
    {
        #region Private Constants
        #endregion

        #region Private Data Members
        #endregion

        #region Public Properties
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ShardMapRecovery()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Recover shard locations after geo-failover.
        /// </summary>
        /// <param name="shardMapManagerconnectionString"></param>
        /// <param name="PrimaryServerName"></param>
        /// <param name="FailoverServerName"></param>
        public void RecoverShardLocationsAfterGeoFailover(string shardMapManagerconnectionString, string PrimaryServerName, string FailoverServerName)
        {
            ShardMapManager shardMapManager = null;
            RecoveryManager recoveryManager = null;
            IEnumerable<ShardMap> shardMaps = null;
            IEnumerable<Shard> shards = null;
            IEnumerable<RecoveryToken> recoveryTokens = null;

            try
            {
                shardMapManager = ShardMapManagerFactory.GetSqlShardMapManager(shardMapManagerconnectionString,
                                                                               ShardMapManagerLoadPolicy.Lazy);
                if (shardMapManager == null)
                {
                    System.Console.WriteLine(string.Format("Shard map manager could not be created via connection string. Check configuration."));
                    return;
                }

                recoveryManager = shardMapManager.GetRecoveryManager();

                shardMaps = shardMapManager.GetShardMaps();

                if (shardMaps == null)
                {
                    System.Console.WriteLine(string.Format("Shard maps not found in shard map manager."));
                    return;
                }

                UpdateExternalDatasourceLocation(shardMapManagerconnectionString, FailoverServerName);

                foreach (ShardMap shardMap in shardMaps)
                {
                    shards = shardMap.GetShards();
                    if (shards == null)
                    {
                        System.Console.WriteLine(string.Format("Shards not retrieved from shard map."));
                        continue;
                    }

                    foreach (Shard shard in shards)

                        if (shard.Location.Server.ToLower() != FailoverServerName.ToLower())
                        {
                            ShardLocation shardLocation = new ShardLocation(FailoverServerName, shard.Location.Database);

                            if (UpdateExternalDatasourceLocation(shardMapManagerconnectionString, FailoverServerName,shard.Location.Database))
                            {
                                recoveryManager.DetachShard(shard.Location);

                                recoveryManager.AttachShard(shardLocation);

                                recoveryTokens = recoveryManager.DetectMappingDifferences(shardLocation);

                                if (recoveryTokens == null)
                                {
                                    System.Console.WriteLine(string.Format("Recovery tokens not retrieved from detect mapping differences."));
                                    continue;
                                }

                                foreach (RecoveryToken recoveryToken in recoveryTokens)
                                {
                                    recoveryManager.ResolveMappingDifferences(recoveryToken, MappingDifferenceResolution.KeepShardMapping);
                                }

                                System.Console.WriteLine("Shard mapping updated for " + shard.Location.Database);
                            }
                        }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        private bool UpdateExternalDatasourceLocation(string shardMapManagerconnectionString,string databaseServer,string databaseName = "InsightsDW")
        {
            var result = false;

            var connStr = shardMapManagerconnectionString;

            if (databaseName != "InsightsDW")
                connStr = Regex.Replace(shardMapManagerconnectionString, "InsightsDW", databaseName, RegexOptions.IgnoreCase);

            try
            {
                
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var cmd = new SqlCommand("[dbo].[UpdateExternalDataSourceLocation]", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ServerLocation", SqlDbType.VarChar) { Value = databaseServer });

                    cmd.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Error:" + databaseName + " - " + ex.Message);
            }

            return result;
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

    }
}
