﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.ElasticDB.Utility.Console
{
    class Program
    {

        private const string Configuration_AppSettingKey_ShardMapManagerConnectionString = "ShardMapManagerConnectionString";
        private const string Configuration_AppSettingKey_PrimaryServer = "PrimaryServer";
        private const string Configuration_AppSettingKey_SecondaryServer = "SecondaryServer";

        static void Main(string[] args)
        {

        ShardMapRecovery shardMapRecovery = null;
            string shardMapManagerConnectionString = string.Empty;
            string primaryServer = string.Empty;
            string secondaryServer = string.Empty;
            string failoverServer = string.Empty;

            try
            {

                shardMapRecovery = new ShardMapRecovery();

                shardMapManagerConnectionString = System.Configuration.ConfigurationManager.AppSettings[Configuration_AppSettingKey_ShardMapManagerConnectionString];
                primaryServer = System.Configuration.ConfigurationManager.AppSettings[Configuration_AppSettingKey_PrimaryServer];
                secondaryServer = System.Configuration.ConfigurationManager.AppSettings[Configuration_AppSettingKey_SecondaryServer];

                var input = PrintMenu(primaryServer, secondaryServer);

                switch (input)
                {
                    case 1: failoverServer = primaryServer;break;
                    case 2: failoverServer = secondaryServer;break;
                    default:return;
                }

                shardMapManagerConnectionString = shardMapManagerConnectionString.Replace("FAILOVER_SERVER", failoverServer);

                shardMapRecovery.RecoverShardLocationsAfterGeoFailover(shardMapManagerConnectionString,
                                                                       primaryServer,
                                                                       failoverServer);

                System.Console.Write("Press any key to Exit.");
                System.Console.Read();
            }
            catch (Exception)
            {
                throw;
            }


        }

        private static int PrintMenu(string primaryServer, string secondaryServer)
        {
            var input = 0;

            System.Console.WriteLine("Choose Failover Server");
            System.Console.WriteLine("----------------------");
            System.Console.WriteLine("1. " + primaryServer);
            System.Console.WriteLine("2. " + secondaryServer);
            System.Console.WriteLine("3. Exit");

            do
            {
                System.Console.Write("Enter number (1-3):");

                var value = System.Console.ReadLine();

                if (int.TryParse(value, out input) == false)
                    System.Console.WriteLine("Invalid Input!");

            } while (!(input >= 1 && input <= 3));
            return input;
        }
    }
}
