
======= PREREQUISITES ======= 

Windows Azure PowerShell

URL: http://www.windowsazure.com/en-us/downloads/
Command-line tools
Windows PowerShell
Install <-- Click link

======= TIPS =======

Instructions to add Azure to Windows PowerShell ISE
URL: https://regularitguy.com/2014/03/13/installing-the-windows-azure-powershell-cmdlets/

Select Azure Subscription and Import Azure Publish Settings File.
URL: https://azure.microsoft.com/en-us/documentation/articles/powershell-install-configure/