
Function Set-ACLAzureWebApps
{
    param($ConfigFileName, $Environment, $LogFileNameBase, $LogFileCategory)
    
    Set-StrictMode -Version Latest
    
    Import-Module $PSScriptRoot\..\..\Lib\Write-ACLLogfile.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLCommonPath.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLAzureSubscription.psm1
    Import-Module $PSScriptRoot\Set-ACLAzureWebApp.psm1
    
    # Retrieve script start date/time.
    $elapsedTime = [System.Diagnostics.Stopwatch]::StartNew()

    # Set service.
    $service = "Maintain"
    
    # Retrieve log file path file name.
    $logFilePathFileName = (Get-ACLLogFilePathFileName -LogFileNameBase $LogFileNameBase -Environment $Environment -Service $service -Category $LogFileCategory)

    # Retrieve error file path file name.
    $errorFilePathFileName = (Get-ACLErrorFilePathFileName -LogFileNameBase $LogFileNameBase -Environment $Environment -Service $service -Category $LogFileCategory)

    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "*** Script $($MyInvocation.ScriptName) started. (ConfigFileName: $($ConfigFileName), Environment: $($Environment))" `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)

    # Retrieve config file path.
    $configFilePath = (Get-ACLConfigFilePath)

    # Retrieve config file path name.
    $configFilePathName = join-path -path $configFilePath -childpath $ConfigFileName

    (Write-Host "=====================================================")
    (Write-Host "Maintain Azure web apps to $($Environment).")
    (Write-Host "=====================================================")

    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "Maintain Azure WebApps to $($Environment)." `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)

    # Get Azure subscription from configuration.
    $environmentConfig = Get-AzureSubscriptionFromConfig  $configFilePathName $Environment $logFilePathFileName $errorFilePathFileName
    
    # Get Azure subscription matching configuration.
    $azureSubscriptionValid = $false

    # Valid Azure subscription for environment in configuration.
    $azureSubscriptionValid = Test-ACLAzureSubscriptionCurrent $environmentConfig.SubscriptionID $environmentConfig.SubscriptionName
    if($azureSubscriptionValid)
    {
        (Write-Host "Azure subscription from configuration matches current Azure subscription in PowerShell session. (SubscriptionName: $($environmentConfig.SubscriptionName).")
        (Write-ACLLogfile -Severity "info" `
                          -LogEntry "Azure subscription from configuration matches current Azure subscription in PowerShell session. (SubscriptionName: $($environmentConfig.SubscriptionName)." `
                          -LogFilePathFileName $logFilePathFileName `
                          -ErrorFilePathFileName $errorFilePathFileName)
    }

    # Create Azure web apps from configuration.
    (Write-Host "+BEGIN: Set-AzureWebAppsFromConfig to $($Environment).")
	Set-AzureWebAppsFromConfig $configFilePathName $Environment $logFilePathFileName $errorFilePathFileName
    (Write-Host "-END: Set-AzureWebAppsFromConfig to $($Environment).")

    (Write-Host "`nSee log file for details: $logFilePathFileName`n")

    (Write-Host "=====================================================")
    (Write-Host "-End maintain Azure web apps to $($Environment).")
    (Write-Host "=====================================================")

    (Write-Host "Elapsed time: $($elapsedTime.Elapsed.ToString())")

    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "Elapsed time: $($elapsedTime.Elapsed.ToString())" `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)

    # Determine whether error log file exits.    
    if((Get-ErrorLogFileExists -ErrorFilePathFileName $errorFilePathFileName) -eq $true)
    {
        Write-Warning -Message "Script $($MyInvocation.ScriptName) completed with error(s).`nSee error log file for details: $errorFilePathFileName`n"
    }
    
    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "*** Script $($MyInvocation.ScriptName) completed." `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)
    return
}

Function Get-AzureSubscriptionFromConfig
{
    param($ConfigFilePathName, $Environment, $LogFilePathFileName, $ErrorFilePathFileName)

    $configurationFound = $false

    [Xml]$configXml = Get-Content $ConfigFilePathName
    if (!$configXml) 
    { 
        throw "Error: Cannot find configuration in $ConfigFilePathName"
    }

    $query = "//Config/Maintain/Environment[@Name='$Environment']"
    $environmentConfig = $configXml | Select-Xml -XPath $query | Select-Object -ExpandProperty Node

    if(!$environmentConfig)
    {
        (Write-ACLLogfile -Severity "error" `
                          -LogEntry "No environment configuration found." `
                          -LogFilePathFileName $logFilePathFileName `
                          -ErrorFilePathFileName $errorFilePathFileName)
    }

    return $environmentConfig
}

Function Set-AzureWebAppsFromConfig
{
    param($ConfigFilePathName, $Environment, $LogFilePathFileName, $ErrorFilePathFileName)

    $configurationFound = $false

    [Xml]$configXml = Get-Content $ConfigFilePathName
    if (!$configXml) 
    { 
        throw "Error: Cannot find configuration in $ConfigFilePathName"
    }

    $query = "//Config/Maintain/Environment[@Name='$Environment']/AzureWebApps/AzureWebApp"
    $azureWebAppsConfig = $configXml | Select-Xml -XPath $query | Select-Object -ExpandProperty Node

	foreach($azureWebAppConfig in $azureWebAppsConfig)
	{

		Set-ACLAzureWebapp $azureWebAppConfig `
                           $LogFilePathFileName `
                           $ErrorFilePathFileName
        $configurationFound = $true

	}

    if($configurationFound = $false)
    {
        (Write-ACLLogfile -Severity "info" `
                          -LogEntry "No Azure web app configuration found." `
                          -LogFilePathFileName $logFilePathFileName `
                          -ErrorFilePathFileName $errorFilePathFileName)
    }

    return
}
