Function Set-ACLAzureWebApp
{
    param($AzureWebAppConfig, $LogFilePathFileName, $ErrorFilePathFileName)

    Set-StrictMode -Version Latest
    
    Import-Module $PSScriptRoot\..\..\Lib\Write-ACLLogfile.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLCommonPath.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLAzureItemInfo.psm1

    if ($AzureWebAppConfig.Skip.ToLower() -eq "true")
    {
    	(Write-ACLLogfile -Severity "info" `
                          -LogEntry "Skip AzureWebApp creation. (Name: $($Name))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
        (Write-Host "Skip AzureWebApp creation. (Name: $($Name)).")
        return
    }

    Switch-AzureMode AzureServiceManagement
    $azureWebAppExist = Test-ACLAzureWebAppExist $AzureWebAppConfig.Name 

    if ($azureWebAppExist)
    {

        if($AzureWebAppConfig -ne $null)
        {
            Set-ACLWebsitePropertiesFromConfig $AzureWebAppConfig.Name $AzureWebAppConfig.Slot $AzureWebAppConfig $LogFilePathFileName $ErrorFilePathFileName
        }

        if($AzureWebAppConfig.AppSettings -ne $null)
        {
            Set-ACLAppSettingsFromConfig $AzureWebAppConfig.Name $AzureWebAppConfig.Slot $AzureWebAppConfig.AppSettings $LogFilePathFileName $ErrorFilePathFileName
        }

        if($AzureWebAppConfig.ConnectionStrings -ne $null)
        {
            Set-ACLConnectionStringsFromConfig $AzureWebAppConfig.Name $AzureWebAppConfig.Slot $AzureWebAppConfig.ConnectionStrings $LogFilePathFileName $ErrorFilePathFileName
        }

    }
    else
    {
        (Write-ACLLogfile -Severity "error" `
                          -LogEntry "Error: Set-AzureWebApp failed because AzureWebApp does not exist. (Name: $($Name))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
    }
    
    Switch-AzureMode AzureServiceManagement

    return
}

Function Set-ACLAppSettingsFromConfig
{

    param($Name, $Slot, $AzureAppSetttingsConfig, $LogFilePathFileName, $ErrorFilePathFileName)

    $returnValue = @{}
    $configurationFound = $false

    if($AzureAppSetttingsConfig -eq $null)
    {
        return
    }

    $azureWebsite = (Get-AzureWebsite -Name $Name -Slot $Slot )
    if($azureWebsite -ne $null)
    {
        $appSettingHashTable = $azureWebsite.AppSettings
        $slotStickyAppSettingNames = $azureWebsite.SlotStickyAppSettingNames
    }

	if($AzureAppSetttingsConfig.Action.ToLower() -eq "Clear")
    {
        $appSettingHashTable.Clear()
    }
	
	foreach($appSetting in $AzureAppSetttingsConfig.AppSetting)
	{
        switch ($appSetting.Action.ToLower())
        {
            delete 
            {
                $removeAppSettingResult = $appSettingHashTable.Remove($appSetting.Key)

                if($slotStickyAppSettingNames -contains $appSetting.Key)
                {
                    $slotStickyAppSettingNames.Remove($appSetting.Key)
                }

                break
            }
            addupdate
            {
                if( $appSettingHashTable.ContainsKey($appSetting.Key))
                {
                    $appSettingHashTable.Set_Item($appSetting.Key,$appSetting.Value)
                }
                else
                {
				    $appSettingHashTable.Add($appSetting.Key,$appSetting.Value)
                }

                if($appSetting.SlotSticky -ne $null -and
                   $appSetting.SlotSticky -eq "true")
                {
                    if($slotStickyAppSettingNames -notcontains $appSetting.Key)
                    {
                        $slotStickyAppSettingNames += $appSetting.Key
                    }
                }

                break
            }
        }

        $configurationFound = $true
	}

    $parameters = @{ }

    if($appSettingHashTable -ne $null)
    {
        $parameters.Add("AppSettings", $appSettingHashTable)
    }

    if($slotStickyAppSettingNames -ne $null)
    {
        $parameters.Add("SlotStickyAppSettingNames", $slotStickyAppSettingNames)
    }

    if($parameters.count -lt 1)
    {
        return
    }

    $parameters.Add("Name", $Name)
    $parameters.Add("Slot", $Slot)

    try
    {
        Set-AzureWebsite @parameters
        (Write-ACLLogfile -Severity "info" `
                          -LogEntry "Info: Set-AzureWebApp updated app settings. (Name: $($Name))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
    }
        catch 
    {
        (Write-ACLLogfile -Severity "error" `
                          -LogEntry "Error: Set-AzureWebApp failed to update appsettings. (Name: $($Name))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
    }
    return 
    	
}

Function Set-ACLConnectionStringsFromConfig
{

    param($Name, $Slot, $AzureConnectionStringsConfig, $LogFilePathFileName, $ErrorFilePathFileName)

    $returnValue = $null

    if($AzureConnectionStringsConfig -eq $null)
    {
        return
    }

    $azureWebsite = (Get-AzureWebsite -Name $Name -Slot $Slot )
    if($azureWebsite -ne $null)
    {
        $connectionStringPropertyBag = $azureWebsite.ConnectionStrings
        $slotStickyConnectionStringNames = $azureWebsite.SlotStickyConnectionStringNames
    }

    $configurationFound = $false

    if($AzureConnectionStringsConfig.Action.ToLower() -eq "Clear")
    {
        $connectionStringPropertyBag.Clear()
    }

	foreach($connectionString in $AzureConnectionStringsConfig.ConnectionString)
	{

        switch ($connectionString.Action.ToLower())
        {
            delete 
            {
                $removeConnectionStringResult = $connectionStringPropertyBag.Remove(($connectionStringPropertyBag | Where-Object {$_.Name -eq $connectionString.Key}))

                if($slotStickyConnectionStringNames -contains $connectionString.Key)
                {
                    $slotStickyConnectionStringNames.Remove($connectionString.Key)
                }

                break
            }
            addupdate
            {
                $connStringInfo = New-Object Microsoft.WindowsAzure.Commands.Utilities.Websites.Services.WebEntities.ConnStringInfo

                $connStringInfo.Name = $connectionString.Key
                $connStringInfo.ConnectionString = $connectionString.Value
                switch($connectionString.Type.ToLower())
                {
                    sqldatabase 
                    { 
                        $connStringInfo.Type = [Microsoft.WindowsAzure.Commands.Utilities.Websites.Services.WebEntities.DatabaseType]::SQLAzure
                    break
                    }
                    sqlserver 
                    {
                        $connStringInfo.Type = [Microsoft.WindowsAzure.Commands.Utilities.Websites.Services.WebEntities.DatabaseType]::SQLServer
                        break
                    }
                    mysql 
                    {
                        $connStringInfo.Type = [Microsoft.WindowsAzure.Commands.Utilities.Websites.Services.WebEntities.DatabaseType]::MySql
                        break
                    }
                    custom 
                    {
                        $connStringInfo.Type = [Microsoft.WindowsAzure.Commands.Utilities.Websites.Services.WebEntities.DatabaseType]::Custom
                        break
                    }
                }

                if( $connectionStringPropertyBag.Find({ param($m) $m.Name.Equals($connectionString.Key) }) )
                {
                    $removeConnectionStringResult = $connectionStringPropertyBag.Remove(($connectionStringPropertyBag | Where-Object {$_.Name -eq $connectionString.Key}))
                }

                $connectionStringPropertyBag.Add($connStringInfo)

                if($connectionString.SlotSticky -ne $null -and
                   $connectionString.SlotSticky -eq "true")
                {
                    if($slotStickyConnectionStringNames -notcontains $connectionString.Key)
                    {
                        $slotStickyConnectionStringNames += $connectionString.Key
                    }
                }

                break
            }
        }

        $configurationFound = $true
	}

    $parameters = @{ }

    if($connectionStringPropertyBag -ne $null)
    {
        $parameters.Add("ConnectionStrings", $connectionStringPropertyBag)
    }

    if($slotStickyConnectionStringNames -ne $null)
    {
        $parameters.Add("SlotStickyConnectionStringNames", $slotStickyConnectionStringNames)
    }

    if($parameters.count -lt 1)
    {
        return
    }

    $parameters.Add("Name", $Name)
    $parameters.Add("Slot", $Slot)
    
    try
    {
        Set-AzureWebsite @parameters
        (Write-ACLLogfile -Severity "info" `
                          -LogEntry "Info: Set-AzureWebApp updated connection strings. (Name: $($Name))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
    }
        catch 
    {
        (Write-ACLLogfile -Severity "error" `
                          -LogEntry "Error: Set-AzureWebApp failed to update connection strings. (Name: $($Name))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
    }

    return
}

Function Set-ACLWebsitePropertiesFromConfig
{
    param($Name, $Slot, $AzureWebAppConfig, $LogFilePathFileName, $ErrorFilePathFileName)

    $parameters = @{ }

    if($AzureWebAppConfig.HasAttribute("HostNames") -eq $true)
    {
        $parameters.Add("HostNames", $AzureWebAppConfig.Hostnames)
    }

    if($AzureWebAppConfig.HasAttribute("Use32BitWorkerProcess") -eq $true)
    {
        $use32BitWorkerProcess = $false
        try { $use32BitWorkerProcess = [System.Convert]::ToBoolean($use32BitWorkerProcess)} 
        catch [FormatException] { $use32BitWorkerProcess = $false }

        $parameters.Add("Use32BitWorkerProcess", $use32BitWorkerProcess)
    }

    if($parameters.count -lt 1)
    {
        return
    }

    $parameters.Add("Name", $Name )
    $parameters.Add("Slot", $Slot)
    
    try
    {
        Set-AzureWebsite @parameters
        (Write-ACLLogfile -Severity "info" `
                          -LogEntry "Info: Set-AzureWebApp updated properties. (Name: $($Name))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
    }
        catch 
    {
        (Write-ACLLogfile -Severity "error" `
                          -LogEntry "Error: Set-AzureWebApp failed to update properties. (Name: $($Name))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
    }

    return
}