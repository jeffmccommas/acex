Function Write-ACLLogfile 
{
    param($Severity, $LogEntry, $LogFilePathFileName, $ErrorFilePathFileName)
    
    Set-StrictMode -Version Latest

    $severityInfo = "info"
    $severityWarning = "warning"
    $severityError = "error"
    $severityUnknown = "unkown"

    #Validate parameter: Severity.
    if ($Severity -eq $null)
    {
        $Severity = $severityUnknown
        Write-Warning "Severity parameter missing"
    }

    #Validate parameter: Log file path file name.
    if ($LogFilePathFileName -eq $null)
    {
        Write-Error "LogFilePathFileName parameter is required." -Category InvalidArgument
    }

    #Validate parameter: Error file path file name.
    if ($ErrorFilePathFileName -eq $null)
    {
        Write-Error "ErrorFilePathFileName parameter is required."  -Category InvalidArgument
    }

    try
    {
        $logFilePath = (Split-Path -Path $LogFilePathFileName)
        $errorFilePath = (Split-Path -Path $ErrorFilePathFileName)

        # Log file path does not already exist.
        if ((Test-Path $logFilePath) -eq 0) 
        {
            # Create log file path.
    		Write-Host "Create log file folder: $logFilePath" -Foregroundcolor Green
    		New-Item -Path $logFilePath -type directory | Out-Null
    	}

        # Write log entry to log file.
        $logTimestamp = "[{0:yyyy-MM-dd  HH:mm:ss}]" -f (Get-Date)
        $logTimestamp + " | " + $Severity + " | " + $LogEntry | Out-File -FilePath $LogFilePathFileName -Encoding "UTF8" -Append


        # Log entry is Error or Warning.
        if ($Severity.ToLower() -eq $severityWarning.ToLower() -or $Severity.ToLower() -eq $severityError.ToLower())
        {
            # Error file path does not already exist.
            if ((Test-Path $errorFilePath) -eq 0) 
            {
                # Create log file path.
        		Write-Host "Create log file folder: $logFilePath" -Foregroundcolor Green
        		New-Item -Path $errorFilePath -type directory | Out-Null
        	}
            # Write log entry to error file.
            $logTimestamp = "[{0:yyyy-MM-dd  HH:mm:ss}]" -f (Get-Date)
            $logTimestamp + " | " + $Severity + " | " + $LogEntry | Out-File -FilePath $ErrorFilePathFileName -Encoding "UTF8" -Append
        }

     }
     catch
     {
        Write-Error "Could not output to log file/error file."
     }   
}

Function Get-ErrorLogFileExists
{
    param($ErrorFilePathFileName)
    
    Set-StrictMode -Version Latest

    #Validate parameter: Error file path file name.
    if ($ErrorFilePathFileName -eq $null)
    {
        Write-Error "ErrorFilePathFileName parameter is required."  -Category InvalidArgument
    }

    try
    {
        $errorFilePath = (Split-Path -Path $ErrorFilePathFileName)

        # Error file path does not already exist.
        if ((Test-Path $ErrorFilePathFileName) -eq $true) 
        {
            # Error log file found.
            return $true
        }
        else
        {
            # Error log file not found.
            return $false
        }

     }
     catch
     {
        Write-Error "Error occurred determining status of error log file."
     }   
}