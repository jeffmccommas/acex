Function Test-ACLAzureResourceGroupExist
{
    param($Name)
    
    Set-StrictMode -Version Latest

    $azureResourceGroupExist = $false

    $azureResourceGroup = $null

    $errorActionPreferenceRollback = $errorActionPreference
    $errorActionPreference = "stop"

    try 
    {
        $azureResourceGroup = Get-AzureResourceGroup $Name
        if (!$azureResourceGroup) 
        { 
            $azureResourceGroupExist = $false 
        }
        else 
        { 
            $azureResourceGroupExist = $true 
        }
    }
    catch 
    { 
        $azureResourceGroupExist = $false 
    }

    $errorActionPreference = $errorActionPreferenceRollback

    return $azureResourceGroupExist
}

Function Test-ACLAzureRedisCacheExist
{
    param($Name, $ResourcegroupName)
    
    Set-StrictMode -Version Latest

    $azureRedisCacheExist = $false

    $azureRedisCache = $null
    
    $errorActionPreferenceRollback = $errorActionPreference
    $errorActionPreference = "stop"

    try 
    {
        $azureRedisCache = Get-AzureRedisCache -Name $Name -ResourceGroupName $ResourcegroupName
        if (!$azureRedisCache) 
        { 
            $azureRedisCacheExist = $false 
        }
        else 
        { 
            $azureRedisCacheExist = $true 
        }
    }
    catch 
    { 
        $azureRedisCacheExist = $false 
    }

    $errorActionPreference = $errorActionPreferenceRollback

    return $azureRedisCacheExist
}

Function Test-ACLAzureStorageAccountExist
{
    param($Name)
    
    Set-StrictMode -Version Latest

    $azureStorageAccountExist = $false

    $azureStorageAccountExist = Test-AzureName -Storage $Name

    return $azureStorageAccountExist
}

Function Test-ACLAzureAppServicePlanExist
{
    param($Name, $ResourceGroupName, $ResourceType)
    
    Set-StrictMode -Version Latest

    $azureAppServicePlanExist = $false
    $azureAppServicePlan = $null

    try
    {
        $azureAppServicePlan = Get-AzureResource -ApiVersion 2014-04-01 -Name $Name -ResourceGroupName $ResourceGroupName -ResourceType $ResourceType -OutputObjectFormat New
        if (!$azureAppServicePlan) 
        { 
            $azureAppServicePlanExist = $false 
        }
        else 
        { 
            $azureAppServicePlanExist = $true 
        }
    }
    catch 
    { 
        $azureAppServicePlanExist = $false 
    }

    return $azureAppServicePlanExist
}

Function Test-ACLAzureWebAppExist
{
    param($Name)
    
    Set-StrictMode -Version Latest

    $azureWebAppExist = $false

    $azureWebAppExist = Test-AzureName -Website $Name

    return $azureWebAppExist
}

Function Test-ACLAzureSqlDatabaseServerExist
{
    param($Name)
    
    Set-StrictMode -Version Latest

    $azureSqlDatabaseServerExist = $false

    $azureRedisCache = $null
    
    $errorActionPreferenceRollback = $errorActionPreference
    $errorActionPreference = "stop"

    try 
    {
        $azureSqlDatabaseServer =  Get-AzureSqlDatabaseServer -ServerName $Name
        if (!$azureSqlDatabaseServer) 
        { 
            $azureSqlDatabaseServerExist = $false 
        }
        else 
        { 
            $azureSqlDatabaseServerExist = $true 
        }
    }
    catch 
    { 
        $azureSqlDatabaseServerExist = $false 
    }

    $errorActionPreference = $errorActionPreferenceRollback

    return $azureSqlDatabaseServerExist
}

Function Test-ACLAzureSqlDatabaseExist
{
    param($Name, $ServerName)
    
    Set-StrictMode -Version Latest

    $azureSqlDatabaseExist = $false

    $azureRedisCache = $null
    
    $errorActionPreferenceRollback = $errorActionPreference
    $errorActionPreference = "stop"

    try 
    {
        $azureSqlDatabase =  Get-AzureSqlDatabase -ServerName $ServerName -DatabaseName $Name
        if (!$azureSqlDatabase) 
        { 
            $azureSqlDatabaseExist = $false 
        }
        else 
        { 
            $azureSqlDatabaseExist = $true 
        }
    }
    catch 
    { 
        $azureSqlDatabaseExist = $false 
    }

    $errorActionPreference = $errorActionPreferenceRollback

    return $azureSqlDatabaseExist
}

Function Test-ACLAzureServiceBusNamespaceExist
{
    param($Name)
    
    Set-StrictMode -Version Latest

    $azureServiceBusNamespaceExist = $false
    $azureServiceBusNamespace = $null

    try
    {
        $azureServiceBusNamespace = Get-AzureSBNamespace -Name $Name
        if (!$azureServiceBusNamespace) 
        { 
            $azureServiceBusNamespaceExist = $false 
        }
        else 
        { 
            $azureServiceBusNamespaceExist = $true 
        }
    }
    catch 
    { 
        $azureServiceBusNamespaceExist = $false 
    }

    return $azureServiceBusNamespaceExist
}

