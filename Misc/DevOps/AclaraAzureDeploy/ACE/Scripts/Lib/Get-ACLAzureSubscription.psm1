Function Test-ACLAzureSubscriptionCurrent
{
    param($AzureSubscriptionId, $AzureSubscriptionName)
    
    Set-StrictMode -Version Latest

    $azureSubscriptionCurrent = $false
    $azureSubscription = $null

    try 
    {
        $azureSubscription = Get-AzureSubscription -SubscriptionName $AzureSubscriptionName
    }
    catch 
    { 
        $azureSubscriptionCurrent = $false 
        $errorActionPreference = "stop"
        throw "Error: Cannot find Azure subscription. (SubscriptionId: $AzureSubscriptionId, SubscriptionName: $AzureSubscriptionName)"
    }

    if ($azureSubscription) 
    { 
        # NOTE: Expecting single Azure subscription.
        if($azureSubscription.SubscriptionId -eq $AzureSubscriptionId -and
            $azureSubscription.SubscriptionName -eq $AzureSubscriptionName -and
            $azureSubscription.IsDefault -eq $true -and
            $azureSubscription.IsCurrent -eq $true)
        {
            $azureSubscriptionCurrent = $true 
        }
        else 
        { 
            $azureSubscriptionCurrent = $false 
            $errorActionPreference = "stop"
            throw "Error: Azure subscription is not current or default. (SubscriptionId: $AzureSubscriptionId, SubscriptionName: $AzureSubscriptionName)"

        }
    }
    else 
    { 
        $azureSubscriptionCurrent = $false 
        $errorActionPreference = "stop"
        throw "Error: Azure subscription is not current or default. (SubscriptionId: $AzureSubscriptionId, SubscriptionName: $AzureSubscriptionName)"

    }

    return $azureSubscriptionCurrent
}