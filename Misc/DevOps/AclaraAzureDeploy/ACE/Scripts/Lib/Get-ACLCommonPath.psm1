
Function Get-ACLLogFilePath
{
    param($LogFileNameBase, $Environment, $Service = "UnknownService", $Category)
    
    Set-StrictMode -Version Latest

    $currentLogFileFolder = "{0:yyyyMMdd}" -f (Get-Date)
    
    return "C:\AclaraAzureDeploy\ACE\LogFiles\$currentLogFileFolder\$Environment\$Service\$Category\$logFileNameBase"
}

Function Get-ACLErrorFilePath
{
    param($LogFileNameBase, $Environment, $Service = "UnknownService", $Category)
    
    Set-StrictMode -Version Latest

    $currentLogFileFolder = "{0:yyyyMMdd}" -f (Get-Date)
    
    return "C:\AclaraAzureDeploy\ACE\LogFiles\$currentLogFileFolder\$Environment\$Service\$Category\$logFileNameBase"
}

Function Get-ACLLogFilePathFileName()
{
    param($logFileNameBase, $Environment, $Service = "UnknownService", $Category)
    
    Set-StrictMode -Version Latest

    $currentLogFileFolder = "{0:yyyyMMdd}" -f (Get-Date)

    $logFileName = $logFileNameBase + "Log.txt"
    
    return "C:\AclaraAzureDeploy\ACE\LogFiles\$currentLogFileFolder\$Environment\$Service\$Category\$logFileNameBase\$logFileName"
}

Function Get-ACLErrorFilePathFileName()
{
    param($logFileNameBase, $Environment, $Service = "UnknownService", $Category)
    
    Set-StrictMode -Version Latest

    $currentLogFileFolder = "{0:yyyyMMdd}" -f (Get-Date)

    $errorFileName = $logFileNameBase + "Error.txt"
    
    return "C:\AclaraAzureDeploy\ACE\LogFiles\$currentLogFileFolder\$Environment\$Service\$Category\$logFileNameBase\$errorFileName"
}

Function Get-ACLConfigFilePath
{
    
    Set-StrictMode -Version Latest

    return "C:\AclaraAzureDeploy\ACE\Scripts\Config\"
}
