[CmdletBinding(PositionalBinding=$True)]
Param(
    [Parameter(Mandatory = $true)]
    [ValidatePattern("^[a-z0-9]*$")]
    [String]$Environment,
    [Parameter(Mandatory = $true)]
    [String]$ConfigFileName
    )
    
Function Main
{
    Set-StrictMode -Version Latest
    
    Import-Module $PSScriptRoot\Lib\New-ACLAzureSqlDatabases.psm1
    
	$Environment = $Environment.ToLower()

	<#TODO: Validate environment parameter.
    Valid values (prod=production, 
                  dr=disaster recovery, 
                  np=Non-production, 
                  uat=User Acceptance Testing, 
                  qa=Quality Assurance, 
                  dev=Development)
    #>
    $logFileNameBase = "New-ACLAzureSqlDatabases"
    $logFileCategory = "ACLAzureSqlDatabases"

    (New-ACLAzureSqlDatabases -ConfigFileName $ConfigFileName `
						      -Environment $Environment `
                              -LogFileNameBase $logFileNameBase `
                              -LogFileCategory $logFileCategory)
}

. Main