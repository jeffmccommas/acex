Function New-ACLAzureResourceGroup
{
    param($Skip, $AlreadyExistAction, $Name, $Location, $LogFilePathFileName, $ErrorFilePathFileName)

    Set-StrictMode -Version Latest
    
    Import-Module $PSScriptRoot\..\..\Lib\Write-ACLLogfile.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLCommonPath.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLAzureItemInfo.psm1

    if ($Skip.ToLower() -eq "true")
    {
    	(Write-ACLLogfile -Severity "info" `
                        -LogEntry "Skip AzureResourceGroup creation. (Name: $($Name), Location: $($Location))." `
                        -LogFilePathFileName $LogFilePathFileName `
                        -ErrorFilePathFileName $ErrorFilePathFileName)
        (Write-Host "Skip AzureResourceGroup creation. (Name: $($Name), Location: $($Location)).")
        return
    }

    Switch-AzureMode AzureResourceManager

    $azureResourceGroupExist = Test-ACLAzureResourcegroupExist $Name

    if (!$azureResourceGroupExist)
    {
	    $resourceGroup = New-AzureResourceGroup -Name $Name -Location $Location

	    if ($resourceGroup)
	    {
	        (Write-ACLLogfile -Severity "info" `
                              -LogEntry "AzureResourceGroup created successfully (Name: $($Name), Location: $($Location))." `
                              -LogFilePathFileName $LogFilePathFileName `
                              -ErrorFilePathFileName $ErrorFilePathFileName)
	    }
	    else
	    {
	        (Write-ACLLogfile -Severity "error" `
                              -LogEntry "Error: New-AzureResourceGroup failed to create AzureResourceGroup (Name: $($Name), Location: $($Location))." `
                              -LogFilePathFileName $LogFilePathFileName `
                              -ErrorFilePathFileName $ErrorFilePathFileName)
	    }
    }
    else
    {
        $severity = ""
        switch($AlreadyExistAction.ToLower())
        {
            "info" {$severity = "info"}
            "warning" {$severity = "warning"}
            "error" {$severity = "error"}
            "throw" {$severity = "error"}
            default {$severity = "info"}
        }
	    (Write-ACLLogfile -Severity $severity `
                          -LogEntry "AzureResourceGroup already exists. (Name: $($Name), Location: $($Location))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
        if ($AlreadyExistAction.ToLower() -eq "throw")
        {
            throw "AzureResourceGroup already exists. (Name: $($Name), Location: $($Location))."
        }
    }
    
    Switch-AzureMode AzureServiceManagement

    return
}
