
Function New-ACLAzureSqlDatabases
{
    param($ConfigFileName, $Environment, $LogFileNameBase, $LogFileCategory)
    
    Set-StrictMode -Version Latest
    
    Import-Module $PSScriptRoot\..\..\Lib\Write-ACLLogfile.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLCommonPath.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLAzureSubscription.psm1
    Import-Module $PSScriptRoot\New-ACLAzureSqlDatabaseServer.psm1
    Import-Module $PSScriptRoot\New-ACLAzureSqlDatabase.psm1
    
    # Retrieve script start date/time.
    $elapsedTime = [System.Diagnostics.Stopwatch]::StartNew()

    # Set service.
    $service = "Setup"
    
    # Retrieve log file path file name.
    $logFilePathFileName = (Get-ACLLogFilePathFileName -LogFileNameBase $LogFileNameBase -Environment $Environment -Service $service -Category $LogFileCategory)

    # Retrieve error file path file name.
    $errorFilePathFileName = (Get-ACLErrorFilePathFileName -LogFileNameBase $LogFileNameBase -Environment $Environment -Service $service -Category $LogFileCategory)

    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "*** Script $($MyInvocation.ScriptName) started. (ConfigFileName: $($ConfigFileName), Environment: $($Environment))" `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)

    # Retrieve config file path.
    $configFilePath = (Get-ACLConfigFilePath)

    # Retrieve config file path name.
    $configFilePathName = join-path -path $configFilePath -childpath $ConfigFileName

    (Write-Host "=====================================================")
    (Write-Host "Setup Azure databases to $($Environment).")
    (Write-Host "=====================================================")

    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "Setup Azure Databases to $($Environment)." `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)

    # Get Azure subscription from configuration.
    $environmentConfig = Get-AzureSubscriptionFromConfig  $configFilePathName $Environment $logFilePathFileName $errorFilePathFileName
    
    # Get Azure subscription matching configuration.
    $azureSubscriptionValid = $false

    # Valid Azure subscription for environment in configuration.
    $azureSubscriptionValid = Test-ACLAzureSubscriptionCurrent $environmentConfig.SubscriptionID $environmentConfig.SubscriptionName
    if($azureSubscriptionValid)
    {
        (Write-Host "Azure subscription from configuration matches current Azure subscription in PowerShell session. (SubscriptionName: $($environmentConfig.SubscriptionName).")
        (Write-ACLLogfile -Severity "info" `
                          -LogEntry "Azure subscription from configuration matches current Azure subscription in PowerShell session. (SubscriptionName: $($environmentConfig.SubscriptionName)." `
                          -LogFilePathFileName $logFilePathFileName `
                          -ErrorFilePathFileName $errorFilePathFileName)
    }

    # Create Azure sql database servers from configuration.
    (Write-Host "+BEGIN: New-AzureSqlDatabaseServersFromConfig to $($Environment).")
	New-AzureSqlDatabaseServersFromConfig $configFilePathName $Environment $logFilePathFileName $errorFilePathFileName
    (Write-Host "-END: New-AzureSqlDatabaseServersFromConfig to $($Environment).")

    # Create Azure sql databases from configuration.
    (Write-Host "+BEGIN: New-AzureSqlDatabasesFromConfig to $($Environment).")
	New-AzureSqlDatabasesFromConfig $configFilePathName $Environment $logFilePathFileName $errorFilePathFileName
    (Write-Host "-END: New-AzureSqlDatabasesFromConfig to $($Environment).")

    (Write-Host "`nSee log file for details: $logFilePathFileName`n")

    (Write-Host "=====================================================")
    (Write-Host "-End setup Azure web apps to $($Environment).")
    (Write-Host "=====================================================")

    (Write-Host "Elapsed time: $($elapsedTime.Elapsed.ToString())")

    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "Elapsed time: $($elapsedTime.Elapsed.ToString())" `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)

    # Determine whether error log file exits.    
    if((Get-ErrorLogFileExists -ErrorFilePathFileName $errorFilePathFileName) -eq $true)
    {
        Write-Warning -Message "Script $($MyInvocation.ScriptName) completed with error(s).`nSee error log file for details: $errorFilePathFileName`n"
    }
    
    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "*** Script $($MyInvocation.ScriptName) completed." `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)
    return
}

Function Get-AzureSubscriptionFromConfig
{
    param($ConfigFilePathName, $Environment, $LogFilePathFileName, $ErrorFilePathFileName)

    $configurationFound = $false

    [Xml]$configXml = Get-Content $ConfigFilePathName
    if (!$configXml) 
    { 
        throw "Error: Cannot find configuration in $ConfigFilePathName"
    }

    $query = "//Config/Setup/Environment[@Name='$Environment']"
    $environmentConfig = $configXml | Select-Xml $query | Select -Expand Node

    if(!$environmentConfig)
    {
        (Write-ACLLogfile -Severity "error" `
                          -LogEntry "No environment configuration found." `
                          -LogFilePathFileName $logFilePathFileName `
                          -ErrorFilePathFileName $errorFilePathFileName)
    }

    return $environmentConfig
}

Function New-AzureSqlDatabaseServersFromConfig
{
    param($ConfigFilePathName, $Environment, $LogFilePathFileName, $ErrorFilePathFileName)

    $configurationFound = $false

    [Xml]$configXml = Get-Content $ConfigFilePathName
    if (!$configXml) 
    { 
        throw "Error: Cannot find configuration in $ConfigFilePathName"
    }

    $query = "//Config/Setup/Environment[@Name='$Environment']/AzureSqlDatabaseServers"
    $azureSqlDatabaseServersConfig = $configXml | Select-Xml $query | Select -Expand Node

	foreach($azureSqlDatabaseServerConfig in $azureSqlDatabaseServersConfig.AzureSqlDatabaseServer)
	{

		New-ACLAzureSqlDatabaseServer $azureSqlDatabaseServerConfig.Skip `
                                      $azureSqlDatabaseServerConfig.AlreadyExistAction `
                                      $azureSqlDatabaseServerConfig.Name `
                                      $azureSqlDatabaseServerConfig.Location `
                                      $azureSqlDatabaseServerConfig.ResourceGroupName `
                                      $azureSqlDatabaseServerConfig.ResourceType `
                                      $azureSqlDatabaseServerConfig.AdministratorLogin `
                                      $azureSqlDatabaseServerConfig.AdministratorLoginPassword `
                                      $azureSqlDatabaseServerConfig.Version `
                                      $LogFilePathFileName `
                                      $ErrorFilePathFileName
        $configurationFound = $true

	}

    if($configurationFound = $false)
    {
        (Write-ACLLogfile -Severity "info" `
                          -LogEntry "No Azure web app configuration found." `
                          -LogFilePathFileName $logFilePathFileName `
                          -ErrorFilePathFileName $errorFilePathFileName)
    }

    return
}

Function New-AzureSqlDatabasesFromConfig
{
    param($ConfigFilePathName, $Environment, $LogFilePathFileName, $ErrorFilePathFileName)

    $configurationFound = $false

    [Xml]$configXml = Get-Content $ConfigFilePathName
    if (!$configXml) 
    { 
        throw "Error: Cannot find configuration in $ConfigFilePathName"
    }

    $query = "//Config/Setup/Environment[@Name='$Environment']/AzureSqlDatabases"
    $azureSqlDatabaseServersConfig = $configXml | Select-Xml $query | Select -Expand Node

	foreach($azureSqlDatabaseConfig in $azureSqlDatabaseServersConfig.AzureSqlDatabase)
	{

		New-ACLAzureSqlDatabase $azureSqlDatabaseConfig.Skip `
                                $azureSqlDatabaseConfig.AlreadyExistAction `
                                $azureSqlDatabaseConfig.Name `
                                $azureSqlDatabaseConfig.ServerName `
                                $azureSqlDatabaseConfig.Edition `
                                $azureSqlDatabaseConfig.MaxSizeGB `
                                $azureSqlDatabaseConfig.Collation `
                                $LogFilePathFileName `
                                $ErrorFilePathFileName
        $configurationFound = $true

	}

    if($configurationFound = $false)
    {
        (Write-ACLLogfile -Severity "info" `
                          -LogEntry "No Azure web app configuration found." `
                          -LogFilePathFileName $logFilePathFileName `
                          -ErrorFilePathFileName $errorFilePathFileName)
    }

    return
}
