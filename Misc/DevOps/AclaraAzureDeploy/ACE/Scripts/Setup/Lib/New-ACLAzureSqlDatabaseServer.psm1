Function New-ACLAzureSqlDatabaseServer
{
    param($Skip, $AlreadyExistAction, $Name, $Location, $ResourceGroupName, $ResourceType, $AdministratorLogin, $AdministratorLoginPassword, $Version, $LogFilePathFileName, $ErrorFilePathFileName)

    Set-StrictMode -Version Latest
    
    Import-Module $PSScriptRoot\..\..\Lib\Write-ACLLogfile.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLCommonPath.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLAzureItemInfo.psm1

    if ($Skip.ToLower() -eq "true")
    {
    	(Write-ACLLogfile -Severity "info" `
                          -LogEntry "Skip AzureSqlDatabaseServer creation. (Name: $($Name), Location: $($Location))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
        (Write-Host "Skip AzureSqlDatabaseServer creation. (Name: $($Name), Location: $($Location)).")
        return
    }

    Switch-AzureMode AzureServiceManagement
    $AzureSqlDatabaseServerExist = Test-ACLAzureSqlDatabaseServerExist $Name 
    Switch-AzureMode AzureResourceManager

    if (!$AzureSqlDatabaseServerExist)
    {

        $propertyOpbject = @{administratorLogin=$AdministratorLogin; administratorLoginPassword=$AdministratorLoginPassword; version=$Version}

        $azureSqlDatabaseServer = New-AzureResource -ApiVersion 2014-04-01 -Name $Name -Location $Location -ResourceGroupName $ResourceGroupName -ResourceType $ResourceType -PropertyObject $propertyOpbject -OutputObjectFormat New -Force

	    if ($azureSqlDatabaseServer)
	    {
	        (Write-ACLLogfile -Severity "info" `
                              -LogEntry "AzureSqlDatabaseServer created successfully (Name: $($Name), Location: $($Location))." `
                              -LogFilePathFileName $LogFilePathFileName `
                              -ErrorFilePathFileName $ErrorFilePathFileName)
	    }
	    else
	    {
	        (Write-ACLLogfile -Severity "error" `
                              -LogEntry "Error: New-AzureSqlDatabaseServer failed to create AzureSqlDatabaseServer (Name: $($Name), Location: $($Location))." `
                              -LogFilePathFileName $LogFilePathFileName `
                              -ErrorFilePathFileName $ErrorFilePathFileName)
	    }
    }
    else
    {
        $severity = ""
        switch($AlreadyExistAction.ToLower())
        {
            "info" {$severity = "info"}
            "warning" {$severity = "warning"}
            "error" {$severity = "error"}
            "throw" {$severity = "error"}
            default {$severity = "info"}
        }
	    (Write-ACLLogfile -Severity $severity `
                          -LogEntry "AzureSqlDatabaseServer already exists. (Name: $($Name), Location: $($Location))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
        if ($AlreadyExistAction.ToLower() -eq "throw")
        {
            throw "AzureSqlDatabaseServer already exists. (Name: $($Name), Location: $($Location))."
        }
    }
    
    Switch-AzureMode AzureServiceManagement

    return
}
