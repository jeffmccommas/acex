Function New-ACLAzureWebApp
{
    param($Skip, $AlreadyExistAction, $Name, $Location, $ResourceGroupName, $ResourceType, $AppServicePlanName, $Slots, $LogFilePathFileName, $ErrorFilePathFileName)

    Set-StrictMode -Version Latest
    
    Import-Module $PSScriptRoot\..\..\Lib\Write-ACLLogfile.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLCommonPath.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLAzureItemInfo.psm1

    if ($Skip.ToLower() -eq "true")
    {
    	(Write-ACLLogfile -Severity "info" `
                          -LogEntry "Skip AzureWebApp creation. (Name: $($Name), Location: $($Location))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
        (Write-Host "Skip AzureWebApp creation. (Name: $($Name), Location: $($Location)).")
        return
    }

    Switch-AzureMode AzureServiceManagement
    $azureWebAppExist = Test-ACLAzureWebAppExist $Name 

    if (!$azureWebAppExist)
    {

        $slotList = @("Production")

        if($Slots -ne $null)
        {
            $slotList = $slotList + $Slots.split(";")
        }

        foreach($slot in $slotList)
        {
            if($slot -eq "Production")
            {
	            $azureWebApp = New-AzureWebsite -Name $Name -Location $Location

	            if ($azureWebApp)
	            {
                    # Configure newly created Azure web app with app service plan.
                    Switch-AzureMode AzureResourceManager
                    $propertyOpbject = $null;
                    $propertyOpbject = @{ 'serverFarm' = $AppServicePlanName }
                    $azureWebSite = Set-AzureResource -Name $Name -ResourceGroupName $ResourceGroupName -ResourceType $ResourceType -ApiVersion 2014-04-01 -PropertyObject $propertyOpbject -OutputObjectFormat New -Force
                    Switch-AzureMode AzureServiceManagement

	                (Write-ACLLogfile -Severity "info" `
                                      -LogEntry "AzureWebApp created successfully (Name: $($Name), Location: $($Location))." `
                                      -LogFilePathFileName $LogFilePathFileName `
                                      -ErrorFilePathFileName $ErrorFilePathFileName)
	            }
	            else
	            {
	                (Write-ACLLogfile -Severity "error" `
                                      -LogEntry "Error: New-AzureWebApp failed to create AzureWebApp (Name: $($Name), Location: $($Location))." `
                                      -LogFilePathFileName $LogFilePathFileName `
                                      -ErrorFilePathFileName $ErrorFilePathFileName)
	            }
            }
            else
            {
	            $azureWebApp = New-AzureWebsite -Name $Name -Location $Location -Slot $slot
	            (Write-ACLLogfile -Severity "info" `
                                    -LogEntry "AzureWebApp deployment slot created successfully (Name: $($Name), Slot: $($slot), Location: $($Location))." `
                                    -LogFilePathFileName $LogFilePathFileName `
                                    -ErrorFilePathFileName $ErrorFilePathFileName)
                Switch-AzureMode AzureResourceManager
            }

        }



    }
    else
    {
        $severity = ""
        switch($AlreadyExistAction.ToLower())
        {
            "info" {$severity = "info"}
            "warning" {$severity = "warning"}
            "error" {$severity = "error"}
            "throw" {$severity = "error"}
            default {$severity = "info"}
        }
	    (Write-ACLLogfile -Severity $severity `
                          -LogEntry "AzureWebApp already exists. (Name: $($Name), Location: $($Location))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
        if ($AlreadyExistAction.ToLower() -eq "throw")
        {
            throw "AzureWebApp already exists. (Name: $($Name), Location: $($Location))."
        }
    }
    
    Switch-AzureMode AzureServiceManagement

    return
}
