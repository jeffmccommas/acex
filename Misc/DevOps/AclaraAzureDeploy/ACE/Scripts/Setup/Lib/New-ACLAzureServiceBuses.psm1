
Function New-ACLAzureServiceBuses
{
    param($ConfigFileName, $Environment, $LogFileNameBase, $LogFileCategory)
    
    Set-StrictMode -Version Latest
    
    Import-Module $PSScriptRoot\..\..\Lib\Write-ACLLogfile.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLCommonPath.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLAzureSubscription.psm1
    Import-Module $PSScriptRoot\New-ACLAzureServiceBus.psm1
    
    # Retrieve script start date/time.
    $elapsedTime = [System.Diagnostics.Stopwatch]::StartNew()

    # Set service.
    $service = "Setup"
    
    # Retrieve log file path file name.
    $logFilePathFileName = (Get-ACLLogFilePathFileName -LogFileNameBase $LogFileNameBase -Environment $Environment -Service $service -Category $LogFileCategory)

    # Retrieve error file path file name.
    $errorFilePathFileName = (Get-ACLErrorFilePathFileName -LogFileNameBase $LogFileNameBase -Environment $Environment -Service $service -Category $LogFileCategory)

    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "*** Script $($MyInvocation.ScriptName) started. (ConfigFileName: $($ConfigFileName), Environment: $($Environment))" `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)

    # Retrieve config file path.
    $configFilePath = (Get-ACLConfigFilePath)

    # Retrieve config file path name.
    $configFilePathName = join-path -path $configFilePath -childpath $ConfigFileName

    (Write-Host "=====================================================")
    (Write-Host "Setup Azure service buses to $($Environment).")
    (Write-Host "=====================================================")

    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "Setup Azure ServiceBuses to $($Environment)." `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)

    # Get Azure subscription from configuration.
    $environmentConfig = Get-AzureSubscriptionFromConfig  $configFilePathName $Environment $logFilePathFileName $errorFilePathFileName
    
    # Get Azure subscription matching configuration.
    $azureSubscriptionValid = $false

    # Valid Azure subscription for environment in configuration.
    $azureSubscriptionValid = Test-ACLAzureSubscriptionCurrent $environmentConfig.SubscriptionID $environmentConfig.SubscriptionName
    if($azureSubscriptionValid)
    {
        (Write-Host "Azure subscription from configuration matches current Azure subscription in PowerShell session. (SubscriptionName: $($environmentConfig.SubscriptionName).")
        (Write-ACLLogfile -Severity "info" `
                          -LogEntry "Azure subscription from configuration matches current Azure subscription in PowerShell session. (SubscriptionName: $($environmentConfig.SubscriptionName)." `
                          -LogFilePathFileName $logFilePathFileName `
                          -ErrorFilePathFileName $errorFilePathFileName)
    }

    # Create Azure service buses from configuration.
    (Write-Host "+BEGIN: New-AzureServiceBusesFromConfig to $($Environment).")
	New-AzureServiceBusesFromConfig $configFilePathName $Environment $logFilePathFileName $errorFilePathFileName
    (Write-Host "-END: New-AzureServiceBusesFromConfig to $($Environment).")

    (Write-Host "`nSee log file for details: $logFilePathFileName`n")

    (Write-Host "=====================================================")
    (Write-Host "-End setup Azure service buses to $($Environment).")
    (Write-Host "=====================================================")

    (Write-Host "Elapsed time: $($elapsedTime.Elapsed.ToString())")

    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "Elapsed time: $($elapsedTime.Elapsed.ToString())" `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)

    # Determine whether error log file exits.    
    if((Get-ErrorLogFileExists -ErrorFilePathFileName $errorFilePathFileName) -eq $true)
    {
        Write-Warning -Message "Script $($MyInvocation.ScriptName) completed with error(s).`nSee error log file for details: $errorFilePathFileName`n"
    }
    
    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "*** Script $($MyInvocation.ScriptName) completed." `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)
    return
}

Function Get-AzureSubscriptionFromConfig
{
    param($ConfigFilePathName, $Environment, $LogFilePathFileName, $ErrorFilePathFileName)

    $configurationFound = $false

    [Xml]$configXml = Get-Content $ConfigFilePathName
    if (!$configXml) 
    { 
        throw "Error: Cannot find configuration in $ConfigFilePathName"
    }

    $query = "//Config/Setup/Environment[@Name='$Environment']"
    $environmentConfig = $configXml | Select-Xml $query | Select -Expand Node

    if(!$environmentConfig)
    {
        (Write-ACLLogfile -Severity "error" `
                          -LogEntry "No environment configuration found." `
                          -LogFilePathFileName $logFilePathFileName `
                          -ErrorFilePathFileName $errorFilePathFileName)
    }

    return $environmentConfig
}

Function New-AzureServiceBusesFromConfig
{
    param($ConfigFilePathName, $Environment, $LogFilePathFileName, $ErrorFilePathFileName)

    $configurationFound = $false

    [Xml]$configXml = Get-Content $ConfigFilePathName
    if (!$configXml) 
    { 
        throw "Error: Cannot find configuration in $ConfigFilePathName"
    }

    $query = "//Config/Setup/Environment[@Name='$Environment']/AzureServiceBuses"
    $azureServiceBusesConfig = $configXml | Select-Xml $query | Select -Expand Node

	foreach($azureServiceBusConfig in $azureServiceBusesConfig.AzureServiceBus)
	{

		New-ACLAzureServiceBusNamespace $azureServiceBusConfig.Skip `
                                        $azureServiceBusConfig.AlreadyExistAction `
                                        $azureServiceBusConfig.Name `
                                        $azureServiceBusConfig.Location `
                                        $azureServiceBusConfig.CreateACSNamespace `
                                        $azureServiceBusConfig.NamespaceType `
                                        $LogFilePathFileName `
                                        $ErrorFilePathFileName
        $configurationFound = $true

	}

    if($configurationFound = $false)
    {
        (Write-ACLLogfile -Severity "info" `
                          -LogEntry "No Azure service bus configuration found." `
                          -LogFilePathFileName $logFilePathFileName `
                          -ErrorFilePathFileName $errorFilePathFileName)
    }

    return
}
