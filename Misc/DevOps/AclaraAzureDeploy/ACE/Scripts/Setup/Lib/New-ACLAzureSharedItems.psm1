
Function New-ACLAzureSharedItems
{
    param($ConfigFileName, $Environment, $LogFileNameBase, $LogFileCategory)
    
    Set-StrictMode -Version Latest
    
    Import-Module $PSScriptRoot\..\..\Lib\Write-ACLLogfile.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLCommonPath.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLAzureSubscription.psm1
    Import-Module $PSScriptRoot\New-ACLAzureResourceGroup.psm1
    Import-Module $PSScriptRoot\New-ACLAzureRedisCache.psm1
    Import-Module $PSScriptRoot\New-ACLAzureStorageAccount.psm1
    
    # Retrieve script start date/time.
    $elapsedTime = [System.Diagnostics.Stopwatch]::StartNew()

    # Set service.
    $service = "Setup"
    
    # Retrieve log file path file name.
    $logFilePathFileName = (Get-ACLLogFilePathFileName -LogFileNameBase $LogFileNameBase -Environment $Environment -Service $service -Category $LogFileCategory)

    # Retrieve error file path file name.
    $errorFilePathFileName = (Get-ACLErrorFilePathFileName -LogFileNameBase $LogFileNameBase -Environment $Environment -Service $service -Category $LogFileCategory)

    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "*** Script $($MyInvocation.ScriptName) started. (ConfigFileName: $($ConfigFileName), Environment: $($Environment))" `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)

    # Retrieve config file path.
    $configFilePath = (Get-ACLConfigFilePath)

    # Retrieve config file path name.
    $configFilePathName = join-path -path $configFilePath -childpath $ConfigFileName

    (Write-Host "=====================================================")
    (Write-Host "Setup Azure shared items to $($Environment).")
    (Write-Host "=====================================================")

    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "Setup Azure shared items to $($Environment)." `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)

    # Get Azure subscription from configuration.
    $environmentConfig = Get-AzureSubscriptionFromConfig  $configFilePathName $Environment $logFilePathFileName $errorFilePathFileName
    
    # Get Azure subscription matching configuration.
    $azureSubscriptionValid = $false

    # Valid Azure subscription for environment in configuration.
    $azureSubscriptionValid = Test-ACLAzureSubscriptionCurrent $environmentConfig.SubscriptionID $environmentConfig.SubscriptionName
    if($azureSubscriptionValid)
    {
        (Write-Host "Azure subscription from configuration matches current Azure subscription in PowerShell session. (SubscriptionName: $($environmentConfig.SubscriptionName).")
        (Write-ACLLogfile -Severity "info" `
                          -LogEntry "Azure subscription from configuration matches current Azure subscription in PowerShell session. (SubscriptionName: $($environmentConfig.SubscriptionName)." `
                          -LogFilePathFileName $logFilePathFileName `
                          -ErrorFilePathFileName $errorFilePathFileName)
    }

    # Create Azure resource groups from configuration.
    (Write-Host "+BEGIN: New-ResourceGroupFromConfig to $($Environment).")
	New-ResourceGroupFromConfig $configFilePathName $Environment $logFilePathFileName $errorFilePathFileName
    (Write-Host "-END: New-ResourceGroupFromConfig to $($Environment).")

    # Create Azure storaage accounts from configuration.
    (Write-Host "+BEGIN: New-AzureStorageAccountFromConfig to $($Environment).")
    New-AzureStorageAccountFromConfig $configFilePathName $Environment $logFilePathFileName $errorFilePathFileName
    (Write-Host "-END: New-AzureStorageAccountFromConfig to $($Environment).")
    
    # Create Azure redis caches from configuration.
    (Write-Host "+BEGIN: New-AzureRedisCacheFromConfig to $($Environment).")
    New-AzureRedisCacheFromConfig $configFilePathName $Environment $logFilePathFileName $errorFilePathFileName
    (Write-Host "-END: New-AzureRedisCacheFromConfig to $($Environment).")

    (Write-Host "`nSee log file for details: $logFilePathFileName`n")

    (Write-Host "=====================================================")
    (Write-Host "-End setup Azure shared items to $($Environment).")
    (Write-Host "=====================================================")

    (Write-Host "Elapsed time: $($elapsedTime.Elapsed.ToString())")

    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "Elapsed time: $($elapsedTime.Elapsed.ToString())" `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)

    # Determine whether error log file exits.    
    if((Get-ErrorLogFileExists -ErrorFilePathFileName $errorFilePathFileName) -eq $true)
    {
        Write-Warning -Message "Script $($MyInvocation.ScriptName) completed with error(s).`nSee error log file for details: $errorFilePathFileName`n"
    }
    
    (Write-ACLLogfile -Severity "info" `
                      -LogEntry "*** Script $($MyInvocation.ScriptName) completed." `
                      -LogFilePathFileName $logFilePathFileName `
                      -ErrorFilePathFileName $errorFilePathFileName)
    return
}

Function Get-AzureSubscriptionFromConfig
{
    param($ConfigFilePathName, $Environment, $LogFilePathFileName, $ErrorFilePathFileName)

    $configurationFound = $false

    [Xml]$configXml = Get-Content $ConfigFilePathName
    if (!$configXml) 
    { 
        throw "Error: Cannot find configuration in $ConfigFilePathName"
    }

    $query = "//Config/Setup/Environment[@Name='$Environment']"
    $environmentConfig = $configXml | Select-Xml -XPath $query | Select-Object -ExpandProperty Node

    if(!$environmentConfig)
    {
        (Write-ACLLogfile -Severity "error" `
                          -LogEntry "No environment configuration found." `
                          -LogFilePathFileName $logFilePathFileName `
                          -ErrorFilePathFileName $errorFilePathFileName)
    }

    return $environmentConfig
}

Function New-ResourceGroupFromConfig
{
    param($ConfigFilePathName, $Environment, $LogFilePathFileName, $ErrorFilePathFileName)

    $configurationFound = $false

    [Xml]$configXml = Get-Content $ConfigFilePathName
    if (!$configXml) 
    { 
        throw "Error: Cannot find configuration in $ConfigFilePathName"
    }

    $query = "//Config/Setup/Environment[@Name='$Environment']/AzureResourceGroups/AzureResourceGroup"
    $azureResourceGroupsConfig = $configXml | Select-Xml -XPath $query | Select-Object -ExpandProperty Node

	foreach($azureResourceGroupConfig in $azureResourceGroupsConfig)
	{

		New-ACLAzureResourceGroup $azureResourceGroupConfig.Skip `
                                  $azureResourceGroupConfig.AlreadyExistAction `
                                  $azureResourceGroupConfig.Name `
                                  $azureResourceGroupConfig.Location `
                                  $LogFilePathFileName `
                                  $ErrorFilePathFileName
        $configurationFound = $true

	}

    if($configurationFound = $false)
    {
        (Write-ACLLogfile -Severity "info" `
                          -LogEntry "No Azure resource group configuration found." `
                          -LogFilePathFileName $logFilePathFileName `
                          -ErrorFilePathFileName $errorFilePathFileName)
    }

    return
}

Function New-AzureRedisCacheFromConfig
{
    param($ConfigFilePathName, $Environment, $LogFilePathFileName, $ErrorFilePathFileName)
    
    $configurationFound = $false

    [Xml]$configXml = Get-Content $ConfigFilePathName
    if (!$configXml) 
    { 
        throw "Error: Cannot find configuration in $ConfigFilePathName"
    }

    $query = "//Config/Setup/Environment[@Name='$Environment']/AzureRedisCaches/AzureRedisCache"
    $azureRedisCachesConfig = $configXml | Select-Xml -XPath $query | Select-Object -ExpandProperty Node

	foreach($azureRedisCacheConfig in $azureRedisCachesConfig)
	{

		New-ACLAzureRedisCache $azureRedisCacheConfig.Skip `
                               $azureRedisCacheConfig.AlreadyExistAction `
                               $azureRedisCacheConfig.Name `
                               $azureRedisCacheConfig.Location `
                               $azureRedisCacheConfig.ResourceGroupName `
                               $azureRedisCacheConfig.Size `
                               $azureRedisCacheConfig.Sku `
                               $LogFilePathFileName `
                               $ErrorFilePathFileName

        $configurationFound = $true

	}

    if($configurationFound = $false)
    {
        (Write-ACLLogfile -Severity "info" `
                          -LogEntry "No Azure redis cache configuration found." `
                          -LogFilePathFileName $logFilePathFileName `
                          -ErrorFilePathFileName $errorFilePathFileName)
    }

    return
}

Function New-AzureStorageAccountFromConfig
{
    param($ConfigFilePathName, $Environment, $LogFilePathFileName, $ErrorFilePathFileName)
    
    $configurationFound = $false

    [Xml]$configXml = Get-Content $ConfigFilePathName
    if (!$configXml) 
    { 
        throw "Error: Cannot find configuration in $ConfigFilePathName"
    }

    $query = "//Config/Setup/Environment[@Name='$Environment']/AzureStorageAccounts/AzureStorageAccount"
    $azureStorageAccountsConfig = $configXml | Select-Xml -XPath $query | Select-Object -ExpandProperty Node

	foreach($azureStorageAccountConfig in $azureStorageAccountsConfig)
	{

		New-ACLAzureStorageAccount $azureStorageAccountConfig.Skip `
                                   $azureStorageAccountConfig.AlreadyExistAction `
                                   $azureStorageAccountConfig.Name `
                                   $azureStorageAccountConfig.Location `
                                   $azureStorageAccountConfig.ResourceGroupName `
                                   $azureStorageAccountConfig.Type `
                                   $LogFilePathFileName `
                                   $ErrorFilePathFileName

        $configurationFound = $true

	}

    if($configurationFound = $false)
    {
        (Write-ACLLogfile -Severity "info" `
                          -LogEntry "No Azure storage account configuration found." `
                          -LogFilePathFileName $logFilePathFileName `
                          -ErrorFilePathFileName $errorFilePathFileName)
    }

    return
}