Function New-ACLAzureStorageAccount
{
    param($Skip, $AlreadyExistAction, $Name, $Location, $ResourceGroupName, $Type, $LogFilePathFileName, $ErrorFilePathFileName)
    
    Set-StrictMode -Version Latest

    Import-Module $PSScriptRoot\..\..\Lib\Write-ACLLogfile.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLCommonPath.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLAzureItemInfo.psm1

    if ($Skip.ToLower() -eq "true")
    {
    	(Write-ACLLogfile -Severity "info" `
                          -LogEntry "Skip AzureStorageAccount creation. (Name: $($Name), Location: $($Location))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
        (Write-Host "Skip AzureStorageAccount creation. (Name: $($Name), Location: $($Location)).")
        return
    }

    Switch-AzureMode AzureServiceManagement
    $azureStorageAccountExist = Test-ACLAzureStorageAccountExist $Name 
    Switch-AzureMode AzureResourceManager

    if (!$azureStorageAccountExist)
    {

	     $azureStorageAccount = New-AzureStorageAccount -StorageAccountName $Name -ResourceGroupName $ResourceGroupName -Location $Location -Type $Type

	    if ( $azureStorageAccount)
	    {
	        (Write-ACLLogfile -Severity "info" `
                              -LogEntry "AzureStorageAccount created successfully (Name: $($Name), Location: $($Location))." `
                              -LogFilePathFileName $LogFilePathFileName `
                              -ErrorFilePathFileName $ErrorFilePathFileName)
	    }
	    else
	    {
	        (Write-ACLLogfile -Severity "error" `
                              -LogEntry "Error: New-AzureStorageAccount failed to create AzureRedisCache (Name: $($Name), Location: $($Location))." `
                              -LogFilePathFileName $LogFilePathFileName `
                              -ErrorFilePathFileName $ErrorFilePathFileName)
	    }

    }
    else
    {

        $severity = ""
        switch($AlreadyExistAction.ToLower())
        {
            "info" {$severity = "info"}
            "warning" {$severity = "warning"}
            "error" {$severity = "error"}
            "throw" {$severity = "error"}
            default {$severity = "info"}
        }
	    (Write-ACLLogfile -Severity $severity `
                          -LogEntry "AzureStorageAccount already exists. (Name: $($Name), Location: $($Location))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
        if ($AlreadyExistAction.ToLower() -eq "throw")
        {
            throw "AzureStorageAccount already exists. (Name: $($Name), Location: $($Location))."
        }

    }
    
    Switch-AzureMode AzureServiceManagement

    return
}
