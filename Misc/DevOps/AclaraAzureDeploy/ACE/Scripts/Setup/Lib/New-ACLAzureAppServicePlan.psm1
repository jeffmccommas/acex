Function New-ACLAzureAppServicePlan
{
    param($Skip, $AlreadyExistAction, $Name, $Location, $ResourceGroupName, $ResourceType, $Sku, $WorkerSize, $NumberOfWorkers, $LogFilePathFileName, $ErrorFilePathFileName)
    
    Set-StrictMode -Version Latest

    Import-Module $PSScriptRoot\..\..\Lib\Write-ACLLogfile.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLCommonPath.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLAzureItemInfo.psm1

    if ($Skip.ToLower() -eq "true")
    {
    	(Write-ACLLogfile -Severity "info" `
                        -LogEntry "Skip AzureAppServicePlan creation. (Name: $($Name), Location: $($Location))." `
                        -LogFilePathFileName $LogFilePathFileName `
                        -ErrorFilePathFileName $ErrorFilePathFileName)
        (Write-Host "Skip AzureAppServicePlan creation. (Name: $($Name), Location: $($Location)).")
        return
    }

    $azureAppServicePlanExist = $false

    Switch-AzureMode AzureResourceManager

    $azureAppServicePlanExist = Test-ACLAzureAppServicePlanExist $Name $ResourceGroupName $ResourceType

    if (!$azureAppServicePlanExist)
    {

        $propertyOpbject = @{"name"= $Name;"sku"= $Sku;"workerSize"= $WorkerSize;"numberOfWorkers"= $NumberOfWorkers}

        $azureAppServicePlan = New-AzureResource -ApiVersion 2014-04-01 -Name $Name -ResourceGroupName $ResourceGroupName -ResourceType $ResourceType -Location $Location -PropertyObject $propertyOpbject -OutputObjectFormat New -Force

	    if ($azureAppServicePlan)
	    {
	        (Write-ACLLogfile -Severity "info" `
                              -LogEntry "AzureAppServicePlan created successfully (Name: $($Name), Location: $($Location))." `
                              -LogFilePathFileName $LogFilePathFileName `
                              -ErrorFilePathFileName $ErrorFilePathFileName)
	    }
	    else
	    {
	        (Write-ACLLogfile -Severity "error" `
                              -LogEntry "Error: New-AzureResource failed to create AzureAppServicePlan (Name: $($Name), Location: $($Location))." `
                              -LogFilePathFileName $LogFilePathFileName `
                              -ErrorFilePathFileName $ErrorFilePathFileName)
	    }
    }
    else
    {

        $severity = ""
        switch($AlreadyExistAction.ToLower())
        {
            "info" {$severity = "info"}
            "warning" {$severity = "warning"}
            "error" {$severity = "error"}
            "throw" {$severity = "error"}
            default {$severity = "info"}
        }
	    (Write-ACLLogfile -Severity $severity `
                          -LogEntry "AzureAppServicePlan already exists. (Name: $($Name), Location: $($Location))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
        if ($AlreadyExistAction.ToLower() -eq "throw")
        {
            throw "AzureAppServicePlan already exists. (Name: $($Name), Location: $($Location))."
        }

    }
    
    Switch-AzureMode AzureServiceManagement

    return
}
