Function New-ACLAzureSqlDatabase
{
    param($Skip, $AlreadyExistAction, $Name, $ServerName, $Edition, $MaxSizeGB, $Collation, $LogFilePathFileName, $ErrorFilePathFileName)

    Set-StrictMode -Version Latest
    
    Import-Module $PSScriptRoot\..\..\Lib\Write-ACLLogfile.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLCommonPath.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLAzureItemInfo.psm1

    if ($Skip.ToLower() -eq "true")
    {
    	(Write-ACLLogfile -Severity "info" `
                          -LogEntry "Skip AzureSqlDatabase creation. (Name: $($Name), ServerName: $($ServerName))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
        (Write-Host "Skip AzureSqlDatabase creation. (Name: $($Name), ServerName: $($ServerName)).")
        return
    }

    Switch-AzureMode AzureServiceManagement
    $azureSqlDatabaseExist = Test-ACLAzureSqlDatabaseExist $Name $ServerName

    if (!$azureSqlDatabaseExist)
    {

        $azureSqlDatabase = New-AzureSqlDatabase -DatabaseName $Name -ServerName $ServerName -Edition $Edition -MaxSizeGB $MaxSizeGB -Collation $Collation

	    if ($azureSqlDatabase)
	    {
	        (Write-ACLLogfile -Severity "info" `
                              -LogEntry "AzureSqlDatabase created successfully (Name: $($Name), ServerName: $($ServerName))." `
                              -LogFilePathFileName $LogFilePathFileName `
                              -ErrorFilePathFileName $ErrorFilePathFileName)
	    }
	    else
	    {
	        (Write-ACLLogfile -Severity "error" `
                              -LogEntry "Error: New-AzureSqlDatabase failed to create AzureSqlDatabase (Name: $($Name), ServerName: $($ServerName))." `
                              -LogFilePathFileName $LogFilePathFileName `
                              -ErrorFilePathFileName $ErrorFilePathFileName)
	    }
    }
    else
    {
        $severity = ""
        switch($AlreadyExistAction.ToLower())
        {
            "info" {$severity = "info"}
            "warning" {$severity = "warning"}
            "error" {$severity = "error"}
            "throw" {$severity = "error"}
            default {$severity = "info"}
        }
	    (Write-ACLLogfile -Severity $severity `
                          -LogEntry "AzureSqlDatabase already exists. (Name: $($Name), ServerName: $($ServerName))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
        if ($AlreadyExistAction.ToLower() -eq "throw")
        {
            throw "AzureSqlDatabase already exists. (Name: $($Name), ServerName: $($ServerName))."
        }
    }
    
    Switch-AzureMode AzureServiceManagement

    return
}
