Function New-ACLAzureServiceBusNamespace
{
    param($Skip, $AlreadyExistAction, $Name, $Location, $CreateACSNamespace, $NamespaceType, $LogFilePathFileName, $ErrorFilePathFileName)

    Set-StrictMode -Version Latest
    
    Import-Module $PSScriptRoot\..\..\Lib\Write-ACLLogfile.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLCommonPath.psm1
    Import-Module $PSScriptRoot\..\..\Lib\Get-ACLAzureItemInfo.psm1

    if ($Skip.ToLower() -eq "true")
    {
    	(Write-ACLLogfile -Severity "info" `
                          -LogEntry "Skip AzureServiceBusNamespace creation. (Name: $($Name), Location: $($Location))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
        (Write-Host "Skip AzureServiceBusNamespace creation. (Name: $($Name), Location: $($Location)).")
        return
    }

    Switch-AzureMode AzureServiceManagement
    $azureServiceBusNamespaceExist = Test-ACLAzureServiceBusNamespaceExist $Name 

    if (!$azureServiceBusNamespaceExist)
    {

        $createACSNamespaceAsBoolean = $false
        try { $createACSNamespaceAsBoolean = [System.Convert]::ToBoolean($CreateACSNamespace)} 
        catch [FormatException] { $createACSNamespaceAsBoolean = $false }

	    $azureServiceBusNamespace = New-AzureSBNamespace -Name $Name -Location $Location -CreateACSNamespace $createACSNamespaceAsBoolean -NamespaceType $NamespaceType

	    if ($azureServiceBusNamespace)
	    {
	        (Write-ACLLogfile -Severity "info" `
                              -LogEntry "AzureServiceBusNamespace created successfully (Name: $($Name), Location: $($Location))." `
                              -LogFilePathFileName $LogFilePathFileName `
                              -ErrorFilePathFileName $ErrorFilePathFileName)
	    }
	    else
	    {
	        (Write-ACLLogfile -Severity "error" `
                              -LogEntry "Error: New-AzureServiceBusNamespace failed to create AzureServiceBusNamespace (Name: $($Name), Location: $($Location))." `
                              -LogFilePathFileName $LogFilePathFileName `
                              -ErrorFilePathFileName $ErrorFilePathFileName)
	    }
    }
    else
    {
        $severity = ""
        switch($AlreadyExistAction.ToLower())
        {
            "info" {$severity = "info"}
            "warning" {$severity = "warning"}
            "error" {$severity = "error"}
            "throw" {$severity = "error"}
            default {$severity = "info"}
        }
	    (Write-ACLLogfile -Severity $severity `
                          -LogEntry "AzureServiceBusNamespace already exists. (Name: $($Name), Location: $($Location))." `
                          -LogFilePathFileName $LogFilePathFileName `
                          -ErrorFilePathFileName $ErrorFilePathFileName)
        if ($AlreadyExistAction.ToLower() -eq "throw")
        {
            throw "AzureServiceBusNamespace already exists. (Name: $($Name), Location: $($Location))."
        }
    }
    
    Switch-AzureMode AzureServiceManagement

    return
}
