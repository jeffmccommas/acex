﻿using Newtonsoft.Json;

namespace CE.ReportPortal.ConsoleApp.Models
{
    public class BillingUsage
    {
        [JsonProperty(PropertyName = "renders")]
        public int Renders { get; set; }
    }
}
