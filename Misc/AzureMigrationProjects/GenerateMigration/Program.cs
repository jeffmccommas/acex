﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Configuration;
using System.Collections.ObjectModel;

using Microsoft.Azure.Management.DataFactories;
using Microsoft.Azure.Management.DataFactories.Models;
using Microsoft.Azure.Management.DataFactories.Common.Models;

using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Azure;
using System.Collections;


namespace GenerateMigration
{
    class Program
    {
        public class TableItem
        {
            public int DWGroup;
            public string Tablename;
            public int joinit;
            public string database;
        }
        static void Main(string[] args)
        {
            // There is a maximum of 100 datasets per datafactory
            // The processing loop generates a OnPrem and a PaaS for each table
            // Never do more than 50 sql tables in a group which = 100 datasets
            //
            DataFactoryManagementClient client;
            GetDevAdfClient(out client);
            //
            string environment = "QA";
            int groupToProcess=1;
            //processByswitch 1 = by client         requires clientId
            //processByswitch 2 = specific table    requires tableNametoProcess
            //processByswitch 3 = delete all datasets for a client
            //processByswitch 4 = Meta data
            //processByswitch 5 = Just Create data Factories
            //processByswitch 6 = does the data Factories and then processes by client
            string processByswitch = "6";
            //
            string resourceGroupName = "GroupAceDQU";            
            string clientId = "Meta";
            string tableNametoProcess = "dbo.FactServicePointBilling";

           // string tableNametoProcess = string.Empty;

            // trying to check for fix
            //commented out line below
            string dataFactoryName = environment + "MigrateOnPremToPaaS-Part";
            //string dataFactoryName = environment + "MigrateOnPremToPaaS";
            //
            // one is by client
            if (processByswitch == "1")
            {
                ProcessByClient(client, resourceGroupName, clientId, dataFactoryName, processByswitch);
            }
            //2 is by table
            if (processByswitch == "2")
            {
                ProcessSpecificTable(client, resourceGroupName, clientId, dataFactoryName, tableNametoProcess, processByswitch);
                
            }
            if (processByswitch == "3")
            {
                ProcessByClient(client, resourceGroupName, clientId, dataFactoryName, processByswitch);

            }
            if (processByswitch == "4")
            {
                ProcessSpecificTable(client, resourceGroupName, "Meta", dataFactoryName, tableNametoProcess, processByswitch);

            }
            if (processByswitch == "5")
            {
                CreateDataFactoriesForAClientID(client, resourceGroupName, clientId, dataFactoryName);
                

            }
            if (processByswitch == "6")
            {
                CreateDataFactoriesForAClientID(client, resourceGroupName, clientId, dataFactoryName);
                ProcessByClient(client, resourceGroupName, clientId, dataFactoryName, processByswitch);


            }

            Console.WriteLine("Hit enter to end");
            Console.ReadLine();
        }

        private static void ProcessSpecificTable(DataFactoryManagementClient client, string resourceGroupName, string clientId, string dataFactoryName, string tableNametoProcess,string  processByswitch)
        {
            ArrayList DW_TableList = LoadTableToList(tableNametoProcess);
            var mytableitem = (TableItem)DW_TableList[0];
            int groupToProcess = mytableitem.DWGroup;
            // trying to check for fix
            //commented out line below
            //dataFactoryName = dataFactoryName + groupToProcess.ToString() + '-' + "224";
            dataFactoryName = dataFactoryName + groupToProcess.ToString() + '-' + clientId.ToString();
            //
            //dataFactoryName = dataFactoryName + groupToProcess.ToString() + '-' + clientId;
            Console.WriteLine("Creating input tables");
            PerformProcessingLoop(clientId, resourceGroupName, dataFactoryName, client, groupToProcess, DW_TableList, processByswitch);

        }
        private static void ProcessByClient(DataFactoryManagementClient client, string resourceGroupName, string clientId, string dataFactoryName, string processByswitch)
        {

            ArrayList DW_TableList = LoadList(clientId);
            
            Console.WriteLine("Creating input tables");

            int groupToProcess;
            if (clientId == "101" || clientId == "224" || clientId == "276" || clientId == "Test" || clientId == "All")
            {
                groupToProcess = 1;
                ProcessGroupForClient(client, resourceGroupName, clientId, dataFactoryName, groupToProcess,  DW_TableList, processByswitch);
                //
                groupToProcess = 2;
                ProcessGroupForClient(client, resourceGroupName, clientId, dataFactoryName, groupToProcess, DW_TableList, processByswitch);
                //
                groupToProcess = 3;
                ProcessGroupForClient(client, resourceGroupName, clientId, dataFactoryName, groupToProcess,  DW_TableList, processByswitch);
                //
                groupToProcess = 4;
                ProcessGroupForClient(client, resourceGroupName, clientId, dataFactoryName, groupToProcess, DW_TableList, processByswitch);
            }
            //        
            if (clientId == "Meta" || clientId == "All")
            {

                groupToProcess = 5;
                ProcessGroupForClient(client, resourceGroupName, clientId, dataFactoryName, groupToProcess, DW_TableList, processByswitch);
                //
                groupToProcess = 6;
                ProcessGroupForClient(client, resourceGroupName, clientId, dataFactoryName, groupToProcess, DW_TableList, processByswitch);
                //
                groupToProcess = 7;
                ProcessGroupForClient(client, resourceGroupName, clientId, dataFactoryName, groupToProcess, DW_TableList, processByswitch);
            }
        
        }

        private static void CreateDatFactory(DataFactoryManagementClient client, string resourceGroupName, string clientId, string dataFactoryName)
        {
            //create the factory
            Console.WriteLine("Creating a data factory " + dataFactoryName.ToString());
            client.DataFactories.CreateOrUpdate(resourceGroupName,
                 new DataFactoryCreateOrUpdateParameters()
                {
                    DataFactory = new DataFactory()
                    {
                        Name = dataFactoryName,
                        Location = "westus",
                        Properties = new DataFactoryProperties() { }
                    }
                }
                );

        }
        private static void CreateDatFactoryLinkedServices(DataFactoryManagementClient client, string resourceGroupName, string clientId, string dataFactoryName)
        {
            // create a linked services
            //Insightsdw, InsightsBulk and  1 shard, or possibly InsightsMetadata
            //datfactory names are like: DevMigrateOnPremToPaaS-Part1-224 
            //
            Console.WriteLine("Creating a linked service to OnPrem InsightsBulk");
            client.LinkedServices.CreateOrUpdate(resourceGroupName, dataFactoryName,
                new LinkedServiceCreateOrUpdateParameters()
                {
                    LinkedService = new LinkedService()
                    {
                        Name = "QAInsightsBulk",
                        Properties = new LinkedServiceProperties(
                            new OnPremisesSqlServerLinkedService()
                            {
                                ConnectionString = "data source=azuQsql001\\Bi1;initial catalog=InsightsBulk;Integrated Security=False;EncryptedCredential=eyJDb25uZWN0aW9uU3RyaW5nIjoiZGF0YSBzb3VyY2U9YXp1UXNxbDAwMVxcQmkxO2luaXRpYWwgY2F0YWxvZz1JbnNpZ2h0c0J1bGs7SW50ZWdyYXRlZCBTZWN1cml0eT1GYWxzZSIsIkVuY3J5cHRlZENyZWRlbnRpYWwiOiJ7XCJDbGFzc1R5cGVcIjpcIk1pY3Jvc29mdC5EYXRhUHJveHkuQ29yZS5JbnRlclNlcnZpY2VEYXRhQ29udHJhY3QuQ3JlZGVudGlhbFNVMDZDWTE0XCIsXCJEYXRhU291cmNlU2V0dGluZ3NcIjpcIntcXFwiQ2xhc3NUeXBlXFxcIjpcXFwiTWljcm9zb2Z0LkRhdGFQcm94eS5Db3JlLkludGVyU2VydmljZURhdGFDb250cmFjdC5EYXRhU291cmNlU2V0dGluZ3NEYXRhVjFcXFwiLFxcXCJDcmVkZW50aWFsVHlwZVxcXCI6MSxcXFwiRW5jcnlwdGVkQ29ubmVjdGlvblxcXCI6MSxcXFwiUHJpdmFjeVxcXCI6M31cIixcIkVuY3J5cHRlZEJsb2JcIjpcIntcXFwiQ2xhc3NUeXBlXFxcIjpcXFwiTWljcm9zb2Z0LkRhdGFQcm94eS5Db3JlLkludGVyU2VydmljZURhdGFDb250cmFjdC5DbGlja09uY2VDcmVkZW50aWFsRGF0YVYyXFxcIixcXFwiRW5jcnlwdGVkQ3JlZGVudGlhbFxcXCI6XFxcIk1JSUM0UVlKS29aSWh2Y05BUWNEb0lJQzBqQ0NBczRDQVFBeGdnRldNSUlCVWdJQkFEQTZNQ1l4SkRBaUJnTlZCQU1URzBGYVZWRlRVVXd3TURFdWMzUm5MbUZqYkdGeVlXTmxMbU52YlFJUVMyK2lXMEI2bmE1QmdYUjNXWElYMlRBTkJna3Foa2lHOXcwQkFRRUZBQVNDQVFBQVZjN1h5TmZ2bHQ5SlRGajBlYkUrQzB0MWRFRWY5bnBObE9BdjF1QVlDMDlSWm9uQmF2UEVPVkxHemNuY0IycWorXFxcXFxcL2tQSEgrSGV4YXpxZnBYN1Y3QUxKRjU5am9rbmE1Qk1jMjBMN2JtSWZ1SzMyazlLN1B4XFxcXFxcL0Y5Nko2eDk3dll1enBJK2V2ZFdRRVZcXFxcXFwvTHlwamZmRVJBendOU3hqcUg3ZXZDMnBXckhlWERKaWoxXFxcXFxcL3NXTkJlMjlKa0h1SnRWMFhkdk11UUpxRjFaSWRoaDMyZWRHN1hIbTNtU0pXek1qYmRwQk8xMXVyY3dsdEtWM1A5QjB3ZUk0dWlpQUw1cTFDaytCYTE4Qlc1ZmowUVxcXFxcXC9XanpaN0k1THhyQUQ0OENjcFptSzlONnppeWhLZWtrQW9UVHBoWU1kcDBcXFxcXFwvRG5XYTZXanBLMjVkaGZvY3lkNU9VbnUxUStodlZNSUlCYlFZSktvWklodmNOQVFjQk1CUUdDQ3FHU0liM0RRTUhCQWdoSVJ3WTZtWmE5b0NDQVVpemprdEI1NlBidEVcXFxcXFwvWGlVRisxQVxcXFxcXC9hV1o2Tm5BVG9CSGxaNzRtZElmeHlWTTZtWGFRMkN2T1VES0FZbllrTWVFVGxlR3RyNCs3V0tOdFZqZVhZYUhTbWplZkxmN2pZczRjXFxcXFxcL3loclJZMGJoRlBTMVlIVlg5NUlncDdTdGszOEFSZmFvajBOSmp1QUtVblRKOVxcXFxcXC9jVWtnazIyR29NRkVkQU9WNDFKWlZKUGZUSm9DanF5NmQreVBYaE1PcU5QKzhaaGNGSzJqbjdwQmt6S0xBV2xCODNrNUs0T1ZEd1M1UUQyd0c2WmNCcWxGcHI2clxcXFxcXC8yMStXMHdYM1xcXFxcXC95TVhvUDVzUEFEVEJ2Sk02XFxcXFxcL0Q1ZVRRKzJMXFxcXFxcL3BWUHJpUjh5R3NRWWIwQWwrQjY2SDU4UlVBbUltaldTdlRld0E0ZzZVU2ZRVXFTaG13dVVYTmk3QkFuR1VcXFxcXFwvWVA1bGJVTmYyZ1NpcUUrdHJKNW91M3JPek10SFJFK3JGNW5OQ3dvcnlpeGtiTzdGcnA5VXJtUjdlVlJkQ1BNWlJ1S3NLNG5hc3Q3bWpERDRLeU52QjZPaGdzWnRBZk4wNUZBUFg4V3dcXFwiLFxcXCJFbmNyeXB0aW9uQ2VydGlmaWNhdGVUaHVtYnByaW50XFxcIjpcXFwiMDM5Mzc5NTVFOUI3MjhFMkEyOEYxQUFDQjMyOEVGRkQ1REMyRTkzNFxcXCIsXFxcIlNpZ25hdHVyZVxcXCI6XFxcIk1JSUJuUVlKS29aSWh2Y05BUWNDb0lJQmpqQ0NBWW9DQVFFeER6QU5CZ2xnaGtnQlpRTUVBZ0VGQURBTEJna3Foa2lHOXcwQkJ3RXhnZ0ZsTUlJQllRSUJBVEE2TUNZeEpEQWlCZ05WQkFNVEcwRmFWVkZUVVV3d01ERXVjM1JuTG1GamJHRnlZV05sTG1OdmJRSVFTMitpVzBCNm5hNUJnWFIzV1hJWDJUQU5CZ2xnaGtnQlpRTUVBZ0VGQURBTkJna3Foa2lHOXcwQkFRRUZBQVNDQVFCY2hJeTF4a1gydGNtXFxcXFxcL0ZjRnM0WkZUbVhKeVNVSHBRMmlzcnZSVU9NR2h0dFdpSHpBZFNEbUpRRXhpNk5QVXpiNmtJYUZveHRSVzcyT01kNjBybXNZU1kyY3BGVklWakN2d2Vwa2p1YUZkSTBLaWJBMGdlakV3Z0ZTVjhpTWJnKzA5RjdkaXpzc1RIYXhKMHE2eGpyUzJ5OTMxRWtINlRRT05XRE1FdTg0UWMrcUdkWGRjWE1CRVQyRWJDYXptVWdySG9QWXZUcGZJTlhTRk1yZXFGbTBZdmZEelFXYzZ2RzBtRmVyVVVWXFxcXFxcL1BrMW9nU1ZheWVDT0prT1h6ZXJOb3lLbmMwTllwYUdPeE5UWlBiWDIrOEtiV3MxUG9hTHFYMnBXbCs3SnMyZEo0SkRhcGpoZHJMeG9TcEpBblhveW4wbm5Ia1NqblhpQktzcGdUbWdiMU94WWVcXFwifVwifSJ9",
                                GatewayName = "QA" + "MigrateOnPremToPaaS",
                                UserName = "",
                                Password = "Acl@r@694"
                            }
                            ) { Description = "InsightsBulk" }
                    }
                }
            );
            //
            Console.WriteLine("Creating a linked service to OnPrem InsightsDW");
            client.LinkedServices.CreateOrUpdate(resourceGroupName, dataFactoryName,
                new LinkedServiceCreateOrUpdateParameters()
                {
                    LinkedService = new LinkedService()
                    {
                        Name = "QAInsightsDW",
                        Properties = new LinkedServiceProperties(
                            new OnPremisesSqlServerLinkedService()
                            {
                                        ConnectionString = "data source=azuQsql001\\Bi1;initial catalog=InsightsDW;Integrated Security=False;EncryptedCredential=eyJDb25uZWN0aW9uU3RyaW5nIjoiZGF0YSBzb3VyY2U9YXp1UXNxbDAwMVxcQmkxO2luaXRpYWwgY2F0YWxvZz1JbnNpZ2h0c0RXO0ludGVncmF0ZWQgU2VjdXJpdHk9RmFsc2UiLCJFbmNyeXB0ZWRDcmVkZW50aWFsIjoie1wiQ2xhc3NUeXBlXCI6XCJNaWNyb3NvZnQuRGF0YVByb3h5LkNvcmUuSW50ZXJTZXJ2aWNlRGF0YUNvbnRyYWN0LkNyZWRlbnRpYWxTVTA2Q1kxNFwiLFwiRGF0YVNvdXJjZVNldHRpbmdzXCI6XCJ7XFxcIkNsYXNzVHlwZVxcXCI6XFxcIk1pY3Jvc29mdC5EYXRhUHJveHkuQ29yZS5JbnRlclNlcnZpY2VEYXRhQ29udHJhY3QuRGF0YVNvdXJjZVNldHRpbmdzRGF0YVYxXFxcIixcXFwiQ3JlZGVudGlhbFR5cGVcXFwiOjEsXFxcIkVuY3J5cHRlZENvbm5lY3Rpb25cXFwiOjEsXFxcIlByaXZhY3lcXFwiOjN9XCIsXCJFbmNyeXB0ZWRCbG9iXCI6XCJ7XFxcIkNsYXNzVHlwZVxcXCI6XFxcIk1pY3Jvc29mdC5EYXRhUHJveHkuQ29yZS5JbnRlclNlcnZpY2VEYXRhQ29udHJhY3QuQ2xpY2tPbmNlQ3JlZGVudGlhbERhdGFWMlxcXCIsXFxcIkVuY3J5cHRlZENyZWRlbnRpYWxcXFwiOlxcXCJNSUlDNFFZSktvWklodmNOQVFjRG9JSUMwakNDQXM0Q0FRQXhnZ0ZXTUlJQlVnSUJBREE2TUNZeEpEQWlCZ05WQkFNVEcwRmFWVkZUVVV3d01ERXVjM1JuTG1GamJHRnlZV05sTG1OdmJRSVFTMitpVzBCNm5hNUJnWFIzV1hJWDJUQU5CZ2txaGtpRzl3MEJBUUVGQUFTQ0FRQmZZUUFYajdzVklSdDk3Z1BrNE9tSXJEbkdHUnBBbktVWWFtdlBHaXZMalNzM00wUkllaXc0VDVjYXpkUDJ1aFFqU2hhdFBQQlVkNkUybXRCUXZYZUxwa0IrdWZxU2lLa3ltOExvcDVVUjdjbWRTNU1IaGNucjlsMXRka08yd21OUWxFVjcwTmJkVzdkaTk5NHBjYTFHNGJXSTU5M1htTFZBbXYzODJLS0xNa3R6QzBsYzkzOGJoaE5laE9VSWZvY3ppazEyejgyNnMyMkhuWklReHplcVF3RTBvaEpXMFI0WENpeE9xNU9cXFxcXFwvVGhaMlBreWVsc0d3YmlCQzdaXFxcXFxcL09CZEtsYkhvZFFvTWZaVzZcXFxcXFwvYlNGZmxFVm80anNIQXQ4K2dDa0lBd0pUK21aWFRaSThwWnF4aENnaGhFQmVLSHBPTjVUbFVVbWlmVjdBcWZzYThEZklnaVhoTUlJQmJRWUpLb1pJaHZjTkFRY0JNQlFHQ0NxR1NJYjNEUU1IQkFoV0NLMFMyNEk4TklDQ0FVZ2taM3o2cWNzbDE2bUlNMXZWOEFmQnNjdlZmYWZXY29VMGdQXFxcXFxcL1FvZkMyR1NGUkZUMklzTFdWSFRvVkU2a0NOaFU0Q0hzTTF3THFHZ28zbUpVczl2OTdjV1xcXFxcXC9jZGNsNXlBQ3NPSFJlcHVcXFxcXFwvZUpKMUljT2RcXFxcXFwvZmFHVnJ6cENnajlwaUtPcldLRXdEMXcxMCtyRmJNVUFVSjlvVVVQbENlelBnUk93d29GWUFLc2U3SmU0YUYwbzBZaTdSVkprQ3FGNGdMemRyM0hVVTc0amFtamlCM21MY0hMYmM3TGprWU8zS3hjc05mVmVaNjBQUWxRZThHNlZJeFM2eEI0b29hSWZMdVl5K0M3eDhRdjNKd1JaNjFNTXFBSFpZU1haKzJwcVNZck11K0lLR2w1UmNOcTBVXFxcXFxcL2dEZ3Uyak43cmY3cDZEaGdmdHRLb2xuZGtlNTFjZlFESTdXUFV6aG9kd1g1VElMT1dMTHFxaXJ5UVVkcU5cXFxcXFwvRG80dDhFMHRWb21Nc0ttaytXVzBqWHREVE1CRHVpV2VhUjF0Z1NHR1l5ejBIXFxcXFxcL2RoMUZIVkJ2TDhNWDk3endPbENpQWYySldSXFxcIixcXFwiRW5jcnlwdGlvbkNlcnRpZmljYXRlVGh1bWJwcmludFxcXCI6XFxcIjAzOTM3OTU1RTlCNzI4RTJBMjhGMUFBQ0IzMjhFRkZENURDMkU5MzRcXFwiLFxcXCJTaWduYXR1cmVcXFwiOlxcXCJNSUlCblFZSktvWklodmNOQVFjQ29JSUJqakNDQVlvQ0FRRXhEekFOQmdsZ2hrZ0JaUU1FQWdFRkFEQUxCZ2txaGtpRzl3MEJCd0V4Z2dGbE1JSUJZUUlCQVRBNk1DWXhKREFpQmdOVkJBTVRHMEZhVlZGVFVVd3dNREV1YzNSbkxtRmpiR0Z5WVdObExtTnZiUUlRUzIraVcwQjZuYTVCZ1hSM1dYSVgyVEFOQmdsZ2hrZ0JaUU1FQWdFRkFEQU5CZ2txaGtpRzl3MEJBUUVGQUFTQ0FRQjBDZDlTOG9aVVdXXFxcXFxcLytNc2VBbFNCSHVEcUNYNUltS1h5Z2dKUDllRjhiNVhHSzJib09DdHAyZzFwdmZyUm1COWNHYmdUcXNPUlR2NU5XZzNibEpzdmJHaTFxSEV1WHdubXBuamZ3TjJDYUNmOENlK1A1MHh0WU8xWUxVUFNYdHhIVDlyVHdLRmxsSVh1SVV1Sk1GRXNTdkR4WWNFU2prZGN6TVhIWFZTaiszeHpyUVlrN0tyNGIwXFxcXFxcL0ttNVBieFNcXFxcXFwvc2ZiQjVSSnpETVRmY3pKcWVLZE92UzIyQll2TFh2VUFKZENud0FyazB3NEpwdFArVkRzUFlMR2NzRjBCMkJwZXc0amw5djdWXFxcXFxcL2JSVGFEZHVqSk1VeTZUSEhHTDJBWm9BS0NHOGVMK3FLaEJUeUhtVCtBT0VQVUpBNVlTQUF4WTY2bXVQTUNMdUswS3R4ZHFOSzR3UitLXFxcIn1cIn0ifQ==",
                                GatewayName = "QA" + "MigrateOnPremToPaaS",
                                UserName = "",
                                Password = "Acl@r@694"
                            }
                            ) { Description = "InsightsDW" }
                    }
                }
            );
            //
            Console.WriteLine("Creating a linked service to OnPrem InsightsMetaData");
            client.LinkedServices.CreateOrUpdate(resourceGroupName, dataFactoryName,
                new LinkedServiceCreateOrUpdateParameters()
                {
                    
                    LinkedService = new LinkedService()
                    {
                        Name = "QAInsightsMetaData", 
                        Properties = new LinkedServiceProperties(
                        new OnPremisesSqlServerLinkedService()
                            {                             
                                ConnectionString =  "data source=azuqsql001\\bi1;initial catalog=InsightsMetaData;Integrated Security=False;EncryptedCredential=eyJDb25uZWN0aW9uU3RyaW5nIjoiZGF0YSBzb3VyY2U9YXp1cXNxbDAwMVxcYmkxO2luaXRpYWwgY2F0YWxvZz1JbnNpZ2h0c01ldGFEYXRhO0ludGVncmF0ZWQgU2VjdXJpdHk9RmFsc2UiLCJFbmNyeXB0ZWRDcmVkZW50aWFsIjoie1wiQ2xhc3NUeXBlXCI6XCJNaWNyb3NvZnQuRGF0YVByb3h5LkNvcmUuSW50ZXJTZXJ2aWNlRGF0YUNvbnRyYWN0LkNyZWRlbnRpYWxTVTA2Q1kxNFwiLFwiRGF0YVNvdXJjZVNldHRpbmdzXCI6XCJ7XFxcIkNsYXNzVHlwZVxcXCI6XFxcIk1pY3Jvc29mdC5EYXRhUHJveHkuQ29yZS5JbnRlclNlcnZpY2VEYXRhQ29udHJhY3QuRGF0YVNvdXJjZVNldHRpbmdzRGF0YVYxXFxcIixcXFwiQ3JlZGVudGlhbFR5cGVcXFwiOjEsXFxcIkVuY3J5cHRlZENvbm5lY3Rpb25cXFwiOjEsXFxcIlByaXZhY3lcXFwiOjN9XCIsXCJFbmNyeXB0ZWRCbG9iXCI6XCJ7XFxcIkNsYXNzVHlwZVxcXCI6XFxcIk1pY3Jvc29mdC5EYXRhUHJveHkuQ29yZS5JbnRlclNlcnZpY2VEYXRhQ29udHJhY3QuQ2xpY2tPbmNlQ3JlZGVudGlhbERhdGFWMlxcXCIsXFxcIkVuY3J5cHRlZENyZWRlbnRpYWxcXFwiOlxcXCJNSUlDNFFZSktvWklodmNOQVFjRG9JSUMwakNDQXM0Q0FRQXhnZ0ZXTUlJQlVnSUJBREE2TUNZeEpEQWlCZ05WQkFNVEcwRmFWVkZUVVV3d01ERXVjM1JuTG1GamJHRnlZV05sTG1OdmJRSVFTMitpVzBCNm5hNUJnWFIzV1hJWDJUQU5CZ2txaGtpRzl3MEJBUUVGQUFTQ0FRQmVJQ0JobGErQndlNVhVOGN1VU5aeFVXcVdScVVBUHhRbytpVjFSNnRMS3doaFIxOVQ2QXB2Rjl5RGVpSFVGWldBQUtYbjlwZjVraitJOU5vR1ZzMm9XZWVVcWhCY1BCK3JOWElJcUtpN29XMXc3RUJVZjR6SG9cXFxcXFwvY1htS0E3SEtwMTcyKzY5cDJEUkhvUnNRaXh5aXI3M2lRVXpYdjZxWmdrSFpGbExQSG9KOEsrR3ZzeWJEU2JDRGxXTWNlWW1PYWNOUnp4d2duMEpFbmNac09BOE1mXFxcXFxcL2lLeFBjMWJcXFxcXFwvV21uVGVNUjV6SFpTN09MUlxcXFxcXC93TmxHK1llODh3bW1naFhFQUxxWjlJbmlpQzRXN1hHakEraDQ4SDB0UmszZEx5bTU5a2JPUDcwWU9DQk1FdjN2WkJZenNGa3hMMUwraEMrZEJYSHdZU3lkVVFxUkN1dWxaZzlLV1VyTUlJQmJRWUpLb1pJaHZjTkFRY0JNQlFHQ0NxR1NJYjNEUU1IQkFoTEhEcnE3ampTdDRDQ0FVaDVEU3FsN0RlaHp0UjNHcG41UXd0eFlZZnBjNXJrWUhPUTBvV1EzZUdEbnRTVXJUT1BpcmpnaEU4UEU2TWsxaSt1UUhjcUhtazBsK29cXFxcXFwvVmxkNXc2TzlNemZRZHNudjIrN0pVUUptQjBRTXNUaFN4XFxcXFxcL3FkOEl0YkNvZWtNRnBQdXUrWDFOYkZRTVxcXFxcXC90Z0djWkpibU5YVWxJZnc2c0hcXFxcXFwvaEE5UnQ1VDZPOWx3ZFp2aHcwc3lPXFxcXFxcLytxMzlDZ3NIc3UyK210OGdBeVRwOTVNbDRoRGhJY0FNUnlFSUVVcVNNNlFyXFxcXFxcL0Y0N3hJZHFDb3JQalQ4UVlLVURcXFxcXFwvRkdRb1QxY1xcXFxcXC9XR1xcXFxcXC9nd240WXhPV1BTNU1HM3lvbTBKeHdOK21qR1Y2WDRPWGtrYkhqRktJOWhTMTBLc0FIRDlYdWxrcmtydkhXZ1RJTkhYMFRqVFBkS1NGTVZYUDdmdnFFM3RjVWVNVnFnTVFvMWxIUGpnNzFZOU1NYUR1dGRiVXNuZnFMOXhEUk9QRFFIRlR3Qk1cXFxcXFwvRjlFNWtSRXNOR1docHBxMDNzallIait4MHFwTnVSRGR0NUEyNFZUY3FNdHhUMllLXFxcIixcXFwiRW5jcnlwdGlvbkNlcnRpZmljYXRlVGh1bWJwcmludFxcXCI6XFxcIjAzOTM3OTU1RTlCNzI4RTJBMjhGMUFBQ0IzMjhFRkZENURDMkU5MzRcXFwiLFxcXCJTaWduYXR1cmVcXFwiOlxcXCJNSUlCblFZSktvWklodmNOQVFjQ29JSUJqakNDQVlvQ0FRRXhEekFOQmdsZ2hrZ0JaUU1FQWdFRkFEQUxCZ2txaGtpRzl3MEJCd0V4Z2dGbE1JSUJZUUlCQVRBNk1DWXhKREFpQmdOVkJBTVRHMEZhVlZGVFVVd3dNREV1YzNSbkxtRmpiR0Z5WVdObExtTnZiUUlRUzIraVcwQjZuYTVCZ1hSM1dYSVgyVEFOQmdsZ2hrZ0JaUU1FQWdFRkFEQU5CZ2txaGtpRzl3MEJBUUVGQUFTQ0FRQm91M3lWZVdkR29QM0ZibU9kRE5QUUhqa0ZUWVc1WFdvSjZsNkNBSVIrTUFibXhQV0U5b24zdGZaNndiNXcwamtpV1NKdmJjeDZ6YUJhV29ObWVKSjYxbVgwMnFcXFxcXFwvYUpTVXRYOHJlekc4VGtoTWMzRHBPVXR2Z09MSXZYTVBLa0R0NGgrYUNRbTcwYXppYTFBbGxZVkF6K1VndWZNb1JaUDdSY2hYNmk4U3dcXFxcXFwvbm5vRmkyXFxcXFxcL1craTl2OUJsQ2JNcVpacWorc3NISk5IRjV3SE54SVFkMHMyV1ZCVHhSSStXY1xcXFxcXC9aZ0RVcmRyWlNoR255clNPaXlzWXdNeVl5SThlVmNqTEQ3Y3JvVzV1d3hsVTBjQ0t2V3hoN3VWWVNwcGdkRFRBR0FrYXp0M2pzUGRBY2tjUGJMZHMwTnZmbEJTR1dLRGozZG1VaHZSU0gyRTluMTNzZXkrZHE3XFxcIn1cIn0ifQ==",
                                GatewayName = "QA" + "MigrateOnPremToPaaS",
                                    UserName= "",
                                    Password= "Acl@r@694"
                            }

                            ) { Description = "InsightsMetaData" }
                    } 
                }
            );
            //
            Console.WriteLine("Creating a linked service to " + "InsightsDW_" + clientId + "_PaaS");
            client.LinkedServices.CreateOrUpdate(resourceGroupName, dataFactoryName,
                new LinkedServiceCreateOrUpdateParameters()
                {
                    LinkedService = new LinkedService()
                    {
                        Name = "InsightsDW_" + clientId + "_PaaS",
                        Properties = new LinkedServiceProperties(
                            new AzureSqlDatabaseLinkedService()
                            {
                                ConnectionString = "Data Source=tcp:aceQsql1c0.database.windows.net,1433;Initial Catalog=InsightsDW_"+ clientId.ToString() + ";User ID=AclaraCEAdmin@aceQsql1c0;Password=Acl@r@282;Encrypt=True;TrustServerCertificate=False;Application Name=\"Azure Data Factory Linked Service\""
                                
                            }
                            )
                    }
                }
            );
            Console.WriteLine("Creating a linked service to PaaS InsightsMetaData");
            client.LinkedServices.CreateOrUpdate(resourceGroupName, dataFactoryName,
                new LinkedServiceCreateOrUpdateParameters()
                {
                    LinkedService = new LinkedService()
                    {
                        Name = "InsightsMetaData" + "_PaaS",
                        Properties = new LinkedServiceProperties(
                            new AzureSqlDatabaseLinkedService()
                            {
                                ConnectionString = "Data Source=tcp:aceQsql1c0.database.windows.net,1433;Initial Catalog=InsightsMetaData;User ID=AclaraCEAdmin@aceQsql1c0;Password=Acl@r@282;Encrypt=True;TrustServerCertificate=False;Application Name=\"Azure Data Factory Linked Service\""

                            }
                            )
                    }
                }
            );
        }
      
        private static void CreateDataFactoriesForAClientID(DataFactoryManagementClient client, string resourceGroupName, string clientId, string dataFactoryName)
        {
            string origname = dataFactoryName;
            int i;
            if (clientId == "Meta")
            {
                for (i = 5; i <= 7; i++)
                {
                    try
                    {
                        dataFactoryName = origname + i.ToString() + '-' + clientId;
                        CreateDatFactory(client, resourceGroupName, clientId, dataFactoryName);
                        CreateDatFactoryLinkedServices(client, resourceGroupName, clientId, dataFactoryName);

                    }
                    catch (Exception ex)
                    {
                        Console.Write(" Shard " + clientId.ToString() + " " + i.ToString() + " DataFactory creation failed: " + dataFactoryName);
                    }
                }
            }
            else
                {
                    for (i = 1; i <= 4; i++)
                    {
                        try
                        {
                            dataFactoryName = origname + i.ToString() + '-' + clientId;
                            CreateDatFactory(client, resourceGroupName, clientId, dataFactoryName);
                            CreateDatFactoryLinkedServices(client, resourceGroupName, clientId, dataFactoryName);

                        }
                        catch (Exception ex)
                        {
                            Console.Write("Mata DataFactory creation failed: " + clientId.ToString() + " " + i.ToString() + dataFactoryName);
                        }
                    }
                }
        }

        private static void ProcessGroupForClient(DataFactoryManagementClient client, string resourceGroupName, string clientId, string dataFactoryName, int groupToProcess, ArrayList DW_TableList, string processByswitch)
        {

            
            //dataFactoryName = dataFactoryName + groupToProcess.ToString() + '-' + clientId;
            // trying to check for fix
            //commented out line below
            //dataFactoryName = dataFactoryName + groupToProcess.ToString() + '-' + "224";
            dataFactoryName = dataFactoryName + groupToProcess.ToString() + '-' + clientId.ToString();
            //
            PerformProcessingLoop(clientId, resourceGroupName, dataFactoryName, client, groupToProcess, DW_TableList, processByswitch);
            Console.WriteLine("Finished with Group " + groupToProcess.ToString() + " for client " + clientId);

        }

        private static void PerformProcessingLoop(string clientId, string resourceGroupName, string dataFactoryName, DataFactoryManagementClient client, int groupToProcess, ArrayList DW_TableList, string processByswitch)
        {
            
            string XXXid = "XXX";

            string mytable;
            int myjoinit;
            string mydatabase;
            string tablename;
            int myDWGroup;
            int i = 0;
            for (i = 0; i <= DW_TableList.Count - 1; i++)
            {
                
                var mytableitem = (TableItem)DW_TableList[i];
                mytable = mytableitem.Tablename;
                myjoinit = mytableitem.joinit;
                mydatabase = mytableitem.database;
                myDWGroup = mytableitem.DWGroup;
                string linkedservicename = "QA" + mydatabase;
                if (myDWGroup != groupToProcess) continue;
                tablename = mytable.Substring(mytable.IndexOf(".") + 1);
                Console.WriteLine("starting to process " + mydatabase + "." + mytable);
                if (processByswitch != "3")
                {
                    CreateOnPrem_DW_Dataset(resourceGroupName, dataFactoryName, client, XXXid, clientId, mytable, tablename, linkedservicename);
                    CreatePaaS_DW_Dataset(resourceGroupName, dataFactoryName, client, XXXid, clientId, mytable, tablename);
                    CreatePipeline(resourceGroupName, dataFactoryName, client, XXXid, clientId, mytable, tablename, myjoinit);
                }
                else
                {
                    ////Delete_PaaS_XXX_DW_Dataset(resourceGroupName, dataFactoryName, client, XXXid, clientId, mytable, tablename);
                    Delete_PaaS_XXX_DW_Dataset(resourceGroupName, dataFactoryName, client, XXXid, clientId, mytable, tablename);
                }
                Console.WriteLine("process for " + mydatabase + "." + mytable + " is complete");
            }
        }

        private static ArrayList LoadList(string clientid)
        {
            ArrayList DW_TableList = new ArrayList();
            if (clientid !="Meta")
            {
                LoadTheTables(DW_TableList);
            }
            else
            {
                LoadTheMetaTables(DW_TableList);
            }
            return DW_TableList;
        }

        private static ArrayList LoadTableToList(string tableNametoProcess)
        {
            ArrayList DW_TableListTemp = new ArrayList();
            LoadTheTables(DW_TableListTemp);
            int i;
            ArrayList DW_TableList = new ArrayList();
            for (i = 0; i <= DW_TableListTemp.Count - 1; i++)
            {
                var mytableitem = (TableItem)DW_TableListTemp[i];
                if (mytableitem.Tablename == tableNametoProcess)
                {
                    DW_TableList.Add(mytableitem);
                    break;
                }
            }            
            return DW_TableList;
        }

        private static void LoadTheTables(ArrayList DW_TableList)
        {
            Load_DW_TablesIntoList(DW_TableList);
            Load_DW_Joins_TablesIntoList(DW_TableList);
            Load_DW_No_Where_TablesIntoList(DW_TableList);
            Load_MetaData_TablesIntoList(DW_TableList);
            Load_DW_referrer_TablesIntoList(DW_TableList);
            Load_InsightsBulk_TablesIntoList(DW_TableList);
            Load_DW_SpecialCases_TablesIntoList(DW_TableList);
        }

        private static void LoadTheMetaTables(ArrayList DW_TableList)
        {
            Load_MetaData_TablesIntoList(DW_TableList);
        }

        private static void GetDevAdfClient( out DataFactoryManagementClient client)
        {
            // create data factory management client



            TokenCloudCredentials aadTokenCredentials =
                new TokenCloudCredentials(
                    ConfigurationManager.AppSettings["SubscriptionId"],
                    GetAuthorizationHeader());

            Uri resourceManagerUri = new Uri(ConfigurationManager.AppSettings["ResourceManagerEndpoint"]);

            client = new DataFactoryManagementClient(aadTokenCredentials, resourceManagerUri);
        }

        private static void CreatePaaS_DW_Dataset(string resourceGroupName, string dataFactoryName, DataFactoryManagementClient client, string xxxid, string clientId, string mytable, string tablename)
        {
            Console.WriteLine("starting to create PaaS dataset " + "PaaS" + tablename + "_" + clientId);
            //fix for stored proc
            if (mytable == "dbo.DimPremise")
            {
                mytable = "DimPremise";
            }
            //
            try
            {
                // create input and output tables

                client.Tables.CreateOrUpdate(resourceGroupName, dataFactoryName, new TableCreateOrUpdateParameters()
                {
                    Table = new Table()
                    {

                        Name = "PaaS" + tablename + "_" + clientId,
                        Properties = new TableProperties()
                        {
                            TypeProperties = new AzureSqlTableDataset()
                            {
                                TableName = mytable

                            },
                            LinkedServiceName = (clientId =="Meta" ? "InsightsMetaData" : ("InsightsDW_" + clientId)) + "_PaaS",
                            

                            Availability = new Availability()
                            {
                                
                                Frequency = SchedulePeriod.Day,
                                Interval = 1
                            }
                        }

                    }
                });
                Console.WriteLine("PaaS dataset " + "PaaS" + tablename + "_" + clientId + " created sucessfully");
            }            
            catch (Exception ex)
            {
                Console.Write("Azure dataset: " + mytable + " failed");
            }
        }
        private static void Delete_PaaS_XXX_DW_Dataset(string resourceGroupName, string dataFactoryName, DataFactoryManagementClient client, string xxxid, string clientId, string mytable, string tableName)
        {
            Console.WriteLine("starting to delete PaaS "+ clientId + " " + tableName + " objects ");
            string Name = string.Format("Copy{0}FromOnPremSQLtoPaaS_{1}", tableName, clientId);
            try
            { 
                client.Pipelines.Delete(resourceGroupName,  dataFactoryName,  Name);
                Console.WriteLine("PaaS dataset " + "PaaS" + tableName + "_" + clientId + " deleted sucessfully");

            
                 Name = "OnPrem" + tableName;

                client.Tables.Delete(resourceGroupName, dataFactoryName, Name);
                Console.WriteLine("PaaS dataset " + "PaaS" + tableName + "_" + clientId + " deleted sucessfully");

            
                Name = "PaaS" + tableName + "_" + clientId;

                client.Tables.Delete(resourceGroupName, dataFactoryName, Name);
                Console.WriteLine("PaaS dataset " + "PaaS" + tableName + "_" + clientId + " deleted sucessfully");
            }
            
            catch (Exception ex)
            {
                Console.Write("Delete for table: " + Name + " failed");
            }
        }

        private static void CreateOnPrem_DW_Dataset(string resourceGroupName, string dataFactoryName, DataFactoryManagementClient client, string xxxid, string clientId, string mytable, string tableName, string linkedService)
        {
            //fix for stored proc
            if (mytable == "dbo.DimPremise")
            {
                mytable = "DimPremise";
            }
            //
            try
            {
                // create input and output tables

                client.Tables.CreateOrUpdate(resourceGroupName, dataFactoryName, new TableCreateOrUpdateParameters()
                {
                    Table = new Table()
                    {
                        
                        Name = "OnPrem" + tableName,
                        Properties = new TableProperties()
                        {
                            TypeProperties = new SqlServerTableDataset()
                            {
                                TableName = mytable

                            },
                            LinkedServiceName = linkedService,
                            
                            External = true,

                            Availability = new Availability()
                            {
                                Frequency = SchedulePeriod.Day,
                                Interval = 1
                                
                            },
                        }

                    }
                });

            }
            catch (Exception ex)
            {
                Console.Write("OnPrem dataset: " + mytable + " failed");
            }
        }

        //These are the insightsdw tables 
        //that require a join 
        //and a where
        //if the joinit =2 the client id must be first column
        //if the joinit=1 the clientid is the last column
        private static void Load_DW_Joins_TablesIntoList(ArrayList Join_TableList)
        {
            Join_TableList.Add(new TableItem{DWGroup=4, Tablename = "dbo.AmiAggregate", joinit=7, database="InsightsDW" });
            Join_TableList.Add(new TableItem{DWGroup=2, Tablename = "dbo.FactActionData", joinit=11, database="InsightsDW" });
            Join_TableList.Add(new TableItem{DWGroup=2, Tablename = "dbo.FactActionItem", joinit=12, database="InsightsDW" });
            Join_TableList.Add(new TableItem{DWGroup=4, Tablename = "dbo.FactCustomerPremise", joinit=1, database="InsightsDW" });
            Join_TableList.Add(new TableItem{DWGroup=4, Tablename = "dbo.FactEnergyObject", joinit=1, database="InsightsDW" });
            Join_TableList.Add(new TableItem{DWGroup=4, Tablename = "dbo.factEventAdditionalInfo", joinit=8, database="InsightsDW" });
            Join_TableList.Add(new TableItem{DWGroup=4, Tablename = "dbo.factEventDetails", joinit=1, database="InsightsDW" });
            //
            Join_TableList.Add(new TableItem{DWGroup=4, Tablename = "dbo.FactServicePoint", joinit=6, database="InsightsDW" });
            //
            //Join_TableList.Add(new TableItem{DWGroup=4, Tablename = "export.HE_Report_actionitems", joinit=1, database="InsightsDW" });
            //Join_TableList.Add(new TableItem{DWGroup=4, Tablename = "export.HE_Report_actionitems_ranked", joinit=1, database="InsightsDW" });
            Join_TableList.Add(new TableItem{DWGroup=4, Tablename = "export.HE_Report_CSV_Measure_history", joinit=9, database="InsightsDW" });
            Join_TableList.Add(new TableItem{DWGroup=4, Tablename = "export.HE_Report_CSV_Promo_History", joinit=9, database="InsightsDW" });
            //Join_TableList.Add(new TableItem{DWGroup=4, Tablename = "export.HE_Report_FactActionItem", joinit=1, database="InsightsDW" });
            Join_TableList.Add(new TableItem{DWGroup=4, Tablename = "export.HE_Report_History", joinit=5, database="InsightsDW" });
            //Join_TableList.Add(new TableItem{DWGroup=4, Tablename = "export.home_energy_report_promo_prereqs", joinit=1, database="InsightsDW" });
            //Join_TableList.Add(new TableItem{DWGroup=4, Tablename = "export.home_energy_report_staging", joinit=1, database="InsightsDW" });
            Join_TableList.Add(new TableItem{DWGroup=4, Tablename = "dbo.PremiseSavings", joinit=1, database="InsightsDW" });


        }

        private static void Load_DW_SpecialCases_TablesIntoList(ArrayList Join_TableList)
        {
         //   Join_TableList.Add(new TableItem { DWGroup = 3, Tablename = "dbo.FactServicePoint", joinit = 6, database = "InsightsDW" });
        }

        //these are the insightsDW tables that require no where clause
        private static void Load_DW_No_Where_TablesIntoList(ArrayList DW_TableList)
        {
            DW_TableList.Add(new TableItem { DWGroup = 1, Tablename = "dbo.BenchmarkErrorType", joinit = 4, database = "InsightsDW" });
            DW_TableList.Add(new TableItem { DWGroup = 1, Tablename = "dbo.BenchmarkGroupType", joinit = 4, database = "InsightsDW" });
            DW_TableList.Add(new TableItem { DWGroup = 1, Tablename = "dbo.DimAction", joinit = 4, database = "InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimActionStatus", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimBillPeriodType", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimChannel", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimCity", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimCommodity", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimCountryRegion", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimCustomerAuthenticationType", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimCustomerType", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimDate", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimEnergyObject", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimEventAction", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimEventClass", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimEventInfo", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimEventType", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimLatestYesOrNo", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimMeterIntervalLength", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimMeterType", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimOrigin", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimPostalCode", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimPremiseAttribute", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimPremiseOption", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimServiceContractType", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimSource", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimStateProvince", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimTime", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimTimePeriod", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem {DWGroup=1, Tablename = "dbo.DimUOM", joinit=4, database="InsightsDW" });
            DW_TableList.Add(new TableItem { DWGroup = 1, Tablename = "dbo.tblDigram", joinit = 4, database = "InsightsDW" });
            DW_TableList.Add(new TableItem { DWGroup = 1, Tablename = "dbo.tblTrigram", joinit = 4, database = "InsightsDW" });
            DW_TableList.Add(new TableItem { DWGroup = 1, Tablename = "dbo.WeatherData", joinit = 4, database = "InsightsDW" });
            DW_TableList.Add(new TableItem { DWGroup = 1, Tablename = "dbo.PromoCodeGasSavings", joinit = 4, database = "InsightsDW" });
        }
        //These are the insights metadatatables
        // no where clause needed
        private static void Load_MetaData_TablesIntoList(ArrayList Meta_TableList)
        {
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "admin.AppTaskHeartbeat", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "admin.AppTaskRunStatus", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "admin.AppTasks", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "admin.Configuration", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "admin.CustomerExportConfiguration", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "admin.ETLBulkConfiguration", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "admin.ETLInfo", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "audit.ActionItemImportErrors", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "audit.BillImportErrors", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "audit.CommandLog", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "audit.CustomerImportErrors", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "audit.DisAggErrorLog", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "audit.EnergyEventImportLog", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "audit.ExecutionErrors", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "audit.ExecutionLog", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "audit.MeasureSavingsErrorLog", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "audit.NotificationEventLog", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "audit.ProcessLog", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "audit.ProfileImportErrors", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "audit.StatisticLog", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientAction", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientActionAppliance", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientActionCondition", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientActionSavings", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientActionSavingsCondition", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientActionSeason", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientActionWhatIfData", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientAppliance", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientApplianceExpression", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientApplianceProfileAttribute", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientAsset", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientAssetLocaleContent", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientBenchmarkGroup", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientCommodity", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientCondition", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientConfiguration", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientConfigurationBulk", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientCurrency", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientEnduse", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientEnduseAppliance", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=5, Tablename = "cm.ClientEnumeration", joinit=4, database="InsightsMetaData" });
            //
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientEnumerationEnumerationItem", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientEnumerationItem", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientExpression", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientFileContent", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientLayout", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientMeasurement", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientProfileAttribute", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientProfileAttributeCondition", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientProfileAttributeProfileOption", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientProfileDefault", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientProfileDefaultCollection", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientProfileOption", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientProfileSection", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientProfileSectionAttribute", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientProfileSectionCondition", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientSeason", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientTab", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientTabAction", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientTabChildTab", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientTabCondition", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientTabConfiguration", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientTabProfileSection", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientTabTextContent", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientTextContent", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientTextContentLocaleContent", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientUOM", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientWhatIfData", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientWidget", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientWidgetCondition", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientWidgetConfiguration", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ClientWidgetTextContent", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ContentCache", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.ContentOrphanLog", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.DatabaseDiagrams", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.EMApplianceRegion", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.EMDailyWeatherDetail", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.EMDegreeDay", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.EMEvaporation", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.EMSolar", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.EMTemperatureProfile", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.EMTemperatureRegion", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.EMZipcode", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.EnvironmentConfiguration", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.TypeAction", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=6, Tablename = "cm.TypeActionStatus", joinit=4, database="InsightsMetaData" });
            //
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeActionType", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeAppliance", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeAsset", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeBenchmarkGroup", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeCategory", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeCommodity", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeCondition", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeConfiguration", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeConfigurationBulk", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeCurrency", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeDifficulty", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeEnduse", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeEnduseCategory", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeEntityLevel", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeEnumeration", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeEnumerationItem", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeEnvironment", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeExpression", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeFileContent", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeHabitInterval", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeLayout", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeLocale", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeMeasurement", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeProfileAttribute", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeProfileAttributeType", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeProfileDefault", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeProfileDefaultCollection", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeProfileOption", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeProfileSection", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeProfileSource", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeQuality", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeSeason", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeTab", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeTabType", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeTextContent", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeUom", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeWhatIfData", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeWidget", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "cm.TypeWidgetType", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "dbo.Client", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "dbo.ClientFilePurgePolicy", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "dbo.ClientPropertyData", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "dbo.OptOutChannel", joinit=4, database="InsightsMetaData" });
            Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "dbo.UtilityPrograms", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "ETL.INV_Action", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "ETL.INV_FactAction", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "ETL.INV_FactBilling", joinit=4, database="InsightsMetaData" });
            //Meta_TableList.Add(new TableItem { DWGroup=7, Tablename = "ETL.INV_FactPremiseAttribute", joinit=4, database="InsightsMetaData" });

        }
        
        //These are the insightsBulk tables that have a client id 
        // and need a where clause
        // but no join
        private static void Load_InsightsBulk_TablesIntoList(ArrayList DW_TableList)
        {
            
            DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.ETBounceOptOut", joinit = 0, database = "InsightsBulk" });
            DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.ETBounceOptOut", joinit = 0, database = "InsightsBulk" });
            DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.ETClickOpen", joinit = 0, database = "InsightsBulk" });
            DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.ETUnprocessed", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.Events", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.RawActionTemp", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.RawAmiTemp", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.RawBillTemp_224", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.RawBillTemp_276", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.RawCsvTemp", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.RawProfileTemp", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.RawProgramTemp_276", joinit = 0, database = "InsightsBulk" });
            DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.ScgCallCenterOptOuts", joinit = 0, database = "InsightsBulk" });
            DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.ScgDailyTotals", joinit = 0, database = "InsightsBulk" });
            DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.ScgEmailOptOuts", joinit = 0, database = "InsightsBulk" });
            DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.ScgOptOuts", joinit = 0, database = "InsightsBulk" });
            DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "dbo.tempqueue", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "ETL.ActionItem", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "ETL.PremiseAttributes_INS", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "ETL.PremiseAttributes_UPD", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "Holding.ActionItem", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "Holding.ActionItemExceptions", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "Holding.Billing", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "Holding.BillingExceptions", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "Holding.Client", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "Holding.Customer", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "Holding.CustomerExceptions", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "Holding.DisAggReCalcQueue", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "Holding.Events", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "Holding.lydiasbills", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "Holding.Premise", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "Holding.PremiseAttributes", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "Holding.PremiseAttributesTEST", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "Holding.Profile", joinit = 0, database = "InsightsBulk" });
            //DW_TableList.Add(new TableItem { DWGroup=3, Tablename = "Holding.ProfileExceptions", joinit = 0, database = "InsightsBulk" });
        }

        //These are the insightsBulk tables that do not have a client id 
        // and dont need a where clause
        // but no join
        private static void Load_InsightsBulk_No_Where_TablesIntoList(ArrayList DW_TableList)
        {
            DW_TableList.Add(new TableItem { DWGroup = 1, Tablename = "dbo.HomeEnergyHistoryReport", joinit = 4, database = "InsightsBulk" });
        }

        //These are the insightsdw tables that have a client id o
        // and need a where clause
        //but no join
        private static void Load_DW_TablesIntoList(ArrayList DW_TableList)
        {        
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.BenchmarkBills", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.BenchmarkErrors", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.BenchmarkGroupClientType", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.BenchmarkGroups", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.BenchmarkGroupsHistory", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.Benchmarks", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.BenchmarksHistory", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.DimClient", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.DimCustomer", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.DimMeter", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.DimPremise", joinit=10, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.DimRateClass", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.DimServiceContract", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.DimServicePoint", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.EmailExclusions", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.factAction", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.FactAmi", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.FactAmiDetail", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.FactBillDisagg", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.FactBilling", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.factClientAttribute", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.FactEvent", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.FactMeasure", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.FactPremiseAttribute", joinit=0, database="InsightsDW"});
            //
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.FactServicePointBilling", joinit=0, database="InsightsDW"});
            //
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.PostalCodeChanges", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="dbo.profile", joinit=0, database="InsightsDW"});
        //DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="ETL.INS_DimCustomer", joinit=0, database="InsightsDW"});
        //DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="ETL.INS_DimPremise", joinit=0, database="InsightsDW"});
        //DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="ETL.INS_FactAction", joinit=0, database="InsightsDW"});
        //DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="ETL.INS_FactBilling", joinit=0, database="InsightsDW"});
        //DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="ETL.INS_FactPremiseAttribute", joinit=0, database="InsightsDW"});
        //DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="ETL.KEY_DimCustomer", joinit=0, database="InsightsDW"});
        //DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="ETL.KEY_DimPremise", joinit=0, database="InsightsDW"});
        //DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="ETL.KEY_FactAction", joinit=0, database="InsightsDW"});
        //DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="ETL.KEY_FactBilling", joinit=0, database="InsightsDW"});
        //DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="ETL.KEY_FactPremiseAttribute", joinit=0, database="InsightsDW"});
        //DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="ETL.T1U_DimCustomer", joinit=0, database="InsightsDW"});
        //DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="ETL.T1U_DimPremise", joinit=0, database="InsightsDW"});
        //DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="ETL.T1U_FactBilling", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="Export.home_energy_report_client", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="Export.home_energy_report_criteria_profile", joinit=13, database="InsightsDW"});
        //DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="Export.home_energy_report_criteria_profile_hold", joinit=0, database="InsightsDW"});
        DW_TableList.Add(new TableItem{DWGroup=2, Tablename ="Export.home_energy_report_promo_codes", joinit=0, database="InsightsDW"});
        }

        //These are the insightsdw tables that have a referrer id 
        // and need a where clause
        //but no join
        private static void Load_DW_referrer_TablesIntoList(ArrayList DW_TableList)
        {
            DW_TableList.Add(new TableItem { DWGroup=4, Tablename = "dbo.PROD_HERS_For_Compare", joinit = 5, database = "InsightsDW" });
            DW_TableList.Add(new TableItem { DWGroup=4, Tablename = "dbo.UAT_HERS_For_Compare", joinit = 5, database = "InsightsDW" });
            DW_TableList.Add(new TableItem { DWGroup=4, Tablename = "Export.HE_Report_History", joinit = 5, database = "InsightsDW" });
            DW_TableList.Add(new TableItem { DWGroup=4, Tablename = "Export.home_energy_report_staging", joinit = 5, database = "InsightsDW" });
            DW_TableList.Add(new TableItem { DWGroup=4, Tablename = "Export.home_energy_report_staging_allcolumns", joinit = 5, database = "InsightsDW" });
        }


        private static void CreatePipeline(string resourceGroupName, string dataFactoryName, DataFactoryManagementClient client, string xxxid, string clientId, string mytable, string tableName, int joinit)
        {
            try
            {
                Console.WriteLine("Creating a pipeline for table {0}  client {1}", tableName, clientId);
                DateTime SliceStart = new DateTime(2015, 8, 28, 0, 0, 0, 0, DateTimeKind.Utc);
                DateTime SliceEnd = SliceStart.AddDays(1);
                client.Pipelines.CreateOrUpdate(resourceGroupName, dataFactoryName,
                    new PipelineCreateOrUpdateParameters()
                    {
                        Pipeline = new Pipeline()
                        {
                            Name = string.Format("Copy{0}FromOnPremSQLtoPaaS_{1}", tableName, clientId),
                            Properties = new PipelineProperties()
                            {
                                Description = "This pipeline has one Copy activity that copies data from an on-prem SQL to Azure Database",

                                // Initial value for pipeline's active period. With this, you won't need to set slice status

                                Start = SliceStart,
                                End = SliceEnd,
                                
                                Activities = new List<Activity>()
                            {
                                CreateCopyActivity(mytable, clientId, tableName, xxxid, joinit)
                            }
                            }
                        }
                    });
                Console.WriteLine("Creating of pipeline for table {0}  client {1}", tableName, clientId + " is complete");
            }
            catch (Exception ex)
            {
                Console.Write("Pipeline: " + mytable + " failed");
            }
        }

        /// <summary>
        /// create an activity that will copy data from on premise sqlserver to azure sqlserver
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="clientId"></param>
        private static Activity CreateCopyActivity(string mytable, string clientId, string tableName, string xxxid, int joinit)
        {
            Console.WriteLine("starting to create copy activity " + string.Format("Copy{0}_{1}", tableName, clientId));
      
            string joinitstring;
            if (joinit == 1)
            {
                joinitstring= "select a.*, dp.clientid from {0} a inner join  insightsdw.dbo.DimPremise dp on dp.premisekey=a.premisekey where Clientid{1}";
            }
            else if (joinit == 2)
            {
                joinitstring = "select dp.clientid, a.* from {0} a inner join  insightsdw.dbo.DimPremise dp on dp.premisekey=a.premisekey where Clientid{1}";
            }
            else if (joinit == 4)  //these have no where clause
            {
                joinitstring = "select * from {0}";
            }
            else if (joinit == 5)  
            {
                joinitstring = "select * from {0} where referrerid{1}";
            }
            else if (joinit == 6)
            {                
                joinitstring = "select a.*, sc.ClientId FROM  {0} a INNER JOIN insightsdw.dbo.DimServiceContract sc ON sc.ServiceContractKey = a.ServiceContractKey where Clientid{1}";
            }
            else if (joinit == 7)
            {                
                joinitstring = "select a.*, dp.ClientId FROM  {0} a INNER JOIN insightsdw.dbo.factami fa ON a.AmiKey=fa.AmiKey INNER JOIN insightsdw.dbo.dimpremise dp ON dp.PremiseKey = fa.PremiseKey where dp.Clientid{1}";
            }
            else if (joinit == 8)
            {
                joinitstring = "select * from {0} a INNER JOIN Insightsdw.dbo.factEventDetails d ON d.EventDetailKey = a.EventDetailKey INNER JOIN Insightsdw.dbo.DimPremise dp ON dp.PremiseKey = d.PremiseKey where Clientid{1}";
            }
            else if (joinit == 9)
            {
                joinitstring = "select * from {0} a INNER JOIN [Export].[home_energy_report_criteria_profile] p ON p.profile_name = a.profile_name where p.Clientid{1}";
            }
            else if (joinit == 10)
            {
                joinitstring = "select PremiseKey ,PostalCodeKey ,CityKey ,SourceKey ,AccountId ,PremiseId ,Street1 ,Street2 ,City ,StateProvince ,Country ,PostalCode ,GasService ,ElectricService ,WaterService ,CreateDate ,UpdateDate ,ClientId ,SourceId ,ETL_LogId ,IsInferred ,TrackingId ,TrackingDate from DimPremise where Clientid{1}";
            }
            else if (joinit == 11)
            {
                joinitstring = "select dp.clientid, a.* from {0} a inner join  insightsdw.dbo.factaction dp on dp.ActionDetailKey=a.ActionDetailKey where Clientid{1}";
            }
            else if (joinit == 12)
            {
                joinitstring = "select dp.clientid, a.* from {0} a inner join  insightsdw.dbo.factaction dp on dp.premisekey=a.premisekey where Clientid{1}";
            }
            else if (joinit == 13)
            {
                joinitstring = "SELECT he_criteria_profile_key, profile_name, clientID, fuel_type, energyobjectcategory_List, period, season_List, enduse_List, ordered_enduse_List, criteria_id1, criteria_detail1, criteria_id2, criteria_detail2, criteria_id3, criteria_detail3, criteria_id4, criteria_detail4, criteria_id5, criteria_detail5, criteria_id6, criteria_detail6, criteria_sort1, criteria_sort2, criteria_sort3, criteria_sort4, criteria_sort5, criteria_sort6, process_id, process_desc, process_time, process_num_rows, process_start_time, process_end_time, ins_time, upd_time  FROM [Export].[home_energy_report_criteria_profile] where Clientid{1}";
            }
            else //should be 0's
            {
                joinitstring = "select * from {0} where Clientid{1}";
            }

            Activity activity;
            string clientIdFoWhere;

            if (clientId == "Test")
            {
                clientIdFoWhere = " in ('87','256')";
            }
            else
            {
                clientIdFoWhere = " = " + clientId;
            }
            if (joinit == 10)

            {
                 activity = new Activity()
                {

                    Name = string.Format("Copy{0}_{1}", tableName, clientId),
                    Description = string.Format("Copy {0} table from on-prem SQL server to Azure PaaS {1} shard Database", tableName, clientId),

                    Inputs = new List<ActivityInput>()
                {
                        new ActivityInput()
                        {
                            Name = string.Format("OnPrem{0}", tableName)


                        }
                },

                    Outputs = new List<ActivityOutput>()
                {
                    new ActivityOutput()
                    {
                        Name = string.Format("PaaS{0}_{1}", tableName, clientId)
                       
                    }
                },

                    TypeProperties = new CopyActivity()
                    {
                        Source = new SqlSource()
                        {
                            SqlReaderQuery = string.Format(joinitstring, mytable, clientIdFoWhere)
                        },
                        Sink = new SqlSink(){
                            SqlWriterStoredProcedureName="CopyDimPremiseFromOnPremSQLtoPaaS",
                            SqlWriterTableType="OnPremTypeDimPremise",
                            WriteBatchSize = 0,
                            WriteBatchTimeout = TimeSpan.FromMinutes(0)
                        }

                    }
                };
            }
            else if (joinit == 13)
             {
                 activity = new Activity()
                {

                    Name = string.Format("Copy{0}_{1}", tableName, clientId),
                    Description = string.Format("Copy {0} table from on-prem SQL server to Azure PaaS {1} shard Database", tableName, clientId),

                    Inputs = new List<ActivityInput>()
                {
                        new ActivityInput()
                        {
                            Name = string.Format("OnPrem{0}", tableName)


                        }
                },

                    Outputs = new List<ActivityOutput>()
                {
                    new ActivityOutput()
                    {
                        Name = string.Format("PaaS{0}_{1}", tableName, clientId)
                       
                    }
                },

                    TypeProperties = new CopyActivity()
                    {
                        Source = new SqlSource()
                        {
                            SqlReaderQuery = string.Format(joinitstring, mytable, clientIdFoWhere)
                        },
                        Sink = new SqlSink(){
                            SqlWriterStoredProcedureName = "Export.Copyhome_energy_report_criteria_profileFromOnPremSQLtoPaaS",
                            SqlWriterTableType = "OnPremTypehome_energy_report_criteria_profile",
                            WriteBatchSize = 0,
                            WriteBatchTimeout = TimeSpan.FromMinutes(0)
                        }

                    }
                };
            }
            else
            {
                activity = new Activity()
                {

                    Name = string.Format("Copy{0}_{1}", tableName, clientId),
                    Description = string.Format("Copy {0} table from on-prem SQL server to Azure PaaS {1} shard Database", tableName, clientId),

                    Inputs = new List<ActivityInput>()
                {
                        new ActivityInput()
                        {
                            Name = string.Format("OnPrem{0}", tableName)


                        }
                },

                    Outputs = new List<ActivityOutput>()
                {
                    new ActivityOutput()
                    {
                        Name = string.Format("PaaS{0}_{1}", tableName, clientId)
                       
                    }
                },

                    TypeProperties = new CopyActivity()
                    {
                        Source = new SqlSource()
                        {
                            SqlReaderQuery = string.Format(joinitstring, mytable, clientIdFoWhere)
                        },
                        Sink = new SqlSink()
                        {
                            WriteBatchSize = 0,
                            WriteBatchTimeout = TimeSpan.FromMinutes(0)
                        }

                    }
                };
            }
           
            Console.WriteLine("create of copy activity " + string.Format("Copy{0}_{1}", tableName, clientId) + " complete");
            return activity;

        }
        public static string GetAuthorizationHeader()
        {
            AuthenticationResult result = null;
            var thread = new Thread(() =>
            {
                try
                {
                    var context = new AuthenticationContext(ConfigurationManager.AppSettings["ActiveDirectoryEndpoint"] + ConfigurationManager.AppSettings["ActiveDirectoryTenantId"]);

                    result = context.AcquireToken(
                        resource: ConfigurationManager.AppSettings["WindowsManagementUri"],
                        clientId: ConfigurationManager.AppSettings["AdfClientId"],
                        redirectUri: new Uri(ConfigurationManager.AppSettings["RedirectUri"]),
                        promptBehavior: PromptBehavior.Auto);
                }
                catch (Exception threadEx)
                {
                    Console.WriteLine(threadEx.Message);
                }
            });

            thread.SetApartmentState(ApartmentState.STA);
            thread.Name = "AcquireTokenThread";
            thread.Start();
            thread.Join();

            if (result != null)
            {
                return result.AccessToken;
            }

            throw new InvalidOperationException("Failed to acquire token");
        }

    }
}
