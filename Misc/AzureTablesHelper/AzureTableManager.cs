﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using System.Runtime.InteropServices;
using System.Configuration;
using System.Diagnostics;
using Microsoft.WindowsAzure.Storage.Table;

namespace AzureTablesHelper
{
    public class AzureTableManager
    {

        /// <summary>
        /// env = dev
        /// tableName = ClientAccount
        /// </summary>
        /// <param name="environment"></param>
        /// <param name="tableName"></param>
        public void GetSelectFromTable(string environment, string tableName)
        {

            // Parse the connection string and return a reference to the storage account.
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString" + "." + environment]);

            // Create the table client.
            var tableClient = storageAccount.CreateCloudTableClient();

            // Retrieve a reference to the table.
            var table = tableClient.GetTableReference(tableName);

        }


        public async Task DeleteFromTableForClientId(string environment, string tableName, int clientId)
        {

            // Parse the connection string and return a reference to the storage account.
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString" + "." + environment]);

            // Create the table client.
            var tableClient = storageAccount.CreateCloudTableClient();

            // Retrieve a reference to the table.
            var table = tableClient.GetTableReference(tableName);

            // Initialize a default TableQuery to retrieve all the entities in the table.
            var tableQuery = new TableQuery<DynamicTableEntity>();

            // Initialize the continuation token to null to start from the beginning of the table.
            TableContinuationToken continuationToken = null;

            do
            {
                // Retrieve a segment(up to 1000 entities).
                var tableQueryResult = await table.ExecuteQuerySegmentedAsync(tableQuery, continuationToken);

                // Assign the new continuation token to tell the service where to
                // continue on the next iteration (or null if it has reached the end).
                continuationToken = tableQueryResult.ContinuationToken;

                // Print the number of rows retrieved.
                Debug.WriteLine("Entities retrieved from {0} = {1}", tableName, tableQueryResult.Results.Count);

                // Delete
                DeleteEntitiesInResult(tableQueryResult, table, tableName, clientId);

                //Loop until a null continuation token is received, indicating the end of the table.
            } while (continuationToken != null);

            Debug.WriteLine($"End of {tableName} table.");

        }

        public async Task DeleteFromTableForClientId(string environment, string tableName, int clientId, string customerPrefix)
        {

            // Parse the connection string and return a reference to the storage account.
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString" + "." + environment]);

            // Create the table client.
            var tableClient = storageAccount.CreateCloudTableClient();

            // Retrieve a reference to the table.
            var table = tableClient.GetTableReference(tableName);

            // Initialize a default TableQuery to retrieve all the entities in the table.
            var tableQuery = new TableQuery<DynamicTableEntity>();

            // Initialize the continuation token to null to start from the beginning of the table.
            TableContinuationToken continuationToken = null;

            do
            {
                // Retrieve a segment(up to 1000 entities).
                var tableQueryResult = await table.ExecuteQuerySegmentedAsync(tableQuery, continuationToken);

                // Assign the new continuation token to tell the service where to
                // continue on the next iteration (or null if it has reached the end).
                continuationToken = tableQueryResult.ContinuationToken;

                // Print the number of rows retrieved.
                Debug.WriteLine("Entities retrieved from {0} = {1}", tableName, tableQueryResult.Results.Count);

                // Delete
                DeleteEntitiesInResult(tableQueryResult, table, tableName, clientId, customerPrefix);

                //Loop until a null continuation token is received, indicating the end of the table.
            } while (continuationToken != null);

            Debug.WriteLine($"End of {tableName} table.");

        }


        /// <summary>
        /// Delete entities in this result.  Parellel deletion.  Faster than the serial deletion.
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="table"></param>
        /// <param name="tableName"></param>/// 
        /// <param name="clientid"></param>
        public void DeleteEntitiesInResult(TableQuerySegment<DynamicTableEntity> entities, CloudTable table, string tableName, int clientid)
        {
            var count = 0;

            var mainsw = new Stopwatch();
            mainsw.Start();

            var sw = new Stopwatch();
            sw.Start();

            // use a Parallel ForEach to make this faster
            Parallel.ForEach(entities, (e, loopState) =>
            {

                if (e.Properties["ClientId"].Int32Value == clientid)
                {
                    var deleteOperation = TableOperation.Delete(e);
                    var res = table.Execute(deleteOperation);

                    // success, no content, delete was successful
                    if (res.HttpStatusCode == 204)
                    {
                        count++;
                    }

                    if ((count % 100) == 0)
                    {
                        sw.Stop();
                        var elapsed = sw.ElapsedMilliseconds;
                        Debug.WriteLine("Deleted {0} from {1} - last 100 took {2} ms.", count, tableName, elapsed);
                        sw.Reset();
                        sw.Start();
                    }
                }

            });


            //foreach (var e in entities)
            //{

            //    // The ClientId is an int32 in most tables
            //    if (e.Properties["ClientId"].Int32Value == clientid)
            //    {
            //        var deleteOperation = TableOperation.Delete(e);
            //        var res = table.Execute(deleteOperation);

            //        // success, no content, delete was successfule
            //        if (res.HttpStatusCode == 204)
            //        {
            //            count++;
            //        }

            //        if ((count % 100) == 0)
            //        {
            //            sw.Stop();
            //            var elapsed = sw.ElapsedMilliseconds;
            //            Debug.WriteLine("Deleted {0} - last 100 took {1} ms.", count, elapsed);
            //            sw.Reset();
            //            sw.Start();
            //        }
            //    }

            //}

            mainsw.Stop();
            var mainelapsed = mainsw.ElapsedMilliseconds;

            Debug.WriteLine("Entities deleted = {0}, elpased time was {1} ms", count, mainelapsed);

        }


        public void DeleteEntitiesInResult(TableQuerySegment<DynamicTableEntity> entities, CloudTable table, string tableName, int clientid, string customerPrefix)
        {
            var count = 0;

            var mainsw = new Stopwatch();
            mainsw.Start();

            var sw = new Stopwatch();
            sw.Start();

            // use a Parallel ForEach to make this faster
            Parallel.ForEach(entities, (e, loopState) =>
            {

                if (e.Properties["ClientId"].Int32Value == clientid && e.Properties["CustomerId"].StringValue.Contains(customerPrefix))
                {
                    var deleteOperation = TableOperation.Delete(e);
                    var res = table.Execute(deleteOperation);

                    // success, no content, delete was successful
                    if (res.HttpStatusCode == 204)
                    {
                        count++;
                    }

                    if ((count % 100) == 0)
                    {
                        sw.Stop();
                        var elapsed = sw.ElapsedMilliseconds;
                        Debug.WriteLine("Deleted {0} from {1} - last 100 took {2} ms.", count, tableName, elapsed);
                        sw.Reset();
                        sw.Start();
                    }
                }

            });

            mainsw.Stop();
            var mainelapsed = mainsw.ElapsedMilliseconds;

            Debug.WriteLine("Entities deleted = {0}, elpased time was {1} ms", count, mainelapsed);

        }



        /// <summary>
        /// Slower delete of entities.
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="table"></param>
        /// <param name="clientid"></param>
        public void SerialDeleteEntitiesInResult(TableQuerySegment<DynamicTableEntity> entities, CloudTable table, int clientid)
        {
            var count = 0;

            var mainsw = new Stopwatch();
            mainsw.Start();

            var sw = new Stopwatch();
            sw.Start();

            foreach (var e in entities)
            {

                // The ClientId is an int32 in most tables
                if (e.Properties["ClientId"].Int32Value == clientid)
                {
                    var deleteOperation = TableOperation.Delete(e);
                    var res = table.Execute(deleteOperation);

                    // success, no content, delete was successfule
                    if (res.HttpStatusCode == 204)
                    {
                        count++;
                    }

                    if ((count % 100) == 0)
                    {
                        sw.Stop();
                        var elapsed = sw.ElapsedMilliseconds;
                        Debug.WriteLine("Deleted {0} - last 100 took {1} ms.", count, elapsed);
                        sw.Reset();
                        sw.Start();
                    }
                }

            }

            mainsw.Stop();
            var mainelapsed = mainsw.ElapsedMilliseconds;

            Debug.WriteLine("Entities deleted = {0}, elpased time was {1} ms", count, mainelapsed);

        }


    }
}
