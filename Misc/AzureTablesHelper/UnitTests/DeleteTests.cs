﻿using System;
using System.Security.Cryptography;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AzureTablesHelper;
using System.Threading.Tasks;

namespace UnitTests
{
    [TestClass]
    public class DeleteTests
    {
        /// <summary>
        /// Use this to clear the given ClientID data from the single specified table.
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DeletefromTableBruteForce()
        {
            var mgr = new AzureTableManager();

            await mgr.DeleteFromTableForClientId( "dev", "AccountMeterLookup", 290);

        }

        /// <summary>
        /// Use this to clear the given ClientID data from the specified tables.
        /// This clear from all but the Ami related tables.
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DeletefromTables()
        {
            var mgr = new AzureTableManager();

            var t1 = mgr.DeleteFromTableForClientId("dev", "AccountMeterLookup", 290);
            var t2 = mgr.DeleteFromTableForClientId("dev", "AccountUpdates", 290);
            var t3 = mgr.DeleteFromTableForClientId("dev", "Billing", 290);
            var t4 = mgr.DeleteFromTableForClientId("dev", "ClientAccount", 290);
            var t5 = mgr.DeleteFromTableForClientId("dev", "CustomerPremiseProfile", 290);
            var t6 = mgr.DeleteFromTableForClientId("dev", "Insight", 290);
            var t7 = mgr.DeleteFromTableForClientId("dev", "Notification", 290);
            var t8 = mgr.DeleteFromTableForClientId("dev", "Subscription", 290);

            //var t3 = mgr.DeleteFromTableForClientId("dev", "Ami", 290);
            //var t3 = mgr.DeleteFromTableForClientId("dev", "AmiReading", 290);
            //var t3 = mgr.DeleteFromTableForClientId("dev", "TallAmi", 290);

            await Task.WhenAll(t1, t2, t3, t4, t5, t6, t7, t8);

        }

        /// <summary>
        /// Use this to clear the given ClientID data from the specified tables.
        /// This clear from all but the Ami related tables.
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DeletefromTables_SpecifyCustomerPrefix()
        {
            var environment = "qa";
            var customerPrefix = "mtest";
            var clientid = 87;

            var mgr = new AzureTableManager();

            var t1 = mgr.DeleteFromTableForClientId(environment, "AccountMeterLookup", clientid, customerPrefix);
            var t2 = mgr.DeleteFromTableForClientId(environment, "AccountUpdates", clientid, customerPrefix);
            var t3 = mgr.DeleteFromTableForClientId(environment, "Billing", clientid, customerPrefix);
            var t4 = mgr.DeleteFromTableForClientId(environment, "ClientAccount", clientid, customerPrefix);
            var t5 = mgr.DeleteFromTableForClientId(environment, "CustomerPremiseProfile", clientid, customerPrefix);
            var t7 = mgr.DeleteFromTableForClientId(environment, "Notification", clientid, customerPrefix);
            var t8 = mgr.DeleteFromTableForClientId(environment, "Subscription", clientid, customerPrefix);

            await Task.WhenAll(t1, t2, t3, t4, t5, t7, t8);

        }

    }
}
