﻿using System;
using TestHarness.Runner.TestCase;

namespace TestHarness.Runner
{
    public class TestRunner
    {
        static int Main(string[] args)
        {
            Console.WriteLine("View the report output here, then press any key to end the program...");

            string clientId = string.Empty;
            if (args.Length != 0)
            {
                System.Console.WriteLine("Please enter a Client Id:");
                clientId = args[0];
                Console.WriteLine(" " + clientId );
           }

            IDataReader factServicePointBillingReader = new FactServicePointBillingDataReader();
            factServicePointBillingReader.ClientId = clientId;
            DataComparator.Compare(factServicePointBillingReader);
            
            IDataReader dimCustomerReader = new DimCustomerDataReader();
            dimCustomerReader.ClientId = clientId;
            DataComparator.Compare(dimCustomerReader);

            IDataReader dimServicePointReader = new DimServicePointDataReader();
            dimServicePointReader.ClientId = clientId;
            DataComparator.Compare(dimServicePointReader);

            IDataReader dimPremiseReader = new DimPremiseDataReader();
            dimPremiseReader.ClientId = clientId;
            DataComparator.Compare(dimPremiseReader);

            Console.ReadKey();
            return 1;
        }
    }
}

