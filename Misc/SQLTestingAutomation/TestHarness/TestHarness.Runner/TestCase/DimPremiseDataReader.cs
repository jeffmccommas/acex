﻿using System;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace TestHarness.Runner.TestCase
{
    class DimPremiseDataReader : IDataReader
    {
        public string ClientId { get; set; }

        CustomConfigReader CustomConfig => GetCustomConfig();

        public DataTable ReadFileData()
        {
            DataTable dataTable = GetDataTable();
            using (var reader = new StreamReader(CustomConfig.FileLocation))
            {
                var line = reader.ReadLine();
                while (!reader.EndOfStream)
                {
                    line = reader.ReadLine();
                    if (!string.IsNullOrWhiteSpace(line))
                    {
                        var values = line.Split('|');
                        DataRow dataRow = dataTable.NewRow();
                        dataRow["premise_id"] = values[3];
                        dataTable.Rows.Add(dataRow);
                    }
                }
            }
            return dataTable;
        }

        public DataTable ReadSqlData()
        {
            string conStr = ConfigurationManager.ConnectionStrings["InsightsEntities"].ToString();
            var shardName = ConfigurationManager.AppSettings[ClientId].ToString();
            var connectionStringconfig = String.Format(conStr, shardName);
            DataTable dataTable = GetDataTable();

            using (SqlConnection conn = new SqlConnection(connectionStringconfig))
            {
                conn.Open();
                using (var command = new SqlCommand(CustomConfig.SqlQuery, conn))
                {
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        DataRow dataRow = dataTable.NewRow();
                        dataRow["premise_id"] = reader.GetString(0);
                        dataTable.Rows.Add(dataRow);
                    }

                    return dataTable;
                }

            }

        }
        public DataTable GetDataTable()
        {
            DataTable table = new DataTable();
            table.TableName = "DimPremise";

            // Declare DataColumn 
            DataColumn column;

            // Create premise_id column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "premise_id";
            table.Columns.Add(column);

            return table;
        }

        public CustomConfigReader GetCustomConfig()
        {
            return CustomConfigReader.GetInstance("customConfig/dimPremise");
        }


    }
}
