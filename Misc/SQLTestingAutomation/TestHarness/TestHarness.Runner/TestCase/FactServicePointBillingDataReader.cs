﻿using System;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace TestHarness.Runner.TestCase
{   
    public class FactServicePointBillingDataReader : IDataReader
    {
        public string ClientId { get; set; }

        CustomConfigReader CustomConfig => GetCustomConfig();

        public DataTable ReadFileData()
        {
            DataTable dataTable = GetDataTable();
            using (var reader = new StreamReader(CustomConfig.FileLocation))
            {
                var line = reader.ReadLine();
                while (!reader.EndOfStream)
                {
                    line = reader.ReadLine();
                    if (!string.IsNullOrWhiteSpace(line))
                    {
                        var values = line.Split('|');
                        DataRow dataRow = dataTable.NewRow();
                        dataRow["account_id"] = values[2];
                        dataRow["premise_id"] = values[3];
                        dataRow["service_read_enddate"] = values[30];
                        dataRow["bill_period_days"] = values[29];
                        dataTable.Rows.Add(dataRow);
                    }
                }
            }
            return dataTable;
        }

        public DataTable ReadSqlData()
        {
            string conStr= ConfigurationManager.ConnectionStrings["InsightsEntities"].ToString();
            var shardName = ConfigurationManager.AppSettings[ClientId].ToString();
            var connectionStringconfig = String.Format(conStr, shardName);
            DataTable dataTable = GetDataTable();
            using (SqlConnection conn = new SqlConnection(connectionStringconfig))
            {
                conn.Open();
                using (var command = new SqlCommand(CustomConfig.SqlQuery, conn))
                {
                    var reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            DataRow dataRow = dataTable.NewRow();
                            dataRow["account_id"] = reader.GetString(0);
                            dataRow["premise_id"] = reader.GetString(1);
                            dataRow["service_read_enddate"] = reader.GetInt32(2).ToString();
                            dataRow["bill_period_days"] = reader.GetInt32(3).ToString();
                            dataTable.Rows.Add(dataRow);
                        }

                        return dataTable;
                    }
                  
                }

            }
        

        public DataTable GetDataTable()
        {
            DataTable table = new DataTable();
            table.TableName = "FactServiceBillingPoint";

            // Declare DataColumn 
            DataColumn column;

            // Create new DataColumn account_id, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "account_id";
            table.Columns.Add(column);

            // Create premise_id column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "premise_id";
            table.Columns.Add(column);

            // Create service_read_enddate column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "service_read_enddate";
            table.Columns.Add(column);

            // Create bill_period_days column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "bill_period_days";
            table.Columns.Add(column);

            return table;
        }

        public CustomConfigReader GetCustomConfig()
        {
            return CustomConfigReader.GetInstance("customConfig/factServicePointBilling");
        }
    }
}
