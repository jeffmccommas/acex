﻿using System.Data;

namespace TestHarness.Runner
{
    public interface IDataReader
    {
        string ClientId { get; set; }
        CustomConfigReader GetCustomConfig();
        DataTable ReadSqlData();
        //Reads File data from File and stores data in DataRows to create DataTable 
        DataTable ReadFileData();
        //Creates Columns in DataTable 
        DataTable GetDataTable();
    }
}
