﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;
using System.Xml;

namespace TestHarness.Runner
{
    public class ConfigReader
    {
        //private NameValueCollection _fileDbNameMapping;
        //private NameValueCollection _whereClauseParams;
        private string _fileLocation;
        private string _queryString;
        private static ConfigReader _configReader;

        private ConfigReader()
        {

        }

        public static ConfigReader GetInstance()
        {
            if (_configReader != null)
                return _configReader;

            _configReader = new ConfigReader();
            return _configReader;

        }
        //public NameValueCollection FileDbNameMapping
        //{
        //    get
        //    {
        //        if (_fileDbNameMapping != null)
        //            return _fileDbNameMapping;

        //        _fileDbNameMapping = ConfigurationManager.GetSection("InputParams/FileDbNameMapping") as NameValueCollection;
        //        return _fileDbNameMapping;
        //    }
        //}

        //public NameValueCollection WhereClauseParams
        //{
        //    get
        //    {
        //        if (_whereClauseParams != null)
        //            return _whereClauseParams;

        //        _whereClauseParams = ConfigurationManager.GetSection("InputParams/WhereClauseParams") as NameValueCollection;
        //        return _whereClauseParams;
        //    }
        //}

        public string FileLocation
        {
            get
            {
                if (!string.IsNullOrEmpty(_fileLocation))
                    return _fileLocation;

                var config = ConfigurationManager.GetSection("InputParams/FileAndDbInfo") as NameValueCollection;
                _fileLocation = config["filePath"];
                return _fileLocation;
            }
        }

        public string QueryString
        {
            get
            {
                if (!string.IsNullOrEmpty(_queryString))
                    return _queryString;

                var config = ConfigurationManager.GetSection("InputParams/FileAndDbInfo") as NameValueCollection;
                _queryString = config["selectQuery"];
                return _queryString;
            }
        }
    }
}
