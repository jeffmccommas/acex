﻿using System.Xml;
using System.IO;

namespace TestHarness.Runner
{
    public class CustomConfigReader
    {
        private string _fileLocation;
        private string _queryString;
        private XmlDocument XmlDoc;
        private string _xmlNodePath;

        private CustomConfigReader(string readerPath)
        {
            XmlDoc = new XmlDocument();
            var custCfgFilePath = Path.Combine(Directory.GetCurrentDirectory(), "CustomConfig.xml");
            XmlDoc.Load(custCfgFilePath);
            _xmlNodePath = readerPath;
        }

        public static CustomConfigReader GetInstance(string readerPath = null)
        {
            return new CustomConfigReader(readerPath);
        }

        public string FileLocation
        {
            get
            {
                if (!string.IsNullOrEmpty(_fileLocation))
                    return _fileLocation;

                _fileLocation = XmlDoc.SelectSingleNode(_xmlNodePath+"/fileName").InnerText;

                return _fileLocation;
            }
        }
        public string SqlQuery
        {
            get
            {
                if (!string.IsNullOrEmpty(_queryString))
                    return _queryString;
              
                _queryString = XmlDoc.SelectSingleNode(_xmlNodePath+"/sqlQuery").InnerText;

                return _queryString;
            }
        }
    }
}
