﻿using System;
using System.Data;
using System.Linq;

namespace TestHarness.Runner
{
    class DataComparator
    {
        public static void Compare(IDataReader reader)
        {
            DataTable fileDataTable = reader.ReadFileData();
            DataTable sqlDataTable = reader.ReadSqlData();

            PrintTable(fileDataTable);
            PrintTable(sqlDataTable);

            // Take INTERSECTION of two DataTables. If any common DataRows are found that passes the test.
            var result = fileDataTable.AsEnumerable().Intersect(sqlDataTable.AsEnumerable(), DataRowComparer.Default);
            if (result.Any<DataRow>())
                Console.WriteLine("\n\nTEST PASSED");
            else
                Console.WriteLine("\n\nTEST FAILED");
        }

        private static void PrintTable(DataTable table)
        {
            Console.WriteLine("---------------------{0}----------------------", table.TableName);
            foreach (DataRow row in table.Rows)
            {
                Console.WriteLine("--- Row ---");
                foreach (var item in row.ItemArray)
                {
                    Console.Write("Item: "); // Print label.
                    Console.WriteLine(item);
                }
            }
        }
    }
}
