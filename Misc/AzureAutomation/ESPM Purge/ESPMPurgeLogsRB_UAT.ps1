<# Revision: 20171017-001 #>
<#Description: This runbook is used to schedule runnign the proc PurgeESPMLogs that purges ESPM Logs from the insights Database#>
workflow ESPMPurgeLogsRB
{



   Write-Verbose "PURGE ESPM LOGS START BEFORE INLINESCRIPT"
   Write-Verbose "INFO: Starting: " 

    inlinescript
    {
        Write-Verbose "JOB START"
        
        Write-Verbose "INFO: Job Starting: " 
        # Create connection to Master DB
        $MasterDatabaseConnection = New-Object System.Data.SqlClient.SqlConnection
        $MasterDatabaseConnection.ConnectionString = "Data Source=aceusql1c0.database.windows.net;Initial Catalog=Insights;Integrated Security=False;User ID=Aclweb@aceusql1c0;Password=Acl@r@393;Connect Timeout=60;Encrypt=False;TrustServerCertificate=False"
        $MasterDatabaseConnection.Open()

        Write-Verbose "CONNECTION OPEN TO DATABASE"
        
        Write-Verbose "INFO: Connection Open to databse at :" 

        # Create command
        $MasterDatabaseCommand = New-Object System.Data.SqlClient.SqlCommand
        $MasterDatabaseCommand.Connection = $MasterDatabaseConnection
        $MasterDatabaseCommand.CommandText = "dbo.PurgeESPMLogs"

        
        Write-Verbose "INFO: DATABASE COMMAND TEXT ASSIGNED : " 

        # Execute the query
        $MasterDatabaseCommand.ExecuteNonQuery()
        
        Write-Verbose "INFO: EXECUTING QUERY : " 
        

        # Close connection to Master DB
        $MasterDatabaseConnection.Close() 
        
        Write-Verbose "CONNECTION CLOSED : " 
        
        
    }    
        $Start = [System.DateTime]::Now
        Write-Verbose "WORK END - AFTER INLINESCRIPT : " 
}