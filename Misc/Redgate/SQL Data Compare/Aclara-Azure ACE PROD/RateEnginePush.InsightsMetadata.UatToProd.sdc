<?xml version="1.0" encoding="utf-16" standalone="yes"?>
<!--
SQL Data Compare
SQL Data Compare
Version:11.1.3.23-->
<Project version="3" type="SQLComparisonToolsProject">
  <DataSource1 version="3" type="LiveDatabaseSource">
    <ServerName>aceUsql1c0.database.windows.net</ServerName>
    <DatabaseName>InsightsMetaData</DatabaseName>
    <Username>AclaraCEAdmin@aceUsql1c0</Username>
    <SavePassword>True</SavePassword>
    <Password encrypted="1">DjgY3mz+gVwyIhIgppWTyw==</Password>
    <ScriptFolderLocation />
    <MigrationsFolderLocation />
    <IntegratedSecurity>False</IntegratedSecurity>
  </DataSource1>
  <DataSource2 version="3" type="LiveDatabaseSource">
    <ServerName>acePsql1c0.database.windows.net</ServerName>
    <DatabaseName>InsightsMetaData</DatabaseName>
    <Username>AclaraCEAdmin@acePsql1c0</Username>
    <SavePassword>True</SavePassword>
    <Password encrypted="1">A4iCOgC8P7cXsxr64NVmLg==</Password>
    <ScriptFolderLocation />
    <MigrationsFolderLocation />
    <IntegratedSecurity>False</IntegratedSecurity>
  </DataSource2>
  <LastCompared>03/09/2017 14:39:31</LastCompared>
  <Options>Default</Options>
  <InRecycleBin>False</InRecycleBin>
  <Direction>0</Direction>
  <ProjectFilter version="1" type="DifferenceFilter">
    <FilterCaseSensitive>False</FilterCaseSensitive>
    <Filters version="1">
      <None version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </None>
      <Assembly version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Assembly>
      <AsymmetricKey version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </AsymmetricKey>
      <Certificate version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Certificate>
      <Contract version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Contract>
      <DdlTrigger version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </DdlTrigger>
      <Default version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Default>
      <ExtendedProperty version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </ExtendedProperty>
      <EventNotification version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </EventNotification>
      <FullTextCatalog version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </FullTextCatalog>
      <FullTextStoplist version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </FullTextStoplist>
      <Function version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Function>
      <MessageType version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </MessageType>
      <PartitionFunction version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </PartitionFunction>
      <PartitionScheme version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </PartitionScheme>
      <Queue version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Queue>
      <Role version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Role>
      <Route version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Route>
      <Rule version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Rule>
      <Schema version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Schema>
      <SearchPropertyList version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </SearchPropertyList>
      <Sequence version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Sequence>
      <Service version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Service>
      <ServiceBinding version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </ServiceBinding>
      <StoredProcedure version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </StoredProcedure>
      <SymmetricKey version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </SymmetricKey>
      <Synonym version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Synonym>
      <Table version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Table>
      <User version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </User>
      <UserDefinedType version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </UserDefinedType>
      <View version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </View>
      <XmlSchemaCollection version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </XmlSchemaCollection>
    </Filters>
  </ProjectFilter>
  <ProjectFilterName />
  <UserNote />
  <SelectedSyncObjects version="1" type="SelectedSyncObjects">
    <Schemas type="ListString" version="2" />
    <Grouping type="ListByte" version="2">
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
    </Grouping>
    <SelectAll>False</SelectAll>
  </SelectedSyncObjects>
  <SCGroupingStyle>0</SCGroupingStyle>
  <SQLOptions>10</SQLOptions>
  <MappingOptions>82</MappingOptions>
  <ComparisonOptions>2</ComparisonOptions>
  <TableActions type="ArrayList" version="1">
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[AuthAudit]:null</Table1>
      <Table2>null:[rateengine].[AuthAudit]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[BaselineRule]:null</Table1>
      <Table2>null:[rateengine].[BaselineRule]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[BilledDemandType]:null</Table1>
      <Table2>null:[rateengine].[BilledDemandType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[ChargeCalcType]:null</Table1>
      <Table2>null:[rateengine].[ChargeCalcType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[UseTypeMap]:null</Table1>
      <Table2>null:[rateengine].[UseTypeMap]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[UseCharge]:null</Table1>
      <Table2>null:[rateengine].[UseCharge]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[UnitOfMeasure]:null</Table1>
      <Table2>null:[rateengine].[UnitOfMeasure]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[tmpTOUExclude]:null</Table1>
      <Table2>null:[rateengine].[tmpTOUExclude]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[tmpEguideRatesToMove]:null</Table1>
      <Table2>null:[rateengine].[tmpEguideRatesToMove]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[TimeOfUseBoundary]:null</Table1>
      <Table2>null:[rateengine].[TimeOfUseBoundary]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[TimeOfUse]:null</Table1>
      <Table2>null:[rateengine].[TimeOfUse]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[TierStructureType]:null</Table1>
      <Table2>null:[rateengine].[TierStructureType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[TierBoundary]:null</Table1>
      <Table2>null:[rateengine].[TierBoundary]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[Tier]:null</Table1>
      <Table2>null:[rateengine].[Tier]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[TerritoryLookupGroup]:null</Table1>
      <Table2>null:[rateengine].[TerritoryLookupGroup]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[TerritoryLookup]:null</Table1>
      <Table2>null:[rateengine].[TerritoryLookup]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[TerritoryBaseRateClass]:null</Table1>
      <Table2>null:[rateengine].[TerritoryBaseRateClass]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[TaxCharge]:null</Table1>
      <Table2>null:[rateengine].[TaxCharge]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[StepBoundary]:null</Table1>
      <Table2>null:[rateengine].[StepBoundary]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[Step]:null</Table1>
      <Table2>null:[rateengine].[Step]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[Setting]:null</Table1>
      <Table2>null:[rateengine].[Setting]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[ServiceType]:null</Table1>
      <Table2>null:[rateengine].[ServiceType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[ServiceCharge]:null</Table1>
      <Table2>null:[rateengine].[ServiceCharge]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[SeasonBoundary]:null</Table1>
      <Table2>null:[rateengine].[SeasonBoundary]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[SeasonalProrateType]:null</Table1>
      <Table2>null:[rateengine].[SeasonalProrateType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[SeasonalDemandProrateType]:null</Table1>
      <Table2>null:[rateengine].[SeasonalDemandProrateType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[Season]:null</Table1>
      <Table2>null:[rateengine].[Season]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RTPStream]:null</Table1>
      <Table2>null:[rateengine].[RTPStream]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RTPPrelim]:null</Table1>
      <Table2>null:[rateengine].[RTPPrelim]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RTPGroup]:null</Table1>
      <Table2>null:[rateengine].[RTPGroup]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RTPFinal]:null</Table1>
      <Table2>null:[rateengine].[RTPFinal]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RTPEventSimulateDuration]:null</Table1>
      <Table2>null:[rateengine].[RTPEventSimulateDuration]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RTPEventSimulate]:null</Table1>
      <Table2>null:[rateengine].[RTPEventSimulate]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RTPDayAhead]:null</Table1>
      <Table2>null:[rateengine].[RTPDayAhead]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RTPCriticalPeak]:null</Table1>
      <Table2>null:[rateengine].[RTPCriticalPeak]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RebateClass]:null</Table1>
      <Table2>null:[rateengine].[RebateClass]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RateMasterText]:null</Table1>
      <Table2>null:[rateengine].[RateMasterText]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RateMasterServiceTerritory]:null</Table1>
      <Table2>null:[rateengine].[RateMasterServiceTerritory]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RateMasterChildren]:null</Table1>
      <Table2>null:[rateengine].[RateMasterChildren]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RateMaster]:null</Table1>
      <Table2>null:[rateengine].[RateMaster]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RateMaintData]:null</Table1>
      <Table2>null:[rateengine].[RateMaintData]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RateDefinition]:null</Table1>
      <Table2>null:[rateengine].[RateDefinition]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[RateCompanySetting]:null</Table1>
      <Table2>null:[rateengine].[RateCompanySetting]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[ProrateType]:null</Table1>
      <Table2>null:[rateengine].[ProrateType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[Pollution]:null</Table1>
      <Table2>null:[rateengine].[Pollution]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[PartType]:null</Table1>
      <Table2>null:[rateengine].[PartType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[NetMeteringType]:null</Table1>
      <Table2>null:[rateengine].[NetMeteringType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[MinimumCharge]:null</Table1>
      <Table2>null:[rateengine].[MinimumCharge]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[LoadType]:null</Table1>
      <Table2>null:[rateengine].[LoadType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[Language]:null</Table1>
      <Table2>null:[rateengine].[Language]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[HolidayGroup]:null</Table1>
      <Table2>null:[rateengine].[HolidayGroup]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[Holiday]:null</Table1>
      <Table2>null:[rateengine].[Holiday]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[FuelCostRecoveryGroup]:null</Table1>
      <Table2>null:[rateengine].[FuelCostRecoveryGroup]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[FuelCostRecovery]:null</Table1>
      <Table2>null:[rateengine].[FuelCostRecovery]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[Fuel]:null</Table1>
      <Table2>null:[rateengine].[Fuel]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[DifferenceType]:null</Table1>
      <Table2>null:[rateengine].[DifferenceType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[DifferenceCategory]:null</Table1>
      <Table2>null:[rateengine].[DifferenceCategory]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[Difference]:null</Table1>
      <Table2>null:[rateengine].[Difference]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[DemandStructureType]:null</Table1>
      <Table2>null:[rateengine].[DemandStructureType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[DemandProrateType]:null</Table1>
      <Table2>null:[rateengine].[DemandProrateType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[DayType]:null</Table1>
      <Table2>null:[rateengine].[DayType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[CustomerType]:null</Table1>
      <Table2>null:[rateengine].[CustomerType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>MapTables</action>
      <Table1>[dbo].[CostType]:null</Table1>
      <Table2>null:[rateengine].[CostType]</Table2>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>UnmapTables</action>
      <val>[dbo].[tmpTOUExclude]:[rateengine].[tmpTOUExclude]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>UnmapTables</action>
      <val>[dbo].[tmpEguideRatesToMove]:[rateengine].[tmpEguideRatesToMove]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectAll</action>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[BaselineRule]:[rateengine].[BaselineRule]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[BilledDemandType]:[rateengine].[BilledDemandType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[ChargeCalcType]:[rateengine].[ChargeCalcType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[CostType]:[rateengine].[CostType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[CustomerType]:[rateengine].[CustomerType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[DayType]:[rateengine].[DayType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[DemandProrateType]:[rateengine].[DemandProrateType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[DemandStructureType]:[rateengine].[DemandStructureType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[Difference]:[rateengine].[Difference]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[DifferenceCategory]:[rateengine].[DifferenceCategory]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[DifferenceType]:[rateengine].[DifferenceType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[Fuel]:[rateengine].[Fuel]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[FuelCostRecovery]:[rateengine].[FuelCostRecovery]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[FuelCostRecoveryGroup]:[rateengine].[FuelCostRecoveryGroup]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[Holiday]:[rateengine].[Holiday]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[HolidayGroup]:[rateengine].[HolidayGroup]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[Language]:[rateengine].[Language]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[LoadType]:[rateengine].[LoadType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[MinimumCharge]:[rateengine].[MinimumCharge]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[Log]:[rateengine].[Log]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[NetMeteringType]:[rateengine].[NetMeteringType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[PartType]:[rateengine].[PartType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[Pollution]:[rateengine].[Pollution]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[ProrateType]:[rateengine].[ProrateType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RateCompanySetting]:[rateengine].[RateCompanySetting]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RateDefinition]:[rateengine].[RateDefinition]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RateMaintData]:[rateengine].[RateMaintData]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RateMaster]:[rateengine].[RateMaster]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RateMasterChildren]:[rateengine].[RateMasterChildren]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RateMasterServiceTerritory]:[rateengine].[RateMasterServiceTerritory]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RateMasterText]:[rateengine].[RateMasterText]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RebateClass]:[rateengine].[RebateClass]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RTPCriticalPeak]:[rateengine].[RTPCriticalPeak]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RTPDayAhead]:[rateengine].[RTPDayAhead]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RTPEventSimulate]:[rateengine].[RTPEventSimulate]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RTPEventSimulateDuration]:[rateengine].[RTPEventSimulateDuration]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RTPFinal]:[rateengine].[RTPFinal]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RTPGroup]:[rateengine].[RTPGroup]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RTPPrelim]:[rateengine].[RTPPrelim]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[RTPStream]:[rateengine].[RTPStream]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[Season]:[rateengine].[Season]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[SeasonalDemandProrateType]:[rateengine].[SeasonalDemandProrateType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[SeasonalProrateType]:[rateengine].[SeasonalProrateType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[SeasonBoundary]:[rateengine].[SeasonBoundary]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[ServiceCharge]:[rateengine].[ServiceCharge]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[ServiceType]:[rateengine].[ServiceType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[Setting]:[rateengine].[Setting]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[Step]:[rateengine].[Step]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[StepBoundary]:[rateengine].[StepBoundary]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[TaxCharge]:[rateengine].[TaxCharge]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[TerritoryBaseRateClass]:[rateengine].[TerritoryBaseRateClass]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[TerritoryLookup]:[rateengine].[TerritoryLookup]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[TerritoryLookupGroup]:[rateengine].[TerritoryLookupGroup]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[Tier]:[rateengine].[Tier]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[TierBoundary]:[rateengine].[TierBoundary]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[TierStructureType]:[rateengine].[TierStructureType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[TimeOfUse]:[rateengine].[TimeOfUse]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[TimeOfUseBoundary]:[rateengine].[TimeOfUseBoundary]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[UnitOfMeasure]:[rateengine].[UnitOfMeasure]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[UseCharge]:[rateengine].[UseCharge]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[rateengine].[UseTypeMap]:[rateengine].[UseTypeMap]</val>
    </value>
  </TableActions>
  <SessionSettings>14</SessionSettings>
  <DCGroupingStyle>0</DCGroupingStyle>
  <SC_DeploymentOptions version="1" type="SC_DeploymentOptions">
    <BackupOptions version="1" type="BackupOptions">
      <BackupProvider>Native</BackupProvider>
      <TypeOfBackup>Full</TypeOfBackup>
      <Folder />
      <Filename />
      <SqbLicenseType>None</SqbLicenseType>
      <SqbVersion>0</SqbVersion>
      <DefaultNativeFolder />
      <DefaultSqbFolder />
      <Password encrypted="1" />
      <NameFileAutomatically>False</NameFileAutomatically>
      <OverwriteIfExists>False</OverwriteIfExists>
      <CompressionLevel>0</CompressionLevel>
      <EncryptionLevel>None</EncryptionLevel>
      <ThreadCount>0</ThreadCount>
      <BackupEnabled>False</BackupEnabled>
    </BackupOptions>
  </SC_DeploymentOptions>
</Project>