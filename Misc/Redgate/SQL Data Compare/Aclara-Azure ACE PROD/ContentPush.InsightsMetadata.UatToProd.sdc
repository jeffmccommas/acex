<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<!--
SQL Data Compare
SQL Data Compare
Version:11.1.3.23-->
<Project version="3" type="SQLComparisonToolsProject">
  <DataSource1 version="3" type="LiveDatabaseSource">
    <ServerName>aceUsql1c0.database.windows.net</ServerName>
    <DatabaseName>InsightsMetadata</DatabaseName>
    <Username>AclaraCEAdmin@aceUsql1c0</Username>
    <SavePassword>True</SavePassword>
    <Password encrypted="1">DjgY3mz+gVwyIhIgppWTyw==</Password>
    <ScriptFolderLocation />
    <MigrationsFolderLocation />
    <IntegratedSecurity>False</IntegratedSecurity>
  </DataSource1>
  <DataSource2 version="3" type="LiveDatabaseSource">
    <ServerName>acePsql1c0.database.windows.net</ServerName>
    <DatabaseName>InsightsMetadata</DatabaseName>
    <Username>AclaraCEAdmin@acePsql1c0</Username>
    <SavePassword>True</SavePassword>
    <Password encrypted="1">A4iCOgC8P7cXsxr64NVmLg==</Password>
    <ScriptFolderLocation />
    <MigrationsFolderLocation />
    <IntegratedSecurity>False</IntegratedSecurity>
  </DataSource2>
  <LastCompared>07/26/2016 16:14:13</LastCompared>
  <Options>Default</Options>
  <InRecycleBin>False</InRecycleBin>
  <Direction>0</Direction>
  <ProjectFilter version="1" type="DifferenceFilter">
    <FilterCaseSensitive>False</FilterCaseSensitive>
    <Filters version="1">
      <None version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </None>
      <Assembly version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Assembly>
      <AsymmetricKey version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </AsymmetricKey>
      <Certificate version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Certificate>
      <Contract version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Contract>
      <DdlTrigger version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </DdlTrigger>
      <Default version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Default>
      <ExtendedProperty version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </ExtendedProperty>
      <EventNotification version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </EventNotification>
      <FullTextCatalog version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </FullTextCatalog>
      <FullTextStoplist version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </FullTextStoplist>
      <Function version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Function>
      <MessageType version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </MessageType>
      <PartitionFunction version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </PartitionFunction>
      <PartitionScheme version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </PartitionScheme>
      <Queue version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Queue>
      <Role version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Role>
      <Route version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Route>
      <Rule version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Rule>
      <Schema version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Schema>
      <SearchPropertyList version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </SearchPropertyList>
      <Sequence version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Sequence>
      <Service version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Service>
      <ServiceBinding version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </ServiceBinding>
      <StoredProcedure version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </StoredProcedure>
      <SymmetricKey version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </SymmetricKey>
      <Synonym version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Synonym>
      <Table version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Table>
      <User version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </User>
      <UserDefinedType version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </UserDefinedType>
      <View version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </View>
      <XmlSchemaCollection version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </XmlSchemaCollection>
    </Filters>
  </ProjectFilter>
  <ProjectFilterName />
  <UserNote />
  <SelectedSyncObjects version="1" type="SelectedSyncObjects">
    <Schemas type="ListString" version="2">
      <value type="string">UgBlAGQARwBhAHQAZQBfAFMAUQBMAEMAbwBtAHAAYQByAGUAXwBEAGUAZgBhAHUAbAB0AF8AUwBjAGgAZQBtAGEA</value>
    </Schemas>
    <UgBlAGQARwBhAHQAZQBfAFMAUQBMAEMAbwBtAHAAYQByAGUAXwBEAGUAZgBhAHUAbAB0AF8AUwBjAGgAZQBtAGEA>V4lv1e067I8aKBLHWsq0SQgCAOUQ0mCesTRnxjZlnqq3b+EIAgDK5431DOkibLfZAmc/Hb4+CAIAct0SUp55lD4VcgypyFWexAgCAPolfIsenJn8f1aLZ/LamswIAgAb4Qtlq5+qEnCazGJyDqEICAIA/Le/q9uMcj7AK98p9Az//QoAAA__</UgBlAGQARwBhAHQAZQBfAFMAUQBMAEMAbwBtAHAAYQByAGUAXwBEAGUAZgBhAHUAbAB0AF8AUwBjAGgAZQBtAGEA>
    <Grouping type="ListByte" version="2">
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
    </Grouping>
    <SelectAll>False</SelectAll>
  </SelectedSyncObjects>
  <SCGroupingStyle>0</SCGroupingStyle>
  <SQLOptions>10</SQLOptions>
  <MappingOptions>82</MappingOptions>
  <ComparisonOptions>2</ComparisonOptions>
  <TableActions type="ArrayList" version="1">
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[dbo].[ClientFilePurgePolicy]:[dbo].[ClientFilePurgePolicy]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[dbo].[ClientPropertyData]:[dbo].[ClientPropertyData]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[dbo].[UtilityPrograms]:[dbo].[UtilityPrograms]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[admin].[AppTaskHeartbeat]:[admin].[AppTaskHeartbeat]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[admin].[AppTaskRunStatus]:[admin].[AppTaskRunStatus]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[admin].[AppTasks]:[admin].[AppTasks]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[admin].[CustomerExportConfiguration]:[admin].[CustomerExportConfiguration]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[admin].[ETLInfo]:[admin].[ETLInfo]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[audit].[ActionItemImportErrors]:[audit].[ActionItemImportErrors]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[audit].[BillImportErrors]:[audit].[BillImportErrors]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[audit].[CustomerImportErrors]:[audit].[CustomerImportErrors]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[audit].[DisAggErrorLog]:[audit].[DisAggErrorLog]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[audit].[ExecutionLog]:[audit].[ExecutionLog]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[audit].[MeasureSavingsErrorLog]:[audit].[MeasureSavingsErrorLog]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[audit].[ProfileImportErrors]:[audit].[ProfileImportErrors]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[DatabaseDiagrams]:[cm].[DatabaseDiagrams]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EMApplianceRegion]:[cm].[EMApplianceRegion]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EMDailyWeatherDetail]:[cm].[EMDailyWeatherDetail]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EMDegreeDay]:[cm].[EMDegreeDay]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EMEvaporation]:[cm].[EMEvaporation]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EMSolar]:[cm].[EMSolar]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ContentCache]:[cm].[ContentCache]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EMTemperatureProfile]:[cm].[EMTemperatureProfile]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EMTemperatureRegion]:[cm].[EMTemperatureRegion]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EMZipcode]:[cm].[EMZipcode]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EnvironmentConfiguration]:[cm].[EnvironmentConfiguration]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[__ShardManagement].[ShardsLocal]:[__ShardManagement].[ShardsLocal]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[__ShardManagement].[ShardMapsLocal]:[__ShardManagement].[ShardMapsLocal]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[__ShardManagement].[ShardMappingsLocal]:[__ShardManagement].[ShardMappingsLocal]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[__ShardManagement].[ShardMapManagerLocal]:[__ShardManagement].[ShardMapManagerLocal]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientAction]:[cm].[ClientAction]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientActionSavings]:[cm].[ClientActionSavings]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientAppliance]:[cm].[ClientAppliance]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientAsset]:[cm].[ClientAsset]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientBenchmarkGroup]:[cm].[ClientBenchmarkGroup]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientCommodity]:[cm].[ClientCommodity]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientCondition]:[cm].[ClientCondition]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientConfiguration]:[cm].[ClientConfiguration]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientConfigurationBulk]:[cm].[ClientConfigurationBulk]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientCurrency]:[cm].[ClientCurrency]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientEnduse]:[cm].[ClientEnduse]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientEnumeration]:[cm].[ClientEnumeration]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientEnumerationItem]:[cm].[ClientEnumerationItem]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientExpression]:[cm].[ClientExpression]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientFileContent]:[cm].[ClientFileContent]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientLayout]:[cm].[ClientLayout]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientMeasurement]:[cm].[ClientMeasurement]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientProfileAttribute]:[cm].[ClientProfileAttribute]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientProfileDefault]:[cm].[ClientProfileDefault]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientProfileDefaultCollection]:[cm].[ClientProfileDefaultCollection]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientProfileOption]:[cm].[ClientProfileOption]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientProfileSection]:[cm].[ClientProfileSection]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientSeason]:[cm].[ClientSeason]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientTab]:[cm].[ClientTab]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientTextContent]:[cm].[ClientTextContent]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientUOM]:[cm].[ClientUOM]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientWhatIfData]:[cm].[ClientWhatIfData]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[ClientWidget]:[cm].[ClientWidget]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeAction]:[cm].[TypeAction]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeActionStatus]:[cm].[TypeActionStatus]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeActionType]:[cm].[TypeActionType]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeAppliance]:[cm].[TypeAppliance]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeAsset]:[cm].[TypeAsset]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeBenchmarkGroup]:[cm].[TypeBenchmarkGroup]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeCategory]:[cm].[TypeCategory]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeCommodity]:[cm].[TypeCommodity]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeCondition]:[cm].[TypeCondition]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeConfiguration]:[cm].[TypeConfiguration]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeConfigurationBulk]:[cm].[TypeConfigurationBulk]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeCurrency]:[cm].[TypeCurrency]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeDifficulty]:[cm].[TypeDifficulty]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeEnduse]:[cm].[TypeEnduse]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeEnduseCategory]:[cm].[TypeEnduseCategory]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeEntityLevel]:[cm].[TypeEntityLevel]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeEnumeration]:[cm].[TypeEnumeration]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeEnumerationItem]:[cm].[TypeEnumerationItem]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeEnvironment]:[cm].[TypeEnvironment]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeExpression]:[cm].[TypeExpression]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeFileContent]:[cm].[TypeFileContent]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeHabitInterval]:[cm].[TypeHabitInterval]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeLayout]:[cm].[TypeLayout]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeLocale]:[cm].[TypeLocale]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeMeasurement]:[cm].[TypeMeasurement]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeProfileAttribute]:[cm].[TypeProfileAttribute]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeProfileAttributeType]:[cm].[TypeProfileAttributeType]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeProfileDefault]:[cm].[TypeProfileDefault]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeProfileDefaultCollection]:[cm].[TypeProfileDefaultCollection]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeProfileOption]:[cm].[TypeProfileOption]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeProfileSection]:[cm].[TypeProfileSection]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeProfileSource]:[cm].[TypeProfileSource]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeQuality]:[cm].[TypeQuality]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeSeason]:[cm].[TypeSeason]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeTab]:[cm].[TypeTab]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeTabType]:[cm].[TypeTabType]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeTextContent]:[cm].[TypeTextContent]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeUom]:[cm].[TypeUom]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeWhatIfData]:[cm].[TypeWhatIfData]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeWidget]:[cm].[TypeWidget]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectColumn</action>
      <ColumnName>DateUpdated:DateUpdated</ColumnName>
      <TableName>[cm].[TypeWidgetType]:[cm].[TypeWidgetType]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[dbo].[Client]:[dbo].[Client]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[AuthAudit]:[rateengine].[AuthAudit]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[BaselineRule]:[rateengine].[BaselineRule]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[BilledDemandType]:[rateengine].[BilledDemandType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[ChargeCalcType]:[rateengine].[ChargeCalcType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[CostType]:[rateengine].[CostType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[CustomerType]:[rateengine].[CustomerType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[DayType]:[rateengine].[DayType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[DemandProrateType]:[rateengine].[DemandProrateType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[DemandStructureType]:[rateengine].[DemandStructureType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[Difference]:[rateengine].[Difference]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[DifferenceCategory]:[rateengine].[DifferenceCategory]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[DifferenceType]:[rateengine].[DifferenceType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[Fuel]:[rateengine].[Fuel]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[FuelCostRecovery]:[rateengine].[FuelCostRecovery]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[FuelCostRecoveryGroup]:[rateengine].[FuelCostRecoveryGroup]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[Holiday]:[rateengine].[Holiday]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[HolidayGroup]:[rateengine].[HolidayGroup]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[Language]:[rateengine].[Language]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[LoadType]:[rateengine].[LoadType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[Log]:[rateengine].[Log]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[MinimumCharge]:[rateengine].[MinimumCharge]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[NetMeteringType]:[rateengine].[NetMeteringType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[PartType]:[rateengine].[PartType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[Pollution]:[rateengine].[Pollution]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[ProrateType]:[rateengine].[ProrateType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RateCompanySetting]:[rateengine].[RateCompanySetting]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RateDefinition]:[rateengine].[RateDefinition]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RateMaintData]:[rateengine].[RateMaintData]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RateMaster]:[rateengine].[RateMaster]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RateMasterChildren]:[rateengine].[RateMasterChildren]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RateMasterServiceTerritory]:[rateengine].[RateMasterServiceTerritory]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RateMasterText]:[rateengine].[RateMasterText]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RebateClass]:[rateengine].[RebateClass]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RTPCriticalPeak]:[rateengine].[RTPCriticalPeak]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RTPDayAhead]:[rateengine].[RTPDayAhead]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RTPEventSimulate]:[rateengine].[RTPEventSimulate]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RTPEventSimulateDuration]:[rateengine].[RTPEventSimulateDuration]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RTPFinal]:[rateengine].[RTPFinal]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RTPGroup]:[rateengine].[RTPGroup]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RTPPrelim]:[rateengine].[RTPPrelim]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[RTPStream]:[rateengine].[RTPStream]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[Season]:[rateengine].[Season]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[SeasonalDemandProrateType]:[rateengine].[SeasonalDemandProrateType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[SeasonalProrateType]:[rateengine].[SeasonalProrateType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[SeasonBoundary]:[rateengine].[SeasonBoundary]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[ServiceCharge]:[rateengine].[ServiceCharge]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[ServiceType]:[rateengine].[ServiceType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[Setting]:[rateengine].[Setting]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[Step]:[rateengine].[Step]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[StepBoundary]:[rateengine].[StepBoundary]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[TaxCharge]:[rateengine].[TaxCharge]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[TerritoryBaseRateClass]:[rateengine].[TerritoryBaseRateClass]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[TerritoryLookup]:[rateengine].[TerritoryLookup]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[TerritoryLookupGroup]:[rateengine].[TerritoryLookupGroup]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[Tier]:[rateengine].[Tier]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[TierBoundary]:[rateengine].[TierBoundary]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[TierStructureType]:[rateengine].[TierStructureType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[TimeOfUse]:[rateengine].[TimeOfUse]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[TimeOfUseBoundary]:[rateengine].[TimeOfUseBoundary]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[UnitOfMeasure]:[rateengine].[UnitOfMeasure]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[UseCharge]:[rateengine].[UseCharge]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[rateengine].[UseTypeMap]:[rateengine].[UseTypeMap]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[admin].[ETLBulkConfiguration]:[admin].[ETLBulkConfiguration]</val>
    </value>
  </TableActions>
  <SessionSettings>15</SessionSettings>
  <DCGroupingStyle>0</DCGroupingStyle>
  <SC_DeploymentOptions version="1" type="SC_DeploymentOptions">
    <BackupOptions version="1" type="BackupOptions">
      <BackupProvider>Native</BackupProvider>
      <TypeOfBackup>Full</TypeOfBackup>
      <Folder>\\NONPRDSQLBU\SQLBackup\QA\azuqsql001BI1</Folder>
      <Filename />
      <SqbLicenseType>None</SqbLicenseType>
      <SqbVersion>0</SqbVersion>
      <DefaultNativeFolder />
      <DefaultSqbFolder />
      <Password encrypted="1" />
      <NameFileAutomatically>False</NameFileAutomatically>
      <OverwriteIfExists>False</OverwriteIfExists>
      <CompressionLevel>0</CompressionLevel>
      <EncryptionLevel>None</EncryptionLevel>
      <ThreadCount>0</ThreadCount>
      <BackupEnabled>False</BackupEnabled>
    </BackupOptions>
  </SC_DeploymentOptions>
</Project>