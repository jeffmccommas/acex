﻿using System.Configuration;
using System.Web.Mvc;

namespace CE.InsightsWeb.Controllers
{
    public class LoginController : Controller
    {
        private const string AppSetting_CEEnvironment = "CEEnvironment";
        private readonly string User1 = "user1";
        private readonly string Password1 = "123";
        private readonly string User2 = "user2";
        private readonly string Password2 = "1234";
        // GET: Login
        public ActionResult Index()
        {
            ViewData["BaseUrl"] = ConfigurationManager.AppSettings.Get("CEInsightsWebBaseURL");
            ViewData["CEEnvironment"] = ConfigurationManager.AppSettings.Get(AppSetting_CEEnvironment);
            return View();
        }

        [HttpGet]
        public JsonResult CheckUser(string user, string password)
        {
            if (User1 == user && Password1 == password)
            {
                return Json("client1", JsonRequestBehavior.AllowGet);
            }
            if (User2 == user && Password2 == password)
            {
                return Json("client2", JsonRequestBehavior.AllowGet);
            }
            return Json("false", JsonRequestBehavior.AllowGet);
        }
    }
}