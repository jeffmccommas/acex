﻿/// <binding BeforeBuild='default' />

var ts = require('gulp-typescript');
var gulp = require('gulp');
var clean = require('gulp-clean');

var destPath = './libs/';

// Delete the dist directory
gulp.task('clean', function () {
    return gulp.src(destPath)
        .pipe(clean());
});

gulp.task("scriptsNStyles", () => {
    gulp.src([
            'core-js/client/**',
            'systemjs/dist/system.src.js',
            'reflect-metadata/**',
            'rxjs/**',
            'zone.js/dist/**',
            '@angular/**',
            'jquery/dist/jquery.*js',
            'bootstrap/dist/js/bootstrap.*js'
    ], {
        cwd: "node_modules/**"
    })
        .pipe(gulp.dest("./libs"))
        .once('end', function () { process.exit(0); });

});

var tsProject = ts.createProject('tsScripts/tsconfig.json', {
    typescript: require('typescript')
});

gulp.task('ts', function (done) {
    //var tsResult = tsProject.src()
    console.log(done);
    var tsResult = gulp.src([
            "tsScripts/*.ts"
    ])
        .pipe(ts(tsProject), undefined, ts.reporter.fullReporter());
    gulp.src("tsScripts/*.js")
    .pipe(gulp.dest("./Scripts"));
    return tsResult.js.pipe(gulp.dest('./Scripts'))
});

gulp.task('watch', ['watch.ts']);

gulp.task('watch.ts', ['ts'], function () {
    return gulp.watch('tsScripts/*.ts', ['ts'])
});

gulp.task('gulp_exit', function () {
    process.exit(0);
});

gulp.task('default', ['scriptsNStyles', 'watch']);

//gulp.task('watch', ['watch.ts'], function () { if (gulp.task('watch.ts').isRunning == false)  { process.exit(0); } });
