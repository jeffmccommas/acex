﻿using System;
using System.ServiceModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Xml.Serialization;

namespace TestMelissa
{
    internal class Program
    {

        /// <summary>
        /// 
        /// </summary>
        private static void Main()
        {
            //var n = new MelissaUILHER2();
            //n.RunMelissaUILHER2();

            const int clientId = 101; // Set to the desired clientId. If needed - see GetMissingPremiseAddress method
            var lstRunDate = DateTime.Parse("07/05/2016"); // Set to the desired last run date. If needed -- see GetMissingPremiseAddress method
            const string connectionString = "Data Source=aceusql1c0.database.windows.Net;Initial Catalog=InsightsDW_101;User ID=AclaraCEAdmin@aceusql1c0;Password=Acl@r@282;Encrypt=true;Trusted_Connection=false;";

            var melissaClass = new Melissa();
            melissaClass.RunMelissa(clientId, lstRunDate, connectionString);

            //TODO: RUN THE Paas_ETL for the client

            Console.WriteLine("");
            Console.WriteLine("==========>> REMEMBER TO RUN THE Paas_ETL <<===========");

        }
    }

    public class Melissa
    {
        // Bogus customer Id for Melissa calls.
        private const string CustomerId = "106409610";

        /// <summary>
        /// Run the Melissa program.
        /// </summary>
        /// <param name="clientId">The desired clientId.</param>
        /// <param name="lstRunDate">The last run date</param>
        /// <param name="connectionString">The database connection string</param>
        public void RunMelissa(int clientId, DateTime lstRunDate, string connectionString)
        {
            Console.WriteLine("Starting");

            try
            {
                // Get the missing premises
                var addresses = GetMissingPremiseAddress(connectionString);

                // Get 50 of the addresses at a time to process.
                var addressList = SplitArray(addresses);


                // Run each address in Parallel and retrieve the Melissa data.
                Parallel.ForEach(addressList, new ParallelOptions {MaxDegreeOfParallelism = 1}, addressesToProcess =>
                    {
                        // Initialize the datatable.
                        var premiseAttributes = new DataTable();
                        premiseAttributes.Columns.Add(new DataColumn("RowId", typeof(int)));
                        premiseAttributes.Columns.Add(new DataColumn("AttributeKey", typeof(string)));
                        premiseAttributes.Columns.Add(new DataColumn("OptionValue", typeof(string)));

                        var premiseInfo = new DataTable();
                        premiseInfo.Columns.Add(new DataColumn("RowId", typeof(int)));
                        premiseInfo.Columns.Add(new DataColumn("Latitude", typeof(float)));
                        premiseInfo.Columns.Add(new DataColumn("Longitude", typeof(float)));
                        premiseInfo.Columns.Add(new DataColumn("Address", typeof(string)));
                        premiseInfo.Columns.Add(new DataColumn("City", typeof(string)));
                        premiseInfo.Columns.Add(new DataColumn("State", typeof(string)));
                        premiseInfo.Columns.Add(new DataColumn("Zip", typeof(string)));
                        premiseInfo.Columns.Add(new DataColumn("ResultCode", typeof(string)));
                        premiseInfo.Columns.Add(new DataColumn("ResultData", typeof(string)));

                        // Retrieve the property data from Melissa.
                        var results = RetrievePropertyData(addressesToProcess);

                        // Process the result of the Melissa call into the datatable
                        foreach (var aResult in results)
                        {
                            var premiseInfoRow = premiseInfo.NewRow();

                            premiseInfoRow[0] = aResult.RecordID;

                            if (aResult.HasError == false)
                            {
                                if (!string.IsNullOrEmpty(aResult.Latitude))
                                {
                                    premiseInfoRow[1] = float.Parse(aResult.Latitude);
                                    premiseInfoRow[2] = float.Parse(aResult.Longitude);
                                }

                                premiseInfoRow[3] = aResult.Address;
                                premiseInfoRow[4] = aResult.City;
                                premiseInfoRow[5] = aResult.State;
                                premiseInfoRow[6] = aResult.Zip;

                                foreach (var att in aResult.PropertyAttributes)
                                {
                                    var attributesRow = premiseAttributes.NewRow();

                                    attributesRow[0] = aResult.RecordID;
                                    attributesRow[1] = att.Key;
                                    attributesRow[2] = att.Value;

                                    premiseAttributes.Rows.Add(attributesRow);
                                }
                            }

                            premiseInfoRow[7] = aResult.ResultCode;
                            premiseInfoRow[8] = aResult.ResultData;
                            premiseInfo.Rows.Add(premiseInfoRow);
                        }

                        // Merge the Premise Information into [Holding].[Premise]
                        try
                        {
                            MergePremiseInfo(premiseInfo, connectionString);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                            throw;
                        }

                        // Merge the Premise Attribute Information into [Holding].[PremiseAttributes]
                        try
                        {
                            MergePremiseAttributes(premiseAttributes, connectionString);
                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine(ex.ToString());
                            throw;
                        }

                        // Merge the Property data
                        try
                        {
                            MergePropertyData(premiseInfo, premiseAttributes, connectionString);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                            throw;
                        }
                    }
                );


                // Remove from the [Holding].[PremiseAttributes] any attribute with no assigned value.
                DeletePremiseAttributesWithNoValues(clientId, connectionString);


                //Dts.TaskResult = (int)ScriptResults.Success;
            }
            catch
            (Exception
                ex)
            {
                if (ex.InnerException != null)
                    ex = ex.InnerException;

                //Dts.TaskResult = (int)ScriptResults.Failure;
                //Dts.Events.FireError(0, "Melissa Data Script Task", ex.Message + "\r" + ex.StackTrace, String.Empty, 0);
            }

        }

        /// <summary>
        /// Retrieve the property data from Melissa.
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private IEnumerable<EntPropertyData> RetrievePropertyData(IReadOnlyList<EntAddress> address)
        {
            Console.WriteLine("Starting Propery Details");

            // Set the binding to HTTPS
            var binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport)
            {
                MaxBufferSize = 2147483647,
                MaxBufferPoolSize = 2147483647,
                MaxReceivedMessageSize = 2147483647,
                OpenTimeout = new TimeSpan(0, 3, 0),
                SendTimeout = new TimeSpan(0, 10, 0),
                ReceiveTimeout = new TimeSpan(0, 10, 0)
            };

            // Setup the URL
            var remoteAddress = new EndpointAddress("https://property.melissadata.net/v3/SOAP/Service.svc");

            // Verify that Melissa knows the Address and get their AddressKey for that address,
            var addressKeys = VerifyAddress(address);

            // Setup the request
            var request = new PropertyData_ServiceReference.RequestArray
            {
                CustomerId = CustomerId,
                Record = new PropertyData_ServiceReference.RequestRecord[address.Count],
                OptPropertyDetail = true
            };

            // Load the request - ALL REQUESTS ARE DONE USING MELISSA ADDRESSKEYs
            for (var counter = 0; counter < address.Count; counter++)
            {
                request.Record[counter] = new PropertyData_ServiceReference.RequestRecord
                {
                    RecordID = address[counter].RecordId,
                    AddressKey = addressKeys[counter]
                };
            }
            request.TotalRecords = address.Count;

            // Make the request.
            var client = new PropertyData_ServiceReference.ServiceClient(binding, remoteAddress);
            var result = client.DoLookup(request);

            // Get the response and process it for ACE.
            var propertyDetails = new EntPropertyData[address.Count];
            for (var counter = 0; counter < address.Count; counter++)
            {
                propertyDetails[counter] = new EntPropertyData
                {
                    RecordID = result.Record[counter].RecordID,
                    ResultCode = result.Record[counter].Result.Code
                };


                using (var stringWriter = new StringWriter())
                {
                    var serializer = new XmlSerializer(typeof(PropertyData_ServiceReference.ResponseRecord));
                    serializer.Serialize(stringWriter, result.Record[counter]);
                    propertyDetails[counter].ResultData = stringWriter.ToString();
                }
                if (result.Record[counter].Result.Code.Contains("YS"))
                {
                    var multifamily = false;

                    propertyDetails[counter].Address = result.Record[counter].PropertyAddress.Address;
                    propertyDetails[counter].City = result.Record[counter].PropertyAddress.City;
                    propertyDetails[counter].State = result.Record[counter].PropertyAddress.State;
                    propertyDetails[counter].Zip = result.Record[counter].PropertyAddress.Zip;
                    propertyDetails[counter].AddressKey = result.Record[counter].PropertyAddress.AddressKey;

                    propertyDetails[counter].Latitude = result.Record[counter].PropertyAddress.Latitude;
                    propertyDetails[counter].Longitude = result.Record[counter].PropertyAddress.Longitude;

                    if (!string.IsNullOrEmpty(result.Record[counter].Building.YearBuilt))
                    {
                        propertyDetails[counter].PropertyAttributes.Add("house.yearbuilt",
                            result.Record[counter].Building.YearBuilt);

                        var yearBuilt = int.Parse(result.Record[counter].Building.YearBuilt);

                        if (yearBuilt > 0 && yearBuilt < 1950)
                            propertyDetails[counter].PropertyAttributes.Add("house.yearbuiltrange",
                                "house.yearbuiltrange.before1950");
                        else if (yearBuilt >= 1950 && yearBuilt < 1960)
                            propertyDetails[counter].PropertyAttributes.Add("house.yearbuiltrange",
                                "house.yearbuiltrange.1955");
                        else if (yearBuilt >= 1960 && yearBuilt < 1970)
                            propertyDetails[counter].PropertyAttributes.Add("house.yearbuiltrange",
                                "house.yearbuiltrange.1965");
                        else if (yearBuilt >= 1970 && yearBuilt < 1980)
                            propertyDetails[counter].PropertyAttributes.Add("house.yearbuiltrange",
                                "house.yearbuiltrange.1975");
                        else if (yearBuilt >= 1980 && yearBuilt < 1990)
                            propertyDetails[counter].PropertyAttributes.Add("house.yearbuiltrange",
                                "house.yearbuiltrange.1985");
                        else if (yearBuilt >= 1990 && yearBuilt <= 1995)
                            propertyDetails[counter].PropertyAttributes.Add("house.yearbuiltrange",
                                "house.yearbuiltrange.1995");
                        else if (yearBuilt > 1995 && yearBuilt <= 2000)
                            propertyDetails[counter].PropertyAttributes.Add("house.yearbuiltrange",
                                "house.yearbuiltrange.2000");
                        else if (yearBuilt > 2000 && yearBuilt <= 2005)
                            propertyDetails[counter].PropertyAttributes.Add("house.yearbuiltrange",
                                "house.yearbuiltrange.2005");
                        else if (yearBuilt > 2005 && yearBuilt <= 2010)
                            propertyDetails[counter].PropertyAttributes.Add("house.yearbuiltrange",
                                "house.yearbuiltrange.2010");
                        else if (yearBuilt > 2010 && yearBuilt <= 2015)
                            propertyDetails[counter].PropertyAttributes.Add("house.yearbuiltrange",
                                "house.yearbuiltrange.2015");
                        else
                            propertyDetails[counter].PropertyAttributes.Add("house.yearbuiltrange",
                                "house.yearbuiltrange.2020");
                    }

                    if (!string.IsNullOrEmpty(result.Record[counter].Building.TotalRooms))
                        propertyDetails[counter].PropertyAttributes.Add("house.rooms",
                            result.Record[counter].Building.TotalRooms);

                    if (!string.IsNullOrEmpty(result.Record[counter].Building.Stories))
                        propertyDetails[counter].PropertyAttributes.Add("house.levels",
                            result.Record[counter].Building.Stories);

                    if (!string.IsNullOrEmpty(result.Record[counter].SquareFootage.LivingSpace))
                    {
                        propertyDetails[counter].PropertyAttributes.Add("house.totalarea",
                            result.Record[counter].SquareFootage.LivingSpace);

                        var totalArea = int.Parse(result.Record[counter].SquareFootage.LivingSpace);

                        if (totalArea > 0 && totalArea < 500)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.lt500");
                        else if (totalArea >= 500 && totalArea <= 600)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.600");
                        else if (totalArea > 600 && totalArea <= 700)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.700");
                        else if (totalArea > 700 && totalArea <= 800)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.800");
                        else if (totalArea > 800 && totalArea <= 900)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.900");
                        else if (totalArea > 900 && totalArea <= 1000)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.1000");
                        else if (totalArea > 1000 && totalArea <= 1250)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.1250");
                        else if (totalArea > 1250 && totalArea <= 1500)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.1500");
                        else if (totalArea > 1500 && totalArea <= 1750)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.1750");
                        else if (totalArea > 1750 && totalArea <= 2000)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.2000");
                        else if (totalArea > 2000 && totalArea <= 2250)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.2250");
                        else if (totalArea > 2250 && totalArea <= 2500)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.2500");
                        else if (totalArea > 2500 && totalArea <= 2750)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.2750");
                        else if (totalArea > 2750 && totalArea <= 3000)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.3000");
                        else if (totalArea > 3000 && totalArea <= 3250)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.3250");
                        else if (totalArea > 3250 && totalArea <= 3500)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.3500");
                        else if (totalArea > 3500 && totalArea <= 3750)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.3750");
                        else if (totalArea > 3750 && totalArea <= 4000)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.4000");
                        else if (totalArea > 4000 && totalArea <= 4500)
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.4500");
                        else
                            propertyDetails[counter].PropertyAttributes.Add("house.totalarearange",
                                "house.totalarearange.gt4500");
                    }

                    if (!string.IsNullOrEmpty(result.Record[counter].Building.BuildingCode))
                    {
                        var buildingCode = result.Record[counter].Building.BuildingCode;

                        if (buildingCode == "R00" || buildingCode == "RS0")
                            propertyDetails[counter].PropertyAttributes.Add("house.style", "house.style.singlefamily");
                        else if (buildingCode == "MCT" || buildingCode == "MCA" || buildingCode == "MCE" ||
                                 buildingCode == "MCH" || buildingCode == "MCM" || buildingCode == "RT0")
                            propertyDetails[counter].PropertyAttributes.Add("house.style", "house.style.townhouse");
                        else if (buildingCode == "MD0" || buildingCode == "MDF")
                            propertyDetails[counter].PropertyAttributes.Add("house.style", "house.style.duplex");
                        else if (buildingCode == "MAA" || buildingCode == "MAH" || buildingCode == "MAL" ||
                                 buildingCode == "MAT" || buildingCode == "MA0")
                            propertyDetails[counter].PropertyAttributes.Add("house.style", "house.style.apartment");
                        else if (buildingCode == "M00" || buildingCode == "MC0" || buildingCode == "M0A" ||
                                 buildingCode == "M0T" || buildingCode == "M0H" || buildingCode == "M50" ||
                                 buildingCode == "M51")
                        {
                            multifamily = true;
                            propertyDetails[counter].PropertyAttributes.Add("house.style", "house.style.multifamily");
                        }
                        else if (buildingCode == "RMO" || buildingCode == "RM1" || buildingCode == "RM2" ||
                                 buildingCode == "RMP")
                            propertyDetails[counter].PropertyAttributes.Add("house.style", "house.style.mobile");

                        if (buildingCode == "MultiFamily" || buildingCode == "Duplex")
                            propertyDetails[counter].PropertyAttributes.Add("house.rented", "house.rented.yes");
                        else
                            propertyDetails[counter].PropertyAttributes.Add("house.rented", "house.rented.no");
                    }

                    if (!string.IsNullOrEmpty(result.Record[counter].Building.HeatingCode))
                    {
                        var heatingCode = result.Record[counter].Building.HeatingCode;

                        if (new List<string> {"00E", "BBE", "FAE", "STE", "HWE", "HPE"}.Contains(heatingCode))
                            propertyDetails[counter].PropertyAttributes.Add("heatsystem.fuel",
                                "heatsystem.fuel.electric");
                        else if (new List<string> {"00G", "BBG", "FAG", "STG", "HWG", "HPG"}.Contains(heatingCode))
                            propertyDetails[counter].PropertyAttributes.Add("heatsystem.fuel", "heatsystem.fuel.gas");
                        else if (heatingCode == "00O")
                            propertyDetails[counter].PropertyAttributes.Add("heatsystem.fuel", "heatsystem.fuel.oil");
                        else if (heatingCode == "WS0")
                            propertyDetails[counter].PropertyAttributes.Add("heatsystem.fuel", "heatsystem.fuel.wood");
                    }

                    if (!string.IsNullOrEmpty(result.Record[counter].Building.HeatingCode))
                    {
                        var heatingCode = result.Record[counter].Building.HeatingCode;

                        if (new List<string> {"FA0", "FAC", "FAE", "FAG", "FAH", "FAO", "FAW"}.Contains(heatingCode))
                            propertyDetails[counter].PropertyAttributes.Add("heatsystem.style",
                                "heatsystem.style.forcedairfurnace");
                        else if (
                            new List<string> {"HS0", "HW0", "HWE", "HWG", "HWL", "HWO", "HWS", "HWW"}.Contains(
                                heatingCode))
                            propertyDetails[counter].PropertyAttributes.Add("heatsystem.style",
                                "heatsystem.style.waterboiler");
                        else if (new List<string> {"ST0", " STE", "STG", "STH", "STO", "STW"}.Contains(heatingCode))
                            propertyDetails[counter].PropertyAttributes.Add("heatsystem.style",
                                "heatsystem.style.steamboiler");
                        else if (
                            new List<string> {"BB0", "BBE", "BBG", "BBO", "BBp", "BH0", "BRE", "BS0", "BTE"}.Contains(
                                heatingCode))
                            propertyDetails[counter].PropertyAttributes.Add("heatsystem.style",
                                "heatsystem.style.baseboardresistance");
                        else if (new List<string> {"HP0", "HPE", "HPG", "HPH", "HPL", "HOO", "HPS"}.Contains(heatingCode))
                            propertyDetails[counter].PropertyAttributes.Add("heatsystem.style",
                                "heatsystem.style.airsourceheatpump");
                    }

                    if (!string.IsNullOrEmpty(result.Record[counter].Building.AirConditioningCode))
                    {
                        var airCondCode = result.Record[counter].Building.AirConditioningCode;

                        if (airCondCode == "000" || airCondCode == "001")
                            propertyDetails[counter].PropertyAttributes.Add("centralac.style", "centralac.style.none");
                        else if (airCondCode == "CEN" || airCondCode == "ACE")
                            propertyDetails[counter].PropertyAttributes.Add("centralac.style",
                                "centralac.style.standardcooling");
                        else if (airCondCode == "EVP")
                            propertyDetails[counter].PropertyAttributes.Add("centralac.style",
                                "centralac.style.evaporativecooling");
                        else if (airCondCode == "CEN & EVP")
                            propertyDetails[counter].PropertyAttributes.Add("centralac.style",
                                "centralac.style.evaporativeandstandard");
                    }

                    if (!string.IsNullOrEmpty(result.Record[counter].Building.FuelCode))
                    {
                        var fuelCode = result.Record[counter].Building.FuelCode;

                        if (new List<string> {"FEL"}.Contains(fuelCode))
                            propertyDetails[counter].PropertyAttributes.Add("waterheater.fuel",
                                "waterheater.fuel.electric");
                        else if (new List<string> {"FGA"}.Contains(fuelCode))
                            propertyDetails[counter].PropertyAttributes.Add("waterheater.fuel", "waterheater.fuel.gas");
                        else if (new List<string> {"FOI", "FOS"}.Contains(fuelCode))
                            propertyDetails[counter].PropertyAttributes.Add("waterheater.fuel", "waterheater.fuel.oil");
                        else if (new List<string> {"FWD", "FWO"}.Contains(fuelCode))
                            propertyDetails[counter].PropertyAttributes.Add("waterheater.fuel", "waterheater.fuel.wood");
                    }

                    if (!string.IsNullOrEmpty(result.Record[counter].Building.Pool))
                    {
                        propertyDetails[counter].PropertyAttributes.Add("pool.count",
                            result.Record[counter].Building.Pool == "Y" ? "1" : "0");

                        if (!string.IsNullOrEmpty(result.Record[counter].Building.PoolCode))
                        {
                            propertyDetails[counter].PropertyAttributes.Add("pool.poolheater",
                                result.Record[counter].Building.PoolCode == "00H"
                                    ? "pool.poolheater.yes"
                                    : "pool.poolheater.no");
                        }
                    }

                    if (!string.IsNullOrEmpty(result.Record[counter].Building.BedRooms))
                    {
                        var bedrooms = double.Parse(result.Record[counter].Building.BedRooms);
                        var people = bedrooms + 1;

                        if (multifamily && !string.IsNullOrEmpty(result.Record[counter].Building.Units))
                        {
                            var units = double.Parse(result.Record[counter].Building.Units);

                            people = (bedrooms / units) + 1;
                        }

                        propertyDetails[counter].PropertyAttributes.Add("house.people",
                            people.ToString(CultureInfo.InvariantCulture));
                    }

                    if (!string.IsNullOrEmpty(result.Record[counter].SquareFootage.LivingSpace) &&
                        !string.IsNullOrEmpty(result.Record[counter].Building.Stories))
                    {
                        var livingSpace = double.Parse(result.Record[counter].SquareFootage.LivingSpace);
                        var stories = double.Parse(result.Record[counter].Building.Stories);

                        if (stories > 0)
                        {
                            var roofArea = livingSpace / stories;
                            propertyDetails[counter].PropertyAttributes.Add("house.roofarea",
                                roofArea.ToString(CultureInfo.InvariantCulture));

                            if (!string.IsNullOrEmpty(result.Record[counter].Lot.SquareFootage))
                            {
                                var lotSquareFeet = double.Parse(result.Record[counter].Lot.SquareFootage);

                                propertyDetails[counter].PropertyAttributes.Add("outsidewater.lawnarea",
                                    ((lotSquareFeet - roofArea) * 0.8).ToString(CultureInfo.InvariantCulture));
                                propertyDetails[counter].PropertyAttributes.Add("outsidewater.gardenarea",
                                    ((lotSquareFeet - roofArea) * 0.2).ToString(CultureInfo.InvariantCulture));
                            }
                        }
                    }

                }
                else
                {
                    propertyDetails[counter].HasError = true;
                }
            }

            Console.WriteLine("End Getting Propery Details - Handled" + propertyDetails.Length);
            return propertyDetails;
        }


        /// <summary>
        /// Before calling for Melissa data verify the address is known by Melissa.
        /// </summary>
        /// <param name="address">The list of addresses to check</param>
        /// <returns></returns>
        private string[] VerifyAddress(IReadOnlyList<EntAddress> address)
        {
            Console.WriteLine("Start Address Check - " + address.Count);

            // Set the binding to HTTPS.
            var binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport)
            {
                MaxBufferSize = 2147483647,
                MaxBufferPoolSize = 2147483647,
                MaxReceivedMessageSize = 2147483647,
                OpenTimeout = new TimeSpan(0, 3, 0),
                SendTimeout = new TimeSpan(0, 5, 0),
                ReceiveTimeout = new TimeSpan(0, 5, 0)
            };

            // Set the URL to call.
            var remoteAddress = new EndpointAddress("https://addresscheck.melissadata.net/v2/SOAP/Service.svc");

            // Setup the request
            var request = new AddressCheck_ServiceReference.RequestArray
            {
                CustomerID = CustomerId,
                Record = new AddressCheck_ServiceReference.RequestArrayRecord[address.Count]
            };

            // Load the request
            for (var counter = 0; counter < address.Count; counter++)
            {
                request.Record[counter] = new AddressCheck_ServiceReference.RequestArrayRecord
                {
                    AddressLine1 = address[counter].Street,
                    City = address[counter].City,
                    State = address[counter].State,
                    Zip = address[counter].Zip
                };
            }

            // Perform the request
            var client = new AddressCheck_ServiceReference.ServiceClient(binding, remoteAddress);
            var result = client.doAddressCheck(request);

            // Get the response 
            var addressKeys = new string[address.Count];
            for (var counter = 0; counter < address.Count; counter++)
            {
                addressKeys[counter] = result.Record[counter].Address.AddressKey;
            }

            Console.WriteLine("Address Check - " + addressKeys.Length);

            // return
            return addressKeys;
        }

        /// <summary>
        /// Retrieve the premises that need Melissa data. The stored procedure call will need
        /// to be modified to the one designed for the specific customer.
        /// </summary>
        /// <param name="connString">The database connection string</param>
        /// <returns></returns>
        private EntAddress[] GetMissingPremiseAddress(string connString)
        {
            return GetMissingPremiseAddress(connString, -1, DateTime.MinValue);
        }

        /// <summary>
        /// Retrieve the premises that need Melissa data. The stored procedure call will need
        /// to be modified to the one designed for the specific customer.
        /// </summary>
        /// <param name="connString">The database connection string</param>
        /// <param name="clientId">The client Id</param>
        /// <param name="lastRunDate">The last run date.</param>
        /// <returns></returns>
        private EntAddress[] GetMissingPremiseAddress(string connString, int clientId, DateTime lastRunDate)
        {
            Console.WriteLine("GetMissingPremiseAddress");

            // Initialize the dataset.
            var dataSet = new DataSet();

            // Call the specific stored procedure.
            using (var sqlConnection = new SqlConnection(connString))
            {
                var command1 = new SqlCommand
                {
                    CommandText = "[Holding].[LoadMissingPremiseAttributesSCGEcho]",
                    CommandType = CommandType.StoredProcedure,
                    Connection = sqlConnection,
                    CommandTimeout = 0
                };
                //command1.Parameters.Add(new SqlParameter("@ClientId", SqlDbType.Int) { Value = clientId });
                //command1.Parameters.Add(new SqlParameter("@LastRunDate", SqlDbType.DateTime) { Value = lastRunDate });

                var adapter = new SqlDataAdapter(command1);
                Console.WriteLine("adapter fill start");
                adapter.Fill(dataSet);
                Console.WriteLine("adapter fill end - " + dataSet.Tables[0].Rows.Count);

            }

            // Create a list of EntAddress
            var addresses = new EntAddress[dataSet.Tables[0].Rows.Count];

            for (var counter = 0; counter < dataSet.Tables[0].Rows.Count; counter++)
            {
                var row = dataSet.Tables[0].Rows[counter];

                addresses[counter] = new EntAddress
                {
                    RecordId = row[0].ToString(),
                    Street = row[1].ToString(),
                    City = row[2].ToString(),
                    State = row[3].ToString(),
                    Zip = row[4].ToString()
                };
            }

            // Return the list
            return addresses;

        }

        /// <summary>
        /// Merge the Premise information into the [Holding].[PremiseAttribute] table.
        /// </summary>
        /// <param name="dataTable">The datatable holding the Premise Attribute information.</param>
        /// <param name="connString">The database connection string.</param>
        private void MergePremiseAttributes(DataTable dataTable, string connString)
        {
            using (var sqlConnection = new SqlConnection(connString))
            {
                var command1 = new SqlCommand
                {
                    CommandText = "[Holding].[MergePremiseAttributes]",
                    CommandType = CommandType.StoredProcedure,
                    Connection = sqlConnection,
                    CommandTimeout = 0
                };


                var tvparam = command1.Parameters.AddWithValue("@PremiseAttributes", dataTable);
                tvparam.SqlDbType = SqlDbType.Structured;

                sqlConnection.Open();
                command1.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Remove from the [Holding].[PremiseAttribute] table any attribute with no assigned value
        /// </summary>
        /// <param name="clientId">The clientId</param>
        /// <param name="connString">The database connection string.</param>
        private void DeletePremiseAttributesWithNoValues(int clientId, string connString)
        {
            using (var sqlConnection = new SqlConnection(connString))
            {
                var command1 = new SqlCommand
                {
                    CommandText = "[Holding].[DeletePremiseAttributesWithNoValues]",
                    CommandType = CommandType.StoredProcedure,
                    Connection = sqlConnection,
                    CommandTimeout = 0
                };

                command1.Parameters.Add(new SqlParameter("@ClientId", SqlDbType.Int) {Value = clientId});

                sqlConnection.Open();
                command1.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Merge the Premise information into the [Holding].[Premise] table.
        /// </summary>
        /// <param name="dataTable">The datatable holding the Premise information.</param>
        /// <param name="connString">The database connection string.</param>
        private void MergePremiseInfo(DataTable dataTable, string connString)
        {
            using (var sqlConnection = new SqlConnection(connString))
            {
                var command1 = new SqlCommand
                {
                    CommandText = "[Holding].[MergePremiseInfo]",
                    CommandType = CommandType.StoredProcedure,
                    Connection = sqlConnection,
                    CommandTimeout = 0
                };


                var tvparam = command1.Parameters.AddWithValue("@PremiseInfo", dataTable);
                tvparam.SqlDbType = SqlDbType.Structured;

                sqlConnection.Open();
                command1.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Load the Melissa property data into the [dbo].[ClientPropertyData] table for record keeping.
        /// </summary>
        /// <param name="dtPremiseInfo">The datatable holding Premise information</param>
        /// <param name="dtPremiseAttributes">The datatabler holding Premise Attribute information.</param>
        /// <param name="connString">The database connection string.</param>
        private void MergePropertyData(DataTable dtPremiseInfo, DataTable dtPremiseAttributes, string connString)
        {
            using (var sqlConnection = new SqlConnection(connString))
            {
                var command1 = new SqlCommand
                {
                    CommandText = "[dbo].[MergePropertyData]",
                    CommandType = CommandType.StoredProcedure,
                    Connection = sqlConnection,
                    CommandTimeout = 0
                };


                var tvparam = command1.Parameters.AddWithValue("@PremiseInfo", dtPremiseInfo);
                tvparam.SqlDbType = SqlDbType.Structured;

                var tvparam1 = command1.Parameters.AddWithValue("@PremiseAttributes", dtPremiseAttributes);
                tvparam1.SqlDbType = SqlDbType.Structured;

                sqlConnection.Open();
                command1.ExecuteNonQuery();
            }
        }


        /// <summary>
        /// Split the address array and return the first 50.
        /// </summary>
        /// <param name="addresses"></param>
        /// <returns></returns>
        private IEnumerable<EntAddress[]> SplitArray(EntAddress[] addresses)
        {
            var splitted = new List<EntAddress[]>();
            var lengthToSplit = 50;

            var arrayLength = addresses.Length;

            for (var i = 0; i < arrayLength; i = i + lengthToSplit)
            {
                if (arrayLength < i + lengthToSplit)
                    lengthToSplit = arrayLength - i;

                var val = new EntAddress[lengthToSplit];

                Array.Copy(addresses, i, val, 0, lengthToSplit);
                splitted.Add(val);
            }

            return splitted;
        }
    }

    /// <summary>
    /// An EntAdress object
    /// </summary>
    public class EntAddress
    {
        public string RecordId { get; set; }

        public string Street { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }
    }

    /// <summary>
    /// An EntPropertyData class.
    /// </summary>
    public class EntPropertyData
    {
        public bool HasError { get; set; }

        public string RecordID { get; set; }
        public string ResultCode { get; set; }
        public string ResultData { get; set; }

        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string AddressKey { get; set; }

        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public Dictionary<string, string> PropertyAttributes { get; set; }


        public EntPropertyData()
        {
            PropertyAttributes = new Dictionary<string, string>();
        }
    }
}
