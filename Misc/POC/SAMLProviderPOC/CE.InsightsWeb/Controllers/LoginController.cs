﻿using System.Configuration;
using System.Web.Mvc;

namespace CE.InsightsWeb.Controllers
{
    public class LoginController : Controller
    {
        private const string AppSetting_CEEnvironment = "CEEnvironment";
        // GET: Login
         
        public ActionResult Index()
        {
            ViewData["BaseUrl"] = ConfigurationManager.AppSettings.Get("CEInsightsWebBaseURL");
            ViewData["CEEnvironment"] = ConfigurationManager.AppSettings.Get(AppSetting_CEEnvironment);
            return View();
        }
    }
}