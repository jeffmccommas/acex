function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	} else {
		window.onload = function() {
			oldonload();
			func();
		}
	}
}

window.onload = function() {
  if (!document.getElementsByTagName) return false;
  var lnks = document.getElementsByTagName("a");
  for (var i=0; i<lnks.length; i++) {
    if (lnks[i].className == "popup") {
      lnks[i].onclick = function() {
        popUp(this.getAttribute("href"));
        return false;
      }
    }
  }
}

function popUp(winURL) {
  window.open(winURL,"popup");
}

function replaceAlign() {
  if (!document.getElementsByTagName) return false;
  if (!document.getElementsByTagName('img')) return false;
  var imgList = document.getElementsByTagName('img');
  for (var i=0; i<imgList.length; i++) {
    var align = imgList[i].getAttribute('align');
		if (align) {
			if (align == 'left') {
				imgList[i].className = 'left';
			}
			if (align == 'right') {
				imgList[i].className = 'right';
			}
			if (align == 'middle') {
				imgList[i].className = 'middle';
			}
     imgList[i].removeAttribute('align')
    }
  }
}

addLoadEvent(replaceAlign);