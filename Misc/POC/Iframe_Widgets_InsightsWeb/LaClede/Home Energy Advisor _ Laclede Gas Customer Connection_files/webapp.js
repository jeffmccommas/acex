var g_bSelectedAllOptions = false


function printThenCloseThisWindow(){
	if (navigator.appName == 'Microsoft Internet Explorer') {
		window.opener.focus();
		window.print();
		window.close();
	}
}

function allowDelete(){
	document.forms[0].hidAllowDelete.value = 'true';
}

function confirmDelete(){
	if (confirm('Are you sure you want to delete this record?  If you click OK you will not be able to undo this action.')){
		document.forms[0].hidAllowDelete.value = 'true';
	}else{
		window.event.returnValue = false;
	}
}

function openChildWindow(sURL) {
	launchWindow(sURL,780,580,'ChildWindow',true,false);
}

function openPrintWindow(sURL) {
	launchWindow(sURL,780,450,'ReportWindow',true,true);
}

function openHelpWindow(sURL) {
	launchWindow(sURL,714,475,'PageHelpWindow',true,false);
}

function printFramedWindow(sURL) {
	if(sURL.toString().indexOf("blank") > 0){
		alert('There is nothing being displayed for you to print');
	}else{
		launchWindow(sURL,780,450,'PrintHelp',true,true);
		setTimeout('m_childWindow.self.print();',1000);
	}
	window.event.returnValue = false;
}

function printWindow(bPrompt, bShowPrintDialog){
	if (bPrompt) {
		window.print();
	}
}

function printWindow()
{
	window.print();
}

function parseOutDatagridPercentSigns(frm,sDataGridName){
	for (i=0;i<frm.elements.length;i++) {
		if (frm.elements[i].type == 'text'){
			if (frm.elements[i].name.indexOf(sDataGridName) > 0){
				if (frm.elements[i].value.indexOf('%') > 0){
					frm.elements[i].value = frm.elements[i].value.replace('%','');
				}
			}
		}
	}
	return true;
}

function SelectAllOptions(frm, btn){
	if (ConfirmLongSelection(frm.elements.length, btn.value)){
	    IWCNavigationJS_pleaseWait_Now();
	
		if (!g_bSelectedAllOptions) {
			for (i=0;i<frm.elements.length;i++) {
				if (frm.elements[i].type == 'checkbox'){
					frm.elements[i].checked = true;
				}
			}
			g_bSelectedAllOptions = true;
			btn.value = 'Un-Select All';
		}else{
			for (i=0;i<frm.elements.length;i++) {
				if (frm.elements[i].type == 'checkbox'){
					frm.elements[i].checked = false;
				}
			}
			g_bSelectedAllOptions = false;
			btn.value = 'Select All';
		}
	}
		
	IWCNavigationJS_hidePleaseWait();
}

function ConfirmLongSelection (iNumOfElements, sAction){
	if (iNumOfElements != null && iNumOfElements > 2000){
		return confirm('Because there are so many records on this page ' + sAction + ' the records could take a long time (anywhere from 1 to 5 minutes).  Are you sure want to continue?  Click \'OK\' if you would like to continue otherwise click \'Cancel\' to cancel.');
	}
	return true;
}

function WriteCsrfToken(n, t) {
    var c = document.getElementById(n);
    if (c != null)
        c.value = t;
}