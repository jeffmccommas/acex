var m_isIE = false, m_isGecko = false;
var m_bUnsupportedBrowser = false;	
var m_range = '';					
var m_childWindow;					
var m_isDirty = false;				
var m_preloadFlag = false;			

if (navigator.appVersion.charAt(0) >= '4') {
	if (navigator.appName == 'Microsoft Internet Explorer') {
		m_isIE = true;
		m_range = 'all.';
	} else {
		m_bUnsupportedBrowser = true;
	}
} else {
	m_bUnsupportedBrowser = true;
}


function back() {
	history.back();
	return true;
}

function attachQueryStringValue(sURL, sQuerystringValue){
	if (sURL.indexOf("?") > 0) {
		sURL += "&" + sQuerystringValue;
	}else{
		sURL += "?" + sQuerystringValue;
	}
	return sURL;
}

function launchWindow(theURL,theWidth,theHeight,theName,bScroll,bFull) {
	var windowFeatures = '';
	
	if(bFull) {
			windowFeatures = windowFeatures + 'toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,resizable=yes';
	}else{
			windowFeatures = windowFeatures + 'toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes';
	}
	
	if(bScroll) {
			windowFeatures = windowFeatures + ',scrollbars=yes';
	}else{
			windowFeatures = windowFeatures + ',scrollbars=no';
	}
	
	windowFeatures = windowFeatures + ',width=' + theWidth + ',height=' + theHeight;

	m_childWindow = window.open(theURL, theName, windowFeatures);
	m_childWindow.focus();
}

function isObject(theObj) {	
	if(theObj == null)
		return false;
	
	if(typeof(theObj) == 'object')
		return true;
	else if(typeof(theObj) == 'function')
	    return true;
	else
		return false;
}

function getTheObject(theObj){
	if(isObject(document.getElementById) && isObject(document.getElementById(theObj))){
		return document.getElementById(theObj);
	} else if(isObject(document.all) && isObject(document.all[theObj])){
		return document.all[theObj];
	} else if (isObject(document.layers) && isObject(document.layers[theObj])){
		return document.layers[theObj];
	} else if(isObject(theObj)){
		return theObj;
	} else {
		return null;
	}
}

function getStyleObject(obj) {
	var theObj;
	
	theObj = getTheObject(obj);
	
	if (isObject(theObj)){
		if(isObject(theObj.style)){
			return theObj.style;
		}
	}
	
	return theObj;
}

function makeReappear(obj, sDisplayPosition) {
	obj = getStyleObject(obj);
	
	if (sDisplayPosition == 'absolute') {
		obj.display = 'block';
		obj.position = sDisplayPosition;
	}

	if (sDisplayPosition == 'inline') {
		obj.display = sDisplayPosition;
		obj.position = 'static';
	}

	if (sDisplayPosition == 'relative') {
		obj.display = 'inline';
		obj.position = 'relative';
	}

	obj.visibility = 'visible';
	obj.zIndex = 100;
}

function makeDisappear(obj) {
	obj = getStyleObject(obj);
	obj.zIndex = 1;
	obj.position = 'absolute';
	obj.visibility = 'hidden';
	obj.display = 'none';
}

function setBGColor(obj, color) {
	getStyleObject(obj).backgroundColor = color;
}

function shiftTo(obj, x, y) {
	var theObj = getStyleObject(obj);
	if (m_isGecko) {
		theObj.left = x;
		theObj.top = y;
	} else {
		theObj.pixelLeft = x;
		theObj.pixelTop = y;
	}
}

function getTheObjectLeft(obj) {
	return getStyleObject(obj).left;
}

function getTheObjectTop(obj) {
	return getStyleObject(obj).top;
}

function setZIndex(obj, zOrder) {
	getStyleObject(obj).zIndex = zOrder;
}

function selectFirstField() {
	for (i=0;i<document.forms[0].elements.length;i++){
		if (!document.forms[0].elements[i].disabled == true){
			if(document.forms[0].elements[i].type == 'text'){
				document.forms[0].elements[i].focus();
				break;
			}else if(document.forms[0].elements[i].type == 'text-area'){
				document.forms[0].elements[i].focus();
				break;
			}else if(document.forms[0].elements[i].type == 'select-multi'){
				document.forms[0].elements[i].focus();
				break;
			}else if(document.forms[0].elements[i].type == 'select-one'){
				document.forms[0].elements[i].focus();
				break;
			}
		}
	}
}

function getForm(obj) {
	var theObj = obj;
	if (typeof obj == 'string') {
		if (m_isGecko) {
			theObj = document.getElementById(obj);
		} else {
			theObj = eval('document.' + obj);
		}
	}
	return theObj;
}

function getFormField(frmName, fldName) {
	var theObj = fldName;
	if ((typeof frmName == 'string') && (typeof fldName == 'string')) {
		theObj = eval('document.' + frmName + '.' + fldName);
	}
	return theObj;
}

function makeDirty(){
	m_isDirty = true;
}

function confirmChanges(frmName,pge){
	if (m_isDirty){
		var msg = 'You\'ve made changes to ' + pge + '.  Would you like to save your changes?';
		if(confirm(msg)){
			getForm(frmName).submit();
		}
	}
}

function submitForm(frm,frmAction) {
	frm.action = frmAction;
	frm.submit();
	return true;
}

function resetForm(frm) {
	frm.reset();
	return true;
}

function cancelEntry() {
}

function isSelected(frmSelect)
{
	if (frmSelect.options.selectedIndex == -1)
		return false;
	else
		return true;
}

function selectedValue(frmSelect)
{
	return(frmSelect.options[frmSelect.options.selectedIndex].value);
}	

function selectedText(frmSelect)
{
	return(frmSelect.options[frmSelect.options.selectedIndex].text);
}	

function radioValue(frmRadio)
{
	return frmRadio[findCheckedOption(frmRadio)].value;
}

function findCheckedOption(frmRadio)
{
	for (var i = 0; i < frmRadio.length; i++)
	{
		if (frmRadio[i].checked == true)
		{
			return i;
		}
	}
	return -1;
}


function isValidPhone(frmField)
{
	var enteredPhone = new String(frmField.value); 
	
	parsedPhone = parseInteger(enteredPhone);

	if (parsedPhone.length ==7){
		return true;
	}
	if (parsedPhone.length ==10){
		return true;
	}	
	if ((parsedPhone.length ==11) && (parsedPhone.charAt(0) ==1)){
		return true;
	}
	return false;
}

function isValidDate(frmField)
{
	if(frmField.type == 'select-one' || frmField.type == 'select-multiple'){
		var fldValue = selectedValue(frmField);
	}
	if((frmField.type == 'text') || (frmField.type == 'textarea')){
		var fldValue = frmField.value;
	}
	if((frmField.type == 'radio')){
		var fldValue = radioValue(frmField);
	}
	
	var theDate = new Date(fldValue);
	
	if (fldValue.length > 0)
	{
		if (isNaN(theDate.getDate()))
		{
			return false;
		}
	}
	
	return true;
}

function validateTextbox(frmField)
{
	if (frmField.value == ''){
		return false;
	}
	return true;
}

function validateSelect(defaultValue, frmSelect)
{
	if (frmSelect.options[frmSelect.selectedIndex].value == defaultValue){
		return false;
	}
	return true;
}

function validateRadio(frmRadio)
{
	for (var i = 0; i < frmRadio.length; i++)
	{
		if (frmRadio[i].checked == true)
		{
			return true;
		}
	}
	return false;
}

function validateForm(frmName,frmAction) {
	var canSubmit = true;
	var frmFieldValue = ''
	var frm = getForm(frmName);
	
	for(i=0;i<frm.elements.length;i++) {
		if ((frm.elements[i].type == 'text') || (frm.elements[i].type == 'textarea')){
			if (!validateTextbox(frm.elements[i])) {
				alert('All entries must be completed.  Please enter a valid value to continue.');
				frm.elements[i].focus();
				return false;
			}
		}
		if ((frm.elements[i].type == 'select-one' || frm.elements[i].type == 'select-multiple' )){
			if (!isSelected(frm.elements[i])) {
				alert('All entries must be completed.  Please select a valid value to continue.');
				frm.elements[i].focus();
				return false;
			}
		}
		if ((frm.elements[i].type == 'radio')){
			if (!validateRadio(getFormField(frm.name, frm.elements[i].name))) {
				alert('All entries must be completed.  Please select an option to continue.');
				frm.elements[i].focus();
				return false;
			}
		}		
	}
	
	frm.action = frmAction;
	frm.submit();
	return true;	
}

function clearField(frmField, sDefaultValue){
	if((frmField.type == 'text') || (frmField.type == 'textarea')){
		if (frmField.value == sDefaultValue){
			frmField.value = '';
		}
	}
	if(frmField.type == 'select-one' || frmField.type == 'select-multiple'){
		if (selectedValue(frmField) == sDefaultValue){
			frmField.options[frmField.options.selectedIndex].value = '';
		}
	}
}

function formatPhone(frmField){
	var tempValue = parseInteger(frmField.value);
	
	if (tempValue.length == 7){
		tempValue = tempValue.substring(0,3) + '-' + tempValue.substring(3,7);
	}
	if (tempValue.length == 10) {
		tempValue = tempValue.substring(0,3) + '-' + tempValue.substring(3,6) + '-' + tempValue.substring(6,10);	
	} 
	if (tempValue.length == 11) {
		tempValue = tempValue.substring(1,4) + '-' + tempValue.substring(4,7) + '-' + tempValue.substring(7,11);	
	}
	frmField.value = tempValue;
}

function setCurrency(currencyField, fieldLabel, fieldRequired) {
	var firstCharacter = '';
	var firstTwoCharacters = '';
	var remainingCharacters = '';
	
	firstCharacter = currencyField.value.substring(0,1);
	if (firstCharacter != '$') {
		currencyField.value = '$' + currencyField.value;
	}
	firstTwoCharacters = currencyField.value.substring(0,2);
	if (firstTwoCharacters == '$.') {
		currencyField.value = '$0.' + currencyField.value.substring(2);
	}

	remainingCharacters = currencyField.value.substring(1,currencyField.value.length);
	if (isNaN(remainingCharacters) == true) {
		alert(fieldLabel + ' must be in US dollars only ($)');
		currencyField.value = '';
		currencyField.focus();
		return false;
	}
	if (remainingCharacters.length == 0 && fieldRequired) {
		alert(fieldLabel + ' is required.  Enter $0 if this ' + fieldLabel + ' is free.');
		currencyField.value = '';
		currencyField.focus();
		return false;
	}
}

function transferOptions(transferFrom, transferTo, default_val, frmObj, direction)
{
	if ((isSelected(transferFrom)) && (transferFrom.options[transferFrom.options.selectedIndex].value != default_val))
	{
		transferTo.options.length = transferTo.options.length + 1;
		for (i = transferTo.options.length - 1; i > 0; i--)
		{	
				transferTo.options[i] = new Option(transferTo.options[i-1].text, transferTo.options[i-1].value, false, false);
		}
	
		transferTo.options[0] = new Option(transferFrom.options[transferFrom.options.selectedIndex].text, transferFrom.options[transferFrom.options.selectedIndex].value, false, false);
	
		for (i = transferFrom.options.selectedIndex; i < transferFrom.options.length - 1; i++)
		{
				transferFrom.options[i] = new Option(transferFrom.options[i + 1].text, transferFrom.options[i + 1].value, false, false);
		}
	
		transferFrom.options[transferFrom.options.length - 1] = null;
		transferFrom.options[0].selected = true;
		transferTo.options[0].selected = true;
	
		if (direction) {
			populateHiddenField(frmObj.hidSelUsers,transferTo,default_val,true);
		} else {
			populateHiddenField(frmObj.hidSelUsers,transferFrom,default_val,true);
		}	
	}
	return;
}

function populateHiddenField(hiddenField, selectList, excludedValue, useValues){
	var hidden_values = '';
	
	for (i = 0; i < selectList.options.length; i++) {
		if (selectList.options[i].value != excludedValue) {
			if (useValues){
				hidden_values = hidden_values + ',' + selectList.options[i].value;
			} else {
				hidden_values = hidden_values + ',' + selectList.options[i].text;
			}
		}
	}
	
	hidden_values = hidden_values.substring(1);
	hiddenField.value = hidden_values;
}

function isInteger(theValue)
{
	var theString = theValue.toString();
	
	for (var i = 0; i < theString.length; i++)
		if ((theString.charAt(i) < '0') || (theString.charAt(i) > '9'))
			return false;
			
	return true;
}

function containsAlphabetCharacter(theValue)
{
	if (containsAlphaUpperCharacter(theValue) || containsAlphaLowerCharacter(theValue))
		return true;

	return false;
}

function containsAlphaUpperCharacter(theValue)
{
	var theString = theValue.toString();
	
	for (var i = 0; i < theString.length; i++)
		if ('A' <= theString.charAt(i) && theString.charAt(i) <= 'Z')
			return true;

	return false;
}

function containsAlphaLowerCharacter(theValue)
{
	var theString = theValue.toString();
	
	for (var i = 0; i < theString.length; i++)
		if ('a' <= theString.charAt(i) && theString.charAt(i) <= 'z')
			return true;

	return false;
}

function containsNumericCharacter(theValue)
{
	var theString = theValue.toString();
	
	for (var i = 0; i < theString.length; i++)
		if ('0' <= theString.charAt(i) && theString.charAt(i) <= '9')
			return true;

	return false;
}

function containsSpecialCharacter(theValue)
{
	var theString = theValue.toString();
	
	for (var i = 0; i < theString.length; i++)
		if (theString.charAt(i) != ' ' && !('0' <= theString.charAt(i) && theString.charAt(i) <= '9') && !('A' <= theString.charAt(i) && theString.charAt(i) <= 'Z') && !('a' <= theString.charAt(i) && theString.charAt(i) <= 'z'))
			return true;

	return false;
}

function isNumber(theValue, lowValue, highValue, bRequired) {
	if (bRequired && theValue.length == 0) {
		return false;
	}
	if (isNaN(theValue) == true) {
		return false;
	}
	if ((theValue < lowValue) || (theValue > highValue)) {
		return false;
	}	
	return true;
}

function isEmail(theEmail){
	var atSignLoc = theEmail.indexOf('@');
	var dotCharLoc = theEmail.lastIndexOf('.');
	
	if ((atSignLoc < 1) || (dotCharLoc < atSignLoc)) {
		return false;
	}
	return true;
}

function parseInteger(initValue)
{
	var parsedValue = new String();
	
	for (var i = 0; i < initValue.length; i++)
	{
		if (isInteger(initValue.charAt(i))){
			parsedValue += initValue.charAt(i);
		}
	}
	return parsedValue;
}

function removeDollarSign(theValue) {
	var newValue = '';
	
	if (theValue.substring(0,1) == '$') {
		newValue = theValue.substring(1,theValue.length)
	} else {
		newValue = theValue
	}
	return newValue;
}

function confirmPassword(newPassword, cnfPassword, minLength, maxLength) {
	var newPswrdLen = newPassword.length;
	
	if ((newPswrdLen < minLength && newPswrdLen > 0) || newPswrdLen > maxLength) {
		alert('Your Password must be from 6 to 50 characters.  Please enter a valid Password.');
		return false;
	}
	if (newPassword != cnfPassword) {
		alert('Your Passwords do not match, please re-enter your Password.');
		return false;
	}
	return true;
}

function confirmEmail(newEmail, cnfEmail, compareEmails){	
	if (!isEmail(newEmail) && newEmail.length>0) {
		alert('The Email Address you entered is invalid.  Please enter your full Email Address (ie. YourName@aol.com).');
		return false;
	}
	if (compareEmails && newEmail.toLowerCase() != cnfEmail.toLowerCase()) {
		alert('Your Email Addresses do not match, please enter a valid Email Address.');
		return false;
	}
	return true;
}


function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}

function changeImages() {
	if (document.images) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}
}
