﻿using System.Linq;
using CE.InsightDataAccess.InsightModels;
using Microsoft.AspNetCore.Mvc;
using CE.Portals.Utils.Models;
using System.Collections.Generic;


namespace CE.Portals.Utils.Controllers
{
    public class HomeController : Controller
    {
        private readonly InsightsContext _insightsDbContext;

        public HomeController(InsightsContext insightsDbContext)
        {
            _insightsDbContext = insightsDbContext;
        }

        public IActionResult Index()
        {
            var endPointTracking = _insightsDbContext.EndpointTracking.FirstOrDefault(x => x.ClientId == 224);
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Log()
        {
            var endPointTracking = _insightsDbContext.EventLog.Take(100).ToList();
            var eventlogviewmodel = new List<EventLogViewModel>();
            endPointTracking.ForEach(x =>
            {
                eventlogviewmodel.Add(new EventLogViewModel()
                {
                    LogId = x.LogId,
                    UserId = x.UserId,
                    EventLevel = x.EventLevel,
                    LoggerName = x.LoggerName,
                    MachineName = x.MachineName,
                    LogMessage = x.LogMessage,
                    PartitionKey = x.PartitionKey,
                    Exception = x.Exception,
                    LogDate = x.LogDate,
                    NewDate = x.NewDate
                });

            });
            return View(eventlogviewmodel);
        }
    }
}
