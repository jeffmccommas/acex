﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.Utils.Models
{
    public class EventLogViewModel
    {
        public int LogId { get; set; }
        public int? UserId { get; set; }
        public string EventLevel { get; set; }
        public string LoggerName { get; set; }
        public string MachineName { get; set; }
        public string LogMessage { get; set; }
        public byte PartitionKey { get; set; }
        public string Exception { get; set; }
        public DateTime? LogDate { get; set; }
        public DateTime NewDate { get; set; }
    }
}
