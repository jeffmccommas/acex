﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;
using log4net;
using log4net.Core;

namespace MockEnergyStarServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MockESService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MockESService.svc or MockESService.svc.cs at the Solution Explorer and start debugging.
    public class MockESService : IMockESService
    {
        ILog Logger = LoggingFactory.GetLogger();

        public System.IO.Stream GetAccountPendingList()
        {
            //File.WriteAllText(@"C:\Temp\log4netDemoLogs.log","testestes");

            Logger.Debug("GetAccountPendingList Called");

            string pageNumber = string.Empty;
            if (WebOperationContext.Current != null)
            {
                pageNumber = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["page"];
            }
            if (string.IsNullOrEmpty(pageNumber)) pageNumber = "0";
            string mockAccountList = new MockData.MockAccounts().GetAccountShareList(Convert.ToInt32(pageNumber));


            WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
            string result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + mockAccountList;
            return new MemoryStream(Encoding.UTF8.GetBytes(result));

        }

        public System.IO.Stream SetAccountStatus(string id)
        {
            Logger.Debug("SetAccountStatus Called");
            var header = WebOperationContext.Current.IncomingRequest.Headers;
            string bodyxml = OperationContext.Current.RequestContext.RequestMessage.ToString();
            //string body = .CreateBufferedCopy(Int32.MaxValue).CreateMessage().ToString();
            if (bodyxml.Contains("<action>Accept</action>"))
            {
                Logger.Debug("Meter Id = " + id + "Account Request Accepted");

            }
            else
            {
                Logger.Debug("Meter Id = " + id + "Account Request Rejected");

            }
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
            string result = "<response status=\"Ok\"/>";
            if (id == "error")
            {
                result = "<response status=\"Error\"/>";
            }
            return new MemoryStream(Encoding.UTF8.GetBytes(result));
        }

        public System.IO.Stream GetMeterSharePendingList()
        {
            string pageNumber = string.Empty;
            if (WebOperationContext.Current != null)
            {
                pageNumber = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["page"];
            }
            if (string.IsNullOrEmpty(pageNumber)) pageNumber = "0";
            string mockMetersList = new MockData.MockMeters().GetMetersShareList(Convert.ToInt32(pageNumber));


            WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
            string result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + mockMetersList;
            return new MemoryStream(Encoding.UTF8.GetBytes(result));
        }

        public System.IO.Stream SetMeterStatus(string meterId)
        {
            Logger.Debug("SetMeterStatus Called");
            var header = WebOperationContext.Current.IncomingRequest.Headers;
            string bodyxml = OperationContext.Current.RequestContext.RequestMessage.ToString();
            //string body = .CreateBufferedCopy(Int32.MaxValue).CreateMessage().ToString();
            if (bodyxml.Contains("<action>Accept</action>"))
            {
                Logger.Debug("Meter Id = " + meterId + "Account Request Accepted");

            }
            else
            {
                Logger.Debug("Meter Id = " + meterId + "Account Request Rejected");

            }
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;

            WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
            string result = "<response status=\"Ok\"/>";
            if (meterId == "error")
            {
                result = "<response status=\"Error\"/>";
            }
            return new MemoryStream(Encoding.UTF8.GetBytes(result));
        }

        public Stream GetAcceptedAccountsList()
        {
            //File.WriteAllText(@"C:\Temp\log4netDemoLogs.log","testestes");

            Logger.Debug("GetAcceptedAccountsList Called");

            string pageNumber = string.Empty;
            if (WebOperationContext.Current != null)
            {
                pageNumber = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["page"];
            }

            string mockAccountList = new MockData.MockAccounts().GetAcceptedAccountList();

            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;

            WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
            string result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + mockAccountList;
            return new MemoryStream(Encoding.UTF8.GetBytes(result));
        }

        public Stream GetAcceptedAccountsDetails(string customerid)
        {
            //File.WriteAllText(@"C:\Temp\log4netDemoLogs.log","testestes");

            Logger.Debug("GetAcceptedAccountsDetails Called");

            string pageNumber = string.Empty;
            if (WebOperationContext.Current != null)
            {
                pageNumber = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["page"];
            }

            string mockAccountList = new MockData.MockAccounts().GetAcceptedAccountDetails(customerid);

            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;

            WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
            string result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + mockAccountList;
            return new MemoryStream(Encoding.UTF8.GetBytes(result));
        }

        public Stream GetAccountPropertyList(string accountid)
        {
            //File.WriteAllText(@"C:\Temp\log4netDemoLogs.log","testestes");

            Logger.Debug("GetAccountPropertyList Called");

            string pageNumber = string.Empty;
            if (WebOperationContext.Current != null)
            {
                pageNumber = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["page"];
            }

            string mockAccountList = new MockData.MockAccounts().GetAccountPropertyList(accountid);

            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;

            WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
            string result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + mockAccountList;
            return new MemoryStream(Encoding.UTF8.GetBytes(result));
        }

        public Stream GetAcceptedMeterList(string propertyid)
        {
            Logger.Debug("GetAcceptedMeterList Called");

            string pageNumber = string.Empty;
            if (WebOperationContext.Current != null)
            {
                pageNumber = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["page"];
            }

            string mockAccountList = new MockData.MockMeters().GetAcceptedMeterList(propertyid);

            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;

            WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
            string result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + mockAccountList;
            return new MemoryStream(Encoding.UTF8.GetBytes(result));
        }

        public Stream GetPropertyDetails(string propertyid)
        {
            Logger.Debug("GetPropertyDetails Called");

            string pageNumber = string.Empty;
            if (WebOperationContext.Current != null)
            {
                pageNumber = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["page"];
            }

            string mockProperty = new MockData.MockAccounts().GetPropertyDetails(propertyid);

            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;

            WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
            string result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + mockProperty;
            return new MemoryStream(Encoding.UTF8.GetBytes(result));
        }

        public Stream GetMeterDetails(string meterid)
        {
            Logger.Debug("GetMeterDetails Called");

            string pageNumber = string.Empty;
            if (WebOperationContext.Current != null)
            {
                pageNumber = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["page"];
            }

            string mockMeter = new MockData.MockMeters().GetMeterDetails(meterid);

            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;

            WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
            string result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + mockMeter;
            return new MemoryStream(Encoding.UTF8.GetBytes(result));
        }

        public Stream GetCustomFields(string accountid)
        {
            Logger.Debug("GetCustomFields Called");

            string pageNumber = string.Empty;
            if (WebOperationContext.Current != null)
            {
                pageNumber = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["page"];
            }

            string mockProperty = new MockData.MockAccounts().GetCustomFields(accountid);

            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;

            WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
            string result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + mockProperty;
            return new MemoryStream(Encoding.UTF8.GetBytes(result));
        }

        public Stream PostMeterConsumptionData(string meterId)
        {
            Logger.Debug("PostMeterConsumptionData Called");
            var header = WebOperationContext.Current.IncomingRequest.Headers;
            string bodyxml = OperationContext.Current.RequestContext.RequestMessage.ToString();
            //string body = .CreateBufferedCopy(Int32.MaxValue).CreateMessage().ToString();
            string checkResult = CheckRequest(bodyxml);
            if (checkResult==string.Empty)
            {
                Logger.Debug("Meter Id = " + meterId + "PostMeterConsumptionData Accepted");
            }
            else
            {

                string errRes = "<response status=\"Error: "+ checkResult + "\"/>";
                Logger.Debug("Meter Id = " + meterId + "PostMeterConsumptionData Rejected");
                return new MemoryStream(Encoding.UTF8.GetBytes(errRes));
                

            }

            string mockmeterdata = new MockData.MockMeters().Get_PostMeterConsumptionResponse(meterId);

            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;

            WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
            string result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + mockmeterdata;
            return new MemoryStream(Encoding.UTF8.GetBytes(result));
        }

        public Stream GetMeterConsumptionData(string meterId)
        {
            Logger.Debug("GetMeterConsumptionData Called");

            string pageNumber = string.Empty;
            if (WebOperationContext.Current != null)
            {
                pageNumber = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["page"];
            }

            string mockProperty = new MockData.MockMeters().GetMeterConsumptionDate(meterId);

            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;

            WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
            string result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + mockProperty;
            return new MemoryStream(Encoding.UTF8.GetBytes(result));
        }

        private string CheckRequest(string request)
        {
            if (!request.Contains("<usage>") || !request.Contains("</usage>"))
                return "usage tag is missing";
            if (!request.Contains("<startDate>") || !request.Contains("</startDate>"))
                return "startDate tag is missing";
            if (!request.Contains("<endDate>") || !request.Contains("</endDate>"))
                return "endDate tag is missing";
            if (!request.Contains("<cost>") || !request.Contains("</cost>"))
                return "cost tag is missing";

            return string.Empty;

        }
    }
}
