﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using log4net.Config;

namespace MockEnergyStarServices
{
    public static class LoggingFactory
    {
        private static ILog _logger;

        private static ILog CreateLogger()
        {
            // Some log4net initialization
            XmlConfigurator.Configure();
            return LogManager.GetLogger("Logger");
        }

        public static ILog GetLogger()
        {
            return _logger ?? (_logger = CreateLogger());
        }
    }
}