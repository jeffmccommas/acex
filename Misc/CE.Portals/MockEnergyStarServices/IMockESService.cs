﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace MockEnergyStarServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMockESService" in both code and config file together.
    [ServiceContract]
    public interface IMockESService
    {
        //http://stackoverflow.com/questions/8720633/using-query-string-parameters-to-disambiguate-a-uritemplate-match
        [OperationContract]
        [WebGet(UriTemplate = "connect/account/pending/list?")]
        System.IO.Stream GetAccountPendingList();

        [OperationContract]
        [WebInvoke(UriTemplate = "connect/account/{accountId}")]
        System.IO.Stream SetAccountStatus(string accountId);

        [OperationContract]
        [WebGet(UriTemplate = "share/meter/pending/list?")]
        System.IO.Stream GetMeterSharePendingList();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "share/meter/{meterId}")]
        System.IO.Stream SetMeterStatus(string meterId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "customer/list")]
        System.IO.Stream GetAcceptedAccountsList();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "customer/{customerid}")]
        System.IO.Stream GetAcceptedAccountsDetails(string customerid);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "account/{accountid}/property/list")]
        System.IO.Stream GetAccountPropertyList(string accountid);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "property/{propertyid}/meter/list?myAccessOnly=true")]
        System.IO.Stream GetAcceptedMeterList(string propertyid);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "property/{propertyid}")]
        System.IO.Stream GetPropertyDetails(string propertyid);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "meter/{meterid}")]
        System.IO.Stream GetMeterDetails(string meterid);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "account/{accountid}/customFieldList")]
        System.IO.Stream GetCustomFields(string accountid);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "meter/{meterId}/consumptionData")]
        System.IO.Stream PostMeterConsumptionData(string meterId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "meter/{meterId}/consumptionData?")]
        System.IO.Stream GetMeterConsumptionData(string meterId);



    }
}
