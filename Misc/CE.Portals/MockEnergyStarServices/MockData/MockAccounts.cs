﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace MockEnergyStarServices.MockData
{
    public class MockAccounts
    {
        public string GetAccountShareList(int pagenumber)
        {
            //return File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\account.xml");

            if (pagenumber == 1)
                return File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\accountpage1.xml");
            if (pagenumber == 2)
                return File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\accountpage2.xml");
            if (pagenumber == 3)
                return File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\accountpage3.xml");

            return File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\account.xml");
        }
        public string GetAcceptedAccountList()
        {
            return File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\acceptedaccount.xml");
        }

        public string GetAcceptedAccountDetails(string customerid)
        {
            string allAccount = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\AcceptedCustomerDetails.xml");
            int startIndex = allAccount.IndexOf("customerid=" + customerid + "-Start");
            if (startIndex != -1)
            {
                startIndex = startIndex + ("customerid=" + customerid + "-Start").Length;
                startIndex = startIndex + 2;
            }
            else
                return "";

            int endIndex = allAccount.IndexOf("customerid=" + customerid + "-End");
            endIndex = endIndex - 2;

            string customerDetails = allAccount.Substring(startIndex,endIndex-startIndex);
            return customerDetails;


        }

        public string GetAccountPropertyList(string accountid)
        {
            return File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\PropertyList.xml");
        }
       
        public string GetPropertyDetails(string propertyid)
        {
            string allProperties = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\PropertyDetails.xml");
            int startIndex = allProperties.IndexOf("propertyid=" + propertyid + "-Start");
            if (startIndex != -1)
            {
                startIndex = startIndex + ("propertyid=" + propertyid + "-Start").Length;
                startIndex = startIndex + 2;
            }
            else
                return "";

            int endIndex = allProperties.IndexOf("propertyid=" + propertyid + "-End");
            endIndex = endIndex - 2;

            string propertyDetails = allProperties.Substring(startIndex, endIndex - startIndex);
            return propertyDetails;
        }

        public string GetCustomFields(string accountid)
        {
            return File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\CustomFields.xml");
        }

        

    }
}