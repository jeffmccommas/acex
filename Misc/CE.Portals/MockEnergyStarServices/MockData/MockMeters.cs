﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web;

namespace MockEnergyStarServices.MockData
{
    public class MockMeters
    {
        public string GetMetersShareList(int pagenumber)
        {
           
           
            if (pagenumber == 1)
                return File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\meterpage1.xml");
            if (pagenumber == 2)
                return File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\meterpage2.xml");
            if (pagenumber == 3)
                return File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\meterpage3.xml");

            return File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\meter.xml");
        }

        public string GetAcceptedMeterList(string propertyid)
        {
            return File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\acceptedMeterList.xml");
        }
        public string GetMeterDetails(string meterid)
        {
            string allmeters = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\MeterDetails.xml");
            int startIndex = allmeters.IndexOf("meterid=" + meterid + "-Start");
            if (startIndex != -1)
            {
                startIndex = startIndex + ("meterid=" + meterid + "-Start").Length;
                startIndex = startIndex + 2;
            }
            else
                return "";

            int endIndex = allmeters.IndexOf("meterid=" + meterid + "-End");
            endIndex = endIndex - 2;

            string meterDetails = allmeters.Substring(startIndex, endIndex - startIndex);
            return meterDetails;
        }
        public string Get_PostMeterConsumptionResponse(string meterid)
        {
            string allmeters = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\MeterConsumptionPostResponse.xml");
            int startIndex = allmeters.IndexOf("meterid=" + meterid + "-Start");
            if (startIndex != -1)
            {
                startIndex = startIndex + ("meterid=" + meterid + "-Start").Length;
                startIndex = startIndex + 2;
            }
            else
                return "";

            int endIndex = allmeters.IndexOf("meterid=" + meterid + "-End");
            endIndex = endIndex - 2;

            string meterDetails = allmeters.Substring(startIndex, endIndex - startIndex);
            return meterDetails;
        }
        public string GetMeterConsumptionDate(string meterid)
        {
            string response = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\MeterConsumptionGetResponse.xml");
            response = UpdateStartDates(response);
            response = UpdateEndDates(response);

            return response;
        }

        private string UpdateStartDates(string response)
        {
            int date_Index = response.IndexOf("<startDate>");

            int date_EndIndex = response.IndexOf("</startDate>");

            int months = 2;
            while (date_Index >= 0 && date_EndIndex > 0)
            {
                date_Index += 11;
                string date = response.Substring(date_Index, date_EndIndex - date_Index);
                Console.WriteLine(date);

                string newdate = String.Format("{0:yyyy-MM-dd}", DateTime.Now.AddMonths(months * -1));

                response = response.Replace("<startDate>" + date + "</startDate>", "<startDate>" + newdate + "</startDate>");

                int nextSearchIndex = date_EndIndex + 12;

                date_Index = response.IndexOf("<startDate>", nextSearchIndex);
                date_EndIndex = response.IndexOf("</startDate>", nextSearchIndex);

                months += 1;

            }
            return response;
        }
        private string UpdateEndDates(string response)
        {
            int date_Index = response.IndexOf("<endDate>");

            int date_EndIndex = response.IndexOf("</endDate>");

            int months = 1;
            while (date_Index >= 0 && date_EndIndex > 0)
            {
                date_Index += 9;
                string date = response.Substring(date_Index, date_EndIndex - date_Index);
                Console.WriteLine(date);

                string newdate = String.Format("{0:yyyy-MM-dd}", DateTime.Now.AddMonths(months * -1));

                response = response.Replace("<endDate>" + date + "</endDate>", "<endDate>" + newdate + "</endDate>");

                int nextSearchIndex = date_EndIndex + 10;

                date_Index = response.IndexOf("<endDate>", nextSearchIndex);
                date_EndIndex = response.IndexOf("</endDate>", nextSearchIndex);

                months += 1;

            }
            return response;
        }
    }
}