﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using CE.WeatherSensitivityWebJobsWeb.Models;
using CE.WeatherSensitivityWebJobsWeb.ReportRunner;

namespace CE.WeatherSensitivityWebJobsWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public async Task<ActionResult> History(int id)
        {
            ViewBag.clientId = id;
            //clientId = "224";
            var history = new JobHistory().GetHistory(id);
            return View(history);
        }

        public async Task<ActionResult> Reports(int id)
        {
            ViewBag.clientId = id;

            var list = new ReportList().GetReportList(id);
            return View(list);
        }
        public void Download(string selectedfile, string localfolderpath, int clientid)
        {
            var blob = new ReportList().Download(clientid, selectedfile, localfolderpath);
            if (blob != null)
            {
                Response.ContentType = blob.ContentType;
                Response.AddHeader("Content-Disposition", "Attachment; filename=" + selectedfile);
                Response.AddHeader("Content-Length", blob.BlobLength);
                Response.BinaryWrite(blob.DataStream.ToArray());
                //return Content("File download successful.");
            }
            else
            {
                //return Content("Error Downloading file.");
            }
        }

        public async Task<ActionResult> RunBenchmarkWebJob(ReportViewModel reportViewModel)
        {

            BenchmarkRunner benchmarkreportrunner = new BenchmarkRunner();
            string content = await benchmarkreportrunner.RunBenchmarks(reportViewModel.BenchmarkModel);
            //  string content = "<h3>test</h3>";

            ViewBag.ResponseMessage = content;
            return Content(content);
        }

        public async Task<ActionResult> RunEmailReportWebJob(ReportViewModel reportViewModel)
        {

            EmailReportRunner emailreportrunner = new EmailReportRunner();
            string content = await emailreportrunner.RunEmailReportWebJob(reportViewModel.EmailReportModel);
            return Content(content);
        }
        public async Task<ActionResult> RunPrintReportWebJob(ReportViewModel reportViewModel)
        {

            PrintReportRunner printreportrunner = new PrintReportRunner();
            string content = await printreportrunner.RunPrintReportWebJob(reportViewModel.PrintReportModel);
            return Content(content);
        }
    }
}