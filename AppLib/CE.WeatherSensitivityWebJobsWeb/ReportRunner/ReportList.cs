﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using CE.WeatherSensitivity.Common.Classes;
using CE.WeatherSensitivityWebJobsWeb.Models;
using InsightsDW.DataModel;
using ReportOutputLib;
using FileInfo = ReportOutputLib.FileInfo;

namespace CE.WeatherSensitivityWebJobsWeb.ReportRunner
{
    public class ReportList
    {
        public AzureBlobInfo Download(int clientId,string fileName,string localPath)
        {
            string message = string.Empty;
            var insightsMetaData = new InsightsMetadata.DataModel.DataAccess.InsightsMetadata();
            //int clientId = Convert.ToInt32(224);
            var appSettings = new List<WSApplicationSetting>();
            try
            {
                string shardName = insightsMetaData.GetShardName(clientId, ConfigurationHelper.GetCurrentEnvironment());
                appSettings = RunProcessCheck.RunApplicationCheck(ConfigurationHelper.GetApplicationName(), shardName,
                    clientId);

                FileServiceForAzureStorage reportFileSrv = new FileServiceForAzureStorage();
                reportFileSrv.ContainerName =
                    (from x in appSettings where x.Name == "StorageContainerName" select x.Value).FirstOrDefault();
                reportFileSrv.StorageKey =
                   (from x in appSettings where x.Name == "StorageAccountKey" select x.Value).FirstOrDefault();
                reportFileSrv.StorageName =
                                   (from x in appSettings where x.Name == "StorageAccountName" select x.Value).FirstOrDefault();
                System.Diagnostics.Debug.WriteLine(localPath + fileName);

                //reportFileSrv.Save(fileName, localPath + fileName);
                return reportFileSrv.Download(fileName);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error downloading: " + ex);
            }

            return null;
        }
        
        public List<FileInfo> GetReportList(int clientId)
        {
            var insightsMetaData = new InsightsMetadata.DataModel.DataAccess.InsightsMetadata();
            //int clientId = Convert.ToInt32(224);
            var appSettings = new List<WSApplicationSetting>();
            try
            {
                string shardName = insightsMetaData.GetShardName(clientId, ConfigurationHelper.GetCurrentEnvironment());
                appSettings = RunProcessCheck.RunApplicationCheck(ConfigurationHelper.GetApplicationName(), shardName,
                    clientId);

                FileServiceForAzureStorage reportFileSrv = new FileServiceForAzureStorage();
                reportFileSrv.ContainerName =
                    (from x in appSettings where x.Name == "StorageContainerName" select x.Value).FirstOrDefault();
                reportFileSrv.StorageKey =
                   (from x in appSettings where x.Name == "StorageAccountKey" select x.Value).FirstOrDefault();
                reportFileSrv.StorageName =
                                   (from x in appSettings where x.Name == "StorageAccountName" select x.Value).FirstOrDefault();

                return reportFileSrv.ListFilesInContainer();

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error GetReportList: " + ex);
            }
            return null;
        }
    }
}