﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CE.WeatherSensitivity.Common.Classes;
using CE.WeatherSensitivity.Common.Models;
using CE.WeatherSensitivityWebJobsWeb.Models;
using InsightsDW.DataModel;

namespace CE.WeatherSensitivityWebJobsWeb.ReportRunner
{
    public class BenchmarkRunner
    {
        public async Task<string> RunBenchmarks(BenchmarkViewModel reportViewModel)
        {
            /* string content = "<div>Process Check Error<br/>" + "ERROR </div>" ;
             return content;*/


            string arguments =
               $"\"-c {reportViewModel.ClientId} -r {reportViewModel.ReportAlias} -d {reportViewModel.ReportRequestDate.ToString("d")}\"";
            string content = string.Empty;
            var insightsMetaData = new InsightsMetadata.DataModel.DataAccess.InsightsMetadata();
            int clientId = Convert.ToInt32(reportViewModel.ClientId);

            var appSettings = new List<WSApplicationSetting>();
            try
            {
                string shardName = insightsMetaData.GetShardName(clientId, ConfigurationHelper.GetCurrentEnvironment());
                appSettings = RunProcessCheck.RunApplicationCheck(ConfigurationHelper.GetApplicationName(), shardName, clientId);

                var options = new ProcessingOptions
                {
                    ShardName = shardName,
                    ReportAlias = reportViewModel.ReportAlias,
                    ClientId = Convert.ToInt32(reportViewModel.ClientId),
                    ReportRequestDate = Convert.ToDateTime(reportViewModel.ReportRequestDate)
                };
                RunProcessCheck.RunCheck(options);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error RunBenchmarks: " + ex);
                content = "<div style='color:#b20000'>Basic application checks failed.<br/>" + ex.StackTrace + "</div>";
                return content;
            }


            string userName = reportViewModel.UserName;//;.Get("username");

            var reportUsers = (from x in appSettings where x.Name == "report_users" select x)
                .FirstOrDefault();
            if (reportUsers != null)
            {
                if (reportUsers.Value.ToLower().Contains(userName.ToLower()))
                //if(true)
                {

                    HttpClient client = new HttpClient();


                    //string originalPath = new Uri(HttpContext.Request.Url.AbsoluteUri).OriginalString;

                    string baseurl= (from x in appSettings where x.Name == "web_job_base_url" select x.Value).FirstOrDefault();
                    //originalPath = "http://aclaceweathersensitivitywebjobswauat.azurewebsites.net/Home/RunEmailWebJob";


                    StringBuilder webjobUrl = new StringBuilder();
                    try
                    {
                        webjobUrl.Append("https://");
                        /* int indexOfbaseUrlStart = originalPath.IndexOf("//");
                         webjobUrl.Append(originalPath.Substring(indexOfbaseUrlStart + 2,
                             originalPath.IndexOf(".") - indexOfbaseUrlStart - 2));
                         webjobUrl.Append(".scm.");
                         int indexOfDotStart = originalPath.IndexOf(".") + 1;
                         int indexOfFirstSlash = originalPath.IndexOf("/", indexOfDotStart);
                         webjobUrl.Append(originalPath.Substring(indexOfDotStart, (indexOfFirstSlash) - indexOfDotStart));
                         */
                        
                        webjobUrl.Append(baseurl);
                        webjobUrl.Append("/api/");
                        //triggeredwebjobs/WSEmailReportWorkflowWebJob/run?arguments=ondemandreport");

                    }
                    catch (Exception ex)
                    {
                        content = ex.Message;
                        content += "Baseurl: " + baseurl;
                        return content;
                    }
                    client.BaseAddress = new Uri(webjobUrl.ToString());


                    string webjobuserName =
                        (from x in appSettings where x.Name == "web_job_account_id" select x)
                            .First().Value;

                    string webjobkey =
                        (from x in appSettings where x.Name == "web_job_key" select x)
                            .First().Value;


                    var byteArray = Encoding.ASCII.GetBytes(webjobuserName + ":" + webjobkey);

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                        Convert.ToBase64String(byteArray));
                    //https://aclaceweathersensitivitywebjobswauat.scm.azurewebsites.net/api/triggeredwebjobs/WSBenchmarksWebJob/run
                    var response =
                        await
                            client.PostAsync(
                                "triggeredwebjobs/WSBenchmarksWebJob/run?arguments=" + arguments, null);

                    content = "<div style='color:#007f00'>URL: -> " + webjobUrl;
                    content += "<br/>Response-->" + response.StatusCode + "</div>";
                }
                else
                {
                    content = "<div style='color:#b20000'>User not found!</div>";
                }
            }
            else
            {
                content = "User setting not found in the database!</h3>";
            }

            return content;
        }
    }
}