﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.UI;
using CE.WeatherSensitivity.Common.Classes;
using CE.WeatherSensitivity.Common.Models;
using CE.WeatherSensitivityWebJobsWeb.Models;
using InsightsDW.DataModel;

namespace CE.WeatherSensitivityWebJobsWeb.ReportRunner
{
    public class JobHistory
    {
        public List<RootObject> GetHistory(int clientId)
        {
            var appSettings = new List<WSApplicationSetting>();
            try
            {
                var insightsMetaData = new InsightsMetadata.DataModel.DataAccess.InsightsMetadata();
                string shardName = insightsMetaData.GetShardName(clientId, ConfigurationHelper.GetCurrentEnvironment());
                appSettings = RunProcessCheck.RunApplicationCheck(ConfigurationHelper.GetApplicationName(), shardName, clientId);
          
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error GetHistory: " + ex);
                return null;
            }

            HttpClient client = new HttpClient();

            //string originalPath = baseurl;
            //string originalPath = "http://aclaceweathersensitivitywebjobswauat.aclarax.com/Home/Reports";

            string baseurl = (from x in appSettings where x.Name == "web_job_base_url" select x.Value).FirstOrDefault();

            StringBuilder webjobUrl = new StringBuilder();
            webjobUrl.Append("https://");
            /*int indexOfbaseUrlStart = originalPath.IndexOf("//");
            webjobUrl.Append(originalPath.Substring(indexOfbaseUrlStart + 2,
                originalPath.IndexOf(".") - indexOfbaseUrlStart - 2));
            webjobUrl.Append(".scm.");
            int indexOfDotStart = originalPath.IndexOf(".") + 1;
            int indexOfFirstSlash = originalPath.IndexOf("/", indexOfDotStart);
            webjobUrl.Append(originalPath.Substring(indexOfDotStart, (indexOfFirstSlash) - indexOfDotStart));
            */

            webjobUrl.Append(baseurl);
            webjobUrl.Append("/api/");
            
            client.BaseAddress = new Uri(webjobUrl.ToString());

          

            string webjobuserName =
                (from x in appSettings where x.Name == "web_job_account_id" select x)
                    .First().Value;

            string webjobkey =
                (from x in appSettings where x.Name == "web_job_key" select x)
                    .First().Value;

            var response = this.InvokeKuduWebApi(webjobUrl.ToString() + "/triggeredwebjobs", webjobuserName, webjobkey);

            // var byteArray = Encoding.ASCII.GetBytes(webjobuserName + ":" + webjobkey);

            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
            //    Convert.ToBase64String(byteArray));

            // var response = await client.getj("triggeredwebjobs");
            //await client.GetAsync("triggeredwebjobs");
            if(!string.IsNullOrEmpty(response))
                return Newtonsoft.Json.JsonConvert.DeserializeObject<List<RootObject>>(response);

            return null;

        }
        private string InvokeKuduWebApi(string url, string userName, string userPassword)
        {
            string responseFromServer = string.Empty;
            try
            {
                var unEncodedString = String.Format($"{userName}:{userPassword}");
                var encodedString = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(unEncodedString));

                //Decodes the string just for fun...this is why you must always use HTTPS with basic 
                //string authorizationHeader = encodedString; 
                //var beginPasswordIndexPosition = authorizationHeader.IndexOf(" ") + 1; 
                //var encodedAuth = authorizationHeader.Substring(beginPasswordIndexPosition); 
                //var decodedAuth = Encoding.UTF8.GetString(Convert.FromBase64String(encodedAuth)); 
                //WriteLine(decodedAuth); 

                //Change this URL to your WebApp hosting the  
                string URL = url;
                System.Net.WebRequest request = System.Net.WebRequest.Create(URL);
                request.Method = "GET";
                request.ContentLength = 0;
                request.Headers["Authorization"] = "Basic " + encodedString;
                System.Net.WebResponse response = request.GetResponse();
                System.IO.Stream dataStream = response.GetResponseStream();
                System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();
                reader.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return responseFromServer;

        }
    }
}