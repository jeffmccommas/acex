﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using CE.WeatherSensitivity.Common.Classes;
using CE.WeatherSensitivity.Common.Logging;
using CE.WeatherSensitivity.Common.Models;
using InsightsDW.DataModel;
using log4net;
using ReportOutputLib;
using WSWorkflowDataAccess;

namespace WSPrintReportWorkflowWebJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    public class Program
    {
        public static ILog Logger;
        public static string _reportFilePath;

        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        private static void Main(string[] args)
        {
            var message = string.Empty;

            var theEnvironment = ConfigurationHelper.GetCurrentEnvironment();

            if (ConfigurationHelper.GetProcessingEnabled() == false)
            {
                Console.WriteLine(
                    $"Processing has been disabled for this environment. Exited without processing. (Environment: {theEnvironment})");
                return;
            }

            Console.WriteLine("Starting print report workflow...");

            var options = new ProcessingOptions();

            var appName = ConfigurationHelper.GetApplicationName();

            if (args.Any())
            {
                var processedArgs = CheckArguments(args);
                if (processedArgs.Length < 5)
                {
                    Console.WriteLine("Ending print report workflow (OnDemand)...Missing parameters");
                    return;
                }

                if (!string.IsNullOrEmpty(args[0]))
                {
                    if (processedArgs[0].ToLower().Equals("ondemandprintreport"))
                    {
                        options.ClientId = Convert.ToInt32(processedArgs[1]); //client id
                        options.ReportAlias = processedArgs[2];
                        options.ReportRequestDate = Convert.ToDateTime(processedArgs[3]);
                        var userId = processedArgs[4];

                        options.IsTrialRun = !string.IsNullOrEmpty(processedArgs[5]) && processedArgs[5].Equals("true", StringComparison.InvariantCultureIgnoreCase) ? "1" : "0";


                        CheckPrintReportSettings(options);
                        var forcastReportSettings = CheckForecastSettings(appName, options.ClientId);

                        GeneratePrintReportCsv(forcastReportSettings, options, userId);
                        Logger.InfoFormat("Checking App settings - complete");
                    }
                    else
                    {
                        Console.WriteLine("Incorrect parameter.");

                    }
                    Console.WriteLine("Ending print report workflow (OnDemand)...");
                    return; //terminate on demand request here
                }
            }



            Console.WriteLine("Ending print workflow...message: " + message);
        }

        /// <summary>
        /// Check the arguments passed in to the program
        /// </summary>
        /// <param name="args">The arguments</param>
        /// <returns></returns>
        private static string[] CheckArguments(string[] args)
        {
            var log = args.Aggregate(string.Empty, (current, arg) => current + arg + "  *");
            Console.WriteLine("Args Recieved: " + log);

            if (args.Any())
            {
                if (args.Length == 1)
                {
                    return args[0].Split(' ');
                }
                if (args.Length >= 5)
                {
                    return args;
                }
            }
            return args;

        }


        private static void GeneratePrintReportCsv(ForecastReportSettings appSettings,
            ProcessingOptions processingOptions, string userId)
        {
            try
            {
                var overwrite = false;

                //var trial = wsReportSetting != null && wsReportSetting.Value == "1" ? "trial" : string.Empty;
                var trial = processingOptions.IsTrialRun.Equals("1", StringComparison.InvariantCultureIgnoreCase) ? "trial" : string.Empty;

                Logger.InfoFormat(
                    "Print Report Requested By: {0} Report Profile:{1} ReportRequestDate: {2:d} Report Type: {3}", userId,
                    processingOptions.ProfileName, processingOptions.ReportRequestDate,
                    trial == string.Empty ? "Non-Trial" : trial);

                IFileService fileService = new FileServiceForAzureStorage();
                fileService.ContainerName = appSettings.StorageContainerName;
                fileService.StorageKey = appSettings.StorageAccountKey;
                fileService.StorageName = appSettings.StorageAccountName;

                var wsdataaccess = new WeatherSensitivityDataAccess(processingOptions, Logger);
                var reportOutput = new ReportOutput(fileService);
                var printReport = wsdataaccess.GetPrintReport();

                if (printReport != null)
                {
                    var printReportName = new StringBuilder(processingOptions.ProfileName);

                    if (trial != string.Empty)
                    {
                        printReportName.Append("_print_" + trial);
                    }
                    else
                    {
                        overwrite = true; //overwrite file if it is non trial
                    }

                    reportOutput.PrintReportOutputFile = printReportName.ToString();
                    reportOutput.WritePrintReportToCSV(printReport, overwrite);
                    Logger.InfoFormat("Print Report workflow complete.");
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine("Print workflow...GeneratePrintReportCsv..exception:" + exp.Message);
                Logger?.Fatal("Print workflow...GeneratePrintReportCsv..exception : ", exp);
            }
            //reportOutput.WriteEmailReportToCSV(null, null);

        }


        private static void CheckPrintReportSettings(ProcessingOptions processingOptions)
        {

            try
            {
                Console.WriteLine("Starting print report workflow...Run");
                var insightsMetaData = new InsightsMetadata.DataModel.DataAccess.InsightsMetadata();
                processingOptions.ShardName = insightsMetaData.GetShardName(processingOptions.ClientId, ConfigurationHelper.GetCurrentEnvironment());

                // Set up the logger.
                InitalizeLog4Net.Initalize(processingOptions.ShardName);
                Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

                Logger.InfoFormat("Checking App settings - ClientId: {0} Profile: {1} Report Request Date: {2}", processingOptions.ClientId, processingOptions.ProfileName, processingOptions.ReportRequestDate.ToString("d"));
                // Determine if the report should be run.
                RunProcessCheck.RunCheck(processingOptions);
            }
            catch (Exception exp)
            {
                Console.WriteLine("Starting print report workflow...Run..exception:" + exp.Message);
                Logger?.Fatal("Starting print report workflow...Run..exception: ", exp);

            }


        }
        private static ForecastReportSettings CheckForecastSettings(string appName, int clientId)
        {

            try
            {
                Console.WriteLine("Starting print workflow...Run");
                var insightsMetaData = new InsightsMetadata.DataModel.DataAccess.InsightsMetadata();
                string shardName = insightsMetaData.GetShardName(clientId, ConfigurationHelper.GetCurrentEnvironment());

                // Set up the logger.
                InitalizeLog4Net.Initalize(shardName);
                Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);


                var appSettings = RunProcessCheck.RunApplicationCheck(appName, shardName, clientId);
                var result = MapToForecastReportSettings(appSettings);


                return result;
            }
            catch (Exception exp)
            {
                Console.WriteLine("Starting forecast print workflow...Run..exception:" + exp.Message);
                Logger?.Fatal("Starting forecast print workflow...Run..exception: ", exp);
                return null;

            }

        }

        /// <summary>
        /// Convert AppSettings to Forecast Settings
        /// </summary>
        /// <param name="appSettings"></param>
        /// <returns></returns>
        private static ForecastReportSettings MapToForecastReportSettings(List<WSApplicationSetting> appSettings)
        {
            try
            {

                return new ForecastReportSettings
                {
                    StorageAccountKey =
                        (from x in appSettings where x.Name == "StorageAccountKey" select x)
                            .First().Value,
                    StorageAccountName =
                        (from x in appSettings where x.Name == "StorageAccountName" select x)
                            .First().Value,
                    StorageContainerName =
                        (from x in appSettings where x.Name == "StorageContainerName" select x)
                            .First().Value

                };
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in application settings:" + ex.Message);
                return null;
            }
        }

    }
}
