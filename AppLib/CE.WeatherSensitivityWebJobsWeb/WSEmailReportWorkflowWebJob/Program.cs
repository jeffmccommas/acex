﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using CE.WeatherSensitivity.Common.Classes;
using CE.WeatherSensitivity.Common.Logging;
using CE.WeatherSensitivity.Common.Models;
using InsightsDW.DataModel;
using log4net;
using ReportOutputLib;
using WeatherForecastService;
using WeatherForecastService.models;
using WSWorkflowDataAccess;

namespace WSEmailReportWorkflowWebJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    public class Program
    {
        public static ILog Logger;


        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main(string[] args)
        {
            var message = string.Empty;

            var theEnvironment = ConfigurationHelper.GetCurrentEnvironment();
            if (ConfigurationHelper.GetProcessingEnabled() == false)
            {
                Console.WriteLine($"Processing has been disabled for this environment. Exited without processing. (Environment: {theEnvironment})");
                return;
            }

            Console.WriteLine("Starting email report workflow...");

            var options = new ProcessingOptions();

            string appName = ConfigurationHelper.GetApplicationName();

            if (args.Any())
            {
                var processedArgs = CheckArguments(args);
                if (processedArgs.Length < 5)
                {
                    Console.WriteLine("Ending email report workflow (OnDemand)...Missing parameters");
                    return;
                }

                if (!string.IsNullOrEmpty(args[0]))
                {
                    if (processedArgs[0].ToLower().Equals("ondemandemailreport"))
                    {
                        options.ClientId = Convert.ToInt32(processedArgs[1]);//client id
                        options.ReportAlias = processedArgs[2];
                        options.ReportRequestDate = Convert.ToDateTime(processedArgs[3]);
                        var userId = processedArgs[4];


                        options.IsTrialRun = !string.IsNullOrEmpty(processedArgs[5]) && processedArgs[5].Equals("true", StringComparison.InvariantCultureIgnoreCase) ? "1" : "0";


                        CheckEmailReportSettings(options);
                        var forcastReportSettings = CheckForecastSettings(appName, options);
                        GenerateEmailReportCsv(forcastReportSettings, options, userId);
                    }
                    else
                    {
                        Console.WriteLine("Incorrect parameter.");

                    }
                    Console.WriteLine("Ending email report workflow (OnDemand)...");
                    return; //terminate on demand request here
                }
            }


            Console.WriteLine("Initializing Forecast email workflow...");
            RunWeatherForecastWorkflow();


            Console.WriteLine("Ending email workflow...message: " + message);
        }

        private static string[] CheckArguments(string[] args)
        {
            string log = string.Empty;
            foreach (var arg in args)
                log += arg + "  *";
            Console.WriteLine("Args Recieved: " + log);

            if (args.Any())
            {
                if (args.Length == 1)
                {
                    return args[0].Split(' ');
                }
                if (args.Length >= 5)
                {
                    return args;
                }
            }
            return args;

        }
        private static void RunWeatherForecastWorkflow()
        {
            //get client list from config
            //override client list with whatever was sent in the args
            var clientIdList = ConfigurationHelper.GetClientIdListForDailyWebJob();
            var appName = ConfigurationHelper.GetApplicationName();
            try
            {

                var clientIds = clientIdList.Split(',').ToList();
                foreach (var clientid in clientIds)
                {
                    var insightsMetaData = new InsightsMetadata.DataModel.DataAccess.InsightsMetadata();
                    var shardName = insightsMetaData.GetShardName(Convert.ToInt32(clientid), ConfigurationHelper.GetCurrentEnvironment());
                    InitalizeLog4Net.Initalize(shardName);
                    Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
                    var appSettings = RunProcessCheck.RunApplicationCheck(appName, shardName, Convert.ToInt32(clientid));
                    Logger.Info("Trying to get postal codes");
                    var zipCodesByStation = RunProcessCheck.CheckPostalCodes(shardName, Convert.ToInt32(clientid));
                    Logger.InfoFormat("Postal code count: {0}", zipCodesByStation.Count);
                    var forecastEmailSettings = MapToForecastReportSettings(appSettings, zipCodesByStation, null);

                    var message = CheckForecastAndGenerateEmails(forecastEmailSettings);
                    Logger.InfoFormat("Weather Forecast Email workflow complete.");
                    Console.WriteLine(message);
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine("Starting email workflow...Run..exception:" + exp.Message);

                Logger.Fatal("Starting email workflow...Run..exception: ", exp);
            }


        }

        private static string CheckForecastAndGenerateEmails(ForecastReportSettings forecastEmailSettings)
        {
            string message;
            if (forecastEmailSettings != null)
            {
                ForecastService forecastService = new ForecastService(forecastEmailSettings, Logger);
                if (!CanRunWeatherSensitivityEmailReport(forecastEmailSettings, forecastService))
                {
                    message = "CanRunWeatherSensitivityEmailReport - returned false";
                    Console.WriteLine("Ending email workflow...message: " + message);
                    return message;
                }

                if (!SendForecastEmail(forecastEmailSettings))
                {
                    message = "CanRunWeatherSensitivityEmailReport - returned true but exception found in send email process.";
                    Logger.Fatal("Error sending forecast email.");
                }
                else
                {
                    message = "Email sent.";
                }
            }
            else
            {
                message = "Problem with application settings. Fatal error.";
                Logger.Fatal("Error sending forecast email. Problem with application settings");
            }

            return message;
        }
        private static bool SendForecastEmail(ForecastReportSettings forecastReportSettings)
        {
            Logger.Info("Sending Forecast Email");
            ForecastService forecastService = new ForecastService(forecastReportSettings, Logger);
            IFileService fileService = new FileService();
            IReportOutput reportOutput = new ReportOutput(fileService);
            Notification notification = new Notification(forecastReportSettings, Logger);

            var zipcodelist = new List<string>();
            zipcodelist.AddRange(forecastReportSettings.ZipCodeList.Split(','));

            var weatherData = forecastService.GetForecast(zipcodelist);

            var result = reportOutput.GenerateSummaryEmail(ForecastMapper.MapToEmailSummary(weatherData, forecastService.GetWeatherSensitivityAttribute(), forecastService));


            return notification.SendEmail(result);


        }
        public static bool CanRunWeatherSensitivityEmailReport(ForecastReportSettings forecastReportSettings, ForecastService forecastService)
        {
            Logger.Info("Checking Weather...");
            bool result = false;
            if (!CheckSettings(forecastReportSettings))
            {
                Logger.Fatal("CanRunWeatherSensitivityEmailReport: Error in forecast settings.");
                return false;
            }

            //ForecastService forecastService = new ForecastService(forecastReportSettings, _logger);
            var forecastList = forecastService.GetForecast(forecastReportSettings.ZipCodeList.Split(',').ToList());
            if (forecastList != null)
            {
                foreach (var forecast in forecastList)
                {
                    var extremeDaysList = forecastService.GetWeatherSensitivityAttribute() ==
                                          WeatherSensitivityAttribute.HOT_WEATHER
                        ? forecastService.GetHotDayExtremeForecast(forecast.daily)
                        : forecastService.GetColdDayExtremeForecast(forecast.daily);

                    result = ThresholdMet(extremeDaysList, forecastReportSettings, forecastService);
                    if (result) break;//even if one zip has extreme consecutive days thats good enough

                }

            }


            Logger.Info(result ? "Extreme weather threshold met." : "Extreme weather threshold not met.");

            return result;
        }

        private static bool ThresholdMet(Daily listOfExtremeWeatherDays, ForecastReportSettings forecastReportSettings, ForecastService forecastService)
        {
            StringBuilder trackingData = new StringBuilder();
            int matchCount = 0;
            int totalDaysToCheck = Convert.ToInt32(forecastReportSettings.ForecastEndPeriod) - Convert.ToInt32(forecastReportSettings.ForecastStartPeriod);
            for (int i = 0; i < totalDaysToCheck; i++)
            {
                string dayToCheck = (DateTime.Now.AddDays(Convert.ToInt32(forecastReportSettings.ForecastStartPeriod) + i)).ToString("d");
                foreach (var day in listOfExtremeWeatherDays.data)
                {
                    Console.WriteLine("left: " + forecastService.UnixTimeStampToDateTime(day.time).ToString("d"));
                    Console.WriteLine("right: " + dayToCheck);
                    Console.WriteLine("*******************");
                    trackingData.Append("Extreme day forecast: [" + forecastService.UnixTimeStampToDateTime(day.time).ToString("d") + "]-" + day.temperatureMax + "**");

                    if (forecastService.UnixTimeStampToDateTime(day.time).ToString("d") == dayToCheck)
                    {
                        matchCount = matchCount + 1;
                        break;
                    }
                }

                Logger.Info(trackingData);
                trackingData.Clear();
            }

            Console.WriteLine("Number of Extreme Days: " + matchCount);
            return matchCount == totalDaysToCheck;
        }

        private static bool CheckSettings(ForecastReportSettings forecastReportSettings)
        {
            if (forecastReportSettings == null)
                return false;

            if (string.IsNullOrEmpty(forecastReportSettings.EmailSubject) ||
                string.IsNullOrEmpty(forecastReportSettings.ForecastServiceUrl) ||
                string.IsNullOrEmpty(forecastReportSettings.ForecastServiceKey) ||
                //string.IsNullOrEmpty(forecastReportSettings.ExtremeHotTempThreshold) ||
                string.IsNullOrEmpty(forecastReportSettings.TemperatureType) ||
                string.IsNullOrEmpty(forecastReportSettings.ZipCodeList) ||
                string.IsNullOrEmpty(forecastReportSettings.ForecastStartPeriod) ||
                string.IsNullOrEmpty(forecastReportSettings.ForecastEndPeriod)
            )
            {
                return false;
            }
            else
            {
                return true;
            }


        }

        private static void GenerateEmailReportCsv(ForecastReportSettings forecastEmailSettings, ProcessingOptions processingOptions, string userId)
        {
            try
            {
                var overwrite = false;

                var trial = !string.IsNullOrEmpty(processingOptions.IsTrialRun) &&
                            processingOptions.IsTrialRun.Equals("1", StringComparison.InvariantCultureIgnoreCase)
                    ? "trial"
                    : string.Empty;

                Logger.InfoFormat("Email Report Requested By: {0} Report Profile:{1} ReportRequestDate: {2:d} Report Type: {3}", userId, processingOptions.ProfileName, processingOptions.ReportRequestDate, trial == string.Empty ? "Non-Trial" : trial);

                IFileService fileService = new FileServiceForAzureStorage();
                fileService.ContainerName = forecastEmailSettings.StorageContainerName;
                fileService.StorageKey = forecastEmailSettings.StorageAccountKey;
                fileService.StorageName = forecastEmailSettings.StorageAccountName;

                var wsdataaccess = new WeatherSensitivityDataAccess(processingOptions, Logger);
                var reportOutput = new ReportOutput(fileService);
                var emailReport = wsdataaccess.GetEmailReport();
                if (emailReport != null)
                {
                    StringBuilder emailReportName = new StringBuilder(processingOptions.ProfileName);

                    if (trial != string.Empty)
                    {
                        emailReportName.Append("_Email_" + trial);
                    }
                    else
                    {
                        overwrite = true;//overwrite file if it is non trial
                    }

                    reportOutput.EmailReportOutputFile = emailReportName.ToString();
                    reportOutput.WriteEmailReportToCSV(emailReport, null, overwrite);
                    Logger.InfoFormat("Email Report workflow complete.");
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine("Email workflow...GenerateEmailReportCsv..exception:" + exp.Message);
                Logger?.Fatal("Email workflow...GenerateEmailReportCsv..exception : ", exp);
            }
            //reportOutput.WriteEmailReportToCSV(null, null);

        }


        /// <summary>
        /// Get report settings and check data
        /// </summary>
        /// <param name="processingOptions"></param>
        private static void CheckEmailReportSettings(ProcessingOptions processingOptions)
        {

            try
            {
                Console.WriteLine("Starting email report workflow...Run");
                var insightsMetaData = new InsightsMetadata.DataModel.DataAccess.InsightsMetadata();
                processingOptions.ShardName = insightsMetaData.GetShardName(processingOptions.ClientId, ConfigurationHelper.GetCurrentEnvironment());

                // Set up the logger.
                InitalizeLog4Net.Initalize(processingOptions.ShardName);
                Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

                // Determine if the report should be run.
                RunProcessCheck.RunCheck(processingOptions);
            }
            catch (Exception exp)
            {
                Console.WriteLine("Starting email report workflow...Run..exception:" + exp.Message);
                Logger.Fatal("Starting email report workflow...Run..exception: ", exp);

            }


        }
        /// <summary>
        /// Get App settings
        /// </summary>
        /// <param name="appName"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        private static ForecastReportSettings CheckForecastSettings(string appName, ProcessingOptions options)
        {

            try
            {
                Console.WriteLine("Checking App Settings");
                var insightsMetaData = new InsightsMetadata.DataModel.DataAccess.InsightsMetadata();
                string shardName = insightsMetaData.GetShardName(options.ClientId, ConfigurationHelper.GetCurrentEnvironment());

                // Set up the logger.
                InitalizeLog4Net.Initalize(shardName);
                Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

                Logger.Debug("Trying to get postal codes");
                var appSettings = RunProcessCheck.RunApplicationCheck(appName, shardName, options.ClientId);
                var postalCodes = RunProcessCheck.CheckPostalCodes(shardName, options.ClientId);
                Logger.DebugFormat("Postal code count: {0}", postalCodes.Count);
                var result = MapToForecastReportSettings(appSettings, postalCodes, options);



                return result;



            }
            catch (Exception exp)
            {
                Console.WriteLine("Checking App Settings...exception:" + exp.Message);
                Logger?.Fatal("Checking App Settings...exception: ", exp);
                return null;

            }


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appSettings"></param>
        /// <param name="postalCodes"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        private static ForecastReportSettings MapToForecastReportSettings(List<WSApplicationSetting> appSettings, List<WSRetrieveStationIdsAndPostalCodes_Result> postalCodes, ProcessingOptions options)
        {
            try
            {
                string zipCodeList = (from x in appSettings where x.Name == "participant_zip_codes" select x)
                    .First().Value;
                List<string> zipCodesByStationList = new List<string>();
                if (postalCodes.Any())
                {
                    foreach (var postalCode in postalCodes)
                    {
                        if (postalCode.PostalCodes != string.Empty)
                            zipCodesByStationList.Add(postalCode.PostalCodes.Split(',').First().Trim());
                    }
                }

                if (zipCodesByStationList.Any())
                    zipCodeList = string.Join(",", zipCodesByStationList);

                Logger?.Info("Postal codes for weather forecast: " + zipCodeList);

                string tempType = string.Empty;
                string hotThreshold = string.Empty;
                string coldThreshold = string.Empty;
                //if (options != null)
                //{
                //    //get threshold from report settings and override app settings
                //    tempType = (from x in options.ReportSettings where x.Name == "temperature_type" select x).First().Value;
                //    hotThreshold = (from x in options.ReportSettings where x.Name == "extreme_hot_day_temp_threshold" select x).First().Value;
                //    coldThreshold = (from x in options.ReportSettings where x.Name == "extreme_cold_day_temp_threshold" select x).First().Value;
                //}

                return new ForecastReportSettings
                {
                    EmailSubject =
                        (from x in appSettings where x.Name == "forecast_email_subject" select x)
                        .First().Value,
                    ToEmailList =
                    (from x in appSettings
                        where x.Name == "forecast_email_distribution_list"
                        select x).First().Value,
                    ExtremeColdTempThreshold = string.IsNullOrEmpty(coldThreshold) ? (from x in appSettings where x.Name == "extreme_cold_day_temp_threshold" select x).First().Value : coldThreshold,
                    ExtremeHotTempThreshold = string.IsNullOrEmpty(hotThreshold) ? (from x in appSettings where x.Name == "extreme_hot_day_temp_threshold" select x).First().Value : hotThreshold,
                    TemperatureType = string.IsNullOrEmpty(tempType) ? (from x in appSettings where x.Name == "temperature_type" select x).First().Value : tempType,
                    ZipCodeList = zipCodeList,

                    ForecastTotalDays =
                        (from x in appSettings where x.Name == "forecast_total_days" select x)
                        .First().Value,
                    ForecastStartPeriod =
                        (from x in appSettings where x.Name == "forecast_start_period" select x)
                        .First().Value,
                    ForecastEndPeriod =
                        (from x in appSettings where x.Name == "forecast_end_period" select x)
                        .First().Value,
                    ForecastServiceUrl =
                        (from x in appSettings where x.Name == "forecast_service_url" select x)
                        .First().Value,
                    ForecastServiceKey =
                        (from x in appSettings where x.Name == "forecast_service_key" select x)
                        .First().Value,
                    ForecastEmailFrom =
                        (from x in appSettings where x.Name == "forecast_email_from" select x)
                        .First().Value,
                    ForecastEmailSmtpPort =
                        (from x in appSettings where x.Name == "forecast_email_smtp_port" select x)
                        .First().Value,
                    ForecastEmailFromName =
                        (from x in appSettings where x.Name == "forecast_email_from_name" select x)
                        .First().Value,
                    ForecastEmailSmtpServer =
                    (from x in appSettings
                        where x.Name == "forecast_email_smtp_server"
                        select x).First().Value,
                    ForecastEmailSmtpPassword =
                    (from x in appSettings
                        where x.Name == "forecast_email_smtp_password"
                        select x).First().Value,
                    ForecastEmailSmtpAuth =
                        (from x in appSettings where x.Name == "forecast_email_smtp_auth" select x)
                        .First().Value,
                    ForecastEmailSmtpSsl =
                        (from x in appSettings where x.Name == "forecast_email_smtp_ssl" select x)
                        .First().Value,
                    ForecastEmailSmtpUser =
                        (from x in appSettings where x.Name == "forecast_email_smtp_user" select x)
                        .First().Value,
                    StorageAccountKey =
                        (from x in appSettings where x.Name == "StorageAccountKey" select x)
                        .First().Value,
                    StorageAccountName =
                        (from x in appSettings where x.Name == "StorageAccountName" select x)
                        .First().Value,
                    StorageContainerName =
                        (from x in appSettings where x.Name == "StorageContainerName" select x)
                        .First().Value

                };
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in application settings (maps):" + ex.Message);
                Logger?.Fatal("Error in application settings (maps):" + ex.Message);
                return null;
            }
        }
    }
}
