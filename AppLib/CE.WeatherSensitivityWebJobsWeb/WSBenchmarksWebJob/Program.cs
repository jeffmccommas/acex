﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using CE.WeatherSensitivity.Common.Classes;
using CE.WeatherSensitivity.Common.Logging;
using CE.WeatherSensitivity.Common.Models;
using CE.WeatherSensitivityBenchmarking.Infastructure.Classes;
using CE.WeatherSensitivityBenchmarking.Infastructure.Models;
using CommandLine;
using InsightsDW.DataModel;
using InsightsDW.DataModel.DataAccess;
using log4net;


namespace WSBenchmarksWebJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    public class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly DateTime RunDateTime = DateTime.Now;

        private static void Main(string[] args)
        {
            if (args.Any())
            {
                var processedArgs = CheckArguments(args);
                if (processedArgs.Length < 3)
                {
                    Console.WriteLine("Ending benchmark process...Missing parameters");
                    return;
                }

                var options = new ProcessingOptions();

                //For Testing
                //////options.ClientId = 224;
                //////options.ReportAlias = "Irina.Test7";
                //////options.ReportRequestDate = new DateTime(2017, 5, 15);

                var parser = new Parser(with => with.HelpWriter = Console.Error);

                if (parser.ParseArgumentsStrict(processedArgs, options, () => Environment.Exit(-2)))
                {

                    // Inidicate program starting
                    Console.WriteLine("Starting Program run for ClientId {0}, ReportAlias {1}, ReportRequestDate {2} - RunDateTime = {3}",
                        options.ClientId, options.ReportAlias, options.ReportRequestDate, RunDateTime);

                    // Run the process.
                    Run(options);

                    // indicate program end.
                    Console.WriteLine("Finished Program run for ClientId {0}, ReportAlias {1}, ReportRequestDate {2} - RunDateTime = {3}",
                            options.ClientId, options.ReportAlias, options.ReportRequestDate, RunDateTime);

                }
            }
            else
            {
                Console.WriteLine("Ending benchmark process...no parameters found");
            }
        }

        /// <summary>
        /// Run
        /// </summary>
        /// <param name="processingOptions">The program options</param>
        private static void Run(ProcessingOptions processingOptions)
        {

            try
            {
                // Get the Shard Name
                var insightsMetaData = new InsightsMetadata.DataModel.DataAccess.InsightsMetadata();
                processingOptions.ShardName = insightsMetaData.GetShardName(processingOptions.ClientId, ConfigurationHelper.GetCurrentEnvironment());

                // Set up the logger.
                InitalizeLog4Net.Initalize(processingOptions.ShardName);

                // Determine if the report should be run.
                RunProcessCheck.RunCheck(processingOptions);

                // If no profile found exit.
                if (string.IsNullOrEmpty(processingOptions.ProfileName))
                {
                    Logger.Error("Could not find Weather Sensitivity Profile with reportAlias:" +
                                      processingOptions.ReportAlias);
                    Console.WriteLine("Could not find Weather Sensitivity Profile with reportAlias:" +
                                     processingOptions.ReportAlias);
                    return;
                }

                var wholeProgStopwatch = new Stopwatch();
                wholeProgStopwatch.Start();
                Logger.InfoFormat("Starting Program run for ClientId {0}, ReportAlias {1}, ReportRequestDate {2}",
                       processingOptions.ClientId, processingOptions.ReportAlias, processingOptions.ReportRequestDate);

                // Calculate the benchmarks
                CalculateBenchmarks(processingOptions);

                // Log end of program run.
                wholeProgStopwatch.Stop();
                Logger.InfoFormat(
                    "Finished Program run in {0} for ClientId {1}, ReportAlias {2}, ReportRequestDate {3} - RunDateTime = {4}",
                        wholeProgStopwatch.Elapsed, processingOptions.ClientId, processingOptions.ReportAlias, processingOptions.ReportRequestDate, RunDateTime);
            }
            catch (Exception eX)
            {
                if (!(eX is ApplicationException && eX.Message.Equals("Handled", StringComparison.InvariantCultureIgnoreCase)))
                {
                    Logger.ErrorFormat("Error encountered during Run of Report - {0} - {1}", eX.Message, eX);
                    Console.WriteLine("Error encountered during Run of Report - {0} - {1}", eX.Message, eX);
                }

            }
        }


        /// <summary>
        /// Calculate the benchmarks.
        /// </summary>
        /// <param name="processingOptions">The processing options</param>
        private static void CalculateBenchmarks(ProcessingOptions processingOptions)
        {

            try
            {
                // Get the Warehouse connection
                var insightsDataWarehouse = new InsightsDataWarehouse(processingOptions.ShardName);

                // Get the GroupIds
                var groupIds = insightsDataWarehouse.GetWsBenchmarkGroupIds(processingOptions.ClientId).ToList();
                Logger.InfoFormat("Found {0} GroupIds", groupIds.Count);

                //////groupIds = new List<int> {32};

                // Get the Ami Information
                DateTime amiStartDate;
                DateTime amiEndDate;
                int minNumberOfAmiDataNeeded;
                GetAmiLookupInfo(processingOptions, out amiStartDate, out amiEndDate, out minNumberOfAmiDataNeeded);

                // Cycle through the groups
                foreach (var groupId in groupIds)
                {
                    Logger.InfoFormat("Benchmarking Group {0}", groupId);

                    var comparisonDataForGroup = new List<ComparisonDataModel>();

                    var benchmarkResults = new List<WSBenchmarkResult>();

                    // Get the consumption data.
                    var consumptionDataGathering = new ConsumptionDataGathering();
                    consumptionDataGathering.GetConsumptionData(processingOptions, groupId, amiStartDate, amiEndDate,
                        comparisonDataForGroup, minNumberOfAmiDataNeeded);

                    // IF no data get the next groups
                    if (!comparisonDataForGroup.Any())
                    {
                        continue;
                    }

                    // Load the benchmark results with data.
                    benchmarkResults.AddRange(comparisonDataForGroup.Select(m => new WSBenchmarkResult
                    {
                        ReportProfileName = processingOptions.ProfileName,
                        CustomerId = m.CustomerId,
                        GroupId = m.GroupId,
                        PremiseId = m.PremiseId,
                        ClientId = processingOptions.ClientId,
                        CommodityDesc = m.CommodityKey,
                        UOMDesc = m.UomKey,
                        AccountId = m.AccountId,
                        ExtremeDay_PcntPeerBenchmarkValue = string.Empty,
                        ExtremeDay_PcntPeerBenchmarkText = string.Empty,
                        Self_PcntBenchmarkValue = string.Empty,
                        Self_PcntBenchmarkText = string.Empty
                    }));

                    // Do the Peer Comparison
                    var extremeStopwatch = new Stopwatch();
                    Logger.InfoFormat("Starting ExtremeDayPeerComparison for Group = {0}", groupId);
                    extremeStopwatch.Start();

                    int weatherDetailCount;
                    var extremeDayPeerComparison = new ExtremeDayPeerComparison(processingOptions, groupId, amiStartDate, amiEndDate, out weatherDetailCount);

                    // No weather data found that meets threshold requirements.
                    if (weatherDetailCount != 0)
                    {
                        extremeDayPeerComparison.ProcessExtremeDayBenchmarks(comparisonDataForGroup, benchmarkResults,
                            processingOptions);
                    }

                    extremeStopwatch.Stop();
                    Logger.InfoFormat("Finished ExtremeDayPeerComparison for Group = {0} - Time Elapsed {1}", groupId,
                        extremeStopwatch.Elapsed);


                    // Do the Self Comparison
                    extremeStopwatch.Reset();
                    Logger.InfoFormat("Starting ExtremeDaySelfComparison for Group = {0}", groupId);
                    extremeStopwatch.Start();

                    var selfComparison = new ExtremeDaySelfComparison(processingOptions, groupId, amiStartDate, amiEndDate, out weatherDetailCount);

                    if (weatherDetailCount != 0)
                    {
                        selfComparison.ProcessSelfBenchmarks(comparisonDataForGroup, benchmarkResults, processingOptions);
                    }

                    extremeStopwatch.Stop();
                    Logger.InfoFormat("Finished ExtremeDaySelfComparison for Group = {0} - Time Elapsed {1}", groupId,
                        extremeStopwatch.Elapsed);

                    // Save the benchmark results
                    extremeStopwatch.Reset();
                    Logger.InfoFormat("Starting Save to database for Group = {0}", groupId);
                    extremeStopwatch.Start();

                    insightsDataWarehouse.SaveBenchmarks(benchmarkResults, RunDateTime);

                    extremeStopwatch.Stop();
                    Logger.InfoFormat("Finished Save to database for Group = {0} - Time Elapsed {1}", groupId,
                        extremeStopwatch.Elapsed);
                }
            }
            catch (Exception eX)
            {
                if (
                    !(eX is ApplicationException &&
                      eX.Message.Equals("Handled", StringComparison.InvariantCultureIgnoreCase)))
                {
                    Logger.ErrorFormat("Error encountered during Run of Report - {0} - {1}", eX.Message, eX);
                }

            }
        }

        /// <summary>
        /// Get the information needed to retrieve the AMI data.
        /// </summary>
        /// <param name="processingOptions">The processing options</param>
        /// <param name="amiStartDate">OUT: The Start DateTime for the AMI search.</param>
        /// <param name="amiEndDate">OUT:  The END DateTime for the AMI search.</param>
        /// <param name="minNumberOfAmiDataNeeded">OUT: The minimum amount of AMI data desired.</param>
        public static void GetAmiLookupInfo(ProcessingOptions processingOptions, out DateTime amiStartDate, out DateTime amiEndDate, out int minNumberOfAmiDataNeeded)
        {
            // Get AMI Start Date
            amiStartDate =
                Convert.ToDateTime(
                    processingOptions.ReportSettings.First(m => m.Name.Equals("ami_start_date")).Value);

            // Get AMI End Date
            amiEndDate =
                Convert.ToDateTime(processingOptions.ReportSettings.First(m => m.Name.Equals("ami_end_date")).Value);

            // Get the timezone
            var timeZone =
                TimeZoneInfo.FindSystemTimeZoneById(
                    processingOptions.ReportSettings.First(m => m.Name.Equals("ami_data_timezone")).Value);

            // Convert the ami start and end date to UTC.
            amiStartDate = TimeZoneInfo.ConvertTime(amiStartDate, timeZone, TimeZoneInfo.Utc);
            amiEndDate = TimeZoneInfo.ConvertTime(amiEndDate, timeZone, TimeZoneInfo.Utc);
            Logger.InfoFormat("Using AMI start date = {0} UTC and AMI end date = {1} UTC", amiStartDate, amiEndDate);

            // Get the number of extreme days worth of AMI data.
            minNumberOfAmiDataNeeded = int.Parse(processingOptions.ReportSettings.First(
                    m => m.Name.Equals("ami_min_data_points", StringComparison.InvariantCultureIgnoreCase))
                .Value);
        }

        /// <summary>
        /// Check the arguments passed in the program.
        /// </summary>
        /// <param name="args">The arguments</param>
        /// <returns>The arguments</returns>
        private static string[] CheckArguments(string[] args)
        {
            var log = args.Aggregate(string.Empty, (current, arg) => current + (arg + "  *"));
            Console.WriteLine("Args Recieved: " + log);

            if (args.Any())
            {
                if (args.Length == 1)
                {
                    return args[0].Split(' ');
                }
                if (args.Length >= 3)
                {
                    return args;
                }
            }
            return args;

        }
    }
}
