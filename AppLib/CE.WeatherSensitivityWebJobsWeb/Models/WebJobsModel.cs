﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CE.WeatherSensitivityWebJobsWeb.Models
{
    public class Settings
    {
    }

    public class LatestRun
    {
        public string id { get; set; }
        public string status { get; set; }
        public string start_time { get; set; }
        public string end_time { get; set; }
        public string duration { get; set; }
        public string output_url { get; set; }
        public string error_url { get; set; }
        public string url { get; set; }
        public string trigger { get; set; }
    }

    public class RootObject
    {
        public string name { get; set; }
        public string runCommand { get; set; }
        public string type { get; set; }
        public string url { get; set; }
        public string history_url { get; set; }
        public string extra_info_url { get; set; }
        public string scheduler_logs_url { get; set; }
        public Settings settings { get; set; }
        public bool using_sdk { get; set; }
        public LatestRun latest_run { get; set; }
    }
}