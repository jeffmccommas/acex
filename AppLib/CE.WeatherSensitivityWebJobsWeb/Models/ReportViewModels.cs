﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CE.WeatherSensitivityWebJobsWeb.Models
{
    public class ReportViewModel
    {
        public EmailReportViewModel EmailReportModel { get; set; }
        public PrintReportViewModel PrintReportModel { get; set; }
        public BenchmarkViewModel BenchmarkModel { get; set; }
    }
    public class EmailReportViewModel
    {
        [Display(Name = "Report Alias")]
        public string ReportAlias { get; set; }

        [Display(Name = "Client Id")]
        public string ClientId { get; set; }

        [Display(Name = "Report Request Date")]
        public DateTime ReportRequestDate { get; set; }

        [Display(Name = "UserName")]
        public string UserName { get; set; }

        public string Trial { get; set; }

    }
    public class PrintReportViewModel
    {
        [Display(Name = "Report Alias")]
        public string ReportAlias { get; set; }

        [Display(Name = "Client Id")]
        public string ClientId { get; set; }

        [Display(Name = "Report Request Date")]
        public DateTime ReportRequestDate { get; set; }

        [Display(Name = "UserName")]
        public string UserName { get; set; }
        public string Trial { get; set; }

    }
    public class BenchmarkViewModel
    {
        [Display(Name = "Report Alias")]
        public string ReportAlias { get; set; }

        [Display(Name = "Client Id")]
        public string ClientId { get; set; }

        [Display(Name = "Report Request Date")]
        public DateTime ReportRequestDate { get; set; }

        [Display(Name = "UserName")]
        public string UserName { get; set; }
        public string Trial { get; set; }

    }

}