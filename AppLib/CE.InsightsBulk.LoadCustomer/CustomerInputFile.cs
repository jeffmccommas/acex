﻿using Aclara.UFx.StatusManagement;
using CE.InsightsBulk.Infrastructure;
using CE.InsightsBulk.Infrastructure.DataAccess;
using CE.InsightsBulk.Infrastructure.Interfaces;
using CE.InsightsBulk.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace CE.InsightsBulk.LoadCustomer
{
    public class CustomerInputFile : IBulkFile
    {

        private FileInfo _customerFile;
        private int _clientId;

        public FileInfo CustomerFile
        {
            get { return _customerFile; }
            set { _customerFile = value; }
        }
        
        protected CustomerInputFile()
        {

        }

        public CustomerInputFile(FileInfo custFile,int ClientId)
        {
            _customerFile = custFile;
            _clientId = ClientId;
        }

        public StatusList ProcessFile()
        {
           string trackingid  = "";
            DateTime trackingIdDate;
            DateTime trackingIdDateTest;
            StatusList statusList = new StatusList();
            int nodeCounter = 0;
            List<Customer> customerTable = new List<Customer>();
            List<Premise> premiseTable = new List<Premise>();
            BulkDataAccess da = new BulkDataAccess();
            try
            {
                trackingIdDate = DateTime.Now;
                int count = _customerFile.Name.Split('_').Length - 1;
                string[] filenameParts = _customerFile.Name.Split('_', '.');

                if (filenameParts.Length <= 1)
                {

                    trackingid = trackingIdDate.ToString();
                }
                else
                {
                    //this should contain the date
                    if (DateTime.TryParse(filenameParts[1], out trackingIdDateTest))
                    {
                        trackingid = filenameParts[1];
                        trackingIdDate = trackingIdDateTest;
                    }

                    else
                        trackingid = trackingIdDate.ToString();

                    if (count > 1)
                    {
                        trackingid = trackingid + "_" + filenameParts[2];
                    }

                }

                // This logic sets-up a custom iteration using linq and yield.  Each element is traversed forward-only, greatly saving resources.
                IEnumerable<XElement> getCustomerElements =
                from el in CustomerInputFileXMLStream.StreamData(_customerFile.FullName)
                select (XElement)el;

                foreach (XElement element in getCustomerElements)
                {
                    XmlNode xmlNode = element.GetXmlNode();

                    if (nodeCounter == 0)
                    {
                        //Create the datatable for storage
                         customerTable = new List<Customer>();
                         premiseTable = new List<Premise>();
                    }
                    //Parse the tracking id out of the file name.  If we don't have one, then use a datetime stamp

                                    // Main Call
                    ProcessNode(trackingid, trackingIdDate, xmlNode, customerTable, premiseTable, ref statusList);

                    //WriteResultToResponseFile(responseFileLocal, result);

                    nodeCounter++;
                    //todo put this in configuration
                    if (nodeCounter >= 1000)
                    {
                        nodeCounter = 0;
                        //WriteOutdata
                        da.WriteCustomerData(customerTable, premiseTable, ref statusList);
                     
                    }
                }
                if (nodeCounter > 0 )
                {
                    //Less than max rows, so write it out
                    da.WriteCustomerData(customerTable, premiseTable, ref statusList);
                }
                    
                
                if (nodeCounter > 0)
                {
                   
                    //FinalizeResponseFile(responseFileLocal);
                }
            }

            catch (Exception ex)
            {
                statusList.Add(new Status(ex, StatusTypes.StatusSeverity.Error));
            }
            finally
            {

            }

            return (statusList);
        }

        private void ProcessNode(string trackingid, DateTime trackingdate, XmlNode rootNode,  List<Customer> customerTable ,   List<Premise> premiseTable, ref StatusList statusList)
        {

            string xmlResult = string.Empty;
            const string NODE_CustomerId = "CustomerId";
            const string NODE_City = "City";
            const string NODE_Street2 = "Street2";
            const string NODE_Street1 = "Street1";
            const string NODE_State = "State";
            const string NODE_MobileNumber = "MobilePhoneNumber";
            const string NODE_PhoneNumber = "PhoneNumber";
            const string NODE_LastName = "LastName";
            const string NODE_IsBusiness = "IsBusiness";
            const string NODE_IsAuthenticated = "IsAuthenticated";
            const string NODE_FirstName = "FirstName";
            const string NODE_EmailAddress = "EmailAddress";
            const string NODE_Country = "Country";
            const string NODE_AlternateEmailAddress = "AlternateEmailAddress";
            const string NODE_Source = "Source";
            const string NODE_PostalCode = "PostalCode";
            const string ACCOUNTNODE_AccountId = "AccountId";
            const string PREMISENODE_PremiseId = "PremiseId";
			const string PREMISENODE_Street2="PremiseStreet2";
			const string PREMISENODE_Street1="PremiseStreet1";
			const string PREMISENODE_City = "PremiseCity";
			const string PREMISENODE_State="PremiseState";
			const string PREMISENODE_PostalCode = "PremisePostalCode";
            const string PREMISENODE_Latitude = "Latitude";
            const string PREMISENODE_Longitude = "Longitude";
            const string PREMISENODE_HasGasService = "HasGasService";
			const string PREMISENODE_HasElectricService="HasElectricService";
			const string PREMISENODE_HasWaterService="HasWaterService";
			const string PREMISENODE_Country="PremiseCountry";
            string source;

             Customer cust = new Customer();
            Premise premise = new Premise();
            //For some reason, customer is coming in as a child node, so just get all children
            XmlNode cNode = rootNode.FirstChild;
       //     var customerList = new DataTable();
          
            cust.City = GetText(cNode, NODE_City);
            cust.CustomerID = GetText(cNode, NODE_CustomerId);
            cust.Street2 = GetText(cNode, NODE_Street2);
            cust.Street1 = GetText(cNode, NODE_Street1);
            cust.State = GetText(cNode, NODE_State);
            cust.postalcode = GetText(cNode, NODE_PostalCode);
            cust.ClientID = _clientId;
            cust.MobilePhoneNumber = GetText(cNode, NODE_MobileNumber);
            cust.PhoneNumber = GetText(cNode, NODE_PhoneNumber);
            cust.LastName = GetText(cNode, NODE_LastName);
            cust.IsBusiness = Convert.ToBoolean(GetText(cNode, NODE_IsBusiness));
            cust.IsAuthenticated = Convert.ToBoolean(GetText(cNode, NODE_IsAuthenticated));
            cust.FirstName = GetText(cNode, NODE_FirstName);
            cust.EmailAddress = GetText(cNode, NODE_EmailAddress);
            cust.Country = GetText(cNode, NODE_Country);
            cust.AlternateEmailAddress = GetText(cNode, NODE_AlternateEmailAddress);
            source = GetText(cNode, NODE_Source);
            cust.TrackingId = trackingid;
            cust.TrackingDate = trackingdate;
            cust.ClientID = _clientId;
            cust.SourceId = convertSourceToID(source);
          
            customerTable.Add(cust);

            //Load the Premise Table
            if (cNode.HasChildNodes)
            {
                var Accounts = cNode.ChildNodes;
                foreach (XmlElement accountNode in Accounts)
                {
                    if (accountNode.HasChildNodes)
                    {
                        var premiseList = accountNode.ChildNodes;
                        foreach (XmlElement premiseNode in premiseList)
                        {
                            premise.City = GetText(premiseNode, PREMISENODE_City);
                            premise.ClientID = _clientId;
                            premise.Country = GetText(premiseNode, PREMISENODE_Country);
                            premise.CustomerID = cust.CustomerID;
                            premise.AccountID = GetText(accountNode, ACCOUNTNODE_AccountId);
                            premise.ElectricService = Convert.ToInt32(Convert.ToBoolean(GetText(premiseNode, PREMISENODE_HasElectricService)));
                            premise.GasService = Convert.ToInt32(Convert.ToBoolean(GetText(premiseNode, PREMISENODE_HasGasService)));
                            premise.WaterService = Convert.ToInt32(Convert.ToBoolean(GetText(premiseNode, PREMISENODE_HasWaterService)));
                            premise.postalcode = GetText(premiseNode, PREMISENODE_PostalCode);
                            premise.PremiseID = GetText(premiseNode, PREMISENODE_PremiseId);
                            premise.SourceId = cust.SourceId;
                            premise.State = GetText(premiseNode, PREMISENODE_State);
                            premise.Street1 = GetText(premiseNode, PREMISENODE_Street1);
                            premise.Street2 = GetText(premiseNode, PREMISENODE_Street2);
                            premise.Latitude = GetText(premiseNode, PREMISENODE_Latitude)==string.Empty ? (double?)null : Convert.ToDouble( GetText(premiseNode, PREMISENODE_Latitude));
                            premise.Longitude = GetText(premiseNode, PREMISENODE_Longitude) == string.Empty ? (double?)null : Convert.ToDouble(GetText(premiseNode, PREMISENODE_Longitude));
                            premise.TrackingDate = trackingdate;
                            premise.TrackingID = trackingid;
                            premiseTable.Add(premise);
                        }

                    }
                    
                }
            }

                
        }

        //Converts Source to SourceID type
        public int convertSourceToID(string sourceID)
        {
            int convertedSource;
                 switch (sourceID)
	        {
                case "aclara":
                    convertedSource = 1;
                    break;
                case "auditor":
                    convertedSource = 2;
                    break;
                case "csr":
                    convertedSource = 3;
                    break;
                case "customer":
                    convertedSource= 4;
                    break;
                case "customermobile":
                    convertedSource= 5;
                    break;
                case "customerweb":
                    convertedSource = 6;
                    break;
                case "thirdparty":
                    convertedSource = 7;
                    break;
                case "utility":
                   convertedSource= 8;
                    break;
		        default:
                    convertedSource= -1;
                    break;
	        }
                 return convertedSource;
        }
        public void OpenFile()
        {
            throw new NotImplementedException();
        }

        public void RenameFile()
        {
            throw new NotImplementedException();
        }


        public void RenameFile(string inFileName, string outFileName)
        {
            throw new NotImplementedException();
        }

        private static string GetText(XmlNode xn, string attrName)
        {
            var attr = xn.Attributes[attrName];
            if (attr == null)
                return string.Empty;
            var temp = attr.Value;
            if (temp == null)
                return string.Empty;
            return temp;
        }
    }
}
