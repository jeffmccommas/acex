﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using CE.InsightsMeta.Model;
using CE.InsightsBulk.Infrastructure.DataAccess;
using System.Xml.Linq;

namespace CE.InsightsBulk.LoadCustomer
{
    public class FileLoadConfiguration
    {

        const string FILEPATH = "FilePath";
        const string CONFIG_FILESEARCHPATTERN = "FileSearchPattern";
        const string CONFIG_FILEWORKINGEXTENSION = "FileWorkingExtension";
        const string CONFIG_FILEDONEEXTENSION = "FileDoneExtension";
        const string ComponentName = "CustomerBulkLoad";
        int _clientID;
        string _environment;    
        const string xmlRoot = "CustomerBulkLoad";
        const string XML_FILESEARCHPATTERN = "FileSearchPattern";
        const string XML_FILEPATH = "FilePath";
        const string CONFIG_ENVIRONMENT = "Environment";

    public FileLoadConfiguration(int clientID)
    {
        _clientID = clientID;
    }
    protected FileLoadConfiguration()
    {

    }
    private string _filePath;

	public string FilePath
	{
		get { return _filePath;}
		set { _filePath = value;}
	}
	private string _fileSearchPattern;

	public string FileSearchPattern
	{
		get { return _fileSearchPattern;}
		set { _fileSearchPattern = value;}
	}

	private string _fileWorkingExtension;

	public string FileWorkingExtension
	{
		get { return _fileWorkingExtension;}
		set { _fileWorkingExtension = value;}
	}

	private string _fileDoneExtension;

	public string FileDoneExtension
	{
		get { return _fileDoneExtension;}
		set { _fileDoneExtension = value;}
	}
	
 
        public void LoadConfiguration()
        {
            BulkFileConfiguration config = new BulkFileConfiguration();
            string xmlConfig;
            try
            {
                //load settings from appconfig
                var appSettings = ConfigurationManager.AppSettings;
                _fileDoneExtension = appSettings[CONFIG_FILEDONEEXTENSION];
                _fileWorkingExtension = appSettings[CONFIG_FILEWORKINGEXTENSION];
                _environment = appSettings[CONFIG_ENVIRONMENT];
                    //Load Settings from file
                config.GetConfiguration(_clientID, ComponentName,_environment,out xmlConfig);
                XDocument xdoc = XDocument.Parse(xmlConfig);
                _filePath = (from x in xdoc.Elements(xmlRoot)
                           select x.Element(XML_FILEPATH).Value).ToList().FirstOrDefault();
                _fileSearchPattern =  (from x in xdoc.Elements(xmlRoot)
                           select x.Element(XML_FILESEARCHPATTERN).Value).ToList().FirstOrDefault();

            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error reading app settings");
            }
        }
    }
}
