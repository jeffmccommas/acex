﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CE.InsightsBulk.Infrastructure;
using System.IO;
using CE.InsightsBulk.Infrastructure.Interfaces;
namespace CE.InsightsBulk.LoadCustomer
{
     class LoadCustomer
    {
        static void Main(string[] args)
        {
            var options = new Options();
            CommandLine.Parser parser = new Parser();
            try
            {

            DirectoryInfo dir;
            IBulkFile custFile;
            bool FilesFound = false;
            if (parser.ParseArguments(args, options))
            {
                // consume Options type properties
                if (options.Verbose)
                {
                    Console.WriteLine("application starting with " + options.ClientId);
                }
                else
                    Console.WriteLine("starting");
            }
            else
                Console.WriteLine(options.GetUsage());

            //Load Configuration
            FileLoadConfiguration config = new FileLoadConfiguration(87);
            config.LoadConfiguration();
            dir = new DirectoryInfo(config.FilePath);
            //Loop through all the files that we have
            foreach (FileInfo file in dir.GetFiles(config.FileSearchPattern))
            {
                Console.WriteLine("FileFound: " + file.Name);
                FilesFound= true;
                custFile = new CustomerInputFile(file,Convert.ToInt32(options.ClientId));
                custFile.ProcessFile();
               
            }
            if (!FilesFound)
            {
                Console.WriteLine("no Files Found");
            }

        }
            catch (Exception e)
            {

                Console.WriteLine("exception = " + e.Message);
            }
        }

    }
}
