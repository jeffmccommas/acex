﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;

namespace CE.InsightsBulk.LoadCustomer
{
    public class Options
    {
  [Option("c",HelpText = "ClientID 0 defaults to all clients", Required = true)]
  public string ClientId { get; set; }

  [Option("v", HelpText = "Print details during execution.")]
  public bool Verbose { get; set; }

   [HelpOption]
  public string GetUsage()
  {
    // this without using CommandLine.Text
    var usage = new StringBuilder();
    usage.AppendLine("Load Customer Bulk Data usage:  LoadCustomer -c87 where 87 = clientid");
    return usage.ToString();
  }
    }
}
