﻿/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp');

gulp.task('bundle:app', function () {
    // place code for your default task here
    var Builder = require('systemjs-builder');
    var builder = new Builder("", "./wwwroot/system.config.js");

    builder.loader.defaultJSExtensions = true;

    builder.bundle('wwwroot/app/main.js', 'wwwroot/app/bundle.js', { minify: true })
   .then(function () { console.log('app build complete'); })
   .catch(function (err) {
       console.log('app build error');
       console.log(err);
   });
});