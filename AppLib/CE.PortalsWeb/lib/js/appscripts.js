﻿
    $(function () {
        $("#dialog").dialog({
            height: "auto",
            width: 400,
            modal: true,
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "explode",
                duration: 1000
            }
        });


    });
   
    $(document).keypress(function (e) {
        if (e.which === 41) {
            if($('#dialog').dialog('isOpen')===true)
                return;


            $.ajax({
                url: "/DashBoard/GetPortalsApiUrl"
            }).then(function (webdata) {
                $.ajax({
                    url: "/DashBoard/GetPortalsWebAppVersion"
                }).then(function(appversion) {
                    $.ajax({
                        url: webdata + "/api/PortalsApi/ApplicationConfig"
                    }).then(function(data) {
                        console.log(data);
                        $('#div1').html(
                            "<ul>" +
                            "<li> Environment: " +
                            data.environment +
                            "</li>" +
                            "<li> API URL - website: " +
                            webdata +
                            "</li>" +
                            "<li> API URL - API: " +
                            data.apiBaseUrl +
                            "</li>" +
                            "<li> Client timeout: " +
                            data.clientcachetimeout +
                            "</li>" +
                            "<li> Server timeout: " +
                            data.servercachetimeout +
                            "</li>" +
                            "<li> Event Hub: " +
                            data.eventtrackinghub +
                            "</li>" +
                            "<li> Version: " +
                            appversion +
                            "</li>" +

                            "</ul>"
                        );


                        $("#dialog").dialog("open");

                    });
                });

            });

        }
    });



    
    
    System.import('app').catch(function (err) { console.error(err); });
    