﻿import { NgModule, CUSTOM_ELEMENTS_SCHEMA, Component, enableProdMode} from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppComponent } from "./app.component";
import { PortalListComponent } from "./components/shared/portallist/portallist.component";
import { HttpModule } from "@angular/http";
import { ObjToArr } from "./components/helpers/pipe";
import { RouterModule, Routes } from "@angular/router"; 
import { BenchmarkPortalComponent } from "./components/benchmark/home.component";
import { ReportsComponent } from "./components/reports/reports.component";
import { WaterComponent } from "./components/water/water.component";
import { DashBoardComponent } from "./components/shared/dashboard/dashboard.component";
import { MetersComponent } from "./components/benchmark/meters.component";
import { GridModule } from "@progress/kendo-angular-grid";
import { ModalModule, AlertModule } from 'ngx-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination'; 

import { AccountDetailsComponent } from "./components/benchmark/accountdetails.component";
import { PropertyDetailsComponent } from "./components/benchmark/propertydetails.component";
import { MeterMappingComponent } from "./components/benchmark/metermapping.component";
import { BillingComponent } from "./components/benchmark/billdata.component";
import { ConsumptionComponent } from "./components/benchmark/consumptiondata.component";
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { PopoverModule } from 'ngx-bootstrap';
import { LoginComponent } from "./components/shared/login/login.component";
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { UserProfile } from "./components/shared/userManagement/userProfile.component";
import { AccordionModule } from 'ngx-bootstrap';
import { Edituser } from "./components/shared/userManagement/editUser.component";
import { AddNewUser } from "./components/shared/userManagement/addNewUser.component";
import { MappedCustomer } from "./components/benchmark/mappedcustomer.component";
import { MeterMapColumnComponent } from "./components/benchmark/metermapcolumn.component";
import { AcceptedMetersNew } from "./components/benchmark/acceptedmetersnew.component";
import { AcceptedMetersList } from "./components/benchmark/acceptedmeterslist.component";
import { MeterDetails } from "./components/benchmark/meterdetails.component";
import { MetricsComponent } from "./components/benchmark/metrics.component";
import { MetricsNotFoundComponent } from "./components/benchmark/metricsnotfound.component";
import { ToggleDivDirective } from "./components/shared/directives/togglediv.directive";
import { ShowHideInput } from './components/shared/directives/showHide.directive';
import { PhonePipe } from "./components/helpers/phonePipe";
import { ModalComponent } from "./components/shared/common/modal.component";

//enableProdMode();

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        GridModule, NgxPaginationModule,
        PopoverModule.forRoot(),
        ModalModule.forRoot(),
        AlertModule.forRoot(),
        AccordionModule.forRoot(),
        ConfirmationPopoverModule.forRoot({           
            focusButton: 'confirm',
            placement: 'left',
            confirmButtonType: 'default',

        }),
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot([
            { path: 'Login', component: LoginComponent },
            {
                path: "Dashboard", component: DashBoardComponent, children: [
                    { path: "BenchMark", component: BenchmarkPortalComponent },
                    { path: "MetersScreen", component: MetersComponent },                                     
                    { path: '', redirectTo: 'BenchMark', pathMatch: 'full' },
                    { path: 'EditUser', component: Edituser },  
                                   
                ]
            },
           
            { path: '', redirectTo: 'Login', pathMatch: 'full' }          
        
        ])],
       declarations: [AppComponent,
        PortalListComponent,
        ObjToArr,
        ReportsComponent,
        WaterComponent,
        DashBoardComponent,
        BenchmarkPortalComponent,
        MetersComponent,
        AccountDetailsComponent,
        PropertyDetailsComponent,
           LoginComponent,
           UserProfile,
           Edituser,
           MeterMappingComponent,
           BillingComponent,
           AddNewUser,
           MappedCustomer,
           MeterMapColumnComponent,
           AcceptedMetersNew,
           AcceptedMetersList,
           MeterDetails,
           ConsumptionComponent,
           ToggleDivDirective,
           ShowHideInput,
           MetricsComponent,
           MetricsNotFoundComponent,
           PhonePipe,
           
            ModalComponent
        ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap: [AppComponent]
})
export class AppModule { }
