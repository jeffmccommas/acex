﻿import { Injectable, Input } from "@angular/core"
import { Http, Headers, URLSearchParams, BaseRequestOptions } from "@angular/http";
import { Subscription } from "rxjs";
import { Observable } from "rxjs/Observable";
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ApiService } from "../../shared/api.service";

@Injectable()
export class PortalListService {
    @Input() res: Object;
    http: Http;
    constructor(http: Http, private activatedRoute: ActivatedRoute, private router: Router, private apiService : ApiService) {
        this.http = http;
    }

    onNavigate(location: string) {
        let navigationExtras: NavigationExtras = {
            queryParamsHandling: "preserve",
            relativeTo: this.activatedRoute

        };
        this.router.navigate([location], navigationExtras);
    }

    getUserDetails(apiURL : string, parameters: string)
    {
        //let headers = new Headers();
        //headers.append("WebToken", parameters);
        //headers.append("Content-Type", "application/json; charset=utf-8");
        //return this.http.get(apiURL + "/api/PortalsApi/UserDetails", {
        //    headers: headers
        //});

        return this.apiService.get(apiURL + "/api/PortalsApi/UserDetails");
        
    }
}