﻿import { runInThisContext } from 'vm';
import { CommonModule } from '@angular/common';
import { NgModule, Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Observable } from "rxjs/Observable";
import { NavigationExtras, Router, RouterModule } from '@angular/router';
import { ApiService } from "../api.service";
import { ActivatedRoute } from '@angular/router';
import { ContentService } from "../content/content.service";
import { PortalListService } from "./portallist.service";
import { UserProfile } from "../userManagement/userProfile.component";
import { ModalDirective } from 'ngx-bootstrap';
import { FormGroup, FormArray, Validators, ReactiveFormsModule } from '@angular/forms';
import { User } from "../user";
import { UserManagementService } from "../userManagement/userManagement.service";
import "rxjs/add/operator/catch";
import { Edituser } from "../userManagement/editUser.component";




@Component({
    selector: 'portallist',
    moduleId: module.id,
    templateUrl: 'portallist.component.html',
    providers: [ApiService, PortalListService]    
})

@NgModule ({
        imports: [CommonModule, RouterModule]
        
})
export class PortalListComponent implements OnInit{
    public arrayOfKeys;

    result: any;
    content: any;
    parameters: string;
    enableLoader = true;
    elementRef: ElementRef;
    fullPath: string;
    router: Router;
    params: string;
    appService: ApiService;
    private tabkey = 'tab.espmportals';
    private WidgetType = 'tab';
    apiBaseUrl: string;
    userDetails: any;
    @ViewChild('userProfile') userProfile: UserProfile;
    @ViewChild('staticuserProfile')
    public staticuserProfile: ModalDirective;
    @ViewChild('modal')
    modal: any;
    public ApplicationUser: any = {};
    public addNewuserComplete: boolean = false;
    public loading: boolean = false;
    public error: boolean = false;
    public errorMessage: string;
    public userMenu : any;
    
    constructor(elementRef: ElementRef, router: Router, appService: ApiService, private contentService: ContentService,
        private portalService: PortalListService, private userManagementService: UserManagementService
        ) {
        this.elementRef = elementRef;
        this.enableLoader = true;
        this.router = router;
        this.appService = appService;
        this.parameters = appService.getParams();
        this.userDetails = null;
       
    }

    ngOnInit() {
        this.appService.getApIUrl().subscribe(
            res => {
                this.apiBaseUrl = res;
                this.portalService.getUserDetails(this.apiBaseUrl, this.parameters).subscribe(res => { this.userDetails = res.json(); console.debug(this.userDetails) });
                this.contentService.getTabContent(this.tabkey, this.WidgetType, this.apiBaseUrl)
                    .subscribe(res => {
                        this.content = res;
                    });
                this.contentService.getUserMenuContent('tab.admin', 'menu', this.apiBaseUrl)
                    .subscribe(res => {
                        this.userMenu = res;
                    })
            });
    }
    
    loadPortals() {
        this.result = {};
        console.debug('load portals');
        console.debug(this.parameters);
        this.appService.getAppPortals()
            .subscribe(res => {
                this.result = res; this.onInitialLoad();
            },
            err => 
                console.log(this.result),
            () => this.disableLoader()
            );
    }

    onInitialLoad()
    {
        var result = this.result.find(x => x._isDefault === true);          
        console.debug(result._component);
                    
    }
    disableLoader() {
        if (this.result === undefined || this.result === null) {
            console.debug('Loader disabled');
        }
        this.enableLoader = false;
    }

    logout(e)
    {
        localStorage.removeItem("currentUser");
    }
    loadUserProfile()
    {
        this.appService.postEvents("Visited Profile", "", "");
        this.staticuserProfile.show();
        this.userProfile.getUserProfileDetails();
    }
    
}


    