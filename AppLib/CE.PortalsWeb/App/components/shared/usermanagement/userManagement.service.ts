﻿import { Injectable, Inject } from "@angular/core";
import { Http, Headers, URLSearchParams, BaseRequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { ApiService } from "../../shared/api.service";
import { Subject } from 'rxjs/Subject';

@Injectable()
export class UserManagementService {

    private notify = new Subject<any>();
    notifyObservable$ = this.notify.asObservable();

    constructor(private http: Http, private apiService: ApiService) {

    }

    getUserProfile(apiBaseUrl: string)
    {
        return this.apiService.get(apiBaseUrl + "/api/PortalsApi/LoggedInUserProfile");
    }

    addNewuser(apiBaseUrl: string, newUser: any)
    {
        return this.apiService.post(apiBaseUrl + '/api/PortalsApi/AddUser', newUser ); 
    }

    getUserDetails(apiBaseUrl: string, UserId: number)
    {
        return this.apiService.get(apiBaseUrl + "/api/PortalsApi/UserDetails?UserId=" + UserId);
    }

    getAllUsers(apiBaseUrl: string) {
        return this.apiService.get(apiBaseUrl + "/api/PortalsApi/UserList");
    }

    updateUser(apiBaseUrl: string, newUser: any) {
        return this.apiService.post(apiBaseUrl + '/api/PortalsApi/EditUser', newUser);
    }
    enableDisableUser(apiBaseUrl: string, UserId: number, enable: boolean)
    {
        let newUser: any = {};
        newUser.UserId = UserId;
        newUser.Enabled = enable;
        return this.apiService.post(apiBaseUrl + '/api/PortalsApi/UpdateUserStatus', newUser);
    }
    public notifyOther(data: any) {
        if (data) {
            this.notify.next(data);
        }
    }
}