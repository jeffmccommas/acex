﻿import { ApiService } from "../api.service"
import { Component, Input, ViewChild, CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { UserManagementService } from './userManagement.service';
import { AddNewUser } from './addNewUser.component';
import { ContentService } from "../content/content.service";
import { Subscription } from 'rxjs/Subscription';

@NgModule({
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    declarations : [AddNewUser]

})

@Component({
    selector: "userprofile",
    moduleId: module.id,
    templateUrl: "userProfile.component.html"

})

export class UserProfile {

    private apiBaseUrl: string;
    private result: any = null;
    private editUsers: boolean = false;
    private loading: boolean = true;
    private enableEdit: boolean = false;
    @Input() public UserId: number = null;
    @ViewChild('editUserComponent') editUserComponent: AddNewUser;
    private tabkey = 'tab.admin';
    private WidgetType = 'manageuser';
    private content: any;
    private subscription: Subscription;
    constructor(private apiService: ApiService, private userManagementService : UserManagementService, private contentService : ContentService) {
    }

    ngOnInit() {
        
        this.apiService.getApIUrl().subscribe(
            res => {
                this.apiBaseUrl = res;
                this.contentService.getManageUserContent(this.tabkey, this.WidgetType, this.apiBaseUrl)
                    .subscribe(res => {
                        this.content = res;
                    });
                this.subscription = this.userManagementService.notifyObservable$.subscribe((res) => {
                    if (res.hasOwnProperty('option') && res.option === 'call_child') {
                        if (res.value === this.UserId)
                            this.getUserDetails(this.UserId);

                    }
                });
               
            });

    }

    getUserProfileDetails()
    {
        this.loading = true;
        this.userManagementService.getUserProfile(this.apiBaseUrl).subscribe(
            res => {
                this.result = res.json();
                this.loading = false;
            });

    }

    getUserDetails(userId : number)
    {
        this.editUsers = true;
        this.loading = true;
        this.userManagementService.getUserDetails(this.apiBaseUrl, userId).subscribe(
            res => {
                this.result = res.json();
                this.loading = false;
            });
    }

    disableUser(choice: boolean, UserId: number) {
        this.apiService.postEvents("User clicked on Disable for user id: " + this.UserId, "", "");
        this.loading = true;
        this.userManagementService.enableDisableUser(this.apiBaseUrl, UserId, !choice).subscribe(res => {
            this.getUserDetails(UserId);
        })
    }

    editUser(UserId : number)
    {
        this.apiService.postEvents("User clicked on Edit for user id: " + this.UserId, "", "");
        this.enableEdit = true;
        console.debug('User ID for Edit' + UserId);
        this.editUserComponent.editUser(UserId);
    }

    canceledituser()
    {
        
        this.enableEdit = false;
    }
}