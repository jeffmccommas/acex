﻿import { ApiService } from "../api.service"
import { Component, Input, ViewChild, Injectable, Inject, forwardRef } from "@angular/core";
import { UserManagementService } from './userManagement.service';
import { ShowHideInput } from '../directives/showHide.directive';
import { UserProfile } from './userProfile.component';
import { ContentService } from "../content/content.service";

@Component({
    selector: "addUser",
    moduleId: module.id,
    templateUrl: 'addUser.component.html'

})

export class AddUser {

    show = false;
    @Input() public UserId: number = null;
    private loading: boolean = false;
    private error: boolean = false;
    private errorMessage: string = "";
    private apiBaseUrl: string;
    public ApplicationUser: any = {};
    public isEdit: boolean = false;
    private tabkey = 'tab.admin';
    private WidgetType = 'adduser';
    private content: any;

    constructor(private apiService: ApiService, private userManagementService: UserManagementService,
        @Inject(forwardRef(() => UserProfile)) private userProfile: UserProfile,
        private contentService: ContentService) {
    }

    ngOnInit() {
        this.apiService.getApIUrl().subscribe(
            res => {
                this.apiBaseUrl = res;
                if (this.UserId != null) {
                    this.editUser(this.UserId);
                }
                this.contentService.getAddUserContent(this.tabkey, this.WidgetType, this.apiBaseUrl).subscribe(res => {
                    this.content = res;
                });

            });
    }

    add_editUser() {
        if (this.UserId == null) {
            this.addNewUser();
        }
        else {
            this.updateUser();
        }
    }
    addNewUser() {
        this.loading = true;
        this.error = false;
        this.show = false;
        this.userManagementService.addNewuser(this.apiBaseUrl, this.ApplicationUser)
            .subscribe(
            data => console.debug('Data' + data),
            err => {
                console.error(err.status);
                if (err.status == 409) {
                    console.error('User Name Already Exists. Please try a different user name.');
                    this.errorMessage = 'User Name Already Exists. Please try a different user name.';
                }
                else if (err.status == 500) {
                    this.errorMessage = 'Server Error: Please contact your system administrator.'
                }
                this.loading = false; this.error = true;
            },
            () => { this.loading = false; }
            );
    }

    updateUser() {
        this.loading = true;
        this.error = false;

        this.userManagementService.addNewuser(this.apiBaseUrl, this.ApplicationUser)
            .subscribe(
            data => console.debug('Data' + data),
            err => {
                console.error(err.status);
                if (err.status == 409) {
                    console.error('User Name Already Exists. Please try a different user name.');
                    this.errorMessage = 'User Name Already Exists. Please try a different user name.';
                }
                else if (err.status == 500) {
                    this.errorMessage = 'Server Error: Please contact your system administrator.'
                }
                this.loading = false; this.error = true;
            },
            () => { this.loading = false; }
            );
    }

    editUser(UserId: number) {
        this.loading = true;
        this.isEdit = true;
        this.userManagementService.getUserDetails(this.apiBaseUrl, UserId).subscribe
            (res => {
                let user = res.json();
                this.ApplicationUser.FirstName = user.firstName;
                this.ApplicationUser.LastName = user.lastName;
                this.ApplicationUser.UserName = user.userName;
                this.ApplicationUser.Administrator = user.isAdminUser;
                this.ApplicationUser.Enabled = user.enabled;
                this.loading = false;
            })
    }

    @ViewChild(ShowHideInput) input: ShowHideInput;

    toggleShow() {
        this.show = !this.show;
        if (this.show) {
            this.input.changeType("text");
        }
        else {
            this.input.changeType("password");
        }
    }
}