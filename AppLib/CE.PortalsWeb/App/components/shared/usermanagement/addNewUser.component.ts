﻿import { ApiService } from "../api.service"
import { Component, Input, Injectable, Inject, forwardRef, ViewChild } from "@angular/core";
import { UserManagementService } from './userManagement.service'
import { UserProfile } from './userProfile.component'
import { ContentService } from "../content/content.service";
import { ShowHideInput } from '../directives/showHide.directive';

@Component({
    selector: "addnewuser",
    moduleId: module.id,
    templateUrl: 'addNewUser.component.html',


})

export class AddNewUser {

    show = false;

    @Input() public UserId: number = null;
    private loading: boolean = false;
    private error: boolean = false;
    private errorMessage: string = "";
    private apiBaseUrl: string;
    public ApplicationUser: any = {};
    public isEdit: boolean = false;
    private tabkey = 'tab.admin';
    private WidgetType = 'adduser';
    private content: any;
    constructor(private apiService: ApiService, private userManagementService: UserManagementService,
        @Inject(forwardRef(() => UserProfile)) private userProfile: UserProfile,
        private contentService: ContentService) {
    }

    ngOnInit() {
        this.apiService.getApIUrl().subscribe(
            res => {
                this.apiBaseUrl = res;
                if (this.UserId != null) {
                    this.editUser(this.UserId);
                }
                this.contentService.getAddUserContent(this.tabkey, this.WidgetType, this.apiBaseUrl).subscribe(res => {
                    this.content = res;
                });
            });
    }

    add_editUser() {
        console.debug('Edit add user component' + this.isEdit);
        if (!this.isEdit) {
            this.addNewUser();
        }
        else {
            this.updateUser();
        }
    }
    addNewUser() {
        this.loading = true;
        this.error = false;
        this.show = false;
        this.userManagementService.addNewuser(this.apiBaseUrl, this.ApplicationUser)
            .subscribe(
            data => console.debug('Data' + data),
            err => {
                console.error(err.status);
                if (err.status == 409) {
                    console.error('User Name Already Exists. Please try a different user name.');
                    this.errorMessage = 'User Name Already Exists. Please try a different user name.';
                }
                else if (err.status == 500) {
                    this.errorMessage = 'Server Error: Please contact your system administrator.';
                }
                this.loading = false; this.error = true;
            },
            () => { this.loading = false; }
            );
    }

    updateUser() {
        this.loading = true;
        this.error = false;
        this.apiService.postEvents("User Saved the changes for the user id: " + this.UserId, "", "");
        this.userManagementService.updateUser(this.apiBaseUrl, this.ApplicationUser)
            .subscribe(
            data => console.debug('Data' + data),
            err => {
                console.error(err.status);
                this.errorMessage = err.statusMessage;
                this.loading = false; this.error = true;

            },
            () => {
                this.loading = false;
                this.isEdit = false;
                this.userProfile.getUserDetails(this.ApplicationUser.UserId);
                this.userProfile.canceledituser();
            }
            );
    }

    editUser(UserId: number) {
        this.loading = true;
        this.isEdit = true;
        console.debug('Edit add user component' + this.isEdit);
        this.userManagementService.getUserDetails(this.apiBaseUrl, UserId).subscribe
            (res => {
                let user = res.json();
                this.ApplicationUser.FirstName = user.firstName;
                this.ApplicationUser.LastName = user.lastName;
                this.ApplicationUser.UserName = user.userName;
                this.ApplicationUser.Administrator = user.isAdminUser;
                this.ApplicationUser.Enabled = user.enabled;
                this.ApplicationUser.UserId = user.userId;
                this.loading = false;
            })
    }

    cancelUserEdit() {
        this.apiService.postEvents("User canceled user edit for user id: " + this.UserId, "", "");
        this.userProfile.canceledituser();
    }

    @ViewChild(ShowHideInput) input: ShowHideInput;

    toggleShow() {
        this.show = !this.show;
        if (this.show) {
            this.input.changeType("text");
        }
        else {
            this.input.changeType("password");
        }
        
    }

}