﻿import { ApiService } from "../api.service"
import { Component, ViewChild } from "@angular/core";
import { UserManagementService } from './userManagement.service'
import { AccordionModule } from 'ngx-bootstrap';
import { UserProfile } from './userProfile.component';
import { ModalDirective } from 'ngx-bootstrap';
import { AddNewUser } from './addNewuser.component'
import { ContentService } from "../content/content.service";
import { ShowHideInput } from '../directives/showHide.directive';

@Component({
    selector: "editUser",
    moduleId: module.id,
    templateUrl: 'editUser.component.html'

})

export class Edituser {

    show = false;
    private apiBaseUrl: string;
    private userList: Object[];
    @ViewChild('userProfileDetails') userProfileDetails: UserProfile;
    @ViewChild('userProfile')
    public userProfile: ModalDirective;
    @ViewChild('modal')
    modal: any;
    private loading: Boolean = false;
    private ApplicationUser: any = {};
    private addNewuserComplete: boolean;
    private error: boolean = false;
    private errorMessage: string;
    public status: any = {
        isFirstOpen: true,
        isOpen: false
    };
    private content: any;
    private tabkey = 'tab.admin';
    private WidgetType = 'adduser';
    constructor(private apiService: ApiService, private userManagementService: UserManagementService, private contentService: ContentService) {
    }

    ngOnInit() {
        this.apiService.postEvents("User clicked on Manage Users", "", "");
        this.apiService.getApIUrl().subscribe(
            res => {
                this.apiBaseUrl = res;
                this.contentService.getAddUserContent(this.tabkey, this.WidgetType, this.apiBaseUrl).subscribe(res => {
                    this.content = res;
                });
                this.loadUsers();

            });
    }
    loadUsers() {
        this.userManagementService.getAllUsers(this.apiBaseUrl).subscribe(
            res => {
                this.userList = res.json();
            });
    }
    openUserDetails(UserId: number) {
        this.userProfileDetails.getUserDetails(UserId);
    }

    onAddNewUser() {
        this.apiService.postEvents("User clicked on Add User", "", "");
        this.loading = false;
        this.ApplicationUser = {};
        this.addNewuserComplete = false;
        this.error = false;
        this.userProfile.show();
    }

    addNewUserPopUp() {


    }

    addNewUserPortalList() {
        this.apiService.postEvents("User clicked on Save to add a new user", "", "");
        this.loading = true;
        this.error = false;
        this.show = false;
        this.userManagementService.addNewuser(this.apiBaseUrl, this.ApplicationUser)
            .subscribe(
            data => console.debug('Data' + data),
            err => {
                console.debug(err.status);
                if (err.status == 409) {
                    console.error('User Name Already Exists. Please try a different user name.');
                    this.errorMessage = 'User Name Already Exists. Please try a different user name.';
                }
                else if (err.status == 500) {
                    this.errorMessage = 'Server Error: Please contact your system administrator.'
                }
                this.loading = false; this.error = true;
            },
            () => { this.loading = false; this.addNewuserComplete = true; this.loadUsers(); }
            );
        //.catch(err => {
        //    if (err.status == 409) {
        //        this.errorMessage = "User Already Exists";
        //    }
        //    else if (err.status == 500)
        //    {
        //        this.errorMessage = "Error Occurred. Please contact you System Administtrator";
        //    }
        //    else
        //    {
        //        this.loading = false;
        //        this.addNewuserComplete = true;
        //    }
        //});
        //;
    }

    @ViewChild(ShowHideInput) input: ShowHideInput;

    toggleShow() {
        this.show = !this.show;
        if (this.show) {
            this.input.changeType("text");
        }
        else {
            this.input.changeType("password");
        }
    }

    loaduserdetails(userid) {
        this.userManagementService.notifyOther({ option: 'call_child', value: userid  });
    }
}