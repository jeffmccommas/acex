﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { ApiService } from "./api.service";
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from "rxjs/Observable";
import { tokenNotExpired } from 'angular2-jwt';



@Injectable()
export class LoginService {
    private isLoggdeIn = false;
    private returnUrl: string;
    private route: ActivatedRoute;
    private router: Router;
    constructor(private http: Http, private apiService: ApiService) {

    }
    login(apiBaseURL: string, username: string, password: string) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(apiBaseURL + '/api/PortalsApi/UserAuth', JSON.stringify({ username: username, password: password }), options);

    }

    logout() {
        localStorage.removeItem('currentUser');
        this.isLoggdeIn = false;
    }


}