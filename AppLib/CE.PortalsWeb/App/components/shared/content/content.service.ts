﻿import { Injectable, Inject } from "@angular/core";
import { Http, Headers, URLSearchParams, BaseRequestOptions } from "@angular/http";

import { Observable } from "rxjs/Observable";
import { Subscription } from "rxjs";
import { Router, ActivatedRoute } from "@angular/router";
import { OnInit, OnDestroy } from "@angular/core";

import { ApiService } from "../../shared/api.service";

@Injectable()
export class ContentService {

    constructor(private http: Http, private apiService: ApiService) {

    }

    getContactsContent(tabKey, widgettype, portalsApiBaseUrl) {
        try {
            console.debug('Get content called' + portalsApiBaseUrl);
            var language = window.navigator.language;
            return this.apiService.get(portalsApiBaseUrl + "/api/InsightsApi/ContactsContent?locale=" + language + "&tabkey=" + tabKey + "&WidgetType=" + widgettype).map(res => res.json());
        }
        catch (ex) {
            throw (ex);
        }
    }

    getMetersContent(tabKey, widgettype, portalsApiBaseUrl) {
        try {
            var language = window.navigator.language;
            return this.apiService.get(portalsApiBaseUrl + "/api/InsightsApi/MetersContent?locale=" + language + "&tabkey=" + tabKey + "&WidgetType=" + widgettype).map(res => res.json());
        }
        catch (ex) {
            throw (ex);
        }
    }

    getTabContent(tabKey, widgettype, portalsApiBaseUrl) {
        try {
            var language = window.navigator.language;
            return this.apiService.get(portalsApiBaseUrl + "/api/InsightsApi/TabContent?locale=" + language + "&tabkey=" + tabKey + "&WidgetType=" + widgettype).map(res => res.json());
        }
        catch (ex) {
            throw (ex);
        }
    }

    getLoginContent(tabKey, widgettype, portalsApiBaseUrl) {
        try {
            var language = window.navigator.language;
            return this.apiService.get(portalsApiBaseUrl + "/api/InsightsApi/LoginContent?locale=" + language + "&tabkey=" + tabKey + "&WidgetType=" + widgettype).map(res => res.json());
        }
        catch (ex) {
            throw (ex);
        }
    }

    getAddUserContent(tabKey, widgettype, portalsApiBaseUrl) {
        try {
            var language = window.navigator.language;
            return this.apiService.get(portalsApiBaseUrl + "/api/InsightsApi/AddUserContent?locale=" + language + "&tabkey=" + tabKey + "&WidgetType=" + widgettype).map(res => res.json());
        }
        catch (ex) {
            throw (ex);
        }
    }

    getManageUserContent(tabKey, widgettype, portalsApiBaseUrl) {
        try {
            var language = window.navigator.language;
            return this.apiService.get(portalsApiBaseUrl + "/api/InsightsApi/ManageUserContent?locale=" + language + "&tabkey=" + tabKey + "&WidgetType=" + widgettype).map(res => res.json());
        }
        catch (ex) {
            throw (ex);
        }
    }

    getEditUserContent(tabKey, widgettype, portalsApiBaseUrl) {
        try {
            var language = window.navigator.language;
            return this.apiService.get(portalsApiBaseUrl + "/api/InsightsApi/EditUserContent?locale=" + language + "&tabkey=" + tabKey + "&WidgetType=" + widgettype).map(res => res.json());
        }
        catch (ex) {
            throw (ex);
        }
    }

    getUserProfileContent(tabKey, widgettype, portalsApiBaseUrl) {
        try {
            var language = window.navigator.language;
            return this.apiService.get(portalsApiBaseUrl + "/api/InsightsApi/UserProfileContent?locale=" + language + "&tabkey=" + tabKey + "&WidgetType=" + widgettype).map(res => res.json());
        }
        catch (ex) {
            throw (ex);
        }
    }

    getUserMenuContent(tabKey, widgettype, portalsApiBaseUrl) {
        try {
            var language = window.navigator.language;
            return this.apiService.get(portalsApiBaseUrl + "/api/InsightsApi/UserMenuContent?locale=" + language + "&tabkey=" + tabKey + "&WidgetType=" + widgettype).map(res => res.json());
        }
        catch (ex) {
            throw (ex);
        }
    }

    getConfigurationContent(portalsApiBaseUrl) {
        try {
            var language = window.navigator.language;
            return this.apiService.get(portalsApiBaseUrl + "/api/InsightsApi/ConfigurationContent?locale=" + language).map(res => res.json());
        }
        catch (ex) {
            throw (ex);
        }
    }
}


