﻿import { Injectable, Inject } from '@angular/core';


@Injectable()
export class ApiConfiguration {
    public Server: string = "http://localhost:51157/";
    //public Server: string = "http://aclaceportalsapiwadev.azurewebsites.net/";
    public ApiUrl: string = "api/";
    public ServerWithApiUrl = this.Server + this.ApiUrl;
    public ApiUserType = "UILBasicAuthUser";
}