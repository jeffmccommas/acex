﻿export class User {
    Userid: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    ClientId: string;
    Administrator: boolean;
    Enabled: boolean;
}