﻿import { Injectable, Inject, EventEmitter } from "@angular/core";
import { Http, Headers, URLSearchParams, BaseRequestOptions, Response, RequestOptions, RequestOptionsArgs, RequestMethod, Request, Connection } from "@angular/http";

import { Observable } from "rxjs/Observable";
import { ApiConfiguration } from "./api.config";
import { Subscription } from "rxjs";
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { OnInit, OnDestroy } from "@angular/core";
import "rxjs/add/operator/catch";
import { tokenNotExpired, JwtHelper } from 'angular2-jwt';


export enum Action { QueryStart, QueryStop };

@Injectable()
export class ApiService {

    http: Http;
    portalsApiBaseUrl: string;
    params: string;
    angparams: string;
    subscription: Subscription;
    activatedRoute: ActivatedRoute;
    jwtHelper: JwtHelper = new JwtHelper();
    process: EventEmitter<any> = new EventEmitter<any>();
    private eventData: any = {};

    constructor(http: Http, apiConfiguration: ApiConfiguration, activatedRoute: ActivatedRoute, private router: Router) {
        this.http = http;
        this.activatedRoute = activatedRoute;
        if (this.portalsApiBaseUrl == null) {
            this.getPortalsApiBaseUrl();
        }

    }

    getPortalsApiBaseUrl() {
        this.getApIUrl()
            .subscribe(
            res => {
                this.portalsApiBaseUrl = res;
                console.debug("api url -> " + res);
            },
            err => console.error("ERROR getting API url" + err.toString()),
            () => console.debug("Get URL call completed")
            );
    }


    ngOnInit() {
    }


    ngOnDestroy() {

    }

    getangParams(): string {

        let query = new URLSearchParams(window.location.search.slice(1));
        this.angparams = decodeURIComponent(query.get("parameters"));
        return this.angparams;


    }
    getParams(): string {

        this.params = localStorage.getItem("userToken");
        if (!this.params) {
            this.router.navigateByUrl('/Login');
        }
        return this.params;
    }

    createAuthorizationHeader(headers: Headers) {
        headers.append("Content-Type", "application/json; charset=utf-8");
        headers.append("WebToken", this.getParams());
    }
    refreshTokensandContent() {


        let headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.get(this.portalsApiBaseUrl + "/api/PortalsApi/RefreshToken",
            {
                headers: headers
            });


    }
    
    get(url) {
        var token = localStorage.getItem('currentUser');
        let callGet: boolean = true;
        if (token === null) {
            callGet = false;
            this.router.navigateByUrl("/Login");
            return Observable.empty<Response>();

        }
        else if (this.jwtHelper.isTokenExpired(token)) {
            
            callGet = false;
            this.router.navigateByUrl("/Login");
            return Observable.empty<Response>();
        }
        else {
            let expdate = this.jwtHelper.getTokenExpirationDate(token);
            var tokenExpTime = new Date(expdate);
            var currentTime = new Date();

            var diffInMs = tokenExpTime.valueOf() - currentTime.valueOf();
            
            let diffInSeconds: number = diffInMs / 1000 ;

            if (diffInSeconds < 20) {
                this.refreshTokensandContent().subscribe(res => {
                    let content = res.json();
                    localStorage.setItem("currentUser", content.accessToken);
                    var token = localStorage.getItem("currentUser");

                });
            }
        }
        if (callGet) {
            let headers = new Headers();
            this.createAuthorizationHeader(headers);
            return this.http.get(url, {
                headers: headers
            });
        }
        else {
            this.router.navigateByUrl("/Login");
            return Observable.empty<Response>();
        }

    }

    post(url, data) {

        var token = localStorage.getItem('currentUser');
        let callGet: boolean = true;
        if (token === null) {

            callGet = false;
            this.router.navigateByUrl("/Login");
            return Observable.empty<Response>();

        }
        else if (this.jwtHelper.isTokenExpired(token)) {

            callGet = false;
            this.router.navigateByUrl("/Login");
            return Observable.empty<Response>();

        }
        else {
            let expdate = this.jwtHelper.getTokenExpirationDate(token);
            var tokenExpTime = new Date(expdate);
            var currentTime = new Date();

            var diffInMs = tokenExpTime.valueOf() - currentTime.valueOf();
            
            let diffInSeconds: number = diffInMs / 1000;

            
            if (diffInSeconds < 20) {
                this.refreshTokensandContent().subscribe(res => {
                    let content = res.json();
                    localStorage.setItem("currentUser", content.accessToken);
                    var token = localStorage.getItem("currentUser"); //TODO:Mali. Can we remove this line?

                });
            }
        }
        if (callGet) {
            let headers = new Headers();
            this.createAuthorizationHeader(headers);
            headers.append("Content-Type", "application/json; charset=utf-8");
            return this.http.post(url, JSON.stringify(data), {
                headers: headers
            });
        }
        else {
            this.router.navigateByUrl("/Login");
            return Observable.empty<Response>();
        }

    }

    postEvents(eventType, accountId, premiseId) {
        console.debug(eventType);
        this.eventData.eventType = eventType;
        this.eventData.accountId = accountId;
        this.eventData.premiseId = premiseId;
        this.eventData.locale = window.navigator.language;
        return this.post(this.portalsApiBaseUrl + "/api/PortalsApi/PostEvents", this.eventData).subscribe(res => { console.debug("Event Posted");});
    }

    getAppPortals() {
        try {
            return this.get("./DashBoard/GetAllPortals?enc=" + this.getParams()).map(res => res.json());//.catch(this.handleError);;
        }
        catch (ex) {
            throw (ex);
        }
    }

    getApIUrl() {
        try {
            return this.http.get("./DashBoard/GetPortalsApiUrl?WebToken=" + this.getParams())
                .map(res => res.json());//.catch(this.handleError);

        }
        catch (ex) {
            throw (ex);
        }
    }

    onNavigate(location: string) {
        let navigationExtras: NavigationExtras = {
            queryParamsHandling : "preserve",
            relativeTo: this.activatedRoute

        };
        this.router.navigate([location], navigationExtras);
    }



}


