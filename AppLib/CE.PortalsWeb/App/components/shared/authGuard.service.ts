﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()


export class AuthGuard implements CanActivate {
    
    constructor(private router: Router) { }
    
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('currentUser') && this.isLoggedIn()) {
            // logged in so return true
            return true;
        }

        this.router.navigate(['/Login'], { queryParams: { returnUrl: state.url } });
        return false;
    }

    isLoggedIn() {
        return tokenNotExpired();
    }
}