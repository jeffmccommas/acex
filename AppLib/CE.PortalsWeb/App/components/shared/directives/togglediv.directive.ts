﻿import { Directive, HostListener } from '@angular/core';

@Directive(
    {
        selector: 'toggle-div'
    }

)
export class ToggleDivDirective {

 
    @HostListener('click', ['$event']) onClick(e) {
        console.log('Trigered click event');
    }
} 