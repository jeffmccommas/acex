﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { LoginService } from '../login.service';
import { ApiService } from '../api.service';
import { AlertService } from '../alert.service';
import { Observable } from 'rxjs/Rx';
import { tokenNotExpired, JwtHelper } from 'angular2-jwt';


@Component({
    selector: 'loginComponent',
    moduleId: module.id,
    templateUrl: 'login.component.html',
    styles: [`.form-signin
{
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
}
.form-signin .form-signin-heading, .form-signin .checkbox
{
    margin-bottom: 10px;
}
.form-signin .checkbox
{
    font-weight: normal;
}
.form-signin .form-control
{
    position: relative;
    font-size: 16px;
    height: auto;
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.form-signin .form-control:focus
{
    z-index: 2;
}
.form-signin input[type="text"]
{
    margin-bottom: -1px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.form-signin input[type="password"]
{
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}
.account-wall
{
    margin-top: 20px;
    padding: 40px 0px 20px 0px;
    background-color: #f7f7f7;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}
.login-title
{
    color: #555;
    font-size: 18px;
    font-weight: 400;
    display: block;
}
.profile-img
{
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
.need-help
{
    margin-top: 10px;
}
.new-account
{
    display: block;
    margin-top: 10px;
}`],
    providers : [ApiService, LoginService, AlertService]

})



export class LoginComponent {
    private apiBaseUrl: string;
    model: any = {};
    loading = false;
    returnUrl: string;
    errorMessage: string;
    showErrorMessage: boolean;


    constructor(private router: Router, private apiService: ApiService, private loginService: LoginService, private alertService: AlertService ) {
        this.returnUrl = 'Dashboard';
        this.errorMessage = '';
        this.showErrorMessage = false;
    }

    ngOnInit() {
       
        localStorage.clear();
        this.apiService.postEvents("User Log out successfull!!", "", "");
    }
    login()
    {
        localStorage.clear();

        this.loading = true;
        this.showErrorMessage = false;
        this.apiService.getApIUrl().subscribe(
            res => {
        this.apiBaseUrl = res;
        this.loginService.login(this.apiBaseUrl, btoa(this.model.UserName), btoa(this.model.Password))
            .catch(err => {
                if (err.status == 403) {
                    let error403 = new Error("You have exceeded the maximum number of invalid login attempts, and your username is now locked. Please contact the system administrator for help.");
                    return Observable.throw(error403);
                }
                else if (err.status == 401) {
                    let error401 = new Error("That is not a valid Username and Password.");
                    return Observable.throw(error401);
                }
                else if (err.status == 404) {
                    let error401 = new Error("User not found / disabled.");
                    return Observable.throw(error401);
                }
                else if (err.status == 500) {
                    let error401 = new Error("Connection Problem. Internal Server Error.");
                    return Observable.throw(error401);
                }
                else return Observable.throw(err);
            })           
            .subscribe(
            res => {
                let content = res.json();
                localStorage.setItem("currentUser", content.AccessToken);
                localStorage.setItem("userToken", content.user);
                let jwtHelper = new JwtHelper();
                var token = localStorage.getItem("currentUser");
                console.debug(jwtHelper.getTokenExpirationDate(token));
                this.router.navigate([this.returnUrl]);
                this.apiService.postEvents("User Log In Successful!!", "", "");
            },
            error => {
                this.errorMessage = error;
                this.showErrorMessage = true;
                this.loading = false;
            });
      });
    }
}