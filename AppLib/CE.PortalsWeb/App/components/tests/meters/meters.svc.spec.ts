﻿import "rxjs/add/operator/map";
import 'rxjs/add/operator/catch';
import { inject, async, TestBed } from "@angular/core/testing";
import { Component, Injector, Injectable } from "@angular/core";
import { BaseRequestOptions, Http, HttpModule, XHRBackend, Response, Headers, RequestOptions, ResponseOptions } from "@angular/http";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { MetersService } from "../../benchmark/services/meters.service";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';

import { Observable } from 'rxjs/Rx';
import { ApiService } from "../../shared/api.service";
import { ApiConfiguration } from "../../shared/api.config";
import { Router, ActivatedRoute } from "@angular/router";


class apiServiceMock {
    get() {
        return Observable.of(
            [
                {
                    'accountId': 'test',
                    'address': '<p><b>Tests Ave</b></p>'
                },
                {
                    'accountId': 'test',
                    'address': '<p><b>Tests Ave</b></p>'
                }
            ]
        );
    }
}
// Be descriptive with titles here. The describe and it titles combined read like a sentence.
describe('meters service test', function () {
    //it('has a failed dummy spec to test 1 equal to 4', function () {
    // An intentionally failing test. 
    //  expect(1).toEqual(4);
    // });

    it('has a success dummy spec to test 2 + 2', function () {
        // An intentionally successful test.
        expect(2 + 2).toEqual(4);
    });

    beforeEach(async(() => {
        TestBed.resetTestEnvironment();

        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [
                MetersService, ApiService, ApiConfiguration,
                { provide: XHRBackend, useClass: MockBackend },
                { provide: ApiService, useClass: apiServiceMock },
                { provide: ActivatedRoute, useValue: { 'params': Observable.from([{ 'id': 1 }]) } }
            ]
        }).compileComponents();;
    }));

    it('can instantiate meters service when inject service',
        inject([MetersService], (service: MetersService) => {
            expect(service instanceof MetersService).toBe(true);
        }));

    it('can provide the mockBackend as XHRBackend',
        inject([XHRBackend], (backend: MockBackend) => {
            expect(backend).not.toBeNull('backend should be provided');
        }));


   let backend: MockBackend;
    let service: MetersService;
    let apisvc: ApiService;
    let apiconfig: ApiConfiguration;
    let activatedroute: ActivatedRoute;
    let response: Response; 

    beforeEach(inject([Http, XHRBackend, ActivatedRoute, ApiConfiguration, ApiService], (http: Http, be: MockBackend, activatedroute: ActivatedRoute, apiconfig: ApiConfiguration, apiservice: ApiService) => {
        backend = be;
        service = new MetersService(http,apiservice); 
        let options = new ResponseOptions({
            body: [
                {
                    accountId: "test",
                    address: "<p><b>Tests Ave</b></p>"
                }
            ]
        });
        response = new Response(options);
    }));


    it('should get pending meters from meters service with page 1', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.getPendingMeters('', '', 1).subscribe((blogs) => {
            expect(blogs[0].accountId).toBe("test");
            expect(blogs[0].address).toBe("<p><b>Tests Ave</b></p>");
        });
    })));
});