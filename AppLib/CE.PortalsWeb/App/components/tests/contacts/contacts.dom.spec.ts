﻿//import "rxjs/add/operator/map"
//import { inject, async, TestBed, ComponentFixture } from "@angular/core/testing";
//import { Component, ViewChild, Injector, ElementRef } from "@angular/core";
////kendo
//import {
//    GridDataResult,
//    PageChangeEvent
//} from "@progress/kendo-angular-grid";
//import { BrowserModule } from "@angular/platform-browser";
//import { BaseRequestOptions, Http, HttpModule, XHRBackend, Response, ResponseOptions } from "@angular/http";

//import { BenchmarkPortalComponent } from "../../benchmark/home.component";
//import { ContactDataService_live } from "../../benchmark/services/contacts.service";
//import { ApiService } from "../../shared/api.service";
//import { ModalDirective } from 'ngx-bootstrap';
//import { ContentService } from "../../shared/content/content.service";
//import { NgxPaginationModule, PaginationInstance } from 'ngx-pagination';
//import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";


////mock contact and api services
//class MockApiService extends ApiService {
//    public res: any;
//    public unit: any = "unittest";

//    get() {
//        return this.unit;
//    }
//}

//class MockContactsService extends ContactDataService_live {
//    public res: any;
//    public unit: any = "unittest";

//    getContacts() {
//        return this.unit;
//    }
//}
////Note: still need to add kendo ui as declaration , custom element and mock it with /hook up to json result set mocked below
//describe('Contacts DOM Tests', () => {
//    beforeEach(async(() => {
//        TestBed.resetTestEnvironment();
//        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
//        ElementRef;
//        TestBed.configureTestingModule({
//            imports: [BrowserModule, HttpModule, NgxPaginationModule],
//            declarations: [BenchmarkPortalComponent]
//        })
//            .overrideComponent(BenchmarkPortalComponent,
//            {
//                remove: { providers: [ContactDataService_live] },
//                add: {
//                    providers: [
//                        { provide: ApiService, useClass: MockApiService },
//                        { provide: ContactDataService_live, useClass: MockContactsService }
//                    ]
//                }
//            }).compileComponents();
//    }));


//    xit('should get contact list', async(() => {
//        const fixture = TestBed.createComponent(BenchmarkPortalComponent);
//        const app = fixture.componentInstance;
//        const element = fixture.nativeElement;
//        var json = JSON.parse('{"title: "sample title", "introtext": "Sample intro", AccountId":"12345","Name":"xxx","Address":"777 High St, Arlington, VA, 22201","State": "P","CustomField1":"x","CustomField2":"x2","Timestamp":"12110120202","ErrorMessage":""}');
//        app.result = json;
//        fixture.detectChanges();
//        expect(element.textContent).toContain('Name');
//    }));


//    xit('should get Accept button and check for buttons on screen', async(() => {
//        const fixture = TestBed.createComponent(BenchmarkPortalComponent);
//        const app = fixture.componentInstance;
//        const element = fixture.nativeElement;
//        var json = JSON.parse('{"AccountId":"12345","Name":"xxx","Address":"777 High St, Arlington, VA, 22201","State": "P","CustomField1":"x","CustomField2":"x2","Timestamp":"12110120202","ErrorMessage":"Error"}');
//        app.result = json;
//        fixture.detectChanges();
//        // can also get the "button" element by CSS selector (e.g., by class name)  button.btn:nth-child(2)
//        const lis = fixture.nativeElement.querySelectorAll("button");
//        expect(lis.length).toBe(2);
//        //find accept button with Accept text
//        expect(element.querySelector('btn-primary')).toContain('Accept');

//    }));

    



//xit('should get Reject button and check for buttons on screen', async(() => {
//    const fixture = TestBed.createComponent(BenchmarkPortalComponent);
//    const app = fixture.componentInstance;
//    const element = fixture.nativeElement;
//    var json = JSON.parse('{"AccountId":"12345","Name":"xxx","Address":"777 High St, Arlington, VA, 22201","State": "P","CustomField1":"x","CustomField2":"x2","Timestamp":"12110120202","ErrorMessage":"Error"}');
//    app.result = json;
//    fixture.detectChanges();
//    // can also get the "button" element by CSS selector (e.g., by class name)  button.btn:nth-child(2)
//    const lis = fixture.nativeElement.querySelectorAll("button");
//    expect(lis.length).toBe(2);
//    //find accept button with Reject text
//    expect(element.querySelector('btn-default')).toContain('Reject');

//}));

    
//});