﻿//import "rxjs/add/operator/map";
//import 'rxjs/add/operator/catch';
//import { inject, async, TestBed } from "@angular/core/testing";
//import { Component, Injector, Injectable } from "@angular/core";
//import { BaseRequestOptions, Http, HttpModule, XHRBackend, Response, Headers, RequestOptions, ResponseOptions } from "@angular/http";
//import { MockBackend, MockConnection } from "@angular/http/testing";
//import { ContactsDataService, ContactDataService_live } from "../../benchmark/services/contacts.service"; 
//import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';

//import { Observable } from 'rxjs/Rx';


//// Be descriptive with titles here. The describe and it titles combined read like a sentence.
//describe('contact service test', function () {
//    //it('has a failed dummy spec to test 1 equal to 4', function () {
//        // An intentionally failing test. 
//      //  expect(1).toEqual(4);
//   // });

//    it('has a success dummy spec to test 2 + 2', function () {
//        // An intentionally successful test.
//        expect(2+2).toEqual(4);
//    });

//    beforeEach(async(() => {
//        TestBed.resetTestEnvironment();

//        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
//        TestBed.configureTestingModule({
//            imports: [HttpModule],
//            providers: [
//                ContactsDataService, ContactDataService_live,
//                { provide: XHRBackend, useClass: MockBackend }
//            ]
//        }).compileComponents();;
//    }));

//    it('can instantiate mock contact service when inject service',
//        inject([ContactsDataService], (service: ContactsDataService) => {
//            expect(service instanceof ContactsDataService).toBe(true);
//        }));

//    it('can instantiate contact service when inject service',
//        inject([ContactDataService_live], (service: ContactDataService_live) => {
//            expect(service instanceof ContactDataService_live).toBe(true);
//        }));

//    it('can provide the mockBackend as XHRBackend',
//        inject([XHRBackend], (backend: MockBackend) => {
//            expect(backend).not.toBeNull('backend should be provided');
//        }));


//    let backend: MockBackend;
//    let service: ContactsDataService;
//    let servicelive: ContactDataService_live;
//    let response: Response;

//    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
//        backend = be;
//        service = new ContactsDataService();
//        servicelive = new ContactDataService_live(http);
//        let options = new ResponseOptions({
//            body: [
//                {
//                    accountId: "test",
//                    address: "<p><b>Tests Ave</b></p>"
//                }
//            ]
//        });
//        response = new Response(options);
//    }));


//    it('should get contacts with dummy data in contacts prototype',
//        async(inject([],
//        () => {
//            backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

//            let blogs = service.getContacts();
//            expect(blogs.length).toBe(8);
//            expect(blogs[0].accountId).toBe("468963-90000");
//            expect(blogs[0].address).toBe("121 State Street Milford, MA 01010");
//            })));

//    it('should get contact from contacts service', async(inject([], () => {
//        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

//        servicelive.getContacts().subscribe((blogs) => {
//            expect(blogs.length).toBe(1);
//            expect(blogs[0].accountId).toBe("test");
//            expect(blogs[0].address).toBe("<p><b>Tests Ave</b></p>");
//        });
//    })));
//});