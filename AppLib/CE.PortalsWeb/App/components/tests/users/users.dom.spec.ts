﻿import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { Observable } from 'rxjs/Rx';

import { ModalDirective, Ng2BootstrapModule, PagerComponent, ModalModule } from 'ngx-bootstrap';
import { ContentService } from "../../shared/content/content.service";
import { BrowserModule, By } from "@angular/platform-browser";
import { BaseRequestOptions, Http, HttpModule, XHRBackend, Response, ResponseOptions } from "@angular/http";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { BenchmarkPortalComponent } from "../../benchmark/home.component";
import { NgxPaginationModule, PaginationInstance } from 'ngx-pagination';
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { inject, async } from "@angular/core/testing";
import { ComponentFixture } from "@angular/core/testing";
import { NgModule, Component, Injector, ElementRef } from "@angular/core";
import { ApiService } from "../../shared/api.service";
import { UserManagementService } from "../../shared/usermanagement/userManagement.service";
import { User } from "../../shared/user";
import { AddNewUser } from "../../shared/usermanagement/addNewUser.component";
import { runInThisContext } from 'vm';
import { CommonModule } from '@angular/common';
import { NavigationExtras, Router, RouterModule } from '@angular/router';
import { FormGroup, FormArray, Validators, ReactiveFormsModule } from '@angular/forms';

import "rxjs/add/operator/catch";
import { TestBed } from "@angular/core/testing";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";

//mock UserManagementService and app services 
class MockApiService extends ApiService {
    public res: any;
    public unit: any = "unittest";

    get() {
        return this.unit;
    }

    getApIUrl() {
        {
            return Observable.of('your object');
        }
    }


    getPortalsApiBaseUrl() {
        return this.unit;
    }
}

class MockUserMgmtService extends UserManagementService {
    public res: any;
    public unit: any = "unittest";

    getUserDetails() {
        this.res.json = JSON.parse('{"UserId":"12345","username":"xxx","firstName":"FName1","lastName":"LName1","ClientId":"1","Administrator":true,"ErrorMessage":""}');
        return Observable.of(this.res);
    }

    addNewuser() {
        this.res.json = JSON.parse('{"UserId":"125","username":"xxx","firstName":"FName2","lastName":"LName2","ClientId":"1","Administrator":"false","ErrorMessage":""}');

        return Observable.of(this.res);
    }
}
    //addUser()
    //Userid: number;
    //username: string;
   // password: string;
   // firstName: string;
    //lastName: string;
    //ClientId: string;
   // Administrator: boolean;
    //Enabled: boolean;

describe('Users DOM Tests', () => {

   beforeEach(async(() => {
        TestBed.resetTestEnvironment();
        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        ElementRef;
        TestBed.configureTestingModule({
                imports: [BrowserModule, HttpModule],
                declarations: [User]
            })
            .overrideComponent(User,
            {
                remove: { providers: [ApiService, UserManagementService] },
                add: {
                    providers: [
                        { provide: ApiService, useClass: MockApiService },
                        { provide: UserManagementService, useClass: MockUserMgmtService } 
                    ]
                }
            })
            .compileComponents();
        }));
	
	
    //xit('Can view user details', function () {
    //    // An intentionally successful test. Revise with new test
     //   expect(2 + 2).toEqual(4);
    //});

   xit('Can add user',async(() => {

        // An intentionally successful test. Revise with new test
  	const fixture = TestBed.createComponent(AddNewUser);
        const app = fixture.componentInstance;
        const element = fixture.nativeElement;
   
         
        fixture.detectChanges();
        expect(element.textContent).toContain('UserId');
    }));


    });