﻿import "rxjs/add/operator/map";
import 'rxjs/add/operator/catch';
import { inject, async, TestBed } from "@angular/core/testing";
import { Component, Injector, Injectable } from "@angular/core";
import { BaseRequestOptions, Http, HttpModule, XHRBackend, Response, Headers, RequestOptions, ResponseOptions } from "@angular/http";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { UserManagementService } from "../../shared/usermanagement/userManagement.service";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { Observable } from 'rxjs/Rx';
import { ApiService } from "../../shared/api.service";
import { ApiConfiguration } from "../../shared/api.config";
import { Router, ActivatedRoute } from "@angular/router";

class apiServiceMock {
    get() {
        return Observable.of(
            [
                {
                    UserId: 1,
                    firstName: "test",
                    lastName: "<p><b>Lnametext</b></p>",
                    Enabled: true
                },
                {   UserId: 2,
                    firstName: "test2",
                    lastName: "<p><b>Lnametext2</b></p>",
                    Enabled: false
                }
            ]
        );
    }
}
// Be descriptive with titles here. The describe and it titles combined read like a sentence.
describe('users service test', function () {
    //it('has a failed dummy spec to test 1 equal to 4', function () {
    // An intentionally failing test. 
    //  expect(1).toEqual(4);
    // });

    it('has a success dummy spec to test 2 + 2', function () {
        // An intentionally successful test.
        expect(2 + 2).toEqual(4);
    });

    beforeEach(async(() => {
        TestBed.resetTestEnvironment();

        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [
                UserManagementService, ApiService, ApiConfiguration,
                { provide: XHRBackend, useClass: MockBackend },
                { provide: ApiService, useClass: apiServiceMock },
                { provide: ActivatedRoute, useValue: { 'params': Observable.from([{ 'id': 1 }]) } }
            ]
        }).compileComponents();;
    }));

    it('can instantiate User Management service when inject service',
        inject([UserManagementService], (service: UserManagementService) => {
            expect(service instanceof UserManagementService).toBe(true);
        }));

    it('can provide the mockBackend as XHRBackend',
        inject([XHRBackend], (backend: MockBackend) => {
            expect(backend).not.toBeNull('backend should be provided');
        }));


    let backend: MockBackend;
    let service: UserManagementService;
    let apisvc: ApiService;
    let apiconfig: ApiConfiguration;
    let activatedroute: ActivatedRoute;
    let response: Response;

    beforeEach(inject([Http, XHRBackend, ActivatedRoute, ApiConfiguration, ApiService], (http: Http, be: MockBackend, activatedroute: ActivatedRoute, apiconfig: ApiConfiguration, apiservice: ApiService) => {
        backend = be;
        service = new UserManagementService(http, apiservice);
        let options = new ResponseOptions({
            body: [
                {
                    UserId: 1,
                    firstName: "test",
                    lastName: "<p><b>Lnametext</b></p>"
                },
                {   UserId: 2,
                    firstName: "test2",
                    lastName: "<p><b>Lnametext2</b></p>"
                }
            ]
        });
        response = new Response(options);
    }));


    it('should get user details from User management service with id', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.getUserDetails('', 1).subscribe((blogs) => {
            expect(blogs[0].firstName).toBe("test");
            expect(blogs[0].lastName).toBe("<p><b>Lnametext</b></p>");
        });
    })));

    it('should get all users from User management service', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.getAllUsers('').subscribe((blogs) => {
            expect(blogs[1].firstName).toBe("test2");
            expect(blogs[1].lastName).toBe("<p><b>Lnametext2</b></p>");
        });
    })));

    it('should get user profile', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.getUserProfile('').subscribe((blogs) => {
            expect(blogs[0].firstName).toBe("test");
            expect(blogs[0].lastName).toBe("<p><b>Lnametext</b></p>");
        });
    })));
});