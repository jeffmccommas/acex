﻿import "rxjs/add/operator/map";
import 'rxjs/add/operator/catch';
import { inject, async, TestBed } from "@angular/core/testing";
import { Component, Injector, Injectable } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Http, Headers, URLSearchParams, HttpModule, XHRBackend, Response, RequestOptions, ResponseOptions } from "@angular/http";
import { MockBackend, MockConnection} from "@angular/http/testing";
import { AccountsService } from "../../benchmark/services/accounts.service";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { Observable } from 'rxjs/Rx';
import { Subscription } from "rxjs";
import { ApiService } from "../../shared/api.service";
import { OnInit, OnDestroy } from "@angular/core";
import { ApiConfiguration } from "../../shared/api.config";

class apiServiceMock {
    get() {
        return Observable.of(
            [
                {
                    'accountId': 'test',
                    'address': '<p><b>Tests Ave</b></p>'
                },
                {
                    'accountId': 'test',
                    'address': '<p><b>Tests Ave</b></p>'
                }
            ]
        );
    }

    post() {
        return Observable.of(
            [
                {
                    'url': 'fakeurl?accountids=1,2,3,4,5'
                }
            ]
        );
    }
}

// Be descriptive with titles here. The describe and it titles combined read like a sentence.
describe('account service tests', function () {

    beforeEach(async(() => {
        TestBed.resetTestEnvironment();
        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [
                AccountsService, ApiService, ApiConfiguration,
                { provide: XHRBackend, useClass: MockBackend },
                { provide: ApiService, useClass: apiServiceMock  },
                { provide: ActivatedRoute, useValue: { 'params': Observable.from([{ 'id': 1 }]) } }
            ]
        }).compileComponents();;
    }));

    it('can instantiate account service when inject service',
        inject([AccountsService], (service: AccountsService) => {
            expect(service instanceof AccountsService).toBe(true);
        }));

    it('can provide the mockBackend as XHRBackend',
        inject([XHRBackend], (backend: MockBackend) => {
            expect(backend).not.toBeNull('backend should be provided');
        }));


    let backend: MockBackend;
    let service: AccountsService; 
    let apisvc: ApiService;
    let apiconfig: ApiConfiguration;
    let activatedroute: ActivatedRoute;
    let response: Response; 
    beforeEach(inject([Http, XHRBackend, ActivatedRoute, ApiConfiguration, ApiService], (http: Http, be: MockBackend, activatedroute: ActivatedRoute, apiconfig: ApiConfiguration, apiservice: ApiService) => {
        backend = be;
        service = new AccountsService(http, apiservice);
         let options = new ResponseOptions({
            body: [
                {
                    accountId: "test",
                    address: "<p><b>Tests Ave</b></p>"
                }
            ]
        });
        response = new Response(options);
    }));

    it('should get pending accounts from accounts service with page 1', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.getPendingAccounts('','',1).subscribe((blogs) => {
            expect(blogs[0].accountId).toBe("test");
            expect(blogs[0].address).toBe("<p><b>Tests Ave</b></p>");
        });
    })));

    it('should get pending accounts from accounts service without optional page number', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.getPendingAccounts('', '').subscribe((blogs) => {
            expect(blogs[0].accountId).toBe("test");
            expect(blogs[0].address).toBe("<p><b>Tests Ave</b></p>");
        });
    })));

    it('should accept pending accounts', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));
        
        const accountIDs = ["1,2,3,4,5"];
        service.acceptPendingaccounts(accountIDs, '').subscribe((blogs) => {
            expect(blogs[0].url).toBe("fakeurl?accountids=" + accountIDs);
        });
    })));

    it('should reject pending accounts', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));
        const accountIDs = ["1,2,3,4,5"];
        service.rejectPendingaccounts([], '').subscribe((blogs) => {
            expect(blogs[0].url).toBe("fakeurl?accountids=" + accountIDs);
        });
    })));

    it('should get accepted accounts', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.getAcceptedAccounts('').subscribe((blogs) => {
            expect(blogs[0].accountId).toBe("test");
            expect(blogs[0].address).toBe("<p><b>Tests Ave</b></p>");
        });
    })));

});