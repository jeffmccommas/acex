﻿import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { Observable } from 'rxjs/Rx';
import { inject, async, TestBed, ComponentFixture } from "@angular/core/testing";
import { Component, ViewChild, Injector, ElementRef, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
//kendo
import {
    GridDataResult
} from "@progress/kendo-angular-grid";
import { AccountsService } from "../../benchmark/services/accounts.service";
import { ApiService } from "../../shared/api.service"; 
import { ModalDirective, Ng2BootstrapModule, PagerComponent, ModalModule} from 'ngx-bootstrap';
import { ContentService } from "../../shared/content/content.service";
import { BrowserModule, By } from "@angular/platform-browser";
import { BaseRequestOptions, Http, HttpModule, XHRBackend, Response, ResponseOptions } from "@angular/http";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { BenchmarkPortalComponent } from "../../benchmark/home.component";
import { NgxPaginationModule, PaginationInstance } from 'ngx-pagination'; 
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
import { NO_ERRORS_SCHEMA } from '@angular/core'
//mock contact and api services
class MockApiService extends ApiService {
    public res: any;
    public unit: any = "unittest";

    get() {
        return this.unit;
    }

    getApIUrl() {
        {
            return Observable.of('your object');
    }
  }


getPortalsApiBaseUrl() {
        return this.unit;
    }
}

class MockAccountsService extends AccountsService {
    public res: any;
     public unit: any = "unittest";

    getPendingAccounts() {
        //  this.res.json = JSON.parse('{"accountId":"12345","name":"xxx","address":"777 High St, Arlington, VA, 22201","customField1":"x","customField2":"x2","Timestamp":"12110120202","ErrorMessage":""}');
          return Observable.of(this.res);
    }

}

class MockContentService extends ContentService {
    public res: any;
    public unit: any = "unittest";

    getContactsContent() {
        return Observable.of('your object');
    }

}
//Note: still need to add kendo ui as declaration , custom element and mock it with /hook up to json result set mocked below
describe('Accounts DOM Tests', () => {
    beforeEach(async(() => {
        TestBed.resetTestEnvironment();
        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        ElementRef;
        TestBed.configureTestingModule({
            imports: [BrowserModule, HttpModule, NgxPaginationModule, Ng2BootstrapModule, ModalModule.forRoot()],
            declarations: [BenchmarkPortalComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        })
            .overrideComponent(BenchmarkPortalComponent,
            {
                remove: { providers: [AccountsService, ApiService, ContentService] },
                add: {
                    providers: [
                        { provide: ApiService, useClass: MockApiService },
                        { provide: AccountsService, useClass: MockAccountsService },
                        { provide: ContentService, useClass: MockContentService },
                    ]
                }
            }).compileComponents();
    }));

    xit('should instantiate BenchmarkPortal component',
        () => {
            let fixture = TestBed.createComponent(BenchmarkPortalComponent);
            TestBed.compileComponents();
            fixture.componentInstance.config.currentPage = 1;
            const app = fixture.componentInstance;
              // create the ComponentFixture, using the Grid component's data input to provide the respective binding, e.g.:
            //fixture.componentInstance.dataItems = [{ id: 1, foo: 'bar' }];
        
            //... assuming the Grid is bound to dataItems, e.g.:
            // <kendo-grid[data]="dataItems" ...


            //error json is undefined when passed like below
            var json = JSON.parse('{"accountId":"12345","name":"xxx","address":"777 High St, Arlington, VA, 22201","customField1":"x","customField2":"x2","Timestamp":"12110120202","ErrorMessage":""}');
            app.result = json;

            fixture.detectChanges();
            expect(fixture.componentInstance instanceof BenchmarkPortalComponent).toBe(true, 'should create BenchmarkPortalComponent');
    });

});