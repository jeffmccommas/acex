﻿import { AccountsService } from "./services/accounts.service";
import { Component, Input, Inject, forwardRef, OnChanges, SimpleChange, EventEmitter, Output } from '@angular/core';
import { ContentService } from "../shared/content/content.service";
import { ApiService } from "../shared/api.service"
//import Metermappingcomponent = require("./metermapping.component");
//import MeterMappingComponent = Metermappingcomponent.MeterMappingComponent;

@Component({
    selector: "mappedcustomer",
    moduleId: module.id,
    templateUrl: "mappedcustomer.component.html"
})

export class MappedCustomer implements OnChanges {
    private errorObject : any 
    @Input() public accountId: string;
    @Input() public premiseId: string;
    @Input() changed: boolean;
    @Output() onNoMapFound = new EventEmitter<any>();
    @Output() onMapLoad = new EventEmitter<boolean>();
    private tabkey = 'tab.espmmeters';
    private WidgetType = 'meters';
    private loading: Boolean = true;
    private noMatch: Boolean = false;
    private apiBaseUrl: string;
    private error: Boolean = false;
    private result: any = {};
    private showMeterMap: Boolean = false;
    public content : any;
    constructor(private accountService: AccountsService, private contentService: ContentService, private apiService: ApiService//,
        //@Inject(forwardRef(() => MeterMappingComponent)) private meterMappingComponent: MeterMappingComponent
        ) 
    {

    }

    ngOnChanges(changes: { [propName: string]: SimpleChange }) {
        console.debug(changes);
        console.debug(changes['accountId']);
        console.debug(changes['premiseId']);
        if (changes['accountId']) {
            this.accountId = changes['accountId'].currentValue;
            console.debug('Change account id');
        }
        if (changes['premiseId']) {
            this.premiseId = changes['premiseId'].currentValue;
            console.debug('Change premise id');
        }
        this.getMappedCustomer();
    }

    ngOnInit() {
       

    }

    getMappedCustomer() {
        this.noMatch = false;
        this.loading = true;
        this.error = false;
        this.onMapLoad.emit(true);
        this.apiService.getApIUrl().subscribe(
            res => {
                this.apiBaseUrl = res;
                this.contentService.getMetersContent(this.tabkey, this.WidgetType, this.apiBaseUrl)
                    .subscribe(res => {
                        this.content = res;
                    });
                if (!this.accountId) {
                    this.noMatch = true;
                    this.loading = false;
                } else {
                    this.accountService.getMappedAccountDetails(this.apiBaseUrl, this.accountId, this.premiseId)
                        .subscribe(res => {
                            this.result = res.json();
                            this.loading = false;
                            this.onMapLoad.emit(false);
                            if (this.result.error) {
                                this.noMatch = true;
                                this.onNoMapFound.emit({
                                    "accountId": this.accountId,
                                    "premiseId": this.premiseId,
                                    "valid": false
                                });
                            } else {
                            this.onNoMapFound.emit({ "accountId": this.accountId, "premiseId": this.premiseId, "valid": true });
                            }
                        });
                }
            },
            err => {
                console.error(err);
                this.loading = false;
                this.error = true;
            }
        );
    }




}