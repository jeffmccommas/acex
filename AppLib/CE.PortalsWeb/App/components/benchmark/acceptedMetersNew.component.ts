﻿import { Component, ViewChild, Input } from '@angular/core';
import { ApiService } from "../shared/api.service"
import { ContentService } from "../shared/content/content.service";
import { PropertyService } from "./services/property.service";
import { AcceptedMetersList } from "./acceptedmeterslist.component";
import { MetricsComponent } from "./metrics.component";

@Component({
    providers: [ApiService],
    moduleId: module.id,
    selector: 'acceptedmetersnew',
    templateUrl: 'acceptedmetersnew.component.html'
})

export class AcceptedMetersNew {

    private apiBaseUrl: string;
    @Input() public accountId: string;
    @Input() public propertyId: string;
    private result: any;
    private tabkey = 'tab.espmmeters';
    private WidgetType = 'meters';
    public errorMessage: string;
    public isPropertySelected: boolean;
    public loading: boolean = false;
    public showMeters: boolean = true;
    public showMetrics: boolean = false;
    public noAccess : Boolean = false;

    private content : any;
    @ViewChild('acceptedmeterslist') acceptedmeterslist: AcceptedMetersList;

    constructor(private apiService: ApiService, private contentService: ContentService, private propertyService: PropertyService) {
        this.accountId = null;
        this.propertyId = null;
    }

    public ngOnInit() {
    }
    public getPropertyDetails() {
        this.noAccess = false;
        if (!this.propertyId) {
            this.errorMessage = "Please select an account and property to see the accepted meters. ";
            this.isPropertySelected = false;
        } else {
            this.apiService.postEvents("User clicked on Connected Meters", this.accountId, this.propertyId);
            this.isPropertySelected = true;
            this.errorMessage = "";
            this.loading = true;
            this.apiService.getApIUrl().subscribe(
                res => {
                    this.apiBaseUrl = res;
                    this.propertyService.getPropertyDetails(this.apiBaseUrl, this.propertyId).subscribe(
                        res => {
                            this.result = (res.json().property);
                            this.loading = false;
                        },
                        error => {
                            console.log('Error ', error);
                            if (error.status === 403) {
                                this.noAccess = true;

                            }
                            this.loading = false;
                        }
                    );

                    this.contentService.getMetersContent(this.tabkey, this.WidgetType, this.apiBaseUrl)
                        .subscribe(res => {
                            this.content = res;
                        });
                });
        }
    }

    public toggle(value: string) {
        if (value === 'meters') {
            this.showMeters= true;
            this.showMetrics = false;
        } else {
            this.showMetrics = true;
            this.showMeters = false;
        }
    }
}