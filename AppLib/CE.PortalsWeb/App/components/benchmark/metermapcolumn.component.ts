﻿import { Component, Input, ViewChild } from "@angular/core";
import { ApiService } from "../shared/api.service";
import { MetersService } from "./services/meters.service";
import { ContentService } from "../shared/content/content.service";


@Component({
    selector: "metermapcolumn",
    moduleId: module.id,
    templateUrl: 'metermapcolumn.component.html'
})



export class MeterMapColumnComponent {

    @Input() public meterId: string;
    @Input() public accountId: string;
    @Input() public premiseId: string;
    @Input() public meterType: string;
    @Input() public isMapped: Boolean;
    result: any;
    loading: boolean = false;
    invaliddata: boolean = false;
    errorMessage: string = "";
    apiBaseUrl: string;
    private content: any;
    private tabkey = 'tab.espmmeters';
    private WidgetType = 'meters';
    constructor(private apiService: ApiService, private metersService: MetersService, private contentService: ContentService) {

    }
   

    ngOnInit() {
        
        this.loading = true;
        if (!this.accountId) {
            this.loading = false;
            this.invaliddata = true;
        } else {
            this.apiService.getApIUrl().subscribe(
                res => {
                    this.apiBaseUrl = res;
                    this.contentService.getMetersContent(this.tabkey, this.WidgetType, this.apiBaseUrl)
                        .subscribe(res => {
                            this.content = res;
                            console.debug(res);
                        });
                    this.loading = false;


                    

                });
        }

    }

   
    
}