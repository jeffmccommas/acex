﻿import { Component, Input, ViewChild} from "@angular/core";
import { PropertyService } from "./services/property.service";
import { Account } from "./services/account";
import { ApiService } from "../shared/api.service";
import { ContentService } from "../shared/content/content.service";
import { Metrics } from "./services/metrics";
import { MetricsNotFoundComponent } from "./metricsnotfound.component";
import { ModalDirective } from 'ngx-bootstrap';
import { ModalComponent } from "../shared/common/modal.component";


@Component({
    selector: "metrics",
    moduleId: module.id,
    templateUrl: 'metrics.component.html'
})
export class MetricsComponent {

    private apiBaseUrl: string;
    @Input()
    public propertyId: string;
    private result: any;
    private errors: any;
    private content: any;
    private propertymetrics: Metrics[];
    private loading: boolean = false;
    private baselinedate: any;
    private currentdate: any;
    private current: any;
    public isMetricsAvailable: Boolean;
    public hideMetricsAvailableMain: Boolean;
    @ViewChild('staticModal')
    public staticModal: ModalDirective;
    @ViewChild('modal1')
    public modal1: ModalComponent;
    @ViewChild('modal')
    modal: any;  
    @ViewChild('metricsnotfound')
    public metricsnotfound: MetricsNotFoundComponent;
    @ViewChild('metricsnotfoundmain')
    public metricsnotfoundmain: MetricsNotFoundComponent;
    constructor(private apiService: ApiService,
        private propertyService: PropertyService,
        private contentService: ContentService) {

    }

  

    ngOnInit() {
        var filterFloat = value => {
            if (/^(\-|\+)?([0-9]+(\.[0-9]+)?|Infinity)$/
                .test(value))
                return Number(value);
            return NaN;
        }
        this.hideMetricsAvailableMain = true;
        this.propertymetrics = [];
        this.loading = true;
        this.current = new Date();
        this.isMetricsAvailable = false;
        this.apiService.getApIUrl().subscribe(
            res => {
                this.apiBaseUrl = res;
                this.contentService.getConfigurationContent(this.apiBaseUrl)
                    .subscribe(res => {
                        this.content = res;
                        for (var config in this.content) {
                            if (this.content.hasOwnProperty(config)) {
                                let context = this.content[config].split(":");
                                if(context[0] !== "energyBaselineDate" || context[0] !== "energyCurrentDate")
                                    this.propertymetrics.push(new Metrics(context[0].trim(), context[1].trim(), "Metrics Not Available", "Metrics Not Available", false, false));
                            }
                        }
                        this.propertyService.getPropertyMetrics(this.apiBaseUrl, this.propertyId, this.current.getMonth().toString(),
                            this.current.getFullYear().toString(),"energyBaselineDate,energyCurrentDate")
                            .subscribe(res => {
                                console.debug(res.json().propertyMetrics.metric);
                                res.json().propertyMetrics.metric.forEach(elm => {
                                    if (typeof(elm.value) !== 'object') this.isMetricsAvailable = true;
                                });
                                if (this.isMetricsAvailable) {
                                    this.hideMetricsAvailableMain = true;
                                    this.baselinedate = new Date(res.json().propertyMetrics.metric[0].value);
                                    this.currentdate = new Date(res.json().propertyMetrics.metric[1].value);
                                    let metrics = this.propertymetrics.map(x => x.value).join(",");
                                    this.propertyService.getPropertyMetrics(this.apiBaseUrl,
                                            this.propertyId,
                                            (this.currentdate.getMonth() + 1 ).toString(),
                                            this.currentdate.getFullYear().toString(),
                                            metrics)
                                        .subscribe(res => {
                                            res.json().propertyMetrics.metric.forEach(
                                                elem => {
                                                    if (typeof(elem.value) !== 'object') {
                                                        let element =
                                                            this.propertymetrics.find(x => x.value === elem.name);
                                                        if (element) {
                                                            this.propertymetrics.find(x => x.value === elem.name)
                                                                .currentValue = elem.value;
                                                            this.propertymetrics.find(x => x.value === elem.name)
                                                                .currentValueAvailable = true;
                                                        }
                                                    }
                                                });
                                            this.propertyService.getPropertyMetrics(this.apiBaseUrl,
                                                    this.propertyId,
                                                    (this.baselinedate.getMonth() + 1).toString(),
                                                    this.baselinedate.getFullYear().toString(),
                                                    metrics)
                                                .subscribe(res => {
                                                    res.json().propertyMetrics.metric.forEach(
                                                        elem => {

                                                            if (typeof (elem.value) !== 'object') {
                                                                let element =
                                                                    this.propertymetrics.find(
                                                                        x => x.value === elem.name);
                                                                if (element) {
                                                                  this.propertymetrics
                                                                            .find(x => x.value === elem.name)
                                                                            .baseLineValue = elem.value;
                                                                  this.propertymetrics
                                                                        .find(x => x.value === elem.name)
                                                                        .baseLineValueAvailable = true;
                                                                }

                                                            }
                                                            this.propertymetrics.forEach(elm => {
                                                                elm.changeAvailable = false;
                                                                if (!isNaN(filterFloat(elm.baseLineValue)) && !isNaN(filterFloat(elm.currentValue)))
                                                                    elm.changeAvailable = true;
                                                                if (!isNaN(filterFloat(elm.baseLineValue)) && elm.value !== "energyStarCertificationYears")
                                                                    elm.baseLineIsNUmber = true;
                                                                if (!isNaN(filterFloat(elm.currentValue)) && elm.value !== "energyStarCertificationYears")
                                                                    elm.currentIsNumber = true;
                                                                if (!isNaN(parseFloat(elm.baseLineValue)) &&
                                                                    !isNaN(parseFloat(elm.currentValue)))
                                                                    elm.change = (parseFloat(elm.currentValue) -
                                                                        parseFloat(elm.baseLineValue)).toFixed(2);
                                                                if (!isNaN(parseFloat(elm.baseLineValue)) &&
                                                                    parseFloat(elm.baseLineValue) !== 0)
                                                                    if (!isNaN(parseFloat(elm.baseLineValue)) &&
                                                                        !isNaN(parseFloat(elm.currentValue)) &&
                                                                        parseFloat(elm.baseLineValue) ===
                                                                        parseFloat(elm.currentValue)) {
                                                                        elm.percentchange = "N/A";
                                                                        elm.changeAvailable = false;
                                                                    }
                                                                    else
                                                                    {
                                                                            elm.percentchange = (parseFloat(elm.change) /
                                                                                parseFloat(elm.baseLineValue) *
                                                                                100).toFixed(2);
                                                                    }

                                                            });
                                                            this.loading = false;

                                                        });
                                                });
                                        });


                                } else {
                                    this.hideMetricsAvailableMain = false;
                                    this.loading = false;
                                    this.metricsnotfoundmain.propertyId = this.propertyId;
                                    this.metricsnotfoundmain.year = this.current.getFullYear();
                                    this.metricsnotfoundmain.month = this.current.getMonth() + 1;
                                    this.metricsnotfoundmain.showReason();

                                }
                            
                            });
                        
                        
                       
                    });

            });
    }

    public showModal(metricsdate) {
        this.metricsnotfound.propertyId = this.propertyId;
        this.metricsnotfound.year = metricsdate.getFullYear();
        this.metricsnotfound.month = metricsdate.getMonth() + 1;
        this.metricsnotfound.showReason();
        this.modal1.show();
    }

 
}