﻿import { Component, Input } from "@angular/core";
import { PropertyService } from "./services/property.service";
import { ApiService } from "../shared/api.service";
import { ContentService } from "../shared/content/content.service";


@Component({
    selector: "metricsnotfound",
    moduleId: module.id,
    templateUrl: 'metricsnotfound.component.html'
})
export class MetricsNotFoundComponent {
    private apiBaseUrl: string;
    @Input() public propertyId: string;
    @Input() public year: string;
    @Input() public month: string;
    private errors: any;
    private content: any;
    private result: any = [];
    private loading : boolean = false;
    private noresult: boolean = false;
    private metricsReason :Object[];
    constructor(private apiService: ApiService,
        private propertyService: PropertyService,
        private contentService: ContentService) {

    }

    ngOnInit() { }

    public showReason() {
        this.loading = true;
        this.noresult = true;
        this.metricsReason = [];
        this.apiService.getApIUrl().subscribe(
            res => {
                this.apiBaseUrl = res;
                this.propertyService.getNoReasonforMetrics(this.apiBaseUrl, this.propertyId, this.month, this.year)
                    .subscribe(
                    res => {
                        console.debug(res.json());
                     if (res.json().alerts.alert) {
                         console.debug(res.json().alerts.alert);
                         let metrics = res.json().alerts.alert;
                                
                         if (!Array.isArray(metrics)) {
                                    this.metricsReason.push({ name: metrics.name, description: metrics.description });
                                } else {
                                    metrics.forEach(message => {
                                        this.metricsReason.push({ name: message.name, description: message.description });
                                    });
                                }
                        }
                        console.debug(this.metricsReason);
                        if (this.metricsReason.length > 0) {
                                this.noresult = false;
                            }
                            this.loading = false;
                        });

            });
    }
}
