﻿import { Component, Input, ViewChild, NgModule } from '@angular/core';
import { ApiService } from "../shared/api.service"
import { MetersService } from "./services/meters.service";
import { ContentService } from "../shared/content/content.service";
import { ConsumptionComponent } from "./consumptiondata.component";
import { MeterMappingComponent } from "./metermapping.component";
import { BillingComponent } from "./billdata.component";
import { ToggleDivDirective } from "../shared/directives/togglediv.directive";

@Component({
    moduleId: module.id,
    selector: 'meterdetails',
    templateUrl: 'meterdetails.component.html'
})
    @NgModule({
        declarations : [ToggleDivDirective]
    })
export class MeterDetails {
    @Input() public meterId: string = null;
    @Input() public result: any;
    public units: string;
    public meterIdonLoad: string;
    public apiBaseUrl: string;
    public loading: boolean = false;
    public isOpen: boolean = false;
    public showMapping: boolean = false;
    public showConsumption: boolean = true;
    private content: any;
    private tabkey = 'tab.espmmeters';
    private WidgetType = 'meters';
    @ViewChild('consumptionComponent') consumptionComponent: ConsumptionComponent;
    @ViewChild('billingComponent') billingComponent: BillingComponent;
    @ViewChild('metermapping') metermapping: MeterMappingComponent;
    
    constructor(private apiService: ApiService, private contentService: ContentService, private meterService: MetersService) {
        this.loading = true;
        this.apiService.getApIUrl().subscribe(
            res => {
                this.apiBaseUrl = res;
                this.contentService.getMetersContent(this.tabkey, this.WidgetType, this.apiBaseUrl)
                    .subscribe(res => {
                        this.content = res;
                        console.debug(res);
                    });
            });
    }

    public ngOnInit() {
        this.units = this.result.unitOfMeasure;
        this.meterIdonLoad = this.result.id;
        this.loading = false;

    }

    public loadMeterData(meterId: string) {
        if (!this.isOpen) {
            this.apiService.postEvents("User clicked to see consumption and mapping details for meter id: " + meterId, "", "");
            this.isOpen = true;
            this.showConsumption = true;
            this.consumptionComponent.meterId = meterId;
            this.billingComponent.meterId = meterId;
            this.metermapping.meterId = meterId;
            this.metermapping.getMappedMeters();
            this.consumptionComponent.getConsumptionData();
            this.billingComponent.getBilldata();
        } else {
            this.apiService.postEvents("User closed the consumption and mapping details for meter id: " + meterId, "","");
            this.isOpen = false;
        }
    }

    public toggle(value: string) {
        if (value === 'consumption') {
            this.apiService.postEvents("Visited Consumption tab for meter id: " + this.consumptionComponent.meterId, "", "");
            this.showConsumption = true;
            this.showMapping = false;
        } else {
            this.apiService.postEvents("Visited on Mapping tab for meter id: " + this.consumptionComponent.meterId, "", "");
            this.showMapping = true;
            this.showConsumption = false;
        }
    }


}
