﻿import { Component, Input } from "@angular/core";
import { PropertyService } from "./services/property.service";
import { Account } from "./services/account";
import { ApiService } from "../shared/api.service";
import { ContentService } from "../shared/content/content.service";



@Component({
    selector: "propertydetails",
    moduleId: module.id,
    templateUrl: 'propertydetails.component.html'
})


export class PropertyDetailsComponent {
    private apiBaseUrl: string;
    @Input() public propertyId: string;
    private result: any;
    private errors: any;
    private content: any;
    private tabkey = 'tab.espmmeters';
    private WidgetType = 'meters';
    private noAccess : Boolean = false;

    constructor(private apiService: ApiService, private propertyService: PropertyService, private contentService: ContentService) {

    }

    ngOnInit() {
        this.noAccess = false;
        this.result = null;
        this.apiService.getApIUrl().subscribe(
            res => {
                this.apiBaseUrl = res;
                this.propertyService.getPropertyDetails(this.apiBaseUrl, this.propertyId)
                    .subscribe(res => {
                            
                        this.result = (res.json().property);
                    },
                    error => {
                        if (error.status === 403) {
                            this.noAccess = true;
                        } else {
                            this.errors = error;
                        }
                    },
                    () => {
                        // 'onCompleted' callback.
                        // No errors, route to new page here
                    }
                    );

                this.contentService.getMetersContent(this.tabkey, this.WidgetType, this.apiBaseUrl)
                    .subscribe(res => {
                        this.content = res;
                        console.debug(res);
                    });
            });
    }
}