﻿import { Component, Input } from "@angular/core";
import { ApiService } from "../shared/api.service";
import { MetersService } from "./services/meters.service";
import { ContentService } from "../shared/content/content.service";


@Component({
    selector: "consumptioncomponent",
    moduleId: module.id,
    templateUrl: 'consumptiondata.component.html'
})


export class ConsumptionComponent {

    @Input() public meterId: string;
    @Input() public unitOfMeasure: string;
    result: any;
    errorMessage: string;
    loading: boolean;
    private tabkey = 'tab.espmmeters';
    private WidgetType = 'meters';
    private content: any;
    private apiBaseURL : string;
    constructor(private apiService: ApiService, private metersService: MetersService, private contentService: ContentService) {

    }

    getConsumptionData() {
        this.loading = true;
        this.apiService.getApIUrl().subscribe(
            res => {
                this.apiBaseURL = res;
                this.metersService.getConsumptionData(res, this.meterId).subscribe(res => {
                    this.result = res.json().meterData.meterConsumption;
                    this.loading = false;
                });
                this.contentService.getMetersContent(this.tabkey, this.WidgetType, this.apiBaseURL)
                    .subscribe(res => {
                        this.content = res;
                    });
            },
            error => {
                this.errorMessage = error;
                this.loading = false;
            });
    }

    ngOnInit() {
        //this.getConsumptionData();
    }
}