﻿import { Component, Input, NgModule } from "@angular/core";
import { ApiService } from "../shared/api.service";
import { MetersService } from "./services/meters.service";
import Pipe = require("../helpers/pipe");
import ObjToArr = Pipe.ObjToArr;
import { ContentService } from "../shared/content/content.service";




@Component({
    selector: "billscomponent",
    moduleId: module.id,
    templateUrl: 'billdata.component.html'
})


export class BillingComponent {

    @Input() public meterId: string; 
    @Input() public unitOfMeasure: string;
    result: any;
    errorMessage: string;
    loadingBillData: boolean = true;
    noData: boolean = false;
    private tabkey = 'tab.espmmeters';
    private WidgetType = 'meters';
    private content: any;
    private apiBaseURL ; string;
    constructor(private apiService: ApiService, private metersService: MetersService, private contentService: ContentService) {

    }
    
    getBilldata() {
        this.loadingBillData = true;
        this.noData = false;
        this.apiService.getApIUrl().subscribe(
            res => {
                this.apiBaseURL = res;
                this.metersService.getBillingData(res, this.meterId).subscribe(res => {
                    this.loadingBillData = false;
                    this.result = res.json();

                });

                this.contentService.getMetersContent(this.tabkey, this.WidgetType, this.apiBaseURL)
                    .subscribe(res => {
                        this.content = res;
                        console.debug(res);
                    });

            },
        error => {
            this.errorMessage = error;
            this.loadingBillData = false;
        });
    }
   
    ngOnInit() {
        //this.getBilldata();
    }
}