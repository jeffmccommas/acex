﻿export class BillData {
    startdate: string;
    enddate: string;
    amount: string;
    cost: string;

    constructor(startdate, enddate, amount, cost) {
        this.startdate = startdate;
        this.enddate = enddate;
        this.amount = amount;
        this.cost = cost;

    }



}
