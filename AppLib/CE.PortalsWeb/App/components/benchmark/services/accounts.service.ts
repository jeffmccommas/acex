﻿import { Injectable, Inject } from "@angular/core";
import { Http, Headers, URLSearchParams, BaseRequestOptions } from "@angular/http";

import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/of'
import { Subscription } from "rxjs";
import { Router, ActivatedRoute } from "@angular/router";
import { OnInit, OnDestroy } from "@angular/core";

import { ApiService } from "../../shared/api.service";
import { GridDataResult } from '@progress/kendo-angular-grid';

@Injectable()
export class AccountsService {

    constructor(private http: Http, private apiService: ApiService) {

    }

    getPendingAccounts(params: string, apiBaseUrl:string, pageNumber?: number) {

        console.debug("getPendingAccounts");

        if (pageNumber) {
            return this.apiService.get(apiBaseUrl + "/api/EnergyStar/PendingAccounts?pageNumber=" + pageNumber);
        } else {
            return this.apiService.get(apiBaseUrl + "/api/EnergyStar/PendingAccounts");
        }
        //With a regular observable you only get the value when it changes, so if you want to console.log out the value you will need to console.log it in the subscription
        //console.log(res.Response);
    }

    getAcceptedAccounts(apiBaseUrl: string) {

        console.debug("getAcceptedAccounts");
        return this.apiService.get(apiBaseUrl + "/api/EnergyStar/AcceptedAccounts");
       
    }

    acceptPendingaccounts(accountIds: string[], apiBaseUrl: string) {
        return this.apiService.post(apiBaseUrl + "/api/EnergyStar/AcceptConnections?accountIds=", accountIds);
    }

    rejectPendingaccounts(accountIds: string[], apiBaseUrl: string) {
        console.debug('rejectPendingaccounts');

        return this.apiService.post(apiBaseUrl + "/api/EnergyStar/RejectConnections?accountIds=",accountIds);
    }

    getAccountDetails(apiBaseUrl: string, accountId: string)
    {
        return this.apiService.get(apiBaseUrl + "/api/EnergyStar/AcceptedAccountDetails?accountId=" + accountId);
    }

    getAcceptedCustomerList(apiBaseUrl: string) {
        return this.apiService.get(apiBaseUrl + "/api/EnergyStar/AcceptCustomersList?skip=0&take=1000");
    }

    getCustomFields(apiBaseUrl: string, accountId: string) {
        return this.apiService.get(apiBaseUrl + "/api/EnergyStar/AcceptedAccountDetails?accountId=" + accountId);
    }

    getMappedAccountDetails(apiBaseUrl: string, accountId: string, premiseId: string) {
        return this.apiService.get(apiBaseUrl + "/api/PortalsApiMap/MappedCustomerInfo?accountId=" + accountId + "&premiseId=" + premiseId);
    }

   
}