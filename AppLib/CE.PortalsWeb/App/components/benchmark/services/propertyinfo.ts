﻿
export class PropertyInfo {
    propertyId: string;
    name: string;
    address: string;
    city: string;
    postalCode: string;
    state: string;
    country: string;
    numberOfBuildings: string;
    constructionStatus: string;
    primaryFunction: string;
    yearBuilt: string;
    grossFloorArea: string;
    occupancyPercentage: string;
    fedProperty: string;

    constructor(propertyId, name, address, city, postalCode, state, country, numberOfBuildings, constructionstatus, primaryfunction, yearbuilt, grossfloorarea,occupancypercentage, fedproperty) {
        this.propertyId = propertyId;
        this.name = name;
        this.address = address;
        this.city = city;
        this.postalCode = postalCode;
        this.state = state;
        this.country = country;
        this.numberOfBuildings = numberOfBuildings;
        this.constructionStatus = constructionstatus;
        this.primaryFunction = primaryfunction;
        this.yearBuilt = yearbuilt;
        this.grossFloorArea = grossfloorarea;
        this.occupancyPercentage = occupancypercentage;
        this.fedProperty = fedproperty;
    }



}
