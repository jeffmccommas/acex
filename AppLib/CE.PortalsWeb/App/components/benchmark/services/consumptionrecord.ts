﻿export class ConsumptionRecord {
    date1: string;
    date2: string;
    dataval1: string;
    dataval2: string;

    constructor(date1, date2, dataval1, dataval2) {
        this.date1 = date1;
        this.date2 = date2;
        this.dataval1 = dataval1;
        this.dataval2 = dataval2;

    }



}
