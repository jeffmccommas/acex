﻿export class EspmMap {
    mapId: number;
    accountId: string;
    meterId: string;
    premiseId: string;
    updatedBy: string;
    updatedDate: string;
    meterType : string;
    espmMapDetails: ESPMMapDetails[];

    constructor(AccountId, premiseId, meterId) {
        this.accountId = AccountId;
        this.premiseId = premiseId;
        this.meterId = meterId;
        this.espmMapDetails = new Array<ESPMMapDetails>();
        this.espmMapDetails.push(new ESPMMapDetails("", "", ""));

    }


}

export class ESPMMapDetails {
    mapDetailId: number;
    mapId: number;
    mapAccountId: string;
    mapPremiseId: string;
    enabled: boolean;
    updatedBy: string;
    updateddate: string;
    uniqueId: string;
    mapAccountIdError: boolean = false;
    mapPremiseIdError : boolean = false;

    constructor(mapId, mapAccountId, mappremiseId) {
        this.mapId = mapId;
        this.mapAccountId = mapAccountId;
        this.mapPremiseId = mappremiseId === "null" ? "" : mappremiseId;
        this.uniqueId = Date.now().toString();
        this.mapAccountIdError = true;

    }


}