﻿import * as moment from 'moment';

export class Account {
    name: any;
    address: string;
    accountId: string;
    timeStamp: string;
    customField1: string;
    customField2: string;
    errorMessage: string;
    isvalid: boolean;
    IsAcceptedAccount: boolean;
    state: boolean;
    pagenumber : string;
    

    constructor(accountId, name, address, timeStamp, customField1, customField2, errorMessage, isValid = true, isAcceptedAccount, pagemnumber) {
        this.accountId = accountId;
        this.name = name;
        this.address = address;

        if (timeStamp)
        //this.timeStamp = new DatePipe('en-US').transform(new Date(Date.parse(timeStamp)), 'MM/dd/yyyy HH:mm:ss');
            this.timeStamp = moment(new Date(Date.parse(timeStamp))).format('MM/DD/YYYY HH:mm:ss');
        else
            this.timeStamp = "";
        this.customField1 = customField1;
        this.customField2 = customField2;
        this.errorMessage = errorMessage;
        this.isvalid = isValid;
        this.IsAcceptedAccount = isAcceptedAccount;
        this.state = false;
        this.pagenumber = pagemnumber;
    }
}
