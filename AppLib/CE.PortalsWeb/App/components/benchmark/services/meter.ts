﻿import { PropertyInfo } from "./Propertyinfo";
import * as moment from 'moment';

export class Meter {
    meterId: string;
    propertyId: string;
    accountId: string;
    customField1: string;
    customField2: string;
    propertyInfo: PropertyInfo;
    name: string;
    metered: string;
    firstBillDate: string;
    inUse: string;
    type: string;
    unitOfMeasure: string;
    timeStamp: string;
    errorMessage: string;
    mappodId: string;
    mapaccountId: string;
    state: boolean;
    isMapped: boolean;
    pageNumber : string;

    constructor(meterId, propertyId, accountId, customField1, customField2,
        propertyInfo, name, metered, firstBillDate, inUse, type, unitOfMeasure, timeStamp, errorMessage, mapaccountId, mappodId, pagenumber, isMapped = false) {
        this.meterId = meterId;
        this.propertyId = propertyId;
        this.accountId = accountId;
        this.customField1 = customField1;
        this.customField2 = customField2;
        this.propertyInfo = propertyInfo;
        this.name = name;
        this.metered = metered;
        this.firstBillDate = firstBillDate;
        this.inUse = inUse;
        this.type = type;
        this.unitOfMeasure = unitOfMeasure;
        if (timeStamp)
             this.timeStamp = moment(new Date(Date.parse(timeStamp))).format('MM/DD/YYYY HH:mm:ss');
        else
            this.timeStamp = "";
        this.errorMessage = errorMessage;
       this.mapaccountId = mapaccountId;
       this.mappodId = mappodId;
       this.state = false;
       this.isMapped = isMapped;
       this.pageNumber = pagenumber;

    }



}
