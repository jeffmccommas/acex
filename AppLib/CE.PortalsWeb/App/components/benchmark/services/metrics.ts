﻿export class Metrics {
    value : string;
    text: string;
    baseLineValue: string;
    baseLineValueAvailable: Boolean;
    currentValue: string;
    currentValueAvailable: Boolean;
    change: string;
    percentchange: string;
    changeAvailable: Boolean = false;
    baseLineIsNUmber: Boolean;
    currentIsNumber : Boolean; 


    constructor(value, text, baselinevalue, currentvalue, baselinevalueAvailable, currentValueAvailable
        ) {
        this.value = value;
        this.text = text;
        this.baseLineValue = baselinevalue;
        this.currentValue = currentvalue;
        this.baseLineValueAvailable = baselinevalueAvailable;
        this.currentValueAvailable = currentValueAvailable;
        this.changeAvailable = false;
        this.baseLineIsNUmber = false;
        this.currentIsNumber = false;
    }

    

}