﻿import { Injectable, Inject } from "@angular/core";
import { Http, Headers, URLSearchParams, BaseRequestOptions } from "@angular/http";
import 'rxjs/add/observable/of'
import { Subscription } from "rxjs";
import { Router, ActivatedRoute } from "@angular/router";
import { OnInit, OnDestroy } from "@angular/core";

import { ApiService } from "../../shared/api.service";


@Injectable()

export class PropertyService {

    constructor(private http: Http, private apiService: ApiService) {

    }

    getPropertyDetails(apiBaseUrl: string, propertyId: string) {
        return this.apiService.get(apiBaseUrl + "/api/EnergyStar/PropertyDetails?propertyId=" + propertyId);
    }


    getAcceptedPropertyList(apiBaseUrl: string, accountId: string) {
        if(accountId)
        return this.apiService.get(apiBaseUrl + "/api/EnergyStar/AcceptedPropertyList?skip=0&take=1000&accountId=" + accountId);
        

    }

    getPropertyBaseLineAndTarget(apiBaseUrl: string, propertyId: string) {
        return this.apiService.get(apiBaseUrl + "/api/EnergyStar/BaselineAndTarget?propertyId=" + propertyId);

    }

    getPropertyMetrics(apiBaseUrl: string, propertyId: string, month : string, year : string, metrics : string) {
        return this.apiService.get(apiBaseUrl + "/api/EnergyStar/GetPropertyMetrics?propertyId=" + propertyId +  "&month=" + month + "&year=" + year + "&metrics=" + metrics);

    }

    getNoReasonforMetrics(apiBaseUrl: string, propertyId: string, month: string, year: string) {
        return this.apiService.get(apiBaseUrl + "/api/EnergyStar/GetPropertyMetricsNoScore?propertyId=" + propertyId + "&month=" + month + "&year=" + year);

    }
}
