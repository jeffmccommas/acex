﻿import { Injectable, Inject } from "@angular/core";
import { Http, Headers, URLSearchParams, BaseRequestOptions } from "@angular/http";

import { Observable } from "rxjs/Observable";
import { Subscription } from "rxjs";
import { Router, ActivatedRoute } from "@angular/router";
import { OnInit, OnDestroy } from "@angular/core";

import { ApiService } from "../../shared/api.service";
import Metersmap = require("./metersmap");
import Meter = require("./meter");
import MeterMap = Metersmap.MeterMap;
import Meters = Meter.Meter;

@Injectable()
export class MetersService {

    constructor(private http: Http, private apiService: ApiService) {

    }

    getPendingMeters(params: string, apiBaseUrl: string, pageNumber?: number ) {

        console.debug("srv->getPendingMeters");
        console.debug(apiBaseUrl + "/api/EnergyStar/PendingMeters");
        if (pageNumber) {
            return this.apiService.get(apiBaseUrl + "/api/EnergyStar/PendingMeters?pageNumber=" + pageNumber);
        } else {
            return this.apiService.get(apiBaseUrl + "/api/EnergyStar/PendingMeters");
        }
        //With a regular observable you only get the value when it changes, so if you want to console.log out the value you will need to console.log it in the subscription
        //console.log(res.Response);
    }

    getAcceptedMeters(apiBaseUrl: string) {

        console.debug("getAcceptedMeters");
        return this.apiService.get(apiBaseUrl + "/api/EnergyStar/AcceptedMeters");

    }
    getMeterMaps(apiBaseUrl: string, accountId: string, premiseId: string, meterId: string) {

        console.debug("meter maps");
        return this.apiService.get(apiBaseUrl + "/api/PortalsApiMap/FindMap?accountId=" + accountId + "&premiseId=" + premiseId + "&meterId=" + meterId);

    }
    acceptPendingmeters(meterIds: MeterMap[], apiBaseUrl: string) {
        console.debug(JSON.stringify(meterIds));
        return this.apiService.post(apiBaseUrl + "/api/EnergyStar/AcceptMeters", meterIds);
    }

    rejectPendingmeters(meterIds: any[], apiBaseUrl: string) {
        console.debug('rejectPendingmeters');

        return this.apiService.post(apiBaseUrl + "/api/EnergyStar/RejectMeters", meterIds);
    }

    getAcceptedMetersList(propertyId: string, apiBaseUrl: string) {

        console.debug("getAcceptedMetersList");
        let acceptedmeters = localStorage.getItem("acceptmeters");
        if (!acceptedmeters) { acceptedmeters = Date.now().toString(); localStorage.setItem("acceptmeters", acceptedmeters);}
        console.debug(acceptedmeters);
        return this.apiService.get(apiBaseUrl + "/api/EnergyStar/AcceptedMeterDetailsList?propertyId=" + propertyId + '&acceptedMeters=' + acceptedmeters) ;

    }

    saveMappedMeters(espmMap: any, apiBaseUrl: string) {
        console.debug("saveMappedMeters");
        console.debug(espmMap);
        return this.apiService.post(apiBaseUrl + "/api/PortalsApiMap/AddMappedMeter", espmMap);
    }

    getIfAllMeterMapped(apiBaseUrl: string, meterId: string , accountId: string, premiseId: string, meterType : string) {

        return this.apiService.get(apiBaseUrl + "/api/PortalsApiMap/IsMapped?accountId=" + accountId + "&premiseId=" + premiseId + "&meterId=" + meterId + "&meterType=" + meterType);

    }

    getIfMeterMapped(apiBaseUrl: string, meters: any) {
        
        if (meters.constructor === Array)
         return this.apiService.post(apiBaseUrl + "/api/PortalsApiMap/IsAllMapped", meters);
        else
            return this.apiService.post(apiBaseUrl + "/api/PortalsApiMap/IsMeterObjectMapped", meters);

    }

    addCustomerId(apiBaseUrl: string, meterId) {

        
        return this.apiService.post(apiBaseUrl + "/api/PortalsApiMap/UpdateCustomerId", meterId);

    }

    getBillingData(apiBaseUrl: string, meterId: string) {

        const today = new Date();
        const endDate = today.toISOString().substring(0, 10);
        const startdate = new Date(today.getFullYear() - 1, today.getMonth(), today.getDay()).toISOString()
            .substring(0, 10);
        return this.apiService.get(apiBaseUrl + "/api/PortalsApiMap/GetAllMeterBillingForMeter?meterId=" + meterId + "&endDate=" + endDate + "&startDate=" + startdate);

    }

    getConsumptionData(apiBaseUrl: string, meterId: string) {

        return this.apiService.get(apiBaseUrl + "/api/EnergyStar/MeterConsumptionData?meterId=" + meterId);

    }

    getMeterDetails(apiBaseUrl: string, meterId: string) {

        return this.apiService.get(apiBaseUrl + "/api/EnergyStar/MeterDetails?meterId=" + meterId);

    }

    getCustomFieldsForMeter(apiBaseUrl: string, meterId: string) {
        return this.apiService.get(apiBaseUrl + "/api/EnergyStar/CustomMeterFields?meterId=" + meterId);
    }

    

   

}