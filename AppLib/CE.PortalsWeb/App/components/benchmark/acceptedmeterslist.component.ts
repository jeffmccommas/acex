﻿import { Component, ViewChild, Input, OnInit } from '@angular/core';
import { ApiService } from "../shared/api.service"
import { MetersService } from "./services/meters.service";
import { ContentService } from "../shared/content/content.service"; 
import { MeterDetails } from "./meterdetails.component"; 
import { AccordionModule } from 'ngx-bootstrap';



@Component({
    moduleId: module.id,
    selector: 'acceptedmeterslist',
    templateUrl: 'acceptedmeterslist.component.html'
   })

export class AcceptedMetersList {
    @Input() public propertyId: string = null;
    public apiBaseUrl: string;
    public loading : boolean = false;
    public result: any = [] ;
    public status: any = {
        isFirstOpen: true,
        isOpen: false
    }; 
    private tabkey = 'tab.espmmeters';
    private WidgetType = 'meters';
    private content: any;

    public unitOfMeasure : string;

    constructor(private apiService: ApiService, private contentService: ContentService, private meterService: MetersService) {


    }
    public ngOnInit() {
        if (this.propertyId) {
            this.result = [];
            this.loading = true;
            this.apiService.getApIUrl().subscribe(
                res => {
                    this.apiBaseUrl = res;
                    this.contentService.getMetersContent(this.tabkey, this.WidgetType, this.apiBaseUrl)
                        .subscribe(res => {
                            this.content = res;
                            console.debug(res);
                        });
                    this.meterService.getAcceptedMetersList(this.propertyId, this.apiBaseUrl).subscribe(
                        res => {

                            this.result = res.json().values;
                            console.debug(this.result);
                            this.loading = false;
                        });
                });
        }

    }

    

    public onunitOfMeasure(unitOfMeasure : string ) {
        this.unitOfMeasure = unitOfMeasure;
    }


}

