﻿import { Component, Input } from "@angular/core";
import { AccountsService } from "./services/accounts.service";
import { Account } from "./services/account";
import { ApiService } from "../shared/api.service";
import { ContentService } from "../shared/content/content.service";


@Component({
    selector: "accountdetails",
    moduleId: module.id,
    templateUrl: 'accountdetails.component.html'
})


export class AccountDetailsComponent 
{
    private apiBaseUrl: string;
    @Input() public accountId: string;
    private result: any;
    private errors: any;
    private content: any;
    private tabkey = 'tab.espmmeters';
    private WidgetType = 'meters';
    private noAccess: Boolean = false;

    constructor(private apiService: ApiService, private accountsService: AccountsService, private contentService: ContentService) {                

    }

    ngOnInit() {
        this.result = null;
        this.noAccess = false;
        this.apiService.getApIUrl().subscribe(
            res => {
                this.apiBaseUrl = res;
                this.accountsService.getAccountDetails(this.apiBaseUrl, this.accountId).subscribe(
                    res => {
                        this.result = (res.json().customer);

                    }
                    ,
                    error => {
                        if (error.status === 403) {
                            this.noAccess = true;
                        } else {
                            this.errors = error;
                        }
                    },
                    () => {
                        // 'onCompleted' callback.
                        // No errors, route to new page here
                    }
                );
                

                this.contentService.getMetersContent(this.tabkey, this.WidgetType, this.apiBaseUrl)
                    .subscribe(res => {
                        this.content = res;
                    });
            });
    }
}