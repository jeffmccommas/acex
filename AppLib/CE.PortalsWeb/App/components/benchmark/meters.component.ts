﻿import { MetersService } from "./services/meters.service";
import { Meter } from "./services/meter";
import { MeterMap } from "./services/metersmap";
import { PropertyInfo } from "./services/propertyinfo";
import { Component, ViewChild } from "@angular/core";
import { AccountsService } from "./services/accounts.service";
import { Account } from "./services/account";
import { ApiService } from "../shared/api.service"
import { ModalDirective } from 'ngx-bootstrap';
import { ContentService } from "../shared/content/content.service";
import { NgxPaginationModule, PaginationInstance } from 'ngx-pagination';
import { NgModule } from "@angular/core";
import { AccountDetailsComponent } from "./accountdetails.component";
import { PropertyDetailsComponent } from "./propertydetails.component";
import { PropertyService } from "./services/property.service";
import { AcceptedMetersNew } from './acceptedmetersnew.component';
import Pipe = require("../helpers/pipe");
import ObjToArr = Pipe.ObjToArr;
//kendo
import {

    GridDataResult,
    PageChangeEvent
    } from "@progress/kendo-angular-grid";
import Metermappingcomponent = require("./metermapping.component");
import MeterMappingComponent = Metermappingcomponent.MeterMappingComponent;

@NgModule({

})

@Component({
    selector: "meter-screen",
    moduleId: module.id,
    templateUrl: "meters.component.html"

})
    
export class MetersComponent {
    public filter: string = '';
    bigTotalItems: number = 200;
    public maxSize: number = 20;
    public directionLinks: boolean = true;
    public autoHide: boolean = false;
    private pageSize = 20;
    public config: PaginationInstance = {

        id: 'advancedMeters',

        itemsPerPage: this.pageSize,

        currentPage: 1
    };

    onPageChange(number: number) {
        this.displayMeter.forEach(x => x.state = false);
        this.checkAllChecked = false;
        this.apiService.postEvents("Clicked on next for page number:" + number, "", "");
        this.config.currentPage = number;
        this.currentPage = number.toString();
        
        this.loadKendoGridData(this.config.currentPage.toString());
        this.disableButtons = true;

    }

    imageSource: boolean = false;

    private errorMessage: "";
    private title: "the title of the meters page";
    private apiBaseUrl: string;
    private nextPageLink?: any;
    private meters: any = [];
    private displayMeter: Meter[];
    private metersobjects;
    //kendo
    private gridView: GridDataResult;
    private data: Object[];

    private skip = 0;
    private paging = true;
    private parameters;
    private tabkey = 'tab.espmmeters';
    private WidgetType = 'meters';
    @ViewChild('staticModalMeters')
    public staticModalMeters: ModalDirective;
    @ViewChild('modal')
    modal: any;
    private hideButtons: boolean;
    private hidePendingMeters: boolean;
    private openRejectDialog: boolean;
    private customers: Object[];
    private properties: Object[];
    private showpopup: boolean = false;
    private loading: boolean = true;
    private result: any;
    private filteredaAccoutnId: string = null;
    private isFiltered: Boolean;
    private filteredAccountName: string;
    private filteredPropertyId: string = null;
    private filteredPropertyName: string;
    private disableButtons: boolean = true;
    private currentPage: string = "0";
    private checkAllChecked: boolean = false;
    private nopropertydata : boolean = false;
    @ViewChild('metermapping') metermapping: MeterMappingComponent;

    @ViewChild('acceptedMetersNew') acceptedMetersNew: AcceptedMetersNew;

    collection = [];


    constructor(private apiService: ApiService, private metersService: MetersService, private accountsService: AccountsService,
        private propertyService: PropertyService, private contentService: ContentService) {
        this.apiService.postEvents("Visited Meters tab", "", "");
        this.parameters = apiService.getangParams();
        this.filteredaAccoutnId = null;
        this.isFiltered = false;
        this.filteredPropertyId = null;


    }

    ngOnInit() {
        this.apiService.getApIUrl().subscribe(
            res => {
                this.currentPage = "1";
                this.apiBaseUrl = res;
                this.meters = [];
                this.nopropertydata = true;
                this.getCustomerList(res);
                this.getPendingMeters(res, this.filteredaAccoutnId, this.filteredPropertyId);
                this.contentService.getMetersContent(this.tabkey, this.WidgetType, this.apiBaseUrl)
                    .subscribe(res => {
                        console.debug(res);
                        this.result = res;
                        this.hidePendingMeters = false;
                        this.hideButtons = false;
                    });
            },
            err => {
                console.error("getApIUrl error");
                console.error(err);
                this.handleError(err);
            },
            () => console.debug("Get URL call completed")
        );
    }
    private getActive() {
        if (this.hideButtons === true)
            return "active";
        else
            return "not";

    }

    private onToggle(): void {
        this.apiService.postEvents("Get toggle popup", "", "");
        console.debug('toggle popup');
        this.showpopup = !this.showpopup;
    }
    private onNavigateMeters(pending: boolean) {
        
        if (pending) {
            this.apiService.postEvents("User clicked on Pending Meters", this.filteredaAccoutnId, this.filteredPropertyId);
            this.imageSource = false;
            //this.getPendingMeters(this.apiBaseUrl, this.filteredaAccoutnId, this.filteredPropertyId);
            this.onReload();
            this.getPendingMeters(this.apiBaseUrl, this.filteredaAccoutnId, this.filteredPropertyId);
        }
        else {
            this.imageSource = true;
            this.openRejectDialog = false;
            this.apiService.postEvents("User clicked on Connected Meters", this.filteredaAccoutnId, this.filteredPropertyId);
            this.getAcceptedMeters(this.apiBaseUrl);
        }

    }

    private filterAccountList(accountId: string, accountName: string) {

        if (accountId) {

            this.isFiltered = true;
            this.properties = null;
            this.nopropertydata = false; 
            this.propertyService.getAcceptedPropertyList(this.apiBaseUrl, accountId).subscribe(res => {
                
                if (parseInt(res.json().count) !== 0) {
                    this.properties = res.json();
                    this.nopropertydata = false;
                } else {
                    this.nopropertydata = true;
                }
            });
            
        }
        
        this.filteredaAccoutnId = accountId;
        this.filteredAccountName = accountName;
        this.filteredPropertyId = null;
        this.filteredPropertyName = this.result.propertyId;
        this.acceptedMetersNew.accountId = accountId;
        this.acceptedMetersNew.propertyId = null;
        this.acceptedMetersNew.getPropertyDetails();
        //if (!this.hideButtons) {
        //    this.getPendingMeters(this.apiBaseUrl, this.filteredaAccoutnId, this.filteredPropertyId);
        //}
        //else {

            
        //}
        this.disableButtons = true;
        this.apiService.postEvents("Visited drop down to filter Account", this.filteredaAccoutnId, this.filteredPropertyId);

    }

    private filterPropertyList(propertyId: string, propertyName: string) {

        if (propertyId !== "-1") {
            this.filteredPropertyId = propertyId;
            this.filteredPropertyName = (propertyName);
            this.isFiltered = true;
            this.acceptedMetersNew.propertyId = propertyId;
            this.acceptedMetersNew.getPropertyDetails();
            //if (!this.hideButtons) {

            //    this.getPendingMeters(this.apiBaseUrl, this.filteredaAccoutnId, this.filteredPropertyId);
            //} else {

                

            //}
            this.disableButtons = true;
            this.apiService.postEvents("Visited drop down to filter property",
                this.filteredaAccoutnId,
                this.filteredPropertyId);
        }
    }

    private clearFilters() {
        this.apiService.postEvents("User clicked on clear filters", this.filteredaAccoutnId, this.filteredPropertyId);
        this.isFiltered = false;
        this.filterAccountList(null, this.result.accountId);
    }

    private getCustomerList(apiUrl: string) {

        this.accountsService.getAcceptedCustomerList(apiUrl).subscribe(res => {
            this.customers = res.json();

        });
    }

    private getAcceptedMeters(apiUrl: string) {
        this.hidePendingMeters = true;
        this.hideButtons = true;
        this.acceptedMetersNew.getPropertyDetails();
    }

    private getPendingMeters(apiUrl: string, accountId: string, propertyId: string) {
        this.loading = true;
        this.displayMeter = [];
        console.debug("MetersComponent->getPendingAccounts - > " + apiUrl);
        this.apiBaseUrl = apiUrl;
        this.hidePendingMeters = false;
        this.metersService.getPendingMeters(this.parameters, this.apiBaseUrl)
            .subscribe(res => {
                    let content = res.json();
                    if (content.pendingList) {
                        this.nextPageLink = content.pendingList.links;
                        this.meters = content.pendingList.meter;
                        this.metersService.getIfMeterMapped(this.apiBaseUrl, this.meters).subscribe(
                            result => {
                                this.meters = new ObjToArr().transform(result.json());
                                this.metersService.addCustomerId(this.apiBaseUrl,
                                    this.meters.filter(a => a.isMapped).map(x => x.meterId).join('|')).subscribe();
                                this.loadKendoGridData("1");
                                this.hideButtons = false;
                                this.loading = false;
                            });


                    }
                    else {
                        this.meters = [];
                        this.gridView = {
                            data: null,
                            total: 0
                        };
                        this.loading = false;
                    }
                   
                    
                },
                err => {
                    console.error("getPendingMeters error");
                    this.handleError(err);
                },
                () => console.debug("getPendingMeters done")

            );
    }

    private pageChange(event: PageChangeEvent): void {
        console.debug(event);
        this.skip = event.skip;
        this.loadKendoGridData(this.config.currentPage.toString());
        this.disableButtons = true;
    }

    private loadKendoGridData(pagenumber: string ): void {

        this.mapToGridView(pagenumber);
        if (this.displayMeter.length > 0) {
            this.gridView = {
                data: this.displayMeter,
                total: this.displayMeter.length
            };
        }
        else {
            this.gridView = {
                data: this.displayMeter,
                total: 0
            };
        }
        if (this.paging) {
            if (this.nextPageLink == null) {

                this.paging = false;
            } else {

                this.nextPage();
            }
        }
    }
    private arrayObjectIndexOf(myArray, searchTerm, property) {
        for (var i = 0, len = myArray.length; i < len; i++) {
            if (myArray[i][property] === searchTerm) return i;
        }
        return -1;
    }

    private onMetersSelect(meterId, accountId, podId, e) {
        //if (this.metersobjects == null)
        //    this.metersobjects = [];
        let checkedany = this.displayMeter.some(x => x.state == true);
        if (e.target.checked) {
            //if (this.metersobjects.length === 0 && this.metersobjects.state == true) {
            //    this.metersobjects.push(new MeterMap(meterId,accountId,podId));
            //}
            //else if (this.arrayObjectIndexOf(this.metersobjects, meterId,"customer") === -1) {
            //    this.metersobjects.push(new MeterMap(meterId, accountId, podId));
            //}
            this.disableButtons = false;
        }
        else if (!checkedany) {
            //var index = this.arrayObjectIndexOf(this.metersobjects, meterId, "customer");
            //this.metersobjects.splice(index, 1);
            //if (this.metersobjects.length === 0) {
            this.disableButtons = true;
            //}
        }
        console.debug(this.metersobjects);
    }

    @ViewChild('autoShownModal') public autoShownModal: ModalDirective;
    public isModalShown: boolean = false;
    public billsView: boolean = false;
    private showModal(accountId: string, podId: string, metername: string, meterId: string, meterType: string) {
        this.billsView = false;
        this.metermapping.accountId = accountId;
        this.metermapping.podId = podId;
        this.metermapping.meterName = metername;
        this.metermapping.meterId = meterId;
        this.metermapping.meterType = meterType;
        this.metermapping.getMappedMeters();

        this.autoShownModal.show();
    }
    private showBillsModal() {
        this.billsView = true;
        this.autoShownModal.show();

    }
    public hideModal(): void {
        this.autoShownModal.hide();
    }

    public onHidden(): void {
        this.isModalShown = false;
    }

    private nextPage() {
        let pagenumber = "";
        let startIndex = 0;
        if (this.nextPageLink.link.constructor === Array) {
            let findNextPageLink = this.nextPageLink.link[0].linkDescription.indexOf("next");
            let nextPageArrayIndex = 0;

            if (findNextPageLink === -1)
                nextPageArrayIndex = 1;


            startIndex = this.nextPageLink.link[nextPageArrayIndex].link.indexOf("?page=");
            if (startIndex !== -1) {
                pagenumber = this.nextPageLink.link[nextPageArrayIndex].link.substr(startIndex + 6, this.nextPageLink.link[nextPageArrayIndex].link.length - (startIndex + 6));
                //this.currentPage = pagenumber;
            }
            
        } else {
            //assuming that ESPM web service will not have "prev page" when it returns the last page
            //we double checked this assumption in their prod environment (7/24/2017)
            startIndex = this.nextPageLink.link.link.indexOf("?page=");
            if (startIndex !== -1) {
                pagenumber = this.nextPageLink.link.link.substr(startIndex + 6, this.nextPageLink.link.link.length - (startIndex + 6));
                
            }
        }
        console.debug("pagenumber:" + pagenumber);
        this.metersService.getPendingMeters(this.parameters, this.apiBaseUrl, parseInt(pagenumber))
            .subscribe(res => {
                    this.currentPage = pagenumber;
                    let content = res.json();
                    this.nextPageLink = content.pendingList.links;
                    console.info(this.nextPageLink);
                    this.meters = content.pendingList.meter;
                    this.metersService.getIfMeterMapped(this.apiBaseUrl, this.meters).subscribe(
                        result => {
                            this.meters = new ObjToArr().transform(result.json());
                            this.metersService.addCustomerId(this.apiBaseUrl,
                                this.meters.filter(a => a.isMapped).map(x => x.meterId).join('|')).subscribe();
                            this.mapToGridView(pagenumber);
                            this.gridView = {
                                data: this.displayMeter,//.slice(this.skip, this.skip + this.pageSize),
                                total: this.displayMeter.length
                            };

                        });

                    this.bigTotalItems = this.displayMeter.length;

                },
                err => {
                    console.error("Error on next page");
                    this.handleError(err);
                },
                () => console.debug("nextPage done")
            );
    }

    private handleError(err) {
        this.errorMessage = err;
        console.error("error getting meters");
    }
    private mapToGridView(pagenumber : string) {

        if (!(this.meters == null)) {
            if (this.meters.constructor === Array) {
                for (let entry of this.meters) {
                    //checking custom fields
                    let customField1 = "";
                    let customField2 = "";
                    let errorMessage = "";
                    if (entry.customFieldList) {
                        if (entry.customFieldList.customField[0].text) {

                            customField1 = entry.customFieldList.customField[0].text;
                        } else {
                            errorMessage = "Account Id (custom field) is required.";
                        }

                        if (entry.customFieldList.customField[1].text)
                            customField2 = entry.customFieldList.customField[1].text;
                    }

                    let propertyInfo = new PropertyInfo(entry.propertyId,
                        entry.propertyInfo.name,
                        entry.propertyInfo.address.address1,
                        entry.propertyInfo.address.city,
                        entry.propertyInfo.address.postalCode,
                        entry.propertyInfo.address.state,
                        entry.propertyInfo.address.country,
                        entry.propertyInfo.numberOfBuildings,
                        entry.propertyInfo.constructionStatus,
                        entry.propertyInfo.primaryFunction,
                        entry.propertyInfo.yearBuilt,
                        entry.propertyInfo.grossFloorArea.value + ' ' + entry.propertyInfo.grossFloorArea.httpMethod,
                        entry.propertyInfo.occupancyPercentage,
                        entry.propertyInfo.isFederalProperty
                    );

                    this.displayMeter.push(new Meter(
                            entry.meterId,
                            entry.propertyId,
                            entry.accountId,
                            entry.customField1,
                            entry.customField2,
                            propertyInfo,
                            entry.meterInfo.name,
                            entry.meterInfo.metered,
                            entry.meterInfo.firstBillDate,
                            entry.meterInfo.inUse,
                            entry.meterInfo.type,
                            entry.meterInfo.unitOfMeasure,
                            entry.shareAudit.lastUpdatedDate,
                            errorMessage,
                            customField1,
                            customField2,
                            pagenumber,
                            entry.isMapped

                        )
                    );

                }
            } else {

                //checking custom fields
                let customField1 = "";
                let customField2 = "";
                let errorMessage = "";
                if (this.meters.customFieldList) {
                    if (this.meters.customFieldList.customField[0].text) {

                        customField1 = this.meters.customFieldList.customField[0].text;
                    } else {
                        errorMessage = "Account Id (custom field) is required.";
                    }

                    if (this.meters.customFieldList.customField[1].text)
                        customField2 = this.meters.customFieldList.customField[1].text;
                }
                let propertyInfo = new PropertyInfo(this.meters.propertyId,
                    this.meters.propertyInfo.name,
                    this.meters.propertyInfo.address,
                    this.meters.propertyInfo.city,
                    this.meters.propertyInfo.postalCode,
                    this.meters.propertyInfo.state,
                    this.meters.propertyInfo.country,
                    this.meters.propertyInfo.numberOfBuildings,
                    this.meters.propertyInfo.constructionStatus,
                    this.meters.propertyInfo.primaryFunction,
                    this.meters.propertyInfo.yearBuilt,
                    this.meters.propertyInfo.grossFloorArea.value + ' ' + this.meters.propertyInfo.grossFloorArea.httpMethod,
                    this.meters.propertyInfo.occupancyPercentage,
                    this.meters.propertyInfo.isFederalProperty
                );

                this.displayMeter.push(new Meter(
                    this.meters.meterId,
                    this.meters.propertyId,
                    this.meters.accountId,
                    this.meters.customField1,
                    this.meters.customField2,
                    propertyInfo,
                    this.meters.meterInfo.name,
                    this.meters.meterInfo.metered,
                    this.meters.meterInfo.firstBillDate,
                    this.meters.meterInfo.inUse,
                    this.meters.meterInfo.type,
                    this.meters.meterInfo.unitOfMeasure,
                    this.meters.shareAudit.lastUpdatedDate,
                    errorMessage,
                    customField1,
                    customField2,
                    pagenumber,
                    this.meters.isMapped
                ));


            }
        }
        
        if (this.filteredaAccoutnId) {
            this.displayMeter = this.displayMeter.filter(x => x.accountId === this.filteredaAccoutnId);
        }
        if (this.filteredPropertyId) {
            this.displayMeter = this.displayMeter.filter(x => x.propertyId === this.filteredPropertyId);
        }
        //this.displayMeter.sort((a, b) => new Date(b.timeStamp).getTime() - new Date(a.timeStamp).getTime());

        this.meters.length = 0;//clear array list


    }
    private checkforSelectedMeters() {

        if (this.metersobjects == null || this.metersobjects.length == 0) {
            this.handleError(this.result.noSelection);
            return false;
        }
        return true;
    }

    private checkAll(ev) {

       
        this.displayMeter.filter(x => x.pageNumber === this.config.currentPage.toString()).forEach(y => y.state = ev.target.checked);
        this.checkAllChecked = true;
        if (ev.target.checked) {
            this.disableButtons = false; 
        }
        else
        { this.disableButtons = true; }
    }

    private isAllChecked() {
       return this.checkAllChecked && this.displayMeter.filter(_ => _.pageNumber == this.config.currentPage.toString()).every(_ => _.state);
    }

    private onAcceptRejectMeters(choice: boolean) {

        this.errorMessage = '';
        this.metersobjects = [];
        if (this.displayMeter.some(x => x.state === true)) {
            this.displayMeter.forEach(x => {
                if (x.state)
                    this.metersobjects.push(new MeterMap(x.meterId, x.mapaccountId, x.mappodId));

            });
            this.onReload();
            this.staticModalMeters.show();
            if (choice == true) {
                this.apiService.postEvents("User Clicked on Accept Pending Meters", "", "");
                this.apiService.getApIUrl().subscribe(
                    res => {
                        this.metersService.acceptPendingmeters(this.metersobjects, res)
                            .subscribe(res => {
                                    localStorage.setItem("acceptmeters", Date.now().toString());
                                    var processedaccounts = res.json();
                                    this.meters = [];
                                    this.displayMeter = this.displayMeter.filter(x => x.state === false);
                                    this.staticModalMeters.hide();
                                    this.getPendingMeters(this.apiBaseUrl, this.filteredaAccoutnId, this.filteredPropertyId);
                                },
                                err => {
                                    this.handleError(err);
                                },
                                () => console.debug("getPendingAccounts done")
                            );
                    });

            }
            else {
                this.apiService.postEvents("User clicked on Reject Pending Meters", "", "");
                this.openRejectDialog = false;
                this.apiService.getApIUrl().subscribe(
                    res => {
                        this.metersService.rejectPendingmeters(this.metersobjects, res)
                            .subscribe(res => {
                                    let processedaccounts = res.json();
                                    this.meters = [];
                                    this.displayMeter = this.displayMeter.filter(x => x.state === false);
                                    this.staticModalMeters.hide();
                                    this.getPendingMeters(this.apiBaseUrl, this.filteredaAccoutnId, this.filteredPropertyId);
                                },
                                err => {
                                    this.handleError(err);
                                },
                                () => console.debug("getPendingAccounts done")
                            );
                    });
            }

        }
        else {
            this.handleError(this.result.noSelection);
            return;
        }

        //console.log(this.metersobjects);

        /*
        this.errorMessage = '';
        if (this.metersobjects == null || this.metersobjects.length == 0) {
            this.handleError(this.result.noSelection);
            return;
        }
        else {
        }
        */

    }

    onClosed(close: boolean) {
        if (close) {
            this.hideModal();
        }
    }

    onSaved(save: boolean) {
        if (save) {
            this.hideModal();
            this.onReload();
            this.getPendingMeters(this.apiBaseUrl, this.filteredaAccoutnId, this.filteredPropertyId);
        }
    }

    onReload() {
    var number = 1;
    this.config.currentPage = number;
    this.currentPage = number.toString();
    this.paging = true;
    }

    onShowAcceptedMeters() {

    }

}
