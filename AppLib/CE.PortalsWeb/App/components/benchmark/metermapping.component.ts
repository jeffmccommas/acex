﻿import { Component, Input, EventEmitter, Output } from "@angular/core";
import { EspmMap } from "./services/espmmap";
import { ESPMMapDetails } from "./services/espmmap";
import { MetersService } from "./services/meters.service";
import { ApiService } from "../shared/api.service"
import { MappedCustomer } from "./mappedcustomer.component";
import Meterscomponent = require("./meters.component");
import MetersComponent = Meterscomponent.MetersComponent;
import { ContentService } from "../shared/content/content.service";
@Component({
    selector: "metermapping",
    moduleId: module.id,
    templateUrl: 'metermapping.component.html'
})


export class MeterMappingComponent {

    private apiBaseUrl: string;

    displayMeter: EspmMap;
    @Input() public accountId: string;
    @Input() public meterId: string;
    @Input() public podId: string;
    @Input() public isPending: Boolean = true;
    @Input() public meterName: string;
    @Input() public meterType: string;
    @Output() onClosed = new EventEmitter<boolean>();
    @Output() onSaved = new EventEmitter<boolean>();
    private loading: boolean = false;
    private statusMessage: string = "";
    private tabkey = 'tab.espmmeters';
    private WidgetType = 'meters';
    private hasErrors: boolean = false;
    private hasErrorMapping: boolean = false;
    private fetchData: boolean = false;
    private content: any;

    constructor(private apiService: ApiService, private metersService: MetersService, private contentService: ContentService
        //@Inject(forwardRef(() => MetersComponent)) private meterComponent: MetersComponent
    ) {

    }

    public getMappedMeters() {
        this.loading = true;
        this.statusMessage = "";
        this.hasErrors = false;
        this.hasErrorMapping = false;
        this.displayMeter = null;
        this
        this.apiService.getApIUrl().subscribe(
            res => {
                this.apiBaseUrl = res;


                this.metersService.getMeterMaps(this.apiBaseUrl, this.accountId, this.podId, this.meterId).subscribe(res => {
                    this.displayMeter = res.json();
                    this.displayMeter.meterType = this.meterType;
                    if (this.displayMeter === null) {
                        this.displayMeter = new EspmMap(this.accountId, this.podId, this.meterId);
                        this.displayMeter.espmMapDetails = [];
                    } else {
                        if (this.displayMeter.meterId === null) this.displayMeter.meterId = this.meterId;
                        if (this.displayMeter.espmMapDetails) {
                            for (let i = 0; i < this.displayMeter.espmMapDetails.length; i++) {
                                this.displayMeter.espmMapDetails[i].mapAccountIdError = false;
                                this.displayMeter.espmMapDetails[i].mapPremiseIdError = false;
                                this.displayMeter.espmMapDetails[i].uniqueId = Date.now() + "_" + i;
                            }
                        } else {
                            this.displayMeter.espmMapDetails = [];
                        }
                    }
                    if (this.isPending === false) {

                        this.metersService.getCustomFieldsForMeter(this.apiBaseUrl, this.meterId).subscribe(
                            res => {
                                if (res.json().customFieldList.customField[0].text) {
                                    this.accountId = res.json().customFieldList.customField[0].text;
                                    this.displayMeter.accountId = this.accountId;
                                }
                                if (res.json().customFieldList.customField[1].text) {
                                    this.podId = res.json().customFieldList.customField[1].text;
                                    this.displayMeter.premiseId = this.podId;
                                }
                            });
                    }
                    this.loading = false;
                });

                this.contentService.getMetersContent(this.tabkey, this.WidgetType, this.apiBaseUrl)
                    .subscribe(res => {
                        this.content = res;
                        console.debug(res);
                    });
            },
            err => {
                console.error("getApIUrl error");
                console.error(err);
                //this.handleError(err);
                //TODO: need to show error on the view and also handle if accountid is null/blank
                this.loading = false;
            },
            () => {
                this.loading = false;
                console.debug("Get URL call completed");
            });
    }
    ngOnInit() {
        this.statusMessage = "";
        //this.accountId = null;
        //this.podId = null;


    }

    public getMeterMapsForAcceptedMeters() {
        if (this.isPending === false) {
            this.apiService.getApIUrl().subscribe(
                res => {
                    this.metersService.getCustomFieldsForMeter(res, this.meterId).subscribe(
                        res => {
                            if (res.json().customFieldList.customField[0].text) {
                                this.accountId = res.json().customFieldList.customField[0].text;
                                this.displayMeter.accountId = this.accountId;
                            }
                            if (res.json().customFieldList.customField[1].text) {
                                this.podId = res.json().customFieldList.customField[1].text;
                                this.displayMeter.premiseId = this.podId;
                            }
                        });
                });
            this.getMappedMeters();
        }
    }

    addmetermap() {
        this.apiService.postEvents("User clicked Add Mapping for the MeterId: " + this.meterId, this.displayMeter.accountId, this.displayMeter.premiseId);
        this.displayMeter.espmMapDetails.push(new ESPMMapDetails(this.displayMeter.mapId, "", ""));
    }

    removemetermap() {
        this.apiService.postEvents("User clicked on Remove Mapping for the MeterId: " + this.meterId, this.displayMeter.accountId, this.displayMeter.premiseId);
        this.displayMeter.espmMapDetails.pop();
        this.fetchData = false;
        if (this.displayMeter.espmMapDetails.every(x => x.mapAccountIdError === false) &&
            this.displayMeter.espmMapDetails.every(x => x.mapPremiseIdError === false)) {
            this.hasErrors = false;
            this.hasErrorMapping = false;
            this.statusMessage = "";

        }

    }
    saveMappedMetersAcceptedMeters() {

    }
    saveMappedMeters() {
        this.apiService.postEvents("User clicked on Save Mapping for the MeterId: " + this.meterId + " and account Id: " + this.displayMeter.accountId, this.displayMeter.accountId, this.displayMeter.premiseId);
        if (this.enableSave() && !this.fetchData) {
            this.statusMessage = "";
            this.loading = true;
            this.metersService.saveMappedMeters(this.displayMeter, this.apiBaseUrl)
                .subscribe(
                    res => {
                        this.loading = false;
                        this.onSaved.emit(true);
                        this.metersService.addCustomerId(this.apiBaseUrl, this.meterId).subscribe()
                        {
                            this.displayMeter.espmMapDetails.every(x => x.enabled = true);
                        };
                    },
                    err => {
                        console.error("getApIUrl error");
                        console.error(err);
                        //this.handleError(err);
                        //TODO: need to show error on the view and also handle if accountid is null/blank
                        this.loading = false;
                    },
                    () => {
                        this.loading = false;
                        console.debug("Get URL call completed");
                    }
                );
        }
    }

    onBlurAccountId(id, value) {
        this.displayMeter.espmMapDetails.find(x => x.uniqueId === id).mapAccountId = value;
        if (value.length === 0) {
            this.statusMessage = this.content.accountIdNotNull;;
            this.displayMeter.espmMapDetails.find(x => x.uniqueId === id).mapAccountIdError = true;
            this.hasErrors = true;
        }
        else {
            this.statusMessage = "";
            //this.displayMeter.espmMapDetails.find(x => x.uniqueId === id).mapAccountId = value;
            this.displayMeter.espmMapDetails.find(x => x.uniqueId === id).mapAccountIdError = false;
            this.hasErrors = false;
        }
    }

    onBlurPremiseId(id, value) {
        this.displayMeter.espmMapDetails.find(x => x.uniqueId === id).mapPremiseId = value;
        let mappedDetail = this.displayMeter.espmMapDetails.find(x => x.uniqueId === id);
        if (mappedDetail) {
            if (mappedDetail.mapAccountId.length === 0) {
                this.statusMessage = this.content.accountIdNotNull;
                this.displayMeter.espmMapDetails.find(x => x.uniqueId === id).mapAccountIdError = true;
                this.hasErrors = true;
            }
            else if (this.displayMeter.espmMapDetails.filter(
                    x => x.mapAccountId === mappedDetail.mapAccountId && x.mapPremiseId === value)
                .length >
                1) {
                this.statusMessage = this.content.accountIdUnique;;
                this.displayMeter.espmMapDetails.find(x => x.uniqueId === id).mapPremiseIdError = true;
                this.hasErrors = true;
            } else {
                this.statusMessage = "";
                this.displayMeter.espmMapDetails.find(x => x.uniqueId === id).mapAccountIdError = false;
                this.displayMeter.espmMapDetails.find(x => x.uniqueId === id).mapPremiseIdError = false;
                this.hasErrors = false;
            }
        } else {
            this.statusMessage = "";
            if (this.displayMeter.espmMapDetails.find(x => x.uniqueId === id).mapAccountId.length > 0) {
                this.displayMeter.espmMapDetails.find(x => x.uniqueId === id).mapAccountIdError = false;
                this.displayMeter.espmMapDetails.find(x => x.uniqueId === id).mapPremiseIdError = false;
            }
            this.hasErrors = false;
        }



    }

    close() {
        this.apiService.postEvents("User Clicked on Cancel Mapping for the MeterId: " + this.meterId, this.displayMeter.accountId, this.displayMeter.premiseId);
        this.onClosed.emit(true);
    }

    enableSave() {
        if (this.displayMeter) {
            //console.log(this.displayMeter.espmMapDetails);
            //console.log(this.hasErrorMapping);
            //console.log(this.hasErrors);
            //console.log(this.displayMeter.espmMapDetails.every(x => x.mapAccountIdError === false));
            //console.log(this.displayMeter.espmMapDetails.every(x => x.mapPremiseIdError === false));
            //return false;
            var enable = !this.hasErrorMapping &&
                !this.hasErrors &&
                this.displayMeter.espmMapDetails.every(x => x.mapAccountIdError === false) &&
                this.displayMeter.espmMapDetails.every(x => x.mapPremiseIdError === false);
            if (!enable) {
                this.hasErrors = true;
            }
            return enable;
        } else {
            return false;
        }

    }
    onNoMapFound(noMapFoundObject: any) {
        if (!noMapFoundObject.valid) {
            this.hasErrorMapping = true;
            this.displayMeter.espmMapDetails.find(x => x.mapAccountId === noMapFoundObject.accountId &&
                x.mapPremiseId === noMapFoundObject.premiseId).mapAccountIdError = true;
            this.displayMeter.espmMapDetails.find(x => x.mapAccountId === noMapFoundObject.accountId &&
                x.mapPremiseId === noMapFoundObject.premiseId).mapPremiseIdError = true;
        } else {
            {

                this.hasErrorMapping = false;
            }
        }

    }

    onFetchData(fetch: boolean) {
        if (fetch) {
            this.fetchData = true;
        }
        else
        { this.fetchData = false; }
    }


}