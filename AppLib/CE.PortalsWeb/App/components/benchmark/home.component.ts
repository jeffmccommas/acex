﻿import { Component, ViewChild } from "@angular/core";
import { AccountsService } from "./services/accounts.service";
import { Account } from "./services/account";
import { ApiService } from "../shared/api.service"

import { ModalDirective } from 'ngx-bootstrap';
import { ContentService } from "../shared/content/content.service";
import { NgxPaginationModule, PaginationInstance } from 'ngx-pagination';


//kendo
import {

    GridDataResult,
    PageChangeEvent
} from "@progress/kendo-angular-grid";
import Accountsservice = require("./services/accounts.service");
import Contentservice = require("../shared/content/content.service");

@Component({
    selector: "benchmark-portal",
    moduleId: module.id,
    templateUrl: "home.component.html"
})



export class BenchmarkPortalComponent {

    public filter: string = '';
    bigTotalItems: number = 200;
    public maxSize: number = 20;
    public directionLinks: boolean = true;
    public autoHide: boolean = false;
    private pageSize = 20;
    public config: PaginationInstance = {

        id: 'advancedContacts',

        itemsPerPage: this.pageSize,

        currentPage: 1
    };

    onPageChange(number: number) {
        this.displayAccount.forEach(x => x.state = false);
        this.checkAllChecked = false;
        this.apiService.postEvents("Clicked on next for page number:" + number, "", "");
        this.config.currentPage = number;

        this.loadKendoGridData(this.config.currentPage.toString());
        this.disableButtons = true;
    }

    imageSource: boolean = false;

    private title: string = "the title of the home page";
    private errorMessage: "";
    private apiBaseUrl: string;
    private nextPageLink?: any;
    private accounts: any;
    private displayAccount: Account[];
    private accountsobjects: string[];
    private gridView: GridDataResult;
    private data: Object[];

    private skip = 0;
    private paging = true;
    private parameters;
    private tabkey = 'tab.espmcontacts';
    private WidgetType = 'contacts';
    result: any;
    //portalsApiBaseUrl:any;
    @ViewChild('staticModal')
    public staticModal: ModalDirective;
    @ViewChild('modal')
    modal: any;
    private hideContactButtons: boolean;
    private openRejectDialog: boolean;
    private loading: boolean = true;
    private disableButtons: boolean = true;
    private currentPage: string = "1";
    private checkAllChecked: boolean = false;

    collection = [];

    constructor(private apiService: ApiService, private accountsService: AccountsService, private contentService: ContentService) {

        console.info("BenchmarkPortalComponent->getPendingAccounts");
        this.parameters = apiService.getangParams();

    }

    ngOnInit() {
        this.loading = true;
        this.apiService.getApIUrl().subscribe(
            res => {
                this.currentPage = "1";
                this.apiBaseUrl = res;
                this.accounts = [];
                this.getPendingAccounts(res);
                this.hideContactButtons = false;
                console.debug("api url -> " + res);
                this.apiService.postEvents("Visited Contacts Tab", "", "");
            },
            err => {
                this.handleError(err);
            },
            () => console.debug("Get URL call completed")
        );

    }
    private getActive() {
        if (this.hideContactButtons === true)
            return "active";
        else
            return "not";

    }

    public onNavigateContacts(pending: boolean) {


        this.displayAccount = [];
        if (pending) {
            this.apiService.postEvents("Visited Pending Accounts", "", "");
            this.imageSource = false;
            console.debug('pending');
            console.debug(this.hideContactButtons);
            this.getPendingAccounts(this.apiBaseUrl);
        }
        else {
            this.apiService.postEvents("Visited Accepted Accounts", "", "");
            this.imageSource = true;
            console.debug('accepted');
            this.openRejectDialog = false;
            console.debug(this.hideContactButtons);
            this.getAccptedAccounts(this.apiBaseUrl);
        }

    }

    private getAccptedAccounts(apiUrl: string) {
        this.loading = true;
        this.accountsService.getAcceptedAccounts(this.apiBaseUrl)
            .subscribe(res => {
                let content = res.json();
                this.accounts = content;
                console.debug('accepted accounts' + this.accounts);
                this.hideContactButtons = true;
                this.loadKendoGridData("1");
                this.loading = false;

            });
    }

    private getPendingAccounts(apiUrl: string) {
        console.debug("BenchmarkPortalComponent->getPendingAccounts - > " + apiUrl);
        this.loading = true;
        this.apiBaseUrl = apiUrl;
        this.displayAccount = [];
        this.contentService.getContactsContent(this.tabkey, this.WidgetType, this.apiBaseUrl)
            .subscribe(res => {
                this.result = res;
                this.accountsService.getPendingAccounts(this.parameters, this.apiBaseUrl)
                    .subscribe(res => {
                        let content = res.json();
                            if (content.pendingList) {
                                this.nextPageLink = content.pendingList.links;
                                this.accounts = content.pendingList.account;
                            } else {
                                this.accounts = [];
                                this.gridView = {
                                    data: null,
                                    total: 0
                                };
                            }
                        this.hideContactButtons = false;
                        this.loadKendoGridData("1");
                        this.loading = false;
                    }
                    );
            },


            err => console.error("ERROR getting API url" + err.toString()));

    }

    private pageChange(event: PageChangeEvent): void {
        this.apiService.postEvents("User clicked on next page", "", "");
        console.debug("Page change event");
        console.debug(event);
        this.skip = event.skip;
        this.loadKendoGridData(this.config.currentPage.toString());
    }

    private loadKendoGridData(pagenumber: string): void {

        console.debug("loadKendoGridData");
        this.mapToGridView(pagenumber);
        if (this.displayAccount.length > 0) {

            this.gridView = {
                data: this.displayAccount,
                total: this.displayAccount.length
            };
        }
        else {
            this.gridView = {
                data: this.displayAccount,
                total: 0
            };
        }

        if (this.paging) {
            if (this.nextPageLink == null) {
                console.debug("this.paging = false");
                this.paging = false;
            } else {
                console.debug("this.nextPage()");
                this.nextPage();
            }
        }
    }

    private onAcceptRejectContacts(choice: boolean) {
        this.errorMessage = '';
        this.accountsobjects = [];

        if (this.displayAccount.some(x => x.state === true)) {
            this.displayAccount.forEach(x => {
                if (x.state)
                    this.accountsobjects.push(x.accountId);

            });

            this.staticModal.show();
            if (choice == true) {
                this.apiService.postEvents("User clicked on Accept for the selected account", "", "");
                this.displayAccount = [];
                this.apiService.getApIUrl().subscribe(
                    res => {
                        this.accountsService.acceptPendingaccounts(this.accountsobjects, res).subscribe(res => {
                            var processedaccounts = res.json();
                            this.accountsService.getPendingAccounts(this.parameters, this.apiBaseUrl)
                                .subscribe(res => {
                                    let content = res.json();
                                    if (content.pendingList) {
                                        this.accounts = content.pendingList.account;
                                        for (let account of this.accounts) {
                                            for (let processed of processedaccounts) {
                                                if ((account.accountId == processed.accountId) &&
                                                    processed.errorMessage != null) {
                                                    account.errorMessage = processed.errorMessage;
                                                    account.isvalid = false;
                                                    break;
                                                }
                                            }
                                        }
                                    } else {
                                        this.accounts = [];
                                    }
                                    this.displayAccount = [];
                                    this.loadKendoGridData("1");
                                    this.staticModal.hide();
                                    this.accountsobjects = [];
                                });
                        },
                            err => {
                                this.handleError(err);
                            },
                            () => console.debug("getPendingAccounts done")
                        );
                    });

            }
            else {
                this.apiService.postEvents("User clicked on reject for the selected account", "", "");
                this.openRejectDialog = false;
                console.debug(this.accountsobjects);
                this.apiService.getApIUrl().subscribe(
                    res => {
                        this.accountsService.rejectPendingaccounts(this.accountsobjects, res).subscribe(res => {
                            this.getPendingAccounts(this.apiBaseUrl);
                            this.accountsobjects = [];
                            this.staticModal.hide();
                        },
                            err => {
                                this.handleError(err);
                            },
                            () => console.debug("getPendingAccounts done")
                        );
                    });
            }

        }

        else {
            this.handleError(this.result.noSelection);
            return;
        }

        // console.log(this.accountsobjects);

        /*
        this.errorMessage = '';
        if (this.accountsobjects == null || this.accountsobjects.length == 0) {
            this.handleError(this.result.noSelection);
        }
        else {  }
        */

    }

    private checkAll(ev) {

        this.displayAccount.filter(x => x.pagenumber === this.config.currentPage.toString()).forEach(y => y.state = ev.target.checked);
        this.checkAllChecked = true;
        if (ev.target.checked)
        { this.disableButtons = false; }
        else
        { this.disableButtons = true; }
    }

    private isAllChecked() {
        return this.checkAllChecked && this.displayAccount.filter(_ => _.pagenumber == this.config.currentPage.toString()).every(_ => _.state);
    }



    private onContactsSelect(accountId, e) {
        let checkedany = this.displayAccount.some(x => x.state == true);
        //if (this.accountsobjects == null)
        //    this.accountsobjects = [];
        if (e.target.checked) {
            //if (this.accountsobjects.length == 0)
            //{
            //    this.accountsobjects.push(accountId);
            //}
            //else if (this.accountsobjects.indexOf(accountId) === -1)
            //{
            //    this.accountsobjects.push(accountId);
            //}
            this.disableButtons = false;
        }
        else if (!checkedany) {
            //var index = this.accountsobjects.indexOf(accountId);
            //this.accountsobjects.splice(index, 1);
            //if (this.accountsobjects.length == 0) {
            this.disableButtons = true;
            //}
        }

    }

    private nextPage() {

        let pagenumber = "";
        let startIndex = 0;

        if (this.nextPageLink.link.constructor === Array) {
            let findNextPageLink = this.nextPageLink.link[0].linkDescription.indexOf("next");
            let nextPageArrayIndex = 0;

            if (findNextPageLink === -1)
                nextPageArrayIndex = 1;


            startIndex = this.nextPageLink.link[nextPageArrayIndex].link.indexOf("?page=");
            if (startIndex !== -1) {
                pagenumber = this.nextPageLink.link[nextPageArrayIndex].link.substr(startIndex + 6, this.nextPageLink.link[nextPageArrayIndex].link.length - (startIndex + 6));
            }
        } else {
            //assuming that ESPM web service will not have "prev page" when it returns the last page
            //we double checked this assumption in their prod environment (7/24/2017)
            startIndex = this.nextPageLink.link.link.indexOf("?page=");
            if (startIndex !== -1) {
                pagenumber = this.nextPageLink.link.link.substr(startIndex + 6, this.nextPageLink.link.link.length - (startIndex + 6));
            }
        }


        console.debug("pagenumber:" + pagenumber);

        this.disableButtons = true;
        this.accountsService.getPendingAccounts(this.parameters, this.apiBaseUrl, parseInt(pagenumber))
            .subscribe(res => {
                this.currentPage = pagenumber;
                let content = res.json();

                this.nextPageLink = content.pendingList.links;
                console.debug(this.nextPageLink);
                this.accounts = content.pendingList.account;
                console.debug(this.accounts);
                this.bigTotalItems = this.displayAccount.length;
                this.mapToGridView(pagenumber);
                this.gridView = {
                    data: this.displayAccount,//.slice(this.skip, this.skip + this.pageSize),
                    total: this.displayAccount.length
                };

                console.debug("getPendingAccounts(2)");
            },
            err => {
                this.handleError(err);
            },
            () => console.debug("nextPage done")
            );
    }
    private handleError(err) {
        this.errorMessage = err;
    }
    private mapToGridView(pagenumber: string) {


        //this.displayAccount = [];

        if (!(this.accounts == null)) {
            if (this.accounts.constructor === Array) {
                for (let entry of this.accounts) {
                    let customField1 = "";
                    let customField2 = "";
                    if (!(entry.customFieldList == null)) {
                        if (!(entry.customFieldList.customField[0] == null))
                            customField1 = entry.customFieldList.customField[0].text;
                        if (!(entry.customFieldList.customField[1] == null))
                            customField2 = entry.customFieldList.customField[1].text;
                    }

                    this.displayAccount.push(new Account(
                        entry.accountId,
                        entry.accountInfo.firstName + " " + entry.accountInfo.lastName,
                        entry.accountInfo.address.address1 +
                        ", " +
                        entry.accountInfo.address.city +
                        ", " +
                        entry.accountInfo.address.state +
                        ", " +
                        entry.accountInfo.address.postalCode,
                        entry.connectionAudit.lastUpdatedDate,
                        customField1,
                        customField2,
                        entry.errorMessage, entry.isvalid, false,pagenumber
                    ));
                }
                //this.displayAccount.sort(function (a, b) {
                //    return new Date(b.timeStamp).getTime() - new Date(a.timeStamp).getTime();
                //});
            } else {

                let customField1 = "";
                let customField2 = "";
                if (!(this.accounts.customFieldList == null)) {
                    if (!(this.accounts.customFieldList.customField[0] == null))
                        customField1 = this.accounts.customFieldList.customField[0].text;
                    if (!(this.accounts.customFieldList.customField[1] == null))
                        customField2 = this.accounts.customFieldList.customField[1].text;
                }

                this.displayAccount.push(new Account(
                    this.accounts.accountId,
                    this.accounts.accountInfo.firstName + " " + this.accounts.accountInfo.lastName,
                    this.accounts.accountInfo.address.address1 +
                    ", " +
                    this.accounts.accountInfo.address.city +
                    ", " +
                    this.accounts.accountInfo.address.state +
                    ", " +
                    this.accounts.accountInfo.address.postalCode,
                    this.accounts.connectionAudit.lastUpdatedDate,
                    customField1,
                    customField2,
                    "error", false, false,pagenumber
                ));

            }
        }

        this.accounts.length = 0;//clear array list


    }





}
