﻿import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'objToArr'
})
export class ObjToArr implements PipeTransform {
    transform(object: any) {
        const newArray = [];
        if (object === null)
            return newArray;
        if (typeof object === 'undefined')
            return newArray;

        if (object.constructor === Array) {
            for (let key in object) {
                if (object.hasOwnProperty(key)) {
                    newArray.push(object[key]);
                }
            }
        } else {
            newArray.push(object);
        }
        return newArray;
    }
}