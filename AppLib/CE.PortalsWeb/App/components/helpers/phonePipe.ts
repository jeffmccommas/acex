﻿import { Pipe } from '@angular/core';

@Pipe({
    name: 'phone'
})

export class PhonePipe {
    transform(tel, args) {
        var value = tel.toString().trim().replace(/\W+/g, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 10: // +1PPP####### -> C (PPP) ###-####
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;

            default:
                return tel;
        }

        if (country == 1) {
            country = "";
        }

        number = number.slice(0, 3) + '-' + number.slice(3);

        return (country + '(' + city + ') ' + number).trim();
    }
}
