﻿import { Component, NgModule } from '@angular/core';
import { AccountsService } from './components/benchmark/services/accounts.service';
import { MetersService } from './components/benchmark/services/meters.service';
import { ApiService } from './components/shared/api.service';
import { ApiConfiguration } from './components/shared/api.config';
import { ContentService } from './components/shared/content/content.service';
import { PropertyService } from './components/benchmark/services/property.service';
import { LoginService } from './components/shared/login.service';
import { AlertService } from './components/shared/alert.service';
import { UserManagementService } from './components/shared/userManagement/userManagement.service'
import { ToggleDivDirective } from './components/shared/directives/togglediv.directive'


@Component({
    moduleId: module.id,
    selector: 'myCOmponent',
    templateUrl: 'app.component.html',
    //styleUrls: ['app.component.css'],
    providers: [ApiService, ApiConfiguration, AccountsService, MetersService, ContentService, PropertyService, LoginService, AlertService, UserManagementService],
    

})

    

export class AppComponent { 

}