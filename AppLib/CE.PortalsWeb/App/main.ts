﻿import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { AppModule } from './app.module';
const platform = platformBrowserDynamic();
import { environment } from './environment';
if (environment.production) {
    enableProdMode();
    console.info("Prod enabled");
}
platform.bootstrapModule(AppModule);
