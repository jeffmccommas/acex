﻿'use strict';
module.exports = {
    rootJsFolder: "./wwwroot/node_modules/",
    rootCssFolder: "./wwwroot/css/",
    rootAppFolder: "./wwwroot/app/",
    test: "./test/",
    testHelper: './test/test-helpers/',
    tsFiles: [
        './app/**/!(*.spec)+(.ts)',
    ],
    tsTestFiles: {
        unit: ['./app/**/*.spec.ts'],
        e2e: ['./test/e2e/**/*.ts'],
        helper: ['./test/test-helpers/**/*.ts']
    },
    report: 'report/',
    sources: {
        jsFilesInclSourcePaths: [
            "node_modules/core-js/client/shim.min.js",
            "node_modules/zone.js/dist/zone.js",
            "node_modules/reflect-metadata/Reflect.js",
            "node_modules/systemjs/dist/system.src.js",
            "node_modules/systemjs/dist/system-polyfills.js",
            "node_modules/es6-shim/es6-shim.min.js",
            "node_modules/jquery/dist/jquery.min.js",
            "node_modules/jquery-ui-dist/jquery-ui.min.js",
            "node_modules/bootstrap/dist/js/bootstrap.js",
            "lib/js/*.js"
        ],
        cssVendorFiles: [
            "node_modules/bootstrap/dist/css/bootstrap.css",
            //"node_modules/jquery-ui-dist/jquery-ui.min.css",
            "lib/css/navbar-fixed-side.css",
            "lib/css/styles.css",
            "lib/css/font-awesome.css",
         
            "node_modules/@progress/kendo-angular-grid/dist/npm/css/main.css",
            "lib/css/grid.css"

            //"node_modules/angular2-toaster/lib/toaster.css",
            //"node_modules/ng2-slim-loading-bar/style.css"
        ],
        codecoveragereport: "report/**/*.*",
        angularRC: "node_modules/@angular/**/*.*",
        //angularToastr: "node_modules/angular2-toaster/**/*.js",
        //angularLoadingBar: "node_modules/ng2-slim-loading-bar/**/*.js",
        Rxjs: "node_modules/rxjs/**/*.*",

        //kendo ui
        progress: 'node_modules/@progress/**/*.*',
        telerik: 'node_modules/@telerik/**/*.*',
        bootstrapmodule: 'node_modules/ngx-bootstrap/**/*.*',
        ngxPagination: 'node_modules/ngx-pagination/**/*.*',
        moment: 'node_modules/moment/moment.js',
        popover: 'node_modules/angular-confirmation-popover/**/*.*',
        taginput: 'node_modules/angular2-tag-input/**/*.*',
        
        fonts: 'lib/fonts/**/*.*',
        angularJWT: 'node_modules/angular2-jwt/**/*.*'
        
    }
};