﻿using System.Collections.Generic;

namespace CE.PortalsWeb.Interfaces
{
    public interface IMapper<TFrom, TTo>
    {
        TTo Map(TFrom from);

        IEnumerable<TTo> Map(IEnumerable<TFrom> from);
    }
}
