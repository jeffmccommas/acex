﻿//using System;
//using CE.Portals.Integrations.Models;
//using Microsoft.Extensions.Caching.Memory;
//using CE.Portals.DataService.Models;
//using Newtonsoft.Json;
//using System.Net;
//using System.Text;

//namespace CE.PortalsWeb.Models
//{
//    public class ApIResponseModel
//    {
//        private const string POST = "POST";
//        private const string GET = "GET";
//        private const string DELETE = "DELETE";

//        private IMemoryCache memorycache;
//        private readonly AppSettings settings;
//        public ApIResponseModel(AppSettings _settings, IMemoryCache _memoryCache)
//        {
//            settings = _settings;
//            memorycache = _memoryCache;
//        }

//        private QueryTool GetCurrentEnvironmentKeys(QueryTool query, PortalWebAPIType portalType, int clientId)
//        {

//            query.AuthType = 1;

//            switch (portalType)
//            {
//                case PortalWebAPIType.ESPMApi: query.BaseUrl = settings.ESPMBaseURL; break;
//                case PortalWebAPIType.InsightsApi:
//                    if (settings.APIBaseURL.Length > 0)
//                    {
//                        //var authHeaders2 = CE.Portals.Integrations.API.PortalAPI.GetInsightsAuthHeaders2(query);
//                        var authHeaders = CE.Portals.Integrations.API.PortalAPI.GetInsightsAuthHeaders(query);
//                        if (authHeaders != null)
//                        {
//                            var authheaders = JsonConvert.DeserializeObject<UserDetails>(authHeaders.Content);

//                            query.selectedCEAccessKeyID = authheaders.CeAccessKeyId;
//                            query.BasicKey = $"{"Basic"} {Base64Encode(authheaders.CeAccessKeyId + ":" + authheaders.CeSecretAccessKey)}";
//                        }
//                    }
//                    query.BaseUrl = settings.InsightsAPIBaseURL; break;
//                case PortalWebAPIType.PortalApi: query.BaseUrl = settings.APIBaseURL; break;
//            }

//            query.MessageIdHeader = "13";
//            query.ChannelHeader = "CE";
//            query.DateTime = DateTime.UtcNow;
//            return query;
//        }

//        public ResponseModel ValidateUser(string parameters, string locale)
//        {
//            var query = new QueryTool { LocaleHeader = locale };
//            query = GetCurrentEnvironmentKeys(query, PortalWebAPIType.PortalApi, 0);
//            query.Content = parameters;
//            query.SelectedMethodApi = POST;
//            query.AllowAnonymousAccess = true;
//            query.ContentType = "application/json";
//            var response = CE.Portals.Integrations.API.PortalAPI.GetUserAuth(query);

//            //if (response.StatusCode == HttpStatusCode.OK)
//            //{
//            //    var content = JsonConvert.DeserializeObject<UserDetails>(response.Content);
//            //    var postWebTokenStr = @"?clientId=" + content.ClientId;                
//            //}
            
//            return response;
//        }

//        public ResponseModel GetESPMSecretKeys(string parameters, string locale)
//        {
//            var query = new QueryTool { LocaleHeader = locale };
            
//            query = GetCurrentEnvironmentKeys(query, PortalWebAPIType.PortalApi, 0);
//            query.Parameters = parameters;
//            query.SelectedMethodApi = GET;
//            var response = CE.Portals.Integrations.API.PortalAPI.GetESPMSecretKeys(query);
//            return response;
//        }

       

//        private static string Base64Encode(string plainText)
//        {
//            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
//            return Convert.ToBase64String(plainTextBytes);
//        }
//    }
//}
