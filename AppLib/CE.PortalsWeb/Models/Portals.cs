﻿namespace CE.PortalsWeb.Models
{
    public class Portals
    {
        public int _id { get; set; }
        public string _name { get; set; }

        public bool _isDefault { get; set; }

        public string _component { get; set; }

    }
}
