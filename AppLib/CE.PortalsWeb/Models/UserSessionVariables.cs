﻿namespace CE.PortalsWeb.Models
{
    public class UserSessionVariables
    {
        public string UserId { get; set; }
        public string ClientId { get; set; }
    }
    
}
