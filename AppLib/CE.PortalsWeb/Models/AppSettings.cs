﻿namespace CE.PortalsWeb.Models
{
    public class AppSettings
    {
        
        public string EventTrackingKey { get; set; }
        public string EventTrackingURL { get; set; }
        public string APIBaseURL { get; set; }
        public string AppVersion { get; set; }

    }
}
