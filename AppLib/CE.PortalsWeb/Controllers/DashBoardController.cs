﻿using CE.PortalsWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;


// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace CE.PortalsWeb.Controllers
{
    public class DashBoardController : Controller
    {
        
        private readonly AppSettings _settings;
        private readonly ILogger _logger;
        public IActionResult Index()
        {
            return View();

        }

        public DashBoardController(IOptions<AppSettings> settings, ILogger<DashBoardController> logger)
        {
            _logger = logger;
            _settings = settings.Value;
           
        }
        //[HttpGet]
        //public JsonResult GetAllPortals()
        //{
        //    _logger.LogDebug("GetAllPortals - start");
        //    string WebToken = HttpContext.Request.Headers["WebToken"].ToString();
        //    if (WebToken.Contains("\""))
        //    {
        //        WebToken = WebToken.Replace("\"", "");
        //    }
        //    var loginUser = JsonConvert.DeserializeObject<DBContext.Loginuser>(QueryStringMiddleware.Decrypt(WebToken));
        //    var portals = _portalsMapper.Map(_loginService.GetAllPortals(loginUser.UserId,loginUser.ClientId));

        //    _logger.LogDebug("GetAllPortals - end");
        //    return Json((portals));
        //}
        
        [HttpGet]
        [ResponseCache(CacheProfileName = "Default")]
        public JsonResult GetPortalsApiUrl()
        {
            _logger.LogDebug("GetPortalsApiUrl called");
            return new JsonResult(_settings.APIBaseURL);
        }
        [HttpGet]
        public JsonResult GetPortalsWebAppVersion()
        {
            _logger.LogDebug("GetPortalsWebAppVersion called");
            
            return new JsonResult(_settings.AppVersion);
            
        }
    }
}
