﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;


namespace CE.PortalsWeb.Controllers
{
    public class HomeController : Controller
    {
        //private readonly IHostingEnvironment _hostingEnvironment;
        //private readonly IMemoryCache _memorycache;

        //private readonly AppSettings _settings;
        //private readonly ILogger _logger;

        //private ApIResponseModel _apiResponseModel;

        public HomeController()
        {
            //_logger = logger;
            //_settings = settings.Value;
            //_memorycache = memoryCache;
            //_apiResponseModel = new ApIResponseModel(_settings, _memorycache);

        }
     
        public IActionResult Index()
        {
            return View();

        }
        public IActionResult Error()
        {
            return View();
        }

        [HttpPost("~/cspreport")]
        public IActionResult CspReport([FromBody] CspReportRequest request)
        {
            // TODO: log request to a datastore somewhere
            string error = ($"CSP Violation: {request.CspReport.DocumentUri}, {request.CspReport.BlockedUri}");

            return Ok();
        }

      
        //public IActionResult Error()
        //{
        //    return View();
        //}
        // GET: /Account/Login
        //[HttpGet]
        //public async Task Login()
        //{
        //    _logger.LogInformation("Login called");
        //    if (HttpContext.User == null || !HttpContext.User.Identity.IsAuthenticated)
        //        await HttpContext.Authentication.ChallengeAsync(OpenIdConnectDefaults.AuthenticationScheme, new AuthenticationProperties { RedirectUri = "/" });
        //}

        // GET: /Account/LogOff
        //[HttpGet]
        //public async Task LogOff()
        //{
        //    if (HttpContext.User.Identity.IsAuthenticated)
        //    {
        //        await HttpContext.Authentication.SignOutAsync(OpenIdConnectDefaults.AuthenticationScheme);
        //        await HttpContext.Authentication.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        //    }
        //}

        //[HttpGet]
        //public async Task EndSession()
        //{
        //    // If AAD sends a single sign-out message to the app, end the user's session, but don't redirect to AAD for sign out.
        //    await HttpContext.Authentication.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        //}

        //[HttpGet]
        //public JsonResult ValidateUser(string userName, string password)
        //{
        //    _logger.LogInformation("Validating User");
        //    string response = string.Empty;
        //    try
        //    {
        //        if(_apiResponseModel==null)
        //            _apiResponseModel = new ApIResponseModel(_settings, _memorycache);



        //        var responseReturn = _apiResponseModel.ValidateUser(JsonConvert.SerializeObject(new ApplicationUser() { UserName = userName, Password = password}), "en-US");
        //        if (responseReturn.StatusCode == System.Net.HttpStatusCode.OK)
        //        {
        //            var loginUser = JsonConvert.DeserializeObject<UserDetails>(responseReturn.Content);
        //            responseReturn.Content = QueryStringMiddleware.Encrypt(responseReturn.Content);

        //        }
        //        response = JsonConvert.SerializeObject(responseReturn);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError("Error Validating User",ex.Message);
        //    }
        //    return Json(response);
        //}
    }

    public class CspReportRequest
    {
        [JsonProperty(PropertyName = "csp-report")]
        public CspReport CspReport { get; set; }
    }

    public class CspReport
    {
        [JsonProperty(PropertyName = "document-uri")]
        public string DocumentUri { get; set; }

        [JsonProperty(PropertyName = "referrer")]
        public string Referrer { get; set; }

        [JsonProperty(PropertyName = "violated-directive")]
        public string ViolatedDirective { get; set; }

        [JsonProperty(PropertyName = "effective-directive")]
        public string EffectiveDirective { get; set; }

        [JsonProperty(PropertyName = "original-policy")]
        public string OriginalPolicy { get; set; }

        [JsonProperty(PropertyName = "blocked-uri")]
        public string BlockedUri { get; set; }

        [JsonProperty(PropertyName = "status-code")]
        public int StatusCode { get; set; }
    }

}

