// Karma configuration
// Generated on Mon Dec 05 2016 12:30:04 GMT-0500 (Eastern Standard Time)

module.exports = function(config) {
  var dependencies = require('./package.json').dependencies;
  var excludedDependencies = [
    'systemjs', 'zone.js', 'font-awesome', 'bootswatch'
  ];
  var configuration = {
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],
    browsers: ['PhantomJS'],
    reporters: ['progress', 'coverage'],

    preprocessors: {
      'appscript/app/**/!(*.spec)+(.js)': ['coverage'],
      'appscript/app/**/*.js': ['sourcemap']
    },

    // Generate json used for remap-istanbul
    coverageReporter: {
      dir: 'report/',
      reporters: [
        { type: 'json', subdir: 'report-json' }
      ]
    },

    files: [
      'node_modules/systemjs/dist/system-polyfills.src.js',
      'appscript/test/test-helpers/setup.js',

      'node_modules/traceur/bin/traceur-runtime.js',
      'node_modules/systemjs/dist/system.src.js',
       // Polyfills
      'node_modules/core-js/client/shim.js',
       // Reflect and Zone.js
      'node_modules/reflect-metadata/Reflect.js',
      'node_modules/zone.js/dist/zone.js',
      'node_modules/zone.js/dist/long-stack-trace-zone.js',
      'node_modules/zone.js/dist/proxy.js',
      'node_modules/zone.js/dist/sync-test.js',
      'node_modules/zone.js/dist/jasmine-patch.js',
      'node_modules/zone.js/dist/async-test.js',
      'node_modules/zone.js/dist/fake-async-test.js',
      
      'systemjs.conf.js',
      'karma-test-shim.js',
      'node_modules/moment/moment.js',
      //'node_modules/ng2-pagination/dist/ng2-pagination-bundle.js',
      { pattern: 'node_modules/ngx-bootstrap/**/*.js', included: false, watched: false },
      { pattern: 'node_modules/ngx-pagination/**/*.js', included: false, watched: false },
      { pattern: 'node_modules/angular2-bootstrap-confirm/**/*.js', included: false, watched: false },
     
      { pattern: 'appscript/app/**/*.js', included: false },
      { pattern: 'appscript/test/test-helpers/*.js', included: false },

      // paths loaded via Angular's component compiler
      // (these paths need to be rewritten, see proxies section)
      { pattern: 'appscript/app/**/*.html', included: false },
      //{ pattern: 'app/**/*.css', included: false },
   
      // paths to support debugging with source maps in dev tools
      { pattern: 'app/**/*.ts', included: false, watched: false },
      { pattern: 'appscript/app/**/*.js.map', included: false, watched: false }
    ],

    // proxied base paths
    proxies: {
      // required for component assests fetched by Angular's compiler
        "/appscript/": "/base/appscript/",
        "/Appscript/": "/base/appsript/",
        "/test/": "/base/appscript/test/",
        "/node_modules/": "/base/node_modules/"
    },

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,
    singleRun: true,
  };

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],

  Object.keys(dependencies).forEach(function(key) {
    if(excludedDependencies.indexOf(key) >= 0) { return; }

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    configuration.files.push({
        pattern: 'node_modules/' + key + '/**/*.js',
        included: false,
        watched: false
    });
  });

  if (process.env.APPVEYOR) {
    configuration.browsers = ['IE'];
    configuration.singleRun = true;
    configuration.browserNoActivityTimeout = 90000; // Note: default value (10000) is not enough
  }

  configuration.browserNoActivityTimeout = 90000;
  config.set(configuration);
}