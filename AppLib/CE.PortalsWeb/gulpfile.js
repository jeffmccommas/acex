﻿/// <binding BeforeBuild='Run:LocalBuild' />
var inlineNg2Template = require('gulp-inline-ng2-template');
var Builder = require('systemjs-builder');
var tsc = require('gulp-typescript');
var tscConfig = require('./tsconfig.json');

var gulp = require('gulp');
var runSeq = require('run-sequence');
var del = require('del');
var buildConfig = require('./gulp.config');
var bundle = require('./tasks/bundle');
var Server = require('karma').Server;
var remapIstanbul = require('remap-istanbul/lib/gulpRemapIstanbul');


var htmlMinifier = require('html-minifier');
var embedTemplates = require('gulp-angular2-embed-templates');
uglify = require('gulp-uglify');

var replace = require('gulp-replace');
gulp.task('copy-jquery-css', function () {
    gulp.src(['node_modules/jquery-ui-dist/jquery-ui.min.css'])
        .pipe(replace('images/ui-icons_444444_256x240.png', '../images/icon-accepted-off.png'))
        .pipe(gulp.dest('./wwwroot/css'));
});

gulp.task('enable-prod', function () {
    gulp.src(['./appscript/app/environment.js'])
        .pipe(replace('production: false', 'production: true'))
        .pipe(gulp.dest('./wwwroot/app'));
});


gulp.task('Run:Build', function (done) {
    runSeq(
   'clean-vendor-js-in-root',
    'clean-vendor-css-in-root',
    'clean-app-in-root',
    'clean-report-in-root',
    'copy-html',
    //'embedtemplates',
    'inline-templates',
    'enable-prod',
    'copy-app',
    'copy-vendor-js-to-wwwroot',
    'copy-vendor-css-to-wwwroot',
    'copy-jquery-css',
    'copy-boostrap-glyphs',
    'copy-report',
    'bundle',
    done);
});

gulp.task('Run:LocalBuild', function (done) {
    runSeq(
   'clean-vendor-js-in-root',
    'clean-vendor-css-in-root',
    'clean-app-in-root',
    'clean-report-in-root',
    'copy-html',
    'copy-localapp',
    'copy-vendor-js-to-wwwroot',
    'copy-vendor-css-to-wwwroot',
    'copy-jquery-css',
    'copy-boostrap-glyphs',
    'copy-report',
    //'bundle',
    done);
});

gulp.task('clean-vendor-js-in-root', function (done) {
    del(buildConfig.rootJsFolder, { force: true }).then(function () {
        done();
    });
});

gulp.task('clean-vendor-css-in-root', function (done) {
    del(buildConfig.rootCssFolder, { force: true }).then(function () {
        done();
    });
});

gulp.task('clean-app-in-root', function (done) {
    del(buildConfig.rootAppFolder, { force: true }).then(function () {
        done();
    });
});

gulp.task('copy-vendor-js-to-wwwroot', function (done) {
    runSeq(
    'copy-angular',
    'copy-rxjs',
    'copy-progress',
    'copy-telerik',
    'copy-bootstrapmodule',
    'copy-allOther',
    'copy-images',
    'copy-moment',
    'copy-ngxpagination',
    'copy-popover',
    'copy-fonts',
    'copy-angular2JWT',
    done);
});

gulp.task('copy-taginput', function () {
    return gulp.src(buildConfig.sources.taginput)
    .pipe(gulp.dest(buildConfig.rootJsFolder + 'angular2-tag-input'));
});

gulp.task('copy-angular2JWT', function () {
    return gulp.src(buildConfig.sources.angularJWT)
    .pipe(gulp.dest(buildConfig.rootJsFolder + 'angular2-jwt'));
});
gulp.task('copy-fonts', function () {
    return gulp.src(buildConfig.sources.fonts)
    .pipe(gulp.dest('./wwwroot/fonts'));
});
gulp.task('copy-angular', function () {
    return gulp.src(buildConfig.sources.angularRC)
    .pipe(gulp.dest(buildConfig.rootJsFolder + '@angular/'));
});

gulp.task('copy-popover', function () {
    return gulp.src(buildConfig.sources.popover)
    .pipe(gulp.dest(buildConfig.rootJsFolder + 'angular-confirmation-popover/'));
});

gulp.task('copy-moment', function () {
    return gulp.src(buildConfig.sources.moment)
    .pipe(gulp.dest(buildConfig.rootJsFolder + 'moment/'));
});


gulp.task('copy-bootstrapmodule', function () {
    return gulp.src(buildConfig.sources.bootstrapmodule)
    .pipe(gulp.dest(buildConfig.rootJsFolder + 'ngx-bootstrap/'));    
});

gulp.task('copy-ngxpagination', function () {
    return gulp.src(buildConfig.sources.ngxPagination)
    .pipe(gulp.dest(buildConfig.rootJsFolder + 'ngx-pagination/'));
});

gulp.task('copy-progress', function () {
    return gulp.src(buildConfig.sources.progress)
    .pipe(gulp.dest(buildConfig.rootJsFolder + '@progress/'));
});
gulp.task('copy-telerik', function () {
    return gulp.src(buildConfig.sources.telerik)
    .pipe(gulp.dest(buildConfig.rootJsFolder + '@telerik/'));
});

gulp.task('copy-html', function () {
    return gulp.src([
       './App/**/*.html',
       './App/**/*.css']
    )
       .pipe(gulp.dest('./appscript/App/'));
});

gulp.task('copy-app', function () {
    return gulp.src([
    './appscript/App/**/*.css'
    ])
    .pipe(gulp.dest('./wwwroot/app/'));
});


gulp.task('copy-localapp', function () {
    return gulp.src([
    './appscript/App/**/*.js',
    './appscript/App/**/*.html',
    './appscript/App/**/*.css'
    ])
    .pipe(gulp.dest('./wwwroot/app/'));
});
gulp.task('copy-rxjs', function () {
    return gulp.src(buildConfig.sources.Rxjs)
    .pipe(gulp.dest(buildConfig.rootJsFolder + 'rxjs/'));
});

gulp.task('copy-allOther', function () {
    return gulp.src(buildConfig.sources.jsFilesInclSourcePaths)
    .pipe(gulp.dest(buildConfig.rootJsFolder));
});
gulp.task('copy-images', function () {
    return gulp.src([
        'lib/css/ajax-loader.gif',
        'lib/images/*.*'

        ]
        
        )
    .pipe(gulp.dest('./wwwroot/images/'));
});
gulp.task('copy-boostrap-glyphs', function () {
    return gulp.src([
        'node_modules/bootstrap/dist/fonts/*.*'

    ]

        )
    .pipe(gulp.dest('./wwwroot/fonts/'));
});

gulp.task('copy-vendor-css-to-wwwroot', function () {
    return gulp.src(buildConfig.sources.cssVendorFiles)
    .pipe(gulp.dest(buildConfig.rootCssFolder));
});

gulp.task('clean-report-in-root', function (done) {
    del('./wwwroot/report/', { force: true }).then(function () {
        done();
    });
});
gulp.task('copy-report', function () {
    return gulp.src(buildConfig.sources.codecoveragereport)
    .pipe(gulp.dest('./wwwroot/report/'));
});


gulp.task('test', ['clean-report', 'unit-test']);

gulp.task('unit-test', function (done) {
    console.log('*****' + __dirname);
    
    new Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, karmaDone).start();

    function karmaDone(exitCode) {
        //console.log('Test Done with exit code: ' + exitCode);
        //remapCoverage();
        if (exitCode === 0) {
            remapCoverage();
            done();
        } else {
            //done('Unit test failed.');
            //done();
        }
    }
});

function remapCoverage() {
    console.log('Remapping coverage to TypeScript format...');
    gulp.src(buildConfig.report + 'report-json/coverage-final.json')
    .pipe(remapIstanbul({
        reports: {
            'lcovonly': buildConfig.report + 'remap/lcov.info',
            'json': buildConfig.report + 'remap/coverage.json',
            'html': buildConfig.report + 'remap/html-report',
            'text-summary': buildConfig.report + 'remap/text-summary.txt'
        }
    }))
    .on('finish', function () {
        console.log('Remapping done! View the result in report/remap/html-report');
    });
}
/* Clean report folder */
gulp.task('clean-report', function () {
    return del([buildConfig.report]);
});

gulp.task('start-watch', function () {
    gulp.watch([
    './App/**/*.js',
    './App/**/*.html',
    './App/**/*.css'
    ], ['copy-app']);
});



gulp.task('bundle', function () {
  
    var builder = new Builder('', './wwwroot/system.config.js');
    /*
   the parameters of the below buildStatic() method are:
       - your transcompiled application boot file (the one wich would contain the bootstrap(MyApp, [PROVIDERS]) function - in my case 'dist/app/boot.js'
       - the output (file into which it would output the bundled code)
       - options {}
*/
    return builder

        .buildStatic('./wwwroot/app/main.js', buildConfig.rootAppFolder + 'main.bundle.js', { minify: true, sourceMaps: false })
        .then(function () {
            console.log('Build complete');
        })
        .catch(function (err) {
            console.log('Build error');
            console.log(err);
        });
});


gulp.task('inline-templates',
    function () {
        return gulp.src(['./appscript/app/**/*.js', '!./appscript/app/environment.js'])
            .pipe(inlineNg2Template({ target: 'es5', useRelativePaths: true, indent: 0, removeLineBreaks: true, templateProcessor: minifyTemplate }))

            //.pipe(tsc(tscConfig.compilerOptions))
            .pipe(uglify())
            .pipe(gulp.dest('./wwwroot/app/'));
    });


gulp.task('embedtemplates', function () {
    gulp.src('./appscript/app/**/*.js')
        .pipe(embedTemplates({
            minimize: {
                quotes: true
            }
        }))
        .pipe(uglify())
        .pipe(gulp.dest('./wwwroot/app/'));
});



function minifyTemplate(path, ext, file, cb) {
    try {
        var minifiedFile = htmlMinifier.minify(file, {
            collapseWhitespace: true,
            caseSensitive: true,
            removeComments: true,
            removeRedundantAttributes: true
        });
        cb(null, minifiedFile);
    }
    catch (err) {
        cb(err);
    }
}