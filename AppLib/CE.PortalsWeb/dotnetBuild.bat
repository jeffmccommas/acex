@echo off
echo *****************************************************
echo Project 1: %1/CE.InsightDataAccess
echo Project 2: %1/CE.Portals.DataService
echo Project 3: %1/CE.Portals.Integrations

dotnet restore %1/CE.InsightDataAccess
dotnet restore %1/CE.Portals.DataService
dotnet restore %1/CE.Portals.Integrations

dotnet pack %1/CE.InsightDataAccess --output %2 --configuration %3
dotnet pack %1/CE.Portals.DataService --output %2 --configuration %3
dotnet pack %1/CE.Portals.Integrations --output %2 --configuration %3

echo *****************************************************
