﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using CE.PortalsWeb.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Net.Http.Headers;


namespace CE.PortalsWeb
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {


            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();


            if (env.IsDevelopment() || env.IsEnvironment("localdev") || env.IsEnvironment("dev"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var duration = Configuration["AppSettings:ApiClientCacheTimeOutSeconds"];

            int reponseCacheDuration = 60; //seconds
            if (!string.IsNullOrEmpty(duration))
            {
                reponseCacheDuration = Convert.ToInt32(duration);
            }

            services.AddMvc(options =>
            {
                options.CacheProfiles.Add("Default",
                    new CacheProfile()
                    {
                        Duration = reponseCacheDuration,
                        Location = ResponseCacheLocation.Client
                    });
                options.CacheProfiles.Add("Never",
                    new CacheProfile()
                    {
                        Location = ResponseCacheLocation.None,
                        NoStore = true
                    });
            }).AddJsonOptions(options =>
                 {
                     options.SerializerSettings.ReferenceLoopHandling =
                         Newtonsoft.Json.ReferenceLoopHandling.Ignore;

                 });

            services.Configure<MvcOptions>(options =>
            {

                options.InputFormatters.OfType<JsonInputFormatter>().First().SupportedMediaTypes.Add(
                        new MediaTypeHeaderValue("application/csp-report")
                    );
                //options.InputFormatters.RemoveType<JsonInputFormatter>();

            });
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.Configure<GzipCompressionProviderOptions>(options => options.Level = System.IO.Compression.CompressionLevel.Optimal);

            services.AddResponseCompression(options =>
            {
                options.MimeTypes = new[]
                {
                    // Default
                    "text/plain",
                    "text/css",
                    "application/javascript",
                    "text/html",
                    "application/xml",
                    "text/xml",
                    "application/json",
                    "text/json",
                    // Custom
                    "image/svg+xml"
                };
            });
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            services.AddMemoryCache();
            services.AddSession();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseResponseCompression();
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddAzureWebAppDiagnostics();

            //app.UseApplicationInsightsRequestTelemetry();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseSession();
            //app.UseApplicationInsightsExceptionTelemetry();
            // Enable CORS
            app.UseCors(config =>
               config.AllowAnyHeader()
                   .AllowAnyMethod()
                   .AllowAnyOrigin());

            app.Use(async (ctx, next) =>
            {
                ctx.Response.Headers.Add("Content-Security-Policy",
                    "script-src 'self' 'unsafe-eval' *.msecnd.net; report-uri /cspreport");
                ctx.Response.Headers.Add("X-Frame-Options", "SAMEORIGIN");
                ctx.Response.Headers.Add("X-Xss-Protection", "1");
                ctx.Response.Headers.Add("X-Content-Type-Options", "nosniff");
                await next();
            });


            app.UseMvc(routes =>
            {

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}");

                routes.MapSpaFallbackRoute("spa-fallback", new { controller = "Home", action = "Index" });
            });

            app.UseDefaultFiles();
            string staticcachedays = Environment.GetEnvironmentVariable("STATIC_BROWSER_CACHE_DAYS");
            if (string.IsNullOrEmpty(staticcachedays))
                staticcachedays = "2";//default to 2 days
            app.UseStaticFiles(new StaticFileOptions()
            {
                OnPrepareResponse =
                    r =>
                    {
                        string path = r.File.PhysicalPath;
                        if (path.EndsWith(".css") || path.EndsWith(".js") || path.EndsWith(".gif") || path.EndsWith(".jpg") || path.EndsWith(".png") || path.EndsWith(".svg"))
                        {
                            TimeSpan maxAge = new TimeSpan(Convert.ToInt32(staticcachedays), 0, 0, 0);
                            r.Context.Response.Headers.Append("Cache-Control", "public,max-age=" + maxAge.TotalSeconds.ToString("0"));
                        }
                    }
            });


        }

    }
}
