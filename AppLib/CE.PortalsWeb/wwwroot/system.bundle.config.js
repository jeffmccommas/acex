﻿/**
 * System configuration for Angular 2 
 * systemjs.config.js provides information to a module loader about where to find application modules, 
 * and registers all the necessary packages.
 */
(function (global) {
    System.config({
        defaultJSExtensions: true,
        paths: {
            // paths serve as alias
            'npm:': 'node_modules/'
        },
        // map tells the System loader where to look for things
        map: {
            // our app is within the app folder
            app: '/app',
            
            
            '@progress': 'npm:@progress',
            '@telerik': 'npm:@telerik',
            // angular bundles
            '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
            '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
            '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
            '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
            '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
            '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
            '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
            'ngx-bootstrap': 'node_modules/ngx-bootstrap',
            'moment': 'node_modules/moment/moment.js',
            'angular-confirmation-popover' : 'node_modules/angular-confirmation-popover/dist/umd/angular-confirmation-popover.js',
          
            'ngx-pagination': 'npm:ngx-pagination',
            'angular2-tag-input': 'node_modules/angular2-tag-input/dist/lib/tag-input.module.js',
            'angular2-jwt': 'node_modules/angular2-jwt/angular2-jwt.js',

            // other libraries
            'rxjs': 'npm:rxjs'
        },
        // packages tells the System loader how to load when no filename and/or no extension
        packages: {

            'ng2-tag-input/modules/components/tag-input.template.html': {
            defaultJSExtension: false
            },

            "@progress/kendo-angular-grid": { "main": "./dist/cdn/js/kendo-angular-grid.js", "defaultExtension": "js" },
            "@progress/kendo-angular-l10n": { "main": "./dist/cdn/js/kendo-angular-l10n.js", "defaultExtension": "js" },
            "@progress/kendo-drawing": { "main": "./dist/cdn/js/kendo-drawing.js", "defaultExtension": "js" },
            "@progress/kendo-angular-intl": { "main": "./dist/cdn/js/kendo-angular-intl.js", "defaultExtension": "js" },
            

            app: {
                main: 'main.bundle.js',
                defaultExtension: 'js'
            },
            rxjs: {
                defaultExtension: 'js'
            },
            'ngx-pagination': { main: './dist/ngx-pagination.umd.js', defaultExtension: 'js' }
            ,
            //'ng2-bootstrap': { format: 'cjs', main: 'bundles/ng2-bootstrap.umd.js', defaultExtension: 'js' },

            'ngx-bootstrap': { format: 'cjs', main: 'bundles/ngx-bootstrap.umd.js', defaultExtension: 'js' },


            'angular2-jwt': { format: 'cjs', main: 'angular2-jwt.js', defaultExtension: 'js' },

            'moment': { main: 'moment.js', defaultExtension: 'js' }
         
        }
    });
})(this);
