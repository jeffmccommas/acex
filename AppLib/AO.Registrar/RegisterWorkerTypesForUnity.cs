﻿using System;
using System.Threading.Tasks;
using AO.Business;
using AO.BusinessContracts;
using CE.AO.Logging;
using CE.AO.Models;
using CE.ContentModel;
using Microsoft.Practices.Unity;
using Moq;

namespace AO.Registrar
{
    internal static class RegisterWorkerTypesForUnity
    {
        public static void RegisterTypes<TLifetime>(IUnityContainer container) where TLifetime : LifetimeManager, new()
        {
            container.RegisterType<ICalculationFacade, CalculationFacade>(new TLifetime());
            container.RegisterType<IEvaluationFacade, EvaluationFacade>(new TLifetime());
            container.RegisterType<INotificationFacade, NotificationFacade>(new TLifetime());
            container.RegisterType<IContentModelFactory, ContentModelFactoryContentful>(new TLifetime());
            container.RegisterType<IClientConfigFacade, ClientConfigFacade>(new TLifetime());
            container.RegisterType<INotification, Notification>(new TLifetime());
            container.RegisterType<ITallAMI, ConsumptionByMeter>(new TLifetime());
            container.RegisterType<IBillingFacade, BillingFacade>(new TLifetime());
            container.RegisterType<IAzureQueue, AzureQueue>(new TLifetime());
            container.RegisterType<ISendSms, SendSms>(new TLifetime());
            container.RegisterType<ISendEmail, SendEmail>(new TLifetime());
            container.RegisterType<IAmiReading, AmiReading>(new TLifetime());
            container.RegisterType<IAccountLookup, AccountLookup>(new TLifetime());
            container.RegisterType<IAccountUpdatesFacade, AccountUpdatesFacade>(new TLifetime());
            container.RegisterType<IAMI, AMI>(new TLifetime());
            container.RegisterType<IReport, Report>(new TLifetime());
            container.RegisterInstance(new Mock<ICalculationFacade>().Object);
            container.RegisterInstance(new Mock<IEvaluationFacade>().Object);
            container.RegisterInstance(new Mock<IReport>().Object);
            container.RegisterInstance(new Mock<INotificationFacade>().Object);
            container.RegisterInstance(new Mock<IAMI>().Object);
            container.RegisterInstance(new Mock<ITallAMI>().Object);
            container.RegisterInstance(new Mock<IAzureQueue>().Object);
            container.RegisterInstance(new Mock<IBillingFacade>().Object);
            container.RegisterInstance(new Mock<IAccountUpdatesFacade>().Object);
            container.RegisterInstance(new Mock<ISendSms>().Object);
            container.RegisterInstance(new Mock<ISendEmail>().Object);
            container.RegisterInstance(GetNotificationMock().Object);
            container.RegisterInstance(GetAmiReadingMock().Object);
            container.RegisterInstance(GetAccountLookupMock().Object);
            container.RegisterInstance(GetClientConfigMock().Object);
        }

        /// <summary>
        /// To Enable Logging
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="unityContainer"></param>
        /// <param name="logModel"></param>
        /// <returns></returns>
        public static T Resolve<T>(this IUnityContainer unityContainer, LogModel logModel)
        {
            return unityContainer.Resolve<T>(new ParameterOverride("logModel", new LogModel()));
        }

        private static Mock<IClientConfigFacade> GetClientConfigMock()
        {
            var clientSetting = new ClientSettings()
            {
                TrumpiaApiKey = "TrApiKey",
                TrumpiaUserName = "TrUserName"
            };
            var iCLientConfigFacade = new Mock<IClientConfigFacade>();
            iCLientConfigFacade.Setup(m => m.GetClientSettings(It.IsAny<int>())).Returns(clientSetting);
            return iCLientConfigFacade;
        }

        private static Mock<IAmiReading> GetAmiReadingMock()
        {
            var nccAmiReadingModel = new NccAmiReadingModel()
            {
                BatteryVoltage = "3.570440054",
                CommodityId = 3,
                CustomerId = "",
                MeterId = "96143503",
                AmiTimeStamp = DateTime.Now,
                ReadingValue = 0,
                Timezone = "PST",
                TransponderId = 46287586,
                TransponderPort = 1,
                UOMId = 3
            };
            var iAmiReading = new Mock<IAmiReading>();
            iAmiReading.Setup(m => m.GetAmiReading(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>())).Returns(Task.FromResult(nccAmiReadingModel));
            return iAmiReading;
        }

        private static Mock<IAccountLookup> GetAccountLookupMock()
        {
            var iAccountLookUpMockObject = new Mock<IAccountLookup>();
            var accountLookupModel = new AccountLookupModel() { AccountId = "" };
            iAccountLookUpMockObject.Setup(m => m.GetAccountMeterDetails(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(accountLookupModel);
            return iAccountLookUpMockObject;
        }

        private static Mock<INotification> GetNotificationMock()
        {
            SmsTemplateModel smsTemplate = new SmsTemplateModel()
            {
                ClientId = 87,
                InsightTypeName = "InsightTypeName",
                Body = "Body-Sample-Data",
                TemplateId = "45"
            };
            var iNotification = new Mock<INotification>();
            iNotification.Setup(m => m.GetSmsTemplateFromDB(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(smsTemplate));
            iNotification.Setup(m => m.GetReplacedSmsTemplateForSubscriptionDoubleOptInConfirmation(It.IsAny<string>(), It.IsAny<SubscriptionModel>(), It.IsAny<ClientSettings>(), It.IsAny<CustomerModel>(), It.IsAny<System.Text.RegularExpressions.MatchCollection>()))
              .Returns("str");

            return iNotification;
        }
    }
}
