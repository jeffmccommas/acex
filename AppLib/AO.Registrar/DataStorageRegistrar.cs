﻿using System;
using System.Configuration;
using AO.Business;
using AO.DataAccess;
using AO.Entities;
using Cassandra.Mapping;
using Microsoft.Azure;
using Microsoft.Practices.Unity;
using StaticConfig = AO.DataAccess.StaticConfig;

namespace AO.Registrar
{
    /// <summary>
    /// Geneirc Dependency Injection Registrar for Aclara One Data Storage verticals.  Ultimately, we may need to introduce various more fine grained 
    /// registrars per execution container, but this is a start.
    /// </summary>

    public class DataStorageRegistrar
    {
        private static bool CassandraMappingsInitialized { get; set; }

        private static bool CassandraSessionInitialized { get; set; }

        /// <summary>
        /// Will Initialize dependency registrations and return a constructed IUnityContainer.
        /// </summary>
        /// <typeparam name="TLifetime"></typeparam>
        public IUnityContainer Initialize<TLifetime>() where TLifetime : LifetimeManager, new()
        {
            var container = new UnityContainer();

            Initialize<TLifetime>(container);

            return container;
        }

        /// <summary>
        /// Initialize DI registrations.
        /// </summary>
        /// <typeparam name="TLifetime"></typeparam>
        /// <param name="container"></param>
        public void Initialize<TLifetime>(IUnityContainer container) where TLifetime : LifetimeManager, new()
        {
            //var storageType = CloudConfigurationManager.GetSetting("DataStorage");
            //var storageConnectionSting = CloudConfigurationManager.GetSetting(StaticConfig.AzureStorageConnectionString);
            var storageType = ConfigurationManager.AppSettings.Get("DataStorage");
            var storageConnectionSting = ConfigurationManager.AppSettings.Get("AzureStorageConnectionString");
            if (storageType.Equals("cassandra", StringComparison.CurrentCultureIgnoreCase))
            {
                AutoMapperConfig.AutoMapperMappings();
				
				//creates azure queues if does not exist                
				if (!string.IsNullOrWhiteSpace(storageConnectionSting))
                    CE.AO.Entities.Init.AzureQueueInit().Wait();

                InitializeCassandraMappings();

                InitializeCassandraSession();

                RegisterCassandraTypesForUnity.RegisterTypes<TLifetime>(container);
            }
            else if (storageType.Equals("azuretables", StringComparison.CurrentCultureIgnoreCase))
            {
                CE.AO.Business.AutoMapperConfig.AutoMapperMappings();
                //table not exist then it will create
                CE.AO.Business.Init.AzureTableInit().Wait();
                //creates azure queues if does not exist                
                if (!string.IsNullOrWhiteSpace(storageConnectionSting))
                    CE.AO.Entities.Init.AzureQueueInit().Wait();

                RegisterAzureTableTypesForUnity.RegisterTypes<TLifetime>(container);
            }
            else
            {
                throw new ApplicationException("Unexpected storage type.");
            }
        }

        /// <summary>
        /// Make sure Cassandra Mappings are only initiated once per App Domain.
        /// </summary>
        private void InitializeCassandraMappings()
        {
            if (!CassandraMappingsInitialized)
            {
                MappingConfiguration.Global.Define<CassandraMapperConfig>();
                CassandraMappingsInitialized = true;
            }
        }

        /// <summary>
        /// Make sure Cassandra Session is created once per App Domain.
        /// </summary>
        private void InitializeCassandraSession()
        {
            if (CassandraSessionInitialized) return;
            var nodesValues = System.Configuration.ConfigurationManager.AppSettings.Get(StaticConfig.CassandraNodes);
            if (nodesValues != null)
                CassandraSession.GetSession();
            CassandraSessionInitialized = true;
        }
    }
}
