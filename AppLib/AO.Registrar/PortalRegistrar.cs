﻿using AO.Business;
using AO.BusinessContracts;
using AO.DataAccess;
using AO.DataAccess.EF;
using AO.DataAccess.Contracts;
using CE.ContentModel;
using Microsoft.Practices.Unity;

namespace AO.Registrar
{
    /// <summary>
    /// Generic Dependency Injection Registrar for Aclara One Data Storage verticals.  Ultimately, we may need to introduce various more fine grained 
    /// registrars per execution container, but this is a start.
    /// </summary>
    public class PortalRegistrar
    {
        /// <summary>
        /// Initialize DI registrations.
        /// </summary>
        /// <typeparam name="TLifetime"></typeparam>
        /// <param name="container"></param>
        public void Initialize<TLifetime>(IUnityContainer container) where TLifetime : LifetimeManager, new()
        {
            container.RegisterType<IContentModelFactory, ContentModelFactoryContentful>(new TLifetime());
            container.RegisterType<IClientConfigFacade, ClientConfigFacade>(new TLifetime());
            container.RegisterType<IIdentityFacade, IdentityFacade>(new TLifetime());
            container.RegisterType<ITenantRepository, TenantRepository>(new TLifetime());
            container.RegisterType<IAoDb, AoDb>(new TLifetime());
        }
    }
}
