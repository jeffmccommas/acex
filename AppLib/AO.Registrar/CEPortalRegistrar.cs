﻿using AO.Business;
using AO.BusinessContracts;
using AO.DataAccess;
using AO.DataAccess.Contracts;
using AO.Entities;
using Cassandra.Mapping;
using CE.AO.Business;
using CE.AO.DataAccess;
using CE.ContentModel;
using Microsoft.Practices.Unity;
using AutoMapperConfig = AO.Business.AutoMapperConfig;
using StaticConfig = AO.DataAccess.StaticConfig;

namespace AO.Registrar
{
    // ReSharper disable once InconsistentNaming
    /// <summary>
    /// Generic Dependency Injection Registrar for Legacy verticals.  Ultimately, we may need to introduce various more fine grained 
    /// registrars per execution container, but this is a start.
    /// </summary>
    public class CEPortalRegistrar
    {
		private static bool CassandraMappingsInitialized { get; set; }

		private static bool CassandraSessionInitialized { get; set; }

		/// <summary>
		/// Initialize DI registrations.
		/// </summary>
		/// <typeparam name="TLifetime"></typeparam>
		/// <param name="container"></param>
		public void Initialize<TLifetime>(IUnityContainer container) where TLifetime : LifetimeManager, new()
        {
			AutoMapperConfig.AutoMapperMappings();
			CE.AO.Business.AutoMapperConfig.AutoMapperMappings();

			InitializeCassandraMappings();

			InitializeCassandraSession();

			container.RegisterType<ICassandraRepository, CassandraRepository>(new TLifetime());
			container.RegisterType<ITableRepository, TableRepository>(new TLifetime());
            container.RegisterType<IPremise, Business.Premise>(new TLifetime());
            container.RegisterType<IAmi, Ami>(new TLifetime());
            container.RegisterType<ITallAMI, ConsumptionByMeter>(new TLifetime());
            container.RegisterType<IBillingCycleSchedule, Business.BillingCycleSchedule>(new TLifetime());
            container.RegisterType<IBilling, Business.Billing>(new TLifetime());
            container.RegisterType<IClientWidgetConfiguration, ClientWidgetConfiguration>(new TLifetime());
            container.RegisterType<ISendEmail, Business.SendEmail>(new TLifetime());
            container.RegisterType<ICustomer, Business.Customer>(new TLifetime());
			container.RegisterType<IClientConfigFacade, ClientConfigFacade>(new TLifetime());
			container.RegisterType<IContentModelFactory, ContentModelFactoryContentful>(new TransientLifetimeManager());
		}


		/// <summary>
		/// Make sure Cassandra Mappings are only initiated once per App Domain.
		/// </summary>
		private void InitializeCassandraMappings()
		{
			if (!CassandraMappingsInitialized)
			{
				MappingConfiguration.Global.Define<CassandraMapperConfig>();
				CassandraMappingsInitialized = true;
			}
		}

		/// <summary>
		/// Make sure Cassandra Session is created once per App Domain.
		/// </summary>
		private void InitializeCassandraSession()
		{
			if (CassandraSessionInitialized) return;
			var nodesValues = System.Configuration.ConfigurationManager.AppSettings.Get(StaticConfig.CassandraNodes);
			if (nodesValues != null)
				CassandraSession.GetSession();
			CassandraSessionInitialized = true;
		}
	}
}
