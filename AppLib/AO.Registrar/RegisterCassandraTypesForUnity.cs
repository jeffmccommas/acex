﻿using AO.Business;
using AO.BusinessContracts;
using AO.BusinessContracts.EventProcessesContracts;
using AO.DataAccess;
using AO.DataAccess.EF;
using AO.DataAccess.Contracts;
using AO.EventProcessors;
using AO.EventProcessors.EventProcessor;
using CE.AO.DataAccess;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Utilities;
using CE.BillToDate;
using CE.ContentModel;
using Microsoft.Practices.Unity;
using Microsoft.ServiceBus.Messaging;

namespace AO.Registrar
{
    public static class RegisterCassandraTypesForUnity
    {
        public static void RegisterTypes<TLifetime>(IUnityContainer container) where TLifetime : LifetimeManager, new()
        {
            container.RegisterType<ITableRepository, TableRepository>(new TLifetime());
            container.RegisterType<IQueueManager, QueueManager>(new TLifetime());
            container.RegisterType<ICassandraRepository, CassandraRepository>(new TLifetime());
            container.RegisterType<IOdbcRepository, OdbcRepository>(new TLifetime());

            container.RegisterType<IAzureQueue, AzureQueue>(new TLifetime());

            container.RegisterType<IAccountLookup, AccountLookup>(new TLifetime());

            container.RegisterType<IAccountUpdates, AccountUpdates>(new TLifetime());

            container.RegisterType<IAccountUpdatesFacade, AccountUpdatesFacade>(new TLifetime());

            container.RegisterType<IAmiReading, AmiReading>(new TLifetime());

            //Added Processing Args to Registrat to be used to access settings form Custom Config. 
            container.RegisterType<IProcessingArgs, ProcessingArgs>(new TLifetime());

            container.RegisterType<ITallAMI, ConsumptionByMeter>(new TLifetime());

            container.RegisterType<IMeterHistoryByMeter, MeterHistoryByMeter>(new TLifetime());

            container.RegisterType<IBilling, Billing>(new TLifetime());

            container.RegisterType<IBillingFacade, BillingFacade>(new TLifetime());

            container.RegisterType<IBillingCycleSchedule, BillingCycleSchedule>(new TLifetime());

            container.RegisterType<ICalculation, Calculation>(new TLifetime());

            container.RegisterType<ICalculationFacade, CalculationFacade>(new TLifetime());

            container.RegisterType<ICustomer, Customer>(new TLifetime());

            container.RegisterType<IClientAccount, ClientAccount>(new TLifetime());

            container.RegisterType<IEvaluation, Evaluation>(new TLifetime());

            container.RegisterType<IEvaluationFacade, EvaluationFacade>(new TLifetime());

            container.RegisterType<ISubscription, Subscription>(new TLifetime());

            container.RegisterType<ISubscriptionFacade, SubscriptionFacade>(new TLifetime());

            container.RegisterType<IPremise, Premise>(new TLifetime());

            container.RegisterType<ITrumpiaRequestDetail, TrumpiaRequestDetail>(new TLifetime());

            container.RegisterType<ICustomerMobileNumberLookup, CustomerMobileNumberLookup>(new TLifetime());

            container.RegisterType<INotification, Notification>(new TLifetime());

            container.RegisterType<INotificationFacade, NotificationFacade>(new TLifetime());

            container.RegisterType<IReport, Report>(new TLifetime());

            container.RegisterType<ITrumpiaKeywordConfiguration, TrumpiaKeywordConfiguration>(new TLifetime());

            container.RegisterType<IMeterAccountMtu, MeterAccountMtu>(new TLifetime());

            container.RegisterType<IMtuTypeMapping, MtuTypeMapping>(new TLifetime());

            container.RegisterType<ISendEmail, SendEmail>(new TLifetime());

            container.RegisterType<ISendSms, SendSms>(new TLifetime());

            container.RegisterType<IContentModelFactory, ContentModelFactoryContentful>(new TransientLifetimeManager());

            container.RegisterType<IClientConfigFacade, ClientConfigFacade>(new TLifetime());

            container.RegisterType<ICalculationManagerFactory, CalculationManagerFactory>(new TLifetime());

            container.RegisterType<IMeterReadFacade, MeterReadFacade>(new TLifetime());

            container.RegisterType<IConsumptionByAccount, ConsumptionByAccount>(new TLifetime());

            container.RegisterType<IDcuAlarm, DcuAlarm>(new TLifetime());

            container.RegisterType<IDcuAlarmHistory, DcuAlarmHistory>(new TLifetime());

            container.RegisterType<IDcuAlarmCode, DcuAlarmCode>(new TLifetime());

            container.RegisterType<IDcuAlarmCodeHistory, DcuAlarmCodeHistory>(new TLifetime());

            container.RegisterType<IDcuAlarmType, DcuAlarmType>(new TLifetime());

            container.RegisterType<IDcuAlarmFacade, DcuAlarmFacade>(new TLifetime());

            container.RegisterType<IDcuProvisioning, DcuProvisioning>(new TLifetime());

            container.RegisterType<IDcuCallData, DcuCallData>(new TLifetime());

            container.RegisterType<IAlarmFacade, AlarmFacade>(new TLifetime());

            container.RegisterType<IAlarm, Alarm>(new TLifetime());

            container.RegisterType<IAlarmTypes, AlarmType>(new TLifetime());

            container.RegisterType<IExportFacade, ExportFacade>(new TLifetime());

            container.RegisterType<ICacheProvider, MemoryCacheProvider>(new TLifetime());

            container.RegisterType<IMeterType, MeterType>(new TLifetime());

            container.RegisterType<IMeterAccountMtuFacade, MeterAccountMtuFacade>(new TLifetime());

            container.RegisterType<IPreAggCalculationFacade, PreAggCalculationFacade>(new TLifetime());

            container.RegisterType<IConsumptionAggByAccount, ConsumptionAggByAccount>(new TLifetime());

            container.RegisterInstance<ITimingMetricsCalculator>(new TimingMetricsCalculator(), new ContainerControlledLifetimeManager());

            container.RegisterType<IEmailRequestDetail, EmailRequestDetail>(new TLifetime());

            container.RegisterType<IOperationsFacade, OperationsFacade>(new TLifetime());

            container.RegisterType<ITenantFacade, TenantFacade>(new TLifetime());

            container.RegisterType<ITenantRepository, TenantRepository>(new TLifetime());

            container.RegisterType<IAoDb, AoDb>(new TLifetime());

            container.RegisterType<ILogRepository, LogRepository>(new TLifetime());

            container.RegisterType<INotificationRepository, NotificationRepository>(new TLifetime());

            container.RegisterInstance<IEventProcessorFactory>(new EventProcessorFactory<IEventProcessor>(), new ContainerControlledLifetimeManager());

            container.RegisterType<ITriggerSubscriber, EventHubLogToCassandraTriggerSubscriber>("LogToCassandraTriggerSubscriber", new TLifetime());

            //container.RegisterType<ITriggerSubscriber, EventHubPreAggCalculationTriggerSubscriber>("PreAggCalculationTriggerSubscriber", new TLifetime());

            container.RegisterType<ITriggerSubscriber, EventHubEventProcessTriggerSubscriber>("EventProcessTriggerSubscriber", new TLifetime());

			//container.RegisterType<ITriggerSubscriber, EventHubValidationEstimationTriggerSubscriber>("ValidationEstimationTriggerSubscriber", new TLifetime());

			//container.RegisterType<IVeeStagedClientMeterByDateRepository, VeeStagedClientMeterByDateRepository>(new TLifetime());

			//container.RegisterType<IVeeFacade, VeeFacade>(new TLifetime());

			#region Registration for EventProcesors

			container.RegisterType<IProcessEvent, DcuAlarmProcessEvent>("DcuAlarmProcessEvent", new TLifetime());
            container.RegisterType<IProcessEvent, AccountUpdatesProcessEvent>("AccountUpdatesProcessEvent", new TLifetime());
            container.RegisterType<IProcessEvent, BillingProcessEvent>("BillingProcessEvent", new TLifetime());
            container.RegisterType<IProcessEvent, DcuCallDataProcessEvent>("DcuCallDataProcessEvent", new TLifetime());
            container.RegisterType<IProcessEvent, DcuProvisioningProcessEvent>("DcuProvisioningProcessEvent", new TLifetime());
            container.RegisterType<IProcessEvent, MeterAccountMtuProcessEvent>("MeterAccountMtuProcessEvent", new TLifetime());
            //container.RegisterType<IProcessEvent, MeterReadProcessEvent>("MeterReadProcessEvent", new TLifetime());
            container.RegisterType<IProcessEvent, MeterReadTriggerCalculationProcessEvent>("MeterReadTriggerCalculationProcessEvent", new TLifetime());
            container.RegisterType<IProcessEvent, NccAmiCalculationProcessEvent>("NccAmiCalculationProcessEvent", new TLifetime());
            container.RegisterType<IProcessEvent, NccAmiConsumptionProcessEvent>("NccAmiConsumptionProcessEvent", new TLifetime());
            container.RegisterType<IProcessEvent, NccAmiReadingProcessEvent>("NccAmiReadingProcessEvent", new TLifetime());
            container.RegisterType<IProcessEvent, SubscriptionProcessEvent>("SubscriptionProcessEvent", new TLifetime());
            container.RegisterType<IProcessEvent, AlarmProcessEvent>("AlarmProcessEvent", new TLifetime());
			
            // Interval specific AMI reading registrations.
            container.RegisterType<IProcessEvent, AmiProcessEvent>("AmiProcessEvent", new TLifetime()); // The master.

            container.RegisterType<IAmiProcessEvent, Ami15MinuteIntervalProcessEvent>("Ami15MinuteIntervalProcessEvent", new TLifetime());
            container.RegisterType<IAmiProcessEvent, Ami30MinuteIntervalProcessEvent>("Ami30MinuteIntervalProcessEvent", new TLifetime());
            container.RegisterType<IAmiProcessEvent, Ami60MinuteIntervalProcessEvent>("Ami60MinuteIntervalProcessEvent", new TLifetime());
            container.RegisterType<IAmiProcessEvent, AmiDailyIntervalProcessEvent>("AmiDailyIntervalProcessEvent", new TLifetime());
            container.RegisterType<IAmiProcessEventHelper, AmiProcessEventHelper>(new TLifetime());
			
			#endregion

			container.RegisterType<IAoLogger, AoLogger>(new TLifetime());                                  
            
            container.RegisterType<IProcessTrigger<AccountPreAggCalculationRequestModel>, EventHubPreAggCalculationTrigger>(new TLifetime());

            //container.RegisterType<IProcessTrigger<VeeValidationEstimationTriggerDto>, ValidationEstimationEventHubTrigger>(new TLifetime());

            container.RegisterType<IUomConversion, UomConversion>(new TLifetime());

            //container.RegisterType<IVeeFacade, VeeFacade>(new TLifetime());

            //container.RegisterType<IVeeStagedClientMeterByDateRepository, VeeStagedClientMeterByDateRepository>(new TLifetime());

        }

        /// <summary>
        /// To Enable Logging
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="unityContainer"></param>
        /// <param name="logModel"></param>
        /// <returns></returns>
        public static T Resolve<T>(this IUnityContainer unityContainer, LogModel logModel)
        {
            return unityContainer.Resolve<T>(new ParameterOverride("logModel", new LogModel()));
        }
    }
}