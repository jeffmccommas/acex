﻿using AO.BusinessContracts;
using CE.AO.Business;
using CE.AO.DataAccess;
using CE.AO.Models;
using CE.BillToDate;
using CE.ContentModel;
using Microsoft.Practices.Unity;

namespace AO.Registrar
{
    internal static class RegisterAzureTableTypesForUnity
    {
        public static void RegisterTypes<TLifetime>(IUnityContainer container) where TLifetime : LifetimeManager, new()
        {
            container.RegisterType<ITableRepository, TableRepository>(new TLifetime());
            container.RegisterType<IQueueRepository, QueueRepository>(new TLifetime());

            container.RegisterType<IAzureQueue, AzureQueue>(new TLifetime());

            //container.RegisterType<IAccountLookup, AccountLookup>(new TLifetime());

            //container.RegisterType<IAccountUpdates, AccountUpdates>(new TLifetime());

            //container.RegisterType<IAccountUpdatesFacade, AccountUpdatesFacade>(new TLifetime());

            //container.RegisterType<IAmi, AMI>(new TLifetime());

            //container.RegisterType<IAmiReading, AmiReading>(new TLifetime());

            //container.RegisterType<ITallAMI, TallAMI>(new TLifetime());

            //container.RegisterType<IBilling, Billing>(new TLifetime());

            //container.RegisterType<IBillingCycleSchedule, BillingCycleSchedule>(new TLifetime());

            //container.RegisterType<ICalculation, Calculation>(new TLifetime());

            //container.RegisterType<ICalculationFacade, CalculationFacade>(new TLifetime());

            //container.RegisterType<ICustomer, Customer>(new TLifetime());

            //container.RegisterType<IClientAccount, ClientAccount>(new TLifetime());

            //container.RegisterType<IEvaluation, Evaluation>(new TLifetime());

            //container.RegisterType<IEvaluationFacade, EvaluationFacade>(new TLifetime());

            //container.RegisterType<ISubscription, Subscription>(new TLifetime());

            //container.RegisterType<ISubscriptionFacade, SubscriptionFacade>(new TLifetime());

            //container.RegisterType<IPremise, Premise>(new TLifetime());

            container.RegisterType<IClientWidgetConfiguration, ClientWidgetConfiguration>(new TLifetime());

            //container.RegisterType<ITrumpiaRequestDetail, TrumpiaRequestDetail>(new TLifetime());

            //container.RegisterType<ICustomerMobileNumberLookup, CustomerMobileNumberLookup>(new TLifetime());

            //container.RegisterType<INotification, Notification>(new TLifetime());

            //container.RegisterType<IReport, Report>(new TLifetime());

            //container.RegisterType<ISendEmail, SendEmail>(new TLifetime());

            //container.RegisterType<ISendSms, SendSms>(new TLifetime());

            container.RegisterType<IContentModelFactory, ContentModelFactoryContentful>(new TransientLifetimeManager());
            //container.RegisterType<IEmailRequestDetail, EmailRequestDetail>();
            //container.RegisterType<ICustomerMobileNumberLookup, CustomerMobileNumberLookup>();

            //container.RegisterType<ICalculationManagerFactory, CalculationManagerFactory>(new TLifetime());

            //container.RegisterType<IExportFacade, Business.ExportFacade>(new TLifetime());
        }

        /// <summary>
        /// To Enable Logging
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="unityContainer"></param>
        /// <param name="logModel"></param>
        /// <returns></returns>
        public static T Resolve<T>(this IUnityContainer unityContainer, LogModel logModel)
        {
            return unityContainer.Resolve<T>(new ParameterOverride("logModel", new LogModel()));

        }
    }
}
