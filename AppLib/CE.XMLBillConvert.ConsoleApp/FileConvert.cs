﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Configuration;
using NLog;

namespace CE.XMLBillConvert.ConsoleApp
{
    public class FileConvert
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger(); 
        private List<string> _tsvLines;
        private string _del = "\t";
        FileHeader _header;


        public FileConvert()
        {
            _tsvLines = new List<string>();
            _header = new FileHeader();
            _logger.Info("Adding header for output.");

            // Add the header to be written to the output file
            _tsvLines.Add(String.Join(_del, _header.HeaderValues));

        }


        public List<string> Convert(System.IO.TextReader file, string clientId)
        {
            int counter = 1;

           
            try
            { 
                #region get xml from cell of input csv file
                string xmlString = file.ReadToEnd();
                string editXmlString = xmlString.Replace(" xmlns=\"Nexus:BillAnalyzer\"", string.Empty);
                XmlDocument doc = new XmlDocument();
            

                doc.LoadXml(editXmlString);

                if (string.IsNullOrEmpty(doc.InnerXml))
                {
                    throw new Exception("Error loading XML document.");
                }
                _logger.Info("Successfully loaded XML. Iterating through nodes");
                #endregion

                #region iterate through xml nodes and generate tsv lines
             
                XmlNodeList nl = doc.SelectNodes("Accounts");
                XmlNode accountsNode = nl[0];
                foreach (XmlNode accountNode in accountsNode.ChildNodes) {
                    AccountMapper accountMapper = new AccountMapper(accountNode,clientId);

                    XmlNode actIndicatorNode = accountNode.SelectSingleNode("AccountIndicator");
                    AccountIndicatorMapper accountIndicatorMapper = new AccountIndicatorMapper(actIndicatorNode);


                    XmlNodeList billnodes = accountNode.SelectNodes("Bill");
                    BillNodesMapper billNodesMapper = new BillNodesMapper();
                    foreach( XmlNode billNode in billnodes){
                        foreach (XmlAttribute attr in billNode.Attributes)
                        {
                            billNodesMapper.AddBillNodeAttribute(attr.Value, attr.Name);
                        }
                        XmlNodeList billCharges = billNode.SelectNodes("Bill_Charge");
                        BillChargeNodeMapper billChargeNodeMapper = new BillChargeNodeMapper(billCharges);
                        

                        // get child node premise
                        XmlNode premiseNode = billNode.SelectSingleNode("Premise");
                        if (premiseNode == null)
                        {
                            throw new Exception("Missing premise node.");
                        }
                        else
                        {
                            PremiseMapper premiseMapper = new PremiseMapper(premiseNode);

                            XmlNodeList serviceNodes = premiseNode.SelectNodes("Service");

                            foreach (XmlNode serviceNode in serviceNodes) {
                                ServiceMapper serviceMapper = new ServiceMapper(serviceNode);
                                
                                XmlNodeList serviceCharges = serviceNode.SelectNodes("Service_Charge");
                                ServiceChargeMapper serviceChargeMapper = new ServiceChargeMapper(serviceCharges);
                                

                                XmlNode meterNode = serviceNode.SelectSingleNode("Meter");
                                if (meterNode != null)
                                {
                                    MeterMapper meterMapper = new MeterMapper(meterNode);

                                    XmlNode readIntervalNode = meterNode.SelectSingleNode("ReadInterval");

                                    ReadIntervalMapper readIntervalMapper = new ReadIntervalMapper(readIntervalNode);
                                   
                                    XmlNode useDetailNode = readIntervalNode.SelectSingleNode("UseDetail");
                                    UseDetailMapper useDetailMapper = new UseDetailMapper(useDetailNode);
                                    

                                    XmlNodeList amountDetails = useDetailNode.SelectNodes("AmountDetail");
                                    AmountDetailMapper amountDetailsMapper = new AmountDetailMapper(amountDetails);
                                    List<IBillMapper> mappers = new List<IBillMapper>();
                                    mappers.Add(accountMapper);
                                    mappers.Add(accountIndicatorMapper);
                                    mappers.Add(billChargeNodeMapper);
                                    mappers.Add(billNodesMapper);
                                    mappers.Add(premiseMapper);
                                    mappers.Add(serviceMapper);
                                    mappers.Add(serviceChargeMapper);
                                    mappers.Add(meterMapper);
                                    mappers.Add(readIntervalMapper);
                                    mappers.Add(useDetailMapper);
                                    mappers.Add(amountDetailsMapper);

                                    StoreLine(mappers);
                                       
                                    
                                }
                            }
                        }
                    }

                }
                #endregion

                counter++;

                return _tsvLines;

            }
            catch (Exception ex)
            {
                
                _logger.Error(ex);
                throw;
                

            }
            finally
            {

                file.Close();
  
            }
            
        }

        private void StoreLine(List<IBillMapper> mappedObjects)
        {
            
            string[] fileLine = new string[_header.HeaderValues.Length];
            foreach(IBillMapper mappedObject in mappedObjects)
            {
                foreach(MappedString mappedString in mappedObject.MappedStrings)
                {
                    int index = Array.IndexOf(_header.HeaderValues, mappedString.Header);
                    if(index == -1)
                    {
                        throw new Exception("Could not find match for mapped header.");
                    }
                    fileLine[index] = mappedString.Value;
                }
            }
            _tsvLines.Add(String.Join(_del,fileLine));
            
        }

    }
}
