﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.XMLBillConvert.ConsoleApp
{
    
    public class FileHeader
    {
        public string[] HeaderValues { get; set; }
        public FileHeader()
        {
            // TODO: make this configurable
            HeaderValues = new string[] {"customer_id",
                  "premise_id",
                  "mail_address_line_1",
                  "mail_address_line_2",
                  "mail_address_line_3",
                  "mail_city",
                  "mail_state",
                  "mail_zip_code",
                  "first_name",
                  "last_name",
                  "phone_1",
                  "phone_2",
                  "email",
                  "customer_type",
                  "account_id",
                  "active_date",
                  "inactive_date",
                  "read_cycle",
                  "rate_code",// RateClass? in ReadInterval
                  "service_point_id",
                  "service_house_number",
                  "service_street_name",
                  "service_unit",
                  "service_city",
                  "service_state",
                  "service_zip_code",
                  "meter_type",
                  "meter_units",
                  "bldg_sq_foot",
                  "year_built",
                  "bedrooms",
                  "assess_value",
                  "usage_value",
                  "bill_enddate",
                  "bill_days",
                  "is_estimate",
                  "usage_charge",
                  "clientId",
                  "programs",
                  "service_commodity",
                  "account_structure_type",
                  "meter_id",
                  "billperiod_type",
                  "meter_replaces_meterid",
                  "service_read_date",
                  "BudgetBillingStatus",
                  "LastPaymentAmount",
                  "LastPaymentDate",
                  "TotalAmount",
                  "PastDue",
                  "PaymentType",
                  "DueDate",
                  "BillMonth",
                  "BillDate",
                  "BillCycle",
                  "Bill_OTHR_OTHR",
                  "Bill_OTHR_OTHR_DESC",
                  "Bill_OTHR_OTHR2",
                  "Bill_OTHR_OTHR2_DESC",
                  "Bill_OTHR_OTHR3",
                  "Bill_OTHR_OTHR3_DESC",
                  "Bill_OTHR_OTHR4",
                  "Bill_OTHR_OTHR4_DESC",
                  "Bill_OTHR_OTHR5",
                  "Bill_OTHR_OTHR5_DESC",
                  "Bill_OTHR_OTHR6",
                  "Bill_OTHR_OTHR6_DESC",
                  "Bill_OTHR_OTHR7",
                  "Bill_OTHR_OTHR7_DESC",
                  "Bill_OTHR_OTHR8",
                  "Bill_OTHR_OTHR8_DESC",
                  "Bill_OTHR_OTHR9",
                  "Bill_OTHR_OTHR9_DESC",
                  "Bill_OTHR_OTHR10",
                  "Bill_OTHR_OTHR10_DESC",
                  "Bill_OTHR_OTHR11",
                  "Bill_OTHR_OTHR11_DESC",
                  "Bill_OTHR_OTHR12",
                  "Bill_OTHR_OTHR12_DESC",
                  "Bill_OTHR_OTHR13",
                  "Bill_OTHR_OTHR13_DESC",
                  "Bill_OTHR_OTHR14",
                  "Bill_OTHR_OTHR14_DESC",
                  "Bill_OTHR_OTHR15",
                  "Bill_OTHR_OTHR15_DESC",
                  "Bill_OTHR_OTHR16",
                  "Bill_OTHR_OTHR16_DESC",
                  "Bill_OTHR_OTHR17",
                  "Bill_OTHR_OTHR17_DESC",
                  "Bill_OTHR_OTHR18",
                  "Bill_OTHR_OTHR18_DESC",
                  "Bill_OTHR_OTHR19",
                  "Bill_OTHR_OTHR19_DESC",
                  "Bill_OTHR_OTHR20",
                  "Bill_OTHR_OTHR20_DESC",
                  "PremiseDescription",
                  "ReadCycle",
                  "RetailSupply",
                  "NextReaddate",
                  "MeterDescription",
                  "MeterLocation",
                  "MeterMultiplier",
                  "MeterType",
                  "MeterTwoWay",
                  "STD_ENGY",
                  "DIST_ENGY",
                  "SUPP_ENGY",
                  "TOTAL",
                  "TOTAL_UOM",
                  "TOTAL_START_READ",
                  "TOTAL_END_READ",
                  "TOTAL_READ_TYPE",
                  "GOVT_OTHR",
                  "GOVT_OTHR_DESC",
                  "STD_CUST",
                  "STD_CUST_DESC",
                  "Service_OTHR_OTHR",
                  "Service_OTHR_OTHR_DESC",
                  "PTR_REBATE",
                  "TOTAL_GENERATED" };
        }
    }
}
