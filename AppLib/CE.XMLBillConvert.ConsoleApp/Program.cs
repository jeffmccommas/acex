﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace CE.XMLBillConvert.ConsoleApp
{
    class Program
    {

        private static Logger _logger = LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            _logger.Info("Starting conversion.");
            string inPath = Properties.AppSettings.Default.inputFilePath;
            string outPath = Properties.AppSettings.Default.outputFilePath;
            string archivePath = Properties.AppSettings.Default.archiveFilePath;

            string completePath = Path.Combine(inPath, "Completed");
            string unzipped = Path.Combine(inPath, "Unzipped");

            string[] files = Directory.GetFiles(inPath, "*.zip");
            try
            {
                foreach (string filePath in files)
                {
                    using (System.IO.Compression.ZipArchive archive = ZipFile.OpenRead(filePath))
                    {
                        _logger.Info("Unzipping {0}.", filePath);
                        foreach (ZipArchiveEntry entry in archive.Entries)
                        {
                            entry.ExtractToFile(Path.Combine(unzipped, entry.FullName));
                        }
                    }
                    string[] archiveFiles = Directory.GetFiles(unzipped, "*.xml");
                    foreach (string archiveFilePath in archiveFiles)
                    {
                        _logger.Info("Beggining conversion of {0}", archiveFilePath);
                        RunFileConvert(archiveFilePath, outPath);
                    }

                    File.Move(filePath, Path.Combine(archivePath, Path.GetFileName(filePath)));

                }
            } catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        static void RunFileConvert(string archiveFilePath, string outPath)
        {
            // if the file is named properly, it will begin with the clientid
            string clientId = Path.GetFileNameWithoutExtension(archiveFilePath).Split('_')[0];
            _logger.Info("Client Id from file name: {0}", clientId);

            try
            {
                // create file stream here
                FileConvert converter = new FileConvert();
                System.IO.StreamReader file =
                new System.IO.StreamReader(archiveFilePath);
                // pass in file stream here
                List<string> fileLines = converter.Convert(file, clientId);
                string outputFilePath = Path.Combine(outPath, System.IO.Path.GetFileNameWithoutExtension(archiveFilePath) + ".tab");
                System.IO.File.WriteAllLines(outputFilePath, fileLines);
                _logger.Info("Wrote output file {0}.", outputFilePath);
                _logger.Info("Conversion of {0} complete. Removing file.", archiveFilePath);
                //converter.Write();
                File.Delete(archiveFilePath);
            }catch (Exception ex)
            {
                _logger.Error(ex,"Error converting file. Moving to problem files directory.");
                // move file to problem files loaction
                System.IO.DirectoryInfo di = System.IO.Directory.GetParent(Path.GetDirectoryName(archiveFilePath));
                File.Move(archiveFilePath, Path.Combine(di.FullName, "ProblemFiles", Path.GetFileName(archiveFilePath))); // store this file
                
            }
        }

    }
}
