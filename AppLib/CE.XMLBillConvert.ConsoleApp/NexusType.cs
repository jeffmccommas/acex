﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.XMLBillConvert.ConsoleApp
{
    public class NexusType
    {
        public string TypeLabel { get; set; }
        public string Tag { get; set; }
        public string ParentTag { get; set; }

        public override bool Equals(object obj)
        {
            NexusType nt = obj as NexusType;
            return nt != null && nt.TypeLabel == this.TypeLabel && nt.Tag == this.Tag && nt.ParentTag ==this.ParentTag;
        }

        public override int GetHashCode()
        {
            return this.TypeLabel.GetHashCode() ^ this.Tag.GetHashCode() ^ this.ParentTag.GetHashCode();
        }
    }
}
