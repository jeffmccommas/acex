﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CE.XMLBillConvert.ConsoleApp
{
    public class ReadIntervalMapper: IBillMapper
    {
        public List<MappedString> MappedStrings { get; set; }

        public ReadIntervalMapper(XmlNode readIntervalNode)
        {
            MappedStrings = new List<MappedString>();
            MappedStrings.Add(new MappedString(DateTime.Parse(readIntervalNode.Attributes["EndDate"].Value).ToString("yyyyMMdd"), "bill_enddate"));
            MappedStrings.Add(new MappedString(readIntervalNode.Attributes["BillDays"].Value, "bill_days"));
            MappedStrings.Add(new MappedString(readIntervalNode.Attributes["RateClass"].Value, "rate_code"));
        }
    }
}
