﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CE.XMLBillConvert.ConsoleApp
{

    public class BillNodesMapper:IBillMapper
    {
        public List<MappedString> MappedStrings { get; set; }

        public BillNodesMapper()
        {
            MappedStrings = new List<MappedString>();
        }

        public void AddBillNodeAttribute(string value, string name)
        {
            MappedStrings.Add(new MappedString(value, name)); // TODO: if attribute names don't match column names perfectly, add a custom mapper method
        }


    }

}
