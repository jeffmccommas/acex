﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CE.XMLBillConvert.ConsoleApp
{
    public class AmountDetailMapper:IBillMapper
    {
        public List<MappedString> MappedStrings { get; set; }

        public AmountDetailMapper(XmlNodeList amountDetailNodes)
        {
            MappedStrings = new List<MappedString>();
            foreach (XmlNode amountDetailNode in amountDetailNodes)
            { 
                MappedStrings.Add(new MappedString(amountDetailNode.Attributes["Amount"].Value, amountDetailNode.Attributes["NexusType"].Value));
            }        
        }
    }
}
