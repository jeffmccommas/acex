﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CE.XMLBillConvert.ConsoleApp
{
    public class PremiseServiceMapper:IBillMapper
    {
        public List<MappedString> MappedStrings { get; set; }

        public PremiseServiceMapper(XmlNode serviceNode)
        {
            MappedStrings = new List<MappedString>();
        }
    }
}
