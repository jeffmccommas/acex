﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CE.XMLBillConvert.ConsoleApp
{
    public class UseDetailMapper:IBillMapper
    {
        public List<MappedString> MappedStrings { get; set; }
        public UseDetailMapper(XmlNode useDetailNode)
        {
            MappedStrings = new List<MappedString>();
            if (useDetailNode.Attributes["NexusType"].Value == "TOTAL")
            {
                MappedStrings.Add(new MappedString(useDetailNode.Attributes["Amount"].Value, "TOTAL"));
                MappedStrings.Add(new MappedString(useDetailNode.Attributes["Units"].Value,"TOTAL_UOM"));
                MappedStrings.Add(new MappedString(useDetailNode.Attributes["StartMeterRead"].Value, "TOTAL_START_READ"));
                MappedStrings.Add(new MappedString(useDetailNode.Attributes["EndMeterRead"].Value, "TOTAL_END_READ"));
                MappedStrings.Add(new MappedString(useDetailNode.Attributes["ReadType"].Value, "TOTAL_READ_TYPE"));
                MappedStrings.Add(new MappedString(useDetailNode.Attributes["Quantity"].Value, "TOTAL_GENERATED"));

            }
        }

    }
}
