﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CE.XMLBillConvert.ConsoleApp
{
    public class ServiceMapper:IBillMapper
    {
        private const string USAGEVALUE_HEADER = "usage_value";
        private const string USAGECHARGE_HEADER = "usage_charge";
        private const string READCYCLE_HEADER = "ReadCycle";
        private const string RETAILSUPPLY_HEADER = "RetailSupply";
        private const string NEXTREADDATE_HEADER = "NextReaddate";
        private const string METERUNITS_HEADER = "meter_units";
        public List<MappedString> MappedStrings { get; set; }

        public ServiceMapper(XmlNode serviceNode)
        {
            MappedStrings = new List<MappedString>();
            MappedStrings.Add(new MappedString(serviceNode.Attributes["TotalServiceUse"].Value,USAGEVALUE_HEADER));
            MappedStrings.Add(new MappedString(serviceNode.Attributes["TotalServiceAmount"].Value, USAGECHARGE_HEADER));

            MappedStrings.Add(new MappedString(serviceNode.Attributes["ReadCycle"].Value, READCYCLE_HEADER));
            MappedStrings.Add(new MappedString(serviceNode.Attributes["RetailSupply"].Value, RETAILSUPPLY_HEADER));
            MappedStrings.Add(new MappedString(serviceNode.Attributes["NextReadDate"].Value, NEXTREADDATE_HEADER));

            string meter_units = string.Empty;
            if (serviceNode.Attributes["Units"] != null)
            {
                switch (serviceNode.Attributes["Units"].Value.ToLower())
                {
                    case "ccf":
                        meter_units = "1";
                        break;
                    case "gal":
                        meter_units = "2";
                        break;
                    case "kwh":
                        meter_units = "3";
                        break;
                    case "lbs":
                        meter_units = "4";
                        break;
                    case "therms":
                        meter_units = "5";
                        break;
                    case "trees":
                        meter_units = "6";
                        break;
                }
                MappedStrings.Add(new MappedString(meter_units, METERUNITS_HEADER));
            }
            else
            {
                throw new Exception("Missing Units."); 
            }
        }
    }
}
