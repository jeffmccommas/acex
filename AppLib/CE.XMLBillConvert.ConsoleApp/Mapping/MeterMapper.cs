﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CE.XMLBillConvert.ConsoleApp
{
    public class MeterMapper:IBillMapper
    {
        public List<MappedString> MappedStrings { get; set; }
        public MeterMapper(XmlNode meterNode)
        {
            MappedStrings = new List<MappedString>();
            MappedStrings.Add(new MappedString(meterNode.Attributes["MeterType"].Value, "meter_type"));
            MappedStrings.Add(new MappedString(meterNode.Attributes["ServicePoint"].Value, "service_point_id"));
            MappedStrings.Add(new MappedString(meterNode.Attributes["Meter"].Value, "meter_id"));
            MappedStrings.Add(new MappedString(meterNode.Attributes["Description"].Value, "MeterDescription"));
            MappedStrings.Add(new MappedString(meterNode.Attributes["MeterLocation"].Value, "MeterLocation"));
            string meterrepacesmeterid = string.Empty;
            if (meterNode.Attributes["ReplacesMeter"] != null)
            {
                meterrepacesmeterid = meterNode.Attributes["ReplacesMeter"].Value;
            }
            MappedStrings.Add(new MappedString(meterrepacesmeterid, "meter_replaces_meterid"));
            MappedStrings.Add(new MappedString(meterNode.Attributes["MeterMultiplier"].Value, "MeterMultiplier"));
            MappedStrings.Add(new MappedString(meterNode.Attributes["MeterType"].Value, "MeterType"));
            MappedStrings.Add(new MappedString(meterNode.Attributes["TwoWay"].Value, "MeterTwoWay"));
        }
    }
}
