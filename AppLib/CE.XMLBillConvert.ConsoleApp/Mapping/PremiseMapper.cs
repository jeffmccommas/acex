﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CE.XMLBillConvert.ConsoleApp
{
    public class PremiseMapper:IBillMapper
    {
        private const string PREMISEID_HEADER = "premise_id";
        private const string PREMISEDESCRIPTION_HEADER = "PremiseDescription";
        private const string MAILADDRESSLINE1_HEADER = "mail_address_line_1";
        private const string MAILCITY_HEADER = "mail_city";
        private const string MAILSTATE_HEADER = "mail_state";
        private const string MAILZIPCODE_HEADER = "mail_zip_code";
        private const string SERVICEHOUSENUMBER_HEADER = "service_house_number";
        private const string SERVICESTREETNAME_HEADER = "service_street_name";
        private const string SERVICEUNIT_HEADER = "service_unit";
        private const string SERVICECITY_HEADER = "service_city";
        private const string SERVICESTATE_HEADER = "service_state";
        private const string SERVICEZIPCODE_HEADER = "service_zip_code";

        public List<MappedString> MappedStrings { get; set; }

        public PremiseMapper(XmlNode premiseNode)
        {
            MappedStrings = new List<MappedString>();
            if (premiseNode.Attributes["PremiseId"] != null)
            {
                MappedStrings.Add(new MappedString(premiseNode.Attributes["PremiseId"].Value, PREMISEID_HEADER));
            }
            else
            {
                throw new Exception("Missing premise ID.");
            }
            if (premiseNode.Attributes["Description"] != null)
            {
                MappedStrings.Add(new MappedString(premiseNode.Attributes["Description"].Value, PREMISEDESCRIPTION_HEADER));
            }

            MappedStrings.Add(new MappedString(premiseNode.Attributes["Addr1"].Value, MAILADDRESSLINE1_HEADER));
            MappedStrings.Add(new MappedString(premiseNode.Attributes["City"].Value, MAILCITY_HEADER));
            MappedStrings.Add(new MappedString(premiseNode.Attributes["State"].Value, MAILSTATE_HEADER));
            MappedStrings.Add(new MappedString(premiseNode.Attributes["Zip"].Value, MAILZIPCODE_HEADER));


            MappedStrings.Add(new MappedString(premiseNode.Attributes["Addr1"].Value, SERVICESTREETNAME_HEADER));

            MappedStrings.Add(new MappedString(premiseNode.Attributes["City"].Value, SERVICECITY_HEADER));
            MappedStrings.Add(new MappedString(premiseNode.Attributes["State"].Value, SERVICESTATE_HEADER));
            MappedStrings.Add(new MappedString(premiseNode.Attributes["Zip"].Value, SERVICEZIPCODE_HEADER));
        }

        
    }
}
