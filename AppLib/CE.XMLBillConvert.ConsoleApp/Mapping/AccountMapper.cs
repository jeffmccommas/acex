﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CE.XMLBillConvert.ConsoleApp
{
    public class AccountMapper : IBillMapper
    {
        public MappedString AccountId { get; set; }
        public MappedString LastName { get; set; }
        public MappedString FirstName { get; set; }
        public MappedString CustomerId { get; set; }

        public List<MappedString> MappedStrings { get; set; }

        public AccountMapper()
        {

        }

        public AccountMapper(XmlNode accountNode, string clientId)
        {
            MappedStrings = new List<MappedString>();
            MappedStrings.Add(new MappedString(clientId, "clientId"));
            AccountId = new MappedString(accountNode.Attributes["Account"].Value, "account_id");
            MappedStrings.Add(AccountId);
            CustomerId = new MappedString(accountNode.Attributes["Account"].Value, "customer_id");
            MappedStrings.Add(CustomerId);
            if (string.IsNullOrEmpty(accountNode.Attributes["LastName"].Value))
            {
                LastName = new MappedString(accountNode.Attributes["FirstName"].Value, "last_name");
            }
            else
            {
                LastName = new MappedString(accountNode.Attributes["LastName"].Value, "last_name");
            }
            FirstName = new MappedString(accountNode.Attributes["FirstName"].Value, "first_name");
            MappedStrings.Add(FirstName);
            MappedStrings.Add(LastName);
           
        }
        

        
    }
}
