﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.XMLBillConvert.ConsoleApp
{
    public class MappedString
    {
        public string Value { get; set; }
        public string Header { get; set; } // stores the position where Value will be written within a CSV row
        public MappedString(string value, string header)
        {
            Value = value;
            Header = header;
        }
    }
}
