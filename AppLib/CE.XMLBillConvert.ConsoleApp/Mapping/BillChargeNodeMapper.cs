﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CE.XMLBillConvert.ConsoleApp
{

    public class BillChargeNodeMapper:IBillMapper
    {
        private string BILL_CHARGE_COLUMN_BASE = "Bill_OTHR_OTHR";
        public List<MappedString> MappedStrings { get; set; }
        public BillChargeNodeMapper()
        {

        }

        public BillChargeNodeMapper(XmlNodeList chargeNodes)
        {
            MappedStrings = new List<MappedString>();
            int index = 0;
            foreach(XmlNode chargeNode in chargeNodes)
            {
                AddBillCharge(chargeNode, index);
                index++;
            }
        }
        public void AddBillCharge(XmlNode chargeNode, int index)
        {
            if(index == 0)
            {
                MappedStrings.Add(new MappedString(chargeNode.Attributes["Amount"].Value, BILL_CHARGE_COLUMN_BASE));
                MappedStrings.Add(new MappedString(chargeNode.Attributes["Description"].Value, BILL_CHARGE_COLUMN_BASE + "_DESC"));
            }
            else if (index < 19)
            {
                int numLabel = index + 1;
                MappedStrings.Add(new MappedString(chargeNode.Attributes["Amount"].Value, BILL_CHARGE_COLUMN_BASE + numLabel));
                MappedStrings.Add(new MappedString(chargeNode.Attributes["Description"].Value, BILL_CHARGE_COLUMN_BASE+ numLabel + "_DESC"));
            } else
            {
                throw new Exception("TOO MANY BILL CHARGES");
            }
        }
    }
}
