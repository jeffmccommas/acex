﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CE.XMLBillConvert.ConsoleApp
{
   
    public class AccountIndicatorMapper:IBillMapper
    {

        private string BUDGETBILLINGSTATUS_HEADER = "BudgetBillingStatus";

        public List<MappedString> MappedStrings { get; set; }
        public MappedString BudgetBillingStatus { get; set; }
        public AccountIndicatorMapper(XmlNode accountIndicatorNode) {
            MappedStrings = new List<MappedString>();
            if (accountIndicatorNode.Attributes["IndicatorType"].Value == "BudgetBillingStatus")
            {
                BudgetBillingStatus = new MappedString(accountIndicatorNode.Attributes["IndicatorValue"].Value, BUDGETBILLINGSTATUS_HEADER);
            }
            MappedStrings.Add(BudgetBillingStatus);
        }

        
    }
}
