﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CE.XMLBillConvert.ConsoleApp
{
    public class ServiceChargeMapper:IBillMapper
    {
        public List<MappedString> MappedStrings { get; set; }

        public ServiceChargeMapper(XmlNodeList serviceChargeNodes)
        {
            MappedStrings = new List<MappedString>();
            foreach (XmlNode serviceCharge in serviceChargeNodes)
            {
                switch (serviceCharge.Attributes["NexusType"].Value)
                {
                    case "STD_CUST":
                        MappedStrings.Add(new MappedString(serviceCharge.Attributes["Amount"].Value, "STD_CUST"));
                        MappedStrings.Add(new MappedString(serviceCharge.Attributes["Description"].Value, "STD_CUST_DESC"));
                        break;
                    case "OTHR_OTHR":
                        MappedStrings.Add(new MappedString(serviceCharge.Attributes["Amount"].Value, "Service_OTHR_OTHR"));
                        MappedStrings.Add(new MappedString(serviceCharge.Attributes["Description"].Value, "Service_OTHR_OTHR_DESC"));
                        break;
                    case "GOVT_OTHR":
                        MappedStrings.Add(new MappedString(serviceCharge.Attributes["Amount"].Value, "GOVT_OTHR"));
                        MappedStrings.Add(new MappedString(serviceCharge.Attributes["Description"].Value, "GOVT_OTHR_DESC"));
                        break;
                    case "PTR_REBATE":
                        MappedStrings.Add(new MappedString(serviceCharge.Attributes["Amount"].Value, "PTR_REBATE"));
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
