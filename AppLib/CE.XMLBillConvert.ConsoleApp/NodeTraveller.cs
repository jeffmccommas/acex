﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CE.XMLBillConvert.ConsoleApp
{
    public class NodeTraveller
    {
        private System.IO.StreamReader file;
        public NodeTraveller(string inputPath, string outputPath, HashSet<NexusType> nexusTypes)
        {
            NexusTypes = nexusTypes;
            InputPath = inputPath;
            OutputPath = outputPath;

            try
            {
                file =
                   new System.IO.StreamReader(inputPath);
                string xmlString = file.ReadToEnd();
                file.Close();
                string editXmlString = xmlString.Replace(" xmlns=\"Nexus:BillAnalyzer\"", string.Empty);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(editXmlString);
                XmlDoc = xmlDoc;
            }
            catch
            {
                
                System.IO.DirectoryInfo di = System.IO.Directory.GetParent(Path.GetDirectoryName(inputPath));
                File.Copy(inputPath, Path.Combine(di.FullName, "ProblemFiles", Path.GetFileName(inputPath))); // store this file
                
            }
        

        }

        public XmlDocument XmlDoc { get; set; }
        public string InputPath { get; set; }
        public string OutputPath { get; set; }


        public HashSet<NexusType> NexusTypes { get; set; }
        /// <summary>
        /// Recursive function to visit every node
        /// </summary>
        private void ListNodes(XmlNode topNode)
        {
            if (topNode.HasChildNodes)
            {
                foreach(XmlNode childNode in topNode.ChildNodes)
                {
                    if (topNode.Name == "Premise" && topNode.ChildNodes.Count > 1)
                    {
                        
                        
                        
                        Console.WriteLine("MULTIPLE SERVICE NODES!!!!!!!!!!!!!!!!!!!!!!!");
                        Console.ReadLine();



                    }
                    if(childNode.Attributes["NexusType"] != null)
                    {
                        NexusType nt = new NexusType();
                        nt.TypeLabel = childNode.Attributes["NexusType"].Value;
                        nt.Tag = childNode.Name;
                        nt.ParentTag = childNode.ParentNode.Name;
                        if (!NexusTypes.Add(nt))
                        {
                            System.Diagnostics.Debug.WriteLine("TypeLabel=" + nt.TypeLabel + " Tag" + nt.Tag + " ParentTag" + nt.ParentTag + " already exists in NexusTypes");
                        }
                        
                    }
                    ListNodes(childNode);
                }
            }
        }

        public HashSet<NexusType> GetNexusTypes()
        {
            if (XmlDoc != null)
            {
                ListNodes(XmlDoc.FirstChild);
            }
            return NexusTypes;

        }
    }
}
