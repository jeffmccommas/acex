﻿using CE.EventTrack.WorkerRole.Utility;
using Microsoft.Azure;
using Microsoft.WindowsAzure.ServiceRuntime;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading;

namespace CE.EventTrack.WorkerRole
{
    public class WorkerRole : RoleEntryPoint
    {
        private Receiver _receiver;
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent _runCompleteEvent = new ManualResetEvent(false);

        public override void Run()
        {
            Trace.TraceInformation("AceEventTrackingWorkerRole is running");

            try
            {
                if (!_cancellationTokenSource.Token.IsCancellationRequested)
                {
                    //Get settings from configuration
                    var eventHubName = CloudConfigurationManager.GetSetting("eventHubName");
                    var consumerGroupName = CloudConfigurationManager.GetSetting("consumerGroupName");
                    var numberOfPartitions = int.Parse(CloudConfigurationManager.GetSetting("numberOfPartitions"));
                    var blobConnectionString = CloudConfigurationManager.GetSetting("AzureStorageConnectionString"); // Required for checkpoint/state

                    //Get AMQP connection string
                    var connectionString = EventHubManager.GetServiceBusConnectionString();

                    //Create event hub if it does not exist
                    var namespaceManager = EventHubManager.GetNamespaceManager(connectionString);
                    EventHubManager.CreateEventHubIfNotExists(eventHubName, numberOfPartitions, namespaceManager);

                    //Create consumer group if it does not exist
                    var group = namespaceManager.CreateConsumerGroupIfNotExists(eventHubName, consumerGroupName);

                    //Start processing messages
                    _receiver = new Receiver(eventHubName, connectionString);

                    //Get host name of worker role instance.  This is used for each environment to obtain
                    //a lease, and to renew the same lease in case of a restart.
                    var hostName = RoleEnvironment.CurrentRoleInstance.Id;
                    _receiver.RegisterEventProcessor(group, blobConnectionString, hostName);
                }

                //Wait for shutdown to be called, else the role will recycle
                _runCompleteEvent.WaitOne();
            }
            finally
            {
                _runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 100; //12

            var result = base.OnStart();

            Trace.TraceInformation("AceEventTrackingWorkerRole has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("ReceiverRole is stopping");

            _runCompleteEvent.Set();
            try
            {
                //Unregister the event processor so other instances will handle the partitions
                _receiver.UnregisterEventProcessor();
            }
            catch (Exception oops)
            {
                Trace.TraceError(oops.Message);
            }

            base.OnStop();

            Trace.TraceInformation("ReceiverRole has stopped");
        }
    }
}
