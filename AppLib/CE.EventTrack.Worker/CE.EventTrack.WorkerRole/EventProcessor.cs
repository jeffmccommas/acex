﻿using CE.EventTrack.Utilities;
using CE.EventTrack.WorkerRole.Utility;
using Microsoft.Azure;
using Microsoft.Azure.Documents;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CE.EventTrack.WorkerRole
{
    public class EventProcessor : IEventProcessor
    {
        PartitionContext _partitionContext;

        public Task OpenAsync(PartitionContext context)
        {
            Trace.TraceInformation(
                $"EventProcessor OpenAsync.  Partition: '{context.Lease.PartitionId}', Offset: '{context.Lease.Offset}'");
            _partitionContext = context;
            return Task.FromResult<object>(null);
        }

        public async Task ProcessEventsAsync(PartitionContext context, IEnumerable<EventData> events)
        {
            try
            {
                foreach (var eventData in events)
                {
                    var data = Encoding.UTF8.GetString(eventData.GetBytes());
                    try
                    {
                        var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                        //var environment = CloudConfigurationManager.GetSetting("Environment");
                        //values["Environment"] = string.IsNullOrWhiteSpace(environment) ? "Environment not specified" : environment;

                        string rowKeyValue, partitionKeyValue;
                        SetPrimaryKeyAndRowKeyValues(ref values, out partitionKeyValue, out rowKeyValue);

                        var isValid = true;
                        var errorMessage = "";

                        ValidateEvent(values, ref isValid, ref errorMessage);

                        if (isValid)
                        {
                            InsertIntoAzureTable(values, partitionKeyValue, rowKeyValue);
                        }
                        else
                        {
                            InsertInDocumentDb(eventData, errorMessage).Wait();
                            //SendEmail(data, errorMessage).Wait();
                        }

                        await context.CheckpointAsync();
                    }
                    catch (Exception oops)
                    {
                        //SendEmail(data, oops.Message).Wait();
                        await context.CheckpointAsync();
                        Trace.TraceError(oops.Message);
                    }
                }

            }
            catch (Exception exp)
            {
                Trace.TraceError("Error in processing: " + exp.Message);
            }
        }

        private static void SetPrimaryKeyAndRowKeyValues(ref Dictionary<string, string> values, out string partitionKeyValue, out string rowKeyValue)
        {
            var date = DateTime.Now;
            var dateString = date.ToString("MM/dd/yyyy").Replace("/", "-");

            var clientId = values.FirstOrDefault(v => string.Equals(v.Key, "clientid", StringComparison.InvariantCultureIgnoreCase)).Value;
            var eventType = values.FirstOrDefault(v => string.Equals(v.Key, "eventtype", StringComparison.InvariantCultureIgnoreCase)).Value;
            var partitionKey = values.Keys.FirstOrDefault(k => string.Equals(k, "PartitionKey"));
            partitionKeyValue =
                $"{(string.IsNullOrWhiteSpace(clientId) ? "" : clientId)}_{(string.IsNullOrWhiteSpace(eventType) ? "" : eventType)}_{dateString}";

            if (partitionKey != null)
                values["PartitionKey"] = partitionKeyValue;
            else
                values.Add("PartitionKey", partitionKeyValue);

            var customerId = values.FirstOrDefault(v => string.Equals(v.Key, "customerid", StringComparison.InvariantCultureIgnoreCase)).Value;
            var accountId = values.FirstOrDefault(v => string.Equals(v.Key, "accountid", StringComparison.InvariantCultureIgnoreCase)).Value;
            var premiseId = values.FirstOrDefault(v => string.Equals(v.Key, "premiseid", StringComparison.InvariantCultureIgnoreCase)).Value;
            var rowKey = values.Keys.FirstOrDefault(k => string.Equals(k, "RowKey"));
            rowKeyValue =
                $"{(string.IsNullOrWhiteSpace(customerId) ? "" : customerId)}_{(string.IsNullOrWhiteSpace(accountId) ? "" : accountId)}_{(string.IsNullOrWhiteSpace(premiseId) ? "" : premiseId)}_{date.Ticks}";

            if (rowKey != null)
                values["RowKey"] = rowKeyValue;
            else
                values.Add("RowKey", rowKeyValue);
        }

        private static void ValidateEvent(IDictionary<string, string> values, ref bool isValid, ref string errorMessage)
        {
            foreach (var value in values)
            {
                //Check if propertynames follow C# identifier rules
                var provider = System.CodeDom.Compiler.CodeDomProvider.CreateProvider("C#");
                if (!string.IsNullOrWhiteSpace(value.Key) && provider.IsValidIdentifier(value.Key) && value.Key.Length <= 255)
                {
                    // Valid
                    if (string.Equals(value.Key, "PartitionKey") || string.Equals(value.Key, "RowKey"))
                    {
                        // Valid
                        if (string.IsNullOrWhiteSpace(value.Value) || value.Value.Any(char.IsControl) ||
                            value.Value.Contains(@"\") || value.Value.Contains("/") || value.Value.Contains("#") ||
                            value.Value.Contains("?"))
                        {
                            // Not valid
                            isValid = false;
                            errorMessage = $"{value.Key} value does not follow system property rules.";
                            break;
                        }
                    }
                }
                else
                {
                    // Not valid
                    isValid = false;
                    errorMessage = $"Property Name: {value.Key} does not follow C# identifier rules.";
                    break;
                }
            }
        }

        private static void InsertIntoAzureTable(IDictionary<string, string> values, string partitionKeyValue, string rowKeyValue)
        {
            // Retrieve the storage account from the connection string.
            var storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("AzureStorageConnectionString"));

            // Create the table client.
            var tableClient = storageAccount.CreateCloudTableClient();

            // Get table name from configuration
            string tableName = Microsoft.Azure.CloudConfigurationManager.GetSetting("AzureStorageTableName");

            // Create the table if it doesn't exist.
            var table = tableClient.GetTableReference(tableName);
            table.CreateIfNotExists();
            values.Remove("PartitionKey");
            values.Remove("RowKey");

            // Insert into Azure Table
            var entity = new DynamicTableEntity
            {
                Properties =
                    values.ToDictionary(value => value.Key, value => new EntityProperty(value.Value)),
                PartitionKey = partitionKeyValue,
                RowKey = rowKeyValue
            };

            table.Execute(TableOperation.Insert(entity));
        }

        private static async Task InsertInDocumentDb(EventData eventData, string errorMessage)
        {
            var doc = new Document();
            doc.SetPropertyValue("DateCreated", DateTime.Now);
            doc.SetPropertyValue("EpochDateCreated", DateTime.Now.ToEpoch());
            doc.SetPropertyValue("ErrorMessage", errorMessage);
            doc.SetPropertyValue("EventData", JsonConvert.DeserializeObject<dynamic>(Encoding.UTF8.GetString(eventData.GetBytes())));
            await DocumentDbRepository<object>.CreateItemAsync(doc);
        }

        //private static async Task SendEmail(string data, string errorMessage)
        //{
        //    try
        //    {
        //        if (Convert.ToBoolean(CloudConfigurationManager.GetSetting("EnableEmail")))
        //        {
        //            var myMessage = new SendGridMessage();
        //            var recipientsString = CloudConfigurationManager.GetSetting("ErrorEmailRecipients");
        //            List<string> recipients;
        //            if (recipientsString.Length > 0)
        //            {
        //                recipients = recipientsString.Split(';').Where(m => !string.IsNullOrWhiteSpace(m)).ToList();
        //            }
        //            else
        //            {
        //                recipients = new List<string>
        //            {
        //                @"rohan.kulkarni@saviantconsulting.com"
        //            };
        //            }

        //            myMessage.AddTo(recipients);
        //            var emailFrom = CloudConfigurationManager.GetSetting("ErrorEmailFrom");
        //            myMessage.From = new MailAddress(emailFrom, "Admin");
        //            myMessage.Subject = "Ace Event Notification - Error occurred while processing Event";
        //            var htmlBody = "Hi,<br/><br/>";
        //            htmlBody += "An error occurred while processing the following Event:<br/><br/>";
        //            htmlBody += $"<b>Event:</b> {data}<br/><b>Error Message:</b> {errorMessage}";
        //            htmlBody += "<br/><br/>Regards,<br/>Admin";
        //            myMessage.Html = htmlBody;

        //            var credentials = new NetworkCredential(CloudConfigurationManager.GetSetting("SendGridUsername"),
        //                CloudConfigurationManager.GetSetting("SendGridPassword"));
        //            var transportWeb = new Web(credentials);
        //            // Send email.
        //            await transportWeb.DeliverAsync(myMessage);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        //Do Nothing
        //    }
        //}

        public async Task CloseAsync(PartitionContext context, CloseReason reason)
        {
            Trace.TraceWarning(
                $"EventProcessor CloseAsync.  Partition '{_partitionContext.Lease.PartitionId}', Reason: '{reason}'.");
            if (reason == CloseReason.Shutdown)
            {
                await context.CheckpointAsync();
            }
        }
    }
}
