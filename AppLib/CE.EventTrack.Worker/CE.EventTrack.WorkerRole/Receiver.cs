﻿using System;
using Microsoft.ServiceBus.Messaging;
using System.Diagnostics;

namespace CE.EventTrack.WorkerRole
{
    class Receiver
    {
        readonly string _eventHubName;
        readonly string _eventHubConnectionString;
        EventProcessorHost _eventProcessorHost;

        public Receiver(string eventHubName, string eventHubConnectionString)
        {
            _eventHubConnectionString = eventHubConnectionString;
            _eventHubName = eventHubName;
        }

        public void RegisterEventProcessor(ConsumerGroupDescription group, string blobConnectionString, string hostName)
        {
            var eventHubClient = EventHubClient.CreateFromConnectionString(_eventHubConnectionString, _eventHubName);

            if (null == group)
            {
                //Use default consumer group
                var defaultConsumerGroup = eventHubClient.GetDefaultConsumerGroup();
                _eventProcessorHost = new EventProcessorHost(hostName, eventHubClient.Path,
                    defaultConsumerGroup.GroupName, _eventHubConnectionString, blobConnectionString);
            }
            else
            {
                //Use custom consumer group
                _eventProcessorHost = new EventProcessorHost(hostName, eventHubClient.Path, group.Name, _eventHubConnectionString, blobConnectionString);
            }

            _eventProcessorHost.PartitionManagerOptions = new PartitionManagerOptions
            {
                AcquireInterval = TimeSpan.FromMilliseconds(10), // Default is 10 seconds 
                RenewInterval = TimeSpan.FromSeconds(30), // Default is 10 seconds 
                LeaseInterval = TimeSpan.FromMinutes(1) // Default value is 30 seconds                        
            };

            var eventProcessorOptions = new EventProcessorOptions
            {
                InvokeProcessorAfterReceiveTimeout = true,                
                MaxBatchSize = 100,
                PrefetchCount = 100,
                ReceiveTimeOut = TimeSpan.FromSeconds(30)
            };

            Trace.TraceInformation("Registering event processor");
            _eventProcessorHost.RegisterEventProcessorAsync<EventProcessor>(eventProcessorOptions).Wait();
        }

        public void UnregisterEventProcessor()
        {
            _eventProcessorHost.UnregisterEventProcessorAsync().Wait();
        }
    }
}
