﻿using Microsoft.Azure;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CE.EventTrack.WorkerRole.Utility
{
    public static class DocumentDbRepository<T>
    {
        //Expose the "database" value from configuration as a property for internal use        
        private static string DatabaseId => CloudConfigurationManager.GetSetting("database");
        //Expose the "collection" value from configuration as a property for internal use        
        private static string CollectionId => CloudConfigurationManager.GetSetting("collection");
        //Use the ReadOrCreateDatabase function to get a reference to the database.        
        private static Database Database => ReadOrCreateDatabase();
        //Use the ReadOrCreateCollection function to get a reference to the collection.        
        private static DocumentCollection Collection => ReadOrCreateCollection(Database.SelfLink);
        //This property establishes a new connection to DocumentDB the first time it is used, 
        //and then reuses this instance for the duration of the application avoiding the
        //overhead of instantiating a new instance of DocumentClient with each request        
        private static DocumentClient Client
        {
            get
            {
                var endpoint = CloudConfigurationManager.GetSetting("endpoint");
                var authKey = CloudConfigurationManager.GetSetting("authKey");
                var endpointUri = new Uri(endpoint);
                return new DocumentClient(endpointUri, authKey);
            }
        }

        public static async Task<Document> CreateItemAsync(T item)
        {
            return await Client.CreateDocumentAsync(Collection.SelfLink, item);
        }

        //Use the Database if it exists, if not create a new Database
        private static Database ReadOrCreateDatabase()
        {
            var db = Client.CreateDatabaseQuery()
                .Where(d => d.Id == DatabaseId)
                .AsEnumerable()
                .FirstOrDefault() ?? Client.CreateDatabaseAsync(new Database { Id = DatabaseId }).Result;

            return db;
        }

        //Use the DocumentCollection if it exists, if not create a new Collection
        private static DocumentCollection ReadOrCreateCollection(string databaseLink)
        {
            var col = Client.CreateDocumentCollectionQuery(databaseLink)
                .Where(c => c.Id == CollectionId)
                .AsEnumerable()
                .FirstOrDefault();

            if (col != null) return col;
            var collectionSpec = new DocumentCollection { Id = CollectionId };
            var requestOptions = new RequestOptions { OfferType = "S1" };

            col = Client.CreateDocumentCollectionAsync(databaseLink, collectionSpec, requestOptions).Result;

            return col;
        }
    }
}