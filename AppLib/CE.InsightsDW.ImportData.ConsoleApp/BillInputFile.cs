﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using Aclara.UFx.StatusManagement;
using CE.InsightsDW.ImportData.Infastructure;
using CE.InsightsDW.ImportData.Infastructure.DataAccess;
using CE.InsightsDW.Model;

// ReSharper disable CheckNamespace
namespace CE.InsightsDW.ImportData
{

    public class BillInputFile : IProcessFile
    {
        private readonly int _clientId;
        private readonly IConfig _config;

        private const string BillingNode_Customerid = "CustomerId";
        private const string BillingNode_Accountid = "AccountId";
        private const string BillingNode_PremiseId = "PremiseId";
        private const string BillingNode_BillStartDate = "StartDate";
        private const string BillingNode_BillEndDate = "EndDate";
        private const string BillingNode_TotalUnits = "TotalUsage";
        private const string BillingNode_TotalCost = "TotalCost";
        private const string BillingNode_CommodityId = "Commodity";
        private const string BillingNode_BillPeriodTypeId = "BillPeriodType";
        private const string BillingNode_BillCycleScheduleId = "BillCycleScheduleId";
        private const string BillingNode_Source = "Source";
        private const string BillingNode_UOM = "UOM";
        private const string BillingNode_ServicePointId = "ServicePointId";
        private const string BillingNode_ServiceContractId = "ServiceContractId";
        private const string BillingNode_MeterId = "MeterId";
        private const string BillingNode_AMIStartDate = "AMIStartDate";
        private const string BillingNode_AMIEndDate = "AMIEndDate";
        private const string BillingNode_RateClass = "RateClass";
        private const string BillingNode_MeterType = "MeterType";
        private const string BillingNode_ReplacedMeterId = "ReplacedMeterId";
        private const string BillingNode_ReadQuality = "ReadQuality";
        private const string BillingNode_ReadDate = "ReadDate";
        private const string BillingNode_DueDate = "DueDate";
        private const string BillingNode_UtilityBillRecordId = "UtilityBillRecordId";
        private const string BillingNode_CanceledUtilityBillRecordId = "CanceledUtilityBillRecordId";
        private const string BillingNode_BillDate = "BillDate";
        private const string BillingNode_BillDays = "BillDays";

        private  Infastructure.InsightsDW _daInsightsDw;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="options"></param>
        /// <param name="config"></param>
        public BillInputFile(Options options, IConfig config)
        {
            _clientId = Convert.ToInt32(options.ClientId);
            _config = config;



        }

        /// <summary>
        /// Process the Billing file
        /// </summary>
        /// <param name="theFile">The file to be processed</param>
        /// <param name="emailMessageContents">Contents of error email message</param>
        /// <param name="logMessageContents">Messages for the log file</param>
        /// <param name="captureCountsForReporting">Capture count data for reporting</param>
        /// <param name="statusList">The Status</param>
        /// <returns>The tracking id</returns>
        public string ProcessFile(FileInfo theFile, ICollection<string> emailMessageContents, ICollection<LoggerMessageModel> logMessageContents, 
            CountsForReportingModel captureCountsForReporting, out StatusList statusList)
        {
            var trackingid = string.Empty;
            statusList = new StatusList();
            var nodeCounter = 0;
            var totalNodeCounter = 0;
            var processedNodeCounter = 0;
            var duplicateBills = 0;

            _daInsightsDw = new Infastructure.InsightsDW(_config.ShardName);

            var theBills = new List<Holding_Billing>();
            var theBillingCostDetails = new List<Holding_Billing_BillCostDetails>();
            var theServiceCostDetails = new List<Holding_Billing_ServiceCostDetails>();

            try
            {
                DateTime trackingIdDate;
                Helpers.SetTrackingData(theFile.Name, out trackingid, out trackingIdDate);
                var xsdFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" +
                                  _config.InputXSD;

                // This logic sets-up a custom iteration using linq and yield.  Each element is traversed forward-only, greatly saving resources.
                var getXmlElements = InputFileXMLStream.StreamData(theFile.FullName).Select(el => el);

                var schemas = new XmlSchemaSet();
                schemas.Add(Constants.FileNamespace, XmlReader.Create(new StreamReader(xsdFile)));

                foreach (var element in getXmlElements)
                {
                    //Validate the node
                    var errors = false;
                    var errStatusList = new StatusList();

                    var doc = new XDocument(new XElement(Constants.RootNode));
                    var xElement = doc.Element(Constants.RootNode);
                    xElement?.Add(element);

                    doc.Validate(schemas, (o, e) =>
                    {
                        errStatusList.Add(new Status(e.Message, StatusTypes.StatusSeverity.Error));

                        var elementData = string.Concat(element.Elements());

                        _daInsightsDw.LogErrorsByInputType(
                            Helpers.ExtractElements(elementData, DWImportConstants.RunType.Bill, _clientId, trackingid),
                            e.Message, theFile.Name, DWImportConstants.RunType.Bill, _config.ImportSpecificLoggingOn);

                        logMessageContents.Add(new LoggerMessageModel { LogLevel = NLog.LogLevel.Error, Exception = new InsightsDWImportException(
                                        $"Validation Error for file {theFile.Name} data: {elementData}",
                                        e.Exception)
                        });

                        errors = true;

                    }, true);


                    if (nodeCounter == 0)
                    {
                        //Create the list for storage
                        theBills = new List<Holding_Billing>();
                    }

                    //Parse the tracking id out of the file name.  If we don't have one, then use a datetime stamp
                    if (!errors)
                    {
                        try
                        {
                            var xmlNode = InputFileXMLStream.GetXmlNode(element);
                            ProcessNode(trackingid, trackingIdDate, xmlNode, theBills, theBillingCostDetails,
                                theServiceCostDetails);
                            nodeCounter++;

                        }
                        catch (Exception eX)
                        {
                            errStatusList.Add(new Status(eX.Message, StatusTypes.StatusSeverity.Error));

                            var elementData = string.Concat(element.Elements());

                            _daInsightsDw.LogErrorsByInputType(
                                Helpers.ExtractElements(elementData, DWImportConstants.RunType.Bill, _clientId,
                                    trackingid),
                                eX.Message, theFile.Name, DWImportConstants.RunType.Bill,
                                _config.ImportSpecificLoggingOn);

                            logMessageContents.Add(new LoggerMessageModel
                            {
                                LogLevel = NLog.LogLevel.Error,
                                Exception =
                                    new InsightsDWImportException(
                                        $"Processing Error for file {theFile.Name} data: " + eX.Message)
                            });

                            errors = true;
                        }
                    }


                    if(errors)
                    {
                        statusList.AddRange(errStatusList);
                        errors = false;
                        errStatusList.Clear();
                    }

                    totalNodeCounter++;

                    // Write out data if reached max batch size
                    if (theBills.Count >= _config.MaxBatchSize)
                    {
                        processedNodeCounter += nodeCounter;
                        nodeCounter = 0;

                        //duplicateBills += WriteDuplicateBillsToLog(theBills, logMessageContents);
                        WriteBillingData(theBills, ref statusList, emailMessageContents, logMessageContents);

                        WriteBilling_BillCostDetailsData(theBillingCostDetails, ref statusList, emailMessageContents, logMessageContents);

                        WriteBilling_ServiceCostDetailsData(theServiceCostDetails, ref statusList, emailMessageContents, logMessageContents);

                        theBills.Clear();
                        theBillingCostDetails.Clear();
                        theServiceCostDetails.Clear();
                    }
                }


                // End of loop. Write what is left.
                if (theBills.Count > 0)
                {
                    processedNodeCounter += nodeCounter;

                    //Less than max rows, so write it out
                    //duplicateBills += WriteDuplicateBillsToLog(theBills, logMessageContents);
                    WriteBillingData(theBills, ref statusList, emailMessageContents, logMessageContents);

                    WriteBilling_BillCostDetailsData(theBillingCostDetails, ref statusList, emailMessageContents, logMessageContents);

                    WriteBilling_ServiceCostDetailsData(theServiceCostDetails, ref statusList, emailMessageContents, logMessageContents);

                    theBills.Clear();
                    theBillingCostDetails.Clear();
                    theServiceCostDetails.Clear();

                }

                var logMessage =  ">>>> Processed " + processedNodeCounter + " Bills out of " + totalNodeCounter + " <<<<";

                statusList.Add(new Status(logMessage, StatusTypes.StatusSeverity.Informational));
                logMessageContents.Add(new LoggerMessageModel { LogLevel = NLog.LogLevel.Info, MessageToLog = logMessage });

                //logMessage = ">>>> Duplicate Bill Count " + duplicateBills + " Bills out of " + totalNodeCounter + " <<<<";

                //statusList.Add(new Status(logMessage, StatusTypes.StatusSeverity.Informational));
                //logMessageContents.Add(new LoggerMessageModel { LogLevel = NLog.LogLevel.Info, MessageToLog = logMessage });

                emailMessageContents.Add(string.Empty);
                emailMessageContents.Add(logMessage);
                emailMessageContents.Add(string.Empty);

                captureCountsForReporting.Count = processedNodeCounter;
                captureCountsForReporting.Comment = logMessage;
            }
            catch (Exception ex)
            {
                statusList.Add(new Status(ex, StatusTypes.StatusSeverity.Error));

                logMessageContents.Add(new LoggerMessageModel
                {
                    LogLevel = NLog.LogLevel.Error,
                    Exception = null,
                    MessageToLog = ex.Message
                });
            }

            return trackingid;
        }
        /// <summary>
        /// Write duplicate bill data in file to log files. 
        /// </summary>
        /// <param name="bills"></param>
        /// /// <param name="logMessageContents"></param>
        public int WriteDuplicateBillsToLog(List<Holding_Billing> bills, ICollection<LoggerMessageModel> logMessageContents)
        {
            
            int duplicateBillCount = 0;
            
               var duplicatesinFile = bills
                    .GroupBy(x => new
                    {
                        x.AccountId,
                        x.PremiseId,
                        x.ServiceContractId,
                        x.CommodityId,
                        x.UOMId,
                        x.BillStartDate,
                        x.BillEndDate,
                        x.ClientId
                    }).Where(y => y.Count() > 1).Select(z => z).ToList();

                foreach (var billGroup in duplicatesinFile)
                {
                    var bill = billGroup.FirstOrDefault();
                    duplicateBillCount += billGroup.Count() - 1;
                    var logMessage = $"{billGroup.Count()} Duplicate bills found in bill file and only one record will be inserted into the holding tables - " +
                                     $"Details Account ID:{bill?.AccountId} PremiseID ID:{bill?.PremiseId} Meter ID:{bill?.MeterId} " +
                                     $"ServicePoint ContractID:{bill?.ServiceContractId} Commodity ID:{bill?.CommodityId} UOM ID:{bill?.UOMId} " +
                                     $"Bill Start Date:{bill?.BillStartDate} Bill End Date:{bill?.BillEndDate} ";

                    logMessageContents.Add(new LoggerMessageModel { LogLevel = NLog.LogLevel.Info, MessageToLog = logMessage });
                }

            using (var insightsDwEntities = new InsightsDWEntities())
            {
                insightsDwEntities.ChangeDatabase(_config.ShardName);
                var duplicatesAcrossFiles = bills.Where(
                    a => insightsDwEntities.Holding_Billing.Any(
                        b => b.AccountId == a.AccountId &&
                        b.ClientId == a.ClientId &&
                        b.PremiseId == a.PremiseId &&
                        b.ServiceContractId == a.ServiceContractId &&
                        b.BillStartDate == a.BillStartDate &&
                        b.BillEndDate == a.BillEndDate &&
                        b.CommodityId == a.CommodityId &&
                        b.UOMId == a.UOMId));

                foreach (var bill in duplicatesAcrossFiles)
                {
                    duplicateBillCount += 1;
                    var logMessage = $"Duplicates found across bill files and only one record will be inserted into the holding tables - " +
                                     $"Details Account ID:{bill?.AccountId} PremiseID ID:{bill?.PremiseId} Meter ID:{bill?.MeterId} " +
                                     $"ServicePoint ContractID:{bill?.ServiceContractId} Commodity ID:{bill?.CommodityId} UOM ID:{bill?.UOMId} " +
                                     $"Bill Start Date:{bill?.BillStartDate} Bill End Date:{bill?.BillEndDate} ";

                    logMessageContents.Add(new LoggerMessageModel { LogLevel = NLog.LogLevel.Info, MessageToLog = logMessage });
                }

            }

            return duplicateBillCount;
        }
        /// <summary>
        /// Write the Billing data to the database.
        /// </summary>
        /// <param name="bills">The Billing data</param>
        /// <param name="statusList">A list of statuses.</param>
        /// <param name="emailMessageContents">Contents of error email message</param>
        /// <param name="logMessageContents">Messages for the log file</param>
        private void WriteBillingData(List<Holding_Billing> bills, ref StatusList statusList,  ICollection<string> emailMessageContents, ICollection<LoggerMessageModel> logMessageContents)
        {
            var results = _daInsightsDw.WriteBillingData(bills);
            if (results != bills.Count)
            {
                var logMessage =  "Not all Billing data was inserted. Only " + results + " out of  " + bills.Count + " saved to database.";
                statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + logMessage, StatusTypes.StatusSeverity.Error));
                logMessageContents.Add(new LoggerMessageModel { LogLevel = NLog.LogLevel.Info, MessageToLog = logMessage });
                emailMessageContents.Add(logMessage);
            }


        }

        /// <summary>
        /// Write the Billing Cost Detail data to the database.
        /// </summary>
        /// <param name="theBillingCostDetails">The Billing Cost Detail data</param>
        /// <param name="statusList">A list of statuses.</param>
        /// <param name="emailMessageContents">Contents of error email message</param>
        /// <param name="logMessageContents">Messages for the log file</param>
        private void WriteBilling_BillCostDetailsData(List<Holding_Billing_BillCostDetails> theBillingCostDetails, ref StatusList statusList, ICollection<string> emailMessageContents, ICollection<LoggerMessageModel> logMessageContents)
        {
            if (theBillingCostDetails.Count > 0)
            {
                var results = _daInsightsDw.WriteBilling_BillCostDetailsData(theBillingCostDetails);
                if (results != theBillingCostDetails.Count)
                {
                    var logMessage = "Not all Billing Cost Detail data was inserted. Only " + results + " out of  " + theBillingCostDetails.Count + " saved to database."; 
                    statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + logMessage, StatusTypes.StatusSeverity.Error));
                    logMessageContents.Add(new LoggerMessageModel { LogLevel = NLog.LogLevel.Info, MessageToLog = logMessage });
                    emailMessageContents.Add(logMessage);
                }


            }
        }


        /// <summary>
        /// Write the Billing Service Cost Detail data to the database.
        /// </summary>
        /// <param name="serviceCostDetailsTable">The Billing Service Cost Detail data</param>
        /// <param name="statusList">A list of statuses.</param>
        /// <param name="emailMessageContents">Contents of error email message</param>
        /// <param name="logMessageContents">Messages for the log file</param>
        private void WriteBilling_ServiceCostDetailsData(List<Holding_Billing_ServiceCostDetails> serviceCostDetailsTable, ref StatusList statusList, ICollection<string> emailMessageContents, ICollection<LoggerMessageModel> logMessageContents)
        {
            if (serviceCostDetailsTable.Count > 0)
            {
                var results = _daInsightsDw.WriteBilling_ServiceCostDetailsData(serviceCostDetailsTable);
                if (results != serviceCostDetailsTable.Count)
                {
                    var logMessage =  "Not all Billing Service Cost Detail data was inserted. Only " + results + " out of  " + serviceCostDetailsTable.Count + " saved to database."; 
                    statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + logMessage, StatusTypes.StatusSeverity.Error));
                    logMessageContents.Add(new LoggerMessageModel { LogLevel = NLog.LogLevel.Info, MessageToLog = logMessage });
                    emailMessageContents.Add(logMessage);
                }


            }
        }

        /// <summary>
        /// Process the XML Node
        /// </summary>
        /// <param name="trackingid">The tracking Id</param>
        /// <param name="trackingdate">The tracking date</param>
        /// <param name="rootNode">The xml node</param>
        /// <param name="bills">The collection of Bills</param>
        /// <param name="billCostDetails">The collection of Bill Cost Details</param>
        /// <param name="serviceCostDetails">The collection of Bill Service Cost Details</param>
        private void ProcessNode(string trackingid, DateTime trackingdate, XmlNode rootNode, ICollection<Holding_Billing> bills, 
            ICollection<Holding_Billing_BillCostDetails> billCostDetails, ICollection<Holding_Billing_ServiceCostDetails> serviceCostDetails  )
        {
            //For some reason, customer is coming in as a child node, so just get all children
            var cNode = rootNode.FirstChild;
            if (!cNode.HasChildNodes)
            {
                return;
            }

            //Load the Billing Table
            foreach (XmlElement accountNode in cNode.ChildNodes)
            {
                if (!accountNode.HasChildNodes)
                {
                    continue;
                }

                foreach (XmlElement billNode in accountNode.ChildNodes)
                {
                    Guid? billingCostDetailsKey = null;
                    Guid? serviceCostDetailsKey = null;

                    // Handle BillCostDetails. 
                    foreach (XmlElement billcostDetailsNode in billNode.GetElementsByTagName("BillCostDetails"))
                    {
                        if (!billcostDetailsNode.HasAttributes)
                        {
                            continue;
                        }

                        billingCostDetailsKey = Guid.NewGuid();

                        foreach (XmlAttribute xmlAttribute in billcostDetailsNode.Attributes)
                        {
                            var billCostDetailsTemp = new Holding_Billing_BillCostDetails
                            {
                                BillingCostDetailName = xmlAttribute.Name,
                                BillingCostDetailValue = xmlAttribute.Value,
                                BillingCostDetailsKey = (Guid)billingCostDetailsKey
                            };


                            if (!string.IsNullOrEmpty(billCostDetailsTemp.BillingCostDetailName))
                            {
                                var returnVal = _daInsightsDw.IsValidNexusType(billCostDetailsTemp.BillingCostDetailName, billCostDetailsTemp.BillingCostDetailValue);


                                if (returnVal == -1)
                                {
                                    throw new ApplicationException("Invalid NexusType --  " + billCostDetailsTemp.BillingCostDetailName);
                                }

                                if (returnVal == -2)
                                {
                                    throw new ApplicationException("Invalid value " + billCostDetailsTemp.BillingCostDetailValue + " for NexusType " + billCostDetailsTemp.BillingCostDetailName);
                                }
                            }

                            billCostDetails.Add(billCostDetailsTemp);
                        }
                    }


                    foreach (XmlElement premiseNode in billNode.GetElementsByTagName("Premise"))
                    {

                        // Handle ServiceCostDetails.
                        foreach (XmlElement serviceCostDetailsNode in billNode.GetElementsByTagName("ServiceCostDetails"))
                        {
                            if (!serviceCostDetailsNode.HasAttributes)
                            {
                                continue;
                            }

                            serviceCostDetailsKey = Guid.NewGuid();

                            foreach (XmlAttribute xmlAttribute in serviceCostDetailsNode.Attributes)
                            {

                                var serviceCostDetailsTemp = new Holding_Billing_ServiceCostDetails
                                {
                                    ServiceCostDetailName = xmlAttribute.Name,
                                    ServiceCostDetailValue = xmlAttribute.Value,
                                    ServiceCostDetailsKey = (Guid) serviceCostDetailsKey
                                };

                                if (!string.IsNullOrEmpty(serviceCostDetailsTemp.ServiceCostDetailName))
                                {
                                    var returnVal = _daInsightsDw.IsValidNexusType(serviceCostDetailsTemp.ServiceCostDetailName, serviceCostDetailsTemp.ServiceCostDetailValue);


                                    if (returnVal == -1)
                                    {
                                        throw new ApplicationException("Invalid NexusType " + serviceCostDetailsTemp.ServiceCostDetailName);
                                    }

                                    if (returnVal == -2)
                                    {
                                        throw new ApplicationException("Invalid value " + serviceCostDetailsTemp.ServiceCostDetailValue + " for NexusType " + serviceCostDetailsTemp.ServiceCostDetailName);
                                    }
                                }

                                serviceCostDetails.Add(serviceCostDetailsTemp);
                            }
                        }

                        // Handle Service Node
                        foreach (XmlElement serviceNode in premiseNode.GetElementsByTagName("Service"))
                        {
                            var billTemp = new Holding_Billing
                            {
                                ClientId = _clientId,
                                CustomerId = Helpers.GetText(cNode, BillingNode_Customerid),
                                AccountId = Helpers.GetText(accountNode, BillingNode_Accountid),
                                PremiseId = Helpers.GetText(premiseNode, BillingNode_PremiseId),
                                BillStartDate = Convert.ToDateTime(Helpers.GetText(billNode, BillingNode_BillStartDate)),
                                BillEndDate = Convert.ToDateTime(Helpers.GetText(billNode, BillingNode_BillEndDate)),
                                TotalUnits = Convert.ToDecimal(Helpers.GetText(serviceNode, BillingNode_TotalUnits)),
                                TotalCost = Convert.ToDecimal(Helpers.GetText(serviceNode, BillingNode_TotalCost)),
                                CommodityId = Convert.ToInt32(Helpers.GetText(serviceNode, BillingNode_CommodityId)),
                                BillPeriodTypeId =Convert.ToInt32(Helpers.GetText(billNode, BillingNode_BillPeriodTypeId)),
                                BillCycleScheduleId = Helpers.GetText(billNode, BillingNode_BillCycleScheduleId),
                                UOMId = Convert.ToInt32(Helpers.GetText(serviceNode, BillingNode_UOM)),
                                ServiceContractId = Helpers.GetText(serviceNode, BillingNode_ServiceContractId),
                                ServicePointId = Helpers.GetText(serviceNode, BillingNode_ServicePointId, true),
                                MeterId = Helpers.GetText(serviceNode, BillingNode_MeterId),
                                AMIStartDate = Helpers.GetDate(serviceNode, BillingNode_AMIStartDate),
                                AMIEndDate = Helpers.GetDate(serviceNode, BillingNode_AMIEndDate),
                                RateClass = Helpers.GetText(serviceNode, BillingNode_RateClass),
                                MeterType = Helpers.GetText(serviceNode, BillingNode_MeterType),
                                ReplacedMeterId = Helpers.GetText(serviceNode, BillingNode_ReplacedMeterId),
                                ReadQuality = Helpers.GetText(serviceNode, BillingNode_ReadQuality),
                                ReadDate = Helpers.GetDate(serviceNode, BillingNode_ReadDate),
                                DueDate = Helpers.GetDate(billNode, BillingNode_DueDate),
                                TrackingId = trackingid,
                                TrackingDate = trackingdate,
                                SourceId =_daInsightsDw.ValidateAndGetSource(Helpers.GetText(cNode, BillingNode_Source)),
                                BillingCostDetailsKey = billingCostDetailsKey,
                                ServiceCostDetailsKey = serviceCostDetailsKey,
                                UtilityBillRecordId = Helpers.GetText(billNode, BillingNode_UtilityBillRecordId, true),
                                CanceledUtilityBillRecordId =Helpers.GetText(billNode, BillingNode_CanceledUtilityBillRecordId, true),
                                BillDate = Helpers.GetDate(billNode, BillingNode_BillDate),
                                BillDays = String.IsNullOrEmpty(Helpers.GetText(billNode, BillingNode_BillDays)) ? 0 : Convert.ToInt32(Helpers.GetText(billNode, BillingNode_BillDays))
                            };

                            // Check for a missing bill end date
                            if (billTemp.BillEndDate.Equals((DateTime)SqlDateTime.MinValue) || billTemp.BillEndDate.Equals((DateTime)SqlDateTime.Null))
                            {
                                throw new ApplicationException("Missing a Bill End Date for Customer " +
                                                               billTemp.CustomerId + "  - Account " + billTemp.AccountId);
                            }

                            // Calculate the bill days
                            if (billTemp.BillDays == 0)
                            {
                                billTemp.BillDays = billTemp.BillEndDate.Subtract(billTemp.BillStartDate).Days + 1;
                            }

                            // The bill start date must be earlier than the end date.
                            if (billTemp.BillEndDate.Subtract(billTemp.BillStartDate).Ticks < 0)
                            {
                                throw new ApplicationException(
                                    "Bill Start Date greater than Bill End Date for Customer " +
                                    billTemp.CustomerId + "  - Account " + billTemp.AccountId);
                            }

                            // Check if valid Meter type
                            if (!string.IsNullOrEmpty(billTemp.MeterType) && !_daInsightsDw.IsValidMeterType(billTemp.MeterType))
                            {
                                throw new ApplicationException("Invalid MeterType " + billTemp.MeterType + " for Customer " + billTemp.CustomerId + "  - Account " + billTemp.AccountId);
                            }

                            // Check if valid UOM type
                            if (!_daInsightsDw.IsValidUom(billTemp.UOMId))
                            {
                                throw new ApplicationException("Invalid UOM " + billTemp.UOMId + " for Customer " + billTemp.CustomerId + "  - Account " + billTemp.AccountId);
                            }

                            // Check if valid Commodity type
                            if (!_daInsightsDw.IsValidCommodity(billTemp.CommodityId))
                            {
                                throw new ApplicationException("Invalid Commodity " + billTemp.CommodityId + " for Customer " + billTemp.CustomerId + "  - Account " + billTemp.AccountId);
                            }

                            // Check if valid Bill Period type
                            if (!_daInsightsDw.IsValidBillPeriod(billTemp.BillPeriodTypeId))
                            {
                                throw new ApplicationException("Invalid Bill Period  : " + billTemp.BillPeriodTypeId + " for Customer " + billTemp.CustomerId + "  - Account " + billTemp.AccountId);
                            }

                            bills.Add(billTemp);
                        }
                    }
                }
            }
        }
    }
}

