﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData
{
    public static class InputFileXMLStream
    {
        private const string CustomerNode = "Customer";

        /// <summary>
        /// Get an XML document from a URI stream by XML Element
        /// </summary>
        /// <param name="uri">The URI</param>
        /// <returns>The XML Elements</returns>
        public static IEnumerable<XElement> StreamData(string uri)
        {
            var settings = new XmlReaderSettings
            {
                IgnoreWhitespace = true,
                IgnoreProcessingInstructions = true,
                IgnoreComments = true
            };

            using (var reader = XmlReader.Create(uri,settings))
            {
                reader.MoveToContent();

                // Parsing for the Customer nodes
                while (!reader.EOF)
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == CustomerNode)
                    {
                        var el = XNode.ReadFrom(reader) as XElement;
                        if (el != null)
                            yield return el;
                    }
                    else
                    {
                        reader.Read();
                    }
                }
            }
        }

        /// <summary>
        /// Convert an XML Element to an XElement
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static XElement GetXElement(this XmlNode node)
        {
            var xDoc = new XDocument();
            using (var xmlWriter = xDoc.CreateWriter())
            {
                node.WriteTo(xmlWriter);
            }
            return xDoc.Root;
        }

        /// <summary>
        /// Get an XML node from an XML Element
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static XmlNode GetXmlNode(this XElement element)
        {
            using (var xmlReader = element.CreateReader())
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(xmlReader);
                return xmlDoc;
            }
        }
    }
}
