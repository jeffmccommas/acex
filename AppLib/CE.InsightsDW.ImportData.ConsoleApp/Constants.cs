﻿namespace CE.InsightsDW.ImportData
{
    public class Constants
    {
        public const string FileNamespace = "Aclara:Insights";
        public const string RootNode = "Customers";
    }
}
