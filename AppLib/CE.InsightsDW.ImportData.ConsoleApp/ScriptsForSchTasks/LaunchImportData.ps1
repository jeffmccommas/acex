[CmdletBinding()]
Param(
        [Parameter(Mandatory=$True,Position=1)]
        [string]$Clientid = "",

        [Parameter(Mandatory=$True,Position=2)]
        [string]$Data = "",

        [Parameter(Mandatory=$True)]
        [string] $Env = ""
)

$theParms = "-c" + $ClientId + " -t" + $Data
write-host "theParms= " $theParms
write-host "Env= " $Env

$wdir = "E:\Aclarax\PaaS\InsightsDW.ImportDataConsoleApp"
$exe = $wdir + "\CE.InsightsDW.ImportData.ConsoleApp.exe"

$result = start-process -ArgumentList $theParms -filepath $exe -workingdirectory $wdir -Wait -Passthru -WindowStyle Hidden
$exitcode = $result.exitcode

$fullerrormessage = "InsightsDW Import Error Occurred for " + $Data + " File on " + $Env + ", ClientId = " + $Clientid  + " Return Code: " + $exitcode
$subject = "InsightsDW Import Error On " + $Env

if ($exitcode -gt 0)
{
   send-mailmessage -from "reply@aclara.com" -to "#cebiengineersdev@aclara.com" -subject $subject -body $fullerrormessage -priority High -smtpServer 10.153.19.6
}