﻿
@echo on
CLS

:: Get the loader tasks
(SCHTASKS /QUERY /FO:table /NH) ^| (FINDSTR /I ^^PaaS_InsightsBulkLoad) > EnableImportDataTasks.txt

:: Disable Them
FOR /F "tokens=1" %%A IN (EnableImportDataTasks.txt) do (
   SCHTASKS /Change /TN %%A /ENABLE
)

:: Delete the EnableImportDataTasks file
DEL EnableImportDataTasks.txt