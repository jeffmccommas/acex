﻿
@echo on
CLS

:: Get the loader tasks
(SCHTASKS /QUERY /FO:table /NH) ^| (FINDSTR /I ^^PaaS_InsightsBulkLoad) > DisableImportDataTasks.txt

:: Disable Them
FOR /F "tokens=1" %%A IN (DisableImportDataTasks.txt) do (
   SCHTASKS /Change /TN %%A /DISABLE
)

:: Delete the DisableImportDataTasks file
DEL DisableImportDataTasks.txt