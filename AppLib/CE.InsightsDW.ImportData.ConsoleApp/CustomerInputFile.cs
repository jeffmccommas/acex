﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using Aclara.UFx.StatusManagement;
using CE.InsightsDW.ImportData.Infastructure;
using CE.InsightsDW.ImportData.Infastructure.DataAccess;
using CE.InsightsDW.Model;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData
{
    public class CustomerInputFile : IProcessFile
    {

        private readonly int _clientId;
        private readonly IConfig _config;

        private const string NODE_CustomerId = "CustomerId";
        private const string NODE_City = "City";
        private const string NODE_Street2 = "Street2";
        private const string NODE_Street1 = "Street1";
        private const string NODE_State = "State";
        private const string NODE_MobileNumber = "MobilePhoneNumber";
        private const string NODE_PhoneNumber = "PhoneNumber";
        private const string NODE_LastName = "LastName";
        private const string NODE_IsBusiness = "IsBusiness";
        private const string NODE_IsAuthenticated = "IsAuthenticated";
        private const string NODE_FirstName = "FirstName";
        private const string NODE_EmailAddress = "EmailAddress";
        private const string NODE_Country = "Country";
        private const string NODE_AlternateEmailAddress = "AlternateEmailAddress";
        private const string NODE_Source = "Source";
        private const string NODE_PostalCode = "PostalCode";

        private const string ACCOUNTNODE_AccountId = "AccountId";
        private const string PREMISENODE_PremiseId = "PremiseId";
        private const string PREMISENODE_Street2 = "PremiseStreet2";
        private const string PREMISENODE_Street1 = "PremiseStreet1";
        private const string PREMISENODE_City = "PremiseCity";
        private const string PREMISENODE_State = "PremiseState";
        private const string PREMISENODE_PostalCode = "PremisePostalCode";
        private const string PREMISENODE_Latitude = "Latitude";
        private const string PREMISENODE_Longitude = "Longitude";
        private const string PREMISENODE_HasGasService = "HasGasService";
        private const string PREMISENODE_HasElectricService = "HasElectricService";
        private const string PREMISENODE_HasWaterService = "HasWaterService";
        private const string PREMISENODE_Country = "PremiseCountry";

        private Infastructure.InsightsDW _daInsightsDw;


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="options"></param>
        /// <param name="config"></param>
        public CustomerInputFile(Options options, IConfig config)
        {
            _clientId = Convert.ToInt32(options.ClientId);
            _config = config;

        }

        /// <summary>
        /// Process the Customer file
        /// </summary>
        /// <param name="theFile">The file to be processed</param>
        /// <param name="emailMessageContents">Contents of error email message</param>
        /// <param name="logMessageContents">Messages for the log file</param>
        /// <param name="statusList">The Status</param>
        /// <returns>The tracking id</returns>
        public string ProcessFile(FileInfo theFile, ICollection<string> emailMessageContents, ICollection<LoggerMessageModel> logMessageContents, 
            CountsForReportingModel captureCountsForReporting, out StatusList statusList)
        {

            var trackingid  = string.Empty;
            statusList = new StatusList();
            var nodeCounter = 0;
            var totalNodeCounter = 0;
            var processedNodeCounter = 0;
            var customers = new List<Holding_Customer>();
            var premises = new List<Holding_Premise>();

            _daInsightsDw = new Infastructure.InsightsDW(_config.ShardName);

            try
            {
                DateTime trackingIdDate;
                Helpers.SetTrackingData(theFile.Name, out trackingid, out trackingIdDate);

                var xsdFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + _config.InputXSD;
                var schemas = new XmlSchemaSet();
                schemas.Add(Constants.FileNamespace, XmlReader.Create(new StreamReader(xsdFile)));

                // This logic sets-up a custom iteration using linq and yield.  Each element is traversed forward-only, greatly saving resources.
                var getCustomerElements =InputFileXMLStream.StreamData(theFile.FullName).Select(el => el);
                
                foreach (var element in getCustomerElements)
                {
                    //Validate the node
                    var errors = false;
                    var errStatusList = new StatusList();
                    var doc = new XDocument(new XElement(Constants.RootNode));
                    var xElement = doc.Element(Constants.RootNode);
                    xElement?.Add(element);

                    doc.Validate(schemas, (o, e) =>
                    {
                        errStatusList.Add(new Status(e.Message, StatusTypes.StatusSeverity.Error));

                        var elementData = string.Concat(element.Elements());

                        _daInsightsDw.LogErrorsByInputType(
                            Helpers.ExtractElements(elementData, DWImportConstants.RunType.Customer, _clientId,
                                trackingid), e.Message, theFile.Name, DWImportConstants.RunType.Customer,
                            _config.ImportSpecificLoggingOn);

                        logMessageContents.Add(new LoggerMessageModel
                        {
                            LogLevel = NLog.LogLevel.Error,
                            Exception = new InsightsDWImportException(
                                $"Validation Error for file {theFile.Name} data: {elementData}",
                                e.Exception)
                        });

                        errors = true;

                    }, true);


                    if (nodeCounter == 0)
                    {
                        //Create the datatable for storage
                        customers = new List<Holding_Customer>();
                        premises = new List<Holding_Premise>();
                    }

                    // Main Call
                    if (!errors)
                    {
                        try
                        {

                            var xmlNode = InputFileXMLStream.GetXmlNode(element);
                            ProcessNode(trackingid, trackingIdDate, xmlNode, customers, premises);
                            nodeCounter++;

                        }
                        catch (Exception eX)
                        {
                            errStatusList.Add(new Status(eX.Message, StatusTypes.StatusSeverity.Error));

                            var elementData = string.Concat(element.Elements());

                            _daInsightsDw.LogErrorsByInputType(
                                Helpers.ExtractElements(elementData, DWImportConstants.RunType.Profile, _clientId, trackingid),
                                eX.Message, theFile.Name, DWImportConstants.RunType.Profile, _config.ImportSpecificLoggingOn);

                            logMessageContents.Add(new LoggerMessageModel
                            {
                                LogLevel = NLog.LogLevel.Error,
                                Exception =
                                    new InsightsDWImportException(
                                        $"Processing Error for file {theFile.Name} data: " + eX.Message)
                            });

                            errors = true;
                        }
                    }

                    if (errors)
                    {
                        statusList.AddRange(errStatusList);
                        errors = false;
                        errStatusList.Clear();
                    }

                    totalNodeCounter++;

                    // Write out data if reached max batch size
                    if (customers.Count >= _config.MaxBatchSize)
                    {
                        processedNodeCounter += nodeCounter;
                        nodeCounter = 0;

                        var results = _daInsightsDw.WriteCustomerData(customers);
                        if (results != customers.Count)
                        {
                            var logMessage1 = "Not all Customer data was inserted. Only " + results + " out of  " + customers.Count + " saved to database.";
                            statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + logMessage1, StatusTypes.StatusSeverity.Error));
                            logMessageContents.Add(new LoggerMessageModel { LogLevel = NLog.LogLevel.Info, MessageToLog = logMessage1 });
                            emailMessageContents.Add(logMessage1);
                        }


                        results = _daInsightsDw.WritePremiseData(premises);
                        if (results != customers.Count)
                        {
                            var logMessage1 = "Not all Premise data was inserted. Only " + results + " out of  " + premises.Count + " saved to database.";
                            statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + logMessage1, StatusTypes.StatusSeverity.Error));
                            logMessageContents.Add(new LoggerMessageModel { LogLevel = NLog.LogLevel.Info, MessageToLog = logMessage1 });
                            emailMessageContents.Add(logMessage1);
                        }

                        customers.Clear();
                    }
                }


                // End of loop. Write what is left.
                if (nodeCounter > 0 )
                {
                    //Less than max rows, so write it out
                    processedNodeCounter += nodeCounter;
                    var results = _daInsightsDw.WriteCustomerData(customers);

                    if (results != customers.Count)
                    {
                        var logMessage1 =  "Not all Customer data was inserted. Only " + results + " out of  " + customers.Count + " saved to database.";
                        statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + logMessage1, StatusTypes.StatusSeverity.Error));
                        logMessageContents.Add(new LoggerMessageModel { LogLevel = NLog.LogLevel.Info, MessageToLog = logMessage1 });
                        emailMessageContents.Add(logMessage1);
                    }


                    results = _daInsightsDw.WritePremiseData(premises);
                    if (results != customers.Count)
                    {
                        var logMessage1 = "Not all Premise data was inserted. Only " + results + " out of  " + premises.Count + " saved to database.";
                        statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + logMessage1, StatusTypes.StatusSeverity.Error));
                        logMessageContents.Add(new LoggerMessageModel { LogLevel = NLog.LogLevel.Info, MessageToLog = logMessage1 });
                        emailMessageContents.Add(logMessage1);
                    }

                }


                var logMessage = "Processed " + processedNodeCounter + " Customers out of " + totalNodeCounter + " <<<<"; 

                statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + logMessage, StatusTypes.StatusSeverity.Informational));
                logMessageContents.Add(new LoggerMessageModel { LogLevel = NLog.LogLevel.Info, MessageToLog = logMessage });
                emailMessageContents.Add(logMessage);

                captureCountsForReporting.Count = processedNodeCounter;
                captureCountsForReporting.Comment = logMessage;

            }
            catch (Exception ex)
            {
                statusList.Add(new Status(ex, StatusTypes.StatusSeverity.Error));
                logMessageContents.Add(new LoggerMessageModel
                {
                    LogLevel = NLog.LogLevel.Error,
                    Exception = null,
                    MessageToLog = ex.Message
                });
            }
            
            return trackingid;

        }

        /// <summary>
        /// Process the XML Node
        /// </summary>
        /// <param name="trackingid">The tracking Id</param>
        /// <param name="trackingdate">The tracking date</param>
        /// <param name="rootNode">The xml node</param>
        /// <param name="customers">The customer data table</param>
        /// <param name="premises">The premise data table</param>
        private void ProcessNode(string trackingid, DateTime trackingdate, XmlNode rootNode, 
            ICollection<Holding_Customer> customers,   ICollection<Holding_Premise> premises)
        {
            //For some reason, customer is coming in as a child node, so just get all children
            var cNode = rootNode.FirstChild;
            if (!cNode.HasChildNodes)
            {
                return;
            }

            var customerTemp = new Holding_Customer
            {
                City = Helpers.GetText(cNode, NODE_City),
                CustomerID = Helpers.GetText(cNode, NODE_CustomerId),
                Street2 = Helpers.GetText(cNode, NODE_Street2),
                Street1 = Helpers.GetText(cNode, NODE_Street1),
                State = Helpers.GetText(cNode, NODE_State),
                postalcode = Helpers.GetText(cNode, NODE_PostalCode),
                ClientID = _clientId,
                MobilePhoneNumber = Helpers.GetText(cNode, NODE_MobileNumber),
                PhoneNumber = Helpers.GetText(cNode, NODE_PhoneNumber),
                LastName = Helpers.GetText(cNode, NODE_LastName),
                FirstName = Helpers.GetText(cNode, NODE_FirstName),
                EmailAddress = Helpers.GetText(cNode, NODE_EmailAddress),
                Country = Helpers.GetText(cNode, NODE_Country),
                AlternateEmailAddress = Helpers.GetText(cNode, NODE_AlternateEmailAddress),
                TrackingId = trackingid,
                TrackingDate = trackingdate,
                SourceId = _daInsightsDw.ValidateAndGetSource(Helpers.GetText(cNode, NODE_Source)),
            };


            // Check if valid Source type

            // Check if valid State/Province and Country
            if (!_daInsightsDw.IsValidStateProvinceAndCountry(customerTemp.State, customerTemp.Country))
            {
                throw new ApplicationException("Invalid Customer  State " + customerTemp.State + " and Country " + customerTemp.Country+ " combination for Customer " + customerTemp.CustomerID);
            }

            bool flag;
            if (bool.TryParse(Helpers.GetText(cNode, NODE_IsBusiness), out flag))
            {
                customerTemp.IsBusiness = flag;
            }
            else
            {
                customerTemp.IsBusiness = null;
            }

            if (bool.TryParse(Helpers.GetText(cNode, NODE_IsAuthenticated), out flag))
            {
                customerTemp.IsAuthenticated = flag;
            }
            else
            {
                customerTemp.IsAuthenticated = null;
            }

            customers.Add(customerTemp);

            //Load the Premise Table
            foreach (XmlElement accountNode in cNode.ChildNodes)
            {
                if (!accountNode.HasChildNodes)
                {
                    continue;
                }

                foreach (XmlElement premiseNode in accountNode.ChildNodes)
                {
                    var premiseTemp = new Holding_Premise
                    {
                        City = Helpers.GetText(premiseNode, PREMISENODE_City),
                        ClientID = _clientId,
                        Country = Helpers.GetText(premiseNode, PREMISENODE_Country),
                        CustomerID = customerTemp.CustomerID,
                        AccountID = Helpers.GetText(accountNode, ACCOUNTNODE_AccountId),

                        ElectricService =
                            Convert.ToInt32(
                                Convert.ToBoolean(Helpers.GetText(premiseNode, PREMISENODE_HasElectricService))),

                        GasService =
                            Convert.ToInt32(Convert.ToBoolean(Helpers.GetText(premiseNode, PREMISENODE_HasGasService))),

                        WaterService =
                            Convert.ToInt32(Convert.ToBoolean(Helpers.GetText(premiseNode, PREMISENODE_HasWaterService))),

                        postalcode = Helpers.GetText(premiseNode, PREMISENODE_PostalCode),
                        PremiseID = Helpers.GetText(premiseNode, PREMISENODE_PremiseId),

                        SourceId = customerTemp.SourceId,

                        State = Helpers.GetText(premiseNode, PREMISENODE_State),
                        Street1 = Helpers.GetText(premiseNode, PREMISENODE_Street1),
                        Street2 = Helpers.GetText(premiseNode, PREMISENODE_Street2),

                        Latitude =
                            Helpers.GetText(premiseNode, PREMISENODE_Latitude) == string.Empty
                                ? (double?) null
                                : Convert.ToDouble(Helpers.GetText(premiseNode, PREMISENODE_Latitude)),

                        Longitude =
                            Helpers.GetText(premiseNode, PREMISENODE_Longitude) == string.Empty
                                ? (double?) null
                                : Convert.ToDouble(Helpers.GetText(premiseNode, PREMISENODE_Longitude)),

                        TrackingDate = trackingdate,
                        TrackingID = trackingid
                    };

                    // Check if valid State/Province and Country
                    if (!_daInsightsDw.IsValidStateProvinceAndCountry(premiseTemp.State, premiseTemp.Country))
                    {
                        throw new ApplicationException("Invalid Customer Premise  State " + premiseTemp.State + " and Country " + premiseTemp.Country + 
                            " combination for Customer " + premiseTemp.CustomerID + " Premise " + premiseTemp.PremiseID);
                    }

                    premises.Add(premiseTemp);
                }
            }
        }
    }
}
