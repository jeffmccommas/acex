﻿using System;
using CE.InsightsDW.ImportData.Infastructure;

namespace CE.InsightsDW.ImportData
{
    public class InsightsDWImportClassFactory
    {
        private const string CustomerFileType = "cust";
        private const string BillingFileType = "bill";
        private const string ProfileFileType = "prof";
        private const string ActionItemFileType = "act";

        /// <summary>
        /// Get metadata access
        /// </summary>
        /// <returns></returns>
        public IMetaDataAccess CreateMetaDataAccess()
        {
            return new InsightsMetadata();
        }

        /// <summary>
        /// Create a InsightsDWImport
        /// </summary>
        /// <param name="options">The program options</param>
        /// <param name="config">The current config</param>
        /// <param name="log">The log file</param>
        /// <returns>A instance of IProcessFile</returns>
        public IProcessFile InsightsDWImport(Options options,IConfig config)
        {
            IProcessFile processFile;

            if (config == null)
            {
                config = CreateConfig(options);
            }

            switch (options.FileType)
            {
                case CustomerFileType:
                    processFile = new CustomerInputFile(options, config);
                    break;

                case BillingFileType:
                    processFile =  new BillInputFile(options, config);
                    break;

                case ProfileFileType:
                    processFile = new ProfileInputFile(options, config);
                    break;

                case ActionItemFileType:
                    processFile = new ActionItemFile(options, config);
                    break;

                default:
                    processFile = new CustomerInputFile(options,config);
                    break;
            }
            return processFile;
        }

        /// <summary>
        /// Create a FileLoad Configuration
        /// </summary>
        /// <param name="options">The program options</param>
        /// <returns>The FileLoadConfiguration</returns>
        public AppConfiguration CreateConfig(Options options)
        {
            try
            {
                var config = new AppConfiguration(Convert.ToInt32(options.ClientId));

                switch (options.FileType)
                {
                    case CustomerFileType:
                        config.LoadCustomerConfiguration();
                        break;

                    case BillingFileType:
                        config.LoadBillingConfiguration();
                        break;

                    case ProfileFileType:
                        config.LoadProfileConfiguration();
                        break;

                    case ActionItemFileType:
                        config.LoadActionItemConfiguration();
                        break;
                }
                return config;
            }
            catch (Exception eX)
            {
                throw new InsightsDWImportException("Cannot Create Configuration. Check InsightsDW Import Data Configuration for ClientId = " + options.ClientId + " : " + eX.Message);
            }
        }
    }
}
