﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using Aclara.UFx.StatusManagement;
using CE.InsightsDW.ImportData.Infastructure;
using CE.InsightsDW.ImportData.Infastructure.DataAccess;
using CE.InsightsDW.Model;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData
{
    public class ActionItemFile : IProcessFile
    {
        private readonly int _clientId;
        private readonly IConfig _config;

        private const string PremiseNode_Customerid = "CustomerId";
        private const string PremiseNode_Accountid = "AccountId";
        private const string PremiseNode_PremiseId = "PremiseId";
        private const string ActionItemNode_ActionKey = "ActionKey";
        private const string ActionItemNode_SubActionKey = "SubActionKey";
        private const string ActionItemNode_Status = "Status";
        private const string ActionItemNode_StatusDate = "StatusDate";
        private const string ActionKeyData_ActionData = "ActionData";
        private const string ActionKeyData_ActionValue = "ActionValue";

        private Infastructure.InsightsDW _daInsightsDw;
        private InsightsMetadata _daMetadata;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="options"></param>
        /// <param name="config"></param>
        public ActionItemFile(Options options, IConfig config)
        {
            _clientId = Convert.ToInt32(options.ClientId);
            _config = config;
        }

        /// <summary>
        /// Process the Action Item file
        /// </summary>
        /// <param name="theFile">The file to be processed</param>
        /// <param name="emailMessageContents">Contents of error email message</param>
        /// <param name="logMessageContents">Messages for the log file</param>
        /// <param name="captureCountsForReporting">Capture count data for reporting</param>
        /// <param name="statusList">The Status</param>
        /// <returns>The tracking id</returns>
        public string ProcessFile(FileInfo theFile, ICollection<string> emailMessageContents, ICollection<LoggerMessageModel> logMessageContents,
            CountsForReportingModel captureCountsForReporting, out StatusList statusList)
        {
            var trackingid = string.Empty;
            statusList = new StatusList();
            var nodeCounter = 0;
            var totalNodeCounter = 0;
            var processedNodeCounter = 0;

            var actionItems = new List<Holding_ActionItem>();
            _daInsightsDw = new Infastructure.InsightsDW(_config.ShardName);
            _daMetadata = new InsightsMetadata();

            try
            {
                DateTime trackingIdDate;
                Helpers.SetTrackingData(theFile.Name, out trackingid, out trackingIdDate);
                var xsdFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" +
                                  _config.InputXSD;

                // This logic sets-up a custom iteration using linq and yield.  Each element is traversed forward-only, greatly saving resources.
                var getCustomerElements = InputFileXMLStream.StreamData(theFile.FullName).Select(el => el);

                var schemas = new XmlSchemaSet();
                schemas.Add(Constants.FileNamespace, XmlReader.Create(new StreamReader(xsdFile)));

                foreach (var element in getCustomerElements)
                {
                    //Validate the node
                    var errors = false;
                    var errStatusList = new StatusList();
                    var doc = new XDocument(new XElement(Constants.RootNode));
                    var xElement = doc.Element(Constants.RootNode);
                    xElement?.Add(element);

                    doc.Validate(schemas, (o, e) =>
                    {
                        errStatusList.Add(new Status(e.Message, StatusTypes.StatusSeverity.Error));

                        var elementData = string.Concat(element.Elements());

                        _daInsightsDw.LogErrorsByInputType(
                            Helpers.ExtractElements(elementData, DWImportConstants.RunType.ActionItem, _clientId,
                                trackingid), e.Message, theFile.FullName, DWImportConstants.RunType.ActionItem,
                            _config.ImportSpecificLoggingOn);

                        logMessageContents.Add(new LoggerMessageModel
                        {
                            LogLevel = NLog.LogLevel.Error,
                            Exception = new InsightsDWImportException(
                                $"Validation Error for file {theFile.Name} data: {elementData}",
                                e.Exception)
                        });

                        errors = true;

                    }, true);

                    if (nodeCounter == 0)
                    {
                        //Create the datatable for storage
                        actionItems = new List<Holding_ActionItem>();
                    }

                    // Main Call
                    if (!errors)
                    {
                        try
                        {
                            var xmlNode = InputFileXMLStream.GetXmlNode(element);
                            ProcessNode(trackingid, trackingIdDate, xmlNode, actionItems);
                            nodeCounter++;

                        }
                        catch (Exception eX)
                        {
                            errStatusList.Add(new Status(eX.Message, StatusTypes.StatusSeverity.Error));

                            var elementData = string.Concat(element.Elements());

                            _daInsightsDw.LogErrorsByInputType(
                                Helpers.ExtractElements(elementData, DWImportConstants.RunType.ActionItem, _clientId,
                                    trackingid),
                                eX.Message, theFile.Name, DWImportConstants.RunType.ActionItem,
                                _config.ImportSpecificLoggingOn);

                            logMessageContents.Add(new LoggerMessageModel
                            {
                                LogLevel = NLog.LogLevel.Error,
                                Exception =
                                    new InsightsDWImportException(
                                        $"Processing Error for file {theFile.Name} data: " + eX.Message)
                            });

                            errors = true;
                        }
                    }

                    if (errors)
                    {
                        statusList.AddRange(errStatusList);
                        errors = false;
                        errStatusList.Clear();
                    }

                    totalNodeCounter++;

                    // Write out data if reached max batch size
                    if (actionItems.Count >= _config.MaxBatchSize)
                    {
                        processedNodeCounter += nodeCounter;
                        var results = _daInsightsDw.WriteActionKeyData(actionItems);
                        nodeCounter = 0;
                        if (results != actionItems.Count)
                        {
                            var logMessage1 = "Not all Action Item data was inserted. Only " + results + " out of  " +
                                              actionItems.Count + " saved to database.";
                            statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + logMessage1,
                                StatusTypes.StatusSeverity.Error));
                            logMessageContents.Add(
                                new LoggerMessageModel {LogLevel = NLog.LogLevel.Info, MessageToLog = logMessage1});
                            emailMessageContents.Add(logMessage1);
                        }

                        actionItems.Clear();
                    }
                }

                // End of loop. Write what is left.
                if (actionItems.Count > 0)
                {
                    processedNodeCounter += nodeCounter;
                    var results = _daInsightsDw.WriteActionKeyData(actionItems);

                    if (results != actionItems.Count)
                    {
                        var logMessage2 =  "Not all Action Item data was inserted. Only " + results + " out of  " + actionItems.Count + " saved to database."; 
                        statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + logMessage2, StatusTypes.StatusSeverity.Error));
                        logMessageContents.Add(new LoggerMessageModel { LogLevel = NLog.LogLevel.Info, MessageToLog = logMessage2 });
                        emailMessageContents.Add(logMessage2);
                    }
                }

                var logMessage = ">>> Processed " +  processedNodeCounter + " Action Items out of " + totalNodeCounter + " <<<<";

                statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + logMessage, StatusTypes.StatusSeverity.Informational));
                logMessageContents.Add(new LoggerMessageModel { LogLevel = NLog.LogLevel.Info, MessageToLog = logMessage });
                emailMessageContents.Add(logMessage);

                captureCountsForReporting.Count = processedNodeCounter;
                captureCountsForReporting.Comment = logMessage;

            }
            catch (Exception ex)
            {
                statusList.Add(new Status(ex, StatusTypes.StatusSeverity.Error));
                logMessageContents.Add(new LoggerMessageModel
                {
                    LogLevel = NLog.LogLevel.Error,
                    Exception = null,
                    MessageToLog = ex.Message
                });

            }

            return trackingid;
        }

        /// <summary>
        /// Process the XML Node
        /// </summary>
        /// <param name="trackingid">The tracking Id</param>
        /// <param name="trackingdate">The tracking date</param>
        /// <param name="rootNode">The xml node</param>
        /// <param name="actionItems">The actionItems to save</param>
        private void ProcessNode(string trackingid, DateTime trackingdate, XmlNode rootNode,
            ICollection<Holding_ActionItem> actionItems)
        {
            //For some reason, customer is coming in as a child node, so just get all children
            var cNode = rootNode.FirstChild;
            if (!cNode.HasChildNodes)
            {
                return;
            }

            //Load the Premise Attribute Table
            foreach (XmlElement accountNode in cNode.ChildNodes)
            {
                if (!accountNode.HasChildNodes)
                {
                    continue;
                }

                foreach (XmlElement premiseNode in accountNode.ChildNodes)
                {
                    foreach (XmlElement actionNode in premiseNode.ChildNodes)
                    {
                        foreach (XmlElement actionDataNode in actionNode.ChildNodes)
                        {
                            var actionItemTemp = new Holding_ActionItem
                            {
                                ClientId = _clientId,
                                CustomerId = Helpers.GetText(cNode, PremiseNode_Customerid),
                                AccountID = Helpers.GetText(accountNode, PremiseNode_Accountid),
                                PremiseId = Helpers.GetText(premiseNode, PremiseNode_PremiseId),
                                ActionKey = Helpers.GetText(actionNode, ActionItemNode_ActionKey),
                                SubActionKey = Helpers.GetText(actionNode, ActionItemNode_SubActionKey),
                                ActionData = Helpers.GetText(actionDataNode, ActionKeyData_ActionData),
                                ActionDataValue = Helpers.GetText(actionDataNode, ActionKeyData_ActionValue),
                                TrackingID = trackingid,
                                TrackingDate = trackingdate,
                                StatusId = _daMetadata.IsValidActionStatus(Helpers.GetText(actionNode, ActionItemNode_Status)),
                                SourceId = _daInsightsDw.ValidateAndGetSource(Helpers.GetText(cNode, ActionItemNode_Status))
                            };

                            var strDateTime = Helpers.GetText(actionNode, ActionItemNode_StatusDate);
                            actionItemTemp.StatusDate = Convert.ToDateTime(strDateTime.Length == 0 ? "1900-01-01" : strDateTime);

                            // Check if valid Action Data Key
                            if (!_daMetadata.IsValidActionKey(actionItemTemp.ActionKey, _clientId))
                            {
                                throw new ApplicationException("Invalid Action Key " + actionItemTemp.ActionKey + " for Bill Customer " + actionItemTemp.CustomerId + "  - Account " + actionItemTemp.AccountID);
                            }

                            actionItems.Add(actionItemTemp);
                        }
                    }
                }
            }
        }
    }
}