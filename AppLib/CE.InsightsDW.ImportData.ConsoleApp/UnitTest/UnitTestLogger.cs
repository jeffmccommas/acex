﻿using System;
using NLog;
using NLog.Targets;

namespace CE.InsightsDW.ImportData.UnitTest
{
    public class UnitTestLogger : Infastructure.ILogger
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public string trackingId;
        private const string propTrackingId = "TrackingId";
        private const string propClientId = "ClientId";
        public int clientId = 0;


        public UnitTestLogger(string logFileName, string trackingId, int clientId)
        {
                var target = LogManager.Configuration.FindTargetByName("textFile") as FileTarget;
            if (target != null)
            {
                target.FileName = logFileName;
            }
            TrackingId = trackingId;
            ClientId = clientId;
        }

        public string TrackingId { get; set; }

        public int ClientId { get; set; }

        private void LogEvent(LogLevel logLevel, string message)
        {
            var theEvent = new LogEventInfo(logLevel, "", message);
            theEvent.Properties[propTrackingId] = TrackingId;
            theEvent.Properties[propClientId] = ClientId;
            Logger.Log(theEvent);
        }

        private void LogEventException(LogLevel logLevel, Exception ex)
        {
            var theEvent = new LogEventInfo(logLevel, "", ex.ToString()) { Exception = ex };
            theEvent.Properties[propTrackingId] = TrackingId;
            theEvent.Properties[propClientId] = ClientId;
            Logger.Log(theEvent);
        }


        public void Info(string trackingid, string message)
        {
            LogEvent(LogLevel.Info, message);
        }

        public void Warn(string trackingid, string message)
        {
            throw new NotImplementedException();
        }

        public void Debug(string trackingid, string message)
        {
            throw new NotImplementedException();
        }

        public void Error(string trackingid, string message)
        {
            LogEvent(LogLevel.Error, message);
        }

        public void ErrorException(string trackingid, Exception ex)
        {
            LogEventException(LogLevel.Error, ex);
        }

        public void Fatal(string trackingid, string message)
        {
            throw new NotImplementedException();
        }

        public void SetName(string logFileName)
        {
            var target = LogManager.Configuration.FindTargetByName("textFile") as FileTarget;
            if (target != null)
            {
                target.FileName = logFileName;
            }
        }




    }
}
