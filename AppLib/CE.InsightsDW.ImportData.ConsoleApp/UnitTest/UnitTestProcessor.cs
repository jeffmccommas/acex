﻿using System;
using System.Collections.Generic;
using System.IO;
using Aclara.UFx.StatusManagement;
using CE.InsightsDW.ImportData.Infastructure;
using CE.InsightsDW.ImportData.Infastructure.DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData.UnitTest
{
    [TestClass]
    public class UnitTestProcessor
    {
        public TestContext TestContext { get; set; }

        private Infastructure.InsightsDW _daInsightsDw;
        private InsightsMetadata _daMetadata;

        [TestInitialize]
        public void FixEfProviderServicesProblem()
        {
            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded. 
            //Make sure the provider assembly is available to the running application. 
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            // ReSharper disable once UnusedVariable
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;

            _daInsightsDw = new Infastructure.InsightsDW();
            _daMetadata = new InsightsMetadata();
        }


        [TestMethod]
        public void ExtractElementCustomerId()
        {
            var cust = @"PremiseId=""12355""";
            var result = Helpers.ExtractElement(cust, "PremiseId");

            Assert.AreEqual("12355", result);
        }

        [TestMethod]
        public void ExtractElementCustomerIdNoQuotes()
        {
            var cust = "PremiseId=12355";
            var result = Helpers.ExtractElement(cust, "PremiseId");

            Assert.AreEqual("12355", result);
        }

        [TestMethod]
        public void IsValidUomTest()
        {
            var trueUomId = 9;
            var falseUomId = 999999;

            Assert.IsTrue(_daInsightsDw.IsValidUom(trueUomId));
            Assert.IsFalse(_daInsightsDw.IsValidUom(falseUomId));

        }

        [TestMethod]
        public void IsValidCommodityTest()
        {
            var trueId = 9;
            var falseId = 99;

            Assert.IsTrue(_daInsightsDw.IsValidCommodity(trueId));

            Assert.IsTrue(!_daInsightsDw.IsValidCommodity(falseId));

        }

        [TestMethod]
        public void IsValidBillPeriodTest()
        {
            var trueId = 1;
            var falseId = 99;

            Assert.IsTrue(_daInsightsDw.IsValidBillPeriod(trueId));

            Assert.IsFalse(_daInsightsDw.IsValidBillPeriod(falseId));

        }

        [TestMethod]
        public void IsValidMeterTypeTest()
        {
            var trueId = "AMR";
            var falseId = "GEORGE";

            Assert.IsTrue(_daInsightsDw.IsValidMeterType(trueId));

            Assert.IsFalse(_daInsightsDw.IsValidMeterType(falseId));

        }

        [TestMethod]
        public void ValidateAndGetSourceTest()
        {
            var trueId = "thirdparty";
            var falseId = "GEORGE";

            Assert.IsTrue(_daInsightsDw.ValidateAndGetSource(trueId) == 7);

            Assert.IsTrue(_daInsightsDw.ValidateAndGetSource(falseId) == -1);

        }


        [TestMethod]
        public void IsValidStateProvinceAndCountryTest()
        {
            var validState = "MA";
            var validCountry = "US";
            var falseState = "QQ";
            var falseCountry = "UK";

            Assert.IsTrue(_daInsightsDw.IsValidStateProvinceAndCountry(validState, validCountry));

            Assert.IsFalse(_daInsightsDw.IsValidStateProvinceAndCountry(falseState, validCountry));

            Assert.IsFalse(_daInsightsDw.IsValidStateProvinceAndCountry(validState, falseCountry));

            Assert.IsFalse(_daInsightsDw.IsValidStateProvinceAndCountry(falseState, falseCountry));

        }

        [TestMethod]
        public void IsValidActionKeyTest()
        {
            var clientId = 0;
            var trueId = "combustionsafetytest";
            var falseId = "GEORGE";

            Assert.IsTrue(_daMetadata.IsValidActionKey(trueId, clientId));

            Assert.IsFalse(_daMetadata.IsValidActionKey(falseId, clientId));

        }

        [TestMethod]
        public void IsValidClientProfileAttributeTest()
        {
            var trueId = "airpurifier.count";
            var falseId = "GEORGE";

            Assert.IsTrue(_daMetadata.IsValidClientProfileAttribute(trueId));

            Assert.IsFalse(_daMetadata.IsValidClientProfileAttribute(falseId));

        }


        [TestMethod]
        public void IsValidActionStatusTest()
        {
            var trueId = "rejected";
            var falseId = "GEORGE";


            Assert.IsTrue(_daMetadata.IsValidActionStatus(trueId) == 5);

            Assert.IsTrue(_daMetadata.IsValidActionStatus(falseId) == -1);
        }





        [TestMethod]
        [DeploymentItem(@"TestData\")]
        [DeploymentItem("customerinfo.xsd")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestData\TestAllFileTypes.csv", "TestAllFileTypes#csv", DataAccessMethod.Sequential)]
        public void RunCustomer()
        {
            try
            {
                var options = new Options
                {
                    ClientId = Convert.ToInt32(TestContext.DataRow["ClientId"].ToString()),
                    FileType = "cust"
                };

                var customerFileName = TestContext.DataRow["customerfilename"].ToString();

                var log = new UnitTestLogger("debugrun.txt", "1", options.ClientId);

                var config = new AppConfiguration(Convert.ToInt32(options.ClientId));
                config.LoadCustomerConfiguration();

                var custInputFile = new CustomerInputFile(options, config);

                var file = new FileInfo(customerFileName);

                var emailMessageContents = new List<string>();
                var logMessageAdds = new List<LoggerMessageModel>();
                var captureCountsForReporting = new CountsForReportingModel
                {
                    ClientId = options.ClientId,
                    FileName = customerFileName,
                    ProcessType = "ImportToInsightsDW_" + options.FileType.ToUpper()
                };

                StatusList statusList;
                var trackingId = custInputFile.ProcessFile(file, emailMessageContents, logMessageAdds, captureCountsForReporting, out statusList);

                if (statusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error))
                {
                    log.Error(trackingId, statusList.GetStatusListAsExceptionString(StatusTypes.StatusSeverity.Error));
                    Assert.Fail();
                }
            }
            catch (Exception)
            {
                Assert.Fail();
            }

        }


        [TestMethod]
        [DeploymentItem(@"TestData\")]
        [DeploymentItem("actionitems.xsd")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestData\TestAllFileTypes.csv", "TestAllFileTypes#csv", DataAccessMethod.Sequential)]
        public void RunActionItem()
        {
            try
            {
                var options = new Options
                {
                    ClientId = Convert.ToInt32(TestContext.DataRow["ClientId"].ToString()),
                    FileType = "act"
                };


                var actionItemFileName = TestContext.DataRow["actionfilename"].ToString();

                var log = new UnitTestLogger("debugrun.txt", "1", options.ClientId);

                var config = new AppConfiguration(Convert.ToInt32(options.ClientId));
                config.LoadActionItemConfiguration();

                var actionItemFile = new ActionItemFile(options, config);

                var file = new FileInfo(actionItemFileName);

                var emailMessageContents = new List<string>();
                var logMessageAdds = new List<LoggerMessageModel>();
                var captureCountsForReporting = new CountsForReportingModel
                {
                    ClientId = options.ClientId,
                    FileName = actionItemFileName,
                    ProcessType = "ImportToInsightsDW_" + options.FileType.ToUpper()
                };

                StatusList statusList;
                var trackingId = actionItemFile.ProcessFile(file, emailMessageContents, logMessageAdds, captureCountsForReporting, out statusList);

                if (statusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error))
                {
                    log.Error(trackingId, statusList.GetStatusListAsExceptionString(StatusTypes.StatusSeverity.Error));
                    Assert.Fail();
                }
            }
            catch (Exception)
            {
                Assert.Fail();
            }

        }

        [TestMethod]
        [DeploymentItem(@"TestData\")]
        [DeploymentItem("billingdata.xsd")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestData\TestAllFileTypes.csv", "TestAllFileTypes#csv", DataAccessMethod.Sequential)]
        public void RunBilling()
        {
            try
            {
                var options = new Options
                {
                    ClientId = Convert.ToInt32(TestContext.DataRow["ClientId"].ToString()),
                    FileType = "bill"
                };

                var billingFileName = TestContext.DataRow["billingfilename"].ToString();

                var log = new UnitTestLogger("debugrun.txt", "1", 87);

                var config = new AppConfiguration(Convert.ToInt32(options.ClientId));
                config.LoadBillingConfiguration();

                var bulkFile = new BillInputFile(options, config);

                var file = new FileInfo(billingFileName);

                var emailMessageContents = new List<string>();
                var logMessageAdds = new List<LoggerMessageModel>();
                var captureCountsForReporting = new CountsForReportingModel
                {
                    ClientId = options.ClientId,
                    FileName = billingFileName,
                    ProcessType = "ImportToInsightsDW_" + options.FileType.ToUpper()
                };

                StatusList statusList;
                var trackingId = bulkFile.ProcessFile(file, emailMessageContents, logMessageAdds, captureCountsForReporting, out statusList);

                if (statusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error))
                {
                    log.Error(trackingId, statusList.GetStatusListAsExceptionString(StatusTypes.StatusSeverity.Error));
                    Assert.Fail();
                }
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        [DeploymentItem(@"TestData\")]
        [DeploymentItem("profile.xsd")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"|DataDirectory|\TestData\TestAllFileTypes.csv", "TestAllFileTypes#csv", DataAccessMethod.Sequential)]
        public void RunProfile()
        {
            try
            {
                var options = new Options
                {
                    ClientId = Convert.ToInt32(TestContext.DataRow["ClientId"].ToString()),
                    FileType = "prof"
                };

                var profileFileName = TestContext.DataRow["profilefilename"].ToString();

                var log = new UnitTestLogger("debugrun.txt", "1", 87);

                var config = new AppConfiguration(Convert.ToInt32(options.ClientId));
                config.LoadProfileConfiguration();

                var bulkFile = new ProfileInputFile(options, config);

                var file = new FileInfo(profileFileName);

                var emailMessageContents = new List<string>();
                var logMessageAdds = new List<LoggerMessageModel>();
                var captureCountsForReporting = new CountsForReportingModel
                {
                    ClientId = options.ClientId,
                    FileName = profileFileName,
                    ProcessType = "ImportToInsightsDW_" + options.FileType.ToUpper()
                };

                StatusList statusList;
                var trackingId = bulkFile.ProcessFile(file, emailMessageContents, logMessageAdds, captureCountsForReporting, out statusList);

                if (statusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error))
                {
                    log.Error(trackingId, statusList.GetStatusListAsExceptionString(StatusTypes.StatusSeverity.Error));
                    Assert.Fail();
                }
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

    }
}
