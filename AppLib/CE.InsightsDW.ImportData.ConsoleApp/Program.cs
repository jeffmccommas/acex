﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using CommandLine;
using System.IO;
using System.Threading.Tasks;
using Aclara.UFx.StatusManagement;
using CE.InsightsDW.ImportData.Infastructure;
using CE.InsightsDW.ImportData.Infastructure.DataAccess;
using CE.InsightsDW.ImportData.Infastructure.Notification;
using NLog;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData
{
    internal class ImportData
    {
        /// <summary>
        /// Main 
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            var options = new Options
            {
                Verbose = true,
                Ignore = false
                //ClientId = 87,
                //FileType = "bill"
            };

            var parser = new Parser(with => with.HelpWriter = Console.Error);

            if (parser.ParseArgumentsStrict(args, options, () => Environment.Exit(-2)))
            {
                Run(options);
        }
    }

        /// <summary>
        /// Run
        /// </summary>
        /// <param name="options">The program options</param>
        private static void Run(Options options)
        {
            var sendErrorEmail = false;

            IMetaDataAccess mdataAccess = null;

            // Contains the email message contents.
            var emailMessageContents = new List<string>
            {
                "The Client Id is " + options.ClientId,
                 "The File Type is " + options.FileType.ToUpper()
            };

            try
            {
                if (options.Verbose)
                {
                    Console.WriteLine("CreateConfig" + DateTime.Now.ToString(CultureInfo.InvariantCulture));
                }

                //Check to see if ETL is running.  If it is, then terminate 
                var classFactory = new InsightsDWImportClassFactory();
                IConfig config = classFactory.CreateConfig(options);
                mdataAccess = classFactory.CreateMetaDataAccess();

                // Check to see if the run is during ETL processing
                if (Process.GetCurrentProcess().StartTime >= config.EtlAvoidanceStartTime &&
                    Process.GetCurrentProcess().StartTime <= config.EtlAvoidanceEndTime) 
                {
                    mdataAccess.LogDbEvent($"etl is currently running.  No processing will occur for client {options.ClientId}",
                        options.ClientId);

                    if (options.Verbose)
                    {
                        Console.WriteLine("etl is currently running.  No processing will occur");
                    }

                    return;
                }

                // Get the notification configurations
                var notification = new Notification();
                var notificationConfig = notification.GetNotificationConfiguration(options.ClientId);

                if (options.Verbose)
                {
                    Console.WriteLine("Searching " + config.InputFilePath);
                }

                emailMessageContents.Add("The Input File Path is " + config.InputFilePath);

                var dir = new DirectoryInfo(config.InputFilePath);
                var workOnTheseFileInfos = dir.GetFiles(config.FileSearchPattern, SearchOption.TopDirectoryOnly)
                    .OrderBy(f => f.Name).ToList();

                Parallel.ForEach(workOnTheseFileInfos, new ParallelOptions { MaxDegreeOfParallelism = config.Parallelism }, file =>
                {
                    var markForError = false;
                    var originalFileName = file.Name;
                    var logName = Path.ChangeExtension(file.FullName, config.LogFileOutputExtension);
                    var trackingId = Helpers.GetTrackingId(file.Name);
                    var currrentlyWorkingFileName = string.Empty;

                    // Initialize the information keepers
                    var emailMessageAdds = new List<string>();
                    var logMessageAdds = new List<LoggerMessageModel>();

                    var captureCountsForReporting = new CountsForReportingModel
                    {
                        ClientId = options.ClientId,
                        FileName = originalFileName,
                        ProcessType = "ImportToInsightsDW_" + options.FileType.ToUpper()
                    };



                    try
                    {
                        if (options.Verbose)
                        {
                            Console.WriteLine("Tracking Id is " + trackingId);
                            Console.WriteLine("LogName is " + logName);
                            Console.WriteLine("Orignal Filename is " + originalFileName);
                        }


                        var loader = classFactory.InsightsDWImport(options, config);

                        currrentlyWorkingFileName = MarkForProcessing(file, config.FileWorkingExtension);

                        // ReSharper disable once RedundantAssignment
                        var statusList = new StatusList
                        {
                            new Status(
                                DateTime.Now.ToString(CultureInfo.InvariantCulture) + " Processing File " +
                                originalFileName,
                                StatusTypes.StatusSeverity.Informational)
                        };

                        emailMessageAdds.Add(string.Empty);
                        emailMessageAdds.Add(
                            "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                        emailMessageAdds.Add(string.Empty);
                        emailMessageAdds.Add(" The Processing File is " + originalFileName);
                        emailMessageAdds.Add(" The Log Filename is " + logName);

                        logMessageAdds.Add(new LoggerMessageModel{ LogLevel = LogLevel.Info,  MessageToLog = $"Processing File {originalFileName} "});

                        // Setup a stopwatch
                        var processingStopWatch = new Stopwatch();
                        processingStopWatch.Start();

                        trackingId = loader.ProcessFile(file, emailMessageAdds, logMessageAdds, captureCountsForReporting, out statusList);

                        processingStopWatch.Stop();
                        var logMessage = " Processing File " +
                                         originalFileName + " complete - elapsed time " +
                                         processingStopWatch.Elapsed;


                        statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + logMessage,
                            StatusTypes.StatusSeverity.Informational));


                        logMessageAdds.Add(new LoggerMessageModel { LogLevel = LogLevel.Info, MessageToLog = logMessage });
                        emailMessageAdds.Add(logMessage);

                        //Mark file as done and move it to archive
                        if (statusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error))
                        {
                            emailMessageAdds.Add("------");
                            emailMessageAdds.Add("The File " + originalFileName +
                                                 " has been marked for an error..Please determine the cause."
                                                     .ToUpper());

                            markForError = true;
                            sendErrorEmail = true;
                        }
                    }
                    catch (AggregateException ae)
                    {
                        ae.Handle((x) =>
                        {
                            if (x is UnauthorizedAccessException) // This we know how to handle.
                            {
                                Console.WriteLine("You do not have permission to access all folders in this path.");
                                Console.WriteLine("See your network administrator or try another path.");
                                return true;
                            }

                            if (options.Verbose)
                            {
                                Console.WriteLine("exception : " + ae.Message + " => " + ae.StackTrace);
                            }
                            return false; // Let anything else stop the application.
                        });
                    }
                    catch (Exception ex)
                    {
                        if (!string.IsNullOrEmpty(logName))
                        {
                            Helpers.WriteLogFile(DateTime.Now, options.ClientId,
                                "exception : " + ex.Message + " => " + ex.StackTrace, logName);

                                var dwImportLogger = new DWImportLogger(logName, trackingId, options.ClientId);
                                dwImportLogger.Error(trackingId, "exception : " + ex.Message + " => " + ex.StackTrace);
                        }
                        try
                        {
                            emailMessageAdds.Add("------");
                            emailMessageAdds.Add(" The File " + originalFileName +
                                                     " has been marked for an error..Please determine the cause."
                                                         .ToUpper());


                            markForError = true;
                            sendErrorEmail = true;
                        }
                        catch (Exception ex1Exception)
                        {
                            mdataAccess?.LogDbEvent("exception : " + ex1Exception.Message + " => " + ex.StackTrace,
                                options.ClientId);
                        }
                    }
                    finally
                    {

                        emailMessageContents.AddRange(emailMessageAdds);
                        HandleLogging(logName, trackingId, options, logMessageAdds);
                        HandleCountsForReporting(captureCountsForReporting, config);

                        if (markForError)
                        {
                            MarkForError(originalFileName, currrentlyWorkingFileName, logName, config.ArchivePath,
                                emailMessageAdds);
                        }
                        else
                        {
                            MarkForArchival(file, config.ArchivePath, config.FileDoneExtension, emailMessageAdds);
                        }


                    }
                });

                // Send one email for all files.
                if (sendErrorEmail)
                {
                    notification.SendEmail(notificationConfig, emailMessageContents, options.ClientId + " - " + options.FileType.ToUpper());
                }

            }
            catch (AggregateException ae)
            {
                ae.Handle((x) =>
                {
                    if (x is UnauthorizedAccessException) // This we know how to handle.
                    {
                        Console.WriteLine("You do not have permission to access all folders in this path.");
                        Console.WriteLine("See your network administrator or try another path.");
                        return true;
                    }

                    if (options.Verbose)
                    {
                        Console.WriteLine("exception : " + ae.Message + " => " + ae.StackTrace);
                    }
                    return false; // Let anything else stop the application.
                });
            }
            catch (Exception ex)
            {
                mdataAccess?.LogDbEvent("exception : " + ex.Message + " => " + ex.StackTrace, options.ClientId);

                if (options.Verbose)
                {
                    Console.WriteLine("exception : " + ex.Message + " => " + ex.StackTrace);
                }

                if (ex is System.Configuration.ConfigurationErrorsException)
                {
                    Environment.Exit(3);
                }

                Environment.Exit(2);
            }

            // Finish
            if (options.Verbose)
            {
                Console.WriteLine("ending" + DateTime.Now.ToString(CultureInfo.InvariantCulture));
            }
        }

        /// <summary>
        /// Mark the file as being processed
        /// </summary>
        /// <param name="file">The filename</param>
        /// <param name="workingExtension">The extension that marks it as being processed</param>
        private static string MarkForProcessing(FileInfo file, string workingExtension)
        {
            //Mark file for processing
            var processingName = Path.ChangeExtension(file.FullName, workingExtension);

            if (File.Exists(processingName))
            {
                File.Delete(processingName);
            }

            file.MoveTo(processingName);

            return processingName;

        }

        /// <summary>
        /// Mark the file for archival
        /// </summary>
        /// <param name="file">The filename</param>
        /// <param name="archivePath">The path for archiving</param>
        /// <param name="doneExtension">The extension that marks the file as done.</param>
        /// <param name="emailMessageContents">Contents of error email message</param>
        private static void MarkForArchival(FileInfo file, string archivePath, string doneExtension, ICollection<string> emailMessageContents)
        {
            var archiveName = archivePath + Path.ChangeExtension(file.Name, doneExtension);

            if (File.Exists(archiveName))
            {
                File.Delete(archiveName);
            }

            file.MoveTo(archiveName);
            emailMessageContents.Add(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": The Archive FileName is " + archiveName);
        }


        /// <summary>
        /// Mark the file as being processed
        /// </summary>
        /// <param name="fileName">The data  filename</param>
        /// <param name="currentlyWorkingFileName"></param>
        /// <param name="logFileName">The log filename</param>
        /// <param name="archivePath">The path for archiving</param>
        /// <param name="emailMessageContents">Contents of error email message</param>
        private static void MarkForError(string fileName, string currentlyWorkingFileName, string logFileName,
            string archivePath, ICollection<string> emailMessageContents)
        {

            //Mark file for error
            var errorFileName = archivePath + fileName + ".ERROR";

            if (File.Exists(errorFileName))
            {
                File.Delete(errorFileName);
            }

            Console.WriteLine("currentlyWorkingFileName = " + currentlyWorkingFileName);

            Console.WriteLine("errorFileName = " + errorFileName);
            emailMessageContents.Add("The Error FileName: " + errorFileName);

            File.Move(currentlyWorkingFileName, errorFileName);

            if (!string.IsNullOrEmpty(logFileName))
            {
                var errorLogFileName = logFileName + ".ERROR";
                if (File.Exists(errorLogFileName))
                {
                    File.Delete(errorLogFileName);
                }

                Console.WriteLine("errorLogFileName = " + errorLogFileName);
                emailMessageContents.Add(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                                         ": The Error LogFileName is " + errorLogFileName);

                Console.WriteLine("logFileName = " + logFileName);


                File.Move(logFileName, errorLogFileName);
            }
        }

        private static readonly object LogLock = new object();

        /// <summary>
        ///  Insert log messages to the logfile.
        /// </summary>
        /// <param name="logName">The name of the log</param>
        /// <param name="trackingId">The tracking id.</param>
        /// <param name="options">The processing options</param>
        /// <param name="logMessageAdds">The messages to log.</param>
        private static void HandleLogging(string logName, string trackingId, Options options, IEnumerable<LoggerMessageModel> logMessageAdds)
            {
                lock (LogLock)
                {
                    var dwImportLogger = new DWImportLogger(logName, trackingId, options.ClientId);
                    foreach (var logMessage in logMessageAdds)
                    {
                        if (logMessage.LogLevel == LogLevel.Info)
                        {
                            dwImportLogger.Info(trackingId, logMessage.MessageToLog);
                        }
                        if (logMessage.LogLevel == LogLevel.Error)
                        {
                            if (logMessage.Exception != null)
                            {
                                dwImportLogger.ErrorException(trackingId, logMessage.Exception);
                            }
                            else
                            {
                                dwImportLogger.Error(trackingId, logMessage.MessageToLog);
                            }
                        }
                    }
                }
            }


        private static IInsightsDWAccess _insightsDw;
        private static readonly object CountLock = new object();

        /// <summary>
        /// Place the information regarding counts for reporting into the database.
        /// </summary>
        /// <param name="captureCountsForReporting"></param>
        private static void HandleCountsForReporting(CountsForReportingModel captureCountsForReporting, IConfig config)
        {
            lock (CountLock)
            {
                if (_insightsDw == null)
                {
                    _insightsDw = new Infastructure.InsightsDW(config.ShardName);
                }

                _insightsDw.LogCountsForReporting(captureCountsForReporting.ClientId, captureCountsForReporting.ProcessType,
                    captureCountsForReporting.Count, captureCountsForReporting.FileName, captureCountsForReporting.Comment);

            }
        }


    }
}
