﻿
namespace CE.AO.Portal.Authentication
{
    public static class Constants
    {
        public const string ETagWildcard = "*";

        public static class AppSettingsKeys
        {
            public const string DefaultStorageConnectionStringKey = "StorageConnectionString";
        }

        public static class TableNames
        {
            public const string RolesTable = "Roles";
            public const string UsersTable = "Users";
            public const string IndexTable = "Index";
            public const string ClientDomainTable = "ClientDomain";
            public const string ClientWidgetConfigurationTable = "ClientWidgetConfiguration";
        }

        public static class RowKeyConstants
        {
            #region Identity User
            public const string PreFixIdentityUserClaim = "C_";
            public const string PreFixIdentityUserRole = "R_";
            public const string PreFixIdentityUserLogin = "L_";
            public const string PreFixIdentityUserEmail = "E_";
            public const string PreFixIdentityUserName = "U_";

            public const string FormatterIdentityUserClaim = PreFixIdentityUserClaim + "{0}";
            public const string FormatterIdentityUserRole = PreFixIdentityUserRole + "{0}";
            public const string FormatterIdentityUserLogin = PreFixIdentityUserLogin + "{0}";
            public const string FormatterIdentityUserEmail = PreFixIdentityUserEmail + "{0}";
            public const string FormatterIdentityUserName = PreFixIdentityUserName + "{0}";
            #endregion

            #region Identity Role
            public const string PreFixIdentityRole = "R_";
            public const string FormatterIdentityRole = PreFixIdentityRole + "{0}";
            #endregion
        }
    }
}