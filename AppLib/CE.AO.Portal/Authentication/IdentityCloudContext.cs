﻿using System;
using CE.AO.Portal.Authentication.Configuration;
using CE.AO.Portal.Authentication.Model;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Portal.Authentication
{
    public class IdentityCloudContext : IdentityCloudContext<string, IdentityUserClaim>
    {
        public IdentityCloudContext()
        { }

        [Obsolete("Please use the default constructor IdentityCloudContext() to load the configSection from web/app.config or " +
            "the constructor IdentityCloudContext(IdentityConfiguration config) for more options.")]
        public IdentityCloudContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }
        public IdentityCloudContext(IdentityConfiguration config) :
            base(config) { }
    }

    // ReSharper disable once UnusedTypeParameter
    public class IdentityCloudContext<TUser> : IdentityCloudContext<string, IdentityUserClaim> where TUser : IdentityUser
    {
        public IdentityCloudContext()
        {
        }

        [Obsolete("Please use the default constructor IdentityCloudContext() to load the configSection from web/app.config or " +
            "the constructor IdentityCloudContext(IdentityConfiguration config) for more options.")]
        public IdentityCloudContext(string connectionStringKey)
            : base(connectionStringKey)
        {
        }

        public IdentityCloudContext(IdentityConfiguration config) :
            base(config) { }
    }

    // ReSharper disable once UnusedTypeParameter
    public class IdentityCloudContext<TKey, TUserClaim> : IDisposable where TUserClaim : IdentityUserClaim<TKey>
    {
        private CloudTableClient _client;
        private bool _disposed;
        private IdentityConfiguration _config;
        private CloudTable _roleTable;
        private CloudTable _indexTable;
        private CloudTable _userTable;
        private CloudTable _clientDomainTable;
        private CloudTable _clientWidgetConfigurationTable;        

        public IdentityCloudContext() 
        {
            IdentityConfiguration config = IdentityConfigurationSection.GetCurrent() ?? new IdentityConfiguration
            {
                StorageConnectionString =  CloudConfigurationManager.GetSetting(Constants.AppSettingsKeys.DefaultStorageConnectionStringKey),
                TablePrefix = string.Empty
            };
            //For backwards compat for those who do not use the new configSection.
            Initialize(config);
        }

        [Obsolete("Please use the default constructor IdentityCloudContext() to load the configSection from web/app.config or " +
            "the constructor IdentityCloudContext(IdentityConfiguration config) for more options.")]
        public IdentityCloudContext(string connectionStringKey)
        {
            string strConnection = CloudConfigurationManager.GetSetting(connectionStringKey);
            Initialize(new IdentityConfiguration
            {
                StorageConnectionString = string.IsNullOrWhiteSpace(strConnection) ?
                    connectionStringKey : strConnection,
                TablePrefix = string.Empty
            });
            
        }

        public IdentityCloudContext(IdentityConfiguration config)
        {
            if (config == null)
            {
                throw new ArgumentNullException(nameof(config));
            }
            Initialize(config);
        }

        private void Initialize(IdentityConfiguration config)
        {
            _config = config;
            _client = CloudStorageAccount.Parse(_config.StorageConnectionString).CreateCloudTableClient();
            if (!string.IsNullOrWhiteSpace(_config.LocationMode))
            {
                LocationMode mode;
                if (Enum.TryParse(_config.LocationMode, out mode))
                {
                    _client.DefaultRequestOptions.LocationMode = mode;
                }
                else
                {
                    throw new ArgumentException(@"Invalid LocationMode defined in config. For more information on geo-replication location modes: http://msdn.microsoft.com/en-us/library/azure/microsoft.windowsazure.storage.retrypolicies.locationmode.aspx", @"config.Loc" + @"ationMode");
                }
            }
            _indexTable = _client.GetTableReference(FormatTableNameWithPrefix(Constants.TableNames.IndexTable));
            _roleTable = _client.GetTableReference(FormatTableNameWithPrefix(Constants.TableNames.RolesTable)); 
            _userTable = _client.GetTableReference(FormatTableNameWithPrefix(Constants.TableNames.UsersTable));
            _clientDomainTable = _client.GetTableReference(FormatTableNameWithPrefix(Constants.TableNames.ClientDomainTable));
            _clientWidgetConfigurationTable = _client.GetTableReference(FormatTableNameWithPrefix(Constants.TableNames.ClientWidgetConfigurationTable));
        }

        ~IdentityCloudContext()
        {
            Dispose(false);
        }

        private string FormatTableNameWithPrefix(string baseTableName)
        {
            if(!string.IsNullOrWhiteSpace(_config.TablePrefix))
            {
                return $"{_config.TablePrefix}{baseTableName}";
            }
            return baseTableName;
        }

        public CloudTable RoleTable
        {
            get
            {
                ThrowIfDisposed();
                return _roleTable;
            }
        }

        public CloudTable UserTable
        {
            get
            {
                ThrowIfDisposed();
                return _userTable;
            }
        }

        public CloudTable IndexTable
        {
            get
            {
                ThrowIfDisposed();
                return _indexTable;
            }
        }

        public CloudTable ClientDomainTable
        {
            get
            {
                ThrowIfDisposed();
                return _clientDomainTable;
            }
        }

        public CloudTable ClientWidgetConfigurationTable
        {
            get
            {
                ThrowIfDisposed();
                return _clientWidgetConfigurationTable;
            }
        }

        private void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                _client = null;
                _indexTable = null;
                _roleTable = null;
                _userTable = null;
                _clientDomainTable = null;
                _disposed = true;
            }
        }
    }

}
