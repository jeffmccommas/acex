﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CE.AO.Portal.Authentication.Helpers;
using CE.AO.Portal.Authentication.Model;
using Microsoft.AspNet.Identity;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage.Table.Queryable;

namespace CE.AO.Portal.Authentication
{
    //public class UserStore<TUser> : UserStore<TUser, IdentityRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim, IdentityClientDomain>, IUserStore<TUser>, Model.IUserStoreCustom<TUser>, IUserStore<TUser, string> where TUser : IdentityUser, new()
    public class UserStore<TUser> : UserStore<TUser, IdentityRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>, IUserStore<TUser> where TUser : IdentityUser, new()
    {
        public UserStore()
            : this(new IdentityCloudContext<TUser>())
        {

        }

        public UserStore(IdentityCloudContext<TUser> context)
            : base(context)
        {
        }

        /// <summary>
        /// Simple table queries allowed. Projections are only allowed for TUser types. 
        /// </summary>
        public override IQueryable<TUser> Users
        {
            get
            {
                TableQueryHelper<TUser> helper = new TableQueryHelper<TUser>(
                    (from t in base.Users
                     where String.Compare(t.RowKey, Constants.RowKeyConstants.PreFixIdentityUserName, StringComparison.Ordinal) > 0
                     select t).AsTableQuery()
                    , GetUserAggregateQuery);

                return helper;
                
            }
        }
        //Fixing code analysis issue CA1063
        //protected override void Dispose(bool disposing)
        //{
        //    base.Dispose(disposing);
        //}
        
    }

    public class UserStore<TUser, TRole, TKey, TUserLogin, TUserRole, TUserClaim> : IUserLoginStore<TUser, TKey>
        , IUserClaimStore<TUser, TKey>
        , IUserRoleStore<TUser, TKey>, IUserPasswordStore<TUser, TKey>
        , IUserSecurityStampStore<TUser, TKey>, IQueryableUserStore<TUser, TKey>
        , IUserEmailStore<TUser, TKey>, IUserPhoneNumberStore<TUser, TKey>
        , IUserTwoFactorStore<TUser, TKey>
        , IUserLockoutStore<TUser, TKey>,IClientDomain where TUser : IdentityUser<TKey, TUserLogin, TUserRole, TUserClaim>, new()
        where TRole : IdentityRole<TKey, TUserRole>, new()
        where TKey : IEquatable<TKey>
        where TUserLogin : IdentityUserLogin<TKey>, new()
        where TUserRole : IdentityUserRole<TKey>, new()
        where TUserClaim : IdentityUserClaim<TKey>, new()
    {
        private bool _disposed;

        private CloudTable _userTable;
        private CloudTable _indexTable;
        private readonly CloudTable _clientDomainTable;
        //private readonly CloudTable _clientWidgetConfigurationTable;

        public UserStore(IdentityCloudContext<TKey, TUserClaim> context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            Context = context;
            _userTable = context.UserTable;
            _indexTable = context.IndexTable;
            _clientDomainTable = context.ClientDomainTable;
            //_clientWidgetConfigurationTable = context.ClientWidgetConfigurationTable;
        }

        public async Task CreateTablesIfNotExists()
        {
            Task<bool>[] tasks = {
                        Context.RoleTable.CreateIfNotExistsAsync(),
                        Context.UserTable.CreateIfNotExistsAsync(),
                        Context.IndexTable.CreateIfNotExistsAsync(),
                        Context.ClientDomainTable.CreateIfNotExistsAsync(),
                        Context.ClientWidgetConfigurationTable.CreateIfNotExistsAsync()
                    };
            await Task.WhenAll(tasks);
        }

        public virtual async Task AddClaimAsync(TUser user, Claim claim)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (claim == null)
            {
                throw new ArgumentNullException(nameof(claim));
            }
            TUserClaim item = Activator.CreateInstance<TUserClaim>();
            item.UserId = user.Id;
            item.ClaimType = claim.Type;
            item.ClaimValue = claim.Value;
            //((IGenerateKeys)item).GenerateKeys();

            user.Claims.Add(item);

            await _userTable.ExecuteAsync(TableOperation.Insert(item));
        }

        public virtual async Task AddLoginAsync(TUser user, UserLoginInfo login)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (login == null)
            {
                throw new ArgumentNullException(nameof(login));
            }
            TUserLogin item = Activator.CreateInstance<TUserLogin>();
            item.UserId = user.Id;
            item.ProviderKey = login.ProviderKey;
            item.LoginProvider = login.LoginProvider;
            //((IGenerateKeys)item).GenerateKeys();

            user.Logins.Add(item);
            IdentityUserIndex index = CreateLoginIndex(item.UserId.ToString(), item);

            await Task.WhenAll(_userTable.ExecuteAsync(TableOperation.Insert(item))
                , _indexTable.ExecuteAsync(TableOperation.InsertOrReplace(index)));

        }

        public virtual async Task AddToRoleAsync(TUser user, string roleName)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException(IdentityResources.ValueCannotBeNullOrEmpty, nameof(roleName));
            }

            TRole roleT = Activator.CreateInstance<TRole>();
            roleT.Name = roleName;
            
            TUserRole userToRole = Activator.CreateInstance<TUserRole>();
            userToRole.UserId = user.Id;
            userToRole.RoleId = roleT.Id;
            userToRole.RoleName = roleT.Name;
            TUserRole item = userToRole;

            
            user.Roles.Add(item);
            roleT.Users.Add(item);

            await _userTable.ExecuteAsync(TableOperation.Insert(item));

        }

        public virtual async Task CreateAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            
            List<Task> tasks = new List<Task>(2) {_userTable.ExecuteAsync(TableOperation.Insert(user))};

            if (!string.IsNullOrWhiteSpace(user.Email))
            {
                IdentityUserIndex index = CreateEmailIndex(user.Id.ToString(), user.Email);
                tasks.Add(_indexTable.ExecuteAsync(TableOperation.InsertOrReplace(index)));
            }

            await Task.WhenAll(tasks.ToArray());
        }

        public virtual async Task DeleteAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            List<Task> tasks = new List<Task>(50);

            BatchOperationHelper userBatch = new BatchOperationHelper();

            userBatch.Add(TableOperation.Delete(user));
            //Don't use the BatchHelper for login index table, partition keys are likely not the same
            //since they are based on provider
            foreach (var userLogin in user.Logins)
            {
                userBatch.Add(TableOperation.Delete(userLogin));

                IdentityUserIndex indexLogin = CreateLoginIndex(user.Id.ToString(), userLogin);

                tasks.Add(_indexTable.ExecuteAsync(TableOperation.Delete(indexLogin)));
            }

            foreach (var userRole in user.Roles)
            {
                userBatch.Add(TableOperation.Delete(userRole));
            }

            foreach (var userClaim in user.Claims)
            {
                userBatch.Add(TableOperation.Delete(userClaim));
            }

            tasks.Add(userBatch.ExecuteBatchAsync(_userTable));
            if (!string.IsNullOrWhiteSpace(user.Email))
            {
                IdentityUserIndex indexEmail = CreateEmailIndex(user.Id.ToString(), user.Email);
                tasks.Add(_indexTable.ExecuteAsync(TableOperation.Delete(indexEmail)));
            }

            await Task.WhenAll(tasks.ToArray());

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                Context?.Dispose();
                _indexTable = null;
                _userTable = null;
                Context = null;
                _disposed = true;
            }
        }

        public virtual async Task<TUser> FindAsync(UserLoginInfo login)
        {
            ThrowIfDisposed();
            if (login == null)
            {
                throw new ArgumentNullException(nameof(login));
            }

            string rowKey = login.GenerateRowKeyUserLoginInfo();
            string partitionKey = KeyHelper.GeneratePartitionKeyIndexByLogin(login.LoginProvider);
            var loginQuery = GetUserIdByIndex(partitionKey, rowKey);

            return await GetUserAggregateAsync(loginQuery);
        }

        public async Task<TUser> FindByEmailAsync(string plainEmail)
        {
            ThrowIfDisposed();
            return await GetUserAggregateAsync(FindByEmailQuery(plainEmail));
        }

        public async Task<IEnumerable<TUser>> FindAllByEmailAsync(string plainEmail)
        {
            ThrowIfDisposed();
            return await GetUsersAggregateAsync(FindByEmailQuery(plainEmail));
        }

        private TableQuery FindByEmailQuery(string plainEmail)
        {
            return GetUserIdsByIndex(KeyHelper.GenerateRowKeyUserEmail(plainEmail));
        }

        private TableQuery GetUserIdByIndex(string partitionkey, string rowkey)
        {
            TableQuery tq = new TableQuery
            {
                TakeCount = 1,
                SelectColumns = new List<string> {"Id"},
                FilterString = TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionkey),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, rowkey))
            };
            return tq;
        }

        private TableQuery GetUserIdsByIndex(string rowkey)
        {
            TableQuery tq = new TableQuery
            {
                SelectColumns = new List<string> {"Id"},
                FilterString = TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, rowkey)
            };
            return tq;
        }

        public virtual Task<TUser> FindByIdAsync(TKey userId)
        {
            ThrowIfDisposed();
            return GetUserAggregateAsync(userId.ToString());
        }

        public virtual Task<TUser> FindByNameAsync(string userName)
        {
            ThrowIfDisposed();
            return GetUserAggregateAsync(userName);

        }

        public Task<int> GetAccessFailedCountAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.AccessFailedCount);
        }

        public virtual Task<IList<Claim>> GetClaimsAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult<IList<Claim>>(user.Claims.Select(c => new Claim(c.ClaimType, c.ClaimValue)).ToList());
        }

        public Task<string> GetEmailAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.EmailConfirmed);
        }

        public Task<bool> GetLockoutEnabledAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.LockoutEnabled);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.LockoutEndDateUtc.HasValue ? new DateTimeOffset(DateTime.SpecifyKind(user.LockoutEndDateUtc.Value, DateTimeKind.Utc)) : new DateTimeOffset());
        }

        public virtual Task<IList<UserLoginInfo>> GetLoginsAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult<IList<UserLoginInfo>>((from l in user.Logins select new UserLoginInfo(l.LoginProvider, l.ProviderKey)).ToList());
        }

        public Task<string> GetPasswordHashAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.PasswordHash);
        }

        public Task<string> GetPhoneNumberAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.PhoneNumber);
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.PhoneNumberConfirmed);
        }

        public virtual Task<IList<string>> GetRolesAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult<IList<string>>(user.Roles.ToList().Select(r => r.RoleName).ToList());
        }

        public Task<string> GetSecurityStampAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.SecurityStamp);
        }

        public Task<bool> GetTwoFactorEnabledAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.TwoFactorEnabled);
        }

        private Task<TUser> GetUserAggregateAsync(string userId)
        {
            var userResults = GetUserAggregateQuery(userId).ToList();
            return Task.FromResult(GetUserAggregate(userId, userResults));
        }

        public Task<string> GetClientByDomainAsync(string domain)
        {
            TableQuery tq = new TableQuery
            {
                FilterString = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, domain.ToLower())
            };
            var client =_clientDomainTable.ExecuteQuery(tq).ToList();

            return Task.FromResult(client.FirstOrDefault() != null ? client.FirstOrDefault()?.RowKey : null);
        }

        private IEnumerable<DynamicTableEntity> GetUserAggregateQuery(string userId)
        {
            TableQuery tq = new TableQuery
            {
                FilterString = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, userId.ToLower())
            };
            return _userTable.ExecuteQuery(tq);
        }

        protected IEnumerable<TUser> GetUserAggregateQuery(IList<string> userIds)
        {
            const double pageSize = 50.0;
            int pages = (int)Math.Ceiling(userIds.Count / pageSize);
            List<TableQuery> listTqs = new List<TableQuery>(pages);

            for (int currentPage = 1; currentPage <= pages; currentPage++)
            {
                var tempUserIds = currentPage > 1 ? userIds.Skip((currentPage - 1) * (int)pageSize).Take((int)pageSize).ToList() : userIds.Take((int)pageSize).ToList();

                TableQuery tq = new TableQuery();
                for (int i = 0; i < tempUserIds.Count; i++)
                {
                    string temp = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, tempUserIds[i].ToLower());
                    tq.FilterString = i > 0 ? TableQuery.CombineFilters(tq.FilterString, TableOperators.Or, temp) : temp;
                }
                listTqs.Add(tq);

            }

            ConcurrentBag<TUser> bag = new ConcurrentBag<TUser>();
#if DEBUG
            DateTime startUserAggTotal = DateTime.UtcNow;
#endif
            Parallel.ForEach(listTqs, q =>
            {
                Parallel.ForEach(_userTable.ExecuteQuery(q)
                    .ToList()
                    .GroupBy(g => g.PartitionKey), s =>
                    {
                        bag.Add(GetUserAggregate(s.Key, s));
                    });
            });
#if DEBUG
            Debug.WriteLine("GetUserAggregateQuery (GetUserAggregateTotal): {0} seconds", (DateTime.UtcNow - startUserAggTotal).TotalSeconds);
#endif
            return bag;
        }

        private TUser GetUserAggregate(string userId, IEnumerable<DynamicTableEntity> userResults)
        {
            TUser user = default(TUser);
            var vUser = userResults.SingleOrDefault(u => u.RowKey.Equals(userId, StringComparison.InvariantCultureIgnoreCase) && u.PartitionKey.Equals(userId, StringComparison.InvariantCultureIgnoreCase));
            var op = new OperationContext();

            if (vUser != null)
            {
                //User
                user = Activator.CreateInstance<TUser>();
                user.ReadEntity(vUser.Properties, op);
                user.PartitionKey = vUser.PartitionKey;
                user.RowKey = vUser.RowKey;
                user.ETag = vUser.ETag;
                user.Timestamp = vUser.Timestamp;
                
            }
            return user;
        }

        private async Task<TUser> GetUserAggregateAsync(TableQuery queryUser)
        {
            return await new TaskFactory<TUser>().StartNew(() =>
            {
                var user = _indexTable.ExecuteQuery(queryUser).FirstOrDefault();
                if (user != null)
                {
                    string userId = user["Id"].StringValue;
                    var userResults = GetUserAggregateQuery(userId).ToList();
                    return GetUserAggregate(userId, userResults);
                }

                return default(TUser);
            });
        }

        private async Task<IEnumerable<TUser>> GetUsersAggregateAsync(TableQuery queryUser)
        {
#if DEBUG
            DateTime startIndex = DateTime.UtcNow;
#endif
            var userIds = _indexTable.ExecuteQuery(queryUser).ToList().Select(u => u["Id"].StringValue).Distinct().ToList();
#if DEBUG
            Debug.WriteLine("GetUsersAggregateAsync (Index query): {0} seconds", (DateTime.UtcNow - startIndex).TotalSeconds);
#endif
            //List<TUser> list = new List<TUser>(userIds.Count);

            return await GetUsersAggregateByIdsAsync(userIds);
        }

        protected async Task<IEnumerable<TUser>> GetUsersAggregateByIdsAsync(IList<string> userIds)
        {
            return await new TaskFactory<IEnumerable<TUser>>().StartNew(() => GetUserAggregateQuery(userIds));
        }

        public Task<bool> HasPasswordAsync(TUser user)
        {
            return Task.FromResult(user.PasswordHash != null);
        }

        public Task<int> IncrementAccessFailedCountAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.AccessFailedCount++;
            return Task.FromResult(user.AccessFailedCount);
        }

        public virtual Task<bool> IsInRoleAsync(TUser user, string roleName)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException(IdentityResources.ValueCannotBeNullOrEmpty, nameof(roleName));
            }

            //Removing the live query. UserManager calls FindById to hydrate the user object first.
            //No need to go to the table again.
            return Task.FromResult(user.Roles.Any(r => r.RowKey == KeyHelper.GenerateRowKeyIdentityRole(roleName)));
        }

        public virtual async Task RemoveClaimAsync(TUser user, Claim claim)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (claim == null)
            {
                throw new ArgumentNullException(nameof(claim));
            }

            if (string.IsNullOrWhiteSpace(claim.Type))
            {
                throw new ArgumentException(IdentityResources.ValueCannotBeNullOrEmpty, "cl" + "aim.Type");
            }

            // Claim ctor doesn't allow Claim.Value to be null. Need to allow string.empty.

            TUserClaim local = (from uc in user.Claims
                                where uc.RowKey == KeyHelper.GenerateRowKeyIdentityUserClaim(claim.Type, claim.Value)
                                select uc).FirstOrDefault();
            {
                user.Claims.Remove(local);
                TableOperation deleteOperation = TableOperation.Delete(local);
                await _userTable.ExecuteAsync(deleteOperation);
            }

        }

        public virtual async Task RemoveFromRoleAsync(TUser user, string roleName)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException(IdentityResources.ValueCannotBeNullOrEmpty, nameof(roleName));
            }

            TUserRole item = user.Roles.FirstOrDefault(r => r.RowKey == KeyHelper.GenerateRowKeyIdentityRole(r.RoleName));
            if (item != null)
            {
                user.Roles.Remove(item);
                TableOperation deleteOperation = TableOperation.Delete(item);

                await _userTable.ExecuteAsync(deleteOperation);
            }
        }

        public virtual async Task RemoveLoginAsync(TUser user, UserLoginInfo login)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (login == null)
            {
                throw new ArgumentNullException(nameof(login));
            }
            string provider = login.LoginProvider;
            string key = login.ProviderKey;
            TUserLogin item = user.Logins.SingleOrDefault(l => l.RowKey == KeyHelper.GenerateRowKeyIdentityUserLogin(provider, key));
            if (item != null)
            {
                user.Logins.Remove(item);
                IdentityUserIndex index = CreateLoginIndex(item.UserId.ToString(), item);
                await Task.WhenAll(_indexTable.ExecuteAsync(TableOperation.Delete(index)),
                                    _userTable.ExecuteAsync(TableOperation.Delete(item)));
            }
        }

        public Task ResetAccessFailedCountAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.AccessFailedCount = 0;
            return Task.FromResult(0);
        }

        public async Task SetEmailAsync(TUser user, string email)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            //Only remove the email if different
            //The UserManager calls UpdateAsync which will generate the new email index record
            if (!string.IsNullOrWhiteSpace(user.Email) && user.Email != email)
            {
                await DeleteEmailIndexAsync(user.Id.ToString(), user.Email);
            }
            user.Email = email;
        }

        private async Task DeleteEmailIndexAsync(string userId, string plainEmail)
        {
            var indexes = (from index in _indexTable.CreateQuery<IdentityUserIndex>()
                           where index.RowKey.Equals(KeyHelper.GenerateRowKeyUserEmail(plainEmail))
                           select index).ToList();
            foreach (IdentityUserIndex de in indexes)
            {
                if (de.Id.Equals(userId, StringComparison.InvariantCultureIgnoreCase))
                {
                    await _indexTable.ExecuteAsync(TableOperation.Delete(de));
                }
            }
        }

        public Task SetEmailConfirmedAsync(TUser user, bool confirmed)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.EmailConfirmed = confirmed;
            return Task.FromResult(0);
        }

        public Task SetLockoutEnabledAsync(TUser user, bool enabled)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.LockoutEnabled = enabled;
            return Task.FromResult(0);
        }

        public Task SetLockoutEndDateAsync(TUser user, DateTimeOffset lockoutEnd)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.LockoutEndDateUtc = lockoutEnd == DateTimeOffset.MinValue ? null : new DateTime?(lockoutEnd.UtcDateTime);
            return Task.FromResult(0);
        }

        public Task SetPasswordHashAsync(TUser user, string passwordHash)
        {
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        public Task SetPhoneNumberAsync(TUser user, string phoneNumber)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.PhoneNumber = phoneNumber;
            return Task.FromResult(0);
        }

        public Task SetPhoneNumberConfirmedAsync(TUser user, bool confirmed)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.PhoneNumberConfirmed = confirmed;
            return Task.FromResult(0);
        }

        public Task SetSecurityStampAsync(TUser user, string stamp)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.SecurityStamp = stamp;
            return Task.FromResult(0);
        }

        public Task SetTwoFactorEnabledAsync(TUser user, bool enabled)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.TwoFactorEnabled = enabled;
            return Task.FromResult(0);
        }

        private void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }

        private TUser ChangeUserName(TUser user)
        {
            List<Task> taskList = new List<Task>(50);
            string userNameKey = KeyHelper.GenerateRowKeyUserName(user.UserName);

            Debug.WriteLine("Old User.Id: {0}", user.Id);
            string oldUserId = user.Id.ToString();
            //Get the old user
            var userRows = GetUserAggregateQuery(user.Id.ToString()).ToList();
            //Insert the new user name rows
            BatchOperationHelper insertBatchHelper = new BatchOperationHelper();
            foreach (DynamicTableEntity oldUserRow in userRows)
            {
                ITableEntity dte;
                if (oldUserRow.RowKey.Equals(user.Id.ToString(), StringComparison.InvariantCultureIgnoreCase))
                {
                    dte = user;
                }
                else
                {
                    dte = new DynamicTableEntity(userNameKey, oldUserRow.RowKey,
                        Constants.ETagWildcard,
                        oldUserRow.Properties);
                }
                insertBatchHelper.Add(TableOperation.Insert(dte));
            }
            taskList.Add(insertBatchHelper.ExecuteBatchAsync(_userTable));
            //Delete the old user
            BatchOperationHelper deleteBatchHelper = new BatchOperationHelper();
            foreach (DynamicTableEntity delUserRow in userRows)
            {
                deleteBatchHelper.Add(TableOperation.Delete(delUserRow));
            }
            taskList.Add(deleteBatchHelper.ExecuteBatchAsync(_userTable));

            // Create the new email index
            if (!string.IsNullOrWhiteSpace(user.Email))
            {
                taskList.Add(DeleteEmailIndexAsync(oldUserId, user.Email));

                IdentityUserIndex indexEmail = CreateEmailIndex(userNameKey, user.Email);

                taskList.Add(_indexTable.ExecuteAsync(TableOperation.InsertOrReplace(indexEmail)));
            }

            // Update the external logins
            foreach (var login in user.Logins)
            {
                IdentityUserIndex indexLogin = CreateLoginIndex(userNameKey, login);
                taskList.Add(_indexTable.ExecuteAsync(TableOperation.InsertOrReplace(indexLogin)));
                login.PartitionKey = userNameKey;
            }

            // Update the claims partitionkeys
            foreach (var claim in user.Claims)
            {
                claim.PartitionKey = userNameKey;
            }

            // Update the roles partitionkeys
            foreach (var role in user.Roles)
            {
                role.PartitionKey = userNameKey;
            }

            Task.WaitAll(taskList.ToArray());
            return user;
        }


        public virtual async Task UpdateAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            List<Task> tasks = new List<Task>(2);

            string userNameKey = KeyHelper.GenerateRowKeyUserName(user.PartitionKey);
            if (user.Id.ToString() != userNameKey)
            {
                tasks.Add(Task.FromResult(ChangeUserName(user)));
            }
            else
            {
                tasks.Add(_userTable.ExecuteAsync(TableOperation.Replace(user)));

                if (!string.IsNullOrWhiteSpace(user.Email))
                {
                    IdentityUserIndex indexEmail = CreateEmailIndex(user.Id.ToString(), user.Email);

                    tasks.Add(_indexTable.ExecuteAsync(TableOperation.InsertOrReplace(indexEmail)));
                }
            }

            await Task.WhenAll(tasks.ToArray());
        }

        public IdentityCloudContext<TKey, TUserClaim> Context { get; private set; }


        public virtual IQueryable<TUser> Users
        {
            get
            {
                ThrowIfDisposed();
                return _userTable.CreateQuery<TUser>();
            }
        }

        /// <summary>
        /// Creates an email index suitable for a crud operation
        /// </summary>
        /// <param name="userid">Formatted UserId from the KeyHelper or IdentityUser.Id.ToString()</param>
        /// <param name="email">Plain email address.</param>
        /// <returns></returns>
        // ReSharper disable once UnusedParameter.Local
        private IdentityUserIndex CreateEmailIndex(string userid, string email)
        {
            return new IdentityUserIndex
            {
                Id = userid.ToLower(),
                PartitionKey = userid.ToLower(),
                // RowKey = KeyHelper.GenerateRowKeyUserEmail(email),
                RowKey = userid.ToLower(),
                KeyVersion = KeyHelper.KeyVersion,
                ETag = Constants.ETagWildcard
            };
        }

        private IdentityUserIndex CreateLoginIndex(string userid, TUserLogin login)
        {
            return new IdentityUserIndex
            {
                Id = userid.ToLower(),
                PartitionKey = KeyHelper.GeneratePartitionKeyIndexByLogin(login.LoginProvider),
                RowKey = login.RowKey,
                KeyVersion = KeyHelper.KeyVersion,
                ETag = Constants.ETagWildcard
            };

        }

        public Task<string> GetCustomerByEmailAsync(string email)
        {
            TableQuery tq = new TableQuery
            {
                FilterString = TableQuery.GenerateFilterCondition("Email", QueryComparisons.Equal, email)
            };
            var customer = _userTable.ExecuteQuery(tq).ToList();

            return Task.FromResult(customer.Select(u => u["CustomerId"].StringValue).FirstOrDefault());
        }
    }
}
