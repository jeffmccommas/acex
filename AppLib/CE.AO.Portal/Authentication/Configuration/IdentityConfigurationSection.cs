﻿using System.Configuration;
using CE.AO.Portal.Authentication.Model;

namespace CE.AO.Portal.Authentication.Configuration
{

    public class IdentityConfigurationSection : ConfigurationSection
    {
        public static string Name => "identityConfiguration";

        public static IdentityConfiguration GetCurrent()
        {
            IdentityConfigurationSection section = ConfigurationManager.GetSection(Name) as IdentityConfigurationSection;
            //Add this code when appSettings configuration are phased out.
            if (section == null)
                throw new ConfigurationErrorsException($"Configuration Section Not Found: {Name}");

            return new IdentityConfiguration
            {
                StorageConnectionString = section.StorageConnectionString,
                TablePrefix = section.TablePrefix,
                LocationMode = section.LocationMode
            };
        }

        [ConfigurationProperty("tablePrefix", DefaultValue = "", IsRequired = false)]
        public string TablePrefix
        {
            get
            {
                return (string)this["tablePrefix"];
            }
            set
            {
                this["tablePrefix"] = value;
            }
        }

        [ConfigurationProperty("storageConnectionString", DefaultValue = "", IsRequired = false)]
        public string StorageConnectionString
        {
            get
            {
                return (string)this["storageConnectionString"];
            }
            set
            {
                this["storageConnectionString"] = value;
            }
        }

        [ConfigurationProperty("locationMode", IsRequired = false)]
        public string LocationMode
        {
            get
            {
                return (string)this["locationMode"];
            }
            set
            {
                this["locationMode"] = value;
            }
        }

    }
    
}