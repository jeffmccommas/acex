﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Portal.Authentication.Helpers
{
    /// <summary>
    /// Used to instantiate multiple TableBatchOperations when the 
    /// TableOperation maximum is reached on a single TableBatchOperation
    /// </summary>
    internal class BatchOperationHelper
    {
        /// <summary>
        /// Current max operations supported in a TableBatchOperation
        /// http://azure.microsoft.com/en-us/documentation/articles/storage-dotnet-how-to-use-tables/#insert-batch
        /// </summary>
        public const int MaxOperationsPerBatch = 100;

        private readonly List<TableBatchOperation> _batches = new List<TableBatchOperation>(100);

        /// <summary>
        /// Adds a TableOperation to a TableBatchOperation
        /// and automatically adds a new TableBatchOperation if max TableOperations are 
        /// exceeded.
        /// </summary>
        /// <param name="operation"></param>
        public void Add(TableOperation operation)
        {
            TableBatchOperation current = GetCurrent();
            if (current.Count == MaxOperationsPerBatch)
            {
                _batches.Add(new TableBatchOperation());
                current = GetCurrent();
            }
            current.Add(operation);
        }

        public async Task<IList<TableResult>> ExecuteBatchAsync(CloudTable table)
        {
            return await new TaskFactory<IList<TableResult>>().StartNew(
            () =>
            {
                ConcurrentBag<TableResult> results = new ConcurrentBag<TableResult>();
                Parallel.ForEach(_batches,
                batchOperation =>
                {
                    var x = table.ExecuteBatch(batchOperation);
                    x.ToList().ForEach(tr => { results.Add(tr); });
                });
                Clear();
                return results.ToList();
            });
        }

        public void Clear()
        {
            _batches.Clear();
        }

        private TableBatchOperation GetCurrent()
        {
            if (_batches.Count < 1)
            {
                _batches.Add(new TableBatchOperation());
            }

            return _batches[_batches.Count - 1];
        }
    }
}