﻿using System;
using Microsoft.AspNet.Identity;

namespace CE.AO.Portal.Authentication.Helpers
{
    public class UriEncodeKeyHelper : BaseKeyHelper
    {
        public const string ReplaceIllegalChar = "%";
        public const string NewCharForIllegalChar = "_";

        public override string GenerateRowKeyUserLoginInfo(UserLoginInfo info)
        {
            string strTemp = $"{EscapeKey(info.LoginProvider)}_{EscapeKey(info.ProviderKey)}";
            return string.Format(strTemp.ToLower());
        }

        public override string GeneratePartitionKeyIndexByLogin(string plainProvider)
        {
            return plainProvider.ToLower();
        }

        public override string GenerateRowKeyUserEmail(string plainEmail)
        {
            return string.Format(plainEmail.ToLower());
        }

        public override string GeneratePartitionKeyIndexByEmail(string plainEmail)
        {
            return EscapeKey(plainEmail).Substring(0, 1).ToLower();
        }

        public override string GenerateRowKeyUserName(string plainUserName)
        {
            return string.Format(plainUserName.ToLower());
        }

        public override string GenerateRowKeyIdentityUserRole(string plainRoleName)
        {
            return string.Format(plainRoleName.ToLower());
        }

        public override string GenerateRowKeyIdentityRole(string plainRoleName)
        {
            return string.Format(plainRoleName.ToLower());
        }

        public override string GeneratePartitionKeyIdentityRole(string plainRoleName)
        {
            return EscapeKey(plainRoleName.Substring(0, 1).ToLower());
        }

        public override string GenerateRowKeyIdentityUserClaim(string claimType, string claimValue)
        {
            string strTemp = $"{EscapeKey(claimType)}_{EscapeKey(claimValue)}";
            return string.Format(strTemp.ToLower());

        }

        public override string ParsePartitionKeyIdentityRoleFromRowKey(string rowKey)
        {
            return rowKey.Substring(Constants.RowKeyConstants.PreFixIdentityRole.Length, 1).ToLower();
        }

        private static string EscapeKey(string keyUnsafe)
        {
            if (!string.IsNullOrWhiteSpace(keyUnsafe))
            {
                // Need to replace '%' because azure bug.
                return Uri.EscapeDataString(keyUnsafe).Replace(ReplaceIllegalChar, NewCharForIllegalChar).ToUpper();
            }
            return null;
        }

        public override string GenerateRowKeyIdentityUserLogin(string loginProvider, string providerKey)
        {
            string strTemp = $"{EscapeKey(loginProvider)}_{EscapeKey(providerKey)}";
            return string.Format(strTemp.ToLower());
        }

        public override double KeyVersion => 0;
    }
}