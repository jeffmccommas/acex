﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CE.AO.Portal.Authentication.Model;
using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Portal.Authentication.Helpers
{
    internal sealed class TableQueryHelper<TT> : IQueryable<TT> where TT : IdentityUser, new()
    {
        private readonly TableQuery<TT> _tableQuery;
        private readonly Func<IList<string>, IEnumerable<TT>> _userEntityFunc;
        private readonly TableQueryProviderHelper<TT> _provider;

        internal TableQueryHelper(TableQuery<TT> tableQuery, Func<IList<string>, IEnumerable<TT>> userEntityFunc)
        {
            _tableQuery = tableQuery;
            _userEntityFunc = userEntityFunc;
            _provider = new TableQueryProviderHelper<TT>(_tableQuery, _userEntityFunc);
        }

        public IEnumerator<TT> GetEnumerator()
        {
            var userIds = _tableQuery.Select(u => u.RowKey).ToList();
            var result = _userEntityFunc(userIds);
            return result.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Type ElementType => _tableQuery.ElementType;

        public Expression Expression => _tableQuery.Expression;

        public IQueryProvider Provider => _provider;
    }

    internal class TableQueryProviderHelper<TElement> : IQueryProvider where TElement : IdentityUser, new()
    {
        private readonly TableQuery<TElement> _tableQuery;
        private readonly Func<IList<string>, IEnumerable<TElement>> _userEntityFunc;

        internal TableQueryProviderHelper(TableQuery<TElement> tableQuery, Func<IList<string>,
            IEnumerable<TElement>> userEntityFunc)
        {
            _tableQuery = tableQuery;
            _userEntityFunc = userEntityFunc;
        }

        public IQueryable<TT> CreateQuery<TT>(Expression expression)
        {
            MethodCallExpression mc = expression as MethodCallExpression;

            if (typeof(TElement) == typeof(TT))
            {
                if (mc != null && mc.Method.Name.Equals("Skip", StringComparison.OrdinalIgnoreCase))
                {
                    var skipToUserIds = _tableQuery.Provider.CreateQuery<TElement>(mc.Arguments[0]).Select(u => u.RowKey).ToList();
                    ConstantExpression c = mc.Arguments[1] as ConstantExpression;
                    if (c != null)
                    {
                        var skipList = skipToUserIds.Skip((int)c.Value).ToList();
                        return skipList.SelectMany(s => _userEntityFunc(new List<string> { s }).Cast<TT>()).AsQueryable();
                    }
                }

                //Force the query here to populate roles, claims and logins
                var userIds = _tableQuery.Provider.CreateQuery<TElement>(expression).Select(u => u.RowKey).ToList();

                var result = _userEntityFunc(userIds);
                return result.Cast<TT>().AsQueryable();
            }

            return _tableQuery.Provider.CreateQuery<TT>(expression);
        }

        public IQueryable CreateQuery(Expression expression)
        {
            return CreateQuery<TElement>(expression);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            MethodCallExpression mc = expression as MethodCallExpression;

            if (mc != null && mc.Method.Name.Equals("Count", StringComparison.OrdinalIgnoreCase))
            {
                Expression temp = mc.Arguments[0];
                var list = _tableQuery.Provider.CreateQuery<TElement>(temp).Select(u => u.RowKey).ToList();
                return (TResult)(object)list.Count;
            }

            //Force the query here to populate roles, claims and logins
            var user = _tableQuery.Provider.Execute<TElement>(expression);

            var result = _userEntityFunc(new List<string> { user.RowKey });
            return result.Cast<TResult>().FirstOrDefault();
        }

        public object Execute(Expression expression)
        {
            throw new NotSupportedException();
        }
    }
}