﻿using System;
using Microsoft.AspNet.Identity;

namespace CE.AO.Portal.Authentication.Helpers
{
    public static class KeyHelper
    {
        private static readonly BaseKeyHelper BaseHelper = new UriEncodeKeyHelper();

        public static string GenerateRowKeyUserLoginInfo(this UserLoginInfo info)
        {
            return BaseHelper.GenerateRowKeyUserLoginInfo(info);
        }

        public static string GeneratePartitionKeyIndexByLogin(string plainProvider)
        {
            return BaseHelper.GeneratePartitionKeyIndexByLogin(plainProvider);
        }

        public static string GenerateRowKeyUserEmail(string plainEmail)
        {
            return BaseHelper.GenerateRowKeyUserEmail(plainEmail);
        }

        [Obsolete("User only for 1.2.9.2 and lower. PartitionKey for the email index has changed to the userid to support non-unique email addresses")]
        public static string GeneratePartitionKeyIndexByEmail(string plainEmail)
        {
            return BaseHelper.GeneratePartitionKeyIndexByEmail(plainEmail);
        }

        public static string GenerateRowKeyUserName(string plainUserName)
        {
            return BaseHelper.GenerateRowKeyUserName(plainUserName);
        }

        public static string GenerateRowKeyIdentityUserRole(string plainRoleName)
        {
            return BaseHelper.GenerateRowKeyIdentityUserRole(plainRoleName);
        }

        public static string GenerateRowKeyIdentityRole(string plainRoleName)
        {
            return BaseHelper.GenerateRowKeyIdentityRole(plainRoleName);
        }

        public static string GeneratePartitionKeyIdentityRole(string plainRoleName)
        {
            return BaseHelper.GeneratePartitionKeyIdentityRole(plainRoleName);
        }

        public static string GenerateRowKeyIdentityUserClaim(string claimType, string claimValue)
        {
            return BaseHelper.GenerateRowKeyIdentityUserClaim(claimType, claimValue);
        }

        public static string GenerateRowKeyIdentityUserLogin(string loginProvider, string providerKey)
        {
            return BaseHelper.GenerateRowKeyIdentityUserLogin(loginProvider, providerKey);
        }

        public static string ParsePartitionKeyIdentityRoleFromRowKey(string rowKey)
        {
            return BaseHelper.ParsePartitionKeyIdentityRoleFromRowKey(rowKey);
        }

        public static double KeyVersion => BaseHelper.KeyVersion;
    }
}