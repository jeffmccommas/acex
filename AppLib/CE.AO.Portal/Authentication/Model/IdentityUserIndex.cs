﻿using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Portal.Authentication.Model
{
    internal class IdentityUserIndex : TableEntity
    {
        /// <summary>
        /// Holds the userid entity key
        /// </summary>
        public string Id { get; set; }

        public double KeyVersion { get; set; }

    }
}
