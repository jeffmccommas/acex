﻿using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Portal.Authentication.Model
{
    public class IdentityUserClaim : IdentityUserClaim<string>
    {
        public double KeyVersion { get; set; }

       [IgnoreProperty]
        public override string UserId
        {
            get
            {
                return PartitionKey;
            }
            set
            {
                PartitionKey = value;
            }
        }
    }

    public class IdentityUserClaim<TKey> : TableEntity
    {
        public virtual string ClaimType { get; set; }

        public virtual string ClaimValue { get; set; }

        public string Id { get; set; }

        public virtual TKey UserId { get; set; }

    }

}
