﻿using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Portal.Authentication.Model
{
    public class IdentityUserRole : IdentityUserRole<string>
    {
        
        public double KeyVersion { get; set; }

        public string Id
        {
            get
            {
                return RoleId;
            }
            set
            {
                RoleId = value;
            }
        }

        [IgnoreProperty]
        public override string UserId
        {
            get
            {
                return PartitionKey;
            }
            set
            {
                PartitionKey = value;
            }
        }


    }


    public class IdentityUserRole<TKey> : TableEntity
    {

        [IgnoreProperty]
        public virtual TKey RoleId { get; set; }

        public virtual TKey UserId { get; set; }

        public string RoleName { get; set; }

    }

}
