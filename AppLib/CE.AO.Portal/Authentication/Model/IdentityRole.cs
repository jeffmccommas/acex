﻿using System.Collections.Generic;
using System.Data.Services.Common;
using Microsoft.AspNet.Identity;
using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Portal.Authentication.Model
{
    [DataServiceKey("PartitionKey", "RowKey")]
    public class IdentityRole : IdentityRole<string, IdentityUserRole>
    {
        public IdentityRole()
        { }

        public double KeyVersion { get; set; }

        public IdentityRole(string roleName)
            : this()
        {
            Name = roleName;
        }

        [IgnoreProperty]
        public override string Id
        {
            get
            {
                return RowKey;
            }
            set
            {
                RowKey = value;
            }
        }
    }

    public class IdentityRole<TKey, TUserRole> : TableEntity,
         IRole<TKey> where TUserRole : IdentityUserRole<TKey>
    {

        public IdentityRole()
        {
            Users = new List<TUserRole>();
        }

        [IgnoreProperty]
        public virtual TKey Id { get; set; }

        public string Name { get; set; }
     
        [IgnoreProperty]
        public ICollection<TUserRole> Users { get; private set; }

    }
}
