﻿using System.Threading.Tasks;

namespace CE.AO.Portal.Authentication.Model
{
    public interface IClientDomain
    {
        Task<string> GetClientByDomainAsync(string domain);
    }
}