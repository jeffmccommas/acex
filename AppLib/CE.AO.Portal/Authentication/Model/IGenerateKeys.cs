﻿
namespace CE.AO.Portal.Authentication.Model
{
    public interface IGenerateKeys
    {
        void GenerateKeys();

        string PeekRowKey();

        double KeyVersion { get; set; }

    }
}
