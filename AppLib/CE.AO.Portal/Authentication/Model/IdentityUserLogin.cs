﻿using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Portal.Authentication.Model
{
    public class IdentityUserLogin : IdentityUserLogin<string>
    {
        
        public double KeyVersion { get; set; }

        [IgnoreProperty]
        public override string UserId
        {
            get
            {
                return PartitionKey;
            }
            set
            {
                PartitionKey = value;
            }
        }
    }

    public class IdentityUserLogin<TKey> : TableEntity
    {
        public virtual string LoginProvider { get; set; }

        public virtual string ProviderKey { get; set; }

        public virtual TKey UserId { get; set; }

        public virtual string Id { get; set; }

    }

}
