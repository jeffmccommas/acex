﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Portal.Authentication.Model
{
    public class IdentityUser : IdentityUser<string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>
        , IUser

    {
        public IdentityUser() { }

        public IdentityUser(string userName)
            : this()
        {
            UserName = userName;
        }

        public double KeyVersion { get; set; }

        public override string Id
        {
            get
            {
                return RowKey;
            }
            set
            {
                RowKey = value;
            }
        }

        public sealed override string UserName
        {
            get
            {
                return base.UserName;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    base.UserName = value.Trim();
                }
            }
        }
    }

    public class IdentityUser<TKey, TLogin, TRole, TClaim> : TableEntity,
        IUser<TKey>
        where TLogin : IdentityUserLogin<TKey>
        where TRole : IdentityUserRole<TKey>
        where TClaim : IdentityUserClaim<TKey>
    {
        public IdentityUser()
        {
            Claims = new List<TClaim>(10);
            Roles = new List<TRole>(10);
            Logins = new List<TLogin>(10);
        }

        public virtual int AccessFailedCount { get; set; }

        [IgnoreProperty]
        public ICollection<TClaim> Claims { get; private set; }

        public virtual string Email { get; set; }

        public virtual bool EmailConfirmed { get; set; }

        [IgnoreProperty]
        public virtual TKey Id { get; set; }

        public virtual bool LockoutEnabled { get; set; }

        public virtual DateTime? LockoutEndDateUtc { get; set; }

        [IgnoreProperty]
        public ICollection<TLogin> Logins { get; private set; }

        public virtual string PasswordHash { get; set; }

        public virtual string PhoneNumber { get; set; }

        public virtual bool PhoneNumberConfirmed { get; set; }

        [IgnoreProperty]
        public ICollection<TRole> Roles { get; private set; }

        public virtual string SecurityStamp { get; set; }

        public virtual bool TwoFactorEnabled { get; set; }

        public virtual string UserName { get; set; }

        public virtual string ClientId { get; set; }

        public virtual string CustomerId { get; set; }

    }

}
