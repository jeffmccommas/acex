﻿namespace CE.AO.Portal.Models
{
    public class MeterList
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public string BillCycleId { get; set; }
    }
}