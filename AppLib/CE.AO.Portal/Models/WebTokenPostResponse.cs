﻿using System;

namespace CE.AO.Portal.Models
{
    public class WebTokenPostResponse
    {
        public bool Message { get; set; }
        public string WebToken { get; set; }
        public bool Status { get; set; }
        public DateTime CreatedUtcDateTime { get; set; }
    }
}