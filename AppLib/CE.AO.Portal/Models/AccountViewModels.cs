﻿using System.ComponentModel.DataAnnotations;

namespace CE.AO.Portal.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = @"Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string Action { get; set; }
        public string ReturnUrl { get; set; }
    }

    public class ManageUserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = @"Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = @"The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = @"New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = @"Confirm new password")]
        [Compare("NewPassword", ErrorMessage = @"The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = @"Email")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = @"The Email field is not a valid e-mail address.")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = @"Password")]
        public string Password { get; set; }

        [Display(Name = @"Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = @"Email")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = @"The Email field is not a valid e-mail address.")]
        public string Email { get; set; }

        [Required]
        //[StringLength(100, ErrorMessage = @"The {0} must be at least {2} characters long.", MinimumLength = 6)]

        //^(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?=.*[a-z])(?=.*[A-Z])(?i-msnx:(?!.*pass|.*password|.*word|.*god|.*\s))(?!^.*\n).*$
        [RegularExpression(@"^(?=.{6})(?=.*[0-9])(?=.*[^A-Za-z])(?=.*[A-Z])(?=.*[a-z]).*$", ErrorMessage = @"Password must be 6 characters long, containing at least one uppercase letter(A-Z), one no. between (0-9) & one special character.")]
        [DataType(DataType.Password)]
        [Display(Name = @"Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = @"Confirm password")]
        [Compare("Password", ErrorMessage = @"The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = @"Customer ID")]
        public string CustomerId { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [Display(Name = @"Email")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = @"The Email field is not a valid e-mail address.")]
        public string Email { get; set; }

        [Required]
        //[StringLength(100, ErrorMessage = @"The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [RegularExpression(@"^(?=.{6})(?=.*[0-9])(?=.*[^A-Za-z])(?=.*[A-Z])(?=.*[a-z]).*$", ErrorMessage = @"Password must be 6 characters long, containing at least one uppercase letter(A-Z), one no. between (0-9) & one special character.")]
        [DataType(DataType.Password)]
        [Display(Name = @"Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = @"Confirm password")]
        [Compare("Password", ErrorMessage = @"The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [Display(Name = @"Email")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = @"The Email field is not a valid e-mail address.")]
        public string Email { get; set; }
    }
}
