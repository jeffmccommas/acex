﻿using System;

namespace CE.AO.Portal.Models
{
    [Serializable]
    public class AmiReadingModels
    { 
        public double ReadingValue { get; set; }
        public DateTime ReadingDate { get; set; }
    }
}
