﻿using CE.AO.Portal.Areas.Reports.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AO.BusinessContracts;
using CE.AO.Portal.Controllers;
using Microsoft.Practices.Unity;

namespace CE.AO.Portal.Areas.Reports.Controllers
{
    public class NotificationController : BaseController
    {
        // GET: Reports/Notification
        public ActionResult Index() 
        {
            return View();
        }

        public ActionResult Charts(int clientId,string interval , DateTime startDate, DateTime endDate)
        {
            var a = NotificationStats(clientId, startDate, endDate);
            
            if (a != null && a.PiaChartList.Count > 0 )
            {
                    a.ClientId = clientId.ToString();
                    a.DeliveryRate = (Convert.ToInt16(a.PiaChartList.Find(p => p.Category == "Delivered").Value) * 100 /Convert.ToInt16(a.PiaChartList.Find(p => p.Category == "Notified").Value)).ToString() + "%";
                    a.TotalDelivery= a.PiaChartList.Find(p => p.Category == "Delivered").Value;
                    a.TotalNotifications = a.PiaChartList.Find(p => p.Category == "Notified").Value;
                    a.Interval = interval;
                    a.TotalBounced = a.PiaChartList.Find(p => p.Category == "Bounced").Value;
            }
            
            return PartialView(a);
        }

        public NotificationReportList NotificationStats(int clientId,DateTime startDate,DateTime endDate)
        {
            INotification notificationsys = UnityContainer.Resolve<INotification>();

            var notificationData = notificationsys.GetNotifications(clientId, startDate, endDate);

            NotificationReportList nrl = new NotificationReportList();
            List<NotificationReportModel> nm = new List<NotificationReportModel>();

            #region unused

            //var query = (from t in Transactions
            //             group t by new { t.MaterialIDnm, t.ProductID }
            // into grp
            //             select new
            //             {
            //                 grp.Key.MaterialID,
            //                 grp.Key.ProductID,
            //                 Quantity = grp.Sum(t => t.Quantity)
            //             }).ToList();

            //var query = (from t in notificationData
            //             group t by t.NotifiedDateTime.ToString("yyyy/mm/dd")
            // into grp
            //             select new
            //             {
            //                count = grp.Sum(p=>p.ClientId)
            //             }).ToList();

            //var qry = cust.GroupBy(cm => new { cm.Customer, cm.OrderDate },
            // (key, group) => new {
            //     Key1 = key.Customer,
            //     Key2 = key.OrderDate,
            //     Count = group.Count()
            // });

            //var qry = notificationData.GroupBy(cm => cm.NotifiedDateTime.ToString("yyyy/mm/dd"),
            // (key, group) => new
            // {
            //     Key1 = key.Customer,
            //     Key2 = key.OrderDate,
            //     Count = group.Count()
            // });

#endregion

            foreach (var notification in notificationData)
            {
                string notifieddate = notification.NotifiedDateTime.ToString("yyyy/MM/dd");

                if (nm.Count(p => p.Date == notifieddate) > 0)
                {
                    foreach (NotificationReportModel u in nm.Where(u => u.Date == notifieddate))
                    {
                        switch (u.Type)
                        {
                            case "Notified":
                            {
                                    if (notification.IsNotified != null && (bool)notification.IsNotified )
                                    {
                                        u.Count = u.Count + 1;
                                    }
                                    break;
                                }
                            case "Delivered":
                            {
                                    if (notification.IsDelivered != null && (bool)notification.IsDelivered)
                                    {
                                        u.Count = u.Count + 1;
                                    }
                                    break;
                            }
                            case "Bounced":
                            {
                                    if (notification.IsBounced != null && (bool)notification.IsBounced)
                                    {
                                        u.Count = u.Count + 1;
                                    }
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    nm.Add(new NotificationReportModel() {Date = notification.NotifiedDateTime.ToString("yyyy/MM/dd") , Count = 1 , Type = "Notified" });
                    nm.Add(new NotificationReportModel() { Date = notification.NotifiedDateTime.ToString("yyyy/MM/dd"), Count = 1, Type = "Delivered" });
                    nm.Add(new NotificationReportModel() { Date = notification.NotifiedDateTime.ToString("yyyy/MM/dd"), Count = 1, Type = "Bounced" });
                }
            }

            #region unused 
            //nm.Add(new NotificationReportModel() { Date = "17/02/2016", Count = 20, Type = "Notified" });
            //nm.Add(new NotificationReportModel() { Date = "17/02/2016", Count = 13, Type = "Delivered" });
            //nm.Add(new NotificationReportModel() { Date = "17/02/2016", Count = 7, Type = "Bounced" });

            //nm.Add(new NotificationReportModel() { Date = "18/02/2016", Count = 18, Type = "Notified" });
            //nm.Add(new NotificationReportModel() { Date = "18/02/2016", Count = 18, Type = "Delivered" });
            //nm.Add(new NotificationReportModel() { Date = "18/02/2016", Count = 0, Type = "Bounced" });

            //nm.Add(new NotificationReportModel() { Date = "19/02/2016", Count = 10, Type = "Notified" });
            //nm.Add(new NotificationReportModel() { Date = "19/02/2016", Count = 8, Type = "Delivered" });
            //nm.Add(new NotificationReportModel() { Date = "19/02/2016", Count = 2, Type = "Bounced" });

            //nm.Add(new NotificationReportModel() { Date = "20/02/2016", Count = 12, Type = "Notified" });
            //nm.Add(new NotificationReportModel() { Date = "20/02/2016", Count = 1, Type = "Delivered" });
            //nm.Add(new NotificationReportModel() { Date = "20/02/2016", Count = 0, Type = "Bounced" });

            //var jsonResult = Json(nm, JsonRequestBehavior.AllowGet);
            //jsonResult.MaxJsonLength = int.MaxValue;
            //return jsonResult;
            #endregion

            nrl.StackChartList = nm;

            var a = (from d in nrl.StackChartList
                    group d by d.Type into g
                      select new PiaChartModel() { Category = g.First().Type, Value = g.Sum(e => e.Count).ToString() }).ToList();
            nrl.PiaChartList = a;                  

            return nrl;

        }
    }
}