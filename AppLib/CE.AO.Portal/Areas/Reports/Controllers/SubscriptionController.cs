﻿using CE.AO.Portal.Areas.Reports.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AO.BusinessContracts;
using CE.AO.Models;
using CE.AO.Portal.Controllers;
using Microsoft.Practices.Unity;

namespace CE.AO.Portal.Areas.Reports.Controllers
{
    public class SubscriptionController : BaseController
    {
        // GET: Reports/Subscription
        public ActionResult Index()
        {
            //var clientSettings =ClientSettings.GetClientSettings(Convert.ToInt32(87));
            return View();
        }

        public ActionResult SubscriptionCharts(int clientId, string interval, DateTime startDate, DateTime endDate, string program)
        {
            var a = GetSubscriptionList(clientId, startDate, endDate, program);

            if (a != null && a.PiaChartList.Count > 0)
            {
                a.ClientId = clientId.ToString();
                a.Interval = string.IsNullOrEmpty(interval) ? "0" : interval;
                a.Program = program;
                a.SubscribedUser = a.PiaChartList.Find(p => p.Category == "Subscribed") == null ? "0" : a.PiaChartList.Find(p => p.Category == "Subscribed").Value;
                a.UnSubscribedUser = a.PiaChartList.Find(p => p.Category == "UnSubscribed") == null ? "0" : a.PiaChartList.Find(p => p.Category == "UnSubscribed").Value;
            }

            return PartialView(a);
        }

        //public JsonResult GetCascadeProducts(int? categories)
        public JsonResult GetProgramsForClientId(int clientId)
        {
            var clientConfigFacadeManager = UnityContainer.Resolve<IClientConfigFacade>();
            var clientSettings = clientConfigFacadeManager.GetClientSettings(Convert.ToInt32(UserManager.ClientId));

            return Json(clientSettings.Programs.Select(p => new { p.ProgramName, ProgramId = p.ProgramName }), JsonRequestBehavior.AllowGet);
        }

        public SubscriptionReportList GetSubscriptionList(int clientId, DateTime startDate, DateTime endDate, string program)
        {
            ISubscription subscriptionsys = UnityContainer.Resolve<ISubscription>();
            SubscriptionReportList subscriptionList = new SubscriptionReportList();
            var listofsubscriptionmodel = subscriptionsys.GetSubscription(clientId, startDate, endDate, program);

            List<SubscriptonReportModel> nm = new List<SubscriptonReportModel>();

            foreach (SubscriptionModel model in listofsubscriptionmodel)
            {
                string subscribeddate = model.SubscriptionDate?.ToString("yyyy/MM/dd");
                //bool isSubscribed = model.IsSelected;
                string type = model.IsSelected ? "Subscribed" : "UnSubscribed";

                if (nm.Count(p => p.Date == subscribeddate && p.Type == type) > 0)
                {
                    foreach (SubscriptonReportModel u in nm.Where(u => u.Date == subscribeddate))
                    {
                        switch (u.Type)
                        {
                            case "Subscribed":
                                {
                                    u.Count = u.Count + 1;
                                    break;
                                }
                            case "UnSubscribed":
                                {
                                    u.Count = u.Count + 1;
                                    break;
                                }
                        }
                    }
                }
                else
                {
                    nm.Add(new SubscriptonReportModel { Date = model.SubscriptionDate?.ToString("yyyy/MM/dd"), Count = 1, Type = type });
                }
            }

            subscriptionList.StackChartList = nm;


            var a = (from d in subscriptionList.StackChartList
                     group d by d.Type into g
                     select new PiaChartModel() { Category = g.First().Type, Value = g.Sum(e => e.Count).ToString() }).ToList();
            subscriptionList.PiaChartList = a;

            return subscriptionList;
        }

    }
}