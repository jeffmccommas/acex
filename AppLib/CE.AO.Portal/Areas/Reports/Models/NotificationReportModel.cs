﻿using System.Collections.Generic;

namespace CE.AO.Portal.Areas.Reports.Models
{
    public class NotificationReportModel
    {
        //"date": "11/30/2011",
        //"close": 192.29,
        //"volume": 7700490,
        //"open": 194.76,
        //"high": 195.3,
        //"low": 188.75,
        //"symbol": "3. AMZN"

        public string Date { get; set; }
        public int Count { get; set; }
        public string Type { get; set; }

    }

    public class NotificationReportList
    {
        //"date": "11/30/2011",
        //"close": 192.29,
        //"volume": 7700490,
        //"open": 194.76,
        //"high": 195.3,
        //"low": 188.75,
        //"symbol": "3. AMZN"

        public List<NotificationReportModel> StackChartList { get; set; }
        public List<PiaChartModel> PiaChartList { get; set; }

        public string ClientId { get; set; }
        public string Interval{ get; set; }
        public string TotalNotifications { get; set; }
        public string TotalDelivery { get; set; }
        public string DeliveryRate { get; set; }
        public string TotalBounced { get; set; }
    }

    public class PiaChartModel
    {
        public string Category { get; set; }
        public string Value { get; set; }
    }
}