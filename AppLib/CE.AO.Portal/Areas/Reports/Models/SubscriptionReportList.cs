﻿using System.Collections.Generic;

namespace CE.AO.Portal.Areas.Reports.Models
{
    public class SubscriptionReportList
    {
        public List<SubscriptonReportModel> StackChartList { get; set; }
        public List<PiaChartModel> PiaChartList { get; set; }
        public string ClientId { get; set; }
        public string Interval { get; set; }
        public string Program { get; set; }
        public string SubscribedUser  { get; set; }
        public string UnSubscribedUser { get; set; }

    }

    public class SubscriptonReportModel
    {
        //"date": "11/30/2011",
        //"close": 192.29,
        //"volume": 7700490,
        //"open": 194.76,
        //"high": 195.3,
        //"low": 188.75,
        //"symbol": "3. AMZN"

        public string Date { get; set; }
        public int Count { get; set; }
        public string Type { get; set; }

    }

}