﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;
using AO.BusinessContracts;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Portal.Models;
using Microsoft.Practices.Unity;
using Newtonsoft.Json.Linq;

namespace CE.AO.Portal.Controllers
{
    [Authorize]
    public class DashboardController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.UsageChartNavigator = ConfigurationManager.AppSettings.Get("UsageChartNavigator");
            ViewBag.Date = DateTime.UtcNow.AddDays(-1).ToString("dd MMM yyyy");
            IPremise premise = UnityContainer.Resolve<IPremise>();
            IEnumerable<PremiseModel> tasks = premise.GetPremises(Convert.ToInt32(UserManager.ClientId), CustomerId);

            var premiseModel = tasks.FirstOrDefault(c => c.ClientId == Convert.ToInt32(UserManager.ClientId));
            
            if (premiseModel != null)
            {
                var address = string.Empty;
                if (!string.IsNullOrEmpty(premiseModel.AddressLine1))
                    address = address + premiseModel.AddressLine1;

                if (!string.IsNullOrEmpty(premiseModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + premiseModel.AddressLine2
                        : address + premiseModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(premiseModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + premiseModel.AddressLine3
                        : address + premiseModel.AddressLine3;
                }
                ViewBag.Address = address;
            }
            
            FillDropDownData();
            return View();
        }

        //Fill the dropdown
        private void FillDropDownData()
        {
            var units = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = @"Units: Gallons",
                    Value = Utilities.Enums.UomType.gal.ToString()
                },
                new SelectListItem
                {
                    Text = @"Units: HCF",
                    Value = Utilities.Enums.UomType.ccf.ToString()
                }
            };
            ViewData["Units"] = units;

            var dayMeasure = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = @"Hours",
                    Value = "minutes"
                },
                new SelectListItem
                {
                    Text = @"Days",
                    Value = "weeks"
                },
                new SelectListItem
                {
                   Text = @"Months",
                   Value = "months"
                }
            };

            ViewData["DayMeasure"] = dayMeasure;

            IPremise premise = UnityContainer.Resolve<IPremise>();
            IEnumerable<PremiseModel> tasks = premise.GetPremises(Convert.ToInt32(UserManager.ClientId), CustomerId);

            List<SelectListItem> accountId = tasks.Select(account => new SelectListItem { Value = account.AccountId, Text = account.AccountId }).ToList();

            //if (tasks.Count() <= 1)
            //{
            //    ViewData["AccountId"] = tasks.Count() == 0 ? null : tasks.First().AccountId;
            //}
            //else
            {
                ViewData["AccountIds"] = accountId.GroupBy(x => x.Text).Select(x => x.First()).ToList();
            }
        }

        private List<ReadingModel> GetReadingData(DateTime startDate, DateTime endDate, string meterValue, string unitValue)
        {
            var clientConfigFacadeManager = UnityContainer.Resolve<IClientConfigFacade>();
            var clientSettings = clientConfigFacadeManager.GetClientSettings(Convert.ToInt32(UserManager.ClientId));
            var clientTimezone = (Utilities.Enums.Timezone)Enum.Parse(typeof(Utilities.Enums.Timezone), Convert.ToString(clientSettings.TimeZone));

            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(clientTimezone.GetDescription());

            var startDateTime = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(startDate), timeZoneInfo);
            var endDateTime = TimeZoneInfo.ConvertTimeToUtc(new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59), timeZoneInfo);
            
            var tallAmiManager = UnityContainer.Resolve<ITallAMI>();
            var amiList = tallAmiManager.GetAmiList(startDateTime, endDateTime, meterValue, Convert.ToInt32(UserManager.ClientId));
            return GetAmiReadingsUnit(tallAmiManager.GetReadings(amiList), unitValue);
        }

        public ActionResult GetData(string startDate, string endDate, string isPrevNext, string value, string meterValue, string billCycleId, string unitValue)
        {
            DateTime startDateUpdated;
            DateTime endDateUpdated;
            var billCycleScheduleManager = UnityContainer.Resolve<IBillingCycleSchedule>();
            BillCycleScheduleModel dates;

            switch (isPrevNext)
            {
                case "Next":
                    if (value == "months")
                    {
                        endDateUpdated = Convert.ToDateTime(endDate).AddDays(1);
                        dates = billCycleScheduleManager.GetBillingCycleScheduleByDateAsync(Convert.ToInt32(UserManager.ClientId), billCycleId, endDateUpdated);
                        if (dates == null) return null;
                        startDateUpdated = Convert.ToDateTime(dates.BeginDate);
                        endDateUpdated = Convert.ToDateTime(dates.EndDate);
                    }
                    else if (value == "weeks")
                    {
                        startDateUpdated = Convert.ToDateTime(endDate).AddDays(1);
                        endDateUpdated = Convert.ToDateTime(startDateUpdated).AddDays(30);
                    }
                    else
                    {
                        startDateUpdated = Convert.ToDateTime(endDate).AddDays(1);
                        endDateUpdated = Convert.ToDateTime(endDate).AddDays(1);
                    }
                    break;
                case "Prev":
                    if (value == "months")
                    {
                        startDateUpdated = Convert.ToDateTime(startDate).AddDays(-1);
                        dates = billCycleScheduleManager.GetBillingCycleScheduleByDateAsync(Convert.ToInt32(UserManager.ClientId), billCycleId, startDateUpdated);
                        if (dates == null) return null;
                        startDateUpdated = Convert.ToDateTime(dates.BeginDate);
                        endDateUpdated = Convert.ToDateTime(dates.EndDate);
                    }
                    else if (value == "weeks")
                    {
                        endDateUpdated = Convert.ToDateTime(startDate).AddDays(-1);
                        startDateUpdated = Convert.ToDateTime(endDateUpdated).AddDays(-30);
                    }
                    else
                    {
                        startDateUpdated = Convert.ToDateTime(endDate).AddDays(-1);
                        endDateUpdated = Convert.ToDateTime(endDate).AddDays(-1);
                    }
                    break;
                default:
                    startDateUpdated = Convert.ToDateTime(startDate);
                    endDateUpdated = Convert.ToDateTime(endDate);
                    break;
            }
            var amiReadings = GetReadingData(startDateUpdated, endDateUpdated, meterValue, unitValue);
            var jsonResult = Json(new
            {
                startDate = startDateUpdated.ToString("dd MMM yyyy"),
                endDate = endDateUpdated.ToString("dd MMM yyyy"),
                amiReadingData = amiReadings
            }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public List<ReadingModel> GetAmiReadingsUnit(List<ReadingModel> amiList, string unitValue)
        {
            if (unitValue.Equals(Utilities.Enums.UomType.ccf.ToString()))
            {
                foreach (var aR in amiList)
                {
                    if (aR.UOMId == (int)Utilities.Enums.UomType.cf)
                    {
                        aR.Quantity = aR.Quantity * 0.01;
                    }
                    if (aR.UOMId == (int)Utilities.Enums.UomType.cgal)
                    {
                        aR.Quantity = aR.Quantity * 1.336806;
                    }
                    if (aR.UOMId == (int)Utilities.Enums.UomType.hgal)
                    {
                        aR.Quantity = aR.Quantity * 1.336806;
                    }
                    if (aR.UOMId == (int)Utilities.Enums.UomType.gal)
                    {
                        aR.Quantity = aR.Quantity * 0.001336806;
                    }
                    if (aR.UOMId == (int)Utilities.Enums.UomType.kgal)
                    {
                        aR.Quantity = aR.Quantity * 0.001336806 * 1000;
                    }
                    aR.UOMId = (int)Utilities.Enums.UomType.ccf;
                }
            }
            else if (unitValue.Equals(Utilities.Enums.UomType.gal.ToString()))
            {
                foreach (var aR in amiList)
                {
                    if (aR.UOMId == (int)Utilities.Enums.UomType.cf)
                    {
                        aR.Quantity = aR.Quantity * 7.48051948052;
                    }
                    if (aR.UOMId == (int)Utilities.Enums.UomType.cgal)
                    {
                        aR.Quantity = aR.Quantity * 0.01;
                    }
                    if (aR.UOMId == (int)Utilities.Enums.UomType.hgal)
                    {
                        aR.Quantity = aR.Quantity * 0.01;
                    }
                    if (aR.UOMId == (int)Utilities.Enums.UomType.ccf)
                    {
                        aR.Quantity = aR.Quantity * 748.051948052;
                    }
                    if (aR.UOMId == (int)Utilities.Enums.UomType.kgal)
                    {
                        aR.Quantity = aR.Quantity * 1000;
                    }
                    aR.UOMId = (int)Utilities.Enums.UomType.gal;
                }
            }

            // format the usage to 2 decimal point
            amiList.ForEach(a => a.Quantity = Math.Round(a.Quantity, 2));
            return amiList;
        }

        public ActionResult GetDayMeasureData(string strStartDate, string strEndDate, string value, string meterValue, string billCycleId, string unitValue)
        {
            var startDate = Convert.ToDateTime(strStartDate);
            var endDate = Convert.ToDateTime(strEndDate);
            DateTime startDateUpdated;
            DateTime endDateUpdated;
            switch (value)
            {
                case "months":
                    var billCycleScheduleManager = UnityContainer.Resolve<IBillingCycleSchedule>();
                    var dates =
                        billCycleScheduleManager.GetBillingCycleScheduleByDateAsync(Convert.ToInt32(UserManager.ClientId),
                            billCycleId, endDate);
                    if (dates == null) return null;
                    startDateUpdated = Convert.ToDateTime(dates.BeginDate);
                    endDateUpdated = Convert.ToDateTime(dates.EndDate);
                    break;
                case "weeks":
                    startDateUpdated = endDate.AddDays(-30);
                    endDateUpdated = endDate;
                    break;
                case "minutes":
                    startDateUpdated = new DateTime(endDate.Year, endDate.Month, endDate.Day, 0, 0, 0);
                    endDateUpdated = endDate;
                    break;
                default:
                    startDateUpdated = new DateTime(startDate.Year, startDate.Month, 1);
                    endDateUpdated = startDateUpdated.AddMonths(1).AddDays(-1);
                    break;

            }
            var amiReadings = GetReadingData(startDateUpdated, endDateUpdated, meterValue, unitValue);
            var jsonResult = Json(new
            {
                startDate = startDateUpdated.ToString("dd MMM yyyy"),
                endDate = endDateUpdated.ToString("dd MMM yyyy"),
                amiReadingData = amiReadings
            }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult GetMeters(string account)
        {
            IBilling billing = UnityContainer.Resolve<IBilling>();

            var serviceList = billing.GetAllServicesFromLastBill(Convert.ToInt32(UserManager.ClientId), CustomerId, account).ToList();
            var meter = serviceList.Where(e => e.MeterType.ToLower() != "nonmetered").ToList();

            var meterIds =
                meter.Select(
                    mId =>
                        new MeterList
                        {
                            Value = mId.MeterId,
                            Text = mId.MeterId,
                            BillCycleId = mId.BillCycleScheduleId
                        }).ToList();

            var jsonResult = Json(meterIds, JsonRequestBehavior.AllowGet);

            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult GetMeterId(string strStartDate, string strEndDate, string meterValue, string unitValue)
        {
            var startDate = Convert.ToDateTime(strStartDate);
            var endDate = Convert.ToDateTime(strEndDate);
            var amiReadingData = GetReadingData(startDate, endDate, meterValue, unitValue);

            var jsonResult = Json(new
            {
                amiReadingData
            }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult WidgetUrl(string account, string meter)
        {
            IBilling billing = UnityContainer.Resolve<IBilling>();
            IClientWidgetConfiguration widget = UnityContainer.Resolve<IClientWidgetConfiguration>();
            var widgetDetails = widget.GetClientWidgetConfiguration(Convert.ToInt32(UserManager.ClientId)).FirstOrDefault();
            var webTokenUrl = widgetDetails?.WebTokenUrl;
            var widgetPageUrl = widgetDetails?.WidgetPageUrl;
            var meterPremise = billing.GetAllServicesFromLastBill(Convert.ToInt32(UserManager.ClientId), CustomerId, account).ToList();
            var premiseId = meterPremise.FirstOrDefault()?.PremiseId;
            //var premiseId = "1218337000";
            //var customerId = 993774751;
            //account = "1218337093";
            var clientId = UserManager.ClientId;
            string finalWebPageUrl;
            using (var handler = new HttpClientHandler { Credentials = new NetworkCredential(widgetDetails?.AccessKeyId, widgetDetails?.Password) })
            using (var client = new HttpClient(handler))
            {
                var accessKeyId = widgetDetails?.AccessKeyId;

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("X-CE-AccessKeyId", accessKeyId);
                JObject customerData = new JObject { { "CustomerId", CustomerId } };

                finalWebPageUrl = generateWebToken(client, customerData, widgetPageUrl, webTokenUrl, clientId, CustomerId, account, premiseId);
            }
            var jsonResult = Json(finalWebPageUrl, JsonRequestBehavior.AllowGet);

            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        
        private string generateWebToken(HttpClient client, JObject customerData, string widgetPageUrl, string webTokenUrl, string clientId, string customerId, string account, string premiseId)
        {
            var finalWebPageUrl = string.Empty;
            var response = client.PostAsJsonAsync(webTokenUrl, customerData).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsAsync<WebTokenPostResponse>().Result;
                finalWebPageUrl =
                    $"{widgetPageUrl}&ClientId={clientId}&Locale=en-US&CustomerId={customerId}&AccountId={account}&PremiseId={premiseId}&ServicePointId=&WebToken={result.WebToken}";
            }
            return finalWebPageUrl;
        }
    }
}