﻿using Microsoft.Practices.Unity;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using CE.AO.Portal.App_Start;
using Microsoft.AspNet.Identity;
using CE.AO.Portal.Models;

namespace CE.AO.Portal.Controllers
{
    public class BaseController : Controller
    {
        // ReSharper disable once InconsistentNaming
        protected ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            protected set
            {
                _userManager = value;
            }
        }
        public IUnityContainer UnityContainer { get; set; }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            UnityContainer = UnityConfig.GetConfiguredContainer();
        }

        public Task<string> GetcustomerIdAsync()
        {
            var email = User.Identity.GetUserName();
            var azureStore = new Authentication.UserStore<ApplicationUser>(new ApplicationDbContext());
            var customerId = azureStore.GetCustomerByEmailAsync(email);
            return customerId;
        }

        public string CustomerId
        {
            get
            {
                var customerId = GetcustomerIdAsync();
                return customerId.Result;
            }
        }
    }
}