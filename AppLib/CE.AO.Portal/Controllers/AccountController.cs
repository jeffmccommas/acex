﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using CE.AO.Portal.Models;
using System.Collections.Generic;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.Practices.Unity;

namespace CE.AO.Portal.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (UserManager.ClientId != null)
                {
                    var user = await UserManager.FindAsync($"{UserManager.ClientId}_{model.Email}", model.Password);
                    if (user != null)
                    {
                        await SignInAsync(user, model.RememberMe);
                        return RedirectToLocal(returnUrl);
                    }
                    else
                    {
                        ModelState.AddModelError("", @"Invalid email or password.");
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (UserManager.ClientId != null)
                {
                    var key = $"{UserManager.ClientId}_{model.Email.ToLower()}";
                    var user = new ApplicationUser()
                    {
                        PartitionKey = key,
                        RowKey = key,
                        Id = key,
                        UserName = model.Email,
                        Email = model.Email,
                        ClientId = UserManager.ClientId,
                        CustomerId = model.CustomerId
                    };

                    var appUser = UserManager.FindById(user.Id);
                    if (appUser == null)    //checked whether user already exist or not
                    {
                        IdentityResult result = await UserManager.CreateAsync(user, model.Password);

                        if (result.Succeeded)
                        {
                            await SignInAsync(user, isPersistent: false);

                            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                            // Send an email with this link
                            string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                            if (Request.Url != null)
                            {
                                var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code },
                                    Request.Url.Scheme);
                                //var fromEmail = CloudConfigurationManager.GetSetting("FromEmail");
                                var clientConfigFacadeManager = UnityContainer.Resolve<IClientConfigFacade>();
                                var clientSettings =
                                    clientConfigFacadeManager.GetClientSettings(Convert.ToInt32(UserManager.ClientId));
                                // email settings
                                var clientEmailUsername = clientSettings.OptInEmailUserName;
                                var clientEmailPassword = clientSettings.OptInEmailPassword;
                                var clientEmailFrom = clientSettings.OptInEmailFrom;

                                string confirmationTemplateId = clientSettings.RegistrationConfirmationEmailTemplateId;

                                var recipients = new List<string>() { user.Email };

                                var address = string.Empty;
                                var customerName = string.Empty;
                                var customerTask = GetCustomerAsync(Convert.ToInt32(user.ClientId), user.CustomerId);
                                Task.WaitAll(customerTask);
                                if (customerTask.Status == TaskStatus.RanToCompletion)
                                {
                                    var customerModel = customerTask.Result;


                                    if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                                        address = address + customerModel.AddressLine1;

                                    if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                                    {
                                        address = !string.IsNullOrEmpty(address)
                                            ? address + ", " + customerModel.AddressLine2
                                            : address + customerModel.AddressLine2;
                                    }

                                    if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                                    {
                                        address = !string.IsNullOrEmpty(address)
                                            ? address + ", " + customerModel.AddressLine3
                                            : address + customerModel.AddressLine3;
                                    }

                                    customerName = customerModel.FirstName;
                                }

                                var accountNumber = user.CustomerId;
                                if (clientSettings.UnmaskedAccountIdEndingDigit > 0)
                                {
                                    accountNumber = AccountLookupModel.GetMaskedAccountId(accountNumber, clientSettings.UnmaskedAccountIdEndingDigit);
                                }

                                var identifiers = new Dictionary<string, List<string>>
                                {
                                    {"-ResetUrl-", new List<string> {"<a href=" + callbackUrl + ">click here</a>"}},
                                    {"-CustomerName-", new List<string> { customerName}},
                                    {"-AccountNumber-", new List<string> {accountNumber}},
                                    {"-AccountAddress-", new List<string> {address}}
                                };

                                var sendEmail = UnityContainer.Resolve<ISendEmail>();
                            
                                await sendEmail.SendEmailAsync(clientEmailFrom, recipients, identifiers,
                                    confirmationTemplateId, clientEmailUsername, clientEmailPassword, null, null);
                            }
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            AddErrors(result);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", @"This email has already been registered.");
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }

            IdentityResult result = await UserManager.ConfirmEmailAsync(userId, code);
            if (result.Succeeded)
            {
                return View("ConfirmEmail");
            }
            else
            {
                AddErrors(result);
                return View();
            }
        }

        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (UserManager.ClientId != null)
                {
                    var key = $"{UserManager.ClientId}_{model.Email.ToLower()}";
                    var user = await UserManager.FindByNameAsync(key);
                    if (user == null || !await UserManager.IsEmailConfirmedAsync(user.Id))
                    {
                        ModelState.AddModelError("", @"The user either does not exist or is not confirmed.");
                        return View();
                    }

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    if (Request.Url != null)
                    {
                        var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code }, protocol: Request.Url.Scheme);
                        //var fromEmail = CloudConfigurationManager.GetSetting("FromEmail");
                        var clientConfigFacadeManager = UnityContainer.Resolve<IClientConfigFacade>();
                        var clientSettings = clientConfigFacadeManager.GetClientSettings(Convert.ToInt32(UserManager.ClientId));

                        // email settings
                        var clientEmailUsername = clientSettings.OptInEmailUserName;
                        var clientEmailPassword = clientSettings.OptInEmailPassword;
                        var clientEmailFrom = clientSettings.OptInEmailFrom;

                        var address = string.Empty;
                        var customerName = string.Empty;
                        var customerTask = GetCustomerAsync(Convert.ToInt32(UserManager.ClientId), user.CustomerId);
                        Task.WaitAll(customerTask);
                        if (customerTask.Status == TaskStatus.RanToCompletion)
                        {
                            var customerModel = customerTask.Result;


                            if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                                address = address + customerModel.AddressLine1;

                            if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                            {
                                address = !string.IsNullOrEmpty(address)
                                    ? address + ", " + customerModel.AddressLine2
                                    : address + customerModel.AddressLine2;
                            }

                            if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                            {
                                address = !string.IsNullOrEmpty(address)
                                    ? address + ", " + customerModel.AddressLine3
                                    : address + customerModel.AddressLine3;
                            }

                            customerName = customerModel.FirstName;
                        }

                        var accountNumber = user.CustomerId;
                        if (clientSettings.UnmaskedAccountIdEndingDigit > 0)
                        {
                            accountNumber = AccountLookupModel.GetMaskedAccountId(accountNumber, clientSettings.UnmaskedAccountIdEndingDigit);
                        }


                        string emailTemplateId = clientSettings.ForgotPasswordEmailTemplateId; //fe782995-4425-4757-9dbf-f3ec5c8ce343;

                        var sendEmail = UnityContainer.Resolve<ISendEmail>();
                        var recipients = new List<string>() { user.Email };
                        var identifiers = new Dictionary<string, List<string>>
                        {
                            {"-HtmlResetUrl-", new List<string> {"<a href=" + callbackUrl + ">click here</a>"}},
                            {"-ResetUrl-", new List<string> {callbackUrl}},
                            {"-CustomerName-", new List<string> {customerName}},
                            {"-AccountNumber-", new List<string> {accountNumber}},
                            {"-AccountAddress-", new List<string> {address}}
                        };

                        await sendEmail.SendEmailAsync(clientEmailFrom, recipients, identifiers,
                            emailTemplateId, clientEmailUsername, clientEmailPassword, null, null);
                    }
                    return RedirectToAction("ForgotPasswordConfirmation", "Account");
                }
            }

            return View(model);
        }

        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            if (code == null)
            {
                return View("Error");
            }
            return View();
        }

        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (UserManager.ClientId != null)
                {
                    var key = $"{UserManager.ClientId}_{model.Email.ToLower()}";
                    var user = await UserManager.FindByNameAsync(key);
                    if (user == null)
                    {
                        ModelState.AddModelError("", @"No user found.");
                        return View();
                    }
                    IdentityResult result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("ResetPasswordConfirmation", "Account");
                    }
                    else
                    {
                        AddErrors(result);
                        return View();
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasPassword)
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                        await SignInAsync(user, isPersistent: false);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            else
            {
                // User does not have a password so remove any validation errors caused by a missing OldPassword field
                ModelState state = ModelState["OldPassword"];
                state?.Errors.Clear();

                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        //private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, await user.GenerateUserIdentityAsync(UserManager));
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            return user?.PasswordHash != null;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                //return RedirectToAction("Index", "Home");
                return RedirectToAction("Index", "Dashboard");
            }
        }

        /// <summary>
        /// return customer info async from business layer
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        private async Task<CustomerModel> GetCustomerAsync(int clientId, string customerId)
        {

            ICustomer customerManager = UnityContainer.Resolve<ICustomer>();
            var task = await Task.Factory.StartNew(() => customerManager.GetCustomerAsync(clientId, customerId)).ConfigureAwait(false);

            return task.Result;

        }

        #endregion
    }
}