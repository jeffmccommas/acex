﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CE.AO.Portal.Startup))]
namespace CE.AO.Portal
{
    public partial class Startup
    {
        
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }

}
