﻿using CE.ControlPanel.Common;
using CE.ControlPanel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace CE.ControlPanel.Controllers
{
    public class ClientUserController : Controller
    {
        private InsightsEntities db = new InsightsEntities();

        // GET: /ClientUser/
        public ActionResult Index(int? clientId)
        {
            var users = new List<User>();
            if (clientId == null)
            {
                users = db.Users.Include(u => u.Client).OrderBy(o => o.ActorName).ToList();
            }
            else
            {
                users = db.Users.Include(u => u.Client).Where(c => c.ClientID == clientId).OrderBy(o => o.ActorName).ToList();
                ((UserSessionVariables)Session[Constants.USER_SESSION_VARIABLE]).ClientEditing = true;
            }
            ViewBag.clientId = clientId;
            return View(users);
        }

        // GET: /ClientUser/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: /ClientUser/Create
        public ActionResult Create(int? clientId)
        {
            var cu = new ClientUser();
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "Name", clientId);
            ViewData["roles"] = cu.GetRoles();
            ViewData["endpoints"] = cu.GetEndpoints();
            return View(cu);
        }

        // POST: /ClientUser/Create       
        [HttpPost]       
        public ActionResult Create(ClientUser user)
        {
            var cu = new ClientUser();
            if (ModelState.IsValid)
            {
                cu.CreateUser(user);
                if (((UserSessionVariables)Session[Constants.USER_SESSION_VARIABLE]).ClientEditing == true)
                {
                    return RedirectToAction("Index", new { clientId = user.ClientID });
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "Name", user.ClientID);
                ViewData["roles"] = cu.GetRoles();
                ViewData["endpoints"] = cu.GetEndpoints();
                return View(user);
            }
        }

        // GET: /ClientUser/Edit/5
        public ActionResult Edit(int id)
        {
            var cu = new ClientUser();
            var clientUser = cu.GetUser(id);
            if (clientUser == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "Name", clientUser.ClientID);
            ViewBag.ClientName = db.Clients.Where(c => c.ClientID == clientUser.ClientID).Select(i => new { Name = i.Name }).First().Name;
            ViewData["roles"] = cu.GetRoles(id);
            ViewData["endpoints"] = cu.GetEndpoints(id);
            return View(clientUser);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult Edit(ClientUser user)
        {
            if (ModelState.IsValid)
            {
                var cu = new ClientUser();
                cu.UpdateUser(user);
                if (((UserSessionVariables)Session[Constants.USER_SESSION_VARIABLE]).ClientEditing == true)
                {
                    return RedirectToAction("Index", new { clientId = user.ClientID });
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return View();
        }

        public ActionResult Environment(int id)
        {

            var env = new Environments();
            var environment = env.GetEnvironments(id);
            if (environment == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = id;
            var cu = new ClientUser();
            var clientUser = cu.GetUser(id);
            Client client = db.Clients.Find(clientUser.ClientID);
            ViewBag.ClientUser = "Client: " + client.Name + "  User: " + clientUser.ActorName;
            ViewBag.AuthType = client.AuthType;
            return View(environment);
        }

        [HttpPost]
        public JsonResult UpdateEnvironment(List<Environments> env, string userID)
        {
            var cu = new ClientUser();
            cu.UpdateEnvironment(env, Convert.ToInt32(userID));
            return Json("");
        }
    }
}
