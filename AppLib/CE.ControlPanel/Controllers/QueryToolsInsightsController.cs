﻿using CE.ControlPanel.Common;
using CE.ControlPanel.DAL;
using CE.ControlPanel.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CE.ControlPanel.Controllers
{
    public class QueryToolsInsightsController : Controller
    {

        private InsightsEntities insightDB = new InsightsEntities();
        private InsightsEntities insightDBUsers = new InsightsEntities();
        private InsightsEntities insightDBAuthType = new InsightsEntities();
        public string strBaseUrl = CE.ControlPanel.Common.Helpers.GetBaseUrl_CEInsightsAPI();
        QueryTool model = new QueryTool();
        //
        // GET: /QueryToolsInsights/
        public ActionResult Index(int clientId)
        {
            ViewData["ClientName"] = insightDB.Clients.Find(clientId).Name;
            using (var repo = new ControlPanelRepository()) { model.selectedAuthName = repo.GetAuthTypeDescription(insightDB.Clients.Find(clientId).AuthType); }

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Get", Value = "Get", Selected = true });
            items.Add(new SelectListItem { Text = "Post", Value = "Post" });
            items.Add(new SelectListItem { Text = "Put", Value = "Put" });
            model.MethodsApi = items;
            model.BaseUrl = strBaseUrl;

            var clientUsers = (from cu in insightDB.Users
                               where cu.ClientID == clientId && cu.EnableInd == true
                               select new { value = cu.UserID, text = cu.ActorName, accessKey = cu.CEAccessKeyID }).Take(20);

            var users = new List<UserDropDownList>();

            foreach (var c in clientUsers)
            {

                users.Add(new UserDropDownList { Text = c.text, Value = c.value.ToString(), AccessKeyId = c.accessKey });
            }

            users.Insert(0, new UserDropDownList() { Value = "-1", Text = "Select" });
            model.DropDownClientUser = users;

            #region API Parameters
            //suggested input for endpoints related text area fields - goes after the /api/v1/ with name , values
            model.Descriptionecho = "Say=1&Name=123&Number=1";
            model.Descriptionbilldisagg = "CustomerId={CustomerId}&StartDate={StartDate}&EndDate={EndDate}&IncludeContent=true&AccountId={AccountId}&PremiseId={PremiseId}&ApplianceKeys={Appliances}&EndUseKeys={EndUses}&CommodityKeys={Commodities}&MeasurementKeys={Measurements}";
            model.Descriptionbenchmark = "CustomerId={CustomerId}&StartDate={StartDate}&EndDate={EndDate}&IncludeContent=true&AccountId={AccountId}&PremiseId={PremiseId}&Count={Count}&GroupKeys={Groups}&MeasurementKeys={Measurements}";
            model.DescriptionEndpointTracking = "StartDate={StartDate}&EndDate={EndDate}&IncludeDetails=false&UserId={UserId}&Verb={Verb}&EndpointId={EndpointId}&MessageId={MessageId}&PageIndex=1";
            model.DescriptionContent = "Refresh=true";
            model.DescriptionProfile = "CustomerId={CustomerId}&AccountId={AccountId}&PremiseId={PremiseId}&ProfileAttributeKeys={ProfileAttributes}&Sources={Sources}&ApplianceKeys={Appliances}&EndUseKeys={EndUses}&StartDate={StartDate}&EndDate={EndDate}&IncludeMissing=false&IncludeContent=false&EntityLevel={EntityLevel}";
            model.DescriptionProfilePost = "";

            ViewData["DescriptionProfilePost"] = @"{ " +
                                                  "\"Customer\": {" +
                                                    "\"Id\": \"some-customerid\"," +
                                                    "\"Attributes\": [" +
                                                      "{" +
                                                        "\"AttributeKey\": \"greenprogram.enrollmentstatus\"," +
                                                        "\"AttributeValue\": \"greenprogram.enrollmentstatus.enrolled\"," +
                                                        "\"SourceKey\": \"csr\"" +
                                                      "}" +
                                                    "]," +
                                                     "\"Accounts\": [" +
                                                       "{" +
                                                         "\"Id\": \"some-accountid\"," +
                                                         "\"Attributes\": [" +
                                                           "{" +
                                                             "\"AttributeKey\": \"budgetbilling.enrollmentstatus\"," +
                                                             "\"AttributeValue\": \"budgetbilling.enrollmentstatus.enrolled\"," +
                                                             "\"SourceKey\": \"csr\"" +
                                                           "}," +
                                                           "{" +
                                                             "\"AttributeKey\": \"paperlessbilling.enrollmentstatus\"," +
                                                             "\"AttributeValue\": \"paperlessbilling.enrollmentstatus.enrolled\"," +
                                                             "\"SourceKey\": \"csr\"" +
                                                           "}" +
                                                         "]," +
                                                         "\"Premises\": [" +
                                                             "{" +
                                                               "\"Id\": \"some-premiseid\"," +
                                                               "\"Attributes\": [" +
                                                                 "{" +
                                                                 "\"AttributeKey\": \"pool.size\"," +
                                                                 "\"AttributeValue\": \"2000\"," +
                                                                 "\"SourceKey\": \"csr\"," +
                                                                 "}," +
                                                                 "{" +
                                                                 "\"AttributeKey\": \"waterheater.fuel\"," +
                                                                 "\"AttributeValue\": \"waterheater.fuel.gas\"," +
                                                                 "\"SourceKey\": \"csr\"" +
                                                                 "}" +
                                                                "]" +
                                                             "}" +
                                                         "]" +
                                                     "}" +
                                                    "]" +
                                                 "}," +
                                                 "\"Validate\": true" +
                                               "}";


            model.DescriptionAction = "CustomerId={CustomerId}&AccountId={AccountId}&PremiseId={PremiseId}&CommodityKeys={CommodityKeys}&Types={Types}&EndUseKeys={EndUseKeys}&ApplianceKeys={ApplianceKeys}&MeasurementKeys={MeasurementKeys}&WhatIfData={WhatIfData}&ProfileDefaultCollectionKey={ProfileDefaultCollectionKey}&IncludeContent=false";

            model.DescriptionActionPlan = "CustomerId={CustomerId}&AccountId={AccountId}&PremiseId={PremiseId}";

            model.DescriptionActionPlanPost = "";

            ViewData["DescriptionActionPlanPost"] = @"{ " +
                                                  "\"Customer\": {" +
                                                    "\"Id\": \"some-customerid\"," +
                                                    "\"Accounts\": [" +
                                                      "{" +
                                                        "\"Id\": \"some-accountid\"," +
                                                        "\"Premises\": [" +
                                                            "{" +
                                                              "\"Id\": \"some-premiseid\"," +
                                                              "\"ActionStatuses\": [" +
                                                                  "{" +
                                                                    "\"ActionKey\": \"takeshortershowers\"," +
                                                                    "\"Status\": \"completed\"," +
                                                                    "\"StatusDate\": \"2014-06-12 09:20:15.640\"" +
                                                                    "}," +
                                                                      "{" +
                                                                         "\"ActionKey\": \"replaceclothesdryer\"," +
                                                                         "\"Status\": \"selected\"," +
                                                                      "}" +
                                                                "]" +
                                                            "}" +
                                                        "]" +
                                                    "}" +
                                                   "]" +
                                                 "}," +
                                                 "\"Validate\": true" +
                                               "}";

            model.DescriptionActionPlanPut = "";

            ViewData["DescriptionActionPlanPut"] = @"{ " +
                                               "}";

            model.DescriptionBill = "CustomerId={CustomerId}&StartDate={StartDate}&EndDate={EndDate}&IncludeContent=true&AccountId={AccountId}&Count={Count}";

            model.DescriptionGreenButtonAmi = "CustomerId={CustomerId}&AccountId={AccountId}&PremiseId={PremiseId}&StartDate={StartDate}&EndDate={EndDate}&IntervalLength={IntervalLength}&CommodityKeys={CommodityKeys}&ReadingTypes={ReadingTypes}&IncludeContent=true";

            model.DescriptionConsumption = "CustomerId={CustomerId}&AccountId={AccountId}&PremiseId={PremiseId}&StartDate={StartDate}&EndDate={EndDate}&ServicePointId={ServicePointId}&ResolutionKey=day&PageIndex=1&IncludeContent=true";

            #endregion
            return View(model);
        }


        /// <summary>
        /// This gets the enabled endpoints for a selected clientuser by access key. Note that current environment would need to have that user
        /// access settings or endpoints will never be shown. No data would be brought back in this case. Dynamic dropdowns setup in the view.
        /// </summary>
        public JsonResult GetUserEndpoints(int id)
        {
            var cu = new ClientUser();
            return Json(cu.GetEndpoints(id), JsonRequestBehavior.AllowGet);
        }

        /// <summary>       
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult QueryRemoteAPIFormResponse(QueryTool queryformModel)
        {
            // setup rest client
            var client = new RestClient(strBaseUrl);
            queryformModel.BaseUrl = Helpers.GetBaseUrl_CEInsightsAPI();

            int userId = Convert.ToInt32(queryformModel.SelectedClientUserName);
            var clientUsers = (from cu in insightDB.Users
                               where cu.UserID == userId
                               select cu).First();
            var clientID = clientUsers.ClientID;
            string user = queryformModel.selectedCEAccessKeyID;

            var authType = (from c in insightDB.Clients
                            where c.ClientID == clientID
                            select c.AuthType).First();

            //method
            var method = Method.GET;
            switch (queryformModel.SelectedMethodApi.ToUpper())
            {
                case "GET":
                    method = Method.GET;
                    break;
                case "POST":
                    method = Method.POST;
                    break;
                case "PUT":
                    method = Method.PUT;
                    break;
                default:
                    method = Method.GET;
                    break;
            }

            //request
            RestRequest request = new RestRequest(method);
            //resource at endpoint
            if (!String.IsNullOrEmpty(queryformModel.selectedEndpoint))
            {
                request.Resource = queryformModel.selectedEndpoint;
            }

            var queryString = string.Empty;
            var encQueryString = string.Empty;

            //parameters
            //based on which endpoint, use the endpoint query entry model field
            switch (queryformModel.selectedEndpoint)
            {
                case "/api/v1/echo":
                    queryString = queryformModel.Descriptionecho;
                    break;
                case "/api/v1/billdisagg":
                    queryString = queryformModel.Descriptionbilldisagg;
                    break;
                case "/api/v1/benchmark":
                    queryString = queryformModel.Descriptionbenchmark;
                    break;
                case "/api/v1/endpointtracking":
                    queryString = queryformModel.DescriptionEndpointTracking;
                    break;
                case "/api/v1/profile":
                    queryString = queryformModel.DescriptionProfile;
                    break;
                case "/api/v1/content":
                    queryString = queryformModel.DescriptionContent;
                    break;
                case "/api/v1/action":
                    queryString = queryformModel.DescriptionAction;
                    break;
                case "/api/v1/actionplan":
                    queryString = queryformModel.DescriptionActionPlan;
                    break;
                case "/api/v1/bill":
                    queryString = queryformModel.DescriptionBill;
                    break;
                case "/api/v1/greenbuttonami":
                    queryString = queryformModel.DescriptionGreenButtonAmi;
                    break;
                case "/api/v1/consumption":
                    queryString = queryformModel.DescriptionConsumption;
                    break;
                default:
                    break;
            }

            if (queryString.Length > 0)
            {
                if (method == Method.GET)
                {
                    encQueryString = HttpUtility.UrlEncode(queryString);
                }
                else if (method == Method.POST || method == Method.PUT)
                {
                    encQueryString = queryString;
                }
            }
            //if encrypt is turned on (checked) then include the whole query 
            //otherwise parse through the query and add to parameters on the restclient request
            if (queryformModel.incEnc)
            {
                if (queryString.Length > 0)
                {
                    encQueryString = Helpers.EncryptQueryAPI(queryString, clientID.ToString(), user);
                    request.AddParameter("enc", encQueryString, ParameterType.GetOrPost);
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(queryString))
                {
                    //get after the ? on the first character
                    string querystringParsed = queryString.Substring(queryString.IndexOf('?') + 1);
                    //split into key values
                    {
                        string[] p = querystringParsed.Split('&');
                        foreach (string s in p)
                        {
                            if (s.IndexOf('=') > -1)
                            {
                                string[] temp = s.Split('=');
                                request.AddParameter(temp[0], temp[1], ParameterType.GetOrPost);
                            }
                        }
                    }
                }
            }

            //headers - three custom headers being used
            //signing
            var signingHandler = new HmacSigningHandler(new QueryToolSecretRepository(),
                                        new CanonicalRepresentationBuilder(),
                                        new HmacSignatureCalculator(),
                                        queryformModel);

            signingHandler.Username = user;


            signingHandler.MessageIdHeader = queryformModel.MessageIdHeader;
            signingHandler.ChannelHeader = queryformModel.ChannelHeader;
            signingHandler.LocaleHeader = queryformModel.LocaleHeader;
            signingHandler.MetaHeader = queryformModel.MetaHeader;
            signingHandler.Date = queryformModel.DateTime;
            //MD5
            if (queryString.Length > 0)
            {
                if (method == Method.GET)
                {
                    signingHandler.QueryString = encQueryString;
                }
                else if (method == Method.POST || method == Method.PUT)
                {
                    signingHandler.Body = encQueryString;
                }
            }

            var signature = signingHandler.Execute(request, client);

            //authentication
            if (authType == 0)
            {
                client.Authenticator = new ACLAuthenticator(user, signature, "CE");
            }
            else
            {
                var env = Helpers.GetCurrentEnvironment();
                var basicKey = (from ue in insightDB.UserEnvironments
                                where ue.UserID == userId && ue.EnvID == env
                                select ue.BasicKey).FirstOrDefault();

                var authHeader = string.Format("{0} {1}", "Basic", Base64Encode(user + ":" + basicKey));
                request.AddParameter("Authorization", authHeader, ParameterType.HttpHeader);
            }

            // execute response
            var response = client.Execute(request);
            var content = response.Content;

            return Json(JsonConvert.SerializeObject(response));
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                insightDB.Dispose();
                insightDBUsers.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}