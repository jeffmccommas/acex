﻿using CE.ControlPanel.Common;
using CE.ControlPanel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace CE.ControlPanel.Controllers
{
    public class ClientController : Controller
    {
        private InsightsEntities db = new InsightsEntities();

        // GET: /Client/
        public ActionResult Index()
        {
            var c = new Clients();
            var usv = ((UserSessionVariables)Session[Constants.USER_SESSION_VARIABLE]);
            return View(c.GetClients(usv.ControlPanelId));
        }

        // GET: /Client/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var usv = ((UserSessionVariables)Session[Constants.USER_SESSION_VARIABLE]);
            var c = new Clients();
            var client = c.GetClients(usv.ControlPanelId).Where(cl => cl.ClientID == id).ToList();

            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        // GET: /Client/Create
        public ActionResult Create()
        {
            ViewBag.AuthType = GetAuthTypeCreateList();
            return View();
        }

        // POST: /Client/Create  
        [HttpPost]        
        public ActionResult Create([Bind(Include = "ClientID,Name,Description,RateCompanyID,ReferrerID,NewDate,UpdDate,AuthType,EnableInd, ClientID")] Client client)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Clients.Add(client);
                    client.UpdDate = DateTime.Now;
                    client.NewDate = DateTime.Now;

                    var cpc = new ControlPanelLoginClient();
                    cpc.CpID = ((UserSessionVariables)Session[Constants.USER_SESSION_VARIABLE]).ControlPanelId;
                    cpc.ClientID = client.ClientID;
                    db.ControlPanelLoginClients.Add(cpc);

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    using (var ent = new InsightsEntities())
                    {
                        var clientList = (from c in ent.Clients
                                          where c.ClientID == client.ClientID
                                          select c.ClientID).ToList();
                        if (clientList.Count > 0)
                        {
                            return Json(new { Success = false, Message = "Duplicate client id." });
                        }
                    }
                }
            }
            else
            {
                ViewBag.AuthType = GetAuthTypeCreateList();
                return View(client);
            }
            return View(client);
        }

        // GET: /Client/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = db.Clients.Find(id);

            ViewBag.AuthType = GetAuthTypeList(client.AuthType);

            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        // POST: /Client/Edit/5  
        [HttpPost]       
        public ActionResult Edit([Bind(Include = "Name,Description,RateCompanyID,ReferrerID,NewDate,AuthType,EnableInd, ClientID")] Client client)
        {
            if (ModelState.IsValid)
            {
                db.Entry(client).State = EntityState.Modified;
                client.UpdDate = DateTime.Now;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(client);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult CheckClientID(string clientID)
        {
            try
            {
                if (clientID.Length == 0)
                {
                    return Json(new { Success = false, Message = "Please enter the required inputs." });
                }
                int id = Convert.ToInt32(clientID);
                using (var ent = new InsightsEntities())
                {
                    var clientList = (from c in ent.Clients
                                      where c.ClientID == id
                                      select c.ClientID).ToList();
                    if (clientList.Count > 0)
                    {
                        return Json(new { Success = false, Message = "Duplicate client id." });
                    }
                }
            }
            catch
            {
                return Json(new { Success = false, Message = "Please enter Client ID as integer value." });
            }

            return Json(new { Success = true });
        }

        private List<SelectListItem> GetAuthTypeList(byte authType)
        {
            using (var ent = new InsightsEntities())
            {
                var authList = (from at in ent.AuthTypes
                                select at).ToList();
                List<SelectListItem> list = new List<SelectListItem>();
                foreach (var a in authList)
                {
                    if (authType == a.AuthTypeID)
                    {
                        list.Add(new SelectListItem { Text = a.AuthName, Value = a.AuthTypeID.ToString(), Selected = true });
                    }
                    else
                    {
                        list.Add(new SelectListItem { Text = a.AuthName, Value = a.AuthTypeID.ToString() });
                    }
                }
                return list;
            }
        }

        private List<SelectListItem> GetAuthTypeCreateList()
        {
            using (var ent = new InsightsEntities())
            {
                var authList = (from at in ent.AuthTypes
                                select at).ToList();
                List<SelectListItem> list = new List<SelectListItem>();
                var cnt = 0;
                foreach (var a in authList)
                {
                    if (cnt == 0)
                    {
                        list.Add(new SelectListItem { Text = a.AuthName, Value = a.AuthTypeID.ToString(), Selected = true });
                    }
                    else
                    {
                        list.Add(new SelectListItem { Text = a.AuthName, Value = a.AuthTypeID.ToString() });
                    }
                    cnt += 1;
                }
                return list;
            }
        }
    }
}
