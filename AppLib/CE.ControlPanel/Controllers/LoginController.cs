﻿using CE.ControlPanel.Common;
using CE.ControlPanel.Models;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace CE.ControlPanel.Controllers
{
    [OverrideAuthentication]
    public class LoginController : Controller
    {
        private const string AppSetting_CEEnvironment = "CEEnvironment";
        //
        // GET: /Login/
        //Control Panel Login and LoginClient Model can be used for html view
        public ActionResult Index()
        {
            var usv = new UserSessionVariables();
            if (ConfigurationManager.AppSettings.Get(AppSetting_CEEnvironment) == "dev" || ConfigurationManager.AppSettings.Get(AppSetting_CEEnvironment) == "localdev")
            {
                usv.EnableEditing = true;
            }
            else
            {
                usv.EnableEditing = false;
            }
            Session[Constants.USER_SESSION_VARIABLE] = usv;
            return View();
        }

        // POST: /Login and bring to Home
        [HttpPost]
        public JsonResult LogIn(string username, string password)
        {
            try
            {
                if (username.Length == 0 || password.Length == 0)
                {
                    return Json("Please enter valid username and password.", JsonRequestBehavior.AllowGet);
                }
                var usv = ((UserSessionVariables)Session[Constants.USER_SESSION_VARIABLE]);
                usv.UserName = username;
                usv.Password = password;

                Session[Constants.USER_SESSION_VARIABLE] = usv;

                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("False", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Change()
        {

            return View();
        }

        [HttpPost]
        public JsonResult ChangePassword(string username, string password, string newpassword, string confnewpassword)
        {
            try
            {
                if (newpassword != confnewpassword)
                {
                    return Json("Password don't match.", JsonRequestBehavior.AllowGet);
                }

                using (var ent = new InsightsEntities())
                {
                    var encryptor = new Encryptor(password);

                    var client = (from ap in ent.ControlPanelLogins
                                  where ap.UserName == username
                                  select ap).FirstOrDefault();
                    if (client == null)
                    {
                        return Json("Bad username or password.", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {

                        if (encryptor.VerifyHash(password, client.PasswordSalt, client.PasswordHash))
                        {

                            var pass = encryptor.GenerateHashWithSalt(newpassword, client.PasswordSalt);
                            client.PasswordHash = pass;
                            client.UpdDate = DateTime.Now;
                            ent.SaveChanges();
                        }
                        else
                        {
                            return Json("Bad username or password.", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("False", JsonRequestBehavior.AllowGet);
            }
        }

        // GET: 
        public ActionResult Create()
        {
            return View();
        }

        // POST: 
        [HttpPost]
        public ActionResult Create([Bind(Include = "FirstName,LastName ,UserName,Password,EnableInd,SuperUserInd")] Logins login)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var ent = new InsightsEntities())
                    {
                        var lList = (from c in ent.ControlPanelLogins
                                     where c.UserName == login.UserName
                                     select c.FirstName).ToList();
                        if (lList.Count > 0)
                        {
                            return Json(new { Success = false, Message = "Duplicate user name." });
                        }

                        var cpl = new ControlPanelLogin();
                        cpl.FirstName = login.FirstName;
                        cpl.LastName = login.LastName;
                        cpl.UserName = login.UserName;
                        cpl.EnableInd = login.EnableInd;

                        var encryptor = new Encryptor("xv");
                        var bytes = new byte[16];
                        using (var rng = new System.Security.Cryptography.RNGCryptoServiceProvider())
                        {
                            rng.GetBytes(bytes);
                        }

                        // and if you need it as a string...
                        string hash = BitConverter.ToString(bytes).Replace("-", "");

                        var pass = encryptor.GenerateHashWithSalt(login.Password, hash);
                        cpl.PasswordHash = pass;
                        cpl.PasswordSalt = hash;

                        cpl.UpdDate = DateTime.Now;
                        cpl.NewDate = DateTime.Now;

                        ent.ControlPanelLogins.Add(cpl);

                        ent.SaveChanges();
                        return RedirectToAction("List");
                    }
                }
                catch
                {

                }
            }
            else
            {

                return View(login);
            }
            return View(login);
        }

        // GET: 
        public ActionResult List()
        {
            var c = new Logins();
            var login = c.GetLogins().ToList();

            if (login == null)
            {
                return HttpNotFound();
            }
            return View(login);
        }

        // GET: 
        public ActionResult Edit(string username)
        {
            var cu = new LoginClients();
            var login = cu.GetLogin(username);
            if (login == null)
            {
                return HttpNotFound();
            }
            ViewData["LoginClientsList"] = cu.GetClients(username);
            return View(login);
        }

        [HttpPost]
        public ActionResult Edit(LoginClients login)
        {
            if (ModelState.IsValid)
            {
                var cu = new LoginClients();
                cu.UpdateLogin(login);
                return RedirectToAction("List");
            }
            return View();
        }
    }
}
