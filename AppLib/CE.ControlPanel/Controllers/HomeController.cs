﻿using System.Web.Mvc;

namespace CE.ControlPanel.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "CE Control Panel.";

            return View();
        }

        /// <summary>       
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Logout()
        {
            Session.Abandon();
            //return RedirectToAction("Index", "login");
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}