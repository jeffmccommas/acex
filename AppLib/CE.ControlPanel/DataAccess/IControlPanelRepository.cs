﻿using CE.ControlPanel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;

namespace CE.ControlPanel.DAL
{
    interface IControlPanelRepository :IDisposable

    {
        ObjectResult<GetClientUserByAccessKey_Result> GetClientUserbyAccessKey(string accessKey, byte currentEnvironment);
        List<string> GetEnabledEndpointsbyUserid(int userid);

        string GetPasswordSalt(string username);

        string GetAuthTypeDescription(byte authtypeid);
    }

}
