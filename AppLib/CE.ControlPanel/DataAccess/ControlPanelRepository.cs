﻿using CE.ControlPanel.Models;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace CE.ControlPanel.DAL
{
    public class ControlPanelRepository : IControlPanelRepository
    {
        InsightsEntities context = new InsightsEntities();
        public ObjectResult<GetClientUserByAccessKey_Result> GetClientUserbyAccessKey(string accessKey, byte currentEnvironment)
        {
             var val = context.GetClientUserByAccessKey(accessKey, currentEnvironment);
             return val;
        }
       
        public List<string> GetEnabledEndpointsbyUserid(int userid)
        {  List<string> endpointnames =  context.Database.SqlQuery<string>
               ("SELECT [Endpoint].Name FROM [UserEndpoint] INNER JOIN [Endpoint] ON [UserEndpoint].EndPointID = [Endpoint].EndpointId WHERE UserID = {0} AND [UserEndpoint].EnableInd = 1",userid)
                   .ToList();
            //add default 'Select' to the list
           endpointnames.Insert(0,"Select");
           return endpointnames;
        }

        public string GetSecretForUser(string ceaccesskey, string environment)
        {
            string CESecretKey = context.Database.SqlQuery<string>
           ("SELECT  TOP (1) [UserEnvironment].CESecretAccessKey FROM [User] INNER JOIN Client ON [User].ClientID = Client.ClientID INNER JOIN UserEnvironment ON [User].UserID = UserEnvironment.UserID WHERE ([User].EnableInd = 1 AND Client.EnableInd = 1 AND [User].CEAccessKeyID = {0} ) AND (UserEnvironment.EnvID = 0 OR UserEnvironment.EnvID = {1}) ORDER BY UserEnvironment.EnvID DESC", ceaccesskey, environment).SingleOrDefault();
            return CESecretKey;
        }

        //gets salt from control panel enabled login user's username (username is unique key at database level)
        public string GetPasswordSalt(string username)
        {
            string passwordSalt = context.Database.SqlQuery<string>
                ("SELECT [ControlPanelLogin].PasswordSalt FROM [ControlPanelLogin] INNER JOIN [ControlPanelLoginClient] ON [ControlPanelLogin].CpID = [ControlPanelLoginClient].CpID WHERE UserName = {0} AND [ControlPanelLogin].EnableInd = 1", username).SingleOrDefault();

            return passwordSalt;
        }

        public string GetAuthTypeDescription(byte authtypeid)
        {
            string authtypeDescription = context.Database.SqlQuery<string>
            ("SELECT AuthDescription FROM [AuthType] WHERE authtypeid = {0}", authtypeid).SingleOrDefault();

            return authtypeDescription;
        }

        public void Dispose()
        {
            context.Dispose();
        }

    }
}