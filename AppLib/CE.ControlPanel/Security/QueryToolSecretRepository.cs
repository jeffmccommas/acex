﻿using CE.ControlPanel.DAL;
using CE.ControlPanel.Models;
using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace CE.ControlPanel.Common
{
    public class QueryToolSecretRepository : ISecretRepository
    {      
        private EnvironmentType _environment = EnvironmentType.prod;    // default = 0
        private const string AppSetting_CEEnvironment = "CEEnvironment";

        public string GetSecretForUser(string username)
        {
            string s = ConfigurationManager.AppSettings.Get(AppSetting_CEEnvironment);
            if (!string.IsNullOrEmpty(s))
            {
                _environment = (EnvironmentType)System.Enum.Parse(typeof(EnvironmentType), s, true);
            }

            var intEnv = (int)_environment;
            var dal = new ControlPanelRepository();
            string secret = dal.GetSecretForUser(username, intEnv.ToString());
            if (secret == null)
            {
                return "";
            }
            return secret;
        }

        private string ComputeHash(string inputData, HashAlgorithm algorithm)
        {
            byte[] inputBytes = Encoding.UTF8.GetBytes(inputData);
            byte[] hashed = algorithm.ComputeHash(inputBytes);
            return Convert.ToBase64String(hashed);
        }
    }
}