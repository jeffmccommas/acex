﻿using CE.ControlPanel.Models;
using RestSharp;
using System;

namespace CE.ControlPanel.Common
{
    public class HmacSigningHandler
    {
        private readonly ISecretRepository _secretRepository;
        private readonly IBuildMessageRepresentation _representationBuilder;
        private readonly ICalculteSignature _signatureCalculator;

        public string Username { get; set; }
        public string environment { get; set; }
        public string QueryString { get; set; }
        public string MessageIdHeader { get; set; }
        public string ChannelHeader { get; set; }
        public string LocaleHeader { get; set; }
        public string MetaHeader { get; set; }
        public string Body { get; set; }
        public DateTime Date { get; set; }


        public HmacSigningHandler(ISecretRepository secretRepository,
                              IBuildMessageRepresentation representationBuilder,
                              ICalculteSignature signatureCalculator,
                               QueryToolForm queryformModel)
        {
            _secretRepository = secretRepository;
            _representationBuilder = representationBuilder;
            _signatureCalculator = signatureCalculator;
        }

        public string Execute(RestRequest request, RestClient client)
        {
             request.AddParameter(Helpers.UsernameHeader, Username, ParameterType.HttpHeader);

            if (Date != null)
            {
                request.AddParameter(Helpers.DateTime, Date.ToString("MM/dd/yyyy HH:mm:ss"), ParameterType.HttpHeader);
            }
            if (MessageIdHeader != null)
            {
                request.AddParameter(Helpers.MessageIdHeader, MessageIdHeader, ParameterType.HttpHeader);
            }
            if (ChannelHeader != null)
            {
                request.AddParameter(Helpers.ChannelHeader, ChannelHeader, ParameterType.HttpHeader);
            }
            if (LocaleHeader != null)
            {
                request.AddParameter(Helpers.LocaleHeader, LocaleHeader, ParameterType.HttpHeader);
            }          
           
            //always append hashtag #cecp:1# if coming from control panel, whether metaheader on form was entered or not, as costs would exclude this type of request
            request.AddParameter(Helpers.MetaHeader, (MetaHeader + " #cecp:1#").Trim(), ParameterType.HttpHeader);
            var representation = string.Empty;
            if (QueryString == null)
            {
                request.AddParameter("application/json; charset=utf-8", Body, ParameterType.RequestBody);
                request.RequestFormat = DataFormat.Json;
                representation = _representationBuilder.BuildRequestRepresentation(request, client, Body);
            }
            else {
                representation = _representationBuilder.BuildRequestRepresentation(request, client, QueryString);
            }
                       
            var secret = _secretRepository.GetSecretForUser(Username);
            string signature = _signatureCalculator.Signature(secret, representation);

            return (signature);
        }
    }
}