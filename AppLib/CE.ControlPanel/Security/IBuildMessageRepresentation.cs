﻿using RestSharp;

namespace CE.ControlPanel.Common
{
    public interface IBuildMessageRepresentation
    {
        string BuildRequestRepresentation(RestRequest requestMessage, RestClient client, string querystring);
    }
}