﻿using RestSharp;
using System;
using System.Linq;

namespace CE.ControlPanel.Common
{
    public class ACLAuthenticator : IAuthenticator
    {
        private readonly string _username;
        private readonly string _signature;
        private readonly string _authScheme;

        public ACLAuthenticator(string aclAccessKeyId, string signature, string authenticationScheme)
        {
            _username = aclAccessKeyId;
            _signature = signature;
            _authScheme = authenticationScheme;
        }

        public void Authenticate(IRestClient client, IRestRequest request)
        {
            // NetworkCredentials always makes two trips, even if with PreAuthenticate,
            // it is also unsafe for many partial trust scenarios
            // request.Credentials = Credentials;
            // thanks TweetSharp!

            // request.Credentials = new NetworkCredential(_username, _password);

            // only add the Authorization parameter if it hasn't been added by a previous Execute
            if (!request.Parameters.Any(p => p.Name.Equals("Authorization", StringComparison.OrdinalIgnoreCase)))
            {
                var token = string.Format("{0}:{1}", _username, _signature);
                var authHeader = string.Format("{0} {1}", _authScheme, token);
                request.AddParameter("Authorization", authHeader, ParameterType.HttpHeader);
            }
        }
    }
}
