﻿using RestSharp;
using System;
using System.Globalization;

namespace CE.ControlPanel.Common
{
    public class CanonicalRepresentationBuilder : IBuildMessageRepresentation
    {
        /// <summary>
        /// Builds message representation as follows:
        /// verb\n +
        /// querytring\n +  
        /// Timestamp\n +
        /// Username\n +
        /// RequestURI
        /// </summary>
        /// <returns></returns>
        public string BuildRequestRepresentation(RestRequest requestMessage, RestClient client, string queryString)
        {
            var dateString = Convert.ToString(requestMessage.Parameters.Find(p => p.Name == Helpers.DateTime).Value, CultureInfo.InvariantCulture);

            string httpMethod = requestMessage.Method.ToString();

            //string contentType = requestMessage.Content.Headers.ContentType.MediaType;
            if (!requestMessage.Parameters.Exists(p => p.Name == Helpers.UsernameHeader))
            {
                return null;
            }

            string username = Convert.ToString(requestMessage.Parameters.Find(p => p.Name == Helpers.UsernameHeader).Value);

            string uri = requestMessage.Resource;

            // you may need to add more headers if thats required for security reasons
            string representation = String.Join("\n", httpMethod,
                queryString, dateString, username, uri);

            return representation;
        }

    }
}