﻿namespace CE.ControlPanel.Common
{
    //some differences from infrastructure
    public interface ICalculteSignature
    {
        string Signature(string secret, string value);
    }
}