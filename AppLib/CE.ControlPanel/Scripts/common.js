﻿function createUUID() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    //s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;
}

function UTCDateTime() {
    var dNow = new Date();
    var utc = new Date(dNow.getTime() + dNow.getTimezoneOffset() * 60000)
    var utcdate = ("0" + (utc.getMonth() + 1)).slice(-2) + '/' + ("0" + utc.getDate()).slice(-2) + '/' + utc.getFullYear() + ' ' + ("0" + utc.getHours()).slice(-2) + ':' + ("0" + utc.getMinutes()).slice(-2) + ':' + ("0" + utc.getSeconds()).slice(-2);
    return utcdate;
}

function ResolveURL(relative) {
    var resolved = relative;
    if (relative.charAt(0) == '~')
        resolved = relativeRoot + relative.substring(2);
    return resolved;
}

$(document).ready(function () {
    $("#btnLogout").click(function () {
        $.ajax({
            url: ResolveURL("~/Home/Logout"),
            async: false,
            type: "POST",
            timeout: 5000,
            data: "",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {              

                var url = $('#lnkLogin').attr('href');
                window.location = url;                
            },
            beforeSend: function () { },
            error: function (x, t, m) {                
            }
        });
    });
});