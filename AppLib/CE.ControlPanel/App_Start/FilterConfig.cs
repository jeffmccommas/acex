﻿using CE.ControlPanel.Filters;
using System.Web.Mvc;

namespace CE.ControlPanel
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomAuthenticationAttribute());
        }
    }
}
