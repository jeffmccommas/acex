﻿using CE.ControlPanel.Common;
using CE.ControlPanel.Models;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;

namespace CE.ControlPanel.Filters
{
    public class CustomAuthenticationAttribute : ActionFilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            var usv = (UserSessionVariables)HttpContext.Current.Session[Constants.USER_SESSION_VARIABLE];
            //Check for valid username and password
            if (usv == null)
            {
                filterContext.Result = RedirectToLogin();
            }
            else if (usv.IsAuthenticated == false)
            {
                if (usv.UserName == null || usv.Password == null)
                {
                    filterContext.Result = RedirectToLogin();
                }
                else
                {
                    using (var ent = new InsightsEntities())
                    {
                        var encryptor = new Encryptor(usv.Password);

                        var client = (from ap in ent.ControlPanelLogins
                                      where ap.UserName == usv.UserName && ap.EnableInd == true
                                      select new { ap.PasswordHash, ap.PasswordSalt, ap.CpID, ap.SuperUserInd }).ToList();
                        if (client.Count == 0)
                        {
                            filterContext.Result = RedirectToLogin();
                        }
                        else
                        {
                            foreach (var c in client)
                            {
                                if (encryptor.VerifyHash(usv.Password, c.PasswordSalt, c.PasswordHash))
                                {
                                    usv.IsAuthenticated = true;
                                    usv.ControlPanelId = c.CpID;
                                    usv.SuperUser = c.SuperUserInd;
                                }
                                else
                                {
                                    filterContext.Result = RedirectToLogin();
                                }
                            }
                        }
                    }
                }
            }
        }
        //Runs after the OnAuthentication method
        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
        }

        private ActionResult RedirectToLogin()
        {
            return new RedirectToRouteResult(new RouteValueDictionary { { "Controller", "login" }, 
                                                                                                { "Action", "Index" }, 
                                                                                                { "Area", "" },
                                                                                                { "RedirectFromFilter", "Yes" }});
        }
    }
}