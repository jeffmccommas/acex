﻿using System;

namespace CE.ControlPanel.Common
{
    [Serializable()]
    public class UserSessionVariables
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int ControlPanelId { get; set; }
        public bool IsAuthenticated { get; set; }
        public bool ClientEditing { get; set; }
        public bool EnableEditing { get; set; }
        public bool SuperUser { get; set; }
    }
}





