﻿using CE.ControlPanel.Models;
using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;


namespace CE.ControlPanel.Common
{
    public static class Helpers
    {
        private static EnvironmentType _environment = EnvironmentType.prod;    // default = 0
        private const string AppSetting_CEEnvironment = "CEEnvironment";
        private const string BaseURL_CEInsightsLocalDev = "CEInsightsAPIBaseURL";

        //list of headers here with their respective checkboxes that if selected will be included in the request
        public const string DateTime = "X-DateTime";
        // this will always be included for now on rest client api request- on 87 CE authentication
        public const string UsernameHeader = "X-CE-AccessKeyId";
        public const string MessageIdHeader = "X-CE-MessageId";
        public const string ChannelHeader = "X-CE-Channel";
        public const string LocaleHeader = "X-CE-Locale";
        public const string MetaHeader = "X-CE-Meta";  ////on end also include "#cecp:1#" to simulate control panel caller so record would be excluded in reported results


        public static byte GetCurrentEnvironment()
        {
            string s = ConfigurationManager.AppSettings.Get(AppSetting_CEEnvironment);

            if (!string.IsNullOrEmpty(s))
            {
                _environment  = (EnvironmentType)System.Enum.Parse(typeof(EnvironmentType), s, true);
            }
            return (byte)_environment;
        }

        public static string GetBaseUrl_CEInsightsAPI()
        {
            string baseUrl = ConfigurationManager.AppSettings.Get(BaseURL_CEInsightsLocalDev);
            return baseUrl;
        }

        public static string EncryptQueryAPI(string plainText, string clientId, string user)
        {
            string key = "";
            ISecretRepository _secret = new QueryToolSecretRepository();
            key = _secret.GetSecretForUser(user);                       

            byte[] EncryptKey = { };
            byte[] IV = { 55, 34, 87, 64, 87, 195, 54, 21 };
            EncryptKey = System.Text.Encoding.UTF8.GetBytes(key.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByte = Encoding.UTF8.GetBytes(plainText);
            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, des.CreateEncryptor(EncryptKey, IV), CryptoStreamMode.Write);
            cStream.Write(inputByte, 0, inputByte.Length);
            cStream.FlushFinalBlock();
            return HttpUtility.UrlEncode(Convert.ToBase64String(mStream.ToArray()));
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

    }
}