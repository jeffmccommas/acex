﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CE.ControlPanel.Models
{
    public class LoginClients
    {
        [Required(ErrorMessage = "User Name is Required")]
        public string UserName { get; set; }

        public bool EnableInd { get; set; }

        // Integer array with name matching that of the checkboxes it's associated with       
        public int[] LoginClientsList { get; set; }

        public LoginClients GetLogin(string username)
        {
            var c = new LoginClients();
            using (var ent = new InsightsEntities())
            {
                var login = (from ue in ent.ControlPanelLogins
                             where ue.UserName == username
                             select ue).FirstOrDefault();

                c.UserName = login.UserName;
                c.EnableInd = login.EnableInd;
            }
            return c;
        }

        public List<ClientsChkBox> GetClients(string username)
        {
            var clients = new List<ClientsChkBox>();
            using (var ent = new InsightsEntities())
            {
                var clientList = (from e in ent.Clients
                                  select e).ToList();

                var loginClients = (from ue in ent.ControlPanelLoginClients
                                    join ur in ent.ControlPanelLogins on ue.CpID equals ur.CpID
                                    where ur.UserName == username
                                    select ue).ToList();

                foreach (var e in clientList)
                {
                    var c = new ClientsChkBox();
                    c.Id = e.ClientID;
                    c.Name = e.Name;
                    foreach (var ur in loginClients)
                    {
                        if (c.Id == ur.ClientID)
                        {
                            c.Enabled = true;
                        }
                    }
                    clients.Add(c);
                }
            }
            return clients;
        }

        public void UpdateLogin(LoginClients user)
        {
            using (var ent = new InsightsEntities())
            {
                List<ControlPanelLogin> login = (from u in ent.ControlPanelLogins
                                                 where u.UserName == user.UserName
                                                 select u).ToList();

                if (login.Count != 0)
                {
                    ControlPanelLogin u = login.ElementAt(0);
                    u.EnableInd = user.EnableInd;
                    u.UpdDate = DateTime.Now;
                }

                var clients = (from e in ent.Clients
                               select e).ToList();


                var loginClients = (from u in ent.ControlPanelLoginClients
                                    join ur in ent.ControlPanelLogins on u.CpID equals ur.CpID
                                    where ur.UserName == user.UserName
                                    select u).ToList();

                foreach (var e in clients)
                {
                    var found = false;
                    var createNew = true;
                    foreach (var ue in loginClients)
                    {
                        if (e.ClientID == ue.ClientID)
                        {
                            if (user.LoginClientsList != null)
                            {
                                foreach (var ie in user.LoginClientsList)
                                {
                                    if (e.ClientID == ie)
                                    {
                                        found = true;
                                        createNew = false;
                                    }
                                }
                            }
                            if (!found)
                            {
                                var cpl = (from u in ent.ControlPanelLoginClients
                                           where u.CpID == ue.CpID && u.ClientID == ue.ClientID
                                           select u).FirstOrDefault();
                                ent.ControlPanelLoginClients.Remove(cpl);
                                createNew = false;
                            }
                        }

                    }
                    var cpId = login.ElementAt(0).CpID;
                    if (createNew)
                    {
                        if (ent.ControlPanelLoginClients.Where(s => s.CpID == cpId && s.ClientID == e.ClientID).ToList().Count == 0)
                        {
                            if (user.LoginClientsList != null)
                            {
                                foreach (var ie in user.LoginClientsList)
                                {
                                    if (e.ClientID == ie)
                                    {
                                        var cpl = new ControlPanelLoginClient();
                                        cpl.CpID = login.ElementAt(0).CpID;
                                        cpl.ClientID = e.ClientID;
                                        ent.ControlPanelLoginClients.Add(cpl);
                                    }
                                }
                            }
                        }
                    }
                }

                ent.SaveChanges();
            }
        }
    }

    public class ClientsChkBox
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Enabled { get; set; }
    }
}