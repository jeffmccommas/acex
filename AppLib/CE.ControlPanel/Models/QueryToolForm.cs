﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace CE.ControlPanel.Models
{
    public partial class QueryToolForm
    {
        /// <summary>
        /// Access key selected on query form - based on the dropdown client user name selected
        /// </summary>
        public string selectedCEAccessKeyID { get; set; }

        public string selectedAuthName { get; set; }

        public int UserId { get; set; }

        public string BaseUrl { get; set; }

        public string SelectedClientUserName { get; set; }


        public string selectedEndpoint { get; set; }

        /// <summary>
        /// below are holding places as description textarea for parameters for a selected endpoint 
        /// which are shown or hidden according to which endpoint is selected. Could go to an array for future with 
        /// meta data of typical parameters that are being used per endpointid
        /// </summary>
        /// 
        public string Descriptionecho { get; set; }
        public string Descriptionbilldisagg { get; set; }
        public string Descriptionbenchmark { get; set; }
        public string DescriptionEndpointTracking { get; set; }
        public string DescriptionProfile { get; set; }
        public string DescriptionContent { get; set; }
        public string DescriptionProfilePost { get; set; }
        public string DescriptionAction { get; set; }
        public string DescriptionActionPlan { get; set; }
        public string DescriptionActionPlanPost { get; set; }
        public string DescriptionActionPlanPut { get; set; }
        public string DescriptionBill { get; set; }
        public string DescriptionGreenButtonAmi { get; set; }
        public string DescriptionConsumption { get; set; }

        public IEnumerable<SelectListItem> DropDownEndpoints { get; set; }

        public string SelectedMethodApi { get; set; }
        public IEnumerable<SelectListItem> MethodsApi { get; set; }


        //encrypt the form fields if checked when sending to remote api request
        public bool incEnc { get; set; }

        public string XDateTimeUTCHeader { get; set; }
        public string MetaHeaderEntry { get; set; }
        public bool MetaHeaderEntryInc { get; set; }

        public string ResponseQueryAPI { get; set; }

    }
}