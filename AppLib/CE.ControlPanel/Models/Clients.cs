﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CE.ControlPanel.Models
{
    public class Clients
    {
        [Required(ErrorMessage = "ClientID is Required")]
        [Range(1, int.MaxValue, ErrorMessage = "ClientID must be a number")]
        public int ClientID { get; set; }

        [Required(ErrorMessage = "Actor Name is Required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Description is Required")]
        public string Description { get; set; }
        public int? RateCompanyID { get; set; }
        public int? ReferrerID { get; set; }
        public DateTime NewDate { get; set; }
        public DateTime UpdDate { get; set; }
        public bool EnableInd { get; set; }
        public string AuthName { get; set; }

        public List<Clients> GetClients(int ControlPanelId)
        {
            using (var ent = new InsightsEntities())
            {
                var clientList = (from c in ent.Clients
                                  join a in ent.AuthTypes on c.AuthType equals a.AuthTypeID
                                  join cplc in ent.ControlPanelLoginClients on c.ClientID equals cplc.ClientID
                                  where cplc.CpID == ControlPanelId
                                  select new { c.ClientID, c.Name, c.Description, c.RateCompanyID, c.ReferrerID, c.NewDate, c.UpdDate, c.EnableInd, a.AuthName }).ToList();

                var cl = new List<Clients>();
                foreach (var c in clientList)
                {
                    var client = new Clients();
                    client.ClientID = c.ClientID;
                    client.Name = c.Name;
                    client.Description = c.Description;
                    client.RateCompanyID = c.RateCompanyID;
                    client.ReferrerID = c.ReferrerID;
                    client.NewDate = c.NewDate;
                    client.UpdDate = c.UpdDate;
                    client.EnableInd = c.EnableInd;
                    client.AuthName = c.AuthName;
                    cl.Add(client);
                }
                return cl;
            }
        }
    }
}