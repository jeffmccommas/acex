﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CE.ControlPanel.Models
{
    public class Logins
    {
        [Required(ErrorMessage = "First Name is Required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name is Required")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "User Name is Required")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password is Required")]
        public string Password { get; set; }
        public bool EnableInd { get; set; }
        public bool SuperUserInd { get; set; }

        public List<Logins> GetLogins()
        {
            using (var ent = new InsightsEntities())
            {
                var loginList = (from c in ent.ControlPanelLogins
                                 select new { c.FirstName, c.LastName, c.UserName, c.SuperUserInd, c.NewDate, c.UpdDate, c.EnableInd, }).ToList();

                var ll = new List<Logins>();
                foreach (var c in loginList)
                {
                    var login = new Logins();

                    login.FirstName = c.FirstName;
                    login.LastName = c.LastName;
                    login.UserName = c.UserName;                    
                    login.EnableInd = c.EnableInd;
                    login.SuperUserInd = c.SuperUserInd;
                    ll.Add(login);
                }
                return ll;
            }
        }
    }
}