﻿using System;
using System.Collections.Generic;

namespace CE.ControlPanel.Models
{
    public class QueryTool : QueryToolForm
    {
        public string UsernameHeader { get; set; }
        public string MessageIdHeader { get; set; }
        public string ChannelHeader { get; set; }
        public string LocaleHeader { get; set; }
        public string MetaHeader { get; set; }
        public DateTime DateTime { get; set; }
        public List<UserDropDownList> DropDownClientUser { get; set; }
    }

    public class UserDropDownList
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public string AccessKeyId { get; set; }
    }
}