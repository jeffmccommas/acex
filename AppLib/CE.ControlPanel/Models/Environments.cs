﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CE.ControlPanel.Models
{
    public class Environments
    {
        public int EnvID { get; set; }
        public string EnvDescription { get; set; }
        public int UserID { get; set; }
        public string CEAccessKey { get; set; }
        public string BasicAccessKey { get; set; }


        public List<Environments> GetEnvironments(int userID)
        {
            using (var ent = new InsightsEntities())
            {
                var envList = (from e in ent.Environments
                               join ue in ent.UserEnvironments.Where(u => u.UserID == userID) on e.EnvID equals ue.EnvID
                               into gj
                               from x in gj.DefaultIfEmpty()

                               select new
                               {
                                   e.Description,
                                   e.EnvID,
                                   UserID = (x == null ? -1 : x.UserID),
                                   AccessKey = (x == null ? String.Empty : x.CESecretAccessKey),
                                   BasicKey = (x == null ? String.Empty : x.BasicKey)
                               }).ToList();

                var ev = new List<Environments>();
                foreach (var e in envList)
                {
                    var env = new Environments();
                    env.EnvID = e.EnvID;
                    env.UserID = e.UserID;
                    env.EnvDescription = e.Description;
                    env.CEAccessKey = e.AccessKey;
                    env.BasicAccessKey = e.BasicKey;

                    ev.Add(env);
                }
                return ev;
            }
        }
    }
}