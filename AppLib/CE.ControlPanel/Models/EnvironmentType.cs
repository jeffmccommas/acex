﻿
namespace CE.ControlPanel.Models
{
    public enum EnvironmentType
    {
        prod,
        uat,
        qa,
        dev,
        localdev
    }
}