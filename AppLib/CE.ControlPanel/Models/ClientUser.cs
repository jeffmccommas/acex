﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CE.ControlPanel.Models
{
    public class ClientUser
    {
        public int UserID { get; set; }

        [Required(ErrorMessage = "ClientID is Required")]
        [Range(1, int.MaxValue, ErrorMessage = "ClientID must be a number")]
        public int ClientID { get; set; }

        [Required(ErrorMessage = "Actor Name is Required")]
        public string ActorName { get; set; }
        [Required(ErrorMessage = "CEAccessKeyID is Required")]
        public string CEAccessKeyID { get; set; }

        public bool EnableInd { get; set; }

        // Integer array with name matching that of the checkboxes it's associated with
        public int[] Roles { get; set; }
        public int[] EndPoints { get; set; }


        public List<Roles> GetRoles()
        {
            var roles = new List<Roles>();
            using (var ent = new InsightsEntities())
            {
                var roleList = (from u in ent.Roles
                                select u).ToList();

                foreach (var r in roleList)
                {
                    var ro = new Roles();
                    ro.Id = r.RoleID;
                    ro.Name = r.Name;
                    roles.Add(ro);
                }
            }
            return roles;
        }

        public List<Endpoints> GetEndpoints()
        {
            var endpoints = new List<Endpoints>();
            using (var ent = new InsightsEntities())
            {
                var endpointList = (from e in ent.Endpoints
                                    select e).ToList();
                foreach (var e in endpointList)
                {
                    var ep = new Endpoints();
                    ep.Id = e.EndpointID;
                    ep.Name = e.Name;
                    endpoints.Add(ep);
                }
            }
            return endpoints;
        }


        public List<Roles> GetRoles(int id)
        {
            var roles = new List<Roles>();
            using (var ent = new InsightsEntities())
            {
                var roleList = (from u in ent.Roles
                                select u).ToList();

                var inRoles = (from ur in ent.UserRoles
                               where ur.UserID == id && ur.EnableInd == true
                               select ur).ToList();

                foreach (var r in roleList)
                {
                    var ro = new Roles();
                    ro.Id = r.RoleID;
                    ro.Name = r.Name;
                    foreach (var ir in inRoles)
                    {
                        if (ro.Id == ir.RoleID)
                        {
                            ro.Enabled = true;
                        }
                    }
                    roles.Add(ro);
                }
            }
            return roles;
        }

        public List<Endpoints> GetEndpoints(int id)
        {
            var endpoints = new List<Endpoints>();
            using (var ent = new InsightsEntities())
            {
                var endpointList = (from e in ent.Endpoints
                                    where e.EndpointID > 0
                                    select e).ToList();

                var userEndpoints = (from ue in ent.UserEndpoints
                                     where ue.UserID == id && ue.EnableInd == true
                                     select ue).ToList();

                foreach (var e in endpointList)
                {
                    var ep = new Endpoints();
                    ep.Id = e.EndpointID;
                    ep.Name = e.Name;
                    foreach (var ur in userEndpoints)
                    {
                        if (ep.Id == ur.EndpointID)
                        {
                            ep.Enabled = true;
                        }
                    }
                    endpoints.Add(ep);
                }
            }
            return endpoints;
        }

        public ClientUser GetUser(int id)
        {
            var cu = new ClientUser();

            using (var ent = new InsightsEntities())
            {
                //var DRactionPs = (from u in ent.Users
                //                  join ur in ent.UserRoles on u.UserID equals ur.UserID
                //                  join r in ent.Roles on ur.RoleID equals r.RoleID
                //                  join ue in ent.UserEndpoints on u.UserID equals ue.UserID
                //                  join e in ent.Endpoints on ue.EndpointID equals e.EndpointID
                //                  where u.UserID == id
                //                  select new { u.UserID, r.Name, EndPointName = e.Name }).ToList();

                var user = (from u in ent.Users
                            where u.UserID == id
                            select u).FirstOrDefault();

                cu.ClientID = user.ClientID;
                cu.UserID = user.UserID;
                cu.ActorName = user.ActorName;
                cu.CEAccessKeyID = user.CEAccessKeyID;
                cu.EnableInd = user.EnableInd;
            }
            return cu;
        }

        public void UpdateUser(ClientUser user)
        {
            using (var ent = new InsightsEntities())
            {
                List<User> clientUser = (from u in ent.Users
                                         where u.UserID == user.UserID
                                         select u).ToList();

                if (clientUser.Count != 0)
                {
                    User u = clientUser.ElementAt(0);
                    u.ActorName = user.ActorName;
                    u.CEAccessKeyID = user.CEAccessKeyID;
                    u.EnableInd = user.EnableInd;
                    u.UpdDate = System.DateTime.Now;
                }

                //roles
                var roles = (from r in ent.Roles
                             select r).ToList();


                var userRoles = (from u in ent.UserRoles
                                 where u.UserID == user.UserID
                                 select u).ToList();

                foreach (var r in roles)
                {
                    var found = false;
                    var createNew = true;
                    foreach (var ur in userRoles)
                    {
                        if (r.RoleID == ur.RoleID)
                        {
                            if (user.Roles != null)
                            {
                                foreach (var ir in user.Roles)
                                {
                                    if (r.RoleID == ir)
                                    {
                                        var uRole = ent.UserRoles.Where(s => s.RoleID == r.RoleID && s.UserID == user.UserID).FirstOrDefault<UserRole>();
                                        uRole.EnableInd = true;
                                        found = true;
                                        createNew = false;
                                    }
                                }
                            }
                            if (!found)
                            {
                                var uRole = ent.UserRoles.Where(s => s.RoleID == r.RoleID && s.UserID == user.UserID).FirstOrDefault<UserRole>();
                                uRole.EnableInd = false;
                                createNew = false;
                            }
                        }
                    }
                    if (createNew)
                    {
                        if (ent.UserRoles.Where(s => s.RoleID == r.RoleID && s.UserID == user.UserID).ToList().Count == 0)
                        {
                            if (user.Roles != null)
                            {
                                foreach (var ir in user.Roles)
                                {
                                    if (r.RoleID == ir)
                                    {
                                        var newRole = new UserRole();
                                        newRole.UserID = user.UserID;
                                        newRole.RoleID = r.RoleID;
                                        newRole.EnableInd = true;
                                        ent.UserRoles.Add(newRole);
                                    }
                                }
                            }
                        }
                    }
                }

                //EndPoints
                var endpoints = (from e in ent.Endpoints
                                 select e).ToList();


                var userEndpoints = (from u in ent.UserEndpoints
                                     where u.UserID == user.UserID
                                     select u).ToList();

                foreach (var e in endpoints)
                {
                    var found = false;
                    var createNew = true;
                    foreach (var ue in userEndpoints)
                    {
                        if (e.EndpointID == ue.EndpointID)
                        {
                            if (user.EndPoints != null)
                            {
                                foreach (var ie in user.EndPoints)
                                {
                                    if (e.EndpointID == ie)
                                    {
                                        var uEndpoint = ent.UserEndpoints.Where(s => s.EndpointID == e.EndpointID && s.UserID == user.UserID).FirstOrDefault<UserEndpoint>();
                                        uEndpoint.EnableInd = true;
                                        found = true;
                                        createNew = false;
                                    }
                                }
                            }
                            if (!found)
                            {
                                var uEndpoint = ent.UserEndpoints.Where(s => s.EndpointID == e.EndpointID && s.UserID == user.UserID).FirstOrDefault<UserEndpoint>();
                                uEndpoint.EnableInd = false;
                                createNew = false;
                            }
                        }

                    }
                    if (createNew)
                    {
                        if (ent.UserEndpoints.Where(s => s.EndpointID == e.EndpointID && s.UserID == user.UserID).ToList().Count == 0)
                        {
                            if (user.EndPoints != null)
                            {
                                foreach (var ie in user.EndPoints)
                                {
                                    if (e.EndpointID == ie)
                                    {
                                        var newEndpoint = new UserEndpoint();
                                        newEndpoint.UserID = user.UserID;
                                        newEndpoint.EndpointID = e.EndpointID;
                                        newEndpoint.EnableInd = true;
                                        ent.UserEndpoints.Add(newEndpoint);
                                    }
                                }
                            }
                        }
                    }
                }

                ent.SaveChanges();
            }
        }

        public void CreateUser(ClientUser user)
        {
            using (var ent = new InsightsEntities())
            {

                var u = new User();
                u.ClientID = user.ClientID;
                u.ActorName = user.ActorName;
                u.CEAccessKeyID = user.CEAccessKeyID;
                u.EnableInd = user.EnableInd;
                u.NewDate = System.DateTime.Now;
                u.UpdDate = System.DateTime.Now;
                ent.Users.Add(u);
                ent.SaveChanges();
                int userId = u.UserID;
                //Roles 
                if (user.Roles != null)
                {
                    foreach (var ir in user.Roles)
                    {
                        var r = new UserRole();
                        r.RoleID = Convert.ToByte(ir);
                        r.UserID = userId;
                        r.EnableInd = true;
                        ent.UserRoles.Add(r);
                    }
                }
                //EndPoints
                if (user.EndPoints != null)
                {
                    foreach (var ie in user.EndPoints)
                    {
                        var e = new UserEndpoint();
                        e.EnableInd = true;
                        e.EndpointID = Convert.ToByte(ie);
                        e.UserID = userId;
                        ent.UserEndpoints.Add(e);
                    }
                }
                ent.SaveChanges();
            }
        }

        public void UpdateEnvironment(List<Environments> environments, int userID)
        {
            using (var ent = new InsightsEntities())
            {
                foreach (var e in environments)
                {
                    if (e.UserID == -1)
                    {
                        var eu = new UserEnvironment();
                        eu.UserID = userID;
                        eu.EnvID = Convert.ToByte(e.EnvID);
                        eu.CESecretAccessKey = e.CEAccessKey;
                        eu.BasicKey = e.BasicAccessKey;
                        eu.UpdDate = System.DateTime.Now;
                        if (e.CEAccessKey != null || e.BasicAccessKey != null)
                        {
                            ent.UserEnvironments.Add(eu);
                        }
                    }
                    else
                    {
                        var uEnv = ent.UserEnvironments.Where(s => s.EnvID == e.EnvID && s.UserID == e.UserID).FirstOrDefault<UserEnvironment>();
                        uEnv.CESecretAccessKey = e.CEAccessKey;
                        uEnv.BasicKey = e.BasicAccessKey;
                        uEnv.UpdDate = System.DateTime.Now;
                    }
                }
                ent.SaveChanges();
            }
        }
    }


    public class Roles
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Enabled { get; set; }
    }

    public class Endpoints
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Enabled { get; set; }
    }
}