﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceSimDataGenerator
{
    public class ACEAmiHeader
    {
        #region Fields
        private readonly string _dailyFields = "IntValue";
        private readonly string _staticFields;
        private ACEReportType _reportType;
        private string _intervalFields;
        private char _del = ',';
        #endregion

        #region Properties
        public int ClientId { get; set; }
        public string MeterId { get; set; }
        public string AccountNumber { get; set; }
        public ACECommodityId Commodity { get; set; }
        public ACEUOMId UOMId { get; set; }
        public double VolumeFactor { get; set; }
        public ACEMeterDirection? Direction { get; set; }
        public ACETOUId TOU { get; set; }
        public DateTime? ProjectedReadDate { get; set; }
        public DateTime TimeStamp { get; set; }
        public string TimeZone { get; set; }
        public char Delimiter { get; set; }

       
        public ACEReportType ReportType {
            get => _reportType;
            
            set
            {
                _reportType = value;
                switch (_reportType)
                {
                    case ACEReportType.Daily:
                        _intervalFields = _dailyFields;
                        break;
                    case ACEReportType.Hourly:
                        _intervalFields = GetHourlyIntervalFields();
                        break;
                    case ACEReportType.FifteenMin:
                        _intervalFields = GetFifteenMinIntervalFields();
                        break;
                    case ACEReportType.ThirtyMin:
                        _intervalFields = GetThirtyMinIntervalFields();
                        break;
                }
                
            }
        }
        #endregion

        #region Constructors
        public ACEAmiHeader()
        {
            _staticFields = GetStaticFields();
        }
        public ACEAmiHeader(ACEReportType reportType)
        {
            ReportType = reportType;
            _staticFields = GetStaticFields();
        }

        public ACEAmiHeader(ACEReportType reportType, char delimiter)
        {
            ReportType = reportType;
            _del = delimiter;
            _staticFields = GetStaticFields();
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            var sb = new StringBuilder(_staticFields);
            sb.Append(_intervalFields);
            return sb.ToString();
        }

        private string GetHourlyIntervalFields()
        {

            var intFields = "IntValue0000" + _del;
            intFields = intFields + "IntValue0100" + _del;
            intFields = intFields + "IntValue0200" + _del;
            intFields = intFields + "IntValue0300" + _del;
            intFields = intFields + "IntValue0400" + _del;
            intFields = intFields + "IntValue0500" + _del;
            intFields = intFields + "IntValue0600" + _del;
            intFields = intFields + "IntValue0700" + _del;
            intFields = intFields + "IntValue0800" + _del;
            intFields = intFields + "IntValue0900" + _del;
            intFields = intFields + "IntValue1000" + _del;
            intFields = intFields + "IntValue1100" + _del;
            intFields = intFields + "IntValue1200" + _del;
            intFields = intFields + "IntValue1300" + _del;
            intFields = intFields + "IntValue1400" + _del;
            intFields = intFields + "IntValue1500" + _del;
            intFields = intFields + "IntValue1600" + _del;
            intFields = intFields + "IntValue1700" + _del;
            intFields = intFields + "IntValue1800" + _del;
            intFields = intFields + "IntValue1900" + _del;
            intFields = intFields + "IntValue2000" + _del;
            intFields = intFields + "IntValue2100" + _del;
            intFields = intFields + "IntValue2200" + _del;
            intFields = intFields + "IntValue2300";
            return intFields;
        }
        private string GetThirtyMinIntervalFields()
        {

            var intFields = "IntValue0000" + _del;
            intFields = intFields + "IntValue0030" + _del;
            intFields = intFields + "IntValue0100" + _del;
            intFields = intFields + "IntValue0130" + _del;
            intFields = intFields + "IntValue0200" + _del;
            intFields = intFields + "IntValue0230" + _del;
            intFields = intFields + "IntValue0300" + _del;
            intFields = intFields + "IntValue0330" + _del;
            intFields = intFields + "IntValue0400" + _del;
            intFields = intFields + "IntValue0430" + _del;
            intFields = intFields + "IntValue0500" + _del;
            intFields = intFields + "IntValue0530" + _del;
            intFields = intFields + "IntValue0600" + _del;
            intFields = intFields + "IntValue0630" + _del;
            intFields = intFields + "IntValue0700" + _del;
            intFields = intFields + "IntValue0730" + _del;
            intFields = intFields + "IntValue0800" + _del;
            intFields = intFields + "IntValue0830" + _del;
            intFields = intFields + "IntValue0900" + _del;
            intFields = intFields + "IntValue0930" + _del;
            intFields = intFields + "IntValue1000" + _del;
            intFields = intFields + "IntValue1030" + _del;
            intFields = intFields + "IntValue1100" + _del;
            intFields = intFields + "IntValue1130" + _del;
            intFields = intFields + "IntValue1200" + _del;
            intFields = intFields + "IntValue1230" + _del;
            intFields = intFields + "IntValue1300" + _del;
            intFields = intFields + "IntValue1330" + _del;
            intFields = intFields + "IntValue1400" + _del;
            intFields = intFields + "IntValue1430" + _del;
            intFields = intFields + "IntValue1500" + _del;
            intFields = intFields + "IntValue1530" + _del;
            intFields = intFields + "IntValue1600" + _del;
            intFields = intFields + "IntValue1630" + _del;
            intFields = intFields + "IntValue1700" + _del;
            intFields = intFields + "IntValue1730" + _del;
            intFields = intFields + "IntValue1800" + _del;
            intFields = intFields + "IntValue1830" + _del;
            intFields = intFields + "IntValue1900" + _del;
            intFields = intFields + "IntValue1930" + _del;
            intFields = intFields + "IntValue2000" + _del;
            intFields = intFields + "IntValue2030" + _del;
            intFields = intFields + "IntValue2100" + _del;
            intFields = intFields + "IntValue2130" + _del;
            intFields = intFields + "IntValue2200" + _del;
            intFields = intFields + "IntValue2230" + _del;
            intFields = intFields + "IntValue2300" + _del;
            intFields = intFields + "IntValue2330";

            return intFields;
        }

        private string GetFifteenMinIntervalFields()
        {

           
            var intFields = "IntValue0000" + _del;
            intFields = intFields + "IntValue0015" + _del;
            intFields = intFields + "IntValue0030" + _del;
            intFields = intFields + "IntValue0045" + _del;
            intFields = intFields + "IntValue0100" + _del;
            intFields = intFields + "IntValue0115" + _del;
            intFields = intFields + "IntValue0130" + _del;
            intFields = intFields + "IntValue0145" + _del;
            intFields = intFields + "IntValue0200" + _del;
            intFields = intFields + "IntValue0215" + _del;
            intFields = intFields + "IntValue0230" + _del;
            intFields = intFields + "IntValue0245" + _del;
            intFields = intFields + "IntValue0300" + _del;
            intFields = intFields + "IntValue0315" + _del;
            intFields = intFields + "IntValue0330" + _del;
            intFields = intFields + "IntValue0345" + _del;
            intFields = intFields + "IntValue0400" + _del;
            intFields = intFields + "IntValue0415" + _del;
            intFields = intFields + "IntValue0430" + _del;
            intFields = intFields + "IntValue0445" + _del;
            intFields = intFields + "IntValue0500" + _del;
            intFields = intFields + "IntValue0515" + _del;
            intFields = intFields + "IntValue0530" + _del;
            intFields = intFields + "IntValue0545" + _del;
            intFields = intFields + "IntValue0600" + _del;
            intFields = intFields + "IntValue0615" + _del;
            intFields = intFields + "IntValue0630" + _del;
            intFields = intFields + "IntValue0645" + _del;
            intFields = intFields + "IntValue0700" + _del;
            intFields = intFields + "IntValue0715" + _del;
            intFields = intFields + "IntValue0730" + _del;
            intFields = intFields + "IntValue0745" + _del;
            intFields = intFields + "IntValue0800" + _del;
            intFields = intFields + "IntValue0815" + _del;
            intFields = intFields + "IntValue0830" + _del;
            intFields = intFields + "IntValue0845" + _del;
            intFields = intFields + "IntValue0900" + _del;
            intFields = intFields + "IntValue0915" + _del;
            intFields = intFields + "IntValue0930" + _del;
            intFields = intFields + "IntValue0945" + _del;
            intFields = intFields + "IntValue1000" + _del;
            intFields = intFields + "IntValue1015" + _del;
            intFields = intFields + "IntValue1030" + _del;
            intFields = intFields + "IntValue1045" + _del;
            intFields = intFields + "IntValue1100" + _del;
            intFields = intFields + "IntValue1115" + _del;
            intFields = intFields + "IntValue1130" + _del;
            intFields = intFields + "IntValue1145" + _del;
            intFields = intFields + "IntValue1200" + _del;
            intFields = intFields + "IntValue1215" + _del;
            intFields = intFields + "IntValue1230" + _del;
            intFields = intFields + "IntValue1245" + _del;
            intFields = intFields + "IntValue1300" + _del;
            intFields = intFields + "IntValue1315" + _del;
            intFields = intFields + "IntValue1330" + _del;
            intFields = intFields + "IntValue1345" + _del;
            intFields = intFields + "IntValue1400" + _del;
            intFields = intFields + "IntValue1415" + _del;
            intFields = intFields + "IntValue1430" + _del;
            intFields = intFields + "IntValue1445" + _del;
            intFields = intFields + "IntValue1500" + _del;
            intFields = intFields + "IntValue1515" + _del;
            intFields = intFields + "IntValue1530" + _del;
            intFields = intFields + "IntValue1545" + _del;
            intFields = intFields + "IntValue1600" + _del;
            intFields = intFields + "IntValue1615" + _del;
            intFields = intFields + "IntValue1630" + _del;
            intFields = intFields + "IntValue1645" + _del;
            intFields = intFields + "IntValue1700" + _del;
            intFields = intFields + "IntValue1715" + _del;
            intFields = intFields + "IntValue1730" + _del;
            intFields = intFields + "IntValue1745" + _del;
            intFields = intFields + "IntValue1800" + _del;
            intFields = intFields + "IntValue1815" + _del;
            intFields = intFields + "IntValue1830" + _del;
            intFields = intFields + "IntValue1845" + _del;
            intFields = intFields + "IntValue1900" + _del;
            intFields = intFields + "IntValue1915" + _del;
            intFields = intFields + "IntValue1930" + _del;
            intFields = intFields + "IntValue1945" + _del;
            intFields = intFields + "IntValue2000" + _del;
            intFields = intFields + "IntValue2015" + _del;
            intFields = intFields + "IntValue2030" + _del;
            intFields = intFields + "IntValue2045" + _del;
            intFields = intFields + "IntValue2100" + _del;
            intFields = intFields + "IntValue2115" + _del;
            intFields = intFields + "IntValue2130" + _del;
            intFields = intFields + "IntValue2145" + _del;
            intFields = intFields + "IntValue2200" + _del;
            intFields = intFields + "IntValue2215" + _del;
            intFields = intFields + "IntValue2230" + _del;
            intFields = intFields + "IntValue2245" + _del;
            intFields = intFields + "IntValue2300" + _del;
            intFields = intFields + "IntValue2315" + _del;
            intFields = intFields + "IntValue2330" + _del;
            intFields = intFields + "IntValue2345";
            return intFields;
        }
        private string GetStaticFields()
        {
            var staticFields = "";
            staticFields = staticFields + "ClientId" + _del;
            staticFields = staticFields + "MeterId" + _del;
            staticFields = staticFields + "AccountNumber" + _del;
            staticFields = staticFields + "ServicePointId" + _del;
            staticFields = staticFields + "CommodityId" + _del;
            staticFields = staticFields + "UOMId" + _del;
            staticFields = staticFields + "VolumeFactor" + _del;
            staticFields = staticFields + "Direction" + _del;
            staticFields = staticFields + "ProjectedReadDate" + _del;
            staticFields = staticFields + "TimeStamp" + _del;
            staticFields = staticFields + "Timezone" + _del;
            return staticFields;
        }
        #endregion

    }

    #region Enums
    public enum ACECommodityId
    {
        Electric = 1,
        Gas = 2,
        Water = 3,
        Sewer = 4,
        Refuse = 5,
        Oil = 6,
        Propane = 7,
        Tv = 8,
        Fire = 9,
        Transportation = 10,
        Other = 99
    }
    public enum ACEUOMId
    {
        kwh = 0,
        therms = 1,
        ccf = 2,
        cf = 3,
        cgal = 4,
        hgal = 5,
        gal = 6,
        kgal = 7,
        hcf = 8,
        cm = 11

    }
    public enum ACEMeterDirection
    {
        Forward = 0,
        Reverse = 1
    }
    public enum ACETOUId
    {
        NoTOU = 0,
        OnPeak = 1,
        OffPeak = 2,
        Shoulder1 = 3,
        Shoulder2 = 4,
        CriticalPeak = 5
    }
    public enum ACEReportType
    {
        Daily,
        Hourly,
        ThirtyMin,
        FifteenMin
    }
    #endregion
}
