﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceSimDataGenerator
{
    public class ACEBillingHeader
    {
        #region Fields
        private int _clientId;
        private string _customerId;
        private string _serviceZipCode;
        private readonly string[] _billingHeaderFields ={
"client_id",
"customer_id",
"account_id",
"premise_id",
"mail_address_line_1",
"mail_address_line_2",
"mail_city",
"mail_state",
"mail_zip_code",
"first_name",
"last_name",
"phone_1",
"phone_2",
"email",
"customer_type",
"service_address_line1",
"service_address_line2",
"service_city",
"service_state",
"service_zip_code",
"service_agreement_id",
"service_point_id",
"active_date",
"inactive_date",
"read_cycle",
"rate_code",
"service_commodity",
"account_structure_type",
"bill_date",
"bill_period_days",
"service_read_enddate", // bill_period_enddate?
"billperiod_type",
"is_estimate",
"usage_value",
"usage_charge",
"meter_type",
"meter_units",
"meter_id",
"meter_replaces_meterid",
"UtilityBillRecordId",
"Cancelled_UtilityBillRecordId",
"bldg_sq_foot",
"year_built",
"bedrooms",
"assess_value",
"Programs",
"basic_charge",
"service_additional_cost",
"standard_charges",
"demand_unit",
"demand_charge",
"timestamp_demand_value"
        };

        //private readonly string[] _billingHeaderFields ={
        //    "customer_id",
        //    "premise_id",
        //    "mail_address_line_1",
        //    "mail_address_line_2",
        //    "mail_address_line_3",
        //    "mail_city",
        //    "mail_state",
        //    "mail_zip_code",
        //    "first_name",
        //    "last_name",
        //    "phone_1",
        //    "phone_2",
        //    "email",
        //    "customer_type",
        //    "account_id",
        //    "active_date",
        //    "inactive_date",
        //    "read_cycle",
        //    "rate_code",
        //    "service_house_number",
        //    "service_street_name",
        //    "service_unit",
        //    "service_city",
        //    "service_state",
        //    "service_zip_code",
        //    "meter_type",
        //    "meter_units",
        //    "bldg_sq_foot",
        //    "year_built",
        //    "bedrooms",
        //    "assess_value",
        //    "usage_value",
        //    "bill_enddate",
        //    "bill_days",
        //    "is_estimate",
        //    "usage_charge",
        //    "clientId",
        //    "programs",
        //    "service_commodity",
        //    "account_structure_type",
        //    "meter_id",
        //    "billperiod_type",
        //    "meter_replaces_meterid",
        //    "service_read_date",
        //    "service_read_startdate",
        //    "service_point_id",
        //    //"BudgetBillingStatus",
        //    //"LastPaymentAmount",
        //    //"LastPaymentDate",
        //    //"TotalAmount",
        //    //"PastDue",
        //    //"PaymentType",
        //    //"DueDate",
        //    //"BillMonth",
        //    //"BillDate",
        //    //"BillCycle",
        //    //"PremiseDescription",
        //    //"ReadCycle",
        //    //"RetailSupply",
        //    //"NextReaddate",
        //    //"MeterDescription",
        //    //"MeterLocation",
        //    //"MeterMultiplier",
        //    //"MeterType",
        //    //"MeterTwoWay",
        //    //"TOTAL",
        //    //"TOTAL_UOM",
        //    //"TOTAL_TIMESTAMP",
        //    "MAX_DMND",
        //    "MAX_DMND_TIMESTAMP",
        //    "MAX_DMND_UOM",
        //    "STD_CUST",
        //    "GOVT_TAX_MUNI",
        //    "STD_MAX_DMND",
        //    "STD_ENGY"
        //};
        
        #endregion

        #region Properties
        public double Rate { get; set; }
        public List<ACEKeyValuePair<string,string>> StaticHeaderValues { get; set; }
        public ACEBillDays BillDates { get; set; }
        public double TaxRate { get; set; }
        public int ClientId { get; set; }
        public string[] BillingHeaderFields
        {
            get => _billingHeaderFields;
        }
        
        public string CustomerId
        {
            get => _customerId;
            set
            {
                _customerId = value;
                StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("customer_id", _customerId.ToString()));
            }
        }
       
        public string ServiceZipCode
        {
            get => _serviceZipCode; 
            set
            {
                _serviceZipCode = value;
                StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("service_zip_code", _serviceZipCode.ToString()));
            }
        }
        #endregion

        #region Constructors
        public ACEBillingHeader()
        {
            StaticHeaderValues = new List<ACEKeyValuePair<string, string>>();
            BillDates = new ACEBillDays();
            
        }
        #endregion

        #region Methods
        public int GetFieldIndexByFieldName(string name)
        {
            for(int i=0; i<_billingHeaderFields.Length; i++)
            {
                if (_billingHeaderFields[i] == name)
                    return i;
            }
            //not found, return nonsense
            throw new Exception("Billing field name '" + name + "' not found.");
        }
        #endregion
    }

   
}
