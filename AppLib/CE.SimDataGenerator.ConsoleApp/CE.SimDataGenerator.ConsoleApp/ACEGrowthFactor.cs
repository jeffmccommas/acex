﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceSimDataGenerator
{
    public class ACEGrowthFactor
    {
        #region Properties
        public DateTime GrowthStart { get; set; }
        public DateTime GrowthEnd { get; set; }
        public double GrowthFactor { get; set; }
        public ACEGrowthPeriod GrowthPeriod { get; set; }
        #endregion

        #region Constructors
        public ACEGrowthFactor(DateTime start, DateTime end, double growthRate, ACEGrowthPeriod period)
        {
            GrowthStart = start;
            GrowthEnd = end;
            GrowthFactor = growthRate;
            GrowthPeriod = period;
        }
        public ACEGrowthFactor()
        {

        }
        #endregion
    }

    #region Enum
    public enum ACEGrowthPeriod
    {
        Hourly,
        Daily,
        Weekly,
        Monthly,
        Yearly,
        None
    }
    #endregion
}
