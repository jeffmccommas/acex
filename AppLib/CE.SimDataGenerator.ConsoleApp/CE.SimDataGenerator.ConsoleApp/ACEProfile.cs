﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceSimDataGenerator
{
    public class ACEProfile
    {
        #region Properties
        public int ClientId { get; set; }
        public ACEAmiHeader AmiHeader { get; set; }
        public ACEBillingHeader BillingHeader { get; set; }
        public double[] HourlyFactors { get; set; }
        public double[] WeekOfYearFactors { get; set; }
        public double[] DayOfWeekFactors { get; set; }
        public int CustomerId { get; set; }
        
       
        public string PremiseId { get; set; }
        public string ServicePointId { get; set; }

        public ACEProfileType ProfileType { get; set; }
        public double BaseLowRange { get; set; }
        public double BaseHighRange { get; set; }
        //public string OutputFolder { get; set; }




        public DateTime StartDate { get ; set;  }
        public DateTime EndDate { get; set; }
        public int DaysBack { get; set; }
        //private DateTime? _endDate;
        //public DateTime EndDate
        //{
        //    get
        //    {
        //        return this._endDate.HasValue
        //            ? this._endDate.Value
        //            : DateTime.Now;
        //    }
        //    set { this._endDate = value; }
        //}
        public GrowthFactorList GrowthFactors { get; set; }

        public int HotWeatherTempThreshhold { get; set; }
        public int HotWeatherFactorDenominator { get; set; } // lower number means greater impact
        public int ColdWeatherTempThreshhold { get; set; }
        public int ColdWeatherFactorDenominator { get; set; } // lower number means greater impact
        #endregion

        #region Constructors
        public ACEProfile()
        {
            AmiHeader = new ACEAmiHeader();
            BillingHeader = new ACEBillingHeader();
            GrowthFactors = new GrowthFactorList();
            EndDate = DateTime.Today.AddDays(-1); // a profile will overwrite this, but if there is no value set, the value will default to yesterday
        }

        public ACEProfile(int clientId, string premiseId, string servicePointId)
        {
            ClientId = clientId;
            PremiseId = premiseId;
            ServicePointId = servicePointId;

            // the parameters should be assigned on the profile and passed down to the child headers
            AmiHeader = new ACEAmiHeader();
            BillingHeader = new ACEBillingHeader();


            HourlyFactors = new double[24];
            WeekOfYearFactors = new double[54];
            DayOfWeekFactors = new double[7];
            GrowthFactors = new GrowthFactorList();

            // TODO: heating type factor??.
            // TODO: cooling type factor??
        }
        #endregion
    }

    #region Enums
    public enum ACEProfileType
    {
        Daily,
        Historical
    }
    #endregion

}