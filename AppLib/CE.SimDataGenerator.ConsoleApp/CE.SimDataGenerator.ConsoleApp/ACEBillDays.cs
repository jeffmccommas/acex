﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceSimDataGenerator
{
    public class ACEBillDays:List<ACEBillDay>
    {
        #region Constructors
        public ACEBillDays()
        {

        }
        #endregion

        #region Methods
        public DateTime GetNextReadDate(DateTime currentReadDate)
        {
            return GetAdjoiningReadDate(currentReadDate, 1);
        }

        public DateTime GetPreviousReadDate(DateTime currentReadDate)
        {
            return GetAdjoiningReadDate(currentReadDate, -1);
        }

        public bool CheckIfBillDate(DateTime checkDate)
        {
           return this.Any(d => d.GetBillDayByYear(checkDate.Year) == checkDate.Date);
        }

        private DateTime GetAdjoiningReadDate(DateTime currentReadDate, int spaces)
        {
            DateTime adjReadDay;

            var sortedDates = (from billdate in this orderby billdate.Month, billdate.Day select billdate).ToList<ACEBillDay>(); // Be sure bill dates are sorted chronologically
            var billingDayIndex = sortedDates.FindIndex(a => a.Day == currentReadDate.Day && a.Month == currentReadDate.Month);

            //var billingDayIndex = sortedDates.IndexOf(new ACEBillDay(currentReadDate.Month, currentReadDate.Day));
            if (billingDayIndex + spaces < this.Count)
            {
                adjReadDay = sortedDates[billingDayIndex + spaces].GetBillDayByYear(currentReadDate.Year);
            }
            else
            {
                adjReadDay = currentReadDate.AddDays(30);
            }
            return adjReadDay;
        }
        #endregion


    }
}
