﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceSimDataGenerator
{
    public class Logger
    {
        private string _filePath;
        
        public Logger(string filePath)
        {
            _filePath = filePath;
            // Is there a current log file?
            // if not create one
            if (!File.Exists(filePath))
            {
                FileStream fs = File.Create(filePath);
                fs.Close();
               
            }
           
        }

        public void WriteToLog(string message)
        {
            File.AppendAllText(_filePath, DateTime.Now.ToString() + ": " + message + Environment.NewLine);
        }
    }
}
