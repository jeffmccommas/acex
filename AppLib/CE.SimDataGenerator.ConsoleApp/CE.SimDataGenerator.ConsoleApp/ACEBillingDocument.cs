﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceSimDataGenerator
{
    public class ACEBillingDocument
    {
        #region Properties
        public ACEProfile Profile { get; set; }
        public List<ACEBillingRow> BillingRows { get; set; }
        public string FilePath { get; set; }
        #endregion

        #region Constructors
        public ACEBillingDocument(ACEProfile profile, string filePath)
        {
            Profile = profile;
            BillingRows = new List<ACEBillingRow>();
            FilePath = filePath;
        }
        #endregion

        #region Methods
        private void AddValueToRow(int rowNumber, string fieldName, string value)
        {
            int fieldIndex = Profile.BillingHeader.GetFieldIndexByFieldName(fieldName);
            BillingRows[rowNumber].BillingFields[fieldIndex] = value;
        }

        public void AddBillingRow(List<KeyValuePair<string, string>> fieldValues)
        {
            // create row
            BillingRows.Add(new ACEBillingRow(Profile.BillingHeader.BillingHeaderFields.Length));

            int lastItemIndex = BillingRows.Count - 1;
            //Add header values to row;
            foreach(ACEKeyValuePair<string,string> staticField in Profile.BillingHeader.StaticHeaderValues)
            {
                AddValueToRow(lastItemIndex, staticField.Key, staticField.Value);
            }

            foreach (KeyValuePair<string,string> fieldValue in fieldValues)
            {
                AddValueToRow(lastItemIndex, fieldValue.Key, fieldValue.Value);
            }
            
            
        }

       
        public void Write()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Join("|", Profile.BillingHeader.BillingHeaderFields));
            foreach(ACEBillingRow row in BillingRows)
            {
                sb.AppendLine(string.Join("|", row.BillingFields));
            }
            System.IO.File.WriteAllText(FilePath, sb.ToString());
        }
        #endregion
    }
}
