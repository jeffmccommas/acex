﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceSimDataGenerator
{
    public class ACEBillDay
    {
        public int Month { get; set; }
        public int Day { get; set; }
        public ACEBillDay()
        {

        }
        public ACEBillDay(int month, int day)
        {
            Month = month;
            Day = day;
            

            // test validity by trying to get currentyear datetime
            DateTime test = GetCurrentYearBillDay();
           

        }

        public DateTime GetCurrentYearBillDay()
        {
            int currentyear = DateTime.Today.Year;
            return GetBillDayByYear(currentyear);
            
        }
        public DateTime GetBillDayByYear(int year)
        {
            DateTime datevalue;
            if (!DateTime.TryParse(year + "-" + this.Month + "-" + this.Day, out datevalue))
            {
                throw new Exception("Invalid month and day combination.");
            }
            else
            {
                return datevalue;
            }
        }

    }
}
