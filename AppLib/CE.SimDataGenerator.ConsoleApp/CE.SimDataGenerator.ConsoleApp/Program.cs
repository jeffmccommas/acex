﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Xml.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceSimDataGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            // UNCOMMENT ONLY FOR PROFILE GENERATION!!!!!!!!!!!!!!!!
            //ACEProfile generate_profile = GetProfile();
            //using (FileStream fs = new FileStream("C:\\ACESimData\\profile.xml", FileMode.Create))
            //{
            //    XmlSerializer xSer = new XmlSerializer(typeof(ACEProfile));

            //    xSer.Serialize(fs, generate_profile);
            //}
            //return;
            // END UNCOMMENT
            string logFilePath = ConfigurationManager.AppSettings["logpath"] + "\\log_" + DateTime.Today.ToString("yyyy_MM_dd") + ".txt";


            Logger logger = new Logger(logFilePath);
            logger.WriteToLog("Application running.");

            if (args.Length == 0)
            {
                // do some complaining and demanding
                logger.WriteToLog("No parameters were found. Application requires input and output file paths as parameters.");
                logger.WriteToLog("APPLICATION END --------------------");
                return;
            } else
            {
                if (!Directory.Exists(args[0]))
                {
                    logger.WriteToLog("Provided input directory does not exist.");
                    logger.WriteToLog("APPLICATION END --------------------");
                    return;
                } 
                
                if (!Directory.Exists(args[1]))
                {
                    logger.WriteToLog("Provided output directory does not exist.");
                    logger.WriteToLog("APPLICATION END --------------------");
                    return;
                }
            }
            if (!Directory.Exists(Path.Combine(args[0], "HistoricalArchive")))
            {
                logger.WriteToLog("Warning: Missing HistoricalArchive Folder");
                logger.WriteToLog("Creating HistoricalArchive Folder");
                Directory.CreateDirectory(Path.Combine(args[0], "HistoricalArchive"));
            }

            var path = args[0]; //@"C:\DEV\ACESimData\Profiles";
            var outputpath = args[1];

            if (!Directory.EnumerateFiles(path, "*.xml").Any())
            {
                logger.WriteToLog("Profile folder " + path + " contains no profiles.");
                logger.WriteToLog("APPLICATION END --------------------");
                return;
            }


            foreach (var file in Directory.EnumerateFiles(path, "*.xml"))
            {
                ACEProfile profile = new ACEProfile();
                try
                {
                    profile = GetProfileFromXml(file);
                    logger.WriteToLog("-----");
                } catch (Exception ex)
                {
                    logger.WriteToLog("****************** ERROR: " + ex.ToString());
                }
                logger.WriteToLog("Processing profile " + file);


                // FOR DAILY MODE, OVERWRITE PROFILE START AND END DATES
                if (profile.ProfileType == ACEProfileType.Daily)
                {
                    logger.WriteToLog("Operating in Daily mode.");
                    profile.EndDate = DateTime.Today.AddDays(profile.DaysBack * -1);

                    // check for bill day
                    bool isBillDay = profile.BillingHeader.BillDates.CheckIfBillDate(DateTime.Today.Date); // we need some sort of error when we run out of billing days
                    // if not bill day just set both start and end for today
                    if (!isBillDay)
                    {
                        
                        profile.StartDate = profile.EndDate;
                    }
                    else
                    { 
                        // if bill day, set startdate to last bill day + 1 day and endDate to today
                        profile.StartDate = profile.BillingHeader.BillDates.GetPreviousReadDate(profile.EndDate.Date).AddDays(1);
                        logger.WriteToLog("Reached bill date. Recreating AMI data since last bill date and generating billing data to match.");
                    }

                } else
                {
                    // 
                    logger.WriteToLog("Operating in historical mode.");
                }

                Generator gen = new Generator(profile, outputpath, logger);
                gen.MakeReadings();
                logger.WriteToLog("Processing of file " + file + " completed.");
                //If historical profile, archive it 
                if (profile.ProfileType == ACEProfileType.Historical)
                {
                    logger.WriteToLog("Archiving historical profile.");
                    File.Move(file, Path.Combine(path, "HistoricalArchive", Path.GetFileName(file)));
                    
                }
                
               
            }
            logger.WriteToLog("APPLICATION END -------------------------------------");

        }

        static ACEProfile GetProfileFromXml(string path)
        {
            
                using (FileStream fs = new FileStream(path, FileMode.Open)) //double check that...
                {
                    XmlSerializer xSer = new XmlSerializer(typeof(ACEProfile));

                    var myObject = xSer.Deserialize(fs);
                    return (ACEProfile)myObject;
                }
           
        }

        static ACEProfile GetProfile()
        {
            ACEProfile profile = new ACEProfile(4, "resdemo12EGW_E", "SP_resdemo12EGW_E");
            profile.BaseLowRange = 0.2;
            profile.BaseHighRange = 0.8;
            profile.ProfileType = ACEProfileType.Historical;
            profile.StartDate = new DateTime(2015, 1, 1);
            profile.EndDate = new DateTime(2017, 1, 1);
            //profile.OutputFolder = "C:\\Dev\\ACESimData";
            // TODO: GET THIS STUFF FROM A FILE. Deserialize it.
            profile.AmiHeader.AccountNumber = "resdemo12EGW";
            profile.AmiHeader.UOMId = ACEUOMId.kwh;
            profile.AmiHeader.Commodity = ACECommodityId.Electric;
            profile.AmiHeader.TimeZone = "EST";
            profile.AmiHeader.MeterId = "resdemo12EGW_E";
            profile.ColdWeatherTempThreshhold = 55;
            profile.HotWeatherTempThreshhold = 80;
            profile.ColdWeatherFactorDenominator = 25; // lower number generates greater impact
            profile.HotWeatherFactorDenominator = 50; // lower number generates greater impact

            //profile.AmiHeader.TimeStamp = DateTime.Today;
            profile.AmiHeader.ProjectedReadDate = new DateTime(2017, 1, 1);
            profile.AmiHeader.ReportType = ACEReportType.FifteenMin;

            profile.BillingHeader.CustomerId = "resdemo12EGW";
            profile.BillingHeader.Rate = 0.13;
            profile.BillingHeader.TaxRate = 0.10;
            profile.BillingHeader.ServiceZipCode = "53545";
  
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string,string>("mail_address_line_1", "123 W MADEUP ST"));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("mail_city", "DEMOVILLE"));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("mail_state", "WI"));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("mail_zip_code", profile.BillingHeader.ServiceZipCode));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("customer_type", "RESIDENTIAL"));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("first_name", "JANE"));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("last_name", "DOE"));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("account_id", profile.AmiHeader.AccountNumber));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("premise_id", profile.PremiseId));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("read_cycle", "1"));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("rate_code", "GS-1"));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("service_street_name", "123 W MADEUP ST"));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("service_city", "DEMOVILLE"));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("service_state", "WI"));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("meter_type", "AMR"));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("client_id", profile.ClientId.ToString()));
            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("meter_id", profile.AmiHeader.MeterId.ToString()));
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("BudgetBillingStatus", "N"));
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("LastPaymentAmount", "0"));
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("TotalAmount", "0"));
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("PremiseDescription", "Single Family"));
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("ReadCycle", "5")); // hard coded for now. I don't know what this means or if it should be dynamic -- bkj
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("RetailSupply", "ConsolidatedBilling"));
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("MeterLocation", "Outside")); // hard coded because who will care
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("MeterMultiplier", "1")); // hard coded
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("MeterType", "AMR")); // hard coded but needs to match hard coded meter_type above

            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("TotalAmount", "0"));
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("TotalAmount", "0"));
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("TotalAmount", "0"));
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("TotalAmount", "0"));
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("TotalAmount", "0"));
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("TotalAmount", "0"));
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("TotalAmount", "0"));
            //profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("TotalAmount", "0"));



            profile.BillingHeader.StaticHeaderValues.Add(new ACEKeyValuePair<string, string>("service_zip_code", "66666")); // add static values such as address with key value pairs. Keys must match header names of billing file
            // etc.
            profile.BillingHeader.BillDates.Add(new ACEBillDay(2,2));
            profile.BillingHeader.BillDates.Add(new ACEBillDay(3, 5));
            profile.BillingHeader.BillDates.Add(new ACEBillDay(4, 2));
            profile.BillingHeader.BillDates.Add(new ACEBillDay(5, 4));
            profile.BillingHeader.BillDates.Add(new ACEBillDay(6, 1));
            profile.BillingHeader.BillDates.Add(new ACEBillDay(7, 3));
            profile.BillingHeader.BillDates.Add(new ACEBillDay( 8, 7));
            profile.BillingHeader.BillDates.Add(new ACEBillDay( 9, 1));
            profile.BillingHeader.BillDates.Add(new ACEBillDay( 10, 2));
            profile.BillingHeader.BillDates.Add(new ACEBillDay( 11, 3));
            profile.BillingHeader.BillDates.Add(new ACEBillDay( 12, 4));
            profile.BillingHeader.BillDates.Add(new ACEBillDay( 1, 1));
            //profile.BillingHeader.BillDates.Add(new DateTime(2016, 2, 1));
            //profile.BillingHeader.BillDates.Add(new DateTime(2016, 3, 1));
            //profile.BillingHeader.BillDates.Add(new DateTime(2016, 4, 1));
            //profile.BillingHeader.BillDates.Add(new DateTime(2016, 5, 1));
            //profile.BillingHeader.BillDates.Add(new DateTime(2016, 6, 1));
            //profile.BillingHeader.BillDates.Add(new DateTime(2016, 7, 1));
            //profile.BillingHeader.BillDates.Add(new DateTime(2016, 8, 1));
            //profile.BillingHeader.BillDates.Add(new DateTime(2016, 9, 1));
            //profile.BillingHeader.BillDates.Add(new DateTime(2016, 10, 1));
            //profile.BillingHeader.BillDates.Add(new DateTime(2016, 11, 1));
            //profile.BillingHeader.BillDates.Add(new DateTime(2016, 12, 1));
            //profile.BillingHeader.BillDates.Add(new DateTime(2017, 1, 1));
            //profile.BillingHeader.BillDates.Add(new DateTime(2017, 6, 25));
            //profile.BillingHeader.BillDates.Add(new DateTime(2017, 7, 27));




            SetDayofWeekFactors(profile.DayOfWeekFactors);
            SetWeekOfYearFactors(profile.WeekOfYearFactors);
            SetHourlyFactors(profile.HourlyFactors);
            SetGrowthFactors(profile.GrowthFactors);
            //profile.BillingCycleDays = 28;
            return profile;
        }
        static void SetDayofWeekFactors(double[] factors)
        {
            // TODO: get this from a profile xml file
            
            // TYPICAL RESIDENTIAL INCREASED USAGE ON SATURDAY
            factors[5] = 1.1;

            
        }

        static void SetWeekOfYearFactors(double[] factors)
        {
            // TODO: get this from a profile xml file

            // TODO: Perhaps replace this seasonal adj with adjustment against actual weather data obtained from a webservice  
            // Seasonal and weather factors handled by historical weather lookups for a given zip code

            // Use weekly factors to simulate large non-seasonal usage changes--festivals, conferences, etc. 


            //factors[0] = 1.3;
            //factors[1] = 1.3;
            //factors[2] = 1.3;
            //factors[3] = 1.3;
            //factors[4] = 1.3;
            //factors[5] = 1.2;
            //factors[6] = 1.2;
            //factors[7] = 1.2;
            //factors[8] = 1.2;
            //factors[23] = 1.3;
            //factors[24] = 1.3;
            //factors[25] = 1.3;
            //factors[26] = 1.3;
            //factors[27] = 1.3;
            //factors[28] = 1.3;
            //factors[29] = 1.3;
            //factors[30] = 1.3;
            //factors[31] = 1.3;
            //factors[32] = 1.3;
            //factors[33] = 1.3;
            //factors[43] = 1.2;
            //factors[44] = 1.2;
            //factors[45] = 1.2;
            //factors[46] = 1.2;
            //factors[47] = 1.3;
            //factors[48] = 1.3;
            //factors[49] = 1.3;
            //factors[50] = 1.3;
            //factors[51] = 1.3;

        }

        static void SetHourlyFactors(double[] factors)
        {
            // TODO: get this from a profile xml file

            // HOURLY TYPICAL RESIDENTIAL ELECTRIC
            factors[0] = 0.4;
            factors[1] = 0.4;
            factors[2] = 0.4;
            factors[3] = 0.4;
            factors[4] = 0.7;
            factors[5] = 1.1;
            factors[6] = 1.1;
            factors[7] = 1.0;
            factors[8] = 0.8;
            factors[9] = 0.9;
            factors[10] = 1.0;
            factors[11] = 1.0;
            factors[12] = 0.9;
            factors[13] = 0.8;
            factors[14] = 1.0;
            factors[15] = 1.1;
            factors[16] = 1.2;
            factors[17] = 1.25;
            factors[18] = 1.40;
            factors[19] = 1.2;
            factors[20] = 0.6;
            factors[21] = 0.5;
            factors[22] = 0.4;
            factors[23] = 0.4;


            

         }

        static void SetGrowthFactors(GrowthFactorList factors)
        {
            factors.Add(new ACEGrowthFactor(new DateTime(2016, 1, 1), new DateTime(2016, 1, 8), 0.1,ACEGrowthPeriod.Daily));
        }
    }
}
