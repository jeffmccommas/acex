﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace AceSimDataGenerator
{
   
    public class ACEAmiDocument
    {
        public List<ACEAmiRow> ACERows { get; set; }
        public ACEProfile Profile { get; set; }
        public string FilePath { get; set; }

        public ACEAmiDocument(List<ACEAmiRow> aceRows, ACEProfile profile, string filePath)
        {
            ACERows = aceRows;
            Profile = profile;
            FilePath = filePath;
        }

        public void Write()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(Profile.AmiHeader.ToString());
            foreach(ACEAmiRow row in ACERows)
            {
                sb.AppendLine(row.ToString());
            }

            // WRITE IT TO DISK
            // TODO: Some handling to warn if path is going to be overwritten. 
            // ERROR handling in the caller
            System.IO.File.WriteAllText(FilePath, sb.ToString());
            
        }

    }
}
