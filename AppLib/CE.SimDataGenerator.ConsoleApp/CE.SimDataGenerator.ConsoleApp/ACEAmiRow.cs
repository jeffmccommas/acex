﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceSimDataGenerator
{
    public class ACEAmiRow
    {
        private int _intervalTime;
        private DateTime _date;
        private ACEProfile _profile;
        // other individual cells

        private string[] _readings;

        public ACEAmiRow(DateTime date, ACEProfile profile)
        {
            _profile = profile;
            ACEReportType type = profile.AmiHeader.ReportType; 
            _date = date;
            int intervals = 0;
            
            switch (type)
            {
                case ACEReportType.Daily:
                    intervals = 1;
                    _intervalTime = 24 * 60;
                    break;
                case ACEReportType.Hourly:
                    intervals = 24;
                    _intervalTime = 60;
                    break;
                case ACEReportType.ThirtyMin:
                    intervals = 48;
                    _intervalTime = 30;
                    break;
                case ACEReportType.FifteenMin:
                    intervals = 96;
                    _intervalTime = 15;
                    break;

            }
            _readings = new string[intervals];

        }

        public int IntervalTime
        {
            get { return _intervalTime; }
        }

        public DateTime Date
        {
            get
            {
                return _date;
            }
        }

        public string[] Readings
        {
            get { return _readings; }
            set { _readings = value; }
        }

        public void SetReading(int hour, string reading)
        {
            _readings[hour] = reading;
        }

        public string ClientId { get; set; }
        public string AccountNumber { get; set; }
        public string MeterId { get; set; }
        public string ServicePointId { get; set; }
        public string CommodityId { get; set; }
        public string UOMId { get; set; }
        public string VolumeFactor { get; set; }
        public string Direction { get; set; }
        public string ProjectedReadDate { get; set; }
        public string TimeStamp { get; set; }
        public string Timezone { get; set; }

        public override string ToString()
        {
            string del = _profile.AmiHeader.Delimiter.ToString(); // USE THE delimiter established in the header

            StringBuilder sb = new StringBuilder();
            sb.Append(_profile.ClientId + del);//sb.Append(_profile.Header. + _del); // CLIENT ID???
            sb.Append(_profile.AmiHeader.MeterId + del);//sb.Append(_profile.Header.MeterId + _del); // MeterId????
            sb.Append(_profile.AmiHeader.AccountNumber + del);        
            sb.Append(_profile.ServicePointId + del);
            sb.Append((int)_profile.AmiHeader.Commodity + del);
            sb.Append((int)_profile.AmiHeader.UOMId + del);
            string volFactor = string.Empty;
            if(_profile.AmiHeader.VolumeFactor != 0)
            {
                volFactor = _profile.AmiHeader.ToString();
            } 
            sb.Append(volFactor + del);
            if (_profile.AmiHeader.Direction != null)
            {
                sb.Append(((int) _profile.AmiHeader.Direction).ToString() + del);
            }
            else
            {
                sb.Append(string.Empty + del);
            }

            string projReadDate = string.Empty;
            if(_profile.AmiHeader.ProjectedReadDate != null)
            {
                projReadDate = _profile.AmiHeader.ProjectedReadDate.ToString();
            }
            sb.Append(projReadDate + del);
            sb.Append(_date.ToString("M/d/yyyy") + del);
            sb.Append(_profile.AmiHeader.TimeZone + del);
            sb.Append(String.Join(del.ToString(), Readings));

            return sb.ToString();
        }
        
    }
}
