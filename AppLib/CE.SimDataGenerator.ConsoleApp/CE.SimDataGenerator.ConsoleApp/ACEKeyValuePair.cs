﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceSimDataGenerator
{
    [Serializable]
    public class ACEKeyValuePair<TKey, TValue>
    {
        #region Properties
        public TKey Key { get; set; }
        public TValue Value { get; set; }
        #endregion

        #region Constructors
        public ACEKeyValuePair()
        {
        }

        public ACEKeyValuePair(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }
        #endregion


    }
}
