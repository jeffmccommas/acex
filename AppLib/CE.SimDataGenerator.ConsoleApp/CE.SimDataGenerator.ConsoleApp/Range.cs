﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceSimDataGenerator
{
    class Range
    {
        public double Low { get; set; }
        public double High { get; set; }
        public double Dif
        {
            get => High - Low; 
        }
        public Range()
        {

        }
        public Range(double low, double high)
        {
            Low = low;
            High = high;
        }
    }
}
