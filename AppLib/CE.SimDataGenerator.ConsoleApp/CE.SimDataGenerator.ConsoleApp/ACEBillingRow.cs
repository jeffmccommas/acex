﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceSimDataGenerator
{
    public class ACEBillingRow
    {
        public string [] BillingFields { get; set; }
        

        public ACEBillingRow(int size)
        {
            BillingFields = new string[size];
        }

    }
}
