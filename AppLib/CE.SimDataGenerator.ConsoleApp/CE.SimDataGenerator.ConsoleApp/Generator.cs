﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceSimDataGenerator
{
    class Generator
    {
        private readonly Random _r;
        private readonly ACEProfile _profile;
        private readonly ACEAmiDocument _aceAmiDoc;
        private double _billingTotal;
        private readonly ACEBillingDocument _aceBillingDoc;
        private readonly Dictionary<DateTime, int> _weatherData;
        private readonly Logger _logger;


        public Generator(ACEProfile profile, string outputPath, Logger logger)
        {
            _logger = logger;
            _r = new Random();
            _profile = profile;
            _weatherData = GetWeatherData(_profile.BillingHeader.ServiceZipCode, _profile.StartDate.ToString("yyyy-MM-dd"), _profile.EndDate.ToString("yyyy-MM-dd"));

            // use top-level client id so we don't have to think about it anywhere else
            _profile.AmiHeader.ClientId = profile.ClientId;
            _profile.BillingHeader.ClientId = profile.ClientId;

            // construct a billing doc using values passed in with the profile
            var utilityLabel = string.Empty;
            switch (_profile.AmiHeader.Commodity)
            {
                case ACECommodityId.Electric:
                    utilityLabel = "E";
                    break;
                case ACECommodityId.Gas:
                    utilityLabel = "G";
                    break;
                case ACECommodityId.Water:
                    utilityLabel = "W";
                    break;
                default:
                    utilityLabel = "Other";
                    break;
            }

            var reportLabel = string.Empty;
            switch (_profile.AmiHeader.ReportType)
            {
                case ACEReportType.FifteenMin:
                    reportLabel = "AMI_15min";
                    break;
                case ACEReportType.ThirtyMin:
                    reportLabel = "AMI_30min";
                    break;
                case ACEReportType.Hourly:
                    reportLabel = "AMI_60min";
                    break;
                default:
                    reportLabel = "AMI_Other";
                    break;
            }

            // Construct Billing Doc
            string billingFileName = System.IO.Path.Combine(outputPath, "simBill_client_" + _profile.ClientId + "_" +_profile.AmiHeader.AccountNumber+ "_"+ _profile.StartDate.ToString("yyyy_MM_dd") + "__" + _profile.EndDate.ToString("yyyy_MM_dd") + "_" + utilityLabel + ".txt");
            _aceBillingDoc = new ACEBillingDocument(_profile, billingFileName);

            // Construct AMI Doc
            string amiFileName = System.IO.Path.Combine(outputPath, "sim_client_" + _profile.ClientId + "_"  + reportLabel + "_" + _profile.AmiHeader.AccountNumber + "_" +  _profile.StartDate.ToString("yyyy_MM_dd") + "__" + _profile.EndDate.ToString("yyyy_MM_dd") + "_" + utilityLabel + ".csv");
            _aceAmiDoc = new ACEAmiDocument(new List<ACEAmiRow>(), _profile, amiFileName); //this needs configured elsewhere

            

        }

        public void MakeReadings()
        {

            DateTime currentDay = _profile.StartDate;
            int billingDayCount = 1;
            // lookup entries in _billingTotalList with identical servicepointIds and last billed dates that occur in the past
            // if an entry is found, set _billingTotal value based on it 
            
            while (currentDay < _profile.EndDate.AddDays(1))
            {
                _aceAmiDoc.ACERows.Add(MakeReadingsRow(currentDay));

                bool isBillDay = _profile.BillingHeader.BillDates.CheckIfBillDate(currentDay); // we need some sort of error when we run out of billing days
                if (isBillDay)
                {  
                    AddBillingLine(currentDay, _billingTotal, billingDayCount);

                    // reset to start a new billing period
                    _billingTotal = 0;
                    billingDayCount = 0;
                }
                currentDay = currentDay.AddDays(1);
                billingDayCount++;
            }
            //if(billingDayCount > 1)
            //{
            //    // There are days that have not had billing data generated. Write total consumption to a file for use later. 
            //    // TODO: LOG TOTAL SINCE LAST BILL DAY. startdate: currentday.AddDays(billingdayCount -1), enddate: Currentday, total: _billingTotal
            //}


            try
            {
                _aceAmiDoc.Write();
                _logger.WriteToLog("Generated AMI file. Wrote " + _aceAmiDoc.ACERows.Count + " line(s) to " + _aceAmiDoc.FilePath);

                if (_aceBillingDoc.BillingRows.Count > 0)
                {
                    _aceBillingDoc.Write();
                    _logger.WriteToLog("Generated billing file. Wrote " + _aceBillingDoc.BillingRows.Count + " line(s) to " + _aceBillingDoc.FilePath);
                }
            } catch (Exception ex)
            {
                _logger.WriteToLog("***************** ERROR *********** " + ex.ToString());
            }
            

        }

        private void AddBillingLine(DateTime billEnd, double consumption, int billDays)
        {
            double rate = _profile.BillingHeader.Rate;
            double tax_rate = _profile.BillingHeader.TaxRate; // NOT USED AS OF 10/19/2017 bkj
            double charge = consumption * rate;
            double tax = charge * tax_rate; // NOT USED AS OF 10/19/2017 bkj
            string meterUnits = string.Empty;
            switch(_profile.AmiHeader.UOMId)
            {
                case ACEUOMId.kwh:
                    meterUnits = "1";
                    break;
                case ACEUOMId.gal:
                    meterUnits = "2";
                    break;
                case ACEUOMId.ccf:
                    meterUnits = "3";
                    break;
            }
            string serviceCommodity = string.Empty;
            switch (_profile.AmiHeader.Commodity)
            {
                case ACECommodityId.Electric:
                    serviceCommodity = "1";
                    break;
                case ACECommodityId.Gas:
                    serviceCommodity = "2";
                    break;
                case ACECommodityId.Water:
                    serviceCommodity = "3";
                    break;
            }


            List<KeyValuePair<string, string>> billingData = new List<KeyValuePair<string, string>>();
            billingData.Add(new KeyValuePair<string, string>("usage_value", consumption.ToString()));
            //billingData.Add(new KeyValuePair<string, string>("bill_enddate", billEnd.ToString("yyyyMMdd")));
            billingData.Add(new KeyValuePair<string, string>("bill_date", billEnd.ToString("yyyyMMdd")));
            //billingData.Add(new KeyValuePair<string, string>("bill_days", billDays.ToString()));
            billingData.Add(new KeyValuePair<string, string>("bill_period_days", billDays.ToString()));
            billingData.Add(new KeyValuePair<string, string>("usage_charge", charge.ToString())); // hardcoded for electric. TODO: SET RATE PER UTILITY ON BILLING HEADER
            //billingData.Add(new KeyValuePair<string, string>("LastPaymentDate", billEnd.AddDays(-29).ToString("M/d/yyyy")));
            //billingData.Add(new KeyValuePair<string, string>("DueDate", billEnd.AddDays(3).ToString("M/d/yyyy")));
            //billingData.Add(new KeyValuePair<string, string>("NextReaddate", _profile.BillingHeader.BillDates.GetNextReadDate(billEnd).ToString("M/d/yyyy")));
            //billingData.Add(new KeyValuePair<string, string>("MeterDescription", _profile.AmiHeader.Commodity.ToString("g")));
            //billingData.Add(new KeyValuePair<string, string>("TOTAL_UOM", _profile.AmiHeader.UOMId.ToString("g")));
            //billingData.Add(new KeyValuePair<string, string>("GOVT_TAX_MUNI", tax.ToString()));
            //billingData.Add(new KeyValuePair<string, string>("bill_taxes", tax.ToString()));
            billingData.Add(new KeyValuePair<string, string>("meter_units", meterUnits));
            billingData.Add(new KeyValuePair<string, string>("service_commodity", serviceCommodity));
            //billingData.Add(new KeyValuePair<string, string>("service_read_date", billEnd.ToString("yyyMMdd")));
            billingData.Add(new KeyValuePair<string, string>("service_read_enddate", billEnd.ToString("yyyMMdd")));
            DateTime serviceReadStartDate = billEnd.AddDays(billDays * -1);
            //billingData.Add(new KeyValuePair<string, string>("service_read_startdate", serviceReadStartDate.ToString("yyyMMdd")));
            billingData.Add((new KeyValuePair<string, string>("premise_id",_profile.PremiseId)));
            billingData.Add((new KeyValuePair<string, string>("client_id", _profile.ClientId.ToString())));



            _aceBillingDoc.AddBillingRow(billingData);

        }

        private ACEAmiRow MakeReadingsRow(DateTime date)
        {
            ACEAmiRow row = new ACEAmiRow(date, _profile);
            
            
            for(int interval=0; interval < row.Readings.Length; interval++)
            {
                //try
                //{
                    int hour = row.Date.AddMinutes(row.IntervalTime * interval).Hour;
                    Range range = GetRange(date, hour, (int) _profile.AmiHeader.Commodity,
                        _profile.BillingHeader.ServiceZipCode);
                    var dif = range.Dif;
                    var low = range.Low;
                    var nextRandom = _r.NextDouble();

                    double readValue = (nextRandom * dif) + low;
                    if (Double.IsNaN(readValue))
                    {
                        throw new Exception("Random number generated NaN. NextRandom =" + nextRandom + ", low =" + low +
                                            ", dif=" + dif);
                    }
                    _billingTotal = _billingTotal + readValue;
                    row.SetReading(interval, readValue.ToString("N3"));
                //}
                //catch (Exception ex)
                //{
                //    _logger.WriteToLog("***************** ERROR *********** " + ex.ToString());
                //}
            }
            return row;
        }
        
        private Range GetRange(DateTime date, int hour, int commodityType, string zipCode)
        {

            // TODO: move all of this logic into the Range object?
            // Adjust based on weather
            int dailyTemp = _weatherData[date];
            int coldWxThreshhold = _profile.ColdWeatherTempThreshhold;
            int hotWxThreshhold = _profile.HotWeatherTempThreshhold;
            int coldWxDenominator = _profile.ColdWeatherFactorDenominator;
            int hotWxDenominator = _profile.HotWeatherFactorDenominator;

            if (hotWxDenominator == 0 || coldWxDenominator == 0)
            {
                throw new Exception("Invalid weather denominator values. Check XML profile.");
            }

            double adjustmentFactor = 0.0;
            // hot: adjust up water and electric
            if(dailyTemp > hotWxThreshhold && (_profile.AmiHeader.Commodity == ACECommodityId.Electric || _profile.AmiHeader.Commodity == ACECommodityId.Water))
            {
                double tempDif = dailyTemp - hotWxThreshhold;
                adjustmentFactor = adjustmentFactor + (tempDif / hotWxDenominator);
                
            } else if (dailyTemp < coldWxThreshhold && (_profile.AmiHeader.Commodity == ACECommodityId.Electric || _profile.AmiHeader.Commodity == ACECommodityId.Gas))
            // cold: adjust up gas and electric
            {
                double tempDif = coldWxThreshhold - dailyTemp;
                adjustmentFactor = adjustmentFactor + (tempDif / coldWxDenominator);
                
            }

            // Adjust based on week
            int weekNumber = GetWeekOfYear(date);
            double weekFactor = _profile.WeekOfYearFactors[weekNumber];
            adjustmentFactor = adjustmentFactor + weekFactor;
                


            // Adjust based on day of week
            double dayOfWeekFactor = _profile.DayOfWeekFactors[(int)date.DayOfWeek];
            adjustmentFactor = adjustmentFactor + dayOfWeekFactor;
               

            // Adjust based on hour
            double hourFactor = _profile.HourlyFactors[hour];
            adjustmentFactor = adjustmentFactor + hourFactor;
                


            // Adjust growth factor
            foreach(var gf in _profile.GrowthFactors)
            {
 
                if(date >= gf.GrowthStart && date <= gf.GrowthEnd)
                {
                    var periods = 0;
                    var gfTime = date.AddHours(hour);
                    switch( gf.GrowthPeriod)
                    {
                        case ACEGrowthPeriod.Daily:
                            periods = (int)(date - gf.GrowthStart).TotalDays + 1;
                            break;
                        case ACEGrowthPeriod.Hourly:
                            periods = (int)(date - gf.GrowthStart).TotalHours + 1;
                            break;
                        case ACEGrowthPeriod.Monthly:
                            periods = (int)((date - gf.GrowthStart).TotalDays / 30) + 1;
                            break;
                        case ACEGrowthPeriod.Yearly:
                            periods = (int)((date - gf.GrowthStart).TotalDays / 365) + 1;
                            break;
                        default:
                            periods = 1;
                            break;
                    }
                    adjustmentFactor = adjustmentFactor + gf.GrowthFactor * periods;
                    
                }
            }
            var baseDif = _profile.BaseHighRange - _profile.BaseLowRange;
            var lowUsage = _profile.BaseLowRange * (1+adjustmentFactor);
            var highUsage = lowUsage + baseDif;
            //var highUsage = _profile.BaseHighRange * (1+adjustmentFactor);
            var range = new Range(lowUsage, highUsage);
            //Console.WriteLine("lowUsage=" +lowUsage+", highUsage = " +highUsage+", dif=" + range.Dif);
            return range;
        }

        private int GetWeekOfYear(DateTime date)
        {
            var dfi = DateTimeFormatInfo.CurrentInfo;
            var cal = dfi.Calendar;
            return cal.GetWeekOfYear(date, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
        }


       
        private Dictionary<DateTime,int> GetWeatherData(string zip, string startdate, string enddate)
        {
            Dictionary<DateTime, int> weatherData = new Dictionary<DateTime, int>();
            var connString = System.Configuration.ConfigurationManager.ConnectionStrings["ACEdb"].ConnectionString;
            string query = "select WeatherReadingDate, AvgTemp from cm.EMDailyWeatherDetail w inner join cm.EMZipcode z on z.stationIDDaily = w.StationID where z.zipcode = '" + zip;
            query = query + "' and WeatherReadingDate between '" + startdate + "' and '" + enddate + "'";
            
            using (SqlConnection conn = new SqlConnection(connString))
            {
                
                var cmd = new SqlCommand(query, conn);

                try
                {
                    conn.Open();
                    SqlDataReader localAvgTempReader = cmd.ExecuteReader();
                    while (localAvgTempReader.Read())
                    {
                        DateTime weatherDate = Convert.ToDateTime(localAvgTempReader[0].ToString());
                        int temp = (int)localAvgTempReader[1];
                        weatherData.Add(weatherDate, temp);
                    }

                }
                catch (Exception ex)
                {
                    _logger.WriteToLog( "************** Error ************** " + ex.ToString());
                }
            }
            return weatherData;
        }

    }
}
