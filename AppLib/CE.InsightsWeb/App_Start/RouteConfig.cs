﻿using System.Web.Mvc;
using System.Web.Routing;

namespace CE.InsightsWeb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            // Attribute routing.
            //routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Authorize",
                url: "oauth/authorize/{id}",
                defaults: new { controller = "Authorize", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
