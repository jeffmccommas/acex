﻿import { browser, by, element } from 'protractor/built';

describe('test website', function () {
    beforeEach(function () {
        browser.waitForAngularEnabled(false);
        browser.driver.get('https://acewebsiteuat.aclarax.com').then(function () {
            element(by.id('txtUserName')).sendKeys('qatest');
            element(by.id('txtPassword')).sendKeys("qa@345R");
            element(by.id('btnLogin')).click();
            browser.sleep(2000);
        });
    });

    it('Detect title on website', function () {
        expect(browser.driver.getTitle()).toEqual('Insights');
    });
}); 