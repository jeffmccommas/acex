"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
describe('test website', function () {
    beforeEach(function () {
        built_1.browser.waitForAngularEnabled(false);
        built_1.browser.driver.get('https://acewebsiteuat.aclarax.com').then(function () {
            built_1.element(built_1.by.id('txtUserName')).sendKeys('qatest');
            built_1.element(built_1.by.id('txtPassword')).sendKeys("qa@345R");
            built_1.element(built_1.by.id('btnLogin')).click();
            built_1.browser.sleep(2000);
        });
    });
    it('Detect title on website', function () {
        expect(built_1.browser.driver.getTitle()).toEqual('Insights');
    });
});
//# sourceMappingURL=testbrowsstack.js.map