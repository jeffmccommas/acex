"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator_1 = require("../e2e-helpers/TokenGenerator");
var BillSummaryPageObject = (function () {
    function BillSummaryPageObject() {
    }
    BillSummaryPageObject.prototype.navigateTo = function (pageID, custID, clientID, accID, premID) {
        var billSummaryUrl = new TokenGenerator_1.TokenGenerator();
        var token = billSummaryUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Page?id=' + pageID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            console.log('url->' + built_1.browser.baseUrl + 'Page?id=' + pageID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            built_1.browser.sleep(5000);
        });
    };
    BillSummaryPageObject.prototype.getclientData = function () {
        var json = require('../TestData/BillSummary/clientDetails.json');
        return json;
    };
    BillSummaryPageObject.prototype.getGasUseElem = function () {
        built_1.browser.sleep(1000);
        var gasUseElement = built_1.element(built_1.by.cssContainingText('.col-xs-6', 'Gas Use So Far'));
        return gasUseElement;
    };
    BillSummaryPageObject.prototype.getElecUseElem = function () {
        built_1.browser.sleep(2000);
        var electricUseElement = built_1.element(built_1.by.cssContainingText('.col-xs-6', 'Electric Use So Far'));
        ;
        return electricUseElement;
    };
    BillSummaryPageObject.prototype.getWaterUseElem = function () {
        built_1.browser.sleep(1000);
        var waterUseElement = built_1.element(built_1.by.cssContainingText('.col-xs-6', 'Water Use So Far'));
        return waterUseElement;
    };
    BillSummaryPageObject.prototype.getBTDElem = function () {
        built_1.browser.sleep(1000);
        var BTDElement = built_1.element(built_1.by.cssContainingText('.col-xs-6', 'Your Bill To Date'));
        return BTDElement;
    };
    BillSummaryPageObject.prototype.getADCElem = function () {
        built_1.browser.sleep(1000);
        var ADCElement = built_1.element(built_1.by.cssContainingText('.col-xs-7', 'Average Daily Cost'));
        return ADCElement;
    };
    BillSummaryPageObject.prototype.getNoBillsMsg = function () {
        built_1.browser.sleep(2000);
        var NoBillsMsg = built_1.element(built_1.by.cssContainingText('.alert.alert-warning', 'No bills are available.'));
        return NoBillsMsg;
    };
    BillSummaryPageObject.prototype.getYesDueDate = function () {
        built_1.browser.sleep(2000);
        var YesDueDate = built_1.element(built_1.by.cssContainingText('strong', 'Amount Due:'));
        return YesDueDate;
    };
    BillSummaryPageObject.prototype.getNoDueDate = function () {
        built_1.browser.sleep(2000);
        var NoDueDate = built_1.element(built_1.by.cssContainingText('strong', 'Amount Due:'));
        return NoDueDate;
    };
    BillSummaryPageObject.prototype.getElectOnly = function () {
        built_1.browser.sleep(1000);
        var electricUseElement = built_1.element(built_1.by.cssContainingText('.col-xs-6', 'Total Electricity Used'));
        return electricUseElement;
    };
    BillSummaryPageObject.prototype.getGasOnly = function () {
        built_1.browser.sleep(1000);
        var gasUseElement = built_1.element(built_1.by.cssContainingText('.col-xs-6', 'Total Gas Used'));
        return gasUseElement;
    };
    BillSummaryPageObject.prototype.getBillHistory = function () {
        built_1.browser.sleep(2000);
        var BillHistory = built_1.element(built_1.by.id('iws_rc_pagewidget_4'));
        return BillHistory;
    };
    BillSummaryPageObject.prototype.getToggleBtn = function () {
        built_1.browser.sleep(1000);
        var toggle = built_1.element(built_1.by.className('panel-heading account-select__heading')).element(built_1.by.className('xs-pl-0 toggle__btn toggle__drk'));
        var Btn = toggle.element(built_1.by.className('btn btn-default xs-ml-10'));
        built_1.browser.actions().mouseMove(Btn).click().perform();
        return Btn;
    };
    BillSummaryPageObject.prototype.getFirstPremise = function () {
        built_1.browser.sleep(1000);
        var firstPremise = built_1.element(built_1.by.id('PageWidget_3')).all(built_1.by.className('panel-body premises')).get(0);
        var radioBtn = firstPremise.all(built_1.by.tagName('input'));
        return radioBtn;
    };
    BillSummaryPageObject.prototype.getSecondPremise = function () {
        built_1.browser.sleep(1000);
        var secondPremise = built_1.element(built_1.by.id('PageWidget_3')).all(built_1.by.className('panel-body premises')).get(1);
        var radioBtn = secondPremise.all(built_1.by.tagName('input'));
        return radioBtn;
    };
    BillSummaryPageObject.prototype.getGasUsage = function () {
        var list = built_1.element(built_1.by.className('bill-statement list-group')).all(built_1.by.tagName('li')).get(0);
        var gasUsage = list.element(built_1.by.tagName('p'));
        return gasUsage;
    };
    BillSummaryPageObject.prototype.getGasCost = function () {
        var list = built_1.element(built_1.by.className('bill-statement list-group')).all(built_1.by.tagName('li')).get(4);
        var gasCost = list.element(built_1.by.tagName('p'));
        return gasCost;
    };
    return BillSummaryPageObject;
}());
exports.BillSummaryPageObject = BillSummaryPageObject;
//# sourceMappingURL=BillSummary.po.js.map