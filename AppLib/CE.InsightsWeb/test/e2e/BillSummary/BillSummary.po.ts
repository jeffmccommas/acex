﻿import { browser, by, element } from 'protractor/built';
import { TokenGenerator } from '../e2e-helpers/TokenGenerator';

export class BillSummaryPageObject {


    navigateTo(pageID, custID, clientID, accID, premID) {

        let billSummaryUrl = new TokenGenerator();

        let token = billSummaryUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);
                //browser.waitForAngularEnabled(false);

                browser.get(browser.baseUrl + 'Page?id=' + pageID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
                console.log('url->' + browser.baseUrl + 'Page?id=' + pageID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
                browser.sleep(5000);
            });
    }

    getclientData() {
        var json = require('../TestData/BillSummary/clientDetails.json');
        return json;
    }

    getGasUseElem() {

        browser.sleep(1000);
        var gasUseElement = element(by.cssContainingText('.col-xs-6', 'Gas Use So Far'));
        return gasUseElement;

    }

    getElecUseElem() {

        browser.sleep(2000);
        var electricUseElement = element(by.cssContainingText('.col-xs-6', 'Electric Use So Far'));;
        return electricUseElement;

    }

    getWaterUseElem() {

        browser.sleep(1000);
        var waterUseElement = element(by.cssContainingText('.col-xs-6', 'Water Use So Far'));
        return waterUseElement;

    }

    getBTDElem() {

        browser.sleep(1000);
        var BTDElement = element(by.cssContainingText('.col-xs-6', 'Your Bill To Date'));
        return BTDElement;

    }

    getADCElem() {

        browser.sleep(1000);
        var ADCElement = element(by.cssContainingText('.col-xs-7', 'Average Daily Cost'));
        return ADCElement;

    }

    getNoBillsMsg() {

        browser.sleep(2000);
        var NoBillsMsg = element(by.cssContainingText('.alert.alert-warning', 'No bills are available.'));
        return NoBillsMsg;

    }

    getYesDueDate() {

        browser.sleep(2000);
        var YesDueDate = element(by.cssContainingText('strong', 'Amount Due:'));
        return YesDueDate;

    }

    getNoDueDate() {
        
        browser.sleep(2000);
        var NoDueDate = element(by.cssContainingText('strong', 'Amount Due:'));

        return NoDueDate;

    }

    getElectOnly() {

        browser.sleep(1000);
        var electricUseElement = element(by.cssContainingText('.col-xs-6', 'Total Electricity Used'));
        return electricUseElement;

    }

    getGasOnly() {
        
        browser.sleep(1000);
        var gasUseElement = element(by.cssContainingText('.col-xs-6', 'Total Gas Used'));
        return gasUseElement;

    }

    getBillHistory() {

        browser.sleep(2000);
        var BillHistory = element(by.id('iws_rc_pagewidget_4'));
        return BillHistory;

    }

    getToggleBtn() {
        browser.sleep(1000);
        var toggle = element(by.className('panel-heading account-select__heading')).element(by.className('xs-pl-0 toggle__btn toggle__drk'));
        var Btn = toggle.element(by.className('btn btn-default xs-ml-10'));
        browser.actions().mouseMove(Btn).click().perform();
        return Btn;

    }

    getFirstPremise() {
        browser.sleep(1000);
        var firstPremise = element(by.id('PageWidget_3')).all(by.className('panel-body premises')).get(0);
        var radioBtn = firstPremise.all(by.tagName('input'));
        return radioBtn;

    }

    getSecondPremise() {
        browser.sleep(1000);
        var secondPremise = element(by.id('PageWidget_3')).all(by.className('panel-body premises')).get(1);
        var radioBtn = secondPremise.all(by.tagName('input'));
        return radioBtn;

    }


    getGasUsage() {
        var list = element(by.className('bill-statement list-group')).all(by.tagName('li')).get(0);
        var gasUsage = list.element(by.tagName('p'));
        return gasUsage;

    }

    getGasCost() {
        var list = element(by.className('bill-statement list-group')).all(by.tagName('li')).get(4);
        var gasCost = list.element(by.tagName('p'));
        return gasCost;
    }

}