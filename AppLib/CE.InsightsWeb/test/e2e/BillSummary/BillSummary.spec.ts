﻿'Use strict';

import { BillSummaryPageObject } from './BillSummary.po';

describe('BillSummary', () => {
    let page: BillSummaryPageObject;

    beforeEach(() => {
        page = new BillSummaryPageObject();
    });


       it('Bill Summary e2e Tests all 3 commodities', () => {

            var clientData = page.getclientData();
            page.navigateTo(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);

            var electricUseElement = page.getElecUseElem();
            expect(electricUseElement.getText()).toMatch('Electric Use So Far');

            var gasUseElement = page.getGasUseElem();
            expect(gasUseElement.getText()).toMatch('Gas Use So Far');

            var waterUseElement = page.getWaterUseElem();
            expect(waterUseElement.getText()).toMatch('Water Use So Far');

            var BTDElement = page.getBTDElem();
            expect(BTDElement.getText()).toMatch('Your Bill To Date');

            var ADCElement = page.getADCElem();
            expect(ADCElement.getText()).toMatch('Average Daily Cost');

        });

        it('Should display "No bills available" for customer with no bills', () => {

            var clientData = page.getclientData();
            page.navigateTo(clientData[1].pageID, clientData[1].custID, clientData[1].clientID, clientData[1].accID, clientData[1].premID);

            var NoBillsMsg = page.getNoBillsMsg();
            
            expect(NoBillsMsg.isDisplayed()).toBeTruthy();

        });

        it('Should display due date ', () => {

            var clientData = page.getclientData();
            page.navigateTo(clientData[2].pageID, clientData[2].custID, clientData[2].clientID, clientData[2].accID, clientData[2].premID);

            var YesDueDate = page.getYesDueDate();
            expect(YesDueDate.getText()).not.toEqual('Amount Due:');


        });

        it('Should show Electric Use Element in Bill Summary', () => {
            var clientData = page.getclientData();
            page.navigateTo(clientData[3].pageID, clientData[3].custID, clientData[3].clientID, clientData[3].accID, clientData[3].premID);

            var ElecOnly = page.getElectOnly();
            expect(ElecOnly.getText()).toMatch('Total Electricity Used');
        });

        it('Should show Gas Element in Bill Summary', () => {

            var clientData = page.getclientData();
            page.navigateTo(clientData[4].pageID, clientData[4].custID, clientData[4].clientID, clientData[4].accID, clientData[4].premID);

            var gasUseElement = page.getGasOnly();
            expect(gasUseElement.getText()).toMatch('Total Gas Used');

        });

        it('Should Navigate to Bill history and display', () => {

            var clientData = page.getclientData();
            page.navigateTo(clientData[5].pageID, clientData[5].custID, clientData[5].clientID, clientData[5].accID, clientData[5].premID);

            var BillHistory = page.getBillHistory();
            
            expect(BillHistory.getAttribute('name')).toEqual('billhistory');
            expect(BillHistory.isDisplayed()).toBeTruthy();

        });

        it('Should not display due date ', () => {

            var clientData = page.getclientData();
            page.navigateTo(clientData[6].pageID, clientData[6].custID, clientData[6].clientID, clientData[6].accID, clientData[6].premID);
    
            var NoDueDate = page.getNoDueDate();

            expect(NoDueDate.getText()).toEqual('Amount Due:');

        });

       it('Verify Last bill  for gas commodity displayed on the billtodate widget by premise', function () {

        var clientData = page.getclientData();
        page.navigateTo(clientData[7].pageID, clientData[7].custID, clientData[7].clientID, clientData[7].accID, clientData[7].premID);

        browser.sleep(5000);
        page.getToggleBtn();
        page.getFirstPremise().click();

        var gasUsage = page.getGasUsage();
        expect(gasUsage.getText()).toEqual('550 Therms');
        var gasCost = page.getGasCost();
        expect(gasCost.getText()).toEqual('$182');

       });

       it('Verify gas commodity for non related premise does NOT display on the page', function () {
       
        page.getSecondPremise().click();
        browser.sleep(5000);

        var gasUsage = page.getGasUsage();
        expect(gasUsage.getText()).toEqual('551 Therms');
        expect(gasUsage.getText()).not.toEqual('1101 Therms');

        var gasCost = page.getGasCost();
        expect(gasCost.getText()).toEqual('$183');
        expect(gasCost.getText()).not.toEqual('$365');

       });

    });