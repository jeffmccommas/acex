﻿import { browser, by, element } from 'protractor/built';

export class LoginPageObject {
    navigateTo() {
        browser.waitForAngularEnabled(false);
        return browser.get(browser.baseUrl);
    }

    setupTestLoginForClient(clientId) {
        element(by.id('txtUserName')).sendKeys('qatest');
        element(by.id('txtPassword')).sendKeys('qa@345R');
        element(by.id('txtCustomer')).sendKeys('993774751');
        element(by.id('txtClient')).sendKeys(clientId);
        element(by.id('txtAccount')).sendKeys('1218337093');
        element(by.id('txtPremise')).sendKeys('1218337000');
        element(by.id("chkbEnableCSR")).click();

    }

    setupInvalidTestUser() {
        element(by.id('txtUserName')).sendKeys('xxxx');
        element(by.id('txtPassword')).sendKeys('xxxx');
        element(by.id('txtCustomer')).sendKeys('993774751');
        element(by.id('txtClient')).sendKeys('87');
        element(by.id('txtAccount')).sendKeys('1218337093');
        element(by.id('txtPremise')).sendKeys('1218337000');
        element(by.id("chkbEnableCSR")).click();

    }

    selectLogin() {
        element(by.id('btnLogin')).click();
    }

    getErrorMessage() {
        return element(by.id('lblError')).getText();
    }

}