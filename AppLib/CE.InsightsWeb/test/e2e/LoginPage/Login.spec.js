'Use strict';
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var Login_po_1 = require("./Login.po");
describe('Login', function () {
    var page;
    beforeEach(function () {
        page = new Login_po_1.LoginPageObject();
    });
    it('logs user in and navigate to new page', function () {
        page.navigateTo();
        page.setupTestLoginForClient('87');
        page.selectLogin();
        expect(built_1.browser.getTitle()).toEqual('Insights');
    });
    it('shows error message for invalid user', function () {
        page.navigateTo();
        page.setupInvalidTestUser();
        page.selectLogin();
        built_1.browser.sleep(1000);
        expect(page.getErrorMessage()).toEqual("Invalid user/password or this user doesn't have access");
    });
});
//# sourceMappingURL=Login.spec.js.map