"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var LoginPageObject = (function () {
    function LoginPageObject() {
    }
    LoginPageObject.prototype.navigateTo = function () {
        built_1.browser.waitForAngularEnabled(false);
        return built_1.browser.get(built_1.browser.baseUrl);
    };
    LoginPageObject.prototype.setupTestLoginForClient = function (clientId) {
        built_1.element(built_1.by.id('txtUserName')).sendKeys('qatest');
        built_1.element(built_1.by.id('txtPassword')).sendKeys('qa@345R');
        built_1.element(built_1.by.id('txtCustomer')).sendKeys('993774751');
        built_1.element(built_1.by.id('txtClient')).sendKeys(clientId);
        built_1.element(built_1.by.id('txtAccount')).sendKeys('1218337093');
        built_1.element(built_1.by.id('txtPremise')).sendKeys('1218337000');
        built_1.element(built_1.by.id("chkbEnableCSR")).click();
    };
    LoginPageObject.prototype.setupInvalidTestUser = function () {
        built_1.element(built_1.by.id('txtUserName')).sendKeys('xxxx');
        built_1.element(built_1.by.id('txtPassword')).sendKeys('xxxx');
        built_1.element(built_1.by.id('txtCustomer')).sendKeys('993774751');
        built_1.element(built_1.by.id('txtClient')).sendKeys('87');
        built_1.element(built_1.by.id('txtAccount')).sendKeys('1218337093');
        built_1.element(built_1.by.id('txtPremise')).sendKeys('1218337000');
        built_1.element(built_1.by.id("chkbEnableCSR")).click();
    };
    LoginPageObject.prototype.selectLogin = function () {
        built_1.element(built_1.by.id('btnLogin')).click();
    };
    LoginPageObject.prototype.getErrorMessage = function () {
        return built_1.element(built_1.by.id('lblError')).getText();
    };
    return LoginPageObject;
}());
exports.LoginPageObject = LoginPageObject;
//# sourceMappingURL=Login.po.js.map