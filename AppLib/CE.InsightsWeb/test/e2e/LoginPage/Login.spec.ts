﻿'Use strict';
import { browser} from 'protractor/built';
//import loginpage = require('../LoginPage/LoginPagePO');
import { LoginPageObject } from './Login.po';

describe('Login', () => {
    let page: LoginPageObject;

    beforeEach(() => {
        page = new LoginPageObject();
    });

    it('logs user in and navigate to new page', () => {

        page.navigateTo();
        page.setupTestLoginForClient('87');
        page.selectLogin();
        expect(browser.getTitle()).toEqual('Insights');
    });

    it('shows error message for invalid user', () => {

        page.navigateTo();
        page.setupInvalidTestUser();
        page.selectLogin();


        browser.sleep(1000);
        expect(page.getErrorMessage()).toEqual("Invalid user/password or this user doesn't have access");

        //browser.wait(protractorpage.getErrorLbl(),"Invalid user/ password or this user doesn't have access"), 5000);
        // expect(.getText).toMatch("Invalid user/ password or this user doesn't have access");

    });


});