﻿import { browser, by, element } from 'protractor/built';
import { TokenGenerator } from '../e2e-helpers/TokenGenerator';

export class ShortProfilePageObject {


    navigateToIframe(custID, clientID, accID, premID) {

        let shortProfileUrl = new TokenGenerator();

        let token = shortProfileUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);
                //browser.waitForAngularEnabled(false);
                browser.get(browser.baseUrl + 'Widget?id=tab.dashboard-profile&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true&enableRemote=true&clearProfileAttr=true');
                console.log('url->' + browser.baseUrl + 'Widget?id=tab.dashboard-profile&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true&enableRemote=true&clearProfileAttr=true');
            });
    }

    navigateTo(custID, clientID, accID, premID) {

        let shortProfileUrl = new TokenGenerator();

        let token = shortProfileUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);
                //browser.waitForAngularEnabled(false);
                browser.get(browser.baseUrl + 'Page?id=1&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true&clearProfileAttr=true');
                console.log('url->' + browser.baseUrl + 'Page?id=1&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true&clearProfileAttr=true');
            });
    }

    getPanelTitle() {
        browser.sleep(2000);
        var a = element(by.tagName('body'));
        return a.element(by.className('panel-title')).getText();
    }

    getPanelTitleNonIframe() {
        browser.sleep(2000);
        var y = element(by.id("iws_rc_pagewidget_5")).element(by.className("panel-heading"));
        return y.getText();
    }
    

}