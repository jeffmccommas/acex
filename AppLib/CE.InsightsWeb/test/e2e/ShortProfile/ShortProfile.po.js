"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator_1 = require("../e2e-helpers/TokenGenerator");
var ShortProfilePageObject = (function () {
    function ShortProfilePageObject() {
    }
    ShortProfilePageObject.prototype.navigateToIframe = function (custID, clientID, accID, premID) {
        var shortProfileUrl = new TokenGenerator_1.TokenGenerator();
        var token = shortProfileUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Widget?id=tab.dashboard-profile&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true&enableRemote=true&clearProfileAttr=true');
            console.log('url->' + built_1.browser.baseUrl + 'Widget?id=tab.dashboard-profile&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true&enableRemote=true&clearProfileAttr=true');
        });
    };
    ShortProfilePageObject.prototype.navigateTo = function (custID, clientID, accID, premID) {
        var shortProfileUrl = new TokenGenerator_1.TokenGenerator();
        var token = shortProfileUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Page?id=1&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true&clearProfileAttr=true');
            console.log('url->' + built_1.browser.baseUrl + 'Page?id=1&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true&clearProfileAttr=true');
        });
    };
    ShortProfilePageObject.prototype.getPanelTitle = function () {
        built_1.browser.sleep(2000);
        var a = built_1.element(built_1.by.tagName('body'));
        return a.element(built_1.by.className('panel-title')).getText();
    };
    ShortProfilePageObject.prototype.getPanelTitleNonIframe = function () {
        built_1.browser.sleep(2000);
        var y = built_1.element(built_1.by.id("iws_rc_pagewidget_5")).element(built_1.by.className("panel-heading"));
        return y.getText();
    };
    return ShortProfilePageObject;
}());
exports.ShortProfilePageObject = ShortProfilePageObject;
//# sourceMappingURL=ShortProfile.po.js.map