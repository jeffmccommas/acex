﻿'Use strict';

import { ShortProfilePageObject } from './ShortProfile.po';

describe('ShortProfile', () => {
    let page: ShortProfilePageObject;

    beforeEach(() => {
        page = new ShortProfilePageObject();
    });

    it('Iframe Client 87', () => {
        var custID = '993774751';
        var clientID = '87';
        var accID = '1218337093';
        var premID = '1218337000';

        page.navigateToIframe(custID, clientID, accID, premID);
        var paneltitle = page.getPanelTitle();
        expect(paneltitle).toEqual('Energy Profile');
    });

    it('Iframe Client 276', () => {
        var custID = '993774751';
        var clientID = '276';
        var accID = '1218337093';
        var premID = '1218337000';

        page.navigateToIframe(custID, clientID, accID, premID);
        var paneltitle = page.getPanelTitle();
        expect(paneltitle).toEqual('Energy Profile');
    });

    it('Non Iframe Client 87', () => {
        var custID = '993774751';
        var clientID = '87';
        var accID = '1218337093';
        var premID = '1218337000';

        page.navigateTo(custID, clientID, accID, premID);
        var paneltitle = page.getPanelTitleNonIframe();
        expect(paneltitle).toEqual('Energy Profile');
    });

});

