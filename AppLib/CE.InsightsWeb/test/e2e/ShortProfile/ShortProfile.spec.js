'Use strict';
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ShortProfile_po_1 = require("./ShortProfile.po");
describe('ShortProfile', function () {
    var page;
    beforeEach(function () {
        page = new ShortProfile_po_1.ShortProfilePageObject();
    });
    it('Iframe Client 87', function () {
        var custID = '993774751';
        var clientID = '87';
        var accID = '1218337093';
        var premID = '1218337000';
        page.navigateToIframe(custID, clientID, accID, premID);
        var paneltitle = page.getPanelTitle();
        expect(paneltitle).toEqual('Energy Profile');
    });
    it('Iframe Client 276', function () {
        var custID = '993774751';
        var clientID = '276';
        var accID = '1218337093';
        var premID = '1218337000';
        page.navigateToIframe(custID, clientID, accID, premID);
        var paneltitle = page.getPanelTitle();
        expect(paneltitle).toEqual('Energy Profile');
    });
    it('Non Iframe Client 87', function () {
        var custID = '993774751';
        var clientID = '87';
        var accID = '1218337093';
        var premID = '1218337000';
        page.navigateTo(custID, clientID, accID, premID);
        var paneltitle = page.getPanelTitleNonIframe();
        expect(paneltitle).toEqual('Energy Profile');
    });
});
//# sourceMappingURL=ShortProfile.spec.js.map