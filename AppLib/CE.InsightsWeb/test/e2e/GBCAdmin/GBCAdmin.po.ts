﻿import { browser, by, element } from 'protractor/built';
import { TokenGenerator } from '../e2e-helpers/TokenGenerator';

export class GBCAdminPageObject {


    navigateTo(custID, clientID, accID, premID) {

        let gbcadminUrl = new TokenGenerator();

        let token = gbcadminUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);
                //browser.waitForAngularEnabled(false);

                (browser.manage() as any).addCookie({ name: 'GBC', value: '87:True' });
                browser.get(browser.baseUrl + 'GreenButtonConnectAdmin');
                console.log('url->' + browser.baseUrl);
                browser.sleep(8000);
            });
    }

    getThirdPartyAppAddForm() {
        element(by.className('btn btn-success')).click();
        var form = element(by.className('panel panel-info'));
        return form;
    }

    //getDataCustodianApplicationStatus() {
    //    browser.sleep(1000);
    //    var dataCustodianApplicationStatus = element(by.tagName('form')).all(by.className('form-control')).get(0);
    //    dataCustodianApplicationStatus.all(by.tagName('option')).get(0).click();
    //    return dataCustodianApplicationStatus;
    //}

    getDataCustodianApplicationStatusRevoked() {
        browser.sleep(1000);
        var dataCustodianApplicationStatus = element(by.tagName('form')).all(by.className('form-control')).get(1);
        dataCustodianApplicationStatus.all(by.tagName('option')).get(3).click();
        return dataCustodianApplicationStatus;
    }



    getDataCustodianScopeSelectionScr() {
        return element(by.css('input[name=dataCustodianScopeSelectionScreenURI]'));
    }

    getDataCustodianBulkRequestURI() {
        return element(by.css('input[name=dataCustodianBulkRequestURI]'));
    }
    getThirdPartyUserPortalScreenURI() {
        return element(by.css('input[name=thirdPartyUserPortalScreenURI]'));
    }
    getThirdPartyNotifyUri() {
        return element(by.css('input[name=thirdPartyNotifyUri]'));
    }
    getThirdPartyScopeSelectionScreen() {
        return element(by.css('input[name=thirdPartyScopeSelectionScreenURI]'));
    }
    getAuthorizationServerUri() {
        return element(by.css('input[name=authorizationServerUri]'));
    }
    getClient_uri() {
        return element(by.css('input[name=client_uri]'));
    }
    getLogo_uri() {
        return element(by.css('input[name=logo_uri]'));
    }
    getRedirect_uri() {
        return element(by.css('input[name=redirect_uri]'));
    }
    getPolicy_uri() {
        return element(by.css('input[name=policy_uri]'));
    }
    getTos_uri() {
        return element(by.css('input[name=tos_uri]'));
    }
    getRegistration_client_uri() {
        var x = element(by.tagName('form')).all(by.tagName('input')).get(20);
        return x;
    }
    getErrorMsgForInvalidUrl() {
        var i, j, m, n;
        for (i = 0; i <= 1; i++) {
            var errorMsg = element(by.tagName('form')).all(by.className('alert alert-danger')).get(i).all(by.tagName('div'));
        }
        for (j = 4; j <= 7; j++) {
            var errorMsg = element(by.tagName('form')).all(by.className('alert alert-danger')).get(j).all(by.tagName('div'));
        }
        for (m = 9; m <= 13; m++) {
            var errorMsg = element(by.tagName('form')).all(by.className('alert alert-danger')).get(m).all(by.tagName('div'));
        }
        for (n = 17; n < 18; n++) {
            var errorMsg = element(by.tagName('form')).all(by.className('alert alert-danger')).get(n).all(by.tagName('div'));
        }
        return errorMsg.getText();
    }

    getThirdPartyPhone() {
        return element(by.css('input[name=thirdPartyPhone]'));
    }
    getErrorMsgForInvalidPhoneField() {
        browser.sleep(1000);
        var invalidNumber = element(by.cssContainingText('.alert-danger', 'Invalid phone number format. Ex. +1 111-222-3333'));
        return invalidNumber.getText();
    }
    goBack() {
        var backBtn = element(by.className('controls col-sm-offset-0 col-sm-10'));
        backBtn.element(by.tagName('a')).click();
        browser.sleep(2000);
        var appList = element(by.className('panel panel-info'));
        return appList;
    }

    getThirdPartyApplicationDescripti() {
        return element(by.css('input[name=thirdPartyApplicationDescription]'));
    }
    getClient_name() {
        return element(by.css('input[name=client_name]'));
    }
    getSoftware_id() {
        return element(by.css('input[name=software_id]'));
    }
    getSoftware_version() {
        return element(by.css('input[name=software_version]'));
    }
    getScope() {
        return element(by.css('input[name=scope]'));
    }

    getSubmitBtnEnabled() {
        browser.sleep(1000);
        var submitBtn = element(by.className('controls col-sm-offset-0 col-sm-10')).all(by.tagName('a')).get(1);
        return submitBtn;
    }

    //getConfirmBtnEnabledBeforeRevokingApp() {

    //    browser.sleep(1000);
    //    var confirmBtn = element.all(by.className('btn btn-block btn-success')).get(0);
    //    return confirmBtn;
    //}

    getBackToThirdPartyApplist() {
        var backBtn = element(by.className('controls col-sm-offset-0 col-sm-10')).all(by.tagName('a')).get(0).click();
        return backBtn;
    }
    getDataCustodianId() {
        return element(by.css('input[name=dataCustodianId]'));
    }

    getDataCustodianResourceEndpoint() {
        return element(by.css('input[name=dataCustodianResourceEndpoint]'));
    }
    getAuthorizationServerAuthorizati() {
        return element(by.css('input[name=authorizationServerAuthorizationEndpoint]'));
    }
    getAuthorizationServerTokenEndpoi() {
        return element(by.css('input[name=authorizationServerTokenEndpoint]'));
    }
    getClient_id() {
        return element(by.css('input[name=client_id]'));
    }
    getClient_id_issued_at() {
        var x = element(by.tagName('form')).all(by.tagName('input')).get(18);
        return x;
    }
    getClient_secret() {
        return element(by.css('input[name=client_secret]'));
    }
    getClient_secret_expires_at() {
        return element(by.css('input[name=client_secret_expires_at]'));
    }
    getToken_endpoint_auth_method() {
        return element(by.css('input[name=token_endpoint_auth_method]'));
    }
    getRegistration_access_token() {
        return element(by.css('input[name=registration_access_token]'));
    }

    getThirdPartyAppsList() {
        var thirdPartyApp = element(by.className('table table-striped table-condensed')).all(by.tagName('a')).get(0).click();
        browser.sleep(2000);
        return thirdPartyApp;
    }

    getThirdPartyListEnabledStatus() {

        //var status = [];
        //var x = [];
        //for (var i = 0; i < 5; i++){
        var status = element(by.className('table table-striped table-condensed')).all(by.tagName('tr')).get(1);
        var x = status.all(by.tagName('td')).get(3);

        return x;
    }

    getThirdPartyNameStatusTrue() {
        //var appname = [];
        //var name = [];
        // for (var i = 0; i < 5; i++) {
        var appname = element(by.className('table table-striped table-condensed')).all(by.tagName('tr')).get(2);
        var name = appname.all(by.tagName('td')).get(1);
        // }
        return name;

    }

    getGBCTestThirdPartyApp() {
        var thirdPartyApp = element(by.className('table table-striped table-condensed')).all(by.tagName('a')).get(1).click();
        browser.sleep(2000);
        return thirdPartyApp;
    }

    //getApplicationStatusDropdown() {
    //    browser.sleep(1000);
    //    var dataCustodianApplicationStatus = element(by.tagName('form')).all(by.className('form-control')).get(1).click();
    //    return dataCustodianApplicationStatus;

    //}

    //getDataCustodianApplicationStatusRevoked() {
    //    browser.sleep(1000);
    //    var dataCustodianApplicationStatus = element(by.tagName('form')).all(by.className('form-control')).get(1);
    //    var revoked = dataCustodianApplicationStatus.all(by.tagName('option')).get(3);
    //    return revoked;
    //}

    getConfirmBtnEnabledBeforeRevokingApp() {

        browser.sleep(1000);
        var confirmBtn = element.all(by.className('btn btn-block btn-success')).get(0);
        return confirmBtn;
    }

    getDataCustodianApplicationStatusProduction() {
        browser.sleep(1000);
        var dataCustodianApplicationStatus = element(by.tagName('form')).all(by.className('form-control')).get(1);
        var x = dataCustodianApplicationStatus.all(by.tagName('option')).get(1);
        return x;
    }

    getCheckEnabled() {

        var checkBox = element(by.css('input[name=enabled]')).click();
        return checkBox;

    }

    getUncheckEnabled() {

        var checkBox = element(by.css('input[name=enabled]')).click();
        return checkBox;

    }


    navigateToNonAdminUser(custID, clientID, accID, premID) {

        let gbcadminUrl = new TokenGenerator();

        let token = gbcadminUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);

                (browser.manage() as any).addCookie({ name: 'GBC', value: '87:False' });
                browser.get(browser.baseUrl + 'GreenButtonConnectAdmin');
                console.log('url->' + browser.baseUrl);
                browser.sleep(5000);
            });
    }
 
}