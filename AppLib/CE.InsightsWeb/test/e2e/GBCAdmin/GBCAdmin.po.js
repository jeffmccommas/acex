"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator_1 = require("../e2e-helpers/TokenGenerator");
var GBCAdminPageObject = (function () {
    function GBCAdminPageObject() {
    }
    GBCAdminPageObject.prototype.navigateTo = function (custID, clientID, accID, premID) {
        var gbcadminUrl = new TokenGenerator_1.TokenGenerator();
        var token = gbcadminUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.manage().addCookie({ name: 'GBC', value: '87:True' });
            built_1.browser.get(built_1.browser.baseUrl + 'GreenButtonConnectAdmin');
            console.log('url->' + built_1.browser.baseUrl);
            built_1.browser.sleep(8000);
        });
    };
    GBCAdminPageObject.prototype.getThirdPartyAppAddForm = function () {
        built_1.element(built_1.by.className('btn btn-success')).click();
        var form = built_1.element(built_1.by.className('panel panel-info'));
        return form;
    };
    GBCAdminPageObject.prototype.getDataCustodianApplicationStatusRevoked = function () {
        built_1.browser.sleep(1000);
        var dataCustodianApplicationStatus = built_1.element(built_1.by.tagName('form')).all(built_1.by.className('form-control')).get(1);
        dataCustodianApplicationStatus.all(built_1.by.tagName('option')).get(3).click();
        return dataCustodianApplicationStatus;
    };
    GBCAdminPageObject.prototype.getDataCustodianScopeSelectionScr = function () {
        return built_1.element(built_1.by.css('input[name=dataCustodianScopeSelectionScreenURI]'));
    };
    GBCAdminPageObject.prototype.getDataCustodianBulkRequestURI = function () {
        return built_1.element(built_1.by.css('input[name=dataCustodianBulkRequestURI]'));
    };
    GBCAdminPageObject.prototype.getThirdPartyUserPortalScreenURI = function () {
        return built_1.element(built_1.by.css('input[name=thirdPartyUserPortalScreenURI]'));
    };
    GBCAdminPageObject.prototype.getThirdPartyNotifyUri = function () {
        return built_1.element(built_1.by.css('input[name=thirdPartyNotifyUri]'));
    };
    GBCAdminPageObject.prototype.getThirdPartyScopeSelectionScreen = function () {
        return built_1.element(built_1.by.css('input[name=thirdPartyScopeSelectionScreenURI]'));
    };
    GBCAdminPageObject.prototype.getAuthorizationServerUri = function () {
        return built_1.element(built_1.by.css('input[name=authorizationServerUri]'));
    };
    GBCAdminPageObject.prototype.getClient_uri = function () {
        return built_1.element(built_1.by.css('input[name=client_uri]'));
    };
    GBCAdminPageObject.prototype.getLogo_uri = function () {
        return built_1.element(built_1.by.css('input[name=logo_uri]'));
    };
    GBCAdminPageObject.prototype.getRedirect_uri = function () {
        return built_1.element(built_1.by.css('input[name=redirect_uri]'));
    };
    GBCAdminPageObject.prototype.getPolicy_uri = function () {
        return built_1.element(built_1.by.css('input[name=policy_uri]'));
    };
    GBCAdminPageObject.prototype.getTos_uri = function () {
        return built_1.element(built_1.by.css('input[name=tos_uri]'));
    };
    GBCAdminPageObject.prototype.getRegistration_client_uri = function () {
        var x = built_1.element(built_1.by.tagName('form')).all(built_1.by.tagName('input')).get(20);
        return x;
    };
    GBCAdminPageObject.prototype.getErrorMsgForInvalidUrl = function () {
        var i, j, m, n;
        for (i = 0; i <= 1; i++) {
            var errorMsg = built_1.element(built_1.by.tagName('form')).all(built_1.by.className('alert alert-danger')).get(i).all(built_1.by.tagName('div'));
        }
        for (j = 4; j <= 7; j++) {
            var errorMsg = built_1.element(built_1.by.tagName('form')).all(built_1.by.className('alert alert-danger')).get(j).all(built_1.by.tagName('div'));
        }
        for (m = 9; m <= 13; m++) {
            var errorMsg = built_1.element(built_1.by.tagName('form')).all(built_1.by.className('alert alert-danger')).get(m).all(built_1.by.tagName('div'));
        }
        for (n = 17; n < 18; n++) {
            var errorMsg = built_1.element(built_1.by.tagName('form')).all(built_1.by.className('alert alert-danger')).get(n).all(built_1.by.tagName('div'));
        }
        return errorMsg.getText();
    };
    GBCAdminPageObject.prototype.getThirdPartyPhone = function () {
        return built_1.element(built_1.by.css('input[name=thirdPartyPhone]'));
    };
    GBCAdminPageObject.prototype.getErrorMsgForInvalidPhoneField = function () {
        built_1.browser.sleep(1000);
        var invalidNumber = built_1.element(built_1.by.cssContainingText('.alert-danger', 'Invalid phone number format. Ex. +1 111-222-3333'));
        return invalidNumber.getText();
    };
    GBCAdminPageObject.prototype.goBack = function () {
        var backBtn = built_1.element(built_1.by.className('controls col-sm-offset-0 col-sm-10'));
        backBtn.element(built_1.by.tagName('a')).click();
        built_1.browser.sleep(2000);
        var appList = built_1.element(built_1.by.className('panel panel-info'));
        return appList;
    };
    GBCAdminPageObject.prototype.getThirdPartyApplicationDescripti = function () {
        return built_1.element(built_1.by.css('input[name=thirdPartyApplicationDescription]'));
    };
    GBCAdminPageObject.prototype.getClient_name = function () {
        return built_1.element(built_1.by.css('input[name=client_name]'));
    };
    GBCAdminPageObject.prototype.getSoftware_id = function () {
        return built_1.element(built_1.by.css('input[name=software_id]'));
    };
    GBCAdminPageObject.prototype.getSoftware_version = function () {
        return built_1.element(built_1.by.css('input[name=software_version]'));
    };
    GBCAdminPageObject.prototype.getScope = function () {
        return built_1.element(built_1.by.css('input[name=scope]'));
    };
    GBCAdminPageObject.prototype.getSubmitBtnEnabled = function () {
        built_1.browser.sleep(1000);
        var submitBtn = built_1.element(built_1.by.className('controls col-sm-offset-0 col-sm-10')).all(built_1.by.tagName('a')).get(1);
        return submitBtn;
    };
    GBCAdminPageObject.prototype.getBackToThirdPartyApplist = function () {
        var backBtn = built_1.element(built_1.by.className('controls col-sm-offset-0 col-sm-10')).all(built_1.by.tagName('a')).get(0).click();
        return backBtn;
    };
    GBCAdminPageObject.prototype.getDataCustodianId = function () {
        return built_1.element(built_1.by.css('input[name=dataCustodianId]'));
    };
    GBCAdminPageObject.prototype.getDataCustodianResourceEndpoint = function () {
        return built_1.element(built_1.by.css('input[name=dataCustodianResourceEndpoint]'));
    };
    GBCAdminPageObject.prototype.getAuthorizationServerAuthorizati = function () {
        return built_1.element(built_1.by.css('input[name=authorizationServerAuthorizationEndpoint]'));
    };
    GBCAdminPageObject.prototype.getAuthorizationServerTokenEndpoi = function () {
        return built_1.element(built_1.by.css('input[name=authorizationServerTokenEndpoint]'));
    };
    GBCAdminPageObject.prototype.getClient_id = function () {
        return built_1.element(built_1.by.css('input[name=client_id]'));
    };
    GBCAdminPageObject.prototype.getClient_id_issued_at = function () {
        var x = built_1.element(built_1.by.tagName('form')).all(built_1.by.tagName('input')).get(18);
        return x;
    };
    GBCAdminPageObject.prototype.getClient_secret = function () {
        return built_1.element(built_1.by.css('input[name=client_secret]'));
    };
    GBCAdminPageObject.prototype.getClient_secret_expires_at = function () {
        return built_1.element(built_1.by.css('input[name=client_secret_expires_at]'));
    };
    GBCAdminPageObject.prototype.getToken_endpoint_auth_method = function () {
        return built_1.element(built_1.by.css('input[name=token_endpoint_auth_method]'));
    };
    GBCAdminPageObject.prototype.getRegistration_access_token = function () {
        return built_1.element(built_1.by.css('input[name=registration_access_token]'));
    };
    GBCAdminPageObject.prototype.getThirdPartyAppsList = function () {
        var thirdPartyApp = built_1.element(built_1.by.className('table table-striped table-condensed')).all(built_1.by.tagName('a')).get(0).click();
        built_1.browser.sleep(2000);
        return thirdPartyApp;
    };
    GBCAdminPageObject.prototype.getThirdPartyListEnabledStatus = function () {
        var status = built_1.element(built_1.by.className('table table-striped table-condensed')).all(built_1.by.tagName('tr')).get(1);
        var x = status.all(built_1.by.tagName('td')).get(3);
        return x;
    };
    GBCAdminPageObject.prototype.getThirdPartyNameStatusTrue = function () {
        var appname = built_1.element(built_1.by.className('table table-striped table-condensed')).all(built_1.by.tagName('tr')).get(2);
        var name = appname.all(built_1.by.tagName('td')).get(1);
        return name;
    };
    GBCAdminPageObject.prototype.getGBCTestThirdPartyApp = function () {
        var thirdPartyApp = built_1.element(built_1.by.className('table table-striped table-condensed')).all(built_1.by.tagName('a')).get(1).click();
        built_1.browser.sleep(2000);
        return thirdPartyApp;
    };
    GBCAdminPageObject.prototype.getConfirmBtnEnabledBeforeRevokingApp = function () {
        built_1.browser.sleep(1000);
        var confirmBtn = built_1.element.all(built_1.by.className('btn btn-block btn-success')).get(0);
        return confirmBtn;
    };
    GBCAdminPageObject.prototype.getDataCustodianApplicationStatusProduction = function () {
        built_1.browser.sleep(1000);
        var dataCustodianApplicationStatus = built_1.element(built_1.by.tagName('form')).all(built_1.by.className('form-control')).get(1);
        var x = dataCustodianApplicationStatus.all(built_1.by.tagName('option')).get(1);
        return x;
    };
    GBCAdminPageObject.prototype.getCheckEnabled = function () {
        var checkBox = built_1.element(built_1.by.css('input[name=enabled]')).click();
        return checkBox;
    };
    GBCAdminPageObject.prototype.getUncheckEnabled = function () {
        var checkBox = built_1.element(built_1.by.css('input[name=enabled]')).click();
        return checkBox;
    };
    GBCAdminPageObject.prototype.navigateToNonAdminUser = function (custID, clientID, accID, premID) {
        var gbcadminUrl = new TokenGenerator_1.TokenGenerator();
        var token = gbcadminUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.manage().addCookie({ name: 'GBC', value: '87:False' });
            built_1.browser.get(built_1.browser.baseUrl + 'GreenButtonConnectAdmin');
            console.log('url->' + built_1.browser.baseUrl);
            built_1.browser.sleep(5000);
        });
    };
    return GBCAdminPageObject;
}());
exports.GBCAdminPageObject = GBCAdminPageObject;
//# sourceMappingURL=GBCAdmin.po.js.map