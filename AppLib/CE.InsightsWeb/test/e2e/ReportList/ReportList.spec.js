"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var Login_po_1 = require("../loginpage/Login.po");
describe('ReportList Tests', function () {
    var page;
    beforeEach(function () {
        page = new Login_po_1.LoginPageObject();
    });
    it('Detect popover confirm button on report list when click on delete button', function () {
        page.navigateTo();
        built_1.browser.sleep(8000);
        page.setupTestLoginForClient('87');
        page.selectLogin();
        built_1.browser.sleep(4000);
        built_1.element(built_1.by.linkText('Dashboard')).click().then(function () {
            built_1.element(built_1.by.id('inputTitle')).clear();
            built_1.element(built_1.by.id('inputTitle')).sendKeys('Your Energy Report test 214');
            built_1.element(built_1.by.name('pdfExport')).click();
            built_1.browser.wait(function () { return built_1.element(built_1.by.linkText('Report')).isPresent(); }, 30 * 1000, '**** Report element took too long to load ****');
            built_1.element(built_1.by.linkText("Report")).click().then(function () {
                built_1.browser.wait(function () { return built_1.element(built_1.by.linkText('Delete')).isPresent(); }, 30 * 1000, '**** Delete element took too long to load ****');
                built_1.element(built_1.by.linkText('Delete')).click();
                var popupTest = built_1.element(built_1.by.xpath('//ng-component/div/div[2]/div/div/button'));
                expect(popupTest.getText()).toMatch('Confirm');
            });
        });
    });
});
//# sourceMappingURL=ReportList.spec.js.map