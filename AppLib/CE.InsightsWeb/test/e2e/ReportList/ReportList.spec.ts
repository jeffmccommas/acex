﻿import { browser, by, element } from 'protractor/built';
 
import { LoginPageObject } from '../loginpage/Login.po';

describe('ReportList Tests', () => {
    let page: LoginPageObject;


    beforeEach(() => {
        page = new LoginPageObject();
    });
    


    it('Detect popover confirm button on report list when click on delete button', () => {
        page.navigateTo();
        browser.sleep(8000);
        page.setupTestLoginForClient('87');
        page.selectLogin();
        browser.sleep(4000);
        element(by.linkText('Dashboard')).click().then(() => {
            element(by.id('inputTitle')).clear();
            element(by.id('inputTitle')).sendKeys('Your Energy Report test 214');
            element(by.name('pdfExport')).click();
            browser.wait(() => element(by.linkText('Report')).isPresent(), 30 * 1000, '**** Report element took too long to load ****');
            element(by.linkText("Report")).click().then(() => {

                //30*1000 is the max time this will wait for the element to show up. It will move forward if condition is met before 30 sec
                browser.wait(() => element(by.linkText('Delete')).isPresent(), 30 * 1000, '**** Delete element took too long to load ****');

                element(by.linkText('Delete')).click();
                var popupTest = element(by.xpath('//ng-component/div/div[2]/div/div/button'));
                expect(popupTest.getText()).toMatch('Confirm');
            });
        });
    });
}); 