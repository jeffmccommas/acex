"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator_1 = require("../e2e-helpers/TokenGenerator");
var ActionDetailPageObject = (function () {
    function ActionDetailPageObject() {
    }
    ActionDetailPageObject.prototype.navigateTo = function (custID, clientID, accID, premID) {
        var actionDetailUrl = new TokenGenerator_1.TokenGenerator();
        var token = actionDetailUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Page?id=11&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            console.log('url->' + built_1.browser.baseUrl + 'Page?id=11&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
        });
    };
    ActionDetailPageObject.prototype.getIntroText = function () {
        built_1.browser.sleep(2000);
        var introText = built_1.element(built_1.by.id('iws_as_wrapper')).element(built_1.by.className('panel panel-default radius'))
            .element(built_1.by.id('iws_as_intro_text')).element(built_1.by.id('createapersonalsavingsplan'));
        return introText;
    };
    ActionDetailPageObject.prototype.getAddToDoList = function () {
        built_1.browser.sleep(1000);
        var y = built_1.element(built_1.by.id('iws_as_actions')).all(built_1.by.className('panel panel-default actions'));
        var addToDoListText = y.get(0).element(built_1.by.className('actions__message--title'));
        return addToDoListText;
    };
    ActionDetailPageObject.prototype.getMarkCompleted = function () {
        built_1.browser.sleep(1000);
        var y = built_1.element(built_1.by.id('iws_as_actions')).all(built_1.by.className('panel panel-default actions'));
        var markCompleted = y.get(1).element(built_1.by.className('actions__message--title'));
        return markCompleted;
    };
    return ActionDetailPageObject;
}());
exports.ActionDetailPageObject = ActionDetailPageObject;
//# sourceMappingURL=ActionDetail.po.js.map