﻿import { browser, by, element } from 'protractor/built';
import { TokenGenerator } from '../e2e-helpers/TokenGenerator';

export class ActionDetailPageObject {


    navigateTo(custID, clientID, accID, premID) {

        let actionDetailUrl = new TokenGenerator();

        let token = actionDetailUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);
                //browser.waitForAngularEnabled(false);

                browser.get(browser.baseUrl + 'Page?id=11&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
                console.log('url->' + browser.baseUrl + 'Page?id=11&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            });
    }

    getIntroText() {

        browser.sleep(2000);
        var introText = element(by.id('iws_as_wrapper')).element(by.className('panel panel-default radius'))
                      .element(by.id('iws_as_intro_text')).element(by.id('createapersonalsavingsplan'));

        return introText;

    }

    getAddToDoList() {

        browser.sleep(1000);
        var y = element(by.id('iws_as_actions')).all(by.className('panel panel-default actions'));
        var addToDoListText = y.get(0).element(by.className('actions__message--title'));

        return addToDoListText;
    }

    getMarkCompleted() {

        browser.sleep(1000);
        var y = element(by.id('iws_as_actions')).all(by.className('panel panel-default actions'));
        var markCompleted = y.get(1).element(by.className('actions__message--title'));

        return markCompleted;
    }

}