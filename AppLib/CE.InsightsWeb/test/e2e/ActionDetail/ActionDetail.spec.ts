﻿'Use strict';

import { ActionDetailPageObject } from './ActionDetail.po';

describe('Action Detail', () => {
    let page: ActionDetailPageObject;

    beforeEach(() => {
        page = new ActionDetailPageObject();
    });

    it('Verify Action Detail back to link when false', function () {
        var custID = '993774751';
        var clientID = '256';
        var accID = '1218337093';
        var premID = '1218337000';

        page.navigateTo(custID, clientID, accID, premID);
        var introText = page.getIntroText();
        var addToDoListText = page.getAddToDoList();
        var a = addToDoListText.all(by.tagName('a')).get(0).getText();

        var markCompleted = page.getMarkCompleted();

        expect(introText.getText()).toEqual('Create a personal savings plan!');
       

        markCompleted.all(by.tagName('a')).get(2).click().then(function () {
            expect(markCompleted.element(by.className('action-completed')).getText()).toEqual('Completed');
        });

        addToDoListText.all(by.tagName('a')).get(1).click().then(function () {
            expect(addToDoListText.element(by.className('action-added-todo')).getText()).toEqual('Added to your plan');
        });

        addToDoListText.all(by.tagName('a')).get(0).click().then(function () {
            expect(element(by.className('footer__backlink')).getText()).toEqual('');

        });
        
    });

    it('Verify Action Detail back to link when false', function () {
        var custID = '993774751';
        var clientID = '87';
        var accID = '1218337093';
        var premID = '1218337000';

        page.navigateTo(custID, clientID, accID, premID);
        var introText = page.getIntroText();
        var addToDoListText = page.getAddToDoList();

        addToDoListText.all(by.tagName('a')).get(0).click().then(function () {
            expect(element(by.className('footer__backlink')).getText()).toEqual('Back to Ways to Save');

        });

    });
}); 