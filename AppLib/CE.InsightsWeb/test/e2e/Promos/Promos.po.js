"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator_1 = require("../e2e-helpers/TokenGenerator");
var PromosPageObject = (function () {
    function PromosPageObject() {
    }
    PromosPageObject.prototype.navigateTo = function (custID, clientID, accID, premID) {
        var promosUrl = new TokenGenerator_1.TokenGenerator();
        var token = promosUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Page?id=1&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            console.log('url->' + built_1.browser.baseUrl + 'Page?id=1&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
        });
    };
    PromosPageObject.prototype.getImgforMax = function () {
        built_1.browser.driver.manage().window().maximize();
        built_1.browser.sleep(3000);
        var PromoImg = built_1.element(built_1.by.css('promo '));
        var Img1 = PromoImg.all(built_1.by.tagName('img'));
        return Img1;
    };
    PromosPageObject.prototype.getImgforMedium = function () {
        built_1.browser.driver.manage().window().setSize(700, 700);
        built_1.browser.sleep(3000);
        var PromoImg = built_1.element(built_1.by.css('promo '));
        var Img1 = PromoImg.all(built_1.by.tagName('img'));
        return Img1;
    };
    PromosPageObject.prototype.getImgforSmall = function () {
        built_1.browser.driver.manage().window().setSize(300, 300);
        built_1.browser.sleep(3000);
        var PromoImg = built_1.element(built_1.by.css('promo '));
        var Img1 = PromoImg.all(built_1.by.tagName('img'));
        return Img1;
    };
    return PromosPageObject;
}());
exports.PromosPageObject = PromosPageObject;
//# sourceMappingURL=Promos.po.js.map