﻿'Use strict';

import { PromosPageObject } from './Promos.po';

describe('Promos', () => {
    let page: PromosPageObject;

    beforeEach(() => {
        page = new PromosPageObject();
    });


    it('Should display Promo Images in small size while window size is Max ', () => {
        var custID = 'QATestActionPromoCond';
        var clientID = '87';
        var accID = 'QATestActionPromoCond';
        var premID = 'QATestActionPromoCond';
        page.navigateTo(custID, clientID, accID, premID);
        var Img1 = page.getImgforMax();
        expect(Img1.get(0).getAttribute('src')).toMatch('//images.contentful.com/wp32f8j5jqi2/27C6fAC3DOYmE66SgsMOY8/0254fae4f94f9456287ffba01e415ef9/cold-weather-alert-sm.png');
        expect(Img1.get(1).getAttribute('src')).toMatch('//images.contentful.com/wp32f8j5jqi2/demopromoinsulation/619068fd46561453d3f3079ba4bbfbce/promo-insulation.jpg');

        browser.executeScript('window.scrollTo(0,1000);').then(() => {
            browser.sleep(3000);
        });

    });

    it('Should display Promo Images in large size while window size is Medium ', () => {
        var custID = 'QATestActionPromoCond';
        var clientID = '87';
        var accID = 'QATestActionPromoCond';
        var premID = 'QATestActionPromoCond';
        page.navigateTo(custID, clientID, accID, premID);
        var Img1 = page.getImgforMedium();
        browser.sleep(3000);
        expect(Img1.get(0).getAttribute('src')).toMatch('//images.contentful.com/wp32f8j5jqi2/qasMogBkiW8yQwgcMi8oY/e77d7167af637a3b6c08880409a19fca/cold-weather-alert-md.png');
        expect(Img1.get(1).getAttribute('src')).toMatch('//images.contentful.com/wp32f8j5jqi2/demopromoinsulationlargecol/90013d341094e03520bda737f1acc2a4/promo-insulation-lg-col.jpg');

        browser.executeScript('window.scrollTo(0,3000);').then(() => {
            browser.sleep(3000);
        });

    });

    it('Should display promo Images in small size while window size is small ', () => {

        var custID = 'QATestActionPromoCond';
        var clientID = '87';
        var accID = 'QATestActionPromoCond';
        var premID = 'QATestActionPromoCond';
        page.navigateTo(custID, clientID, accID, premID);
        var Img1 = page.getImgforSmall();

        expect(Img1.get(0).getAttribute('src')).toMatch('//images.contentful.com/wp32f8j5jqi2/27C6fAC3DOYmE66SgsMOY8/0254fae4f94f9456287ffba01e415ef9/cold-weather-alert-sm.png');
        expect(Img1.get(1).getAttribute('src')).toMatch('//images.contentful.com/wp32f8j5jqi2/demopromoinsulation/619068fd46561453d3f3079ba4bbfbce/promo-insulation.jpg');

        browser.executeScript('window.scrollTo(0,5000);').then(() => {
            browser.sleep(2000);
        });

    });
});