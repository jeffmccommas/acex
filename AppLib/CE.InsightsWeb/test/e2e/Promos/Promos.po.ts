﻿import { browser, by, element } from 'protractor/built';
import { TokenGenerator } from '../e2e-helpers/TokenGenerator';

export class PromosPageObject {


    navigateTo(custID, clientID, accID, premID) {

        let promosUrl = new TokenGenerator();

        let token = promosUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);
                //browser.waitForAngularEnabled(false);

                browser.get(browser.baseUrl + 'Page?id=1&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
                console.log('url->' + browser.baseUrl + 'Page?id=1&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            });
    }

    getImgforMax() {

        browser.driver.manage().window().maximize();
        browser.sleep(3000);
        var PromoImg = element(by.css('promo '));
        var Img1 = PromoImg.all(by.tagName('img'));

        return Img1;
    }

    getImgforMedium() {

        browser.driver.manage().window().setSize(700, 700);
        browser.sleep(3000);
        var PromoImg = element(by.css('promo '));
        var Img1 = PromoImg.all(by.tagName('img'));

        return Img1;
    }

    getImgforSmall() {

        browser.driver.manage().window().setSize(300, 300);
        browser.sleep(3000);
        var PromoImg = element(by.css('promo '));
        var Img1 = PromoImg.all(by.tagName('img'));

        return Img1;
    }

}