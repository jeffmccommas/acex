﻿import { by, element, browser } from 'protractor/built';

export class TokenGenerator {

    //returns a token which is valid for 60 minutes and can be used to directly access tabs
    //only page objects should use this. It should not be used in .spec files directly
    getToken(custID, clientID, accID, premID) {
        console.info('Get token------------> start'); 

        browser.waitForAngularEnabled(false);
        browser.get(browser.baseUrl + 'Page/PostToken?customerId=' + custID + '&clientId=' + clientID + '&accountId=' + accID + '&premiseId=' + premID + '&username=qatest&password=qa@345R&clearProfileAttr=false&csrLogin=true&disableRemote=true');
        return element.all(by.tagName('pre')).first().getText();
    }

}