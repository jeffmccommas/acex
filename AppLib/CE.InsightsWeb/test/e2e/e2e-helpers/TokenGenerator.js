"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator = (function () {
    function TokenGenerator() {
    }
    TokenGenerator.prototype.getToken = function (custID, clientID, accID, premID) {
        console.info('Get token------------> start');
        built_1.browser.waitForAngularEnabled(false);
        built_1.browser.get(built_1.browser.baseUrl + 'Page/PostToken?customerId=' + custID + '&clientId=' + clientID + '&accountId=' + accID + '&premiseId=' + premID + '&username=qatest&password=qa@345R&clearProfileAttr=false&csrLogin=true&disableRemote=true');
        return built_1.element.all(built_1.by.tagName('pre')).first().getText();
    };
    return TokenGenerator;
}());
exports.TokenGenerator = TokenGenerator;
//# sourceMappingURL=TokenGenerator.js.map