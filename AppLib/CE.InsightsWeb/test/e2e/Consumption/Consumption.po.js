"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator_1 = require("../e2e-helpers/TokenGenerator");
var ConsumptionPageObject = (function () {
    function ConsumptionPageObject() {
    }
    ConsumptionPageObject.prototype.navigateTo = function (custID, clientID, accID, premID) {
        var consumptionUrl = new TokenGenerator_1.TokenGenerator();
        var token = consumptionUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Widget?id=tab.myusage-consumption&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            console.log('url->' + built_1.browser.baseUrl + 'Widget?id=tab.myusage-consumption&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true&GroupName=CSR&RoleName=CSRAdmin');
            built_1.browser.sleep(8000);
        });
    };
    ConsumptionPageObject.prototype.getSelectMonths = function () {
        var btn = built_1.element(built_1.by.className('btn-default btn dropdown-toggle deep-link-highlight'));
        btn.click();
        var select_months = built_1.element(built_1.by.className('dropdown-menu')).all(built_1.by.tagName('a'));
        var months = select_months.get(0).click();
        return months;
    };
    ConsumptionPageObject.prototype.getAvgUsageAMIchart = function () {
        var avgUsageChart = built_1.element(built_1.by.className('highcharts-series highcharts-series-3'));
        return avgUsageChart;
    };
    ConsumptionPageObject.prototype.getAvgUsage = function () {
        var legends = built_1.element(built_1.by.className('highcharts-legend')).all(built_1.by.className('highcharts-legend-item')).get(3);
        var avgUsage = legends.click();
        return avgUsage;
    };
    ConsumptionPageObject.prototype.getelectricAMI = function () {
        var electricAMI = built_1.element(built_1.by.className('btn btn-default xs-ml-5 icon__electric')).click();
        return electricAMI;
    };
    ConsumptionPageObject.prototype.getwaterAMI = function () {
        var waterAMI = built_1.element(built_1.by.className('btn btn-default xs-ml-5 icon__water')).click();
        return waterAMI;
    };
    return ConsumptionPageObject;
}());
exports.ConsumptionPageObject = ConsumptionPageObject;
//# sourceMappingURL=Consumption.po.js.map