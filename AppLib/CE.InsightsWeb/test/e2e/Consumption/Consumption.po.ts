﻿import { browser, by, element } from 'protractor/built';
import { TokenGenerator } from '../e2e-helpers/TokenGenerator';


export class ConsumptionPageObject {


    navigateTo(custID, clientID, accID, premID) {

        let consumptionUrl = new TokenGenerator();

        let token = consumptionUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);

                browser.get(browser.baseUrl + 'Widget?id=tab.myusage-consumption&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
                console.log('url->' + browser.baseUrl + 'Widget?id=tab.myusage-consumption&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true&GroupName=CSR&RoleName=CSRAdmin');
                browser.sleep(8000);
            });
    }


    getSelectMonths() {

        var btn = element(by.className('btn-default btn dropdown-toggle deep-link-highlight'));
        btn.click();
        var select_months = element(by.className('dropdown-menu')).all(by.tagName('a'));
        var months = select_months.get(0).click();
        return months;
        
    }

    getAvgUsageAMIchart() {

        var avgUsageChart = element(by.className('highcharts-series highcharts-series-3'));
        return avgUsageChart;

    }

    getAvgUsage() {

        var legends = element(by.className('highcharts-legend')).all(by.className('highcharts-legend-item')).get(3);
        var avgUsage = legends.click();
        return avgUsage;

    }

    getelectricAMI() {
        var electricAMI = element(by.className('btn btn-default xs-ml-5 icon__electric')).click();
        return electricAMI;
    }

    getwaterAMI() {

        var waterAMI = element(by.className('btn btn-default xs-ml-5 icon__water')).click();
        return waterAMI;

    }

}