'Use strict';
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Consumption_po_1 = require("./Consumption.po");
describe('Consumption', function () {
    var page;
    beforeEach(function () {
        page = new Consumption_po_1.ConsumptionPageObject();
    });
    it('Verifying monthly avg usage for gas commodity', function () {
        var custID = '993774751';
        var clientID = '87';
        var accID = '1218337093';
        var premID = '1218337000';
        page.navigateTo(custID, clientID, accID, premID);
        browser.sleep(8000);
        page.getSelectMonths();
        browser.sleep(5000);
        var avgUsgaeAMI = page.getAvgUsageAMIchart();
        expect(avgUsgaeAMI.isDisplayed()).toBe(true);
        page.getAvgUsage();
        var avgUsgaeAMI = page.getAvgUsageAMIchart();
        expect(avgUsgaeAMI.isDisplayed()).toBe(false);
    });
    it('Verifying monthly avg usage for electric commodity', function () {
        page.getelectricAMI();
        browser.sleep(4000);
        var avgUsgaeAMI = page.getAvgUsageAMIchart();
        expect(avgUsgaeAMI.isDisplayed()).toBe(true);
        page.getAvgUsage();
        var avgUsgaeAMI = page.getAvgUsageAMIchart();
        expect(avgUsgaeAMI.isDisplayed()).toBe(false);
    });
    it('Verifying monthly avg usage for water commodity', function () {
        page.getwaterAMI();
        browser.sleep(4000);
        var avgUsgaeAMI = page.getAvgUsageAMIchart();
        expect(avgUsgaeAMI.isDisplayed()).toBe(true);
        page.getAvgUsage();
        var avgUsgaeAMI = page.getAvgUsageAMIchart();
        expect(avgUsgaeAMI.isDisplayed()).toBe(false);
    });
    it('Verifying avg usage for electric commodity', function () {
        var custID = 'resdemo60minEGW';
        var clientID = '4';
        var accID = 'resdemo60minEGW';
        var premID = 'resdemo60minEGW';
        page.navigateTo(custID, clientID, accID, premID);
        browser.sleep(5000);
        page.getSelectMonths();
        browser.sleep(1000);
        page.getelectricAMI();
        browser.sleep(1000);
        var avgUsageAMI = page.getAvgUsageAMIchart();
        expect(avgUsageAMI.isDisplayed()).toBe(true);
        page.getAvgUsage();
        var avgUsageAMI = page.getAvgUsageAMIchart();
        expect(avgUsageAMI.isDisplayed()).toBe(false);
    });
});
//# sourceMappingURL=Consumption.spec.js.map