﻿import { browser, by, element } from 'protractor/built';
import { TokenGenerator } from '../e2e-helpers/TokenGenerator';

export class BilledUsagePageObject {


    navigateTo(custID, clientID, accID, premID) {

        let billUsageUrl = new TokenGenerator();

        let token = billUsageUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function(data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);
                browser.waitForAngularEnabled(true);
                browser.get(browser.baseUrl + 'Widget?id=tab.myusage-billedusagechart&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=1');
                console.log('url->' + browser.baseUrl + 'Widget?id=tab.myusage-billedusagechart&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=1');
                browser.sleep(2000);
            });
    }


    getBilledUsageChart() {

        browser.sleep(2000);
        var BilledUsage = element(by.id('iws_rc_pagewidget_7'));
        return BilledUsage;

    }
    
    getBilledUsageTable() {
        browser.sleep(2000);
        var pageAccessButton = element(by.className('btn btn-default chart-table-spacer icon__table'));
        pageAccessButton.click();
        var billUsagetable = element(by.className('table-responsive'));
        var tableRow = billUsagetable.element(by.tagName('tbody')).all(by.tagName('td'));

        var rowData = tableRow.map(function (data) {
            return data.getText();
        });
        return rowData;

    }

    getOnePointDisplay() {

        var onePointDisplay = element(by.className('highcharts-series highcharts-series-1 highcharts-tracker')).element(by.tagName('rect'));
        return onePointDisplay;

    }

    getHoverData() {

        var HoverData = element(by.className('highcharts-tooltip'));
        var Records = HoverData.element(by.tagName('text')).all(by.tagName('tspan'));

        var HovData = Records.map(function (data) {
            return data.getText();
        });
        return HovData;
    }

    getMonthFirstHalfBill() {
        browser.sleep(2000);
        var pageAccessButton = element(by.className('btn btn-default chart-table-spacer icon__table'));
        pageAccessButton.click();
        var billUsagetable = element(by.className('table-responsive'));
        var tableRow = billUsagetable.element(by.tagName('tbody')).all(by.tagName('td')).get(1);
        return tableRow;

    }

    getMonthSecondHalfBill() {
        browser.sleep(2000);
        var billUsagetable = element(by.className('table-responsive'));
        var tableRow = billUsagetable.element(by.tagName('tbody')).all(by.tagName('td')).get(4);

        return tableRow;
    }

    getPreviousYearData() {
        var prevyrGraph = element(by.className('highcharts-series highcharts-series-0 highcharts-tracker'));
        return prevyrGraph;

    }

    getCurrentYearData() {
        var prevyrGraph = element(by.className('highcharts-series highcharts-series-1 highcharts-tracker')).all(by.tagName('rect')).get(0);
        return prevyrGraph;

    }
}
