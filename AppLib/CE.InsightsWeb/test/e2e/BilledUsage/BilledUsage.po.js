"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator_1 = require("../e2e-helpers/TokenGenerator");
var BilledUsagePageObject = (function () {
    function BilledUsagePageObject() {
    }
    BilledUsagePageObject.prototype.navigateTo = function (custID, clientID, accID, premID) {
        var billUsageUrl = new TokenGenerator_1.TokenGenerator();
        var token = billUsageUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.waitForAngularEnabled(true);
            built_1.browser.get(built_1.browser.baseUrl + 'Widget?id=tab.myusage-billedusagechart&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=1');
            console.log('url->' + built_1.browser.baseUrl + 'Widget?id=tab.myusage-billedusagechart&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=1');
            built_1.browser.sleep(2000);
        });
    };
    BilledUsagePageObject.prototype.getBilledUsageChart = function () {
        built_1.browser.sleep(2000);
        var BilledUsage = built_1.element(built_1.by.id('iws_rc_pagewidget_7'));
        return BilledUsage;
    };
    BilledUsagePageObject.prototype.getBilledUsageTable = function () {
        built_1.browser.sleep(2000);
        var pageAccessButton = built_1.element(built_1.by.className('btn btn-default chart-table-spacer icon__table'));
        pageAccessButton.click();
        var billUsagetable = built_1.element(built_1.by.className('table-responsive'));
        var tableRow = billUsagetable.element(built_1.by.tagName('tbody')).all(built_1.by.tagName('td'));
        var rowData = tableRow.map(function (data) {
            return data.getText();
        });
        return rowData;
    };
    BilledUsagePageObject.prototype.getOnePointDisplay = function () {
        var onePointDisplay = built_1.element(built_1.by.className('highcharts-series highcharts-series-1 highcharts-tracker')).element(built_1.by.tagName('rect'));
        return onePointDisplay;
    };
    BilledUsagePageObject.prototype.getHoverData = function () {
        var HoverData = built_1.element(built_1.by.className('highcharts-tooltip'));
        var Records = HoverData.element(built_1.by.tagName('text')).all(built_1.by.tagName('tspan'));
        var HovData = Records.map(function (data) {
            return data.getText();
        });
        return HovData;
    };
    BilledUsagePageObject.prototype.getMonthFirstHalfBill = function () {
        built_1.browser.sleep(2000);
        var pageAccessButton = built_1.element(built_1.by.className('btn btn-default chart-table-spacer icon__table'));
        pageAccessButton.click();
        var billUsagetable = built_1.element(built_1.by.className('table-responsive'));
        var tableRow = billUsagetable.element(built_1.by.tagName('tbody')).all(built_1.by.tagName('td')).get(1);
        return tableRow;
    };
    BilledUsagePageObject.prototype.getMonthSecondHalfBill = function () {
        built_1.browser.sleep(2000);
        var billUsagetable = built_1.element(built_1.by.className('table-responsive'));
        var tableRow = billUsagetable.element(built_1.by.tagName('tbody')).all(built_1.by.tagName('td')).get(4);
        return tableRow;
    };
    BilledUsagePageObject.prototype.getPreviousYearData = function () {
        var prevyrGraph = built_1.element(built_1.by.className('highcharts-series highcharts-series-0 highcharts-tracker'));
        return prevyrGraph;
    };
    BilledUsagePageObject.prototype.getCurrentYearData = function () {
        var prevyrGraph = built_1.element(built_1.by.className('highcharts-series highcharts-series-1 highcharts-tracker')).all(built_1.by.tagName('rect')).get(0);
        return prevyrGraph;
    };
    return BilledUsagePageObject;
}());
exports.BilledUsagePageObject = BilledUsagePageObject;
//# sourceMappingURL=BilledUsage.po.js.map