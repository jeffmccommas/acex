'Use strict';
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BilledUsage_po_1 = require("./BilledUsage.po");
describe('BilledUsage', function () {
    var page;
    beforeEach(function () {
        page = new BilledUsage_po_1.BilledUsagePageObject();
    });
    it('Billed Usage widget is dispaying 1 kwH per month', function () {
        var custID = '1000002';
        var clientID = '87';
        var accID = '1000002';
        var premID = '1000002';
        page.navigateTo(custID, clientID, accID, premID);
        var billedUsage = page.getBilledUsageTable();
        var json = require('../TestData/BilledUsage/BilledUsage.json');
        expect(billedUsage).toEqual([json.Month[0], json.Last12Months[0], json.Previous12Months[0],
            json.Month[1], json.Last12Months[1], json.Previous12Months[1],
            json.Month[2], json.Last12Months[2], json.Previous12Months[2],
            json.Month[3], json.Last12Months[3], json.Previous12Months[3],
            json.Month[4], json.Last12Months[4], json.Previous12Months[4],
            json.Month[5], json.Last12Months[5], json.Previous12Months[5],
            json.Month[6], json.Last12Months[6], json.Previous12Months[6],
            json.Month[7], json.Last12Months[7], json.Previous12Months[7],
            json.Month[8], json.Last12Months[8], json.Previous12Months[8],
            json.Month[9], json.Last12Months[9], json.Previous12Months[9],
            json.Month[10], json.Last12Months[10], json.Previous12Months[10],
            json.Month[11], json.Last12Months[11], json.Previous12Months[11]]);
    });
    it('Verify graph for gas premise with two bills in one month same service point', function () {
        var custID = 'custQAInttest_07072015085241_5';
        var clientID = '87';
        var accID = 'acctQAInttest_07072015085241_5';
        var premID = 'premQAInttest_07072015085241_5';
        page.navigateTo(custID, clientID, accID, premID);
        var onePointDisplay = page.getOnePointDisplay();
        expect(onePointDisplay.isDisplayed()).toBe(true);
        onePointDisplay.click();
        var hoverData = page.getHoverData();
        expect(hoverData).toEqual(['550.00 Therms | $365 |', '58°|', 'Billed on 12/30/2016', '551.00 Therms | $365 |', '58°|', 'Billed on 12/15/2016']);
        var billfirsthalf = page.getMonthFirstHalfBill();
        var billsecondhalf = page.getMonthSecondHalfBill();
        expect(billfirsthalf.getText()).toContain('$365');
        expect(billsecondhalf.getText()).toContain('$365');
    });
    it('Verify graph for premise service points (same commodity)', function () {
        var custID = '1000005';
        var clientID = '87';
        var accID = '1000005';
        var premID = '1000005';
        page.navigateTo(custID, clientID, accID, premID);
        browser.sleep(2000);
        var PrevYrGraph = page.getPreviousYearData();
        expect(PrevYrGraph.isDisplayed()).toBe(false);
        var currentYr = page.getCurrentYearData();
        currentYr.click();
        expect(currentYr.isDisplayed()).toBe(true);
    });
});
//# sourceMappingURL=BilledUsage.spec.js.map