"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator_1 = require("../e2e-helpers/TokenGenerator");
var BillDisaggPageObject = (function () {
    function BillDisaggPageObject() {
    }
    BillDisaggPageObject.prototype.navigateTo = function (custID, clientID, accID, premID) {
        var billDisaggUrl = new TokenGenerator_1.TokenGenerator();
        var token = billDisaggUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Widget?id=tab.myusage-billdisagg&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            console.log('url->' + built_1.browser.baseUrl + 'Widget?id=tab.myusage-billdisagg&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true&GroupName=CSR&RoleName=CSRAdmin');
            built_1.browser.sleep(1000);
        });
    };
    BillDisaggPageObject.prototype.getAnnualUseByCategory = function () {
        return built_1.element(built_1.by.cssContainingText('.panel-title', 'Annual Use By Category'));
    };
    BillDisaggPageObject.prototype.getHoverData = function () {
        var HoverData = built_1.element(built_1.by.className('highcharts-tooltip'));
        var Records = HoverData.element(built_1.by.tagName('text')).all(built_1.by.tagName('tspan'));
        var HovData = Records.map(function (data) {
            return data.getText();
        });
        return HovData;
    };
    BillDisaggPageObject.prototype.getBillDisaggTable = function () {
        built_1.element(built_1.by.className('btn btn-default chart-table-spacer icon__table')).click();
        var billDisaggtable = built_1.element(built_1.by.className('billCycle__table xs-mr-0 xs-p-15 xs-pt-0'));
        var tableHead = billDisaggtable.element(built_1.by.tagName('thead')).all(built_1.by.tagName('th'));
        var rowsHeaders = tableHead.map(function (a) {
            return a.getText();
        });
        return rowsHeaders;
    };
    BillDisaggPageObject.prototype.getBillDisaggTableData = function () {
        var billDisaggtable = built_1.element(built_1.by.className('billCycle__table xs-mr-0 xs-p-15 xs-pt-0'));
        var tableRow = billDisaggtable.element(built_1.by.tagName('tbody')).all(built_1.by.tagName('td'));
        var rowData = tableRow.map(function (data) {
            return data.getText();
        });
        return rowData;
    };
    BillDisaggPageObject.prototype.getPercentOnlyArray = function () {
        var billUsage = built_1.element(built_1.by.className('table-condensed table-responsive xs-mt-10'));
        var row = billUsage.all(built_1.by.tagName('th'));
        var cellTexts = row.map(function (ele) {
            return ele.getText();
        });
        return cellTexts;
    };
    BillDisaggPageObject.prototype.getCount = function () {
        built_1.element(built_1.by.className('btn btn-default chart-table-spacer icon__table')).click();
        var billDisaggtable = built_1.element(built_1.by.className('billCycle__table xs-mr-0 xs-p-15 xs-pt-0'));
        var tableRow = billDisaggtable.element(built_1.by.tagName('tbody')).all(built_1.by.tagName('tr'));
        var count = tableRow.count();
        return count;
    };
    BillDisaggPageObject.prototype.getPieChartFirstPortion = function () {
        var Portion = built_1.element(built_1.by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(built_1.by.tagName('path')).get(0);
        return Portion;
    };
    BillDisaggPageObject.prototype.getPieChartSecondPortion = function () {
        var Portion = built_1.element(built_1.by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(built_1.by.tagName('path')).get(1);
        return Portion;
    };
    BillDisaggPageObject.prototype.getPieChartThirdPortion = function () {
        var Portion = built_1.element(built_1.by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(built_1.by.tagName('path')).get(2);
        return Portion;
    };
    BillDisaggPageObject.prototype.getPieChartFourthPortion = function () {
        var Portion = built_1.element(built_1.by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(built_1.by.tagName('path')).get(3);
        return Portion;
    };
    BillDisaggPageObject.prototype.getPieChartFivfthPortion = function () {
        var Portion = built_1.element(built_1.by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(built_1.by.tagName('path')).get(4);
        return Portion;
    };
    BillDisaggPageObject.prototype.getPieChartSixthPortion = function () {
        var Portion = built_1.element(built_1.by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(built_1.by.tagName('path')).get(5);
        return Portion;
    };
    BillDisaggPageObject.prototype.getPieChartSeventhPortion = function () {
        var Portion = built_1.element(built_1.by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(built_1.by.tagName('path')).get(6);
        return Portion;
    };
    BillDisaggPageObject.prototype.getPieChartEighthPortion = function () {
        var Portion = built_1.element(built_1.by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(built_1.by.tagName('path')).get(7);
        return Portion;
    };
    BillDisaggPageObject.prototype.getElectricIcon = function () {
        built_1.element(built_1.by.className('icon icon-bolt')).click();
    };
    return BillDisaggPageObject;
}());
exports.BillDisaggPageObject = BillDisaggPageObject;
//# sourceMappingURL=BillDisagg.po.js.map