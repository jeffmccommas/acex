﻿import { browser, by, element } from 'protractor/built';
import { TokenGenerator } from '../e2e-helpers/TokenGenerator';


export class BillDisaggPageObject {
    

    navigateTo(custID, clientID, accID, premID) {

        let billDisaggUrl = new TokenGenerator();

        let token = billDisaggUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {
                
                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);
                
                browser.get(browser.baseUrl + 'Widget?id=tab.myusage-billdisagg&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');               
                console.log('url->' + browser.baseUrl + 'Widget?id=tab.myusage-billdisagg&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true&GroupName=CSR&RoleName=CSRAdmin');
                browser.sleep(1000);
            });
    }

    getAnnualUseByCategory() {
        return element(by.cssContainingText('.panel-title', 'Annual Use By Category'));
    }

    getHoverData() {

       var HoverData = element(by.className('highcharts-tooltip'));
       var Records = HoverData.element(by.tagName('text')).all(by.tagName('tspan'));

       var HovData = Records.map(function (data) {
           return data.getText();
       });
       return HovData;
    }

    getBillDisaggTable() {
        element(by.className('btn btn-default chart-table-spacer icon__table')).click();
        var billDisaggtable = element(by.className('billCycle__table xs-mr-0 xs-p-15 xs-pt-0'));
        var tableHead = billDisaggtable.element(by.tagName('thead')).all(by.tagName('th'));

        var rowsHeaders = tableHead.map(function (a) {
           return a.getText();
        });
        return rowsHeaders;
    }

    getBillDisaggTableData() {
        
        var billDisaggtable = element(by.className('billCycle__table xs-mr-0 xs-p-15 xs-pt-0'));
        var tableRow = billDisaggtable.element(by.tagName('tbody')).all(by.tagName('td'));

        var rowData = tableRow.map(function (data) {
            return data.getText();
        });
        return rowData;
    }

    getPercentOnlyArray() {

        var billUsage = element(by.className('table-condensed table-responsive xs-mt-10'));
        var row = billUsage.all(by.tagName('th'));

        var cellTexts = row.map(function (ele) {

            return ele.getText();

        });
        return cellTexts;
    }

    getCount() {
        element(by.className('btn btn-default chart-table-spacer icon__table')).click();
        
            var billDisaggtable = element(by.className('billCycle__table xs-mr-0 xs-p-15 xs-pt-0'));
            var tableRow = billDisaggtable.element(by.tagName('tbody')).all(by.tagName('tr'));
            var count = tableRow.count();
            return count;
    }
    getPieChartFirstPortion() {

        var Portion = element(by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(by.tagName('path')).get(0);
        return Portion;

    }

    getPieChartSecondPortion() {

        var Portion = element(by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(by.tagName('path')).get(1);
        return Portion;

    }
    getPieChartThirdPortion() {

        var Portion = element(by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(by.tagName('path')).get(2);
        return Portion;

    }

    getPieChartFourthPortion() {

        var Portion = element(by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(by.tagName('path')).get(3);
        return Portion;

    }
    getPieChartFivfthPortion() {

        var Portion = element(by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(by.tagName('path')).get(4);
        return Portion;

    }

    getPieChartSixthPortion() {

        var Portion = element(by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(by.tagName('path')).get(5);
        return Portion;

    }
    getPieChartSeventhPortion() {

        var Portion = element(by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(by.tagName('path')).get(6);
        return Portion;

    }

    getPieChartEighthPortion() {

        var Portion = element(by.className('highcharts-series highcharts-series-0 highcharts-tracker')).all(by.tagName('path')).get(7);
        return Portion;

    }

    getElectricIcon() {

        element(by.className('icon icon-bolt')).click();
        
    }
}

   

  /* getMyUsage() {
        return element(by.linkText('My Usage'));
    }
    selectMyUsage() {
         element(by.linkText('My Usage')).click();
    }*/

    /*selectNextKey() {
        browser.actions().sendKeys(Key.ARROW_RIGHT).perform();
    }

    selectPrevKey() {
        browser.actions().sendKeys(Key.ARROW_LEFT).perform();
    }

    selectEscapeKey() {
        browser.actions().sendKeys(Key.ESCAPE).perform();
    }
}*/