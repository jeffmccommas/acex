﻿import { browser, by, element } from 'protractor/built';
import { TokenGenerator } from '../e2e-helpers/TokenGenerator';

export class BillToDatePageObject {


    navigateTo(pageID, custID, clientID, accID, premID) {

        let billtoDateUrl = new TokenGenerator();

        let token = billtoDateUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);

                browser.get(browser.baseUrl + 'Page?id=' + pageID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
                console.log('url->' + browser.baseUrl + 'Page?id=' + pageID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
                browser.sleep(5000);
            });
    }

    getclientData() {
        var json = require('../TestData/BilltoDate/clientDetails.json');
        return json;
    }

    getNextBillTab() {
        browser.sleep(1000);
        var nextBillElement = element(by.cssContainingText('.nav-tab-title.active', 'Your Next Bill'));
        return nextBillElement;
    }
 

}