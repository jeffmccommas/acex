"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator_1 = require("../e2e-helpers/TokenGenerator");
var BillToDatePageObject = (function () {
    function BillToDatePageObject() {
    }
    BillToDatePageObject.prototype.navigateTo = function (pageID, custID, clientID, accID, premID) {
        var billtoDateUrl = new TokenGenerator_1.TokenGenerator();
        var token = billtoDateUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Page?id=' + pageID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            console.log('url->' + built_1.browser.baseUrl + 'Page?id=' + pageID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            built_1.browser.sleep(5000);
        });
    };
    BillToDatePageObject.prototype.getclientData = function () {
        var json = require('../TestData/BilltoDate/clientDetails.json');
        return json;
    };
    BillToDatePageObject.prototype.getNextBillTab = function () {
        built_1.browser.sleep(1000);
        var nextBillElement = built_1.element(built_1.by.cssContainingText('.nav-tab-title.active', 'Your Next Bill'));
        return nextBillElement;
    };
    return BillToDatePageObject;
}());
exports.BillToDatePageObject = BillToDatePageObject;
//# sourceMappingURL=BillDate.po.js.map