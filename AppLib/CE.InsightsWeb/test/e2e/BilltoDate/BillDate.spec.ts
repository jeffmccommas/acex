﻿'Use strict';

import { BillToDatePageObject } from './BillDate.po';

describe('BillToDate', () => {
    let page: BillToDatePageObject;

    beforeEach(() => {
        page = new BillToDatePageObject();
    });


    it('Bill to Date navigate to active NextBill tab when billtodatev1 returns data', () => {

        var clientData = page.getclientData();
        page.navigateTo(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);

        var btdElement = page.getNextBillTab();
        expect(btdElement.getText()).toContain('Your Next Bill');
    });


});