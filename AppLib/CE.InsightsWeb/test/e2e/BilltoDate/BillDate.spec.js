'Use strict';
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BillDate_po_1 = require("./BillDate.po");
describe('BillToDate', function () {
    var page;
    beforeEach(function () {
        page = new BillDate_po_1.BillToDatePageObject();
    });
    it('Bill to Date navigate to active NextBill tab when billtodatev1 returns data', function () {
        var clientData = page.getclientData();
        page.navigateTo(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var btdElement = page.getNextBillTab();
        expect(btdElement.getText()).toContain('Your Next Bill');
    });
});
//# sourceMappingURL=BillDate.spec.js.map