﻿import { browser, by, element } from 'protractor/built';
import { TokenGenerator } from '../e2e-helpers/TokenGenerator';

export class BillHistoryPageObject {


    navigateTo(custID, clientID, accID, premID) {

        let billHistoryUrl = new TokenGenerator();

        let token = billHistoryUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);
                //browser.waitForAngularEnabled(false);

                browser.get(browser.baseUrl + 'Page?id=110&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
                console.log('url->' + browser.baseUrl + 'Page?id=110&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            });
    }

    getElecOnly() {

        var electricUseElement = element(by.css('.account-select__info'));
        var cells = electricUseElement.element(by.className('col-md-4 xs-pr-0 xs-pl-0 xs-pb-10'));
        return cells;
        
    }

    getElectOnlyArray() {
        browser.sleep(2000);
        var billHistory = element(by.className('billing-history table-responsive'));
        var row = billHistory.all(by.tagName('th'));

        var cellTexts = row.map(function (elm) {
            return elm.getText();
        });
        return cellTexts;

    }

    getNoBills() {
        browser.sleep(2000);
        var electricUseElement = element(by.cssContainingText('.alert-warning', 'No bills are available.'));
        return electricUseElement;
    }

    getNoCompareLinkActions() {

        browser.sleep(2000);
        var billHistory = element(by.className('billing-history table-responsive'));
        var lastHeader = billHistory.all(by.tagName('th')).last();
        return lastHeader;
    }

    getNoCompareLinkRow() {

        browser.sleep(2000);
        var billHistory = element(by.className('billing-history table-responsive'));
        var tableRow = billHistory.element(by.tagName('tbody')).all(by.tagName('tr'));

        return tableRow;
    }

    getNoCompareLinkOnLastRow() {

        browser.sleep(2000);
        var billHistory = element(by.className('billing-history table-responsive'));
        var footer = billHistory.element(by.tagName('tfoot')).all(by.tagName('img'));


        footer.get(1).isEnabled().then(function (value) {

            if (value) {
                footer.get(1).click();
            }
            else {
                return;
            }
        });
        
        var tableRow = billHistory.element(by.tagName('tbody')).all(by.tagName('tr'));
        var lastRowData = tableRow.last().all(by.tagName('td'));

        return lastRowData;
    }

    getLastBeforeRowData(){

        var tableRow = this.getNoCompareLinkRow();
        var lastRowData = this.getNoCompareLinkOnLastRow();
        var lastBeforeRowData = tableRow.get(-2).all(by.tagName('td'));
        return lastBeforeRowData;

    }
}