'Use strict';
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BillHistory_po_1 = require("./BillHistory.po");
describe('BillHistory', function () {
    var page;
    beforeEach(function () {
        page = new BillHistory_po_1.BillHistoryPageObject();
    });
    it('Should show Electric Use Element in Bill History', function () {
        var custID = '9000E1';
        var clientID = '87';
        var accID = '9000E1';
        var premID = '9000E1P';
        page.navigateTo(custID, clientID, accID, premID);
        var ElecOnly = page.getElecOnly();
        var ElecOnlyArray = page.getElectOnlyArray();
        expect(ElecOnly.getText()).toEqual('Electric');
        expect(ElecOnlyArray).toEqual(["Bill Date", "Electric", "Total", "Actions"]);
    });
    it('Should display No bills are available message in Bill History', function () {
        var custID = 'QATestProfileNoBill';
        var clientID = '87';
        var accID = 'QATestProfileNoBill';
        var premID = 'QATestProfileNoBill';
        page.navigateTo(custID, clientID, accID, premID);
        var NoBills = page.getNoBills();
        expect(NoBills.getText()).toMatch('No bills are available.');
    });
    it('Should not display Compare link on widget in Bill History', function () {
        var custID = '993774751';
        var clientID = '256';
        var accID = '1218337093';
        var premID = '1218337000';
        page.navigateTo(custID, clientID, accID, premID);
        var lastHeader = page.getNoCompareLinkActions();
        var tableRow = page.getNoCompareLinkRow();
        expect(lastHeader.getText()).not.toEqual('Actions');
        tableRow.count().then(function (value) {
            while (value - 1 != 0) {
                expect(tableRow.get(value - 1).all(by.tagName('td')).last().getText()).toEqual("");
                value = value - 1;
            }
        });
    });
    it('Should not display Compare Link on last row (earliest date) in Bill History', function () {
        var custID = '993774751';
        var clientID = '87';
        var accID = '1218337093';
        var premID = '1218337000';
        page.navigateTo(custID, clientID, accID, premID);
        var lastRowData = page.getNoCompareLinkOnLastRow();
        var lastBeforeRowData = page.getLastBeforeRowData();
        expect(lastRowData.last().getText()).toEqual('');
        expect(lastBeforeRowData.last().getText()).toEqual('Compare');
        lastBeforeRowData.last().isEnabled();
    });
});
//# sourceMappingURL=BillHistory.spec.js.map