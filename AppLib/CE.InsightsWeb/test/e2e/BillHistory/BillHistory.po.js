"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator_1 = require("../e2e-helpers/TokenGenerator");
var BillHistoryPageObject = (function () {
    function BillHistoryPageObject() {
    }
    BillHistoryPageObject.prototype.navigateTo = function (custID, clientID, accID, premID) {
        var billHistoryUrl = new TokenGenerator_1.TokenGenerator();
        var token = billHistoryUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Page?id=110&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            console.log('url->' + built_1.browser.baseUrl + 'Page?id=110&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
        });
    };
    BillHistoryPageObject.prototype.getElecOnly = function () {
        var electricUseElement = built_1.element(built_1.by.css('.account-select__info'));
        var cells = electricUseElement.element(built_1.by.className('col-md-4 xs-pr-0 xs-pl-0 xs-pb-10'));
        return cells;
    };
    BillHistoryPageObject.prototype.getElectOnlyArray = function () {
        built_1.browser.sleep(2000);
        var billHistory = built_1.element(built_1.by.className('billing-history table-responsive'));
        var row = billHistory.all(built_1.by.tagName('th'));
        var cellTexts = row.map(function (elm) {
            return elm.getText();
        });
        return cellTexts;
    };
    BillHistoryPageObject.prototype.getNoBills = function () {
        built_1.browser.sleep(2000);
        var electricUseElement = built_1.element(built_1.by.cssContainingText('.alert-warning', 'No bills are available.'));
        return electricUseElement;
    };
    BillHistoryPageObject.prototype.getNoCompareLinkActions = function () {
        built_1.browser.sleep(2000);
        var billHistory = built_1.element(built_1.by.className('billing-history table-responsive'));
        var lastHeader = billHistory.all(built_1.by.tagName('th')).last();
        return lastHeader;
    };
    BillHistoryPageObject.prototype.getNoCompareLinkRow = function () {
        built_1.browser.sleep(2000);
        var billHistory = built_1.element(built_1.by.className('billing-history table-responsive'));
        var tableRow = billHistory.element(built_1.by.tagName('tbody')).all(built_1.by.tagName('tr'));
        return tableRow;
    };
    BillHistoryPageObject.prototype.getNoCompareLinkOnLastRow = function () {
        built_1.browser.sleep(2000);
        var billHistory = built_1.element(built_1.by.className('billing-history table-responsive'));
        var footer = billHistory.element(built_1.by.tagName('tfoot')).all(built_1.by.tagName('img'));
        footer.get(1).isEnabled().then(function (value) {
            if (value) {
                footer.get(1).click();
            }
            else {
                return;
            }
        });
        var tableRow = billHistory.element(built_1.by.tagName('tbody')).all(built_1.by.tagName('tr'));
        var lastRowData = tableRow.last().all(built_1.by.tagName('td'));
        return lastRowData;
    };
    BillHistoryPageObject.prototype.getLastBeforeRowData = function () {
        var tableRow = this.getNoCompareLinkRow();
        var lastRowData = this.getNoCompareLinkOnLastRow();
        var lastBeforeRowData = tableRow.get(-2).all(built_1.by.tagName('td'));
        return lastBeforeRowData;
    };
    return BillHistoryPageObject;
}());
exports.BillHistoryPageObject = BillHistoryPageObject;
//# sourceMappingURL=BillHistory.po.js.map