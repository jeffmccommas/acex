﻿'Use strict';

import { BillHistoryPageObject } from './BillHistory.po';

describe('BillHistory', () => {
    let page: BillHistoryPageObject;

    beforeEach(() => {
        page = new BillHistoryPageObject();
    });

    it('Should show Electric Use Element in Bill History', () => {
        var custID = '9000E1';
        var clientID = '87';
        var accID = '9000E1';
        var premID = '9000E1P';

        page.navigateTo(custID, clientID, accID, premID);
        var ElecOnly = page.getElecOnly();
        var ElecOnlyArray = page.getElectOnlyArray();

        //browser.sleep(2000);
        //element(by.linkText('My Bills')).click();

        //browser.sleep(2000);
        //element(by.css('.pills')).element(by.id('tab_110')).element(by.css('.navbar-link')).click();

        //browser.sleep(2000);
        //var electricUseElement = element(by.css('.account-select__info'));
        //var cells = electricUseElement.element(by.className('col-md-4 xs-pr-0 xs-pl-0 xs-pb-10'));

        //var billHistory = element(by.className('billing-history table-responsive'));
        //var row = billHistory.all(by.tagName('th'));

        //var cellTexts = row.map(function (elm) {
        //    return elm.getText();
        //});

        expect(ElecOnly.getText()).toEqual('Electric');

        expect(ElecOnlyArray).toEqual(["Bill Date", "Electric", "Total", "Actions"]);
        

    });

    it('Should display No bills are available message in Bill History', () => {
        var custID = 'QATestProfileNoBill';
        var clientID = '87';
        var accID = 'QATestProfileNoBill';
        var premID = 'QATestProfileNoBill';

        page.navigateTo(custID, clientID, accID, premID);
        var NoBills = page.getNoBills();
        expect(NoBills.getText()).toMatch('No bills are available.');

    });

    it('Should not display Compare link on widget in Bill History', () => {

        var custID = '993774751';
        var clientID = '256';
        var accID = '1218337093';
        var premID = '1218337000';

        page.navigateTo(custID, clientID, accID, premID);
        var lastHeader = page.getNoCompareLinkActions();
        var tableRow = page.getNoCompareLinkRow();

        expect(lastHeader.getText()).not.toEqual('Actions');

        tableRow.count().then(function (value) {
            while (value - 1 != 0) {
                expect(tableRow.get(value - 1).all(by.tagName('td')).last().getText()).toEqual("");
                value = value - 1;
            }

        });

    });

    it('Should not display Compare Link on last row (earliest date) in Bill History', () => {

        var custID = '993774751';
        var clientID = '87';
        var accID = '1218337093';
        var premID = '1218337000';

        page.navigateTo(custID, clientID, accID, premID);
        var lastRowData = page.getNoCompareLinkOnLastRow();
        var lastBeforeRowData = page.getLastBeforeRowData();
        expect(lastRowData.last().getText()).toEqual('');
        expect(lastBeforeRowData.last().getText()).toEqual('Compare');
        lastBeforeRowData.last().isEnabled();

    });

});

