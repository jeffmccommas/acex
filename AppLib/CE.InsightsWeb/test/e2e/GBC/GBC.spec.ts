﻿'Use strict';

import { GBCPageObject } from './GBC.po';


describe('GBC', () => {
    let page: GBCPageObject;

    beforeEach(() => {
        page = new GBCPageObject();
    });

    
    //var clientData = page.getCilentData();
    //var GBCappnames = page.getGBCAppNames();

   /* it('Verify the GBC and GBC Admin Third Party App Items', () => {
        var custID = '993774751';
        var clientID = '87';
        var accID = '1218337093';
        var premID = '1218337000';
        var pageID = '8';

        page.getGBCAdminPage(custID, clientID, accID, premID);
        browser.sleep(2000);

        var status = page.getAppStatusGBCAdmin();
        var appname = page.getGBCAdminAppName();

        if (expect(status.getText()).toMatch('true')) {
            if (expect(appname.getText()).toMatch('Toast Analytics')) {

                page.navigateToGBCPage(pageID, custID, clientID, accID, premID);

                var gasUseElement = element(by.cssContainingText('.gbc__item', 'Toast Analytics'));
                expect(gasUseElement.getText()).toMatch("Toast Analytics");
            }


            //else if (expect(appname.getText()).toMatch('Test Client')) {

            //    page.navigateTo(pageID, custID, clientID, accID, premID);
            //    var GBCappname = page.getGBCAppName();
            //    expect(GBCappname.getText()).toMatch('Test Client');
            //}
        }
       
    });
    
    */
    it('Revoke a third party app on GBC Admin Page', function () {

        var clientData = page.getCilentData();
        var GBCappnames = page.getGBCAppNames();
        page.navigateToGBCPage(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var url = browser.baseUrl;
        browser.getCurrentUrl().then(function (url) {
          // console.info("----=---> " + url);
           if (url === 'https://acewebsitedev.aclarax.com') {

               var name = page.getGBCappList();
               expect(name).toEqual([GBCappnames.DevAppNames[0], GBCappnames.DevAppNames[1]]);
           }
           else if (url === 'https://acewebsiteqa.aclarax.com') {

               var name = page.getGBCappList();
            expect(name).toEqual([GBCappnames.QAAppNames[0], GBCappnames.QAAppNames[1]]);

           }
           else if (url === 'https://acewebsiteuat.aclarax.com') {

               var name = page.getGBCappList();
               expect(name).toEqual([GBCappnames.UATAppNames[0], GBCappnames.UATAppNames[1], GBCappnames.UATAppNames[2]]);
           }

        });


        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        browser.sleep(5000);
        page.getGBCAdminSelectApp();
        page.getGBCAdminRevokeApp();
        page.getSubmitBtn();
        page.getConfirmationBtn();
        
    });

    it('Verfiy if the status of the third party app on GBC Admin Page is marked as false', function () {

        var clientData = page.getCilentData();
        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var status = page.getAppStatusGBCAdmin();
        expect(status.getText()).toMatch('false');
        

    });


    it('Verify that revoked app isnt not visible on GBC page', function () {

        var clientData = page.getCilentData();
        var GBCappnames = page.getGBCAppNames();
        page.navigateToGBCPage(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);

    var url = browser.baseUrl;
    browser.getCurrentUrl().then(function (url) {
        if (url === 'https://acewebsitedev.aclarax.com') {

            var name = page.getGBCappList();
            expect(name).toEqual(GBCappnames.DevAppNames[1]);
        }

        else if (url === 'https://acewebsiteqa.aclarax.com') {

            var name = page.getGBCappList();
            expect(name).toEqual(GBCappnames.QAAppNames[1]);

        }
        else if (url === 'https://acewebsiteuat.aclarax.com') {

            var name = page.getGBCappList();
            expect(name).toEqual([GBCappnames.UATAppNames[1], GBCappnames.UATAppNames[2]]);
        }
    });

    });

    it('Set the application  data custodian status to default/Production', function () {

        var clientData = page.getCilentData();
        var GBCappnames = page.getGBCAppNames();
        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);

        page.getGBCAdminSelectApp();
        page.getGBCAdminCheckEnabledStatus();
        page.getGBCAdminProductionStatus();
        page.getSubmitBtn();
    });

    it('Verfiy if the status of the third party app on GBC Admin Page is marked as True', function () {

        var clientData = page.getCilentData();
        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var status = page.getAppStatusGBCAdmin();
        expect(status.getText()).toMatch('true');

    });

    it('Verify that revoked app is back in production and visible on GBC page', function () {

        var clientData = page.getCilentData();
        var GBCappnames = page.getGBCAppNames();
        page.navigateToGBCPage(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var url = browser.baseUrl;
        browser.getCurrentUrl().then(function (url) {
            if (url === 'https://acewebsitedev.aclarax.com') {

                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.DevAppNames[0], GBCappnames.DevAppNames[1]]);
            }
            else if (url === 'https://acewebsiteqa.aclarax.com') {

                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.QAAppNames[0], GBCappnames.QAAppNames[1]]);

            }
            else if (url === 'https://acewebsiteuat.aclarax.com') {

                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.UATAppNames[0], GBCappnames.UATAppNames[1], GBCappnames.UATAppNames[2]]);
            }
        });
    });

    it('Disables a third party app on GBC Admin Page', function () {

        var clientData = page.getCilentData();
        var GBCappnames = page.getGBCAppNames();
        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        page.getGBCAdminSelectApp();
        page.getGBCAdminUncheckEnabledStatus();
        page.getSubmitBtn();
    });

    it('Verfiy if the status of the third party app on GBC Admin Page is marked as false/Disabled', function () {

        var clientData = page.getCilentData();
        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var status = page.getAppStatusGBCAdmin();
        expect(status.getText()).toMatch('false');


    });

    it('Verify that Disabled app is not visible on GBC page', function () {

        var clientData = page.getCilentData();
        var GBCappnames = page.getGBCAppNames();
        page.navigateToGBCPage(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);

        var url = browser.baseUrl;
        browser.getCurrentUrl().then(function (url) {
            if (url === 'https://acewebsitedev.aclarax.com') {

                var name = page.getGBCappList();
                expect(name).toEqual(GBCappnames.DevAppNames[1]);
            }

            else if (url === 'https://acewebsiteqa.aclarax.com') {

                var name = page.getGBCappList();
                expect(name).toEqual(GBCappnames.QAAppNames[1]);

            }
            else if (url === 'https://acewebsiteuat.aclarax.com') {

                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.UATAppNames[1], GBCappnames.UATAppNames[2]]);
            }
        });
    });

    it('Enables a third party app on GBC Admin Page', function () {

        var clientData = page.getCilentData();
        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);

        page.getGBCAdminSelectApp();
        page.getGBCAdminCheckEnabledStatus();
        page.getSubmitBtn();
    });

    it('Verfiy if the status of the third party app on GBC Admin Page is marked as True/Enabled', function () {

        var clientData = page.getCilentData();
        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var status = page.getAppStatusGBCAdmin();
        expect(status.getText()).toMatch('true');

    });

    it('Verify that Enabled app is visible on GBC page again', function () {

    var clientData = page.getCilentData();
    var GBCappnames = page.getGBCAppNames();
        page.navigateToGBCPage(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);

        var url = browser.baseUrl;
        browser.getCurrentUrl().then(function (url) {
            if (url === 'https://acewebsitedev.aclarax.com') {

                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.DevAppNames[0], GBCappnames.DevAppNames[1]]);
            }
            else if (url === 'https://acewebsiteqa.aclarax.com') {

                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.QAAppNames[0], GBCappnames.QAAppNames[1]]);

            }
            else if (url === 'https://acewebsiteuat.aclarax.com') {

                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.UATAppNames[0], GBCappnames.UATAppNames[1], GBCappnames.UATAppNames[2]]);
            }
        });


    });
});
