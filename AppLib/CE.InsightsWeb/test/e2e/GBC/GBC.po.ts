﻿import { browser, by, element } from 'protractor/built';
import { TokenGenerator } from '../e2e-helpers/TokenGenerator';
import { GBCAdminPageObject } from '../GBCAdmin/GBCAdmin.po';

export class GBCPageObject {


    navigateToGBCPage(pageID, custID, clientID, accID, premID) {

        let gbcUrl = new TokenGenerator();

        let token = gbcUrl.getToken(custID, clientID, accID, premID);


        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);

                browser.get(browser.baseUrl + 'Page?id=' + pageID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&Locale=en-US&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true' + '&tmpl=gbc');
                console.log('url->' + browser.baseUrl + 'Page?id=' + pageID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&Locale=en-US&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true' + '&tmpl=gbc');
                browser.sleep(8000);
            });
    }

    getCilentData() {
        var json = require('../TestData/GBC/GBCclient.json');
        return json;
    }

    getGBCAppNames() {
        var json = require('../TestData/GBC/GBCAppNames.json');
        return json;
    }

    getGBCAdminPage(custID, clientID, accID, premID) {

        let GbcAdminPage = new GBCAdminPageObject();

        let page = GbcAdminPage.navigateTo(custID, clientID, accID, premID);
    }

    getGBCAdminAppName() {

        let GbcAdminPage = new GBCAdminPageObject();
        let appname = GbcAdminPage.getThirdPartyNameStatusTrue();

        return appname;

    }

    getGBCAppName() {
        //var appname = element(by.id('iws_rc_pagewidget_4')).all(by.className('gbc__item xs-m-15')).get(0);
        var name = element(by.className('gbc')).all(by.tagName('strong')).get(1);
        return name;

    }

    getAppStatusGBCAdmin() {

        let GbcAdminPage = new GBCAdminPageObject();
        let appname = GbcAdminPage.getThirdPartyListEnabledStatus();

        return appname;

    }

    getGBCappList() {

        var app = element(by.className('gbc')).all(by.tagName('h2'));
        var appname = app.all(by.tagName('strong'));

        var cellTexts = appname.map(function (elm) {
            return elm.getText();
        });
        return cellTexts;

    }

    getGBCAdminSelectApp() {

        let GbcAdminPage = new GBCAdminPageObject();
        let app = GbcAdminPage.getGBCTestThirdPartyApp();

        return app;

    }

    //getGBCAdminRevokeAppDropDown() {

    //    let GbcAdminPage = new GBCAdminPageObject();
    //    let dropdwn = GbcAdminPage.getApplicationStatusDropdown();

    //    return dropdwn;

    //}

    getGBCAdminRevokeApp() {

        let GbcAdminPage = new GBCAdminPageObject();
        let status = GbcAdminPage.getDataCustodianApplicationStatusRevoked().click();

        return status;

    }

    getGBCAdminProductionStatus() {

        let GbcAdminPage = new GBCAdminPageObject();
        let status = GbcAdminPage.getDataCustodianApplicationStatusProduction().click();

        return status;

    }

    getGBCAdminCheckEnabledStatus() {

        let GbcAdminPage = new GBCAdminPageObject();
        let enabledTrue = GbcAdminPage.getCheckEnabled();

        return enabledTrue;

    }

    getGBCAdminUncheckEnabledStatus() {

        let GbcAdminPage = new GBCAdminPageObject();
        let enabledFalse = GbcAdminPage.getUncheckEnabled();

        return enabledFalse;

    }
    getSubmitBtn() {

        let GbcAdminPage = new GBCAdminPageObject();
        let SubmitForm = GbcAdminPage.getSubmitBtnEnabled().click();

        return SubmitForm;

    }

    getConfirmationBtn() {

        let GbcAdminPage = new GBCAdminPageObject();
        let Confirm = GbcAdminPage.getConfirmBtnEnabledBeforeRevokingApp().click();

        return Confirm;

    }

}

