"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator_1 = require("../e2e-helpers/TokenGenerator");
var GBCAdmin_po_1 = require("../GBCAdmin/GBCAdmin.po");
var GBCPageObject = (function () {
    function GBCPageObject() {
    }
    GBCPageObject.prototype.navigateToGBCPage = function (pageID, custID, clientID, accID, premID) {
        var gbcUrl = new TokenGenerator_1.TokenGenerator();
        var token = gbcUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Page?id=' + pageID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&Locale=en-US&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true' + '&tmpl=gbc');
            console.log('url->' + built_1.browser.baseUrl + 'Page?id=' + pageID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&Locale=en-US&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true' + '&tmpl=gbc');
            built_1.browser.sleep(8000);
        });
    };
    GBCPageObject.prototype.getCilentData = function () {
        var json = require('../TestData/GBC/GBCclient.json');
        return json;
    };
    GBCPageObject.prototype.getGBCAppNames = function () {
        var json = require('../TestData/GBC/GBCAppNames.json');
        return json;
    };
    GBCPageObject.prototype.getGBCAdminPage = function (custID, clientID, accID, premID) {
        var GbcAdminPage = new GBCAdmin_po_1.GBCAdminPageObject();
        var page = GbcAdminPage.navigateTo(custID, clientID, accID, premID);
    };
    GBCPageObject.prototype.getGBCAdminAppName = function () {
        var GbcAdminPage = new GBCAdmin_po_1.GBCAdminPageObject();
        var appname = GbcAdminPage.getThirdPartyNameStatusTrue();
        return appname;
    };
    GBCPageObject.prototype.getGBCAppName = function () {
        var name = built_1.element(built_1.by.className('gbc')).all(built_1.by.tagName('strong')).get(1);
        return name;
    };
    GBCPageObject.prototype.getAppStatusGBCAdmin = function () {
        var GbcAdminPage = new GBCAdmin_po_1.GBCAdminPageObject();
        var appname = GbcAdminPage.getThirdPartyListEnabledStatus();
        return appname;
    };
    GBCPageObject.prototype.getGBCappList = function () {
        var app = built_1.element(built_1.by.className('gbc')).all(built_1.by.tagName('h2'));
        var appname = app.all(built_1.by.tagName('strong'));
        var cellTexts = appname.map(function (elm) {
            return elm.getText();
        });
        return cellTexts;
    };
    GBCPageObject.prototype.getGBCAdminSelectApp = function () {
        var GbcAdminPage = new GBCAdmin_po_1.GBCAdminPageObject();
        var app = GbcAdminPage.getGBCTestThirdPartyApp();
        return app;
    };
    GBCPageObject.prototype.getGBCAdminRevokeApp = function () {
        var GbcAdminPage = new GBCAdmin_po_1.GBCAdminPageObject();
        var status = GbcAdminPage.getDataCustodianApplicationStatusRevoked().click();
        return status;
    };
    GBCPageObject.prototype.getGBCAdminProductionStatus = function () {
        var GbcAdminPage = new GBCAdmin_po_1.GBCAdminPageObject();
        var status = GbcAdminPage.getDataCustodianApplicationStatusProduction().click();
        return status;
    };
    GBCPageObject.prototype.getGBCAdminCheckEnabledStatus = function () {
        var GbcAdminPage = new GBCAdmin_po_1.GBCAdminPageObject();
        var enabledTrue = GbcAdminPage.getCheckEnabled();
        return enabledTrue;
    };
    GBCPageObject.prototype.getGBCAdminUncheckEnabledStatus = function () {
        var GbcAdminPage = new GBCAdmin_po_1.GBCAdminPageObject();
        var enabledFalse = GbcAdminPage.getUncheckEnabled();
        return enabledFalse;
    };
    GBCPageObject.prototype.getSubmitBtn = function () {
        var GbcAdminPage = new GBCAdmin_po_1.GBCAdminPageObject();
        var SubmitForm = GbcAdminPage.getSubmitBtnEnabled().click();
        return SubmitForm;
    };
    GBCPageObject.prototype.getConfirmationBtn = function () {
        var GbcAdminPage = new GBCAdmin_po_1.GBCAdminPageObject();
        var Confirm = GbcAdminPage.getConfirmBtnEnabledBeforeRevokingApp().click();
        return Confirm;
    };
    return GBCPageObject;
}());
exports.GBCPageObject = GBCPageObject;
//# sourceMappingURL=GBC.po.js.map