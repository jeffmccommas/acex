'Use strict';
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GBC_po_1 = require("./GBC.po");
describe('GBC', function () {
    var page;
    beforeEach(function () {
        page = new GBC_po_1.GBCPageObject();
    });
    it('Revoke a third party app on GBC Admin Page', function () {
        var clientData = page.getCilentData();
        var GBCappnames = page.getGBCAppNames();
        page.navigateToGBCPage(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var url = browser.baseUrl;
        browser.getCurrentUrl().then(function (url) {
            if (url === 'https://acewebsitedev.aclarax.com') {
                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.DevAppNames[0], GBCappnames.DevAppNames[1]]);
            }
            else if (url === 'https://acewebsiteqa.aclarax.com') {
                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.QAAppNames[0], GBCappnames.QAAppNames[1]]);
            }
            else if (url === 'https://acewebsiteuat.aclarax.com') {
                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.UATAppNames[0], GBCappnames.UATAppNames[1], GBCappnames.UATAppNames[2]]);
            }
        });
        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        browser.sleep(5000);
        page.getGBCAdminSelectApp();
        page.getGBCAdminRevokeApp();
        page.getSubmitBtn();
        page.getConfirmationBtn();
    });
    it('Verfiy if the status of the third party app on GBC Admin Page is marked as false', function () {
        var clientData = page.getCilentData();
        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var status = page.getAppStatusGBCAdmin();
        expect(status.getText()).toMatch('false');
    });
    it('Verify that revoked app isnt not visible on GBC page', function () {
        var clientData = page.getCilentData();
        var GBCappnames = page.getGBCAppNames();
        page.navigateToGBCPage(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var url = browser.baseUrl;
        browser.getCurrentUrl().then(function (url) {
            if (url === 'https://acewebsitedev.aclarax.com') {
                var name = page.getGBCappList();
                expect(name).toEqual(GBCappnames.DevAppNames[1]);
            }
            else if (url === 'https://acewebsiteqa.aclarax.com') {
                var name = page.getGBCappList();
                expect(name).toEqual(GBCappnames.QAAppNames[1]);
            }
            else if (url === 'https://acewebsiteuat.aclarax.com') {
                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.UATAppNames[1], GBCappnames.UATAppNames[2]]);
            }
        });
    });
    it('Set the application  data custodian status to default/Production', function () {
        var clientData = page.getCilentData();
        var GBCappnames = page.getGBCAppNames();
        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        page.getGBCAdminSelectApp();
        page.getGBCAdminCheckEnabledStatus();
        page.getGBCAdminProductionStatus();
        page.getSubmitBtn();
    });
    it('Verfiy if the status of the third party app on GBC Admin Page is marked as True', function () {
        var clientData = page.getCilentData();
        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var status = page.getAppStatusGBCAdmin();
        expect(status.getText()).toMatch('true');
    });
    it('Verify that revoked app is back in production and visible on GBC page', function () {
        var clientData = page.getCilentData();
        var GBCappnames = page.getGBCAppNames();
        page.navigateToGBCPage(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var url = browser.baseUrl;
        browser.getCurrentUrl().then(function (url) {
            if (url === 'https://acewebsitedev.aclarax.com') {
                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.DevAppNames[0], GBCappnames.DevAppNames[1]]);
            }
            else if (url === 'https://acewebsiteqa.aclarax.com') {
                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.QAAppNames[0], GBCappnames.QAAppNames[1]]);
            }
            else if (url === 'https://acewebsiteuat.aclarax.com') {
                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.UATAppNames[0], GBCappnames.UATAppNames[1], GBCappnames.UATAppNames[2]]);
            }
        });
    });
    it('Disables a third party app on GBC Admin Page', function () {
        var clientData = page.getCilentData();
        var GBCappnames = page.getGBCAppNames();
        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        page.getGBCAdminSelectApp();
        page.getGBCAdminUncheckEnabledStatus();
        page.getSubmitBtn();
    });
    it('Verfiy if the status of the third party app on GBC Admin Page is marked as false/Disabled', function () {
        var clientData = page.getCilentData();
        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var status = page.getAppStatusGBCAdmin();
        expect(status.getText()).toMatch('false');
    });
    it('Verify that Disabled app is not visible on GBC page', function () {
        var clientData = page.getCilentData();
        var GBCappnames = page.getGBCAppNames();
        page.navigateToGBCPage(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var url = browser.baseUrl;
        browser.getCurrentUrl().then(function (url) {
            if (url === 'https://acewebsitedev.aclarax.com') {
                var name = page.getGBCappList();
                expect(name).toEqual(GBCappnames.DevAppNames[1]);
            }
            else if (url === 'https://acewebsiteqa.aclarax.com') {
                var name = page.getGBCappList();
                expect(name).toEqual(GBCappnames.QAAppNames[1]);
            }
            else if (url === 'https://acewebsiteuat.aclarax.com') {
                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.UATAppNames[1], GBCappnames.UATAppNames[2]]);
            }
        });
    });
    it('Enables a third party app on GBC Admin Page', function () {
        var clientData = page.getCilentData();
        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        page.getGBCAdminSelectApp();
        page.getGBCAdminCheckEnabledStatus();
        page.getSubmitBtn();
    });
    it('Verfiy if the status of the third party app on GBC Admin Page is marked as True/Enabled', function () {
        var clientData = page.getCilentData();
        page.getGBCAdminPage(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var status = page.getAppStatusGBCAdmin();
        expect(status.getText()).toMatch('true');
    });
    it('Verify that Enabled app is visible on GBC page again', function () {
        var clientData = page.getCilentData();
        var GBCappnames = page.getGBCAppNames();
        page.navigateToGBCPage(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        var url = browser.baseUrl;
        browser.getCurrentUrl().then(function (url) {
            if (url === 'https://acewebsitedev.aclarax.com') {
                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.DevAppNames[0], GBCappnames.DevAppNames[1]]);
            }
            else if (url === 'https://acewebsiteqa.aclarax.com') {
                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.QAAppNames[0], GBCappnames.QAAppNames[1]]);
            }
            else if (url === 'https://acewebsiteuat.aclarax.com') {
                var name = page.getGBCappList();
                expect(name).toEqual([GBCappnames.UATAppNames[0], GBCappnames.UATAppNames[1], GBCappnames.UATAppNames[2]]);
            }
        });
    });
});
//# sourceMappingURL=GBC.spec.js.map