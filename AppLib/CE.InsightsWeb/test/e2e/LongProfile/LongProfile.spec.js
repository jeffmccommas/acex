'Use strict';
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LongProfile_po_1 = require("./LongProfile.po");
describe('LongProfile', function () {
    var page;
    beforeEach(function () {
        page = new LongProfile_po_1.LongProfilePageObject();
    });
    it('Should navigate to My Savings(Long Profile) tab', function () {
        var clientData = page.getClientData();
        page.navigateTo(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);
        browser.sleep(10000);
    });
    it('Your Home page 1, test if the focus set on first question', function () {
        element(by.linkText("Your Home")).click();
        browser.sleep(5000);
        page.checkSetFocusOnFirstQuestion();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Your Home page 2, test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Appliances page 1 of 8(Refrigerator), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Appliances page 2 of 8(Secondary Refrigerator), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Appliances page 3 of 8(Washing Machine), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Appliances page 4 of 8(Dryer), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Appliances page 5 of 8(Cooking), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Appliances page 6 of 8(Dishwasher), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Appliances page 7 of 8(Televisions), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Appliances page 8 of 8(Computers), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Heating and Cooling page 1 of 4(Heating), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Heating and Cooling page 2 of 4(Secondry Heating), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Heating and Cooling page 3 of 4(Insulation), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Heating and Cooling page 4 of 4(Central Air Conditioning), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Lighting page 1 of 1, test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Water page 1 of 6( Water Heating), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Water page 2 of 6(Showers), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Water page 3 of 6(Lawn Care), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Water page 4 of 6(Well Pump), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Water page 5 of 6(Garden Care), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Water page 6 of 6(Faucets and Toilets), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Miscellaneous page 1 of 4(Other Kitchen Appliances), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Miscellaneous page 2 of 4(Your Health), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Miscellaneous page 3 of 4(Comfort), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(2000);
        page.clickForwardButton();
        browser.sleep(5000);
    });
    it('Miscellaneous page 4 of 4(Other), test if the focus is set on first question and the page scrolls up', function () {
        page.checkSetFocusOnFirstQuestion();
        page.checkScrollUpFunction();
        page.scrollFullDown();
        browser.sleep(5000);
    });
});
//# sourceMappingURL=LongProfile.spec.js.map