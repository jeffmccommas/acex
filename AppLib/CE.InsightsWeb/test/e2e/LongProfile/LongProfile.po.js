"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator_1 = require("../e2e-helpers/TokenGenerator");
var LongProfilePageObject = (function () {
    function LongProfilePageObject() {
    }
    LongProfilePageObject.prototype.navigateTo = function (custID, clientID, accID, premID) {
        var longProfileUrl = new TokenGenerator_1.TokenGenerator();
        var token = longProfileUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Page?id=12&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            console.log('url->' + built_1.browser.baseUrl + 'Page?id=12&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
        });
    };
    LongProfilePageObject.prototype.getClientData = function () {
        var json = require('../TestData/LongProfile/LongProfileClients.json');
        return json;
    };
    LongProfilePageObject.prototype.checkSetFocusOnFirstQuestion = function () {
        var checkSetFocus = built_1.element(built_1.by.id('focus-0')).equals(built_1.browser.driver.switchTo().activeElement())
            .then(function (equals) { return expect(equals).toBe(true); });
        return checkSetFocus;
    };
    LongProfilePageObject.prototype.clickForwardButton = function () {
        var clickForward = built_1.element(built_1.by.xpath("//img[@src='Content/Images/icon-page-right.png']")).click();
        return clickForward;
    };
    LongProfilePageObject.prototype.checkScrollUpFunction = function () {
        var checkScrollUp = built_1.browser.executeScript('return window.pageYOffset;').then(function (pos) {
            expect(pos).toBeGreaterThan(360);
        });
        return checkScrollUp;
    };
    LongProfilePageObject.prototype.scrollFullDown = function () {
        built_1.browser.executeScript('window.scrollTo(0,10000);');
    };
    return LongProfilePageObject;
}());
exports.LongProfilePageObject = LongProfilePageObject;
//# sourceMappingURL=LongProfile.po.js.map