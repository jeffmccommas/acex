﻿import { browser, by, element } from 'protractor/built';
import { TokenGenerator } from '../e2e-helpers/TokenGenerator';

export class LongProfilePageObject {


    //navigateToIframe(custID, clientID, accID, premID) {

    //    let shortProfileUrl = new TokenGenerator();

    //    let token = shortProfileUrl.getToken(custID, clientID, accID, premID);

    //    return token
    //        .then(function (data) {

    //            console.log('token' + data);

    //            let temp1 = JSON.parse(data);
    //            let temp2 = JSON.parse(temp1.Content);
    //            console.log('*****+ ***>' + temp2.WebToken);
    //            //browser.waitForAngularEnabled(false);
    //            browser.get(browser.baseUrl + 'Widget?id=tab.dashboard-profile&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true&enableRemote=true&clearProfileAttr=true');
    //            console.log('url->' + browser.baseUrl + 'Widget?id=tab.dashboard-profile&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true&enableRemote=true&clearProfileAttr=true');
    //        });
    //}

    navigateTo(custID, clientID, accID, premID) {

        let longProfileUrl = new TokenGenerator();

        let token = longProfileUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);
                //browser.waitForAngularEnabled(false);
                browser.get(browser.baseUrl + 'Page?id=12&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
                console.log('url->' + browser.baseUrl + 'Page?id=12&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            });
    }


    getClientData() {
        var json = require('../TestData/LongProfile/LongProfileClients.json');
        return json;
    }

    checkSetFocusOnFirstQuestion() {
        var checkSetFocus = element(by.id('focus-0')).equals(browser.driver.switchTo().activeElement())
            .then(equals => expect(equals).toBe(true));
        return checkSetFocus;
    }

    clickForwardButton() {
        var clickForward = element(by.xpath("//img[@src='Content/Images/icon-page-right.png']")).click();
        return clickForward;
    }

    checkScrollUpFunction() {

        var checkScrollUp = browser.executeScript('return window.pageYOffset;').then(function (pos) {
            expect(pos).toBeGreaterThan(360);
        });
        return checkScrollUp;
    }

    scrollFullDown() {
        browser.executeScript('window.scrollTo(0,10000);');
        //return scrollDown;
    }
}