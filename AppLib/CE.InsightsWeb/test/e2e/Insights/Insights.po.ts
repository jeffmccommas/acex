﻿import { browser, by, element } from 'protractor/built';
import { TokenGenerator } from '../e2e-helpers/TokenGenerator';
// The require statement will import your JSON file into the json variable
 
export class InsightsPageObject {


    navigateTo(custID, clientID, accID, premID) {

        let insightsUrl = new TokenGenerator();

        let token = insightsUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);
       
                browser.get(browser.baseUrl + 'Widget?id=tab.dashboard-insights&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=1');
                console.log('url->' + browser.baseUrl + 'Widget?id=tab.dashboard-insights&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=1');
                browser.sleep(5000);
            });
    }


    getInsightsCostImpactDecrease() {
        browser.sleep(3000);
        this.clickExpandIcon();
        var insightCostImpactDec = element(by.id('iws_in_notification_insight.usagecostdecrease'));

        return insightCostImpactDec;

    }

    getInsightsCostImpactIncrease() {

        browser.sleep(2000);
        this.clickExpandIcon();
        var insightCostImpactInc = element(by.id('iws_in_notification_insight.usagecostdecrease'));
        return insightCostImpactInc;

    }

    getInsightsCostSmallImpact() {

        browser.sleep(2000);
        this.clickExpandIcon();
        var insightCostImpactSmall = element(by.id('iws_in_main'));
        return insightCostImpactSmall;

    }

    getInsightsCosts() {

        browser.sleep(2000);
        this.clickExpandIcon();
        var insightCostImpactSmall = element(by.id('iws_in_main'));
        return insightCostImpactSmall;

    }

    getInsightsCount() {

        browser.sleep(2000);
        this.clickExpandIcon();
        var insightsCount = browser
            .findElements(by.css('.xs-mb-15'));
        return insightsCount;
    }

    getInsightsCountCollapsed() {
        browser.sleep(2000);
        var insightsCount = browser
            .findElements(by.css('.xs-mb-15'));
        return insightsCount;
    }

    getInsightsRecentTitleCollapsed() {
        browser.sleep(3000);
        var insightsTitleText = element(by.id('iws_in_recent_title'));
        return insightsTitleText; 
    }

    getInsightsBillHighlightarrowdown() {
        browser.sleep(3000);
        this.clickExpandIcon();
        var billHighlightIcon = element(by.id('iws_in_icon_insight.usagecostdecrease'));
        return billHighlightIcon;
    }

    getInsightsBillHighlightarrowup() {
        browser.sleep(2000);
        this.clickExpandIcon();
        var billHighlightIcon = element(by.id('iws_in_icon_insight.usagecostincrease'));
        return billHighlightIcon;
    }

    getExpandCollapseIcon() {
        browser.sleep(3000);
        var pageExpandIcon = element(by.id("iws_view_all_icon"));
        return pageExpandIcon;
    }

    getConfiguredInsights() {
        browser.sleep(3000);
        var insightTitleId = element(by.id("insight.installcflspecial-as_ai_title"));
        return insightTitleId;
    }

    //assumes fully expanded insights
    getConfiguredInsightsbyId(passedVal) {
        browser.sleep(3000);
        this.clickExpandIcon();
        var insightFound = element(by.id(passedVal));
        return insightFound;
    }


    //not sure if move to e2ehelpers (used more than once)?
    private clickExpandIcon() {
        var pageExpandIcon = element(by.id("iws_view_all_icon"));
        pageExpandIcon.click(); 
    }

    getJsonData() {
        // The require statement will import your JSON file into the json variable
        var jsonData = require('../TestData/Insights/test.json');
        return jsonData;
    }

}
