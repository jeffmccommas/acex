'Use strict';
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Insights_po_1 = require("./Insights.po");
describe('Insights', function () {
    var page;
    beforeEach(function () {
        page = new Insights_po_1.InsightsPageObject();
    });
    it('Insights widget is displaying cost impact usage increase demo account', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[0].custID, testData[0].clientID, testData[0].accID, testData[0].premID);
        console.log(testData[0].custid);
        var usageCost = page.getInsightsCostImpactIncrease();
        expect(usageCost).toBeDefined();
    });
    it('Insights widget is displaying cost impact usage decrease demo account', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[0].custID, testData[0].clientID, testData[0].accID, testData[0].premID);
        var usageCost = page.getInsightsCostImpactDecrease();
        console.log(usageCost);
        expect(usageCost).toBeDefined();
    });
    it('Insights widget is displaying cost impact usage decrease icon down demo account', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[0].custID, testData[0].clientID, testData[0].accID, testData[0].premID);
        var iconCost = page.getInsightsBillHighlightarrowdown();
        expect(iconCost).toBeDefined();
    });
    it('Insights widget is displaying cost impact usage increase icon up demo account', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[0].custID, testData[0].clientID, testData[0].accID, testData[0].premID);
        var iconCost = page.getInsightsBillHighlightarrowup();
        expect(iconCost).toBeDefined();
    });
    it('Insights widget Verify expand/collapse icon displayed when configured (default)', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[0].custID, testData[0].clientID, testData[0].accID, testData[0].premID);
        var expandicon = page.getExpandCollapseIcon().isDisplayed();
        expect(expandicon).toBeTruthy();
    });
    it('Insights widget Verify expand/collapse icon is not displayed when set to false (client specific)', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[7].custID, testData[7].clientID, testData[7].accID, testData[7].premID);
        var expandicon = page.getExpandCollapseIcon().isPresent();
        expect(expandicon).toBeFalsy();
    });
    it('Insights widget Verify in expanded mode maxinsightstoshow eight insights are displayed when configured (default)', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[0].custID, testData[0].clientID, testData[0].accID, testData[0].premID);
        var expandicon = page.getInsightsCount().then(function (data) {
            expect(data.length).toEqual(8);
        });
    });
    it('Insights widget Verify in collapsed mode insightsrowcount 3 insights are displayed when configured (default)', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[0].custID, testData[0].clientID, testData[0].accID, testData[0].premID);
        var collapseicon = page.getInsightsCountCollapsed().then(function (data) {
            expect(data.length).toEqual(3);
        });
    });
    it('Insights widget Verify in collapsed mode showrecenttitle text for insights are displayed when configured (default)', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[0].custID, testData[0].clientID, testData[0].accID, testData[0].premID);
        var titleText = page.getInsightsRecentTitleCollapsed();
        expect(titleText.getText()).toEqual('Recent (3)');
    });
    it('Insights widget Verify in collapsed mode showrecenttitle text for insights is displayed when configured (nondefault)', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[4].custID, testData[4].clientID, testData[4].accID, testData[4].premID);
        var titleText = page.getInsightsRecentTitleCollapsed();
        expect(titleText.getText()).toEqual('Recent (3)');
    });
    it('Insights widget bill highlights verify combined billhighlight when one premise multiservice gas', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[3].custID, testData[3].clientID, testData[3].accID, testData[3].premID);
        var usageCost = page.getInsightsCosts();
        var txtUsageCost = usageCost.getText();
        expect(txtUsageCost).toContain("Your Gas charges were $100.00 lower than your previous bill");
    });
    it('Insights widget bill highlights verify filter by premise 2 billhighlight when two premise two services multicommodity', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[6].custID, testData[6].clientID, testData[6].accID, testData[6].premID);
        var usageCost = page.getInsightsCosts();
        var txtUsageCost = usageCost.getText();
        expect(txtUsageCost).toContain("A longer billing period increased your Gas usage compared to the previous bill. Impact to your latest bill: $5.87.");
        expect(txtUsageCost).toContain("A longer billing period increased your Electric usage compared to the previous bill. Impact to your latest bill: $6.18");
    });
    it('Insights widget bill highlights verify filter by premise 1 billhighlight when two premise two services multicommodity', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[5].custID, testData[5].clientID, testData[5].accID, testData[5].premID);
        var usageCost = page.getInsightsCosts();
        var txtUsageCost = usageCost.getText();
        expect(txtUsageCost).toContain("A longer billing period increased your Gas usage compared to the previous bill. Impact to your latest bill: $5.87.");
    });
    it('Insights widget negative test is not displaying cost impact usage when impact too small for water demo account', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[0].custID, testData[0].clientID, testData[0].accID, testData[0].premID);
        var usageCost = page.getInsightsCostSmallImpact();
        var txtUsageCost = usageCost.getText();
        expect(txtUsageCost).not.toContain("Your Water charges");
    });
    it('Insights widget negative test is not displaying cost impact usage when no impact 0 bill', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[1].custID, testData[1].clientID, testData[1].accID, testData[1].premID);
        var usageCost = page.getInsightsCostImpactDecrease();
        expect(usageCost.isPresent()).toBeFalsy();
    });
    it('Insights widget negative test is not displaying cost impact usage when one bill', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[2].custID, testData[2].clientID, testData[2].accID, testData[2].premID);
        var usageCost = page.getInsightsCostImpactDecrease();
        expect(usageCost.isPresent()).toBeFalsy();
    });
    it('Insights widget only displays one insight related to client specific configuration of insightstoshow', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[7].custID, testData[7].clientID, testData[7].accID, testData[7].premID);
        var cflInsight = page.getConfiguredInsights();
        expect(cflInsight.isPresent()).toBeTruthy();
        var titleText = page.getInsightsRecentTitleCollapsed();
        expect(titleText.getText()).toEqual('Recent (1)');
    });
    it('Insights widget ami highlight highest interval found', function () {
        var testData = page.getJsonData();
        page.navigateTo(testData[0].custID, testData[0].clientID, testData[0].accID, testData[0].premID);
        var cflInsight = page.getConfiguredInsightsbyId('iws_in_icon_insight.highestinterval');
        expect(cflInsight.isPresent()).toBeTruthy();
    });
});
//# sourceMappingURL=Insights.spec.js.map