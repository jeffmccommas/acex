"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator_1 = require("../e2e-helpers/TokenGenerator");
var InsightsPageObject = (function () {
    function InsightsPageObject() {
    }
    InsightsPageObject.prototype.navigateTo = function (custID, clientID, accID, premID) {
        var insightsUrl = new TokenGenerator_1.TokenGenerator();
        var token = insightsUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Widget?id=tab.dashboard-insights&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=1');
            console.log('url->' + built_1.browser.baseUrl + 'Widget?id=tab.dashboard-insights&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=1');
            built_1.browser.sleep(5000);
        });
    };
    InsightsPageObject.prototype.getInsightsCostImpactDecrease = function () {
        built_1.browser.sleep(3000);
        this.clickExpandIcon();
        var insightCostImpactDec = built_1.element(built_1.by.id('iws_in_notification_insight.usagecostdecrease'));
        return insightCostImpactDec;
    };
    InsightsPageObject.prototype.getInsightsCostImpactIncrease = function () {
        built_1.browser.sleep(2000);
        this.clickExpandIcon();
        var insightCostImpactInc = built_1.element(built_1.by.id('iws_in_notification_insight.usagecostdecrease'));
        return insightCostImpactInc;
    };
    InsightsPageObject.prototype.getInsightsCostSmallImpact = function () {
        built_1.browser.sleep(2000);
        this.clickExpandIcon();
        var insightCostImpactSmall = built_1.element(built_1.by.id('iws_in_main'));
        return insightCostImpactSmall;
    };
    InsightsPageObject.prototype.getInsightsCosts = function () {
        built_1.browser.sleep(2000);
        this.clickExpandIcon();
        var insightCostImpactSmall = built_1.element(built_1.by.id('iws_in_main'));
        return insightCostImpactSmall;
    };
    InsightsPageObject.prototype.getInsightsCount = function () {
        built_1.browser.sleep(2000);
        this.clickExpandIcon();
        var insightsCount = built_1.browser
            .findElements(built_1.by.css('.xs-mb-15'));
        return insightsCount;
    };
    InsightsPageObject.prototype.getInsightsCountCollapsed = function () {
        built_1.browser.sleep(2000);
        var insightsCount = built_1.browser
            .findElements(built_1.by.css('.xs-mb-15'));
        return insightsCount;
    };
    InsightsPageObject.prototype.getInsightsRecentTitleCollapsed = function () {
        built_1.browser.sleep(3000);
        var insightsTitleText = built_1.element(built_1.by.id('iws_in_recent_title'));
        return insightsTitleText;
    };
    InsightsPageObject.prototype.getInsightsBillHighlightarrowdown = function () {
        built_1.browser.sleep(3000);
        this.clickExpandIcon();
        var billHighlightIcon = built_1.element(built_1.by.id('iws_in_icon_insight.usagecostdecrease'));
        return billHighlightIcon;
    };
    InsightsPageObject.prototype.getInsightsBillHighlightarrowup = function () {
        built_1.browser.sleep(2000);
        this.clickExpandIcon();
        var billHighlightIcon = built_1.element(built_1.by.id('iws_in_icon_insight.usagecostincrease'));
        return billHighlightIcon;
    };
    InsightsPageObject.prototype.getExpandCollapseIcon = function () {
        built_1.browser.sleep(3000);
        var pageExpandIcon = built_1.element(built_1.by.id("iws_view_all_icon"));
        return pageExpandIcon;
    };
    InsightsPageObject.prototype.getConfiguredInsights = function () {
        built_1.browser.sleep(3000);
        var insightTitleId = built_1.element(built_1.by.id("insight.installcflspecial-as_ai_title"));
        return insightTitleId;
    };
    InsightsPageObject.prototype.getConfiguredInsightsbyId = function (passedVal) {
        built_1.browser.sleep(3000);
        this.clickExpandIcon();
        var insightFound = built_1.element(built_1.by.id(passedVal));
        return insightFound;
    };
    InsightsPageObject.prototype.clickExpandIcon = function () {
        var pageExpandIcon = built_1.element(built_1.by.id("iws_view_all_icon"));
        pageExpandIcon.click();
    };
    InsightsPageObject.prototype.getJsonData = function () {
        var jsonData = require('../TestData/Insights/test.json');
        return jsonData;
    };
    return InsightsPageObject;
}());
exports.InsightsPageObject = InsightsPageObject;
//# sourceMappingURL=Insights.po.js.map