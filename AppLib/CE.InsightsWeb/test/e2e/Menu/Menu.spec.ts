﻿'Use strict';

import { MenuPageObject } from './Menu.po';

describe('Menu', () => {
    let page: MenuPageObject;

    beforeEach(() => {
        page = new MenuPageObject();
    });

    it(' Verify that Dashboard/MyUsage/MySavings/MyBills menu is visible', function () {

        var clientData = page.getclientData();
        page.navigateToShowMenu(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID); 

        browser.sleep(2000);
        var menu = page.getnavBar();
        expect(menu.isPresent()).toBeTruthy();

    });

    it(' Verify that default url displays the menu', function () {

        var clientData = page.getclientData();
        page.navigateToDefault(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);

        browser.sleep(2000);
        var menu = page.getnavBar();
        expect(menu.isPresent()).toBeTruthy();

    });

    it('Verify that if showsubtabs is set to false the menu will not be displayed/visible ', function () {

        var clientData = page.getclientData();
        page.navigateToHideMenu(clientData[0].pageID, clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);

        browser.sleep(2000);
        var menu = page.getnavBar();
        expect(menu.isPresent()).toBeFalsy();

    });

});