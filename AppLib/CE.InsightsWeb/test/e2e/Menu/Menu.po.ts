﻿import { browser, by, element } from 'protractor/built';
import { TokenGenerator } from '../e2e-helpers/TokenGenerator';

export class MenuPageObject {


    navigateToShowMenu(pageID, custID, clientID, accID, premID) {

        let menuUrl = new TokenGenerator();

        let token = menuUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);
                //browser.waitForAngularEnabled(false);

                browser.get(browser.baseUrl + 'Page?id=' + pageID + '&sid=11' + '&ClientId=' + clientID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ServicePointId=&WebToken=' + temp2.WebToken + '&Locale=en-US&showSubTabs=false');
                console.log('url->' +browser.baseUrl + 'Page?id=' + pageID + '&sid=11' + '&ClientId=' + clientID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ServicePointId=&WebToken=' + temp2.WebToken + '&Locale=en-US&showSubTabs=false');

            });
    }

    navigateToDefault(pageID, custID, clientID, accID, premID) {

        let menuUrl = new TokenGenerator();

        let token = menuUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);
                //browser.waitForAngularEnabled(false);

                browser.get(browser.baseUrl + 'Page?id=' + pageID + '&sid=11' + '&ClientId=' + clientID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ServicePointId=&WebToken=' + temp2.WebToken + '&Locale=en-US');
                console.log('url->' + browser.baseUrl + 'Page?id=' + pageID + '&sid=11' + '&ClientId=' + clientID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ServicePointId=&WebToken=' + temp2.WebToken + '&Locale=en-US');

            });
    }

    navigateToHideMenu(pageID, custID, clientID, accID, premID) {

        let menuUrl = new TokenGenerator();

        let token = menuUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);
                //browser.waitForAngularEnabled(false);

                browser.get(browser.baseUrl + 'Page?id=' + pageID + '&sid=11' + '&ClientId=' + clientID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ServicePointId=&WebToken=' + temp2.WebToken + '&Locale=en-US&showSubTabs=true');
                console.log('url->' + browser.baseUrl + 'Page?id=' + pageID + '&sid=11' + '&ClientId=' + clientID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ServicePointId=&WebToken=' + temp2.WebToken + '&Locale=en-US&showSubTabs=true');

            });
    }

    getclientData() {
        var json = require('../TestData/Menu/clientData.json');
        return json;
    }

    getnavBar() {
        var navBar = element(by.id('acl_nav'));
        return navBar;
    }





}