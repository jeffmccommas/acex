"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator_1 = require("../e2e-helpers/TokenGenerator");
var MenuPageObject = (function () {
    function MenuPageObject() {
    }
    MenuPageObject.prototype.navigateToShowMenu = function (pageID, custID, clientID, accID, premID) {
        var menuUrl = new TokenGenerator_1.TokenGenerator();
        var token = menuUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Page?id=' + pageID + '&sid=11' + '&ClientId=' + clientID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ServicePointId=&WebToken=' + temp2.WebToken + '&Locale=en-US&showSubTabs=false');
            console.log('url->' + built_1.browser.baseUrl + 'Page?id=' + pageID + '&sid=11' + '&ClientId=' + clientID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ServicePointId=&WebToken=' + temp2.WebToken + '&Locale=en-US&showSubTabs=false');
        });
    };
    MenuPageObject.prototype.navigateToDefault = function (pageID, custID, clientID, accID, premID) {
        var menuUrl = new TokenGenerator_1.TokenGenerator();
        var token = menuUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Page?id=' + pageID + '&sid=11' + '&ClientId=' + clientID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ServicePointId=&WebToken=' + temp2.WebToken + '&Locale=en-US');
            console.log('url->' + built_1.browser.baseUrl + 'Page?id=' + pageID + '&sid=11' + '&ClientId=' + clientID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ServicePointId=&WebToken=' + temp2.WebToken + '&Locale=en-US');
        });
    };
    MenuPageObject.prototype.navigateToHideMenu = function (pageID, custID, clientID, accID, premID) {
        var menuUrl = new TokenGenerator_1.TokenGenerator();
        var token = menuUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Page?id=' + pageID + '&sid=11' + '&ClientId=' + clientID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ServicePointId=&WebToken=' + temp2.WebToken + '&Locale=en-US&showSubTabs=true');
            console.log('url->' + built_1.browser.baseUrl + 'Page?id=' + pageID + '&sid=11' + '&ClientId=' + clientID + '&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ServicePointId=&WebToken=' + temp2.WebToken + '&Locale=en-US&showSubTabs=true');
        });
    };
    MenuPageObject.prototype.getclientData = function () {
        var json = require('../TestData/Menu/clientData.json');
        return json;
    };
    MenuPageObject.prototype.getnavBar = function () {
        var navBar = built_1.element(built_1.by.id('acl_nav'));
        return navBar;
    };
    return MenuPageObject;
}());
exports.MenuPageObject = MenuPageObject;
//# sourceMappingURL=Menu.po.js.map