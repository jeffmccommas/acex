﻿'Use strict';
import { BillComparisonPageObject } from './BillComparison.po';

describe('Should verify Bill Comparison in My Bills tab', () => {

    let page: BillComparisonPageObject;

    beforeEach(() => {
        page = new BillComparisonPageObject();
    });


  it('Should Verify usage and cost text display Bill Comparison For all the three Commodities', function () {

      var clientData = page.getCilentData();

       page.navigateTo(clientData[0].custID, clientData[0].clientID, clientData[0].accID, clientData[0].premID);

       var json = page.getBillCompareAllData();
       var electric = page.getElectricUsage();
       expect(electric).toEqual([json.electric[0], json.electric[1], json.electric[2], json.electric[3]]);

       var electric = page.getElectricCost();
       expect(electric).toEqual([json.elecCost[0], json.elecCost[1], json.elecCost[2], json.elecCost[3]]);

       var gas = page.getGasUsage();
       expect(gas).toEqual([json.gas[0], json.gas[1], json.gas[2], json.gas[3]]);

       var gas = page.getGasCost();
       expect(gas).toEqual([json.gasCost[0], json.gasCost[1], json.gasCost[2], json.gasCost[3]]);

       var water = page.getWaterUsage();
       expect(water).toEqual([json.water[0], json.water[1], json.water[2], json.water[3]]);

       var water = page.getWaterCost();
       expect(water).toEqual([json.waterCost[0], json.waterCost[1], json.waterCost[2], json.waterCost[3]]);


   });

   it('Should verify that the cost values for the comodities are greater than zero', function () {
       browser.sleep(1000);
       setTestForElectricCostValues();
       setTestForGasCostValues();
       setTestForWaterCostValues();
       
   });

   it('Verify chart is not displayed for negative values', function () {

       var clientData = page.getCilentData();
       page.navigateTo(clientData[2].custID, clientData[2].clientID, clientData[2].accID, clientData[2].premID);

       browser.sleep(2000);
       var totalCostLastMonth = page.getTotalCostLastMonth();
       if (expect(totalCostLastMonth.getText()).toMatch('\(\)')) {
            var chartDisplay = page.getchartDisplay();
           expect(browser.isElementPresent(chartDisplay)).toBe(false);
       } else
       {
           var chartDisplay = page.getchartDisplay();
           expect(browser.isElementPresent(chartDisplay)).toBe(true);
       }

   });

   it('Should Verify text display Bill Comparison For One Commodity', function () {

       var clientData = page.getCilentData();
       page.navigateTo(clientData[1].custID, clientData[1].clientID, clientData[1].accID, clientData[1].premID);

       var billComparisonHeadData = page.getBillComparisonHeaderData();
       var billComparisonRowData = page.getBillComparisonTableRowData();
       var labels = page.getBillComparisonChartData();

       var json = page.getBillCompData();
       expect(billComparisonHeadData).toEqual([json[0].header[0], json[0].header[1], json[0].header[2], json[0].header[3]]);
       expect(billComparisonRowData).toEqual([json[0].days[0], json[0].days[1], json[0].days[2], json[0].days[3],
       json[0].temp[0], json[0].temp[1], json[0].temp[2], json[0].temp[3],
       json[0].electric[0], json[0].electric[1], json[0].electric[2], json[0].electric[3],
       json[0].cost[0], json[0].cost[1], json[0].cost[2], json[0].cost[3]]);
       expect(labels).toEqual([json[0].header[1], json[0].header[2]]);
   });

   it('Verify premise with only electric bills displays only electric commodity for bills', function () {
       var elecomm = page.getElectricCommodity();
       var json = page.getBillCompData();
       expect(elecomm.getText()).toEqual(json[0].electric[0]);

       var json = page.getBillCompareAllData();
       expect(elecomm.getText()).not.toEqual(json[0].water[0]);
       expect(elecomm.getText()).not.toEqual(json[0].gas[0]);
   });

   it('Check for the correct formats of bill and its difference to be positive', function () {
       browser.sleep(1000);
       var difference = page.getcostDifference();
       expect(difference.getText()).toContain('$');
       expect(difference.getText()).not.toContain('-');

   });
   
   
   it(' Verify premise with no bills displays no bills available content', function () {

        var clientData = page.getCilentData();
        page.navigateTo(clientData[3].custID, clientData[3].clientID, clientData[3].accID, clientData[3].premID);

        var errorMsg = page.getErrorMsg();
        expect(errorMsg.getText()).toMatch( 'No bills are available.');

        browser.sleep(2000);

    }); 


    it('Verify customer with different bill dates, filter by premise with only relevant bill dates on comparison', function () {

        var clientData = page.getCilentData();
        page.navigateTo(clientData[4].custID, clientData[4].clientID, clientData[4].accID, clientData[4].premID);
        browser.sleep(5000);
        page.getChoosetoCompare();

        var datesCount = page.getDateCount();
        expect(datesCount).toEqual(3);
        browser.sleep(2000);
        var Dates = page.getDatesofMultiplePremise();
        expect(Dates).toEqual(['04/30/2017', '03/31/2017', '02/28/2017']);

        page.getSecondPremise().click();
        browser.sleep(5000);

        page.getChoosetoCompare();
        var datesCount = page.getDateCount();
        expect(datesCount).toEqual(3);
        browser.sleep(2000);
        var Dates = page.getDatesofMultiplePremise();
        expect(Dates).not.toEqual(['04/30/2017', '03/31/2017', '02/28/2017']);
        expect(Dates).toEqual(['05/12/2017', '04/12/2017', '03/13/2017']);
   });

    it('Verify premise for gas commodity with two services summarizes bills together', function() {

        var clientData = page.getCilentData();
        page.navigateTo(clientData[5].custID, clientData[5].clientID, clientData[5].accID, clientData[5].premID);

        var gascomm = page.getGasCommodity();
        var json = page.getBillCompData();
        expect(gascomm.getText()).toEqual(json[1].gas[0]);

        var gascommUsageprevMonth = page.getGasCommUsagePrevMonth();
        var gascommusagecurrmonth = page.getGasCommUsageCurrentMonth();
        var gascommbillprev = page.getGasCommBillPrevMonth();
        var gascommbillcurr = page.getGasCommBillCurrentMonth();

        expect(gascommUsageprevMonth.getText()).toEqual(json[1].gas[1]);
        expect(gascommusagecurrmonth.getText()).toEqual(json[1].gas[2]);
        browser.sleep(2000);
        expect(gascommbillprev.getText()).toEqual(json[1].cost[1]);
        expect(gascommbillcurr.getText()).toEqual(json[1].cost[2]);
    });


    it('e2e BillComp: Verify premise with one bill displays one bill available content', function() {

        var clientData = page.getCilentData();
        page.navigateTo(clientData[6].custID, clientData[6].clientID, clientData[6].accID, clientData[6].premID);

        var errorMsg = page.getErrorMsg();
        expect(errorMsg.getText()).toMatch('There is only one bill available. Come back when you have more bills!');

        browser.sleep(2000);

    }); 

   function setTestForElectricCostValues() {

        var electric = page.getElectricCostLastMonth();
        expect(electric.getText()).not.toContain('$0.00');
        expect(electric.getText()).not.toContain('-');

        var electric = page.getElectricCostThisMonth();
        expect(electric.getText()).not.toContain('$0.00');
        expect(electric.getText()).not.toContain('-');
       
   }

   function setTestForGasCostValues() {

        var gas = page.getGasCostLastMonth();
        expect(gas.getText()).not.toContain('$0.00');
        expect(gas.getText()).not.toContain('-');

        var gas = page.getGasCostThisMonth();
        expect(gas.getText()).not.toContain('$0.00');
        expect(gas.getText()).not.toContain('-');
   }

   function setTestForWaterCostValues() {

        var water = page.getWaterCostLastMonth();
        expect(water.getText()).not.toContain('$0.00');
        expect(water.getText()).not.toContain('-');

        var water = page.getWaterCostThisMonth();
        expect(water.getText()).not.toContain('$0.00');
        expect(water.getText()).not.toContain('-');
   }

});

