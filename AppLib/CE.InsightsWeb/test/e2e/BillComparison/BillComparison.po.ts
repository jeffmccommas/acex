﻿import { browser, by, element } from 'protractor/built';
import { TokenGenerator } from '../e2e-helpers/TokenGenerator';

export class BillComparisonPageObject {


    navigateTo(custID, clientID, accID, premID) {

        let billComparisonUrl = new TokenGenerator();

        let token = billComparisonUrl.getToken(custID, clientID, accID, premID);

        return token
            .then(function (data) {

                console.log('token' + data);

                let temp1 = JSON.parse(data);
                let temp2 = JSON.parse(temp1.Content);
                console.log('*****+ ***>' + temp2.WebToken);
                //browser.waitForAngularEnabled(false);

                browser.get(browser.baseUrl + 'Page?id=120&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
                console.log('url->' + browser.baseUrl + 'Page?id=120&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            });
    }

    getCilentData() {
        var json = require('../TestData/BillComparison/client.json');
        return json;
    }

    getBillCompareAllData() {
        var json = require('../TestData/BillComparison/BillCompareAll.json');
        return json;
    }

    getBillCompData() {
        var json = require('../TestData/BillComparison/BillComp.json');
        return json;
    }

    getBillComparisonHeaderData() {

        var billComparisontable = element(by.className('bill-comparison'));
        var tableHead = billComparisontable.element(by.tagName("thead")).all(by.tagName("th"));
        var headData = tableHead.map(function (data) {
            return data.getText();
        });
        return headData;
        }

    getBillComparisonTableRowData() {

        var billComparisontable = element(by.className('bill-comparison'));
        var tableRow = billComparisontable.element(by.tagName('tbody')).all(by.tagName('td'));

        var rowData = tableRow.map(function (data) {
            return data.getText();
        });
        return rowData;
    }

    getBillComparisonChartData() {

        browser.sleep(1000);
        element(by.id("iws_bc_a_comparison_selection")).click();
        browser.sleep(1000);
        element(by.id("iws_bc_ddl_ul_compare")).all(by.tagName('a')).get(0).click();
        browser.sleep(1000);
        var x = element(by.className("highcharts-axis-labels highcharts-xaxis-labels")).all(by.tagName("tspan"));
        var label = x.map(function (data) {
            return data.getText();
        });
        return label;
    }

    getcostDifference() {
        var cost = element(by.className('table table-striped')).all(by.tagName('tr')).get(4);
        var diff = cost.all(by.tagName('td')).get(3);
        return diff;
    }

    getBillComparisonWidget() {
        browser.sleep(1000);
        element(by.id("iws_bc_a_comparison_selection")).click();
        var x = element(by.id("iws_bc_ddl_ul_compare")).all(by.tagName('a')).get(3).click();
        var compareBtn = element(by.id('iws_bc_ddl_btn_compare')).click();
        return compareBtn;
    }
    getElectricUsage() {
        var electricUsage = element(by.className('table-responsive')).all(by.tagName('tr')).get(3);
        var elec = electricUsage.all(by.tagName('td'));
        var text = elec.map(function (data) {
        return data.getText();
        });
         return text;
        }

    getElectricCost() {
       var electricCost = element(by.className('table-responsive')).all(by.tagName('tr')).get(4);
       var elec = electricCost.all(by.tagName('td'));
       var text = elec.map(function (data) {
       return data.getText();
       });
    return text;
    }

    getGasUsage() {
        var gasUsage = element(by.className('table-responsive')).all(by.tagName('tr')).get(5);
        var gas = gasUsage.all(by.tagName('td'));
        var text = gas.map(function (data) {
            return data.getText();
        });
        return text;
    }

    getGasCost() {
        var gasCost = element(by.className('table-responsive')).all(by.tagName('tr')).get(6);
        var gas = gasCost.all(by.tagName('td'));
        var text = gas.map(function (data) {
            return data.getText();
        });
        return text;
    }

    getWaterUsage() {
        var waterUsage = element(by.className('table-responsive')).all(by.tagName('tr')).get(7);
        var water = waterUsage.all(by.tagName('td'));
        var text = water.map(function (data) {
            return data.getText();
        });
        return text;
    }

    getWaterCost() {
        var waterCost = element(by.className('table-responsive')).all(by.tagName('tr')).get(8);
        var water = waterCost.all(by.tagName('td'));
        var text = water.map(function (data) {
            return data.getText();
        });
        return text;
    }

    getElectricCostLastMonth() {
            var electricCost = element(by.className('table-responsive')).all(by.tagName('tr')).get(4);
            var elec = electricCost.all(by.tagName('td')).get(1);
            return elec;
        
    }
    getElectricCostThisMonth() {
        var electricCost = element(by.className('table-responsive')).all(by.tagName('tr')).get(4);
        var elec = electricCost.all(by.tagName('td')).get(2);
        return elec;

    }
    getGasCostLastMonth() {
        var electricCost = element(by.className('table-responsive')).all(by.tagName('tr')).get(6);
        var gas = electricCost.all(by.tagName('td')).get(1);
        return gas;

    }
    getGasCostThisMonth() {
        var electricCost = element(by.className('table-responsive')).all(by.tagName('tr')).get(6);
        var gas = electricCost.all(by.tagName('td')).get(2);
        return gas;

    }
    getWaterCostLastMonth() {
        var WaterCost = element(by.className('table-responsive')).all(by.tagName('tr')).get(8);
        var water = WaterCost.all(by.tagName('td')).get(1);
        return water;

    }
    getWaterCostThisMonth() {
        var WaterCost = element(by.className('table-responsive')).all(by.tagName('tr')).get(8);
        var water = WaterCost.all(by.tagName('td')).get(2);
        return water;

    }

    getTotalCostLastMonth() {
        var total = element(by.className('table-responsive')).all(by.tagName('tr')).get(3);
        var totalCost = total.all(by.tagName('td')).get(1);
        return totalCost;
    }

    getchartDisplay() {
        var chart = element(by.className('iws_bc_chart'));
        return chart;

    }

    getErrorMsg() {

        var errorMsg = element(by.id('iws_bc_main')).all(by.className('alert alert-warning')).all(by.tagName('strong'));
        return errorMsg;
    }

    getChoosetoCompare() {
        browser.sleep(1000);
        element(by.id("iws_bc_a_comparison_selection")).click();
        browser.sleep(1000);
        var select = element(by.id('iws_bc_ddl_types')).all(by.tagName('li')).get(2);
        var choosetoCompare = select.element(by.tagName('a'));
        browser.actions().mouseMove(choosetoCompare).click().perform();
        return choosetoCompare;

    }

    getDateCount() {
        browser.sleep(1000);
        element(by.id('iws_bc_a_from_selection')).click();
        var date = element(by.id('iws_bc_ddl_ul_from')).all(by.tagName('li')).count();
       // browser.actions().mouseMove(date).click().perform();
        return date;

    }

    getSecondPremise() {

        var x = element(by.className('panel-default account-select'));
        element(by.className('text-right pull-right')).click();
        var secondPremise = element(by.id('PageWidget_3')).all(by.className('panel-body premises')).get(1);
        var radioBtn = secondPremise.element(by.tagName('input'));
        return radioBtn;
    }

    getDatesofMultiplePremise() {
        var dates = element(by.id('iws_bc_ddl_ul_from')).all(by.tagName('a'));
        var text = dates.map(function (data) {
            return data.getText();
        });
        return text;


    }

    getElectricCommodity() {

        var comm = element(by.className('table table-striped')).element(by.tagName('tbody'));
        var elecomm = comm.all(by.tagName('td')).get(8);
        return elecomm;
    }

    getGasCommodity() {

        var comm = element(by.className('table table-striped')).element(by.tagName('tbody'));
        var gascomm = comm.all(by.tagName('td')).get(8);
        return gascomm;
    }

    getGasCommUsagePrevMonth() {
        var comm = element(by.className('table table-striped')).element(by.tagName('tbody'));
        var gascomm = comm.all(by.tagName('td')).get(9);
        return gascomm;
    }

    getGasCommUsageCurrentMonth() {

        var comm = element(by.className('table table-striped')).element(by.tagName('tbody'));
        var gascomm = comm.all(by.tagName('td')).get(10);
        return gascomm;
    }

    getGasCommBillPrevMonth() {

        var comm = element(by.className('table table-striped')).element(by.tagName('tbody'));
        var gascomm = comm.all(by.tagName('td')).get(13);
        return gascomm;
    }

    getGasCommBillCurrentMonth() {

        var comm = element(by.className('table table-striped')).element(by.tagName('tbody'));
        var gascomm = comm.all(by.tagName('td')).get(14);
        return gascomm;
    }
   
}