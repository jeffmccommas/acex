"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var built_1 = require("protractor/built");
var TokenGenerator_1 = require("../e2e-helpers/TokenGenerator");
var BillComparisonPageObject = (function () {
    function BillComparisonPageObject() {
    }
    BillComparisonPageObject.prototype.navigateTo = function (custID, clientID, accID, premID) {
        var billComparisonUrl = new TokenGenerator_1.TokenGenerator();
        var token = billComparisonUrl.getToken(custID, clientID, accID, premID);
        return token
            .then(function (data) {
            console.log('token' + data);
            var temp1 = JSON.parse(data);
            var temp2 = JSON.parse(temp1.Content);
            console.log('*****+ ***>' + temp2.WebToken);
            built_1.browser.get(built_1.browser.baseUrl + 'Page?id=120&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
            console.log('url->' + built_1.browser.baseUrl + 'Page?id=120&CustomerId=' + custID + '&AccountId=' + accID + '&PremiseId=' + premID + '&ClientId=' + clientID + '&WebToken=' + temp2.WebToken + '&ServicePointId=&ShowSubTabs=true');
        });
    };
    BillComparisonPageObject.prototype.getCilentData = function () {
        var json = require('../TestData/BillComparison/client.json');
        return json;
    };
    BillComparisonPageObject.prototype.getBillCompareAllData = function () {
        var json = require('../TestData/BillComparison/BillCompareAll.json');
        return json;
    };
    BillComparisonPageObject.prototype.getBillCompData = function () {
        var json = require('../TestData/BillComparison/BillComp.json');
        return json;
    };
    BillComparisonPageObject.prototype.getBillComparisonHeaderData = function () {
        var billComparisontable = built_1.element(built_1.by.className('bill-comparison'));
        var tableHead = billComparisontable.element(built_1.by.tagName("thead")).all(built_1.by.tagName("th"));
        var headData = tableHead.map(function (data) {
            return data.getText();
        });
        return headData;
    };
    BillComparisonPageObject.prototype.getBillComparisonTableRowData = function () {
        var billComparisontable = built_1.element(built_1.by.className('bill-comparison'));
        var tableRow = billComparisontable.element(built_1.by.tagName('tbody')).all(built_1.by.tagName('td'));
        var rowData = tableRow.map(function (data) {
            return data.getText();
        });
        return rowData;
    };
    BillComparisonPageObject.prototype.getBillComparisonChartData = function () {
        built_1.browser.sleep(1000);
        built_1.element(built_1.by.id("iws_bc_a_comparison_selection")).click();
        built_1.browser.sleep(1000);
        built_1.element(built_1.by.id("iws_bc_ddl_ul_compare")).all(built_1.by.tagName('a')).get(0).click();
        built_1.browser.sleep(1000);
        var x = built_1.element(built_1.by.className("highcharts-axis-labels highcharts-xaxis-labels")).all(built_1.by.tagName("tspan"));
        var label = x.map(function (data) {
            return data.getText();
        });
        return label;
    };
    BillComparisonPageObject.prototype.getcostDifference = function () {
        var cost = built_1.element(built_1.by.className('table table-striped')).all(built_1.by.tagName('tr')).get(4);
        var diff = cost.all(built_1.by.tagName('td')).get(3);
        return diff;
    };
    BillComparisonPageObject.prototype.getBillComparisonWidget = function () {
        built_1.browser.sleep(1000);
        built_1.element(built_1.by.id("iws_bc_a_comparison_selection")).click();
        var x = built_1.element(built_1.by.id("iws_bc_ddl_ul_compare")).all(built_1.by.tagName('a')).get(3).click();
        var compareBtn = built_1.element(built_1.by.id('iws_bc_ddl_btn_compare')).click();
        return compareBtn;
    };
    BillComparisonPageObject.prototype.getElectricUsage = function () {
        var electricUsage = built_1.element(built_1.by.className('table-responsive')).all(built_1.by.tagName('tr')).get(3);
        var elec = electricUsage.all(built_1.by.tagName('td'));
        var text = elec.map(function (data) {
            return data.getText();
        });
        return text;
    };
    BillComparisonPageObject.prototype.getElectricCost = function () {
        var electricCost = built_1.element(built_1.by.className('table-responsive')).all(built_1.by.tagName('tr')).get(4);
        var elec = electricCost.all(built_1.by.tagName('td'));
        var text = elec.map(function (data) {
            return data.getText();
        });
        return text;
    };
    BillComparisonPageObject.prototype.getGasUsage = function () {
        var gasUsage = built_1.element(built_1.by.className('table-responsive')).all(built_1.by.tagName('tr')).get(5);
        var gas = gasUsage.all(built_1.by.tagName('td'));
        var text = gas.map(function (data) {
            return data.getText();
        });
        return text;
    };
    BillComparisonPageObject.prototype.getGasCost = function () {
        var gasCost = built_1.element(built_1.by.className('table-responsive')).all(built_1.by.tagName('tr')).get(6);
        var gas = gasCost.all(built_1.by.tagName('td'));
        var text = gas.map(function (data) {
            return data.getText();
        });
        return text;
    };
    BillComparisonPageObject.prototype.getWaterUsage = function () {
        var waterUsage = built_1.element(built_1.by.className('table-responsive')).all(built_1.by.tagName('tr')).get(7);
        var water = waterUsage.all(built_1.by.tagName('td'));
        var text = water.map(function (data) {
            return data.getText();
        });
        return text;
    };
    BillComparisonPageObject.prototype.getWaterCost = function () {
        var waterCost = built_1.element(built_1.by.className('table-responsive')).all(built_1.by.tagName('tr')).get(8);
        var water = waterCost.all(built_1.by.tagName('td'));
        var text = water.map(function (data) {
            return data.getText();
        });
        return text;
    };
    BillComparisonPageObject.prototype.getElectricCostLastMonth = function () {
        var electricCost = built_1.element(built_1.by.className('table-responsive')).all(built_1.by.tagName('tr')).get(4);
        var elec = electricCost.all(built_1.by.tagName('td')).get(1);
        return elec;
    };
    BillComparisonPageObject.prototype.getElectricCostThisMonth = function () {
        var electricCost = built_1.element(built_1.by.className('table-responsive')).all(built_1.by.tagName('tr')).get(4);
        var elec = electricCost.all(built_1.by.tagName('td')).get(2);
        return elec;
    };
    BillComparisonPageObject.prototype.getGasCostLastMonth = function () {
        var electricCost = built_1.element(built_1.by.className('table-responsive')).all(built_1.by.tagName('tr')).get(6);
        var gas = electricCost.all(built_1.by.tagName('td')).get(1);
        return gas;
    };
    BillComparisonPageObject.prototype.getGasCostThisMonth = function () {
        var electricCost = built_1.element(built_1.by.className('table-responsive')).all(built_1.by.tagName('tr')).get(6);
        var gas = electricCost.all(built_1.by.tagName('td')).get(2);
        return gas;
    };
    BillComparisonPageObject.prototype.getWaterCostLastMonth = function () {
        var WaterCost = built_1.element(built_1.by.className('table-responsive')).all(built_1.by.tagName('tr')).get(8);
        var water = WaterCost.all(built_1.by.tagName('td')).get(1);
        return water;
    };
    BillComparisonPageObject.prototype.getWaterCostThisMonth = function () {
        var WaterCost = built_1.element(built_1.by.className('table-responsive')).all(built_1.by.tagName('tr')).get(8);
        var water = WaterCost.all(built_1.by.tagName('td')).get(2);
        return water;
    };
    BillComparisonPageObject.prototype.getTotalCostLastMonth = function () {
        var total = built_1.element(built_1.by.className('table-responsive')).all(built_1.by.tagName('tr')).get(3);
        var totalCost = total.all(built_1.by.tagName('td')).get(1);
        return totalCost;
    };
    BillComparisonPageObject.prototype.getchartDisplay = function () {
        var chart = built_1.element(built_1.by.className('iws_bc_chart'));
        return chart;
    };
    BillComparisonPageObject.prototype.getErrorMsg = function () {
        var errorMsg = built_1.element(built_1.by.id('iws_bc_main')).all(built_1.by.className('alert alert-warning')).all(built_1.by.tagName('strong'));
        return errorMsg;
    };
    BillComparisonPageObject.prototype.getChoosetoCompare = function () {
        built_1.browser.sleep(1000);
        built_1.element(built_1.by.id("iws_bc_a_comparison_selection")).click();
        built_1.browser.sleep(1000);
        var select = built_1.element(built_1.by.id('iws_bc_ddl_types')).all(built_1.by.tagName('li')).get(2);
        var choosetoCompare = select.element(built_1.by.tagName('a'));
        built_1.browser.actions().mouseMove(choosetoCompare).click().perform();
        return choosetoCompare;
    };
    BillComparisonPageObject.prototype.getDateCount = function () {
        built_1.browser.sleep(1000);
        built_1.element(built_1.by.id('iws_bc_a_from_selection')).click();
        var date = built_1.element(built_1.by.id('iws_bc_ddl_ul_from')).all(built_1.by.tagName('li')).count();
        return date;
    };
    BillComparisonPageObject.prototype.getSecondPremise = function () {
        var x = built_1.element(built_1.by.className('panel-default account-select'));
        built_1.element(built_1.by.className('text-right pull-right')).click();
        var secondPremise = built_1.element(built_1.by.id('PageWidget_3')).all(built_1.by.className('panel-body premises')).get(1);
        var radioBtn = secondPremise.element(built_1.by.tagName('input'));
        return radioBtn;
    };
    BillComparisonPageObject.prototype.getDatesofMultiplePremise = function () {
        var dates = built_1.element(built_1.by.id('iws_bc_ddl_ul_from')).all(built_1.by.tagName('a'));
        var text = dates.map(function (data) {
            return data.getText();
        });
        return text;
    };
    BillComparisonPageObject.prototype.getElectricCommodity = function () {
        var comm = built_1.element(built_1.by.className('table table-striped')).element(built_1.by.tagName('tbody'));
        var elecomm = comm.all(built_1.by.tagName('td')).get(8);
        return elecomm;
    };
    BillComparisonPageObject.prototype.getGasCommodity = function () {
        var comm = built_1.element(built_1.by.className('table table-striped')).element(built_1.by.tagName('tbody'));
        var gascomm = comm.all(built_1.by.tagName('td')).get(8);
        return gascomm;
    };
    BillComparisonPageObject.prototype.getGasCommUsagePrevMonth = function () {
        var comm = built_1.element(built_1.by.className('table table-striped')).element(built_1.by.tagName('tbody'));
        var gascomm = comm.all(built_1.by.tagName('td')).get(9);
        return gascomm;
    };
    BillComparisonPageObject.prototype.getGasCommUsageCurrentMonth = function () {
        var comm = built_1.element(built_1.by.className('table table-striped')).element(built_1.by.tagName('tbody'));
        var gascomm = comm.all(built_1.by.tagName('td')).get(10);
        return gascomm;
    };
    BillComparisonPageObject.prototype.getGasCommBillPrevMonth = function () {
        var comm = built_1.element(built_1.by.className('table table-striped')).element(built_1.by.tagName('tbody'));
        var gascomm = comm.all(built_1.by.tagName('td')).get(13);
        return gascomm;
    };
    BillComparisonPageObject.prototype.getGasCommBillCurrentMonth = function () {
        var comm = built_1.element(built_1.by.className('table table-striped')).element(built_1.by.tagName('tbody'));
        var gascomm = comm.all(built_1.by.tagName('td')).get(14);
        return gascomm;
    };
    return BillComparisonPageObject;
}());
exports.BillComparisonPageObject = BillComparisonPageObject;
//# sourceMappingURL=BillComparison.po.js.map