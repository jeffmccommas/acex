(function (global) {
    var paths = {
        'n:*': 'node_modules/*',
        'npm:': 'node_modules/'
    };

    //map tells the System loader where to look for things
    var map = {
        'app': 'app',
        'rxjs': 'node_modules/rxjs',
        'ngx-bootstrap': 'node_modules/ngx-bootstrap',
        'ngx-bootstrap/ngx-bootstrap': 'node_modules/ngx-bootstrap/bundles/ngx-bootstrap.js',
        'ng-recaptcha': 'node_modules/ng-recaptcha',

        '@angular/core': 'node_modules/@angular/core/bundles/core.umd.min.js',
        '@angular/common': 'node_modules/@angular/common/bundles/common.umd.min.js',
        '@angular/compiler': 'node_modules/@angular/compiler/bundles/compiler.umd.min.js',
        '@angular/platform-browser': 'node_modules/@angular/platform-browser/bundles/platform-browser.umd.min.js',
        '@angular/platform-browser-dynamic': 'node_modules/@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.min.js',
        '@angular/http': 'node_modules/@angular/http/bundles/http.umd.min.js',
        '@angular/router': 'node_modules/@angular/router/bundles/router.umd.min.js',
        '@angular/forms': 'node_modules/@angular/forms/bundles/forms.umd.min.js',
        'angular2-in-memory-web-api': 'node_modules/angular2-in-memory-web-api',
        'angular-confirmation-popover': 'node_modules/angular-confirmation-popover/dist/umd/angular-confirmation-popover.js',

        // angular testing umd bundles
        '@angular/core/testing': 'npm:@angular/core/bundles/core-testing.umd.js',
        '@angular/common/testing': 'npm:@angular/common/bundles/common-testing.umd.js',
        '@angular/compiler/testing': 'npm:@angular/compiler/bundles/compiler-testing.umd.js',
        '@angular/platform-browser/testing': 'npm:@angular/platform-browser/bundles/platform-browser-testing.umd.js',
        '@angular/platform-browser-dynamic/testing': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic-testing.umd.js',
        '@angular/http/testing': 'npm:@angular/http/bundles/http-testing.umd.js',
        '@angular/router/testing': 'npm:@angular/router/bundles/router-testing.umd.js',
        '@angular/forms/testing': 'npm:@angular/forms/bundles/forms-testing.umd.js'
    };

    //packages tells the System loader how to load when no filename and/or no extension
    var packages = {
        '.': {},
        'app': { defaultExtension: 'js' },
        'rxjs': { defaultExtension: 'js' },
        'test': { defaultExtension: 'js' },
        'ng-recaptcha': { format: 'cjs', main: 'index.js', defaultExtension: 'js' },
        'ngx-bootstrap': { format: 'cjs', main: 'bundles/ngx-bootstrap.umd.js', defaultExtension: 'js' },
        'angular2-in-memory-web-api': { defaultExtension: 'js' }
    };
    
    var config = {
        map: map,
        packages: packages,
        paths: paths
    }

    if (global.filterSystemConfig) { global.filterSystemConfig(config); }

    System.config(config);
})(this);

