﻿using System;
using System.Collections.Generic;

namespace CE.InsightsWeb.Common.AclaraJson
{
    public class LegendData
    {
        public string Icon { get; set; }
        public string Text { get; set; }
        public string Usage { get; set; }
        public string Dollars { get; set; }
        public string Percent { get; set; }
        public string UOM { get; set; }
    }

    public class DisaggDetail
    {
        public string CommodityKey { get; set; }
        public double UsageQuantity { get; set; }
        public string UsageUOMKey { get; set; }
        public double CostAmount { get; set; }
        public string CostCurrencyKey { get; set; }
    }

    public class DisaggDetail2
    {
        public string CommodityKey { get; set; }
        public double UsageQuantity { get; set; }
        public string UsageUOMKey { get; set; }
        public double CostAmount { get; set; }
        public string CostCurrencyKey { get; set; }
    }

    public class Appliance
    {
        public string Key { get; set; }
        public List<DisaggDetail2> DisaggDetails { get; set; }
    }

    public class EndUs
    {
        public string Key { get; set; }
        public List<DisaggDetail> DisaggDetails { get; set; }
        public List<Appliance> Appliances { get; set; }
    }

    public class Premis
    {
        public string Id { get; set; }
        public List<DisaggStatus> DisaggStatuses { get; set; }
        public List<EndUs> EndUses { get; set; }
    }

    public class DisaggStatus
    {
        public Enums.DisaggStatusType Status { get; set; }
        public Enums.CommodityType CommodityKey { get; set; }
    }


    public class Account
    {
        public string Id { get; set; }
        public string BillStartDate { get; set; }
        public string BillEndDate { get; set; }
        public List<Premis> Premises { get; set; }
    }

    public class Customer
    {
        public List<Account> Accounts { get; set; }
    }

    public class Appliance2
    {
        public string key { get; set; }
        public string namekey { get; set; }
        public double averagecostperyear { get; set; }
        public string profileattributes { get; set; }
        public string expressions { get; set; }
    }

    public class Commodity
    {
        public string key { get; set; }
        public string namekey { get; set; }
    }

    public class Configuration
    {
        public string key { get; set; }
        public string category { get; set; }
        public string value { get; set; }
    }

    public class Currency
    {
        public string key { get; set; }
        public string namekey { get; set; }
        public string symbolkey { get; set; }
    }

    public class EndUse
    {
        public string key { get; set; }
        public string namekey { get; set; }
        public string category { get; set; }
        public string appliances { get; set; }
    }

    public class Measurement
    {
        public string key { get; set; }
        public string namekey { get; set; }
    }

    public class TextContent
    {
        public string key { get; set; }
        public string category { get; set; }
        public string shorttext { get; set; }
        public string mediumtext { get; set; }
        public string longtext { get; set; }
    }

    public class UOM
    {
        public string key { get; set; }
        public string namekey { get; set; }
    }

    public class Content
    {
        public List<Appliance2> Appliance { get; set; }
        public List<Commodity> Commodity { get; set; }
        public List<Configuration> Configuration { get; set; }
        public List<Currency> Currency { get; set; }
        public List<EndUse> EndUse { get; set; }
        public List<Measurement> Measurement { get; set; }
        public List<TextContent> TextContent { get; set; }
        public List<UOM> UOM { get; set; }
    }

    public class EndUseRoot
    {
        public int ClientId { get; set; }
        public Customer Customer { get; set; }
        public Content Content { get; set; }
    }

    //Profile
    public class Attribute
    {
        public string AttributeKey { get; set; }
        public string AttributeValue { get; set; }
        public string EffectiveDate { get; set; }
        public string SourceKey { get; set; }
    }

    public class ProfilePremis
    {
        public string Id { get; set; }
        public List<Attribute> Attributes { get; set; }
    }

    public class ProfileAccount
    {
        public string Id { get; set; }
        public List<Attribute> Attributes { get; set; }
        public List<ProfilePremis> Premises { get; set; }
    }

    public class ProfileCustomer
    {
        public string Id { get; set; }
        public List<Attribute> Attributes { get; set; }
        public List<ProfileAccount> Accounts { get; set; }
    }

    public class ProfileRoot
    {
        public int ClientId { get; set; }
        public ProfileCustomer Customer { get; set; }
    }

    //ActionPlan
    public class AdditionalInfo
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class ActionStatus
    {
        public string ActionKey { get; set; }
        public string Status { get; set; }
        public string StatusDate { get; set; }
        public string SourceKey { get; set; }
        public List<AdditionalInfo> AdditionalInfo { get; set; }
        public string SubActionKey { get; set; }
    }

    public class ActionPlanPremis
    {
        public string Id { get; set; }
        public List<ActionStatus> ActionStatuses { get; set; }
    }

    public class ActionPlanAccount
    {
        public string Id { get; set; }
        public List<ActionPlanPremis> Premises { get; set; }
    }

    public class ActionPlanCustomer
    {
        public string Id { get; set; }
        public List<ActionPlanAccount> Accounts { get; set; }
    }

    public class ActionPlanRoot
    {
        public int ClientId { get; set; }
        public ActionPlanCustomer Customer { get; set; }
    }

    //Action
    public class Action
    {
        public string Key { get; set; }
        public double AnnualCost { get; set; }
        public double UpfrontCost { get; set; }
        public double CostVariancePercent { get; set; }
        public double AnnualSavingsEstimate { get; set; }
        public string AnnualSavingsEstimateCurrencyKey { get; set; }
        public string SavingsEstimateQuality { get; set; }
        public List<ActionDetail> ActionDetails { get; set; }
        public double PaybackTime { get; set; }
        public double ROI { get; set; }
        public string Popularity { get; set; }
        public string Priority { get; set; }
        public string RebateAmount { get; set; }
    }

    public class BillHighlight
    {
        public string ActionKey { get; set; }
        public double CostImpact { get; set; }
        public double PercentageImpact { get; set; }
        public string Commoditykey { get; set; }
        public string ServiceId { get; set; }
        public string MeterId { get; set; }
    }

    public class AmiHighlight
    {
        public string ActionKey { get; set; }
        public double CostImpact { get; set; }
        public double PercentageImpact { get; set; }
        public string Commoditykey { get; set; }
        public string ServiceId { get; set; }
        public string MeterId { get; set; }
        public DateTime TimeStamp { get; set; }
    }

    public class ActionDetail
    {
        public string CommodityKey { get; set; }
        public double SavingsEstimate { get; set; }
        public string SavingsEstimateCurrencyKey { get; set; }
        public double UsageSavingsEstimate { get; set; }
        public string UsageSavingsEstimateUomKey { get; set; }
    }

    public class ActionPremis
    {
        public string Id { get; set; }
        public List<Action> Actions { get; set; }
        public List<BillHighlight> BillHighlights { get; set; }
        public List<AmiHighlight> AmiHighlights { get; set; }
    }

    public class ActionAccount
    {
        public string Id { get; set; }
        public List<ActionPremis> Premises { get; set; }
    }

    public class ActionCustomer
    {
        public string Id { get; set; }
        public List<ActionAccount> Accounts { get; set; }
    }

    public class ActionRoot
    {
        public int ClientId { get; set; }
        public ActionCustomer Customer { get; set; }
    }

    //Bill
    public class BillDetail
    {
        /// <summary>
        /// Name of the detail.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Value of the detail.
        /// </summary>
        public string Value { get; set; }
    }
    public class Service
    {
        public string Id { get; set; }
        public double TotalServiceUse { get; set; }
        public double CostofUsage { get; set; }
        public double AdditionalServiceCost { get; set; }
        public string CommodityKey { get; set; }
        public string UOMKey { get; set; }
        public string BillStartDate { get; set; }
        public string BillEndDate { get; set; }
        public int BillDays { get; set; }
        public int AverageTemperature { get; set; }
        public string RateClass { get; set; }
        public List<BillDetail> CostDetails { get; set; }
    }

    public class BillPremis
    {
        public string Id { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public List<Service> Service { get; set; }
        public string LatestBillEndDate { get; set; }
    }

    public class Bill
    {
        public double TotalAmount { get; set; }
        public double AdditionalBillCost { get; set; }
        public string BillDate { get; set; }
        public string BillFrequency { get; set; }
        public string BillDueDate { get; set; }
        public List<BillPremis> Premises { get; set; }
        public string LatestBillEndDate { get; set; }
    }

    public class BillAccount
    {
        public string Id { get; set; }
        public List<Bill> Bills { get; set; }
    }

    public class BillCustomer
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<BillAccount> Accounts { get; set; }
    }

    public class BillRoot
    {
        public int ClientId { get; set; }
        public BillCustomer Customer { get; set; }
    }

    public class WebTokenResponse
    {
        public bool IsActive { get; set; }
        public System.DateTime? CreatedUtcDateTime { get; set; }
        public string AdditionalInfo { get; set; }
    }

    public class BenchmarkMeasurement
    {
        public string Key { get; set; }
        public double MyQuantity { get; set; }
        public double AverageQuantity { get; set; }
        public double EfficientQuantity { get; set; }
        public string UOMKey { get; set; }
    }

    public class Benchmark
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CommodityKey { get; set; }
        public string GroupKey { get; set; }
        public int GroupCount { get; set; }
        public double MyUsage { get; set; }
        public double AverageUsage { get; set; }
        public double EfficientUsage { get; set; }
        public string UOMKey { get; set; }
        public double MyCost { get; set; }
        public double AverageCost { get; set; }
        public double EfficientCost { get; set; }
        public string CostCurrencyKey { get; set; }
        public List<BenchmarkMeasurement> Measurements { get; set; }
    }

    public class BenchmarkPremis
    {
        public string Id { get; set; }
        public List<Benchmark> Benchmarks { get; set; }
    }

    public class BenchmarkAccount
    {
        public string Id { get; set; }
        public List<BenchmarkPremis> Premises { get; set; }
    }

    public class BenchmarkCustomer
    {
        public string Id { get; set; }
        public List<BenchmarkAccount> Accounts { get; set; }
    }

    public class BenchmarkRoot
    {
        public int ClientId { get; set; }
        public BenchmarkCustomer Customer { get; set; }
    }

    public class Datum
    {
        public string DateTime { get; set; }
        public double Value { get; set; }
        public List<TouConsumption> TouConsumptions { get; set; }
    }

    public class TouConsumption
    {
        public string TouKey { get; set; }
        public double Value { get; set; }
    }
    public class ConsumptionWeather
    {    
        public string Date { get; set; }
        public int AvgTemp { get; set; }
    }
    public class ConsumptionRoot
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string MeterIds { get; set; }
        public string ResolutionKey { get; set; }
        public string ResolutionsAvailable { get; set; }
        public string CommodityKey { get; set; }
        public string UOMKey { get; set; }
        public double AverageUsage { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public List<Datum> Data { get; set; }
        public List<ConsumptionWeather> Weather { get; set; }
    }

    public class CustomerInfoMeter
    {
        public string Id { get; set; }
        public string IsDeleted { get; set; }
        public string ReplacedMeterId { get; set; }
    }

    public class CustomerInfoServicePoint
    {
        public string Id { get; set; }
        public List<CustomerInfoMeter> Meters { get; set; }
    }

    public class CustomerInfoService
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string ActiveDate { get; set; }
        public string InActiveDate { get; set; }
        public string CommodityKey { get; set; }
        public string BudgetBillingInd { get; set; }
        public List<CustomerInfoServicePoint> ServicePoints { get; set; }
    }

    public class CustomerInfoPremise
    {
        public string Id { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public List<CustomerInfoService> Service { get; set; }
    }

    public class CustomerInfoAccount
    {
        public string Id { get; set; }
        public List<CustomerInfoPremise> Premises { get; set; }
    }


    public class ReportList
    {
        public List<Report> Files { get; set; }
    }

    public class Report
    {
        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string RoleId { get; set; }
        public string UserId { get; set; }
        public string TypeId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string FileSourceId { get; set; }
        public string CreateDate { get; set; }
        public string RelativeTime { get; set; }
    }

    public class CustomerInfoCustomer
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string AlternateEmailAddress { get; set; }
        public string Language { get; set; }
        public List<CustomerInfoAccount> Accounts { get; set; }
    }

    public class CustomerInfoRoot
    {
        public int ClientId { get; set; }
        public CustomerInfoCustomer Customer { get; set; }
    }

    public class BillToDateService
    {
        public string Id { get; set; }
        public double UseToDate { get; set; }
        public double UseProjected { get; set; }
        public double CostToDate { get; set; }
        public double CostProjected { get; set; }
        public string CommodityKey { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ReadDate { get; set; }
        public int DaysIntoCycle { get; set; }
        public string UomKey { get; set; }
        public string RateClass { get; set; }
    }

    public class BillToDate
    {
        public string BillStartDate { get; set; }
        public string BillEndDate { get; set; }
        public double BillDays { get; set; }
        public double TotalCost { get; set; }
        public double TotalProjectedCost { get; set; }
        public double AverageDailyCost { get; set; }
        public List<BillToDateService> Services { get; set; }
        public bool HasError { get; set; }
    }

    public class BillToDateRoot
    {
        public BillToDate BillToDate { get; set; }
    }


    public class RataClass
    {
        public string RateClassID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CommodityKey { get; set; }
        public string CustomerType { get; set; }
        public bool ContainsPTR { get; set; }
        public bool IsTiered { get; set; }
        public bool IsTimeOfUse { get; set; }
        public bool IsSeasonal { get; set; }
        public string UomKey { get; set; }
    }

    public class UseCharge
    {
        public string BaseOrTierKey { get; set; }
        public string TimeOfUseKey { get; set; }
        public string SeasonKey { get; set; }
        public string PartKey { get; set; }
        public string CostKey { get; set; }
        public bool IsFuelCostRecovery { get; set; }
        public bool IsRealTimePricing { get; set; }
        public double ChargeValue { get; set; }
    }

    public class ServiceCharge
    {
        public string CalculationKey { get; set; }
        public string StepKey { get; set; }
        public string SeasonKey { get; set; }
        public string RebateClassKey { get; set; }
        public string CostKey { get; set; }
        public double ChargeValue { get; set; }
    }

    public class TierBoundary
    {
        public string TimeOfUseKey { get; set; }
        public string SeasonKey { get; set; }
        public double Threshold { get; set; }
        public string BaseOrTierKey { get; set; }
    }

    public class FinalizedTierBoundary
    {
        public string TimeOfUseKey { get; set; }
        public string SeasonKey { get; set; }
        public double Threshold { get; set; }
        public string BaseOrTierKey { get; set; }
    }

    public class TouBoundary
    {
        public string TimeOfUseKey { get; set; }
        public string SeasonKey { get; set; }
        public string DayKey { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }

    public class SeasonBoundary
    {
        public string SeasonKey { get; set; }
        public string StartDay { get; set; }
        public string EndDay { get; set; }
    }

    public class Details
    {
        public List<UseCharge> UseCharges { get; set; }
        public List<ServiceCharge> ServiceCharges { get; set; }
        public List<TierBoundary> TierBoundaries { get; set; }
        public List<FinalizedTierBoundary> FinalizedTierBoundaries { get; set; }
        public List<TouBoundary> TouBoundaries { get; set; }
        public List<SeasonBoundary> SeasonBoundaries { get; set; }
        public List<object> MinimumCharges { get; set; }
        public List<object> TaxCharges { get; set; }
    }

    public class RateRoot
    {
        public RataClass RateClass { get; set; }
        public Details Details { get; set; }
    }

    public class SubscriptionInsight
    {
        public string Name { get; set; }
        public string Level { get; set; }
        public double? Threshold { get; set; }
        public bool IsSelected { get; set; }
        public string AllowedChannels { get; set; }
        public string Channel { get; set; }
        public string ThresholdType { get; set; }
        public decimal? ThresholdMin { get; set; }
        public decimal? ThresholdMax { get; set; }
        public string UomKey { get; set; }
    }

    public class SubscriptionService
    {
        public string Id { get; set; }
        //public string ContractId { get; set; }
        public string CommodityKey { get; set; }
        public List<SubscriptionInsight> Insights { get; set; }
    }

    public class SubscriptionPremis
    {
        public string Id { get; set; }
        public List<SubscriptionInsight> Insight { get; set; }
        public List<SubscriptionService> Services { get; set; }
    }

    public class SubscriptionAccount
    {
        public string Id { get; set; }
        public List<SubscriptionPremis> Premises { get; set; }
        public List<SubscriptionInsight> Insights { get; set; }
    }

    public class SubscriptionCustomer
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public List<SubscriptionInsight> Insights { get; set; }
        public List<SubscriptionAccount> Accounts { get; set; }
    }

    public class SubscriptionRoot
    {
        public SubscriptionCustomer Customer { get; set; }
    }

    public class SubscribedCustomerInsight
    {
        public string Id { get; set; }
        public List<SubscribedInsight> Insights { get; set; }
    }
    public class SubscribedInsight
    {
        public string Name { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string ServiceId { get; set; }
        //public string ServicePointId { get; set; }
        public string Level { get; set; }
        public bool IsSelected { get; set; }
        public decimal? Threshold { get; set; }
        public string Channel { get; set; }
    }

    public class Status
    {
        public string Message { get; set; }
        public StatusType StatusType { get; set; }
    }

    public enum StatusType
    {
        Undefined,
        ParameterErrors,
        ModelErrors
    }

    //Energy Model

    public class CustomerParameters
    {
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string Zipcode { get; set; }
    }

    public class RegionParameters
    {
        public int TemperatureRegionId { get; set; }
        public int SolarRegionId { get; set; }
        public int ApplianceRegionId { get; set; }
        public string State { get; set; }
    }

    public class TotalEnergyUse
    {
        public string CommodityName { get; set; }
        public double Quantity { get; set; }
        public double Cost { get; set; }
        public double RecRatio { get; set; }
        public double WeatherFactor { get; set; }
        public string UnitOfMeasureName { get; set; }
    }

    public class EvaluationContext
    {
        public string ContextName { get; set; }
    }

    public class ModelEnergyUse
    {
        public string CommodityName { get; set; }
        public EvaluationContext EvaluationContext { get; set; }
        public double Quantity { get; set; }
        public double Cost { get; set; }
        public double RecRatio { get; set; }
        public double WeatherFactor { get; set; }
        public string UnitOfMeasureName { get; set; }
    }

    public class ModelAppliance
    {
        public string Name { get; set; }
        public int Confidence { get; set; }
        public List<ModelEnergyUse> EnergyUses { get; set; }
    }

    public class ModelEnergyUse2
    {
        public string CommodityName { get; set; }
        public double Quantity { get; set; }
        public double Cost { get; set; }
        public double RecRatio { get; set; }
        public double WeatherFactor { get; set; }
        public string UnitOfMeasureName { get; set; }
    }

    public class ModelEndUse
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public int Confidence { get; set; }
        public List<ModelEnergyUse2> EnergyUses { get; set; }
        public List<string> ApplianceNames { get; set; }
    }

    public class Metric
    {
        public string Name { get; set; }
        public int Duration { get; set; }
    }

    public class Data
    {
        public int ClientId { get; set; }
        public CustomerParameters CustomerParameters { get; set; }
        public RegionParameters RegionParameters { get; set; }
        public int Confidence { get; set; }
        public List<TotalEnergyUse> TotalEnergyUses { get; set; }
        public List<ModelAppliance> Appliances { get; set; }
        public List<ModelEndUse> EndUses { get; set; }
        public List<Metric> Metrics { get; set; }
    }

    public class ModelRoot
    {
        public Data Data { get; set; }
    }

    //End Energy Model

}