#region Using

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;

#endregion

// ReSharper disable CheckNamespace
namespace CE.InsightsWeb.Common.QueryStringEncryptor
{

    /// <inheritdoc />
    /// <summary>
    /// Summary description for QueryStringModule
    /// </summary>
    public class QueryStringModule : IHttpModule
    {

        #region IHttpModule Members

        public void Dispose()
        {
            // Nothing to dispose
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += Context_BeginRequest;
        }

        #endregion

        private const string ParameterName = "enc=";
        private const string EncryptionKey = "UFXAclaraSoftware";

        private static void Context_BeginRequest(object sender, EventArgs e)
        {
            var context = HttpContext.Current;
            if (!context.Request.RawUrl.Contains("Page")) return;

            var query = ExtractQuery(context.Request.RawUrl);
            var path = GetVirtualPath();

            var indexOfEnc = query.IndexOf(ParameterName, StringComparison.OrdinalIgnoreCase);

            if (indexOfEnc > -1)
            {
                // Decrypts the query string and rewrites the path.                   
                context.RewritePath(path, string.Empty, query.Substring(0, indexOfEnc) + Decrypt(query.Substring(indexOfEnc + ParameterName.Length)));
            }
            else if (context.Request.HttpMethod == "GET" || context.Request.HttpMethod == "POST")
            {
                // Encrypt the query string and redirects to the encrypted URL.
                // Remove if you don't want all query strings to be encrypted automatically.
                var encryptedQuery = Encrypt(query);
                var tempRawUrl = context.Request.RawUrl.ToLower();
                if (!(context.Request.HttpMethod == "POST" && tempRawUrl.Contains("ha")))
                {
                    context.Response.Redirect(path + encryptedQuery);
                }
            }
        }

        /// <summary>
        /// Parses the current URL and extracts the virtual path without query string.
        /// </summary>
        /// <returns>The virtual path of the current URL.</returns>
        private static string GetVirtualPath()
        {
            var path = HttpContext.Current.Request.RawUrl;
            path = path.Substring(0, path.IndexOf("?", StringComparison.Ordinal));
            path = path.Substring(path.LastIndexOf("/", StringComparison.Ordinal) + 1);
            return path;
        }

        /// <summary>
        /// Parses a URL and returns the query string.
        /// </summary>
        /// <param name="url">The URL to parse.</param>
        /// <returns>The query string without the question mark.</returns>
        private static string ExtractQuery(string url)
        {
            var index = url.IndexOf("?", StringComparison.Ordinal) + 1;
            return url.Substring(index);
        }

        #region Encryption/decryption

        /// <summary>
        /// The salt value used to strengthen the encryption.
        /// </summary>
        private static readonly byte[] Salt = Encoding.ASCII.GetBytes(EncryptionKey.Length.ToString());

        /// <summary>
        /// Encrypts any string using the Rijndael algorithm.
        /// </summary>
        /// <param name="inputText">The string to encrypt.</param>
        /// <returns>A Base64 encrypted string.</returns>
        public static string Encrypt(string inputText)
        {
            var rijndaelCipher = new RijndaelManaged();
            var plainText = Encoding.Unicode.GetBytes(inputText);
            var secretKey = new PasswordDeriveBytes(EncryptionKey, Salt);

#pragma warning disable 618
            using (var encryptor = rijndaelCipher.CreateEncryptor(secretKey.GetBytes(32), secretKey.GetBytes(16)))
#pragma warning restore 618
            {
                using (var memoryStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(plainText, 0, plainText.Length);
                        cryptoStream.FlushFinalBlock();
                        return "?" + ParameterName + Convert.ToBase64String(memoryStream.ToArray());
                    }
                }
            }
        }

        /// <summary>
        /// Decrypts a previously encrypted string.
        /// </summary>
        /// <param name="inputText">The encrypted string to decrypt.</param>
        /// <returns>A decrypted string.</returns>
        public static string Decrypt(string inputText)
        {
            var rijndaelCipher = new RijndaelManaged();
            var encryptedData = Convert.FromBase64String(inputText);
            var secretKey = new PasswordDeriveBytes(EncryptionKey, Salt);

#pragma warning disable 618
            using (var decryptor = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16)))
#pragma warning restore 618
            {
                using (var memoryStream = new MemoryStream(encryptedData))
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        var plainText = new byte[encryptedData.Length];
                        var decryptedCount = cryptoStream.Read(plainText, 0, plainText.Length);
                        return Encoding.Unicode.GetString(plainText, 0, decryptedCount);
                    }
                }
            }
        }

        #endregion

        public static string ConvertStringToHex(string input, Encoding encoding)
        {
            var stringBytes = encoding.GetBytes("Acl@ra" + input);
            var sbBytes = new StringBuilder(stringBytes.Length * 2);
            foreach (var b in stringBytes)
            {
                sbBytes.AppendFormat("{0:X2}", b);
            }
            return sbBytes.ToString();
        }
        public static string ConvertHexToString(string hexInput, Encoding encoding)
        {
            var numberChars = hexInput.Length;
            var bytes = new byte[numberChars / 2];
            for (var i = 0; i < numberChars; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(hexInput.Substring(i, 2), 16);
            }
            return encoding.GetString(bytes).Substring(6);
        }
    }
}
//public static string EncryptQueryString(string url)
//{
//    var context = HttpContext.Current;
//    string path;
//    if (url.Contains("?"))
//    {

//        path = url.Substring(0, url.IndexOf("?", StringComparison.Ordinal));
//        var query = ExtractQuery(url);

//        if (context.Request.HttpMethod != "GET" && context.Request.HttpMethod != "POST") return path;
//        // Encrypt the query string and redirects to the encrypted URL.
//        // Remove if you don't want all query strings to be encrypted automatically.
//        var encryptedQuery = Encrypt(query);
//        path = path + encryptedQuery;
//    }
//    else
//    {
//        path = url;
//    }
//    return path;
//}