﻿namespace CE.InsightsWeb.Common
{
    public static class Constants
    {
        public static string kClientId = "ClientId";
        public static string kCustomerId = "CustomerId";
        public static string kAccountId = "AccountId";
        public static string kPremiseId = "PremiseId";
        public static string kPremiseType = "PremiseType";
        public static string kPremiseTypeResidential = "premisetype.residential";
        public static string kPremiseTypeBusiess = "premisetype.business"; 
        public static string kServicePointId = "ServicePointId";
        public static string kWebToken = "WebToken";
        public static string kLocale = "Locale";
        public static int kCache_Expiry = 20;
        public static string kLocaleEnglish = "en-US";
        public static string kInvalidWebToken = "Invalid webtoken.";
        public static string kUserId = "UserId";
        public static string kGroupName = "GroupName";
        public static string kRoleName = "RoleName";
        public static string kId = "id";
        public static string kSid = "sid";
        public static string kEnableRemote = "enableRemote";
        public static string kShowSubTabs = "ShowSubTabs";
    }
}
