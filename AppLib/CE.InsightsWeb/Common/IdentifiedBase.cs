﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using CE.InsightsIntegration.Models;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using CE.InsightsWeb.Helpers;
using CE.InsightsWeb.Models;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Common.Base
{
    public class IdentifiedBase : Controller
    {
        protected JsonParams Parameters;

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (Request.IsAjaxRequest() || requestContext.RouteData.Values["controller"].ToString() == "login") return;
            if (Request[Constants.kClientId] == null || Request[Constants.kCustomerId] == null ||
                Request[Constants.kAccountId] == null || Request[Constants.kPremiseId] == null ||
                Request[Constants.kServicePointId] == null || Request[Constants.kWebToken] == null) return;
            Parameters = new JsonParams
            {
                cl = Request[Constants.kClientId],
                c = Request[Constants.kCustomerId],
                a = Request[Constants.kAccountId],
                p = Request[Constants.kPremiseId],
                pt = Request[Constants.kPremiseType] ?? Constants.kPremiseTypeResidential,
                s = Request[Constants.kServicePointId],
                w = Request[Constants.kWebToken],
                l = Request[Constants.kLocale] ?? Constants.kLocaleEnglish,
                u = Request[Constants.kUserId],
                g = Request[Constants.kGroupName],
                r = Request[Constants.kRoleName],
                id = Request[Constants.kId],
                sid = Request[Constants.kSid],
                st = Request[Constants.kShowSubTabs]
            };

            var rm = new APIResponseModels();

            if (HttpRuntime.Cache["WebTokenValidated" + Parameters.cl + "" + Parameters.c + "" + Parameters.a + "" + Parameters.p + "" + Parameters.l + "" + Parameters.w] == null)
            {
                HttpRuntime.Cache.Insert("WebTokenValidated" + Parameters.cl + "" + Parameters.c + "" + Parameters.a + "" + Parameters.p + "" + Parameters.l + "" + Parameters.w, "true",
                    null, DateTime.Now.AddMinutes(1), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);

                string webTokenStr;
                if (Parameters.u != null && Parameters.g != null && Parameters.r != null)
                {
                    webTokenStr = "CustomerId=" + Parameters.c + "&WebToken=" + Parameters.w + "&UserId=" + Parameters.u + "&GroupName=" + Parameters.g + "&RoleName=" + Parameters.r;
                }
                else
                {
                    webTokenStr = "CustomerId=" + Parameters.c + "&WebToken=" + Parameters.w;
                }
                var resp = (Response)Json(rm.GetWebToken(webTokenStr, Parameters.l, Convert.ToInt32(Parameters.cl)), JsonRequestBehavior.AllowGet).Data;
                if (resp.StatusCode == 200)
                {
                    var wtr = (WebTokenResponse)JsonConvert.DeserializeObject(resp.Content,
                        typeof(WebTokenResponse));
                    if (!wtr.IsActive)
                    {
                        throw new Exception(Constants.kInvalidWebToken);
                    }
                }
                else
                {
                    throw new Exception(Constants.kInvalidWebToken);
                }
            }

            if (HttpRuntime.Cache["Login" + Request[Constants.kClientId] + "" + Request[Constants.kCustomerId] + "" +Request[Constants.kWebToken]] != null) return;

            var p = new ProfileModels();
            var profileResponse = p.GetProfileAttributesFromCache(Parameters);

            var etm = new EventTrackingModels();

            //var browser = Request.Browser;
            //var s = "Browser Capabilities\n"
            //           + "Type = " + browser.Type + "\n"
            //           + "Name = " + browser.Browser + "\n"
            //           + "Version = " + browser.Version + "\n"
            //           + "Major Version = " + browser.MajorVersion + "\n"
            //           + "Minor Version = " + browser.MinorVersion + "\n"
            //           + "Platform = " + browser.Platform + "\n"
            //           + "Is Beta = " + browser.Beta + "\n"
            //           + "Is Crawler = " + browser.Crawler + "\n"
            //           + "Is AOL = " + browser.AOL + "\n"
            //           + "Is Win16 = " + browser.Win16 + "\n"
            //           + "Is Win32 = " + browser.Win32 + "\n"
            //           + "Supports Frames = " + browser.Frames + "\n"
            //           + "Supports Tables = " + browser.Tables + "\n"
            //           + "Supports Cookies = " + browser.Cookies + "\n"
            //           + "Supports VBScript = " + browser.VBScript + "\n"
            //           + "Supports JavaScript = " + browser.EcmaScriptVersion + "\n"
            //           + "Supports Java Applets = " + browser.JavaApplets + "\n"
            //           + "Supports ActiveX Controls = " + browser.ActiveXControls + "\n"
            //           + "Supports JavaScript Version = " + browser["JavaScriptVersion"] + "\n";
                    
            var ca = Dashboard.LayoutFramework.Layout.CombineAttributes(profileResponse);

            var eventAttribute = new Dictionary<string, string>();
            if (ca != null)
            {
                if (ca.FirstOrDefault(x => x.Key == "firstlogindate") == null)
                {
                    eventAttribute.Add("NewUser", "yes");
                    var quest = new Question
                    {
                        Key = "firstlogindate",
                        Answer = DateTime.UtcNow.ToString("s"),
                        Level = "premise"
                    };
                    var pm = new ProfileModels();
                    pm.WriteProfileAttributes(
                        QueryStringModule.Encrypt(JsonConvert.SerializeObject(Parameters)).Substring(5),
                        quest);
                }
                else
                {
                    eventAttribute.Add("NewUser", "no");
                }
                        
                if (ca.FirstOrDefault(x => x.Key == "hasactionplan") == null)
                {
                    var actionPlan = WaysToSaveModels.GetActionPlanFromCache(Parameters);
                    if (actionPlan?.Customer?.Accounts != null && actionPlan.Customer.Accounts.Count > 0)
                    {
                        if (actionPlan.Customer.Accounts[0].Premises != null &&
                            actionPlan.Customer.Accounts[0].Premises.Count > 0)
                        {
                            if (actionPlan.Customer.Accounts[0].Premises[0].ActionStatuses != null &&
                                actionPlan.Customer.Accounts[0].Premises[0].ActionStatuses.Count > 0)
                            {
                                var quest = new Question
                                {
                                    Key = "hasactionplan",
                                    Answer = "yes",
                                    Level = "premise"
                                };
                                var pm = new ProfileModels();
                                pm.WriteProfileAttributes(
                                    QueryStringModule.Encrypt(JsonConvert.SerializeObject(Parameters)).Substring(5),
                                    quest);
                            }
                        }
                    }
                }
            }
            var response = (Response)JsonConvert.DeserializeObject(etm.PostEvents(eventAttribute,
                    QueryStringModule.Encrypt(JsonConvert.SerializeObject(Parameters)).Substring(5),"Aclara Website Login"),typeof(Response));
            HttpRuntime.Cache.Insert("Login" + Request[Constants.kClientId] + "" + Request[Constants.kCustomerId] + "" +
                                     Request[Constants.kWebToken],true, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration,
                CacheItemPriority.NotRemovable, null);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.IsDebuggingEnabled == false)
            {
                //if (!Request.IsAjaxRequest())
                //{
                GeneralHelper.Compress(filterContext);
                //}
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext?.Exception != null)
            {
                //GeneralHelper.LogError(filterContext.Exception, area);
            }
            base.OnException(filterContext);
            ViewPageExtensions.SetupMVCError(filterContext);
        }
    }
}