﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Helpers;
using CE.InsightsWeb.Models;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;


namespace CE.Dashboard.LayoutFramework
{
    public class WidgetInfo
    {
        public readonly string WidgetList;
        public string Parameters { get; set; }
        public string TabKey { get; private set; }
        public string LoadingText { get; private set; }
        public string WidgetError { get; private set; }
        public string Browser { get; private set; }
        public string PartialBrowserSupport { get; private set; }
        public bool IsCsr { get; private set; }
        public WidgetInfo()
        {
        }
        public WidgetInfo(string clientId, string tabKey, string widetType, string locale)
        {
            var rc = CMS.GetContent(int.Parse(clientId), locale);
            LoadingText = CMS.GetCommonContent(rc, "common.loadingmessage");
            WidgetError = CMS.GetCommonContent(rc, "common.fatalerrormessage");
            TabKey = tabKey;
            WidgetList = widetType;
            Browser = GeneralHelper.GetBrowser();
            PartialBrowserSupport = CMS.GetCommonContent(rc, "common.partialbrowsermessage");
            IsCsr = false;
        }
    }
    public class Layout
    {
        public static Layoutjson GetLayout(int clientId, int tabId, string locale, string customerId, string accountId, string premiseId, string premiseType, string language, string webToken, string userId, string groupName, string roleName) => GetLayoutFromContent(tabId, clientId, locale, customerId, accountId, premiseId, premiseType, webToken, userId, groupName, roleName);
        private static Layoutjson GetLayoutFromContent(int tabId, int clientId, string locale, string customerId, string accountId, string premiseId, string premiseType, string webToken, string userId, string groupName, string roleName)
        {
            var rc = CMS.GetContent(clientId, locale);
            var parameters = new JsonParams
            {
                cl = clientId.ToString(),
                c = customerId,
                a = accountId,
                p = premiseId,
                pt = premiseType,
                s = "",
                w = webToken,
                l = locale ?? Constants.kLocaleEnglish
            };
            var pm = new ProfileModels();
            var profileResponse = pm.GetProfileAttributesFromCache(parameters);
            var profAttr = CombineAttributes(profileResponse);
            var tabs = rc.Content.Tab.Where(x => x.url == "C" + tabId.ToString());
            var tabkey = string.Empty;
            foreach (var t in tabs)
            {
                var conditionList = new List<Condition>();
                if (t.conditions.Length > 0)
                {
                    var s = t.conditions.Split(';');
                    conditionList.AddRange(s.Select(i => rc.Content.Condition.Where(x => x.key == i).FirstOrDefault()));
                }

                if (!ProfileModels.ValidateConditions(conditionList, profAttr)) continue;
                tabkey = t.key;
                break;
            }

            var layoutkey = rc.Content.Tab.Where(x => x.url == "C" + tabId.ToString()).Select(x => x.layoutkey).FirstOrDefault();
            var layout = rc.Content.Layout.Where(x => x.key == layoutkey).Select(x => x.layoutjson).FirstOrDefault();
            layout.WidgetList = "";
            if (ViewPageExtensions.ValidateConditions(tabId, clientId, userId, groupName, roleName, webToken))
            {
                var rn = 0;
                foreach (var r in layout.Layout.Rows)
                {
                    rn++;
                    var cn = 0;
                    foreach (var c in r.Columns)
                    {
                        cn++;
                        var widgets = new List<WidgetList>();
                        //get a list of widgets for this location
                        var wgts =
                            rc.Content.Widget.Where(t => t.tabkey == tabkey)
                                .Where(x => x.row == rn)
                                .Where(x => x.column == cn)
                                .OrderBy(x => x.order)
                                .ToList();

                        foreach (var wg in wgts)
                        {
                            var conditionList = new List<Condition>();
                            if (wg.conditions.Length > 0)
                            {
                                var s = wg.conditions.Split(';');
                                conditionList.AddRange(
                                    s.Select(i => rc.Content.Condition.Where(x => x.key == i).FirstOrDefault()));
                            }

                            //Evaluate condition for this widget   
                            // first time user with no attributes?
                            if (profileResponse.Customer != null)
                            {


                                if (ProfileModels.ValidateConditions(conditionList, profAttr))
                                {
                                    if (!layout.WidgetList.Contains(wg.widgettype))
                                    {
                                        layout.WidgetList += wg.widgettype + ",";
                                    }
                                    var wl = new WidgetList
                                    {
                                        Name = wg.widgettype,
                                        Instance = "row" + wg.row + "col" + wg.column + "order" + wg.order
                                    };
                                    widgets.Add(wl);
                                }
                            }
                            else
                            {
                                if (!layout.WidgetList.Contains(wg.widgettype))
                                {

                                    layout.WidgetList += wg.widgettype + ",";

                                }
                                var wl = new WidgetList
                                {
                                    Name = wg.widgettype,
                                    Instance = "row" + wg.row + "col" + wg.column + "order" + wg.order
                                };
                                widgets.Add(wl);
                            }
                            c.Widgets = widgets;
                        }
                    }
                }
            }

            layout.WidgetList = layout.WidgetList.TrimEnd(' ', ',');
            layout.TabKey = tabkey;
            layout.LoadingText = CMS.GetCommonContent(rc, "common.loadingmessage");
            layout.WidgetError = CMS.GetCommonContent(rc, "common.fatalerrormessage");
            layout.PartialBrowserSupport = CMS.GetCommonContent(rc, "common.partialbrowsermessage");
            layout.YellowFadeColor = CMS.GetCommonContent(rc, "common.yellowfadetechniquecolor");

            layout.Json = JsonConvert.SerializeObject(layout.Layout);

            return layout;
        }
        public static List<Question> CombineAttributes(ProfileRoot profileResponse)
        {
            Mapper.CreateMap<Attribute, Question>()
                                      .ForMember(dest => dest.Key, opt => opt.MapFrom(src => src.AttributeKey))
                                      .ForMember(dest => dest.Answer, opt => opt.MapFrom(src => src.AttributeValue));

            var profAttr = new List<Question>();
            IEnumerable<Question> profAttrAcct = null;
            if (profileResponse.Customer == null) return profAttr;
            if (profileResponse.Customer.Accounts != null && profileResponse.Customer.Accounts.Count > 0)
            {
                if (profileResponse.Customer.Accounts[0].Premises?.Count > 0)
                {
                    Mapper.Map(profileResponse.Customer.Accounts[0].Premises[0].Attributes, profAttr);
                }
                if (profileResponse.Customer.Accounts.Count > 0)
                {
                    profAttrAcct = Mapper.Map(profileResponse.Customer.Accounts[0].Attributes, new List<Question>());
                }
            }
            IEnumerable<Question> profAttrCust = Mapper.Map(profileResponse.Customer.Attributes, new List<Question>());

            if (profAttrAcct != null)
                profAttr.AddRange(profAttrAcct);
            profAttr.AddRange(profAttrCust);

            return profAttr;
        }
    }
}
