﻿using System.Web.Mvc;
using CE.InsightsWeb.Helpers;

namespace CE.InsightsWeb.Common.Filters
{
    public class CompressAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            GeneralHelper.Compress(filterContext);
        }
    }
}
