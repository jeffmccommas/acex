﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Configuration;
using System.Web.Mvc;
using CE.Dashboard.LayoutFramework;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using CE.InsightsWeb.Helpers;
using CE.InsightsWeb.Models;
using Fasterflect;
using Newtonsoft.Json;

public static class ViewPageExtensions
{
    #region Declarations
    private const string DefaultClientId = "Default";
    #endregion

    public static bool IsCSR(this WebViewPage webPage) => ValidateCsr(webPage.Request[Constants.kUserId], webPage.Request[Constants.kGroupName], webPage.Request[Constants.kRoleName], webPage.Request[Constants.kWebToken]);

    public static bool IsDebugEnabled(this WebViewPage webPage) => ((CompilationSection)ConfigurationManager.GetSection(@"system.web/compilation")).Debug;

    public static bool ShowPremiseSelect(this WebViewPage webPage)
    {
        var content = CMS.GetContent(int.Parse(webPage.Request[Constants.kClientId]), webPage.Request[Constants.kLocale], true).Content;
        var enablePremiseSelect = CMS.GetCommonContent(content, "premiseselect.enablepremiseselect", "config");
        if (enablePremiseSelect != "true")
        {
            return false;
        }

        var showSinglePremise = CMS.GetCommonContent(content, "premiseselect.showsinglepremise", "config");
        var customerInfo = CustomerInfoModels.GetCustomerInfoFromCache(CreateParams(webPage));
        if (customerInfo.Customer == null)
        {
            return false;
        }
        var acctCount = customerInfo.Customer.Accounts.Count;
        var premCount = customerInfo.Customer.Accounts.SelectMany(x => x.Premises).Count();

        return acctCount != 1 || premCount != 1 || showSinglePremise == "true";
    }

    public static bool HideMenu(this WebViewPage webPage) => webPage.Request["HideMenu"] != null && webPage.Request["HideMenu"] == "true";
    public static bool EnableRemote(this WebViewPage webPage) => webPage.Request["tmpl"] != null && webPage.Request["tmpl"] == "rc";

    public static bool EnableRemoteContainerCheck(this WebViewPage webPage) => webPage.Request["tmpl"] != null && webPage.Request["tmpl"] == "rc" && (webPage.Request[Constants.kClientId] != "4" && webPage.Request[Constants.kClientId] != "118");

    public static MvcHtmlString WriteScriptBlocks(this WebViewPage webPage)
    {
        var sb = new StringBuilder();
        var isDebugEnabled = ((CompilationSection)ConfigurationManager.GetSection(@"system.web/compilation")).Debug;

        if (!isDebugEnabled)
        {
            sb.Append("<script src='./App/app.module.min.js'></script>");
        }

        var controller = webPage.ViewContext.RouteData.GetRequiredString("controller");

        switch (controller)
        {
            case "Page":
                {
                    var m = (Layoutjson)webPage.Model;

                    if (m != null)
                    {
                        sb.Append(CreateResourceSets(m.WidgetList, webPage, isDebugEnabled));
                    }
                }
                break;
            case "Widget":
                {
                    var m = (WidgetInfo)webPage.Model;

                    if (m != null)
                    {
                        sb.Append(CreateResourceSets(m.WidgetList, webPage, isDebugEnabled));
                    }
                }
                break;
        }

        var wt = webPage.Request[Constants.kWebToken];
        if (wt == null) return MvcHtmlString.Create("");
        var browser = GeneralHelper.GetBrowser().Split(';')[0];
        if (string.IsNullOrEmpty(browser))
        {
            throw new Exception("Un-Supported Browser");
        }
        if (browser == "-firefox")
        {
            browser = "";
        }

        try
        {
            sb.Append("<link href='" + Combres.WebExtensions.CombresUrl(webPage.Request[Constants.kClientId] + browser) + "' rel='stylesheet' type='text/css'/>");
            return MvcHtmlString.Create(sb.ToString());
        }
        catch (Exception)
        {
            sb.Append("<link href='" + Combres.WebExtensions.CombresUrl(DefaultClientId + browser) + "' rel='stylesheet' type='text/css'/>");
            return MvcHtmlString.Create(sb.ToString());
        }
    }

    public static MvcHtmlString CreateMenu(this WebViewPage webPage)
    {
        var cr = CMS.GetContent(int.Parse(webPage.Request[Constants.kClientId]), webPage.Request[Constants.kLocale]);
        var showSubTab = false;
        if (!Convert.ToBoolean(CMS.GetCommonContent(cr, "showmenu", "config")) && string.IsNullOrEmpty(webPage.Request[Constants.kShowSubTabs]))
        {
            return MvcHtmlString.Create("");
        }
        if (!string.IsNullOrEmpty(webPage.Request[Constants.kShowSubTabs]))
        {
            showSubTab = Convert.ToBoolean(webPage.Request[Constants.kShowSubTabs]);
        }
        var tl = cr.Content.Tab.Where(x => x.tabtype == "main").OrderBy(x => x.menuorder).ToList();
        var sb = new StringBuilder();
        var tabs = new StringBuilder();
        var subTabs = new StringBuilder();
        var pid = int.Parse(webPage.Request["id"]);

        if (tl.Count > 0)
        {
            if (!showSubTab)
            {
                sb.Append("<nav class=\"navbar navbar-default acl-topnav\">");
                sb.Append("    <div tabindex=\"0\" class=\"navbar-header\">");
                sb.Append("        <button tabindex=\"0\" class=\"navbar-toggle\" type=\"button\" data-toggle=\"collapse\" data-target=\".navbar-ex1-collapse\">");
                sb.Append("            <span class=\"sr-only\">Toggle navigation</span>");
                sb.Append("            <span class=\"icon-bar\"></span>");
                sb.Append("            <span class=\"icon-bar\"></span>");
                sb.Append("            <span class=\"icon-bar\"></span>");
                sb.Append("        </button>");
                sb.Append("    </div>");
                sb.Append("<div tabindex=\"0\" class=\"collapse navbar-collapse navbar-ex1-collapse text-xs-center text-ms.pills.nav-center\" >\n");
                sb.Append("<div class=\"row\" id=\"acl_nav\">\n");
                sb.Append(" <ul class=\"nav navbar-nav acl-nav\" id=\"myItems_listbox\" role=\"menu\" >\n");
            }
            var selectedSubTab = "";
            if (webPage.Request["sid"] != null)
            {
                selectedSubTab = webPage.Request["sid"];
            }
            var parameters = new JsonParams
            {
                cl = webPage.Request[Constants.kClientId],
                c = webPage.Request[Constants.kCustomerId],
                a = webPage.Request[Constants.kAccountId],
                p = webPage.Request[Constants.kPremiseId],
                s = webPage.Request[Constants.kServicePointId],
                w = webPage.Request[Constants.kWebToken],
                l = webPage.Request[Constants.kLocale] ?? Constants.kLocaleEnglish,
                u = webPage.Request[Constants.kUserId],
                g = webPage.Request[Constants.kGroupName],
                r = webPage.Request[Constants.kRoleName],
                id = webPage.Request[Constants.kId],
                sid = webPage.Request[Constants.kSid]
            };
            var pm = new ProfileModels();
            var profAttr = CE.Dashboard.LayoutFramework.Layout.CombineAttributes(pm.GetProfileAttributesFromCache(parameters));
            foreach (var t in tl)
            {
                if (showSubTab)
                {
                    int.TryParse(t.url.Substring(1), out var tabUrlId);
                    if (tabUrlId != pid)
                    {
                        continue;
                    }
                }

                var firstSubTabId = "";
                if (int.TryParse(t.url.Substring(1), out var id))
                {
                    if (pid == id)
                    {
                        if (!string.IsNullOrEmpty(t.childtabs))
                        {
                            firstSubTabId = t.childtabs.Substring(0, t.childtabs.IndexOf(';'));
                            firstSubTabId = cr.Content.Tab.FirstOrDefault(x => x.key == firstSubTabId).url.Substring(1);

                            if (string.IsNullOrEmpty(selectedSubTab))
                            {
                                selectedSubTab = firstSubTabId;

                            }
                            foreach (var childTabKey in t.childtabs.Split(';'))
                            {
                                var st = cr.Content.Tab.FirstOrDefault(x => x.key == childTabKey);
                                var subTabId = st.url.Substring(1);

                                if (ProfileModels.ValidateConditions(GetConditionList(st, cr), profAttr))
                                {
                                    subTabs.Append(selectedSubTab == subTabId
                                        ? BuildSubTab(webPage, cr, id.ToString(), subTabId, childTabKey, true)
                                        : BuildSubTab(webPage, cr, id.ToString(), subTabId, childTabKey, false));
                                }
                            }
                        }
                        if (showSubTab) continue;
                        if (ProfileModels.ValidateConditions(GetConditionList(t, cr), profAttr))
                        {
                            if (ValidateConditions(id, Convert.ToInt32(parameters.cl), parameters.u, parameters.g, parameters.r, parameters.w))
                            {
                                tabs.Append(BuildTab(webPage, cr, id.ToString(), t.key, selectedSubTab, true));
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(t.childtabs))
                        {
                            firstSubTabId = t.childtabs.Substring(0, t.childtabs.IndexOf(';'));
                            firstSubTabId = cr.Content.Tab.FirstOrDefault(x => x.key == firstSubTabId).url.Substring(1);
                        }
                        if (ProfileModels.ValidateConditions(GetConditionList(t, cr), profAttr))
                        {
                            if (ValidateConditions(id, Convert.ToInt32(parameters.cl), parameters.u, parameters.g, parameters.r, parameters.w))
                            {
                                tabs.Append(BuildTab(webPage, cr, id.ToString(), t.key, firstSubTabId, false));
                            }
                        }
                    }
                }
            }

            sb.Append(tabs);

            if (!showSubTab)
            {
                sb.Append(" </ul>\n");
                sb.Append(" </div>");
                sb.Append(" </div>");
                sb.Append(" </nav>");
            }
            if (subTabs.Length <= 0) return MvcHtmlString.Create(sb.ToString());
            sb.Append("<div class=\"row\">");
            sb.Append("<div class=\"col-sm-12\">");
            sb.Append("<ul class=\"pills nav nav-pills\" role=\"menu\">");
            sb.Append(subTabs);
            sb.Append("</ul>");
            sb.Append("</div>");
            sb.Append("</div>");
        }
        return MvcHtmlString.Create(sb.ToString());
    }

    private static string BuildTab(WebViewPage webPage, RootContentObject cr, string tabId, string tabKey, string subTabId, bool active)
    {
        var tab = new StringBuilder();
        tab.Append(" <li id=\"");
        tab.Append("tab_");
        tab.Append(tabId);
        tab.Append("\" role=\"menuitem\"");

        if (active)
        {
            tab.Append(" class=\"active\" >");
            if (subTabId == string.Empty)
            {
                var eventAttribute = new Dictionary<string, string> { { "TabId", tabKey } };
                var etm = new EventTrackingModels();
                etm.PostEvents(eventAttribute, QueryStringModule.Encrypt(JsonConvert.SerializeObject(CreateParams(webPage))).Substring(5), "Visited Tab");
            }
        }
        else
        {
            tab.Append(" >");
        }
        var tabName = CMS.GetCommonContent(cr, tabKey + ".name");

        var link = "id=" + tabId + "&sid=" + subTabId + GeneralHelper.BuildQuerystring(webPage.Request);
        tab.Append("<a " + tabName + "\" href=\"");

        var enc = QueryStringModule.Encrypt(link);
        tab.Append("Page" + enc);
        tab.Append("\" class=\"navbar-link\">");
        tab.Append(tabName);
        tab.Append("</a></li>\n");
        return tab.ToString();
    }

    private static string BuildSubTab(WebViewPage webPage, RootContentObject cr, string tabId, string subTabId, string subTabKey, bool active)
    {
        var tab = new StringBuilder();
        tab.Append(" <li id=\"");
        tab.Append("tab_");
        tab.Append(subTabId);
        tab.Append("\" role=\"menuitem\"");

        if (active)
        {
            tab.Append(" class=\"active\" >");

            var eventAttribute = new Dictionary<string, string> { { "TabId", subTabKey } };
            var etm = new EventTrackingModels();
            etm.PostEvents(eventAttribute, QueryStringModule.Encrypt(JsonConvert.SerializeObject(CreateParams(webPage))).Substring(5), "Visited Tab");
        }
        else
        {
            tab.Append(" >");
        }

        var tabName = CMS.GetCommonContent(cr, subTabKey + ".name");
        tab.Append("<a " + tabName + "\" href=\"");
        var enc = QueryStringModule.Encrypt("id=" + tabId + "&sid=" + subTabId + GeneralHelper.BuildQuerystring(webPage.Request));
        tab.Append("Page" + enc);
        tab.Append("\" class=\"navbar-link\">");
        tab.Append(tabName);
        tab.Append("</a></li>\n");
        return tab.ToString();
    }

    public static void SetupMVCError(ExceptionContext filterContext)
    {
        var customErrorSection = (CustomErrorsSection)ConfigurationManager.GetSection("system.web/customErrors");
        //Verify that customErrors exists in web.config && customErrors is disabled 
        // On Dev we need to suppress the custom Error page and show the .NET YOD error page
        if (customErrorSection != null && customErrorSection.Mode == CustomErrorsMode.Off)
            return;

        if (filterContext.Exception != null && Enum.IsDefined(typeof(Enums.ErrorClassification), filterContext.Exception.Message))
        {
            var type = (Enums.ErrorClassification)Enum.Parse(typeof(Enums.ErrorClassification), filterContext.Exception.Message, true);
            if (type == Enums.ErrorClassification.Ignore)
                return;
        }
        var errorModel = new ErrorModel();
        errorModel.Message = errorModel.GetErrorContent(filterContext.Exception.Message);
        errorModel.MessageDetails = filterContext.Exception.ToString();
        var controllerName = (string)filterContext.RouteData.Values["controller"];
        var actionName = (string)filterContext.RouteData.Values["action"];
        var model = new HandleErrorInfo(filterContext.Exception, controllerName, actionName);
        var result = new ViewResult { ViewName = "Error" };
        result.ViewBag.Title = errorModel.Title;

        result.ViewData = new ViewDataDictionary<HandleErrorInfo>(model)
        {
            ["ErrorMessage"] = errorModel.Message,
            ["ErrorMessageDetails"] = string.Empty
        };
        //Ensure no exception details are displayed on web pages.
        result.TempData = filterContext.Controller.TempData;
        filterContext.Result = result;
        filterContext.ExceptionHandled = true;
        filterContext.HttpContext.Response.Clear();
        filterContext.HttpContext.Response.StatusCode = 500;
        filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
    }

    private static string CreateResourceSets(string m, WebViewPage webPage, bool isDebugEnabled)
    {
        var sb = new StringBuilder();
        if (m.Length <= 0) return sb.ToString();

        var w = m.Split(',');
        foreach (var i in w)
        {
            sb.Append("<link href='" + Combres.WebExtensions.CombresUrl(i + "Css") + "' rel='stylesheet' type='text/css'/>");
            switch (i)
            {
                case "greenbutton":
                    break;
                case "longprofile":
                    break;
                case "profile":
                    break;
                case "subscription":
                    break;
                case "reportlist":
                    break;
                case "reportcreate":
                    break;
                case "peercomparison":
                    break;
                case "quicklinks":
                    break;
                case "promo":
                    break;
                case "billsummary":
                    break;
                case "billhistory":
                    break;
                case "threshold":
                    break;
                case "billdisagg":
                    break;
                case "thirdpartyselection":
                    break;
                case "consumption":
                    break;
                case "billedusagechart":
                    break;
                case "billtodate":
                    break;
                default:
                    sb.Append("<script src='" + Combres.WebExtensions.CombresUrl(i + "Js") + "' async></script>");
                    break;
            }

        }

        if (!ValidateCsr(webPage.Request[Constants.kUserId], webPage.Request[Constants.kGroupName], webPage.Request[Constants.kRoleName], webPage.Request[Constants.kWebToken]))
        {
            if (!ShowPremiseSelect(webPage)) return sb.ToString();
            sb.Append("<link href='" + Combres.WebExtensions.CombresUrl("premiseselectCss") + "' rel='stylesheet' type='text/css'/>");
        }
        else
        {
            sb.Append("<link href='" + Combres.WebExtensions.CombresUrl("customerinfoCss") + "' rel='stylesheet' type='text/css'/>");
        }
        return sb.ToString();
    }

    private static List<Condition> GetConditionList(Tab tab, RootContentObject cr)
    {
        var conditionListSubTab = new List<Condition>();
        if (tab.conditions.Length <= 0) return conditionListSubTab;
        var s = tab.conditions.Split(';');
        conditionListSubTab.AddRange(s.Select(i => cr.Content.Condition.FirstOrDefault(x => x.key == i)));
        return conditionListSubTab;
    }

    private static JsonParams CreateParams(WebViewPage webPage)
    {
        var parameters = new JsonParams
        {
            cl = webPage.Request[Constants.kClientId],
            c = webPage.Request[Constants.kCustomerId],
            a = webPage.Request[Constants.kAccountId],
            p = webPage.Request[Constants.kPremiseId],
            s = webPage.Request[Constants.kServicePointId],
            w = webPage.Request[Constants.kWebToken],
            l = webPage.Request[Constants.kLocale],
            u = webPage.Request[Constants.kUserId],
            g = webPage.Request[Constants.kGroupName],
            r = webPage.Request[Constants.kRoleName],
            id = webPage.Request[Constants.kId],
            sid = webPage.Request[Constants.kSid]
        };
        parameters.l = webPage.Request[Constants.kLocale] ?? Constants.kLocaleEnglish;
        return parameters;
    }

    public static bool ValidateConditions(int tabId, int clientId, string userId, string groupName, string roleName, string webToken)
    {
        if (HttpRuntime.Cache["ValidCSRCondition" + tabId + "" + clientId + "" + userId + "" + groupName + "" + roleName] == null)
        {
            //check for tab conditions in ClientTabConditions
            var transactionOptions = new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            ICollection clientTabConditions;
            if (HttpRuntime.Cache["ValidCSRConditionTabs" + "" + clientId] == null)
            {
                using (var transScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
                {
                    using (var ent = new InsightsEntities())
                    {
                        ent.Database.Connection.Open();

                        var cTabConditions = (from ctc in ent.ClientTabConditions
                                                   join g in ent.Groups on ctc.GroupID equals g.GroupID
                                                   join r in ent.Roles on ctc.RoleID equals r.RoleID
                                                   where ctc.ClientID == clientId
                                                   select new { ctc.GroupID, ctc.RoleID, g.GroupName, r.RoleName, ctc.TabID }).ToList();

                        HttpRuntime.Cache.Insert("ValidCSRConditionTabs" + "" + clientId, cTabConditions, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
                        clientTabConditions = cTabConditions;
                    }
                    transScope.Complete();
                }
            }
            else
            {
                 clientTabConditions = (ICollection)HttpRuntime.Cache["ValidCSRConditionTabs" + "" + clientId];
            }

            var grpExists = false;
            var clientTabExists = false;
            foreach (var prop in clientTabConditions)
            {
                var tId = prop.GetPropertyValue("TabID");
                if ((string)tId != tabId.ToString()) continue;
                clientTabExists = true;

                if (string.IsNullOrEmpty(groupName) || string.IsNullOrEmpty(roleName)) continue;
                var grpName = prop.GetPropertyValue("GroupName").ToString();
                var rName = prop.GetPropertyValue("RoleName").ToString();
                if (grpName != groupName || rName != roleName) continue;
                grpExists = true;
                break;
            }

            //condition doesn't exist for the tab and user is not CSR
            if (clientTabExists == false && string.IsNullOrEmpty(userId))
            {
                return true;
            }
            if (clientTabExists && string.IsNullOrEmpty(userId)) //condition exists for the tab and user is not CSR
            {
                return false;
            }
            if (clientTabExists && userId.Length > 0)//condition exists for the tab and user is CSR
            {
                //validate group
                if (grpExists)
                {
                    HttpRuntime.Cache.Insert("ValidCSRCondition" + tabId + "" + clientId + "" + userId + "" + groupName + "" + roleName, true, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
                    return true;
                }
                HttpRuntime.Cache.Insert("ValidCSRCondition" + tabId + "" + clientId + "" + userId + "" + groupName + "" + roleName, false, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
                return false;
            }
        }
        else
        {
            return (bool)HttpRuntime.Cache["ValidCSRCondition" + tabId + "" + clientId + "" + userId + "" + groupName + "" + roleName];
        }
        HttpRuntime.Cache.Insert("ValidCSRCondition" + tabId + "" + clientId + "" + userId + "" + groupName + "" + roleName, true, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
        return true;
    }

    public static bool ValidateCsr(string userId, string groupName, string roleName, string webToken)
    {
        if (HttpRuntime.Cache["ValidCSR" + userId + "" + groupName + "" + roleName] == null)
        {
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(groupName) || string.IsNullOrEmpty(roleName))
            {
                HttpRuntime.Cache.Insert("ValidCSR" + userId + "" + groupName + "" + roleName, false, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
                return false;
            }

            var transactionOptions = new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (var transScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();

                    var group = (from g in ent.Groups
                                 where g.GroupName == groupName
                                 select new { g.GroupID }).ToList();

                    var role = (from r in ent.Roles
                                where r.RoleName == roleName
                                select new { r.RoleID }).ToList();

                    if (group.Count == 0 || role.Count == 0 || string.IsNullOrEmpty(userId))
                    {
                        HttpRuntime.Cache.Insert("ValidCSR" + userId + "" + groupName + "" + roleName, false, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
                        return false;
                    }
                }
                transScope.Complete();
            }
        }
        else
        {
            return (bool)HttpRuntime.Cache["ValidCSR" + userId + "" + groupName + "" + roleName];
        }
        HttpRuntime.Cache.Insert("ValidCSR" + userId + "" + groupName + "" + roleName, true, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
        return true;
    }

    public static DateTime CovertToClientTimeZone(DateTime timeStamp, string timeZoneMapping, JsonParams jp) => TimeZoneInfo.ConvertTimeFromUtc(timeStamp, TimeZoneInfo.FindSystemTimeZoneById(GetTimeZone(jp, timeZoneMapping)));

    public static string GetTimeZone(JsonParams jp, string timeZoneMapping)
    {
        var customerInfo = CustomerInfoModels.GetCustomerInfoFromCache(jp);
        if (customerInfo?.Customer?.PostalCode == null) { return ""; }
        var pc = timeZoneMapping.Split(';').ToList().FirstOrDefault(x => x.Contains(customerInfo.Customer.PostalCode));
        return pc?.Split(',')[1];
    }

    public static string EncryptParams(JsonParams p)
    {
        var qs = Constants.kClientId + "=" + p.cl
                 + "&" + Constants.kCustomerId + "=" + p.c
                 + "&" + Constants.kAccountId + "=" + p.a
                 + "&" + Constants.kPremiseId + "=" + p.p
                 + "&" + Constants.kServicePointId + "=" + p.s
                 + "&" + Constants.kWebToken + "=" + p.w
                 + "&" + Constants.kLocale + "=" + p.l
                 + "&" + Constants.kUserId + "=" + p.u
                 + "&" + Constants.kGroupName + "=" + p.g
                 + "&" + Constants.kRoleName + "=" + p.r;

        return QueryStringModule.Encrypt(qs).Substring(5);
    }
}

public static class EnumExtensions
{
    public static string GetDescriptionOfEnum(this Enum value)
    {
        var fieldInfo = value.GetType().GetField(value.ToString());

        var descriptionAttribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DescriptionAttribute)) as DescriptionAttribute;

        return descriptionAttribute == null ? value.ToString() : descriptionAttribute.Description;
    }
}