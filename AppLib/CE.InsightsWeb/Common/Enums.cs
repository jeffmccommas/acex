﻿using System.ComponentModel;

namespace CE.InsightsWeb.Common
{
    public class Enums
    {
        public enum PartialView
        {
            Header = 1,
            HeaderRemote,
            Footer,
            FooterRemote,
            SideNavBegin,
            SideNavEnd,
            SideNav,
            SideNavRemote,
            Frame
        }

        public enum ErrorClassification
        {
            General = 1,
            Http403,
            Http404,
            SessionExpired,
            ClientScript,
            CookieDisabled,
            Ignore,
            LogAndIgnore,
            Unknown
        }

        public enum ImageSize
        {
            Small = 0,
            Medium = 1,
            Large = 2
        }

        public enum DisaggStatusType
        {
            Unspecified,
            ActualBills,
            ComputedBills,
            ModelOnly,
            Failed
        }

        public enum BillComparisonType
        {
            SameMonthLastYear,
            PreviousBill,
            MostExpensiveToLeastExpensive,
            PeakSummerToPeakWinter
        }
        public enum CommodityType
        {
            Unspecified,
            Electric,
            Gas,
            Water,
            Oil,
            Propane,
            Wood,
            Coal,
            HotWater,
            OtherWater,
            ThermoFlow
        }

        public enum EnvironmentType
        {
            Internal,
            External
        }

        public enum Timezone
        {
            [Description("Atlantic Standard Time")]
            ADT,
            [Description("Atlantic Standard Time")]
            AST,
            [Description("Alaskan Standard Time")]
            AKDT,
            [Description("Alaskan Standard Time")]
            AKST,
            [Description("Central Standard Time")]
            CDT,
            [Description("Central Standard Time")]
            CST,
            [Description("Eastern Standard Time")]
            EDT,
            [Description("Eastern Standard Time")]
            EST,
            [Description("Greenwich Standard Time")]
            GMT,
            [Description("Hawaiian Standard Time")]
            HADT,
            [Description("Hawaiian Standard Time")]
            HAST,
            [Description("Mountain Standard Time")]
            MDT,
            [Description("Mountain Standard Time")]
            MST,
            [Description("Pacific Standard Time")]
            PDT,
            [Description("Pacific Standard Time")]
            PST,
            [Description("UTC")]
            UTC
        }

    }
}
