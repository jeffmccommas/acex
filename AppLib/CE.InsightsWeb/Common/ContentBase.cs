﻿using System.Collections.Generic;
using CE.InsightsWeb.Helpers;

namespace CE.InsightsWeb.Common
{
    /// <summary>
    /// Base class for all the common contents
    /// </summary>
    public class ContentBase
    {
        public string Title { get; set; }
        public string IntroText { get; set; }
        public string SubTitle { get; set; }
        public string ShowTitle { get; set; }
        public string ShowIntro { get; set; }
        public string ShowSubTitle { get; set; }
        public string Footer { get; set; }
        public string FooterLink { get; set; }
        public string FooterLinkText { get; set; }
        public string FooterLinkType { get; set; }
        public string ShowFooterText { get; set; }
        public string ShowFooterLink { get; set; }

        protected ContentBase()
        {
        }

        protected ContentBase(List<Configuration> config, List<TextContent> content, string tabKey, string widetType, JsonParams jp)
        {
            Title = CMS.GetWidgetContent(content, tabKey, widetType, "title");
            IntroText = CMS.GetWidgetContent(content, tabKey, widetType, "introtext");
            SubTitle = CMS.GetWidgetContent(content, tabKey, widetType, "subtitle");
            Footer = CMS.GetWidgetContent(content, tabKey, widetType, "footer");
            FooterLinkText = CMS.GetWidgetContent(content, tabKey, widetType, "footerlinktext");
            FooterLink = GeneralHelper.CreateLink(CMS.GetWidgetConfig(config, tabKey, widetType, "footerlinktype"), jp, CMS.GetWidgetConfig(config, tabKey, widetType, "footerlink"));

            ShowTitle = CMS.GetWidgetConfig(config, tabKey, widetType, "showtitle");
            ShowIntro = CMS.GetWidgetConfig(config, tabKey, widetType, "showintro");
            ShowSubTitle = CMS.GetWidgetConfig(config, tabKey, widetType, "showsubtitle");
            ShowFooterText = CMS.GetWidgetConfig(config, tabKey, widetType, "showfootertext");
            ShowFooterLink = CMS.GetWidgetConfig(config, tabKey, widetType, "showfooterlink");
        }

        protected ContentBase(RootContentObject content, string widetType, JsonParams jp)
        {
            Title = CMS.GetCommonContent(content, widetType + ".title");
            IntroText = CMS.GetCommonContent(content, widetType + ".introtext");
            SubTitle = CMS.GetCommonContent(content, widetType + ".subtitle");
            Footer = CMS.GetCommonContent(content, widetType + ".footer");
            FooterLinkText = CMS.GetCommonContent(content, widetType + ".footerlinktext");
            FooterLink = GeneralHelper.CreateLink(CMS.GetCommonContent(content, widetType + ".footerlinktype", "config"), jp, CMS.GetCommonContent(content, widetType + ".footerlink", "config"));

            ShowTitle = CMS.GetCommonContent(content, widetType + ".showtitle", "config");
            ShowIntro = CMS.GetCommonContent(content, widetType + ".showintro", "config");
            ShowSubTitle = CMS.GetCommonContent(content, widetType + ".showsubtitle", "config");
            ShowFooterText = CMS.GetCommonContent(content, widetType + ".showfootertext", "config");
            ShowFooterLink = CMS.GetCommonContent(content, widetType + ".showfooterlink", "config");
        }
    }
}