﻿using System;

namespace CE.InsightsWeb.Common
{  
    [Serializable()]
    public class JsonParams
    {
        public string cl;
        public string c;
        public string a;
        public string p;
        public string pt;
        public string s;
        public string w;
        public string l;
        public string u;
        public string g;
        public string r;
        public string id;
        public string sid;
        public string st;
    }     
}
