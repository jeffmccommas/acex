﻿aclaraJQuery(document).ready(function () {
    try {
        aclaraJQuery('#btnLogin').click(function () {
            GetToken();
        });

        aclaraJQuery('input[type=text], input[type=checkbox]').on('keypress', function (event) {
            if (event.keyCode == '13') {
                aclaraJQuery('#btnLogin').click();
            }
        });

    }
    catch (ex) { }
});

function GetToken() {
    aclaraJQuery('#btnLogin').html('Please Wait...');
    aclaraJQuery('#btnLogin').off("click");
    aclaraJQuery('#lblError').hide();
    var csr;

    //all good
    csr = "&UserId=CSR13&GroupName=CSR&RoleName=CSRAdmin";

    //missing group and role values
    //csr = "&UserId=CSR13&GroupName=&RoleName=";

    //missing role value
    //csr = "&UserId=CSR13&GroupName=CSR&RoleName=";

    //missing group value
    //csr = "&UserId=CSR13&GroupName=&RoleName=CSRAdmin";

    //missing user id value
    //csr = "&UserId=&GroupName=CSR&RoleName=CSRAdmin";

    //missing all values
    //csr = "&UserId=&GroupName=&RoleName=";

    //missing role
    //csr = "&UserId=&GroupName=";

    //missing group
    //csr = "&UserId=CSR13&RoleName=CSRAdmin";

    //missing userid
    //csr = "&GroupName=CSR&RoleName=CSRAdmin";

    if (aclaraJQuery("#txtCustomer").val() != "") {
        aclaraJQuery.ajax({
            url: ResolveURL("~/Page/PostToken?customerId=" + aclaraJQuery("#txtCustomer").val() + "&clientId=" + aclaraJQuery("#txtClient").val() + "&accountId=" + aclaraJQuery("#txtAccount").val() + "&premiseId=" + aclaraJQuery("#txtPremise").val() + "&username=" + aclaraJQuery("#txtUserName").val() + "&password=" + aclaraJQuery("#txtPassword").val() + "&clearProfileAttr=" + aclaraJQuery('#chkbClearProfileAttr').is(':checked') + "&csrLogin=" + aclaraJQuery('#chkbEnableCSR').is(':checked') + csr + "&enableRemote=" + aclaraJQuery('#chkEnableRemote').is(':checked')),
            async: false,
            cache: false,
            type: "GET",
            data: "",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                data = JSON.parse(msg.Content);
                if (aclaraJQuery('#chkbEnableWidgets').is(':checked')) {
                    aclaraJQuery('[id^=iws_height_wrapper_]').hide();
                    if (msg.StatusCode == 200) {
                        var base = ResolveURL("~/Widget?id=tab.myusage-billdisagg&CustomerId=" + aclaraJQuery('#txtCustomer').val() + "&AccountId=" + aclaraJQuery('#txtAccount').val() + "&PremiseId=" + aclaraJQuery('#txtPremise').val() + "&ClientId=" + aclaraJQuery('#txtClient').val() + "&Locale=" + aclaraJQuery("#ddlLocale").val() + "&WebToken=" + data.WebToken + "&ServicePointId=" + aclaraJQuery("#txtService").val() + "&enableRemote=" + aclaraJQuery('#chkEnableRemote').is(':checked'));
                        var base1 = ResolveURL("~/Widget?id=tab.myusage-billedusagechart&CustomerId=" + aclaraJQuery('#txtCustomer').val() + "&AccountId=" + aclaraJQuery('#txtAccount').val() + "&PremiseId=" + aclaraJQuery('#txtPremise').val() + "&ClientId=" + aclaraJQuery('#txtClient').val() + "&Locale=" + aclaraJQuery("#ddlLocale").val() + "&WebToken=" + data.WebToken + "&ServicePointId=" + aclaraJQuery("#txtService").val() + "&enableRemote=" + aclaraJQuery('#chkEnableRemote').is(':checked'));
                        var base2 = ResolveURL("~/Widget?id=tab.dashboard-insights&CustomerId=" + aclaraJQuery('#txtCustomer').val() + "&AccountId=" + aclaraJQuery('#txtAccount').val() + "&PremiseId=" + aclaraJQuery('#txtPremise').val() + "&ClientId=" + aclaraJQuery('#txtClient').val() + "&Locale=" + aclaraJQuery("#ddlLocale").val() + "&WebToken=" + data.WebToken + "&ServicePointId=" + aclaraJQuery("#txtService").val() + "&enableRemote=" + aclaraJQuery('#chkEnableRemote').is(':checked'));
                        var base3 = ResolveURL("~/Widget?id=tab.dashboard-setgoal&CustomerId=" + aclaraJQuery('#txtCustomer').val() + "&AccountId=" + aclaraJQuery('#txtAccount').val() + "&PremiseId=" + aclaraJQuery('#txtPremise').val() + "&ClientId=" + aclaraJQuery('#txtClient').val() + "&Locale=" + aclaraJQuery("#ddlLocale").val() + "&WebToken=" + data.WebToken + "&ServicePointId=" + aclaraJQuery("#txtService").val() + "&enableRemote=" + aclaraJQuery('#chkEnableRemote').is(':checked'));
                        var base4 = ResolveURL("~/Widget?id=tab.dashboard-billcomparison&CustomerId=" + aclaraJQuery('#txtCustomer').val() + "&AccountId=" + aclaraJQuery('#txtAccount').val() + "&PremiseId=" + aclaraJQuery('#txtPremise').val() + "&ClientId=" + aclaraJQuery('#txtClient').val() + "&Locale=" + aclaraJQuery("#ddlLocale").val() + "&WebToken=" + data.WebToken + "&ServicePointId=" + aclaraJQuery("#txtService").val() + "&enableRemote=" + aclaraJQuery('#chkEnableRemote').is(':checked'));
                        var base5 = ResolveURL("~/Widget?id=tab.dashboard-actions&CustomerId=" + aclaraJQuery('#txtCustomer').val() + "&AccountId=" + aclaraJQuery('#txtAccount').val() + "&PremiseId=" + aclaraJQuery('#txtPremise').val() + "&ClientId=" + aclaraJQuery('#txtClient').val() + "&Locale=" + aclaraJQuery("#ddlLocale").val() + "&WebToken=" + data.WebToken + "&ServicePointId=" + aclaraJQuery("#txtService").val() + "&enableRemote=" + aclaraJQuery('#chkEnableRemote').is(':checked'));
                        var base6 = ResolveURL("~/Widget?id=tab.myplan-actionplan&CustomerId=" + aclaraJQuery('#txtCustomer').val() + "&AccountId=" + aclaraJQuery('#txtAccount').val() + "&PremiseId=" + aclaraJQuery('#txtPremise').val() + "&ClientId=" + aclaraJQuery('#txtClient').val() + "&Locale=" + aclaraJQuery("#ddlLocale").val() + "&WebToken=" + data.WebToken + "&ServicePointId=" + aclaraJQuery("#txtService").val() + "&enableRemote=" + aclaraJQuery('#chkEnableRemote').is(':checked'));
                        var base7 = ResolveURL("~/Widget?id=tab.myusage-greenbutton&CustomerId=" + aclaraJQuery('#txtCustomer').val() + "&AccountId=" + aclaraJQuery('#txtAccount').val() + "&PremiseId=" + aclaraJQuery('#txtPremise').val() + "&ClientId=" + aclaraJQuery('#txtClient').val() + "&Locale=" + aclaraJQuery("#ddlLocale").val() + "&WebToken=" + data.WebToken + "&ServicePointId=" + aclaraJQuery("#txtService").val() + "&enableRemote=" + aclaraJQuery('#chkEnableRemote').is(':checked'));
                        var base8 = ResolveURL("~/Widget?id=tab.dashboard-profile&CustomerId=" + aclaraJQuery('#txtCustomer').val() + "&AccountId=" + aclaraJQuery('#txtAccount').val() + "&PremiseId=" + aclaraJQuery('#txtPremise').val() + "&ClientId=" + aclaraJQuery('#txtClient').val() + "&Locale=" + aclaraJQuery("#ddlLocale").val() + "&WebToken=" + data.WebToken + "&ServicePointId=" + aclaraJQuery("#txtService").val() + "&enableRemote=" + aclaraJQuery('#chkEnableRemote').is(':checked'));
                        var base9 = ResolveURL("~/Widget?id=tab.energyprofile-longprofile&CustomerId=" + aclaraJQuery('#txtCustomer').val() + "&AccountId=" + aclaraJQuery('#txtAccount').val() + "&PremiseId=" + aclaraJQuery('#txtPremise').val() + "&ClientId=" + aclaraJQuery('#txtClient').val() + "&Locale=" + aclaraJQuery("#ddlLocale").val() + "&WebToken=" + data.WebToken + "&ServicePointId=" + aclaraJQuery("#txtService").val() + "&enableRemote=" + aclaraJQuery('#chkEnableRemote').is(':checked'));
                        var base10 = ResolveURL("~/Widget?id=tab.myusage-consumption&CustomerId=" + aclaraJQuery('#txtCustomer').val() + "&AccountId=" + aclaraJQuery('#txtAccount').val() + "&PremiseId=" + aclaraJQuery('#txtPremise').val() + "&ClientId=" + aclaraJQuery('#txtClient').val() + "&Locale=" + aclaraJQuery("#ddlLocale").val() + "&WebToken=" + data.WebToken + "&ServicePointId=" + aclaraJQuery("#txtService").val() + "&enableRemote=" + aclaraJQuery('#chkEnableRemote').is(':checked'));
                   

                        aclaraJQuery('#iws_height_wrapper_billdisagg').attr('src', base).show();
                        aclaraJQuery('#iws_height_wrapper_insights').attr('src', base2).show();
                        aclaraJQuery('#iws_height_wrapper_setgoal').attr('src', base3).show();
                        aclaraJQuery('#iws_height_wrapper_billcomparison').attr('src', base4).show();
                        aclaraJQuery('#iws_height_wrapper_actions').attr('src', base5).show();
                        aclaraJQuery('#iws_height_wrapper_actionplan').attr('src', base6).show();
                        aclaraJQuery('#iws_height_wrapper_greenbutton').attr('src', base7).show();
                        aclaraJQuery('#iws_height_wrapper_profile').attr('src', base8).show();
                        aclaraJQuery('#iws_height_wrapper_longprofile').attr('src', base9).show();                      
                        aclaraJQuery('#iws_height_wrapper_consumption').attr('src', base10).show();  

                        aclaraJQuery('#btnLogin').html('Log In');

                    } else {
                        aclaraJQuery('#btnLogin').html('Log In');
                        if (msg.StatusDescription == null) {
                            aclaraJQuery('#lblError').html('Internal Error');
                        } else {
                            aclaraJQuery('#lblError').html(msg.StatusDescription);
                        }
                        aclaraJQuery('#lblError').show();
                    }
                    aclaraJQuery('#btnLogin').on("click", GetToken);
                } else {
                    aclaraJQuery('#iws_height_wrapper_billdisagg').hide();
                    aclaraJQuery('#iws_height_wrapper_billedusagechart').hide();
                    var tmpl;
                    if (aclaraJQuery('#chkEnableRemote').is(':checked')) {
                        tmpl = "rc";
                    } else {
                        tmpl = "";
                    }

                    var hideMenu;
                    if (aclaraJQuery('#chkHideInternalMenu').is(':checked')) {
                        hideMenu = "true";
                    } else {
                        hideMenu = "false";
                    }

                    if (msg.StatusCode == 200) {
                        var url;
                        if (aclaraJQuery('#chkGreenButtonConnectAdmin').is(':checked')) {
                            url = ResolveURL("~/GreenButtonConnectAdmin");
                        }
                        else if (aclaraJQuery('#chkGreenButtonConnect').is(':checked')) {
                            url = ResolveURL("~/Page?id=8&CustomerId=" + aclaraJQuery('#txtCustomer').val() + "&AccountId=" + aclaraJQuery('#txtAccount').val() + "&PremiseId=" + aclaraJQuery('#txtPremise').val() + "&ClientId=" + aclaraJQuery('#txtClient').val() + "&Locale=" + aclaraJQuery("#ddlLocale").val() + "&WebToken=" + data.WebToken + "&ServicePointId=" + aclaraJQuery("#txtService").val() + "&ShowSubTabs=" + aclaraJQuery('#chkShowSubTabs').is(':checked') + "&tmpl=gbc");
                        }
                        else if (aclaraJQuery('#chkbEnableCSR').is(':checked')) {
                            url = ResolveURL("~/Page?id=1000&CustomerId=" + aclaraJQuery('#txtCustomer').val() + "&AccountId=" + aclaraJQuery('#txtAccount').val() + "&PremiseId=" + aclaraJQuery('#txtPremise').val() + "&ClientId=" + aclaraJQuery('#txtClient').val() + "&Locale=" + aclaraJQuery("#ddlLocale").val() + "&WebToken=" + data.WebToken + "&ServicePointId=" + aclaraJQuery("#txtService").val() + csr + "&tmpl=" + tmpl + "&hidemenu=" + hideMenu + "&ShowSubTabs=" + aclaraJQuery('#chkShowSubTabs').is(':checked'));
                        }
                        else {
                            url = ResolveURL("~/Page?id=1&CustomerId=" + aclaraJQuery('#txtCustomer').val() + "&AccountId=" + aclaraJQuery('#txtAccount').val() + "&PremiseId=" + aclaraJQuery('#txtPremise').val() + "&ClientId=" + aclaraJQuery('#txtClient').val() + "&Locale=" + aclaraJQuery("#ddlLocale").val() + "&WebToken=" + data.WebToken + "&ServicePointId=" + aclaraJQuery("#txtService").val() + "&tmpl=" + tmpl + "&hidemenu=" + hideMenu + "&ShowSubTabs=" + aclaraJQuery('#chkShowSubTabs').is(':checked'));
                        }
                        window.location = url;
                    } else {
                        aclaraJQuery('#btnLogin').html('Log In');
                        if (msg.StatusDescription == null) {
                            aclaraJQuery('#lblError').html('Internal Error');
                        } else {
                            aclaraJQuery('#lblError').html(msg.StatusDescription);
                        }
                        aclaraJQuery('#lblError').show();
                    }
                    aclaraJQuery('#btnLogin').on("click", GetToken);
                }
            }
        }).fail(function (jqXHR, textStatus) {
            window.location = relativeRoot + "Error/Error";
        });
    }
}



