﻿/// <reference path="Jquery/jquery-2.1.0-vsdoc.js" />
(function ($) {
    $.fn.accesibleloader = function (options) {
        // Set the options
        options = $.extend({}, $.fn.accesibleloader.defaults, options);
        $('#loading-placeholder-' + this.attr('id')).remove();
        loadImage(this.attr('id'), options.imagePath, options.loadingMessage, options.loadingDivClass);
    };

    // Public defaults.
    $.fn.accesibleloader.defaults = {
        imagePath: "Content/Images/loading.gif",
        loadingMessage: "Loading...",
        loadingDivClass: "acl-loader"
    };

    function loadImage(controlID, imagePath, loadingMessage, loadingDivClass) {
        $('#' + controlID).show();
        $('#' + controlID).append('<div id="loading-placeholder-' + controlID + '" class=' + loadingDivClass + ' tabindex=0><span class="acl-access">' + loadingMessage + '</span><p>' + loadingMessage + '</p><img alt="loading" src="' + imagePath + '" /></div>');
        $('#loading-placeholder').focus();
    }

    $.fn.accesibleloader.unloadImage = function (contentDiv, controlID) {
        var placeholder = $('#loading-placeholder-' + controlID);
        var focusWasHere = document.activeElement === placeholder[0];
        placeholder.parent().hide();
        $('#' + contentDiv).show();
        var src = $('#' + contentDiv);
        //Only move focus if it was on the loading placeholder to begin with.
        if (focusWasHere) {
            $(':tabbable:first', src).focus();
        }
    }

})(jQuery);



