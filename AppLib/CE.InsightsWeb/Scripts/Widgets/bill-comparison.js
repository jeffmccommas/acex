﻿aclaraJQuery(document).ready(function () {
    //iws_dl_comp = "previousbill";
    billComparisonFunctions.InitializeWidget();
});

var billComparisonFunctions = new function () {

    var iws_bc;
    var fromDate = null;
    var toDate = null;
    var iws_comp_selected = "";
    var iws_dl_highlight = false;
    var customLabelText = "";

    this.InitializeWidget = function () {

        billComparisonFunctions.DeepLink();

        aclaraJQuery("#iws_bc_wrapper").hide();

        aclaraJQuery("#iws_biws_bc_comparec_loader").accesibleloader({
            imagePath: ResolveURL("~/content/images/loading.gif"),
            loadingMessage: iws_loading_text
        });

        billComparisonFunctions.GetData();

        //use template attributes to populate titles
        //iws_bc = null;
        //iws_bc.NoBills = true;
        //iws_bc.NoBillsText = "Sorry no bills available....";

        //iws_bc.OneBill = true;
        //iws_bc.OneBillText = "Sorry only one bill available....";

        if (iws_bc === null || iws_bc === undefined) {
            billComparisonFunctions.ShowError();
        }
        else if (iws_bc.NoBills === true) {
            billComparisonFunctions.HideContentShowMessage(iws_bc.NoBillsText);
        }
        else if (iws_bc.OneBill === true) {
            billComparisonFunctions.HideContentShowMessage(iws_bc.OneBillText);
        } else {

            template = aclaraJQuery.templates("#tmpl_bc_default");
            aclaraJQuery("#iws_bc_main").empty().append(template.render(iws_bc));

            //iws_bc.ShowTitle = "true";
            if (iws_bc.ShowTitle !== "true") {
                aclaraJQuery("#iws_bc_title").remove();
            }

            //iws_bc.ShowSubtitle = "true";
            if (iws_bc.ShowSubtitle !== "true") {
                aclaraJQuery("#iws_bc_subtitle").remove();
            }

            if (iws_bc.ShowIntro !== "true") {
                aclaraJQuery("#iws_bc_intro_text").remove();
            } else {
                var converter = new showdown.Converter();
                iws_bc.IntroText = converter.makeHtml(iws_bc.IntroText);
            }

            if (iws_bc.ShowFooterText === "false") {
                aclaraJQuery("#iws_bc_footertext").remove();
            }

            if (iws_bc.ShowFooterLink === "false") {
                aclaraJQuery("#iws_bc_footerlink").remove();
            } else {
                if (iws_bc.FooterLink.toLowerCase().indexOf("type=pm") > 0) {
                    aclaraJQuery("#iws_bc_footerlink a").addClass("iframe-external-url").attr("href", iws_bc.FooterLink.split("?type=pm")[0]);
                }
            }

            var comparisonVals = iws_bc.CompareDropDownKeys.split(",");
            var comparisonTexts = iws_bc.CompareDropDownValues.split(",");

            var comparisons = "";

            if (iws_comp_selected === "") {
                iws_comp_selected = comparisonVals[0];
            }



            if (!iws_bc.isIos) {
                for (i = 0; i < comparisonVals.length; i++) {
                    comparisons += "<li><a id=\"iws_bc_ddl_li_comparison-" +
                        comparisonVals[i] +
                        "\"" +
                        " href=\"#\" onclick=\"return false;\">" +
                        comparisonTexts[i] +
                        "</a></li>";
                    if (comparisonVals[i] === iws_comp_selected) {
                        aclaraJQuery("#iws_bc_a_comparison_selection").html(comparisonTexts[i]);
                    }
                }
                aclaraJQuery("#iws_bc_ddl_ul_compare").prepend(comparisons);
                aclaraJQuery("#iws_bc_ddl_ul_compare").focus();


                aclaraJQuery("[id^=iws_bc_ddl_li_comparison-]")
                    .off()
                    .click(function () {
                        billComparisonFunctions.Change(aclaraJQuery(this).attr("id").split("-")[1]);
                        aclaraJQuery("#iws_bc_a_comparison_selection").html(aclaraJQuery(this).text());
                    });

            }

            if (iws_bc.isIos) {
                for (i = 0; i < comparisonVals.length; i++) {
                    comparisons += "<option value=\"" +
                        comparisonVals[i] +
                        "\"" +
                        ">" +
                        comparisonTexts[i] +
                        "</option>";
                }
                aclaraJQuery("#iws_bc_ddl_select").append(comparisons);
                aclaraJQuery("#iws_bc_ddl_select").focus();


                aclaraJQuery("#iws_bc_ddl_select").on('change', function () {
                    billComparisonFunctions.Change(this.value);
                })
            }


            fromDate = iws_bc.Data.CompareFrom.CompareDate;
            toDate = iws_bc.Data.CompareTo.CompareDate;

            aclaraJQuery("#iws_bc_a_from_selection").html(iws_bc.Data.CompareFrom.CompareDateFormatted);
            aclaraJQuery("#iws_bc_a_to_selection").html(iws_bc.Data.CompareTo.CompareDateFormatted);

            billComparisonFunctions.BuildFromDropdown();
            billComparisonFunctions.BuildToDropdown();

            if (iws_bc.ShowTable === "true") {
                billComparisonFunctions.DrawTable();
            }

            if (iws_bc.Data.CompareFrom.TotalCost < 0 || iws_bc.Data.CompareTo.TotalCost < 0) {
                aclaraJQuery("#iws_bc_chart").remove();
            }
            else {
                billComparisonFunctions.DrawChart();
            }

        }



        aclaraJQuery("#iws_bc_wrapper").show();
        aclaraJQuery.fn.accesibleloader.unloadImage("iws_bc_wrapper", "iws_bc_loader");

        if (iws_dl_highlight === true) {
            billComparisonFunctions.HighLightElement();
        }

        IframeExternalUrl(aclaraJQuery("[class*=container]"));
    };

    this.Change = function (selection) {
        aclaraJQuery("#iws_bc_ddl_lbl_error").text("");
        iws_comp_selected = selection;

        if (iws_comp_selected === "custom") {
            billComparisonFunctions.BuildFromDropdown();
            billComparisonFunctions.BuildToDropdown();
            aclaraJQuery("#iws_bc_date_range").show();
        } else {

            aclaraJQuery("#iws_bc_date_range").hide();

            billComparisonFunctions.GetData();
            if (iws_bc.ShowTable === "true") {
                billComparisonFunctions.DrawTable();
            }

            if (iws_bc.Data.CompareFrom.TotalCost < 0 || iws_bc.Data.CompareTo.TotalCost < 0) {
                aclaraJQuery("#iws_bc_chart").remove();
            } else {
                billComparisonFunctions.DrawChart();
            }

            fromDate = iws_bc.Data.CompareFrom.CompareDate;
            toDate = iws_bc.Data.CompareTo.CompareDate;

            aclaraJQuery("#iws_bc_a_from_selection").html(iws_bc.Data.CompareFrom.CompareDateFormatted);
            aclaraJQuery("#iws_bc_a_to_selection").html(iws_bc.Data.CompareTo.CompareDateFormatted);
        }
        
    }
    this.GetData = function () {

        if (iws_comp_selected !== "") {
            fromDate = null;
            toDate = null;
        }
        else if (toDate == null && compareTo !== "") {
            toDate = compareTo;
        }

        var data = {
            "tabKey": tabKey,
            "parameters": modParams,
            "fromDate": fromDate,
            "toDate": toDate,
            "type": iws_comp_selected
        };
        aclaraJQuery.ajax({
            url: ResolveURL("~/BillComparison/Get"),
            type: "POST",
            async: false,
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                iws_bc = JSON.parse(data);
                iws_bc.isIos = useRegularDropdown();
                //iws_bc.isIos = true;
            }
        }).fail(function (jqXHR, textStatus) {
            if (jqXHR.status === 0 || jqXHR.readyState === 0) {
                return;
            }
            else {
                billComparisonFunctions.ShowError();
                aclaraJQuery("#iws_bc_wrapper").hide();
            }
        });

    };
    this.DrawTable = function () {

        var commodityData = { ShowGas: "false", ShowElectric: "false", ShowWater: "false", MultipleCommodities: "false", GasFrom: "", GasTo: "", GasDiff: "", ElectricFrom: "", ElectricTo: "", ElectricDiff: "", WaterFrom: "", WaterTo: "", WaterDiff: "", GasCostFrom: "", GasCostTo: "", ElectricCostFrom: "", ElectricCostTo: "", WaterCostFrom: "", WaterCostTo: "" };

        var comFrom;
        for (i = 0; i < iws_bc.Data.CompareFrom.Commodities.length; i++) {
            comFrom = iws_bc.Data.CompareFrom.Commodities[i];
            if (comFrom.Commodity === "gas" && iws_bc.Commodities.indexOf("gas") !== -1) {
                commodityData.ShowGas = "true";
                commodityData.GasFrom = iws_bc.Data.CompareFrom.Commodities[i].TotalUse + " " + iws_bc.Data.CompareFrom.Commodities[i].UOM;
                commodityData.GasCostFrom = iws_bc.Data.CompareFrom.Commodities[i].TotalCostFormatted;
            }

            else if (comFrom.Commodity === "electric" && iws_bc.Commodities.indexOf("electric") !== -1) {
                commodityData.ShowElectric = "true";
                commodityData.ElectricFrom = iws_bc.Data.CompareFrom.Commodities[i].TotalUse + " " + iws_bc.Data.CompareFrom.Commodities[i].UOM;
                commodityData.ElectricCostFrom = iws_bc.Data.CompareFrom.Commodities[i].TotalCostFormatted;
            }
            else if (comFrom.Commodity === "water" && iws_bc.Commodities.indexOf("water") !== -1) {
                commodityData.ShowWater = "true";
                commodityData.WaterFrom = iws_bc.Data.CompareFrom.Commodities[i].TotalUse + " " + iws_bc.Data.CompareFrom.Commodities[i].UOM;
                commodityData.WaterCostFrom = iws_bc.Data.CompareFrom.Commodities[i].TotalCostFormatted;
            }
        }

        if (commodityData.GasFrom === "") {
            commodityData.GasFrom = "0";
        }

        if (commodityData.ElectricFrom == "") {
            commodityData.ElectricFrom = "0";
        }

        if (commodityData.WaterFrom == "") {
            commodityData.WaterFrom = "0";
        }

        if (commodityData.GasCostFrom == "") {
            commodityData.GasCostFrom = "0";
        }

        if (commodityData.ElectricCostFrom == "") {
            commodityData.ElectricCostFrom = "0";
        }

        if (commodityData.WaterCostFrom == "") {
            commodityData.WaterCostFrom = "0";
        }

        var comTo;
        for (i = 0; i < iws_bc.Data.CompareTo.Commodities.length; i++) {
            comTo = iws_bc.Data.CompareTo.Commodities[i];
            if (comTo.Commodity == "gas" && iws_bc.Commodities.indexOf("gas") !== -1) {
                commodityData.ShowGas = "true";
                commodityData.GasTo = iws_bc.Data.CompareTo.Commodities[i].TotalUse + " " + iws_bc.Data.CompareTo.Commodities[i].UOM;
                commodityData.GasCostTo = iws_bc.Data.CompareTo.Commodities[i].TotalCostFormatted;
            }
            else if (comTo.Commodity == "electric" && iws_bc.Commodities.indexOf("electric") !== -1) {
                commodityData.ShowElectric = "true";
                commodityData.ElectricTo = iws_bc.Data.CompareTo.Commodities[i].TotalUse + " " + iws_bc.Data.CompareTo.Commodities[i].UOM;
                commodityData.ElectricCostTo = iws_bc.Data.CompareTo.Commodities[i].TotalCostFormatted;
            }
            else if (comTo.Commodity == "water" && iws_bc.Commodities.indexOf("water") !== -1) {
                commodityData.ShowWater = "true";
                commodityData.WaterTo = iws_bc.Data.CompareTo.Commodities[i].TotalUse + " " + iws_bc.Data.CompareTo.Commodities[i].UOM;
                commodityData.WaterCostTo = iws_bc.Data.CompareTo.Commodities[i].TotalCostFormatted;
            }
        }

        if ((commodityData.ShowGas == "true" && commodityData.ShowElectric == "true" && commodityData.ShowWater == "true") ||
            (commodityData.ShowGas == "true" && commodityData.ShowElectric == "true" && commodityData.ShowWater == "false") ||
            (commodityData.ShowGas == "true" && commodityData.ShowElectric == "false" && commodityData.ShowWater == "true") ||
            (commodityData.ShowGas == "false" && commodityData.ShowElectric == "true" && commodityData.ShowWater == "true")) {
            commodityData.MultipleCommodities = "true";
        }

        if (commodityData.GasTo == "") {
            commodityData.GasTo = "-";
        }

        if (commodityData.ElectricTo == "") {
            commodityData.ElectricTo = "-";
        }

        if (commodityData.WaterTo == "") {
            commodityData.WaterTo = "-";
        }
        if (commodityData.GasCostTo == "") {
            commodityData.GasCostTo = "-";
        }

        if (commodityData.ElectricCostTo == "") {
            commodityData.ElectricCostTo = "-";
        }

        if (commodityData.WaterCostTo == "") {
            commodityData.WaterCostTo = "-";
        }

        if (iws_bc.Data.DifferenceGasUse.substring(0, 1) == "0") {
            commodityData.GasDiff = "-";
        }
        else if (iws_bc.Data.DifferenceGasUse > 0) {
            commodityData.GasDiff = "+" + iws_bc.Data.DifferenceGasUse;
        }
        else {
            commodityData.GasDiff = iws_bc.Data.DifferenceGasUse;
        }
        
        if (iws_bc.Data.DifferenceElectricUse.substring(0, 1) == "0") {
            commodityData.ElectricDiff = "-";
        }
        else if (iws_bc.Data.DifferenceElectricUse > 0) {
            commodityData.ElectricDiff = "+" + iws_bc.Data.DifferenceElectricUse;
        }
        else {
            commodityData.ElectricDiff = iws_bc.Data.DifferenceElectricUse;
        }

        if (iws_bc.Data.DifferenceWaterUse.substring(0, 1) == "0") {
            commodityData.WaterDiff = "-";
        }
        else if (iws_bc.Data.DifferenceWaterUse > 0) {
            commodityData.WaterDiff = "+" + iws_bc.Data.DifferenceWaterUse;
        }
        else {
            commodityData.WaterDiff = iws_bc.Data.DifferenceWaterUse;
        }

        iws_bc.CommodityData = commodityData;

        var billDaysData = { ShowBillDays: "" };
        if ((iws_bc.Data.CompareFrom.NumberOfDays != null && iws_bc.Data.CompareFrom.NumberOfDays != "") || (iws_bc.Data.CompareTo.NumberOfDays != null && iws_bc.Data.CompareTo.NumberOfDays != "")) {
            billDaysData.ShowBillDays = "true";
        }
        else {
            billDaysData.ShowBillDays = "false";
        }

        iws_bc.BillDaysData = billDaysData;

        if (iws_bc.Data.DifferenceNumberOfDays == "" || iws_bc.Data.DifferenceNumberOfDays == 0) {
            iws_bc.Data.DifferenceNumberOfDays = "-";
        }
        else if (iws_bc.Data.DifferenceNumberOfDays > 0) {
            iws_bc.Data.DifferenceNumberOfDays = "+" + iws_bc.Data.DifferenceNumberOfDays;
        }

        var avgTempData = { ShowAvgTemp: "" };

        if (iws_bc.Data.DifferenceAvgTemp == 0) {
            iws_bc.Data.DifferenceAvgTemp = "-";
        }
        else if (iws_bc.Data.CompareFrom.AvgTemp != 0 || iws_bc.Data.CompareTo.AvgTemp != 0) {
            avgTempData.ShowAvgTemp = "true";
            if (iws_bc.Data.DifferenceAvgTemp > 0) {
                iws_bc.Data.DifferenceAvgTemp = "+" + iws_bc.Data.DifferenceAvgTemp;
            }
        }
        else {
            avgTempData.ShowAvgTemp = "false";
        }

        iws_bc.AverageTemperatureData = avgTempData;

        var utilityCostsData = { ShowUtilityCosts: "" };
        if (iws_bc.Data.CompareFrom.UtilityCost > 0 || iws_bc.Data.CompareTo.UtilityCost > 0) {

            utilityCostsData.ShowUtilityCosts = "true";
            if (iws_bc.Data.DifferenceUtilityCostFormatted > 0) {
                iws_bc.Data.DifferenceUtilityCostFormatted = "+" + iws_bc.Data.DifferenceUtilityCostFormatted;
            }
        }
        else {
            utilityCostsData.ShowAdditionalCosts = "false";
        }

        iws_bc.UtilityCostsData = utilityCostsData;

        template = aclaraJQuery.templates("#tmpl_billcomparison");
        aclaraJQuery("#iws_bc_table").empty().append(template.render(iws_bc));


    };
    this.DrawChart = function () {

        var fromDate = new Date(iws_bc.Data.CompareFrom.CompareDate);
        var toDate = new Date(iws_bc.Data.CompareTo.CompareDate);

        var options = {
            chart: {
                renderTo: 'iws_bc_chart',
                type: 'bar',
                height: '200',
                marginTop: '25',
                marginBottom: '25'
            },
            credits: {
                enabled: false
            },
            title: {
                text: null
            },
            yAxis: {
                gridLineColor: 'white',
                gridLineWidth: 0,
                tickWidth: 0,
                labels: {
                    enabled: false,
                },
                title: {
                    text: null
                }
            },
            xAxis: {
                categories: [iws_bc.Data.CompareFrom.CompareDateFormatted, iws_bc.Data.CompareTo.CompareDateFormatted],
                labels: {
                    align: 'left',
                    x: 20,
                    y: -30,
                    style: {
                        fontSize: '13px',
                        textShadow: 'null',
                        fontWeight: 'bold'
                    }
                },
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            plotOptions: {

                candlestick: {
                    lineColor: '#ffffff'
                },
                bar: {
                    Height: 20,
                    dataLabels: {
                        formatter: function () {
                            return this.point.formattedCost;
                        },
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || '#1a1a1a',
                        align: 'right',
                        style: {
                            fontSize: '14px',
                            textShadow: 'null',
                            fontWeight: 'bold'
                        }
                    },
                }
            },
            series: [{
                data: [{ y: iws_bc.Data.CompareFrom.TotalCost, formattedCost: iws_bc.TotalCostFromFormatted },
                { y: iws_bc.Data.CompareTo.TotalCost, formattedCost: iws_bc.TotalCostToFormatted }]
            }]
        };

        aclaraJQuery("#iws_bc_wrapper").show();
        var chart = new Highcharts.Chart(options);

    };
    this.BuildFromDropdown = function () {
        var fromDates = "";
        var i = 0;

        aclaraJQuery.each(iws_bc.Dates, function (key, value) {
            if (i > 0) {
                
                if (iws_bc.isIos) {
                    fromDates += "<option value=\"" +
                        key +
                        "\"" +
                        ">" +
                        value +
                        "</option>";
                }
                else
                {
                    fromDates += "<li role=\"option\"><a id=\"iws_bc_ddl_li_from_" + key + "\" href=\"#\" onclick=\"return false;\" val=\"" + key + "\">" + value + "</a></li>";             
                }
                
            }

            i++;
        });


        if (iws_bc.isIos)
        {     
            aclaraJQuery("#iws_bc_ddl_from_select").empty().append(fromDates);
            aclaraJQuery("#iws_bc_ddl_from_select").off().on('change', function () {
                fromDate = this.value;
                if (new Date(toDate) <= new Date(fromDate)) {
                    billComparisonFunctions.BuildToDropdown();
                    aclaraJQuery("#iws_bc_ddl_lbl_error").text(iws_bc.CompareDatePrompt + " " + aclaraJQuery(this).find("option:selected").text());
                    //aclaraJQuery("#iws_bc_a_to_selection").html(iws_bc.ChooseDate);
                    aclaraJQuery("#iws_bc_ddl_btn_compare").hide();
                }
                else {
                    aclaraJQuery("#iws_bc_ddl_lbl_error").text("");
                    billComparisonFunctions.BuildToDropdown();
                }
                })
        }
        else
        {
            aclaraJQuery("#iws_bc_ddl_ul_from").empty().append(fromDates);
            aclaraJQuery("[id^=iws_bc_ddl_li_from_]").each(function () {
                aclaraJQuery(this).off().click(function (e) {

                    aclaraJQuery("#iws_bc_a_from_selection").html(aclaraJQuery(this).text());
                    fromDate = aclaraJQuery(this).attr("val");

                    if (new Date(toDate) <= new Date(fromDate)) {
                        billComparisonFunctions.BuildToDropdown();
                        aclaraJQuery("#iws_bc_ddl_lbl_error").text(iws_bc.CompareDatePrompt + " " + aclaraJQuery("#iws_bc_a_from_selection").text());
                        aclaraJQuery("#iws_bc_a_to_selection").html(iws_bc.ChooseDate);
                        aclaraJQuery("#iws_bc_ddl_btn_compare").hide();
                    }
                    else {
                        aclaraJQuery("#iws_bc_ddl_lbl_error").text("");
                        billComparisonFunctions.BuildToDropdown();
                    }

                });
            });
        }



    };
    this.BuildToDropdown = function () {
        var toDates = "";
        if (iws_bc.isIos) {
            toDates += "<option value=\"-1\">" + iws_bc.ChooseDate + "</option>";
        }
        aclaraJQuery.each(iws_bc.Dates, function (key, value) {
            if (new Date(fromDate) < new Date(key)) {
                
                if (iws_bc.isIos) {
                    toDates += "<option value=\"" +
                        key +
                        "\"" +
                        ">" +
                        value +
                        "</option>";
                }
                else
                {
                    toDates += "<li role=\"option\"><a id=\"iws_bc_ddl_li_to_" + key + "\" href=\"#\" onclick=\"return false;\" val=\"" + key + "\">" + value + "</a></li>";
                }
            }
        });

        if (iws_bc.isIos)
        {
            aclaraJQuery("#iws_bc_ddl_to_select").empty().append(toDates);
            aclaraJQuery("#iws_bc_ddl_to_select").off().on('change', function () {
                toDate = this.value;
                if (toDate != "-1")
                {
                    aclaraJQuery("#iws_bc_ddl_lbl_error").text("");
                    aclaraJQuery("#iws_bc_ddl_btn_compare").show();
                }
                else
                {

                }
            })
        }
        else
        {
            aclaraJQuery("#iws_bc_ddl_ul_to").empty().append(toDates);
            aclaraJQuery("[id^=iws_bc_ddl_li_to_]").each(function () {
                aclaraJQuery(this).off().click(function () {

                    aclaraJQuery("#iws_bc_a_to_selection").html(aclaraJQuery(this).text());
                    toDate = aclaraJQuery(this).attr("val");
                    aclaraJQuery("#iws_bc_ddl_lbl_error").text("");
                    aclaraJQuery("#iws_bc_ddl_btn_compare").show();
                });

                //aclaraJQuery("#iws_bc_ddl_btn_compare").off().click(function (e) {
                //    e.stopPropagation();
                //    iws_comp_selected = "";

                //    billComparisonFunctions.GetData();

                //    if (iws_bc.ShowTable == "true") {
                //        billComparisonFunctions.DrawTable();
                //    }

                //    if (iws_bc.Data.CompareFrom.TotalCost < 0 || iws_bc.Data.CompareTo.TotalCost < 0) {
                //        aclaraJQuery("#iws_bc_chart").remove();
                //    }
                //    else {
                //        billComparisonFunctions.DrawChart();
                //    }

                //});

            });
        }

        aclaraJQuery("#iws_bc_ddl_btn_compare").off().click(function (e) {
            e.stopPropagation();
            iws_comp_selected = "";

            billComparisonFunctions.GetData();

            if (iws_bc.ShowTable == "true") {
                billComparisonFunctions.DrawTable();
            }

            if (iws_bc.Data.CompareFrom.TotalCost < 0 || iws_bc.Data.CompareTo.TotalCost < 0) {
                aclaraJQuery("#iws_bc_chart").remove();
            }
            else {
                billComparisonFunctions.DrawChart();
            }

        });

        if (aclaraJQuery("#iws_bc_ddl_to_select").val() == "-1"){
            aclaraJQuery("#iws_bc_ddl_btn_compare").hide();
        }

    };
    this.DeepLink = function () {

        if (iws_dl_comp != null && iws_dl_comp != "") {
            iws_comp_selected = iws_dl_comp;
            iws_dl_highlight = true;
        }
    };
    this.HighLightElement = function () {
        aclaraJQuery(".deep-link-highlight:first").focus();
        aclaraJQuery(".deep-link-highlight").effect("highlight", { color: iws_dl_yfc }, 2000);
        billComparisonFunctions.ResetDeepLinkVariables();
    };
    this.ResetDeepLinkVariables = function () {
        iws_dl_tp = "";
        iws_dl_w = "";
        iws_dl_wt = "";
        iws_dl_ws = "";
        iws_dl_enum = "";
        iws_dl_acid = "";
        iws_dl_res = "";
        iws_dl_comp = "";
    };
    this.ShowError = function () {
        var template = aclaraJQuery.templates("#tmpl_bc_fatal_error");
        var error = {};
        error.Message = iws_widget_error;
        aclaraJQuery("#iws_bc_main").html(template.render(error));
        aclaraJQuery.fn.accesibleloader.unloadImage("iws_bc_wrapper", "iws_bc_loader");
        aclaraJQuery("#iws_bc_wrapper").show();
    };

    this.HideContentShowMessage = function (msg) {
        var template = aclaraJQuery.templates("#tmpl_bc_no_data");
        var noData = {};
        if (iws_bc.ShowTitle == "true") {
            noData.Title = iws_bc.Title;
            noData.ShowTitle = true;
        }
        noData.Message = msg;
        aclaraJQuery("#iws_bc_main").html(template.render(noData));
        aclaraJQuery.fn.accesibleloader.unloadImage("iws_bc_wrapper", "iws_bc_loader");
        aclaraJQuery("#iws_bc_wrapper").show();
    };

};




