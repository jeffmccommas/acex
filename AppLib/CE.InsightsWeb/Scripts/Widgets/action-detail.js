﻿var actionDetailFunctions = new function () {

    var parentType;
    var parentObj;

    this.InitializeWidget = function (parType, currentTab, key, actions, highlight) {
        var actionUpdated = false;
        parentType = parType;

        var action;

        if (parentType == "iws_as")
        {
            parentObj = iws_as;
        }
        else if (parentType == "iws_ap")
        {
            parentObj = iws_ap;
        }

        for (var i = 0; i < actions.length; i++) {
            if (actions[i].Key == key) {
                action = actions[i];
            }
        };

        parentActions = parentObj.Tabs[currentTab].Actions;

        for (var i = 0; i < parentActions.length; i++) {
            if (parentActions[i].Key == key) {
                action.Plan = parentActions[i].Plan;
            }
        };

        aclaraJQuery("#iws_ad_loader").accesibleloader({
            imagePath: ResolveURL("~/content/images/loading.gif"),
            loadingMessage: iws_loading_text
        });

        var template = aclaraJQuery.templates("#tmpl_ad_default");
        aclaraJQuery("#iws_ad_main").replaceWith(template.render(parentObj));

        //var opts = parentObj.DetailAddToPlanOptions.split(",");
        //action.AddButtonText = opts[0];
        //action.CompleteButtonText = opts[1];

        //var template = aclaraJQuery.templates("#tmpl_ad_btns");
        //aclaraJQuery("#iws_ad_btns").replaceWith(template.render(action));

        if (parentObj.DetailShowTitle == "true")
        {
            aclaraJQuery("#iws_ad_title").text(parentObj.Title);
        }
        else
        {
            aclaraJQuery("#iws_ad_title").remove();
        }
        var converter = new showdown.Converter();
        if (parentObj.DetailShowIntro == "true") {
            aclaraJQuery("#iws_ad_intro_text").append(converter.makeHtml(parentObj.DetailIntroText));
        }

        if (parentObj.DetailShowFooterText == "false") {
            aclaraJQuery("#iws_ad_footertext").remove();
        }
        else {
            aclaraJQuery("#iws_ad_footertext").html(parentObj.DetailFooter);
        }

        if (parentObj.DetailShowFooterLink == "false") {
            aclaraJQuery("#iws_ad_footerlink").remove();
        }
        else {
            aclaraJQuery("#iws_ad_footerlink a").attr("href", parentObj.DetailFooterLink).text(parentObj.DetailFooterLinkText);
            if (parentObj.DetailFooterLink.toLowerCase().indexOf("type=pm") > 0) {
                aclaraJQuery("#iws_ad_footerlink a").addClass("iframe-external-url").attr("href", parentObj.DetailFooterLink.split("?type=pm")[0]);
            }
        }

        aclaraJQuery("#iws_ad_ai_img").attr("src", action.ImageUrl);
        aclaraJQuery("#iws_ad_ai_img").attr("alt", action.ImageTitle);

        aclaraJQuery("#iws_ad_ai_title").text(action.Title);
        aclaraJQuery("#iws_ad_ai_category").html(parentObj.DetailCategoryTitle + "<br><span>" + action.Category + "</span>");

        if (action.UpfrontCost == 0)
        {
            aclaraJQuery("#iws_ad_ai_cost").html(parentObj.DetailCostTitle + "<br><span>" + parentObj.DetailFree + "</span>");
        }
        else
        {
            aclaraJQuery("#iws_ad_ai_cost").html(parentObj.DetailCostTitle + "<br><span>" + action.UpfrontCostFormatted + "</span>");
        }

        aclaraJQuery("#iws_ad_ai_savings").html(parentObj.DetailSavingsTitle + "<br><span>" + action.AnnualSavingsEstimateFormatted + "</span>");

        var circles = 0;
        switch (action.Difficulty) {
            case 0:
                circles = 1;
                break;
            case 1:
                circles = 2;
                break;
            case 2:
                circles = 3;
                break;
            case 3:
                circles = 4;
                break;
        }

        var difficulty = parentObj.DetailDifficultyTitle + "<br>";
        for (var i = 0; i < circles; i++) {
            difficulty += "<div class=\"detail-items__circle\"></div>";
            
        }

        aclaraJQuery("#iws_ad_ai_difficulty").html(difficulty);

        aclaraJQuery("#iws_details_bottom").empty().append(converter.makeHtml(action.DescriptionMarkDown));

        if (actions.length > 1)
        {

            aclaraJQuery("#iws_details_bottom").append("<div class=\"while-youre-at-it\" >" + parentObj.DetailWhileYoureAtIt + "</div>");

            var acts = "";
            for (var i = 0; i < actions.length; i++) {
                if (actions[i].Key != key)
                {
                    acts += actions[i].MarkDown + "\n";
                }
            }

            aclaraJQuery("#iws_details_bottom").append(converter.makeHtml(acts));

            aclaraJQuery("[href*='iws_ad_ra']").each(function () {

                aclaraJQuery(this).attr("id", aclaraJQuery(this).attr("href"));
                aclaraJQuery(this).attr("href", "#");

                aclaraJQuery(this).click(function (e) {
                    e.preventDefault();
                    var key = aclaraJQuery(this).attr('id').split("-")[1];
                    //aclaraJQuery("#iws_ad_plan_message").empty();
                        actionDetailFunctions.InitializeWidget(parType, currentTab, key, actions);
                });
            });
        }

        var status = "";
        if (action.Plan != null)
        {
            action.Status = action.Plan.Status;
        }
        else
        {
            action.Status = "";
        }

        actionDetailFunctions.SetInitialActionButtonsState(action);
        var key = action.Key.replace(/\./g, '\\.');
        aclaraJQuery("#iws_ad_btn1-" + key).off().click(function () {
            var key = aclaraJQuery(this).attr('id').split("-")[1];
            var status = aclaraJQuery(this).attr('val');
            actionDetailFunctions.UpdateActionStatus(key, currentTab, status);
            actionDetailFunctions.SetActionButtonsState(status);
        });

        aclaraJQuery("#iws_ad_btn2-" + key).off().click(function () {
            var key = aclaraJQuery(this).attr('id').split("-")[1];
            var status = aclaraJQuery(this).attr('val');
            actionDetailFunctions.UpdateActionStatus(key, currentTab, status);
            actionDetailFunctions.SetActionButtonsState(status);
        });

        if (parentObj.DetailShowBackLink === "true") {

            aclaraJQuery("#iws_ad_back").text(parentObj.DetailBackLinkText)
                .off()
                .click(function() {
                    actionDetailFunctions.Back(parentType);
                });
        } else {
            aclaraJQuery("#iws_ad_back").remove();
        }

        var ei = [];
        ei.push({ key: "ActionId", value: key });
        ei.push({ key: "TabId", value: tabKey });
        var data = {
            "eventInfo": ei,
            "parameters": modParams,
            "eventType": "View Action Detail"
        };

        PostEvents(data);

        aclaraJQuery("#iws_ad_wrapper").show();

        aclaraJQuery.fn.accesibleloader.unloadImage("iws_ad_wrapper", "iws_ad_loader");

        if (highlight == true)
        {
            actionDetailFunctions.HighLightElement();
        }
    }

    this.Back = function (parentType) {
        aclaraJQuery("#iws_ad_wrapper").hide();
        if (parentType === "iws_as") {
            waysToSaveFunctions.BuildActionList(iws_as_current_page - 1);
            aclaraJQuery("#iws_as_wrapper").show();
        }
        else if (parentType === "iws_ap") {
            planFunctions.BuildInPlanActionList();
            planFunctions.BuildCompletedActionList();
            aclaraJQuery("#iws_ap_wrapper").show();
        }    
    }

    this.SetInitialActionButtonsState = function (action) {
        var key = action.Key.replace(/\./g, '\\.');
        if (action.Status == "selected") {
            var opts = parentObj.DetailAddedToDoOptions.split(",");

            action.Button1Text = opts[0];
            action.Button1Value = "completed";
            action.Button2Text = opts[1];
            action.Button2Value = "cancelled";

            var template = aclaraJQuery.templates("#tmpl_ad_btns");
            aclaraJQuery("#iws_ad_btns").replaceWith(template.render(action));
        }
        else if (action.Status == "completed") {
            var opts = parentObj.DetailCompletedOptions.split(",");

            action.Button1Text = opts[0];
            action.Button1Value = "selected";
            action.Button2Text = opts[1];
            action.Button2Value = "cancelled";
        }
        else if (action.Status == "") {
            var opts = parentObj.DetailAddToPlanOptions.split(",");

            action.Button1Text = opts[0];
            action.Button1Value = "selected";
            action.Button2Text = opts[1];
            action.Button2Value = "completed";
        }

        var template = aclaraJQuery.templates("#tmpl_ad_btns");
        aclaraJQuery("#iws_ad_btns").empty().append(template.render(action));

    }

    this.SetActionButtonsState = function (status) {

        if (status == "selected")
        {
            aclaraJQuery("#iws_ad_btns").empty().append("<p class=\"detail-added\"><img src=\"Content/Images/icon-round-green-check.png\" width=\"19\" height=\"19\" />" + parentObj.DetailAddedMessage + " <a href=\"" +parentObj.DetailActionPlanLink + "\" >" + parentObj.DetailActionPlanLinkText + "</a></p>");
        }
        else if (status == "completed") 
        {
            aclaraJQuery("#iws_ad_btns").empty().append("<p class=\"detail-completed\"><img src=\"Content/Images/icon-round-green-check.png\" width=\"19\" height=\"19\" />" +parentObj.DetailCompletedMessage + " <a href=\"" +parentObj.DetailActionPlanLink + "\" >" +parentObj.DetailActionPlanLinkText + "</a></p>");
        }
        else if (status == "cancelled")
        {
            aclaraJQuery("#iws_ad_btns").empty().append("<p class=\"detail-removed\"><img src=\"Content/Images/icon-round-green-check.png\" width=\"19\" height=\"19\" />" + parentObj.DetailRemovedMessage + " <a href=\"" + parentObj.DetailActionPlanLink + "\" >" + parentObj.DetailActionPlanLinkText + "</a></p>");
        }

    }


    this.UpdateActionStatus = function (key, currentTab, status) {

        actionUpdated = true;
        var actions;

        if (parentType == "iws_as") {
            actions = iws_as.Tabs[currentTab].Actions;
        }
        else if (parentType == "iws_ap") {
            actions = iws_ap.Tabs[currentTab].Actions;
        }

        var Plan = { Key: key, Status: status };
        
        var ei = [];
        ei.push({ key: "TabId", value: tabKey });

        for (var i = 0; i < actions.length; i++) {
            if (actions[i].Key == key) {
                if (actions[i].Plan != null) {
                    actions[i].Plan.Status = status;
                }
                else {
                    actions[i].Plan = Plan;
                }

                ei.push({ key: "ActionId", value: actions[i].Key });
                ei.push({ key: "EstimatedSavings", value: actions[i].AnnualSavingsEstimate });
                ei.push({ key: "EstimatedCost", value: actions[i].UpfrontCost });
                ei.push({ key: "EstimatedRebate", value: actions[i].RebateAmount });

                break;
            }
        }

        var data;
        if (status == "selected") {
            data = {
                "eventInfo": ei,
                "parameters": modParams,
                "eventType": "Add Action"
            }

        }
        else if (status == "completed") {
            data = {
                "eventInfo": ei,
                "parameters": modParams,
                "eventType": "Complete Action"
            }
        }
        else if (status == "cancelled") {
            data = {
                "eventInfo": ei,
                "parameters": modParams,
                "eventType": "Remove Action"
            }
        }

        PostEvents(data);

        var dat = {
            "plan": [{ Key: key, Status: status }],
            "parameters": modParams
        }

        aclaraJQuery.ajax({
            url: ResolveURL("~/ActionPlan/PostActions"),
            type: "POST",
            data: JSON.stringify(dat),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function () {
                if (typeof goalFunctions != "undefined") {
                    goalFunctions.InitializeWidget();
                }
            },
            error: function () {

            }
        });

    }

    this.HighLightElement = function () {
        aclaraJQuery(".deep-link-highlight:first").focus();
        aclaraJQuery(".deep-link-highlight").effect("highlight", { color: iws_dl_yfc }, 2000);
    }

}






