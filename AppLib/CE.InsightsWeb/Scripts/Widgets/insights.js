﻿aclaraJQuery(document).ready(function () {
    insightsFunctions.InitializeWidget();
});

var insightsFunctions = new function () {
    var iws_in;
    var expanded;

    this.InitializeWidget = function () {

        aclaraJQuery("#iws_in_wrapper").hide();

        aclaraJQuery("#iws_in_loader").accesibleloader({
            imagePath: ResolveURL("~/content/images/loading.gif"),
            loadingMessage: iws_loading_text
        });

        insightsFunctions.GetData();

    };

    this.SetContent = function () {
        // iws_in = null;  //uncomment to test fatal error

        //iws_in.NoInsights = true; //uncomment to test no insights message 
        //iws_in.NoInsightsText = "Sorry no insights available...."; //uncomment to test no insights message 

        if (iws_in === null || iws_in === undefined) {
            insightsFunctions.ShowError();
        }
        else if (iws_in.NoInsights === true) {
            insightsFunctions.HideContentShowMessage(iws_in.NoInsightsText);
        }
        else {
            var converter = new showdown.Converter();
            for (var i = 0; i < iws_in.InsightList.length; i++) {
                iws_in.InsightList[i].Title = converter.makeHtml(iws_in.InsightList[i].Title);
            }

            iws_in.isCSR = isCSR;

            if (iws_in.RowCount == "") {
                iws_in.RowCount = iws_in.InsightList.length;
            }

            iws_in.InsightsToShow = iws_in.InsightList.slice(0, iws_in.RowCount);
            iws_in.InsightsToShowLinks = aclaraJQuery.extend(true, [], iws_in.InsightList);
            expanded = false;

            var template = aclaraJQuery.templates("#tmpl_in_default");
            aclaraJQuery("#iws_in_main").html(template.render(iws_in));

            template = aclaraJQuery.templates("#tmpl_in_list");
            aclaraJQuery("#iws_in_list").html(template.render(iws_in));

            ////iws_in.ShowTitle = "true";
            if (iws_in.ShowTitle != "true") {
                aclaraJQuery("#iws_in_title").remove();
            }

            ////iws_in.ShowSubTitle = "true";
            if (iws_in.ShowSubTitle !== "true") {
                aclaraJQuery("#iws_in_subtitle").remove();
            }

            if (iws_in.ShowIntro === "true") {
                converter = new Showdown.converter();
                iws_in.IntroText = converter.makeHtml(iws_in.IntroText);
            }
            else {
                aclaraJQuery("#iws_in_intro_text").remove();
            }

            if (iws_in.ShowRecentTitle !== "true") {
                aclaraJQuery("#iws_in_recent_title").remove();
            }

            if (iws_in.ShowFooterText !== "true") {
                aclaraJQuery("#iws_in_footertext").remove();
            }

            if (iws_in.ShowExpandCollapse !== "true" || iws_in.MaxInsightsToShow <= iws_in.RowCount) {
                aclaraJQuery("#iws_in_viewall").remove();
            }

            if (iws_in.ShowFooterLink !== "true") {
                aclaraJQuery("#iws_in_footerlink").remove();
            } else
                insightsFunctions.CheckLinkType(iws_in.FooterLink, "iws_in_footerlink");

            aclaraJQuery("#iws_in_viewall").on("click", function () {

                if (expanded === false) {
                    expanded = true;
                    aclaraJQuery("#iws_view_all_icon").switchClass("icon-toggle-down", "icon-toggle-up");
                    aclaraJQuery("#iws_expand_collapse_label").text(iws_in.CollapseLabel);
                    iws_in.InsightsToShow = iws_in.InsightList.slice(0, iws_in.MaxInsightsToShow);
                    aclaraJQuery("#iws_in_recent_title").hide();
                }
                else {
                    expanded = false;
                    aclaraJQuery("#iws_expand_collapse_label").text(iws_in.ExpandLabel);
                    aclaraJQuery("#iws_view_all_icon").switchClass("icon-toggle-up", "icon-toggle-down");
                    iws_in.InsightsToShow = iws_in.InsightList.slice(0, iws_in.RowCount);
                    aclaraJQuery("#iws_in_recent_title").show();
                }

                template = aclaraJQuery.templates("#tmpl_in_list");
                aclaraJQuery("#iws_in_list").html(template.render(iws_in));
                insightsFunctions.BindEventLogging();
                insightsFunctions.AddExternalLinkClass();
            });
            IframeExternalUrl(aclaraJQuery("[class*=container]"));
            insightsFunctions.BindEventLogging();
            insightsFunctions.AddExternalLinkClass();
        }

        aclaraJQuery("#iws_in_wrapper").show();
        aclaraJQuery.fn.accesibleloader.unloadImage("iws_in_wrapper", "iws_in_loader");
    }

    this.GetData = function () {
        var data = {
            "tabKey": tabKey,
            "parameters": modParams
        };

        aclaraJQuery.ajax({
            url: ResolveURL("~/Insights/Get"),
            type: "POST",
            async: true,
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                iws_in = JSON.parse(data);
                insightsFunctions.SetContent();
            }
        }).fail(function (jqXHR, textStatus) {
            if (jqXHR.status === 0 || jqXHR.readyState === 0) {
                return;
            }
            else {
                insightsFunctions.ShowError();
            }
        });


    };

    this.BindEventLogging = function () {
        aclaraJQuery("[id^='iws_in_link_notification-'],[id^='iws_detail_link_notification']").off().click(function (e) {
            e.preventDefault();
            var ei = [];
            var actionId = aclaraJQuery(this).attr("name");
            var link = aclaraJQuery(this).attr("href");
            var linkClass = aclaraJQuery(this).attr("class");
            ei.push({ key: "ActionId", value: actionId });
            ei.push({ key: "TabId", value: tabKey });
            ei.push({ key: "Link", value: link });
            var data = {
                "eventInfo": ei,
                "parameters": modParams,
                "eventType": "Click Insight"
            };

            PostEvents(data);
            if (linkClass.indexOf("iframe-external-url") > -1) {
                aclaraJQuery(".iframe-resizer").attr('data-href', link);
            } else
                window.location.href = link;


        });
    }

    this.ShowError = function () {
        var template = aclaraJQuery.templates("#tmpl_in_fatal_error");
        var error = {};
        error.Message = iws_widget_error;
        aclaraJQuery("#iws_in_main").html(template.render(error));
        aclaraJQuery.fn.accesibleloader.unloadImage("iws_in_wrapper", "iws_in_loader");
        aclaraJQuery("#iws_in_wrapper").show();
    };

    this.HideContentShowMessage = function (msg) {
        var template = aclaraJQuery.templates("#tmpl_in_no_data");
        var noData = {};
        if (iws_in.ShowTitle === "true") {
            noData.Title = iws_in.Title;
            noData.ShowTitle = true;
        }
        noData.Message = msg;
        aclaraJQuery("#iws_in_main").html(template.render(noData));
        aclaraJQuery.fn.accesibleloader.unloadImage("iws_in_wrapper", "iws_in_loader");
        aclaraJQuery("#iws_in_wrapper").show();
    };

    this.CheckLinkType = function (link, id) {
        var result = [link, ""];
        if (link.toLowerCase().indexOf("type=pm") > 0) {
            if (id !== undefined) {
                aclaraJQuery("[id*=" + id + "]").find('a').addClass("iframe-external-url");
                aclaraJQuery("[id*=" + id + "]").find('a').attr("href", aclaraJQuery("[id*=" + id + "]").find('a').attr("href").split("?type=pm")[0]);
            }
            link = link.split("?type=pm")[0];
            result = [link, "external"];
            return result;
        }
        return result;
    };

    this.AddExternalLinkClass = function () {
        for (var j = 0; j < iws_in.InsightsToShow.length; j++) {
            for (var i = 0; i < iws_in.InsightsToShowLinks.length; i++) {
                if (iws_in.InsightsToShow[j].Title === iws_in.InsightsToShowLinks[i].Title) {
                    if (iws_in.InsightsToShowLinks[i].Link.indexOf("type=pm") > 0) {
                        if (j < 1) {
                            if (!aclaraJQuery("#iws_detail_link_notification").hasClass("iframe-external-url")) {
                                aclaraJQuery("#iws_detail_link_notification").attr("class", "iframe-external-url " + aclaraJQuery("#iws_detail_link_notification").attr("class"));
                            }
                            aclaraJQuery("#iws_detail_link_notification").attr("href", aclaraJQuery("#iws_detail_link_notification").attr("href").split("?type=pm")[0]);
                        } else {
                            if (!aclaraJQuery("[id*=iws_in_link_notification-" + j + "]").hasClass("iframe-external-url")) {
                                aclaraJQuery("[id*=iws_in_link_notification-" + j + "]").attr("class", "iframe-external-url " + aclaraJQuery("[id*=iws_in_link_notification-" + j + "]").attr("class"));
                            }
                            aclaraJQuery("[id*=iws_in_link_notification-" + j + "]").attr("href", aclaraJQuery("[id*=iws_in_link_notification-" + j + "]").attr("href").split("?type=pm")[0]);
                        }
                        var link = iws_in.InsightsToShowLinks[i].Link.split("?type=pm")[0];
                        iws_in.InsightsToShow[j].Link = link;
                    }
                }
            }
        }
    };
};