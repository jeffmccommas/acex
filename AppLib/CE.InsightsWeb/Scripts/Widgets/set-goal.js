﻿aclaraJQuery(document).ready(function () {

    goalFunctions.InitializeWidget();

});

var goalFunctions = new function () {

    var widgetStyle = "";
    var iws_gl;
    var prevView = "setGoal";
    var iws_dl_highlight = false;

    this.InitializeWidget = function () {

        aclaraJQuery("#iws_gl_wrapper").hide();

        goalFunctions.DeepLink();

        aclaraJQuery("#iws_gl_loader").accesibleloader({
            imagePath: ResolveURL("~/content/images/loading.gif"),
            loadingMessage: iws_loading_text
        });

        goalFunctions.GetData(false);

        //iws_gl = null;

        if (iws_gl === undefined || iws_gl === null) {
            goalFunctions.ShowError();
        }
        else
        {

            if (iws_gl.ShowIntro === "true") {
                var converter = new showdown.Converter();
                iws_gl.IntroText = converter.makeHtml(iws_gl.IntroText);
            }

            var template = aclaraJQuery.templates("#tmpl_gl_default");
            aclaraJQuery("#iws_gl_main").html(template.render(iws_gl));

            widgetStyle = iws_gl.HtmlTemplate;

            if (iws_gl.ShowTitle !== "true") {
                aclaraJQuery("#iws_gl_title").remove();
            }

            if (iws_gl.ShowIntro !== "true") {
                aclaraJQuery("#iws_gl_intro_text").remove();
            }

            if (iws_gl.ShowFooterText !== "true") {
                aclaraJQuery("#iws_gl_footertext").remove();
            }

            if (iws_gl.ShowFooterLink !== "true") {
                aclaraJQuery("#iws_gl_footerlink").remove();
            } else {
                if (iws_gl.FooterLink.toLowerCase().indexOf("type=pm") > 0) {
                    aclaraJQuery("#iws_gl_footerlink").find('a').addClass('iframe-external-url').attr("href", iws_gl.FooterLink.split("?type=pm")[0]);
                    iws_gl.FooterLink = iws_gl.FooterLink.split("?type=pm")[0];
                }
            }

            if (iws_gl.ShowIntro == "true") {
                var converter = new Showdown.converter();
                aclaraJQuery("#iws_gl_intro_text").empty().append(converter.makeHtml(iws_gl.IntroText));
            }

            goalFunctions.BuildNoGoalYet();
            goalFunctions.BuildOptions();
            goalFunctions.BuildSavedGoal();

            var goalAmount = iws_gl.GoalAmount;

            if (goalAmount === 0) {
                goalFunctions.ShowNoGoalYet();
            }
            else {
                prevView = "showGoal";
                goalFunctions.ShowSavedGoal();
            }

            aclaraJQuery("#iws_gl_wrapper").show();
            aclaraJQuery.fn.accesibleloader.unloadImage("iws_gl_wrapper", "iws_gl_loader");

            if (iws_dl_highlight === true) {
                goalFunctions.HighLightElement();
            }

        }

        IframeExternalUrl(aclaraJQuery("[class*=container]"));
    }


    this.BuildNoGoalYet = function () {
        aclaraJQuery("#iws_gl_intro_title").text(iws_gl.IntroTitle);
        aclaraJQuery("#iws_gl_intro_subtitle").text(iws_gl.IntroSubtitle);
        aclaraJQuery("#iws_gl_intro_button").html(iws_gl.IntroSetgoalButton + "<br><em>" + iws_gl.IntroSetgoalButtonDesc + "</em>");
        aclaraJQuery("#iws_gl_intro_options").html("<em>" + iws_gl.IntroOr + "</em><br><a id=\"iws_gl_moreoptions\" >" + iws_gl.IntroMoreOptions + "</a>");

        var template = aclaraJQuery.templates("#tmpl_setgoal" + widgetStyle + "_nogoalyet");
        aclaraJQuery("#iws_gl_nogoalyet_wrapper").empty().append(template.render(iws_gl));

        aclaraJQuery("#iws_gl_intro_options").off('click').click(function () {
            goalFunctions.GetData(true);
            prevView = "setGoal";
            goalFunctions.ShowOptions();
            aclaraJQuery("#iws_gl_option0").focus();
        });

        aclaraJQuery("#iws_gl_intro_button").off('click').click(function () {
            goalFunctions.SaveGoal(iws_gl.RecommendedPercent, iws_gl.RecommendedAmount, "set");
            prevView = "showGoal";
            goalFunctions.GetData(false);
            goalFunctions.BuildSavedGoal();
            goalFunctions.ShowSavedGoal();
        });

        goalFunctions.ShowNoGoalYet();
    }


    this.ShowNoGoalYet = function () {
        aclaraJQuery("#iws_gl_nogoalyet_wrapper").show();
        aclaraJQuery("#iws_gl_choosegoal_wrapper").hide();
        aclaraJQuery("#iws_gl_savedgoal_wrapper").hide();

    }

    this.BuildOptions = function () {
        var template = aclaraJQuery.templates("#tmpl_setgoal" + widgetStyle + "_choosegoal");
        aclaraJQuery("#iws_gl_choosegoal_wrapper").empty().append(template.render(iws_gl));

        for (var i = 0; i < iws_gl.GoalOptions.length; i++) {
            
            if (iws_gl.GoalOptions[i].Selected === "selected") {
                aclaraJQuery("#iws_gl_option" + i).prop('checked', true);
            }
        }

        aclaraJQuery("#iws_gl_save").off('click').click(function () {
            var val = aclaraJQuery("input:radio[name=iws_savings_goal]:checked").val().split("-");
            goalFunctions.SaveGoal(val[0], val[1], "save");
            goalFunctions.GetData(false);
            goalFunctions.BuildSavedGoal();

        });

        aclaraJQuery("#iws_gl_cancel").off('click').click(function () {
            if (prevView === "setGoal") {
                goalFunctions.ShowNoGoalYet();
            }
            else if (prevView === "showGoal") {
                goalFunctions.ShowSavedGoal();
            }

        });

        goalFunctions.ShowOptions();
    }

    this.ShowOptions = function () {
        aclaraJQuery("#iws_gl_nogoalyet_wrapper").hide();
        aclaraJQuery("#iws_gl_choosegoal_wrapper").show();
        aclaraJQuery("#iws_gl_savedgoal_wrapper").hide();
    }

    this.BuildSavedGoal = function () {
        
        var template = aclaraJQuery.templates("#tmpl_setgoal" + widgetStyle + "_savedgoal");
        aclaraJQuery("#iws_gl_savedgoal_wrapper").empty().append(template.render(iws_gl));

        //if (iws_gl.PlanAmount > 0 && iws_gl.PlanAmount < iws_gl.GoalAmount) {
        //    aclaraJQuery("#iws_gl_showgoal_amt_sm").empty().append(iws_gl.MyGoalAddMoreActions + "<span> " + iws_gl.GoalAmountFormatted + "</span>");
        //}
        //else if (iws_gl.PlanAmount >= iws_gl.GoalAmount) {
        //    aclaraJQuery("#iws_gl_showgoal_amt_sm").empty();
        //}
        //else {
        //    aclaraJQuery("#iws_gl_showgoal_amt_sm").empty().append(iws_gl.MyGoalCreatePlan);
        //}

        if (iws_gl.ShowWaysToSaveLink === "false") {
            aclaraJQuery("#iws_gl_goto_actions").remove();
        } else {
            if (iws_gl.WaysToSaveLink.toLowerCase().indexOf("type=pm") > 0) {
                aclaraJQuery("#iws_gl_goto_actions").addClass('iframe-external-url').attr("href", iws_gl.WaysToSaveLink.split("?type=pm")[0]);
                iws_gl.WaysToSaveLink = iws_gl.WaysToSaveLink.split("?type=pm")[0];
            }
        }

        var percentOfGoal = (iws_gl.PlanAmount / iws_gl.GoalAmount) * 100;

        if (percentOfGoal > 100)
        {
            percentOfGoal = 100;
        }

        aclaraJQuery("#iws_gl_percent_complete").css("width", percentOfGoal + "%");
        aclaraJQuery("#iws_gl_percent_complete").attr("aria-valuenow", percentOfGoal + "%");

        aclaraJQuery("#iws_gl_edit").off('click').click(function () {
            goalFunctions.GetData(false);
            goalFunctions.BuildOptions(false);
            goalFunctions.ShowOptions();
            aclaraJQuery("#iws_gl_option0").focus();
        });

        goalFunctions.ShowSavedGoal();

    }

    this.ShowSavedGoal = function () {
        aclaraJQuery("#iws_gl_nogoalyet_wrapper").hide();
        aclaraJQuery("#iws_gl_choosegoal_wrapper").hide();
        aclaraJQuery("#iws_gl_savedgoal_wrapper").show();
    }

    this.GetData = function (getOptions) {

        var data = {
            "tabKey": tabKey,
            "parameters": modParams,
            "getOptions": getOptions
        }

        aclaraJQuery.ajax({
            url: ResolveURL("~/SetGoal/GetGoal"),
            type: "POST",
            async: false,
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {

                iws_gl = JSON.parse(data);
            }
            }).fail(function (jqXHR, textStatus) {
                if (jqXHR.status === 0 || jqXHR.readyState === 0) {
                    return;
                }
                else {
                    goalFunctions.ShowError();
                }
            });

    }

    this.SaveGoal = function (percent, dollar, type) {

        for (var i = 0; i < iws_gl.Attributes.length; i++) {
            if (iws_gl.Attributes[i].Key === "savingsgoalpercent") {
                iws_gl.Attributes[i].Answer = percent;
            }
            else if (iws_gl.Attributes[i].Key === "savingsgoalamount") {
                iws_gl.Attributes[i].Answer = dollar;
            }
        }

        var ei = [];
        ei.push({ key: "TabId", value: tabKey });
        ei.push({ key: "SavingsAmount", value: dollar });
        ei.push({ key: "SavingsPercentage", value: percent });

        var data;
        if (type === "set") {
            data = {
                "eventInfo": ei,
                "parameters": modParams,
                "eventType": "Set Goal"
            }
        }
        if (type === "save") {
            data = {
                "eventInfo": ei,
                "parameters": modParams,
                "eventType": "Edit Goal"
            }
        }

        PostEvents(data);

        var dat = {
            "goal": iws_gl,
            "parameters": modParams
        }
        
        aclaraJQuery.ajax({
            url: ResolveURL("~/SetGoal/PostGoal"),
            async: false,
            type: "POST",
            data: JSON.stringify(dat),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function () {

            },

            error: function () {

            }
        });

    }

    //this.GoalAmount = function () {
    //    for (var i = 0; i < iws_gl.Attributes.length; i++) {
    //        if (iws_gl.Attributes[i].Key === "savingsgoalamount") {
    //            return iws_gl.Attributes[i].Answer;
    //        }
    //    }
    //}

    this.DeepLink = function () {

        if (iws_dl_w !== undefined && iws_dl_w === "setgoal") {
            iws_dl_highlight = true;
        }
    }

    this.HighLightElement = function () {
        aclaraJQuery(".deep-link-highlight:first").focus();
        aclaraJQuery(".deep-link-highlight").effect("highlight", { color: iws_dl_yfc }, 2000);
        goalFunctions.ResetDeepLinkVariables();
    }

    this.ResetDeepLinkVariables = function () {
        iws_dl_tp = "";
        iws_dl_w = "";
        iws_dl_wt = "";
        iws_dl_ws = "";
        iws_dl_enum = "";
        iws_dl_acid = "";
        iws_dl_res = "";
        iws_dl_comp = "";
    }

    this.ShowError = function () {
        var template = aclaraJQuery.templates("#tmpl_gl_fatal_error");
        var error = {};
        error.Message = iws_widget_error;
        aclaraJQuery("#iws_gl_main").html(template.render(error));
        aclaraJQuery.fn.accesibleloader.unloadImage("iws_gl_wrapper", "iws_gl_loader");
        aclaraJQuery("#iws_gl_wrapper").show();
    };

}






