﻿aclaraJQuery(document).ready(function () {

    aclaraJQuery("[id*=iws_ct_wrapper-]").each(function () {

        var id = aclaraJQuery(this).attr('id').split("-");
        var instance = id[1];
        contentFunctions.InitializeWidget(instance);

    });

});

var contentFunctions = new function () {

    var instances = [];
    var iws_ct;
    var instance;

    this.InitializeWidget = function (i) {

        instance = i;

        aclaraJQuery("#iws_ct_wrapper").hide();

        aclaraJQuery("#iws_ct_loader" + "-" + instance).accesibleloader({
            imagePath: ResolveURL("~/content/images/loading.gif"),
            loadingMessage: iws_loading_text
        });

        contentFunctions.GetData(false);

        iws_ct = contentFunctions.GetInstance(instance);

        //iws_ct = null;  //uncomment to test fatal error

        //iws_ct.NoContent = true; //uncomment to test no content
        //iws_ct.NoContentMessage = "Sorry no content"; //uncomment to test no no content

        if (iws_ct === null || iws_ct === undefined) {
            contentFunctions.ShowError();
        }
        else if (iws_ct.NoContent === true) {
            contentFunctions.HideContentShowMessage(iws_ct.NoContentMessage);
        }
        else {

            var template = aclaraJQuery.templates("#tmpl_ct_default" + "-" + instance);
            aclaraJQuery("#iws_ct_main" + "-" + instance).html(template.render(iws_ct));

            if (iws_ct.ShowIntro == "true")
            {
                var converter = new showdown.Converter({ tables: true });
                iws_ct.IntroText = converter.makeHtml(iws_ct.IntroText);
            }
            else
            {
                aclaraJQuery("#iws_ct_intro_text" + "-" + instance).remove();
            }

            //iws_ct.ShowTitle = "true";
            if (iws_ct.ShowTitle != "true") {
                aclaraJQuery("#iws_ct_title" + "-" + instance).remove();
            }

            ////iws_ct.ShowSubTitle = "true";
            if (iws_ct.ShowSubTitle != "true") {
                aclaraJQuery("#iws_ct_subtitle" + "-" + instance).remove();
            }

            if (iws_ct.ShowFooterText == "false") {
                aclaraJQuery("#iws_ct_footertext" + "-" + instance).remove();
            }

            if (iws_ct.ShowFooterLink == "false") {
                aclaraJQuery("#iws_ct_footerlink" + "-" + instance).remove();
            }

            contentFunctions.ChangeContent();

            aclaraJQuery(window).resize(function () {
                contentFunctions.ChangeContent();
            });

        }

        contentFunctions.HideLoader();

    };

    this.ChangeContent = function () {

        var converter = new showdown.Converter({ tables: true });
        for (var i = 0; i < instances.length; i++) {

            iws_ct = contentFunctions.GetInstance(instances[i].instance);

            instance = instances[i].instance;

            aclaraJQuery("#iws_ct_content" + "-" + instance).empty();

            if (aclaraJQuery("#iws_ct_wrapper" + "-" + instance).width() > iws_ct.ImageLowBreakPoint && aclaraJQuery("#iws_ct_wrapper" + "-" + instance).width() <= iws_ct.ImageHighBreakPoint) {
                aclaraJQuery("#iws_ct_content" + "-" + instance).html(converter.makeHtml(iws_ct.MediumText));
            }
            else if (aclaraJQuery("#iws_ct_wrapper" + "-" + instance).width() <= iws_ct.ImageLowBreakPoint) {
                aclaraJQuery("#iws_ct_content" + "-" + instance).html(converter.makeHtml(iws_ct.ShortText));
            }
            else {
                aclaraJQuery("#iws_ct_content" + "-" + instance).html(converter.makeHtml(iws_ct.LongText));
            }

            aclaraJQuery("#iws_ct_main" + "-" + instance + " table").wrapAll("<div class=\"table-responsive\"></div>").addClass("table table-striped");
            aclaraJQuery("#iws_ct_main" + "-" + instance + " img").addClass("img-responsive");

        }

    };

    this.GetInstance = function () {

        for (var i = 0; i < instances.length; i++) {
            if (instances[i].instance == instance) {
                return instances[i].data;
            }
        }
    };

    this.GetData = function (async) {
        var data = {
            "tabKey": tabKey,
            "parameters": modParams,
            "instance": instance
        };

        aclaraJQuery.ajax({
            url: ResolveURL("~/Content/Get"),
            type: "POST",
            async: async,
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                instances.push({ instance: instance, data: JSON.parse(data) });
                //iws_ct = JSON.parse(data);
            }
        }).fail(function (jqXHR, textStatus) {
            if (jqXHR.status === 0 || jqXHR.readyState === 0) {
                return;
            }
            else {
                contentFunctions.ShowError();
            }
        });
    };

    this.ShowError = function () {
        var template = aclaraJQuery.templates("#tmpl_ct_fatal_error" + "-" + instance);
        var error = {};
        error.Message = iws_widget_error;
        aclaraJQuery("#iws_ct_main" + "-" + instance).html(template.render(error));
        aclaraJQuery.fn.accesibleloader.unloadImage("iws_ct_wrapper" + "-" + instance, "iws_ct_loader" + "-" + instance);
        aclaraJQuery("#iws_ct_wrapper" + "-" + instance).show();
    };

    this.HideContentShowMessage = function (msg) {
        var template = aclaraJQuery.templates("#tmpl_ct_no_data" + "-" + "-" + instance);
        var noData = {};
        if (iws_ct.ShowTitle == "true") {
            noData.Title = iws_ct.Title;
            noData.ShowTitle = true;
        }
        noData.Message = msg;
        aclaraJQuery("#iws_ct_main" + "-" + instance).html(template.render(noData));
        contentFunctions.HideLoader();
    };

    this.HideLoader = function () {
        aclaraJQuery("#iws_ct_wrapper" + "-" + instance).show();
        aclaraJQuery.fn.accesibleloader.unloadImage("iws_ct_wrapper" + "-" + instance, "iws_ct_loader" + "-" + instance);
    };

};