﻿var iws_ap;

aclaraJQuery(document).ready(function () {
    planFunctions.InitializeWidget();
});

var planFunctions = new function () {

    this.InitializeWidget = function () {

        aclaraJQuery("#iws_ap_wrapper").hide();

        iws_ap_current_tab = parseInt(0);
        iws_ap_current_sort = parseInt(0);

        aclaraJQuery("#iws_ap_loader").accesibleloader({
            imagePath: ResolveURL("~/content/images/loading.gif"),
            loadingMessage: iws_loading_text
        });

        planFunctions.GetData(false);

        IframeExternalUrl(aclaraJQuery("[class*=container]"));
    }

    this.GetData = function () {

        var data = {
            "tabKey": tabKey,
            "parameters": modParams
        }

        aclaraJQuery.ajax({
            url: ResolveURL("~/ActionPlan/GetActions"),
            type: "POST",
            async: true,
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {

                iws_ap = JSON.parse(data);

                //iws_ap = null;

                if (iws_ap === null) {
                    planFunctions.ShowError();
                }
                else {

                    if (iws_ap.ShowIntro == "true") {
                        var converter = new showdown.Converter();
                        iws_ap.IntroText = converter.makeHtml(iws_ap.IntroText);
                    }
                    else {
                        aclaraJQuery("#iws_ap_intro_text").remove();
                    }

                    var template = aclaraJQuery.templates("#tmpl_ap_default");
                    aclaraJQuery("#iws_ap_main").html(template.render(iws_ap));

                    iws_ap_max_actions = parseInt(iws_ap.RowCount);

                    if (iws_ap.ShowTitle != "true") {
                        aclaraJQuery("#iws_ap_title").remove();
                    }

                    if (iws_ap.Subtitle != "true") {
                        aclaraJQuery("#iws_ap_subtitle").remove();
                    }

                    if (iws_ap.ShowFooterText != "true") {
                        aclaraJQuery("#iws_ap_footertext").remove();
                    }

                    if (iws_ap.ShowFooterLink != "true") {
                        aclaraJQuery("#iws_ap_footerlink").remove();
                    } else {
                        if (iws_ap.FooterLink.toLowerCase().indexOf("type=pm") > 0) {
                            aclaraJQuery("#iws_ap_footerlink a").addClass("iframe-external-url").attr("href", iws_ap.FooterLink.split("?type=pm")[0]);
                        }
                    }

                    iws_ap_current_sort = parseInt(1);

                    planFunctions.Sort();

                    planFunctions.BuildInPlanActionList();
                    planFunctions.BuildCompletedActionList();
                    planFunctions.BindDetailsLinks();

                    aclaraJQuery("#iws_ap_wrapper").show();
                    aclaraJQuery.fn.accesibleloader.unloadImage("iws_ap_wrapper", "iws_ap_loader");

                }
            }
        }).fail(function (jqXHR, textStatus) {
            if (jqXHR.status === 0 || jqXHR.readyState === 0) {
                return;
            }
            else {
                planFunctions.ShowError();
            }
        });

    }

    this.Compare = function (prop) {
        return function (a, b) {
            return b[prop] - a[prop];
        }
    }

    this.Sort = function () {

        iws_ap.Tabs[0].Actions = iws_ap.Tabs[0].Actions.sort(this.Compare('UpfrontCost')).sort(this.Compare('AnnualSavingsEstimate'));

    }

    this.BuildInPlanActionList = function () {
        var lis = "";
        var actions = iws_ap.Tabs[0].Actions;
        aclaraJQuery("#iws_ap_ip_actions").empty();
        var inPlanCount = 0;

        template = aclaraJQuery.templates("#tmpl_ap_ip_action");

        var opts = iws_ap.CompleteOptions.split(",");

        for (var i = 0; i < actions.length; i++) {
            if (actions[i].Plan.Status == "selected" || actions[i].Plan.Status == "NA") {
                inPlanCount += 1;
                actions[i].ButtonClass = "primary-btn";
                actions[i].Status = actions[i].Plan.Status;
                actions[i].ID = i;
                actions[i].NotApplicableMessage = iws_ap.NotApplicableMessage;
                actions[i].CostTitle = iws_ap.CostTitle;
                actions[i].SavingsTitle = iws_ap.SavingsTitle;

                if (actions[i].Plan.Status == "selected") {
                    actions[i].Val0 = "completed";
                    actions[i].Text0 = opts[0];

                    actions[i].Val1 = "cancelled";
                    actions[i].Text1 = opts[1];
                    actions[i].SelectedText = iws_ap.Complete;
                } else {
                    actions[i].Val0 = "cancelled";
                    actions[i].Text0 = opts[1];
                    actions[i].SelectedText = opts[1];
                }

                lis += template.render(actions[i]);

            }
        }

        aclaraJQuery("#iws_ap_ip_actions").append("<h5 class=\"lead\">" + iws_ap.ToDoTitle + " (" + inPlanCount + ")</h5>");

        if (inPlanCount > 0) {

            aclaraJQuery("#iws_ap_ip_actions").append(lis);
            planFunctions.BindDetailsLinks();

        }
        else {
            aclaraJQuery("#iws_ap_ip_actions").append(iws_ap.NoActionsMessage + " <a href=\"" + iws_ap.WaysToSaveLink + "\">" + iws_ap.WaysToSaveLinkText + "</a>");
        }

        aclaraJQuery("[id*='iws_ap_btn_']").each(function () {
            planFunctions.ProcessDDLClicks(this);
        });

    }

    this.ProcessDDLClicks = function (id) {
        aclaraJQuery(id).off().click(function () {
            var key = aclaraJQuery(id).attr('id').split("-")[1];
            var status = aclaraJQuery(id).attr('val');
            planFunctions.UpdateActionStatus(key, status);

            planFunctions.BuildInPlanActionList();
            planFunctions.BuildCompletedActionList();
        });
    }

    this.BuildCompletedActionList = function () {
        var lis = "";
        var actions = iws_ap.Tabs[0].Actions;
        aclaraJQuery("#iws_ap_cpl_actions").empty();
        var inPlanCount = 0;

        template = aclaraJQuery.templates("#tmpl_ap_ip_action");

        var opts = iws_ap.MoveOptions.split(",");

        for (var i = 0; i < actions.length; i++) {
            if (actions[i].Plan.Status == "completed") {
                inPlanCount += 1;
                actions[i].ButtonClass = "default-btn";
                actions[i].Status = "completed";
                actions[i].ID = i;
                actions[i].NotApplicableMessage = iws_ap.NotApplicableMessage;
                actions[i].CostTitle = iws_ap.CostTitle;
                actions[i].SavingsTitle = iws_ap.SavingsTitle;

                actions[i].Val0 = "selected";
                actions[i].Text0 = opts[0];

                actions[i].Val1 = "cancelled";
                actions[i].Text1 = opts[1];
                actions[i].SelectedText = iws_ap.Move;
                lis += template.render(actions[i]);

            }
        }

        aclaraJQuery("#iws_ap_cpl_actions").append("<h5 class=\"lead\">" + iws_ap.CompletedTitle + " (" + inPlanCount + ")</h5>");

        if (inPlanCount > 0) {

            aclaraJQuery("#iws_ap_cpl_actions").append(lis);
            planFunctions.BindDetailsLinks();

        }

        aclaraJQuery("[id*='iws_ap_btn_']").each(function () {
            planFunctions.ProcessDDLClicks(this);
        });

    }

    this.BindDetailsLinks = function () {

        aclaraJQuery("[id*='-ap_ai_img']").each(function () {
            var key = aclaraJQuery(this).attr('id').split("-")[0];
            aclaraJQuery(this).off().click(function () {
                var actions = iws_ap.Tabs[0].Actions;
                for (var i = 0; i < actions.length; i++) {
                    if (actions[i].Key == key) {
                        aclaraJQuery("#iws_ap_wrapper").hide();
                        var acts = actions[i].RecommendedActions;
                        acts.push(actions[i]);
                        actionDetailFunctions.InitializeWidget("iws_ap", 0, actions[i].Key, acts);
                    }
                };
            });
        });

        aclaraJQuery("[id*='-ap_ai_title']").each(function () {
            var key = aclaraJQuery(this).attr('id').split("-")[0];
            aclaraJQuery(this).off().click(function () {
                var actions = iws_ap.Tabs[0].Actions;
                for (var i = 0; i < actions.length; i++) {
                    if (actions[i].Key == key) {
                        aclaraJQuery("#iws_ap_wrapper").hide();
                        var acts = actions[i].RecommendedActions;
                        acts.push(actions[i]);
                        actionDetailFunctions.InitializeWidget("iws_ap", 0, actions[i].Key, acts);
                    }
                };
            });
        });

    }

    this.UpdateActionStatus = function (key, status) {

        var Plan = { Key: key, Status: status };
        var actions = iws_ap.Tabs[0].Actions;

        var ei = [];
        ei.push({ key: "TabId", value: tabKey });

        for (var i = 0; i < actions.length; i++) {
            if (actions[i].Key == key) {
                if (actions[i].Plan != null) {
                    actions[i].Plan.Status = status;
                }
                else {
                    actions[i].Plan = Plan;
                }

                ei.push({ key: "ActionId", value: actions[i].Key });
                ei.push({ key: "EstimatedSavings", value: actions[i].AnnualSavingsEstimate });
                ei.push({ key: "EstimatedCost", value: actions[i].UpfrontCost });
                ei.push({ key: "EstimatedRebate", value: actions[i].RebateAmount });

                break;
            }
        }

        var data;
        if (status == "selected") {
            data = {
                "eventInfo": ei,
                "parameters": modParams,
                "eventType": "Add Action"
            }

        }
        else if (status == "completed") {
            data = {
                "eventInfo": ei,
                "parameters": modParams,
                "eventType": "Complete Action"
            }
        }
        else if (status == "cancelled") {
            data = {
                "eventInfo": ei,
                "parameters": modParams,
                "eventType": "Remove Action"
            }
        }

        PostEvents(data);

        var dat = {
            "plan": [{ Key: key, Status: status }],
            "parameters": modParams
        }

        aclaraJQuery.ajax({
            url: ResolveURL("~/ActionPlan/PostActions"),
            type: "POST",
            data: JSON.stringify(dat),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function () {
                if (typeof goalFunctions != "undefined" && status != "completed") {
                    goalFunctions.InitializeWidget();
                }
            },
            error: function () {

            }
        });

    }

    this.ShowError = function () {
        var template = aclaraJQuery.templates("#tmpl_ap_fatal_error");
        var error = {};
        error.Message = iws_widget_error;
        aclaraJQuery("#iws_ap_main").html(template.render(error));
        aclaraJQuery.fn.accesibleloader.unloadImage("iws_ap_wrapper", "iws_ap_loader");
        aclaraJQuery("#iws_ap_wrapper").show();
    };
}






