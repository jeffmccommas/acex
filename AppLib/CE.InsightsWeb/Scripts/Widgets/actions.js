﻿var iws_plan;
var iws_as;
var iws_as_current_page;
aclaraJQuery(document).ready(function () {
    waysToSaveFunctions.InitializeWidget();
});



var waysToSaveFunctions = new function () {

    var iws_as_update;
    var iws_as_current_tab;
    var iws_as_current_sort;
    var iws_as_max_actions;
    var iws_as_total_pages;
    var iws_dl_highlight;

    this.InitializeWidget = function () {

        aclaraJQuery("#iws_as_wrapper").hide();
        aclaraJQuery("#iws_ad_loader").hide();

        iws_as_current_tab = parseInt(0);
        iws_as_current_sort = parseInt(0);

        aclaraJQuery("#iws_as_loader").accesibleloader({
            imagePath: ResolveURL("~/content/images/loading.gif"),
            loadingMessage: iws_loading_text
        });

        waysToSaveFunctions.GetData();
        IframeExternalUrl(aclaraJQuery("[class*=container]"));
    };

    this.GetData = function () {

        var data = {
            "tabKey": tabKey,
            "parameters": modParams
        };

        aclaraJQuery.ajax({
            url: ResolveURL("~/Actions/GetActions"),
            type: "POST",
            async: true,
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                iws_as = JSON.parse(data);
                //iws_as = null;
                if (iws_as === null || iws_as === undefined) {
                    waysToSaveFunctions.ShowError();
                }
                else {
                    var template = aclaraJQuery.templates("#tmpl_as_default");
                    aclaraJQuery("#iws_as_main").empty();
                    aclaraJQuery("#iws_as_main").replaceWith(template.render(iws_as));
                    waysToSaveFunctions.UpdatePlanCounts();
                    if (waysToSaveFunctions.DeepLink()) {
                        waysToSaveFunctions.ShowList();
                    }
                }

            }
        }).fail(function (jqXHR, textStatus) {
            if (jqXHR.status === 0 || jqXHR.readyState === 0) {
                return;
            }
            else {
                waysToSaveFunctions.ShowError();
            }
        });

    };


    this.Compare = function (prop) {
        return function (a, b) {
            //return b[prop] - a[prop];
            return (a[prop] < b[prop]) ? 1 : (a[prop] > b[prop]) ? -1 : 0;
        };
    };

    this.CompareAsc = function (prop) {
        return function (a, b) {
            //return a[prop] - b[prop];
            return (b[prop] < a[prop]) ? 1 : (b[prop] > a[prop]) ? -1 : 0;
        };
    };

    this.Sort = function () {
        var actions = iws_as.Tabs[iws_as_current_tab].Actions;
        if (iws_as_current_sort == "0") {
            actions.sort(this.CompareAsc('ActionPriority'));
        }
        else if (iws_as_current_sort == "1") {
            actions.sort(dynamicSortMultiple("AnnualSavingsEstimate-Desc", "UpfrontCost-Asc"));
        }
        else if (iws_as_current_sort == "2") {
            actions.sort(this.CompareAsc('Difficulty'));
        }

        iws_as.Tabs[iws_as_current_tab].Actions = actions;
    };

    this.BuildSortDropdown = function () {

        var sort = {};
        template = aclaraJQuery.templates("#tmpl_as_sort");

        sort.SortLabel = iws_as.SortLabel;
        sort.SortByRecommended = iws_as.SortByRecommended;
        sort.SortBySavings = iws_as.SortBySavings;
        sort.SortByEasiest = iws_as.SortByEasiest;
        sort.isIos = useRegularDropdown();
        //sort.isIos = true;
        if (iws_as.DefaultSort == "recommended") {
            sort.DefaultSortText = iws_as.SortByRecommended;
        }
        else if (iws_as.DefaultSort == "savings") {
            sort.DefaultSortText = iws_as.SortBySavings;
        }
        else if (iws_as.DefaultSort == "easiest") {
            sort.DefaultSortText = iws_as.SortByEasiest;
        }

        aclaraJQuery("#iws_as_header").addClass("sort-enabled");
        aclaraJQuery("#iws_as_header").empty().prepend(template.render(sort));

        if (!sort.isIos) {
            aclaraJQuery("[id*='_as_ddl_sort_']").click(function () {
                iws_as_current_sort = aclaraJQuery(this).attr("val");
                aclaraJQuery("#iws_as_ddl_selection").text(aclaraJQuery(this).text());
                waysToSaveFunctions.Sort();
                waysToSaveFunctions.BuildActionList(0);
            });
        }

        if (sort.isIos) {
            aclaraJQuery('#iws_as_ddl_select').on('change', function () {
                if (this.value != -1) {
                    iws_as_current_sort = this.value;
                    waysToSaveFunctions.Sort();
                    waysToSaveFunctions.BuildActionList(0);
                }
            })
        }


    };

    this.BuildPager = function () {

        aclaraJQuery("#iws_as_pager").empty();
        var actions = iws_as.Tabs[iws_as_current_tab].Actions;

        if (actions.length > iws_as_max_actions) {

            aclaraJQuery("<li id=\"iws_as_page_prev\" ><a onclick=\"return false;\" class=\"arrow\" href=\"#\"><img src=\"~/../content/images/icon-page-left.png\" class=\"bill_left_nav\" alt=\"previous\" /></a></li>").appendTo("#iws_as_pager");

            var perPage = iws_as_max_actions;
            var numItems = actions.length;
            iws_as_total_pages = Math.ceil(numItems / perPage);
            iws_as_current_page = 1;
            var curr = 1;

            while (iws_as_total_pages >= curr) {
                aclaraJQuery("<li class=\"pn\"><a onclick=\"return false;\" id=\"iws_as_pagerlink-" + curr + "\" href=\"#\">" + (curr) + "</a></li>").appendTo("#iws_as_pager");
                curr++;
            }

            aclaraJQuery("<li id=\"iws_as_page_next\" ><a onclick=\"return false;\" class=\"arrow\" href=\"#\"><img src=\"~/../content/images/icon-page-right.png\" class=\"bill_right_nav\" alt=\"next\" /></a></li>").appendTo("#iws_as_pager");

            aclaraJQuery(".pn.active").removeClass("active");
            aclaraJQuery("#iws_as_page_prev").addClass("disabled");
            aclaraJQuery("#iws_as_pagerlink-1").parent().addClass("active");

            aclaraJQuery("#iws_as_page_next").click(function () {
                if (iws_as_current_page < (iws_as_total_pages)) {
                    aclaraJQuery("#iws_as_page_prev").removeClass("disabled");
                    iws_as_current_page += 1;

                    aclaraJQuery(".pn.active").removeClass("active");
                    aclaraJQuery("#iws_as_pagerlink-" + iws_as_current_page).parent().addClass("active");

                    waysToSaveFunctions.BuildActionList(iws_as_current_page - 1);
                }

                if (iws_as_current_page == iws_as_total_pages) {
                    aclaraJQuery("#iws_as_page_next").addClass("disabled");
                }

            });

            aclaraJQuery("#iws_as_page_prev").click(function () {
                if (iws_as_current_page > 1) {
                    aclaraJQuery("#iws_as_page_next").removeClass("disabled");
                    iws_as_current_page -= 1;

                    aclaraJQuery(".pn.active").removeClass("active");
                    aclaraJQuery("#iws_as_pagerlink-" + iws_as_current_page).parent().addClass("active");

                    waysToSaveFunctions.BuildActionList(iws_as_current_page - 1);
                }

                if (iws_as_current_page == 1) {
                    aclaraJQuery("#iws_as_page_prev").addClass("disabled");
                }

            });

            aclaraJQuery("[id*='iws_as_pagerlink-']").each(function () {
                aclaraJQuery(this).click(function (e) {
                    e.preventDefault();

                    var pageId = aclaraJQuery(this).attr('id').split("-")[1];
                    iws_as_current_page = parseInt(pageId);

                    aclaraJQuery(".pn.active").removeClass("active");
                    aclaraJQuery(this).parent().addClass("active");

                    if (iws_as_current_page === 1) {
                        aclaraJQuery("#iws_as_page_prev").addClass("disabled");
                        aclaraJQuery("#iws_as_page_next").removeClass("disabled");
                    }
                    else {
                        aclaraJQuery("#iws_as_page_prev").removeClass("disabled");
                    }

                    if (iws_as_current_page == iws_as_total_pages) {
                        aclaraJQuery("#iws_as_page_next").addClass("disabled");
                        aclaraJQuery("#iws_as_page_prev").removeClass("disabled");
                    }
                    else {
                        aclaraJQuery("#iws_as_page_next").removeClass("disabled");
                    }

                    waysToSaveFunctions.BuildActionList(pageId - 1);
                });
            });
        }



    };

    this.BuildTabs = function () {
        var lis = "";
        var tabs = iws_as.Tabs;
        template = aclaraJQuery.templates("#tmpl_as_tab_li");

        var addHighlightClass = false;
        var tabIndexForHightlight;
        for (var i = 0; i < tabs.length; i++) {
            if (iws_dl_highlight === true && tabs[i].Key == iws_dl_wt) {
                addHighlightClass = true;
                tabIndexForHightlight = i;
                iws_as_current_tab = i;
            }
            tabs[i].TabIndex = i;
            lis += template.render(tabs[i]);
        }

        aclaraJQuery("#iws_as_tabs").empty().append(lis);

        if (addHighlightClass) {
            aclaraJQuery("#iws_as_tab-" + tabIndexForHightlight).parent().addClass("deep-link-highlight");
        }
        else {
            aclaraJQuery("#iws_as_tab-0").parent().addClass("active");
        }

    };

    this.BuildActionList = function (page) {

        aclaraJQuery("#iws_as_header").show();
        aclaraJQuery("#iws_as_intro_text").show();
        aclaraJQuery("#iws_as_footer").show();

        var startAt = page * iws_as_max_actions,
            endOn = startAt + iws_as_max_actions;

        var actions = iws_as.Tabs[iws_as_current_tab].Actions.slice(startAt, endOn);
        aclaraJQuery("#iws_as_actions").empty();

        if (actions.length > 0) {
            iws_as_update = actions[0];
            template = aclaraJQuery.templates("#tmpl_as_tab_rec_action");

            for (var i = 0; i < actions.length; i++) {
                actions[i].ID = i;
                actions[i].CostTitle = iws_as.CostTitle;
                actions[i].SavingsTitle = iws_as.SavingsTitle;
                var opts = iws_as.AddToPlanOptions.split(",");
                actions[i].AddButtonText = opts[0];
                actions[i].CompleteButtonText = opts[1];
                aclaraJQuery("#iws_as_actions").append(template.render(actions[i]));
            }
        }
        else {
            aclaraJQuery("#iws_as_header").hide();
            aclaraJQuery("#iws_as_intro_text").hide();
            aclaraJQuery("#iws_as_footer").hide();
            aclaraJQuery("#iws_as_actions").append("<div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>" + iws_as.NoActionsMessage + "</strong></div>");
        }

        aclaraJQuery("[id*='iws_as_btns-']").each(function () {
            var key = aclaraJQuery(this).attr('id').split("-")[1];
            var status = waysToSaveFunctions.GetActionStatus(key);
            waysToSaveFunctions.SetActionButtonsState(key, status);
        });

        aclaraJQuery("[id*='iws_as_btn_add-']").each(function () {
            aclaraJQuery(this).off().click(function () {
                var key = aclaraJQuery(this).attr('id').split("-")[1];
                var status = "selected";
                waysToSaveFunctions.UpdateActionStatus(key, status);
                waysToSaveFunctions.SetActionButtonsState(key, status);
            });
        });

        aclaraJQuery("[id*='iws_as_btn_complete-']").each(function () {
            aclaraJQuery(this).off().click(function () {
                var key = aclaraJQuery(this).attr('id').split("-")[1];
                var status = "completed";
                waysToSaveFunctions.UpdateActionStatus(key, status);
                waysToSaveFunctions.SetActionButtonsState(key, status);
            });
        });

        waysToSaveFunctions.BindDetailsLinks();

    };

    this.BindDetailsLinks = function () {

        aclaraJQuery("[id*='-as_ai_img']").each(function () {
            aclaraJQuery(this).off().click(function () {
                var key = aclaraJQuery(this).attr('id').split("-")[0];
                waysToSaveFunctions.ShowDetails(iws_as_current_tab, key);
            });
        });

        aclaraJQuery("[id*='-as_ai_title']").each(function () {
            aclaraJQuery(this).off().click(function () {
                var key = aclaraJQuery(this).attr('id').split("-")[0];
                waysToSaveFunctions.ShowDetails(iws_as_current_tab, key);
            });
        });

    };

    this.SetActionButtonsState = function (key, status) {
        var key = key.replace(/\./g, '\\.');
        if (status == "selected") {
            aclaraJQuery("#iws_as_btn_add-" + key).replaceWith("<p class=\"action-added-todo\"><img src=\"Content/Images/icon-round-green-check.png\" width=\"19\" height=\"19\" />" + iws_as.AddedToDo + " <a href=\"" + iws_as.ActionPlanLink + "\" >" + iws_as.ActionPlanLinkText + "</a></p>");

            aclaraJQuery("#iws_as_btn_complete-" + key).remove();
        }
        else if (status == "completed") {
            aclaraJQuery("#iws_as_btn_complete-" + key).replaceWith("<p class=\"action-completed\"><img src=\"Content/Images/icon-round-green-check.png\" width=\"19\" height=\"19\" />" + iws_as.Completed + "</p>");

            aclaraJQuery("#iws_as_btn_add-" + key).remove();
        }
        else {
            aclaraJQuery("#iws_as_btn_add-" + key).show();
            aclaraJQuery("#iws_as_btn_complete-" + key).show();
        }
    };

    this.GetActionStatus = function (key) {

        var actions = iws_as.Tabs[iws_as_current_tab].Actions;
        var status = "";
        for (var i = 0; i < actions.length; i++) {
            if (actions[i].Key == key) {
                if (actions[i].Plan !== null) {
                    status = actions[i].Plan.Status;
                }
            }
        }

        return status;
    };

    this.UpdateActionStatus = function (key, status) {

        var Plan = { Key: key, Status: status };
        var actions = iws_as.Tabs[iws_as_current_tab].Actions;

        var ei = [];
        ei.push({ key: "TabId", value: tabKey });

        for (var i = 0; i < actions.length; i++) {
            if (actions[i].Key == key) {
                if (actions[i].Plan !== null) {
                    actions[i].Plan.Status = status;
                }
                else {
                    actions[i].Plan = Plan;
                }

                ei.push({ key: "ActionId", value: actions[i].Key });
                ei.push({ key: "EstimatedSavings", value: actions[i].AnnualSavingsEstimate });
                ei.push({ key: "EstimatedCost", value: actions[i].UpfrontCost });
                ei.push({ key: "EstimatedRebate", value: actions[i].RebateAmount });

                break;
            }
        }
        var data;
        if (status == "selected") {
            data = {
                "eventInfo": ei,
                "parameters": modParams,
                "eventType": "Add Action"
            };
            iws_as.ToDoCount = parseInt(iws_as.ToDoCount) + 1;
        }
        else if (status == "completed") {
            data = {
                "eventInfo": ei,
                "parameters": modParams,
                "eventType": "Complete Action"
            };
            iws_as.CompletedCount = parseInt(iws_as.CompletedCount) + 1;
        }
        else if (status == "cancelled") {
            data = {
                "eventInfo": ei,
                "parameters": modParams,
                "eventType": "Remove Action"
            };
        }

        PostEvents(data);

        var dat = {
            "plan": [{ Key: key, Status: status }],
            "parameters": modParams
        };

        aclaraJQuery.ajax({
            url: ResolveURL("~/Actions/PostActions"),
            type: "POST",
            data: JSON.stringify(dat),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                waysToSaveFunctions.UpdatePlanCounts();
                if (typeof goalFunctions != "undefined") {
                    goalFunctions.InitializeWidget();
                }
            },
            error: function () {

            }
        });

    };

    this.UpdatePlanCounts = function () {

        var regExp = /{%todocount%}/;

        if (iws_as.ToDoList.length > 0) {
            var matches = regExp.exec(iws_as.ToDoList);
            var todo = iws_as.ToDoList.replace(matches[0], iws_as.ToDoCount);
            aclaraJQuery("#iws_as_todolist").text(todo);
        }

        if (iws_as.CompletedList.length > 0) {
            regExp = /{%completedcount%}/;
            matches = regExp.exec(iws_as.CompletedList);
            var completed = iws_as.CompletedList.replace(matches[0], iws_as.CompletedCount);
            aclaraJQuery("#iws_as_completedlist").text(completed);
        }

    };

    this.ShowList = function () {
        iws_as_max_actions = parseInt(iws_as.RowCount);
        //iws_as_max_actions = 6;
        if (iws_as.ShowTitle == "true") {
            aclaraJQuery("#iws_as_title").text(iws_as.Title);
        }
        else {
            aclaraJQuery("#iws_as_title").remove();
        }

        if (iws_as.ShowIntro == "true") {
            var converter = new showdown.Converter();
            aclaraJQuery("#iws_as_intro_text").empty().append(converter.makeHtml(iws_as.IntroText));
        }

        if (iws_as.ShowSort == "true") {
            waysToSaveFunctions.BuildSortDropdown();
        }

        if (iws_as.ShowSubTitle == "true") {
            aclaraJQuery("#iws_as_header").append(iws_as.Subtitle);
        }

        if (iws_as.ShowSubTitle == "true") {
            aclaraJQuery("#iws_as_pop-actions").append(iws_as.Subtitle);
        }

        if (iws_as.ShowFooterText == "false") {
            aclaraJQuery("#iws_as_footertext").remove();
        }
        else {
            aclaraJQuery("#iws_as_footertext").html(iws_as.Footer);
        }

        if (iws_as.ShowFooterLink == "false") {
            aclaraJQuery("#iws_as_footerlink").remove();
        }
        else {
            aclaraJQuery("#iws_as_footerlink a").attr("href", iws_as.FooterLink).text(iws_as.FooterLinkText);
            if (iws_as.FooterLink.toLowerCase().indexOf("type=pm") > 0) {
                aclaraJQuery("#iws_as_footerlink a").addClass("iframe-external-url").attr("href", iws_as.FooterLink.split("?type=pm")[0]);
            }
        }

        if (iws_as.ShowPagination == "true") {
            waysToSaveFunctions.BuildPager();
        }

        if (iws_as.DefaultSort == "recommended") {
            iws_as_current_sort = parseInt(0);
        }
        else if (iws_as.DefaultSort == "savings") {
            iws_as_current_sort = parseInt(1);
        }
        else if (iws_as.DefaultSort == "easiest") {
            iws_as_current_sort = parseInt(2);
        }

        waysToSaveFunctions.Sort();

        if (iws_as.ShowTabs == "true") {
            waysToSaveFunctions.BuildTabs();
        }
        else {
            aclaraJQuery("#iws_as_tabs_wrapper").hide();
            aclaraJQuery("#iws_as_actions").addClass("padding-top");
        }

        waysToSaveFunctions.BuildActionList(0);

        aclaraJQuery("[id*='iws_as_tab-']").each(function () {
            aclaraJQuery(this).click(function (e) {
                e.preventDefault();
                var id = aclaraJQuery(this).attr('id').split("-");
                iws_as_current_tab = parseInt(id[1]);
                waysToSaveFunctions.Sort();
                waysToSaveFunctions.BuildActionList(0);
                waysToSaveFunctions.BuildPager();

                if (iws_dl_highlight === false) {
                    aclaraJQuery("[id*='iws_as_tab-']").parent().removeClass("active");
                    aclaraJQuery(this).parent().addClass("active");
                }
            });
        });

        aclaraJQuery.fn.accesibleloader.unloadImage("iws_as_wrapper", "iws_as_loader");

        if (iws_dl_highlight === true) {
            aclaraJQuery("#iws_as_tab-" + iws_as_current_tab).click();
            waysToSaveFunctions.HighLightElement();
        }
    };

    this.ShowDetails = function (tab, key) {

        var foundAction = false;
        if (tab == -1) {
            for (var t = 0; t < iws_as.Tabs.length; t++) {
                var actions = iws_as.Tabs[t].Actions;
                for (var i = 0; i < actions.length; i++) {
                    if (actions[i].Key == key) {
                        aclaraJQuery("#iws_as_wrapper").hide();
                        var acts = actions[i].RecommendedActions;
                        acts.push(actions[i]);
                        actionDetailFunctions.InitializeWidget("iws_as", iws_as_current_tab, actions[i].Key, acts, true);
                        foundAction = true;
                    }
                }
            }
        }
        else {
            var actions = iws_as.Tabs[tab].Actions;
            for (var i = 0; i < actions.length; i++) {
                if (actions[i].Key == key) {
                    aclaraJQuery("#iws_as_wrapper").hide();
                    var acts = actions[i].RecommendedActions;
                    acts.push(actions[i]);
                    actionDetailFunctions.InitializeWidget("iws_as", iws_as_current_tab, actions[i].Key, acts, false);
                    foundAction = true;
                }
            }
        }

        if (foundAction === false) {
            waysToSaveFunctions.ShowList();
        }

    };

    this.DeepLink = function () {

        var keep_current_view = true;
        iws_dl_highlight = false;

        if (iws_dl_w == "actions") {

            if (iws_dl_wst !== null) {
                if (iws_dl_wst == "details") {
                    aclaraJQuery("#iws_as_loader").hide();
                    waysToSaveFunctions.ShowDetails(-1, iws_dl_akey);
                    iws_dl_highlight = true;
                    keep_current_view = false;
                }
                else if (iws_dl_wst == "list") {
                    aclaraJQuery("#iws_as_loader").hide();
                    iws_dl_highlight = true;
                }
            }
        }

        return keep_current_view;
    };

    this.HighLightElement = function () {
        aclaraJQuery(".deep-link-highlight:first").focus();
        aclaraJQuery(".deep-link-highlight").effect("highlight", { color: iws_dl_yfc }, 2000, function () { aclaraJQuery("#iws_as_tab-" + iws_as_current_tab).parent().removeClass("deep-link-highlight").addClass("active") });
        iws_dl_highlight = false;
        waysToSaveFunctions.ResetDeepLinkVariables();
    };

    this.ResetDeepLinkVariables = function () {
        iws_dl_tp = "";
        iws_dl_w = "";
        iws_dl_wt = "";
        iws_dl_ws = "";
        iws_dl_enum = "";
        iws_dl_acid = "";
        iws_dl_res = "";
        iws_dl_comp = "";
    };

    this.ShowError = function () {
        var template = aclaraJQuery.templates("#tmpl_as_fatal_error");
        var error = {};
        error.Message = iws_widget_error;
        aclaraJQuery("#iws_as_main").html(template.render(error));
        aclaraJQuery.fn.accesibleloader.unloadImage("iws_as_wrapper", "iws_as_loader");
        aclaraJQuery("#iws_as_wrapper").show();
    };

};


