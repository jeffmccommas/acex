/// <reference path="../Jquery/jquery-2.1.0-vsdoc.js" />
var currentHeight = "0";
var newHeight;
var defaultParentUrl = "";
var decryptedUrl;
function initialize() {
    var legacy = false;
    if (legacy) {
        window.setInterval(setNewHeight, 100); // time in MS
    }
    else {
        window.setInterval(setHeight, 100); // time in MS
        window.setInterval(redirectParent, 100); // time in MS
    }
}

//** legacy **//
function setNewHeight() {
    if (window['postMessage']) {
        newHeight = getDocHeight();
        if (newHeight != currentHeight) {
            pm({
                target: window.parent,
                type: 'pgFrame',
                data: newHeight,
                url: getParentUrl()
            });
            currentHeight = newHeight;
        }
    }
}

function getDocHeight() {
    var h = "0";
    var postIdHeight = '';
    aclaraJQuery('[id^=iws_height_wrapper]').each(function () {
        var content = this;
        if ((content.scrollHeight != undefined || content.scrollHeight != null) && (content.scrollHeight != 0)) {
            h = content.scrollHeight;
        } else {
            h = content.offsetHeight;
        }
        var height = parseInt(h) + 60;
        if (aclaraJQuery(this).attr('data-href') !== undefined && aclaraJQuery(this).attr('data-href') !== null) {
            postIdHeight = aclaraJQuery(this).attr('id') + ',' + height + ',' + aclaraJQuery(this).attr('data-href');
        } else {
            postIdHeight = aclaraJQuery(this).attr('id') + ',' + height;
        }
    });
    return postIdHeight;
}
//** end legacy **//

function setHeight() {
    if (window['postMessage']) {

        var newHeight = 0;
        var content = aclaraJQuery('[id^=iws_height_wrapper]')[0];

        if ((content.scrollHeight != undefined || content.scrollHeight != null) && (content.scrollHeight != 0)) {
            newHeight = content.scrollHeight;
        } else {
            newHeight = content.offsetHeight;
        }
        var newHeight = parseInt(newHeight) + 60;

        if (newHeight != currentHeight) {

            var data = {
                'iframeId': content.id,
                'action': 'setHeight',
                'value': newHeight
            };

            pm({
                target: window.parent,
                type: 'pgFrame',
                data: data,
                url: getParentUrl()
            });
            currentHeight = newHeight;
        }
    }
}

function scrollParent(top) {
    if (window['postMessage']) {
        var data = {
            'iframeId': aclaraJQuery('[id^=iws_height_wrapper]')[0].id,
            'action': 'scroll',
            'value': top
        };
        pm({
            target: window.parent,
            type: 'pgFrame',
            data: data,
            url: getParentUrl()
        });
    }
}

function redirectParent() {
    var content = aclaraJQuery('[id^=iws_height_wrapper]')[0];
    if (aclaraJQuery(content).attr('data-href') !== undefined && aclaraJQuery(content).attr('data-href') !== null && aclaraJQuery(content).attr('data-href') !== "") {
        if (window['postMessage']) {
            var data = {
                'iframeId': content.id,
                'action': 'redirect',
                'value': aclaraJQuery(content).attr('data-href')
            };
            aclaraJQuery(content).attr("data-href", "");
            pm({
                target: window.parent,
                type: 'pgFrame',
                data: data,
                url: getParentUrl()
            });
        }
    }
}

function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func;
    }
    else {
        window.onload = function () {
            if (oldonload) {
                oldonload();
            }
            func();
        }
    }
}

function getParentUrl() {
    var parentUrl = "";
    var url = window.location.href;
    var idx = url.indexOf("parentUrl=");
    if (idx > 0) {
        parentUrl = url.substring(idx + 10);
    }
    if (parentUrl.length > 0) {
        parentUrl = 'http' + parentUrl;
        aclaraJQuery.cookie("aclParentUrl", parentUrl);
    }
    return parentUrl;
}

aclaraJQuery(document).ready(function () {
    addLoadEvent(initialize);
});
