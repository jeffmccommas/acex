﻿var aclaraJQuery = $.noConflict();

function ResolveURL(relative) {
    var resolved = relative;
    if (relative.charAt(0) == '~')
        resolved = relativeRoot + relative.substring(2);
    return resolved;
}

function scrollPage(y) {
    if (typeof iws_widget_id != 'undefined' && iws_widget_id != null && iws_widget_id != "") {
        scrollParent(y);
    }
    else {
        window.scrollTo(0, y + window.pageYOffset);
    }
}

function RedirectToErrorPage(redirectUrl) {
    window.location = redirectUrl;
}

aclaraJQuery(document).ready(function () {

    aclaraJQuery.ajaxSettings.traditional = true;
    if (typeof iws_widget_id != 'undefined') {
        if (iws_widget_id != null) {
            aclaraJQuery('.iframe-resizer').attr('id', "iws_height_wrapper_" + iws_widget_id);
        }
    }

    if (aclaraJQuery('.high-contrast-test').css('background-image') === 'none') {
        aclaraJQuery('html').addClass('high-contrast-mode');
    }

    if (typeof browser !== typeof undefined && browser == '-chrome') {
        var attr = aclaraJQuery('html').attr('hc');
        if (typeof attr !== typeof undefined && attr !== false) {
            aclaraJQuery('html').addClass('high-contrast-mode');
        }
    }

    if (typeof browser != 'undefined') {
        if (browser != null) {
            if (browser.length != 0) {
                var support = browser.split(";");
                if (support[1] == "partial") {
                    aclaraJQuery('#partialBrowserSupport').html(partial_browser);
                    aclaraJQuery('#partialBrowserSupport').show();
                }
            }
        }
    }

    if (typeof angWidgets != 'undefined') {
        if (angWidgets !== null) {
            System.import('App/app.module.js').then(window.onload).catch(console.error.bind(console));
        }
    }
});

function IframeExternalUrl(thisObj, angWidget) {
    if (angWidget !== true) {
        thisObj.find("[class*='iframe-external-url']")
            .on("click",
            function () {
                IframeResizerHref(this);
            });
    } else {
        IframeResizerHref(thisObj);
    }
}

function IframeResizerHref(thisObj) {
    var link = aclaraJQuery(thisObj).attr('href');
    if ((link.indexOf("type=pm") > 0) || (link.indexOf("type=external") > 0)) {
        link = link.split("?type=")[0];
    }
    aclaraJQuery(".iframe-resizer").attr('data-href', link);
}



function useRegularDropdown() {
    var useReg = false;
    if ((/iPad|iPhone|iPod/.test(navigator.userAgent) || (navigator.userAgent.indexOf("Safari") > -1) && navigator.userAgent.indexOf('Chrome') == -1)) {
        useReg = true;
    }
    return useReg;
}


function StripHTMLAndTrim(text) {
    var htmlStriper = /<(?:.|\s)*?>/g;
    text = text.replace(htmlStriper, " ");
    while (text.indexOf("  ") >= 0) {
        text = text.replace("  ", " ");
    }
    return text.replace(/^\s+|\s+aclaraJQuery/g, "");
}

function htmlEscape(str) {
    return String(str)
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/ /g, '+');
}

function GetCurrentDateTime() {
    var dt = new Date();
    return (dt.getMonth() + 1) + '-' + dt.getDate() + '-' + dt.getFullYear() + 't' + dt.getHours() + ':' + dt.getMinutes() + ':' + dt.getSeconds();
}

function PostEvents(data) {
    aclaraJQuery.ajax({
        url: ResolveURL("~/EventTracking/PostEvents"),
        type: "POST",
        async: true,
        data: JSON.stringify(data),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            return data;
        },
        error: function () {
        }
    });
}

function dynamicSort(property, order) {
    return function (obj1, obj2) {
        if (order == "Asc") {
            return obj1[property] > obj2[property] ? 1
                : obj1[property] < obj2[property] ? -1 : 0;
        }
        else if (order == "Desc") {
            return obj1[property] < obj2[property] ? 1
                : obj1[property] > obj2[property] ? -1 : 0;
        }

    }
}

function dynamicSortMultiple() {
    /*
     * save the arguments object as it will be overwritten
     * note that arguments object is an array-like object
     * consisting of the names of the properties to sort by
     */
    var props = arguments;
    return function (obj1, obj2) {
        var i = 0, result = 0, numberOfProperties = props.length;
        /* try getting a different result from 0 (equal)
         * as long as we have extra properties to compare
         */
        while (result === 0 && i < numberOfProperties) {

            prop = props[i].split("-");
            p = prop[0];
            o = prop[1];
            result = dynamicSort(p, o)(obj1, obj2);
            i++;
        }
        return result;
    }
}


function HighLightElement() {
    aclaraJQuery(".deep-link-highlight:first").focus();
    aclaraJQuery(".deep-link-highlight").effect("highlight", { color: iws_dl_yfc }, 2000, function () { aclaraJQuery("#iws_lp_tab-" + iws_lp_current_tab).parent().removeClass("deep-link-highlight").addClass("active") });
    iws_dl_highlight = false;
    longProfileFunctions.ResetDeepLinkVariables();
}

function ResetDeepLinkVariables() {
    iws_dl_tp = "";
    iws_dl_w = "";
    iws_dl_wt = "";
    iws_dl_ws = "";
    iws_dl_enum = "";
    iws_dl_acid = "";
    iws_dl_res = "";
    iws_dl_comp = "";
}

/*Start Kendo UI PDF Generation logic */

function pdfKendoUIExport(excludeWidgetList, reporttitle, reportnotes, tabKey, isCSR) {
    // get iframe object from html document
    var frmreport = aclaraJQuery("#frmcsrreport");
    var frmdoc = frmreport[0].contentWindow ? frmreport[0].contentWindow.document : frmreport[0].contentDocument;
    var frmbody = aclaraJQuery(frmdoc.body);
    var frmhead = aclaraJQuery(frmdoc.head);
    // Clear old content of iframe document
    frmbody.html('');
    frmhead.html('');
    var divmain = aclaraJQuery('#iws_height_wrapper');
    ResizeChartBeforeReportCreate();
    var divcontent = aclaraJQuery('#iws_height_wrapper').html();

    var pdfStyle = CreatePDFExportStyleOverride();
    // Include Title and Notes into PDF Report
    var reportHeader = '<div class="container"><div id="iws_rc_pagewidget_0" name="reporttitle"><p><h1 class="ReportCreateTitle">' + reporttitle + '</h1></p><p class="ReportCreateTitle">' + reportnotes + '</p> </div></div>';
    var pdftemplate = '<script type="x/kendo-template" id="pdf-page-template"><div class="pdf-page-template"><div class="footer">Page #:pageNum# of #:totalPages# </div></div></script>';
    var contentlink = document.querySelectorAll('link');
    var i;
    for (i = 0; i < contentlink.length; i++) {
        frmhead.append('<link href="' + contentlink[i].href + '" rel="stylesheet" type="text/css">');
    }

    var pdfScript = '<script> var relativeRoot = ""; function ResolveURL(relative) {var resolved = relative;if (relative.charAt(0) == "~") resolved = relativeRoot + relative.substring(2);return resolved;}</script>';

    frmhead.append(pdfStyle);
    frmhead.append(pdfScript);
    frmbody.append("<div id='frm_iws_height_wrapper' border='0px #ff0000 solid;'></div><div id='frmpage' class='page'><div id='widgetDiv' class='subpage'></div></div>");
    var frmhightwrapper = frmbody.find('#frm_iws_height_wrapper');
    var frmpage = frmbody.find('#frmpage');
    var frmdivcontent = frmbody.find('#widgetDiv');
    frmhightwrapper.prepend(reportHeader);
    frmhightwrapper.prepend(pdftemplate);
    frmhightwrapper.append(divcontent);
    frmdivcontent.prepend(pdftemplate);
    MoveWidgetsToIFrame(frmhightwrapper, frmdivcontent, excludeWidgetList);

    setTimeout(function () {
        replaceSVGWithCanvas(frmdivcontent);
        setTimeout(function () {
            ResizeChartAfterReportCreate();
            AddManualPageBreak(frmdivcontent);
            kendo.drawing.drawDOM(frmpage, {
                avoidLinks: true,
                paperSize: "auto",
                forcePageBreak: ".page-break",
                //landscape: false,
                // scale: 0.4,
                template: frmdivcontent.find("#pdf-page-template").html()
                //margin: { left: "1cm", top: "1cm", right: "1cm", bottom: "1cm" }
            })
                .then(function (group) {
                    // Render the result as a PDF file
                    return kendo.drawing.exportPDF(group);
                })
                .done(function (data) {
                    // Post the report create data.
                    result = PostReportCreate(reporttitle, reportnotes, isCSR, data);
                    // Save the PDF file
                    kendo.saveAs({
                        dataURI: data,
                        fileName: reporttitle + ".pdf"
                    });

                });

        }, 1000);
    }, 2000);

    return true;
}


function MoveWidgetsToIFrame(frmhightwrapper, frmdivcontent, excludeWidgetList) {
    var tabName = tabKey;
    var index = tabKey.indexOf('.');
    if (index != -1)
        tabName = tabKey.substr(index + 1);
    for (var widgetindex = 0; widgetindex < iws_pagewidget_count; widgetindex++) {
        var widget = frmhightwrapper.find("#iws_rc_pagewidget_" + widgetindex);
        if (widget != null) {
            var widgetname = widget.attr("name");
            var index = excludeWidgetList.indexOf(tabName + '.' + widgetname);

            if (index == -1 && widgetindex != 2)
            { widget.appendTo(frmdivcontent); }
        }
    }
}

function CreatePDFExportStyleOverride() {
    var pdfStyle = '<style>';
    pdfStyle += '.k-pdf-export #footer, .k-pdf-export #iws_rccsr_wrapper, .k-pdf-export #iws_rc_wrapper, .k-pdf-export #iws_rc_pagewidget_2 ';
    pdfStyle += '{display: none; } ';
    pdfStyle += '.panel.panel-notify {height: auto;} ';
    pdfStyle += '.panel.notification-detail {height: auto;} ';
    pdfStyle += '.panel.actions {height: auto;} ';
    pdfStyle += '.icon-overview:before {margin-left: -7px !important; } ';
    pdfStyle += '.nav-icons .icon:before {margin-left: -7px !important; } ';
    pdfStyle += '.icon-overview:after {margin-right: 7px !important; width: 7px; } ';
    pdfStyle += '.icon-overview p { width: 4px; margin: 0; padding: 0 } ';
    pdfStyle += '.nav-icons .icon:after {margin-right: -7px !important; } ';
    pdfStyle += '.icon-overview { content: "." !important;} ';
    pdfStyle += '.details__headers p, ol li, ul li, ul li a, body, .panel-bill-usage .table a  { font-size: 12px !important;} ';
    pdfStyle += '.detail-featured-text { width: 375px !important;} ';
    pdfStyle += '.brand-logo { display: none;} ';
    pdfStyle += ' #iws_pr_section_questions .panel-body { padding-bottom: 0; padding-top: 0; margin-bottom: 0} ';
    pdfStyle += '.icon__table, .navbar-icons, .nav-modified { display: none !important;} ';
    pdfStyle += '.toggle__btn { display: none;} ';
    pdfStyle += '.actions__container {height: auto;} ';
    pdfStyle += '.actions__message { width: 70% !important} ';
    pdfStyle += '.actions__costSavings { width: 30% !important; position: relative; right: 0} ';
    pdfStyle += '.footerlink a:after, .actions__buttons, .sort-component, i.fa.fa-exchange {display: none;} ';
    pdfStyle += '.panel-footer a:after {display: none;} ';
    pdfStyle += '.actionplan {height: auto;} ';
    pdfStyle += '.actionplan__container {height: auto;} ';
    pdfStyle += '.panel-body-recommended .image-recommended img  {height: auto;} ';
    pdfStyle += '.pdf-page-template > * {position: absolute;left: 20px;right: 20px;font-size: 90%;}';
    pdfStyle += '.pdf-page-template .footer {bottom: 20px;border-top: 0px solid #000;}';
    pdfStyle += '.page { width: 210mm;padding: 5mm;margin: 5mm auto; border: 1px #FFFFFF solid;border-radius: 5px;background: white;box-shadow: 0 0 5px rgba(0, 0, 0, 0.1); }';
    pdfStyle += '.subpage { padding: 0.5cm; border: 0px red solid; outline: 1cm #FFEAEA solid;}';
    pdfStyle += '.icon-flame:before {content: "Gas Commodity"; alt: "Gas Commodity"; color: #000; z-index: 200}';
    pdfStyle += '.icon-bolt:before {content: "Gas Commodity"; alt: "Electricity Commodity"; color: #000; z-index: 200}';
    pdfStyle += '.icon-drop:before {content: "Gas Commodity"; alt: "Water Commodity"; color: #000; z-index: 200}';
    pdfStyle += '.icon__gas, .icon__electric, .icon__water { display: none}';
    pdfStyle += '.icon__gas.icon_focus, .icon__electric.icon_focus, .icon__water.icon_focus { display: inline-block !important}';
    pdfStyle += '.utility-icons a.icon__electric.icon_focus span, .utility-icons a.icon__gas.icon_focus span, .utility-icons a.icon__water.icon_focus span, .utility-icons a.icon__table.icon_focus, .utility-icons a.icon__graph.icon_focus { position: relative !important; overflow: visible; border: none; color: #000}';
    pdfStyle += '.print-only { display: inline-block; color: #000}';
    pdfStyle += ' </style>';

    return pdfStyle;
}


// Function to change an SVG to a Canvas
function replaceSVGWithCanvas(frmdivcontent) {

    var svgElements = frmdivcontent.find('svg');

    //replace all svgs with a temp canvas
    svgElements.each(function () {

        // canvg doesn't cope very well with em font sizes so find the calculated size in pixels and replace it in the element.
        aclaraJQuery.each(aclaraJQuery(this).find('[style*=em]'), function (index, el) {
            aclaraJQuery(this).css('font-size', getStyle(el, 'font-size'));
        });
        var svgCanvas = document.createElement("canvas");
        svgCanvas.className = "svgTempCanvas";
        //convert SVG into a XML string
        var xml = (new XMLSerializer()).serializeToString(this);

        // Removing the name space as IE throws an error
        xml = xml.replace(/xmlns=\"http:\/\/www\.w3\.org\/2000\/svg\"/, '');

        //draw the SVG onto a canvas
        canvg(svgCanvas, xml);

        aclaraJQuery(svgCanvas).insertAfter(this);
        //hide the SVG element
        aclaraJQuery(this).attr('class', 'tempSvgHide');
        aclaraJQuery(this).hide();

    });
}

function ResizeChartBeforeReportCreate() {
    var docwidth = aclaraJQuery('.container').width();
    aclaraJQuery(Highcharts.charts).each(function (i, chart) {
        if (typeof chart != 'undefined') {
            if (chart !== null) {
                chart.oldhasUserSize = chart.hasUserSize;
                chart.resetParams = [chart.chartWidth, chart.chartHeight, false];
                if (docwidth >= 800) {
                    var height = chart.chartHeight;
                    var width = 650;
                    if (chart.chartWidth > 650) {
                        chart.setSize(width, height, false);
                    }
                }
            }
        }
    });
}

function ResizeChartAfterReportCreate() {
    aclaraJQuery(Highcharts.charts).each(function (i, chart) {
        if (typeof chart != 'undefined') {
            if (chart !== null) {
                chart.setSize.apply(chart, chart.resetParams);
                chart.hasUserSize = chart.oldhasUserSize;
            }
        }
    });
}

function AddManualPageBreak(frmdivcontent) {
    var maxpageheight = 850;
    var pagesize = 0;
    var widgetlist = frmdivcontent.find("div[id^='iws_rc_pagewidget_']");

    for (var widgetindex = 0; widgetindex < widgetlist.length; widgetindex++) {
        var widget = aclaraJQuery(widgetlist[widgetindex]);
        var divheight = widget.height();
        var divwidth = widget.width();
        if ((pagesize + divheight) > maxpageheight) {
            if (pagesize < maxpageheight) {
                aclaraJQuery('<div style="height:' + (maxpageheight - pagesize) + 'px;width:' + divwidth + 'px;">&nbsp;</div>').insertBefore(widget);
            }
            aclaraJQuery('<span class="page-break"></span>').insertBefore(widget);
            pagesize = divheight;
        } else {
            pagesize = pagesize + divheight;
        }
    }
}


function PostReportCreate(reporttitle, notes, iscsr, filedata) {
    var data = {
        "tabKey": tabKey,
        "parameters": modParams,
        "file": filedata,
        "title": reporttitle,
        "notes": notes,
        "iscsr": iscsr
    };

    aclaraJQuery.ajax({
        url: ResolveURL("~/ReportCreate/PostReportCreate"),
        type: "POST",
        async: true,
        data: JSON.stringify(data),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (rdata) {
            var responseJson = JSON.parse(rdata);
            if (responseJson.StatusCode != 200) {
                if (iscsr) {
                    aclaraJQuery("#lblPostErrorRCCSR").show();
                } else {
                    aclaraJQuery("#lblPostErrorRC").show();
                }
            } else {
                var ei = [];
                ei.push({ key: "TabId", value: tabKey });
                var data = {
                    "eventInfo": ei,
                    "parameters": modParams,
                    "eventType": "Create Report"
                };

                PostEvents(data);
            }
        }
    }).fail(function (jqXHR, textStatus) {
        if (jqXHR.status === 0 || jqXHR.readyState === 0) {
            return false;
        }
        else {
            return false;
        }
    });

}
/*End  Kendo UI PDF Generation logic*/

function UpdateWeatherLegend(title, chartID) {
    var chart = aclaraJQuery("[id='" + chartID + "']").highcharts();
    chart.yAxis[1].update({
        title: { text: title }
    });
}

function ConsumptionHighLighElement() {
    aclaraJQuery(".deep-link-highlight:first").effect("highlight", { color: iws_dl_yfc }, 2000);
}

function ConsumptionEmptyChart() {
    aclaraJQuery("#iws_cn_chart").empty();
}