"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
require("rxjs/add/operator/map");
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var SubscriptionService = (function () {
    function SubscriptionService(http) {
        this.http = http;
    }
    SubscriptionService.prototype.getSubscriptions = function (tabKey, params) {
        try {
            return this.http.get("./Subscription/Get?tabkey=" + tabKey + "&parameters=" + window.btoa(params)).map(function (res) { return res.json(); });
        }
        catch (ex) {
            throw (ex);
        }
    };
    SubscriptionService.prototype.postSubscriptions = function (data) {
        var headers = new http_1.Headers();
        headers.append("Content-Type", "application/json");
        return this.http.post("./Subscription/Post", JSON.stringify(data), { headers: headers }).map(function (res) { return res.json(); });
    };
    return SubscriptionService;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], SubscriptionService.prototype, "res", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], SubscriptionService.prototype, "fromUnitTest", void 0);
SubscriptionService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], SubscriptionService);
exports.SubscriptionService = SubscriptionService;
//# sourceMappingURL=subscription.service.js.map