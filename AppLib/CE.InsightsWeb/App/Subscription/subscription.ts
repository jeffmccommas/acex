﻿import {Component, ElementRef, OnInit} from "@angular/core";
import {SubscriptionService} from "./subscription.service";
import {AppService, IAppParams, IDeepLinkVariables} from "../app.service";

@Component({
    selector: 'subscription',
    templateUrl: 'App/Subscription/subscription.html',
    providers: [SubscriptionService, AppService]
})

export class Subscription implements OnInit {
    elementRef: ElementRef;
    result: any;
    postResponse: Object;
    postEvent: Object;
    error: Object;
    static errorMessage: string;
    static widgetKey = "subscription";
    static fromUnitTest = "unittest";
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    subscriptionService: SubscriptionService;
    appService: AppService;
    params: Array<IAppParams>;
    deepLinkVariables: Array<IDeepLinkVariables>;
    saved: Boolean;
    failed: Boolean;
    deepLinkEnabled: Boolean;
    formValid: Boolean;
    requiredValidationEnabled: Boolean;
    saveButtonLabel: string;
    previousServiceId: string;
    previousAccountId: string;
    parameters: string;
    tabKey: string;
    fromUTest: Boolean;

    constructor(subscriptionService: SubscriptionService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.requiredValidationEnabled = true;
        this.deepLinkEnabled = true;
        this.previousServiceId = "";
        this.previousAccountId = "";
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.deepLinkVariables = appService.getDeepLinkVariables();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.subscriptionService = subscriptionService;
        this.appService = appService;

        if (this.subscriptionService.res !== undefined) {
            this.result = subscriptionService.res;
            this.fromUTest = this.subscriptionService.fromUnitTest;
        }
    }

    ngOnInit() {
        //this.loadSubscriptions();
        //this.errorOccurred = true;

        if (this.subscriptionService.getSubscriptions("","").toString() != Subscription.fromUnitTest){
            this.loadSubscriptions();
        }
    }

    loadSubscriptions() {
        this.result = {};
        this.subscriptionService.getSubscriptions(this.tabKey, this.parameters)
            .subscribe(res => this.result = res,
            err => this.appService.logError(this, err),
            () => this.disableLoader()
            );
    }

    disableLoader() {
        if (!this.fromUTest) {
            setTimeout(() => this.appService.setHtml(this.elementRef), 1);
        }
        if (this.result === undefined || this.result === null) {
            this.appService.logError(this, '');
        }
        this.enableLoader = false;
    }

    channelEnabled(channel) {
        let returnVal = false;
        if (this.result.Channels.indexOf(channel) !== -1) {
            returnVal = true;
        }
        return returnVal;
    }

    emailDisabled(n) {
        let returnVal = false;
        if (this.result.Channels.indexOf("email") !== -1 && !n.EmailEnabled) {
            returnVal = true;
        }
        return returnVal;
    }

    smsDisabled(n) {
        let returnVal = false;
        if (this.result.Channels.indexOf("sms") !== -1 && !n.SmsEnabled) {
            returnVal = true;
        }
        return returnVal;
    }

    toggleInput(n) {
        if (n.EmailSelected === false && n.SmsSelected === false) {
            n.Threshold = "";
        }
    }

    displayThreshold(n) {
        let returnVal = "";
        if (n.Threshold !== 0) {
            returnVal = n.Threshold;
        }

        return returnVal;
    }

    postData() {
        this.saved = false;
        this.validateForm();
        if (this.formValid === true) {
            const data = {
                "profile": this.result,
                "parameters": this.parameters,
                "notifications": this.result.Notifications
            };

            this.subscriptionService.postSubscriptions(data)
                .subscribe(res => this.postResponse = res,
                err => this.appService.logError(this, err),
                () => this.subscriptionsSaved()
                );
        }
    }

    subscriptionsSaved() {
        this.saved = true;
        if (!this.fromUTest) {
            this.postEvents();
        }
    }

    postEvents() {
        const ei = [];
        ei.push({ key: "TabId", value: this.tabKey });

        this.appService.postEvents(ei, this.parameters, "Update Subscription")
            .subscribe(res => this.postEvent = res,
            err => this.appService.logError(this, err)
            );
    }

    validateForm() {
        let errorCount = 0;
        const notifications = this.result.Notifications;
        for (let nt in notifications) {
            if (notifications.hasOwnProperty(nt)) {
                const n = notifications[nt];
                if ((n.ThresholdType === "cost" || n.ThresholdType === "usage") && (n.EmailSelected || n.SmsSelected)) {
                    const t = n.Threshold;
                    if (this.requiredValidationEnabled === true && t === "") {
                        n.InvalidRequired = true;
                        errorCount += 1;
                        break;
                    } else {
                        n.InvalidRequired = false;
                    }

                    if (n.InvalidRequired === false && (this.appService.isNumeric(t) === false || (parseFloat(t) < parseFloat(n.ThresholdMin) || parseFloat(t) > parseFloat(n.ThresholdMax)))) {
                        n.InvalidRange = true;
                        errorCount += 1;
                        break;
                    } else {
                        n.InvalidRange = false;
                    }
                }

            }
        }

        if (errorCount === 0) {
            this.formValid = true;
        } else {
            this.formValid = false;
        }

    }

}

