﻿import "rxjs/add/operator/map";
import {Injectable, Input} from "@angular/core"
import {Http, Headers} from "@angular/http";

@Injectable()
export class SubscriptionService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    getSubscriptions(tabKey, params) {
        try {
            return this.http.get(`./Subscription/Get?tabkey=${tabKey}&parameters=${window.btoa(params)}`).map(res => res.json());
        }
        catch (ex){
            throw(ex)
        }

    }

    postSubscriptions(data) {
        const headers = new Headers();
        headers.append("Content-Type", "application/json");
        return this.http.post("./Subscription/Post", JSON.stringify(data), { headers: headers }).map(res => res.json());
    }
}

