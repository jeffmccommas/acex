﻿import {Component, ElementRef, OnInit} from "@angular/core";
import {PremiseSelectService} from "./premiseselect.service";
import {AppService, IAppParams} from "../app.service";

@Component({
    selector: 'premiseselect',
    templateUrl: 'App/PremiseSelect/premiseselect.html',
    providers: [PremiseSelectService, AppService]
})

export class PremiseSelect implements OnInit {
    elementRef: ElementRef;
    result: any;
    postResponse: Object; 
    postEvent: Object;
    error: Object;
    static errorMessage: string;
    static widgetKey = "premiseselect";
    static fromUnitTest = "unittest";
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    premiseSelectService: PremiseSelectService;
    appService: AppService;
    params: Array<IAppParams>;

    parameters: string;
    tabKey: string;
    showPanel: Boolean;

    account: string;
    address1: string;
    address2: string;
    city: string;
    state: string;
    postalCode: string;
    commodities: string;
    loadingMessage: string;
    fromUTest: Boolean;

    constructor(premiseSelectService: PremiseSelectService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.showPanel = false;
        this.loadingMessage = "";
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.premiseSelectService = premiseSelectService;
        this.appService = appService;

        if (this.premiseSelectService.res !== undefined) {
            this.result = premiseSelectService.res;
            this.fromUTest = this.premiseSelectService.fromUnitTest;
        }
    }


    ngOnInit() {
        if (this.premiseSelectService.getAccountPremise("", "").toString() !== PremiseSelect.fromUnitTest) {
            this.loadCustomerInfo();
        }
    }

    loadCustomerInfo() {
        this.result = {};
        this.premiseSelectService.getAccountPremise(this.tabKey, this.parameters)
            .subscribe(res => this.result = res,
            err => this.appService.logError(this, err),
            () => this.disableLoader()
            );
    }

    disableLoader() {
        if (!this.fromUTest) {
            setTimeout(() => this.appService.setHtml(this.elementRef), 1);
        }
        if (this.result === undefined || this.result === null || this.result.NoAccounts == true) {
            this.appService.logError(this, "no api data available");
        } else {
            this.setSelectedAddress();
        }
        this.enableLoader = false;
    }

    setPremise(premise) {
        //this.showPanel = false;
        if (premise.Selected === false) {
            this.loadingAccount();
            window.location.href = premise.Url;
        }
    } 

    loadingAccount() {
        this.loadingMessage = this.result.LoadingMessage;
        this.account = "";
        this.address1 = "";
        this.address2 = "";  
        this.city = "";
        this.state = "";
        this.postalCode = "";
        this.commodities = "";
    }

    setSelectedAddress() {
        const accounts = this.result.AccountList;
        for (let a in accounts) {
            if (accounts.hasOwnProperty(a)) {
                const premises = accounts[a].Premises;
                for (let p in premises) {
                    if (premises.hasOwnProperty(p)) {
                        const prem = premises[p];
                        if (prem.Selected) {
                            this.account = accounts[a].Id;
                            this.address1 = prem.Addr1;
                            this.address2 = prem.Addr2;
                            this.city = prem.City + ",";
                            this.state = prem.StateProvince;
                            this.postalCode = prem.PostalCode;
                            this.commodities = prem.Commodities;
                        }
                    }
                }
            }
        }
    }
}



