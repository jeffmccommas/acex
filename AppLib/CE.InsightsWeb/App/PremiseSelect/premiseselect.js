"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var premiseselect_service_1 = require("./premiseselect.service");
var app_service_1 = require("../app.service");
var PremiseSelect = (function () {
    function PremiseSelect(premiseSelectService, appService, elementRef) {
        this.enableLoader = true;
        this.showPanel = false;
        this.loadingMessage = "";
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.premiseSelectService = premiseSelectService;
        this.appService = appService;
        if (this.premiseSelectService.res !== undefined) {
            this.result = premiseSelectService.res;
            this.fromUTest = this.premiseSelectService.fromUnitTest;
        }
    }
    PremiseSelect_1 = PremiseSelect;
    PremiseSelect.prototype.ngOnInit = function () {
        if (this.premiseSelectService.getAccountPremise("", "").toString() !== PremiseSelect_1.fromUnitTest) {
            this.loadCustomerInfo();
        }
    };
    PremiseSelect.prototype.loadCustomerInfo = function () {
        var _this = this;
        this.result = {};
        this.premiseSelectService.getAccountPremise(this.tabKey, this.parameters)
            .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.disableLoader(); });
    };
    PremiseSelect.prototype.disableLoader = function () {
        var _this = this;
        if (!this.fromUTest) {
            setTimeout(function () { return _this.appService.setHtml(_this.elementRef); }, 1);
        }
        if (this.result === undefined || this.result === null || this.result.NoAccounts == true) {
            this.appService.logError(this, "no api data available");
        }
        else {
            this.setSelectedAddress();
        }
        this.enableLoader = false;
    };
    PremiseSelect.prototype.setPremise = function (premise) {
        if (premise.Selected === false) {
            this.loadingAccount();
            window.location.href = premise.Url;
        }
    };
    PremiseSelect.prototype.loadingAccount = function () {
        this.loadingMessage = this.result.LoadingMessage;
        this.account = "";
        this.address1 = "";
        this.address2 = "";
        this.city = "";
        this.state = "";
        this.postalCode = "";
        this.commodities = "";
    };
    PremiseSelect.prototype.setSelectedAddress = function () {
        var accounts = this.result.AccountList;
        for (var a in accounts) {
            if (accounts.hasOwnProperty(a)) {
                var premises = accounts[a].Premises;
                for (var p in premises) {
                    if (premises.hasOwnProperty(p)) {
                        var prem = premises[p];
                        if (prem.Selected) {
                            this.account = accounts[a].Id;
                            this.address1 = prem.Addr1;
                            this.address2 = prem.Addr2;
                            this.city = prem.City + ",";
                            this.state = prem.StateProvince;
                            this.postalCode = prem.PostalCode;
                            this.commodities = prem.Commodities;
                        }
                    }
                }
            }
        }
    };
    PremiseSelect.widgetKey = "premiseselect";
    PremiseSelect.fromUnitTest = "unittest";
    PremiseSelect = PremiseSelect_1 = __decorate([
        core_1.Component({
            selector: 'premiseselect',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"panel panel-default\"><div *ngIf=\"result.ShowTitle == 'true' && result.Title !== ''\" class=\"panel-heading\"><h1 class=\"xs-mb-0\" role=\"presentation\">{{result.Title}}</h1></div><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-hand-right\"></span> <strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div></div><div *ngIf=\"!errorOccurred\" class=\"panel panel-default account-select\"><div class=\"panel-default account-select\"><div class=\"panel-heading account-select__heading\"><div class=\"row\"><div *ngIf=\"(result.AccountList.length > 1 || result.AccountList[0].Premises.length > 1) && loadingMessage === ''\" class=\"text-right pull-right\"><div class=\"xs-pl-0 toggle__btn toggle__drk\"><a class=\"btn btn-default xs-ml-10\" href=\"#\" onclick=\"return false;\" (click)=\"showPanel = !showPanel\" role=\"button\"><span *ngIf=\"!showPanel\">{{result.ExpandLabel}}</span> <span *ngIf=\"showPanel\">{{result.CollapseLabel}}</span> <span [ngClass]=\"showPanel ? 'icon icon-toggle-up' : 'icon icon-toggle-down'\"></span></a></div></div></div><div class=\"row xs-pl-15 xs-pr-15\"><h3 class=\"sr-only\" role=\"heading\" aria-level=\"3\">Account premise</h3><div class=\"account-select__info account-select__heading-account text-left table-responsive xs-ml-0 xs-mr-0 xs-p-15\"><div class=\"col-ms-6 col-lg-5 xs-pr-0 xs-pl-0\"><div class=\"col-md-6 xs-pr-0 xs-pl-0 xs-pb-10\"><span class=\"icon icon-home xs-mr-10 hidden-xs hidden-ms\" aria-hidden=\"true\"></span><div class=\"inline-block\"><strong>{{result.AccountNumberLabel}}</strong><span class=\"dont-break-out xs-pr-15\"><strong>{{account}}</strong></span></div></div><div class=\"col-md-6 xs-pr-0 xs-pl-0 xs-pb-10\" *ngIf=\"result.FirstName.length > 1\">{{result.FirstName}} {{result.LastName}}</div></div><div class=\"col-ms-6 col-lg-7 xs-pr-0 xs-pl-0\"><div class=\"col-md-8 xs-pr-0 xs-pl-0 xs-pb-10\" *ngIf=\"address1.length > 1\">{{address1}} {{address2}} {{city}} {{state}} {{postalCode}}</div><div class=\"col-md-4 xs-pr-0 xs-pl-0 xs-pb-10\">{{commodities}}</div></div></div></div></div><div *ngIf=\"showPanel\"><div *ngFor=\"let a of result.AccountList | objToArr;\"><div class=\"panel-body xs-mb-0 xs-pb-0\" role=\"heading\" aria-level=\"2\"><p><strong>{{result.AccountNumberLabel}} {{a.Id}}</strong></p></div><h3 class=\"sr-only\" role=\"heading\" aria-level=\"3\">Select premise</h3><div *ngFor=\"let p of a.Premises | objToArr;\"><div class=\"panel-body xs-p-0 xs-m-0\"><div class=\"panel-body premises\"><div class=\"col-xs-1 account-select__checkmark xs-pl-0 xs-pr-0 text-left\"><input [checked]=\"p.Selected\" (click)=\"setPremise(p)\" type=\"radio\" name=\"premises\"></div><div class=\"col-xs-2 account-select__account xs-pr-0\"><p class=\"dont-break-out\">{{p.Id}}</p></div><div class=\"col-xs-6 account-select__address xs-pr-0\"><p>{{p.Addr1}} {{p.Addr2}} {{p.City}}, {{p.Country}} {{p.PostalCode}}</p></div><div class=\"col-xs-3 account-select__utility xs-pr-0\"><p>{{p.Commodities}}</p></div></div></div></div></div></div></div></div></div>",
            providers: [premiseselect_service_1.PremiseSelectService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [premiseselect_service_1.PremiseSelectService, app_service_1.AppService, core_1.ElementRef])
    ], PremiseSelect);
    return PremiseSelect;
    var PremiseSelect_1;
}());
exports.PremiseSelect = PremiseSelect;

//# sourceMappingURL=premiseselect.js.map
