﻿import "rxjs/add/operator/map";
import {Injectable, Input} from "@angular/core"
import {Http} from "@angular/http";

@Injectable()
export class PremiseSelectService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    getAccountPremise(tabKey, params) {
        return this.http.get(`./CustomerInfo/GetAccountPremise?tabkey=${tabKey}&parameters=${window.btoa(params)}`).map(res => res.json());
    }

}

