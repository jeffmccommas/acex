﻿import "rxjs/add/operator/map";
import {Injectable,Renderer} from "@angular/core"
import {Http, Headers} from "@angular/http";

@Injectable()
export class AppService {
    deepLinkVariables: Array<IDeepLinkVariables>;
    params: Array<IAppParams>;
    http: Http;
    renderer: Renderer;

    constructor(http: Http, renderer: Renderer) {
        this.http = http;
        this.renderer = renderer;
    }

    getParams(): Array<IAppParams> {
        if (this.params == null) {
            return [{ tabKey: tabKey, parameters: modParams, errorMessage: iws_widget_error }];
        } else {
            return this.params;
        }
    }

    getDeepLinkVariables(): Array<IDeepLinkVariables> {
        if (this.deepLinkVariables == null) {
            return [{ deepLinkWidget: iws_dl_w, deepLinkWidgetTab: iws_dl_wt, deepLinkWidgetSection: iws_dl_ws}];
        } else {
            return this.deepLinkVariables;
        }
    }

    getHtmlFromMarkDown(m) {
        const converter = new showdown.Converter();
        return converter.makeHtml(m);
    }

    pdfExport(excludeWidgetList, reporttitle, reportnotes, tabKey, isCSR) {
        var pdfkendoUIExport = new pdfKendoUIExport(excludeWidgetList, reporttitle, reportnotes, tabKey, isCSR);
        return pdfkendoUIExport;
    }

    iframeExternalUrl(obj) {
        IframeExternalUrl(obj,true);
    }

    useRegularDropdown() {
        var useReg = false;
        if ((/iPad|iPhone|iPod/.test(navigator.userAgent) || (navigator.userAgent.indexOf("Safari") > -1) && navigator.userAgent.indexOf('Chrome') == -1)) {
            useReg = true;
        }
        return useReg;
    }

    clearGlobalVariables(wKey) {
        if (wKey === iws_dl_w) {
            iws_dl_w = "";
            iws_dl_wt = "";
            iws_dl_ws = "";
        }
    }

    getLayout() {
        return JSON.parse(iws_json);
    }

    isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    logError(t, err) {
        t.errorOccurred = true;
        t.enableLoader = false;
        console.error(`There was an error: ${err}`);
        //alert(err);
    }

    setHtml(elementRef) {
        const markdowns = elementRef.nativeElement.querySelectorAll(".markdown");
        for (let m in markdowns) {
            if (markdowns.hasOwnProperty(m)) {
                markdowns[m].innerHTML = this.getHtmlFromMarkDown(markdowns[m].innerHTML);
            }
        }
    }

    setIframeExternalClass(elementRef) {
        var dvlinks = elementRef.nativeElement.querySelectorAll(".dv-external-links");
        var links = [],j=0;
        for (var k = 0; k < dvlinks.length; k++) {
            var temp = dvlinks[k].querySelectorAll("a");
            for (var l = 0; l < temp.length; l++) {
                links[j] = temp[l];
                j++;
            }
        }
        if (links !== undefined || links !== null) {
            for (var i = 0; i < links.length; i++) {
                if (links[i].href.indexOf("?type=pm") > -1) {
                    links[i].setAttribute("class", "iframe-external-url");
                    links[i].setAttribute("id", "link_" + i);
                    var link = links[i].href.split("?type=")[0];
                    links[i].href = link;
                    this.renderer.listen(links[i], 'click', (event) => { this.iframeExternalUrl(elementRef.nativeElement.querySelectorAll("[id='" +event.currentTarget.id+"']"))});                
                }
            }
        }
    }

    postEvents(ei, modParams, eventType) {
        const headers = new Headers();
        headers.append("Content-Type", "application/json");

        const data = {
            "eventInfo": ei,
            "parameters": modParams,
            "eventType": eventType
        };
        return this.http.post("./EventTracking/PostEvents", JSON.stringify(data), { headers: headers }).map(res => res.json());
    }

    BuildBillDisaggChart(result, pieData, showPercentageOnly) {

            var series = { data: [] };
            for (let data in pieData) {
                if (pieData.hasOwnProperty(data)) {
                    var d = pieData[data].split("|");
                    series.data.push({ name: d[0], y: parseFloat(d[1]), color: d[2], percent: d[1], usage: d[3], cost: d[4], uom: d[5] });
                }
            }

            var options = {
                chart: {
                    renderTo: 'pie-chart',
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    formatter: function () {  
                        var t = '<div class="padding-left"><b>' + this.point.name + '</b></div><br/>' +
                            '<div class="padding-left-right">' + result.PercentColumnHeader + ": " + this.point.y + '%</div><br/>';

                        if (!showPercentageOnly){
                            t += '<div class="padding-left-right">' + result.CostColumnHeader + ": " + this.point.cost + '</div><br/>'
                        }

                        if (result.All.PieData === null && !showPercentageOnly) {
                            t += '<div class="padding-left-right">' + result.UsageColumnHeader + ": " + this.point.usage + ' ' + this.point.uom + '</div><br/>';
                        }

                        return t;                      
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        },
                        showInLegend: true
                    }
                },
                exporting: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                series: [{}]
            };

            options.series[0] = series;
            var chart = new Highcharts.Chart(options);
        
    }

    buildUsageChart(result,options,commodity,commodityCount) {
        var chart;
        options = {
            chart: {
                renderTo: "usage_chart",
                marginTop: 50
            },
            style: {
                fontFamily: "arial",
                fontWeight: "700", // ave temp label right 45 degree
                fontSize: "14px"
            },
            title: {
                text: "",
                x: -20 //center
            },
            xAxis: {
                labels: {
                    style: {
                        textTransform: "uppercase",
                        fontWeight: "300 !important",
                        fontSize: "12px" // bottom x labels
                    },
                    rotation: -45
                },
                type: "category",
                rotation: -45
            },
            tooltip: {
                formatter: function () {
                    if (this.series.name === result.AvgTemp) {
                        return '<div class="padding-left">' +
                            this.series.name +
                            '</div><br/><div class="padding-left-right">' +
                            this.point.tooltip_value_formatted +
                            "</div>";
                    } else {
                        var servcies = this.point.Tip.Services;
                        var tt = "";
                        for (let i of servcies) {
                            if (tt !== "")
                                tt = tt + "<br/>";
                            tt = tt +
                        '<div class="padding-left">' +
                        i.ServiceAndCost +
                        ' |</div><br/><div class="padding-left-right">' +
                        result.WeatherLabel +
                        " " +
                        this.point.tooltip_value_formatted +
                        "|</div>"+
                        '<br/><div class="padding-left">' +
                        result.BillDate + " " +
                        i.XAxis +
                        "</div><br/>";
                        }
                        
                        return tt;
                    }
                }
            },
            plotOptions: {
                series: {
                    animation: false,
                    marker: {
                        radius: 5,
                        symbol: result.ChartAnchortype
                    }
                }
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: []
        };
        if (result.NoBills === true) {
            chart = new Highcharts.Chart(options);
            chart.hideNoData();
            chart.showNoData(result.NoBillsText);
            chart.legend.destroy();
        } else {
            options.chart.type = result.ChartType;
            //options.chart.type = "column";

            var yAxis = {
                minPadding: 0,
                startOnTick: true,
                title: {
                    text: result.UOMText,
                    align: "high",
                    y: -30,
                    offset: 15,
                    rotation: 0,
                    fontWeight: "100"
                },
                gridLineColor: "#EEEEEE",
                gridLineDashStyle: "longdash",
                reversedStacks: false
            };

            options.yAxis = yAxis;

            this.usageChartAddSeries(result.PrevYearLegend, result.ChartLineColor2, result.BilledUsagePreviousYear, options, commodity, commodityCount, result);
            this.usageChartAddSeries(result.CurrentYearLegend, result.ChartLineColor1, result.BilledUsageLastYear, options, commodity, commodityCount, result);
            if (result.EnableWeather === "true") {
                this.addWeatherSeries(options, commodity,commodityCount,result);
            };

            chart = new Highcharts.Chart(options);
            chart.legend.render();
        }
    }

    usageChartAddSeries(seriesName, seriesColor, data, options, commodityFilter, commodityCount,result ) {
        var count = (commodityCount * 12) - 1;
        var y = 0;
        var seriesData = [];
        for (var i = count; i >= 0; i--) {
            if (commodityFilter === data[i]["Commodity"]) {
                result.UOMText = data[i].UOM;
                var name = "";
                y = 0;
                name = data[i]["XAxis"];
                y = data[i]["YAxis"];
                if (y <= 0) {
                    y = null;
                }
                var dataPoint = {
                    name: name,
                    y: y,
                    Tip: data[i]["Tip"],
                    tooltip_value_formatted: data[i].Tip.AverageTemperature + "°"
                };
                seriesData.push(dataPoint);
            }
        }
        var series = {
            name: seriesName,
            data: seriesData,
            animation: false,
            color: seriesColor,
            lineWidth: parseInt(result.lineThickness)
        };
        options.series.push(series);
    }

    addWeatherSeries(options, commodityFilter, commodityCount,result) {
        var count = (commodityCount * 12) - 1;

        var yAxis = [
            {
                min: 0,
                minPadding: 0,
                startOnTick: true,
                title: {
                    text: result.UOMText,
                    align: "high",
                    y: -30,
                    offset: 15,
                    rotation: 0,
                    style: {
                        fontFamily: "arial",
                        fontWeight: "700",
                        fontSize: "14px"
                    }
                },
                gridLineColor: "#EEEEEE",
                gridLineDashStyle: "longdash",
                reversedStacks: false
            }, {
                labels: {
                    style: {
                        color: result.ChartLineColor2
                    },
                    format: "{value}°"
                },
                title: {
                    text: "",
                    style: {
                        fontFamily: "arial",
                        fontWeight: "700",
                        fontSize: "14px"
                    }
                },
                opposite: true

            }
        ];

        if (result.ShowWeatherByDefault) {
            yAxis[1].title.text = result.AvgTemp;
        }

        var weatherData = [];

        for (var i = count; i >= 0; i--) {
            if (commodityFilter === result.BilledUsageLastYear[i]["Commodity"]) {
                var n = result.BilledUsageLastYear[i]["XAxis"];
                var y;
                if (result.BilledUsageLastYear[i].Tip.AverageTemperature !== 0)
                    y = result.BilledUsageLastYear[i].Tip.AverageTemperature;    
                else
                    y = null;
                var x = { name: n, y: y, tooltip_value_formatted: y + "°" };
                weatherData.push(x);
            }
        }

        var weatherSeries = {
            name: result.AvgTemp,
            type: "line",
            color: result.ChartLineColor3,
            fillOpacity: 0.2,
            data: weatherData,
            yAxis: 1,
            visible: result.ShowWeatherByDefault,
            animation: false,
            tooltip: false,
            marker: {
                enabled: false
            },
            lineWidth: parseInt(result.LineThickness),
            events: {
                legendItemClick: function (event) {
                    result.ShowWeatherByDefault = this.visible ? false : true;
                    var title = "";
                    if (result.ShowWeatherByDefault) {
                        title = result.AvgTemp;
                    }
                    UpdateWeatherLegend(title,"usage_chart");
                }

            }
        }
        options.yAxis = yAxis;
        options.series.push(weatherSeries);
    }

    buildConsumptionChart(result, options,params,elementRef) {
        var chart;
        var allow_drilling;
        ConsumptionEmptyChart();
        options = {
            chart: {
                renderTo: "iws_cn_chart",
                events: {
                    drilldown: function (e) {
                        
                        if (!e.seriesOptions) {
                            params.current_page = 0;
                            params.paging = false;
                            params.drilling = true;
                            params.endDate = e.point.value;
                            params.res_selected = parseInt(result.SelectedResolutionValue) + 1;
                            elementRef.nativeElement.querySelector("[id='iws_cn_res-" + params.res_selected + "']")
                                .setAttribute("data-drilling", true);
                            elementRef.nativeElement.querySelector("[id='iws_cn_res-" + params.res_selected + "']")
                                .setAttribute("data-enddate", e.point.value);
                           
                            elementRef.nativeElement.querySelector("[id='iws_cn_res-" + params.res_selected + "']")
                                .click();
                            params.drilling = false;
                        }
                    }
                },
                marginTop: 50,
                style: {
                    fontFamily: "arial",
                    fontWeight: "700", // ave temp label right 45 degree
                    fontSize: "14px"
                }
            },
            title: {
                text: "",
                x: -20 //center
            },
            xAxis: {
                labels: {
                    style: {
                        textTransform: "uppercase",
                        fontWeight: "300 !important",
                        fontSize: "12px" // bottom x labels
                    },
                    rotation: -45
                },
                type: "category",
                rotation: -45
            },
            tooltip: {
                formatter: function () {
                    if (this.series.name === result.AverageUsageLabel) {
                        return '<div class="padding-left">' +
                            this.series.name +
                            '</div><br/><div class="padding-left-right">' +
                            this.point.tooltip_value_formatted+"</div>";
                    } else if (this.series.name === result.AvgTemp) {
                        return '<div class="padding-left">' +
                            this.series.name +
                            '</div><br/><div class="padding-left-right">' +
                            this.point.tooltip_value_formatted +
                            "</div>";
                    } else {
                        return '<div class="padding-left">' +
                            this.series.name +
                            '</div><br/><div class="padding-left">' +
                            this.point.tooltip_date +
                            '</div><br/><div class="padding-left-right">' +
                            this.point.tooltip_value_formatted+
                        '</div>';
                    }
                }
            },
            plotOptions: {
                series: {
                    animation: false,
                    marker: {
                        radius: 5,
                        symbol: result.ChartAnchortype
                    }
                }
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: []
        };
        if (result.NoConsumption === true) {
            chart = new Highcharts.Chart(options);
            chart.hideNoData();
            chart.showNoData(result.NoConsumptionText);
            chart.legend.destroy();
        } else {

            //iws_cn.ChartType = "column";
            options.chart.type = result.ChartType;
            allow_drilling = true;
            if (params.res_selected === params.max_res) {
                allow_drilling = false;
            }

            var yAxis = {
                minPadding: 0,
                startOnTick: true,
                title: {
                    text: result.UOMText,
                    align: "high",
                    y: -30,
                    offset: 15,
                    rotation: 0,
                    fontWeight: "100"
                },
                gridLineColor: "#EEEEEE",
                gridLineDashStyle: "longdash",
                reversedStacks: false
            };

            options.yAxis = yAxis;

            if (result.IsTou && result.ChartType === "column" && params.res_selected !== "month") {
                //AddTOUsageSeries
                for (var i = 0; i <= result.TouSeriesList.length - 1; i++) {
                    this.consumptionAddSeries(result.TouSeriesList[i].Label,
                        result.TouSeriesList[i].Color,
                        result.TouSeriesList[i].TouSeries,
                        0,
                        result.TouSeriesList[i].TouSeries.length - 1,
                        result.UOMText, result, options, params, allow_drilling);
                }
                options.plotOptions.column =
                    {
                        stacking: "normal"
                    };
            } else {
                //AddTotalUsageSeries
                switch (params.res_selected) {
                    case "month":
                        //previous year usage series
                        this.consumptionAddSeries(result
                            .Previous12Months,
                            result.ChartLineColor2,
                            result.Data,
                            0,
                            11,
                            result.UOMText, result, options, params, allow_drilling);
                        //current year usage series
                        this.consumptionAddSeries(result.YourUse, result.ChartLineColor1, result.Data, 12, 23, result.UOMText, result, options, params, allow_drilling);
                        break;

                    default:
                        this.consumptionAddSeries(result.YourUse,
                            result.ChartLineColor1,
                            result.Data,
                            0,
                            result.Data.length - 1,
                            result.UOMText, result, options, params, allow_drilling);
                        break;
                }
            }

            if (result.EnableWeather === "true" && result.Weather.length > 0) {
                this.consumptionAddWeatherSeries(options,params,result);
            }
            //AddAvgUsageSeries
            if (result.EnableAverageUsage === "true") {
                var avgUsageData = [];
                var endAt = 11;

                if (params.res_selected !== "month") {
                    endAt = result.Data.length - 1;
                }
                for (var i = 0; i <= endAt; i++) {
                    var yVal = result.AverageUsage;
                    var x = {
                        y: yVal,
                        drilldown: false,
                        type: "line",
                        tooltip_value_formatted: result.AverageUsageFormatted + " " + result.UOMText
                    };
                    avgUsageData.push(x);
                }
                var avgUsageSeries = {
                    name: result.AverageUsageLabel,
                    type: "line",
                    color: result.AverageUsageLineColor,
                    fillOpacity: 0.2,
                    data: avgUsageData,
                    yAxis: 0,
                    visible: result.ShowAverageUsageByDefault,
                    animation: false,
                    marker: {
                        enabled: false
                    },
                    lineWidth: parseInt(result.LineThickness),
                    events: {
                        legendItemClick: function (event) {
                            result.ShowAverageUsageByDefault = this.visible ? false : true;
                        }
                    }
                };
                options.series.push(avgUsageSeries);
            }
            Highcharts.Tick.prototype.drillable = () => { };
            chart = new Highcharts.Chart(options);
            
            chart.legend.render();
            return options;
        }
}

    consumptionAddWeatherSeries(options,params,result) {
        var startAt = 12;
        var endAt = 23;

        if (params.res_selected !== "month") {
            startAt = 0;
            endAt = result.Data.length - 1;
        }

        var yAxis = [
            {
                min: 0,
                minPadding: 0,
                startOnTick: true,
                title: { //  value in top left, CCF
                    text: result.UOMText,
                    align: "high",
                    y: -30,
                    offset: 15,
                    rotation: 0,
                    style: {
                        fontFamily: "arial",
                        fontWeight: "700",
                        fontSize: "14px"
                    }
                },
                gridLineColor: "#EEEEEE",
                gridLineDashStyle: "longdash",
                reversedStacks: false
            }, {
                labels: {
                    style: {
                        color: result.ChartLineColor3
                    },
                    format: "{value}°"
                },
                title: {
                    text: "",
                    style: {
                        fontFamily: "arial",
                        fontWeight: "700",
                        fontSize: "14px"
                    }
                },
                opposite: true

            }
        ];

        if (result.ShowWeatherByDefault) {
            yAxis[1].title.text = result.WeatherLabel;
        }

        var weatherData = [];

        for (var i = startAt; i <= endAt; i++) {
            var n = result.Weather[i]["DateTimeChart"];
            var y = result.Weather[i]["AvgTemp"];
            var v;
            var tooltip = "";
            if (y !== null) {
                tooltip = y + "°";
            }

            var x = {
                name: n,
                y: y,
                drilldown: false,
                type: "spline",
                value: result.Weather[i]["Date"],
                tooltip_date: result.Weather[i]["DateTimeLabel"],
                tooltip_value_formatted: tooltip
            };
            weatherData.push(x);
        }

        var weatherSeries = {
            name: result.WeatherLabel,
            type: "line",
            color: result.ChartLineColor3,
            fillOpacity: 0.2,
            data: weatherData,
            yAxis: 1,
            visible: result.ShowWeatherByDefault,
            animation: false,
            marker: {
                enabled: false
            },
            lineWidth: parseInt(result.LineThickness),
            events: {
                legendItemClick: function (event) {
                    result.ShowWeatherByDefault = this.visible ? false : true;
                    var title = "";
                    if (result.ShowWeatherByDefault) {
                        title = result.WeatherLabel;
                    }
                    UpdateWeatherLegend(title, "iws_cn_chart");
                }
            }

        };
        options.yAxis = yAxis;
        options.series.push(weatherSeries);
    }

    consumptionAddSeries(seriesName, seriesColor, data, startAt, endAt, uom, result, options, params, allow_drilling) {
        var seriesData = [];
        for (var i = startAt; i <= endAt; i++) {
            var name = "";
            var y = 0;
            name = data[i]["DateTimeChart"];
            y = data[i]["Value"];
            var tooltip = "";
            if (y <= 0) {
                y = null;
            }

            if (y !== null) {
                tooltip = data[i].ValueFormatted + " " + uom;
            }

            var dataPoint = {
                name: name,
                y: y,
                drilldown: allow_drilling,
                value: data[i]["DateTime"],
                tooltip_date: data[i]["DateTimeLabel"],
                tooltip_value_formatted: tooltip
            };
            seriesData.push(dataPoint);
        }

        var series = {
            name: seriesName,
            data: seriesData,
            animation: false,
            color: seriesColor,
            lineWidth: parseInt(result.LineThickness)
        };

        options.series.push(series);
    }

    consumptionHighLightElement(elementRef) {
        ConsumptionHighLighElement();
    }
}
    

export interface IAppParams {
    tabKey: string;
    parameters: string;
    errorMessage: string;
}

export interface IDeepLinkVariables {
    deepLinkWidget: string;
    deepLinkWidgetTab: string;
    deepLinkWidgetSection: string;
}