﻿//import {Component, ElementRef, DynamicComponentLoader, Injector, ComponentRef, AfterViewChecked, ApplicationRef, provide } from "@angular/core";
//import {bootstrap} from "@angular/platform-browser-dynamic";
//import {HTTP_PROVIDERS, RequestOptions} from "@angular/http";
//import {LongProfile} from "./Profile/longprofile";
//import {AppService, IAppParams, IDeepLinkVariables} from "./app.service";
//import {DefaultRequestOptions} from "./Helper/defaultrequestoptions";

//@Component({
//    selector: 'site',
//    templateUrl: 'App/site.html',
//    providers: [AppService]
//})
//export class Site implements AfterViewChecked {
//    data: any;
//    elementRef: ElementRef;
//    dynamicComponentLoader: DynamicComponentLoader;
//    injector: Injector;
//    appRef: ApplicationRef;
//    layout: any;

//    constructor(dynamicComponentLoader: DynamicComponentLoader, elementRef: ElementRef, injector: Injector, appRef: ApplicationRef, appService: AppService) {
//        this.elementRef = elementRef;
//        this.dynamicComponentLoader = dynamicComponentLoader;
//        this.injector = injector;
//        this.appRef = appRef;
//        this.layout = appService.getLayout();
//    }

//    ngAfterViewInit() {

//        const rows = this.layout.Rows;
//        for (let row in rows) {
//            if (rows.hasOwnProperty(row)) {
//                const cols = rows[row].Columns;
//                for (let col in cols) {
//                    if (cols.hasOwnProperty(col)) {
//                        const widgets = cols[col].Widgets;
//                        for (let widget in widgets) {
//                            if (widgets.hasOwnProperty(widget)) {
//                                this.dynamicComponentLoader.loadAsRoot(LongProfile, '#' + widgets[widget].Name + "-" + widgets[widget].Instance, this.injector)
//                                    .then(componentRef => {
//                                        // componentRef.instance.myModel = this.myModel;
//                                        (<any>this.appRef)._loadComponent(componentRef);
//                                    });
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//}

//bootstrap(Site, [HTTP_PROVIDERS, provide(RequestOptions, { useClass: DefaultRequestOptions })]);