"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var billdisagg_service_1 = require("./billdisagg.service");
var app_service_1 = require("../app.service");
var BillDisagg = (function () {
    function BillDisagg(billDisaggService, appService, elementRef) {
        this.enableLoader = true;
        this.showChart = true;
        this.noData = false;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.billDisaggService = billDisaggService;
        this.appService = appService;
        this.fromUTest = false;
        if (this.billDisaggService.res !== undefined) {
            this.result = billDisaggService.res;
            this.fromUTest = this.billDisaggService.fromUnitTest;
        }
    }
    BillDisagg_1 = BillDisagg;
    BillDisagg.prototype.ngOnInit = function () {
        if (this.billDisaggService.getBillDisagg("", "").toString() !== BillDisagg_1.fromUnitTest) {
            this.loadbillDisagg();
        }
    };
    BillDisagg.prototype.loadbillDisagg = function () {
        var _this = this;
        this.result = {};
        this.billDisaggService.getBillDisagg(this.tabKey, this.parameters)
            .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.disableLoader(); });
    };
    BillDisagg.prototype.disableLoader = function () {
        var _this = this;
        if (!this.fromUTest) {
            setTimeout(function () { return _this.appService.setHtml(_this.elementRef); }, 1);
            setTimeout(function () { return _this.appService.setIframeExternalClass(_this.elementRef); }, 1);
        }
        this.enableLoader = false;
        if (this.result.All.PieData === null && this.result.Gas.PieData === null && this.result.Electric.PieData === null && this.result.Water.PieData === null) {
            this.noData = true;
        }
        else {
            var com = "";
            if (this.result.Dropdown.length === 0) {
                com = "all";
            }
            else {
                com = this.result.Dropdown[0].Value;
            }
            setTimeout(function () { return _this.getUsage(com); });
        }
    };
    BillDisagg.prototype.getUsage = function (com) {
        this.selectedCommodity = com;
        if (com === "all") {
            this.pieData = JSON.parse(this.result.All.PieData);
            this.legendData = JSON.parse(this.result.All.LegendData);
            this.showPercentageOnly = this.result.All.ShowPercentageOnly;
        }
        else if (com === "electric") {
            this.pieData = JSON.parse(this.result.Electric.PieData);
            this.legendData = JSON.parse(this.result.Electric.LegendData);
            this.showPercentageOnly = this.result.Electric.ShowPercentageOnly;
        }
        else if (com === "gas") {
            this.pieData = JSON.parse(this.result.Gas.PieData);
            this.legendData = JSON.parse(this.result.Gas.LegendData);
            this.showPercentageOnly = this.result.Gas.ShowPercentageOnly;
        }
        else if (com === "water") {
            this.pieData = JSON.parse(this.result.Water.PieData);
            this.legendData = JSON.parse(this.result.Water.LegendData);
            this.showPercentageOnly = this.result.Water.ShowPercentageOnly;
        }
        if (!this.fromUTest) {
            this.appService.BuildBillDisaggChart(this.result, this.pieData, this.showPercentageOnly);
        }
    };
    BillDisagg.fromUnitTest = "unittest";
    BillDisagg = BillDisagg_1 = __decorate([
        core_1.Component({
            selector: 'billdisagg',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"panel panel-default\"><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div></div><div *ngIf=\"!errorOccurred\" class=\"panel panel-default\"><div *ngIf=\"result.ShowTitle == 'true' && result.Title !== ''\" class=\"panel-heading\"><h1 class=\"panel-title\" role=\"presentation\">{{result.Title}}</h1></div><div class=\"panel-body utility\"><div class=\"utility-icons pull-right\"><div class=\"u-icon-focus\"><ng-container *ngIf=\"result.ShowCombinedView == 'false'\"><a *ngFor=\"let c of result.Dropdown\" href=\"#\" (click)=\"getUsage(c.Value)\" onclick=\"return false;\" class=\"btn btn-default icon__{{c.Value}} xs-ml-5\" [ngClass]=\"c.Value === selectedCommodity ? 'icon_focus' : ''\" role=\"button\"><span class=\"sr-only\">{{c.Value}} commodity</span> <span class=\"icon {{c.Font}}\"></span> <span *ngIf=\"result.ShowCommodityLabels == 'true'\" class=\"hidden-xs\">{{c.Text}}</span></a></ng-container><a href=\"#\" onclick=\"return false;\" (click)=\"showChart = !showChart\" class=\"btn btn-default chart-table-spacer\" [ngClass]=\"showChart ? 'icon__table' : 'icon__graph'\" role=\"button\"><span class=\"sr-only\">Toggle chart table data</span> <span class=\"icon\" [ngClass]=\"showChart ? 'icon-table' : 'icon-graph'\"></span></a></div></div></div><div *ngIf=\"result.ShowIntro == 'true' && result.ShowIntro !== ''\" class=\"intro-text\" style=\"white-space: pre-line\">{{result.IntroText}}</div><div *ngIf=\"noData\" class=\"alert alert-warning\"><span class=\"glyphicon glyphicon-alert xs-mr-10\"></span> <strong>{{result.NoModelledUsage}}</strong></div><div *ngIf=\"!noData\" class=\"panel-body xs-pt-0 xs-pl-0 xs-pr-0 xs-pb-0 xs-mb-0\"><div class=\"billCycle__piechart\" [style.display]=\"showChart ? 'block' : 'none'\"><div id=\"pie-chart\"></div></div><div class=\"billCycle__table xs-mr-0 xs-p-15 xs-pt-0\" [style.display]=\"showChart ? 'none' : 'block'\"><div class=\"table-condensed table-responsive xs-mt-10\"><table class=\"table xs-mb-0\" role=\"presentation\"><thead><tr><th></th><th>{{result.PercentColumnHeader}}</th><th *ngIf=\"!showPercentageOnly\">{{result.CostColumnHeader}}</th><th *ngIf=\"result.All.PieData === null && !showPercentageOnly\">{{result.UsageColumnHeader}}</th></tr></thead><tbody><tr *ngFor=\"let ld of legendData\"><td>{{ld.Text}}</td><td>{{ld.Percent}}</td><td *ngIf=\"!showPercentageOnly\">{{ld.Dollars}}</td><td *ngIf=\"result.All.PieData === null && !showPercentageOnly\">{{ld.Usage}} {{ld.UOM}}</td></tr></tbody></table></div></div></div><div *ngIf=\"result.ShowFooterText === 'true' || result.ShowFooterLink === 'true' || result.ShowBillHistoryLink === 'true'\" class=\"panel-footer\"><div *ngIf=\"result.ShowBillHistoryLink === 'true'\" class=\"dv-external-links\"><a href=\"{{result.BillHistoryLink}}\">{{result.ViewBillHistory}}</a></div><div *ngIf=\"result.ShowFooterText === 'true'\" class=\"footertext\">{{result.Footer}}</div><div *ngIf=\"result.ShowFooterLink === 'true'\" class=\"footerlink dv-external-links\"><a href=\"{{result.FooterLink}}\">{{result.FooterLinkText}}</a></div></div></div></div>",
            providers: [billdisagg_service_1.BillDisaggService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [billdisagg_service_1.BillDisaggService, app_service_1.AppService, core_1.ElementRef])
    ], BillDisagg);
    return BillDisagg;
    var BillDisagg_1;
}());
exports.BillDisagg = BillDisagg;

//# sourceMappingURL=billdisagg.js.map
