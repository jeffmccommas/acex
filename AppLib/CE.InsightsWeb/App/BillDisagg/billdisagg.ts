﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { BillDisaggService} from "./billdisagg.service";
import { AppService, IAppParams } from "../app.service";

@Component({
    selector: 'billdisagg',
    templateUrl: 'App/BillDisagg/billdisagg.html',
    providers: [BillDisaggService, AppService]
})
export class BillDisagg implements OnInit {
    elementRef: ElementRef;
    result: any;
    error: Object;
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    billDisaggService: BillDisaggService;
    appService: AppService;
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string;
    fromUTest: Boolean;
    static fromUnitTest = "unittest";
    selectedCommodity: string;
    showChart: Boolean;
    noData: Boolean;
    pieData: any;
    legendData: any;
    showPercentageOnly: Boolean;

    constructor(billDisaggService: BillDisaggService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.showChart = true;
        this.noData = false;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.billDisaggService = billDisaggService;
        this.appService = appService;
        this.fromUTest = false;
        if (this.billDisaggService.res !== undefined) {
            this.result = billDisaggService.res;
            this.fromUTest = this.billDisaggService.fromUnitTest;
        }
    }

    ngOnInit() {
        if (this.billDisaggService.getBillDisagg("", "").toString() !== BillDisagg.fromUnitTest) {
            this.loadbillDisagg();
        }
    }

    loadbillDisagg() {
        this.result = {};
            this.billDisaggService.getBillDisagg(this.tabKey, this.parameters)
                .subscribe(res => this.result = res,
                err => this.appService.logError(this, err),
                () => this.disableLoader()
                ); 
    }

    disableLoader() {
        if (!this.fromUTest) {
            setTimeout(() => this.appService.setHtml(this.elementRef), 1);
            setTimeout(() => this.appService.setIframeExternalClass(this.elementRef), 1);
        }

        //check for data here
        this.enableLoader = false;

        if (this.result.All.PieData === null && this.result.Gas.PieData === null && this.result.Electric.PieData === null && this.result.Water.PieData === null) {
            this.noData = true;
        }
        else {
            var com = "";
            if (this.result.Dropdown.length === 0) {
                com = "all";
            } else {
                com = this.result.Dropdown[0].Value;
            }
            setTimeout(() => this.getUsage(com));
        }

    } 

    getUsage(com) {
        this.selectedCommodity = com;

        if (com === "all") {
            this.pieData = JSON.parse(this.result.All.PieData);
            this.legendData = JSON.parse(this.result.All.LegendData);
            this.showPercentageOnly = this.result.All.ShowPercentageOnly; 
        } 
        else if (com === "electric") {
            this.pieData = JSON.parse(this.result.Electric.PieData);
            this.legendData = JSON.parse(this.result.Electric.LegendData); 
            this.showPercentageOnly = this.result.Electric.ShowPercentageOnly;   
        }
        else if (com === "gas") {
            this.pieData = JSON.parse(this.result.Gas.PieData);
            this.legendData = JSON.parse(this.result.Gas.LegendData); 
            this.showPercentageOnly = this.result.Gas.ShowPercentageOnly; 
        }
        else if (com === "water") {
            this.pieData = JSON.parse(this.result.Water.PieData); 
            this.legendData = JSON.parse(this.result.Water.LegendData); 
            this.showPercentageOnly = this.result.Water.ShowPercentageOnly; 
        }
         
        if (!this.fromUTest) {
            this.appService.BuildBillDisaggChart(this.result, this.pieData, this.showPercentageOnly);
        }
    }
}