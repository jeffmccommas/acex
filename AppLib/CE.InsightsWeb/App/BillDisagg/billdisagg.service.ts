﻿import "rxjs/add/operator/map";
import {Injectable, Input} from "@angular/core"
import {Http} from "@angular/http";

@Injectable()
export class BillDisaggService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    getBillDisagg(tabKey, params) {
        return this.http.get(`./BillDisagg/Get?tabKey=${tabKey}&parameters=${window.btoa(params)}`).map(res => res.json());
    }

}

