﻿import {Component, ElementRef, OnInit} from "@angular/core";
import {CustomerInfoService} from "./customerinfo.service";
import { AppService, IAppParams } from "../app.service";


@Component({
    selector: 'customerinfo',
    templateUrl: 'App/CustomerInfo/customerinfo.html',
    providers: [CustomerInfoService, AppService]
})

export class CustomerInfo implements OnInit {
    elementRef: ElementRef;
    result: any;
    postResponse: Object;
    postEvent: Object;
    error: Object;
    static errorMessage: string;
    static widgetKey = "customerinfo"; 
    static fromUnitTest = "unittest";
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    customerInfoService: CustomerInfoService;
    appService: AppService; 
    params: Array<IAppParams>;
    saved: Boolean;
    failed: Boolean;
    deepLinkEnabled: Boolean;
    formValid: Boolean;
    requiredValidationEnabled: Boolean;
    saveButtonLabel: string;
    previousServiceId: string;
    previousAccountId: string;
    parameters: string;
    tabKey: string;
    premiseId: string;
    address1: string;
    address2: string;
    city: string;
    state: string;
    postalCode: string;
    fromUTest: Boolean;
    useRegularSelect: Boolean;


    constructor(customerInfoService: CustomerInfoService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.requiredValidationEnabled = true;
        this.deepLinkEnabled = true;
        this.previousServiceId = "";
        this.previousAccountId = "";
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.customerInfoService = customerInfoService;
        this.appService = appService;
        this.useRegularSelect = this.useRegularDropdown();
        //this.useRegularSelect = true;
        if (this.customerInfoService.res !== undefined) {
            this.result = customerInfoService.res;
            this.fromUTest = this.customerInfoService.fromUnitTest;
        }
    }

    ngOnInit() {
        if (this.customerInfoService.getCustomerInfo("", "").toString() !== CustomerInfo.fromUnitTest) {
            this.loadCustomerInfo();
        }
    }

    loadCustomerInfo() {
        this.result = {};
        this.customerInfoService.getCustomerInfo(this.tabKey, this.parameters)
            .subscribe(res => this.result = res,
            err => this.appService.logError(this, err),
            () => this.disableLoader()
            );
    }

    disableLoader() {
        if (!this.fromUTest) {
            setTimeout(() => this.appService.setHtml(this.elementRef), 1);
        }
        if (this.result === undefined || this.result === null || this.result.NoAccounts == true) {
            this.appService.logError(this, "no api data available");
        } else {
            this.setSelectedAddress(); 
        }
        this.enableLoader = false;
    }

    setSelectedAddress() {
        const addresses = this.result.Addresses;
        for (let a in addresses) {
            if (addresses.hasOwnProperty(a)) {
                const adr = addresses[a];
                if (adr.Selected) {
                    this.premiseId = adr.Id;
                    this.address1 = adr.Addr1;
                    this.address2 = adr.Addr2;
                    this.city = adr.City;
                    this.state = adr.StateProvince;
                    this.postalCode = adr.PostalCode;
                }
            }
        }
    }

    changePremise(url) {
        window.location.href = url;
    }


    useRegularDropdown() {
        var useReg = false;
        if ((/iPad|iPhone|iPod/.test(navigator.userAgent) || (navigator.userAgent.indexOf("Safari") > -1) && navigator.userAgent.indexOf('Chrome') == -1)) {
            useReg = true;
        }
        return useReg;
    }


}


