﻿import "rxjs/add/operator/map";
import {Injectable, Input} from "@angular/core"
import {Http } from "@angular/http";

@Injectable()
export class CustomerInfoService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    getCustomerInfo(tabKey, params) {
        return this.http.get(`./CustomerInfo/Get?tabkey=${tabKey}&parameters=${window.btoa(params)}`).map(res => res.json());
    }

}

