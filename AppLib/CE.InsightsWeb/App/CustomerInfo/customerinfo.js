"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var customerinfo_service_1 = require("./customerinfo.service");
var app_service_1 = require("../app.service");
var CustomerInfo = (function () {
    function CustomerInfo(customerInfoService, appService, elementRef) {
        this.enableLoader = true;
        this.requiredValidationEnabled = true;
        this.deepLinkEnabled = true;
        this.previousServiceId = "";
        this.previousAccountId = "";
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.customerInfoService = customerInfoService;
        this.appService = appService;
        this.useRegularSelect = this.useRegularDropdown();
        if (this.customerInfoService.res !== undefined) {
            this.result = customerInfoService.res;
            this.fromUTest = this.customerInfoService.fromUnitTest;
        }
    }
    CustomerInfo_1 = CustomerInfo;
    CustomerInfo.prototype.ngOnInit = function () {
        if (this.customerInfoService.getCustomerInfo("", "").toString() !== CustomerInfo_1.fromUnitTest) {
            this.loadCustomerInfo();
        }
    };
    CustomerInfo.prototype.loadCustomerInfo = function () {
        var _this = this;
        this.result = {};
        this.customerInfoService.getCustomerInfo(this.tabKey, this.parameters)
            .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.disableLoader(); });
    };
    CustomerInfo.prototype.disableLoader = function () {
        var _this = this;
        if (!this.fromUTest) {
            setTimeout(function () { return _this.appService.setHtml(_this.elementRef); }, 1);
        }
        if (this.result === undefined || this.result === null || this.result.NoAccounts == true) {
            this.appService.logError(this, "no api data available");
        }
        else {
            this.setSelectedAddress();
        }
        this.enableLoader = false;
    };
    CustomerInfo.prototype.setSelectedAddress = function () {
        var addresses = this.result.Addresses;
        for (var a in addresses) {
            if (addresses.hasOwnProperty(a)) {
                var adr = addresses[a];
                if (adr.Selected) {
                    this.premiseId = adr.Id;
                    this.address1 = adr.Addr1;
                    this.address2 = adr.Addr2;
                    this.city = adr.City;
                    this.state = adr.StateProvince;
                    this.postalCode = adr.PostalCode;
                }
            }
        }
    };
    CustomerInfo.prototype.changePremise = function (url) {
        window.location.href = url;
    };
    CustomerInfo.prototype.useRegularDropdown = function () {
        var useReg = false;
        if ((/iPad|iPhone|iPod/.test(navigator.userAgent) || (navigator.userAgent.indexOf("Safari") > -1) && navigator.userAgent.indexOf('Chrome') == -1)) {
            useReg = true;
        }
        return useReg;
    };
    CustomerInfo.widgetKey = "customerinfo";
    CustomerInfo.fromUnitTest = "unittest";
    CustomerInfo = CustomerInfo_1 = __decorate([
        core_1.Component({
            selector: 'customerinfo',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-hand-right\"></span> <strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div><div *ngIf=\"!errorOccurred\"><div class=\"panel panel-default\"><div class=\"panel-body xs-p-15 customerinfo__body\"><div class=\"customerinfo__report\"><span class=\"glyphicon glyphicon-stats\" aria-hidden=\"true\"></span><a class=\"customerinfo__report--link\" href=\"#create-report-csr\">Create Report</a></div><div class=\"info-header col-md-3\" role=\"navigation\" aria-label=\"\" style=\"padding: 20px 0 15px 10px\"><h2 style=\"padding-left: 0\">{{result.FirstName}} {{result.LastName}}</h2><h5>{{result.CustomerIdLabel}}</h5>&nbsp;<span>{{result.CustomerId}}</span><br><h5>{{result.AccountNumberLabel}}</h5>&nbsp;<span>{{result.AccountId}}</span><h5 style=\"\">{{result.QuestionAnsweredText}}</h5>&nbsp;&nbsp;<span *ngIf=\"result.ShowProfile === 'true'\"><a class=\"customerinfo__report--linkresults\" href=\"{{result.ViewLink}}\">{{result.ProfileLink}}</a></span></div><div class=\"info-header col-md-4\" role=\"navigation\" aria-label=\"\" style=\"padding: 20px 0 15px 10px\"><h2 style=\"padding-left: 0\">{{result.AddressLabel}}</h2><div *ngIf=\"!useRegularSelect && result.Addresses.length > 1\" class=\"dropdown text-left customerinfo__dropdown\"><button href=\"#\" data-toggle=\"dropdown\" class=\"btn btn-default dropdown-toggle customerinfo__dropdown--btn\"><p class=\"select pull-left xs-m-0 text-left xs-pr-20 customerinfo__dropdown--btnp\">#{{premiseId}} {{address1}}</p><span class=\"pull-right\"><img height=\"6\" src=\"Content/Images/caret-darker.png\" width=\"10\" alt=\"triangle image for button\"></span></button><ul class=\"dropdown-menu customerinfo__dropdown--list\" role=\"menu\"><li *ngFor=\"let a of result.Addresses | objToArr;\" role=\"menuitem\"><a href=\"{{a.Url}}\">#{{a.Id}} {{a.Addr1}}</a></li></ul></div><select *ngIf=\"useRegularSelect && result.Addresses.length > 1\" (change)=\"changePremise($event.target.value)\" class=\"iosHeight\"><option *ngFor=\"let a of result.Addresses | objToArr; let i = index\" value=\"{{a.Url}}\" [attr.selected]=\"a.Selected ? '' : null\">#{{a.Id}} {{a.Addr1}}</option></select><div *ngIf=\"result.Addresses.length === 1\"><div class=\"customerinfo__font\">{{address1}}</div><div class=\"customerinfo__font\" *ngIf=\"result.Addresses[0].Addr2.length > 0\">{{address2}}</div><div class=\"customerinfo__font\">{{city}} {{state}} {{postalCode}}</div></div></div><div class=\"info-header col-md-5 xs-pt-20 xs-pr-0 xs-pb-15 xs-pl-15\" role=\"navigation\"><h2 class=\"xs-pl-0\">{{result.ServiceInfoLabel}}</h2><div *ngFor=\"let s of result.ServiceInfo | objToArr;\"><h5>{{s.CommodityName}}:</h5>&nbsp;<span>{{s.Id}}{{result.MeterLabel}}{{s.MeterId}}</span></div></div></div></div></div></div>",
            providers: [customerinfo_service_1.CustomerInfoService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [customerinfo_service_1.CustomerInfoService, app_service_1.AppService, core_1.ElementRef])
    ], CustomerInfo);
    return CustomerInfo;
    var CustomerInfo_1;
}());
exports.CustomerInfo = CustomerInfo;

//# sourceMappingURL=customerinfo.js.map
