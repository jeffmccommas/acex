﻿import { NgModule, enableProdMode, Directive, Type } from "@angular/core";
import { DirectiveResolver } from '@angular/compiler';
import { RequestOptions, HttpModule } from "@angular/http";
import { FormsModule } from "@angular/forms";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { BrowserModule } from "@angular/platform-browser";
import { Loader } from "./Loader/loader";
import { RecaptchaModule, RECAPTCHA_SETTINGS, RecaptchaSettings } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { ObjToArr } from "./Helper/pipe";
import { DefaultRequestOptions } from "./Helper/defaultrequestoptions";
import { LongProfile } from "./Profile/longprofile";
import { PremiseSelect } from "./PremiseSelect/premiseselect";
import { Subscription } from "./Subscription/subscription";
import { Unauthenticated } from "./Unauthenticated/unauthenticated";
import { Promo } from "./Promo/promo";
import { PeerComparison } from "./PeerComparison/peercomparison";
import { Profile } from "./Profile/profile";
import { CustomerInfo } from "./CustomerInfo/customerinfo";
import { BillHistory } from "./BillHistory/billhistory";
import { ReportCreate } from "./ReportCreate/reportcreate";
import { BilledUsageChart } from "./BilledUsageChart/billedusagechart";
import { GreenButton } from "./GreenButton/greenbutton";
import { GBCAdmin } from "./GreenButton/gbcadmin";
import { BillDisagg } from "./BillDisagg/billdisagg";
import { DatepickerModule, ModalModule } from 'ngx-bootstrap';
import { ReportList } from "./ReportList/reportlist";
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { QuickLinks } from "./QuickLinks/quicklinks";
import { BillSummary } from "./BillSummary/billsummary";
import { BillToDate } from "./BillToDate/billtodate";
import { Threshold } from "./Threshold/threshold";
import { ThirdPartySelection } from "./ThirdPartySelection/thirdpartyselection";
import { ThirdPartyScopeSelection } from "./ThirdPartyScopeSelection/thirdpartyscopeselection";
import { Consumption } from "./Consumption/consumption";
import { App } from "./app";

class AceViewResolver extends DirectiveResolver {
    resolve(type: Type<any>, throwIfNotFound?: boolean): Directive {
        const view = super.resolve(type, throwIfNotFound);
        if (view.selector === "app") {
            let output = "";

            let containerClass = "container";
            if (typeof enableRemote != 'undefined') {
                if (enableRemote === 'True') {
                    containerClass = "";
                }
            }

            if (typeof angWidgetsLayout != 'undefined') {
                if (angWidgetsLayout !== null) {
                    const w = angWidgetsLayout.split(",");
                    //Header
                    // here we enable the container class only when we have a sidebar
                    for (let i = 0; i < w.length; i++) {
                        if (w[i] !== null && w[i].length !== 0) {
                            if (w[i] === "customerinfo" || w[i] === "premiseselect") {
                                output = '<div class="' + containerClass + '" id="PageWidget_3" name="customerinfo" ><' + w[i] + '></' + w[i] + '> </div>';
                            }
                        }
                    }

                    if (typeof angWidgetsHtml != 'undefined') {
                        if (angWidgetsHtml !== null) {
                            //Ang widgets html from view
                            output = output + aclaraJQuery("<div>").html(angWidgetsHtml).text();
                        }
                    }

                    //Footer
                    for (let k = 0; k < w.length; k++) {
                        if (w[k] !== null && w[k].length !== 0) {
                            if (w[k] === "reportcreate") {
                                output = output + '<div class="' + containerClass + '"><' + w[k] + '></' + w[k] + '> </div>';
                            }
                        }
                    }

                    if (typeof nonAngWidgets != 'undefined') {
                        if (nonAngWidgets !== null) {
                            //Non ang widget html replacement
                            var comhtml;
                            const nw = nonAngWidgets.split(",");
                            for (let j = 0; j < nw.length; j++) {
                                if (nw[j] !== null && nw[j].length !== 0) {
                                    aclaraJQuery("#" + nw[j]).each(function () {
                                        const cnt = aclaraJQuery(this).children().length;
                                        if (cnt > 1) {
                                            comhtml = aclaraJQuery(this).html();
                                            output = output.replace(nw[j] + "_html", "<div id='" + nw[j] + "_html'></div>");
                                        }
                                    });
                                }
                            }
                        }
                    }                   
                }
            } else {
                if (typeof angWidgetsHtml != 'undefined') {
                    if (angWidgetsHtml !== null) {
                        //Ang widgets html from view
                        output = output + aclaraJQuery("<div>").html(angWidgetsHtml).text();
                    }
                }
            }
            (<any>view).template = output;
        }
        return view;
    }
}

@NgModule({
    imports: [BrowserModule, FormsModule, HttpModule, RecaptchaFormsModule, DatepickerModule.forRoot(), ConfirmationPopoverModule.forRoot(), ModalModule.forRoot(), RecaptchaModule.forRoot()],
    declarations: [App, LongProfile, Subscription, Promo, PeerComparison, Profile, CustomerInfo, BillHistory,
                    ReportCreate, PremiseSelect, GreenButton, Consumption, BilledUsageChart, BillDisagg,
                    ReportList, QuickLinks, BillSummary, BillToDate, Threshold, ThirdPartySelection, GBCAdmin,
                    ThirdPartyScopeSelection, Unauthenticated,  Loader, ObjToArr],
    bootstrap: [App],
    providers: [
        { provide: RequestOptions, useClass: DefaultRequestOptions },
        { provide: RECAPTCHA_SETTINGS, useValue: { siteKey: '6LfR_TMUAAAAAM39id5_EkA35jOc1fA_-yaDWm0Y' } as RecaptchaSettings }
    ]
})
export class AppModule { }

if (typeof isDebugEnabled != 'undefined') {
    if (isDebugEnabled != null) {
        if (isDebugEnabled === "False") {
            enableProdMode();
        }
    }
} 

platformBrowserDynamic().bootstrapModule(AppModule, {
    providers: [
        { provide: DirectiveResolver, useClass: AceViewResolver }
    ]
});