"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var thirdpartyselection_service_1 = require("./thirdpartyselection.service");
var app_service_1 = require("../app.service");
var ThirdPartySelection = (function () {
    function ThirdPartySelection(thirdpartyselectionService, appService, elementRef) {
        this.enableLoader = true;
        this.cookiesEnabled = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.thirdpartyselectionService = thirdpartyselectionService;
        this.appService = appService;
        this.fromUTest = false;
        if (this.thirdpartyselectionService.res !== undefined) {
            this.result = thirdpartyselectionService.res;
            this.fromUTest = this.thirdpartyselectionService.fromUnitTest;
        }
    }
    ThirdPartySelection.prototype.ngOnInit = function () {
        this.loadThirdPartySelection();
    };
    ThirdPartySelection.prototype.loadThirdPartySelection = function () {
        var _this = this;
        this.result = {};
        this.thirdpartyselectionService.GetThirdPartySelection(this.tabKey, this.parameters)
            .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.disableLoader(); });
    };
    ThirdPartySelection.prototype.disableLoader = function () {
        var _this = this;
        if (!this.fromUTest) {
            setTimeout(function () { return _this.appService.setHtml(_this.elementRef); }, 1);
        }
        this.cookiesEnabled = this.checkCookies();
        this.enableLoader = false;
        this.postEvents(null, "Visit Third Party Selection Page");
    };
    ThirdPartySelection.prototype.connectThirdParty = function (applicationInfo) {
        var ei = [];
        ei.push({ key: "ThirdPartyAppId", value: applicationInfo.application_information_id });
        this.postEvents(ei, "Request App Connect");
        window.location.href = applicationInfo.thirdPartyScopeSelectionScreenURI;
    };
    ThirdPartySelection.prototype.disconnectThirdParty = function (applicationInfo) {
        var _this = this;
        this.thirdpartyselectionService.DisconnectThirdParty(applicationInfo.client_id, this.parameters)
            .subscribe(function (res) { return _this.postResponse = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.disconnected(applicationInfo); });
    };
    ThirdPartySelection.prototype.disconnected = function (applicationInfo) {
        var _this = this;
        var ei = [];
        ei.push({ key: "ThirdPartyAppId", value: applicationInfo.application_information_id });
        this.postEvents(ei, "Request App Disconnect");
        var ac = this.result.GreenButtonApplicationConnected;
        for (var a in ac) {
            if (ac.hasOwnProperty(a)) {
                var app = ac[a];
                if (app.ApplicationId === applicationInfo.application_information_id && app.Connected) {
                    app.Connected = false;
                    app.Disconnected = true;
                }
            }
        }
        if (!this.fromUTest) {
            setTimeout(function () { return _this.appService.setHtml(_this.elementRef); }, 1);
        }
    };
    ThirdPartySelection.prototype.isConnected = function (applicationInfo) {
        var connected = false;
        var ac = this.result.GreenButtonApplicationConnected;
        for (var a in ac) {
            if (ac.hasOwnProperty(a)) {
                var app = ac[a];
                if (app.ApplicationId === applicationInfo.application_information_id && app.Connected) {
                    connected = true;
                }
                else {
                    applicationInfo.TermsAndConditions = app.TermsAndConditions;
                }
            }
        }
        return connected;
    };
    ThirdPartySelection.prototype.isDisconnected = function (applicationInfo) {
        var disconnected = false;
        var ac = this.result.GreenButtonApplicationConnected;
        for (var a in ac) {
            if (ac.hasOwnProperty(a)) {
                var app = ac[a];
                if (app.ApplicationId === applicationInfo.application_information_id && app.Disconnected) {
                    disconnected = true;
                }
            }
        }
        return disconnected;
    };
    ThirdPartySelection.prototype.checkCookies = function () {
        var cookieEnabled = (navigator.cookieEnabled) ? true : false;
        if (typeof navigator.cookieEnabled === "undefined" && !cookieEnabled) {
            document.cookie = "testcookie";
            cookieEnabled = (document.cookie.indexOf("testcookie") !== -1) ? true : false;
        }
        return (cookieEnabled);
    };
    ThirdPartySelection.prototype.termsLinkClicked = function (applicationInfo) {
        var ei = [];
        ei.push({ key: "ThirdPartyAppId", value: applicationInfo.application_information_id });
        this.postEvents(ei, "Click Terms Link");
    };
    ThirdPartySelection.prototype.uncheckTerms = function (applicationInfo) {
        applicationInfo.TermsAndConditionsAccepted = false;
    };
    ThirdPartySelection.prototype.postEvents = function (ei, eventType) {
        var _this = this;
        if (this.fromUTest === false) {
            this.appService.postEvents(ei, this.parameters, eventType)
                .subscribe(function (res) { return _this.postEvent = res; }, function (err) { return _this.appService.logError(_this, err); });
        }
    };
    ThirdPartySelection = __decorate([
        core_1.Component({
            selector: 'thirdpartyselection',
            template: "<div class=\"row\"><div class=\"gbc__logo text-center xs-pt-20\"><img alt=\"logo\" something=\"else\" class=\"img-responsive\" src=\"{{result.ClientLogo}}\"></div></div><div class=\"gbc\"><div class=\"panel panel-default gbc__container\"><div *ngIf=\"result.ShowTitle == 'true'\" class=\"panel-heading gbc__title\"><h1 role=\"presentation\" class=\"panel-title\">{{result.Title}}</h1></div><div *ngIf=\"result.ShowSubTitle === 'true'\" class=\"gbc__subtitle\"><div class=\"panel-body intro-text xs-pb-0 xs-pt-0 xs-pb-15 xs-mb-0\"><h2 *ngIf=\"result.ShowSubTitle == 'true' && result.SubTitle !== ''\" role=\"presentation\" class=\"subtitle gbc__h2\">{{result.SubTitle}}</h2></div></div><div class=\"panel-body gbc__header xs-m-15 xs-mt-0\" *ngIf=\"result.ShowIntro == 'true' && result.ShowIntro !== ''\"><div class=\"row\"><div class=\"col-sm-3 col-md-2 hidden-ms hidden-xs\"><p class=\"gbc__icon xs-ml-0 xs-mr-15\"><img alt=\"logo\" class=\"img-responsive\" src=\"{{result.GreenButtonConnectLogo}}\"></p></div><div class=\"col-sm-9 col-md-10\"><p class=\"lead gbc__intro markdown xs-mb-0\">{{result.IntroText}}</p></div></div></div><div *ngIf=\"!cookiesEnabled\" class=\"panel-body xs-p-0 xs-m-0\"><div class=\"alert alert-warning\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{result.CookieMessage}}</strong> <span class=\"sr-only\">{{result.CookieMessage}}</span></div></div><div class=\"panel-body xs-p-0 xs-m-0\"><div *ngIf=\"cookiesEnabled && result.NoThirdParties\" class=\"alert alert-warning\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{result.NoThirdPartiesMessage}}</strong> <span class=\"sr-only\">{{result.NoThirdPartiesMessage}}</span></div></div><div *ngIf=\"cookiesEnabled && !result.NoThirdParties\"><div *ngFor=\"let ai of result.GreenButtonApplicationInformation\" class=\"gbc__item xs-m-15\"><div class=\"container-fluid\"><div class=\"row\"><div class=\"xs-p-15\"><div class=\"pull-left\"><div *ngIf=\"ai.logo_uri != null && ai.logo_uri != ''\" class=\"pull-left hidden-xs xs-mr-15\"><img alt=\"logo\" class=\"img-responsive img-rounded thirdparty__img\" src=\"{{ai.logo_uri}}\"></div><div class=\"xs-pt-0 xs-m-0 ms-pl-15 sm-pl-15 md-pl-15 lg-pl-15\"><h2 class=\"gbc__text--h2 xs-p-15 xs-pl-0 ms-pt-0 xs-pb-5\"><strong>{{ai.client_name}}</strong></h2><p class=\"xs-pl-0 xs-mb-0 xs-pb-15\">{{ai.thirdPartyApplicationDescription}}</p></div></div></div></div><div class=\"row\"><div class=\"xs-p-15 xs-pt-0 text-center\"><div *ngIf=\"!isConnected(ai)\" class=\"text-center xs-pb-15 xs-pl-0 xs-pb-0 xs-pr-0\"><label><span (click)=\"staticModal.show(); termsLinkClicked(ai)\" class=\"terms__link\">{{result.TermsAndConditionsLinkText}}</span></label><div class=\"modal fade\" bsModal #staticModal=\"bs-modal\" [config]=\"{backdrop: 'static'}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\"><div class=\"modal-dialog modal-md\"><div class=\"modal-content text-left\"><div class=\"modal-header\"><button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"staticModal.hide();uncheckTerms(ai);\"><span aria-hidden=\"true\">&times;</span></button><h4 class=\"modal-title pull-left\">{{result.TermsAndConditionsTitle}}</h4></div><div class=\"modal-body small xs-mt-20 xs-mb-20 markdown\">{{ai.termsAndConditions}}</div><div class=\"text-center xs-mb-15\"><label class=\"gbc__term-link\"><input type=\"checkbox\" #acceptance (change)=\"ai.TermsAndConditionsAccepted = acceptance.checked\" [checked]=\"ai.TermsAndConditionsAccepted\"><span class=\"terms__link\">{{result.TermsAndConditionsAgreeText}}</span></label><a (click)=\"connectThirdParty(ai)\" class=\"btn btn-connect xs-mb-15 ms-mb-0 xs-pr-0 xs-mr-5\" [ngClass]=\"ai.TermsAndConditionsAccepted ? 'active' : 'disabled'\">{{result.ConnectButton}} <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></a></div></div></div></div></div><div *ngIf=\"isConnected(ai)\" class=\"text-center xs-pb-15 xs-pl-0 xs-pb-0 xs-pr-0\"><a (click)=\"disconnectThirdParty(ai)\" class=\"btn btn-disconnect pull-none-xs pull-left-ms pull-left-sm pull-left-md pull-left-lg xs-mb-15 ms-mb-0 xs-pl-5\">{{result.DisconnectButton}} <i class=\"fa fa-times\" aria-hidden=\"true\"></i></a></div></div></div></div><div *ngIf=\"isDisconnected(ai)\" class=\"panel-body xs-p-0 xs-mb-0\"><div class=\"gbc__disconnected alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{result.DisconnectMessage}}</strong> <span class=\"sr-only\">{{result.DisconnectMessage}}</span></div></div></div><div *ngIf=\"result.ShowFooterText === 'true' || result.ShowFooterLink === 'true'\"><div class=\"panel-footer\"><div *ngIf=\"result.ShowFooterText === 'true'\" class=\"footertext\">{{result.Footer}}</div><div *ngIf=\"result.ShowFooterLink === 'true'\" class=\"footerlink\"><a href=\"{{result.FooterLink}}\">{{result.FooterLinkText}}</a></div></div></div></div></div></div>",
            providers: [thirdpartyselection_service_1.ThirdPartySelectionService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [thirdpartyselection_service_1.ThirdPartySelectionService, app_service_1.AppService, core_1.ElementRef])
    ], ThirdPartySelection);
    return ThirdPartySelection;
}());
exports.ThirdPartySelection = ThirdPartySelection;

//# sourceMappingURL=thirdpartyselection.js.map
