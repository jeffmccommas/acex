﻿import "rxjs/add/operator/map";
import { Injectable, Input } from "@angular/core"
import { Http } from "@angular/http";

@Injectable()
export class ThirdPartySelectionService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    GetThirdPartySelection(tabKey, params) {
        return this.http.get(`./GreenButtonConnect/GetRegisteredApplications?tabKey=${tabKey}&parameters=${window.btoa(params)}`).map(res => res.json());
    }

    //////ConnectThirdParty(dataCustodianId, thirdPartyScopeSelectionScreenUri, scope, params) {
    //////    //thirdPartyScopeSelectionScreenUri = "thirdpartyscopeselection";
    //////    var tabKey = "";
    //////    return this.http.get(`./GreenButtonConnect/ConnectThirdParty?dataCustodianId=${dataCustodianId}&thirdPartyScopeSelectionScreenUri=${thirdPartyScopeSelectionScreenUri}&scope=${scope}&parameters=${window.btoa(params)}`).map(res => res.json()); 
    //////    //return this.http.get(`./GreenButtonConnect/ConnectThirdParty?tabKey=${tabKey}&parameters=${window.btoa(params)}`).map(res => res.json());
    //////}

    DisconnectThirdParty(thirdPartyId, params) {
        return this.http.get(`./GreenButtonConnect/DisconnectThirdParty?thirdPartyId=${thirdPartyId}&parameters=${window.btoa(params)}`).map(res => res.json());
    }

}

