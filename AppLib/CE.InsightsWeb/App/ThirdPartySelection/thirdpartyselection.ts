﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { ThirdPartySelectionService } from "./thirdpartyselection.service";
import { AppService, IAppParams } from "../app.service";

@Component({
    selector: 'thirdpartyselection',
    templateUrl: 'App/ThirdPartySelection/thirdpartyselection.html',
    providers: [ThirdPartySelectionService, AppService]
})
export class ThirdPartySelection implements OnInit {
    elementRef: ElementRef;
    result: any;
    postResponse: Object;
    error: Object;
    postEvent: Object;
    enableLoader: Boolean; 
    errorOccurred: Boolean;
    errorMessage: string;
    thirdpartyselectionService: ThirdPartySelectionService;
    appService: AppService;
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string;
    cookiesEnabled: Boolean;
    fromUTest: Boolean;

    constructor(thirdpartyselectionService: ThirdPartySelectionService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.cookiesEnabled = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.thirdpartyselectionService = thirdpartyselectionService;
        this.appService = appService;
        this.fromUTest = false;

        if (this.thirdpartyselectionService.res !== undefined) {
            this.result = thirdpartyselectionService.res;
            this.fromUTest = this.thirdpartyselectionService.fromUnitTest;
        }

    }

    ngOnInit() {
        this.loadThirdPartySelection();
    }

    loadThirdPartySelection() {
        this.result = {};
        this.thirdpartyselectionService.GetThirdPartySelection(this.tabKey, this.parameters)
            .subscribe(res => this.result = res,
            err => this.appService.logError(this, err),
            () => this.disableLoader()
            );
    }

    disableLoader() {
        if (!this.fromUTest) {
            setTimeout(() => this.appService.setHtml(this.elementRef), 1);
        }
        this.cookiesEnabled = this.checkCookies();
        this.enableLoader = false;
        this.postEvents(null, "Visit Third Party Selection Page");
    }

    connectThirdParty(applicationInfo) {
        const ei = [];
        ei.push({ key: "ThirdPartyAppId", value: applicationInfo.application_information_id });
        this.postEvents(ei, "Request App Connect");
        window.location.href = applicationInfo.thirdPartyScopeSelectionScreenURI;
    }

    disconnectThirdParty(applicationInfo) {
        this.thirdpartyselectionService.DisconnectThirdParty(applicationInfo.client_id, this.parameters)
            .subscribe(res => this.postResponse = res,
            err => this.appService.logError(this, err),
            () => this.disconnected(applicationInfo)
            );
    }

    disconnected(applicationInfo) {
        const ei = [];
        ei.push({ key: "ThirdPartyAppId", value: applicationInfo.application_information_id });
        this.postEvents(ei, "Request App Disconnect");

        const ac = this.result.GreenButtonApplicationConnected;
        for (let a in ac) {
            if (ac.hasOwnProperty(a)) {
                const app = ac[a];
                if (app.ApplicationId === applicationInfo.application_information_id && app.Connected) {
                    app.Connected = false;
                    app.Disconnected = true;
                }
            }
        }
        if (!this.fromUTest) {
            setTimeout(() => this.appService.setHtml(this.elementRef), 1);
        }
    }

    isConnected(applicationInfo) {
        var connected = false;
        const ac = this.result.GreenButtonApplicationConnected;
        for (let a in ac) {
            if (ac.hasOwnProperty(a)) {
                const app = ac[a];
                if (app.ApplicationId === applicationInfo.application_information_id && app.Connected) {
                    connected = true;
                } else {
                    applicationInfo.TermsAndConditions = app.TermsAndConditions;
                }
            }
        }
        return connected;
    }

    isDisconnected(applicationInfo) {
        var disconnected = false;
        const ac = this.result.GreenButtonApplicationConnected;
        for (let a in ac) {
            if (ac.hasOwnProperty(a)) {
                const app = ac[a];
                if (app.ApplicationId === applicationInfo.application_information_id && app.Disconnected) {
                    disconnected = true;
                }
            }
        }
        return disconnected;
    }

    checkCookies() {
        var cookieEnabled = (navigator.cookieEnabled) ? true : false;

        if (typeof navigator.cookieEnabled === "undefined" && !cookieEnabled) {
            document.cookie = "testcookie";
            cookieEnabled = (document.cookie.indexOf("testcookie") !== -1) ? true : false;
        }
        return (cookieEnabled);
    }

    termsLinkClicked(applicationInfo) {
        const ei = [];
        ei.push({ key: "ThirdPartyAppId", value: applicationInfo.application_information_id });
        this.postEvents(ei, "Click Terms Link");
    }

    uncheckTerms(applicationInfo) {
        applicationInfo.TermsAndConditionsAccepted = false;
    }

    postEvents(ei, eventType) {
        if (this.fromUTest === false) {
            this.appService.postEvents(ei, this.parameters, eventType)
                .subscribe(res => this.postEvent = res,
                err => this.appService.logError(this, err)
                );
        }
    }

}