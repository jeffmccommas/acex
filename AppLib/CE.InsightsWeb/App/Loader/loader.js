"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Loader = (function () {
    function Loader() {
    }
    Loader.prototype.ngOnInit = function () {
    };
    Loader.prototype.ngOnDestroy = function () {
    };
    Loader = __decorate([
        core_1.Component({
            selector: 'Loader',
            template: "<div id=\"loading-placeholder\" tabindex=\"0\" class=\"acl-loader\"><p class=\"acl-loader-text\">Loading...</p><img class=\"acl-loader-image\" alt=\"your content...\" src=\"Content/Images/loading.gif\"></div>"
        })
    ], Loader);
    return Loader;
}());
exports.Loader = Loader;

//# sourceMappingURL=loader.js.map
