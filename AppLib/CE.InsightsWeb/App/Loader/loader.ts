import {Component, OnInit, OnDestroy} from '@angular/core';

@Component({
    selector: 'Loader',
    templateUrl: 'App/Loader/loader.html'
})

export class Loader implements OnInit, OnDestroy {
    ngOnInit() {
        //console.log('onInit');
    }
    ngOnDestroy() {
        //console.log('onDestroy');
    }
}
