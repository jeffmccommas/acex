﻿(function (global) {
    var paths = {
        'npm:': 'node_modules/'
    };

    //map tells the System loader where to look for things
    var map = {
        'app': 'app',
        'rxjs': 'node_modules/rxjs',
        'ngx-bootstrap': 'node_modules/ngx-bootstrap',
        'ng-recaptcha': 'node_modules/ng-recaptcha',
        '@angular/core': 'node_modules/@angular/core/bundles/core.umd.min.js',
        '@angular/common': 'node_modules/@angular/common/bundles/common.umd.min.js',
        '@angular/compiler': 'node_modules/@angular/compiler/bundles/compiler.umd.min.js',
        '@angular/platform-browser': 'node_modules/@angular/platform-browser/bundles/platform-browser.umd.min.js',
        '@angular/platform-browser-dynamic': 'node_modules/@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.min.js',
        '@angular/http': 'node_modules/@angular/http/bundles/http.umd.min.js',
        '@angular/router': 'node_modules/@angular/router/bundles/router.umd.min.js',
        '@angular/forms': 'node_modules/@angular/forms/bundles/forms.umd.min.js',
        'angular2-in-memory-web-api': 'node_modules/angular2-in-memory-web-api',
        'angular-confirmation-popover': 'node_modules/angular-confirmation-popover/dist/umd/angular-confirmation-popover.js'
    };

    //packages tells the System loader how to load when no filename and/or no extension
    var packages = {
        '.':{},
        'app': { defaultExtension: 'js' },
        'rxjs': { defaultExtension: 'js' },
        'ngx-bootstrap': { format: 'cjs', main: 'bundles/ngx-bootstrap.umd.js', defaultExtension: 'js' },
        'ng-recaptcha': { format: 'cjs', main: 'index.js', defaultExtension: 'js' },
        'angular2-in-memory-web-api': { defaultExtension: 'js' }
    };

    var config = {
        map: map,
        packages: packages,
        paths: paths//,
        //warnings: true
    }

    // filterSystemConfig - index.html's chance to modify config before we register it.
    //if (global.filterSystemConfig) { global.filterSystemConfig(config); }

    System.config(config);
})(this);

