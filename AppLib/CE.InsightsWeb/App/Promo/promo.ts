﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { PromoService } from "./promo.service";
import { AppService, IAppParams } from "../app.service";

@Component({
    selector: 'promo',
    templateUrl: 'App/Promo/promo.html',
    providers: [PromoService, AppService]
})
export class Promo implements OnInit {
    elementRef: ElementRef;
    result: any;
    error: Object;
    postEvent: Object;
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    promoService: PromoService;
    appService: AppService;
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string;
    fromUTest: Boolean;
    width: any;
    constructor(promoService: PromoService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.promoService = promoService;
        this.appService = appService;
        this.fromUTest = false;
        this.width = 0;
        if (this.promoService.res !== undefined) {
            this.result = promoService.res;
            this.fromUTest = this.promoService.fromUnitTest;
        }
        
    }

    ngOnInit() {
        this.loadpromos();
    }

    loadpromos() {
        this.result = {};
        if (!this.fromUTest) {
            this.promoService.GetPromos(this.tabKey, this.parameters)
                .subscribe(res => this.result = res,
                    err => this.appService.logError(this, err),
                    ()=>this.loadImgbyWidth()
                );
        }
        this.enableLoader = false;
    }

    loadImgbyWidth() {
        var childnodes = this.elementRef.nativeElement.childNodes;
       
            for (var i = 0; i < childnodes.length; i++) {
                if (childnodes[i].nodeName === "DIV") {
                    var firstChildNode = childnodes[i];
                    for (var j = 0; j < firstChildNode.childNodes.length; j++) {
                        if (firstChildNode.childNodes[j].nodeName === "DIV") {
                            var secondChildNode = firstChildNode.childNodes[j];
                            for (var k = 0; k < secondChildNode.childNodes.length; k++) {
                                if (secondChildNode.childNodes[k].nodeName === "DIV" && secondChildNode.childNodes[k].id === "iws_po_wrapper") {
                                    this.width = secondChildNode.childNodes[k].offsetWidth;
                                }
                            }
                        }
                    }
                }
            }
              
    }

    popUp(url,key) {
        this.postEvents(url, key, 'Click Promo Link');
        window.open(url);
    }


    postEvents(url, key, eventType) {
        const ei = [];
        ei.push({ key: "ActionId", value: key });
        ei.push({ key: "TabId", value: this.tabKey });
        if (url!=="") {
            ei.push({ key: "Link", value: url });    
        }
        if (!this.fromUTest) {
            this.appService.postEvents(ei, this.parameters, eventType)
                .subscribe(res => this.postEvent = res,
                err => this.appService.logError(this, err)
                );
        }
    }
}