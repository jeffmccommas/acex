"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var promo_service_1 = require("./promo.service");
var app_service_1 = require("../app.service");
var Promo = (function () {
    function Promo(promoService, appService, elementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.promoService = promoService;
        this.appService = appService;
        this.fromUTest = false;
        this.width = 0;
        if (this.promoService.res !== undefined) {
            this.result = promoService.res;
            this.fromUTest = this.promoService.fromUnitTest;
        }
    }
    Promo.prototype.ngOnInit = function () {
        this.loadpromos();
    };
    Promo.prototype.loadpromos = function () {
        var _this = this;
        this.result = {};
        if (!this.fromUTest) {
            this.promoService.GetPromos(this.tabKey, this.parameters)
                .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.loadImgbyWidth(); });
        }
        this.enableLoader = false;
    };
    Promo.prototype.loadImgbyWidth = function () {
        var childnodes = this.elementRef.nativeElement.childNodes;
        for (var i = 0; i < childnodes.length; i++) {
            if (childnodes[i].nodeName === "DIV") {
                var firstChildNode = childnodes[i];
                for (var j = 0; j < firstChildNode.childNodes.length; j++) {
                    if (firstChildNode.childNodes[j].nodeName === "DIV") {
                        var secondChildNode = firstChildNode.childNodes[j];
                        for (var k = 0; k < secondChildNode.childNodes.length; k++) {
                            if (secondChildNode.childNodes[k].nodeName === "DIV" && secondChildNode.childNodes[k].id === "iws_po_wrapper") {
                                this.width = secondChildNode.childNodes[k].offsetWidth;
                            }
                        }
                    }
                }
            }
        }
    };
    Promo.prototype.popUp = function (url, key) {
        this.postEvents(url, key, 'Click Promo Link');
        window.open(url);
    };
    Promo.prototype.postEvents = function (url, key, eventType) {
        var _this = this;
        var ei = [];
        ei.push({ key: "ActionId", value: key });
        ei.push({ key: "TabId", value: this.tabKey });
        if (url !== "") {
            ei.push({ key: "Link", value: url });
        }
        if (!this.fromUTest) {
            this.appService.postEvents(ei, this.parameters, eventType)
                .subscribe(function (res) { return _this.postEvent = res; }, function (err) { return _this.appService.logError(_this, err); });
        }
    };
    Promo = __decorate([
        core_1.Component({
            selector: 'promo',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"panel panel-default\"><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div></div><div *ngIf=\"!errorOccurred\"><div id=\"iws_po_wrapper\" class=\"promo panel panel-default\" style=\"width: 100%\" (window:resize)=\"loadImgbyWidth()\"><div *ngIf=\"result.ShowTitle == 'true'\" class=\"panel-heading\"><h1 role=\"presentation\">{{result.Title}}</h1></div><div *ngIf=\"result.ShowIntro == 'true'\" class=\"markdown\">{{result.IntroText}}</div><div *ngFor=\"let p of result | objToArr\"><span class=\"sr-only\">Opens in new window</span><div *ngIf=\"p.RebateMediumImageUrl !== '' && p.RebateMediumImageUrl.length > 0\"><img alt=\"{{p.PromoText}}\" *ngIf=\"width > p.ImageLowBreakPoint && width<=p.ImageHighBreakPoint\" class=\"img-responsive cursor__pointer\" [src]=\"p.RebateMediumImageUrl\" (click)=\"popUp(p.RebateUrl,p.Key)\" (load)=\"postEvents('',p.Key,'View Promo')\"> <img alt=\"{{p.PromoText}}\" *ngIf=\"width <= p.ImageLowBreakPoint\" class=\"img-responsive cursor__pointer\" [src]=\"p.RebateSmallImageUrl\" (click)=\"popUp(p.RebateUrl,p.Key)\" (load)=\"postEvents('',p.Key,'View Promo')\"> <img alt=\"{{p.PromoText}}\" *ngIf=\"width > p.ImageHighBreakPoint\" class=\"img-responsive cursor__pointer\" [src]=\"p.RebateLargeImageUrl\" (click)=\"popUp(p.RebateUrl,p.Key)\" (load)=\"postEvents('',p.Key,'View Promo')\"></div><div *ngIf=\"p.RebateMediumImageUrl == '' || p.RebateMediumImageUrl.length === 0\"><div *ngIf=\"p.PromoText  !== '' && p.PromotText.length > 0\" class=\"markdown\">{{p.PromoText}}</div></div></div><div *ngIf=\"result.ShowFooterText === 'true' || result.ShowFooterLink === 'true'\"><div class=\"panel-footer\"><div *ngIf=\"result.ShowFooterText === 'true'\" class=\"footertext\">{{result.Footer}}</div><div *ngIf=\"result.ShowFooterLink === 'true'\" class=\"footerlink\"><a href=\"{{result.FooterLink}}\">{{result.FooterLinkText}}</a></div></div></div></div></div></div>",
            providers: [promo_service_1.PromoService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [promo_service_1.PromoService, app_service_1.AppService, core_1.ElementRef])
    ], Promo);
    return Promo;
}());
exports.Promo = Promo;

//# sourceMappingURL=promo.js.map
