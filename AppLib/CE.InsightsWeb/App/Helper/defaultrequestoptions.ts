﻿import {Headers, BaseRequestOptions} from "@angular/http";
import {Injectable} from "@angular/core"

@Injectable()
export class DefaultRequestOptions extends BaseRequestOptions {
    headers: Headers = new Headers({
        "Cache-Control": "no-cache",
        "Pragma": "no-cache",
        "Content-Type": "application/json"
    });
}
