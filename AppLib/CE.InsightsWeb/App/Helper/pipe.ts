import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'objToArr'
})
export class ObjToArr implements PipeTransform {
    transform(object: any) {
        const newArray = [];
        for (let key in object) {
            if (object.hasOwnProperty(key)) {
                newArray.push(object[key]);
            }
        }
        return newArray;
    }
}