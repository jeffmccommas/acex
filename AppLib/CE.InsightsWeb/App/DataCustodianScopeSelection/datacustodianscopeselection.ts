﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { DataCustodianScopeSelectionService } from "./datacustodianscopeselection.service";
import { AppService, IAppParams } from "../app.service";

@Component({
    selector: 'datacustodianscopeselection',
    templateUrl: 'App/DataCustodianScopeSelection/datacustodianscopeselection.html',
    providers: [DataCustodianScopeSelectionService, AppService]
})
export class DataCustodianScopeSelection implements OnInit {
    elementRef: ElementRef;
    result: any;
    error: Object;
    postEvent: Object;
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    dataCustodianScopeSelectionService: DataCustodianScopeSelectionService;
    appService: AppService;
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string;
    fromUTest: Boolean;

    constructor(dataCustodianScopeSelectionService: DataCustodianScopeSelectionService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        //this.params = appService.getParams();
        this.parameters = "params";//this.params[0].parameters;
        this.tabKey = "1"; //this.params[0].tabKey;
        this.errorMessage = "error"; //this.params[0].errorMessage;
        this.dataCustodianScopeSelectionService = dataCustodianScopeSelectionService;
        this.appService = appService;
        this.fromUTest = false;

        if (this.dataCustodianScopeSelectionService.res !== undefined) {
            this.result = dataCustodianScopeSelectionService.res;
            this.fromUTest = this.dataCustodianScopeSelectionService.fromUnitTest;
        }

    }

    ngOnInit() {
        //this.loadDataCustodianScopeSelection();
    }

    loadDataCustodianScopeSelection() {
        this.result = {};
        this.dataCustodianScopeSelectionService.GetDataCustodianScopeSelection(this.tabKey, this.parameters)
            .subscribe(res => this.result = res,
            err => this.appService.logError(this, err)
            );

        this.enableLoader = false;
    }

    updateScopes() {

    }

}